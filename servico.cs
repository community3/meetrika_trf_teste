/*
               File: Servico
        Description: Servico
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:15:21.7
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class servico : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxJX_Action50") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            XC_50_0V130( ) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vSERVICO_UO") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GXDLVvSERVICO_UO0V130( ) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"SERVICO_VINCULADO") == 0 )
         {
            AV16ServicoGrupo_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16ServicoGrupo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16ServicoGrupo_Codigo), 6, 0)));
            AV7Servico_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Servico_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vSERVICO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7Servico_Codigo), "ZZZZZ9")));
            A157ServicoGrupo_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A157ServicoGrupo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A157ServicoGrupo_Codigo), 6, 0)));
            A640Servico_Vinculados = (short)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A640Servico_Vinculados", StringUtil.LTrim( StringUtil.Str( (decimal)(A640Servico_Vinculados), 4, 0)));
            A632Servico_Ativo = (bool)(BooleanUtil.Val(GetNextPar( )));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A632Servico_Ativo", A632Servico_Ativo);
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GXDLASERVICO_VINCULADO0V130( AV16ServicoGrupo_Codigo, AV7Servico_Codigo, A157ServicoGrupo_Codigo, A640Servico_Vinculados, A632Servico_Ativo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"SERVICO_ANTERIOR") == 0 )
         {
            AV7Servico_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Servico_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vSERVICO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7Servico_Codigo), "ZZZZZ9")));
            A632Servico_Ativo = (bool)(BooleanUtil.Val(GetNextPar( )));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A632Servico_Ativo", A632Servico_Ativo);
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GXDLASERVICO_ANTERIOR0V130( AV7Servico_Codigo, A632Servico_Ativo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"SERVICO_POSTERIOR") == 0 )
         {
            AV7Servico_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Servico_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vSERVICO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7Servico_Codigo), "ZZZZZ9")));
            A632Servico_Ativo = (bool)(BooleanUtil.Val(GetNextPar( )));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A632Servico_Ativo", A632Servico_Ativo);
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GXDLASERVICO_POSTERIOR0V130( AV7Servico_Codigo, A632Servico_Ativo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vRESPONSAVEL_CODIGO") == 0 )
         {
            ajax_req_read_hidden_sdt(GetNextPar( ), AV8WWPContext);
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GXDLVvRESPONSAVEL_CODIGO0V130( AV8WWPContext) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"SERVICO_LINNEGCOD") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GXDLASERVICO_LINNEGCOD0V130( ) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxAggSel7"+"_"+"SERVICO_RESPONSAVEL") == 0 )
         {
            A155Servico_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GX7ASASERVICO_RESPONSAVEL0V130( A155Servico_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxAggSel8"+"_"+"SERVICO_VINCULADOS") == 0 )
         {
            A155Servico_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GX8ASASERVICO_VINCULADOS0V130( A155Servico_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_52") == 0 )
         {
            A157ServicoGrupo_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A157ServicoGrupo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A157ServicoGrupo_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_52( A157ServicoGrupo_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_55") == 0 )
         {
            A631Servico_Vinculado = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n631Servico_Vinculado = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A631Servico_Vinculado", StringUtil.LTrim( StringUtil.Str( (decimal)(A631Servico_Vinculado), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_55( A631Servico_Vinculado) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_54") == 0 )
         {
            A2047Servico_LinNegCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n2047Servico_LinNegCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2047Servico_LinNegCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2047Servico_LinNegCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_54( A2047Servico_LinNegCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_53") == 0 )
         {
            A633Servico_UO = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n633Servico_UO = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A633Servico_UO", StringUtil.LTrim( StringUtil.Str( (decimal)(A633Servico_UO), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_53( A633Servico_UO) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV7Servico_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Servico_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vSERVICO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7Servico_Codigo), "ZZZZZ9")));
               AV16ServicoGrupo_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16ServicoGrupo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16ServicoGrupo_Codigo), 6, 0)));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         dynavServico_uo.Name = "vSERVICO_UO";
         dynavServico_uo.WebTags = "";
         cmbServico_UORespExclusiva.Name = "SERVICO_UORESPEXCLUSIVA";
         cmbServico_UORespExclusiva.WebTags = "";
         cmbServico_UORespExclusiva.addItem(StringUtil.BoolToStr( false), "Herdada pelas sub-unidades", 0);
         cmbServico_UORespExclusiva.addItem(StringUtil.BoolToStr( true), "Exclusiva", 0);
         if ( cmbServico_UORespExclusiva.ItemCount > 0 )
         {
            A1077Servico_UORespExclusiva = StringUtil.StrToBool( cmbServico_UORespExclusiva.getValidValue(StringUtil.BoolToStr( A1077Servico_UORespExclusiva)));
            n1077Servico_UORespExclusiva = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1077Servico_UORespExclusiva", A1077Servico_UORespExclusiva);
         }
         dynServicoGrupo_Codigo.Name = "SERVICOGRUPO_CODIGO";
         dynServicoGrupo_Codigo.WebTags = "";
         dynServicoGrupo_Codigo.removeAllItems();
         /* Using cursor T000V8 */
         pr_default.execute(6);
         while ( (pr_default.getStatus(6) != 101) )
         {
            dynServicoGrupo_Codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(T000V8_A157ServicoGrupo_Codigo[0]), 6, 0)), T000V8_A158ServicoGrupo_Descricao[0], 0);
            pr_default.readNext(6);
         }
         pr_default.close(6);
         if ( dynServicoGrupo_Codigo.ItemCount > 0 )
         {
            A157ServicoGrupo_Codigo = (int)(NumberUtil.Val( dynServicoGrupo_Codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A157ServicoGrupo_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A157ServicoGrupo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A157ServicoGrupo_Codigo), 6, 0)));
         }
         dynServico_Vinculado.Name = "SERVICO_VINCULADO";
         dynServico_Vinculado.WebTags = "";
         dynServico_Anterior.Name = "SERVICO_ANTERIOR";
         dynServico_Anterior.WebTags = "";
         dynServico_Posterior.Name = "SERVICO_POSTERIOR";
         dynServico_Posterior.WebTags = "";
         cmbServico_Terceriza.Name = "SERVICO_TERCERIZA";
         cmbServico_Terceriza.WebTags = "";
         cmbServico_Terceriza.addItem(StringUtil.BoolToStr( false), "Desconhecido", 0);
         cmbServico_Terceriza.addItem(StringUtil.BoolToStr( true), "Sim", 0);
         cmbServico_Terceriza.addItem(StringUtil.BoolToStr( false), "N�o", 0);
         if ( cmbServico_Terceriza.ItemCount > 0 )
         {
            A889Servico_Terceriza = StringUtil.StrToBool( cmbServico_Terceriza.getValidValue(StringUtil.BoolToStr( A889Servico_Terceriza)));
            n889Servico_Terceriza = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A889Servico_Terceriza", A889Servico_Terceriza);
         }
         cmbServico_Tela.Name = "SERVICO_TELA";
         cmbServico_Tela.WebTags = "";
         cmbServico_Tela.addItem("", "(Nenhuma)", 0);
         cmbServico_Tela.addItem("CNT", "Contagem", 0);
         cmbServico_Tela.addItem("CHK", "Check List", 0);
         if ( cmbServico_Tela.ItemCount > 0 )
         {
            A1061Servico_Tela = cmbServico_Tela.getValidValue(A1061Servico_Tela);
            n1061Servico_Tela = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1061Servico_Tela", A1061Servico_Tela);
         }
         cmbServico_Atende.Name = "SERVICO_ATENDE";
         cmbServico_Atende.WebTags = "";
         cmbServico_Atende.addItem("S", "Software", 0);
         cmbServico_Atende.addItem("H", "Hardware", 0);
         if ( cmbServico_Atende.ItemCount > 0 )
         {
            A1072Servico_Atende = cmbServico_Atende.getValidValue(A1072Servico_Atende);
            n1072Servico_Atende = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1072Servico_Atende", A1072Servico_Atende);
         }
         cmbServico_ObrigaValores.Name = "SERVICO_OBRIGAVALORES";
         cmbServico_ObrigaValores.WebTags = "";
         cmbServico_ObrigaValores.addItem("", "Nenhum", 0);
         cmbServico_ObrigaValores.addItem("L", "Valor L�quido", 0);
         cmbServico_ObrigaValores.addItem("B", "Valor Bruto", 0);
         cmbServico_ObrigaValores.addItem("A", "Ambos", 0);
         if ( cmbServico_ObrigaValores.ItemCount > 0 )
         {
            A1429Servico_ObrigaValores = cmbServico_ObrigaValores.getValidValue(A1429Servico_ObrigaValores);
            n1429Servico_ObrigaValores = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1429Servico_ObrigaValores", A1429Servico_ObrigaValores);
         }
         cmbServico_ObjetoControle.Name = "SERVICO_OBJETOCONTROLE";
         cmbServico_ObjetoControle.WebTags = "";
         cmbServico_ObjetoControle.addItem("SIS", "Sistema", 0);
         cmbServico_ObjetoControle.addItem("PRC", "Processo", 0);
         cmbServico_ObjetoControle.addItem("PRD", "Produto", 0);
         if ( cmbServico_ObjetoControle.ItemCount > 0 )
         {
            A1436Servico_ObjetoControle = cmbServico_ObjetoControle.getValidValue(A1436Servico_ObjetoControle);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1436Servico_ObjetoControle", A1436Servico_ObjetoControle);
         }
         cmbServico_IsPublico.Name = "SERVICO_ISPUBLICO";
         cmbServico_IsPublico.WebTags = "";
         cmbServico_IsPublico.addItem(StringUtil.BoolToStr( false), "N�o", 0);
         cmbServico_IsPublico.addItem(StringUtil.BoolToStr( true), "Sim", 0);
         if ( cmbServico_IsPublico.ItemCount > 0 )
         {
            if ( (false==A1635Servico_IsPublico) )
            {
               A1635Servico_IsPublico = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1635Servico_IsPublico", A1635Servico_IsPublico);
            }
            A1635Servico_IsPublico = StringUtil.StrToBool( cmbServico_IsPublico.getValidValue(StringUtil.BoolToStr( A1635Servico_IsPublico)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1635Servico_IsPublico", A1635Servico_IsPublico);
         }
         dynavResponsavel_codigo.Name = "vRESPONSAVEL_CODIGO";
         dynavResponsavel_codigo.WebTags = "";
         dynServico_LinNegCod.Name = "SERVICO_LINNEGCOD";
         dynServico_LinNegCod.WebTags = "";
         cmbServico_IsOrigemReferencia.Name = "SERVICO_ISORIGEMREFERENCIA";
         cmbServico_IsOrigemReferencia.WebTags = "";
         cmbServico_IsOrigemReferencia.addItem(StringUtil.BoolToStr( false), "N�o", 0);
         cmbServico_IsOrigemReferencia.addItem(StringUtil.BoolToStr( true), "Sim", 0);
         if ( cmbServico_IsOrigemReferencia.ItemCount > 0 )
         {
            A2092Servico_IsOrigemReferencia = StringUtil.StrToBool( cmbServico_IsOrigemReferencia.getValidValue(StringUtil.BoolToStr( A2092Servico_IsOrigemReferencia)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2092Servico_IsOrigemReferencia", A2092Servico_IsOrigemReferencia);
         }
         cmbServico_PausaSLA.Name = "SERVICO_PAUSASLA";
         cmbServico_PausaSLA.WebTags = "";
         cmbServico_PausaSLA.addItem(StringUtil.BoolToStr( false), "N�o", 0);
         cmbServico_PausaSLA.addItem(StringUtil.BoolToStr( true), "Sim", 0);
         if ( cmbServico_PausaSLA.ItemCount > 0 )
         {
            if ( (false==A2131Servico_PausaSLA) )
            {
               A2131Servico_PausaSLA = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2131Servico_PausaSLA", A2131Servico_PausaSLA);
            }
            A2131Servico_PausaSLA = StringUtil.StrToBool( cmbServico_PausaSLA.getValidValue(StringUtil.BoolToStr( A2131Servico_PausaSLA)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2131Servico_PausaSLA", A2131Servico_PausaSLA);
         }
         cmbServico_Ativo.Name = "SERVICO_ATIVO";
         cmbServico_Ativo.WebTags = "";
         cmbServico_Ativo.addItem(StringUtil.BoolToStr( false), "N�o", 0);
         cmbServico_Ativo.addItem(StringUtil.BoolToStr( true), "Sim", 0);
         if ( cmbServico_Ativo.ItemCount > 0 )
         {
            if ( (false==A632Servico_Ativo) )
            {
               A632Servico_Ativo = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A632Servico_Ativo", A632Servico_Ativo);
            }
            A632Servico_Ativo = StringUtil.StrToBool( cmbServico_Ativo.getValidValue(StringUtil.BoolToStr( A632Servico_Ativo)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A632Servico_Ativo", A632Servico_Ativo);
         }
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Servico", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = cmbServico_UORespExclusiva_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public servico( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public servico( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           int aP1_Servico_Codigo ,
                           ref int aP2_ServicoGrupo_Codigo )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV7Servico_Codigo = aP1_Servico_Codigo;
         this.AV16ServicoGrupo_Codigo = aP2_ServicoGrupo_Codigo;
         executePrivate();
         aP2_ServicoGrupo_Codigo=this.AV16ServicoGrupo_Codigo;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         dynavServico_uo = new GXCombobox();
         cmbServico_UORespExclusiva = new GXCombobox();
         dynServicoGrupo_Codigo = new GXCombobox();
         dynServico_Vinculado = new GXCombobox();
         dynServico_Anterior = new GXCombobox();
         dynServico_Posterior = new GXCombobox();
         cmbServico_Terceriza = new GXCombobox();
         cmbServico_Tela = new GXCombobox();
         cmbServico_Atende = new GXCombobox();
         cmbServico_ObrigaValores = new GXCombobox();
         cmbServico_ObjetoControle = new GXCombobox();
         cmbServico_IsPublico = new GXCombobox();
         dynavResponsavel_codigo = new GXCombobox();
         dynServico_LinNegCod = new GXCombobox();
         cmbServico_IsOrigemReferencia = new GXCombobox();
         cmbServico_PausaSLA = new GXCombobox();
         cmbServico_Ativo = new GXCombobox();
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
         if ( dynavServico_uo.ItemCount > 0 )
         {
            AV17Servico_UO = (int)(NumberUtil.Val( dynavServico_uo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV17Servico_UO), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Servico_UO", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17Servico_UO), 6, 0)));
         }
         if ( cmbServico_UORespExclusiva.ItemCount > 0 )
         {
            A1077Servico_UORespExclusiva = StringUtil.StrToBool( cmbServico_UORespExclusiva.getValidValue(StringUtil.BoolToStr( A1077Servico_UORespExclusiva)));
            n1077Servico_UORespExclusiva = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1077Servico_UORespExclusiva", A1077Servico_UORespExclusiva);
         }
         if ( dynServicoGrupo_Codigo.ItemCount > 0 )
         {
            A157ServicoGrupo_Codigo = (int)(NumberUtil.Val( dynServicoGrupo_Codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A157ServicoGrupo_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A157ServicoGrupo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A157ServicoGrupo_Codigo), 6, 0)));
         }
         if ( dynServico_Vinculado.ItemCount > 0 )
         {
            A631Servico_Vinculado = (int)(NumberUtil.Val( dynServico_Vinculado.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A631Servico_Vinculado), 6, 0))), "."));
            n631Servico_Vinculado = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A631Servico_Vinculado", StringUtil.LTrim( StringUtil.Str( (decimal)(A631Servico_Vinculado), 6, 0)));
         }
         if ( dynServico_Anterior.ItemCount > 0 )
         {
            A1545Servico_Anterior = (int)(NumberUtil.Val( dynServico_Anterior.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1545Servico_Anterior), 6, 0))), "."));
            n1545Servico_Anterior = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1545Servico_Anterior", StringUtil.LTrim( StringUtil.Str( (decimal)(A1545Servico_Anterior), 6, 0)));
         }
         if ( dynServico_Posterior.ItemCount > 0 )
         {
            A1546Servico_Posterior = (int)(NumberUtil.Val( dynServico_Posterior.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1546Servico_Posterior), 6, 0))), "."));
            n1546Servico_Posterior = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1546Servico_Posterior", StringUtil.LTrim( StringUtil.Str( (decimal)(A1546Servico_Posterior), 6, 0)));
         }
         if ( cmbServico_Terceriza.ItemCount > 0 )
         {
            A889Servico_Terceriza = StringUtil.StrToBool( cmbServico_Terceriza.getValidValue(StringUtil.BoolToStr( A889Servico_Terceriza)));
            n889Servico_Terceriza = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A889Servico_Terceriza", A889Servico_Terceriza);
         }
         if ( cmbServico_Tela.ItemCount > 0 )
         {
            A1061Servico_Tela = cmbServico_Tela.getValidValue(A1061Servico_Tela);
            n1061Servico_Tela = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1061Servico_Tela", A1061Servico_Tela);
         }
         if ( cmbServico_Atende.ItemCount > 0 )
         {
            A1072Servico_Atende = cmbServico_Atende.getValidValue(A1072Servico_Atende);
            n1072Servico_Atende = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1072Servico_Atende", A1072Servico_Atende);
         }
         if ( cmbServico_ObrigaValores.ItemCount > 0 )
         {
            A1429Servico_ObrigaValores = cmbServico_ObrigaValores.getValidValue(A1429Servico_ObrigaValores);
            n1429Servico_ObrigaValores = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1429Servico_ObrigaValores", A1429Servico_ObrigaValores);
         }
         if ( cmbServico_ObjetoControle.ItemCount > 0 )
         {
            A1436Servico_ObjetoControle = cmbServico_ObjetoControle.getValidValue(A1436Servico_ObjetoControle);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1436Servico_ObjetoControle", A1436Servico_ObjetoControle);
         }
         if ( cmbServico_IsPublico.ItemCount > 0 )
         {
            A1635Servico_IsPublico = StringUtil.StrToBool( cmbServico_IsPublico.getValidValue(StringUtil.BoolToStr( A1635Servico_IsPublico)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1635Servico_IsPublico", A1635Servico_IsPublico);
         }
         if ( dynavResponsavel_codigo.ItemCount > 0 )
         {
            AV22Responsavel_Codigo = (int)(NumberUtil.Val( dynavResponsavel_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV22Responsavel_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Responsavel_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22Responsavel_Codigo), 6, 0)));
         }
         if ( dynServico_LinNegCod.ItemCount > 0 )
         {
            A2047Servico_LinNegCod = (int)(NumberUtil.Val( dynServico_LinNegCod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A2047Servico_LinNegCod), 6, 0))), "."));
            n2047Servico_LinNegCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2047Servico_LinNegCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2047Servico_LinNegCod), 6, 0)));
         }
         if ( cmbServico_IsOrigemReferencia.ItemCount > 0 )
         {
            A2092Servico_IsOrigemReferencia = StringUtil.StrToBool( cmbServico_IsOrigemReferencia.getValidValue(StringUtil.BoolToStr( A2092Servico_IsOrigemReferencia)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2092Servico_IsOrigemReferencia", A2092Servico_IsOrigemReferencia);
         }
         if ( cmbServico_PausaSLA.ItemCount > 0 )
         {
            A2131Servico_PausaSLA = StringUtil.StrToBool( cmbServico_PausaSLA.getValidValue(StringUtil.BoolToStr( A2131Servico_PausaSLA)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2131Servico_PausaSLA", A2131Servico_PausaSLA);
         }
         if ( cmbServico_Ativo.ItemCount > 0 )
         {
            A632Servico_Ativo = StringUtil.StrToBool( cmbServico_Ativo.getValidValue(StringUtil.BoolToStr( A632Servico_Ativo)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A632Servico_Ativo", A632Servico_Ativo);
         }
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_0V130( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_0V130e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtavContratante_codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV20Contratante_Codigo), 6, 0, ",", "")), ((edtavContratante_codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV20Contratante_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(AV20Contratante_Codigo), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratante_codigo_Jsonclick, 0, "Attribute", "", "", "", edtavContratante_codigo_Visible, edtavContratante_codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Servico.htm");
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtServico_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A155Servico_Codigo), 6, 0, ",", "")), ((edtServico_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A155Servico_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A155Servico_Codigo), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtServico_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtServico_Codigo_Visible, edtServico_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_Servico.htm");
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_0V130( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_0V130( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_0V130e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_155_0V130( true) ;
         }
         return  ;
      }

      protected void wb_table3_155_0V130e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_0V130e( true) ;
         }
         else
         {
            wb_table1_2_0V130e( false) ;
         }
      }

      protected void wb_table3_155_0V130( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 158,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", "Confirmar", bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_trn_enter_Visible, bttBtn_trn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_Servico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 160,'',false,'',0)\"";
            ClassString = "BtnDelete";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_delete_Internalname, "", "Eliminar", bttBtn_trn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_trn_delete_Visible, bttBtn_trn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_Servico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 162,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "", "Fechar", bttBtn_trn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_trn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_Servico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_155_0V130e( true) ;
         }
         else
         {
            wb_table3_155_0V130e( false) ;
         }
      }

      protected void wb_table2_5_0V130( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table4_13_0V130( true) ;
         }
         return  ;
      }

      protected void wb_table4_13_0V130e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_0V130e( true) ;
         }
         else
         {
            wb_table2_5_0V130e( false) ;
         }
      }

      protected void wb_table4_13_0V130( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableData", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservico_uo_Internalname, "Unidade Organizacional", "", "", lblTextblockservico_uo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Servico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table5_18_0V130( true) ;
         }
         return  ;
      }

      protected void wb_table5_18_0V130e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservico_uorespexclusiva_Internalname, "Visibilidade", "", "", lblTextblockservico_uorespexclusiva_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Servico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 27,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbServico_UORespExclusiva, cmbServico_UORespExclusiva_Internalname, StringUtil.BoolToStr( A1077Servico_UORespExclusiva), 1, cmbServico_UORespExclusiva_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "boolean", "", 1, cmbServico_UORespExclusiva.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,27);\"", "", true, "HLP_Servico.htm");
            cmbServico_UORespExclusiva.CurrentValue = StringUtil.BoolToStr( A1077Servico_UORespExclusiva);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbServico_UORespExclusiva_Internalname, "Values", (String)(cmbServico_UORespExclusiva.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservicogrupo_codigo_Internalname, "Grupo de Servi�os", "", "", lblTextblockservicogrupo_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Servico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 34,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynServicoGrupo_Codigo, dynServicoGrupo_Codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A157ServicoGrupo_Codigo), 6, 0)), 1, dynServicoGrupo_Codigo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, dynServicoGrupo_Codigo.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,34);\"", "", true, "HLP_Servico.htm");
            dynServicoGrupo_Codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A157ServicoGrupo_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynServicoGrupo_Codigo_Internalname, "Values", (String)(dynServicoGrupo_Codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservico_nome_Internalname, "Nome", "", "", lblTextblockservico_nome_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Servico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='RequiredDataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 43,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtServico_Nome_Internalname, StringUtil.RTrim( A608Servico_Nome), StringUtil.RTrim( context.localUtil.Format( A608Servico_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,43);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtServico_Nome_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtServico_Nome_Enabled, 0, "text", "", 380, "px", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_Servico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservico_sigla_Internalname, "Sigla", "", "", lblTextblockservico_sigla_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Servico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='RequiredDataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 47,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtServico_Sigla_Internalname, StringUtil.RTrim( A605Servico_Sigla), StringUtil.RTrim( context.localUtil.Format( A605Servico_Sigla, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,47);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtServico_Sigla_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtServico_Sigla_Enabled, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "Sigla", "left", true, "HLP_Servico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservico_vinculado_Internalname, "Servi�o vinculado", "", "", lblTextblockservico_vinculado_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Servico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 51,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynServico_Vinculado, dynServico_Vinculado_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A631Servico_Vinculado), 6, 0)), 1, dynServico_Vinculado_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, dynServico_Vinculado.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,51);\"", "", true, "HLP_Servico.htm");
            dynServico_Vinculado.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A631Servico_Vinculado), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynServico_Vinculado_Internalname, "Values", (String)(dynServico_Vinculado.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservico_descricao_Internalname, "Descri��o", "", "", lblTextblockservico_descricao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Servico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"5\"  class='DataContentCell'>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 56,'',false,'',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtServico_Descricao_Internalname, A156Servico_Descricao, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,56);\"", 0, 1, edtServico_Descricao_Enabled, 0, 600, "px", 2, "row", StyleString, ClassString, "", "500", 1, "", "", -1, true, "DescricaoLonga", "HLP_Servico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservico_anterior_Internalname, "Servico Anterior", "", "", lblTextblockservico_anterior_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Servico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 61,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynServico_Anterior, dynServico_Anterior_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A1545Servico_Anterior), 6, 0)), 1, dynServico_Anterior_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, dynServico_Anterior.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,61);\"", "", true, "HLP_Servico.htm");
            dynServico_Anterior.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1545Servico_Anterior), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynServico_Anterior_Internalname, "Values", (String)(dynServico_Anterior.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservico_posterior_Internalname, "Servico Posterior", "", "", lblTextblockservico_posterior_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Servico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 65,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynServico_Posterior, dynServico_Posterior_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A1546Servico_Posterior), 6, 0)), 1, dynServico_Posterior_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, dynServico_Posterior.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,65);\"", "", true, "HLP_Servico.htm");
            dynServico_Posterior.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1546Servico_Posterior), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynServico_Posterior_Internalname, "Values", (String)(dynServico_Posterior.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservico_terceriza_Internalname, "Pode ser tercerizado?", "", "", lblTextblockservico_terceriza_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Servico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 72,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbServico_Terceriza, cmbServico_Terceriza_Internalname, StringUtil.BoolToStr( A889Servico_Terceriza), 1, cmbServico_Terceriza_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "boolean", "", 1, cmbServico_Terceriza.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,72);\"", "", true, "HLP_Servico.htm");
            cmbServico_Terceriza.CurrentValue = StringUtil.BoolToStr( A889Servico_Terceriza);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbServico_Terceriza_Internalname, "Values", (String)(cmbServico_Terceriza.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservico_tela_Internalname, "Tela", "", "", lblTextblockservico_tela_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Servico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 76,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbServico_Tela, cmbServico_Tela_Internalname, StringUtil.RTrim( A1061Servico_Tela), 1, cmbServico_Tela_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, cmbServico_Tela.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,76);\"", "", true, "HLP_Servico.htm");
            cmbServico_Tela.CurrentValue = StringUtil.RTrim( A1061Servico_Tela);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbServico_Tela_Internalname, "Values", (String)(cmbServico_Tela.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservico_atende_Internalname, "Atende", "", "", lblTextblockservico_atende_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Servico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 83,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbServico_Atende, cmbServico_Atende_Internalname, StringUtil.RTrim( A1072Servico_Atende), 1, cmbServico_Atende_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, cmbServico_Atende.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,83);\"", "", true, "HLP_Servico.htm");
            cmbServico_Atende.CurrentValue = StringUtil.RTrim( A1072Servico_Atende);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbServico_Atende_Internalname, "Values", (String)(cmbServico_Atende.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservico_obrigavalores_Internalname, "Obriga Valores", "", "", lblTextblockservico_obrigavalores_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Servico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 87,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbServico_ObrigaValores, cmbServico_ObrigaValores_Internalname, StringUtil.RTrim( A1429Servico_ObrigaValores), 1, cmbServico_ObrigaValores_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, cmbServico_ObrigaValores.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,87);\"", "", true, "HLP_Servico.htm");
            cmbServico_ObrigaValores.CurrentValue = StringUtil.RTrim( A1429Servico_ObrigaValores);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbServico_ObrigaValores_Internalname, "Values", (String)(cmbServico_ObrigaValores.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservico_objetocontrole_Internalname, "Objeto de Controle", "", "", lblTextblockservico_objetocontrole_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Servico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 91,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbServico_ObjetoControle, cmbServico_ObjetoControle_Internalname, StringUtil.RTrim( A1436Servico_ObjetoControle), 1, cmbServico_ObjetoControle_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, cmbServico_ObjetoControle.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,91);\"", "", true, "HLP_Servico.htm");
            cmbServico_ObjetoControle.CurrentValue = StringUtil.RTrim( A1436Servico_ObjetoControle);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbServico_ObjetoControle_Internalname, "Values", (String)(cmbServico_ObjetoControle.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservico_percpgm_Internalname, "Perc. de Pagamento", "", "", lblTextblockservico_percpgm_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Servico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table6_96_0V130( true) ;
         }
         return  ;
      }

      protected void wb_table6_96_0V130e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservico_perccnc_Internalname, "Perc. de Cancelamento", "", "", lblTextblockservico_perccnc_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Servico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table7_105_0V130( true) ;
         }
         return  ;
      }

      protected void wb_table7_105_0V130e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellTextblockservico_perctmp_cell_Internalname+"\"  class='"+cellTextblockservico_perctmp_cell_Class+"'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservico_perctmp_Internalname, "Perc. de Tempo", "", "", lblTextblockservico_perctmp_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Servico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellServico_perctmp_cell_Internalname+"\"  class='"+cellServico_perctmp_cell_Class+"'>") ;
            wb_table8_114_0V130( true) ;
         }
         return  ;
      }

      protected void wb_table8_114_0V130e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservico_ispublico_Internalname, "P�blico?", "", "", lblTextblockservico_ispublico_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Servico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 124,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbServico_IsPublico, cmbServico_IsPublico_Internalname, StringUtil.BoolToStr( A1635Servico_IsPublico), 1, cmbServico_IsPublico_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"ESERVICO_ISPUBLICO.CLICK."+"'", "boolean", "", 1, cmbServico_IsPublico.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,124);\"", "", true, "HLP_Servico.htm");
            cmbServico_IsPublico.CurrentValue = StringUtil.BoolToStr( A1635Servico_IsPublico);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbServico_IsPublico_Internalname, "Values", (String)(cmbServico_IsPublico.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockresponsavel_codigo_Internalname, "Respons�vel", "", "", lblTextblockresponsavel_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", lblTextblockresponsavel_codigo_Visible, 1, 0, "HLP_Servico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 128,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavResponsavel_codigo, dynavResponsavel_codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV22Responsavel_Codigo), 6, 0)), 1, dynavResponsavel_codigo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", dynavResponsavel_codigo.Visible, dynavResponsavel_codigo.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,128);\"", "", true, "HLP_Servico.htm");
            dynavResponsavel_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22Responsavel_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavResponsavel_codigo_Internalname, "Values", (String)(dynavResponsavel_codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservico_linnegcod_Internalname, "Linha de Neg�cio", "", "", lblTextblockservico_linnegcod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Servico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 135,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynServico_LinNegCod, dynServico_LinNegCod_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A2047Servico_LinNegCod), 6, 0)), 1, dynServico_LinNegCod_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, dynServico_LinNegCod.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,135);\"", "", true, "HLP_Servico.htm");
            dynServico_LinNegCod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A2047Servico_LinNegCod), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynServico_LinNegCod_Internalname, "Values", (String)(dynServico_LinNegCod.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservico_isorigemreferencia_Internalname, "Exige Origem de Refer�ncia", "", "", lblTextblockservico_isorigemreferencia_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Servico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 139,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbServico_IsOrigemReferencia, cmbServico_IsOrigemReferencia_Internalname, StringUtil.BoolToStr( A2092Servico_IsOrigemReferencia), 1, cmbServico_IsOrigemReferencia_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "boolean", "", 1, cmbServico_IsOrigemReferencia.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,139);\"", "", true, "HLP_Servico.htm");
            cmbServico_IsOrigemReferencia.CurrentValue = StringUtil.BoolToStr( A2092Servico_IsOrigemReferencia);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbServico_IsOrigemReferencia_Internalname, "Values", (String)(cmbServico_IsOrigemReferencia.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservico_pausasla_Internalname, "Pausar o prazo da SLA?", "", "", lblTextblockservico_pausasla_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Servico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 143,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbServico_PausaSLA, cmbServico_PausaSLA_Internalname, StringUtil.BoolToStr( A2131Servico_PausaSLA), 1, cmbServico_PausaSLA_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "boolean", "", 1, cmbServico_PausaSLA.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,143);\"", "", true, "HLP_Servico.htm");
            cmbServico_PausaSLA.CurrentValue = StringUtil.BoolToStr( A2131Servico_PausaSLA);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbServico_PausaSLA_Internalname, "Values", (String)(cmbServico_PausaSLA.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservico_ativo_Internalname, "Ativo?", "", "", lblTextblockservico_ativo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", lblTextblockservico_ativo_Visible, 1, 0, "HLP_Servico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 148,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbServico_Ativo, cmbServico_Ativo_Internalname, StringUtil.BoolToStr( A632Servico_Ativo), 1, cmbServico_Ativo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "boolean", "", cmbServico_Ativo.Visible, cmbServico_Ativo.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,148);\"", "", true, "HLP_Servico.htm");
            cmbServico_Ativo.CurrentValue = StringUtil.BoolToStr( A632Servico_Ativo);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbServico_Ativo_Internalname, "Values", (String)(cmbServico_Ativo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_13_0V130e( true) ;
         }
         else
         {
            wb_table4_13_0V130e( false) ;
         }
      }

      protected void wb_table8_114_0V130( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedservico_perctmp_Internalname, tblTablemergedservico_perctmp_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 117,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtServico_PercTmp_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1534Servico_PercTmp), 4, 0, ",", "")), ((edtServico_PercTmp_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1534Servico_PercTmp), "ZZZ9")) : context.localUtil.Format( (decimal)(A1534Servico_PercTmp), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,117);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtServico_PercTmp_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtServico_PercTmp_Visible, edtServico_PercTmp_Enabled, 0, "text", "", 40, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "center", false, "HLP_Servico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblServico_perctmp_righttext_Internalname, "�%", "", "", lblServico_perctmp_righttext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Servico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_114_0V130e( true) ;
         }
         else
         {
            wb_table8_114_0V130e( false) ;
         }
      }

      protected void wb_table7_105_0V130( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedservico_perccnc_Internalname, tblTablemergedservico_perccnc_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 108,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtServico_PercCnc_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1536Servico_PercCnc), 4, 0, ",", "")), ((edtServico_PercCnc_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1536Servico_PercCnc), "ZZZ9")) : context.localUtil.Format( (decimal)(A1536Servico_PercCnc), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,108);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtServico_PercCnc_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtServico_PercCnc_Enabled, 0, "text", "", 40, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "center", false, "HLP_Servico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblServico_perccnc_righttext_Internalname, "�%", "", "", lblServico_perccnc_righttext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Servico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_105_0V130e( true) ;
         }
         else
         {
            wb_table7_105_0V130e( false) ;
         }
      }

      protected void wb_table6_96_0V130( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedservico_percpgm_Internalname, tblTablemergedservico_percpgm_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 99,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtServico_PercPgm_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1535Servico_PercPgm), 4, 0, ",", "")), ((edtServico_PercPgm_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1535Servico_PercPgm), "ZZZ9")) : context.localUtil.Format( (decimal)(A1535Servico_PercPgm), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,99);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtServico_PercPgm_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtServico_PercPgm_Enabled, 0, "text", "", 40, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "center", false, "HLP_Servico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblServico_percpgm_righttext_Internalname, "�%", "", "", lblServico_percpgm_righttext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Servico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_96_0V130e( true) ;
         }
         else
         {
            wb_table6_96_0V130e( false) ;
         }
      }

      protected void wb_table5_18_0V130( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedservico_uo_Internalname, tblTablemergedservico_uo_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavServico_uo, dynavServico_uo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV17Servico_UO), 6, 0)), 1, dynavServico_uo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, dynavServico_uo.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,21);\"", "", true, "HLP_Servico.htm");
            dynavServico_uo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17Servico_UO), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavServico_uo_Internalname, "Values", (String)(dynavServico_uo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgSelectuo_Internalname, context.GetImagePath( "f5b04895-0024-488b-8e3b-b687ca4598ee", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgSelectuo_Visible, 1, "", "Selecionar Unidade Organizacional", 0, 0, 0, "px", 0, "px", 0, 0, 7, imgSelectuo_Jsonclick, "'"+""+"'"+",false,"+"'"+"e110v130_client"+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Servico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_18_0V130e( true) ;
         }
         else
         {
            wb_table5_18_0V130e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E120V2 */
         E120V2 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               dynavServico_uo.CurrentValue = cgiGet( dynavServico_uo_Internalname);
               AV17Servico_UO = (int)(NumberUtil.Val( cgiGet( dynavServico_uo_Internalname), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Servico_UO", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17Servico_UO), 6, 0)));
               cmbServico_UORespExclusiva.CurrentValue = cgiGet( cmbServico_UORespExclusiva_Internalname);
               A1077Servico_UORespExclusiva = StringUtil.StrToBool( cgiGet( cmbServico_UORespExclusiva_Internalname));
               n1077Servico_UORespExclusiva = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1077Servico_UORespExclusiva", A1077Servico_UORespExclusiva);
               n1077Servico_UORespExclusiva = ((false==A1077Servico_UORespExclusiva) ? true : false);
               dynServicoGrupo_Codigo.CurrentValue = cgiGet( dynServicoGrupo_Codigo_Internalname);
               A157ServicoGrupo_Codigo = (int)(NumberUtil.Val( cgiGet( dynServicoGrupo_Codigo_Internalname), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A157ServicoGrupo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A157ServicoGrupo_Codigo), 6, 0)));
               A608Servico_Nome = StringUtil.Upper( cgiGet( edtServico_Nome_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A608Servico_Nome", A608Servico_Nome);
               A605Servico_Sigla = StringUtil.Upper( cgiGet( edtServico_Sigla_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A605Servico_Sigla", A605Servico_Sigla);
               dynServico_Vinculado.CurrentValue = cgiGet( dynServico_Vinculado_Internalname);
               A631Servico_Vinculado = (int)(NumberUtil.Val( cgiGet( dynServico_Vinculado_Internalname), "."));
               n631Servico_Vinculado = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A631Servico_Vinculado", StringUtil.LTrim( StringUtil.Str( (decimal)(A631Servico_Vinculado), 6, 0)));
               n631Servico_Vinculado = ((0==A631Servico_Vinculado) ? true : false);
               A156Servico_Descricao = cgiGet( edtServico_Descricao_Internalname);
               n156Servico_Descricao = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A156Servico_Descricao", A156Servico_Descricao);
               n156Servico_Descricao = (String.IsNullOrEmpty(StringUtil.RTrim( A156Servico_Descricao)) ? true : false);
               dynServico_Anterior.CurrentValue = cgiGet( dynServico_Anterior_Internalname);
               A1545Servico_Anterior = (int)(NumberUtil.Val( cgiGet( dynServico_Anterior_Internalname), "."));
               n1545Servico_Anterior = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1545Servico_Anterior", StringUtil.LTrim( StringUtil.Str( (decimal)(A1545Servico_Anterior), 6, 0)));
               n1545Servico_Anterior = ((0==A1545Servico_Anterior) ? true : false);
               dynServico_Posterior.CurrentValue = cgiGet( dynServico_Posterior_Internalname);
               A1546Servico_Posterior = (int)(NumberUtil.Val( cgiGet( dynServico_Posterior_Internalname), "."));
               n1546Servico_Posterior = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1546Servico_Posterior", StringUtil.LTrim( StringUtil.Str( (decimal)(A1546Servico_Posterior), 6, 0)));
               n1546Servico_Posterior = ((0==A1546Servico_Posterior) ? true : false);
               cmbServico_Terceriza.CurrentValue = cgiGet( cmbServico_Terceriza_Internalname);
               A889Servico_Terceriza = StringUtil.StrToBool( cgiGet( cmbServico_Terceriza_Internalname));
               n889Servico_Terceriza = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A889Servico_Terceriza", A889Servico_Terceriza);
               n889Servico_Terceriza = ((false==A889Servico_Terceriza) ? true : false);
               cmbServico_Tela.CurrentValue = cgiGet( cmbServico_Tela_Internalname);
               A1061Servico_Tela = cgiGet( cmbServico_Tela_Internalname);
               n1061Servico_Tela = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1061Servico_Tela", A1061Servico_Tela);
               n1061Servico_Tela = (String.IsNullOrEmpty(StringUtil.RTrim( A1061Servico_Tela)) ? true : false);
               cmbServico_Atende.CurrentValue = cgiGet( cmbServico_Atende_Internalname);
               A1072Servico_Atende = cgiGet( cmbServico_Atende_Internalname);
               n1072Servico_Atende = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1072Servico_Atende", A1072Servico_Atende);
               n1072Servico_Atende = (String.IsNullOrEmpty(StringUtil.RTrim( A1072Servico_Atende)) ? true : false);
               cmbServico_ObrigaValores.CurrentValue = cgiGet( cmbServico_ObrigaValores_Internalname);
               A1429Servico_ObrigaValores = cgiGet( cmbServico_ObrigaValores_Internalname);
               n1429Servico_ObrigaValores = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1429Servico_ObrigaValores", A1429Servico_ObrigaValores);
               n1429Servico_ObrigaValores = (String.IsNullOrEmpty(StringUtil.RTrim( A1429Servico_ObrigaValores)) ? true : false);
               cmbServico_ObjetoControle.CurrentValue = cgiGet( cmbServico_ObjetoControle_Internalname);
               A1436Servico_ObjetoControle = cgiGet( cmbServico_ObjetoControle_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1436Servico_ObjetoControle", A1436Servico_ObjetoControle);
               if ( ( ( context.localUtil.CToN( cgiGet( edtServico_PercPgm_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtServico_PercPgm_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "SERVICO_PERCPGM");
                  AnyError = 1;
                  GX_FocusControl = edtServico_PercPgm_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1535Servico_PercPgm = 0;
                  n1535Servico_PercPgm = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1535Servico_PercPgm", StringUtil.LTrim( StringUtil.Str( (decimal)(A1535Servico_PercPgm), 4, 0)));
               }
               else
               {
                  A1535Servico_PercPgm = (short)(context.localUtil.CToN( cgiGet( edtServico_PercPgm_Internalname), ",", "."));
                  n1535Servico_PercPgm = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1535Servico_PercPgm", StringUtil.LTrim( StringUtil.Str( (decimal)(A1535Servico_PercPgm), 4, 0)));
               }
               n1535Servico_PercPgm = ((0==A1535Servico_PercPgm) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtServico_PercCnc_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtServico_PercCnc_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "SERVICO_PERCCNC");
                  AnyError = 1;
                  GX_FocusControl = edtServico_PercCnc_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1536Servico_PercCnc = 0;
                  n1536Servico_PercCnc = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1536Servico_PercCnc", StringUtil.LTrim( StringUtil.Str( (decimal)(A1536Servico_PercCnc), 4, 0)));
               }
               else
               {
                  A1536Servico_PercCnc = (short)(context.localUtil.CToN( cgiGet( edtServico_PercCnc_Internalname), ",", "."));
                  n1536Servico_PercCnc = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1536Servico_PercCnc", StringUtil.LTrim( StringUtil.Str( (decimal)(A1536Servico_PercCnc), 4, 0)));
               }
               n1536Servico_PercCnc = ((0==A1536Servico_PercCnc) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtServico_PercTmp_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtServico_PercTmp_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "SERVICO_PERCTMP");
                  AnyError = 1;
                  GX_FocusControl = edtServico_PercTmp_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1534Servico_PercTmp = 0;
                  n1534Servico_PercTmp = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1534Servico_PercTmp", StringUtil.LTrim( StringUtil.Str( (decimal)(A1534Servico_PercTmp), 4, 0)));
               }
               else
               {
                  A1534Servico_PercTmp = (short)(context.localUtil.CToN( cgiGet( edtServico_PercTmp_Internalname), ",", "."));
                  n1534Servico_PercTmp = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1534Servico_PercTmp", StringUtil.LTrim( StringUtil.Str( (decimal)(A1534Servico_PercTmp), 4, 0)));
               }
               n1534Servico_PercTmp = ((0==A1534Servico_PercTmp) ? true : false);
               cmbServico_IsPublico.CurrentValue = cgiGet( cmbServico_IsPublico_Internalname);
               A1635Servico_IsPublico = StringUtil.StrToBool( cgiGet( cmbServico_IsPublico_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1635Servico_IsPublico", A1635Servico_IsPublico);
               dynavResponsavel_codigo.CurrentValue = cgiGet( dynavResponsavel_codigo_Internalname);
               AV22Responsavel_Codigo = (int)(NumberUtil.Val( cgiGet( dynavResponsavel_codigo_Internalname), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Responsavel_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22Responsavel_Codigo), 6, 0)));
               dynServico_LinNegCod.CurrentValue = cgiGet( dynServico_LinNegCod_Internalname);
               A2047Servico_LinNegCod = (int)(NumberUtil.Val( cgiGet( dynServico_LinNegCod_Internalname), "."));
               n2047Servico_LinNegCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2047Servico_LinNegCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2047Servico_LinNegCod), 6, 0)));
               n2047Servico_LinNegCod = ((0==A2047Servico_LinNegCod) ? true : false);
               cmbServico_IsOrigemReferencia.CurrentValue = cgiGet( cmbServico_IsOrigemReferencia_Internalname);
               A2092Servico_IsOrigemReferencia = StringUtil.StrToBool( cgiGet( cmbServico_IsOrigemReferencia_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2092Servico_IsOrigemReferencia", A2092Servico_IsOrigemReferencia);
               cmbServico_PausaSLA.CurrentValue = cgiGet( cmbServico_PausaSLA_Internalname);
               A2131Servico_PausaSLA = StringUtil.StrToBool( cgiGet( cmbServico_PausaSLA_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2131Servico_PausaSLA", A2131Servico_PausaSLA);
               cmbServico_Ativo.CurrentValue = cgiGet( cmbServico_Ativo_Internalname);
               A632Servico_Ativo = StringUtil.StrToBool( cgiGet( cmbServico_Ativo_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A632Servico_Ativo", A632Servico_Ativo);
               AV20Contratante_Codigo = (int)(context.localUtil.CToN( cgiGet( edtavContratante_codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20Contratante_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20Contratante_Codigo), 6, 0)));
               A155Servico_Codigo = (int)(context.localUtil.CToN( cgiGet( edtServico_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
               /* Read saved values. */
               Z155Servico_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z155Servico_Codigo"), ",", "."));
               Z608Servico_Nome = cgiGet( "Z608Servico_Nome");
               Z605Servico_Sigla = cgiGet( "Z605Servico_Sigla");
               Z1077Servico_UORespExclusiva = StringUtil.StrToBool( cgiGet( "Z1077Servico_UORespExclusiva"));
               n1077Servico_UORespExclusiva = ((false==A1077Servico_UORespExclusiva) ? true : false);
               Z889Servico_Terceriza = StringUtil.StrToBool( cgiGet( "Z889Servico_Terceriza"));
               n889Servico_Terceriza = ((false==A889Servico_Terceriza) ? true : false);
               Z1061Servico_Tela = cgiGet( "Z1061Servico_Tela");
               n1061Servico_Tela = (String.IsNullOrEmpty(StringUtil.RTrim( A1061Servico_Tela)) ? true : false);
               Z1072Servico_Atende = cgiGet( "Z1072Servico_Atende");
               n1072Servico_Atende = (String.IsNullOrEmpty(StringUtil.RTrim( A1072Servico_Atende)) ? true : false);
               Z1429Servico_ObrigaValores = cgiGet( "Z1429Servico_ObrigaValores");
               n1429Servico_ObrigaValores = (String.IsNullOrEmpty(StringUtil.RTrim( A1429Servico_ObrigaValores)) ? true : false);
               Z1436Servico_ObjetoControle = cgiGet( "Z1436Servico_ObjetoControle");
               Z1530Servico_TipoHierarquia = (short)(context.localUtil.CToN( cgiGet( "Z1530Servico_TipoHierarquia"), ",", "."));
               n1530Servico_TipoHierarquia = ((0==A1530Servico_TipoHierarquia) ? true : false);
               Z1534Servico_PercTmp = (short)(context.localUtil.CToN( cgiGet( "Z1534Servico_PercTmp"), ",", "."));
               n1534Servico_PercTmp = ((0==A1534Servico_PercTmp) ? true : false);
               Z1535Servico_PercPgm = (short)(context.localUtil.CToN( cgiGet( "Z1535Servico_PercPgm"), ",", "."));
               n1535Servico_PercPgm = ((0==A1535Servico_PercPgm) ? true : false);
               Z1536Servico_PercCnc = (short)(context.localUtil.CToN( cgiGet( "Z1536Servico_PercCnc"), ",", "."));
               n1536Servico_PercCnc = ((0==A1536Servico_PercCnc) ? true : false);
               Z1545Servico_Anterior = (int)(context.localUtil.CToN( cgiGet( "Z1545Servico_Anterior"), ",", "."));
               n1545Servico_Anterior = ((0==A1545Servico_Anterior) ? true : false);
               Z1546Servico_Posterior = (int)(context.localUtil.CToN( cgiGet( "Z1546Servico_Posterior"), ",", "."));
               n1546Servico_Posterior = ((0==A1546Servico_Posterior) ? true : false);
               Z632Servico_Ativo = StringUtil.StrToBool( cgiGet( "Z632Servico_Ativo"));
               Z1635Servico_IsPublico = StringUtil.StrToBool( cgiGet( "Z1635Servico_IsPublico"));
               Z2092Servico_IsOrigemReferencia = StringUtil.StrToBool( cgiGet( "Z2092Servico_IsOrigemReferencia"));
               Z2131Servico_PausaSLA = StringUtil.StrToBool( cgiGet( "Z2131Servico_PausaSLA"));
               Z157ServicoGrupo_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z157ServicoGrupo_Codigo"), ",", "."));
               Z633Servico_UO = (int)(context.localUtil.CToN( cgiGet( "Z633Servico_UO"), ",", "."));
               n633Servico_UO = ((0==A633Servico_UO) ? true : false);
               Z2047Servico_LinNegCod = (int)(context.localUtil.CToN( cgiGet( "Z2047Servico_LinNegCod"), ",", "."));
               n2047Servico_LinNegCod = ((0==A2047Servico_LinNegCod) ? true : false);
               Z631Servico_Vinculado = (int)(context.localUtil.CToN( cgiGet( "Z631Servico_Vinculado"), ",", "."));
               n631Servico_Vinculado = ((0==A631Servico_Vinculado) ? true : false);
               A1530Servico_TipoHierarquia = (short)(context.localUtil.CToN( cgiGet( "Z1530Servico_TipoHierarquia"), ",", "."));
               n1530Servico_TipoHierarquia = false;
               n1530Servico_TipoHierarquia = ((0==A1530Servico_TipoHierarquia) ? true : false);
               A633Servico_UO = (int)(context.localUtil.CToN( cgiGet( "Z633Servico_UO"), ",", "."));
               n633Servico_UO = false;
               n633Servico_UO = ((0==A633Servico_UO) ? true : false);
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               N157ServicoGrupo_Codigo = (int)(context.localUtil.CToN( cgiGet( "N157ServicoGrupo_Codigo"), ",", "."));
               N633Servico_UO = (int)(context.localUtil.CToN( cgiGet( "N633Servico_UO"), ",", "."));
               n633Servico_UO = ((0==A633Servico_UO) ? true : false);
               N631Servico_Vinculado = (int)(context.localUtil.CToN( cgiGet( "N631Servico_Vinculado"), ",", "."));
               n631Servico_Vinculado = ((0==A631Servico_Vinculado) ? true : false);
               N2047Servico_LinNegCod = (int)(context.localUtil.CToN( cgiGet( "N2047Servico_LinNegCod"), ",", "."));
               n2047Servico_LinNegCod = ((0==A2047Servico_LinNegCod) ? true : false);
               A1551Servico_Responsavel = (int)(context.localUtil.CToN( cgiGet( "SERVICO_RESPONSAVEL"), ",", "."));
               A640Servico_Vinculados = (short)(context.localUtil.CToN( cgiGet( "SERVICO_VINCULADOS"), ",", "."));
               A2107Servico_Identificacao = cgiGet( "SERVICO_IDENTIFICACAO");
               AV7Servico_Codigo = (int)(context.localUtil.CToN( cgiGet( "vSERVICO_CODIGO"), ",", "."));
               AV11Insert_ServicoGrupo_Codigo = (int)(context.localUtil.CToN( cgiGet( "vINSERT_SERVICOGRUPO_CODIGO"), ",", "."));
               AV15Insert_Servico_UO = (int)(context.localUtil.CToN( cgiGet( "vINSERT_SERVICO_UO"), ",", "."));
               A633Servico_UO = (int)(context.localUtil.CToN( cgiGet( "SERVICO_UO"), ",", "."));
               n633Servico_UO = ((0==A633Servico_UO) ? true : false);
               AV14Insert_Servico_Vinculado = (int)(context.localUtil.CToN( cgiGet( "vINSERT_SERVICO_VINCULADO"), ",", "."));
               AV24Insert_Servico_LinNegCod = (int)(context.localUtil.CToN( cgiGet( "vINSERT_SERVICO_LINNEGCOD"), ",", "."));
               AV21Servico_Responsavel = (int)(context.localUtil.CToN( cgiGet( "vSERVICO_RESPONSAVEL"), ",", "."));
               Gx_BScreen = (short)(context.localUtil.CToN( cgiGet( "vGXBSCREEN"), ",", "."));
               A1530Servico_TipoHierarquia = (short)(context.localUtil.CToN( cgiGet( "SERVICO_TIPOHIERARQUIA"), ",", "."));
               n1530Servico_TipoHierarquia = ((0==A1530Servico_TipoHierarquia) ? true : false);
               AV16ServicoGrupo_Codigo = (int)(context.localUtil.CToN( cgiGet( "vSERVICOGRUPO_CODIGO"), ",", "."));
               ajax_req_read_hidden_sdt(cgiGet( "vWWPCONTEXT"), AV8WWPContext);
               ajax_req_read_hidden_sdt(cgiGet( "vAUDITINGOBJECT"), AV19AuditingObject);
               A158ServicoGrupo_Descricao = cgiGet( "SERVICOGRUPO_DESCRICAO");
               A2048Servico_LinNegDsc = cgiGet( "SERVICO_LINNEGDSC");
               n2048Servico_LinNegDsc = false;
               A641Servico_VincDesc = cgiGet( "SERVICO_VINCDESC");
               n641Servico_VincDesc = false;
               A1557Servico_VincSigla = cgiGet( "SERVICO_VINCSIGLA");
               n1557Servico_VincSigla = false;
               AV25Pgmname = cgiGet( "vPGMNAME");
               Gx_mode = cgiGet( "vMODE");
               Dvpanel_tableattributes_Width = cgiGet( "DVPANEL_TABLEATTRIBUTES_Width");
               Dvpanel_tableattributes_Height = cgiGet( "DVPANEL_TABLEATTRIBUTES_Height");
               Dvpanel_tableattributes_Cls = cgiGet( "DVPANEL_TABLEATTRIBUTES_Cls");
               Dvpanel_tableattributes_Title = cgiGet( "DVPANEL_TABLEATTRIBUTES_Title");
               Dvpanel_tableattributes_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsible"));
               Dvpanel_tableattributes_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsed"));
               Dvpanel_tableattributes_Enabled = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Enabled"));
               Dvpanel_tableattributes_Class = cgiGet( "DVPANEL_TABLEATTRIBUTES_Class");
               Dvpanel_tableattributes_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autowidth"));
               Dvpanel_tableattributes_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoheight"));
               Dvpanel_tableattributes_Showheader = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showheader"));
               Dvpanel_tableattributes_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showcollapseicon"));
               Dvpanel_tableattributes_Iconposition = cgiGet( "DVPANEL_TABLEATTRIBUTES_Iconposition");
               Dvpanel_tableattributes_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoscroll"));
               Dvpanel_tableattributes_Visible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Visible"));
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "Servico";
               A155Servico_Codigo = (int)(context.localUtil.CToN( cgiGet( edtServico_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A155Servico_Codigo), "ZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( AV25Pgmname, ""));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1530Servico_TipoHierarquia), "ZZZ9");
               hsh = cgiGet( "hsh");
               if ( ( ! ( ( A155Servico_Codigo != Z155Servico_Codigo ) ) || ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ) && ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("servico:[SecurityCheckFailed value for]"+"Servico_Codigo:"+context.localUtil.Format( (decimal)(A155Servico_Codigo), "ZZZZZ9"));
                  GXUtil.WriteLog("servico:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GXUtil.WriteLog("servico:[SecurityCheckFailed value for]"+"Pgmname:"+StringUtil.RTrim( context.localUtil.Format( AV25Pgmname, "")));
                  GXUtil.WriteLog("servico:[SecurityCheckFailed value for]"+"Servico_TipoHierarquia:"+context.localUtil.Format( (decimal)(A1530Servico_TipoHierarquia), "ZZZ9"));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  A155Servico_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode130 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     Gx_mode = sMode130;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound130 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_0V0( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_trn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "SERVICO_CODIGO");
                        AnyError = 1;
                        GX_FocusControl = edtServico_Codigo_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E120V2 */
                           E120V2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E130V2 */
                           E130V2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "SERVICO_ISPUBLICO.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E140V2 */
                           E140V2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E130V2 */
            E130V2 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll0V130( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         bttBtn_trn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtn_trn_delete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_trn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
            }
            DisableAttributes0V130( ) ;
         }
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavServico_uo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavServico_uo.Enabled), 5, 0)));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavResponsavel_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavResponsavel_codigo.Enabled), 5, 0)));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratante_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratante_codigo_Enabled), 5, 0)));
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_0V0( )
      {
         BeforeValidate0V130( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls0V130( ) ;
            }
            else
            {
               CheckExtendedTable0V130( ) ;
               CloseExtendedTableCursors0V130( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         }
      }

      protected void ResetCaption0V0( )
      {
      }

      protected void E120V2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         /* Execute user subroutine: 'ATTRIBUTESSECURITYCODE' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
         if ( ( StringUtil.StrCmp(AV9TrnContext.gxTpr_Transactionname, AV25Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV26GXV1 = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26GXV1), 8, 0)));
            while ( AV26GXV1 <= AV9TrnContext.gxTpr_Attributes.Count )
            {
               AV12TrnContextAtt = ((wwpbaseobjects.SdtWWPTransactionContext_Attribute)AV9TrnContext.gxTpr_Attributes.Item(AV26GXV1));
               if ( StringUtil.StrCmp(AV12TrnContextAtt.gxTpr_Attributename, "ServicoGrupo_Codigo") == 0 )
               {
                  AV11Insert_ServicoGrupo_Codigo = (int)(NumberUtil.Val( AV12TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Insert_ServicoGrupo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Insert_ServicoGrupo_Codigo), 6, 0)));
               }
               else if ( StringUtil.StrCmp(AV12TrnContextAtt.gxTpr_Attributename, "Servico_UO") == 0 )
               {
                  AV15Insert_Servico_UO = (int)(NumberUtil.Val( AV12TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15Insert_Servico_UO", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15Insert_Servico_UO), 6, 0)));
               }
               else if ( StringUtil.StrCmp(AV12TrnContextAtt.gxTpr_Attributename, "Servico_Vinculado") == 0 )
               {
                  AV14Insert_Servico_Vinculado = (int)(NumberUtil.Val( AV12TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14Insert_Servico_Vinculado", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14Insert_Servico_Vinculado), 6, 0)));
               }
               else if ( StringUtil.StrCmp(AV12TrnContextAtt.gxTpr_Attributename, "Servico_LinNegCod") == 0 )
               {
                  AV24Insert_Servico_LinNegCod = (int)(NumberUtil.Val( AV12TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Insert_Servico_LinNegCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24Insert_Servico_LinNegCod), 6, 0)));
               }
               AV26GXV1 = (int)(AV26GXV1+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26GXV1), 8, 0)));
            }
         }
         edtavContratante_codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratante_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratante_codigo_Visible), 5, 0)));
         edtServico_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtServico_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtServico_Codigo_Visible), 5, 0)));
         AV17Servico_UO = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Servico_UO", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17Servico_UO), 6, 0)));
         GXt_int1 = AV22Responsavel_Codigo;
         new prc_getservico_responsavel(context ).execute( ref  AV7Servico_Codigo, out  GXt_int1) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Servico_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vSERVICO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7Servico_Codigo), "ZZZZZ9")));
         AV22Responsavel_Codigo = GXt_int1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Responsavel_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22Responsavel_Codigo), 6, 0)));
         if ( (0==AV8WWPContext.gxTpr_Contratante_codigo) )
         {
            GXt_int1 = AV20Contratante_Codigo;
            new prc_retornacodigocontratanteareaatrabalho(context ).execute(  AV8WWPContext.gxTpr_Areatrabalho_codigo, out  GXt_int1) ;
            AV20Contratante_Codigo = GXt_int1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20Contratante_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20Contratante_Codigo), 6, 0)));
         }
         else
         {
            AV20Contratante_Codigo = AV8WWPContext.gxTpr_Contratante_codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20Contratante_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20Contratante_Codigo), 6, 0)));
         }
      }

      protected void E130V2( )
      {
         /* After Trn Routine */
         new wwpbaseobjects.audittransaction(context ).execute(  AV19AuditingObject,  AV25Pgmname) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Pgmname", AV25Pgmname);
         if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV9TrnContext.gxTpr_Callerondelete )
         {
            context.wjLoc = formatLink("wwservico.aspx") ;
            context.wjLocDisableFrm = 1;
         }
         context.setWebReturnParms(new Object[] {(int)AV16ServicoGrupo_Codigo});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void S112( )
      {
         /* 'ATTRIBUTESSECURITYCODE' Routine */
         edtServico_PercTmp_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtServico_PercTmp_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtServico_PercTmp_Visible), 5, 0)));
         cellServico_perctmp_cell_Class = "Invisible";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellServico_perctmp_cell_Internalname, "Class", cellServico_perctmp_cell_Class);
         cellTextblockservico_perctmp_cell_Class = "Invisible";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellTextblockservico_perctmp_cell_Internalname, "Class", cellTextblockservico_perctmp_cell_Class);
      }

      protected void E140V2( )
      {
         /* Servico_IsPublico_Click Routine */
         if ( A1635Servico_IsPublico )
         {
            GXt_int1 = AV22Responsavel_Codigo;
            new prc_getservico_responsavel(context ).execute( ref  AV7Servico_Codigo, out  GXt_int1) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Servico_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vSERVICO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7Servico_Codigo), "ZZZZZ9")));
            AV22Responsavel_Codigo = GXt_int1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Responsavel_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22Responsavel_Codigo), 6, 0)));
         }
         else
         {
            AV22Responsavel_Codigo = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Responsavel_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22Responsavel_Codigo), 6, 0)));
         }
         dynavResponsavel_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22Responsavel_Codigo), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavResponsavel_codigo_Internalname, "Values", dynavResponsavel_codigo.ToJavascriptSource());
      }

      protected void ZM0V130( short GX_JID )
      {
         if ( ( GX_JID == 51 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z608Servico_Nome = T000V3_A608Servico_Nome[0];
               Z605Servico_Sigla = T000V3_A605Servico_Sigla[0];
               Z1077Servico_UORespExclusiva = T000V3_A1077Servico_UORespExclusiva[0];
               Z889Servico_Terceriza = T000V3_A889Servico_Terceriza[0];
               Z1061Servico_Tela = T000V3_A1061Servico_Tela[0];
               Z1072Servico_Atende = T000V3_A1072Servico_Atende[0];
               Z1429Servico_ObrigaValores = T000V3_A1429Servico_ObrigaValores[0];
               Z1436Servico_ObjetoControle = T000V3_A1436Servico_ObjetoControle[0];
               Z1530Servico_TipoHierarquia = T000V3_A1530Servico_TipoHierarquia[0];
               Z1534Servico_PercTmp = T000V3_A1534Servico_PercTmp[0];
               Z1535Servico_PercPgm = T000V3_A1535Servico_PercPgm[0];
               Z1536Servico_PercCnc = T000V3_A1536Servico_PercCnc[0];
               Z1545Servico_Anterior = T000V3_A1545Servico_Anterior[0];
               Z1546Servico_Posterior = T000V3_A1546Servico_Posterior[0];
               Z632Servico_Ativo = T000V3_A632Servico_Ativo[0];
               Z1635Servico_IsPublico = T000V3_A1635Servico_IsPublico[0];
               Z2092Servico_IsOrigemReferencia = T000V3_A2092Servico_IsOrigemReferencia[0];
               Z2131Servico_PausaSLA = T000V3_A2131Servico_PausaSLA[0];
               Z157ServicoGrupo_Codigo = T000V3_A157ServicoGrupo_Codigo[0];
               Z633Servico_UO = T000V3_A633Servico_UO[0];
               Z2047Servico_LinNegCod = T000V3_A2047Servico_LinNegCod[0];
               Z631Servico_Vinculado = T000V3_A631Servico_Vinculado[0];
            }
            else
            {
               Z608Servico_Nome = A608Servico_Nome;
               Z605Servico_Sigla = A605Servico_Sigla;
               Z1077Servico_UORespExclusiva = A1077Servico_UORespExclusiva;
               Z889Servico_Terceriza = A889Servico_Terceriza;
               Z1061Servico_Tela = A1061Servico_Tela;
               Z1072Servico_Atende = A1072Servico_Atende;
               Z1429Servico_ObrigaValores = A1429Servico_ObrigaValores;
               Z1436Servico_ObjetoControle = A1436Servico_ObjetoControle;
               Z1530Servico_TipoHierarquia = A1530Servico_TipoHierarquia;
               Z1534Servico_PercTmp = A1534Servico_PercTmp;
               Z1535Servico_PercPgm = A1535Servico_PercPgm;
               Z1536Servico_PercCnc = A1536Servico_PercCnc;
               Z1545Servico_Anterior = A1545Servico_Anterior;
               Z1546Servico_Posterior = A1546Servico_Posterior;
               Z632Servico_Ativo = A632Servico_Ativo;
               Z1635Servico_IsPublico = A1635Servico_IsPublico;
               Z2092Servico_IsOrigemReferencia = A2092Servico_IsOrigemReferencia;
               Z2131Servico_PausaSLA = A2131Servico_PausaSLA;
               Z157ServicoGrupo_Codigo = A157ServicoGrupo_Codigo;
               Z633Servico_UO = A633Servico_UO;
               Z2047Servico_LinNegCod = A2047Servico_LinNegCod;
               Z631Servico_Vinculado = A631Servico_Vinculado;
            }
         }
         if ( GX_JID == -51 )
         {
            Z155Servico_Codigo = A155Servico_Codigo;
            Z608Servico_Nome = A608Servico_Nome;
            Z156Servico_Descricao = A156Servico_Descricao;
            Z605Servico_Sigla = A605Servico_Sigla;
            Z1077Servico_UORespExclusiva = A1077Servico_UORespExclusiva;
            Z889Servico_Terceriza = A889Servico_Terceriza;
            Z1061Servico_Tela = A1061Servico_Tela;
            Z1072Servico_Atende = A1072Servico_Atende;
            Z1429Servico_ObrigaValores = A1429Servico_ObrigaValores;
            Z1436Servico_ObjetoControle = A1436Servico_ObjetoControle;
            Z1530Servico_TipoHierarquia = A1530Servico_TipoHierarquia;
            Z1534Servico_PercTmp = A1534Servico_PercTmp;
            Z1535Servico_PercPgm = A1535Servico_PercPgm;
            Z1536Servico_PercCnc = A1536Servico_PercCnc;
            Z1545Servico_Anterior = A1545Servico_Anterior;
            Z1546Servico_Posterior = A1546Servico_Posterior;
            Z632Servico_Ativo = A632Servico_Ativo;
            Z1635Servico_IsPublico = A1635Servico_IsPublico;
            Z2092Servico_IsOrigemReferencia = A2092Servico_IsOrigemReferencia;
            Z2131Servico_PausaSLA = A2131Servico_PausaSLA;
            Z157ServicoGrupo_Codigo = A157ServicoGrupo_Codigo;
            Z633Servico_UO = A633Servico_UO;
            Z2047Servico_LinNegCod = A2047Servico_LinNegCod;
            Z631Servico_Vinculado = A631Servico_Vinculado;
            Z158ServicoGrupo_Descricao = A158ServicoGrupo_Descricao;
            Z641Servico_VincDesc = A641Servico_VincDesc;
            Z1557Servico_VincSigla = A1557Servico_VincSigla;
            Z2048Servico_LinNegDsc = A2048Servico_LinNegDsc;
         }
      }

      protected void standaloneNotModal( )
      {
         GXVvSERVICO_UO_html0V130( ) ;
         GXASERVICO_LINNEGCOD_html0V130( ) ;
         edtServico_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtServico_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtServico_Codigo_Enabled), 5, 0)));
         Gx_BScreen = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         AV25Pgmname = "Servico";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Pgmname", AV25Pgmname);
         edtServico_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtServico_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtServico_Codigo_Enabled), 5, 0)));
         bttBtn_trn_delete_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         if ( ! (0==AV7Servico_Codigo) )
         {
            A155Servico_Codigo = AV7Servico_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
         }
         GXVvRESPONSAVEL_CODIGO_html0V130( AV8WWPContext) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_ServicoGrupo_Codigo) )
         {
            dynServicoGrupo_Codigo.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynServicoGrupo_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynServicoGrupo_Codigo.Enabled), 5, 0)));
         }
         else
         {
            dynServicoGrupo_Codigo.Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynServicoGrupo_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynServicoGrupo_Codigo.Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV14Insert_Servico_Vinculado) )
         {
            dynServico_Vinculado.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynServico_Vinculado_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynServico_Vinculado.Enabled), 5, 0)));
         }
         else
         {
            dynServico_Vinculado.Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynServico_Vinculado_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynServico_Vinculado.Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV24Insert_Servico_LinNegCod) )
         {
            dynServico_LinNegCod.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynServico_LinNegCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynServico_LinNegCod.Enabled), 5, 0)));
         }
         else
         {
            dynServico_LinNegCod.Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynServico_LinNegCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynServico_LinNegCod.Enabled), 5, 0)));
         }
      }

      protected void standaloneModal( )
      {
         cmbServico_Ativo.Visible = (!( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbServico_Ativo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbServico_Ativo.Visible), 5, 0)));
         lblTextblockservico_ativo_Visible = (!( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockservico_ativo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockservico_ativo_Visible), 5, 0)));
         imgSelectuo_Visible = (( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ||( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )  ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgSelectuo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgSelectuo_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV24Insert_Servico_LinNegCod) )
         {
            A2047Servico_LinNegCod = AV24Insert_Servico_LinNegCod;
            n2047Servico_LinNegCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2047Servico_LinNegCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2047Servico_LinNegCod), 6, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV14Insert_Servico_Vinculado) )
         {
            A631Servico_Vinculado = AV14Insert_Servico_Vinculado;
            n631Servico_Vinculado = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A631Servico_Vinculado", StringUtil.LTrim( StringUtil.Str( (decimal)(A631Servico_Vinculado), 6, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV15Insert_Servico_UO) )
         {
            A633Servico_UO = AV15Insert_Servico_UO;
            n633Servico_UO = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A633Servico_UO", StringUtil.LTrim( StringUtil.Str( (decimal)(A633Servico_UO), 6, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_ServicoGrupo_Codigo) )
         {
            A157ServicoGrupo_Codigo = AV11Insert_ServicoGrupo_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A157ServicoGrupo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A157ServicoGrupo_Codigo), 6, 0)));
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (false==A2131Servico_PausaSLA) && ( Gx_BScreen == 0 ) )
         {
            A2131Servico_PausaSLA = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2131Servico_PausaSLA", A2131Servico_PausaSLA);
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (false==A1635Servico_IsPublico) && ( Gx_BScreen == 0 ) )
         {
            A1635Servico_IsPublico = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1635Servico_IsPublico", A1635Servico_IsPublico);
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (false==A632Servico_Ativo) && ( Gx_BScreen == 0 ) )
         {
            A632Servico_Ativo = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A632Servico_Ativo", A632Servico_Ativo);
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (0==A1530Servico_TipoHierarquia) && ( Gx_BScreen == 0 ) )
         {
            A1530Servico_TipoHierarquia = 1;
            n1530Servico_TipoHierarquia = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1530Servico_TipoHierarquia", StringUtil.LTrim( StringUtil.Str( (decimal)(A1530Servico_TipoHierarquia), 4, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
            /* Using cursor T000V6 */
            pr_default.execute(4, new Object[] {n2047Servico_LinNegCod, A2047Servico_LinNegCod});
            A2048Servico_LinNegDsc = T000V6_A2048Servico_LinNegDsc[0];
            n2048Servico_LinNegDsc = T000V6_n2048Servico_LinNegDsc[0];
            pr_default.close(4);
            GXt_int1 = A1551Servico_Responsavel;
            new prc_getservico_responsavel(context ).execute( ref  A155Servico_Codigo, out  GXt_int1) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
            A1551Servico_Responsavel = GXt_int1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1551Servico_Responsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(A1551Servico_Responsavel), 6, 0)));
            GXt_int2 = A640Servico_Vinculados;
            new prc_qtdeservicosvinculados(context ).execute(  A155Servico_Codigo, out  GXt_int2) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
            A640Servico_Vinculados = GXt_int2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A640Servico_Vinculados", StringUtil.LTrim( StringUtil.Str( (decimal)(A640Servico_Vinculados), 4, 0)));
            /* Using cursor T000V7 */
            pr_default.execute(5, new Object[] {n631Servico_Vinculado, A631Servico_Vinculado});
            A641Servico_VincDesc = T000V7_A641Servico_VincDesc[0];
            n641Servico_VincDesc = T000V7_n641Servico_VincDesc[0];
            A1557Servico_VincSigla = T000V7_A1557Servico_VincSigla[0];
            n1557Servico_VincSigla = T000V7_n1557Servico_VincSigla[0];
            pr_default.close(5);
            edtServico_PercTmp_Visible = (T000V3_n631Servico_Vinculado[0] ? 1 : 0);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtServico_PercTmp_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtServico_PercTmp_Visible), 5, 0)));
            if ( ! ( T000V3_n631Servico_Vinculado[0] ) )
            {
               cellServico_perctmp_cell_Class = "Invisible";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellServico_perctmp_cell_Internalname, "Class", cellServico_perctmp_cell_Class);
            }
            else
            {
               if ( T000V3_n631Servico_Vinculado[0] )
               {
                  cellServico_perctmp_cell_Class = "DataContentCell";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellServico_perctmp_cell_Internalname, "Class", cellServico_perctmp_cell_Class);
               }
            }
            if ( ! ( T000V3_n631Servico_Vinculado[0] ) )
            {
               cellTextblockservico_perctmp_cell_Class = "Invisible";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellTextblockservico_perctmp_cell_Internalname, "Class", cellTextblockservico_perctmp_cell_Class);
            }
            else
            {
               if ( T000V3_n631Servico_Vinculado[0] )
               {
                  cellTextblockservico_perctmp_cell_Class = "DataDescriptionCell";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellTextblockservico_perctmp_cell_Internalname, "Class", cellTextblockservico_perctmp_cell_Class);
               }
            }
            /* Using cursor T000V4 */
            pr_default.execute(2, new Object[] {A157ServicoGrupo_Codigo});
            A158ServicoGrupo_Descricao = T000V4_A158ServicoGrupo_Descricao[0];
            pr_default.close(2);
            dynavResponsavel_codigo.Visible = (A1635Servico_IsPublico ? 1 : 0);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavResponsavel_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavResponsavel_codigo.Visible), 5, 0)));
            lblTextblockresponsavel_codigo_Visible = (A1635Servico_IsPublico ? 1 : 0);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockresponsavel_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockresponsavel_codigo_Visible), 5, 0)));
            if ( ! A1635Servico_IsPublico )
            {
               AV21Servico_Responsavel = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Servico_Responsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21Servico_Responsavel), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Servico_Responsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21Servico_Responsavel), 6, 0)));
            }
            GXASERVICO_ANTERIOR_html0V130( AV7Servico_Codigo, A632Servico_Ativo) ;
            GXASERVICO_POSTERIOR_html0V130( AV7Servico_Codigo, A632Servico_Ativo) ;
            GXASERVICO_VINCULADO_html0V130( AV16ServicoGrupo_Codigo, AV7Servico_Codigo, A157ServicoGrupo_Codigo, A640Servico_Vinculados, A632Servico_Ativo) ;
         }
      }

      protected void Load0V130( )
      {
         /* Using cursor T000V9 */
         pr_default.execute(7, new Object[] {A155Servico_Codigo});
         if ( (pr_default.getStatus(7) != 101) )
         {
            RcdFound130 = 1;
            A608Servico_Nome = T000V9_A608Servico_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A608Servico_Nome", A608Servico_Nome);
            A156Servico_Descricao = T000V9_A156Servico_Descricao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A156Servico_Descricao", A156Servico_Descricao);
            n156Servico_Descricao = T000V9_n156Servico_Descricao[0];
            A605Servico_Sigla = T000V9_A605Servico_Sigla[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A605Servico_Sigla", A605Servico_Sigla);
            A158ServicoGrupo_Descricao = T000V9_A158ServicoGrupo_Descricao[0];
            A1077Servico_UORespExclusiva = T000V9_A1077Servico_UORespExclusiva[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1077Servico_UORespExclusiva", A1077Servico_UORespExclusiva);
            n1077Servico_UORespExclusiva = T000V9_n1077Servico_UORespExclusiva[0];
            A641Servico_VincDesc = T000V9_A641Servico_VincDesc[0];
            n641Servico_VincDesc = T000V9_n641Servico_VincDesc[0];
            A1557Servico_VincSigla = T000V9_A1557Servico_VincSigla[0];
            n1557Servico_VincSigla = T000V9_n1557Servico_VincSigla[0];
            A889Servico_Terceriza = T000V9_A889Servico_Terceriza[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A889Servico_Terceriza", A889Servico_Terceriza);
            n889Servico_Terceriza = T000V9_n889Servico_Terceriza[0];
            A1061Servico_Tela = T000V9_A1061Servico_Tela[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1061Servico_Tela", A1061Servico_Tela);
            n1061Servico_Tela = T000V9_n1061Servico_Tela[0];
            A1072Servico_Atende = T000V9_A1072Servico_Atende[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1072Servico_Atende", A1072Servico_Atende);
            n1072Servico_Atende = T000V9_n1072Servico_Atende[0];
            A1429Servico_ObrigaValores = T000V9_A1429Servico_ObrigaValores[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1429Servico_ObrigaValores", A1429Servico_ObrigaValores);
            n1429Servico_ObrigaValores = T000V9_n1429Servico_ObrigaValores[0];
            A1436Servico_ObjetoControle = T000V9_A1436Servico_ObjetoControle[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1436Servico_ObjetoControle", A1436Servico_ObjetoControle);
            A1530Servico_TipoHierarquia = T000V9_A1530Servico_TipoHierarquia[0];
            n1530Servico_TipoHierarquia = T000V9_n1530Servico_TipoHierarquia[0];
            A1534Servico_PercTmp = T000V9_A1534Servico_PercTmp[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1534Servico_PercTmp", StringUtil.LTrim( StringUtil.Str( (decimal)(A1534Servico_PercTmp), 4, 0)));
            n1534Servico_PercTmp = T000V9_n1534Servico_PercTmp[0];
            A1535Servico_PercPgm = T000V9_A1535Servico_PercPgm[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1535Servico_PercPgm", StringUtil.LTrim( StringUtil.Str( (decimal)(A1535Servico_PercPgm), 4, 0)));
            n1535Servico_PercPgm = T000V9_n1535Servico_PercPgm[0];
            A1536Servico_PercCnc = T000V9_A1536Servico_PercCnc[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1536Servico_PercCnc", StringUtil.LTrim( StringUtil.Str( (decimal)(A1536Servico_PercCnc), 4, 0)));
            n1536Servico_PercCnc = T000V9_n1536Servico_PercCnc[0];
            A1545Servico_Anterior = T000V9_A1545Servico_Anterior[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1545Servico_Anterior", StringUtil.LTrim( StringUtil.Str( (decimal)(A1545Servico_Anterior), 6, 0)));
            n1545Servico_Anterior = T000V9_n1545Servico_Anterior[0];
            A1546Servico_Posterior = T000V9_A1546Servico_Posterior[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1546Servico_Posterior", StringUtil.LTrim( StringUtil.Str( (decimal)(A1546Servico_Posterior), 6, 0)));
            n1546Servico_Posterior = T000V9_n1546Servico_Posterior[0];
            A632Servico_Ativo = T000V9_A632Servico_Ativo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A632Servico_Ativo", A632Servico_Ativo);
            A1635Servico_IsPublico = T000V9_A1635Servico_IsPublico[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1635Servico_IsPublico", A1635Servico_IsPublico);
            A2048Servico_LinNegDsc = T000V9_A2048Servico_LinNegDsc[0];
            n2048Servico_LinNegDsc = T000V9_n2048Servico_LinNegDsc[0];
            A2092Servico_IsOrigemReferencia = T000V9_A2092Servico_IsOrigemReferencia[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2092Servico_IsOrigemReferencia", A2092Servico_IsOrigemReferencia);
            A2131Servico_PausaSLA = T000V9_A2131Servico_PausaSLA[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2131Servico_PausaSLA", A2131Servico_PausaSLA);
            A157ServicoGrupo_Codigo = T000V9_A157ServicoGrupo_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A157ServicoGrupo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A157ServicoGrupo_Codigo), 6, 0)));
            A633Servico_UO = T000V9_A633Servico_UO[0];
            n633Servico_UO = T000V9_n633Servico_UO[0];
            A2047Servico_LinNegCod = T000V9_A2047Servico_LinNegCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2047Servico_LinNegCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2047Servico_LinNegCod), 6, 0)));
            n2047Servico_LinNegCod = T000V9_n2047Servico_LinNegCod[0];
            A631Servico_Vinculado = T000V9_A631Servico_Vinculado[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A631Servico_Vinculado", StringUtil.LTrim( StringUtil.Str( (decimal)(A631Servico_Vinculado), 6, 0)));
            n631Servico_Vinculado = T000V9_n631Servico_Vinculado[0];
            ZM0V130( -51) ;
         }
         pr_default.close(7);
         OnLoadActions0V130( ) ;
      }

      protected void OnLoadActions0V130( )
      {
         GXASERVICO_ANTERIOR_html0V130( AV7Servico_Codigo, A632Servico_Ativo) ;
         GXASERVICO_POSTERIOR_html0V130( AV7Servico_Codigo, A632Servico_Ativo) ;
         if ( (0==AV17Servico_UO) && ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            AV17Servico_UO = A633Servico_UO;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Servico_UO", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17Servico_UO), 6, 0)));
         }
         GXt_int1 = A1551Servico_Responsavel;
         new prc_getservico_responsavel(context ).execute( ref  A155Servico_Codigo, out  GXt_int1) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
         A1551Servico_Responsavel = GXt_int1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1551Servico_Responsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(A1551Servico_Responsavel), 6, 0)));
         GXt_int2 = A640Servico_Vinculados;
         new prc_qtdeservicosvinculados(context ).execute(  A155Servico_Codigo, out  GXt_int2) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
         A640Servico_Vinculados = GXt_int2;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A640Servico_Vinculados", StringUtil.LTrim( StringUtil.Str( (decimal)(A640Servico_Vinculados), 4, 0)));
         GXASERVICO_VINCULADO_html0V130( AV16ServicoGrupo_Codigo, AV7Servico_Codigo, A157ServicoGrupo_Codigo, A640Servico_Vinculados, A632Servico_Ativo) ;
         A2107Servico_Identificacao = StringUtil.Concat( StringUtil.Trim( A605Servico_Sigla), StringUtil.Trim( A608Servico_Nome), " - ");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2107Servico_Identificacao", A2107Servico_Identificacao);
         edtServico_PercTmp_Visible = (T000V3_n631Servico_Vinculado[0] ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtServico_PercTmp_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtServico_PercTmp_Visible), 5, 0)));
         if ( ! ( T000V3_n631Servico_Vinculado[0] ) )
         {
            cellServico_perctmp_cell_Class = "Invisible";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellServico_perctmp_cell_Internalname, "Class", cellServico_perctmp_cell_Class);
         }
         else
         {
            if ( T000V3_n631Servico_Vinculado[0] )
            {
               cellServico_perctmp_cell_Class = "DataContentCell";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellServico_perctmp_cell_Internalname, "Class", cellServico_perctmp_cell_Class);
            }
         }
         if ( ! ( T000V3_n631Servico_Vinculado[0] ) )
         {
            cellTextblockservico_perctmp_cell_Class = "Invisible";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellTextblockservico_perctmp_cell_Internalname, "Class", cellTextblockservico_perctmp_cell_Class);
         }
         else
         {
            if ( T000V3_n631Servico_Vinculado[0] )
            {
               cellTextblockservico_perctmp_cell_Class = "DataDescriptionCell";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellTextblockservico_perctmp_cell_Internalname, "Class", cellTextblockservico_perctmp_cell_Class);
            }
         }
         dynavResponsavel_codigo.Visible = (A1635Servico_IsPublico ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavResponsavel_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavResponsavel_codigo.Visible), 5, 0)));
         lblTextblockresponsavel_codigo_Visible = (A1635Servico_IsPublico ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockresponsavel_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockresponsavel_codigo_Visible), 5, 0)));
         if ( ! A1635Servico_IsPublico )
         {
            AV21Servico_Responsavel = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Servico_Responsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21Servico_Responsavel), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Servico_Responsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21Servico_Responsavel), 6, 0)));
         }
      }

      protected void CheckExtendedTable0V130( )
      {
         Gx_BScreen = 1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         standaloneModal( ) ;
         GXASERVICO_ANTERIOR_html0V130( AV7Servico_Codigo, A632Servico_Ativo) ;
         GXASERVICO_POSTERIOR_html0V130( AV7Servico_Codigo, A632Servico_Ativo) ;
         if ( (0==AV17Servico_UO) && ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            AV17Servico_UO = A633Servico_UO;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Servico_UO", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17Servico_UO), 6, 0)));
         }
         GXt_int1 = A1551Servico_Responsavel;
         new prc_getservico_responsavel(context ).execute( ref  A155Servico_Codigo, out  GXt_int1) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
         A1551Servico_Responsavel = GXt_int1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1551Servico_Responsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(A1551Servico_Responsavel), 6, 0)));
         GXt_int2 = A640Servico_Vinculados;
         new prc_qtdeservicosvinculados(context ).execute(  A155Servico_Codigo, out  GXt_int2) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
         A640Servico_Vinculados = GXt_int2;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A640Servico_Vinculados", StringUtil.LTrim( StringUtil.Str( (decimal)(A640Servico_Vinculados), 4, 0)));
         GXASERVICO_VINCULADO_html0V130( AV16ServicoGrupo_Codigo, AV7Servico_Codigo, A157ServicoGrupo_Codigo, A640Servico_Vinculados, A632Servico_Ativo) ;
         A2107Servico_Identificacao = StringUtil.Concat( StringUtil.Trim( A605Servico_Sigla), StringUtil.Trim( A608Servico_Nome), " - ");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2107Servico_Identificacao", A2107Servico_Identificacao);
         if ( String.IsNullOrEmpty(StringUtil.RTrim( A608Servico_Nome)) )
         {
            GX_msglist.addItem("Nome � obrigat�rio.", 1, "SERVICO_NOME");
            AnyError = 1;
            GX_FocusControl = edtServico_Nome_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( A605Servico_Sigla)) )
         {
            GX_msglist.addItem("Sigla � obrigat�rio.", 1, "SERVICO_SIGLA");
            AnyError = 1;
            GX_FocusControl = edtServico_Sigla_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         /* Using cursor T000V4 */
         pr_default.execute(2, new Object[] {A157ServicoGrupo_Codigo});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Servico Grupo'.", "ForeignKeyNotFound", 1, "SERVICOGRUPO_CODIGO");
            AnyError = 1;
            GX_FocusControl = dynServicoGrupo_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A158ServicoGrupo_Descricao = T000V4_A158ServicoGrupo_Descricao[0];
         pr_default.close(2);
         /* Using cursor T000V7 */
         pr_default.execute(5, new Object[] {n631Servico_Vinculado, A631Servico_Vinculado});
         if ( (pr_default.getStatus(5) == 101) )
         {
            if ( ! ( (0==A631Servico_Vinculado) ) )
            {
               GX_msglist.addItem("N�o existe 'Servico_Servico Vinculado'.", "ForeignKeyNotFound", 1, "SERVICO_VINCULADO");
               AnyError = 1;
               GX_FocusControl = dynServico_Vinculado_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         A641Servico_VincDesc = T000V7_A641Servico_VincDesc[0];
         n641Servico_VincDesc = T000V7_n641Servico_VincDesc[0];
         A1557Servico_VincSigla = T000V7_A1557Servico_VincSigla[0];
         n1557Servico_VincSigla = T000V7_n1557Servico_VincSigla[0];
         pr_default.close(5);
         edtServico_PercTmp_Visible = (T000V3_n631Servico_Vinculado[0] ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtServico_PercTmp_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtServico_PercTmp_Visible), 5, 0)));
         if ( ! ( T000V3_n631Servico_Vinculado[0] ) )
         {
            cellServico_perctmp_cell_Class = "Invisible";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellServico_perctmp_cell_Internalname, "Class", cellServico_perctmp_cell_Class);
         }
         else
         {
            if ( T000V3_n631Servico_Vinculado[0] )
            {
               cellServico_perctmp_cell_Class = "DataContentCell";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellServico_perctmp_cell_Internalname, "Class", cellServico_perctmp_cell_Class);
            }
         }
         if ( ! ( T000V3_n631Servico_Vinculado[0] ) )
         {
            cellTextblockservico_perctmp_cell_Class = "Invisible";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellTextblockservico_perctmp_cell_Internalname, "Class", cellTextblockservico_perctmp_cell_Class);
         }
         else
         {
            if ( T000V3_n631Servico_Vinculado[0] )
            {
               cellTextblockservico_perctmp_cell_Class = "DataDescriptionCell";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellTextblockservico_perctmp_cell_Internalname, "Class", cellTextblockservico_perctmp_cell_Class);
            }
         }
         if ( ! ( ( StringUtil.StrCmp(A1061Servico_Tela, "CNT") == 0 ) || ( StringUtil.StrCmp(A1061Servico_Tela, "CHK") == 0 ) || String.IsNullOrEmpty(StringUtil.RTrim( A1061Servico_Tela)) ) )
         {
            GX_msglist.addItem("Campo Tela fora do intervalo", "OutOfRange", 1, "SERVICO_TELA");
            AnyError = 1;
            GX_FocusControl = cmbServico_Tela_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( ! ( ( StringUtil.StrCmp(A1072Servico_Atende, "S") == 0 ) || ( StringUtil.StrCmp(A1072Servico_Atende, "H") == 0 ) || String.IsNullOrEmpty(StringUtil.RTrim( A1072Servico_Atende)) ) )
         {
            GX_msglist.addItem("Campo Atende fora do intervalo", "OutOfRange", 1, "SERVICO_ATENDE");
            AnyError = 1;
            GX_FocusControl = cmbServico_Atende_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( ! ( ( StringUtil.StrCmp(A1429Servico_ObrigaValores, "") == 0 ) || ( StringUtil.StrCmp(A1429Servico_ObrigaValores, "L") == 0 ) || ( StringUtil.StrCmp(A1429Servico_ObrigaValores, "B") == 0 ) || ( StringUtil.StrCmp(A1429Servico_ObrigaValores, "A") == 0 ) || String.IsNullOrEmpty(StringUtil.RTrim( A1429Servico_ObrigaValores)) ) )
         {
            GX_msglist.addItem("Campo Obriga Valores fora do intervalo", "OutOfRange", 1, "SERVICO_OBRIGAVALORES");
            AnyError = 1;
            GX_FocusControl = cmbServico_ObrigaValores_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( ! ( ( StringUtil.StrCmp(A1436Servico_ObjetoControle, "SIS") == 0 ) || ( StringUtil.StrCmp(A1436Servico_ObjetoControle, "PRC") == 0 ) || ( StringUtil.StrCmp(A1436Servico_ObjetoControle, "PRD") == 0 ) ) )
         {
            GX_msglist.addItem("Campo Servico_Objeto Controle fora do intervalo", "OutOfRange", 1, "SERVICO_OBJETOCONTROLE");
            AnyError = 1;
            GX_FocusControl = cmbServico_ObjetoControle_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         dynavResponsavel_codigo.Visible = (A1635Servico_IsPublico ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavResponsavel_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavResponsavel_codigo.Visible), 5, 0)));
         lblTextblockresponsavel_codigo_Visible = (A1635Servico_IsPublico ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockresponsavel_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockresponsavel_codigo_Visible), 5, 0)));
         if ( ! A1635Servico_IsPublico )
         {
            AV21Servico_Responsavel = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Servico_Responsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21Servico_Responsavel), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Servico_Responsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21Servico_Responsavel), 6, 0)));
         }
         /* Using cursor T000V6 */
         pr_default.execute(4, new Object[] {n2047Servico_LinNegCod, A2047Servico_LinNegCod});
         if ( (pr_default.getStatus(4) == 101) )
         {
            if ( ! ( (0==A2047Servico_LinNegCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Servico_Linha Negocio'.", "ForeignKeyNotFound", 1, "SERVICO_LINNEGCOD");
               AnyError = 1;
               GX_FocusControl = dynServico_LinNegCod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         A2048Servico_LinNegDsc = T000V6_A2048Servico_LinNegDsc[0];
         n2048Servico_LinNegDsc = T000V6_n2048Servico_LinNegDsc[0];
         pr_default.close(4);
         /* Using cursor T000V5 */
         pr_default.execute(3, new Object[] {n633Servico_UO, A633Servico_UO});
         if ( (pr_default.getStatus(3) == 101) )
         {
            if ( ! ( (0==A633Servico_UO) ) )
            {
               GX_msglist.addItem("N�o existe 'Servico_UnidadeOrganizacional'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         pr_default.close(3);
      }

      protected void CloseExtendedTableCursors0V130( )
      {
         pr_default.close(2);
         pr_default.close(5);
         pr_default.close(4);
         pr_default.close(3);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_52( int A157ServicoGrupo_Codigo )
      {
         /* Using cursor T000V10 */
         pr_default.execute(8, new Object[] {A157ServicoGrupo_Codigo});
         if ( (pr_default.getStatus(8) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Servico Grupo'.", "ForeignKeyNotFound", 1, "SERVICOGRUPO_CODIGO");
            AnyError = 1;
            GX_FocusControl = dynServicoGrupo_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A158ServicoGrupo_Descricao = T000V10_A158ServicoGrupo_Descricao[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( A158ServicoGrupo_Descricao)+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(8) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(8);
      }

      protected void gxLoad_55( int A631Servico_Vinculado )
      {
         /* Using cursor T000V11 */
         pr_default.execute(9, new Object[] {n631Servico_Vinculado, A631Servico_Vinculado});
         if ( (pr_default.getStatus(9) == 101) )
         {
            if ( ! ( (0==A631Servico_Vinculado) ) )
            {
               GX_msglist.addItem("N�o existe 'Servico_Servico Vinculado'.", "ForeignKeyNotFound", 1, "SERVICO_VINCULADO");
               AnyError = 1;
               GX_FocusControl = dynServico_Vinculado_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         A641Servico_VincDesc = T000V11_A641Servico_VincDesc[0];
         n641Servico_VincDesc = T000V11_n641Servico_VincDesc[0];
         A1557Servico_VincSigla = T000V11_A1557Servico_VincSigla[0];
         n1557Servico_VincSigla = T000V11_n1557Servico_VincSigla[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( A641Servico_VincDesc)+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A1557Servico_VincSigla))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(9) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(9);
      }

      protected void gxLoad_54( int A2047Servico_LinNegCod )
      {
         /* Using cursor T000V12 */
         pr_default.execute(10, new Object[] {n2047Servico_LinNegCod, A2047Servico_LinNegCod});
         if ( (pr_default.getStatus(10) == 101) )
         {
            if ( ! ( (0==A2047Servico_LinNegCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Servico_Linha Negocio'.", "ForeignKeyNotFound", 1, "SERVICO_LINNEGCOD");
               AnyError = 1;
               GX_FocusControl = dynServico_LinNegCod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         A2048Servico_LinNegDsc = T000V12_A2048Servico_LinNegDsc[0];
         n2048Servico_LinNegDsc = T000V12_n2048Servico_LinNegDsc[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( A2048Servico_LinNegDsc)+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(10) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(10);
      }

      protected void gxLoad_53( int A633Servico_UO )
      {
         /* Using cursor T000V13 */
         pr_default.execute(11, new Object[] {n633Servico_UO, A633Servico_UO});
         if ( (pr_default.getStatus(11) == 101) )
         {
            if ( ! ( (0==A633Servico_UO) ) )
            {
               GX_msglist.addItem("N�o existe 'Servico_UnidadeOrganizacional'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(11) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(11);
      }

      protected void GetKey0V130( )
      {
         /* Using cursor T000V14 */
         pr_default.execute(12, new Object[] {A155Servico_Codigo});
         if ( (pr_default.getStatus(12) != 101) )
         {
            RcdFound130 = 1;
         }
         else
         {
            RcdFound130 = 0;
         }
         pr_default.close(12);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T000V3 */
         pr_default.execute(1, new Object[] {A155Servico_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM0V130( 51) ;
            RcdFound130 = 1;
            A155Servico_Codigo = T000V3_A155Servico_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
            A608Servico_Nome = T000V3_A608Servico_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A608Servico_Nome", A608Servico_Nome);
            A156Servico_Descricao = T000V3_A156Servico_Descricao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A156Servico_Descricao", A156Servico_Descricao);
            n156Servico_Descricao = T000V3_n156Servico_Descricao[0];
            A605Servico_Sigla = T000V3_A605Servico_Sigla[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A605Servico_Sigla", A605Servico_Sigla);
            A1077Servico_UORespExclusiva = T000V3_A1077Servico_UORespExclusiva[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1077Servico_UORespExclusiva", A1077Servico_UORespExclusiva);
            n1077Servico_UORespExclusiva = T000V3_n1077Servico_UORespExclusiva[0];
            A889Servico_Terceriza = T000V3_A889Servico_Terceriza[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A889Servico_Terceriza", A889Servico_Terceriza);
            n889Servico_Terceriza = T000V3_n889Servico_Terceriza[0];
            A1061Servico_Tela = T000V3_A1061Servico_Tela[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1061Servico_Tela", A1061Servico_Tela);
            n1061Servico_Tela = T000V3_n1061Servico_Tela[0];
            A1072Servico_Atende = T000V3_A1072Servico_Atende[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1072Servico_Atende", A1072Servico_Atende);
            n1072Servico_Atende = T000V3_n1072Servico_Atende[0];
            A1429Servico_ObrigaValores = T000V3_A1429Servico_ObrigaValores[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1429Servico_ObrigaValores", A1429Servico_ObrigaValores);
            n1429Servico_ObrigaValores = T000V3_n1429Servico_ObrigaValores[0];
            A1436Servico_ObjetoControle = T000V3_A1436Servico_ObjetoControle[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1436Servico_ObjetoControle", A1436Servico_ObjetoControle);
            A1530Servico_TipoHierarquia = T000V3_A1530Servico_TipoHierarquia[0];
            n1530Servico_TipoHierarquia = T000V3_n1530Servico_TipoHierarquia[0];
            A1534Servico_PercTmp = T000V3_A1534Servico_PercTmp[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1534Servico_PercTmp", StringUtil.LTrim( StringUtil.Str( (decimal)(A1534Servico_PercTmp), 4, 0)));
            n1534Servico_PercTmp = T000V3_n1534Servico_PercTmp[0];
            A1535Servico_PercPgm = T000V3_A1535Servico_PercPgm[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1535Servico_PercPgm", StringUtil.LTrim( StringUtil.Str( (decimal)(A1535Servico_PercPgm), 4, 0)));
            n1535Servico_PercPgm = T000V3_n1535Servico_PercPgm[0];
            A1536Servico_PercCnc = T000V3_A1536Servico_PercCnc[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1536Servico_PercCnc", StringUtil.LTrim( StringUtil.Str( (decimal)(A1536Servico_PercCnc), 4, 0)));
            n1536Servico_PercCnc = T000V3_n1536Servico_PercCnc[0];
            A1545Servico_Anterior = T000V3_A1545Servico_Anterior[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1545Servico_Anterior", StringUtil.LTrim( StringUtil.Str( (decimal)(A1545Servico_Anterior), 6, 0)));
            n1545Servico_Anterior = T000V3_n1545Servico_Anterior[0];
            A1546Servico_Posterior = T000V3_A1546Servico_Posterior[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1546Servico_Posterior", StringUtil.LTrim( StringUtil.Str( (decimal)(A1546Servico_Posterior), 6, 0)));
            n1546Servico_Posterior = T000V3_n1546Servico_Posterior[0];
            A632Servico_Ativo = T000V3_A632Servico_Ativo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A632Servico_Ativo", A632Servico_Ativo);
            A1635Servico_IsPublico = T000V3_A1635Servico_IsPublico[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1635Servico_IsPublico", A1635Servico_IsPublico);
            A2092Servico_IsOrigemReferencia = T000V3_A2092Servico_IsOrigemReferencia[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2092Servico_IsOrigemReferencia", A2092Servico_IsOrigemReferencia);
            A2131Servico_PausaSLA = T000V3_A2131Servico_PausaSLA[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2131Servico_PausaSLA", A2131Servico_PausaSLA);
            A157ServicoGrupo_Codigo = T000V3_A157ServicoGrupo_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A157ServicoGrupo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A157ServicoGrupo_Codigo), 6, 0)));
            A633Servico_UO = T000V3_A633Servico_UO[0];
            n633Servico_UO = T000V3_n633Servico_UO[0];
            A2047Servico_LinNegCod = T000V3_A2047Servico_LinNegCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2047Servico_LinNegCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2047Servico_LinNegCod), 6, 0)));
            n2047Servico_LinNegCod = T000V3_n2047Servico_LinNegCod[0];
            A631Servico_Vinculado = T000V3_A631Servico_Vinculado[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A631Servico_Vinculado", StringUtil.LTrim( StringUtil.Str( (decimal)(A631Servico_Vinculado), 6, 0)));
            n631Servico_Vinculado = T000V3_n631Servico_Vinculado[0];
            Z155Servico_Codigo = A155Servico_Codigo;
            sMode130 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load0V130( ) ;
            if ( AnyError == 1 )
            {
               RcdFound130 = 0;
               InitializeNonKey0V130( ) ;
            }
            Gx_mode = sMode130;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound130 = 0;
            InitializeNonKey0V130( ) ;
            sMode130 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal( ) ;
            Gx_mode = sMode130;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey0V130( ) ;
         if ( RcdFound130 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound130 = 0;
         /* Using cursor T000V15 */
         pr_default.execute(13, new Object[] {A155Servico_Codigo});
         if ( (pr_default.getStatus(13) != 101) )
         {
            while ( (pr_default.getStatus(13) != 101) && ( ( T000V15_A155Servico_Codigo[0] < A155Servico_Codigo ) ) )
            {
               pr_default.readNext(13);
            }
            if ( (pr_default.getStatus(13) != 101) && ( ( T000V15_A155Servico_Codigo[0] > A155Servico_Codigo ) ) )
            {
               A155Servico_Codigo = T000V15_A155Servico_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
               RcdFound130 = 1;
            }
         }
         pr_default.close(13);
      }

      protected void move_previous( )
      {
         RcdFound130 = 0;
         /* Using cursor T000V16 */
         pr_default.execute(14, new Object[] {A155Servico_Codigo});
         if ( (pr_default.getStatus(14) != 101) )
         {
            while ( (pr_default.getStatus(14) != 101) && ( ( T000V16_A155Servico_Codigo[0] > A155Servico_Codigo ) ) )
            {
               pr_default.readNext(14);
            }
            if ( (pr_default.getStatus(14) != 101) && ( ( T000V16_A155Servico_Codigo[0] < A155Servico_Codigo ) ) )
            {
               A155Servico_Codigo = T000V16_A155Servico_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
               RcdFound130 = 1;
            }
         }
         pr_default.close(14);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey0V130( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = cmbServico_UORespExclusiva_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert0V130( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound130 == 1 )
            {
               if ( A155Servico_Codigo != Z155Servico_Codigo )
               {
                  A155Servico_Codigo = Z155Servico_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "SERVICO_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtServico_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = cmbServico_UORespExclusiva_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  Update0V130( ) ;
                  GX_FocusControl = cmbServico_UORespExclusiva_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A155Servico_Codigo != Z155Servico_Codigo )
               {
                  /* Insert record */
                  GX_FocusControl = cmbServico_UORespExclusiva_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert0V130( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "SERVICO_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtServico_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     /* Insert record */
                     GX_FocusControl = cmbServico_UORespExclusiva_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert0V130( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( A155Servico_Codigo != Z155Servico_Codigo )
         {
            A155Servico_Codigo = Z155Servico_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "SERVICO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtServico_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = cmbServico_UORespExclusiva_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency0V130( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T000V2 */
            pr_default.execute(0, new Object[] {A155Servico_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Servico"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            Gx_longc = false;
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z608Servico_Nome, T000V2_A608Servico_Nome[0]) != 0 ) || ( StringUtil.StrCmp(Z605Servico_Sigla, T000V2_A605Servico_Sigla[0]) != 0 ) || ( Z1077Servico_UORespExclusiva != T000V2_A1077Servico_UORespExclusiva[0] ) || ( Z889Servico_Terceriza != T000V2_A889Servico_Terceriza[0] ) || ( StringUtil.StrCmp(Z1061Servico_Tela, T000V2_A1061Servico_Tela[0]) != 0 ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( StringUtil.StrCmp(Z1072Servico_Atende, T000V2_A1072Servico_Atende[0]) != 0 ) || ( StringUtil.StrCmp(Z1429Servico_ObrigaValores, T000V2_A1429Servico_ObrigaValores[0]) != 0 ) || ( StringUtil.StrCmp(Z1436Servico_ObjetoControle, T000V2_A1436Servico_ObjetoControle[0]) != 0 ) || ( Z1530Servico_TipoHierarquia != T000V2_A1530Servico_TipoHierarquia[0] ) || ( Z1534Servico_PercTmp != T000V2_A1534Servico_PercTmp[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z1535Servico_PercPgm != T000V2_A1535Servico_PercPgm[0] ) || ( Z1536Servico_PercCnc != T000V2_A1536Servico_PercCnc[0] ) || ( Z1545Servico_Anterior != T000V2_A1545Servico_Anterior[0] ) || ( Z1546Servico_Posterior != T000V2_A1546Servico_Posterior[0] ) || ( Z632Servico_Ativo != T000V2_A632Servico_Ativo[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z1635Servico_IsPublico != T000V2_A1635Servico_IsPublico[0] ) || ( Z2092Servico_IsOrigemReferencia != T000V2_A2092Servico_IsOrigemReferencia[0] ) || ( Z2131Servico_PausaSLA != T000V2_A2131Servico_PausaSLA[0] ) || ( Z157ServicoGrupo_Codigo != T000V2_A157ServicoGrupo_Codigo[0] ) || ( Z633Servico_UO != T000V2_A633Servico_UO[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z2047Servico_LinNegCod != T000V2_A2047Servico_LinNegCod[0] ) || ( Z631Servico_Vinculado != T000V2_A631Servico_Vinculado[0] ) )
            {
               if ( StringUtil.StrCmp(Z608Servico_Nome, T000V2_A608Servico_Nome[0]) != 0 )
               {
                  GXUtil.WriteLog("servico:[seudo value changed for attri]"+"Servico_Nome");
                  GXUtil.WriteLogRaw("Old: ",Z608Servico_Nome);
                  GXUtil.WriteLogRaw("Current: ",T000V2_A608Servico_Nome[0]);
               }
               if ( StringUtil.StrCmp(Z605Servico_Sigla, T000V2_A605Servico_Sigla[0]) != 0 )
               {
                  GXUtil.WriteLog("servico:[seudo value changed for attri]"+"Servico_Sigla");
                  GXUtil.WriteLogRaw("Old: ",Z605Servico_Sigla);
                  GXUtil.WriteLogRaw("Current: ",T000V2_A605Servico_Sigla[0]);
               }
               if ( Z1077Servico_UORespExclusiva != T000V2_A1077Servico_UORespExclusiva[0] )
               {
                  GXUtil.WriteLog("servico:[seudo value changed for attri]"+"Servico_UORespExclusiva");
                  GXUtil.WriteLogRaw("Old: ",Z1077Servico_UORespExclusiva);
                  GXUtil.WriteLogRaw("Current: ",T000V2_A1077Servico_UORespExclusiva[0]);
               }
               if ( Z889Servico_Terceriza != T000V2_A889Servico_Terceriza[0] )
               {
                  GXUtil.WriteLog("servico:[seudo value changed for attri]"+"Servico_Terceriza");
                  GXUtil.WriteLogRaw("Old: ",Z889Servico_Terceriza);
                  GXUtil.WriteLogRaw("Current: ",T000V2_A889Servico_Terceriza[0]);
               }
               if ( StringUtil.StrCmp(Z1061Servico_Tela, T000V2_A1061Servico_Tela[0]) != 0 )
               {
                  GXUtil.WriteLog("servico:[seudo value changed for attri]"+"Servico_Tela");
                  GXUtil.WriteLogRaw("Old: ",Z1061Servico_Tela);
                  GXUtil.WriteLogRaw("Current: ",T000V2_A1061Servico_Tela[0]);
               }
               if ( StringUtil.StrCmp(Z1072Servico_Atende, T000V2_A1072Servico_Atende[0]) != 0 )
               {
                  GXUtil.WriteLog("servico:[seudo value changed for attri]"+"Servico_Atende");
                  GXUtil.WriteLogRaw("Old: ",Z1072Servico_Atende);
                  GXUtil.WriteLogRaw("Current: ",T000V2_A1072Servico_Atende[0]);
               }
               if ( StringUtil.StrCmp(Z1429Servico_ObrigaValores, T000V2_A1429Servico_ObrigaValores[0]) != 0 )
               {
                  GXUtil.WriteLog("servico:[seudo value changed for attri]"+"Servico_ObrigaValores");
                  GXUtil.WriteLogRaw("Old: ",Z1429Servico_ObrigaValores);
                  GXUtil.WriteLogRaw("Current: ",T000V2_A1429Servico_ObrigaValores[0]);
               }
               if ( StringUtil.StrCmp(Z1436Servico_ObjetoControle, T000V2_A1436Servico_ObjetoControle[0]) != 0 )
               {
                  GXUtil.WriteLog("servico:[seudo value changed for attri]"+"Servico_ObjetoControle");
                  GXUtil.WriteLogRaw("Old: ",Z1436Servico_ObjetoControle);
                  GXUtil.WriteLogRaw("Current: ",T000V2_A1436Servico_ObjetoControle[0]);
               }
               if ( Z1530Servico_TipoHierarquia != T000V2_A1530Servico_TipoHierarquia[0] )
               {
                  GXUtil.WriteLog("servico:[seudo value changed for attri]"+"Servico_TipoHierarquia");
                  GXUtil.WriteLogRaw("Old: ",Z1530Servico_TipoHierarquia);
                  GXUtil.WriteLogRaw("Current: ",T000V2_A1530Servico_TipoHierarquia[0]);
               }
               if ( Z1534Servico_PercTmp != T000V2_A1534Servico_PercTmp[0] )
               {
                  GXUtil.WriteLog("servico:[seudo value changed for attri]"+"Servico_PercTmp");
                  GXUtil.WriteLogRaw("Old: ",Z1534Servico_PercTmp);
                  GXUtil.WriteLogRaw("Current: ",T000V2_A1534Servico_PercTmp[0]);
               }
               if ( Z1535Servico_PercPgm != T000V2_A1535Servico_PercPgm[0] )
               {
                  GXUtil.WriteLog("servico:[seudo value changed for attri]"+"Servico_PercPgm");
                  GXUtil.WriteLogRaw("Old: ",Z1535Servico_PercPgm);
                  GXUtil.WriteLogRaw("Current: ",T000V2_A1535Servico_PercPgm[0]);
               }
               if ( Z1536Servico_PercCnc != T000V2_A1536Servico_PercCnc[0] )
               {
                  GXUtil.WriteLog("servico:[seudo value changed for attri]"+"Servico_PercCnc");
                  GXUtil.WriteLogRaw("Old: ",Z1536Servico_PercCnc);
                  GXUtil.WriteLogRaw("Current: ",T000V2_A1536Servico_PercCnc[0]);
               }
               if ( Z1545Servico_Anterior != T000V2_A1545Servico_Anterior[0] )
               {
                  GXUtil.WriteLog("servico:[seudo value changed for attri]"+"Servico_Anterior");
                  GXUtil.WriteLogRaw("Old: ",Z1545Servico_Anterior);
                  GXUtil.WriteLogRaw("Current: ",T000V2_A1545Servico_Anterior[0]);
               }
               if ( Z1546Servico_Posterior != T000V2_A1546Servico_Posterior[0] )
               {
                  GXUtil.WriteLog("servico:[seudo value changed for attri]"+"Servico_Posterior");
                  GXUtil.WriteLogRaw("Old: ",Z1546Servico_Posterior);
                  GXUtil.WriteLogRaw("Current: ",T000V2_A1546Servico_Posterior[0]);
               }
               if ( Z632Servico_Ativo != T000V2_A632Servico_Ativo[0] )
               {
                  GXUtil.WriteLog("servico:[seudo value changed for attri]"+"Servico_Ativo");
                  GXUtil.WriteLogRaw("Old: ",Z632Servico_Ativo);
                  GXUtil.WriteLogRaw("Current: ",T000V2_A632Servico_Ativo[0]);
               }
               if ( Z1635Servico_IsPublico != T000V2_A1635Servico_IsPublico[0] )
               {
                  GXUtil.WriteLog("servico:[seudo value changed for attri]"+"Servico_IsPublico");
                  GXUtil.WriteLogRaw("Old: ",Z1635Servico_IsPublico);
                  GXUtil.WriteLogRaw("Current: ",T000V2_A1635Servico_IsPublico[0]);
               }
               if ( Z2092Servico_IsOrigemReferencia != T000V2_A2092Servico_IsOrigemReferencia[0] )
               {
                  GXUtil.WriteLog("servico:[seudo value changed for attri]"+"Servico_IsOrigemReferencia");
                  GXUtil.WriteLogRaw("Old: ",Z2092Servico_IsOrigemReferencia);
                  GXUtil.WriteLogRaw("Current: ",T000V2_A2092Servico_IsOrigemReferencia[0]);
               }
               if ( Z2131Servico_PausaSLA != T000V2_A2131Servico_PausaSLA[0] )
               {
                  GXUtil.WriteLog("servico:[seudo value changed for attri]"+"Servico_PausaSLA");
                  GXUtil.WriteLogRaw("Old: ",Z2131Servico_PausaSLA);
                  GXUtil.WriteLogRaw("Current: ",T000V2_A2131Servico_PausaSLA[0]);
               }
               if ( Z157ServicoGrupo_Codigo != T000V2_A157ServicoGrupo_Codigo[0] )
               {
                  GXUtil.WriteLog("servico:[seudo value changed for attri]"+"ServicoGrupo_Codigo");
                  GXUtil.WriteLogRaw("Old: ",Z157ServicoGrupo_Codigo);
                  GXUtil.WriteLogRaw("Current: ",T000V2_A157ServicoGrupo_Codigo[0]);
               }
               if ( Z633Servico_UO != T000V2_A633Servico_UO[0] )
               {
                  GXUtil.WriteLog("servico:[seudo value changed for attri]"+"Servico_UO");
                  GXUtil.WriteLogRaw("Old: ",Z633Servico_UO);
                  GXUtil.WriteLogRaw("Current: ",T000V2_A633Servico_UO[0]);
               }
               if ( Z2047Servico_LinNegCod != T000V2_A2047Servico_LinNegCod[0] )
               {
                  GXUtil.WriteLog("servico:[seudo value changed for attri]"+"Servico_LinNegCod");
                  GXUtil.WriteLogRaw("Old: ",Z2047Servico_LinNegCod);
                  GXUtil.WriteLogRaw("Current: ",T000V2_A2047Servico_LinNegCod[0]);
               }
               if ( Z631Servico_Vinculado != T000V2_A631Servico_Vinculado[0] )
               {
                  GXUtil.WriteLog("servico:[seudo value changed for attri]"+"Servico_Vinculado");
                  GXUtil.WriteLogRaw("Old: ",Z631Servico_Vinculado);
                  GXUtil.WriteLogRaw("Current: ",T000V2_A631Servico_Vinculado[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"Servico"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert0V130( )
      {
         BeforeValidate0V130( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable0V130( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM0V130( 0) ;
            CheckOptimisticConcurrency0V130( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm0V130( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert0V130( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000V17 */
                     pr_default.execute(15, new Object[] {A608Servico_Nome, n156Servico_Descricao, A156Servico_Descricao, A605Servico_Sigla, n1077Servico_UORespExclusiva, A1077Servico_UORespExclusiva, n889Servico_Terceriza, A889Servico_Terceriza, n1061Servico_Tela, A1061Servico_Tela, n1072Servico_Atende, A1072Servico_Atende, n1429Servico_ObrigaValores, A1429Servico_ObrigaValores, A1436Servico_ObjetoControle, n1530Servico_TipoHierarquia, A1530Servico_TipoHierarquia, n1534Servico_PercTmp, A1534Servico_PercTmp, n1535Servico_PercPgm, A1535Servico_PercPgm, n1536Servico_PercCnc, A1536Servico_PercCnc, n1545Servico_Anterior, A1545Servico_Anterior, n1546Servico_Posterior, A1546Servico_Posterior, A632Servico_Ativo, A1635Servico_IsPublico, A2092Servico_IsOrigemReferencia, A2131Servico_PausaSLA, A157ServicoGrupo_Codigo, n633Servico_UO, A633Servico_UO, n2047Servico_LinNegCod, A2047Servico_LinNegCod, n631Servico_Vinculado, A631Servico_Vinculado});
                     A155Servico_Codigo = T000V17_A155Servico_Codigo[0];
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
                     pr_default.close(15);
                     dsDefault.SmartCacheProvider.SetUpdated("Servico") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        new prc_updservico_responsavel(context ).execute(  ((StringUtil.StrCmp(Gx_mode, "DLT")==0) ? "DLT" : "UPD"),  AV20Contratante_Codigo,  AV7Servico_Codigo,  AV22Responsavel_Codigo) ;
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption0V0( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load0V130( ) ;
            }
            EndLevel0V130( ) ;
         }
         CloseExtendedTableCursors0V130( ) ;
      }

      protected void Update0V130( )
      {
         BeforeValidate0V130( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable0V130( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency0V130( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm0V130( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate0V130( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000V18 */
                     pr_default.execute(16, new Object[] {A608Servico_Nome, n156Servico_Descricao, A156Servico_Descricao, A605Servico_Sigla, n1077Servico_UORespExclusiva, A1077Servico_UORespExclusiva, n889Servico_Terceriza, A889Servico_Terceriza, n1061Servico_Tela, A1061Servico_Tela, n1072Servico_Atende, A1072Servico_Atende, n1429Servico_ObrigaValores, A1429Servico_ObrigaValores, A1436Servico_ObjetoControle, n1530Servico_TipoHierarquia, A1530Servico_TipoHierarquia, n1534Servico_PercTmp, A1534Servico_PercTmp, n1535Servico_PercPgm, A1535Servico_PercPgm, n1536Servico_PercCnc, A1536Servico_PercCnc, n1545Servico_Anterior, A1545Servico_Anterior, n1546Servico_Posterior, A1546Servico_Posterior, A632Servico_Ativo, A1635Servico_IsPublico, A2092Servico_IsOrigemReferencia, A2131Servico_PausaSLA, A157ServicoGrupo_Codigo, n633Servico_UO, A633Servico_UO, n2047Servico_LinNegCod, A2047Servico_LinNegCod, n631Servico_Vinculado, A631Servico_Vinculado, A155Servico_Codigo});
                     pr_default.close(16);
                     dsDefault.SmartCacheProvider.SetUpdated("Servico") ;
                     if ( (pr_default.getStatus(16) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Servico"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate0V130( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        new prc_updservico_responsavel(context ).execute(  ((StringUtil.StrCmp(Gx_mode, "DLT")==0) ? "DLT" : "UPD"),  AV20Contratante_Codigo,  AV7Servico_Codigo,  AV22Responsavel_Codigo) ;
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel0V130( ) ;
         }
         CloseExtendedTableCursors0V130( ) ;
      }

      protected void DeferredUpdate0V130( )
      {
      }

      protected void delete( )
      {
         BeforeValidate0V130( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency0V130( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls0V130( ) ;
            AfterConfirm0V130( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete0V130( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T000V19 */
                  pr_default.execute(17, new Object[] {A155Servico_Codigo});
                  pr_default.close(17);
                  dsDefault.SmartCacheProvider.SetUpdated("Servico") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                        {
                           if ( AnyError == 0 )
                           {
                              context.nUserReturn = 1;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode130 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel0V130( ) ;
         Gx_mode = sMode130;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls0V130( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            GXt_int1 = A1551Servico_Responsavel;
            new prc_getservico_responsavel(context ).execute( ref  A155Servico_Codigo, out  GXt_int1) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
            A1551Servico_Responsavel = GXt_int1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1551Servico_Responsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(A1551Servico_Responsavel), 6, 0)));
            GXt_int2 = A640Servico_Vinculados;
            new prc_qtdeservicosvinculados(context ).execute(  A155Servico_Codigo, out  GXt_int2) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
            A640Servico_Vinculados = GXt_int2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A640Servico_Vinculados", StringUtil.LTrim( StringUtil.Str( (decimal)(A640Servico_Vinculados), 4, 0)));
            A2107Servico_Identificacao = StringUtil.Concat( StringUtil.Trim( A605Servico_Sigla), StringUtil.Trim( A608Servico_Nome), " - ");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2107Servico_Identificacao", A2107Servico_Identificacao);
            /* Using cursor T000V20 */
            pr_default.execute(18, new Object[] {A157ServicoGrupo_Codigo});
            A158ServicoGrupo_Descricao = T000V20_A158ServicoGrupo_Descricao[0];
            pr_default.close(18);
            /* Using cursor T000V21 */
            pr_default.execute(19, new Object[] {n631Servico_Vinculado, A631Servico_Vinculado});
            A641Servico_VincDesc = T000V21_A641Servico_VincDesc[0];
            n641Servico_VincDesc = T000V21_n641Servico_VincDesc[0];
            A1557Servico_VincSigla = T000V21_A1557Servico_VincSigla[0];
            n1557Servico_VincSigla = T000V21_n1557Servico_VincSigla[0];
            pr_default.close(19);
            edtServico_PercTmp_Visible = (T000V3_n631Servico_Vinculado[0] ? 1 : 0);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtServico_PercTmp_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtServico_PercTmp_Visible), 5, 0)));
            if ( ! ( T000V3_n631Servico_Vinculado[0] ) )
            {
               cellServico_perctmp_cell_Class = "Invisible";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellServico_perctmp_cell_Internalname, "Class", cellServico_perctmp_cell_Class);
            }
            else
            {
               if ( T000V3_n631Servico_Vinculado[0] )
               {
                  cellServico_perctmp_cell_Class = "DataContentCell";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellServico_perctmp_cell_Internalname, "Class", cellServico_perctmp_cell_Class);
               }
            }
            if ( ! ( T000V3_n631Servico_Vinculado[0] ) )
            {
               cellTextblockservico_perctmp_cell_Class = "Invisible";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellTextblockservico_perctmp_cell_Internalname, "Class", cellTextblockservico_perctmp_cell_Class);
            }
            else
            {
               if ( T000V3_n631Servico_Vinculado[0] )
               {
                  cellTextblockservico_perctmp_cell_Class = "DataDescriptionCell";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellTextblockservico_perctmp_cell_Internalname, "Class", cellTextblockservico_perctmp_cell_Class);
               }
            }
            GXASERVICO_VINCULADO_html0V130( AV16ServicoGrupo_Codigo, AV7Servico_Codigo, A157ServicoGrupo_Codigo, A640Servico_Vinculados, A632Servico_Ativo) ;
            GXASERVICO_ANTERIOR_html0V130( AV7Servico_Codigo, A632Servico_Ativo) ;
            GXASERVICO_POSTERIOR_html0V130( AV7Servico_Codigo, A632Servico_Ativo) ;
            dynavResponsavel_codigo.Visible = (A1635Servico_IsPublico ? 1 : 0);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavResponsavel_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavResponsavel_codigo.Visible), 5, 0)));
            lblTextblockresponsavel_codigo_Visible = (A1635Servico_IsPublico ? 1 : 0);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockresponsavel_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockresponsavel_codigo_Visible), 5, 0)));
            if ( ! A1635Servico_IsPublico )
            {
               AV21Servico_Responsavel = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Servico_Responsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21Servico_Responsavel), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Servico_Responsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21Servico_Responsavel), 6, 0)));
            }
            /* Using cursor T000V22 */
            pr_default.execute(20, new Object[] {n2047Servico_LinNegCod, A2047Servico_LinNegCod});
            A2048Servico_LinNegDsc = T000V22_A2048Servico_LinNegDsc[0];
            n2048Servico_LinNegDsc = T000V22_n2048Servico_LinNegDsc[0];
            pr_default.close(20);
            if ( (0==AV17Servico_UO) && ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               AV17Servico_UO = A633Servico_UO;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Servico_UO", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17Servico_UO), 6, 0)));
            }
         }
         if ( AnyError == 0 )
         {
            /* Using cursor T000V23 */
            pr_default.execute(21, new Object[] {A155Servico_Codigo});
            if ( (pr_default.getStatus(21) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Servico"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(21);
            /* Using cursor T000V24 */
            pr_default.execute(22, new Object[] {A155Servico_Codigo});
            if ( (pr_default.getStatus(22) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Servico Fluxo"+" ("+"Servico Fluxo_Servico Posterior"+")"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(22);
            /* Using cursor T000V25 */
            pr_default.execute(23, new Object[] {A155Servico_Codigo});
            if ( (pr_default.getStatus(23) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Servico Fluxo"+" ("+"Servico_Servico Fluxo"+")"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(23);
            /* Using cursor T000V26 */
            pr_default.execute(24, new Object[] {A155Servico_Codigo});
            if ( (pr_default.getStatus(24) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Prioridades padr�o"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(24);
            /* Using cursor T000V27 */
            pr_default.execute(25, new Object[] {A155Servico_Codigo});
            if ( (pr_default.getStatus(25) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Sistemas do Servi�o"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(25);
            /* Using cursor T000V28 */
            pr_default.execute(26, new Object[] {A155Servico_Codigo});
            if ( (pr_default.getStatus(26) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Area de Trabalho"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(26);
            /* Using cursor T000V29 */
            pr_default.execute(27, new Object[] {A155Servico_Codigo});
            if ( (pr_default.getStatus(27) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Usuario Servicos"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(27);
            /* Using cursor T000V30 */
            pr_default.execute(28, new Object[] {A155Servico_Codigo});
            if ( (pr_default.getStatus(28) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Resultado das Contagens"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(28);
            /* Using cursor T000V31 */
            pr_default.execute(29, new Object[] {A155Servico_Codigo});
            if ( (pr_default.getStatus(29) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Solicitacoes"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(29);
            /* Using cursor T000V32 */
            pr_default.execute(30, new Object[] {A155Servico_Codigo});
            if ( (pr_default.getStatus(30) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Servico Ans"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(30);
            /* Using cursor T000V33 */
            pr_default.execute(31, new Object[] {A155Servico_Codigo});
            if ( (pr_default.getStatus(31) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contrato Servicos"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(31);
            /* Using cursor T000V34 */
            pr_default.execute(32, new Object[] {A155Servico_Codigo});
            if ( (pr_default.getStatus(32) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Check Lists"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(32);
            /* Using cursor T000V35 */
            pr_default.execute(33, new Object[] {A155Servico_Codigo});
            if ( (pr_default.getStatus(33) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Artefatos"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(33);
         }
      }

      protected void EndLevel0V130( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete0V130( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            pr_default.close(18);
            pr_default.close(20);
            pr_default.close(19);
            context.CommitDataStores( "Servico");
            if ( AnyError == 0 )
            {
               ConfirmValues0V0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            pr_default.close(18);
            pr_default.close(20);
            pr_default.close(19);
            context.RollbackDataStores( "Servico");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart0V130( )
      {
         /* Scan By routine */
         /* Using cursor T000V36 */
         pr_default.execute(34);
         RcdFound130 = 0;
         if ( (pr_default.getStatus(34) != 101) )
         {
            RcdFound130 = 1;
            A155Servico_Codigo = T000V36_A155Servico_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext0V130( )
      {
         /* Scan next routine */
         pr_default.readNext(34);
         RcdFound130 = 0;
         if ( (pr_default.getStatus(34) != 101) )
         {
            RcdFound130 = 1;
            A155Servico_Codigo = T000V36_A155Servico_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
         }
      }

      protected void ScanEnd0V130( )
      {
         pr_default.close(34);
      }

      protected void AfterConfirm0V130( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert0V130( )
      {
         /* Before Insert Rules */
         if ( (0==AV17Servico_UO) )
         {
            A633Servico_UO = 0;
            n633Servico_UO = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A633Servico_UO", StringUtil.LTrim( StringUtil.Str( (decimal)(A633Servico_UO), 6, 0)));
            n633Servico_UO = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A633Servico_UO", StringUtil.LTrim( StringUtil.Str( (decimal)(A633Servico_UO), 6, 0)));
         }
         else
         {
            if ( ! (0==AV17Servico_UO) )
            {
               A633Servico_UO = AV17Servico_UO;
               n633Servico_UO = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A633Servico_UO", StringUtil.LTrim( StringUtil.Str( (decimal)(A633Servico_UO), 6, 0)));
            }
         }
      }

      protected void BeforeUpdate0V130( )
      {
         /* Before Update Rules */
         if ( (0==AV17Servico_UO) )
         {
            A633Servico_UO = 0;
            n633Servico_UO = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A633Servico_UO", StringUtil.LTrim( StringUtil.Str( (decimal)(A633Servico_UO), 6, 0)));
            n633Servico_UO = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A633Servico_UO", StringUtil.LTrim( StringUtil.Str( (decimal)(A633Servico_UO), 6, 0)));
         }
         else
         {
            if ( ! (0==AV17Servico_UO) )
            {
               A633Servico_UO = AV17Servico_UO;
               n633Servico_UO = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A633Servico_UO", StringUtil.LTrim( StringUtil.Str( (decimal)(A633Servico_UO), 6, 0)));
            }
         }
         new loadauditservico(context ).execute(  "Y", ref  AV19AuditingObject,  A155Servico_Codigo,  Gx_mode) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void BeforeDelete0V130( )
      {
         /* Before Delete Rules */
         new loadauditservico(context ).execute(  "Y", ref  AV19AuditingObject,  A155Servico_Codigo,  Gx_mode) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void BeforeComplete0V130( )
      {
         /* Before Complete Rules */
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            new loadauditservico(context ).execute(  "N", ref  AV19AuditingObject,  A155Servico_Codigo,  Gx_mode) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )  )
         {
            new loadauditservico(context ).execute(  "N", ref  AV19AuditingObject,  A155Servico_Codigo,  Gx_mode) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
      }

      protected void BeforeValidate0V130( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes0V130( )
      {
         dynavServico_uo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavServico_uo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavServico_uo.Enabled), 5, 0)));
         cmbServico_UORespExclusiva.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbServico_UORespExclusiva_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbServico_UORespExclusiva.Enabled), 5, 0)));
         dynServicoGrupo_Codigo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynServicoGrupo_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynServicoGrupo_Codigo.Enabled), 5, 0)));
         edtServico_Nome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtServico_Nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtServico_Nome_Enabled), 5, 0)));
         edtServico_Sigla_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtServico_Sigla_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtServico_Sigla_Enabled), 5, 0)));
         dynServico_Vinculado.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynServico_Vinculado_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynServico_Vinculado.Enabled), 5, 0)));
         edtServico_Descricao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtServico_Descricao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtServico_Descricao_Enabled), 5, 0)));
         dynServico_Anterior.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynServico_Anterior_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynServico_Anterior.Enabled), 5, 0)));
         dynServico_Posterior.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynServico_Posterior_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynServico_Posterior.Enabled), 5, 0)));
         cmbServico_Terceriza.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbServico_Terceriza_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbServico_Terceriza.Enabled), 5, 0)));
         cmbServico_Tela.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbServico_Tela_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbServico_Tela.Enabled), 5, 0)));
         cmbServico_Atende.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbServico_Atende_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbServico_Atende.Enabled), 5, 0)));
         cmbServico_ObrigaValores.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbServico_ObrigaValores_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbServico_ObrigaValores.Enabled), 5, 0)));
         cmbServico_ObjetoControle.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbServico_ObjetoControle_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbServico_ObjetoControle.Enabled), 5, 0)));
         edtServico_PercPgm_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtServico_PercPgm_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtServico_PercPgm_Enabled), 5, 0)));
         edtServico_PercCnc_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtServico_PercCnc_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtServico_PercCnc_Enabled), 5, 0)));
         edtServico_PercTmp_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtServico_PercTmp_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtServico_PercTmp_Enabled), 5, 0)));
         cmbServico_IsPublico.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbServico_IsPublico_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbServico_IsPublico.Enabled), 5, 0)));
         dynavResponsavel_codigo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavResponsavel_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavResponsavel_codigo.Enabled), 5, 0)));
         dynServico_LinNegCod.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynServico_LinNegCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynServico_LinNegCod.Enabled), 5, 0)));
         cmbServico_IsOrigemReferencia.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbServico_IsOrigemReferencia_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbServico_IsOrigemReferencia.Enabled), 5, 0)));
         cmbServico_PausaSLA.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbServico_PausaSLA_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbServico_PausaSLA.Enabled), 5, 0)));
         cmbServico_Ativo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbServico_Ativo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbServico_Ativo.Enabled), 5, 0)));
         edtavContratante_codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratante_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratante_codigo_Enabled), 5, 0)));
         edtServico_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtServico_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtServico_Codigo_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues0V0( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20206216152480");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("servico.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7Servico_Codigo) + "," + UrlEncode("" +AV16ServicoGrupo_Codigo)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z155Servico_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z155Servico_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z608Servico_Nome", StringUtil.RTrim( Z608Servico_Nome));
         GxWebStd.gx_hidden_field( context, "Z605Servico_Sigla", StringUtil.RTrim( Z605Servico_Sigla));
         GxWebStd.gx_boolean_hidden_field( context, "Z1077Servico_UORespExclusiva", Z1077Servico_UORespExclusiva);
         GxWebStd.gx_boolean_hidden_field( context, "Z889Servico_Terceriza", Z889Servico_Terceriza);
         GxWebStd.gx_hidden_field( context, "Z1061Servico_Tela", StringUtil.RTrim( Z1061Servico_Tela));
         GxWebStd.gx_hidden_field( context, "Z1072Servico_Atende", StringUtil.RTrim( Z1072Servico_Atende));
         GxWebStd.gx_hidden_field( context, "Z1429Servico_ObrigaValores", StringUtil.RTrim( Z1429Servico_ObrigaValores));
         GxWebStd.gx_hidden_field( context, "Z1436Servico_ObjetoControle", StringUtil.RTrim( Z1436Servico_ObjetoControle));
         GxWebStd.gx_hidden_field( context, "Z1530Servico_TipoHierarquia", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1530Servico_TipoHierarquia), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1534Servico_PercTmp", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1534Servico_PercTmp), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1535Servico_PercPgm", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1535Servico_PercPgm), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1536Servico_PercCnc", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1536Servico_PercCnc), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1545Servico_Anterior", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1545Servico_Anterior), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1546Servico_Posterior", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1546Servico_Posterior), 6, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "Z632Servico_Ativo", Z632Servico_Ativo);
         GxWebStd.gx_boolean_hidden_field( context, "Z1635Servico_IsPublico", Z1635Servico_IsPublico);
         GxWebStd.gx_boolean_hidden_field( context, "Z2092Servico_IsOrigemReferencia", Z2092Servico_IsOrigemReferencia);
         GxWebStd.gx_boolean_hidden_field( context, "Z2131Servico_PausaSLA", Z2131Servico_PausaSLA);
         GxWebStd.gx_hidden_field( context, "Z157ServicoGrupo_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z157ServicoGrupo_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z633Servico_UO", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z633Servico_UO), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z2047Servico_LinNegCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z2047Servico_LinNegCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z631Servico_Vinculado", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z631Servico_Vinculado), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "N157ServicoGrupo_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(A157ServicoGrupo_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "N633Servico_UO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A633Servico_UO), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "N631Servico_Vinculado", StringUtil.LTrim( StringUtil.NToC( (decimal)(A631Servico_Vinculado), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "N2047Servico_LinNegCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(A2047Servico_LinNegCod), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV9TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV9TrnContext);
         }
         GxWebStd.gx_hidden_field( context, "SERVICO_RESPONSAVEL", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1551Servico_Responsavel), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "SERVICO_VINCULADOS", StringUtil.LTrim( StringUtil.NToC( (decimal)(A640Servico_Vinculados), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "SERVICO_IDENTIFICACAO", A2107Servico_Identificacao);
         GxWebStd.gx_hidden_field( context, "vSERVICO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7Servico_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_SERVICOGRUPO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV11Insert_ServicoGrupo_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_SERVICO_UO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV15Insert_Servico_UO), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "SERVICO_UO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A633Servico_UO), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_SERVICO_VINCULADO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV14Insert_Servico_Vinculado), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_SERVICO_LINNEGCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV24Insert_Servico_LinNegCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vSERVICO_RESPONSAVEL", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV21Servico_Responsavel), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGXBSCREEN", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gx_BScreen), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "SERVICO_TIPOHIERARQUIA", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1530Servico_TipoHierarquia), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vSERVICOGRUPO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV16ServicoGrupo_Codigo), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vWWPCONTEXT", AV8WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vWWPCONTEXT", AV8WWPContext);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vAUDITINGOBJECT", AV19AuditingObject);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vAUDITINGOBJECT", AV19AuditingObject);
         }
         GxWebStd.gx_hidden_field( context, "SERVICOGRUPO_DESCRICAO", A158ServicoGrupo_Descricao);
         GxWebStd.gx_hidden_field( context, "SERVICO_LINNEGDSC", A2048Servico_LinNegDsc);
         GxWebStd.gx_hidden_field( context, "SERVICO_VINCDESC", A641Servico_VincDesc);
         GxWebStd.gx_hidden_field( context, "SERVICO_VINCSIGLA", StringUtil.RTrim( A1557Servico_VincSigla));
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV25Pgmname));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vSERVICO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7Servico_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Width", StringUtil.RTrim( Dvpanel_tableattributes_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Cls", StringUtil.RTrim( Dvpanel_tableattributes_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Title", StringUtil.RTrim( Dvpanel_tableattributes_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsible", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsed", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Enabled", StringUtil.BoolToStr( Dvpanel_tableattributes_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autowidth", StringUtil.BoolToStr( Dvpanel_tableattributes_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoheight", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tableattributes_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Iconposition", StringUtil.RTrim( Dvpanel_tableattributes_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoscroll", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoscroll));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "Servico";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A155Servico_Codigo), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( AV25Pgmname, ""));
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1530Servico_TipoHierarquia), "ZZZ9");
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("servico:[SendSecurityCheck value for]"+"Servico_Codigo:"+context.localUtil.Format( (decimal)(A155Servico_Codigo), "ZZZZZ9"));
         GXUtil.WriteLog("servico:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
         GXUtil.WriteLog("servico:[SendSecurityCheck value for]"+"Pgmname:"+StringUtil.RTrim( context.localUtil.Format( AV25Pgmname, "")));
         GXUtil.WriteLog("servico:[SendSecurityCheck value for]"+"Servico_TipoHierarquia:"+context.localUtil.Format( (decimal)(A1530Servico_TipoHierarquia), "ZZZ9"));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("servico.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7Servico_Codigo) + "," + UrlEncode("" +AV16ServicoGrupo_Codigo) ;
      }

      public override String GetPgmname( )
      {
         return "Servico" ;
      }

      public override String GetPgmdesc( )
      {
         return "Servico" ;
      }

      protected void InitializeNonKey0V130( )
      {
         A157ServicoGrupo_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A157ServicoGrupo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A157ServicoGrupo_Codigo), 6, 0)));
         A633Servico_UO = 0;
         n633Servico_UO = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A633Servico_UO", StringUtil.LTrim( StringUtil.Str( (decimal)(A633Servico_UO), 6, 0)));
         A631Servico_Vinculado = 0;
         n631Servico_Vinculado = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A631Servico_Vinculado", StringUtil.LTrim( StringUtil.Str( (decimal)(A631Servico_Vinculado), 6, 0)));
         n631Servico_Vinculado = ((0==A631Servico_Vinculado) ? true : false);
         A2047Servico_LinNegCod = 0;
         n2047Servico_LinNegCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2047Servico_LinNegCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2047Servico_LinNegCod), 6, 0)));
         n2047Servico_LinNegCod = ((0==A2047Servico_LinNegCod) ? true : false);
         AV19AuditingObject = new wwpbaseobjects.SdtAuditingObject(context);
         A2107Servico_Identificacao = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2107Servico_Identificacao", A2107Servico_Identificacao);
         A640Servico_Vinculados = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A640Servico_Vinculados", StringUtil.LTrim( StringUtil.Str( (decimal)(A640Servico_Vinculados), 4, 0)));
         A1551Servico_Responsavel = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1551Servico_Responsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(A1551Servico_Responsavel), 6, 0)));
         AV21Servico_Responsavel = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Servico_Responsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21Servico_Responsavel), 6, 0)));
         A608Servico_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A608Servico_Nome", A608Servico_Nome);
         A156Servico_Descricao = "";
         n156Servico_Descricao = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A156Servico_Descricao", A156Servico_Descricao);
         n156Servico_Descricao = (String.IsNullOrEmpty(StringUtil.RTrim( A156Servico_Descricao)) ? true : false);
         A605Servico_Sigla = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A605Servico_Sigla", A605Servico_Sigla);
         A158ServicoGrupo_Descricao = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A158ServicoGrupo_Descricao", A158ServicoGrupo_Descricao);
         A1077Servico_UORespExclusiva = false;
         n1077Servico_UORespExclusiva = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1077Servico_UORespExclusiva", A1077Servico_UORespExclusiva);
         n1077Servico_UORespExclusiva = ((false==A1077Servico_UORespExclusiva) ? true : false);
         A641Servico_VincDesc = "";
         n641Servico_VincDesc = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A641Servico_VincDesc", A641Servico_VincDesc);
         A1557Servico_VincSigla = "";
         n1557Servico_VincSigla = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1557Servico_VincSigla", A1557Servico_VincSigla);
         A889Servico_Terceriza = false;
         n889Servico_Terceriza = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A889Servico_Terceriza", A889Servico_Terceriza);
         n889Servico_Terceriza = ((false==A889Servico_Terceriza) ? true : false);
         A1061Servico_Tela = "";
         n1061Servico_Tela = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1061Servico_Tela", A1061Servico_Tela);
         n1061Servico_Tela = (String.IsNullOrEmpty(StringUtil.RTrim( A1061Servico_Tela)) ? true : false);
         A1072Servico_Atende = "";
         n1072Servico_Atende = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1072Servico_Atende", A1072Servico_Atende);
         n1072Servico_Atende = (String.IsNullOrEmpty(StringUtil.RTrim( A1072Servico_Atende)) ? true : false);
         A1429Servico_ObrigaValores = "";
         n1429Servico_ObrigaValores = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1429Servico_ObrigaValores", A1429Servico_ObrigaValores);
         n1429Servico_ObrigaValores = (String.IsNullOrEmpty(StringUtil.RTrim( A1429Servico_ObrigaValores)) ? true : false);
         A1436Servico_ObjetoControle = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1436Servico_ObjetoControle", A1436Servico_ObjetoControle);
         A1534Servico_PercTmp = 0;
         n1534Servico_PercTmp = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1534Servico_PercTmp", StringUtil.LTrim( StringUtil.Str( (decimal)(A1534Servico_PercTmp), 4, 0)));
         n1534Servico_PercTmp = ((0==A1534Servico_PercTmp) ? true : false);
         A1535Servico_PercPgm = 0;
         n1535Servico_PercPgm = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1535Servico_PercPgm", StringUtil.LTrim( StringUtil.Str( (decimal)(A1535Servico_PercPgm), 4, 0)));
         n1535Servico_PercPgm = ((0==A1535Servico_PercPgm) ? true : false);
         A1536Servico_PercCnc = 0;
         n1536Servico_PercCnc = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1536Servico_PercCnc", StringUtil.LTrim( StringUtil.Str( (decimal)(A1536Servico_PercCnc), 4, 0)));
         n1536Servico_PercCnc = ((0==A1536Servico_PercCnc) ? true : false);
         A1545Servico_Anterior = 0;
         n1545Servico_Anterior = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1545Servico_Anterior", StringUtil.LTrim( StringUtil.Str( (decimal)(A1545Servico_Anterior), 6, 0)));
         n1545Servico_Anterior = ((0==A1545Servico_Anterior) ? true : false);
         A1546Servico_Posterior = 0;
         n1546Servico_Posterior = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1546Servico_Posterior", StringUtil.LTrim( StringUtil.Str( (decimal)(A1546Servico_Posterior), 6, 0)));
         n1546Servico_Posterior = ((0==A1546Servico_Posterior) ? true : false);
         A2048Servico_LinNegDsc = "";
         n2048Servico_LinNegDsc = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2048Servico_LinNegDsc", A2048Servico_LinNegDsc);
         A2092Servico_IsOrigemReferencia = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2092Servico_IsOrigemReferencia", A2092Servico_IsOrigemReferencia);
         A1530Servico_TipoHierarquia = 1;
         n1530Servico_TipoHierarquia = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1530Servico_TipoHierarquia", StringUtil.LTrim( StringUtil.Str( (decimal)(A1530Servico_TipoHierarquia), 4, 0)));
         A632Servico_Ativo = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A632Servico_Ativo", A632Servico_Ativo);
         A1635Servico_IsPublico = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1635Servico_IsPublico", A1635Servico_IsPublico);
         A2131Servico_PausaSLA = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2131Servico_PausaSLA", A2131Servico_PausaSLA);
         Z608Servico_Nome = "";
         Z605Servico_Sigla = "";
         Z1077Servico_UORespExclusiva = false;
         Z889Servico_Terceriza = false;
         Z1061Servico_Tela = "";
         Z1072Servico_Atende = "";
         Z1429Servico_ObrigaValores = "";
         Z1436Servico_ObjetoControle = "";
         Z1530Servico_TipoHierarquia = 0;
         Z1534Servico_PercTmp = 0;
         Z1535Servico_PercPgm = 0;
         Z1536Servico_PercCnc = 0;
         Z1545Servico_Anterior = 0;
         Z1546Servico_Posterior = 0;
         Z632Servico_Ativo = false;
         Z1635Servico_IsPublico = false;
         Z2092Servico_IsOrigemReferencia = false;
         Z2131Servico_PausaSLA = false;
         Z157ServicoGrupo_Codigo = 0;
         Z633Servico_UO = 0;
         Z2047Servico_LinNegCod = 0;
         Z631Servico_Vinculado = 0;
      }

      protected void InitAll0V130( )
      {
         A155Servico_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
         InitializeNonKey0V130( ) ;
      }

      protected void StandaloneModalInsert( )
      {
         A2131Servico_PausaSLA = i2131Servico_PausaSLA;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2131Servico_PausaSLA", A2131Servico_PausaSLA);
         A1635Servico_IsPublico = i1635Servico_IsPublico;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1635Servico_IsPublico", A1635Servico_IsPublico);
         A632Servico_Ativo = i632Servico_Ativo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A632Servico_Ativo", A632Servico_Ativo);
         A1530Servico_TipoHierarquia = i1530Servico_TipoHierarquia;
         n1530Servico_TipoHierarquia = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1530Servico_TipoHierarquia", StringUtil.LTrim( StringUtil.Str( (decimal)(A1530Servico_TipoHierarquia), 4, 0)));
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20206216152529");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("servico.js", "?20206216152529");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockservico_uo_Internalname = "TEXTBLOCKSERVICO_UO";
         dynavServico_uo_Internalname = "vSERVICO_UO";
         imgSelectuo_Internalname = "SELECTUO";
         tblTablemergedservico_uo_Internalname = "TABLEMERGEDSERVICO_UO";
         lblTextblockservico_uorespexclusiva_Internalname = "TEXTBLOCKSERVICO_UORESPEXCLUSIVA";
         cmbServico_UORespExclusiva_Internalname = "SERVICO_UORESPEXCLUSIVA";
         lblTextblockservicogrupo_codigo_Internalname = "TEXTBLOCKSERVICOGRUPO_CODIGO";
         dynServicoGrupo_Codigo_Internalname = "SERVICOGRUPO_CODIGO";
         lblTextblockservico_nome_Internalname = "TEXTBLOCKSERVICO_NOME";
         edtServico_Nome_Internalname = "SERVICO_NOME";
         lblTextblockservico_sigla_Internalname = "TEXTBLOCKSERVICO_SIGLA";
         edtServico_Sigla_Internalname = "SERVICO_SIGLA";
         lblTextblockservico_vinculado_Internalname = "TEXTBLOCKSERVICO_VINCULADO";
         dynServico_Vinculado_Internalname = "SERVICO_VINCULADO";
         lblTextblockservico_descricao_Internalname = "TEXTBLOCKSERVICO_DESCRICAO";
         edtServico_Descricao_Internalname = "SERVICO_DESCRICAO";
         lblTextblockservico_anterior_Internalname = "TEXTBLOCKSERVICO_ANTERIOR";
         dynServico_Anterior_Internalname = "SERVICO_ANTERIOR";
         lblTextblockservico_posterior_Internalname = "TEXTBLOCKSERVICO_POSTERIOR";
         dynServico_Posterior_Internalname = "SERVICO_POSTERIOR";
         lblTextblockservico_terceriza_Internalname = "TEXTBLOCKSERVICO_TERCERIZA";
         cmbServico_Terceriza_Internalname = "SERVICO_TERCERIZA";
         lblTextblockservico_tela_Internalname = "TEXTBLOCKSERVICO_TELA";
         cmbServico_Tela_Internalname = "SERVICO_TELA";
         lblTextblockservico_atende_Internalname = "TEXTBLOCKSERVICO_ATENDE";
         cmbServico_Atende_Internalname = "SERVICO_ATENDE";
         lblTextblockservico_obrigavalores_Internalname = "TEXTBLOCKSERVICO_OBRIGAVALORES";
         cmbServico_ObrigaValores_Internalname = "SERVICO_OBRIGAVALORES";
         lblTextblockservico_objetocontrole_Internalname = "TEXTBLOCKSERVICO_OBJETOCONTROLE";
         cmbServico_ObjetoControle_Internalname = "SERVICO_OBJETOCONTROLE";
         lblTextblockservico_percpgm_Internalname = "TEXTBLOCKSERVICO_PERCPGM";
         edtServico_PercPgm_Internalname = "SERVICO_PERCPGM";
         lblServico_percpgm_righttext_Internalname = "SERVICO_PERCPGM_RIGHTTEXT";
         tblTablemergedservico_percpgm_Internalname = "TABLEMERGEDSERVICO_PERCPGM";
         lblTextblockservico_perccnc_Internalname = "TEXTBLOCKSERVICO_PERCCNC";
         edtServico_PercCnc_Internalname = "SERVICO_PERCCNC";
         lblServico_perccnc_righttext_Internalname = "SERVICO_PERCCNC_RIGHTTEXT";
         tblTablemergedservico_perccnc_Internalname = "TABLEMERGEDSERVICO_PERCCNC";
         lblTextblockservico_perctmp_Internalname = "TEXTBLOCKSERVICO_PERCTMP";
         cellTextblockservico_perctmp_cell_Internalname = "TEXTBLOCKSERVICO_PERCTMP_CELL";
         edtServico_PercTmp_Internalname = "SERVICO_PERCTMP";
         lblServico_perctmp_righttext_Internalname = "SERVICO_PERCTMP_RIGHTTEXT";
         tblTablemergedservico_perctmp_Internalname = "TABLEMERGEDSERVICO_PERCTMP";
         cellServico_perctmp_cell_Internalname = "SERVICO_PERCTMP_CELL";
         lblTextblockservico_ispublico_Internalname = "TEXTBLOCKSERVICO_ISPUBLICO";
         cmbServico_IsPublico_Internalname = "SERVICO_ISPUBLICO";
         lblTextblockresponsavel_codigo_Internalname = "TEXTBLOCKRESPONSAVEL_CODIGO";
         dynavResponsavel_codigo_Internalname = "vRESPONSAVEL_CODIGO";
         lblTextblockservico_linnegcod_Internalname = "TEXTBLOCKSERVICO_LINNEGCOD";
         dynServico_LinNegCod_Internalname = "SERVICO_LINNEGCOD";
         lblTextblockservico_isorigemreferencia_Internalname = "TEXTBLOCKSERVICO_ISORIGEMREFERENCIA";
         cmbServico_IsOrigemReferencia_Internalname = "SERVICO_ISORIGEMREFERENCIA";
         lblTextblockservico_pausasla_Internalname = "TEXTBLOCKSERVICO_PAUSASLA";
         cmbServico_PausaSLA_Internalname = "SERVICO_PAUSASLA";
         lblTextblockservico_ativo_Internalname = "TEXTBLOCKSERVICO_ATIVO";
         cmbServico_Ativo_Internalname = "SERVICO_ATIVO";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         Dvpanel_tableattributes_Internalname = "DVPANEL_TABLEATTRIBUTES";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtn_trn_delete_Internalname = "BTN_TRN_DELETE";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         edtavContratante_codigo_Internalname = "vCONTRATANTE_CODIGO";
         edtServico_Codigo_Internalname = "SERVICO_CODIGO";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Dvpanel_tableattributes_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Iconposition = "left";
         Dvpanel_tableattributes_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tableattributes_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Title = "Servi�o";
         Dvpanel_tableattributes_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tableattributes_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Servico";
         imgSelectuo_Visible = 1;
         dynavServico_uo_Jsonclick = "";
         dynavServico_uo.Enabled = 1;
         edtServico_PercPgm_Jsonclick = "";
         edtServico_PercPgm_Enabled = 1;
         edtServico_PercCnc_Jsonclick = "";
         edtServico_PercCnc_Enabled = 1;
         edtServico_PercTmp_Jsonclick = "";
         edtServico_PercTmp_Enabled = 1;
         edtServico_PercTmp_Visible = 1;
         cmbServico_Ativo_Jsonclick = "";
         cmbServico_Ativo.Enabled = 1;
         cmbServico_Ativo.Visible = 1;
         lblTextblockservico_ativo_Visible = 1;
         cmbServico_PausaSLA_Jsonclick = "";
         cmbServico_PausaSLA.Enabled = 1;
         cmbServico_IsOrigemReferencia_Jsonclick = "";
         cmbServico_IsOrigemReferencia.Enabled = 1;
         dynServico_LinNegCod_Jsonclick = "";
         dynServico_LinNegCod.Enabled = 1;
         dynavResponsavel_codigo_Jsonclick = "";
         dynavResponsavel_codigo.Enabled = 1;
         dynavResponsavel_codigo.Visible = 1;
         lblTextblockresponsavel_codigo_Visible = 1;
         cmbServico_IsPublico_Jsonclick = "";
         cmbServico_IsPublico.Enabled = 1;
         cellServico_perctmp_cell_Class = "";
         cellTextblockservico_perctmp_cell_Class = "";
         cmbServico_ObjetoControle_Jsonclick = "";
         cmbServico_ObjetoControle.Enabled = 1;
         cmbServico_ObrigaValores_Jsonclick = "";
         cmbServico_ObrigaValores.Enabled = 1;
         cmbServico_Atende_Jsonclick = "";
         cmbServico_Atende.Enabled = 1;
         cmbServico_Tela_Jsonclick = "";
         cmbServico_Tela.Enabled = 1;
         cmbServico_Terceriza_Jsonclick = "";
         cmbServico_Terceriza.Enabled = 1;
         dynServico_Posterior_Jsonclick = "";
         dynServico_Posterior.Enabled = 1;
         dynServico_Anterior_Jsonclick = "";
         dynServico_Anterior.Enabled = 1;
         edtServico_Descricao_Enabled = 1;
         dynServico_Vinculado_Jsonclick = "";
         dynServico_Vinculado.Enabled = 1;
         edtServico_Sigla_Jsonclick = "";
         edtServico_Sigla_Enabled = 1;
         edtServico_Nome_Jsonclick = "";
         edtServico_Nome_Enabled = 1;
         dynServicoGrupo_Codigo_Jsonclick = "";
         dynServicoGrupo_Codigo.Enabled = 1;
         cmbServico_UORespExclusiva_Jsonclick = "";
         cmbServico_UORespExclusiva.Enabled = 1;
         bttBtn_trn_cancel_Visible = 1;
         bttBtn_trn_delete_Enabled = 0;
         bttBtn_trn_delete_Visible = 1;
         bttBtn_trn_enter_Enabled = 1;
         bttBtn_trn_enter_Visible = 1;
         edtServico_Codigo_Jsonclick = "";
         edtServico_Codigo_Enabled = 0;
         edtServico_Codigo_Visible = 1;
         edtavContratante_codigo_Jsonclick = "";
         edtavContratante_codigo_Enabled = 0;
         edtavContratante_codigo_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         GXASERVICO_ANTERIOR_html0V130( AV7Servico_Codigo, A632Servico_Ativo) ;
         GXASERVICO_ANTERIOR_html0V130( AV7Servico_Codigo, A632Servico_Ativo) ;
         GXASERVICO_POSTERIOR_html0V130( AV7Servico_Codigo, A632Servico_Ativo) ;
         GXASERVICO_POSTERIOR_html0V130( AV7Servico_Codigo, A632Servico_Ativo) ;
         GXASERVICO_VINCULADO_html0V130( AV16ServicoGrupo_Codigo, AV7Servico_Codigo, A157ServicoGrupo_Codigo, A640Servico_Vinculados, A632Servico_Ativo) ;
         GXASERVICO_VINCULADO_html0V130( AV16ServicoGrupo_Codigo, AV7Servico_Codigo, A157ServicoGrupo_Codigo, A640Servico_Vinculados, A632Servico_Ativo) ;
         GXASERVICO_VINCULADO_html0V130( AV16ServicoGrupo_Codigo, AV7Servico_Codigo, A157ServicoGrupo_Codigo, A640Servico_Vinculados, A632Servico_Ativo) ;
         GXASERVICO_VINCULADO_html0V130( AV16ServicoGrupo_Codigo, AV7Servico_Codigo, A157ServicoGrupo_Codigo, A640Servico_Vinculados, A632Servico_Ativo) ;
         GXASERVICO_VINCULADO_html0V130( AV16ServicoGrupo_Codigo, AV7Servico_Codigo, A157ServicoGrupo_Codigo, A640Servico_Vinculados, A632Servico_Ativo) ;
         /* End function dynload_actions */
      }

      protected void GXDLASERVICOGRUPO_CODIGO0V1( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLASERVICOGRUPO_CODIGO_data0V1( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXASERVICOGRUPO_CODIGO_html0V1( )
      {
         int gxdynajaxvalue ;
         GXDLASERVICOGRUPO_CODIGO_data0V1( ) ;
         gxdynajaxindex = 1;
         dynServicoGrupo_Codigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynServicoGrupo_Codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLASERVICOGRUPO_CODIGO_data0V1( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         /* Using cursor T000V37 */
         pr_default.execute(35);
         while ( (pr_default.getStatus(35) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(T000V37_A157ServicoGrupo_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(T000V37_A158ServicoGrupo_Descricao[0]);
            pr_default.readNext(35);
         }
         pr_default.close(35);
      }

      protected void GXDLVvSERVICO_UO0V130( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvSERVICO_UO_data0V130( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvSERVICO_UO_html0V130( )
      {
         int gxdynajaxvalue ;
         GXDLVvSERVICO_UO_data0V130( ) ;
         gxdynajaxindex = 1;
         dynavServico_uo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavServico_uo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLVvSERVICO_UO_data0V130( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhuma)");
         /* Using cursor T000V38 */
         pr_default.execute(36);
         while ( (pr_default.getStatus(36) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(T000V38_A611UnidadeOrganizacional_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( T000V38_A612UnidadeOrganizacional_Nome[0]));
            pr_default.readNext(36);
         }
         pr_default.close(36);
      }

      protected void GXDLASERVICO_VINCULADO0V130( int AV16ServicoGrupo_Codigo ,
                                                  int AV7Servico_Codigo ,
                                                  int A157ServicoGrupo_Codigo ,
                                                  short A640Servico_Vinculados ,
                                                  bool A632Servico_Ativo )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLASERVICO_VINCULADO_data0V130( AV16ServicoGrupo_Codigo, AV7Servico_Codigo, A157ServicoGrupo_Codigo, A640Servico_Vinculados, A632Servico_Ativo) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXASERVICO_VINCULADO_html0V130( int AV16ServicoGrupo_Codigo ,
                                                     int AV7Servico_Codigo ,
                                                     int A157ServicoGrupo_Codigo ,
                                                     short A640Servico_Vinculados ,
                                                     bool A632Servico_Ativo )
      {
         int gxdynajaxvalue ;
         GXDLASERVICO_VINCULADO_data0V130( AV16ServicoGrupo_Codigo, AV7Servico_Codigo, A157ServicoGrupo_Codigo, A640Servico_Vinculados, A632Servico_Ativo) ;
         gxdynajaxindex = 1;
         dynServico_Vinculado.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynServico_Vinculado.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLASERVICO_VINCULADO_data0V130( int AV16ServicoGrupo_Codigo ,
                                                       int AV7Servico_Codigo ,
                                                       int A157ServicoGrupo_Codigo ,
                                                       short A640Servico_Vinculados ,
                                                       bool A632Servico_Ativo )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhum)");
         /* Using cursor T000V39 */
         pr_default.execute(37, new Object[] {AV16ServicoGrupo_Codigo, A157ServicoGrupo_Codigo, AV7Servico_Codigo, A640Servico_Vinculados, A632Servico_Ativo});
         while ( (pr_default.getStatus(37) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(T000V39_A155Servico_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( T000V39_A608Servico_Nome[0]));
            pr_default.readNext(37);
         }
         pr_default.close(37);
      }

      protected void GXDLASERVICO_ANTERIOR0V130( int AV7Servico_Codigo ,
                                                 bool A632Servico_Ativo )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLASERVICO_ANTERIOR_data0V130( AV7Servico_Codigo, A632Servico_Ativo) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXASERVICO_ANTERIOR_html0V130( int AV7Servico_Codigo ,
                                                    bool A632Servico_Ativo )
      {
         int gxdynajaxvalue ;
         GXDLASERVICO_ANTERIOR_data0V130( AV7Servico_Codigo, A632Servico_Ativo) ;
         gxdynajaxindex = 1;
         dynServico_Anterior.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynServico_Anterior.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLASERVICO_ANTERIOR_data0V130( int AV7Servico_Codigo ,
                                                      bool A632Servico_Ativo )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhum)");
         /* Using cursor T000V40 */
         pr_default.execute(38, new Object[] {AV7Servico_Codigo, A632Servico_Ativo});
         while ( (pr_default.getStatus(38) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(T000V40_A155Servico_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( T000V40_A605Servico_Sigla[0]));
            pr_default.readNext(38);
         }
         pr_default.close(38);
      }

      protected void GXDLASERVICO_POSTERIOR0V130( int AV7Servico_Codigo ,
                                                  bool A632Servico_Ativo )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLASERVICO_POSTERIOR_data0V130( AV7Servico_Codigo, A632Servico_Ativo) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXASERVICO_POSTERIOR_html0V130( int AV7Servico_Codigo ,
                                                     bool A632Servico_Ativo )
      {
         int gxdynajaxvalue ;
         GXDLASERVICO_POSTERIOR_data0V130( AV7Servico_Codigo, A632Servico_Ativo) ;
         gxdynajaxindex = 1;
         dynServico_Posterior.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynServico_Posterior.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLASERVICO_POSTERIOR_data0V130( int AV7Servico_Codigo ,
                                                       bool A632Servico_Ativo )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhum)");
         /* Using cursor T000V41 */
         pr_default.execute(39, new Object[] {AV7Servico_Codigo, A632Servico_Ativo});
         while ( (pr_default.getStatus(39) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(T000V41_A155Servico_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( T000V41_A605Servico_Sigla[0]));
            pr_default.readNext(39);
         }
         pr_default.close(39);
      }

      protected void GXDLVvRESPONSAVEL_CODIGO0V130( wwpbaseobjects.SdtWWPContext AV8WWPContext )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvRESPONSAVEL_CODIGO_data0V130( AV8WWPContext) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvRESPONSAVEL_CODIGO_html0V130( wwpbaseobjects.SdtWWPContext AV8WWPContext )
      {
         int gxdynajaxvalue ;
         GXDLVvRESPONSAVEL_CODIGO_data0V130( AV8WWPContext) ;
         gxdynajaxindex = 1;
         dynavResponsavel_codigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavResponsavel_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLVvRESPONSAVEL_CODIGO_data0V130( wwpbaseobjects.SdtWWPContext AV8WWPContext )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhum)");
         /* Using cursor T000V43 */
         pr_default.execute(40, new Object[] {AV8WWPContext.gxTpr_Areatrabalho_codigo});
         while ( (pr_default.getStatus(40) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(T000V43_A60ContratanteUsuario_UsuarioCod[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( T000V43_A62ContratanteUsuario_UsuarioPessoaNom[0]));
            pr_default.readNext(40);
         }
         pr_default.close(40);
      }

      protected void GXDLASERVICO_LINNEGCOD0V130( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLASERVICO_LINNEGCOD_data0V130( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXASERVICO_LINNEGCOD_html0V130( )
      {
         int gxdynajaxvalue ;
         GXDLASERVICO_LINNEGCOD_data0V130( ) ;
         gxdynajaxindex = 1;
         dynServico_LinNegCod.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynServico_LinNegCod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLASERVICO_LINNEGCOD_data0V130( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhuma)");
         /* Using cursor T000V44 */
         pr_default.execute(41);
         while ( (pr_default.getStatus(41) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(T000V44_A2047Servico_LinNegCod[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(T000V44_A2037LinhaNegocio_Identificador[0]);
            pr_default.readNext(41);
         }
         pr_default.close(41);
      }

      protected void GX7ASASERVICO_RESPONSAVEL0V130( int A155Servico_Codigo )
      {
         GXt_int1 = A1551Servico_Responsavel;
         new prc_getservico_responsavel(context ).execute( ref  A155Servico_Codigo, out  GXt_int1) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
         A1551Servico_Responsavel = GXt_int1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1551Servico_Responsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(A1551Servico_Responsavel), 6, 0)));
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A1551Servico_Responsavel), 6, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      protected void GX8ASASERVICO_VINCULADOS0V130( int A155Servico_Codigo )
      {
         GXt_int2 = A640Servico_Vinculados;
         new prc_qtdeservicosvinculados(context ).execute(  A155Servico_Codigo, out  GXt_int2) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
         A640Servico_Vinculados = GXt_int2;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A640Servico_Vinculados", StringUtil.LTrim( StringUtil.Str( (decimal)(A640Servico_Vinculados), 4, 0)));
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A640Servico_Vinculados), 4, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      protected void XC_46_0V130( wwpbaseobjects.SdtAuditingObject AV19AuditingObject ,
                                  int A155Servico_Codigo ,
                                  String Gx_mode )
      {
         new loadauditservico(context ).execute(  "Y", ref  AV19AuditingObject,  A155Servico_Codigo,  Gx_mode) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.EncodeString( AV19AuditingObject.ToXml(false, true, "AuditingObject", "GxEv3Up14_MeetrikaVs3")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      protected void XC_47_0V130( wwpbaseobjects.SdtAuditingObject AV19AuditingObject ,
                                  int A155Servico_Codigo ,
                                  String Gx_mode )
      {
         new loadauditservico(context ).execute(  "Y", ref  AV19AuditingObject,  A155Servico_Codigo,  Gx_mode) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.EncodeString( AV19AuditingObject.ToXml(false, true, "AuditingObject", "GxEv3Up14_MeetrikaVs3")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      protected void XC_48_0V130( String Gx_mode ,
                                  wwpbaseobjects.SdtAuditingObject AV19AuditingObject ,
                                  int A155Servico_Codigo )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            new loadauditservico(context ).execute(  "N", ref  AV19AuditingObject,  A155Servico_Codigo,  Gx_mode) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.EncodeString( AV19AuditingObject.ToXml(false, true, "AuditingObject", "GxEv3Up14_MeetrikaVs3")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      protected void XC_49_0V130( String Gx_mode ,
                                  wwpbaseobjects.SdtAuditingObject AV19AuditingObject ,
                                  int A155Servico_Codigo )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )  )
         {
            new loadauditservico(context ).execute(  "N", ref  AV19AuditingObject,  A155Servico_Codigo,  Gx_mode) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.EncodeString( AV19AuditingObject.ToXml(false, true, "AuditingObject", "GxEv3Up14_MeetrikaVs3")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      protected void XC_50_0V130( )
      {
         new prc_updservico_responsavel(context ).execute(  ((StringUtil.StrCmp(Gx_mode, "DLT")==0) ? "DLT" : "UPD"),  AV20Contratante_Codigo,  AV7Servico_Codigo,  AV22Responsavel_Codigo) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      public void Valid_Servico_codigo( int GX_Parm1 ,
                                        int GX_Parm2 ,
                                        short GX_Parm3 )
      {
         A155Servico_Codigo = GX_Parm1;
         A1551Servico_Responsavel = GX_Parm2;
         A640Servico_Vinculados = GX_Parm3;
         GXt_int1 = A1551Servico_Responsavel;
         new prc_getservico_responsavel(context ).execute( ref  A155Servico_Codigo, out  GXt_int1) ;
         A1551Servico_Responsavel = GXt_int1;
         GXt_int2 = A640Servico_Vinculados;
         new prc_qtdeservicosvinculados(context ).execute(  A155Servico_Codigo, out  GXt_int2) ;
         A640Servico_Vinculados = GXt_int2;
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
         }
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A1551Servico_Responsavel), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A640Servico_Vinculados), 4, 0, ".", "")));
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Servicogrupo_codigo( GXCombobox dynGX_Parm1 ,
                                             int GX_Parm2 ,
                                             int GX_Parm3 ,
                                             short GX_Parm4 ,
                                             GXCombobox cmbGX_Parm5 ,
                                             String GX_Parm6 ,
                                             GXCombobox dynGX_Parm7 )
      {
         dynServicoGrupo_Codigo = dynGX_Parm1;
         A157ServicoGrupo_Codigo = (int)(NumberUtil.Val( dynServicoGrupo_Codigo.CurrentValue, "."));
         AV16ServicoGrupo_Codigo = GX_Parm2;
         AV7Servico_Codigo = GX_Parm3;
         A640Servico_Vinculados = GX_Parm4;
         cmbServico_Ativo = cmbGX_Parm5;
         A632Servico_Ativo = StringUtil.StrToBool( cmbServico_Ativo.CurrentValue);
         A158ServicoGrupo_Descricao = GX_Parm6;
         dynServico_Vinculado = dynGX_Parm7;
         A631Servico_Vinculado = (int)(NumberUtil.Val( dynServico_Vinculado.CurrentValue, "."));
         n631Servico_Vinculado = false;
         /* Using cursor T000V45 */
         pr_default.execute(42, new Object[] {A157ServicoGrupo_Codigo});
         if ( (pr_default.getStatus(42) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Servico Grupo'.", "ForeignKeyNotFound", 1, "SERVICOGRUPO_CODIGO");
            AnyError = 1;
            GX_FocusControl = dynServicoGrupo_Codigo_Internalname;
         }
         A158ServicoGrupo_Descricao = T000V45_A158ServicoGrupo_Descricao[0];
         pr_default.close(42);
         GXASERVICO_VINCULADO_html0V130( AV16ServicoGrupo_Codigo, AV7Servico_Codigo, A157ServicoGrupo_Codigo, A640Servico_Vinculados, A632Servico_Ativo) ;
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A158ServicoGrupo_Descricao = "";
         }
         dynServico_Vinculado.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A631Servico_Vinculado), 6, 0));
         isValidOutput.Add(A158ServicoGrupo_Descricao);
         if ( dynServico_Vinculado.ItemCount > 0 )
         {
            A631Servico_Vinculado = (int)(NumberUtil.Val( dynServico_Vinculado.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A631Servico_Vinculado), 6, 0))), "."));
            n631Servico_Vinculado = false;
         }
         dynServico_Vinculado.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A631Servico_Vinculado), 6, 0));
         isValidOutput.Add(dynServico_Vinculado);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Servico_vinculado( GXCombobox dynGX_Parm1 ,
                                           String GX_Parm2 ,
                                           String GX_Parm3 )
      {
         dynServico_Vinculado = dynGX_Parm1;
         A631Servico_Vinculado = (int)(NumberUtil.Val( dynServico_Vinculado.CurrentValue, "."));
         n631Servico_Vinculado = false;
         A641Servico_VincDesc = GX_Parm2;
         n641Servico_VincDesc = false;
         A1557Servico_VincSigla = GX_Parm3;
         n1557Servico_VincSigla = false;
         /* Using cursor T000V46 */
         pr_default.execute(43, new Object[] {n631Servico_Vinculado, A631Servico_Vinculado});
         if ( (pr_default.getStatus(43) == 101) )
         {
            if ( ! ( (0==A631Servico_Vinculado) ) )
            {
               GX_msglist.addItem("N�o existe 'Servico_Servico Vinculado'.", "ForeignKeyNotFound", 1, "SERVICO_VINCULADO");
               AnyError = 1;
               GX_FocusControl = dynServico_Vinculado_Internalname;
            }
         }
         A641Servico_VincDesc = T000V46_A641Servico_VincDesc[0];
         n641Servico_VincDesc = T000V46_n641Servico_VincDesc[0];
         A1557Servico_VincSigla = T000V46_A1557Servico_VincSigla[0];
         n1557Servico_VincSigla = T000V46_n1557Servico_VincSigla[0];
         pr_default.close(43);
         edtServico_PercTmp_Visible = (T000V3_n631Servico_Vinculado[0] ? 1 : 0);
         if ( ! ( T000V3_n631Servico_Vinculado[0] ) )
         {
            cellServico_perctmp_cell_Class = "Invisible";
         }
         else
         {
            if ( T000V3_n631Servico_Vinculado[0] )
            {
               cellServico_perctmp_cell_Class = "DataContentCell";
            }
         }
         if ( ! ( T000V3_n631Servico_Vinculado[0] ) )
         {
            cellTextblockservico_perctmp_cell_Class = "Invisible";
         }
         else
         {
            if ( T000V3_n631Servico_Vinculado[0] )
            {
               cellTextblockservico_perctmp_cell_Class = "DataDescriptionCell";
            }
         }
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A641Servico_VincDesc = "";
            n641Servico_VincDesc = false;
            A1557Servico_VincSigla = "";
            n1557Servico_VincSigla = false;
         }
         isValidOutput.Add(A641Servico_VincDesc);
         isValidOutput.Add(StringUtil.RTrim( A1557Servico_VincSigla));
         isValidOutput.Add(edtServico_PercTmp_Visible);
         isValidOutput.Add(cellServico_perctmp_cell_Class);
         isValidOutput.Add(cellTextblockservico_perctmp_cell_Class);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Servico_ispublico( GXCombobox cmbGX_Parm1 ,
                                           int GX_Parm2 )
      {
         cmbServico_IsPublico = cmbGX_Parm1;
         A1635Servico_IsPublico = StringUtil.StrToBool( cmbServico_IsPublico.CurrentValue);
         AV21Servico_Responsavel = GX_Parm2;
         dynavResponsavel_codigo.Visible = (A1635Servico_IsPublico ? 1 : 0);
         lblTextblockresponsavel_codigo_Visible = (A1635Servico_IsPublico ? 1 : 0);
         if ( ! A1635Servico_IsPublico )
         {
            AV21Servico_Responsavel = 0;
         }
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
         }
         isValidOutput.Add(dynavResponsavel_codigo.Visible);
         isValidOutput.Add(lblTextblockresponsavel_codigo_Visible);
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(AV21Servico_Responsavel), 6, 0, ".", "")));
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Servico_linnegcod( GXCombobox dynGX_Parm1 ,
                                           String GX_Parm2 )
      {
         dynServico_LinNegCod = dynGX_Parm1;
         A2047Servico_LinNegCod = (int)(NumberUtil.Val( dynServico_LinNegCod.CurrentValue, "."));
         n2047Servico_LinNegCod = false;
         A2048Servico_LinNegDsc = GX_Parm2;
         n2048Servico_LinNegDsc = false;
         /* Using cursor T000V47 */
         pr_default.execute(44, new Object[] {n2047Servico_LinNegCod, A2047Servico_LinNegCod});
         if ( (pr_default.getStatus(44) == 101) )
         {
            if ( ! ( (0==A2047Servico_LinNegCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Servico_Linha Negocio'.", "ForeignKeyNotFound", 1, "SERVICO_LINNEGCOD");
               AnyError = 1;
               GX_FocusControl = dynServico_LinNegCod_Internalname;
            }
         }
         A2048Servico_LinNegDsc = T000V47_A2048Servico_LinNegDsc[0];
         n2048Servico_LinNegDsc = T000V47_n2048Servico_LinNegDsc[0];
         pr_default.close(44);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A2048Servico_LinNegDsc = "";
            n2048Servico_LinNegDsc = false;
         }
         isValidOutput.Add(A2048Servico_LinNegDsc);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Servico_ativo( int GX_Parm1 ,
                                       int GX_Parm2 ,
                                       GXCombobox dynGX_Parm3 ,
                                       short GX_Parm4 ,
                                       GXCombobox cmbGX_Parm5 ,
                                       GXCombobox dynGX_Parm6 ,
                                       GXCombobox dynGX_Parm7 ,
                                       GXCombobox dynGX_Parm8 )
      {
         AV16ServicoGrupo_Codigo = GX_Parm1;
         AV7Servico_Codigo = GX_Parm2;
         dynServicoGrupo_Codigo = dynGX_Parm3;
         A157ServicoGrupo_Codigo = (int)(NumberUtil.Val( dynServicoGrupo_Codigo.CurrentValue, "."));
         A640Servico_Vinculados = GX_Parm4;
         cmbServico_Ativo = cmbGX_Parm5;
         A632Servico_Ativo = StringUtil.StrToBool( cmbServico_Ativo.CurrentValue);
         dynServico_Vinculado = dynGX_Parm6;
         A631Servico_Vinculado = (int)(NumberUtil.Val( dynServico_Vinculado.CurrentValue, "."));
         n631Servico_Vinculado = false;
         dynServico_Anterior = dynGX_Parm7;
         A1545Servico_Anterior = (int)(NumberUtil.Val( dynServico_Anterior.CurrentValue, "."));
         n1545Servico_Anterior = false;
         dynServico_Posterior = dynGX_Parm8;
         A1546Servico_Posterior = (int)(NumberUtil.Val( dynServico_Posterior.CurrentValue, "."));
         n1546Servico_Posterior = false;
         GXASERVICO_VINCULADO_html0V130( AV16ServicoGrupo_Codigo, AV7Servico_Codigo, A157ServicoGrupo_Codigo, A640Servico_Vinculados, A632Servico_Ativo) ;
         GXASERVICO_ANTERIOR_html0V130( AV7Servico_Codigo, A632Servico_Ativo) ;
         GXASERVICO_POSTERIOR_html0V130( AV7Servico_Codigo, A632Servico_Ativo) ;
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
         }
         dynServico_Vinculado.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A631Servico_Vinculado), 6, 0));
         dynServico_Anterior.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1545Servico_Anterior), 6, 0));
         dynServico_Posterior.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1546Servico_Posterior), 6, 0));
         if ( dynServico_Vinculado.ItemCount > 0 )
         {
            A631Servico_Vinculado = (int)(NumberUtil.Val( dynServico_Vinculado.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A631Servico_Vinculado), 6, 0))), "."));
            n631Servico_Vinculado = false;
         }
         dynServico_Vinculado.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A631Servico_Vinculado), 6, 0));
         isValidOutput.Add(dynServico_Vinculado);
         if ( dynServico_Anterior.ItemCount > 0 )
         {
            A1545Servico_Anterior = (int)(NumberUtil.Val( dynServico_Anterior.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1545Servico_Anterior), 6, 0))), "."));
            n1545Servico_Anterior = false;
         }
         dynServico_Anterior.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1545Servico_Anterior), 6, 0));
         isValidOutput.Add(dynServico_Anterior);
         if ( dynServico_Posterior.ItemCount > 0 )
         {
            A1546Servico_Posterior = (int)(NumberUtil.Val( dynServico_Posterior.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1546Servico_Posterior), 6, 0))), "."));
            n1546Servico_Posterior = false;
         }
         dynServico_Posterior.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1546Servico_Posterior), 6, 0));
         isValidOutput.Add(dynServico_Posterior);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV7Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV16ServicoGrupo_Codigo',fld:'vSERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E130V2',iparms:[{av:'AV19AuditingObject',fld:'vAUDITINGOBJECT',pic:'',nv:null},{av:'AV25Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV9TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null}],oparms:[]}");
         setEventMetadata("'DOSELECTUO'","{handler:'E110V130',iparms:[{av:'AV17Servico_UO',fld:'vSERVICO_UO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV17Servico_UO',fld:'vSERVICO_UO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("SERVICO_ISPUBLICO.CLICK","{handler:'E140V2',iparms:[{av:'A1635Servico_IsPublico',fld:'SERVICO_ISPUBLICO',pic:'',nv:false},{av:'AV7Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV22Responsavel_Codigo',fld:'vRESPONSAVEL_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(42);
         pr_default.close(18);
         pr_default.close(44);
         pr_default.close(20);
         pr_default.close(43);
         pr_default.close(19);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         Z608Servico_Nome = "";
         Z605Servico_Sigla = "";
         Z1061Servico_Tela = "";
         Z1072Servico_Atende = "";
         Z1429Servico_ObrigaValores = "";
         Z1436Servico_ObjetoControle = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GXKey = "";
         T000V8_A157ServicoGrupo_Codigo = new int[1] ;
         T000V8_A158ServicoGrupo_Descricao = new String[] {""} ;
         A1061Servico_Tela = "";
         A1072Servico_Atende = "";
         A1429Servico_ObrigaValores = "";
         A1436Servico_ObjetoControle = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtn_trn_delete_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         lblTextblockservico_uo_Jsonclick = "";
         lblTextblockservico_uorespexclusiva_Jsonclick = "";
         lblTextblockservicogrupo_codigo_Jsonclick = "";
         lblTextblockservico_nome_Jsonclick = "";
         A608Servico_Nome = "";
         lblTextblockservico_sigla_Jsonclick = "";
         A605Servico_Sigla = "";
         lblTextblockservico_vinculado_Jsonclick = "";
         lblTextblockservico_descricao_Jsonclick = "";
         A156Servico_Descricao = "";
         lblTextblockservico_anterior_Jsonclick = "";
         lblTextblockservico_posterior_Jsonclick = "";
         lblTextblockservico_terceriza_Jsonclick = "";
         lblTextblockservico_tela_Jsonclick = "";
         lblTextblockservico_atende_Jsonclick = "";
         lblTextblockservico_obrigavalores_Jsonclick = "";
         lblTextblockservico_objetocontrole_Jsonclick = "";
         lblTextblockservico_percpgm_Jsonclick = "";
         lblTextblockservico_perccnc_Jsonclick = "";
         lblTextblockservico_perctmp_Jsonclick = "";
         lblTextblockservico_ispublico_Jsonclick = "";
         lblTextblockresponsavel_codigo_Jsonclick = "";
         lblTextblockservico_linnegcod_Jsonclick = "";
         lblTextblockservico_isorigemreferencia_Jsonclick = "";
         lblTextblockservico_pausasla_Jsonclick = "";
         lblTextblockservico_ativo_Jsonclick = "";
         lblServico_perctmp_righttext_Jsonclick = "";
         lblServico_perccnc_righttext_Jsonclick = "";
         lblServico_percpgm_righttext_Jsonclick = "";
         imgSelectuo_Jsonclick = "";
         A2107Servico_Identificacao = "";
         AV19AuditingObject = new wwpbaseobjects.SdtAuditingObject(context);
         A158ServicoGrupo_Descricao = "";
         A2048Servico_LinNegDsc = "";
         A641Servico_VincDesc = "";
         A1557Servico_VincSigla = "";
         AV25Pgmname = "";
         Dvpanel_tableattributes_Height = "";
         Dvpanel_tableattributes_Class = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode130 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         AV12TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         Z156Servico_Descricao = "";
         Z158ServicoGrupo_Descricao = "";
         Z641Servico_VincDesc = "";
         Z1557Servico_VincSigla = "";
         Z2048Servico_LinNegDsc = "";
         T000V6_A2048Servico_LinNegDsc = new String[] {""} ;
         T000V6_n2048Servico_LinNegDsc = new bool[] {false} ;
         T000V7_A641Servico_VincDesc = new String[] {""} ;
         T000V7_n641Servico_VincDesc = new bool[] {false} ;
         T000V7_A1557Servico_VincSigla = new String[] {""} ;
         T000V7_n1557Servico_VincSigla = new bool[] {false} ;
         T000V3_n631Servico_Vinculado = new bool[] {false} ;
         T000V4_A158ServicoGrupo_Descricao = new String[] {""} ;
         T000V9_A155Servico_Codigo = new int[1] ;
         T000V9_A608Servico_Nome = new String[] {""} ;
         T000V9_A156Servico_Descricao = new String[] {""} ;
         T000V9_n156Servico_Descricao = new bool[] {false} ;
         T000V9_A605Servico_Sigla = new String[] {""} ;
         T000V9_A158ServicoGrupo_Descricao = new String[] {""} ;
         T000V9_A1077Servico_UORespExclusiva = new bool[] {false} ;
         T000V9_n1077Servico_UORespExclusiva = new bool[] {false} ;
         T000V9_A641Servico_VincDesc = new String[] {""} ;
         T000V9_n641Servico_VincDesc = new bool[] {false} ;
         T000V9_A1557Servico_VincSigla = new String[] {""} ;
         T000V9_n1557Servico_VincSigla = new bool[] {false} ;
         T000V9_A889Servico_Terceriza = new bool[] {false} ;
         T000V9_n889Servico_Terceriza = new bool[] {false} ;
         T000V9_A1061Servico_Tela = new String[] {""} ;
         T000V9_n1061Servico_Tela = new bool[] {false} ;
         T000V9_A1072Servico_Atende = new String[] {""} ;
         T000V9_n1072Servico_Atende = new bool[] {false} ;
         T000V9_A1429Servico_ObrigaValores = new String[] {""} ;
         T000V9_n1429Servico_ObrigaValores = new bool[] {false} ;
         T000V9_A1436Servico_ObjetoControle = new String[] {""} ;
         T000V9_A1530Servico_TipoHierarquia = new short[1] ;
         T000V9_n1530Servico_TipoHierarquia = new bool[] {false} ;
         T000V9_A1534Servico_PercTmp = new short[1] ;
         T000V9_n1534Servico_PercTmp = new bool[] {false} ;
         T000V9_A1535Servico_PercPgm = new short[1] ;
         T000V9_n1535Servico_PercPgm = new bool[] {false} ;
         T000V9_A1536Servico_PercCnc = new short[1] ;
         T000V9_n1536Servico_PercCnc = new bool[] {false} ;
         T000V9_A1545Servico_Anterior = new int[1] ;
         T000V9_n1545Servico_Anterior = new bool[] {false} ;
         T000V9_A1546Servico_Posterior = new int[1] ;
         T000V9_n1546Servico_Posterior = new bool[] {false} ;
         T000V9_A632Servico_Ativo = new bool[] {false} ;
         T000V9_A1635Servico_IsPublico = new bool[] {false} ;
         T000V9_A2048Servico_LinNegDsc = new String[] {""} ;
         T000V9_n2048Servico_LinNegDsc = new bool[] {false} ;
         T000V9_A2092Servico_IsOrigemReferencia = new bool[] {false} ;
         T000V9_A2131Servico_PausaSLA = new bool[] {false} ;
         T000V9_A157ServicoGrupo_Codigo = new int[1] ;
         T000V9_A633Servico_UO = new int[1] ;
         T000V9_n633Servico_UO = new bool[] {false} ;
         T000V9_A2047Servico_LinNegCod = new int[1] ;
         T000V9_n2047Servico_LinNegCod = new bool[] {false} ;
         T000V9_A631Servico_Vinculado = new int[1] ;
         T000V9_n631Servico_Vinculado = new bool[] {false} ;
         T000V5_A633Servico_UO = new int[1] ;
         T000V5_n633Servico_UO = new bool[] {false} ;
         T000V10_A158ServicoGrupo_Descricao = new String[] {""} ;
         T000V11_A641Servico_VincDesc = new String[] {""} ;
         T000V11_n641Servico_VincDesc = new bool[] {false} ;
         T000V11_A1557Servico_VincSigla = new String[] {""} ;
         T000V11_n1557Servico_VincSigla = new bool[] {false} ;
         T000V12_A2048Servico_LinNegDsc = new String[] {""} ;
         T000V12_n2048Servico_LinNegDsc = new bool[] {false} ;
         T000V13_A633Servico_UO = new int[1] ;
         T000V13_n633Servico_UO = new bool[] {false} ;
         T000V14_A155Servico_Codigo = new int[1] ;
         T000V3_A155Servico_Codigo = new int[1] ;
         T000V3_A608Servico_Nome = new String[] {""} ;
         T000V3_A156Servico_Descricao = new String[] {""} ;
         T000V3_n156Servico_Descricao = new bool[] {false} ;
         T000V3_A605Servico_Sigla = new String[] {""} ;
         T000V3_A1077Servico_UORespExclusiva = new bool[] {false} ;
         T000V3_n1077Servico_UORespExclusiva = new bool[] {false} ;
         T000V3_A889Servico_Terceriza = new bool[] {false} ;
         T000V3_n889Servico_Terceriza = new bool[] {false} ;
         T000V3_A1061Servico_Tela = new String[] {""} ;
         T000V3_n1061Servico_Tela = new bool[] {false} ;
         T000V3_A1072Servico_Atende = new String[] {""} ;
         T000V3_n1072Servico_Atende = new bool[] {false} ;
         T000V3_A1429Servico_ObrigaValores = new String[] {""} ;
         T000V3_n1429Servico_ObrigaValores = new bool[] {false} ;
         T000V3_A1436Servico_ObjetoControle = new String[] {""} ;
         T000V3_A1530Servico_TipoHierarquia = new short[1] ;
         T000V3_n1530Servico_TipoHierarquia = new bool[] {false} ;
         T000V3_A1534Servico_PercTmp = new short[1] ;
         T000V3_n1534Servico_PercTmp = new bool[] {false} ;
         T000V3_A1535Servico_PercPgm = new short[1] ;
         T000V3_n1535Servico_PercPgm = new bool[] {false} ;
         T000V3_A1536Servico_PercCnc = new short[1] ;
         T000V3_n1536Servico_PercCnc = new bool[] {false} ;
         T000V3_A1545Servico_Anterior = new int[1] ;
         T000V3_n1545Servico_Anterior = new bool[] {false} ;
         T000V3_A1546Servico_Posterior = new int[1] ;
         T000V3_n1546Servico_Posterior = new bool[] {false} ;
         T000V3_A632Servico_Ativo = new bool[] {false} ;
         T000V3_A1635Servico_IsPublico = new bool[] {false} ;
         T000V3_A2092Servico_IsOrigemReferencia = new bool[] {false} ;
         T000V3_A2131Servico_PausaSLA = new bool[] {false} ;
         T000V3_A157ServicoGrupo_Codigo = new int[1] ;
         T000V3_A633Servico_UO = new int[1] ;
         T000V3_n633Servico_UO = new bool[] {false} ;
         T000V3_A2047Servico_LinNegCod = new int[1] ;
         T000V3_n2047Servico_LinNegCod = new bool[] {false} ;
         T000V3_A631Servico_Vinculado = new int[1] ;
         T000V15_A155Servico_Codigo = new int[1] ;
         T000V16_A155Servico_Codigo = new int[1] ;
         T000V2_A155Servico_Codigo = new int[1] ;
         T000V2_A608Servico_Nome = new String[] {""} ;
         T000V2_A156Servico_Descricao = new String[] {""} ;
         T000V2_n156Servico_Descricao = new bool[] {false} ;
         T000V2_A605Servico_Sigla = new String[] {""} ;
         T000V2_A1077Servico_UORespExclusiva = new bool[] {false} ;
         T000V2_n1077Servico_UORespExclusiva = new bool[] {false} ;
         T000V2_A889Servico_Terceriza = new bool[] {false} ;
         T000V2_n889Servico_Terceriza = new bool[] {false} ;
         T000V2_A1061Servico_Tela = new String[] {""} ;
         T000V2_n1061Servico_Tela = new bool[] {false} ;
         T000V2_A1072Servico_Atende = new String[] {""} ;
         T000V2_n1072Servico_Atende = new bool[] {false} ;
         T000V2_A1429Servico_ObrigaValores = new String[] {""} ;
         T000V2_n1429Servico_ObrigaValores = new bool[] {false} ;
         T000V2_A1436Servico_ObjetoControle = new String[] {""} ;
         T000V2_A1530Servico_TipoHierarquia = new short[1] ;
         T000V2_n1530Servico_TipoHierarquia = new bool[] {false} ;
         T000V2_A1534Servico_PercTmp = new short[1] ;
         T000V2_n1534Servico_PercTmp = new bool[] {false} ;
         T000V2_A1535Servico_PercPgm = new short[1] ;
         T000V2_n1535Servico_PercPgm = new bool[] {false} ;
         T000V2_A1536Servico_PercCnc = new short[1] ;
         T000V2_n1536Servico_PercCnc = new bool[] {false} ;
         T000V2_A1545Servico_Anterior = new int[1] ;
         T000V2_n1545Servico_Anterior = new bool[] {false} ;
         T000V2_A1546Servico_Posterior = new int[1] ;
         T000V2_n1546Servico_Posterior = new bool[] {false} ;
         T000V2_A632Servico_Ativo = new bool[] {false} ;
         T000V2_A1635Servico_IsPublico = new bool[] {false} ;
         T000V2_A2092Servico_IsOrigemReferencia = new bool[] {false} ;
         T000V2_A2131Servico_PausaSLA = new bool[] {false} ;
         T000V2_A157ServicoGrupo_Codigo = new int[1] ;
         T000V2_A633Servico_UO = new int[1] ;
         T000V2_n633Servico_UO = new bool[] {false} ;
         T000V2_A2047Servico_LinNegCod = new int[1] ;
         T000V2_n2047Servico_LinNegCod = new bool[] {false} ;
         T000V2_A631Servico_Vinculado = new int[1] ;
         T000V2_n631Servico_Vinculado = new bool[] {false} ;
         T000V17_A155Servico_Codigo = new int[1] ;
         T000V20_A158ServicoGrupo_Descricao = new String[] {""} ;
         T000V21_A641Servico_VincDesc = new String[] {""} ;
         T000V21_n641Servico_VincDesc = new bool[] {false} ;
         T000V21_A1557Servico_VincSigla = new String[] {""} ;
         T000V21_n1557Servico_VincSigla = new bool[] {false} ;
         T000V22_A2048Servico_LinNegDsc = new String[] {""} ;
         T000V22_n2048Servico_LinNegDsc = new bool[] {false} ;
         T000V23_A631Servico_Vinculado = new int[1] ;
         T000V23_n631Servico_Vinculado = new bool[] {false} ;
         T000V24_A1528ServicoFluxo_Codigo = new int[1] ;
         T000V25_A1528ServicoFluxo_Codigo = new int[1] ;
         T000V26_A1440ServicoPrioridade_Codigo = new int[1] ;
         T000V27_A160ContratoServicos_Codigo = new int[1] ;
         T000V27_A1067ContratoServicosSistemas_ServicoCod = new int[1] ;
         T000V27_A1063ContratoServicosSistemas_SistemaCod = new int[1] ;
         T000V28_A5AreaTrabalho_Codigo = new int[1] ;
         T000V29_A828UsuarioServicos_UsuarioCod = new int[1] ;
         T000V29_A829UsuarioServicos_ServicoCod = new int[1] ;
         T000V30_A456ContagemResultado_Codigo = new int[1] ;
         T000V31_A439Solicitacoes_Codigo = new int[1] ;
         T000V32_A319ServicoAns_Codigo = new int[1] ;
         T000V33_A160ContratoServicos_Codigo = new int[1] ;
         T000V34_A155Servico_Codigo = new int[1] ;
         T000V34_A1839Check_Codigo = new int[1] ;
         T000V35_A155Servico_Codigo = new int[1] ;
         T000V35_A1766ServicoArtefato_ArtefatoCod = new int[1] ;
         T000V36_A155Servico_Codigo = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         T000V37_A157ServicoGrupo_Codigo = new int[1] ;
         T000V37_A158ServicoGrupo_Descricao = new String[] {""} ;
         T000V38_A611UnidadeOrganizacional_Codigo = new int[1] ;
         T000V38_A612UnidadeOrganizacional_Nome = new String[] {""} ;
         T000V39_A155Servico_Codigo = new int[1] ;
         T000V39_A608Servico_Nome = new String[] {""} ;
         T000V40_A155Servico_Codigo = new int[1] ;
         T000V40_A605Servico_Sigla = new String[] {""} ;
         T000V41_A155Servico_Codigo = new int[1] ;
         T000V41_A605Servico_Sigla = new String[] {""} ;
         T000V43_A61ContratanteUsuario_UsuarioPessoaCod = new int[1] ;
         T000V43_n61ContratanteUsuario_UsuarioPessoaCod = new bool[] {false} ;
         T000V43_A63ContratanteUsuario_ContratanteCod = new int[1] ;
         T000V43_A60ContratanteUsuario_UsuarioCod = new int[1] ;
         T000V43_A62ContratanteUsuario_UsuarioPessoaNom = new String[] {""} ;
         T000V43_n62ContratanteUsuario_UsuarioPessoaNom = new bool[] {false} ;
         T000V43_A1020ContratanteUsuario_AreaTrabalhoCod = new int[1] ;
         T000V43_n1020ContratanteUsuario_AreaTrabalhoCod = new bool[] {false} ;
         T000V44_A2047Servico_LinNegCod = new int[1] ;
         T000V44_n2047Servico_LinNegCod = new bool[] {false} ;
         T000V44_A2037LinhaNegocio_Identificador = new String[] {""} ;
         T000V44_n2037LinhaNegocio_Identificador = new bool[] {false} ;
         isValidOutput = new GxUnknownObjectCollection();
         T000V45_A158ServicoGrupo_Descricao = new String[] {""} ;
         T000V46_A641Servico_VincDesc = new String[] {""} ;
         T000V46_n641Servico_VincDesc = new bool[] {false} ;
         T000V46_A1557Servico_VincSigla = new String[] {""} ;
         T000V46_n1557Servico_VincSigla = new bool[] {false} ;
         T000V47_A2048Servico_LinNegDsc = new String[] {""} ;
         T000V47_n2048Servico_LinNegDsc = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.servico__default(),
            new Object[][] {
                new Object[] {
               T000V2_A155Servico_Codigo, T000V2_A608Servico_Nome, T000V2_A156Servico_Descricao, T000V2_n156Servico_Descricao, T000V2_A605Servico_Sigla, T000V2_A1077Servico_UORespExclusiva, T000V2_n1077Servico_UORespExclusiva, T000V2_A889Servico_Terceriza, T000V2_n889Servico_Terceriza, T000V2_A1061Servico_Tela,
               T000V2_n1061Servico_Tela, T000V2_A1072Servico_Atende, T000V2_n1072Servico_Atende, T000V2_A1429Servico_ObrigaValores, T000V2_n1429Servico_ObrigaValores, T000V2_A1436Servico_ObjetoControle, T000V2_A1530Servico_TipoHierarquia, T000V2_n1530Servico_TipoHierarquia, T000V2_A1534Servico_PercTmp, T000V2_n1534Servico_PercTmp,
               T000V2_A1535Servico_PercPgm, T000V2_n1535Servico_PercPgm, T000V2_A1536Servico_PercCnc, T000V2_n1536Servico_PercCnc, T000V2_A1545Servico_Anterior, T000V2_n1545Servico_Anterior, T000V2_A1546Servico_Posterior, T000V2_n1546Servico_Posterior, T000V2_A632Servico_Ativo, T000V2_A1635Servico_IsPublico,
               T000V2_A2092Servico_IsOrigemReferencia, T000V2_A2131Servico_PausaSLA, T000V2_A157ServicoGrupo_Codigo, T000V2_A633Servico_UO, T000V2_n633Servico_UO, T000V2_A2047Servico_LinNegCod, T000V2_n2047Servico_LinNegCod, T000V2_A631Servico_Vinculado, T000V2_n631Servico_Vinculado
               }
               , new Object[] {
               T000V3_A155Servico_Codigo, T000V3_A608Servico_Nome, T000V3_A156Servico_Descricao, T000V3_n156Servico_Descricao, T000V3_A605Servico_Sigla, T000V3_A1077Servico_UORespExclusiva, T000V3_n1077Servico_UORespExclusiva, T000V3_A889Servico_Terceriza, T000V3_n889Servico_Terceriza, T000V3_A1061Servico_Tela,
               T000V3_n1061Servico_Tela, T000V3_A1072Servico_Atende, T000V3_n1072Servico_Atende, T000V3_A1429Servico_ObrigaValores, T000V3_n1429Servico_ObrigaValores, T000V3_A1436Servico_ObjetoControle, T000V3_A1530Servico_TipoHierarquia, T000V3_n1530Servico_TipoHierarquia, T000V3_A1534Servico_PercTmp, T000V3_n1534Servico_PercTmp,
               T000V3_A1535Servico_PercPgm, T000V3_n1535Servico_PercPgm, T000V3_A1536Servico_PercCnc, T000V3_n1536Servico_PercCnc, T000V3_A1545Servico_Anterior, T000V3_n1545Servico_Anterior, T000V3_A1546Servico_Posterior, T000V3_n1546Servico_Posterior, T000V3_A632Servico_Ativo, T000V3_A1635Servico_IsPublico,
               T000V3_A2092Servico_IsOrigemReferencia, T000V3_A2131Servico_PausaSLA, T000V3_A157ServicoGrupo_Codigo, T000V3_A633Servico_UO, T000V3_n633Servico_UO, T000V3_A2047Servico_LinNegCod, T000V3_n2047Servico_LinNegCod, T000V3_A631Servico_Vinculado, T000V3_n631Servico_Vinculado
               }
               , new Object[] {
               T000V4_A158ServicoGrupo_Descricao
               }
               , new Object[] {
               T000V5_A633Servico_UO
               }
               , new Object[] {
               T000V6_A2048Servico_LinNegDsc, T000V6_n2048Servico_LinNegDsc
               }
               , new Object[] {
               T000V7_A641Servico_VincDesc, T000V7_n641Servico_VincDesc, T000V7_A1557Servico_VincSigla, T000V7_n1557Servico_VincSigla
               }
               , new Object[] {
               T000V8_A157ServicoGrupo_Codigo, T000V8_A158ServicoGrupo_Descricao
               }
               , new Object[] {
               T000V9_A155Servico_Codigo, T000V9_A608Servico_Nome, T000V9_A156Servico_Descricao, T000V9_n156Servico_Descricao, T000V9_A605Servico_Sigla, T000V9_A158ServicoGrupo_Descricao, T000V9_A1077Servico_UORespExclusiva, T000V9_n1077Servico_UORespExclusiva, T000V9_A641Servico_VincDesc, T000V9_n641Servico_VincDesc,
               T000V9_A1557Servico_VincSigla, T000V9_n1557Servico_VincSigla, T000V9_A889Servico_Terceriza, T000V9_n889Servico_Terceriza, T000V9_A1061Servico_Tela, T000V9_n1061Servico_Tela, T000V9_A1072Servico_Atende, T000V9_n1072Servico_Atende, T000V9_A1429Servico_ObrigaValores, T000V9_n1429Servico_ObrigaValores,
               T000V9_A1436Servico_ObjetoControle, T000V9_A1530Servico_TipoHierarquia, T000V9_n1530Servico_TipoHierarquia, T000V9_A1534Servico_PercTmp, T000V9_n1534Servico_PercTmp, T000V9_A1535Servico_PercPgm, T000V9_n1535Servico_PercPgm, T000V9_A1536Servico_PercCnc, T000V9_n1536Servico_PercCnc, T000V9_A1545Servico_Anterior,
               T000V9_n1545Servico_Anterior, T000V9_A1546Servico_Posterior, T000V9_n1546Servico_Posterior, T000V9_A632Servico_Ativo, T000V9_A1635Servico_IsPublico, T000V9_A2048Servico_LinNegDsc, T000V9_n2048Servico_LinNegDsc, T000V9_A2092Servico_IsOrigemReferencia, T000V9_A2131Servico_PausaSLA, T000V9_A157ServicoGrupo_Codigo,
               T000V9_A633Servico_UO, T000V9_n633Servico_UO, T000V9_A2047Servico_LinNegCod, T000V9_n2047Servico_LinNegCod, T000V9_A631Servico_Vinculado, T000V9_n631Servico_Vinculado
               }
               , new Object[] {
               T000V10_A158ServicoGrupo_Descricao
               }
               , new Object[] {
               T000V11_A641Servico_VincDesc, T000V11_n641Servico_VincDesc, T000V11_A1557Servico_VincSigla, T000V11_n1557Servico_VincSigla
               }
               , new Object[] {
               T000V12_A2048Servico_LinNegDsc, T000V12_n2048Servico_LinNegDsc
               }
               , new Object[] {
               T000V13_A633Servico_UO
               }
               , new Object[] {
               T000V14_A155Servico_Codigo
               }
               , new Object[] {
               T000V15_A155Servico_Codigo
               }
               , new Object[] {
               T000V16_A155Servico_Codigo
               }
               , new Object[] {
               T000V17_A155Servico_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T000V20_A158ServicoGrupo_Descricao
               }
               , new Object[] {
               T000V21_A641Servico_VincDesc, T000V21_n641Servico_VincDesc, T000V21_A1557Servico_VincSigla, T000V21_n1557Servico_VincSigla
               }
               , new Object[] {
               T000V22_A2048Servico_LinNegDsc, T000V22_n2048Servico_LinNegDsc
               }
               , new Object[] {
               T000V23_A631Servico_Vinculado
               }
               , new Object[] {
               T000V24_A1528ServicoFluxo_Codigo
               }
               , new Object[] {
               T000V25_A1528ServicoFluxo_Codigo
               }
               , new Object[] {
               T000V26_A1440ServicoPrioridade_Codigo
               }
               , new Object[] {
               T000V27_A160ContratoServicos_Codigo, T000V27_A1067ContratoServicosSistemas_ServicoCod, T000V27_A1063ContratoServicosSistemas_SistemaCod
               }
               , new Object[] {
               T000V28_A5AreaTrabalho_Codigo
               }
               , new Object[] {
               T000V29_A828UsuarioServicos_UsuarioCod, T000V29_A829UsuarioServicos_ServicoCod
               }
               , new Object[] {
               T000V30_A456ContagemResultado_Codigo
               }
               , new Object[] {
               T000V31_A439Solicitacoes_Codigo
               }
               , new Object[] {
               T000V32_A319ServicoAns_Codigo
               }
               , new Object[] {
               T000V33_A160ContratoServicos_Codigo
               }
               , new Object[] {
               T000V34_A155Servico_Codigo, T000V34_A1839Check_Codigo
               }
               , new Object[] {
               T000V35_A155Servico_Codigo, T000V35_A1766ServicoArtefato_ArtefatoCod
               }
               , new Object[] {
               T000V36_A155Servico_Codigo
               }
               , new Object[] {
               T000V37_A157ServicoGrupo_Codigo, T000V37_A158ServicoGrupo_Descricao
               }
               , new Object[] {
               T000V38_A611UnidadeOrganizacional_Codigo, T000V38_A612UnidadeOrganizacional_Nome
               }
               , new Object[] {
               T000V39_A155Servico_Codigo, T000V39_A608Servico_Nome
               }
               , new Object[] {
               T000V40_A155Servico_Codigo, T000V40_A605Servico_Sigla
               }
               , new Object[] {
               T000V41_A155Servico_Codigo, T000V41_A605Servico_Sigla
               }
               , new Object[] {
               T000V43_A61ContratanteUsuario_UsuarioPessoaCod, T000V43_n61ContratanteUsuario_UsuarioPessoaCod, T000V43_A63ContratanteUsuario_ContratanteCod, T000V43_A60ContratanteUsuario_UsuarioCod, T000V43_A62ContratanteUsuario_UsuarioPessoaNom, T000V43_n62ContratanteUsuario_UsuarioPessoaNom, T000V43_A1020ContratanteUsuario_AreaTrabalhoCod, T000V43_n1020ContratanteUsuario_AreaTrabalhoCod
               }
               , new Object[] {
               T000V44_A2047Servico_LinNegCod, T000V44_A2037LinhaNegocio_Identificador, T000V44_n2037LinhaNegocio_Identificador
               }
               , new Object[] {
               T000V45_A158ServicoGrupo_Descricao
               }
               , new Object[] {
               T000V46_A641Servico_VincDesc, T000V46_n641Servico_VincDesc, T000V46_A1557Servico_VincSigla, T000V46_n1557Servico_VincSigla
               }
               , new Object[] {
               T000V47_A2048Servico_LinNegDsc, T000V47_n2048Servico_LinNegDsc
               }
            }
         );
         Z2131Servico_PausaSLA = false;
         A2131Servico_PausaSLA = false;
         i2131Servico_PausaSLA = false;
         Z1635Servico_IsPublico = false;
         A1635Servico_IsPublico = false;
         i1635Servico_IsPublico = false;
         Z632Servico_Ativo = true;
         i632Servico_Ativo = true;
         A632Servico_Ativo = true;
         Z1530Servico_TipoHierarquia = 1;
         n1530Servico_TipoHierarquia = false;
         A1530Servico_TipoHierarquia = 1;
         n1530Servico_TipoHierarquia = false;
         i1530Servico_TipoHierarquia = 1;
         n1530Servico_TipoHierarquia = false;
         AV25Pgmname = "Servico";
      }

      private short Z1530Servico_TipoHierarquia ;
      private short Z1534Servico_PercTmp ;
      private short Z1535Servico_PercPgm ;
      private short Z1536Servico_PercCnc ;
      private short GxWebError ;
      private short A640Servico_Vinculados ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short A1534Servico_PercTmp ;
      private short A1536Servico_PercCnc ;
      private short A1535Servico_PercPgm ;
      private short A1530Servico_TipoHierarquia ;
      private short Gx_BScreen ;
      private short RcdFound130 ;
      private short GX_JID ;
      private short gxajaxcallmode ;
      private short i1530Servico_TipoHierarquia ;
      private short GXt_int2 ;
      private short wbTemp ;
      private int wcpOAV7Servico_Codigo ;
      private int wcpOAV16ServicoGrupo_Codigo ;
      private int Z155Servico_Codigo ;
      private int Z1545Servico_Anterior ;
      private int Z1546Servico_Posterior ;
      private int Z157ServicoGrupo_Codigo ;
      private int Z633Servico_UO ;
      private int Z2047Servico_LinNegCod ;
      private int Z631Servico_Vinculado ;
      private int N157ServicoGrupo_Codigo ;
      private int N633Servico_UO ;
      private int N631Servico_Vinculado ;
      private int N2047Servico_LinNegCod ;
      private int AV16ServicoGrupo_Codigo ;
      private int AV7Servico_Codigo ;
      private int A157ServicoGrupo_Codigo ;
      private int A155Servico_Codigo ;
      private int A631Servico_Vinculado ;
      private int A2047Servico_LinNegCod ;
      private int A633Servico_UO ;
      private int trnEnded ;
      private int AV17Servico_UO ;
      private int A1545Servico_Anterior ;
      private int A1546Servico_Posterior ;
      private int AV22Responsavel_Codigo ;
      private int AV20Contratante_Codigo ;
      private int edtavContratante_codigo_Enabled ;
      private int edtavContratante_codigo_Visible ;
      private int edtServico_Codigo_Enabled ;
      private int edtServico_Codigo_Visible ;
      private int bttBtn_trn_enter_Visible ;
      private int bttBtn_trn_enter_Enabled ;
      private int bttBtn_trn_delete_Visible ;
      private int bttBtn_trn_delete_Enabled ;
      private int bttBtn_trn_cancel_Visible ;
      private int edtServico_Nome_Enabled ;
      private int edtServico_Sigla_Enabled ;
      private int edtServico_Descricao_Enabled ;
      private int lblTextblockresponsavel_codigo_Visible ;
      private int lblTextblockservico_ativo_Visible ;
      private int edtServico_PercTmp_Enabled ;
      private int edtServico_PercTmp_Visible ;
      private int edtServico_PercCnc_Enabled ;
      private int edtServico_PercPgm_Enabled ;
      private int imgSelectuo_Visible ;
      private int A1551Servico_Responsavel ;
      private int AV11Insert_ServicoGrupo_Codigo ;
      private int AV15Insert_Servico_UO ;
      private int AV14Insert_Servico_Vinculado ;
      private int AV24Insert_Servico_LinNegCod ;
      private int AV21Servico_Responsavel ;
      private int AV26GXV1 ;
      private int idxLst ;
      private int gxdynajaxindex ;
      private int GXt_int1 ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String Z608Servico_Nome ;
      private String Z605Servico_Sigla ;
      private String Z1061Servico_Tela ;
      private String Z1072Servico_Atende ;
      private String Z1429Servico_ObrigaValores ;
      private String Z1436Servico_ObjetoControle ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String Gx_mode ;
      private String GXKey ;
      private String A1061Servico_Tela ;
      private String A1072Servico_Atende ;
      private String A1429Servico_ObrigaValores ;
      private String A1436Servico_ObjetoControle ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String cmbServico_UORespExclusiva_Internalname ;
      private String edtavContratante_codigo_Internalname ;
      private String edtavContratante_codigo_Jsonclick ;
      private String edtServico_Codigo_Internalname ;
      private String edtServico_Codigo_Jsonclick ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtn_trn_delete_Internalname ;
      private String bttBtn_trn_delete_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockservico_uo_Internalname ;
      private String lblTextblockservico_uo_Jsonclick ;
      private String lblTextblockservico_uorespexclusiva_Internalname ;
      private String lblTextblockservico_uorespexclusiva_Jsonclick ;
      private String cmbServico_UORespExclusiva_Jsonclick ;
      private String lblTextblockservicogrupo_codigo_Internalname ;
      private String lblTextblockservicogrupo_codigo_Jsonclick ;
      private String dynServicoGrupo_Codigo_Internalname ;
      private String dynServicoGrupo_Codigo_Jsonclick ;
      private String lblTextblockservico_nome_Internalname ;
      private String lblTextblockservico_nome_Jsonclick ;
      private String edtServico_Nome_Internalname ;
      private String A608Servico_Nome ;
      private String edtServico_Nome_Jsonclick ;
      private String lblTextblockservico_sigla_Internalname ;
      private String lblTextblockservico_sigla_Jsonclick ;
      private String edtServico_Sigla_Internalname ;
      private String A605Servico_Sigla ;
      private String edtServico_Sigla_Jsonclick ;
      private String lblTextblockservico_vinculado_Internalname ;
      private String lblTextblockservico_vinculado_Jsonclick ;
      private String dynServico_Vinculado_Internalname ;
      private String dynServico_Vinculado_Jsonclick ;
      private String lblTextblockservico_descricao_Internalname ;
      private String lblTextblockservico_descricao_Jsonclick ;
      private String edtServico_Descricao_Internalname ;
      private String lblTextblockservico_anterior_Internalname ;
      private String lblTextblockservico_anterior_Jsonclick ;
      private String dynServico_Anterior_Internalname ;
      private String dynServico_Anterior_Jsonclick ;
      private String lblTextblockservico_posterior_Internalname ;
      private String lblTextblockservico_posterior_Jsonclick ;
      private String dynServico_Posterior_Internalname ;
      private String dynServico_Posterior_Jsonclick ;
      private String lblTextblockservico_terceriza_Internalname ;
      private String lblTextblockservico_terceriza_Jsonclick ;
      private String cmbServico_Terceriza_Internalname ;
      private String cmbServico_Terceriza_Jsonclick ;
      private String lblTextblockservico_tela_Internalname ;
      private String lblTextblockservico_tela_Jsonclick ;
      private String cmbServico_Tela_Internalname ;
      private String cmbServico_Tela_Jsonclick ;
      private String lblTextblockservico_atende_Internalname ;
      private String lblTextblockservico_atende_Jsonclick ;
      private String cmbServico_Atende_Internalname ;
      private String cmbServico_Atende_Jsonclick ;
      private String lblTextblockservico_obrigavalores_Internalname ;
      private String lblTextblockservico_obrigavalores_Jsonclick ;
      private String cmbServico_ObrigaValores_Internalname ;
      private String cmbServico_ObrigaValores_Jsonclick ;
      private String lblTextblockservico_objetocontrole_Internalname ;
      private String lblTextblockservico_objetocontrole_Jsonclick ;
      private String cmbServico_ObjetoControle_Internalname ;
      private String cmbServico_ObjetoControle_Jsonclick ;
      private String lblTextblockservico_percpgm_Internalname ;
      private String lblTextblockservico_percpgm_Jsonclick ;
      private String lblTextblockservico_perccnc_Internalname ;
      private String lblTextblockservico_perccnc_Jsonclick ;
      private String cellTextblockservico_perctmp_cell_Internalname ;
      private String cellTextblockservico_perctmp_cell_Class ;
      private String lblTextblockservico_perctmp_Internalname ;
      private String lblTextblockservico_perctmp_Jsonclick ;
      private String cellServico_perctmp_cell_Internalname ;
      private String cellServico_perctmp_cell_Class ;
      private String lblTextblockservico_ispublico_Internalname ;
      private String lblTextblockservico_ispublico_Jsonclick ;
      private String cmbServico_IsPublico_Internalname ;
      private String cmbServico_IsPublico_Jsonclick ;
      private String lblTextblockresponsavel_codigo_Internalname ;
      private String lblTextblockresponsavel_codigo_Jsonclick ;
      private String dynavResponsavel_codigo_Internalname ;
      private String dynavResponsavel_codigo_Jsonclick ;
      private String lblTextblockservico_linnegcod_Internalname ;
      private String lblTextblockservico_linnegcod_Jsonclick ;
      private String dynServico_LinNegCod_Internalname ;
      private String dynServico_LinNegCod_Jsonclick ;
      private String lblTextblockservico_isorigemreferencia_Internalname ;
      private String lblTextblockservico_isorigemreferencia_Jsonclick ;
      private String cmbServico_IsOrigemReferencia_Internalname ;
      private String cmbServico_IsOrigemReferencia_Jsonclick ;
      private String lblTextblockservico_pausasla_Internalname ;
      private String lblTextblockservico_pausasla_Jsonclick ;
      private String cmbServico_PausaSLA_Internalname ;
      private String cmbServico_PausaSLA_Jsonclick ;
      private String lblTextblockservico_ativo_Internalname ;
      private String lblTextblockservico_ativo_Jsonclick ;
      private String cmbServico_Ativo_Internalname ;
      private String cmbServico_Ativo_Jsonclick ;
      private String tblTablemergedservico_perctmp_Internalname ;
      private String edtServico_PercTmp_Internalname ;
      private String edtServico_PercTmp_Jsonclick ;
      private String lblServico_perctmp_righttext_Internalname ;
      private String lblServico_perctmp_righttext_Jsonclick ;
      private String tblTablemergedservico_perccnc_Internalname ;
      private String edtServico_PercCnc_Internalname ;
      private String edtServico_PercCnc_Jsonclick ;
      private String lblServico_perccnc_righttext_Internalname ;
      private String lblServico_perccnc_righttext_Jsonclick ;
      private String tblTablemergedservico_percpgm_Internalname ;
      private String edtServico_PercPgm_Internalname ;
      private String edtServico_PercPgm_Jsonclick ;
      private String lblServico_percpgm_righttext_Internalname ;
      private String lblServico_percpgm_righttext_Jsonclick ;
      private String tblTablemergedservico_uo_Internalname ;
      private String dynavServico_uo_Internalname ;
      private String dynavServico_uo_Jsonclick ;
      private String imgSelectuo_Internalname ;
      private String imgSelectuo_Jsonclick ;
      private String A1557Servico_VincSigla ;
      private String AV25Pgmname ;
      private String Dvpanel_tableattributes_Width ;
      private String Dvpanel_tableattributes_Height ;
      private String Dvpanel_tableattributes_Cls ;
      private String Dvpanel_tableattributes_Title ;
      private String Dvpanel_tableattributes_Class ;
      private String Dvpanel_tableattributes_Iconposition ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode130 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String Z1557Servico_VincSigla ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Dvpanel_tableattributes_Internalname ;
      private String gxwrpcisep ;
      private bool Z1077Servico_UORespExclusiva ;
      private bool Z889Servico_Terceriza ;
      private bool Z632Servico_Ativo ;
      private bool Z1635Servico_IsPublico ;
      private bool Z2092Servico_IsOrigemReferencia ;
      private bool Z2131Servico_PausaSLA ;
      private bool entryPointCalled ;
      private bool A632Servico_Ativo ;
      private bool n631Servico_Vinculado ;
      private bool n2047Servico_LinNegCod ;
      private bool n633Servico_UO ;
      private bool toggleJsOutput ;
      private bool A1077Servico_UORespExclusiva ;
      private bool n1077Servico_UORespExclusiva ;
      private bool A889Servico_Terceriza ;
      private bool n889Servico_Terceriza ;
      private bool n1061Servico_Tela ;
      private bool n1072Servico_Atende ;
      private bool n1429Servico_ObrigaValores ;
      private bool A1635Servico_IsPublico ;
      private bool A2092Servico_IsOrigemReferencia ;
      private bool A2131Servico_PausaSLA ;
      private bool wbErr ;
      private bool n1545Servico_Anterior ;
      private bool n1546Servico_Posterior ;
      private bool n156Servico_Descricao ;
      private bool n1535Servico_PercPgm ;
      private bool n1536Servico_PercCnc ;
      private bool n1534Servico_PercTmp ;
      private bool n1530Servico_TipoHierarquia ;
      private bool n2048Servico_LinNegDsc ;
      private bool n641Servico_VincDesc ;
      private bool n1557Servico_VincSigla ;
      private bool Dvpanel_tableattributes_Collapsible ;
      private bool Dvpanel_tableattributes_Collapsed ;
      private bool Dvpanel_tableattributes_Enabled ;
      private bool Dvpanel_tableattributes_Autowidth ;
      private bool Dvpanel_tableattributes_Autoheight ;
      private bool Dvpanel_tableattributes_Showheader ;
      private bool Dvpanel_tableattributes_Showcollapseicon ;
      private bool Dvpanel_tableattributes_Autoscroll ;
      private bool Dvpanel_tableattributes_Visible ;
      private bool returnInSub ;
      private bool Gx_longc ;
      private bool i2131Servico_PausaSLA ;
      private bool i1635Servico_IsPublico ;
      private bool i632Servico_Ativo ;
      private String A156Servico_Descricao ;
      private String A641Servico_VincDesc ;
      private String Z156Servico_Descricao ;
      private String Z641Servico_VincDesc ;
      private String A2107Servico_Identificacao ;
      private String A158ServicoGrupo_Descricao ;
      private String A2048Servico_LinNegDsc ;
      private String Z158ServicoGrupo_Descricao ;
      private String Z2048Servico_LinNegDsc ;
      private IGxSession AV10WebSession ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GxUnknownObjectCollection isValidOutput ;
      private IDataStoreProvider pr_default ;
      private int[] T000V8_A157ServicoGrupo_Codigo ;
      private String[] T000V8_A158ServicoGrupo_Descricao ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP2_ServicoGrupo_Codigo ;
      private GXCombobox dynavServico_uo ;
      private GXCombobox cmbServico_UORespExclusiva ;
      private GXCombobox dynServicoGrupo_Codigo ;
      private GXCombobox dynServico_Vinculado ;
      private GXCombobox dynServico_Anterior ;
      private GXCombobox dynServico_Posterior ;
      private GXCombobox cmbServico_Terceriza ;
      private GXCombobox cmbServico_Tela ;
      private GXCombobox cmbServico_Atende ;
      private GXCombobox cmbServico_ObrigaValores ;
      private GXCombobox cmbServico_ObjetoControle ;
      private GXCombobox cmbServico_IsPublico ;
      private GXCombobox dynavResponsavel_codigo ;
      private GXCombobox dynServico_LinNegCod ;
      private GXCombobox cmbServico_IsOrigemReferencia ;
      private GXCombobox cmbServico_PausaSLA ;
      private GXCombobox cmbServico_Ativo ;
      private String[] T000V6_A2048Servico_LinNegDsc ;
      private bool[] T000V6_n2048Servico_LinNegDsc ;
      private String[] T000V7_A641Servico_VincDesc ;
      private bool[] T000V7_n641Servico_VincDesc ;
      private String[] T000V7_A1557Servico_VincSigla ;
      private bool[] T000V7_n1557Servico_VincSigla ;
      private bool[] T000V3_n631Servico_Vinculado ;
      private String[] T000V4_A158ServicoGrupo_Descricao ;
      private int[] T000V9_A155Servico_Codigo ;
      private String[] T000V9_A608Servico_Nome ;
      private String[] T000V9_A156Servico_Descricao ;
      private bool[] T000V9_n156Servico_Descricao ;
      private String[] T000V9_A605Servico_Sigla ;
      private String[] T000V9_A158ServicoGrupo_Descricao ;
      private bool[] T000V9_A1077Servico_UORespExclusiva ;
      private bool[] T000V9_n1077Servico_UORespExclusiva ;
      private String[] T000V9_A641Servico_VincDesc ;
      private bool[] T000V9_n641Servico_VincDesc ;
      private String[] T000V9_A1557Servico_VincSigla ;
      private bool[] T000V9_n1557Servico_VincSigla ;
      private bool[] T000V9_A889Servico_Terceriza ;
      private bool[] T000V9_n889Servico_Terceriza ;
      private String[] T000V9_A1061Servico_Tela ;
      private bool[] T000V9_n1061Servico_Tela ;
      private String[] T000V9_A1072Servico_Atende ;
      private bool[] T000V9_n1072Servico_Atende ;
      private String[] T000V9_A1429Servico_ObrigaValores ;
      private bool[] T000V9_n1429Servico_ObrigaValores ;
      private String[] T000V9_A1436Servico_ObjetoControle ;
      private short[] T000V9_A1530Servico_TipoHierarquia ;
      private bool[] T000V9_n1530Servico_TipoHierarquia ;
      private short[] T000V9_A1534Servico_PercTmp ;
      private bool[] T000V9_n1534Servico_PercTmp ;
      private short[] T000V9_A1535Servico_PercPgm ;
      private bool[] T000V9_n1535Servico_PercPgm ;
      private short[] T000V9_A1536Servico_PercCnc ;
      private bool[] T000V9_n1536Servico_PercCnc ;
      private int[] T000V9_A1545Servico_Anterior ;
      private bool[] T000V9_n1545Servico_Anterior ;
      private int[] T000V9_A1546Servico_Posterior ;
      private bool[] T000V9_n1546Servico_Posterior ;
      private bool[] T000V9_A632Servico_Ativo ;
      private bool[] T000V9_A1635Servico_IsPublico ;
      private String[] T000V9_A2048Servico_LinNegDsc ;
      private bool[] T000V9_n2048Servico_LinNegDsc ;
      private bool[] T000V9_A2092Servico_IsOrigemReferencia ;
      private bool[] T000V9_A2131Servico_PausaSLA ;
      private int[] T000V9_A157ServicoGrupo_Codigo ;
      private int[] T000V9_A633Servico_UO ;
      private bool[] T000V9_n633Servico_UO ;
      private int[] T000V9_A2047Servico_LinNegCod ;
      private bool[] T000V9_n2047Servico_LinNegCod ;
      private int[] T000V9_A631Servico_Vinculado ;
      private bool[] T000V9_n631Servico_Vinculado ;
      private int[] T000V5_A633Servico_UO ;
      private bool[] T000V5_n633Servico_UO ;
      private String[] T000V10_A158ServicoGrupo_Descricao ;
      private String[] T000V11_A641Servico_VincDesc ;
      private bool[] T000V11_n641Servico_VincDesc ;
      private String[] T000V11_A1557Servico_VincSigla ;
      private bool[] T000V11_n1557Servico_VincSigla ;
      private String[] T000V12_A2048Servico_LinNegDsc ;
      private bool[] T000V12_n2048Servico_LinNegDsc ;
      private int[] T000V13_A633Servico_UO ;
      private bool[] T000V13_n633Servico_UO ;
      private int[] T000V14_A155Servico_Codigo ;
      private int[] T000V3_A155Servico_Codigo ;
      private String[] T000V3_A608Servico_Nome ;
      private String[] T000V3_A156Servico_Descricao ;
      private bool[] T000V3_n156Servico_Descricao ;
      private String[] T000V3_A605Servico_Sigla ;
      private bool[] T000V3_A1077Servico_UORespExclusiva ;
      private bool[] T000V3_n1077Servico_UORespExclusiva ;
      private bool[] T000V3_A889Servico_Terceriza ;
      private bool[] T000V3_n889Servico_Terceriza ;
      private String[] T000V3_A1061Servico_Tela ;
      private bool[] T000V3_n1061Servico_Tela ;
      private String[] T000V3_A1072Servico_Atende ;
      private bool[] T000V3_n1072Servico_Atende ;
      private String[] T000V3_A1429Servico_ObrigaValores ;
      private bool[] T000V3_n1429Servico_ObrigaValores ;
      private String[] T000V3_A1436Servico_ObjetoControle ;
      private short[] T000V3_A1530Servico_TipoHierarquia ;
      private bool[] T000V3_n1530Servico_TipoHierarquia ;
      private short[] T000V3_A1534Servico_PercTmp ;
      private bool[] T000V3_n1534Servico_PercTmp ;
      private short[] T000V3_A1535Servico_PercPgm ;
      private bool[] T000V3_n1535Servico_PercPgm ;
      private short[] T000V3_A1536Servico_PercCnc ;
      private bool[] T000V3_n1536Servico_PercCnc ;
      private int[] T000V3_A1545Servico_Anterior ;
      private bool[] T000V3_n1545Servico_Anterior ;
      private int[] T000V3_A1546Servico_Posterior ;
      private bool[] T000V3_n1546Servico_Posterior ;
      private bool[] T000V3_A632Servico_Ativo ;
      private bool[] T000V3_A1635Servico_IsPublico ;
      private bool[] T000V3_A2092Servico_IsOrigemReferencia ;
      private bool[] T000V3_A2131Servico_PausaSLA ;
      private int[] T000V3_A157ServicoGrupo_Codigo ;
      private int[] T000V3_A633Servico_UO ;
      private bool[] T000V3_n633Servico_UO ;
      private int[] T000V3_A2047Servico_LinNegCod ;
      private bool[] T000V3_n2047Servico_LinNegCod ;
      private int[] T000V3_A631Servico_Vinculado ;
      private int[] T000V15_A155Servico_Codigo ;
      private int[] T000V16_A155Servico_Codigo ;
      private int[] T000V2_A155Servico_Codigo ;
      private String[] T000V2_A608Servico_Nome ;
      private String[] T000V2_A156Servico_Descricao ;
      private bool[] T000V2_n156Servico_Descricao ;
      private String[] T000V2_A605Servico_Sigla ;
      private bool[] T000V2_A1077Servico_UORespExclusiva ;
      private bool[] T000V2_n1077Servico_UORespExclusiva ;
      private bool[] T000V2_A889Servico_Terceriza ;
      private bool[] T000V2_n889Servico_Terceriza ;
      private String[] T000V2_A1061Servico_Tela ;
      private bool[] T000V2_n1061Servico_Tela ;
      private String[] T000V2_A1072Servico_Atende ;
      private bool[] T000V2_n1072Servico_Atende ;
      private String[] T000V2_A1429Servico_ObrigaValores ;
      private bool[] T000V2_n1429Servico_ObrigaValores ;
      private String[] T000V2_A1436Servico_ObjetoControle ;
      private short[] T000V2_A1530Servico_TipoHierarquia ;
      private bool[] T000V2_n1530Servico_TipoHierarquia ;
      private short[] T000V2_A1534Servico_PercTmp ;
      private bool[] T000V2_n1534Servico_PercTmp ;
      private short[] T000V2_A1535Servico_PercPgm ;
      private bool[] T000V2_n1535Servico_PercPgm ;
      private short[] T000V2_A1536Servico_PercCnc ;
      private bool[] T000V2_n1536Servico_PercCnc ;
      private int[] T000V2_A1545Servico_Anterior ;
      private bool[] T000V2_n1545Servico_Anterior ;
      private int[] T000V2_A1546Servico_Posterior ;
      private bool[] T000V2_n1546Servico_Posterior ;
      private bool[] T000V2_A632Servico_Ativo ;
      private bool[] T000V2_A1635Servico_IsPublico ;
      private bool[] T000V2_A2092Servico_IsOrigemReferencia ;
      private bool[] T000V2_A2131Servico_PausaSLA ;
      private int[] T000V2_A157ServicoGrupo_Codigo ;
      private int[] T000V2_A633Servico_UO ;
      private bool[] T000V2_n633Servico_UO ;
      private int[] T000V2_A2047Servico_LinNegCod ;
      private bool[] T000V2_n2047Servico_LinNegCod ;
      private int[] T000V2_A631Servico_Vinculado ;
      private bool[] T000V2_n631Servico_Vinculado ;
      private int[] T000V17_A155Servico_Codigo ;
      private String[] T000V20_A158ServicoGrupo_Descricao ;
      private String[] T000V21_A641Servico_VincDesc ;
      private bool[] T000V21_n641Servico_VincDesc ;
      private String[] T000V21_A1557Servico_VincSigla ;
      private bool[] T000V21_n1557Servico_VincSigla ;
      private String[] T000V22_A2048Servico_LinNegDsc ;
      private bool[] T000V22_n2048Servico_LinNegDsc ;
      private int[] T000V23_A631Servico_Vinculado ;
      private bool[] T000V23_n631Servico_Vinculado ;
      private int[] T000V24_A1528ServicoFluxo_Codigo ;
      private int[] T000V25_A1528ServicoFluxo_Codigo ;
      private int[] T000V26_A1440ServicoPrioridade_Codigo ;
      private int[] T000V27_A160ContratoServicos_Codigo ;
      private int[] T000V27_A1067ContratoServicosSistemas_ServicoCod ;
      private int[] T000V27_A1063ContratoServicosSistemas_SistemaCod ;
      private int[] T000V28_A5AreaTrabalho_Codigo ;
      private int[] T000V29_A828UsuarioServicos_UsuarioCod ;
      private int[] T000V29_A829UsuarioServicos_ServicoCod ;
      private int[] T000V30_A456ContagemResultado_Codigo ;
      private int[] T000V31_A439Solicitacoes_Codigo ;
      private int[] T000V32_A319ServicoAns_Codigo ;
      private int[] T000V33_A160ContratoServicos_Codigo ;
      private int[] T000V34_A155Servico_Codigo ;
      private int[] T000V34_A1839Check_Codigo ;
      private int[] T000V35_A155Servico_Codigo ;
      private int[] T000V35_A1766ServicoArtefato_ArtefatoCod ;
      private int[] T000V36_A155Servico_Codigo ;
      private int[] T000V37_A157ServicoGrupo_Codigo ;
      private String[] T000V37_A158ServicoGrupo_Descricao ;
      private int[] T000V38_A611UnidadeOrganizacional_Codigo ;
      private String[] T000V38_A612UnidadeOrganizacional_Nome ;
      private int[] T000V39_A155Servico_Codigo ;
      private String[] T000V39_A608Servico_Nome ;
      private int[] T000V40_A155Servico_Codigo ;
      private String[] T000V40_A605Servico_Sigla ;
      private int[] T000V41_A155Servico_Codigo ;
      private String[] T000V41_A605Servico_Sigla ;
      private int[] T000V43_A61ContratanteUsuario_UsuarioPessoaCod ;
      private bool[] T000V43_n61ContratanteUsuario_UsuarioPessoaCod ;
      private int[] T000V43_A63ContratanteUsuario_ContratanteCod ;
      private int[] T000V43_A60ContratanteUsuario_UsuarioCod ;
      private String[] T000V43_A62ContratanteUsuario_UsuarioPessoaNom ;
      private bool[] T000V43_n62ContratanteUsuario_UsuarioPessoaNom ;
      private int[] T000V43_A1020ContratanteUsuario_AreaTrabalhoCod ;
      private bool[] T000V43_n1020ContratanteUsuario_AreaTrabalhoCod ;
      private int[] T000V44_A2047Servico_LinNegCod ;
      private bool[] T000V44_n2047Servico_LinNegCod ;
      private String[] T000V44_A2037LinhaNegocio_Identificador ;
      private bool[] T000V44_n2037LinhaNegocio_Identificador ;
      private String[] T000V45_A158ServicoGrupo_Descricao ;
      private String[] T000V46_A641Servico_VincDesc ;
      private bool[] T000V46_n641Servico_VincDesc ;
      private String[] T000V46_A1557Servico_VincSigla ;
      private bool[] T000V46_n1557Servico_VincSigla ;
      private String[] T000V47_A2048Servico_LinNegDsc ;
      private bool[] T000V47_n2048Servico_LinNegDsc ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtAuditingObject AV19AuditingObject ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV12TrnContextAtt ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
   }

   public class servico__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
         ,new UpdateCursor(def[16])
         ,new UpdateCursor(def[17])
         ,new ForEachCursor(def[18])
         ,new ForEachCursor(def[19])
         ,new ForEachCursor(def[20])
         ,new ForEachCursor(def[21])
         ,new ForEachCursor(def[22])
         ,new ForEachCursor(def[23])
         ,new ForEachCursor(def[24])
         ,new ForEachCursor(def[25])
         ,new ForEachCursor(def[26])
         ,new ForEachCursor(def[27])
         ,new ForEachCursor(def[28])
         ,new ForEachCursor(def[29])
         ,new ForEachCursor(def[30])
         ,new ForEachCursor(def[31])
         ,new ForEachCursor(def[32])
         ,new ForEachCursor(def[33])
         ,new ForEachCursor(def[34])
         ,new ForEachCursor(def[35])
         ,new ForEachCursor(def[36])
         ,new ForEachCursor(def[37])
         ,new ForEachCursor(def[38])
         ,new ForEachCursor(def[39])
         ,new ForEachCursor(def[40])
         ,new ForEachCursor(def[41])
         ,new ForEachCursor(def[42])
         ,new ForEachCursor(def[43])
         ,new ForEachCursor(def[44])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT000V8 ;
          prmT000V8 = new Object[] {
          } ;
          Object[] prmT000V9 ;
          prmT000V9 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000V4 ;
          prmT000V4 = new Object[] {
          new Object[] {"@ServicoGrupo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000V7 ;
          prmT000V7 = new Object[] {
          new Object[] {"@Servico_Vinculado",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000V6 ;
          prmT000V6 = new Object[] {
          new Object[] {"@Servico_LinNegCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000V5 ;
          prmT000V5 = new Object[] {
          new Object[] {"@Servico_UO",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000V10 ;
          prmT000V10 = new Object[] {
          new Object[] {"@ServicoGrupo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000V11 ;
          prmT000V11 = new Object[] {
          new Object[] {"@Servico_Vinculado",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000V12 ;
          prmT000V12 = new Object[] {
          new Object[] {"@Servico_LinNegCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000V13 ;
          prmT000V13 = new Object[] {
          new Object[] {"@Servico_UO",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000V14 ;
          prmT000V14 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000V3 ;
          prmT000V3 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000V15 ;
          prmT000V15 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000V16 ;
          prmT000V16 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000V2 ;
          prmT000V2 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000V17 ;
          prmT000V17 = new Object[] {
          new Object[] {"@Servico_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@Servico_Descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@Servico_Sigla",SqlDbType.Char,15,0} ,
          new Object[] {"@Servico_UORespExclusiva",SqlDbType.Bit,4,0} ,
          new Object[] {"@Servico_Terceriza",SqlDbType.Bit,4,0} ,
          new Object[] {"@Servico_Tela",SqlDbType.Char,3,0} ,
          new Object[] {"@Servico_Atende",SqlDbType.Char,1,0} ,
          new Object[] {"@Servico_ObrigaValores",SqlDbType.Char,1,0} ,
          new Object[] {"@Servico_ObjetoControle",SqlDbType.Char,3,0} ,
          new Object[] {"@Servico_TipoHierarquia",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Servico_PercTmp",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Servico_PercPgm",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Servico_PercCnc",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Servico_Anterior",SqlDbType.Int,6,0} ,
          new Object[] {"@Servico_Posterior",SqlDbType.Int,6,0} ,
          new Object[] {"@Servico_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@Servico_IsPublico",SqlDbType.Bit,4,0} ,
          new Object[] {"@Servico_IsOrigemReferencia",SqlDbType.Bit,4,0} ,
          new Object[] {"@Servico_PausaSLA",SqlDbType.Bit,4,0} ,
          new Object[] {"@ServicoGrupo_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Servico_UO",SqlDbType.Int,6,0} ,
          new Object[] {"@Servico_LinNegCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Servico_Vinculado",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000V18 ;
          prmT000V18 = new Object[] {
          new Object[] {"@Servico_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@Servico_Descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@Servico_Sigla",SqlDbType.Char,15,0} ,
          new Object[] {"@Servico_UORespExclusiva",SqlDbType.Bit,4,0} ,
          new Object[] {"@Servico_Terceriza",SqlDbType.Bit,4,0} ,
          new Object[] {"@Servico_Tela",SqlDbType.Char,3,0} ,
          new Object[] {"@Servico_Atende",SqlDbType.Char,1,0} ,
          new Object[] {"@Servico_ObrigaValores",SqlDbType.Char,1,0} ,
          new Object[] {"@Servico_ObjetoControle",SqlDbType.Char,3,0} ,
          new Object[] {"@Servico_TipoHierarquia",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Servico_PercTmp",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Servico_PercPgm",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Servico_PercCnc",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Servico_Anterior",SqlDbType.Int,6,0} ,
          new Object[] {"@Servico_Posterior",SqlDbType.Int,6,0} ,
          new Object[] {"@Servico_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@Servico_IsPublico",SqlDbType.Bit,4,0} ,
          new Object[] {"@Servico_IsOrigemReferencia",SqlDbType.Bit,4,0} ,
          new Object[] {"@Servico_PausaSLA",SqlDbType.Bit,4,0} ,
          new Object[] {"@ServicoGrupo_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Servico_UO",SqlDbType.Int,6,0} ,
          new Object[] {"@Servico_LinNegCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Servico_Vinculado",SqlDbType.Int,6,0} ,
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000V19 ;
          prmT000V19 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000V20 ;
          prmT000V20 = new Object[] {
          new Object[] {"@ServicoGrupo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000V21 ;
          prmT000V21 = new Object[] {
          new Object[] {"@Servico_Vinculado",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000V22 ;
          prmT000V22 = new Object[] {
          new Object[] {"@Servico_LinNegCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000V23 ;
          prmT000V23 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000V24 ;
          prmT000V24 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000V25 ;
          prmT000V25 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000V26 ;
          prmT000V26 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000V27 ;
          prmT000V27 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000V28 ;
          prmT000V28 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000V29 ;
          prmT000V29 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000V30 ;
          prmT000V30 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000V31 ;
          prmT000V31 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000V32 ;
          prmT000V32 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000V33 ;
          prmT000V33 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000V34 ;
          prmT000V34 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000V35 ;
          prmT000V35 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000V36 ;
          prmT000V36 = new Object[] {
          } ;
          Object[] prmT000V37 ;
          prmT000V37 = new Object[] {
          } ;
          Object[] prmT000V38 ;
          prmT000V38 = new Object[] {
          } ;
          Object[] prmT000V39 ;
          prmT000V39 = new Object[] {
          new Object[] {"@AV16ServicoGrupo_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ServicoGrupo_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV7Servico_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Servico_Vinculados",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Servico_Ativo",SqlDbType.Bit,4,0}
          } ;
          Object[] prmT000V40 ;
          prmT000V40 = new Object[] {
          new Object[] {"@AV7Servico_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Servico_Ativo",SqlDbType.Bit,4,0}
          } ;
          Object[] prmT000V41 ;
          prmT000V41 = new Object[] {
          new Object[] {"@AV7Servico_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Servico_Ativo",SqlDbType.Bit,4,0}
          } ;
          Object[] prmT000V43 ;
          prmT000V43 = new Object[] {
          new Object[] {"@AV8WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000V44 ;
          prmT000V44 = new Object[] {
          } ;
          Object[] prmT000V45 ;
          prmT000V45 = new Object[] {
          new Object[] {"@ServicoGrupo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000V46 ;
          prmT000V46 = new Object[] {
          new Object[] {"@Servico_Vinculado",SqlDbType.Int,6,0}
          } ;
          Object[] prmT000V47 ;
          prmT000V47 = new Object[] {
          new Object[] {"@Servico_LinNegCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T000V2", "SELECT [Servico_Codigo], [Servico_Nome], [Servico_Descricao], [Servico_Sigla], [Servico_UORespExclusiva], [Servico_Terceriza], [Servico_Tela], [Servico_Atende], [Servico_ObrigaValores], [Servico_ObjetoControle], [Servico_TipoHierarquia], [Servico_PercTmp], [Servico_PercPgm], [Servico_PercCnc], [Servico_Anterior], [Servico_Posterior], [Servico_Ativo], [Servico_IsPublico], [Servico_IsOrigemReferencia], [Servico_PausaSLA], [ServicoGrupo_Codigo], [Servico_UO] AS Servico_UO, [Servico_LinNegCod] AS Servico_LinNegCod, [Servico_Vinculado] AS Servico_Vinculado FROM [Servico] WITH (UPDLOCK) WHERE [Servico_Codigo] = @Servico_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000V2,1,0,true,false )
             ,new CursorDef("T000V3", "SELECT [Servico_Codigo], [Servico_Nome], [Servico_Descricao], [Servico_Sigla], [Servico_UORespExclusiva], [Servico_Terceriza], [Servico_Tela], [Servico_Atende], [Servico_ObrigaValores], [Servico_ObjetoControle], [Servico_TipoHierarquia], [Servico_PercTmp], [Servico_PercPgm], [Servico_PercCnc], [Servico_Anterior], [Servico_Posterior], [Servico_Ativo], [Servico_IsPublico], [Servico_IsOrigemReferencia], [Servico_PausaSLA], [ServicoGrupo_Codigo], [Servico_UO] AS Servico_UO, [Servico_LinNegCod] AS Servico_LinNegCod, [Servico_Vinculado] AS Servico_Vinculado FROM [Servico] WITH (NOLOCK) WHERE [Servico_Codigo] = @Servico_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000V3,1,0,true,false )
             ,new CursorDef("T000V4", "SELECT [ServicoGrupo_Descricao] FROM [ServicoGrupo] WITH (NOLOCK) WHERE [ServicoGrupo_Codigo] = @ServicoGrupo_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000V4,1,0,true,false )
             ,new CursorDef("T000V5", "SELECT [UnidadeOrganizacional_Codigo] AS Servico_UO FROM [Geral_UnidadeOrganizacional] WITH (NOLOCK) WHERE [UnidadeOrganizacional_Codigo] = @Servico_UO ",true, GxErrorMask.GX_NOMASK, false, this,prmT000V5,1,0,true,false )
             ,new CursorDef("T000V6", "SELECT [LinhaNegocio_Descricao] AS Servico_LinNegDsc FROM [LinhaNegocio] WITH (NOLOCK) WHERE [LinhadeNegocio_Codigo] = @Servico_LinNegCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT000V6,1,0,true,false )
             ,new CursorDef("T000V7", "SELECT [Servico_Descricao] AS Servico_VincDesc, [Servico_Sigla] AS Servico_VincSigla FROM [Servico] WITH (NOLOCK) WHERE [Servico_Codigo] = @Servico_Vinculado ",true, GxErrorMask.GX_NOMASK, false, this,prmT000V7,1,0,true,false )
             ,new CursorDef("T000V8", "SELECT [ServicoGrupo_Codigo], [ServicoGrupo_Descricao] FROM [ServicoGrupo] WITH (NOLOCK) ORDER BY [ServicoGrupo_Descricao] ",true, GxErrorMask.GX_NOMASK, false, this,prmT000V8,0,0,true,false )
             ,new CursorDef("T000V9", "SELECT TM1.[Servico_Codigo], TM1.[Servico_Nome], TM1.[Servico_Descricao], TM1.[Servico_Sigla], T2.[ServicoGrupo_Descricao], TM1.[Servico_UORespExclusiva], T3.[Servico_Descricao] AS Servico_VincDesc, T3.[Servico_Sigla] AS Servico_VincSigla, TM1.[Servico_Terceriza], TM1.[Servico_Tela], TM1.[Servico_Atende], TM1.[Servico_ObrigaValores], TM1.[Servico_ObjetoControle], TM1.[Servico_TipoHierarquia], TM1.[Servico_PercTmp], TM1.[Servico_PercPgm], TM1.[Servico_PercCnc], TM1.[Servico_Anterior], TM1.[Servico_Posterior], TM1.[Servico_Ativo], TM1.[Servico_IsPublico], T4.[LinhaNegocio_Descricao] AS Servico_LinNegDsc, TM1.[Servico_IsOrigemReferencia], TM1.[Servico_PausaSLA], TM1.[ServicoGrupo_Codigo], TM1.[Servico_UO] AS Servico_UO, TM1.[Servico_LinNegCod] AS Servico_LinNegCod, TM1.[Servico_Vinculado] AS Servico_Vinculado FROM ((([Servico] TM1 WITH (NOLOCK) INNER JOIN [ServicoGrupo] T2 WITH (NOLOCK) ON T2.[ServicoGrupo_Codigo] = TM1.[ServicoGrupo_Codigo]) LEFT JOIN [Servico] T3 WITH (NOLOCK) ON T3.[Servico_Codigo] = TM1.[Servico_Vinculado]) LEFT JOIN [LinhaNegocio] T4 WITH (NOLOCK) ON T4.[LinhadeNegocio_Codigo] = TM1.[Servico_LinNegCod]) WHERE TM1.[Servico_Codigo] = @Servico_Codigo ORDER BY TM1.[Servico_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT000V9,100,0,true,false )
             ,new CursorDef("T000V10", "SELECT [ServicoGrupo_Descricao] FROM [ServicoGrupo] WITH (NOLOCK) WHERE [ServicoGrupo_Codigo] = @ServicoGrupo_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000V10,1,0,true,false )
             ,new CursorDef("T000V11", "SELECT [Servico_Descricao] AS Servico_VincDesc, [Servico_Sigla] AS Servico_VincSigla FROM [Servico] WITH (NOLOCK) WHERE [Servico_Codigo] = @Servico_Vinculado ",true, GxErrorMask.GX_NOMASK, false, this,prmT000V11,1,0,true,false )
             ,new CursorDef("T000V12", "SELECT [LinhaNegocio_Descricao] AS Servico_LinNegDsc FROM [LinhaNegocio] WITH (NOLOCK) WHERE [LinhadeNegocio_Codigo] = @Servico_LinNegCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT000V12,1,0,true,false )
             ,new CursorDef("T000V13", "SELECT [UnidadeOrganizacional_Codigo] AS Servico_UO FROM [Geral_UnidadeOrganizacional] WITH (NOLOCK) WHERE [UnidadeOrganizacional_Codigo] = @Servico_UO ",true, GxErrorMask.GX_NOMASK, false, this,prmT000V13,1,0,true,false )
             ,new CursorDef("T000V14", "SELECT [Servico_Codigo] FROM [Servico] WITH (NOLOCK) WHERE [Servico_Codigo] = @Servico_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000V14,1,0,true,false )
             ,new CursorDef("T000V15", "SELECT TOP 1 [Servico_Codigo] FROM [Servico] WITH (NOLOCK) WHERE ( [Servico_Codigo] > @Servico_Codigo) ORDER BY [Servico_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000V15,1,0,true,true )
             ,new CursorDef("T000V16", "SELECT TOP 1 [Servico_Codigo] FROM [Servico] WITH (NOLOCK) WHERE ( [Servico_Codigo] < @Servico_Codigo) ORDER BY [Servico_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000V16,1,0,true,true )
             ,new CursorDef("T000V17", "INSERT INTO [Servico]([Servico_Nome], [Servico_Descricao], [Servico_Sigla], [Servico_UORespExclusiva], [Servico_Terceriza], [Servico_Tela], [Servico_Atende], [Servico_ObrigaValores], [Servico_ObjetoControle], [Servico_TipoHierarquia], [Servico_PercTmp], [Servico_PercPgm], [Servico_PercCnc], [Servico_Anterior], [Servico_Posterior], [Servico_Ativo], [Servico_IsPublico], [Servico_IsOrigemReferencia], [Servico_PausaSLA], [ServicoGrupo_Codigo], [Servico_UO], [Servico_LinNegCod], [Servico_Vinculado]) VALUES(@Servico_Nome, @Servico_Descricao, @Servico_Sigla, @Servico_UORespExclusiva, @Servico_Terceriza, @Servico_Tela, @Servico_Atende, @Servico_ObrigaValores, @Servico_ObjetoControle, @Servico_TipoHierarquia, @Servico_PercTmp, @Servico_PercPgm, @Servico_PercCnc, @Servico_Anterior, @Servico_Posterior, @Servico_Ativo, @Servico_IsPublico, @Servico_IsOrigemReferencia, @Servico_PausaSLA, @ServicoGrupo_Codigo, @Servico_UO, @Servico_LinNegCod, @Servico_Vinculado); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT000V17)
             ,new CursorDef("T000V18", "UPDATE [Servico] SET [Servico_Nome]=@Servico_Nome, [Servico_Descricao]=@Servico_Descricao, [Servico_Sigla]=@Servico_Sigla, [Servico_UORespExclusiva]=@Servico_UORespExclusiva, [Servico_Terceriza]=@Servico_Terceriza, [Servico_Tela]=@Servico_Tela, [Servico_Atende]=@Servico_Atende, [Servico_ObrigaValores]=@Servico_ObrigaValores, [Servico_ObjetoControle]=@Servico_ObjetoControle, [Servico_TipoHierarquia]=@Servico_TipoHierarquia, [Servico_PercTmp]=@Servico_PercTmp, [Servico_PercPgm]=@Servico_PercPgm, [Servico_PercCnc]=@Servico_PercCnc, [Servico_Anterior]=@Servico_Anterior, [Servico_Posterior]=@Servico_Posterior, [Servico_Ativo]=@Servico_Ativo, [Servico_IsPublico]=@Servico_IsPublico, [Servico_IsOrigemReferencia]=@Servico_IsOrigemReferencia, [Servico_PausaSLA]=@Servico_PausaSLA, [ServicoGrupo_Codigo]=@ServicoGrupo_Codigo, [Servico_UO]=@Servico_UO, [Servico_LinNegCod]=@Servico_LinNegCod, [Servico_Vinculado]=@Servico_Vinculado  WHERE [Servico_Codigo] = @Servico_Codigo", GxErrorMask.GX_NOMASK,prmT000V18)
             ,new CursorDef("T000V19", "DELETE FROM [Servico]  WHERE [Servico_Codigo] = @Servico_Codigo", GxErrorMask.GX_NOMASK,prmT000V19)
             ,new CursorDef("T000V20", "SELECT [ServicoGrupo_Descricao] FROM [ServicoGrupo] WITH (NOLOCK) WHERE [ServicoGrupo_Codigo] = @ServicoGrupo_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000V20,1,0,true,false )
             ,new CursorDef("T000V21", "SELECT [Servico_Descricao] AS Servico_VincDesc, [Servico_Sigla] AS Servico_VincSigla FROM [Servico] WITH (NOLOCK) WHERE [Servico_Codigo] = @Servico_Vinculado ",true, GxErrorMask.GX_NOMASK, false, this,prmT000V21,1,0,true,false )
             ,new CursorDef("T000V22", "SELECT [LinhaNegocio_Descricao] AS Servico_LinNegDsc FROM [LinhaNegocio] WITH (NOLOCK) WHERE [LinhadeNegocio_Codigo] = @Servico_LinNegCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT000V22,1,0,true,false )
             ,new CursorDef("T000V23", "SELECT TOP 1 [Servico_Codigo] AS Servico_Vinculado FROM [Servico] WITH (NOLOCK) WHERE [Servico_Vinculado] = @Servico_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000V23,1,0,true,true )
             ,new CursorDef("T000V24", "SELECT TOP 1 [ServicoFluxo_Codigo] FROM [ServicoFluxo] WITH (NOLOCK) WHERE [ServicoFluxo_ServicoPos] = @Servico_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000V24,1,0,true,true )
             ,new CursorDef("T000V25", "SELECT TOP 1 [ServicoFluxo_Codigo] FROM [ServicoFluxo] WITH (NOLOCK) WHERE [ServicoFluxo_ServicoCod] = @Servico_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000V25,1,0,true,true )
             ,new CursorDef("T000V26", "SELECT TOP 1 [ServicoPrioridade_Codigo] FROM [ServicoPrioridade] WITH (NOLOCK) WHERE [ServicoPrioridade_SrvCod] = @Servico_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000V26,1,0,true,true )
             ,new CursorDef("T000V27", "SELECT TOP 1 [ContratoServicos_Codigo], [ContratoServicosSistemas_ServicoCod], [ContratoServicosSistemas_SistemaCod] FROM [ContratoServicosSistemas] WITH (NOLOCK) WHERE [ContratoServicosSistemas_ServicoCod] = @Servico_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000V27,1,0,true,true )
             ,new CursorDef("T000V28", "SELECT TOP 1 [AreaTrabalho_Codigo] FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_ServicoPadrao] = @Servico_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000V28,1,0,true,true )
             ,new CursorDef("T000V29", "SELECT TOP 1 [UsuarioServicos_UsuarioCod], [UsuarioServicos_ServicoCod] FROM [UsuarioServicos] WITH (NOLOCK) WHERE [UsuarioServicos_ServicoCod] = @Servico_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000V29,1,0,true,true )
             ,new CursorDef("T000V30", "SELECT TOP 1 [ContagemResultado_Codigo] FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_ServicoSS] = @Servico_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000V30,1,0,true,true )
             ,new CursorDef("T000V31", "SELECT TOP 1 [Solicitacoes_Codigo] FROM [Solicitacoes] WITH (NOLOCK) WHERE [Servico_Codigo] = @Servico_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000V31,1,0,true,true )
             ,new CursorDef("T000V32", "SELECT TOP 1 [ServicoAns_Codigo] FROM [ServicoAns] WITH (NOLOCK) WHERE [Servico_Codigo] = @Servico_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000V32,1,0,true,true )
             ,new CursorDef("T000V33", "SELECT TOP 1 [ContratoServicos_Codigo] FROM [ContratoServicos] WITH (NOLOCK) WHERE [Servico_Codigo] = @Servico_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000V33,1,0,true,true )
             ,new CursorDef("T000V34", "SELECT TOP 1 [Servico_Codigo], [Check_Codigo] FROM [ServicoCheck] WITH (NOLOCK) WHERE [Servico_Codigo] = @Servico_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000V34,1,0,true,true )
             ,new CursorDef("T000V35", "SELECT TOP 1 [Servico_Codigo], [ServicoArtefato_ArtefatoCod] FROM [ServicoArtefatos] WITH (NOLOCK) WHERE [Servico_Codigo] = @Servico_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000V35,1,0,true,true )
             ,new CursorDef("T000V36", "SELECT [Servico_Codigo] FROM [Servico] WITH (NOLOCK) ORDER BY [Servico_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT000V36,100,0,true,false )
             ,new CursorDef("T000V37", "SELECT [ServicoGrupo_Codigo], [ServicoGrupo_Descricao] FROM [ServicoGrupo] WITH (NOLOCK) ORDER BY [ServicoGrupo_Descricao] ",true, GxErrorMask.GX_NOMASK, false, this,prmT000V37,0,0,true,false )
             ,new CursorDef("T000V38", "SELECT [UnidadeOrganizacional_Codigo], [UnidadeOrganizacional_Nome] FROM [Geral_UnidadeOrganizacional] WITH (NOLOCK) ORDER BY [UnidadeOrganizacional_Nome] ",true, GxErrorMask.GX_NOMASK, false, this,prmT000V38,0,0,true,false )
             ,new CursorDef("T000V39", "SELECT [Servico_Codigo], [Servico_Nome] FROM [Servico] WITH (NOLOCK) WHERE ((@AV16ServicoGrupo_Codigo = convert(int, 0)) or @ServicoGrupo_Codigo = @AV16ServicoGrupo_Codigo) AND ([Servico_Codigo] <> @AV7Servico_Codigo) AND ((@Servico_Vinculados = convert(int, 0))) AND (@Servico_Ativo = 1) ORDER BY [Servico_Nome] ",true, GxErrorMask.GX_NOMASK, false, this,prmT000V39,0,0,true,false )
             ,new CursorDef("T000V40", "SELECT [Servico_Codigo], [Servico_Sigla] FROM [Servico] WITH (NOLOCK) WHERE ([Servico_Codigo] <> @AV7Servico_Codigo) AND (@Servico_Ativo = 1) AND ([Servico_TipoHierarquia] = 1) ORDER BY [Servico_Sigla] ",true, GxErrorMask.GX_NOMASK, false, this,prmT000V40,0,0,true,false )
             ,new CursorDef("T000V41", "SELECT [Servico_Codigo], [Servico_Sigla] FROM [Servico] WITH (NOLOCK) WHERE ([Servico_Codigo] <> @AV7Servico_Codigo) AND (@Servico_Ativo = 1) AND ([Servico_TipoHierarquia] = 1) ORDER BY [Servico_Sigla] ",true, GxErrorMask.GX_NOMASK, false, this,prmT000V41,0,0,true,false )
             ,new CursorDef("T000V43", "SELECT T3.[Pessoa_Codigo] AS ContratanteUsuario_UsuarioPessoaCod, T1.[ContratanteUsuario_ContratanteCod], T1.[ContratanteUsuario_UsuarioCod] AS ContratanteUsuario_UsuarioCod, T3.[Pessoa_Nome] AS ContratanteUsuario_UsuarioPessoaNom, COALESCE( T4.[ContratanteUsuario_AreaTrabalhoCod], 0) AS ContratanteUsuario_AreaTrabalhoCod FROM ((([ContratanteUsuario] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[ContratanteUsuario_UsuarioCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod]) LEFT JOIN (SELECT MIN(T5.[AreaTrabalho_Codigo]) AS ContratanteUsuario_AreaTrabalhoCod, T6.[ContratanteUsuario_ContratanteCod], T6.[ContratanteUsuario_UsuarioCod] AS ContratanteUsuario_UsuarioCod FROM [AreaTrabalho] T5 WITH (NOLOCK),  [ContratanteUsuario] T6 WITH (NOLOCK) WHERE T5.[Contratante_Codigo] = T6.[ContratanteUsuario_ContratanteCod] GROUP BY T6.[ContratanteUsuario_ContratanteCod], T6.[ContratanteUsuario_UsuarioCod] ) T4 ON T4.[ContratanteUsuario_ContratanteCod] = T1.[ContratanteUsuario_ContratanteCod] AND T4.[ContratanteUsuario_UsuarioCod] = T1.[ContratanteUsuario_UsuarioCod]) WHERE COALESCE( T4.[ContratanteUsuario_AreaTrabalhoCod], 0) = @AV8WWPCo_1Areatrabalho_codigo ORDER BY T3.[Pessoa_Nome] ",true, GxErrorMask.GX_NOMASK, false, this,prmT000V43,0,0,true,false )
             ,new CursorDef("T000V44", "SELECT [LinhadeNegocio_Codigo] AS Servico_LinNegCod, [LinhaNegocio_Identificador] FROM [LinhaNegocio] WITH (NOLOCK) ORDER BY [LinhaNegocio_Identificador] ",true, GxErrorMask.GX_NOMASK, false, this,prmT000V44,0,0,true,false )
             ,new CursorDef("T000V45", "SELECT [ServicoGrupo_Descricao] FROM [ServicoGrupo] WITH (NOLOCK) WHERE [ServicoGrupo_Codigo] = @ServicoGrupo_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT000V45,1,0,true,false )
             ,new CursorDef("T000V46", "SELECT [Servico_Descricao] AS Servico_VincDesc, [Servico_Sigla] AS Servico_VincSigla FROM [Servico] WITH (NOLOCK) WHERE [Servico_Codigo] = @Servico_Vinculado ",true, GxErrorMask.GX_NOMASK, false, this,prmT000V46,1,0,true,false )
             ,new CursorDef("T000V47", "SELECT [LinhaNegocio_Descricao] AS Servico_LinNegDsc FROM [LinhaNegocio] WITH (NOLOCK) WHERE [LinhadeNegocio_Codigo] = @Servico_LinNegCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT000V47,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getString(4, 15) ;
                ((bool[]) buf[5])[0] = rslt.getBool(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((bool[]) buf[7])[0] = rslt.getBool(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((String[]) buf[9])[0] = rslt.getString(7, 3) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((String[]) buf[11])[0] = rslt.getString(8, 1) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((String[]) buf[13])[0] = rslt.getString(9, 1) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                ((String[]) buf[15])[0] = rslt.getString(10, 3) ;
                ((short[]) buf[16])[0] = rslt.getShort(11) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(11);
                ((short[]) buf[18])[0] = rslt.getShort(12) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(12);
                ((short[]) buf[20])[0] = rslt.getShort(13) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(13);
                ((short[]) buf[22])[0] = rslt.getShort(14) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(14);
                ((int[]) buf[24])[0] = rslt.getInt(15) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(15);
                ((int[]) buf[26])[0] = rslt.getInt(16) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(16);
                ((bool[]) buf[28])[0] = rslt.getBool(17) ;
                ((bool[]) buf[29])[0] = rslt.getBool(18) ;
                ((bool[]) buf[30])[0] = rslt.getBool(19) ;
                ((bool[]) buf[31])[0] = rslt.getBool(20) ;
                ((int[]) buf[32])[0] = rslt.getInt(21) ;
                ((int[]) buf[33])[0] = rslt.getInt(22) ;
                ((bool[]) buf[34])[0] = rslt.wasNull(22);
                ((int[]) buf[35])[0] = rslt.getInt(23) ;
                ((bool[]) buf[36])[0] = rslt.wasNull(23);
                ((int[]) buf[37])[0] = rslt.getInt(24) ;
                ((bool[]) buf[38])[0] = rslt.wasNull(24);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getString(4, 15) ;
                ((bool[]) buf[5])[0] = rslt.getBool(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((bool[]) buf[7])[0] = rslt.getBool(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((String[]) buf[9])[0] = rslt.getString(7, 3) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((String[]) buf[11])[0] = rslt.getString(8, 1) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((String[]) buf[13])[0] = rslt.getString(9, 1) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                ((String[]) buf[15])[0] = rslt.getString(10, 3) ;
                ((short[]) buf[16])[0] = rslt.getShort(11) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(11);
                ((short[]) buf[18])[0] = rslt.getShort(12) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(12);
                ((short[]) buf[20])[0] = rslt.getShort(13) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(13);
                ((short[]) buf[22])[0] = rslt.getShort(14) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(14);
                ((int[]) buf[24])[0] = rslt.getInt(15) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(15);
                ((int[]) buf[26])[0] = rslt.getInt(16) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(16);
                ((bool[]) buf[28])[0] = rslt.getBool(17) ;
                ((bool[]) buf[29])[0] = rslt.getBool(18) ;
                ((bool[]) buf[30])[0] = rslt.getBool(19) ;
                ((bool[]) buf[31])[0] = rslt.getBool(20) ;
                ((int[]) buf[32])[0] = rslt.getInt(21) ;
                ((int[]) buf[33])[0] = rslt.getInt(22) ;
                ((bool[]) buf[34])[0] = rslt.wasNull(22);
                ((int[]) buf[35])[0] = rslt.getInt(23) ;
                ((bool[]) buf[36])[0] = rslt.wasNull(23);
                ((int[]) buf[37])[0] = rslt.getInt(24) ;
                ((bool[]) buf[38])[0] = rslt.wasNull(24);
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 5 :
                ((String[]) buf[0])[0] = rslt.getLongVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 15) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getString(4, 15) ;
                ((String[]) buf[5])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[6])[0] = rslt.getBool(6) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(6);
                ((String[]) buf[8])[0] = rslt.getLongVarchar(7) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(7);
                ((String[]) buf[10])[0] = rslt.getString(8, 15) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(8);
                ((bool[]) buf[12])[0] = rslt.getBool(9) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(9);
                ((String[]) buf[14])[0] = rslt.getString(10, 3) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(10);
                ((String[]) buf[16])[0] = rslt.getString(11, 1) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(11);
                ((String[]) buf[18])[0] = rslt.getString(12, 1) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(12);
                ((String[]) buf[20])[0] = rslt.getString(13, 3) ;
                ((short[]) buf[21])[0] = rslt.getShort(14) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(14);
                ((short[]) buf[23])[0] = rslt.getShort(15) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(15);
                ((short[]) buf[25])[0] = rslt.getShort(16) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(16);
                ((short[]) buf[27])[0] = rslt.getShort(17) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(17);
                ((int[]) buf[29])[0] = rslt.getInt(18) ;
                ((bool[]) buf[30])[0] = rslt.wasNull(18);
                ((int[]) buf[31])[0] = rslt.getInt(19) ;
                ((bool[]) buf[32])[0] = rslt.wasNull(19);
                ((bool[]) buf[33])[0] = rslt.getBool(20) ;
                ((bool[]) buf[34])[0] = rslt.getBool(21) ;
                ((String[]) buf[35])[0] = rslt.getVarchar(22) ;
                ((bool[]) buf[36])[0] = rslt.wasNull(22);
                ((bool[]) buf[37])[0] = rslt.getBool(23) ;
                ((bool[]) buf[38])[0] = rslt.getBool(24) ;
                ((int[]) buf[39])[0] = rslt.getInt(25) ;
                ((int[]) buf[40])[0] = rslt.getInt(26) ;
                ((bool[]) buf[41])[0] = rslt.wasNull(26);
                ((int[]) buf[42])[0] = rslt.getInt(27) ;
                ((bool[]) buf[43])[0] = rslt.wasNull(27);
                ((int[]) buf[44])[0] = rslt.getInt(28) ;
                ((bool[]) buf[45])[0] = rslt.wasNull(28);
                return;
             case 8 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                return;
             case 9 :
                ((String[]) buf[0])[0] = rslt.getLongVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 15) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 10 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 13 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 14 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 15 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 18 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                return;
             case 19 :
                ((String[]) buf[0])[0] = rslt.getLongVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 15) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 20 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 21 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 22 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 23 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 24 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 25 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 26 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 27 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 28 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 29 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
       getresults30( cursor, rslt, buf) ;
    }

    public void getresults30( int cursor ,
                              IFieldGetter rslt ,
                              Object[] buf )
    {
       switch ( cursor )
       {
             case 30 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 31 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 32 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 33 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 34 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 35 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                return;
             case 36 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                return;
             case 37 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                return;
             case 38 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 15) ;
                return;
             case 39 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 15) ;
                return;
             case 40 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((String[]) buf[4])[0] = rslt.getString(4, 100) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                return;
             case 41 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
             case 42 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                return;
             case 43 :
                ((String[]) buf[0])[0] = rslt.getLongVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 15) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 44 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 4 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 5 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 10 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 11 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 14 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 15 :
                stmt.SetParameter(1, (String)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[2]);
                }
                stmt.SetParameter(3, (String)parms[3]);
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 4 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(4, (bool)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 5 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(5, (bool)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 6 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(6, (String)parms[9]);
                }
                if ( (bool)parms[10] )
                {
                   stmt.setNull( 7 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(7, (String)parms[11]);
                }
                if ( (bool)parms[12] )
                {
                   stmt.setNull( 8 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(8, (String)parms[13]);
                }
                stmt.SetParameter(9, (String)parms[14]);
                if ( (bool)parms[15] )
                {
                   stmt.setNull( 10 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(10, (short)parms[16]);
                }
                if ( (bool)parms[17] )
                {
                   stmt.setNull( 11 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(11, (short)parms[18]);
                }
                if ( (bool)parms[19] )
                {
                   stmt.setNull( 12 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(12, (short)parms[20]);
                }
                if ( (bool)parms[21] )
                {
                   stmt.setNull( 13 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(13, (short)parms[22]);
                }
                if ( (bool)parms[23] )
                {
                   stmt.setNull( 14 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(14, (int)parms[24]);
                }
                if ( (bool)parms[25] )
                {
                   stmt.setNull( 15 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(15, (int)parms[26]);
                }
                stmt.SetParameter(16, (bool)parms[27]);
                stmt.SetParameter(17, (bool)parms[28]);
                stmt.SetParameter(18, (bool)parms[29]);
                stmt.SetParameter(19, (bool)parms[30]);
                stmt.SetParameter(20, (int)parms[31]);
                if ( (bool)parms[32] )
                {
                   stmt.setNull( 21 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(21, (int)parms[33]);
                }
                if ( (bool)parms[34] )
                {
                   stmt.setNull( 22 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(22, (int)parms[35]);
                }
                if ( (bool)parms[36] )
                {
                   stmt.setNull( 23 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(23, (int)parms[37]);
                }
                return;
             case 16 :
                stmt.SetParameter(1, (String)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[2]);
                }
                stmt.SetParameter(3, (String)parms[3]);
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 4 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(4, (bool)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 5 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(5, (bool)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 6 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(6, (String)parms[9]);
                }
                if ( (bool)parms[10] )
                {
                   stmt.setNull( 7 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(7, (String)parms[11]);
                }
                if ( (bool)parms[12] )
                {
                   stmt.setNull( 8 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(8, (String)parms[13]);
                }
                stmt.SetParameter(9, (String)parms[14]);
                if ( (bool)parms[15] )
                {
                   stmt.setNull( 10 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(10, (short)parms[16]);
                }
                if ( (bool)parms[17] )
                {
                   stmt.setNull( 11 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(11, (short)parms[18]);
                }
                if ( (bool)parms[19] )
                {
                   stmt.setNull( 12 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(12, (short)parms[20]);
                }
                if ( (bool)parms[21] )
                {
                   stmt.setNull( 13 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(13, (short)parms[22]);
                }
                if ( (bool)parms[23] )
                {
                   stmt.setNull( 14 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(14, (int)parms[24]);
                }
                if ( (bool)parms[25] )
                {
                   stmt.setNull( 15 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(15, (int)parms[26]);
                }
                stmt.SetParameter(16, (bool)parms[27]);
                stmt.SetParameter(17, (bool)parms[28]);
                stmt.SetParameter(18, (bool)parms[29]);
                stmt.SetParameter(19, (bool)parms[30]);
                stmt.SetParameter(20, (int)parms[31]);
                if ( (bool)parms[32] )
                {
                   stmt.setNull( 21 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(21, (int)parms[33]);
                }
                if ( (bool)parms[34] )
                {
                   stmt.setNull( 22 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(22, (int)parms[35]);
                }
                if ( (bool)parms[36] )
                {
                   stmt.setNull( 23 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(23, (int)parms[37]);
                }
                stmt.SetParameter(24, (int)parms[38]);
                return;
             case 17 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 18 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 19 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 20 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 21 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 22 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 23 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 24 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 25 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 26 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 27 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 28 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 29 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
       setparameters30( cursor, stmt, parms) ;
    }

    public void setparameters30( int cursor ,
                                 IFieldSetter stmt ,
                                 Object[] parms )
    {
       switch ( cursor )
       {
             case 30 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 31 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 32 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 33 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 37 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                stmt.SetParameter(4, (short)parms[3]);
                stmt.SetParameter(5, (bool)parms[4]);
                return;
             case 38 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (bool)parms[1]);
                return;
             case 39 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (bool)parms[1]);
                return;
             case 40 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 42 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 43 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 44 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
       }
    }

 }

}
