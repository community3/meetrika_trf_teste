/*
               File: type_SdtParametrosSistema
        Description: Parametros do Sistema
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 23:0:50.28
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "ParametrosSistema" )]
   [XmlType(TypeName =  "ParametrosSistema" , Namespace = "GxEv3Up14_MeetrikaVs3" )]
   [Serializable]
   public class SdtParametrosSistema : GxSilentTrnSdt, System.Web.SessionState.IRequiresSessionState
   {
      public SdtParametrosSistema( )
      {
         /* Constructor for serialization */
         gxTv_SdtParametrosSistema_Parametrossistema_nomesistema = "";
         gxTv_SdtParametrosSistema_Parametrossistema_textohome = "";
         gxTv_SdtParametrosSistema_Parametrossistema_emailsdahost = "";
         gxTv_SdtParametrosSistema_Parametrossistema_emailsdauser = "";
         gxTv_SdtParametrosSistema_Parametrossistema_emailsdapass = "";
         gxTv_SdtParametrosSistema_Parametrossistema_emailsdakey = "";
         gxTv_SdtParametrosSistema_Parametrossistema_pathcrtf = "";
         gxTv_SdtParametrosSistema_Parametrossistema_hostmensuracao = "";
         gxTv_SdtParametrosSistema_Parametrossistema_sqlcomputername = "";
         gxTv_SdtParametrosSistema_Parametrossistema_sqlservername = "";
         gxTv_SdtParametrosSistema_Parametrossistema_sqledition = "";
         gxTv_SdtParametrosSistema_Parametrossistema_sqlinstance = "";
         gxTv_SdtParametrosSistema_Parametrossistema_dbname = "";
         gxTv_SdtParametrosSistema_Parametrossistema_flsevd = "";
         gxTv_SdtParametrosSistema_Parametrossistema_urlapp = "";
         gxTv_SdtParametrosSistema_Parametrossistema_urlotherver = "";
         gxTv_SdtParametrosSistema_Parametrossistema_validacao = "";
         gxTv_SdtParametrosSistema_Parametrossistema_imagem = "";
         gxTv_SdtParametrosSistema_Parametrossistema_imagem_gxi = "";
         gxTv_SdtParametrosSistema_Mode = "";
         gxTv_SdtParametrosSistema_Parametrossistema_nomesistema_Z = "";
         gxTv_SdtParametrosSistema_Parametrossistema_emailsdahost_Z = "";
         gxTv_SdtParametrosSistema_Parametrossistema_emailsdauser_Z = "";
         gxTv_SdtParametrosSistema_Parametrossistema_emailsdapass_Z = "";
         gxTv_SdtParametrosSistema_Parametrossistema_emailsdakey_Z = "";
         gxTv_SdtParametrosSistema_Parametrossistema_pathcrtf_Z = "";
         gxTv_SdtParametrosSistema_Parametrossistema_hostmensuracao_Z = "";
         gxTv_SdtParametrosSistema_Parametrossistema_sqlcomputername_Z = "";
         gxTv_SdtParametrosSistema_Parametrossistema_sqlservername_Z = "";
         gxTv_SdtParametrosSistema_Parametrossistema_sqledition_Z = "";
         gxTv_SdtParametrosSistema_Parametrossistema_sqlinstance_Z = "";
         gxTv_SdtParametrosSistema_Parametrossistema_dbname_Z = "";
         gxTv_SdtParametrosSistema_Parametrossistema_flsevd_Z = "";
         gxTv_SdtParametrosSistema_Parametrossistema_urlapp_Z = "";
         gxTv_SdtParametrosSistema_Parametrossistema_urlotherver_Z = "";
         gxTv_SdtParametrosSistema_Parametrossistema_validacao_Z = "";
         gxTv_SdtParametrosSistema_Parametrossistema_imagem_gxi_Z = "";
      }

      public SdtParametrosSistema( IGxContext context )
      {
         this.context = context;
         constructorCallingAssembly = Assembly.GetCallingAssembly();
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public void Load( int AV330ParametrosSistema_Codigo )
      {
         IGxSilentTrn obj ;
         obj = getTransaction();
         obj.LoadKey(new Object[] {(int)AV330ParametrosSistema_Codigo});
         return  ;
      }

      public override Object[][] GetBCKey( )
      {
         return (Object[][])(new Object[][]{new Object[]{"ParametrosSistema_Codigo", typeof(int)}}) ;
      }

      public override IGxCollection GetMessages( )
      {
         short item = 1 ;
         IGxCollection msgs = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs") ;
         SdtMessages_Message m1 ;
         IGxSilentTrn trn = getTransaction() ;
         msglist msgList = trn.GetMessages() ;
         while ( item <= msgList.ItemCount )
         {
            m1 = new SdtMessages_Message(context);
            m1.gxTpr_Id = msgList.getItemValue(item);
            m1.gxTpr_Description = msgList.getItemText(item);
            m1.gxTpr_Type = msgList.getItemType(item);
            msgs.Add(m1, 0);
            item = (short)(item+1);
         }
         return msgs ;
      }

      public override GXProperties GetMetadata( )
      {
         GXProperties metadata = new GXProperties() ;
         metadata.Set("Name", "ParametrosSistema");
         metadata.Set("BT", "ParametrosSistema");
         metadata.Set("PK", "[ \"ParametrosSistema_Codigo\" ]");
         metadata.Set("AllowInsert", "True");
         metadata.Set("AllowUpdate", "True");
         metadata.Set("AllowDelete", "True");
         return metadata ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         if ( ! includeState )
         {
            XmlAttributeOverrides ov = new XmlAttributeOverrides();
            XmlAttributes attrs = new XmlAttributes();
            attrs.XmlIgnore = true;
            ov.Add(this.GetType(),  "gxTpr_Parametrossistema_imagem_gxi" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Mode" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Initialized" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Parametrossistema_codigo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Parametrossistema_nomesistema_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Parametrossistema_appid_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Parametrossistema_licensiadocadastrado_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Parametrossistema_padronizarstrings_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Parametrossistema_emailsdahost_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Parametrossistema_emailsdauser_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Parametrossistema_emailsdapass_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Parametrossistema_emailsdakey_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Parametrossistema_emailsdaaut_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Parametrossistema_emailsdaport_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Parametrossistema_emailsdasec_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Parametrossistema_fatorajuste_Z_double" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Parametrossistema_pathcrtf_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Parametrossistema_hostmensuracao_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Parametrossistema_sqlcomputername_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Parametrossistema_sqlservername_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Parametrossistema_sqledition_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Parametrossistema_sqlinstance_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Parametrossistema_dbname_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Parametrossistema_flsevd_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Parametrossistema_urlapp_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Parametrossistema_urlotherver_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Parametrossistema_validacao_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Parametrossistema_imagem_gxi_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Parametrossistema_licensiadocadastrado_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Parametrossistema_emailsdahost_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Parametrossistema_emailsdauser_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Parametrossistema_emailsdapass_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Parametrossistema_emailsdakey_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Parametrossistema_emailsdaaut_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Parametrossistema_emailsdaport_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Parametrossistema_emailsdasec_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Parametrossistema_fatorajuste_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Parametrossistema_pathcrtf_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Parametrossistema_hostmensuracao_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Parametrossistema_sqlcomputername_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Parametrossistema_sqlservername_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Parametrossistema_sqledition_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Parametrossistema_sqlinstance_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Parametrossistema_dbname_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Parametrossistema_flsevd_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Parametrossistema_urlapp_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Parametrossistema_urlotherver_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Parametrossistema_validacao_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Parametrossistema_imagem_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Parametrossistema_imagem_gxi_N" , attrs);
            xmls = new XmlSerializer(this.GetType(), ov, new Type[0], null, sNameSpace);
         }
         else
         {
            xmls = new XmlSerializer(this.GetType(), sNameSpace);
         }
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtParametrosSistema deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_MeetrikaVs3" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtParametrosSistema)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtParametrosSistema obj ;
         obj = this;
         obj.gxTpr_Parametrossistema_codigo = deserialized.gxTpr_Parametrossistema_codigo;
         obj.gxTpr_Parametrossistema_nomesistema = deserialized.gxTpr_Parametrossistema_nomesistema;
         obj.gxTpr_Parametrossistema_appid = deserialized.gxTpr_Parametrossistema_appid;
         obj.gxTpr_Parametrossistema_textohome = deserialized.gxTpr_Parametrossistema_textohome;
         obj.gxTpr_Parametrossistema_licensiadocadastrado = deserialized.gxTpr_Parametrossistema_licensiadocadastrado;
         obj.gxTpr_Parametrossistema_padronizarstrings = deserialized.gxTpr_Parametrossistema_padronizarstrings;
         obj.gxTpr_Parametrossistema_emailsdahost = deserialized.gxTpr_Parametrossistema_emailsdahost;
         obj.gxTpr_Parametrossistema_emailsdauser = deserialized.gxTpr_Parametrossistema_emailsdauser;
         obj.gxTpr_Parametrossistema_emailsdapass = deserialized.gxTpr_Parametrossistema_emailsdapass;
         obj.gxTpr_Parametrossistema_emailsdakey = deserialized.gxTpr_Parametrossistema_emailsdakey;
         obj.gxTpr_Parametrossistema_emailsdaaut = deserialized.gxTpr_Parametrossistema_emailsdaaut;
         obj.gxTpr_Parametrossistema_emailsdaport = deserialized.gxTpr_Parametrossistema_emailsdaport;
         obj.gxTpr_Parametrossistema_emailsdasec = deserialized.gxTpr_Parametrossistema_emailsdasec;
         obj.gxTpr_Parametrossistema_fatorajuste = deserialized.gxTpr_Parametrossistema_fatorajuste;
         obj.gxTpr_Parametrossistema_pathcrtf = deserialized.gxTpr_Parametrossistema_pathcrtf;
         obj.gxTpr_Parametrossistema_hostmensuracao = deserialized.gxTpr_Parametrossistema_hostmensuracao;
         obj.gxTpr_Parametrossistema_sqlcomputername = deserialized.gxTpr_Parametrossistema_sqlcomputername;
         obj.gxTpr_Parametrossistema_sqlservername = deserialized.gxTpr_Parametrossistema_sqlservername;
         obj.gxTpr_Parametrossistema_sqledition = deserialized.gxTpr_Parametrossistema_sqledition;
         obj.gxTpr_Parametrossistema_sqlinstance = deserialized.gxTpr_Parametrossistema_sqlinstance;
         obj.gxTpr_Parametrossistema_dbname = deserialized.gxTpr_Parametrossistema_dbname;
         obj.gxTpr_Parametrossistema_flsevd = deserialized.gxTpr_Parametrossistema_flsevd;
         obj.gxTpr_Parametrossistema_urlapp = deserialized.gxTpr_Parametrossistema_urlapp;
         obj.gxTpr_Parametrossistema_urlotherver = deserialized.gxTpr_Parametrossistema_urlotherver;
         obj.gxTpr_Parametrossistema_validacao = deserialized.gxTpr_Parametrossistema_validacao;
         obj.gxTpr_Parametrossistema_imagem = deserialized.gxTpr_Parametrossistema_imagem;
         obj.gxTpr_Parametrossistema_imagem_gxi = deserialized.gxTpr_Parametrossistema_imagem_gxi;
         obj.gxTpr_Mode = deserialized.gxTpr_Mode;
         obj.gxTpr_Initialized = deserialized.gxTpr_Initialized;
         obj.gxTpr_Parametrossistema_codigo_Z = deserialized.gxTpr_Parametrossistema_codigo_Z;
         obj.gxTpr_Parametrossistema_nomesistema_Z = deserialized.gxTpr_Parametrossistema_nomesistema_Z;
         obj.gxTpr_Parametrossistema_appid_Z = deserialized.gxTpr_Parametrossistema_appid_Z;
         obj.gxTpr_Parametrossistema_licensiadocadastrado_Z = deserialized.gxTpr_Parametrossistema_licensiadocadastrado_Z;
         obj.gxTpr_Parametrossistema_padronizarstrings_Z = deserialized.gxTpr_Parametrossistema_padronizarstrings_Z;
         obj.gxTpr_Parametrossistema_emailsdahost_Z = deserialized.gxTpr_Parametrossistema_emailsdahost_Z;
         obj.gxTpr_Parametrossistema_emailsdauser_Z = deserialized.gxTpr_Parametrossistema_emailsdauser_Z;
         obj.gxTpr_Parametrossistema_emailsdapass_Z = deserialized.gxTpr_Parametrossistema_emailsdapass_Z;
         obj.gxTpr_Parametrossistema_emailsdakey_Z = deserialized.gxTpr_Parametrossistema_emailsdakey_Z;
         obj.gxTpr_Parametrossistema_emailsdaaut_Z = deserialized.gxTpr_Parametrossistema_emailsdaaut_Z;
         obj.gxTpr_Parametrossistema_emailsdaport_Z = deserialized.gxTpr_Parametrossistema_emailsdaport_Z;
         obj.gxTpr_Parametrossistema_emailsdasec_Z = deserialized.gxTpr_Parametrossistema_emailsdasec_Z;
         obj.gxTpr_Parametrossistema_fatorajuste_Z = deserialized.gxTpr_Parametrossistema_fatorajuste_Z;
         obj.gxTpr_Parametrossistema_pathcrtf_Z = deserialized.gxTpr_Parametrossistema_pathcrtf_Z;
         obj.gxTpr_Parametrossistema_hostmensuracao_Z = deserialized.gxTpr_Parametrossistema_hostmensuracao_Z;
         obj.gxTpr_Parametrossistema_sqlcomputername_Z = deserialized.gxTpr_Parametrossistema_sqlcomputername_Z;
         obj.gxTpr_Parametrossistema_sqlservername_Z = deserialized.gxTpr_Parametrossistema_sqlservername_Z;
         obj.gxTpr_Parametrossistema_sqledition_Z = deserialized.gxTpr_Parametrossistema_sqledition_Z;
         obj.gxTpr_Parametrossistema_sqlinstance_Z = deserialized.gxTpr_Parametrossistema_sqlinstance_Z;
         obj.gxTpr_Parametrossistema_dbname_Z = deserialized.gxTpr_Parametrossistema_dbname_Z;
         obj.gxTpr_Parametrossistema_flsevd_Z = deserialized.gxTpr_Parametrossistema_flsevd_Z;
         obj.gxTpr_Parametrossistema_urlapp_Z = deserialized.gxTpr_Parametrossistema_urlapp_Z;
         obj.gxTpr_Parametrossistema_urlotherver_Z = deserialized.gxTpr_Parametrossistema_urlotherver_Z;
         obj.gxTpr_Parametrossistema_validacao_Z = deserialized.gxTpr_Parametrossistema_validacao_Z;
         obj.gxTpr_Parametrossistema_imagem_gxi_Z = deserialized.gxTpr_Parametrossistema_imagem_gxi_Z;
         obj.gxTpr_Parametrossistema_licensiadocadastrado_N = deserialized.gxTpr_Parametrossistema_licensiadocadastrado_N;
         obj.gxTpr_Parametrossistema_emailsdahost_N = deserialized.gxTpr_Parametrossistema_emailsdahost_N;
         obj.gxTpr_Parametrossistema_emailsdauser_N = deserialized.gxTpr_Parametrossistema_emailsdauser_N;
         obj.gxTpr_Parametrossistema_emailsdapass_N = deserialized.gxTpr_Parametrossistema_emailsdapass_N;
         obj.gxTpr_Parametrossistema_emailsdakey_N = deserialized.gxTpr_Parametrossistema_emailsdakey_N;
         obj.gxTpr_Parametrossistema_emailsdaaut_N = deserialized.gxTpr_Parametrossistema_emailsdaaut_N;
         obj.gxTpr_Parametrossistema_emailsdaport_N = deserialized.gxTpr_Parametrossistema_emailsdaport_N;
         obj.gxTpr_Parametrossistema_emailsdasec_N = deserialized.gxTpr_Parametrossistema_emailsdasec_N;
         obj.gxTpr_Parametrossistema_fatorajuste_N = deserialized.gxTpr_Parametrossistema_fatorajuste_N;
         obj.gxTpr_Parametrossistema_pathcrtf_N = deserialized.gxTpr_Parametrossistema_pathcrtf_N;
         obj.gxTpr_Parametrossistema_hostmensuracao_N = deserialized.gxTpr_Parametrossistema_hostmensuracao_N;
         obj.gxTpr_Parametrossistema_sqlcomputername_N = deserialized.gxTpr_Parametrossistema_sqlcomputername_N;
         obj.gxTpr_Parametrossistema_sqlservername_N = deserialized.gxTpr_Parametrossistema_sqlservername_N;
         obj.gxTpr_Parametrossistema_sqledition_N = deserialized.gxTpr_Parametrossistema_sqledition_N;
         obj.gxTpr_Parametrossistema_sqlinstance_N = deserialized.gxTpr_Parametrossistema_sqlinstance_N;
         obj.gxTpr_Parametrossistema_dbname_N = deserialized.gxTpr_Parametrossistema_dbname_N;
         obj.gxTpr_Parametrossistema_flsevd_N = deserialized.gxTpr_Parametrossistema_flsevd_N;
         obj.gxTpr_Parametrossistema_urlapp_N = deserialized.gxTpr_Parametrossistema_urlapp_N;
         obj.gxTpr_Parametrossistema_urlotherver_N = deserialized.gxTpr_Parametrossistema_urlotherver_N;
         obj.gxTpr_Parametrossistema_validacao_N = deserialized.gxTpr_Parametrossistema_validacao_N;
         obj.gxTpr_Parametrossistema_imagem_N = deserialized.gxTpr_Parametrossistema_imagem_N;
         obj.gxTpr_Parametrossistema_imagem_gxi_N = deserialized.gxTpr_Parametrossistema_imagem_gxi_N;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "ParametrosSistema_Codigo") )
               {
                  gxTv_SdtParametrosSistema_Parametrossistema_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ParametrosSistema_NomeSistema") )
               {
                  gxTv_SdtParametrosSistema_Parametrossistema_nomesistema = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ParametrosSistema_AppID") )
               {
                  gxTv_SdtParametrosSistema_Parametrossistema_appid = (long)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ParametrosSistema_TextoHome") )
               {
                  gxTv_SdtParametrosSistema_Parametrossistema_textohome = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ParametrosSistema_LicensiadoCadastrado") )
               {
                  gxTv_SdtParametrosSistema_Parametrossistema_licensiadocadastrado = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ParametrosSistema_PadronizarStrings") )
               {
                  gxTv_SdtParametrosSistema_Parametrossistema_padronizarstrings = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ParametrosSistema_EmailSdaHost") )
               {
                  gxTv_SdtParametrosSistema_Parametrossistema_emailsdahost = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ParametrosSistema_EmailSdaUser") )
               {
                  gxTv_SdtParametrosSistema_Parametrossistema_emailsdauser = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ParametrosSistema_EmailSdaPass") )
               {
                  gxTv_SdtParametrosSistema_Parametrossistema_emailsdapass = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ParametrosSistema_EmailSdaKey") )
               {
                  gxTv_SdtParametrosSistema_Parametrossistema_emailsdakey = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ParametrosSistema_EmailSdaAut") )
               {
                  gxTv_SdtParametrosSistema_Parametrossistema_emailsdaaut = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ParametrosSistema_EmailSdaPort") )
               {
                  gxTv_SdtParametrosSistema_Parametrossistema_emailsdaport = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ParametrosSistema_EmailSdaSec") )
               {
                  gxTv_SdtParametrosSistema_Parametrossistema_emailsdasec = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ParametrosSistema_FatorAjuste") )
               {
                  gxTv_SdtParametrosSistema_Parametrossistema_fatorajuste = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ParametrosSistema_PathCrtf") )
               {
                  gxTv_SdtParametrosSistema_Parametrossistema_pathcrtf = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ParametrosSistema_HostMensuracao") )
               {
                  gxTv_SdtParametrosSistema_Parametrossistema_hostmensuracao = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ParametrosSistema_SQLComputerName") )
               {
                  gxTv_SdtParametrosSistema_Parametrossistema_sqlcomputername = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ParametrosSistema_SQLServerName") )
               {
                  gxTv_SdtParametrosSistema_Parametrossistema_sqlservername = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ParametrosSistema_SQLEdition") )
               {
                  gxTv_SdtParametrosSistema_Parametrossistema_sqledition = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ParametrosSistema_SQLInstance") )
               {
                  gxTv_SdtParametrosSistema_Parametrossistema_sqlinstance = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ParametrosSistema_DBName") )
               {
                  gxTv_SdtParametrosSistema_Parametrossistema_dbname = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ParametrosSistema_FlsEvd") )
               {
                  gxTv_SdtParametrosSistema_Parametrossistema_flsevd = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ParametrosSistema_URLApp") )
               {
                  gxTv_SdtParametrosSistema_Parametrossistema_urlapp = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ParametrosSistema_URLOtherVer") )
               {
                  gxTv_SdtParametrosSistema_Parametrossistema_urlotherver = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ParametrosSistema_Validacao") )
               {
                  gxTv_SdtParametrosSistema_Parametrossistema_validacao = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ParametrosSistema_Imagem") )
               {
                  gxTv_SdtParametrosSistema_Parametrossistema_imagem = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ParametrosSistema_Imagem_GXI") )
               {
                  gxTv_SdtParametrosSistema_Parametrossistema_imagem_gxi = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Mode") )
               {
                  gxTv_SdtParametrosSistema_Mode = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Initialized") )
               {
                  gxTv_SdtParametrosSistema_Initialized = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ParametrosSistema_Codigo_Z") )
               {
                  gxTv_SdtParametrosSistema_Parametrossistema_codigo_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ParametrosSistema_NomeSistema_Z") )
               {
                  gxTv_SdtParametrosSistema_Parametrossistema_nomesistema_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ParametrosSistema_AppID_Z") )
               {
                  gxTv_SdtParametrosSistema_Parametrossistema_appid_Z = (long)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ParametrosSistema_LicensiadoCadastrado_Z") )
               {
                  gxTv_SdtParametrosSistema_Parametrossistema_licensiadocadastrado_Z = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ParametrosSistema_PadronizarStrings_Z") )
               {
                  gxTv_SdtParametrosSistema_Parametrossistema_padronizarstrings_Z = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ParametrosSistema_EmailSdaHost_Z") )
               {
                  gxTv_SdtParametrosSistema_Parametrossistema_emailsdahost_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ParametrosSistema_EmailSdaUser_Z") )
               {
                  gxTv_SdtParametrosSistema_Parametrossistema_emailsdauser_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ParametrosSistema_EmailSdaPass_Z") )
               {
                  gxTv_SdtParametrosSistema_Parametrossistema_emailsdapass_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ParametrosSistema_EmailSdaKey_Z") )
               {
                  gxTv_SdtParametrosSistema_Parametrossistema_emailsdakey_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ParametrosSistema_EmailSdaAut_Z") )
               {
                  gxTv_SdtParametrosSistema_Parametrossistema_emailsdaaut_Z = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ParametrosSistema_EmailSdaPort_Z") )
               {
                  gxTv_SdtParametrosSistema_Parametrossistema_emailsdaport_Z = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ParametrosSistema_EmailSdaSec_Z") )
               {
                  gxTv_SdtParametrosSistema_Parametrossistema_emailsdasec_Z = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ParametrosSistema_FatorAjuste_Z") )
               {
                  gxTv_SdtParametrosSistema_Parametrossistema_fatorajuste_Z = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ParametrosSistema_PathCrtf_Z") )
               {
                  gxTv_SdtParametrosSistema_Parametrossistema_pathcrtf_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ParametrosSistema_HostMensuracao_Z") )
               {
                  gxTv_SdtParametrosSistema_Parametrossistema_hostmensuracao_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ParametrosSistema_SQLComputerName_Z") )
               {
                  gxTv_SdtParametrosSistema_Parametrossistema_sqlcomputername_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ParametrosSistema_SQLServerName_Z") )
               {
                  gxTv_SdtParametrosSistema_Parametrossistema_sqlservername_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ParametrosSistema_SQLEdition_Z") )
               {
                  gxTv_SdtParametrosSistema_Parametrossistema_sqledition_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ParametrosSistema_SQLInstance_Z") )
               {
                  gxTv_SdtParametrosSistema_Parametrossistema_sqlinstance_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ParametrosSistema_DBName_Z") )
               {
                  gxTv_SdtParametrosSistema_Parametrossistema_dbname_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ParametrosSistema_FlsEvd_Z") )
               {
                  gxTv_SdtParametrosSistema_Parametrossistema_flsevd_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ParametrosSistema_URLApp_Z") )
               {
                  gxTv_SdtParametrosSistema_Parametrossistema_urlapp_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ParametrosSistema_URLOtherVer_Z") )
               {
                  gxTv_SdtParametrosSistema_Parametrossistema_urlotherver_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ParametrosSistema_Validacao_Z") )
               {
                  gxTv_SdtParametrosSistema_Parametrossistema_validacao_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ParametrosSistema_Imagem_GXI_Z") )
               {
                  gxTv_SdtParametrosSistema_Parametrossistema_imagem_gxi_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ParametrosSistema_LicensiadoCadastrado_N") )
               {
                  gxTv_SdtParametrosSistema_Parametrossistema_licensiadocadastrado_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ParametrosSistema_EmailSdaHost_N") )
               {
                  gxTv_SdtParametrosSistema_Parametrossistema_emailsdahost_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ParametrosSistema_EmailSdaUser_N") )
               {
                  gxTv_SdtParametrosSistema_Parametrossistema_emailsdauser_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ParametrosSistema_EmailSdaPass_N") )
               {
                  gxTv_SdtParametrosSistema_Parametrossistema_emailsdapass_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ParametrosSistema_EmailSdaKey_N") )
               {
                  gxTv_SdtParametrosSistema_Parametrossistema_emailsdakey_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ParametrosSistema_EmailSdaAut_N") )
               {
                  gxTv_SdtParametrosSistema_Parametrossistema_emailsdaaut_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ParametrosSistema_EmailSdaPort_N") )
               {
                  gxTv_SdtParametrosSistema_Parametrossistema_emailsdaport_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ParametrosSistema_EmailSdaSec_N") )
               {
                  gxTv_SdtParametrosSistema_Parametrossistema_emailsdasec_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ParametrosSistema_FatorAjuste_N") )
               {
                  gxTv_SdtParametrosSistema_Parametrossistema_fatorajuste_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ParametrosSistema_PathCrtf_N") )
               {
                  gxTv_SdtParametrosSistema_Parametrossistema_pathcrtf_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ParametrosSistema_HostMensuracao_N") )
               {
                  gxTv_SdtParametrosSistema_Parametrossistema_hostmensuracao_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ParametrosSistema_SQLComputerName_N") )
               {
                  gxTv_SdtParametrosSistema_Parametrossistema_sqlcomputername_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ParametrosSistema_SQLServerName_N") )
               {
                  gxTv_SdtParametrosSistema_Parametrossistema_sqlservername_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ParametrosSistema_SQLEdition_N") )
               {
                  gxTv_SdtParametrosSistema_Parametrossistema_sqledition_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ParametrosSistema_SQLInstance_N") )
               {
                  gxTv_SdtParametrosSistema_Parametrossistema_sqlinstance_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ParametrosSistema_DBName_N") )
               {
                  gxTv_SdtParametrosSistema_Parametrossistema_dbname_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ParametrosSistema_FlsEvd_N") )
               {
                  gxTv_SdtParametrosSistema_Parametrossistema_flsevd_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ParametrosSistema_URLApp_N") )
               {
                  gxTv_SdtParametrosSistema_Parametrossistema_urlapp_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ParametrosSistema_URLOtherVer_N") )
               {
                  gxTv_SdtParametrosSistema_Parametrossistema_urlotherver_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ParametrosSistema_Validacao_N") )
               {
                  gxTv_SdtParametrosSistema_Parametrossistema_validacao_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ParametrosSistema_Imagem_N") )
               {
                  gxTv_SdtParametrosSistema_Parametrossistema_imagem_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ParametrosSistema_Imagem_GXI_N") )
               {
                  gxTv_SdtParametrosSistema_Parametrossistema_imagem_gxi_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "ParametrosSistema";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "GxEv3Up14_MeetrikaVs3";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("ParametrosSistema_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtParametrosSistema_Parametrossistema_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ParametrosSistema_NomeSistema", StringUtil.RTrim( gxTv_SdtParametrosSistema_Parametrossistema_nomesistema));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ParametrosSistema_AppID", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtParametrosSistema_Parametrossistema_appid), 12, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ParametrosSistema_TextoHome", StringUtil.RTrim( gxTv_SdtParametrosSistema_Parametrossistema_textohome));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ParametrosSistema_LicensiadoCadastrado", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtParametrosSistema_Parametrossistema_licensiadocadastrado)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ParametrosSistema_PadronizarStrings", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtParametrosSistema_Parametrossistema_padronizarstrings)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ParametrosSistema_EmailSdaHost", StringUtil.RTrim( gxTv_SdtParametrosSistema_Parametrossistema_emailsdahost));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ParametrosSistema_EmailSdaUser", StringUtil.RTrim( gxTv_SdtParametrosSistema_Parametrossistema_emailsdauser));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ParametrosSistema_EmailSdaPass", StringUtil.RTrim( gxTv_SdtParametrosSistema_Parametrossistema_emailsdapass));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ParametrosSistema_EmailSdaKey", StringUtil.RTrim( gxTv_SdtParametrosSistema_Parametrossistema_emailsdakey));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ParametrosSistema_EmailSdaAut", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtParametrosSistema_Parametrossistema_emailsdaaut)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ParametrosSistema_EmailSdaPort", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtParametrosSistema_Parametrossistema_emailsdaport), 4, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ParametrosSistema_EmailSdaSec", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtParametrosSistema_Parametrossistema_emailsdasec), 4, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ParametrosSistema_FatorAjuste", StringUtil.Trim( StringUtil.Str( gxTv_SdtParametrosSistema_Parametrossistema_fatorajuste, 6, 2)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ParametrosSistema_PathCrtf", StringUtil.RTrim( gxTv_SdtParametrosSistema_Parametrossistema_pathcrtf));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ParametrosSistema_HostMensuracao", StringUtil.RTrim( gxTv_SdtParametrosSistema_Parametrossistema_hostmensuracao));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ParametrosSistema_SQLComputerName", StringUtil.RTrim( gxTv_SdtParametrosSistema_Parametrossistema_sqlcomputername));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ParametrosSistema_SQLServerName", StringUtil.RTrim( gxTv_SdtParametrosSistema_Parametrossistema_sqlservername));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ParametrosSistema_SQLEdition", StringUtil.RTrim( gxTv_SdtParametrosSistema_Parametrossistema_sqledition));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ParametrosSistema_SQLInstance", StringUtil.RTrim( gxTv_SdtParametrosSistema_Parametrossistema_sqlinstance));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ParametrosSistema_DBName", StringUtil.RTrim( gxTv_SdtParametrosSistema_Parametrossistema_dbname));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ParametrosSistema_FlsEvd", StringUtil.RTrim( gxTv_SdtParametrosSistema_Parametrossistema_flsevd));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ParametrosSistema_URLApp", StringUtil.RTrim( gxTv_SdtParametrosSistema_Parametrossistema_urlapp));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ParametrosSistema_URLOtherVer", StringUtil.RTrim( gxTv_SdtParametrosSistema_Parametrossistema_urlotherver));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ParametrosSistema_Validacao", StringUtil.RTrim( gxTv_SdtParametrosSistema_Parametrossistema_validacao));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ParametrosSistema_Imagem", StringUtil.RTrim( gxTv_SdtParametrosSistema_Parametrossistema_imagem));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         if ( sIncludeState )
         {
            oWriter.WriteElement("ParametrosSistema_Imagem_GXI", StringUtil.RTrim( gxTv_SdtParametrosSistema_Parametrossistema_imagem_gxi));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Mode", StringUtil.RTrim( gxTv_SdtParametrosSistema_Mode));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Initialized", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtParametrosSistema_Initialized), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ParametrosSistema_Codigo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtParametrosSistema_Parametrossistema_codigo_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ParametrosSistema_NomeSistema_Z", StringUtil.RTrim( gxTv_SdtParametrosSistema_Parametrossistema_nomesistema_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ParametrosSistema_AppID_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtParametrosSistema_Parametrossistema_appid_Z), 12, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ParametrosSistema_LicensiadoCadastrado_Z", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtParametrosSistema_Parametrossistema_licensiadocadastrado_Z)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ParametrosSistema_PadronizarStrings_Z", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtParametrosSistema_Parametrossistema_padronizarstrings_Z)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ParametrosSistema_EmailSdaHost_Z", StringUtil.RTrim( gxTv_SdtParametrosSistema_Parametrossistema_emailsdahost_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ParametrosSistema_EmailSdaUser_Z", StringUtil.RTrim( gxTv_SdtParametrosSistema_Parametrossistema_emailsdauser_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ParametrosSistema_EmailSdaPass_Z", StringUtil.RTrim( gxTv_SdtParametrosSistema_Parametrossistema_emailsdapass_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ParametrosSistema_EmailSdaKey_Z", StringUtil.RTrim( gxTv_SdtParametrosSistema_Parametrossistema_emailsdakey_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ParametrosSistema_EmailSdaAut_Z", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtParametrosSistema_Parametrossistema_emailsdaaut_Z)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ParametrosSistema_EmailSdaPort_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtParametrosSistema_Parametrossistema_emailsdaport_Z), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ParametrosSistema_EmailSdaSec_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtParametrosSistema_Parametrossistema_emailsdasec_Z), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ParametrosSistema_FatorAjuste_Z", StringUtil.Trim( StringUtil.Str( gxTv_SdtParametrosSistema_Parametrossistema_fatorajuste_Z, 6, 2)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ParametrosSistema_PathCrtf_Z", StringUtil.RTrim( gxTv_SdtParametrosSistema_Parametrossistema_pathcrtf_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ParametrosSistema_HostMensuracao_Z", StringUtil.RTrim( gxTv_SdtParametrosSistema_Parametrossistema_hostmensuracao_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ParametrosSistema_SQLComputerName_Z", StringUtil.RTrim( gxTv_SdtParametrosSistema_Parametrossistema_sqlcomputername_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ParametrosSistema_SQLServerName_Z", StringUtil.RTrim( gxTv_SdtParametrosSistema_Parametrossistema_sqlservername_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ParametrosSistema_SQLEdition_Z", StringUtil.RTrim( gxTv_SdtParametrosSistema_Parametrossistema_sqledition_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ParametrosSistema_SQLInstance_Z", StringUtil.RTrim( gxTv_SdtParametrosSistema_Parametrossistema_sqlinstance_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ParametrosSistema_DBName_Z", StringUtil.RTrim( gxTv_SdtParametrosSistema_Parametrossistema_dbname_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ParametrosSistema_FlsEvd_Z", StringUtil.RTrim( gxTv_SdtParametrosSistema_Parametrossistema_flsevd_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ParametrosSistema_URLApp_Z", StringUtil.RTrim( gxTv_SdtParametrosSistema_Parametrossistema_urlapp_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ParametrosSistema_URLOtherVer_Z", StringUtil.RTrim( gxTv_SdtParametrosSistema_Parametrossistema_urlotherver_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ParametrosSistema_Validacao_Z", StringUtil.RTrim( gxTv_SdtParametrosSistema_Parametrossistema_validacao_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ParametrosSistema_Imagem_GXI_Z", StringUtil.RTrim( gxTv_SdtParametrosSistema_Parametrossistema_imagem_gxi_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ParametrosSistema_LicensiadoCadastrado_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtParametrosSistema_Parametrossistema_licensiadocadastrado_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ParametrosSistema_EmailSdaHost_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtParametrosSistema_Parametrossistema_emailsdahost_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ParametrosSistema_EmailSdaUser_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtParametrosSistema_Parametrossistema_emailsdauser_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ParametrosSistema_EmailSdaPass_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtParametrosSistema_Parametrossistema_emailsdapass_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ParametrosSistema_EmailSdaKey_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtParametrosSistema_Parametrossistema_emailsdakey_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ParametrosSistema_EmailSdaAut_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtParametrosSistema_Parametrossistema_emailsdaaut_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ParametrosSistema_EmailSdaPort_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtParametrosSistema_Parametrossistema_emailsdaport_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ParametrosSistema_EmailSdaSec_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtParametrosSistema_Parametrossistema_emailsdasec_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ParametrosSistema_FatorAjuste_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtParametrosSistema_Parametrossistema_fatorajuste_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ParametrosSistema_PathCrtf_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtParametrosSistema_Parametrossistema_pathcrtf_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ParametrosSistema_HostMensuracao_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtParametrosSistema_Parametrossistema_hostmensuracao_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ParametrosSistema_SQLComputerName_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtParametrosSistema_Parametrossistema_sqlcomputername_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ParametrosSistema_SQLServerName_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtParametrosSistema_Parametrossistema_sqlservername_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ParametrosSistema_SQLEdition_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtParametrosSistema_Parametrossistema_sqledition_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ParametrosSistema_SQLInstance_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtParametrosSistema_Parametrossistema_sqlinstance_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ParametrosSistema_DBName_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtParametrosSistema_Parametrossistema_dbname_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ParametrosSistema_FlsEvd_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtParametrosSistema_Parametrossistema_flsevd_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ParametrosSistema_URLApp_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtParametrosSistema_Parametrossistema_urlapp_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ParametrosSistema_URLOtherVer_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtParametrosSistema_Parametrossistema_urlotherver_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ParametrosSistema_Validacao_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtParametrosSistema_Parametrossistema_validacao_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ParametrosSistema_Imagem_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtParametrosSistema_Parametrossistema_imagem_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ParametrosSistema_Imagem_GXI_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtParametrosSistema_Parametrossistema_imagem_gxi_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("ParametrosSistema_Codigo", gxTv_SdtParametrosSistema_Parametrossistema_codigo, false);
         AddObjectProperty("ParametrosSistema_NomeSistema", gxTv_SdtParametrosSistema_Parametrossistema_nomesistema, false);
         AddObjectProperty("ParametrosSistema_AppID", gxTv_SdtParametrosSistema_Parametrossistema_appid, false);
         AddObjectProperty("ParametrosSistema_TextoHome", gxTv_SdtParametrosSistema_Parametrossistema_textohome, false);
         AddObjectProperty("ParametrosSistema_LicensiadoCadastrado", gxTv_SdtParametrosSistema_Parametrossistema_licensiadocadastrado, false);
         AddObjectProperty("ParametrosSistema_PadronizarStrings", gxTv_SdtParametrosSistema_Parametrossistema_padronizarstrings, false);
         AddObjectProperty("ParametrosSistema_EmailSdaHost", gxTv_SdtParametrosSistema_Parametrossistema_emailsdahost, false);
         AddObjectProperty("ParametrosSistema_EmailSdaUser", gxTv_SdtParametrosSistema_Parametrossistema_emailsdauser, false);
         AddObjectProperty("ParametrosSistema_EmailSdaPass", gxTv_SdtParametrosSistema_Parametrossistema_emailsdapass, false);
         AddObjectProperty("ParametrosSistema_EmailSdaKey", gxTv_SdtParametrosSistema_Parametrossistema_emailsdakey, false);
         AddObjectProperty("ParametrosSistema_EmailSdaAut", gxTv_SdtParametrosSistema_Parametrossistema_emailsdaaut, false);
         AddObjectProperty("ParametrosSistema_EmailSdaPort", gxTv_SdtParametrosSistema_Parametrossistema_emailsdaport, false);
         AddObjectProperty("ParametrosSistema_EmailSdaSec", gxTv_SdtParametrosSistema_Parametrossistema_emailsdasec, false);
         AddObjectProperty("ParametrosSistema_FatorAjuste", gxTv_SdtParametrosSistema_Parametrossistema_fatorajuste, false);
         AddObjectProperty("ParametrosSistema_PathCrtf", gxTv_SdtParametrosSistema_Parametrossistema_pathcrtf, false);
         AddObjectProperty("ParametrosSistema_HostMensuracao", gxTv_SdtParametrosSistema_Parametrossistema_hostmensuracao, false);
         AddObjectProperty("ParametrosSistema_SQLComputerName", gxTv_SdtParametrosSistema_Parametrossistema_sqlcomputername, false);
         AddObjectProperty("ParametrosSistema_SQLServerName", gxTv_SdtParametrosSistema_Parametrossistema_sqlservername, false);
         AddObjectProperty("ParametrosSistema_SQLEdition", gxTv_SdtParametrosSistema_Parametrossistema_sqledition, false);
         AddObjectProperty("ParametrosSistema_SQLInstance", gxTv_SdtParametrosSistema_Parametrossistema_sqlinstance, false);
         AddObjectProperty("ParametrosSistema_DBName", gxTv_SdtParametrosSistema_Parametrossistema_dbname, false);
         AddObjectProperty("ParametrosSistema_FlsEvd", gxTv_SdtParametrosSistema_Parametrossistema_flsevd, false);
         AddObjectProperty("ParametrosSistema_URLApp", gxTv_SdtParametrosSistema_Parametrossistema_urlapp, false);
         AddObjectProperty("ParametrosSistema_URLOtherVer", gxTv_SdtParametrosSistema_Parametrossistema_urlotherver, false);
         AddObjectProperty("ParametrosSistema_Validacao", gxTv_SdtParametrosSistema_Parametrossistema_validacao, false);
         AddObjectProperty("ParametrosSistema_Imagem", gxTv_SdtParametrosSistema_Parametrossistema_imagem, false);
         if ( includeState )
         {
            AddObjectProperty("ParametrosSistema_Imagem_GXI", gxTv_SdtParametrosSistema_Parametrossistema_imagem_gxi, false);
            AddObjectProperty("Mode", gxTv_SdtParametrosSistema_Mode, false);
            AddObjectProperty("Initialized", gxTv_SdtParametrosSistema_Initialized, false);
            AddObjectProperty("ParametrosSistema_Codigo_Z", gxTv_SdtParametrosSistema_Parametrossistema_codigo_Z, false);
            AddObjectProperty("ParametrosSistema_NomeSistema_Z", gxTv_SdtParametrosSistema_Parametrossistema_nomesistema_Z, false);
            AddObjectProperty("ParametrosSistema_AppID_Z", gxTv_SdtParametrosSistema_Parametrossistema_appid_Z, false);
            AddObjectProperty("ParametrosSistema_LicensiadoCadastrado_Z", gxTv_SdtParametrosSistema_Parametrossistema_licensiadocadastrado_Z, false);
            AddObjectProperty("ParametrosSistema_PadronizarStrings_Z", gxTv_SdtParametrosSistema_Parametrossistema_padronizarstrings_Z, false);
            AddObjectProperty("ParametrosSistema_EmailSdaHost_Z", gxTv_SdtParametrosSistema_Parametrossistema_emailsdahost_Z, false);
            AddObjectProperty("ParametrosSistema_EmailSdaUser_Z", gxTv_SdtParametrosSistema_Parametrossistema_emailsdauser_Z, false);
            AddObjectProperty("ParametrosSistema_EmailSdaPass_Z", gxTv_SdtParametrosSistema_Parametrossistema_emailsdapass_Z, false);
            AddObjectProperty("ParametrosSistema_EmailSdaKey_Z", gxTv_SdtParametrosSistema_Parametrossistema_emailsdakey_Z, false);
            AddObjectProperty("ParametrosSistema_EmailSdaAut_Z", gxTv_SdtParametrosSistema_Parametrossistema_emailsdaaut_Z, false);
            AddObjectProperty("ParametrosSistema_EmailSdaPort_Z", gxTv_SdtParametrosSistema_Parametrossistema_emailsdaport_Z, false);
            AddObjectProperty("ParametrosSistema_EmailSdaSec_Z", gxTv_SdtParametrosSistema_Parametrossistema_emailsdasec_Z, false);
            AddObjectProperty("ParametrosSistema_FatorAjuste_Z", gxTv_SdtParametrosSistema_Parametrossistema_fatorajuste_Z, false);
            AddObjectProperty("ParametrosSistema_PathCrtf_Z", gxTv_SdtParametrosSistema_Parametrossistema_pathcrtf_Z, false);
            AddObjectProperty("ParametrosSistema_HostMensuracao_Z", gxTv_SdtParametrosSistema_Parametrossistema_hostmensuracao_Z, false);
            AddObjectProperty("ParametrosSistema_SQLComputerName_Z", gxTv_SdtParametrosSistema_Parametrossistema_sqlcomputername_Z, false);
            AddObjectProperty("ParametrosSistema_SQLServerName_Z", gxTv_SdtParametrosSistema_Parametrossistema_sqlservername_Z, false);
            AddObjectProperty("ParametrosSistema_SQLEdition_Z", gxTv_SdtParametrosSistema_Parametrossistema_sqledition_Z, false);
            AddObjectProperty("ParametrosSistema_SQLInstance_Z", gxTv_SdtParametrosSistema_Parametrossistema_sqlinstance_Z, false);
            AddObjectProperty("ParametrosSistema_DBName_Z", gxTv_SdtParametrosSistema_Parametrossistema_dbname_Z, false);
            AddObjectProperty("ParametrosSistema_FlsEvd_Z", gxTv_SdtParametrosSistema_Parametrossistema_flsevd_Z, false);
            AddObjectProperty("ParametrosSistema_URLApp_Z", gxTv_SdtParametrosSistema_Parametrossistema_urlapp_Z, false);
            AddObjectProperty("ParametrosSistema_URLOtherVer_Z", gxTv_SdtParametrosSistema_Parametrossistema_urlotherver_Z, false);
            AddObjectProperty("ParametrosSistema_Validacao_Z", gxTv_SdtParametrosSistema_Parametrossistema_validacao_Z, false);
            AddObjectProperty("ParametrosSistema_Imagem_GXI_Z", gxTv_SdtParametrosSistema_Parametrossistema_imagem_gxi_Z, false);
            AddObjectProperty("ParametrosSistema_LicensiadoCadastrado_N", gxTv_SdtParametrosSistema_Parametrossistema_licensiadocadastrado_N, false);
            AddObjectProperty("ParametrosSistema_EmailSdaHost_N", gxTv_SdtParametrosSistema_Parametrossistema_emailsdahost_N, false);
            AddObjectProperty("ParametrosSistema_EmailSdaUser_N", gxTv_SdtParametrosSistema_Parametrossistema_emailsdauser_N, false);
            AddObjectProperty("ParametrosSistema_EmailSdaPass_N", gxTv_SdtParametrosSistema_Parametrossistema_emailsdapass_N, false);
            AddObjectProperty("ParametrosSistema_EmailSdaKey_N", gxTv_SdtParametrosSistema_Parametrossistema_emailsdakey_N, false);
            AddObjectProperty("ParametrosSistema_EmailSdaAut_N", gxTv_SdtParametrosSistema_Parametrossistema_emailsdaaut_N, false);
            AddObjectProperty("ParametrosSistema_EmailSdaPort_N", gxTv_SdtParametrosSistema_Parametrossistema_emailsdaport_N, false);
            AddObjectProperty("ParametrosSistema_EmailSdaSec_N", gxTv_SdtParametrosSistema_Parametrossistema_emailsdasec_N, false);
            AddObjectProperty("ParametrosSistema_FatorAjuste_N", gxTv_SdtParametrosSistema_Parametrossistema_fatorajuste_N, false);
            AddObjectProperty("ParametrosSistema_PathCrtf_N", gxTv_SdtParametrosSistema_Parametrossistema_pathcrtf_N, false);
            AddObjectProperty("ParametrosSistema_HostMensuracao_N", gxTv_SdtParametrosSistema_Parametrossistema_hostmensuracao_N, false);
            AddObjectProperty("ParametrosSistema_SQLComputerName_N", gxTv_SdtParametrosSistema_Parametrossistema_sqlcomputername_N, false);
            AddObjectProperty("ParametrosSistema_SQLServerName_N", gxTv_SdtParametrosSistema_Parametrossistema_sqlservername_N, false);
            AddObjectProperty("ParametrosSistema_SQLEdition_N", gxTv_SdtParametrosSistema_Parametrossistema_sqledition_N, false);
            AddObjectProperty("ParametrosSistema_SQLInstance_N", gxTv_SdtParametrosSistema_Parametrossistema_sqlinstance_N, false);
            AddObjectProperty("ParametrosSistema_DBName_N", gxTv_SdtParametrosSistema_Parametrossistema_dbname_N, false);
            AddObjectProperty("ParametrosSistema_FlsEvd_N", gxTv_SdtParametrosSistema_Parametrossistema_flsevd_N, false);
            AddObjectProperty("ParametrosSistema_URLApp_N", gxTv_SdtParametrosSistema_Parametrossistema_urlapp_N, false);
            AddObjectProperty("ParametrosSistema_URLOtherVer_N", gxTv_SdtParametrosSistema_Parametrossistema_urlotherver_N, false);
            AddObjectProperty("ParametrosSistema_Validacao_N", gxTv_SdtParametrosSistema_Parametrossistema_validacao_N, false);
            AddObjectProperty("ParametrosSistema_Imagem_N", gxTv_SdtParametrosSistema_Parametrossistema_imagem_N, false);
            AddObjectProperty("ParametrosSistema_Imagem_GXI_N", gxTv_SdtParametrosSistema_Parametrossistema_imagem_gxi_N, false);
         }
         return  ;
      }

      [  SoapElement( ElementName = "ParametrosSistema_Codigo" )]
      [  XmlElement( ElementName = "ParametrosSistema_Codigo"   )]
      public int gxTpr_Parametrossistema_codigo
      {
         get {
            return gxTv_SdtParametrosSistema_Parametrossistema_codigo ;
         }

         set {
            if ( gxTv_SdtParametrosSistema_Parametrossistema_codigo != value )
            {
               gxTv_SdtParametrosSistema_Mode = "INS";
               this.gxTv_SdtParametrosSistema_Parametrossistema_codigo_Z_SetNull( );
               this.gxTv_SdtParametrosSistema_Parametrossistema_nomesistema_Z_SetNull( );
               this.gxTv_SdtParametrosSistema_Parametrossistema_appid_Z_SetNull( );
               this.gxTv_SdtParametrosSistema_Parametrossistema_licensiadocadastrado_Z_SetNull( );
               this.gxTv_SdtParametrosSistema_Parametrossistema_padronizarstrings_Z_SetNull( );
               this.gxTv_SdtParametrosSistema_Parametrossistema_emailsdahost_Z_SetNull( );
               this.gxTv_SdtParametrosSistema_Parametrossistema_emailsdauser_Z_SetNull( );
               this.gxTv_SdtParametrosSistema_Parametrossistema_emailsdapass_Z_SetNull( );
               this.gxTv_SdtParametrosSistema_Parametrossistema_emailsdakey_Z_SetNull( );
               this.gxTv_SdtParametrosSistema_Parametrossistema_emailsdaaut_Z_SetNull( );
               this.gxTv_SdtParametrosSistema_Parametrossistema_emailsdaport_Z_SetNull( );
               this.gxTv_SdtParametrosSistema_Parametrossistema_emailsdasec_Z_SetNull( );
               this.gxTv_SdtParametrosSistema_Parametrossistema_fatorajuste_Z_SetNull( );
               this.gxTv_SdtParametrosSistema_Parametrossistema_pathcrtf_Z_SetNull( );
               this.gxTv_SdtParametrosSistema_Parametrossistema_hostmensuracao_Z_SetNull( );
               this.gxTv_SdtParametrosSistema_Parametrossistema_sqlcomputername_Z_SetNull( );
               this.gxTv_SdtParametrosSistema_Parametrossistema_sqlservername_Z_SetNull( );
               this.gxTv_SdtParametrosSistema_Parametrossistema_sqledition_Z_SetNull( );
               this.gxTv_SdtParametrosSistema_Parametrossistema_sqlinstance_Z_SetNull( );
               this.gxTv_SdtParametrosSistema_Parametrossistema_dbname_Z_SetNull( );
               this.gxTv_SdtParametrosSistema_Parametrossistema_flsevd_Z_SetNull( );
               this.gxTv_SdtParametrosSistema_Parametrossistema_urlapp_Z_SetNull( );
               this.gxTv_SdtParametrosSistema_Parametrossistema_urlotherver_Z_SetNull( );
               this.gxTv_SdtParametrosSistema_Parametrossistema_validacao_Z_SetNull( );
               this.gxTv_SdtParametrosSistema_Parametrossistema_imagem_gxi_Z_SetNull( );
            }
            gxTv_SdtParametrosSistema_Parametrossistema_codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ParametrosSistema_NomeSistema" )]
      [  XmlElement( ElementName = "ParametrosSistema_NomeSistema"   )]
      public String gxTpr_Parametrossistema_nomesistema
      {
         get {
            return gxTv_SdtParametrosSistema_Parametrossistema_nomesistema ;
         }

         set {
            gxTv_SdtParametrosSistema_Parametrossistema_nomesistema = (String)(value);
         }

      }

      [  SoapElement( ElementName = "ParametrosSistema_AppID" )]
      [  XmlElement( ElementName = "ParametrosSistema_AppID"   )]
      public long gxTpr_Parametrossistema_appid
      {
         get {
            return gxTv_SdtParametrosSistema_Parametrossistema_appid ;
         }

         set {
            gxTv_SdtParametrosSistema_Parametrossistema_appid = (long)(value);
         }

      }

      [  SoapElement( ElementName = "ParametrosSistema_TextoHome" )]
      [  XmlElement( ElementName = "ParametrosSistema_TextoHome"   )]
      public String gxTpr_Parametrossistema_textohome
      {
         get {
            return gxTv_SdtParametrosSistema_Parametrossistema_textohome ;
         }

         set {
            gxTv_SdtParametrosSistema_Parametrossistema_textohome = (String)(value);
         }

      }

      [  SoapElement( ElementName = "ParametrosSistema_LicensiadoCadastrado" )]
      [  XmlElement( ElementName = "ParametrosSistema_LicensiadoCadastrado"   )]
      public bool gxTpr_Parametrossistema_licensiadocadastrado
      {
         get {
            return gxTv_SdtParametrosSistema_Parametrossistema_licensiadocadastrado ;
         }

         set {
            gxTv_SdtParametrosSistema_Parametrossistema_licensiadocadastrado_N = 0;
            gxTv_SdtParametrosSistema_Parametrossistema_licensiadocadastrado = value;
         }

      }

      public void gxTv_SdtParametrosSistema_Parametrossistema_licensiadocadastrado_SetNull( )
      {
         gxTv_SdtParametrosSistema_Parametrossistema_licensiadocadastrado_N = 1;
         gxTv_SdtParametrosSistema_Parametrossistema_licensiadocadastrado = false;
         return  ;
      }

      public bool gxTv_SdtParametrosSistema_Parametrossistema_licensiadocadastrado_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ParametrosSistema_PadronizarStrings" )]
      [  XmlElement( ElementName = "ParametrosSistema_PadronizarStrings"   )]
      public bool gxTpr_Parametrossistema_padronizarstrings
      {
         get {
            return gxTv_SdtParametrosSistema_Parametrossistema_padronizarstrings ;
         }

         set {
            gxTv_SdtParametrosSistema_Parametrossistema_padronizarstrings = value;
         }

      }

      [  SoapElement( ElementName = "ParametrosSistema_EmailSdaHost" )]
      [  XmlElement( ElementName = "ParametrosSistema_EmailSdaHost"   )]
      public String gxTpr_Parametrossistema_emailsdahost
      {
         get {
            return gxTv_SdtParametrosSistema_Parametrossistema_emailsdahost ;
         }

         set {
            gxTv_SdtParametrosSistema_Parametrossistema_emailsdahost_N = 0;
            gxTv_SdtParametrosSistema_Parametrossistema_emailsdahost = (String)(value);
         }

      }

      public void gxTv_SdtParametrosSistema_Parametrossistema_emailsdahost_SetNull( )
      {
         gxTv_SdtParametrosSistema_Parametrossistema_emailsdahost_N = 1;
         gxTv_SdtParametrosSistema_Parametrossistema_emailsdahost = "";
         return  ;
      }

      public bool gxTv_SdtParametrosSistema_Parametrossistema_emailsdahost_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ParametrosSistema_EmailSdaUser" )]
      [  XmlElement( ElementName = "ParametrosSistema_EmailSdaUser"   )]
      public String gxTpr_Parametrossistema_emailsdauser
      {
         get {
            return gxTv_SdtParametrosSistema_Parametrossistema_emailsdauser ;
         }

         set {
            gxTv_SdtParametrosSistema_Parametrossistema_emailsdauser_N = 0;
            gxTv_SdtParametrosSistema_Parametrossistema_emailsdauser = (String)(value);
         }

      }

      public void gxTv_SdtParametrosSistema_Parametrossistema_emailsdauser_SetNull( )
      {
         gxTv_SdtParametrosSistema_Parametrossistema_emailsdauser_N = 1;
         gxTv_SdtParametrosSistema_Parametrossistema_emailsdauser = "";
         return  ;
      }

      public bool gxTv_SdtParametrosSistema_Parametrossistema_emailsdauser_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ParametrosSistema_EmailSdaPass" )]
      [  XmlElement( ElementName = "ParametrosSistema_EmailSdaPass"   )]
      public String gxTpr_Parametrossistema_emailsdapass
      {
         get {
            return gxTv_SdtParametrosSistema_Parametrossistema_emailsdapass ;
         }

         set {
            gxTv_SdtParametrosSistema_Parametrossistema_emailsdapass_N = 0;
            gxTv_SdtParametrosSistema_Parametrossistema_emailsdapass = (String)(value);
         }

      }

      public void gxTv_SdtParametrosSistema_Parametrossistema_emailsdapass_SetNull( )
      {
         gxTv_SdtParametrosSistema_Parametrossistema_emailsdapass_N = 1;
         gxTv_SdtParametrosSistema_Parametrossistema_emailsdapass = "";
         return  ;
      }

      public bool gxTv_SdtParametrosSistema_Parametrossistema_emailsdapass_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ParametrosSistema_EmailSdaKey" )]
      [  XmlElement( ElementName = "ParametrosSistema_EmailSdaKey"   )]
      public String gxTpr_Parametrossistema_emailsdakey
      {
         get {
            return gxTv_SdtParametrosSistema_Parametrossistema_emailsdakey ;
         }

         set {
            gxTv_SdtParametrosSistema_Parametrossistema_emailsdakey_N = 0;
            gxTv_SdtParametrosSistema_Parametrossistema_emailsdakey = (String)(value);
         }

      }

      public void gxTv_SdtParametrosSistema_Parametrossistema_emailsdakey_SetNull( )
      {
         gxTv_SdtParametrosSistema_Parametrossistema_emailsdakey_N = 1;
         gxTv_SdtParametrosSistema_Parametrossistema_emailsdakey = "";
         return  ;
      }

      public bool gxTv_SdtParametrosSistema_Parametrossistema_emailsdakey_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ParametrosSistema_EmailSdaAut" )]
      [  XmlElement( ElementName = "ParametrosSistema_EmailSdaAut"   )]
      public bool gxTpr_Parametrossistema_emailsdaaut
      {
         get {
            return gxTv_SdtParametrosSistema_Parametrossistema_emailsdaaut ;
         }

         set {
            gxTv_SdtParametrosSistema_Parametrossistema_emailsdaaut_N = 0;
            gxTv_SdtParametrosSistema_Parametrossistema_emailsdaaut = value;
         }

      }

      public void gxTv_SdtParametrosSistema_Parametrossistema_emailsdaaut_SetNull( )
      {
         gxTv_SdtParametrosSistema_Parametrossistema_emailsdaaut_N = 1;
         gxTv_SdtParametrosSistema_Parametrossistema_emailsdaaut = false;
         return  ;
      }

      public bool gxTv_SdtParametrosSistema_Parametrossistema_emailsdaaut_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ParametrosSistema_EmailSdaPort" )]
      [  XmlElement( ElementName = "ParametrosSistema_EmailSdaPort"   )]
      public short gxTpr_Parametrossistema_emailsdaport
      {
         get {
            return gxTv_SdtParametrosSistema_Parametrossistema_emailsdaport ;
         }

         set {
            gxTv_SdtParametrosSistema_Parametrossistema_emailsdaport_N = 0;
            gxTv_SdtParametrosSistema_Parametrossistema_emailsdaport = (short)(value);
         }

      }

      public void gxTv_SdtParametrosSistema_Parametrossistema_emailsdaport_SetNull( )
      {
         gxTv_SdtParametrosSistema_Parametrossistema_emailsdaport_N = 1;
         gxTv_SdtParametrosSistema_Parametrossistema_emailsdaport = 0;
         return  ;
      }

      public bool gxTv_SdtParametrosSistema_Parametrossistema_emailsdaport_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ParametrosSistema_EmailSdaSec" )]
      [  XmlElement( ElementName = "ParametrosSistema_EmailSdaSec"   )]
      public short gxTpr_Parametrossistema_emailsdasec
      {
         get {
            return gxTv_SdtParametrosSistema_Parametrossistema_emailsdasec ;
         }

         set {
            gxTv_SdtParametrosSistema_Parametrossistema_emailsdasec_N = 0;
            gxTv_SdtParametrosSistema_Parametrossistema_emailsdasec = (short)(value);
         }

      }

      public void gxTv_SdtParametrosSistema_Parametrossistema_emailsdasec_SetNull( )
      {
         gxTv_SdtParametrosSistema_Parametrossistema_emailsdasec_N = 1;
         gxTv_SdtParametrosSistema_Parametrossistema_emailsdasec = 0;
         return  ;
      }

      public bool gxTv_SdtParametrosSistema_Parametrossistema_emailsdasec_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ParametrosSistema_FatorAjuste" )]
      [  XmlElement( ElementName = "ParametrosSistema_FatorAjuste"   )]
      public double gxTpr_Parametrossistema_fatorajuste_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtParametrosSistema_Parametrossistema_fatorajuste) ;
         }

         set {
            gxTv_SdtParametrosSistema_Parametrossistema_fatorajuste_N = 0;
            gxTv_SdtParametrosSistema_Parametrossistema_fatorajuste = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Parametrossistema_fatorajuste
      {
         get {
            return gxTv_SdtParametrosSistema_Parametrossistema_fatorajuste ;
         }

         set {
            gxTv_SdtParametrosSistema_Parametrossistema_fatorajuste_N = 0;
            gxTv_SdtParametrosSistema_Parametrossistema_fatorajuste = (decimal)(value);
         }

      }

      public void gxTv_SdtParametrosSistema_Parametrossistema_fatorajuste_SetNull( )
      {
         gxTv_SdtParametrosSistema_Parametrossistema_fatorajuste_N = 1;
         gxTv_SdtParametrosSistema_Parametrossistema_fatorajuste = 0;
         return  ;
      }

      public bool gxTv_SdtParametrosSistema_Parametrossistema_fatorajuste_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ParametrosSistema_PathCrtf" )]
      [  XmlElement( ElementName = "ParametrosSistema_PathCrtf"   )]
      public String gxTpr_Parametrossistema_pathcrtf
      {
         get {
            return gxTv_SdtParametrosSistema_Parametrossistema_pathcrtf ;
         }

         set {
            gxTv_SdtParametrosSistema_Parametrossistema_pathcrtf_N = 0;
            gxTv_SdtParametrosSistema_Parametrossistema_pathcrtf = (String)(value);
         }

      }

      public void gxTv_SdtParametrosSistema_Parametrossistema_pathcrtf_SetNull( )
      {
         gxTv_SdtParametrosSistema_Parametrossistema_pathcrtf_N = 1;
         gxTv_SdtParametrosSistema_Parametrossistema_pathcrtf = "";
         return  ;
      }

      public bool gxTv_SdtParametrosSistema_Parametrossistema_pathcrtf_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ParametrosSistema_HostMensuracao" )]
      [  XmlElement( ElementName = "ParametrosSistema_HostMensuracao"   )]
      public String gxTpr_Parametrossistema_hostmensuracao
      {
         get {
            return gxTv_SdtParametrosSistema_Parametrossistema_hostmensuracao ;
         }

         set {
            gxTv_SdtParametrosSistema_Parametrossistema_hostmensuracao_N = 0;
            gxTv_SdtParametrosSistema_Parametrossistema_hostmensuracao = (String)(value);
         }

      }

      public void gxTv_SdtParametrosSistema_Parametrossistema_hostmensuracao_SetNull( )
      {
         gxTv_SdtParametrosSistema_Parametrossistema_hostmensuracao_N = 1;
         gxTv_SdtParametrosSistema_Parametrossistema_hostmensuracao = "";
         return  ;
      }

      public bool gxTv_SdtParametrosSistema_Parametrossistema_hostmensuracao_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ParametrosSistema_SQLComputerName" )]
      [  XmlElement( ElementName = "ParametrosSistema_SQLComputerName"   )]
      public String gxTpr_Parametrossistema_sqlcomputername
      {
         get {
            return gxTv_SdtParametrosSistema_Parametrossistema_sqlcomputername ;
         }

         set {
            gxTv_SdtParametrosSistema_Parametrossistema_sqlcomputername_N = 0;
            gxTv_SdtParametrosSistema_Parametrossistema_sqlcomputername = (String)(value);
         }

      }

      public void gxTv_SdtParametrosSistema_Parametrossistema_sqlcomputername_SetNull( )
      {
         gxTv_SdtParametrosSistema_Parametrossistema_sqlcomputername_N = 1;
         gxTv_SdtParametrosSistema_Parametrossistema_sqlcomputername = "";
         return  ;
      }

      public bool gxTv_SdtParametrosSistema_Parametrossistema_sqlcomputername_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ParametrosSistema_SQLServerName" )]
      [  XmlElement( ElementName = "ParametrosSistema_SQLServerName"   )]
      public String gxTpr_Parametrossistema_sqlservername
      {
         get {
            return gxTv_SdtParametrosSistema_Parametrossistema_sqlservername ;
         }

         set {
            gxTv_SdtParametrosSistema_Parametrossistema_sqlservername_N = 0;
            gxTv_SdtParametrosSistema_Parametrossistema_sqlservername = (String)(value);
         }

      }

      public void gxTv_SdtParametrosSistema_Parametrossistema_sqlservername_SetNull( )
      {
         gxTv_SdtParametrosSistema_Parametrossistema_sqlservername_N = 1;
         gxTv_SdtParametrosSistema_Parametrossistema_sqlservername = "";
         return  ;
      }

      public bool gxTv_SdtParametrosSistema_Parametrossistema_sqlservername_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ParametrosSistema_SQLEdition" )]
      [  XmlElement( ElementName = "ParametrosSistema_SQLEdition"   )]
      public String gxTpr_Parametrossistema_sqledition
      {
         get {
            return gxTv_SdtParametrosSistema_Parametrossistema_sqledition ;
         }

         set {
            gxTv_SdtParametrosSistema_Parametrossistema_sqledition_N = 0;
            gxTv_SdtParametrosSistema_Parametrossistema_sqledition = (String)(value);
         }

      }

      public void gxTv_SdtParametrosSistema_Parametrossistema_sqledition_SetNull( )
      {
         gxTv_SdtParametrosSistema_Parametrossistema_sqledition_N = 1;
         gxTv_SdtParametrosSistema_Parametrossistema_sqledition = "";
         return  ;
      }

      public bool gxTv_SdtParametrosSistema_Parametrossistema_sqledition_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ParametrosSistema_SQLInstance" )]
      [  XmlElement( ElementName = "ParametrosSistema_SQLInstance"   )]
      public String gxTpr_Parametrossistema_sqlinstance
      {
         get {
            return gxTv_SdtParametrosSistema_Parametrossistema_sqlinstance ;
         }

         set {
            gxTv_SdtParametrosSistema_Parametrossistema_sqlinstance_N = 0;
            gxTv_SdtParametrosSistema_Parametrossistema_sqlinstance = (String)(value);
         }

      }

      public void gxTv_SdtParametrosSistema_Parametrossistema_sqlinstance_SetNull( )
      {
         gxTv_SdtParametrosSistema_Parametrossistema_sqlinstance_N = 1;
         gxTv_SdtParametrosSistema_Parametrossistema_sqlinstance = "";
         return  ;
      }

      public bool gxTv_SdtParametrosSistema_Parametrossistema_sqlinstance_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ParametrosSistema_DBName" )]
      [  XmlElement( ElementName = "ParametrosSistema_DBName"   )]
      public String gxTpr_Parametrossistema_dbname
      {
         get {
            return gxTv_SdtParametrosSistema_Parametrossistema_dbname ;
         }

         set {
            gxTv_SdtParametrosSistema_Parametrossistema_dbname_N = 0;
            gxTv_SdtParametrosSistema_Parametrossistema_dbname = (String)(value);
         }

      }

      public void gxTv_SdtParametrosSistema_Parametrossistema_dbname_SetNull( )
      {
         gxTv_SdtParametrosSistema_Parametrossistema_dbname_N = 1;
         gxTv_SdtParametrosSistema_Parametrossistema_dbname = "";
         return  ;
      }

      public bool gxTv_SdtParametrosSistema_Parametrossistema_dbname_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ParametrosSistema_FlsEvd" )]
      [  XmlElement( ElementName = "ParametrosSistema_FlsEvd"   )]
      public String gxTpr_Parametrossistema_flsevd
      {
         get {
            return gxTv_SdtParametrosSistema_Parametrossistema_flsevd ;
         }

         set {
            gxTv_SdtParametrosSistema_Parametrossistema_flsevd_N = 0;
            gxTv_SdtParametrosSistema_Parametrossistema_flsevd = (String)(value);
         }

      }

      public void gxTv_SdtParametrosSistema_Parametrossistema_flsevd_SetNull( )
      {
         gxTv_SdtParametrosSistema_Parametrossistema_flsevd_N = 1;
         gxTv_SdtParametrosSistema_Parametrossistema_flsevd = "";
         return  ;
      }

      public bool gxTv_SdtParametrosSistema_Parametrossistema_flsevd_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ParametrosSistema_URLApp" )]
      [  XmlElement( ElementName = "ParametrosSistema_URLApp"   )]
      public String gxTpr_Parametrossistema_urlapp
      {
         get {
            return gxTv_SdtParametrosSistema_Parametrossistema_urlapp ;
         }

         set {
            gxTv_SdtParametrosSistema_Parametrossistema_urlapp_N = 0;
            gxTv_SdtParametrosSistema_Parametrossistema_urlapp = (String)(value);
         }

      }

      public void gxTv_SdtParametrosSistema_Parametrossistema_urlapp_SetNull( )
      {
         gxTv_SdtParametrosSistema_Parametrossistema_urlapp_N = 1;
         gxTv_SdtParametrosSistema_Parametrossistema_urlapp = "";
         return  ;
      }

      public bool gxTv_SdtParametrosSistema_Parametrossistema_urlapp_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ParametrosSistema_URLOtherVer" )]
      [  XmlElement( ElementName = "ParametrosSistema_URLOtherVer"   )]
      public String gxTpr_Parametrossistema_urlotherver
      {
         get {
            return gxTv_SdtParametrosSistema_Parametrossistema_urlotherver ;
         }

         set {
            gxTv_SdtParametrosSistema_Parametrossistema_urlotherver_N = 0;
            gxTv_SdtParametrosSistema_Parametrossistema_urlotherver = (String)(value);
         }

      }

      public void gxTv_SdtParametrosSistema_Parametrossistema_urlotherver_SetNull( )
      {
         gxTv_SdtParametrosSistema_Parametrossistema_urlotherver_N = 1;
         gxTv_SdtParametrosSistema_Parametrossistema_urlotherver = "";
         return  ;
      }

      public bool gxTv_SdtParametrosSistema_Parametrossistema_urlotherver_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ParametrosSistema_Validacao" )]
      [  XmlElement( ElementName = "ParametrosSistema_Validacao"   )]
      public String gxTpr_Parametrossistema_validacao
      {
         get {
            return gxTv_SdtParametrosSistema_Parametrossistema_validacao ;
         }

         set {
            gxTv_SdtParametrosSistema_Parametrossistema_validacao_N = 0;
            gxTv_SdtParametrosSistema_Parametrossistema_validacao = (String)(value);
         }

      }

      public void gxTv_SdtParametrosSistema_Parametrossistema_validacao_SetNull( )
      {
         gxTv_SdtParametrosSistema_Parametrossistema_validacao_N = 1;
         gxTv_SdtParametrosSistema_Parametrossistema_validacao = "";
         return  ;
      }

      public bool gxTv_SdtParametrosSistema_Parametrossistema_validacao_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ParametrosSistema_Imagem" )]
      [  XmlElement( ElementName = "ParametrosSistema_Imagem"   )]
      [GxUpload()]
      public String gxTpr_Parametrossistema_imagem
      {
         get {
            return gxTv_SdtParametrosSistema_Parametrossistema_imagem ;
         }

         set {
            gxTv_SdtParametrosSistema_Parametrossistema_imagem_N = 0;
            gxTv_SdtParametrosSistema_Parametrossistema_imagem = (String)(value);
         }

      }

      public void gxTv_SdtParametrosSistema_Parametrossistema_imagem_SetNull( )
      {
         gxTv_SdtParametrosSistema_Parametrossistema_imagem_N = 1;
         gxTv_SdtParametrosSistema_Parametrossistema_imagem = "";
         return  ;
      }

      public bool gxTv_SdtParametrosSistema_Parametrossistema_imagem_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ParametrosSistema_Imagem_GXI" )]
      [  XmlElement( ElementName = "ParametrosSistema_Imagem_GXI"   )]
      public String gxTpr_Parametrossistema_imagem_gxi
      {
         get {
            return gxTv_SdtParametrosSistema_Parametrossistema_imagem_gxi ;
         }

         set {
            gxTv_SdtParametrosSistema_Parametrossistema_imagem_gxi_N = 0;
            gxTv_SdtParametrosSistema_Parametrossistema_imagem_gxi = (String)(value);
         }

      }

      public void gxTv_SdtParametrosSistema_Parametrossistema_imagem_gxi_SetNull( )
      {
         gxTv_SdtParametrosSistema_Parametrossistema_imagem_gxi_N = 1;
         gxTv_SdtParametrosSistema_Parametrossistema_imagem_gxi = "";
         return  ;
      }

      public bool gxTv_SdtParametrosSistema_Parametrossistema_imagem_gxi_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Mode" )]
      [  XmlElement( ElementName = "Mode"   )]
      public String gxTpr_Mode
      {
         get {
            return gxTv_SdtParametrosSistema_Mode ;
         }

         set {
            gxTv_SdtParametrosSistema_Mode = (String)(value);
         }

      }

      public void gxTv_SdtParametrosSistema_Mode_SetNull( )
      {
         gxTv_SdtParametrosSistema_Mode = "";
         return  ;
      }

      public bool gxTv_SdtParametrosSistema_Mode_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Initialized" )]
      [  XmlElement( ElementName = "Initialized"   )]
      public short gxTpr_Initialized
      {
         get {
            return gxTv_SdtParametrosSistema_Initialized ;
         }

         set {
            gxTv_SdtParametrosSistema_Initialized = (short)(value);
         }

      }

      public void gxTv_SdtParametrosSistema_Initialized_SetNull( )
      {
         gxTv_SdtParametrosSistema_Initialized = 0;
         return  ;
      }

      public bool gxTv_SdtParametrosSistema_Initialized_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ParametrosSistema_Codigo_Z" )]
      [  XmlElement( ElementName = "ParametrosSistema_Codigo_Z"   )]
      public int gxTpr_Parametrossistema_codigo_Z
      {
         get {
            return gxTv_SdtParametrosSistema_Parametrossistema_codigo_Z ;
         }

         set {
            gxTv_SdtParametrosSistema_Parametrossistema_codigo_Z = (int)(value);
         }

      }

      public void gxTv_SdtParametrosSistema_Parametrossistema_codigo_Z_SetNull( )
      {
         gxTv_SdtParametrosSistema_Parametrossistema_codigo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtParametrosSistema_Parametrossistema_codigo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ParametrosSistema_NomeSistema_Z" )]
      [  XmlElement( ElementName = "ParametrosSistema_NomeSistema_Z"   )]
      public String gxTpr_Parametrossistema_nomesistema_Z
      {
         get {
            return gxTv_SdtParametrosSistema_Parametrossistema_nomesistema_Z ;
         }

         set {
            gxTv_SdtParametrosSistema_Parametrossistema_nomesistema_Z = (String)(value);
         }

      }

      public void gxTv_SdtParametrosSistema_Parametrossistema_nomesistema_Z_SetNull( )
      {
         gxTv_SdtParametrosSistema_Parametrossistema_nomesistema_Z = "";
         return  ;
      }

      public bool gxTv_SdtParametrosSistema_Parametrossistema_nomesistema_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ParametrosSistema_AppID_Z" )]
      [  XmlElement( ElementName = "ParametrosSistema_AppID_Z"   )]
      public long gxTpr_Parametrossistema_appid_Z
      {
         get {
            return gxTv_SdtParametrosSistema_Parametrossistema_appid_Z ;
         }

         set {
            gxTv_SdtParametrosSistema_Parametrossistema_appid_Z = (long)(value);
         }

      }

      public void gxTv_SdtParametrosSistema_Parametrossistema_appid_Z_SetNull( )
      {
         gxTv_SdtParametrosSistema_Parametrossistema_appid_Z = 0;
         return  ;
      }

      public bool gxTv_SdtParametrosSistema_Parametrossistema_appid_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ParametrosSistema_LicensiadoCadastrado_Z" )]
      [  XmlElement( ElementName = "ParametrosSistema_LicensiadoCadastrado_Z"   )]
      public bool gxTpr_Parametrossistema_licensiadocadastrado_Z
      {
         get {
            return gxTv_SdtParametrosSistema_Parametrossistema_licensiadocadastrado_Z ;
         }

         set {
            gxTv_SdtParametrosSistema_Parametrossistema_licensiadocadastrado_Z = value;
         }

      }

      public void gxTv_SdtParametrosSistema_Parametrossistema_licensiadocadastrado_Z_SetNull( )
      {
         gxTv_SdtParametrosSistema_Parametrossistema_licensiadocadastrado_Z = false;
         return  ;
      }

      public bool gxTv_SdtParametrosSistema_Parametrossistema_licensiadocadastrado_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ParametrosSistema_PadronizarStrings_Z" )]
      [  XmlElement( ElementName = "ParametrosSistema_PadronizarStrings_Z"   )]
      public bool gxTpr_Parametrossistema_padronizarstrings_Z
      {
         get {
            return gxTv_SdtParametrosSistema_Parametrossistema_padronizarstrings_Z ;
         }

         set {
            gxTv_SdtParametrosSistema_Parametrossistema_padronizarstrings_Z = value;
         }

      }

      public void gxTv_SdtParametrosSistema_Parametrossistema_padronizarstrings_Z_SetNull( )
      {
         gxTv_SdtParametrosSistema_Parametrossistema_padronizarstrings_Z = false;
         return  ;
      }

      public bool gxTv_SdtParametrosSistema_Parametrossistema_padronizarstrings_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ParametrosSistema_EmailSdaHost_Z" )]
      [  XmlElement( ElementName = "ParametrosSistema_EmailSdaHost_Z"   )]
      public String gxTpr_Parametrossistema_emailsdahost_Z
      {
         get {
            return gxTv_SdtParametrosSistema_Parametrossistema_emailsdahost_Z ;
         }

         set {
            gxTv_SdtParametrosSistema_Parametrossistema_emailsdahost_Z = (String)(value);
         }

      }

      public void gxTv_SdtParametrosSistema_Parametrossistema_emailsdahost_Z_SetNull( )
      {
         gxTv_SdtParametrosSistema_Parametrossistema_emailsdahost_Z = "";
         return  ;
      }

      public bool gxTv_SdtParametrosSistema_Parametrossistema_emailsdahost_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ParametrosSistema_EmailSdaUser_Z" )]
      [  XmlElement( ElementName = "ParametrosSistema_EmailSdaUser_Z"   )]
      public String gxTpr_Parametrossistema_emailsdauser_Z
      {
         get {
            return gxTv_SdtParametrosSistema_Parametrossistema_emailsdauser_Z ;
         }

         set {
            gxTv_SdtParametrosSistema_Parametrossistema_emailsdauser_Z = (String)(value);
         }

      }

      public void gxTv_SdtParametrosSistema_Parametrossistema_emailsdauser_Z_SetNull( )
      {
         gxTv_SdtParametrosSistema_Parametrossistema_emailsdauser_Z = "";
         return  ;
      }

      public bool gxTv_SdtParametrosSistema_Parametrossistema_emailsdauser_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ParametrosSistema_EmailSdaPass_Z" )]
      [  XmlElement( ElementName = "ParametrosSistema_EmailSdaPass_Z"   )]
      public String gxTpr_Parametrossistema_emailsdapass_Z
      {
         get {
            return gxTv_SdtParametrosSistema_Parametrossistema_emailsdapass_Z ;
         }

         set {
            gxTv_SdtParametrosSistema_Parametrossistema_emailsdapass_Z = (String)(value);
         }

      }

      public void gxTv_SdtParametrosSistema_Parametrossistema_emailsdapass_Z_SetNull( )
      {
         gxTv_SdtParametrosSistema_Parametrossistema_emailsdapass_Z = "";
         return  ;
      }

      public bool gxTv_SdtParametrosSistema_Parametrossistema_emailsdapass_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ParametrosSistema_EmailSdaKey_Z" )]
      [  XmlElement( ElementName = "ParametrosSistema_EmailSdaKey_Z"   )]
      public String gxTpr_Parametrossistema_emailsdakey_Z
      {
         get {
            return gxTv_SdtParametrosSistema_Parametrossistema_emailsdakey_Z ;
         }

         set {
            gxTv_SdtParametrosSistema_Parametrossistema_emailsdakey_Z = (String)(value);
         }

      }

      public void gxTv_SdtParametrosSistema_Parametrossistema_emailsdakey_Z_SetNull( )
      {
         gxTv_SdtParametrosSistema_Parametrossistema_emailsdakey_Z = "";
         return  ;
      }

      public bool gxTv_SdtParametrosSistema_Parametrossistema_emailsdakey_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ParametrosSistema_EmailSdaAut_Z" )]
      [  XmlElement( ElementName = "ParametrosSistema_EmailSdaAut_Z"   )]
      public bool gxTpr_Parametrossistema_emailsdaaut_Z
      {
         get {
            return gxTv_SdtParametrosSistema_Parametrossistema_emailsdaaut_Z ;
         }

         set {
            gxTv_SdtParametrosSistema_Parametrossistema_emailsdaaut_Z = value;
         }

      }

      public void gxTv_SdtParametrosSistema_Parametrossistema_emailsdaaut_Z_SetNull( )
      {
         gxTv_SdtParametrosSistema_Parametrossistema_emailsdaaut_Z = false;
         return  ;
      }

      public bool gxTv_SdtParametrosSistema_Parametrossistema_emailsdaaut_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ParametrosSistema_EmailSdaPort_Z" )]
      [  XmlElement( ElementName = "ParametrosSistema_EmailSdaPort_Z"   )]
      public short gxTpr_Parametrossistema_emailsdaport_Z
      {
         get {
            return gxTv_SdtParametrosSistema_Parametrossistema_emailsdaport_Z ;
         }

         set {
            gxTv_SdtParametrosSistema_Parametrossistema_emailsdaport_Z = (short)(value);
         }

      }

      public void gxTv_SdtParametrosSistema_Parametrossistema_emailsdaport_Z_SetNull( )
      {
         gxTv_SdtParametrosSistema_Parametrossistema_emailsdaport_Z = 0;
         return  ;
      }

      public bool gxTv_SdtParametrosSistema_Parametrossistema_emailsdaport_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ParametrosSistema_EmailSdaSec_Z" )]
      [  XmlElement( ElementName = "ParametrosSistema_EmailSdaSec_Z"   )]
      public short gxTpr_Parametrossistema_emailsdasec_Z
      {
         get {
            return gxTv_SdtParametrosSistema_Parametrossistema_emailsdasec_Z ;
         }

         set {
            gxTv_SdtParametrosSistema_Parametrossistema_emailsdasec_Z = (short)(value);
         }

      }

      public void gxTv_SdtParametrosSistema_Parametrossistema_emailsdasec_Z_SetNull( )
      {
         gxTv_SdtParametrosSistema_Parametrossistema_emailsdasec_Z = 0;
         return  ;
      }

      public bool gxTv_SdtParametrosSistema_Parametrossistema_emailsdasec_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ParametrosSistema_FatorAjuste_Z" )]
      [  XmlElement( ElementName = "ParametrosSistema_FatorAjuste_Z"   )]
      public double gxTpr_Parametrossistema_fatorajuste_Z_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtParametrosSistema_Parametrossistema_fatorajuste_Z) ;
         }

         set {
            gxTv_SdtParametrosSistema_Parametrossistema_fatorajuste_Z = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Parametrossistema_fatorajuste_Z
      {
         get {
            return gxTv_SdtParametrosSistema_Parametrossistema_fatorajuste_Z ;
         }

         set {
            gxTv_SdtParametrosSistema_Parametrossistema_fatorajuste_Z = (decimal)(value);
         }

      }

      public void gxTv_SdtParametrosSistema_Parametrossistema_fatorajuste_Z_SetNull( )
      {
         gxTv_SdtParametrosSistema_Parametrossistema_fatorajuste_Z = 0;
         return  ;
      }

      public bool gxTv_SdtParametrosSistema_Parametrossistema_fatorajuste_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ParametrosSistema_PathCrtf_Z" )]
      [  XmlElement( ElementName = "ParametrosSistema_PathCrtf_Z"   )]
      public String gxTpr_Parametrossistema_pathcrtf_Z
      {
         get {
            return gxTv_SdtParametrosSistema_Parametrossistema_pathcrtf_Z ;
         }

         set {
            gxTv_SdtParametrosSistema_Parametrossistema_pathcrtf_Z = (String)(value);
         }

      }

      public void gxTv_SdtParametrosSistema_Parametrossistema_pathcrtf_Z_SetNull( )
      {
         gxTv_SdtParametrosSistema_Parametrossistema_pathcrtf_Z = "";
         return  ;
      }

      public bool gxTv_SdtParametrosSistema_Parametrossistema_pathcrtf_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ParametrosSistema_HostMensuracao_Z" )]
      [  XmlElement( ElementName = "ParametrosSistema_HostMensuracao_Z"   )]
      public String gxTpr_Parametrossistema_hostmensuracao_Z
      {
         get {
            return gxTv_SdtParametrosSistema_Parametrossistema_hostmensuracao_Z ;
         }

         set {
            gxTv_SdtParametrosSistema_Parametrossistema_hostmensuracao_Z = (String)(value);
         }

      }

      public void gxTv_SdtParametrosSistema_Parametrossistema_hostmensuracao_Z_SetNull( )
      {
         gxTv_SdtParametrosSistema_Parametrossistema_hostmensuracao_Z = "";
         return  ;
      }

      public bool gxTv_SdtParametrosSistema_Parametrossistema_hostmensuracao_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ParametrosSistema_SQLComputerName_Z" )]
      [  XmlElement( ElementName = "ParametrosSistema_SQLComputerName_Z"   )]
      public String gxTpr_Parametrossistema_sqlcomputername_Z
      {
         get {
            return gxTv_SdtParametrosSistema_Parametrossistema_sqlcomputername_Z ;
         }

         set {
            gxTv_SdtParametrosSistema_Parametrossistema_sqlcomputername_Z = (String)(value);
         }

      }

      public void gxTv_SdtParametrosSistema_Parametrossistema_sqlcomputername_Z_SetNull( )
      {
         gxTv_SdtParametrosSistema_Parametrossistema_sqlcomputername_Z = "";
         return  ;
      }

      public bool gxTv_SdtParametrosSistema_Parametrossistema_sqlcomputername_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ParametrosSistema_SQLServerName_Z" )]
      [  XmlElement( ElementName = "ParametrosSistema_SQLServerName_Z"   )]
      public String gxTpr_Parametrossistema_sqlservername_Z
      {
         get {
            return gxTv_SdtParametrosSistema_Parametrossistema_sqlservername_Z ;
         }

         set {
            gxTv_SdtParametrosSistema_Parametrossistema_sqlservername_Z = (String)(value);
         }

      }

      public void gxTv_SdtParametrosSistema_Parametrossistema_sqlservername_Z_SetNull( )
      {
         gxTv_SdtParametrosSistema_Parametrossistema_sqlservername_Z = "";
         return  ;
      }

      public bool gxTv_SdtParametrosSistema_Parametrossistema_sqlservername_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ParametrosSistema_SQLEdition_Z" )]
      [  XmlElement( ElementName = "ParametrosSistema_SQLEdition_Z"   )]
      public String gxTpr_Parametrossistema_sqledition_Z
      {
         get {
            return gxTv_SdtParametrosSistema_Parametrossistema_sqledition_Z ;
         }

         set {
            gxTv_SdtParametrosSistema_Parametrossistema_sqledition_Z = (String)(value);
         }

      }

      public void gxTv_SdtParametrosSistema_Parametrossistema_sqledition_Z_SetNull( )
      {
         gxTv_SdtParametrosSistema_Parametrossistema_sqledition_Z = "";
         return  ;
      }

      public bool gxTv_SdtParametrosSistema_Parametrossistema_sqledition_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ParametrosSistema_SQLInstance_Z" )]
      [  XmlElement( ElementName = "ParametrosSistema_SQLInstance_Z"   )]
      public String gxTpr_Parametrossistema_sqlinstance_Z
      {
         get {
            return gxTv_SdtParametrosSistema_Parametrossistema_sqlinstance_Z ;
         }

         set {
            gxTv_SdtParametrosSistema_Parametrossistema_sqlinstance_Z = (String)(value);
         }

      }

      public void gxTv_SdtParametrosSistema_Parametrossistema_sqlinstance_Z_SetNull( )
      {
         gxTv_SdtParametrosSistema_Parametrossistema_sqlinstance_Z = "";
         return  ;
      }

      public bool gxTv_SdtParametrosSistema_Parametrossistema_sqlinstance_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ParametrosSistema_DBName_Z" )]
      [  XmlElement( ElementName = "ParametrosSistema_DBName_Z"   )]
      public String gxTpr_Parametrossistema_dbname_Z
      {
         get {
            return gxTv_SdtParametrosSistema_Parametrossistema_dbname_Z ;
         }

         set {
            gxTv_SdtParametrosSistema_Parametrossistema_dbname_Z = (String)(value);
         }

      }

      public void gxTv_SdtParametrosSistema_Parametrossistema_dbname_Z_SetNull( )
      {
         gxTv_SdtParametrosSistema_Parametrossistema_dbname_Z = "";
         return  ;
      }

      public bool gxTv_SdtParametrosSistema_Parametrossistema_dbname_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ParametrosSistema_FlsEvd_Z" )]
      [  XmlElement( ElementName = "ParametrosSistema_FlsEvd_Z"   )]
      public String gxTpr_Parametrossistema_flsevd_Z
      {
         get {
            return gxTv_SdtParametrosSistema_Parametrossistema_flsevd_Z ;
         }

         set {
            gxTv_SdtParametrosSistema_Parametrossistema_flsevd_Z = (String)(value);
         }

      }

      public void gxTv_SdtParametrosSistema_Parametrossistema_flsevd_Z_SetNull( )
      {
         gxTv_SdtParametrosSistema_Parametrossistema_flsevd_Z = "";
         return  ;
      }

      public bool gxTv_SdtParametrosSistema_Parametrossistema_flsevd_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ParametrosSistema_URLApp_Z" )]
      [  XmlElement( ElementName = "ParametrosSistema_URLApp_Z"   )]
      public String gxTpr_Parametrossistema_urlapp_Z
      {
         get {
            return gxTv_SdtParametrosSistema_Parametrossistema_urlapp_Z ;
         }

         set {
            gxTv_SdtParametrosSistema_Parametrossistema_urlapp_Z = (String)(value);
         }

      }

      public void gxTv_SdtParametrosSistema_Parametrossistema_urlapp_Z_SetNull( )
      {
         gxTv_SdtParametrosSistema_Parametrossistema_urlapp_Z = "";
         return  ;
      }

      public bool gxTv_SdtParametrosSistema_Parametrossistema_urlapp_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ParametrosSistema_URLOtherVer_Z" )]
      [  XmlElement( ElementName = "ParametrosSistema_URLOtherVer_Z"   )]
      public String gxTpr_Parametrossistema_urlotherver_Z
      {
         get {
            return gxTv_SdtParametrosSistema_Parametrossistema_urlotherver_Z ;
         }

         set {
            gxTv_SdtParametrosSistema_Parametrossistema_urlotherver_Z = (String)(value);
         }

      }

      public void gxTv_SdtParametrosSistema_Parametrossistema_urlotherver_Z_SetNull( )
      {
         gxTv_SdtParametrosSistema_Parametrossistema_urlotherver_Z = "";
         return  ;
      }

      public bool gxTv_SdtParametrosSistema_Parametrossistema_urlotherver_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ParametrosSistema_Validacao_Z" )]
      [  XmlElement( ElementName = "ParametrosSistema_Validacao_Z"   )]
      public String gxTpr_Parametrossistema_validacao_Z
      {
         get {
            return gxTv_SdtParametrosSistema_Parametrossistema_validacao_Z ;
         }

         set {
            gxTv_SdtParametrosSistema_Parametrossistema_validacao_Z = (String)(value);
         }

      }

      public void gxTv_SdtParametrosSistema_Parametrossistema_validacao_Z_SetNull( )
      {
         gxTv_SdtParametrosSistema_Parametrossistema_validacao_Z = "";
         return  ;
      }

      public bool gxTv_SdtParametrosSistema_Parametrossistema_validacao_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ParametrosSistema_Imagem_GXI_Z" )]
      [  XmlElement( ElementName = "ParametrosSistema_Imagem_GXI_Z"   )]
      public String gxTpr_Parametrossistema_imagem_gxi_Z
      {
         get {
            return gxTv_SdtParametrosSistema_Parametrossistema_imagem_gxi_Z ;
         }

         set {
            gxTv_SdtParametrosSistema_Parametrossistema_imagem_gxi_Z = (String)(value);
         }

      }

      public void gxTv_SdtParametrosSistema_Parametrossistema_imagem_gxi_Z_SetNull( )
      {
         gxTv_SdtParametrosSistema_Parametrossistema_imagem_gxi_Z = "";
         return  ;
      }

      public bool gxTv_SdtParametrosSistema_Parametrossistema_imagem_gxi_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ParametrosSistema_LicensiadoCadastrado_N" )]
      [  XmlElement( ElementName = "ParametrosSistema_LicensiadoCadastrado_N"   )]
      public short gxTpr_Parametrossistema_licensiadocadastrado_N
      {
         get {
            return gxTv_SdtParametrosSistema_Parametrossistema_licensiadocadastrado_N ;
         }

         set {
            gxTv_SdtParametrosSistema_Parametrossistema_licensiadocadastrado_N = (short)(value);
         }

      }

      public void gxTv_SdtParametrosSistema_Parametrossistema_licensiadocadastrado_N_SetNull( )
      {
         gxTv_SdtParametrosSistema_Parametrossistema_licensiadocadastrado_N = 0;
         return  ;
      }

      public bool gxTv_SdtParametrosSistema_Parametrossistema_licensiadocadastrado_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ParametrosSistema_EmailSdaHost_N" )]
      [  XmlElement( ElementName = "ParametrosSistema_EmailSdaHost_N"   )]
      public short gxTpr_Parametrossistema_emailsdahost_N
      {
         get {
            return gxTv_SdtParametrosSistema_Parametrossistema_emailsdahost_N ;
         }

         set {
            gxTv_SdtParametrosSistema_Parametrossistema_emailsdahost_N = (short)(value);
         }

      }

      public void gxTv_SdtParametrosSistema_Parametrossistema_emailsdahost_N_SetNull( )
      {
         gxTv_SdtParametrosSistema_Parametrossistema_emailsdahost_N = 0;
         return  ;
      }

      public bool gxTv_SdtParametrosSistema_Parametrossistema_emailsdahost_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ParametrosSistema_EmailSdaUser_N" )]
      [  XmlElement( ElementName = "ParametrosSistema_EmailSdaUser_N"   )]
      public short gxTpr_Parametrossistema_emailsdauser_N
      {
         get {
            return gxTv_SdtParametrosSistema_Parametrossistema_emailsdauser_N ;
         }

         set {
            gxTv_SdtParametrosSistema_Parametrossistema_emailsdauser_N = (short)(value);
         }

      }

      public void gxTv_SdtParametrosSistema_Parametrossistema_emailsdauser_N_SetNull( )
      {
         gxTv_SdtParametrosSistema_Parametrossistema_emailsdauser_N = 0;
         return  ;
      }

      public bool gxTv_SdtParametrosSistema_Parametrossistema_emailsdauser_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ParametrosSistema_EmailSdaPass_N" )]
      [  XmlElement( ElementName = "ParametrosSistema_EmailSdaPass_N"   )]
      public short gxTpr_Parametrossistema_emailsdapass_N
      {
         get {
            return gxTv_SdtParametrosSistema_Parametrossistema_emailsdapass_N ;
         }

         set {
            gxTv_SdtParametrosSistema_Parametrossistema_emailsdapass_N = (short)(value);
         }

      }

      public void gxTv_SdtParametrosSistema_Parametrossistema_emailsdapass_N_SetNull( )
      {
         gxTv_SdtParametrosSistema_Parametrossistema_emailsdapass_N = 0;
         return  ;
      }

      public bool gxTv_SdtParametrosSistema_Parametrossistema_emailsdapass_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ParametrosSistema_EmailSdaKey_N" )]
      [  XmlElement( ElementName = "ParametrosSistema_EmailSdaKey_N"   )]
      public short gxTpr_Parametrossistema_emailsdakey_N
      {
         get {
            return gxTv_SdtParametrosSistema_Parametrossistema_emailsdakey_N ;
         }

         set {
            gxTv_SdtParametrosSistema_Parametrossistema_emailsdakey_N = (short)(value);
         }

      }

      public void gxTv_SdtParametrosSistema_Parametrossistema_emailsdakey_N_SetNull( )
      {
         gxTv_SdtParametrosSistema_Parametrossistema_emailsdakey_N = 0;
         return  ;
      }

      public bool gxTv_SdtParametrosSistema_Parametrossistema_emailsdakey_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ParametrosSistema_EmailSdaAut_N" )]
      [  XmlElement( ElementName = "ParametrosSistema_EmailSdaAut_N"   )]
      public short gxTpr_Parametrossistema_emailsdaaut_N
      {
         get {
            return gxTv_SdtParametrosSistema_Parametrossistema_emailsdaaut_N ;
         }

         set {
            gxTv_SdtParametrosSistema_Parametrossistema_emailsdaaut_N = (short)(value);
         }

      }

      public void gxTv_SdtParametrosSistema_Parametrossistema_emailsdaaut_N_SetNull( )
      {
         gxTv_SdtParametrosSistema_Parametrossistema_emailsdaaut_N = 0;
         return  ;
      }

      public bool gxTv_SdtParametrosSistema_Parametrossistema_emailsdaaut_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ParametrosSistema_EmailSdaPort_N" )]
      [  XmlElement( ElementName = "ParametrosSistema_EmailSdaPort_N"   )]
      public short gxTpr_Parametrossistema_emailsdaport_N
      {
         get {
            return gxTv_SdtParametrosSistema_Parametrossistema_emailsdaport_N ;
         }

         set {
            gxTv_SdtParametrosSistema_Parametrossistema_emailsdaport_N = (short)(value);
         }

      }

      public void gxTv_SdtParametrosSistema_Parametrossistema_emailsdaport_N_SetNull( )
      {
         gxTv_SdtParametrosSistema_Parametrossistema_emailsdaport_N = 0;
         return  ;
      }

      public bool gxTv_SdtParametrosSistema_Parametrossistema_emailsdaport_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ParametrosSistema_EmailSdaSec_N" )]
      [  XmlElement( ElementName = "ParametrosSistema_EmailSdaSec_N"   )]
      public short gxTpr_Parametrossistema_emailsdasec_N
      {
         get {
            return gxTv_SdtParametrosSistema_Parametrossistema_emailsdasec_N ;
         }

         set {
            gxTv_SdtParametrosSistema_Parametrossistema_emailsdasec_N = (short)(value);
         }

      }

      public void gxTv_SdtParametrosSistema_Parametrossistema_emailsdasec_N_SetNull( )
      {
         gxTv_SdtParametrosSistema_Parametrossistema_emailsdasec_N = 0;
         return  ;
      }

      public bool gxTv_SdtParametrosSistema_Parametrossistema_emailsdasec_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ParametrosSistema_FatorAjuste_N" )]
      [  XmlElement( ElementName = "ParametrosSistema_FatorAjuste_N"   )]
      public short gxTpr_Parametrossistema_fatorajuste_N
      {
         get {
            return gxTv_SdtParametrosSistema_Parametrossistema_fatorajuste_N ;
         }

         set {
            gxTv_SdtParametrosSistema_Parametrossistema_fatorajuste_N = (short)(value);
         }

      }

      public void gxTv_SdtParametrosSistema_Parametrossistema_fatorajuste_N_SetNull( )
      {
         gxTv_SdtParametrosSistema_Parametrossistema_fatorajuste_N = 0;
         return  ;
      }

      public bool gxTv_SdtParametrosSistema_Parametrossistema_fatorajuste_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ParametrosSistema_PathCrtf_N" )]
      [  XmlElement( ElementName = "ParametrosSistema_PathCrtf_N"   )]
      public short gxTpr_Parametrossistema_pathcrtf_N
      {
         get {
            return gxTv_SdtParametrosSistema_Parametrossistema_pathcrtf_N ;
         }

         set {
            gxTv_SdtParametrosSistema_Parametrossistema_pathcrtf_N = (short)(value);
         }

      }

      public void gxTv_SdtParametrosSistema_Parametrossistema_pathcrtf_N_SetNull( )
      {
         gxTv_SdtParametrosSistema_Parametrossistema_pathcrtf_N = 0;
         return  ;
      }

      public bool gxTv_SdtParametrosSistema_Parametrossistema_pathcrtf_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ParametrosSistema_HostMensuracao_N" )]
      [  XmlElement( ElementName = "ParametrosSistema_HostMensuracao_N"   )]
      public short gxTpr_Parametrossistema_hostmensuracao_N
      {
         get {
            return gxTv_SdtParametrosSistema_Parametrossistema_hostmensuracao_N ;
         }

         set {
            gxTv_SdtParametrosSistema_Parametrossistema_hostmensuracao_N = (short)(value);
         }

      }

      public void gxTv_SdtParametrosSistema_Parametrossistema_hostmensuracao_N_SetNull( )
      {
         gxTv_SdtParametrosSistema_Parametrossistema_hostmensuracao_N = 0;
         return  ;
      }

      public bool gxTv_SdtParametrosSistema_Parametrossistema_hostmensuracao_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ParametrosSistema_SQLComputerName_N" )]
      [  XmlElement( ElementName = "ParametrosSistema_SQLComputerName_N"   )]
      public short gxTpr_Parametrossistema_sqlcomputername_N
      {
         get {
            return gxTv_SdtParametrosSistema_Parametrossistema_sqlcomputername_N ;
         }

         set {
            gxTv_SdtParametrosSistema_Parametrossistema_sqlcomputername_N = (short)(value);
         }

      }

      public void gxTv_SdtParametrosSistema_Parametrossistema_sqlcomputername_N_SetNull( )
      {
         gxTv_SdtParametrosSistema_Parametrossistema_sqlcomputername_N = 0;
         return  ;
      }

      public bool gxTv_SdtParametrosSistema_Parametrossistema_sqlcomputername_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ParametrosSistema_SQLServerName_N" )]
      [  XmlElement( ElementName = "ParametrosSistema_SQLServerName_N"   )]
      public short gxTpr_Parametrossistema_sqlservername_N
      {
         get {
            return gxTv_SdtParametrosSistema_Parametrossistema_sqlservername_N ;
         }

         set {
            gxTv_SdtParametrosSistema_Parametrossistema_sqlservername_N = (short)(value);
         }

      }

      public void gxTv_SdtParametrosSistema_Parametrossistema_sqlservername_N_SetNull( )
      {
         gxTv_SdtParametrosSistema_Parametrossistema_sqlservername_N = 0;
         return  ;
      }

      public bool gxTv_SdtParametrosSistema_Parametrossistema_sqlservername_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ParametrosSistema_SQLEdition_N" )]
      [  XmlElement( ElementName = "ParametrosSistema_SQLEdition_N"   )]
      public short gxTpr_Parametrossistema_sqledition_N
      {
         get {
            return gxTv_SdtParametrosSistema_Parametrossistema_sqledition_N ;
         }

         set {
            gxTv_SdtParametrosSistema_Parametrossistema_sqledition_N = (short)(value);
         }

      }

      public void gxTv_SdtParametrosSistema_Parametrossistema_sqledition_N_SetNull( )
      {
         gxTv_SdtParametrosSistema_Parametrossistema_sqledition_N = 0;
         return  ;
      }

      public bool gxTv_SdtParametrosSistema_Parametrossistema_sqledition_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ParametrosSistema_SQLInstance_N" )]
      [  XmlElement( ElementName = "ParametrosSistema_SQLInstance_N"   )]
      public short gxTpr_Parametrossistema_sqlinstance_N
      {
         get {
            return gxTv_SdtParametrosSistema_Parametrossistema_sqlinstance_N ;
         }

         set {
            gxTv_SdtParametrosSistema_Parametrossistema_sqlinstance_N = (short)(value);
         }

      }

      public void gxTv_SdtParametrosSistema_Parametrossistema_sqlinstance_N_SetNull( )
      {
         gxTv_SdtParametrosSistema_Parametrossistema_sqlinstance_N = 0;
         return  ;
      }

      public bool gxTv_SdtParametrosSistema_Parametrossistema_sqlinstance_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ParametrosSistema_DBName_N" )]
      [  XmlElement( ElementName = "ParametrosSistema_DBName_N"   )]
      public short gxTpr_Parametrossistema_dbname_N
      {
         get {
            return gxTv_SdtParametrosSistema_Parametrossistema_dbname_N ;
         }

         set {
            gxTv_SdtParametrosSistema_Parametrossistema_dbname_N = (short)(value);
         }

      }

      public void gxTv_SdtParametrosSistema_Parametrossistema_dbname_N_SetNull( )
      {
         gxTv_SdtParametrosSistema_Parametrossistema_dbname_N = 0;
         return  ;
      }

      public bool gxTv_SdtParametrosSistema_Parametrossistema_dbname_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ParametrosSistema_FlsEvd_N" )]
      [  XmlElement( ElementName = "ParametrosSistema_FlsEvd_N"   )]
      public short gxTpr_Parametrossistema_flsevd_N
      {
         get {
            return gxTv_SdtParametrosSistema_Parametrossistema_flsevd_N ;
         }

         set {
            gxTv_SdtParametrosSistema_Parametrossistema_flsevd_N = (short)(value);
         }

      }

      public void gxTv_SdtParametrosSistema_Parametrossistema_flsevd_N_SetNull( )
      {
         gxTv_SdtParametrosSistema_Parametrossistema_flsevd_N = 0;
         return  ;
      }

      public bool gxTv_SdtParametrosSistema_Parametrossistema_flsevd_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ParametrosSistema_URLApp_N" )]
      [  XmlElement( ElementName = "ParametrosSistema_URLApp_N"   )]
      public short gxTpr_Parametrossistema_urlapp_N
      {
         get {
            return gxTv_SdtParametrosSistema_Parametrossistema_urlapp_N ;
         }

         set {
            gxTv_SdtParametrosSistema_Parametrossistema_urlapp_N = (short)(value);
         }

      }

      public void gxTv_SdtParametrosSistema_Parametrossistema_urlapp_N_SetNull( )
      {
         gxTv_SdtParametrosSistema_Parametrossistema_urlapp_N = 0;
         return  ;
      }

      public bool gxTv_SdtParametrosSistema_Parametrossistema_urlapp_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ParametrosSistema_URLOtherVer_N" )]
      [  XmlElement( ElementName = "ParametrosSistema_URLOtherVer_N"   )]
      public short gxTpr_Parametrossistema_urlotherver_N
      {
         get {
            return gxTv_SdtParametrosSistema_Parametrossistema_urlotherver_N ;
         }

         set {
            gxTv_SdtParametrosSistema_Parametrossistema_urlotherver_N = (short)(value);
         }

      }

      public void gxTv_SdtParametrosSistema_Parametrossistema_urlotherver_N_SetNull( )
      {
         gxTv_SdtParametrosSistema_Parametrossistema_urlotherver_N = 0;
         return  ;
      }

      public bool gxTv_SdtParametrosSistema_Parametrossistema_urlotherver_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ParametrosSistema_Validacao_N" )]
      [  XmlElement( ElementName = "ParametrosSistema_Validacao_N"   )]
      public short gxTpr_Parametrossistema_validacao_N
      {
         get {
            return gxTv_SdtParametrosSistema_Parametrossistema_validacao_N ;
         }

         set {
            gxTv_SdtParametrosSistema_Parametrossistema_validacao_N = (short)(value);
         }

      }

      public void gxTv_SdtParametrosSistema_Parametrossistema_validacao_N_SetNull( )
      {
         gxTv_SdtParametrosSistema_Parametrossistema_validacao_N = 0;
         return  ;
      }

      public bool gxTv_SdtParametrosSistema_Parametrossistema_validacao_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ParametrosSistema_Imagem_N" )]
      [  XmlElement( ElementName = "ParametrosSistema_Imagem_N"   )]
      public short gxTpr_Parametrossistema_imagem_N
      {
         get {
            return gxTv_SdtParametrosSistema_Parametrossistema_imagem_N ;
         }

         set {
            gxTv_SdtParametrosSistema_Parametrossistema_imagem_N = (short)(value);
         }

      }

      public void gxTv_SdtParametrosSistema_Parametrossistema_imagem_N_SetNull( )
      {
         gxTv_SdtParametrosSistema_Parametrossistema_imagem_N = 0;
         return  ;
      }

      public bool gxTv_SdtParametrosSistema_Parametrossistema_imagem_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ParametrosSistema_Imagem_GXI_N" )]
      [  XmlElement( ElementName = "ParametrosSistema_Imagem_GXI_N"   )]
      public short gxTpr_Parametrossistema_imagem_gxi_N
      {
         get {
            return gxTv_SdtParametrosSistema_Parametrossistema_imagem_gxi_N ;
         }

         set {
            gxTv_SdtParametrosSistema_Parametrossistema_imagem_gxi_N = (short)(value);
         }

      }

      public void gxTv_SdtParametrosSistema_Parametrossistema_imagem_gxi_N_SetNull( )
      {
         gxTv_SdtParametrosSistema_Parametrossistema_imagem_gxi_N = 0;
         return  ;
      }

      public bool gxTv_SdtParametrosSistema_Parametrossistema_imagem_gxi_N_IsNull( )
      {
         return false ;
      }

      public void initialize( )
      {
         gxTv_SdtParametrosSistema_Parametrossistema_nomesistema = "";
         gxTv_SdtParametrosSistema_Parametrossistema_textohome = "";
         gxTv_SdtParametrosSistema_Parametrossistema_emailsdahost = "";
         gxTv_SdtParametrosSistema_Parametrossistema_emailsdauser = "";
         gxTv_SdtParametrosSistema_Parametrossistema_emailsdapass = "";
         gxTv_SdtParametrosSistema_Parametrossistema_emailsdakey = "";
         gxTv_SdtParametrosSistema_Parametrossistema_pathcrtf = "";
         gxTv_SdtParametrosSistema_Parametrossistema_hostmensuracao = "";
         gxTv_SdtParametrosSistema_Parametrossistema_sqlcomputername = "";
         gxTv_SdtParametrosSistema_Parametrossistema_sqlservername = "";
         gxTv_SdtParametrosSistema_Parametrossistema_sqledition = "";
         gxTv_SdtParametrosSistema_Parametrossistema_sqlinstance = "";
         gxTv_SdtParametrosSistema_Parametrossistema_dbname = "";
         gxTv_SdtParametrosSistema_Parametrossistema_flsevd = "";
         gxTv_SdtParametrosSistema_Parametrossistema_urlapp = "";
         gxTv_SdtParametrosSistema_Parametrossistema_urlotherver = "";
         gxTv_SdtParametrosSistema_Parametrossistema_validacao = "";
         gxTv_SdtParametrosSistema_Parametrossistema_imagem = "";
         gxTv_SdtParametrosSistema_Parametrossistema_imagem_gxi = "";
         gxTv_SdtParametrosSistema_Mode = "";
         gxTv_SdtParametrosSistema_Parametrossistema_nomesistema_Z = "";
         gxTv_SdtParametrosSistema_Parametrossistema_emailsdahost_Z = "";
         gxTv_SdtParametrosSistema_Parametrossistema_emailsdauser_Z = "";
         gxTv_SdtParametrosSistema_Parametrossistema_emailsdapass_Z = "";
         gxTv_SdtParametrosSistema_Parametrossistema_emailsdakey_Z = "";
         gxTv_SdtParametrosSistema_Parametrossistema_pathcrtf_Z = "";
         gxTv_SdtParametrosSistema_Parametrossistema_hostmensuracao_Z = "";
         gxTv_SdtParametrosSistema_Parametrossistema_sqlcomputername_Z = "";
         gxTv_SdtParametrosSistema_Parametrossistema_sqlservername_Z = "";
         gxTv_SdtParametrosSistema_Parametrossistema_sqledition_Z = "";
         gxTv_SdtParametrosSistema_Parametrossistema_sqlinstance_Z = "";
         gxTv_SdtParametrosSistema_Parametrossistema_dbname_Z = "";
         gxTv_SdtParametrosSistema_Parametrossistema_flsevd_Z = "";
         gxTv_SdtParametrosSistema_Parametrossistema_urlapp_Z = "";
         gxTv_SdtParametrosSistema_Parametrossistema_urlotherver_Z = "";
         gxTv_SdtParametrosSistema_Parametrossistema_validacao_Z = "";
         gxTv_SdtParametrosSistema_Parametrossistema_imagem_gxi_Z = "";
         gxTv_SdtParametrosSistema_Parametrossistema_licensiadocadastrado = false;
         gxTv_SdtParametrosSistema_Parametrossistema_padronizarstrings = true;
         gxTv_SdtParametrosSistema_Parametrossistema_emailsdaaut = false;
         gxTv_SdtParametrosSistema_Parametrossistema_fatorajuste = (decimal)(1);
         sTagName = "";
         IGxSilentTrn obj ;
         obj = (IGxSilentTrn)ClassLoader.FindInstance( "parametrossistema", "GeneXus.Programs.parametrossistema_bc", new Object[] {context}, constructorCallingAssembly);
         obj.initialize();
         obj.SetSDT(this, 1);
         setTransaction( obj) ;
         obj.SetMode("INS");
         return  ;
      }

      private short gxTv_SdtParametrosSistema_Parametrossistema_emailsdaport ;
      private short gxTv_SdtParametrosSistema_Parametrossistema_emailsdasec ;
      private short gxTv_SdtParametrosSistema_Initialized ;
      private short gxTv_SdtParametrosSistema_Parametrossistema_emailsdaport_Z ;
      private short gxTv_SdtParametrosSistema_Parametrossistema_emailsdasec_Z ;
      private short gxTv_SdtParametrosSistema_Parametrossistema_licensiadocadastrado_N ;
      private short gxTv_SdtParametrosSistema_Parametrossistema_emailsdahost_N ;
      private short gxTv_SdtParametrosSistema_Parametrossistema_emailsdauser_N ;
      private short gxTv_SdtParametrosSistema_Parametrossistema_emailsdapass_N ;
      private short gxTv_SdtParametrosSistema_Parametrossistema_emailsdakey_N ;
      private short gxTv_SdtParametrosSistema_Parametrossistema_emailsdaaut_N ;
      private short gxTv_SdtParametrosSistema_Parametrossistema_emailsdaport_N ;
      private short gxTv_SdtParametrosSistema_Parametrossistema_emailsdasec_N ;
      private short gxTv_SdtParametrosSistema_Parametrossistema_fatorajuste_N ;
      private short gxTv_SdtParametrosSistema_Parametrossistema_pathcrtf_N ;
      private short gxTv_SdtParametrosSistema_Parametrossistema_hostmensuracao_N ;
      private short gxTv_SdtParametrosSistema_Parametrossistema_sqlcomputername_N ;
      private short gxTv_SdtParametrosSistema_Parametrossistema_sqlservername_N ;
      private short gxTv_SdtParametrosSistema_Parametrossistema_sqledition_N ;
      private short gxTv_SdtParametrosSistema_Parametrossistema_sqlinstance_N ;
      private short gxTv_SdtParametrosSistema_Parametrossistema_dbname_N ;
      private short gxTv_SdtParametrosSistema_Parametrossistema_flsevd_N ;
      private short gxTv_SdtParametrosSistema_Parametrossistema_urlapp_N ;
      private short gxTv_SdtParametrosSistema_Parametrossistema_urlotherver_N ;
      private short gxTv_SdtParametrosSistema_Parametrossistema_validacao_N ;
      private short gxTv_SdtParametrosSistema_Parametrossistema_imagem_N ;
      private short gxTv_SdtParametrosSistema_Parametrossistema_imagem_gxi_N ;
      private short readOk ;
      private short nOutParmCount ;
      private int gxTv_SdtParametrosSistema_Parametrossistema_codigo ;
      private int gxTv_SdtParametrosSistema_Parametrossistema_codigo_Z ;
      private long gxTv_SdtParametrosSistema_Parametrossistema_appid ;
      private long gxTv_SdtParametrosSistema_Parametrossistema_appid_Z ;
      private decimal gxTv_SdtParametrosSistema_Parametrossistema_fatorajuste ;
      private decimal gxTv_SdtParametrosSistema_Parametrossistema_fatorajuste_Z ;
      private String gxTv_SdtParametrosSistema_Parametrossistema_nomesistema ;
      private String gxTv_SdtParametrosSistema_Parametrossistema_emailsdakey ;
      private String gxTv_SdtParametrosSistema_Mode ;
      private String gxTv_SdtParametrosSistema_Parametrossistema_nomesistema_Z ;
      private String gxTv_SdtParametrosSistema_Parametrossistema_emailsdakey_Z ;
      private String sTagName ;
      private bool gxTv_SdtParametrosSistema_Parametrossistema_licensiadocadastrado ;
      private bool gxTv_SdtParametrosSistema_Parametrossistema_padronizarstrings ;
      private bool gxTv_SdtParametrosSistema_Parametrossistema_emailsdaaut ;
      private bool gxTv_SdtParametrosSistema_Parametrossistema_licensiadocadastrado_Z ;
      private bool gxTv_SdtParametrosSistema_Parametrossistema_padronizarstrings_Z ;
      private bool gxTv_SdtParametrosSistema_Parametrossistema_emailsdaaut_Z ;
      private String gxTv_SdtParametrosSistema_Parametrossistema_textohome ;
      private String gxTv_SdtParametrosSistema_Parametrossistema_emailsdahost ;
      private String gxTv_SdtParametrosSistema_Parametrossistema_emailsdauser ;
      private String gxTv_SdtParametrosSistema_Parametrossistema_emailsdapass ;
      private String gxTv_SdtParametrosSistema_Parametrossistema_pathcrtf ;
      private String gxTv_SdtParametrosSistema_Parametrossistema_hostmensuracao ;
      private String gxTv_SdtParametrosSistema_Parametrossistema_sqlcomputername ;
      private String gxTv_SdtParametrosSistema_Parametrossistema_sqlservername ;
      private String gxTv_SdtParametrosSistema_Parametrossistema_sqledition ;
      private String gxTv_SdtParametrosSistema_Parametrossistema_sqlinstance ;
      private String gxTv_SdtParametrosSistema_Parametrossistema_dbname ;
      private String gxTv_SdtParametrosSistema_Parametrossistema_flsevd ;
      private String gxTv_SdtParametrosSistema_Parametrossistema_urlapp ;
      private String gxTv_SdtParametrosSistema_Parametrossistema_urlotherver ;
      private String gxTv_SdtParametrosSistema_Parametrossistema_validacao ;
      private String gxTv_SdtParametrosSistema_Parametrossistema_imagem_gxi ;
      private String gxTv_SdtParametrosSistema_Parametrossistema_emailsdahost_Z ;
      private String gxTv_SdtParametrosSistema_Parametrossistema_emailsdauser_Z ;
      private String gxTv_SdtParametrosSistema_Parametrossistema_emailsdapass_Z ;
      private String gxTv_SdtParametrosSistema_Parametrossistema_pathcrtf_Z ;
      private String gxTv_SdtParametrosSistema_Parametrossistema_hostmensuracao_Z ;
      private String gxTv_SdtParametrosSistema_Parametrossistema_sqlcomputername_Z ;
      private String gxTv_SdtParametrosSistema_Parametrossistema_sqlservername_Z ;
      private String gxTv_SdtParametrosSistema_Parametrossistema_sqledition_Z ;
      private String gxTv_SdtParametrosSistema_Parametrossistema_sqlinstance_Z ;
      private String gxTv_SdtParametrosSistema_Parametrossistema_dbname_Z ;
      private String gxTv_SdtParametrosSistema_Parametrossistema_flsevd_Z ;
      private String gxTv_SdtParametrosSistema_Parametrossistema_urlapp_Z ;
      private String gxTv_SdtParametrosSistema_Parametrossistema_urlotherver_Z ;
      private String gxTv_SdtParametrosSistema_Parametrossistema_validacao_Z ;
      private String gxTv_SdtParametrosSistema_Parametrossistema_imagem_gxi_Z ;
      private String gxTv_SdtParametrosSistema_Parametrossistema_imagem ;
      private Assembly constructorCallingAssembly ;
   }

   [DataContract(Name = @"ParametrosSistema", Namespace = "GxEv3Up14_MeetrikaVs3")]
   public class SdtParametrosSistema_RESTInterface : GxGenericCollectionItem<SdtParametrosSistema>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtParametrosSistema_RESTInterface( ) : base()
      {
      }

      public SdtParametrosSistema_RESTInterface( SdtParametrosSistema psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "ParametrosSistema_Codigo" , Order = 0 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Parametrossistema_codigo
      {
         get {
            return sdt.gxTpr_Parametrossistema_codigo ;
         }

         set {
            sdt.gxTpr_Parametrossistema_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ParametrosSistema_NomeSistema" , Order = 1 )]
      [GxSeudo()]
      public String gxTpr_Parametrossistema_nomesistema
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Parametrossistema_nomesistema) ;
         }

         set {
            sdt.gxTpr_Parametrossistema_nomesistema = (String)(value);
         }

      }

      [DataMember( Name = "ParametrosSistema_AppID" , Order = 2 )]
      [GxSeudo()]
      public String gxTpr_Parametrossistema_appid
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( (decimal)(sdt.gxTpr_Parametrossistema_appid), 12, 0)) ;
         }

         set {
            sdt.gxTpr_Parametrossistema_appid = (long)(NumberUtil.Val( (String)(value), "."));
         }

      }

      [DataMember( Name = "ParametrosSistema_TextoHome" , Order = 3 )]
      public String gxTpr_Parametrossistema_textohome
      {
         get {
            return sdt.gxTpr_Parametrossistema_textohome ;
         }

         set {
            sdt.gxTpr_Parametrossistema_textohome = (String)(value);
         }

      }

      [DataMember( Name = "ParametrosSistema_LicensiadoCadastrado" , Order = 4 )]
      [GxSeudo()]
      public bool gxTpr_Parametrossistema_licensiadocadastrado
      {
         get {
            return sdt.gxTpr_Parametrossistema_licensiadocadastrado ;
         }

         set {
            sdt.gxTpr_Parametrossistema_licensiadocadastrado = value;
         }

      }

      [DataMember( Name = "ParametrosSistema_PadronizarStrings" , Order = 5 )]
      [GxSeudo()]
      public bool gxTpr_Parametrossistema_padronizarstrings
      {
         get {
            return sdt.gxTpr_Parametrossistema_padronizarstrings ;
         }

         set {
            sdt.gxTpr_Parametrossistema_padronizarstrings = value;
         }

      }

      [DataMember( Name = "ParametrosSistema_EmailSdaHost" , Order = 6 )]
      [GxSeudo()]
      public String gxTpr_Parametrossistema_emailsdahost
      {
         get {
            return sdt.gxTpr_Parametrossistema_emailsdahost ;
         }

         set {
            sdt.gxTpr_Parametrossistema_emailsdahost = (String)(value);
         }

      }

      [DataMember( Name = "ParametrosSistema_EmailSdaUser" , Order = 7 )]
      [GxSeudo()]
      public String gxTpr_Parametrossistema_emailsdauser
      {
         get {
            return sdt.gxTpr_Parametrossistema_emailsdauser ;
         }

         set {
            sdt.gxTpr_Parametrossistema_emailsdauser = (String)(value);
         }

      }

      [DataMember( Name = "ParametrosSistema_EmailSdaPass" , Order = 8 )]
      [GxSeudo()]
      public String gxTpr_Parametrossistema_emailsdapass
      {
         get {
            return sdt.gxTpr_Parametrossistema_emailsdapass ;
         }

         set {
            sdt.gxTpr_Parametrossistema_emailsdapass = (String)(value);
         }

      }

      [DataMember( Name = "ParametrosSistema_EmailSdaKey" , Order = 9 )]
      [GxSeudo()]
      public String gxTpr_Parametrossistema_emailsdakey
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Parametrossistema_emailsdakey) ;
         }

         set {
            sdt.gxTpr_Parametrossistema_emailsdakey = (String)(value);
         }

      }

      [DataMember( Name = "ParametrosSistema_EmailSdaAut" , Order = 10 )]
      [GxSeudo()]
      public bool gxTpr_Parametrossistema_emailsdaaut
      {
         get {
            return sdt.gxTpr_Parametrossistema_emailsdaaut ;
         }

         set {
            sdt.gxTpr_Parametrossistema_emailsdaaut = value;
         }

      }

      [DataMember( Name = "ParametrosSistema_EmailSdaPort" , Order = 11 )]
      [GxSeudo()]
      public Nullable<short> gxTpr_Parametrossistema_emailsdaport
      {
         get {
            return sdt.gxTpr_Parametrossistema_emailsdaport ;
         }

         set {
            sdt.gxTpr_Parametrossistema_emailsdaport = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ParametrosSistema_EmailSdaSec" , Order = 12 )]
      [GxSeudo()]
      public Nullable<short> gxTpr_Parametrossistema_emailsdasec
      {
         get {
            return sdt.gxTpr_Parametrossistema_emailsdasec ;
         }

         set {
            sdt.gxTpr_Parametrossistema_emailsdasec = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ParametrosSistema_FatorAjuste" , Order = 13 )]
      [GxSeudo()]
      public Nullable<decimal> gxTpr_Parametrossistema_fatorajuste
      {
         get {
            return sdt.gxTpr_Parametrossistema_fatorajuste ;
         }

         set {
            sdt.gxTpr_Parametrossistema_fatorajuste = (decimal)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ParametrosSistema_PathCrtf" , Order = 14 )]
      [GxSeudo()]
      public String gxTpr_Parametrossistema_pathcrtf
      {
         get {
            return sdt.gxTpr_Parametrossistema_pathcrtf ;
         }

         set {
            sdt.gxTpr_Parametrossistema_pathcrtf = (String)(value);
         }

      }

      [DataMember( Name = "ParametrosSistema_HostMensuracao" , Order = 15 )]
      [GxSeudo()]
      public String gxTpr_Parametrossistema_hostmensuracao
      {
         get {
            return sdt.gxTpr_Parametrossistema_hostmensuracao ;
         }

         set {
            sdt.gxTpr_Parametrossistema_hostmensuracao = (String)(value);
         }

      }

      [DataMember( Name = "ParametrosSistema_SQLComputerName" , Order = 16 )]
      [GxSeudo()]
      public String gxTpr_Parametrossistema_sqlcomputername
      {
         get {
            return sdt.gxTpr_Parametrossistema_sqlcomputername ;
         }

         set {
            sdt.gxTpr_Parametrossistema_sqlcomputername = (String)(value);
         }

      }

      [DataMember( Name = "ParametrosSistema_SQLServerName" , Order = 17 )]
      [GxSeudo()]
      public String gxTpr_Parametrossistema_sqlservername
      {
         get {
            return sdt.gxTpr_Parametrossistema_sqlservername ;
         }

         set {
            sdt.gxTpr_Parametrossistema_sqlservername = (String)(value);
         }

      }

      [DataMember( Name = "ParametrosSistema_SQLEdition" , Order = 18 )]
      [GxSeudo()]
      public String gxTpr_Parametrossistema_sqledition
      {
         get {
            return sdt.gxTpr_Parametrossistema_sqledition ;
         }

         set {
            sdt.gxTpr_Parametrossistema_sqledition = (String)(value);
         }

      }

      [DataMember( Name = "ParametrosSistema_SQLInstance" , Order = 19 )]
      [GxSeudo()]
      public String gxTpr_Parametrossistema_sqlinstance
      {
         get {
            return sdt.gxTpr_Parametrossistema_sqlinstance ;
         }

         set {
            sdt.gxTpr_Parametrossistema_sqlinstance = (String)(value);
         }

      }

      [DataMember( Name = "ParametrosSistema_DBName" , Order = 20 )]
      [GxSeudo()]
      public String gxTpr_Parametrossistema_dbname
      {
         get {
            return sdt.gxTpr_Parametrossistema_dbname ;
         }

         set {
            sdt.gxTpr_Parametrossistema_dbname = (String)(value);
         }

      }

      [DataMember( Name = "ParametrosSistema_FlsEvd" , Order = 21 )]
      [GxSeudo()]
      public String gxTpr_Parametrossistema_flsevd
      {
         get {
            return sdt.gxTpr_Parametrossistema_flsevd ;
         }

         set {
            sdt.gxTpr_Parametrossistema_flsevd = (String)(value);
         }

      }

      [DataMember( Name = "ParametrosSistema_URLApp" , Order = 22 )]
      [GxSeudo()]
      public String gxTpr_Parametrossistema_urlapp
      {
         get {
            return sdt.gxTpr_Parametrossistema_urlapp ;
         }

         set {
            sdt.gxTpr_Parametrossistema_urlapp = (String)(value);
         }

      }

      [DataMember( Name = "ParametrosSistema_URLOtherVer" , Order = 23 )]
      [GxSeudo()]
      public String gxTpr_Parametrossistema_urlotherver
      {
         get {
            return sdt.gxTpr_Parametrossistema_urlotherver ;
         }

         set {
            sdt.gxTpr_Parametrossistema_urlotherver = (String)(value);
         }

      }

      [DataMember( Name = "ParametrosSistema_Validacao" , Order = 24 )]
      [GxSeudo()]
      public String gxTpr_Parametrossistema_validacao
      {
         get {
            return sdt.gxTpr_Parametrossistema_validacao ;
         }

         set {
            sdt.gxTpr_Parametrossistema_validacao = (String)(value);
         }

      }

      [DataMember( Name = "ParametrosSistema_Imagem" , Order = 25 )]
      [GxUpload()]
      public String gxTpr_Parametrossistema_imagem
      {
         get {
            return (!String.IsNullOrEmpty(StringUtil.RTrim( sdt.gxTpr_Parametrossistema_imagem)) ? PathUtil.RelativePath( sdt.gxTpr_Parametrossistema_imagem) : StringUtil.RTrim( sdt.gxTpr_Parametrossistema_imagem_gxi)) ;
         }

         set {
            sdt.gxTpr_Parametrossistema_imagem = (String)(value);
         }

      }

      public SdtParametrosSistema sdt
      {
         get {
            return (SdtParametrosSistema)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtParametrosSistema() ;
         }
      }

      [DataMember( Name = "gx_md5_hash", Order = 76 )]
      public string Hash
      {
         get {
            if ( StringUtil.StrCmp(md5Hash, null) == 0 )
            {
               md5Hash = (String)(getHash());
            }
            return md5Hash ;
         }

         set {
            md5Hash = value ;
         }

      }

      private String md5Hash ;
   }

}
