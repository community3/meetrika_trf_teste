/*
               File: PRC_CarregaContratadas
        Description: Carrega Contratadas
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:52:57.86
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_carregacontratadas : GXProcedure
   {
      public prc_carregacontratadas( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_carregacontratadas( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_Contratada_AreaTrabalhoCod ,
                           out IGxCollection aP1_Codigos ,
                           out IGxCollection aP2_Descricoes ,
                           out IGxCollection aP3_TipoFabrica )
      {
         this.A52Contratada_AreaTrabalhoCod = aP0_Contratada_AreaTrabalhoCod;
         this.AV8Codigos = new GxSimpleCollection() ;
         this.AV9Descricoes = new GxSimpleCollection() ;
         this.AV10TipoFabrica = new GxSimpleCollection() ;
         initialize();
         executePrivate();
         aP0_Contratada_AreaTrabalhoCod=this.A52Contratada_AreaTrabalhoCod;
         aP1_Codigos=this.AV8Codigos;
         aP2_Descricoes=this.AV9Descricoes;
         aP3_TipoFabrica=this.AV10TipoFabrica;
      }

      public IGxCollection executeUdp( ref int aP0_Contratada_AreaTrabalhoCod ,
                                       out IGxCollection aP1_Codigos ,
                                       out IGxCollection aP2_Descricoes )
      {
         this.A52Contratada_AreaTrabalhoCod = aP0_Contratada_AreaTrabalhoCod;
         this.AV8Codigos = new GxSimpleCollection() ;
         this.AV9Descricoes = new GxSimpleCollection() ;
         this.AV10TipoFabrica = new GxSimpleCollection() ;
         initialize();
         executePrivate();
         aP0_Contratada_AreaTrabalhoCod=this.A52Contratada_AreaTrabalhoCod;
         aP1_Codigos=this.AV8Codigos;
         aP2_Descricoes=this.AV9Descricoes;
         aP3_TipoFabrica=this.AV10TipoFabrica;
         return AV10TipoFabrica ;
      }

      public void executeSubmit( ref int aP0_Contratada_AreaTrabalhoCod ,
                                 out IGxCollection aP1_Codigos ,
                                 out IGxCollection aP2_Descricoes ,
                                 out IGxCollection aP3_TipoFabrica )
      {
         prc_carregacontratadas objprc_carregacontratadas;
         objprc_carregacontratadas = new prc_carregacontratadas();
         objprc_carregacontratadas.A52Contratada_AreaTrabalhoCod = aP0_Contratada_AreaTrabalhoCod;
         objprc_carregacontratadas.AV8Codigos = new GxSimpleCollection() ;
         objprc_carregacontratadas.AV9Descricoes = new GxSimpleCollection() ;
         objprc_carregacontratadas.AV10TipoFabrica = new GxSimpleCollection() ;
         objprc_carregacontratadas.context.SetSubmitInitialConfig(context);
         objprc_carregacontratadas.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_carregacontratadas);
         aP0_Contratada_AreaTrabalhoCod=this.A52Contratada_AreaTrabalhoCod;
         aP1_Codigos=this.AV8Codigos;
         aP2_Descricoes=this.AV9Descricoes;
         aP3_TipoFabrica=this.AV10TipoFabrica;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_carregacontratadas)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00DD2 */
         pr_default.execute(0, new Object[] {A52Contratada_AreaTrabalhoCod});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A39Contratada_Codigo = P00DD2_A39Contratada_Codigo[0];
            A516Contratada_TipoFabrica = P00DD2_A516Contratada_TipoFabrica[0];
            A438Contratada_Sigla = P00DD2_A438Contratada_Sigla[0];
            AV8Codigos.Add(A39Contratada_Codigo, 0);
            AV9Descricoes.Add(A438Contratada_Sigla, 0);
            AV10TipoFabrica.Add(A516Contratada_TipoFabrica, 0);
            pr_default.readNext(0);
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00DD2_A52Contratada_AreaTrabalhoCod = new int[1] ;
         P00DD2_A39Contratada_Codigo = new int[1] ;
         P00DD2_A516Contratada_TipoFabrica = new String[] {""} ;
         P00DD2_A438Contratada_Sigla = new String[] {""} ;
         A516Contratada_TipoFabrica = "";
         A438Contratada_Sigla = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_carregacontratadas__default(),
            new Object[][] {
                new Object[] {
               P00DD2_A52Contratada_AreaTrabalhoCod, P00DD2_A39Contratada_Codigo, P00DD2_A516Contratada_TipoFabrica, P00DD2_A438Contratada_Sigla
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A52Contratada_AreaTrabalhoCod ;
      private int A39Contratada_Codigo ;
      private String scmdbuf ;
      private String A516Contratada_TipoFabrica ;
      private String A438Contratada_Sigla ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_Contratada_AreaTrabalhoCod ;
      private IDataStoreProvider pr_default ;
      private int[] P00DD2_A52Contratada_AreaTrabalhoCod ;
      private int[] P00DD2_A39Contratada_Codigo ;
      private String[] P00DD2_A516Contratada_TipoFabrica ;
      private String[] P00DD2_A438Contratada_Sigla ;
      private IGxCollection aP1_Codigos ;
      private IGxCollection aP2_Descricoes ;
      private IGxCollection aP3_TipoFabrica ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV8Codigos ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV10TipoFabrica ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV9Descricoes ;
   }

   public class prc_carregacontratadas__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00DD2 ;
          prmP00DD2 = new Object[] {
          new Object[] {"@Contratada_AreaTrabalhoCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00DD2", "SELECT [Contratada_AreaTrabalhoCod], [Contratada_Codigo], [Contratada_TipoFabrica], [Contratada_Sigla] FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_AreaTrabalhoCod] = @Contratada_AreaTrabalhoCod ORDER BY [Contratada_Sigla] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00DD2,100,0,false,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 1) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 15) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
