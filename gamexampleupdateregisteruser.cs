/*
               File: GAMExampleUpdateRegisterUser
        Description: Update register user
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 23:11:59.45
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class gamexampleupdateregisteruser : GXHttpHandler, System.Web.SessionState.IRequiresSessionState
   {
      public gamexampleupdateregisteruser( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("GeneXusXEv2");
      }

      public gamexampleupdateregisteruser( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref String aP0_ApplicationClientId )
      {
         this.AV6ApplicationClientId = aP0_ApplicationClientId;
         executePrivate();
         aP0_ApplicationClientId=this.AV6ApplicationClientId;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavGender = new GXCombobox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("GeneXusXEv2");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV6ApplicationClientId = gxfirstwebparm;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6ApplicationClientId", AV6ApplicationClientId);
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            ValidateSpaRequest();
            PA2H2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               context.Gx_err = 0;
               edtavName_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavName_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavName_Enabled), 5, 0)));
               WS2H2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  WE2H2( ) ;
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( "Update register user") ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20204282312276");
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.CloseHtmlHeader();
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("gamexampleupdateregisteruser.aspx") + "?" + UrlEncode(StringUtil.RTrim(AV6ApplicationClientId))+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "vAPPLICATIONCLIENTID", StringUtil.RTrim( AV6ApplicationClientId));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "GAMExampleUpdateRegisterUser";
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( AV22Name, ""));
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("gamexampleupdateregisteruser:[SendSecurityCheck value for]"+"Name:"+StringUtil.RTrim( context.localUtil.Format( AV22Name, "")));
      }

      protected void RenderHtmlCloseForm2H2( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         context.WriteHtmlTextNl( "</form>") ;
         include_jscripts( ) ;
         context.WriteHtmlTextNl( "</body>") ;
         context.WriteHtmlTextNl( "</html>") ;
      }

      public override String GetPgmname( )
      {
         return "GAMExampleUpdateRegisterUser" ;
      }

      public override String GetPgmdesc( )
      {
         return "Update register user" ;
      }

      protected void WB2H0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            RenderHtmlHeaders( ) ;
            RenderHtmlOpenForm( ) ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbformtitle_Internalname, "Complete User registration data", "", "", lblTbformtitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Title", 0, "", 1, 1, 0, "HLP_GAMExampleUpdateRegisterUser.htm");
            wb_table1_3_2H2( true) ;
         }
         else
         {
            wb_table1_3_2H2( false) ;
         }
         return  ;
      }

      protected void wb_table1_3_2H2e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void START2H2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Update register user", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUP2H0( ) ;
      }

      protected void WS2H2( )
      {
         START2H2( ) ;
         EVT2H2( ) ;
      }

      protected void EVT2H2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E112H2 */
                           E112H2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( ! wbErr )
                           {
                              Rfr0gs = false;
                              if ( ! Rfr0gs )
                              {
                                 /* Execute user event: E122H2 */
                                 E122H2 ();
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E132H2 */
                           E132H2 ();
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           dynload_actions( ) ;
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void WE2H2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseForm2H2( ) ;
            }
         }
      }

      protected void PA2H2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            cmbavGender.Name = "vGENDER";
            cmbavGender.WebTags = "";
            cmbavGender.addItem("N", "Not Specified", 0);
            cmbavGender.addItem("F", "Female", 0);
            cmbavGender.addItem("M", "Male", 0);
            if ( cmbavGender.ItemCount > 0 )
            {
               AV14Gender = cmbavGender.getValidValue(AV14Gender);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14Gender", AV14Gender);
            }
            if ( toggleJsOutput )
            {
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavName_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavGender.ItemCount > 0 )
         {
            AV14Gender = cmbavGender.getValidValue(AV14Gender);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14Gender", AV14Gender);
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF2H2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavName_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavName_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavName_Enabled), 5, 0)));
      }

      protected void RF2H2( )
      {
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Execute user event: E132H2 */
            E132H2 ();
            WB2H0( ) ;
         }
      }

      protected void STRUP2H0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         edtavName_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavName_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavName_Enabled), 5, 0)));
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E112H2 */
         E112H2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            AV22Name = cgiGet( edtavName_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Name", AV22Name);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vNAME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV22Name, ""))));
            AV27ReqEmail = cgiGet( imgavReqemail_Internalname);
            AV10EMail = cgiGet( edtavEmail_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10EMail", AV10EMail);
            AV28ReqFirstName = cgiGet( imgavReqfirstname_Internalname);
            AV13FirstName = cgiGet( edtavFirstname_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13FirstName", AV13FirstName);
            AV31ReqLastName = cgiGet( imgavReqlastname_Internalname);
            AV18LastName = cgiGet( edtavLastname_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18LastName", AV18LastName);
            AV26ReqBirthday = cgiGet( imgavReqbirthday_Internalname);
            if ( context.localUtil.VCDate( cgiGet( edtavBirthday_Internalname), 2) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Birthday"}), 1, "vBIRTHDAY");
               GX_FocusControl = edtavBirthday_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV9Birthday = DateTime.MinValue;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9Birthday", context.localUtil.Format(AV9Birthday, "99/99/9999"));
            }
            else
            {
               AV9Birthday = context.localUtil.CToD( cgiGet( edtavBirthday_Internalname), 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9Birthday", context.localUtil.Format(AV9Birthday, "99/99/9999"));
            }
            AV29ReqGender = cgiGet( imgavReqgender_Internalname);
            cmbavGender.CurrentValue = cgiGet( cmbavGender_Internalname);
            AV14Gender = cgiGet( cmbavGender_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14Gender", AV14Gender);
            AV30ReqIcon = cgiGet( imgavReqicon_Internalname);
            /* Read saved values. */
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            forbiddenHiddens = "hsh" + "GAMExampleUpdateRegisterUser";
            AV22Name = cgiGet( edtavName_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Name", AV22Name);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vNAME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV22Name, ""))));
            forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( AV22Name, ""));
            hsh = cgiGet( "hsh");
            if ( ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
            {
               GXUtil.WriteLog("gamexampleupdateregisteruser:[SecurityCheckFailed value for]"+"Name:"+StringUtil.RTrim( context.localUtil.Format( AV22Name, "")));
               GxWebError = 1;
               context.HttpContext.Response.StatusDescription = 403.ToString();
               context.HttpContext.Response.StatusCode = 403;
               context.WriteHtmlText( "<title>403 Forbidden</title>") ;
               context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
               context.WriteHtmlText( "<p /><hr />") ;
               GXUtil.WriteLog("send_http_error_code " + 403.ToString());
               return  ;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E112H2 */
         E112H2 ();
         if (returnInSub) return;
      }

      protected void E112H2( )
      {
         /* Start Routine */
         AV33User = new SdtGAMRepository(context).getuserbykeytocompleteuserdata(out  AV12Errors);
         if ( AV12Errors.Count > 0 )
         {
            /* Execute user subroutine: 'DISPLAYMESSAGES' */
            S112 ();
            if (returnInSub) return;
         }
         else
         {
            AV22Name = AV33User.gxTpr_Name;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Name", AV22Name);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vNAME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV22Name, ""))));
            AV10EMail = AV33User.gxTpr_Email;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10EMail", AV10EMail);
            AV13FirstName = AV33User.gxTpr_Firstname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13FirstName", AV13FirstName);
            AV18LastName = AV33User.gxTpr_Lastname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18LastName", AV18LastName);
            AV9Birthday = AV33User.gxTpr_Birthday;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9Birthday", context.localUtil.Format(AV9Birthday, "99/99/9999"));
            AV14Gender = AV33User.gxTpr_Gender;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14Gender", AV14Gender);
         }
         AV25Repository = new SdtGAMRepository(context).get();
         AV15isInformationRequired = false;
         if ( AV25Repository.gxTpr_Requiredemail )
         {
            AV15isInformationRequired = true;
            AV27ReqEmail = context.GetImagePath( "d654d7ee-a582-41b1-9b48-d99c7b5818ee", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavReqemail_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV27ReqEmail)) ? AV37Reqemail_GXI : context.convertURL( context.PathToRelativeUrl( AV27ReqEmail))));
            AV37Reqemail_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "d654d7ee-a582-41b1-9b48-d99c7b5818ee", "", context.GetTheme( )));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavReqemail_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV27ReqEmail)) ? AV37Reqemail_GXI : context.convertURL( context.PathToRelativeUrl( AV27ReqEmail))));
         }
         else
         {
            imgavReqemail_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavReqemail_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgavReqemail_Visible), 5, 0)));
         }
         if ( AV25Repository.gxTpr_Requiredfirstname )
         {
            AV15isInformationRequired = true;
            AV28ReqFirstName = context.GetImagePath( "d654d7ee-a582-41b1-9b48-d99c7b5818ee", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavReqfirstname_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV28ReqFirstName)) ? AV38Reqfirstname_GXI : context.convertURL( context.PathToRelativeUrl( AV28ReqFirstName))));
            AV38Reqfirstname_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "d654d7ee-a582-41b1-9b48-d99c7b5818ee", "", context.GetTheme( )));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavReqfirstname_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV28ReqFirstName)) ? AV38Reqfirstname_GXI : context.convertURL( context.PathToRelativeUrl( AV28ReqFirstName))));
         }
         else
         {
            imgavReqfirstname_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavReqfirstname_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgavReqfirstname_Visible), 5, 0)));
         }
         if ( AV25Repository.gxTpr_Requiredlastname )
         {
            AV15isInformationRequired = true;
            AV31ReqLastName = context.GetImagePath( "d654d7ee-a582-41b1-9b48-d99c7b5818ee", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavReqlastname_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV31ReqLastName)) ? AV39Reqlastname_GXI : context.convertURL( context.PathToRelativeUrl( AV31ReqLastName))));
            AV39Reqlastname_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "d654d7ee-a582-41b1-9b48-d99c7b5818ee", "", context.GetTheme( )));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavReqlastname_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV31ReqLastName)) ? AV39Reqlastname_GXI : context.convertURL( context.PathToRelativeUrl( AV31ReqLastName))));
         }
         else
         {
            imgavReqlastname_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavReqlastname_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgavReqlastname_Visible), 5, 0)));
         }
         if ( AV25Repository.gxTpr_Requiredbirthday )
         {
            AV15isInformationRequired = true;
            AV26ReqBirthday = context.GetImagePath( "d654d7ee-a582-41b1-9b48-d99c7b5818ee", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavReqbirthday_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV26ReqBirthday)) ? AV40Reqbirthday_GXI : context.convertURL( context.PathToRelativeUrl( AV26ReqBirthday))));
            AV40Reqbirthday_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "d654d7ee-a582-41b1-9b48-d99c7b5818ee", "", context.GetTheme( )));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavReqbirthday_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV26ReqBirthday)) ? AV40Reqbirthday_GXI : context.convertURL( context.PathToRelativeUrl( AV26ReqBirthday))));
         }
         else
         {
            imgavReqbirthday_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavReqbirthday_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgavReqbirthday_Visible), 5, 0)));
         }
         if ( AV25Repository.gxTpr_Requiredgender )
         {
            AV15isInformationRequired = true;
            AV29ReqGender = context.GetImagePath( "d654d7ee-a582-41b1-9b48-d99c7b5818ee", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavReqgender_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV29ReqGender)) ? AV41Reqgender_GXI : context.convertURL( context.PathToRelativeUrl( AV29ReqGender))));
            AV41Reqgender_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "d654d7ee-a582-41b1-9b48-d99c7b5818ee", "", context.GetTheme( )));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavReqgender_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV29ReqGender)) ? AV41Reqgender_GXI : context.convertURL( context.PathToRelativeUrl( AV29ReqGender))));
         }
         else
         {
            imgavReqgender_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavReqgender_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgavReqgender_Visible), 5, 0)));
         }
         if ( AV15isInformationRequired )
         {
            AV30ReqIcon = context.GetImagePath( "d654d7ee-a582-41b1-9b48-d99c7b5818ee", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavReqicon_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV30ReqIcon)) ? AV42Reqicon_GXI : context.convertURL( context.PathToRelativeUrl( AV30ReqIcon))));
            AV42Reqicon_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "d654d7ee-a582-41b1-9b48-d99c7b5818ee", "", context.GetTheme( )));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavReqicon_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV30ReqIcon)) ? AV42Reqicon_GXI : context.convertURL( context.PathToRelativeUrl( AV30ReqIcon))));
            lblTbinficonreq_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbinficonreq_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTbinficonreq_Visible), 5, 0)));
         }
         else
         {
            imgavReqicon_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavReqicon_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgavReqicon_Visible), 5, 0)));
            lblTbinficonreq_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbinficonreq_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTbinficonreq_Visible), 5, 0)));
         }
      }

      public void GXEnter( )
      {
         /* Execute user event: E122H2 */
         E122H2 ();
         if (returnInSub) return;
      }

      protected void E122H2( )
      {
         /* Enter Routine */
         AV33User.load( AV33User.gxTpr_Guid);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV33User", AV33User);
         AV33User.gxTpr_Name = AV22Name;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV33User", AV33User);
         AV33User.gxTpr_Email = AV10EMail;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV33User", AV33User);
         AV33User.gxTpr_Firstname = AV13FirstName;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV33User", AV33User);
         AV33User.gxTpr_Lastname = AV18LastName;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV33User", AV33User);
         AV33User.gxTpr_Birthday = AV9Birthday;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV33User", AV33User);
         AV33User.gxTpr_Gender = AV14Gender;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV33User", AV33User);
         AV16isOK = new SdtGAMRepository(context).updateuserbykeytocompleteuserdata(AV33User, out  AV12Errors);
         if ( AV16isOK )
         {
            if ( new SdtGAMRepository(context).isremoteauthentication(AV6ApplicationClientId) )
            {
               new SdtGAMRepository(context).redirecttoremoteauthentication() ;
            }
            else
            {
               AV32URL = new SdtGAMRepository(context).getlasterrorsurl();
               if ( String.IsNullOrEmpty(StringUtil.RTrim( AV32URL)) )
               {
                  context.wjLoc = formatLink("gamhome.aspx") ;
                  context.wjLocDisableFrm = 1;
               }
               else
               {
                  context.wjLoc = formatLink(AV32URL) ;
                  context.wjLocDisableFrm = 0;
               }
            }
         }
         else
         {
            /* Execute user subroutine: 'DISPLAYMESSAGES' */
            S112 ();
            if (returnInSub) return;
         }
      }

      protected void S112( )
      {
         /* 'DISPLAYMESSAGES' Routine */
         AV43GXV1 = 1;
         while ( AV43GXV1 <= AV12Errors.Count )
         {
            AV11Error = ((SdtGAMError)AV12Errors.Item(AV43GXV1));
            GX_msglist.addItem(StringUtil.Format( "%1 (GAM%2)", AV11Error.gxTpr_Message, StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Error.gxTpr_Code), 12, 0)), "", "", "", "", "", "", ""));
            AV43GXV1 = (int)(AV43GXV1+1);
         }
      }

      protected void nextLoad( )
      {
      }

      protected void E132H2( )
      {
         /* Load Routine */
      }

      protected void wb_table1_3_2H2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "width:10px")+"\">") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "width:150px")+"\">") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbname2_Internalname, "User Name", "", "", lblTbname2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleUpdateRegisterUser.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 14,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavName_Internalname, AV22Name, StringUtil.RTrim( context.localUtil.Format( AV22Name, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,14);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavName_Jsonclick, 0, "Attribute", "", "", "", 1, edtavName_Enabled, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, 0, true, "GAMUserIdentification", "left", true, "HLP_GAMExampleUpdateRegisterUser.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "height:16px")+"\">") ;
            /* Static Bitmap Variable */
            GxWebStd.gx_hidden_field( context, imgavReqemail_Internalname, AV27ReqEmail);
            ClassString = "Image";
            StyleString = "";
            AV27ReqEmail_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV27ReqEmail))&&String.IsNullOrEmpty(StringUtil.RTrim( AV37Reqemail_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV27ReqEmail)));
            GxWebStd.gx_bitmap( context, imgavReqemail_Internalname, (String.IsNullOrEmpty(StringUtil.RTrim( AV27ReqEmail)) ? AV37Reqemail_GXI : context.PathToRelativeUrl( AV27ReqEmail)), "", "", "", context.GetTheme( ), imgavReqemail_Visible, 0, "", "", 0, -1, 0, "", 0, "", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", 1, AV27ReqEmail_IsBlob, false, "HLP_GAMExampleUpdateRegisterUser.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbemail_Internalname, "Email", "", "", lblTbemail_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleUpdateRegisterUser.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavEmail_Internalname, AV10EMail, StringUtil.RTrim( context.localUtil.Format( AV10EMail, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,21);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavEmail_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, 0, true, "GAMEMail", "left", true, "HLP_GAMExampleUpdateRegisterUser.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "height:16px")+"\">") ;
            /* Static Bitmap Variable */
            GxWebStd.gx_hidden_field( context, imgavReqfirstname_Internalname, AV28ReqFirstName);
            ClassString = "Image";
            StyleString = "";
            AV28ReqFirstName_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV28ReqFirstName))&&String.IsNullOrEmpty(StringUtil.RTrim( AV38Reqfirstname_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV28ReqFirstName)));
            GxWebStd.gx_bitmap( context, imgavReqfirstname_Internalname, (String.IsNullOrEmpty(StringUtil.RTrim( AV28ReqFirstName)) ? AV38Reqfirstname_GXI : context.PathToRelativeUrl( AV28ReqFirstName)), "", "", "", context.GetTheme( ), imgavReqfirstname_Visible, 0, "", "", 0, -1, 0, "", 0, "", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", 1, AV28ReqFirstName_IsBlob, false, "HLP_GAMExampleUpdateRegisterUser.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbfirstname_Internalname, "First Name", "", "", lblTbfirstname_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleUpdateRegisterUser.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 28,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavFirstname_Internalname, StringUtil.RTrim( AV13FirstName), StringUtil.RTrim( context.localUtil.Format( AV13FirstName, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,28);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavFirstname_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 60, "chr", 1, "row", 60, 0, 0, 0, 1, -1, -1, true, "GAMDescriptionShort", "left", true, "HLP_GAMExampleUpdateRegisterUser.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Static Bitmap Variable */
            GxWebStd.gx_hidden_field( context, imgavReqlastname_Internalname, AV31ReqLastName);
            ClassString = "Image";
            StyleString = "";
            AV31ReqLastName_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV31ReqLastName))&&String.IsNullOrEmpty(StringUtil.RTrim( AV39Reqlastname_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV31ReqLastName)));
            GxWebStd.gx_bitmap( context, imgavReqlastname_Internalname, (String.IsNullOrEmpty(StringUtil.RTrim( AV31ReqLastName)) ? AV39Reqlastname_GXI : context.PathToRelativeUrl( AV31ReqLastName)), "", "", "", context.GetTheme( ), imgavReqlastname_Visible, 0, "", "", 0, -1, 0, "", 0, "", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", 1, AV31ReqLastName_IsBlob, false, "HLP_GAMExampleUpdateRegisterUser.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTblastname_Internalname, "Last Name", "", "", lblTblastname_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleUpdateRegisterUser.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 35,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavLastname_Internalname, StringUtil.RTrim( AV18LastName), StringUtil.RTrim( context.localUtil.Format( AV18LastName, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,35);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavLastname_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 60, "chr", 1, "row", 60, 0, 0, 0, 1, -1, -1, true, "GAMDescriptionShort", "left", true, "HLP_GAMExampleUpdateRegisterUser.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Static Bitmap Variable */
            GxWebStd.gx_hidden_field( context, imgavReqbirthday_Internalname, AV26ReqBirthday);
            ClassString = "Image";
            StyleString = "";
            AV26ReqBirthday_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV26ReqBirthday))&&String.IsNullOrEmpty(StringUtil.RTrim( AV40Reqbirthday_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV26ReqBirthday)));
            GxWebStd.gx_bitmap( context, imgavReqbirthday_Internalname, (String.IsNullOrEmpty(StringUtil.RTrim( AV26ReqBirthday)) ? AV40Reqbirthday_GXI : context.PathToRelativeUrl( AV26ReqBirthday)), "", "", "", context.GetTheme( ), imgavReqbirthday_Visible, 0, "", "", 0, -1, 0, "", 0, "", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", 1, AV26ReqBirthday_IsBlob, false, "HLP_GAMExampleUpdateRegisterUser.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbbirthday_Internalname, "Birthday", "", "", lblTbbirthday_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleUpdateRegisterUser.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 42,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavBirthday_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavBirthday_Internalname, context.localUtil.Format(AV9Birthday, "99/99/9999"), context.localUtil.Format( AV9Birthday, "99/99/9999"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 10,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,42);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavBirthday_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 10, "chr", 1, "row", 10, 0, 0, 0, 1, -1, 0, true, "GAMDate", "right", false, "HLP_GAMExampleUpdateRegisterUser.htm");
            GxWebStd.gx_bitmap( context, edtavBirthday_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_GAMExampleUpdateRegisterUser.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Static Bitmap Variable */
            GxWebStd.gx_hidden_field( context, imgavReqgender_Internalname, AV29ReqGender);
            ClassString = "Image";
            StyleString = "";
            AV29ReqGender_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV29ReqGender))&&String.IsNullOrEmpty(StringUtil.RTrim( AV41Reqgender_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV29ReqGender)));
            GxWebStd.gx_bitmap( context, imgavReqgender_Internalname, (String.IsNullOrEmpty(StringUtil.RTrim( AV29ReqGender)) ? AV41Reqgender_GXI : context.PathToRelativeUrl( AV29ReqGender)), "", "", "", context.GetTheme( ), imgavReqgender_Visible, 0, "", "", 0, -1, 0, "", 0, "", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", 1, AV29ReqGender_IsBlob, false, "HLP_GAMExampleUpdateRegisterUser.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbgender_Internalname, "Gender", "", "", lblTbgender_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleUpdateRegisterUser.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "height:9px")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavGender, cmbavGender_Internalname, StringUtil.RTrim( AV14Gender), 1, cmbavGender_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,49);\"", "", true, "HLP_GAMExampleUpdateRegisterUser.htm");
            cmbavGender.CurrentValue = StringUtil.RTrim( AV14Gender);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavGender_Internalname, "Values", (String)(cmbavGender.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "height:18px")+"\">") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Static Bitmap Variable */
            GxWebStd.gx_hidden_field( context, imgavReqicon_Internalname, AV30ReqIcon);
            ClassString = "Image";
            StyleString = "";
            AV30ReqIcon_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV30ReqIcon))&&String.IsNullOrEmpty(StringUtil.RTrim( AV42Reqicon_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV30ReqIcon)));
            GxWebStd.gx_bitmap( context, imgavReqicon_Internalname, (String.IsNullOrEmpty(StringUtil.RTrim( AV30ReqIcon)) ? AV42Reqicon_GXI : context.PathToRelativeUrl( AV30ReqIcon)), "", "", "", context.GetTheme( ), imgavReqicon_Visible, 0, "", "", 0, -1, 0, "", 0, "", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", 1, AV30ReqIcon_IsBlob, false, "HLP_GAMExampleUpdateRegisterUser.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbinficonreq_Internalname, "Information required.", "", "", lblTbinficonreq_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "font-family:'Microsoft Sans Serif'; font-size:8.0pt; font-weight:normal; font-style:normal;", "Label", 0, "", lblTbinficonreq_Visible, 1, 0, "HLP_GAMExampleUpdateRegisterUser.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_64_2H2( true) ;
         }
         else
         {
            wb_table2_64_2H2( false) ;
         }
         return  ;
      }

      protected void wb_table2_64_2H2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_3_2H2e( true) ;
         }
         else
         {
            wb_table1_3_2H2e( false) ;
         }
      }

      protected void wb_table2_64_2H2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTblbuttons_Internalname, tblTblbuttons_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 67,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnconfirm_Internalname, "", "Confirmar", bttBtnconfirm_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_GAMExampleUpdateRegisterUser.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "width:2px")+"\">") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 70,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnclose_Internalname, "", "Fechar", bttBtnclose_Jsonclick, 1, "Fechar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_GAMExampleUpdateRegisterUser.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_64_2H2e( true) ;
         }
         else
         {
            wb_table2_64_2H2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV6ApplicationClientId = (String)getParm(obj,0);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6ApplicationClientId", AV6ApplicationClientId);
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("GeneXusXEv2");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA2H2( ) ;
         WS2H2( ) ;
         WE2H2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?2249785");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20204282312469");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gamexampleupdateregisteruser.js", "?20204282312470");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTbformtitle_Internalname = "TBFORMTITLE";
         lblTbname2_Internalname = "TBNAME2";
         edtavName_Internalname = "vNAME";
         imgavReqemail_Internalname = "vREQEMAIL";
         lblTbemail_Internalname = "TBEMAIL";
         edtavEmail_Internalname = "vEMAIL";
         imgavReqfirstname_Internalname = "vREQFIRSTNAME";
         lblTbfirstname_Internalname = "TBFIRSTNAME";
         edtavFirstname_Internalname = "vFIRSTNAME";
         imgavReqlastname_Internalname = "vREQLASTNAME";
         lblTblastname_Internalname = "TBLASTNAME";
         edtavLastname_Internalname = "vLASTNAME";
         imgavReqbirthday_Internalname = "vREQBIRTHDAY";
         lblTbbirthday_Internalname = "TBBIRTHDAY";
         edtavBirthday_Internalname = "vBIRTHDAY";
         imgavReqgender_Internalname = "vREQGENDER";
         lblTbgender_Internalname = "TBGENDER";
         cmbavGender_Internalname = "vGENDER";
         imgavReqicon_Internalname = "vREQICON";
         lblTbinficonreq_Internalname = "TBINFICONREQ";
         bttBtnconfirm_Internalname = "BTNCONFIRM";
         bttBtnclose_Internalname = "BTNCLOSE";
         tblTblbuttons_Internalname = "TBLBUTTONS";
         tblTable1_Internalname = "TABLE1";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         init_default_properties( ) ;
         lblTbinficonreq_Visible = 1;
         cmbavGender_Jsonclick = "";
         edtavBirthday_Jsonclick = "";
         edtavLastname_Jsonclick = "";
         edtavFirstname_Jsonclick = "";
         edtavEmail_Jsonclick = "";
         edtavName_Jsonclick = "";
         edtavName_Enabled = 1;
         imgavReqicon_Visible = 1;
         imgavReqgender_Visible = 1;
         imgavReqbirthday_Visible = 1;
         imgavReqlastname_Visible = 1;
         imgavReqfirstname_Visible = 1;
         imgavReqemail_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
      }

      protected override bool IsSpaSupported( )
      {
         return false ;
      }

      public override void InitializeDynEvents( )
      {
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         wcpOAV6ApplicationClientId = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GXKey = "";
         forbiddenHiddens = "";
         AV22Name = "";
         GX_FocusControl = "";
         sPrefix = "";
         lblTbformtitle_Jsonclick = "";
         Form = new GXWebForm();
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV14Gender = "";
         AV27ReqEmail = "";
         AV10EMail = "";
         AV28ReqFirstName = "";
         AV13FirstName = "";
         AV31ReqLastName = "";
         AV18LastName = "";
         AV26ReqBirthday = "";
         AV9Birthday = DateTime.MinValue;
         AV29ReqGender = "";
         AV30ReqIcon = "";
         hsh = "";
         AV33User = new SdtGAMUser(context);
         AV12Errors = new GxExternalCollection( context, "SdtGAMError", "GeneXus.Programs");
         AV25Repository = new SdtGAMRepository(context);
         AV37Reqemail_GXI = "";
         AV38Reqfirstname_GXI = "";
         AV39Reqlastname_GXI = "";
         AV40Reqbirthday_GXI = "";
         AV41Reqgender_GXI = "";
         AV42Reqicon_GXI = "";
         AV32URL = "";
         AV11Error = new SdtGAMError(context);
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         lblTbname2_Jsonclick = "";
         TempTags = "";
         lblTbemail_Jsonclick = "";
         lblTbfirstname_Jsonclick = "";
         lblTblastname_Jsonclick = "";
         lblTbbirthday_Jsonclick = "";
         lblTbgender_Jsonclick = "";
         lblTbinficonreq_Jsonclick = "";
         bttBtnconfirm_Jsonclick = "";
         bttBtnclose_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavName_Enabled = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short initialized ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXWrapped ;
      private int edtavName_Enabled ;
      private int imgavReqemail_Visible ;
      private int imgavReqfirstname_Visible ;
      private int imgavReqlastname_Visible ;
      private int imgavReqbirthday_Visible ;
      private int imgavReqgender_Visible ;
      private int lblTbinficonreq_Visible ;
      private int imgavReqicon_Visible ;
      private int AV43GXV1 ;
      private int idxLst ;
      private String AV6ApplicationClientId ;
      private String wcpOAV6ApplicationClientId ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String edtavName_Internalname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GXKey ;
      private String forbiddenHiddens ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String lblTbformtitle_Internalname ;
      private String lblTbformtitle_Jsonclick ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String AV14Gender ;
      private String imgavReqemail_Internalname ;
      private String edtavEmail_Internalname ;
      private String imgavReqfirstname_Internalname ;
      private String AV13FirstName ;
      private String edtavFirstname_Internalname ;
      private String imgavReqlastname_Internalname ;
      private String AV18LastName ;
      private String edtavLastname_Internalname ;
      private String imgavReqbirthday_Internalname ;
      private String edtavBirthday_Internalname ;
      private String imgavReqgender_Internalname ;
      private String cmbavGender_Internalname ;
      private String imgavReqicon_Internalname ;
      private String hsh ;
      private String lblTbinficonreq_Internalname ;
      private String sStyleString ;
      private String tblTable1_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String lblTbname2_Internalname ;
      private String lblTbname2_Jsonclick ;
      private String TempTags ;
      private String edtavName_Jsonclick ;
      private String lblTbemail_Internalname ;
      private String lblTbemail_Jsonclick ;
      private String edtavEmail_Jsonclick ;
      private String lblTbfirstname_Internalname ;
      private String lblTbfirstname_Jsonclick ;
      private String edtavFirstname_Jsonclick ;
      private String lblTblastname_Internalname ;
      private String lblTblastname_Jsonclick ;
      private String edtavLastname_Jsonclick ;
      private String lblTbbirthday_Internalname ;
      private String lblTbbirthday_Jsonclick ;
      private String edtavBirthday_Jsonclick ;
      private String lblTbgender_Internalname ;
      private String lblTbgender_Jsonclick ;
      private String cmbavGender_Jsonclick ;
      private String lblTbinficonreq_Jsonclick ;
      private String tblTblbuttons_Internalname ;
      private String bttBtnconfirm_Internalname ;
      private String bttBtnconfirm_Jsonclick ;
      private String bttBtnclose_Internalname ;
      private String bttBtnclose_Jsonclick ;
      private DateTime AV9Birthday ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private bool AV15isInformationRequired ;
      private bool AV16isOK ;
      private bool AV27ReqEmail_IsBlob ;
      private bool AV28ReqFirstName_IsBlob ;
      private bool AV31ReqLastName_IsBlob ;
      private bool AV26ReqBirthday_IsBlob ;
      private bool AV29ReqGender_IsBlob ;
      private bool AV30ReqIcon_IsBlob ;
      private String AV22Name ;
      private String AV10EMail ;
      private String AV37Reqemail_GXI ;
      private String AV38Reqfirstname_GXI ;
      private String AV39Reqlastname_GXI ;
      private String AV40Reqbirthday_GXI ;
      private String AV41Reqgender_GXI ;
      private String AV42Reqicon_GXI ;
      private String AV32URL ;
      private String AV27ReqEmail ;
      private String AV28ReqFirstName ;
      private String AV31ReqLastName ;
      private String AV26ReqBirthday ;
      private String AV29ReqGender ;
      private String AV30ReqIcon ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private String aP0_ApplicationClientId ;
      private GXCombobox cmbavGender ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      [ObjectCollection(ItemType=typeof( SdtGAMError ))]
      private IGxCollection AV12Errors ;
      private SdtGAMError AV11Error ;
      private SdtGAMRepository AV25Repository ;
      private SdtGAMUser AV33User ;
   }

}
