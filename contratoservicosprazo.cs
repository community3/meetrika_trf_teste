/*
               File: ContratoServicosPrazo
        Description: Prazo
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/29/2020 9:30:30.29
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contratoservicosprazo : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_35") == 0 )
         {
            A903ContratoServicosPrazo_CntSrvCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A903ContratoServicosPrazo_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A903ContratoServicosPrazo_CntSrvCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_35( A903ContratoServicosPrazo_CntSrvCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_37") == 0 )
         {
            A1212ContratoServicos_UnidadeContratada = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n1212ContratoServicos_UnidadeContratada = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1212ContratoServicos_UnidadeContratada", StringUtil.LTrim( StringUtil.Str( (decimal)(A1212ContratoServicos_UnidadeContratada), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_37( A1212ContratoServicos_UnidadeContratada) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_36") == 0 )
         {
            A1095ContratoServicosPrazo_ServicoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n1095ContratoServicosPrazo_ServicoCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1095ContratoServicosPrazo_ServicoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1095ContratoServicosPrazo_ServicoCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_36( A1095ContratoServicosPrazo_ServicoCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_38") == 0 )
         {
            A903ContratoServicosPrazo_CntSrvCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A903ContratoServicosPrazo_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A903ContratoServicosPrazo_CntSrvCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_38( A903ContratoServicosPrazo_CntSrvCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_39") == 0 )
         {
            A903ContratoServicosPrazo_CntSrvCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A903ContratoServicosPrazo_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A903ContratoServicosPrazo_CntSrvCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_39( A903ContratoServicosPrazo_CntSrvCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
         {
            nRC_GXsfl_97 = (short)(NumberUtil.Val( GetNextPar( ), "."));
            nGXsfl_97_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
            sGXsfl_97_idx = GetNextPar( );
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxnrGrid_newrow( ) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid2") == 0 )
         {
            nRC_GXsfl_112 = (short)(NumberUtil.Val( GetNextPar( ), "."));
            nGXsfl_112_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
            sGXsfl_112_idx = GetNextPar( );
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxnrGrid2_newrow( ) ;
            return  ;
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV7ContratoServicosPrazo_CntSrvCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7ContratoServicosPrazo_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContratoServicosPrazo_CntSrvCod), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTRATOSERVICOSPRAZO_CNTSRVCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7ContratoServicosPrazo_CntSrvCod), "ZZZZZ9")));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         cmbContratoServicosPrazo_Tipo.Name = "CONTRATOSERVICOSPRAZO_TIPO";
         cmbContratoServicosPrazo_Tipo.WebTags = "";
         cmbContratoServicosPrazo_Tipo.addItem("", "(Nenhum)", 0);
         cmbContratoServicosPrazo_Tipo.addItem("A", "Acumulado", 0);
         cmbContratoServicosPrazo_Tipo.addItem("C", "Complexidade", 0);
         cmbContratoServicosPrazo_Tipo.addItem("S", "Solicitado", 0);
         cmbContratoServicosPrazo_Tipo.addItem("F", "Fixo", 0);
         cmbContratoServicosPrazo_Tipo.addItem("E", "El�stico", 0);
         cmbContratoServicosPrazo_Tipo.addItem("V", "Vari�vel", 0);
         cmbContratoServicosPrazo_Tipo.addItem("P", "Progress�o Aritm�tica", 0);
         cmbContratoServicosPrazo_Tipo.addItem("O", "Combinado", 0);
         if ( cmbContratoServicosPrazo_Tipo.ItemCount > 0 )
         {
            A904ContratoServicosPrazo_Tipo = cmbContratoServicosPrazo_Tipo.getValidValue(A904ContratoServicosPrazo_Tipo);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A904ContratoServicosPrazo_Tipo", A904ContratoServicosPrazo_Tipo);
         }
         cmbContratoServicosPrazo_Momento.Name = "CONTRATOSERVICOSPRAZO_MOMENTO";
         cmbContratoServicosPrazo_Momento.WebTags = "";
         cmbContratoServicosPrazo_Momento.addItem("0", "Sempre", 0);
         cmbContratoServicosPrazo_Momento.addItem("1", "Exceto na Solicita��o", 0);
         if ( cmbContratoServicosPrazo_Momento.ItemCount > 0 )
         {
            A1823ContratoServicosPrazo_Momento = (short)(NumberUtil.Val( cmbContratoServicosPrazo_Momento.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1823ContratoServicosPrazo_Momento), 4, 0))), "."));
            n1823ContratoServicosPrazo_Momento = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1823ContratoServicosPrazo_Momento", StringUtil.LTrim( StringUtil.Str( (decimal)(A1823ContratoServicosPrazo_Momento), 4, 0)));
         }
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Prazo", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = cmbContratoServicosPrazo_Tipo_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public contratoservicosprazo( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public contratoservicosprazo( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           int aP1_ContratoServicosPrazo_CntSrvCod )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV7ContratoServicosPrazo_CntSrvCod = aP1_ContratoServicosPrazo_CntSrvCod;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbContratoServicosPrazo_Tipo = new GXCombobox();
         cmbContratoServicosPrazo_Momento = new GXCombobox();
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbContratoServicosPrazo_Tipo.ItemCount > 0 )
         {
            A904ContratoServicosPrazo_Tipo = cmbContratoServicosPrazo_Tipo.getValidValue(A904ContratoServicosPrazo_Tipo);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A904ContratoServicosPrazo_Tipo", A904ContratoServicosPrazo_Tipo);
         }
         if ( cmbContratoServicosPrazo_Momento.ItemCount > 0 )
         {
            A1823ContratoServicosPrazo_Momento = (short)(NumberUtil.Val( cmbContratoServicosPrazo_Momento.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1823ContratoServicosPrazo_Momento), 4, 0))), "."));
            n1823ContratoServicosPrazo_Momento = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1823ContratoServicosPrazo_Momento", StringUtil.LTrim( StringUtil.Str( (decimal)(A1823ContratoServicosPrazo_Momento), 4, 0)));
         }
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_2U112( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_2U112e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 127,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContratoServicosPrazo_CntSrvCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A903ContratoServicosPrazo_CntSrvCod), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(A903ContratoServicosPrazo_CntSrvCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,127);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoServicosPrazo_CntSrvCod_Jsonclick, 0, "Attribute", "", "", "", edtContratoServicosPrazo_CntSrvCod_Visible, edtContratoServicosPrazo_CntSrvCod_Enabled, 1, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContratoServicosPrazo.htm");
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_2U112( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_2U112( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_2U112e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_89_2U112( true) ;
         }
         return  ;
      }

      protected void wb_table3_89_2U112e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_104_2U112( true) ;
         }
         return  ;
      }

      protected void wb_table4_104_2U112e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table5_119_2U112( true) ;
         }
         return  ;
      }

      protected void wb_table5_119_2U112e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_2U112e( true) ;
         }
         else
         {
            wb_table1_2_2U112e( false) ;
         }
      }

      protected void wb_table5_119_2U112( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 122,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", "Confirmar", bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_trn_enter_Visible, bttBtn_trn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContratoServicosPrazo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 124,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "", "Fechar", bttBtn_trn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_trn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContratoServicosPrazo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 126,'',false,'',0)\"";
            ClassString = "btn btn-danger";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_delete_Internalname, "", "Eliminar", bttBtn_trn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_trn_delete_Visible, bttBtn_trn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContratoServicosPrazo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_119_2U112e( true) ;
         }
         else
         {
            wb_table5_119_2U112e( false) ;
         }
      }

      protected void wb_table4_104_2U112( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTblplanejamento_Internalname, tblTblplanejamento_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_PLANEJAMENTOContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_PLANEJAMENTOContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table6_109_2U112( true) ;
         }
         return  ;
      }

      protected void wb_table6_109_2U112e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_104_2U112e( true) ;
         }
         else
         {
            wb_table4_104_2U112e( false) ;
         }
      }

      protected void wb_table6_109_2U112( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblPlanejamento_Internalname, tblPlanejamento_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            Grid2Container.AddObjectProperty("GridName", "Grid2");
            Grid2Container.AddObjectProperty("Class", "WorkWithBorder WorkWith");
            Grid2Container.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
            Grid2Container.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
            Grid2Container.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid2_Backcolorstyle), 1, 0, ".", "")));
            Grid2Container.AddObjectProperty("CmpContext", "");
            Grid2Container.AddObjectProperty("InMasterPage", "false");
            Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
            Grid2Column.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1724ContratoServicosPrazoPlnj_Sequencial), 3, 0, ".", "")));
            Grid2Column.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosPrazoPlnj_Sequencial_Enabled), 5, 0, ".", "")));
            Grid2Container.AddColumnProperties(Grid2Column);
            Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
            Grid2Column.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A1734ContratoServicosPrazoPlnj_Inicio, 14, 5, ".", "")));
            Grid2Column.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosPrazoPlnj_Inicio_Enabled), 5, 0, ".", "")));
            Grid2Container.AddColumnProperties(Grid2Column);
            Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
            Grid2Column.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A1735ContratoServicosPrazoPlnj_Fim, 14, 5, ".", "")));
            Grid2Column.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosPrazoPlnj_Fim_Enabled), 5, 0, ".", "")));
            Grid2Container.AddColumnProperties(Grid2Column);
            Grid2Column = GXWebColumn.GetNew(isAjaxCallMode( ));
            Grid2Column.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1736ContratoServicosPrazoPlnj_Dias), 4, 0, ".", "")));
            Grid2Column.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosPrazoPlnj_Dias_Enabled), 5, 0, ".", "")));
            Grid2Container.AddColumnProperties(Grid2Column);
            Grid2Container.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid2_Allowselection), 1, 0, ".", "")));
            Grid2Container.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid2_Selectioncolor), 9, 0, ".", "")));
            Grid2Container.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid2_Allowhovering), 1, 0, ".", "")));
            Grid2Container.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid2_Hoveringcolor), 9, 0, ".", "")));
            Grid2Container.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid2_Allowcollapsing), 1, 0, ".", "")));
            Grid2Container.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid2_Collapsed), 1, 0, ".", "")));
            nGXsfl_112_idx = 0;
            if ( ( nKeyPressed == 1 ) && ( AnyError == 0 ) )
            {
               /* Enter key processing. */
               nBlankRcdCount188 = 5;
               if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
               {
                  /* Display confirmed (stored) records */
                  nRcdExists_188 = 1;
                  ScanStart2U188( ) ;
                  while ( RcdFound188 != 0 )
                  {
                     init_level_properties188( ) ;
                     getByPrimaryKey2U188( ) ;
                     AddRow2U188( ) ;
                     ScanNext2U188( ) ;
                  }
                  ScanEnd2U188( ) ;
                  nBlankRcdCount188 = 5;
               }
            }
            else if ( ( nKeyPressed == 3 ) || ( nKeyPressed == 4 ) || ( ( nKeyPressed == 1 ) && ( AnyError != 0 ) ) )
            {
               /* Button check  or addlines. */
               B1737ContratoServicosPrazo_QtdPlnj = A1737ContratoServicosPrazo_QtdPlnj;
               n1737ContratoServicosPrazo_QtdPlnj = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1737ContratoServicosPrazo_QtdPlnj", StringUtil.LTrim( StringUtil.Str( (decimal)(A1737ContratoServicosPrazo_QtdPlnj), 4, 0)));
               B910ContratoServicosPrazo_QtdRegras = A910ContratoServicosPrazo_QtdRegras;
               n910ContratoServicosPrazo_QtdRegras = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A910ContratoServicosPrazo_QtdRegras", StringUtil.LTrim( StringUtil.Str( (decimal)(A910ContratoServicosPrazo_QtdRegras), 4, 0)));
               standaloneNotModal2U188( ) ;
               standaloneModal2U188( ) ;
               sMode188 = Gx_mode;
               while ( nGXsfl_112_idx < nRC_GXsfl_112 )
               {
                  ReadRow2U188( ) ;
                  edtContratoServicosPrazoPlnj_Sequencial_Enabled = (int)(context.localUtil.CToN( cgiGet( "CONTRATOSERVICOSPRAZOPLNJ_SEQUENCIAL_"+sGXsfl_112_idx+"Enabled"), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosPrazoPlnj_Sequencial_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosPrazoPlnj_Sequencial_Enabled), 5, 0)));
                  edtContratoServicosPrazoPlnj_Inicio_Enabled = (int)(context.localUtil.CToN( cgiGet( "CONTRATOSERVICOSPRAZOPLNJ_INICIO_"+sGXsfl_112_idx+"Enabled"), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosPrazoPlnj_Inicio_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosPrazoPlnj_Inicio_Enabled), 5, 0)));
                  edtContratoServicosPrazoPlnj_Fim_Enabled = (int)(context.localUtil.CToN( cgiGet( "CONTRATOSERVICOSPRAZOPLNJ_FIM_"+sGXsfl_112_idx+"Enabled"), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosPrazoPlnj_Fim_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosPrazoPlnj_Fim_Enabled), 5, 0)));
                  edtContratoServicosPrazoPlnj_Dias_Enabled = (int)(context.localUtil.CToN( cgiGet( "CONTRATOSERVICOSPRAZOPLNJ_DIAS_"+sGXsfl_112_idx+"Enabled"), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosPrazoPlnj_Dias_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosPrazoPlnj_Dias_Enabled), 5, 0)));
                  if ( ( nRcdExists_188 == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") != 0 ) )
                  {
                     Gx_mode = "INS";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     standaloneModal2U188( ) ;
                  }
                  SendRow2U188( ) ;
               }
               Gx_mode = sMode188;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
               A1737ContratoServicosPrazo_QtdPlnj = B1737ContratoServicosPrazo_QtdPlnj;
               n1737ContratoServicosPrazo_QtdPlnj = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1737ContratoServicosPrazo_QtdPlnj", StringUtil.LTrim( StringUtil.Str( (decimal)(A1737ContratoServicosPrazo_QtdPlnj), 4, 0)));
               A910ContratoServicosPrazo_QtdRegras = B910ContratoServicosPrazo_QtdRegras;
               n910ContratoServicosPrazo_QtdRegras = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A910ContratoServicosPrazo_QtdRegras", StringUtil.LTrim( StringUtil.Str( (decimal)(A910ContratoServicosPrazo_QtdRegras), 4, 0)));
            }
            else
            {
               /* Get or get-alike key processing. */
               nBlankRcdCount188 = 5;
               nRcdExists_188 = 1;
               if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
               {
                  ScanStart2U188( ) ;
                  while ( RcdFound188 != 0 )
                  {
                     sGXsfl_112_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_112_idx+1), 4, 0)), 4, "0");
                     SubsflControlProps_112188( ) ;
                     init_level_properties188( ) ;
                     standaloneNotModal2U188( ) ;
                     getByPrimaryKey2U188( ) ;
                     standaloneModal2U188( ) ;
                     AddRow2U188( ) ;
                     ScanNext2U188( ) ;
                  }
                  ScanEnd2U188( ) ;
               }
            }
            /* Initialize fields for 'new' records and send them. */
            if ( ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 ) && ( StringUtil.StrCmp(Gx_mode, "DLT") != 0 ) )
            {
               sMode188 = Gx_mode;
               Gx_mode = "INS";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
               sGXsfl_112_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_112_idx+1), 4, 0)), 4, "0");
               SubsflControlProps_112188( ) ;
               InitAll2U188( ) ;
               init_level_properties188( ) ;
               B1737ContratoServicosPrazo_QtdPlnj = A1737ContratoServicosPrazo_QtdPlnj;
               n1737ContratoServicosPrazo_QtdPlnj = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1737ContratoServicosPrazo_QtdPlnj", StringUtil.LTrim( StringUtil.Str( (decimal)(A1737ContratoServicosPrazo_QtdPlnj), 4, 0)));
               B910ContratoServicosPrazo_QtdRegras = A910ContratoServicosPrazo_QtdRegras;
               n910ContratoServicosPrazo_QtdRegras = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A910ContratoServicosPrazo_QtdRegras", StringUtil.LTrim( StringUtil.Str( (decimal)(A910ContratoServicosPrazo_QtdRegras), 4, 0)));
               standaloneNotModal2U188( ) ;
               standaloneModal2U188( ) ;
               nRcdExists_188 = 0;
               nIsMod_188 = 0;
               nRcdDeleted_188 = 0;
               nBlankRcdCount188 = (short)(nBlankRcdUsr188+nBlankRcdCount188);
               fRowAdded = 0;
               while ( nBlankRcdCount188 > 0 )
               {
                  AddRow2U188( ) ;
                  if ( ( nKeyPressed == 4 ) && ( fRowAdded == 0 ) )
                  {
                     fRowAdded = 1;
                     GX_FocusControl = edtContratoServicosPrazoPlnj_Inicio_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  nBlankRcdCount188 = (short)(nBlankRcdCount188-1);
               }
               Gx_mode = sMode188;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
               A1737ContratoServicosPrazo_QtdPlnj = B1737ContratoServicosPrazo_QtdPlnj;
               n1737ContratoServicosPrazo_QtdPlnj = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1737ContratoServicosPrazo_QtdPlnj", StringUtil.LTrim( StringUtil.Str( (decimal)(A1737ContratoServicosPrazo_QtdPlnj), 4, 0)));
               A910ContratoServicosPrazo_QtdRegras = B910ContratoServicosPrazo_QtdRegras;
               n910ContratoServicosPrazo_QtdRegras = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A910ContratoServicosPrazo_QtdRegras", StringUtil.LTrim( StringUtil.Str( (decimal)(A910ContratoServicosPrazo_QtdRegras), 4, 0)));
            }
            sStyleString = "";
            context.WriteHtmlText( "<div id=\""+"Grid2Container"+"Div\" "+sStyleString+">"+"</div>") ;
            context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid2", Grid2Container);
            if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
            {
               GxWebStd.gx_hidden_field( context, "Grid2ContainerData", Grid2Container.ToJavascriptSource());
            }
            if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
            {
               GxWebStd.gx_hidden_field( context, "Grid2ContainerData"+"V", Grid2Container.GridValuesHidden());
            }
            else
            {
               context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"Grid2ContainerData"+"V"+"\" value='"+Grid2Container.GridValuesHidden()+"'/>") ;
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_109_2U112e( true) ;
         }
         else
         {
            wb_table6_109_2U112e( false) ;
         }
      }

      protected void wb_table3_89_2U112( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTblregras_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTblregras_Internalname, tblTblregras_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_REGRASContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_REGRASContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table7_94_2U112( true) ;
         }
         return  ;
      }

      protected void wb_table7_94_2U112e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_89_2U112e( true) ;
         }
         else
         {
            wb_table3_89_2U112e( false) ;
         }
      }

      protected void wb_table7_94_2U112( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblRegras_Internalname, tblRegras_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.AddObjectProperty("GridName", "Grid");
            GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
            GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
            GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
            GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
            GridContainer.AddObjectProperty("CmpContext", "");
            GridContainer.AddObjectProperty("InMasterPage", "false");
            GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A906ContratoServicosPrazoRegra_Sequencial), 4, 0, ".", "")));
            GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosPrazoRegra_Sequencial_Enabled), 5, 0, ".", "")));
            GridContainer.AddColumnProperties(GridColumn);
            GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A907ContratoServicosPrazoRegra_Inicio, 14, 5, ".", "")));
            GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosPrazoRegra_Inicio_Enabled), 5, 0, ".", "")));
            GridContainer.AddColumnProperties(GridColumn);
            GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A908ContratoServicosPrazoRegra_Fim, 14, 5, ".", "")));
            GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosPrazoRegra_Fim_Enabled), 5, 0, ".", "")));
            GridContainer.AddColumnProperties(GridColumn);
            GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
            GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A909ContratoServicosPrazoRegra_Dias), 3, 0, ".", "")));
            GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosPrazoRegra_Dias_Enabled), 5, 0, ".", "")));
            GridContainer.AddColumnProperties(GridColumn);
            GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
            GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
            GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
            GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
            GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
            GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            nGXsfl_97_idx = 0;
            if ( ( nKeyPressed == 1 ) && ( AnyError == 0 ) )
            {
               /* Enter key processing. */
               nBlankRcdCount113 = 5;
               if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
               {
                  /* Display confirmed (stored) records */
                  nRcdExists_113 = 1;
                  ScanStart2U113( ) ;
                  while ( RcdFound113 != 0 )
                  {
                     init_level_properties113( ) ;
                     getByPrimaryKey2U113( ) ;
                     AddRow2U113( ) ;
                     ScanNext2U113( ) ;
                  }
                  ScanEnd2U113( ) ;
                  nBlankRcdCount113 = 5;
               }
            }
            else if ( ( nKeyPressed == 3 ) || ( nKeyPressed == 4 ) || ( ( nKeyPressed == 1 ) && ( AnyError != 0 ) ) )
            {
               /* Button check  or addlines. */
               B1737ContratoServicosPrazo_QtdPlnj = A1737ContratoServicosPrazo_QtdPlnj;
               n1737ContratoServicosPrazo_QtdPlnj = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1737ContratoServicosPrazo_QtdPlnj", StringUtil.LTrim( StringUtil.Str( (decimal)(A1737ContratoServicosPrazo_QtdPlnj), 4, 0)));
               B910ContratoServicosPrazo_QtdRegras = A910ContratoServicosPrazo_QtdRegras;
               n910ContratoServicosPrazo_QtdRegras = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A910ContratoServicosPrazo_QtdRegras", StringUtil.LTrim( StringUtil.Str( (decimal)(A910ContratoServicosPrazo_QtdRegras), 4, 0)));
               standaloneNotModal2U113( ) ;
               standaloneModal2U113( ) ;
               sMode113 = Gx_mode;
               while ( nGXsfl_97_idx < nRC_GXsfl_97 )
               {
                  ReadRow2U113( ) ;
                  edtContratoServicosPrazoRegra_Sequencial_Enabled = (int)(context.localUtil.CToN( cgiGet( "CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_"+sGXsfl_97_idx+"Enabled"), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosPrazoRegra_Sequencial_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosPrazoRegra_Sequencial_Enabled), 5, 0)));
                  edtContratoServicosPrazoRegra_Inicio_Enabled = (int)(context.localUtil.CToN( cgiGet( "CONTRATOSERVICOSPRAZOREGRA_INICIO_"+sGXsfl_97_idx+"Enabled"), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosPrazoRegra_Inicio_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosPrazoRegra_Inicio_Enabled), 5, 0)));
                  edtContratoServicosPrazoRegra_Fim_Enabled = (int)(context.localUtil.CToN( cgiGet( "CONTRATOSERVICOSPRAZOREGRA_FIM_"+sGXsfl_97_idx+"Enabled"), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosPrazoRegra_Fim_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosPrazoRegra_Fim_Enabled), 5, 0)));
                  edtContratoServicosPrazoRegra_Dias_Enabled = (int)(context.localUtil.CToN( cgiGet( "CONTRATOSERVICOSPRAZOREGRA_DIAS_"+sGXsfl_97_idx+"Enabled"), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosPrazoRegra_Dias_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosPrazoRegra_Dias_Enabled), 5, 0)));
                  if ( ( nRcdExists_113 == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") != 0 ) )
                  {
                     Gx_mode = "INS";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     standaloneModal2U113( ) ;
                  }
                  SendRow2U113( ) ;
               }
               Gx_mode = sMode113;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
               A1737ContratoServicosPrazo_QtdPlnj = B1737ContratoServicosPrazo_QtdPlnj;
               n1737ContratoServicosPrazo_QtdPlnj = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1737ContratoServicosPrazo_QtdPlnj", StringUtil.LTrim( StringUtil.Str( (decimal)(A1737ContratoServicosPrazo_QtdPlnj), 4, 0)));
               A910ContratoServicosPrazo_QtdRegras = B910ContratoServicosPrazo_QtdRegras;
               n910ContratoServicosPrazo_QtdRegras = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A910ContratoServicosPrazo_QtdRegras", StringUtil.LTrim( StringUtil.Str( (decimal)(A910ContratoServicosPrazo_QtdRegras), 4, 0)));
            }
            else
            {
               /* Get or get-alike key processing. */
               nBlankRcdCount113 = 5;
               nRcdExists_113 = 1;
               if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
               {
                  ScanStart2U113( ) ;
                  while ( RcdFound113 != 0 )
                  {
                     sGXsfl_97_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_97_idx+1), 4, 0)), 4, "0");
                     SubsflControlProps_97113( ) ;
                     init_level_properties113( ) ;
                     standaloneNotModal2U113( ) ;
                     getByPrimaryKey2U113( ) ;
                     standaloneModal2U113( ) ;
                     AddRow2U113( ) ;
                     ScanNext2U113( ) ;
                  }
                  ScanEnd2U113( ) ;
               }
            }
            /* Initialize fields for 'new' records and send them. */
            if ( ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 ) && ( StringUtil.StrCmp(Gx_mode, "DLT") != 0 ) )
            {
               sMode113 = Gx_mode;
               Gx_mode = "INS";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
               sGXsfl_97_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_97_idx+1), 4, 0)), 4, "0");
               SubsflControlProps_97113( ) ;
               InitAll2U113( ) ;
               init_level_properties113( ) ;
               B1737ContratoServicosPrazo_QtdPlnj = A1737ContratoServicosPrazo_QtdPlnj;
               n1737ContratoServicosPrazo_QtdPlnj = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1737ContratoServicosPrazo_QtdPlnj", StringUtil.LTrim( StringUtil.Str( (decimal)(A1737ContratoServicosPrazo_QtdPlnj), 4, 0)));
               B910ContratoServicosPrazo_QtdRegras = A910ContratoServicosPrazo_QtdRegras;
               n910ContratoServicosPrazo_QtdRegras = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A910ContratoServicosPrazo_QtdRegras", StringUtil.LTrim( StringUtil.Str( (decimal)(A910ContratoServicosPrazo_QtdRegras), 4, 0)));
               standaloneNotModal2U113( ) ;
               standaloneModal2U113( ) ;
               nRcdExists_113 = 0;
               nIsMod_113 = 0;
               nRcdDeleted_113 = 0;
               nBlankRcdCount113 = (short)(nBlankRcdUsr113+nBlankRcdCount113);
               fRowAdded = 0;
               while ( nBlankRcdCount113 > 0 )
               {
                  AddRow2U113( ) ;
                  if ( ( nKeyPressed == 4 ) && ( fRowAdded == 0 ) )
                  {
                     fRowAdded = 1;
                     GX_FocusControl = edtContratoServicosPrazoRegra_Inicio_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  nBlankRcdCount113 = (short)(nBlankRcdCount113-1);
               }
               Gx_mode = sMode113;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
               A1737ContratoServicosPrazo_QtdPlnj = B1737ContratoServicosPrazo_QtdPlnj;
               n1737ContratoServicosPrazo_QtdPlnj = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1737ContratoServicosPrazo_QtdPlnj", StringUtil.LTrim( StringUtil.Str( (decimal)(A1737ContratoServicosPrazo_QtdPlnj), 4, 0)));
               A910ContratoServicosPrazo_QtdRegras = B910ContratoServicosPrazo_QtdRegras;
               n910ContratoServicosPrazo_QtdRegras = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A910ContratoServicosPrazo_QtdRegras", StringUtil.LTrim( StringUtil.Str( (decimal)(A910ContratoServicosPrazo_QtdRegras), 4, 0)));
            }
            sStyleString = "";
            context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
            context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
            if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
            {
               GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
            }
            if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
            {
               GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
            }
            else
            {
               context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_94_2U112e( true) ;
         }
         else
         {
            wb_table7_94_2U112e( false) ;
         }
      }

      protected void wb_table2_5_2U112( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TextBlockTitleCell'>") ;
            wb_table8_11_2U112( true) ;
         }
         return  ;
      }

      protected void wb_table8_11_2U112e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table9_21_2U112( true) ;
         }
         return  ;
      }

      protected void wb_table9_21_2U112e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table10_51_2U112( true) ;
         }
         return  ;
      }

      protected void wb_table10_51_2U112e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_2U112e( true) ;
         }
         else
         {
            wb_table2_5_2U112e( false) ;
         }
      }

      protected void wb_table10_51_2U112( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTblcomplexidade_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTblcomplexidade_Internalname, tblTblcomplexidade_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_COMPLEXIDADEContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_COMPLEXIDADEContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table11_56_2U112( true) ;
         }
         return  ;
      }

      protected void wb_table11_56_2U112e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table10_51_2U112e( true) ;
         }
         else
         {
            wb_table10_51_2U112e( false) ;
         }
      }

      protected void wb_table11_56_2U112( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblComplexidade_Internalname, tblComplexidade_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicosprazo_diasbaixa_Internalname, "Baixa", "", "", lblTextblockcontratoservicosprazo_diasbaixa_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosPrazo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table12_61_2U112( true) ;
         }
         return  ;
      }

      protected void wb_table12_61_2U112e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicosprazo_diasmedia_Internalname, "M�dia", "", "", lblTextblockcontratoservicosprazo_diasmedia_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosPrazo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table13_71_2U112( true) ;
         }
         return  ;
      }

      protected void wb_table13_71_2U112e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicosprazo_diasalta_Internalname, "Alta", "", "", lblTextblockcontratoservicosprazo_diasalta_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosPrazo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table14_81_2U112( true) ;
         }
         return  ;
      }

      protected void wb_table14_81_2U112e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table11_56_2U112e( true) ;
         }
         else
         {
            wb_table11_56_2U112e( false) ;
         }
      }

      protected void wb_table14_81_2U112( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedcontratoservicosprazo_diasalta_Internalname, tblTablemergedcontratoservicosprazo_diasalta_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 84,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContratoServicosPrazo_DiasAlta_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1263ContratoServicosPrazo_DiasAlta), 3, 0, ",", "")), ((edtContratoServicosPrazo_DiasAlta_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1263ContratoServicosPrazo_DiasAlta), "ZZ9")) : context.localUtil.Format( (decimal)(A1263ContratoServicosPrazo_DiasAlta), "ZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,84);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoServicosPrazo_DiasAlta_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContratoServicosPrazo_DiasAlta_Enabled, 0, "text", "", 60, "px", 1, "row", 3, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoServicosPrazo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblContratoservicosprazo_diasalta_righttext_Internalname, "dias", "", "", lblContratoservicosprazo_diasalta_righttext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosPrazo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table14_81_2U112e( true) ;
         }
         else
         {
            wb_table14_81_2U112e( false) ;
         }
      }

      protected void wb_table13_71_2U112( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedcontratoservicosprazo_diasmedia_Internalname, tblTablemergedcontratoservicosprazo_diasmedia_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 74,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContratoServicosPrazo_DiasMedia_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1264ContratoServicosPrazo_DiasMedia), 3, 0, ",", "")), ((edtContratoServicosPrazo_DiasMedia_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1264ContratoServicosPrazo_DiasMedia), "ZZ9")) : context.localUtil.Format( (decimal)(A1264ContratoServicosPrazo_DiasMedia), "ZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,74);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoServicosPrazo_DiasMedia_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContratoServicosPrazo_DiasMedia_Enabled, 0, "text", "", 60, "px", 1, "row", 3, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoServicosPrazo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblContratoservicosprazo_diasmedia_righttext_Internalname, "dias", "", "", lblContratoservicosprazo_diasmedia_righttext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosPrazo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table13_71_2U112e( true) ;
         }
         else
         {
            wb_table13_71_2U112e( false) ;
         }
      }

      protected void wb_table12_61_2U112( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedcontratoservicosprazo_diasbaixa_Internalname, tblTablemergedcontratoservicosprazo_diasbaixa_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 64,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContratoServicosPrazo_DiasBaixa_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1265ContratoServicosPrazo_DiasBaixa), 3, 0, ",", "")), ((edtContratoServicosPrazo_DiasBaixa_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1265ContratoServicosPrazo_DiasBaixa), "ZZ9")) : context.localUtil.Format( (decimal)(A1265ContratoServicosPrazo_DiasBaixa), "ZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,64);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoServicosPrazo_DiasBaixa_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContratoServicosPrazo_DiasBaixa_Enabled, 0, "text", "", 60, "px", 1, "row", 3, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoServicosPrazo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblContratoservicosprazo_diasbaixa_righttext_Internalname, "dias", "", "", lblContratoservicosprazo_diasbaixa_righttext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosPrazo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table12_61_2U112e( true) ;
         }
         else
         {
            wb_table12_61_2U112e( false) ;
         }
      }

      protected void wb_table9_21_2U112( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "Table", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicosprazo_tipo_Internalname, "Tipo", "", "", lblTextblockcontratoservicosprazo_tipo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosPrazo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 26,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContratoServicosPrazo_Tipo, cmbContratoServicosPrazo_Tipo_Internalname, StringUtil.RTrim( A904ContratoServicosPrazo_Tipo), 1, cmbContratoServicosPrazo_Tipo_Jsonclick, 7, "'"+""+"'"+",false,"+"'"+"e112u112_client"+"'", "char", "", 1, cmbContratoServicosPrazo_Tipo.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,26);\"", "", true, "HLP_ContratoServicosPrazo.htm");
            cmbContratoServicosPrazo_Tipo.CurrentValue = StringUtil.RTrim( A904ContratoServicosPrazo_Tipo);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratoServicosPrazo_Tipo_Internalname, "Values", (String)(cmbContratoServicosPrazo_Tipo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicosprazo_dias_Internalname, "Dias", "", "", lblTextblockcontratoservicosprazo_dias_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", lblTextblockcontratoservicosprazo_dias_Visible, 1, 0, "HLP_ContratoServicosPrazo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 30,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContratoServicosPrazo_Dias_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A905ContratoServicosPrazo_Dias), 3, 0, ",", "")), ((edtContratoServicosPrazo_Dias_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A905ContratoServicosPrazo_Dias), "ZZ9")) : context.localUtil.Format( (decimal)(A905ContratoServicosPrazo_Dias), "ZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,30);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoServicosPrazo_Dias_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtContratoServicosPrazo_Dias_Visible, edtContratoServicosPrazo_Dias_Enabled, 0, "text", "", 40, "px", 1, "row", 3, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoServicosPrazo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicosprazo_momento_Internalname, "Usar", "", "", lblTextblockcontratoservicosprazo_momento_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoServicosPrazo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 34,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContratoServicosPrazo_Momento, cmbContratoServicosPrazo_Momento_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A1823ContratoServicosPrazo_Momento), 4, 0)), 1, cmbContratoServicosPrazo_Momento_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, cmbContratoServicosPrazo_Momento.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,34);\"", "", true, "HLP_ContratoServicosPrazo.htm");
            cmbContratoServicosPrazo_Momento.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1823ContratoServicosPrazo_Momento), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratoServicosPrazo_Momento_Internalname, "Values", (String)(cmbContratoServicosPrazo_Momento.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratoservicosprazo_cada_Internalname, "Cada", "", "", lblTextblockcontratoservicosprazo_cada_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", lblTextblockcontratoservicosprazo_cada_Visible, 1, 0, "HLP_ContratoServicosPrazo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table15_39_2U112( true) ;
         }
         return  ;
      }

      protected void wb_table15_39_2U112e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_21_2U112e( true) ;
         }
         else
         {
            wb_table9_21_2U112e( false) ;
         }
      }

      protected void wb_table15_39_2U112( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedcontratoservicosprazo_cada_Internalname, tblTablemergedcontratoservicosprazo_cada_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 42,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContratoServicosPrazo_Cada_Internalname, StringUtil.LTrim( StringUtil.NToC( A1456ContratoServicosPrazo_Cada, 14, 5, ",", "")), ((edtContratoServicosPrazo_Cada_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A1456ContratoServicosPrazo_Cada, "ZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( A1456ContratoServicosPrazo_Cada, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,42);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoServicosPrazo_Cada_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtContratoServicosPrazo_Cada_Visible, edtContratoServicosPrazo_Cada_Enabled, 0, "text", "", 80, "px", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "PontosDeFuncao", "right", false, "HLP_ContratoServicosPrazo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratoServicos_UndCntSgl_Internalname, StringUtil.RTrim( A1712ContratoServicos_UndCntSgl), StringUtil.RTrim( context.localUtil.Format( A1712ContratoServicos_UndCntSgl, "@!")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoServicos_UndCntSgl_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtContratoServicos_UndCntSgl_Visible, edtContratoServicos_UndCntSgl_Enabled, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "Sigla", "left", true, "HLP_ContratoServicosPrazo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table15_39_2U112e( true) ;
         }
         else
         {
            wb_table15_39_2U112e( false) ;
         }
      }

      protected void wb_table8_11_2U112( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedviewtitle_Internalname, tblTablemergedviewtitle_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblViewtitle_Internalname, "Servi�o :: ", "", "", lblViewtitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_ContratoServicosPrazo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* Single line edit */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            /* * Property isDeleted not supported in */
            GxWebStd.gx_single_line_edit( context, edtavServico_sigla_Internalname, StringUtil.RTrim( AV11Servico_Sigla), StringUtil.RTrim( context.localUtil.Format( AV11Servico_Sigla, "@!")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavServico_sigla_Jsonclick, 0, "AttributeTitleWWP", "", "", "", 1, edtavServico_sigla_Enabled, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContratoServicosPrazo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_11_2U112e( true) ;
         }
         else
         {
            wb_table8_11_2U112e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E122U2 */
         E122U2 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               AV11Servico_Sigla = StringUtil.Upper( cgiGet( edtavServico_sigla_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Servico_Sigla", AV11Servico_Sigla);
               cmbContratoServicosPrazo_Tipo.Name = cmbContratoServicosPrazo_Tipo_Internalname;
               cmbContratoServicosPrazo_Tipo.CurrentValue = cgiGet( cmbContratoServicosPrazo_Tipo_Internalname);
               A904ContratoServicosPrazo_Tipo = cgiGet( cmbContratoServicosPrazo_Tipo_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A904ContratoServicosPrazo_Tipo", A904ContratoServicosPrazo_Tipo);
               if ( ( ( context.localUtil.CToN( cgiGet( edtContratoServicosPrazo_Dias_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContratoServicosPrazo_Dias_Internalname), ",", ".") > Convert.ToDecimal( 999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTRATOSERVICOSPRAZO_DIAS");
                  AnyError = 1;
                  GX_FocusControl = edtContratoServicosPrazo_Dias_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A905ContratoServicosPrazo_Dias = 0;
                  n905ContratoServicosPrazo_Dias = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A905ContratoServicosPrazo_Dias", StringUtil.LTrim( StringUtil.Str( (decimal)(A905ContratoServicosPrazo_Dias), 3, 0)));
               }
               else
               {
                  A905ContratoServicosPrazo_Dias = (short)(context.localUtil.CToN( cgiGet( edtContratoServicosPrazo_Dias_Internalname), ",", "."));
                  n905ContratoServicosPrazo_Dias = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A905ContratoServicosPrazo_Dias", StringUtil.LTrim( StringUtil.Str( (decimal)(A905ContratoServicosPrazo_Dias), 3, 0)));
               }
               n905ContratoServicosPrazo_Dias = ((0==A905ContratoServicosPrazo_Dias) ? true : false);
               cmbContratoServicosPrazo_Momento.Name = cmbContratoServicosPrazo_Momento_Internalname;
               cmbContratoServicosPrazo_Momento.CurrentValue = cgiGet( cmbContratoServicosPrazo_Momento_Internalname);
               A1823ContratoServicosPrazo_Momento = (short)(NumberUtil.Val( cgiGet( cmbContratoServicosPrazo_Momento_Internalname), "."));
               n1823ContratoServicosPrazo_Momento = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1823ContratoServicosPrazo_Momento", StringUtil.LTrim( StringUtil.Str( (decimal)(A1823ContratoServicosPrazo_Momento), 4, 0)));
               n1823ContratoServicosPrazo_Momento = ((0==A1823ContratoServicosPrazo_Momento) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtContratoServicosPrazo_Cada_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContratoServicosPrazo_Cada_Internalname), ",", ".") > 99999999.99999m ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTRATOSERVICOSPRAZO_CADA");
                  AnyError = 1;
                  GX_FocusControl = edtContratoServicosPrazo_Cada_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1456ContratoServicosPrazo_Cada = 0;
                  n1456ContratoServicosPrazo_Cada = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1456ContratoServicosPrazo_Cada", StringUtil.LTrim( StringUtil.Str( A1456ContratoServicosPrazo_Cada, 14, 5)));
               }
               else
               {
                  A1456ContratoServicosPrazo_Cada = context.localUtil.CToN( cgiGet( edtContratoServicosPrazo_Cada_Internalname), ",", ".");
                  n1456ContratoServicosPrazo_Cada = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1456ContratoServicosPrazo_Cada", StringUtil.LTrim( StringUtil.Str( A1456ContratoServicosPrazo_Cada, 14, 5)));
               }
               n1456ContratoServicosPrazo_Cada = ((Convert.ToDecimal(0)==A1456ContratoServicosPrazo_Cada) ? true : false);
               A1712ContratoServicos_UndCntSgl = StringUtil.Upper( cgiGet( edtContratoServicos_UndCntSgl_Internalname));
               n1712ContratoServicos_UndCntSgl = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1712ContratoServicos_UndCntSgl", A1712ContratoServicos_UndCntSgl);
               if ( ( ( context.localUtil.CToN( cgiGet( edtContratoServicosPrazo_DiasBaixa_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContratoServicosPrazo_DiasBaixa_Internalname), ",", ".") > Convert.ToDecimal( 999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTRATOSERVICOSPRAZO_DIASBAIXA");
                  AnyError = 1;
                  GX_FocusControl = edtContratoServicosPrazo_DiasBaixa_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1265ContratoServicosPrazo_DiasBaixa = 0;
                  n1265ContratoServicosPrazo_DiasBaixa = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1265ContratoServicosPrazo_DiasBaixa", StringUtil.LTrim( StringUtil.Str( (decimal)(A1265ContratoServicosPrazo_DiasBaixa), 3, 0)));
               }
               else
               {
                  A1265ContratoServicosPrazo_DiasBaixa = (short)(context.localUtil.CToN( cgiGet( edtContratoServicosPrazo_DiasBaixa_Internalname), ",", "."));
                  n1265ContratoServicosPrazo_DiasBaixa = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1265ContratoServicosPrazo_DiasBaixa", StringUtil.LTrim( StringUtil.Str( (decimal)(A1265ContratoServicosPrazo_DiasBaixa), 3, 0)));
               }
               n1265ContratoServicosPrazo_DiasBaixa = ((0==A1265ContratoServicosPrazo_DiasBaixa) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtContratoServicosPrazo_DiasMedia_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContratoServicosPrazo_DiasMedia_Internalname), ",", ".") > Convert.ToDecimal( 999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTRATOSERVICOSPRAZO_DIASMEDIA");
                  AnyError = 1;
                  GX_FocusControl = edtContratoServicosPrazo_DiasMedia_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1264ContratoServicosPrazo_DiasMedia = 0;
                  n1264ContratoServicosPrazo_DiasMedia = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1264ContratoServicosPrazo_DiasMedia", StringUtil.LTrim( StringUtil.Str( (decimal)(A1264ContratoServicosPrazo_DiasMedia), 3, 0)));
               }
               else
               {
                  A1264ContratoServicosPrazo_DiasMedia = (short)(context.localUtil.CToN( cgiGet( edtContratoServicosPrazo_DiasMedia_Internalname), ",", "."));
                  n1264ContratoServicosPrazo_DiasMedia = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1264ContratoServicosPrazo_DiasMedia", StringUtil.LTrim( StringUtil.Str( (decimal)(A1264ContratoServicosPrazo_DiasMedia), 3, 0)));
               }
               n1264ContratoServicosPrazo_DiasMedia = ((0==A1264ContratoServicosPrazo_DiasMedia) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtContratoServicosPrazo_DiasAlta_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContratoServicosPrazo_DiasAlta_Internalname), ",", ".") > Convert.ToDecimal( 999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTRATOSERVICOSPRAZO_DIASALTA");
                  AnyError = 1;
                  GX_FocusControl = edtContratoServicosPrazo_DiasAlta_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1263ContratoServicosPrazo_DiasAlta = 0;
                  n1263ContratoServicosPrazo_DiasAlta = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1263ContratoServicosPrazo_DiasAlta", StringUtil.LTrim( StringUtil.Str( (decimal)(A1263ContratoServicosPrazo_DiasAlta), 3, 0)));
               }
               else
               {
                  A1263ContratoServicosPrazo_DiasAlta = (short)(context.localUtil.CToN( cgiGet( edtContratoServicosPrazo_DiasAlta_Internalname), ",", "."));
                  n1263ContratoServicosPrazo_DiasAlta = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1263ContratoServicosPrazo_DiasAlta", StringUtil.LTrim( StringUtil.Str( (decimal)(A1263ContratoServicosPrazo_DiasAlta), 3, 0)));
               }
               n1263ContratoServicosPrazo_DiasAlta = ((0==A1263ContratoServicosPrazo_DiasAlta) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtContratoServicosPrazo_CntSrvCod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContratoServicosPrazo_CntSrvCod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTRATOSERVICOSPRAZO_CNTSRVCOD");
                  AnyError = 1;
                  GX_FocusControl = edtContratoServicosPrazo_CntSrvCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A903ContratoServicosPrazo_CntSrvCod = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A903ContratoServicosPrazo_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A903ContratoServicosPrazo_CntSrvCod), 6, 0)));
               }
               else
               {
                  A903ContratoServicosPrazo_CntSrvCod = (int)(context.localUtil.CToN( cgiGet( edtContratoServicosPrazo_CntSrvCod_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A903ContratoServicosPrazo_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A903ContratoServicosPrazo_CntSrvCod), 6, 0)));
               }
               /* Read saved values. */
               Z903ContratoServicosPrazo_CntSrvCod = (int)(context.localUtil.CToN( cgiGet( "Z903ContratoServicosPrazo_CntSrvCod"), ",", "."));
               Z905ContratoServicosPrazo_Dias = (short)(context.localUtil.CToN( cgiGet( "Z905ContratoServicosPrazo_Dias"), ",", "."));
               n905ContratoServicosPrazo_Dias = ((0==A905ContratoServicosPrazo_Dias) ? true : false);
               Z904ContratoServicosPrazo_Tipo = cgiGet( "Z904ContratoServicosPrazo_Tipo");
               Z1456ContratoServicosPrazo_Cada = context.localUtil.CToN( cgiGet( "Z1456ContratoServicosPrazo_Cada"), ",", ".");
               n1456ContratoServicosPrazo_Cada = ((Convert.ToDecimal(0)==A1456ContratoServicosPrazo_Cada) ? true : false);
               Z1263ContratoServicosPrazo_DiasAlta = (short)(context.localUtil.CToN( cgiGet( "Z1263ContratoServicosPrazo_DiasAlta"), ",", "."));
               n1263ContratoServicosPrazo_DiasAlta = ((0==A1263ContratoServicosPrazo_DiasAlta) ? true : false);
               Z1264ContratoServicosPrazo_DiasMedia = (short)(context.localUtil.CToN( cgiGet( "Z1264ContratoServicosPrazo_DiasMedia"), ",", "."));
               n1264ContratoServicosPrazo_DiasMedia = ((0==A1264ContratoServicosPrazo_DiasMedia) ? true : false);
               Z1265ContratoServicosPrazo_DiasBaixa = (short)(context.localUtil.CToN( cgiGet( "Z1265ContratoServicosPrazo_DiasBaixa"), ",", "."));
               n1265ContratoServicosPrazo_DiasBaixa = ((0==A1265ContratoServicosPrazo_DiasBaixa) ? true : false);
               Z1823ContratoServicosPrazo_Momento = (short)(context.localUtil.CToN( cgiGet( "Z1823ContratoServicosPrazo_Momento"), ",", "."));
               n1823ContratoServicosPrazo_Momento = ((0==A1823ContratoServicosPrazo_Momento) ? true : false);
               O1737ContratoServicosPrazo_QtdPlnj = (short)(context.localUtil.CToN( cgiGet( "O1737ContratoServicosPrazo_QtdPlnj"), ",", "."));
               O910ContratoServicosPrazo_QtdRegras = (short)(context.localUtil.CToN( cgiGet( "O910ContratoServicosPrazo_QtdRegras"), ",", "."));
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               nRC_GXsfl_97 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_97"), ",", "."));
               nRC_GXsfl_112 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_112"), ",", "."));
               AV7ContratoServicosPrazo_CntSrvCod = (int)(context.localUtil.CToN( cgiGet( "vCONTRATOSERVICOSPRAZO_CNTSRVCOD"), ",", "."));
               A1096ContratoServicosPrazo_ServicoSigla = cgiGet( "CONTRATOSERVICOSPRAZO_SERVICOSIGLA");
               n1096ContratoServicosPrazo_ServicoSigla = false;
               A910ContratoServicosPrazo_QtdRegras = (short)(context.localUtil.CToN( cgiGet( "CONTRATOSERVICOSPRAZO_QTDREGRAS"), ",", "."));
               ajax_req_read_hidden_sdt(cgiGet( "vAUDITINGOBJECT"), AV12AuditingObject);
               A1212ContratoServicos_UnidadeContratada = (int)(context.localUtil.CToN( cgiGet( "CONTRATOSERVICOS_UNIDADECONTRATADA"), ",", "."));
               A1095ContratoServicosPrazo_ServicoCod = (int)(context.localUtil.CToN( cgiGet( "CONTRATOSERVICOSPRAZO_SERVICOCOD"), ",", "."));
               A1737ContratoServicosPrazo_QtdPlnj = (short)(context.localUtil.CToN( cgiGet( "CONTRATOSERVICOSPRAZO_QTDPLNJ"), ",", "."));
               AV14Pgmname = cgiGet( "vPGMNAME");
               Gx_BScreen = (short)(context.localUtil.CToN( cgiGet( "vGXBSCREEN"), ",", "."));
               Gx_mode = cgiGet( "vMODE");
               Dvpanel_tableattributes_Width = cgiGet( "DVPANEL_TABLEATTRIBUTES_Width");
               Dvpanel_tableattributes_Height = cgiGet( "DVPANEL_TABLEATTRIBUTES_Height");
               Dvpanel_tableattributes_Cls = cgiGet( "DVPANEL_TABLEATTRIBUTES_Cls");
               Dvpanel_tableattributes_Title = cgiGet( "DVPANEL_TABLEATTRIBUTES_Title");
               Dvpanel_tableattributes_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsible"));
               Dvpanel_tableattributes_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsed"));
               Dvpanel_tableattributes_Enabled = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Enabled"));
               Dvpanel_tableattributes_Class = cgiGet( "DVPANEL_TABLEATTRIBUTES_Class");
               Dvpanel_tableattributes_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autowidth"));
               Dvpanel_tableattributes_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoheight"));
               Dvpanel_tableattributes_Showheader = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showheader"));
               Dvpanel_tableattributes_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showcollapseicon"));
               Dvpanel_tableattributes_Iconposition = cgiGet( "DVPANEL_TABLEATTRIBUTES_Iconposition");
               Dvpanel_tableattributes_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoscroll"));
               Dvpanel_tableattributes_Visible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Visible"));
               Dvpanel_complexidade_Width = cgiGet( "DVPANEL_COMPLEXIDADE_Width");
               Dvpanel_complexidade_Height = cgiGet( "DVPANEL_COMPLEXIDADE_Height");
               Dvpanel_complexidade_Cls = cgiGet( "DVPANEL_COMPLEXIDADE_Cls");
               Dvpanel_complexidade_Title = cgiGet( "DVPANEL_COMPLEXIDADE_Title");
               Dvpanel_complexidade_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_COMPLEXIDADE_Collapsible"));
               Dvpanel_complexidade_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_COMPLEXIDADE_Collapsed"));
               Dvpanel_complexidade_Enabled = StringUtil.StrToBool( cgiGet( "DVPANEL_COMPLEXIDADE_Enabled"));
               Dvpanel_complexidade_Class = cgiGet( "DVPANEL_COMPLEXIDADE_Class");
               Dvpanel_complexidade_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_COMPLEXIDADE_Autowidth"));
               Dvpanel_complexidade_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_COMPLEXIDADE_Autoheight"));
               Dvpanel_complexidade_Showheader = StringUtil.StrToBool( cgiGet( "DVPANEL_COMPLEXIDADE_Showheader"));
               Dvpanel_complexidade_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_COMPLEXIDADE_Showcollapseicon"));
               Dvpanel_complexidade_Iconposition = cgiGet( "DVPANEL_COMPLEXIDADE_Iconposition");
               Dvpanel_complexidade_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_COMPLEXIDADE_Autoscroll"));
               Dvpanel_complexidade_Visible = StringUtil.StrToBool( cgiGet( "DVPANEL_COMPLEXIDADE_Visible"));
               Dvpanel_regras_Width = cgiGet( "DVPANEL_REGRAS_Width");
               Dvpanel_regras_Height = cgiGet( "DVPANEL_REGRAS_Height");
               Dvpanel_regras_Cls = cgiGet( "DVPANEL_REGRAS_Cls");
               Dvpanel_regras_Title = cgiGet( "DVPANEL_REGRAS_Title");
               Dvpanel_regras_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_REGRAS_Collapsible"));
               Dvpanel_regras_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_REGRAS_Collapsed"));
               Dvpanel_regras_Enabled = StringUtil.StrToBool( cgiGet( "DVPANEL_REGRAS_Enabled"));
               Dvpanel_regras_Class = cgiGet( "DVPANEL_REGRAS_Class");
               Dvpanel_regras_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_REGRAS_Autowidth"));
               Dvpanel_regras_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_REGRAS_Autoheight"));
               Dvpanel_regras_Showheader = StringUtil.StrToBool( cgiGet( "DVPANEL_REGRAS_Showheader"));
               Dvpanel_regras_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_REGRAS_Showcollapseicon"));
               Dvpanel_regras_Iconposition = cgiGet( "DVPANEL_REGRAS_Iconposition");
               Dvpanel_regras_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_REGRAS_Autoscroll"));
               Dvpanel_regras_Visible = StringUtil.StrToBool( cgiGet( "DVPANEL_REGRAS_Visible"));
               Dvpanel_planejamento_Width = cgiGet( "DVPANEL_PLANEJAMENTO_Width");
               Dvpanel_planejamento_Height = cgiGet( "DVPANEL_PLANEJAMENTO_Height");
               Dvpanel_planejamento_Cls = cgiGet( "DVPANEL_PLANEJAMENTO_Cls");
               Dvpanel_planejamento_Title = cgiGet( "DVPANEL_PLANEJAMENTO_Title");
               Dvpanel_planejamento_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_PLANEJAMENTO_Collapsible"));
               Dvpanel_planejamento_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_PLANEJAMENTO_Collapsed"));
               Dvpanel_planejamento_Enabled = StringUtil.StrToBool( cgiGet( "DVPANEL_PLANEJAMENTO_Enabled"));
               Dvpanel_planejamento_Class = cgiGet( "DVPANEL_PLANEJAMENTO_Class");
               Dvpanel_planejamento_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_PLANEJAMENTO_Autowidth"));
               Dvpanel_planejamento_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_PLANEJAMENTO_Autoheight"));
               Dvpanel_planejamento_Showheader = StringUtil.StrToBool( cgiGet( "DVPANEL_PLANEJAMENTO_Showheader"));
               Dvpanel_planejamento_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_PLANEJAMENTO_Showcollapseicon"));
               Dvpanel_planejamento_Iconposition = cgiGet( "DVPANEL_PLANEJAMENTO_Iconposition");
               Dvpanel_planejamento_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_PLANEJAMENTO_Autoscroll"));
               Dvpanel_planejamento_Visible = StringUtil.StrToBool( cgiGet( "DVPANEL_PLANEJAMENTO_Visible"));
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "ContratoServicosPrazo";
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( AV14Pgmname, ""));
               hsh = cgiGet( "hsh");
               if ( ( ! ( ( A903ContratoServicosPrazo_CntSrvCod != Z903ContratoServicosPrazo_CntSrvCod ) ) || ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ) && ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("contratoservicosprazo:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GXUtil.WriteLog("contratoservicosprazo:[SecurityCheckFailed value for]"+"Pgmname:"+StringUtil.RTrim( context.localUtil.Format( AV14Pgmname, "")));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               /* Check if conditions changed and reset current page numbers */
               /* Check if conditions changed and reset current page numbers */
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  A903ContratoServicosPrazo_CntSrvCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A903ContratoServicosPrazo_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A903ContratoServicosPrazo_CntSrvCod), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode112 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     Gx_mode = sMode112;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound112 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_2U0( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_trn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "CONTRATOSERVICOSPRAZO_CNTSRVCOD");
                        AnyError = 1;
                        GX_FocusControl = edtContratoServicosPrazo_CntSrvCod_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E122U2 */
                           E122U2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E132U2 */
                           E132U2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                        sEvtType = StringUtil.Right( sEvt, 4);
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E132U2 */
            E132U2 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll2U112( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         bttBtn_trn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtn_trn_delete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_trn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
            }
            DisableAttributes2U112( ) ;
         }
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavServico_sigla_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavServico_sigla_Enabled), 5, 0)));
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_2U0( )
      {
         BeforeValidate2U112( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls2U112( ) ;
            }
            else
            {
               CheckExtendedTable2U112( ) ;
               CloseExtendedTableCursors2U112( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            /* Save parent mode. */
            sMode112 = Gx_mode;
            CONFIRM_2U113( ) ;
            if ( AnyError == 0 )
            {
               CONFIRM_2U188( ) ;
               if ( AnyError == 0 )
               {
                  /* Restore parent mode. */
                  Gx_mode = sMode112;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  IsConfirmed = 1;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
               }
            }
            /* Restore parent mode. */
            Gx_mode = sMode112;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
      }

      protected void CONFIRM_2U188( )
      {
         s1737ContratoServicosPrazo_QtdPlnj = O1737ContratoServicosPrazo_QtdPlnj;
         n1737ContratoServicosPrazo_QtdPlnj = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1737ContratoServicosPrazo_QtdPlnj", StringUtil.LTrim( StringUtil.Str( (decimal)(A1737ContratoServicosPrazo_QtdPlnj), 4, 0)));
         nGXsfl_112_idx = 0;
         while ( nGXsfl_112_idx < nRC_GXsfl_112 )
         {
            ReadRow2U188( ) ;
            if ( ( nRcdExists_188 != 0 ) || ( nIsMod_188 != 0 ) )
            {
               GetKey2U188( ) ;
               if ( ( nRcdExists_188 == 0 ) && ( nRcdDeleted_188 == 0 ) )
               {
                  if ( RcdFound188 == 0 )
                  {
                     Gx_mode = "INS";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     BeforeValidate2U188( ) ;
                     if ( AnyError == 0 )
                     {
                        CheckExtendedTable2U188( ) ;
                        CloseExtendedTableCursors2U188( ) ;
                        if ( AnyError == 0 )
                        {
                           IsConfirmed = 1;
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
                        }
                        O1737ContratoServicosPrazo_QtdPlnj = A1737ContratoServicosPrazo_QtdPlnj;
                        n1737ContratoServicosPrazo_QtdPlnj = false;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1737ContratoServicosPrazo_QtdPlnj", StringUtil.LTrim( StringUtil.Str( (decimal)(A1737ContratoServicosPrazo_QtdPlnj), 4, 0)));
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "CONTRATOSERVICOSPRAZO_CNTSRVCOD");
                     AnyError = 1;
                     GX_FocusControl = edtContratoServicosPrazo_CntSrvCod_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( RcdFound188 != 0 )
                  {
                     if ( nRcdDeleted_188 != 0 )
                     {
                        Gx_mode = "DLT";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                        getByPrimaryKey2U188( ) ;
                        Load2U188( ) ;
                        BeforeValidate2U188( ) ;
                        if ( AnyError == 0 )
                        {
                           OnDeleteControls2U188( ) ;
                           O1737ContratoServicosPrazo_QtdPlnj = A1737ContratoServicosPrazo_QtdPlnj;
                           n1737ContratoServicosPrazo_QtdPlnj = false;
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1737ContratoServicosPrazo_QtdPlnj", StringUtil.LTrim( StringUtil.Str( (decimal)(A1737ContratoServicosPrazo_QtdPlnj), 4, 0)));
                        }
                     }
                     else
                     {
                        if ( nIsMod_188 != 0 )
                        {
                           Gx_mode = "UPD";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                           BeforeValidate2U188( ) ;
                           if ( AnyError == 0 )
                           {
                              CheckExtendedTable2U188( ) ;
                              CloseExtendedTableCursors2U188( ) ;
                              if ( AnyError == 0 )
                              {
                                 IsConfirmed = 1;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
                              }
                              O1737ContratoServicosPrazo_QtdPlnj = A1737ContratoServicosPrazo_QtdPlnj;
                              n1737ContratoServicosPrazo_QtdPlnj = false;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1737ContratoServicosPrazo_QtdPlnj", StringUtil.LTrim( StringUtil.Str( (decimal)(A1737ContratoServicosPrazo_QtdPlnj), 4, 0)));
                           }
                        }
                     }
                  }
                  else
                  {
                     if ( nRcdDeleted_188 == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "CONTRATOSERVICOSPRAZO_CNTSRVCOD");
                        AnyError = 1;
                        GX_FocusControl = edtContratoServicosPrazo_CntSrvCod_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
            ChangePostValue( edtContratoServicosPrazoPlnj_Sequencial_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1724ContratoServicosPrazoPlnj_Sequencial), 3, 0, ",", ""))) ;
            ChangePostValue( edtContratoServicosPrazoPlnj_Inicio_Internalname, StringUtil.LTrim( StringUtil.NToC( A1734ContratoServicosPrazoPlnj_Inicio, 14, 5, ",", ""))) ;
            ChangePostValue( edtContratoServicosPrazoPlnj_Fim_Internalname, StringUtil.LTrim( StringUtil.NToC( A1735ContratoServicosPrazoPlnj_Fim, 14, 5, ",", ""))) ;
            ChangePostValue( edtContratoServicosPrazoPlnj_Dias_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1736ContratoServicosPrazoPlnj_Dias), 4, 0, ",", ""))) ;
            ChangePostValue( "ZT_"+"Z1724ContratoServicosPrazoPlnj_Sequencial_"+sGXsfl_112_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1724ContratoServicosPrazoPlnj_Sequencial), 3, 0, ",", ""))) ;
            ChangePostValue( "ZT_"+"Z1734ContratoServicosPrazoPlnj_Inicio_"+sGXsfl_112_idx, StringUtil.LTrim( StringUtil.NToC( Z1734ContratoServicosPrazoPlnj_Inicio, 14, 5, ",", ""))) ;
            ChangePostValue( "ZT_"+"Z1735ContratoServicosPrazoPlnj_Fim_"+sGXsfl_112_idx, StringUtil.LTrim( StringUtil.NToC( Z1735ContratoServicosPrazoPlnj_Fim, 14, 5, ",", ""))) ;
            ChangePostValue( "ZT_"+"Z1736ContratoServicosPrazoPlnj_Dias_"+sGXsfl_112_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1736ContratoServicosPrazoPlnj_Dias), 4, 0, ",", ""))) ;
            ChangePostValue( "nRcdDeleted_188_"+sGXsfl_112_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdDeleted_188), 4, 0, ",", ""))) ;
            ChangePostValue( "nRcdExists_188_"+sGXsfl_112_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdExists_188), 4, 0, ",", ""))) ;
            ChangePostValue( "nIsMod_188_"+sGXsfl_112_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nIsMod_188), 4, 0, ",", ""))) ;
            if ( nIsMod_188 != 0 )
            {
               ChangePostValue( "CONTRATOSERVICOSPRAZOPLNJ_SEQUENCIAL_"+sGXsfl_112_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosPrazoPlnj_Sequencial_Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "CONTRATOSERVICOSPRAZOPLNJ_INICIO_"+sGXsfl_112_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosPrazoPlnj_Inicio_Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "CONTRATOSERVICOSPRAZOPLNJ_FIM_"+sGXsfl_112_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosPrazoPlnj_Fim_Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "CONTRATOSERVICOSPRAZOPLNJ_DIAS_"+sGXsfl_112_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosPrazoPlnj_Dias_Enabled), 5, 0, ".", ""))) ;
            }
         }
         O1737ContratoServicosPrazo_QtdPlnj = s1737ContratoServicosPrazo_QtdPlnj;
         n1737ContratoServicosPrazo_QtdPlnj = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1737ContratoServicosPrazo_QtdPlnj", StringUtil.LTrim( StringUtil.Str( (decimal)(A1737ContratoServicosPrazo_QtdPlnj), 4, 0)));
         /* Start of After( level) rules */
         /* End of After( level) rules */
      }

      protected void CONFIRM_2U113( )
      {
         s910ContratoServicosPrazo_QtdRegras = O910ContratoServicosPrazo_QtdRegras;
         n910ContratoServicosPrazo_QtdRegras = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A910ContratoServicosPrazo_QtdRegras", StringUtil.LTrim( StringUtil.Str( (decimal)(A910ContratoServicosPrazo_QtdRegras), 4, 0)));
         nGXsfl_97_idx = 0;
         while ( nGXsfl_97_idx < nRC_GXsfl_97 )
         {
            ReadRow2U113( ) ;
            if ( ( nRcdExists_113 != 0 ) || ( nIsMod_113 != 0 ) )
            {
               GetKey2U113( ) ;
               if ( ( nRcdExists_113 == 0 ) && ( nRcdDeleted_113 == 0 ) )
               {
                  if ( RcdFound113 == 0 )
                  {
                     Gx_mode = "INS";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     BeforeValidate2U113( ) ;
                     if ( AnyError == 0 )
                     {
                        CheckExtendedTable2U113( ) ;
                        CloseExtendedTableCursors2U113( ) ;
                        if ( AnyError == 0 )
                        {
                           IsConfirmed = 1;
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
                        }
                        O910ContratoServicosPrazo_QtdRegras = A910ContratoServicosPrazo_QtdRegras;
                        n910ContratoServicosPrazo_QtdRegras = false;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A910ContratoServicosPrazo_QtdRegras", StringUtil.LTrim( StringUtil.Str( (decimal)(A910ContratoServicosPrazo_QtdRegras), 4, 0)));
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "CONTRATOSERVICOSPRAZO_CNTSRVCOD");
                     AnyError = 1;
                     GX_FocusControl = edtContratoServicosPrazo_CntSrvCod_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( RcdFound113 != 0 )
                  {
                     if ( nRcdDeleted_113 != 0 )
                     {
                        Gx_mode = "DLT";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                        getByPrimaryKey2U113( ) ;
                        Load2U113( ) ;
                        BeforeValidate2U113( ) ;
                        if ( AnyError == 0 )
                        {
                           OnDeleteControls2U113( ) ;
                           O910ContratoServicosPrazo_QtdRegras = A910ContratoServicosPrazo_QtdRegras;
                           n910ContratoServicosPrazo_QtdRegras = false;
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A910ContratoServicosPrazo_QtdRegras", StringUtil.LTrim( StringUtil.Str( (decimal)(A910ContratoServicosPrazo_QtdRegras), 4, 0)));
                        }
                     }
                     else
                     {
                        if ( nIsMod_113 != 0 )
                        {
                           Gx_mode = "UPD";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                           BeforeValidate2U113( ) ;
                           if ( AnyError == 0 )
                           {
                              CheckExtendedTable2U113( ) ;
                              CloseExtendedTableCursors2U113( ) ;
                              if ( AnyError == 0 )
                              {
                                 IsConfirmed = 1;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
                              }
                              O910ContratoServicosPrazo_QtdRegras = A910ContratoServicosPrazo_QtdRegras;
                              n910ContratoServicosPrazo_QtdRegras = false;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A910ContratoServicosPrazo_QtdRegras", StringUtil.LTrim( StringUtil.Str( (decimal)(A910ContratoServicosPrazo_QtdRegras), 4, 0)));
                           }
                        }
                     }
                  }
                  else
                  {
                     if ( nRcdDeleted_113 == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "CONTRATOSERVICOSPRAZO_CNTSRVCOD");
                        AnyError = 1;
                        GX_FocusControl = edtContratoServicosPrazo_CntSrvCod_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
            ChangePostValue( edtContratoServicosPrazoRegra_Sequencial_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A906ContratoServicosPrazoRegra_Sequencial), 4, 0, ",", ""))) ;
            ChangePostValue( edtContratoServicosPrazoRegra_Inicio_Internalname, StringUtil.LTrim( StringUtil.NToC( A907ContratoServicosPrazoRegra_Inicio, 14, 5, ",", ""))) ;
            ChangePostValue( edtContratoServicosPrazoRegra_Fim_Internalname, StringUtil.LTrim( StringUtil.NToC( A908ContratoServicosPrazoRegra_Fim, 14, 5, ",", ""))) ;
            ChangePostValue( edtContratoServicosPrazoRegra_Dias_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A909ContratoServicosPrazoRegra_Dias), 3, 0, ",", ""))) ;
            ChangePostValue( "ZT_"+"Z906ContratoServicosPrazoRegra_Sequencial_"+sGXsfl_97_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(Z906ContratoServicosPrazoRegra_Sequencial), 4, 0, ",", ""))) ;
            ChangePostValue( "ZT_"+"Z907ContratoServicosPrazoRegra_Inicio_"+sGXsfl_97_idx, StringUtil.LTrim( StringUtil.NToC( Z907ContratoServicosPrazoRegra_Inicio, 14, 5, ",", ""))) ;
            ChangePostValue( "ZT_"+"Z908ContratoServicosPrazoRegra_Fim_"+sGXsfl_97_idx, StringUtil.LTrim( StringUtil.NToC( Z908ContratoServicosPrazoRegra_Fim, 14, 5, ",", ""))) ;
            ChangePostValue( "ZT_"+"Z909ContratoServicosPrazoRegra_Dias_"+sGXsfl_97_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(Z909ContratoServicosPrazoRegra_Dias), 3, 0, ",", ""))) ;
            ChangePostValue( "nRcdDeleted_113_"+sGXsfl_97_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdDeleted_113), 4, 0, ",", ""))) ;
            ChangePostValue( "nRcdExists_113_"+sGXsfl_97_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdExists_113), 4, 0, ",", ""))) ;
            ChangePostValue( "nIsMod_113_"+sGXsfl_97_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nIsMod_113), 4, 0, ",", ""))) ;
            if ( nIsMod_113 != 0 )
            {
               ChangePostValue( "CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_"+sGXsfl_97_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosPrazoRegra_Sequencial_Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "CONTRATOSERVICOSPRAZOREGRA_INICIO_"+sGXsfl_97_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosPrazoRegra_Inicio_Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "CONTRATOSERVICOSPRAZOREGRA_FIM_"+sGXsfl_97_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosPrazoRegra_Fim_Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "CONTRATOSERVICOSPRAZOREGRA_DIAS_"+sGXsfl_97_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosPrazoRegra_Dias_Enabled), 5, 0, ".", ""))) ;
            }
         }
         O910ContratoServicosPrazo_QtdRegras = s910ContratoServicosPrazo_QtdRegras;
         n910ContratoServicosPrazo_QtdRegras = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A910ContratoServicosPrazo_QtdRegras", StringUtil.LTrim( StringUtil.Str( (decimal)(A910ContratoServicosPrazo_QtdRegras), 4, 0)));
         /* Start of After( level) rules */
         /* End of After( level) rules */
      }

      protected void ResetCaption2U0( )
      {
      }

      protected void E122U2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
         edtContratoServicosPrazo_CntSrvCod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosPrazo_CntSrvCod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosPrazo_CntSrvCod_Visible), 5, 0)));
      }

      protected void E132U2( )
      {
         /* After Trn Routine */
         new wwpbaseobjects.audittransaction(context ).execute(  AV12AuditingObject,  AV14Pgmname) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14Pgmname", AV14Pgmname);
         if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV9TrnContext.gxTpr_Callerondelete )
         {
            context.wjLoc = formatLink("wwcontratoservicosprazo.aspx") ;
            context.wjLocDisableFrm = 1;
         }
         context.setWebReturnParms(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void S112( )
      {
         /* 'VISUALIZAR' Routine */
         lblTextblockcontratoservicosprazo_dias_Visible = (((StringUtil.StrCmp(A904ContratoServicosPrazo_Tipo, "F")==0)||(StringUtil.StrCmp(A904ContratoServicosPrazo_Tipo, "E")==0)||(StringUtil.StrCmp(A904ContratoServicosPrazo_Tipo, "P")==0)) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockcontratoservicosprazo_dias_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockcontratoservicosprazo_dias_Visible), 5, 0)));
         edtContratoServicosPrazo_Dias_Visible = (((StringUtil.StrCmp(A904ContratoServicosPrazo_Tipo, "F")==0)||(StringUtil.StrCmp(A904ContratoServicosPrazo_Tipo, "E")==0)||(StringUtil.StrCmp(A904ContratoServicosPrazo_Tipo, "P")==0)) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosPrazo_Dias_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosPrazo_Dias_Visible), 5, 0)));
         lblTextblockcontratoservicosprazo_cada_Visible = ((StringUtil.StrCmp(A904ContratoServicosPrazo_Tipo, "E")==0)||(StringUtil.StrCmp(A904ContratoServicosPrazo_Tipo, "P")==0) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockcontratoservicosprazo_cada_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockcontratoservicosprazo_cada_Visible), 5, 0)));
         edtContratoServicosPrazo_Cada_Visible = ((StringUtil.StrCmp(A904ContratoServicosPrazo_Tipo, "E")==0)||(StringUtil.StrCmp(A904ContratoServicosPrazo_Tipo, "P")==0) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosPrazo_Cada_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosPrazo_Cada_Visible), 5, 0)));
         edtContratoServicos_UndCntSgl_Visible = ((StringUtil.StrCmp(A904ContratoServicosPrazo_Tipo, "E")==0)||(StringUtil.StrCmp(A904ContratoServicosPrazo_Tipo, "P")==0) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicos_UndCntSgl_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicos_UndCntSgl_Visible), 5, 0)));
         tblTblregras_Visible = (((StringUtil.StrCmp(A904ContratoServicosPrazo_Tipo, "V")==0)) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTblregras_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTblregras_Visible), 5, 0)));
         tblTblcomplexidade_Visible = (((StringUtil.StrCmp(A904ContratoServicosPrazo_Tipo, "C")==0)) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTblcomplexidade_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTblcomplexidade_Visible), 5, 0)));
      }

      protected void ZM2U112( short GX_JID )
      {
         if ( ( GX_JID == 34 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z905ContratoServicosPrazo_Dias = T002U7_A905ContratoServicosPrazo_Dias[0];
               Z904ContratoServicosPrazo_Tipo = T002U7_A904ContratoServicosPrazo_Tipo[0];
               Z1456ContratoServicosPrazo_Cada = T002U7_A1456ContratoServicosPrazo_Cada[0];
               Z1263ContratoServicosPrazo_DiasAlta = T002U7_A1263ContratoServicosPrazo_DiasAlta[0];
               Z1264ContratoServicosPrazo_DiasMedia = T002U7_A1264ContratoServicosPrazo_DiasMedia[0];
               Z1265ContratoServicosPrazo_DiasBaixa = T002U7_A1265ContratoServicosPrazo_DiasBaixa[0];
               Z1823ContratoServicosPrazo_Momento = T002U7_A1823ContratoServicosPrazo_Momento[0];
            }
            else
            {
               Z905ContratoServicosPrazo_Dias = A905ContratoServicosPrazo_Dias;
               Z904ContratoServicosPrazo_Tipo = A904ContratoServicosPrazo_Tipo;
               Z1456ContratoServicosPrazo_Cada = A1456ContratoServicosPrazo_Cada;
               Z1263ContratoServicosPrazo_DiasAlta = A1263ContratoServicosPrazo_DiasAlta;
               Z1264ContratoServicosPrazo_DiasMedia = A1264ContratoServicosPrazo_DiasMedia;
               Z1265ContratoServicosPrazo_DiasBaixa = A1265ContratoServicosPrazo_DiasBaixa;
               Z1823ContratoServicosPrazo_Momento = A1823ContratoServicosPrazo_Momento;
            }
         }
         if ( GX_JID == -34 )
         {
            Z905ContratoServicosPrazo_Dias = A905ContratoServicosPrazo_Dias;
            Z904ContratoServicosPrazo_Tipo = A904ContratoServicosPrazo_Tipo;
            Z1456ContratoServicosPrazo_Cada = A1456ContratoServicosPrazo_Cada;
            Z1263ContratoServicosPrazo_DiasAlta = A1263ContratoServicosPrazo_DiasAlta;
            Z1264ContratoServicosPrazo_DiasMedia = A1264ContratoServicosPrazo_DiasMedia;
            Z1265ContratoServicosPrazo_DiasBaixa = A1265ContratoServicosPrazo_DiasBaixa;
            Z1823ContratoServicosPrazo_Momento = A1823ContratoServicosPrazo_Momento;
            Z903ContratoServicosPrazo_CntSrvCod = A903ContratoServicosPrazo_CntSrvCod;
            Z1212ContratoServicos_UnidadeContratada = A1212ContratoServicos_UnidadeContratada;
            Z1095ContratoServicosPrazo_ServicoCod = A1095ContratoServicosPrazo_ServicoCod;
            Z1712ContratoServicos_UndCntSgl = A1712ContratoServicos_UndCntSgl;
            Z1096ContratoServicosPrazo_ServicoSigla = A1096ContratoServicosPrazo_ServicoSigla;
            Z910ContratoServicosPrazo_QtdRegras = A910ContratoServicosPrazo_QtdRegras;
            Z1737ContratoServicosPrazo_QtdPlnj = A1737ContratoServicosPrazo_QtdPlnj;
         }
      }

      protected void standaloneNotModal( )
      {
         edtContratoServicos_UndCntSgl_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicos_UndCntSgl_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicos_UndCntSgl_Enabled), 5, 0)));
         AV14Pgmname = "ContratoServicosPrazo";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14Pgmname", AV14Pgmname);
         Gx_BScreen = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         edtContratoServicos_UndCntSgl_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicos_UndCntSgl_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicos_UndCntSgl_Enabled), 5, 0)));
         bttBtn_trn_delete_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         if ( ! (0==AV7ContratoServicosPrazo_CntSrvCod) )
         {
            A903ContratoServicosPrazo_CntSrvCod = AV7ContratoServicosPrazo_CntSrvCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A903ContratoServicosPrazo_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A903ContratoServicosPrazo_CntSrvCod), 6, 0)));
         }
         if ( ! (0==AV7ContratoServicosPrazo_CntSrvCod) )
         {
            edtContratoServicosPrazo_CntSrvCod_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosPrazo_CntSrvCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosPrazo_CntSrvCod_Enabled), 5, 0)));
         }
         else
         {
            edtContratoServicosPrazo_CntSrvCod_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosPrazo_CntSrvCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosPrazo_CntSrvCod_Enabled), 5, 0)));
         }
         if ( ! (0==AV7ContratoServicosPrazo_CntSrvCod) )
         {
            edtContratoServicosPrazo_CntSrvCod_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosPrazo_CntSrvCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosPrazo_CntSrvCod_Enabled), 5, 0)));
         }
      }

      protected void standaloneModal( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
            /* Using cursor T002U8 */
            pr_default.execute(6, new Object[] {A903ContratoServicosPrazo_CntSrvCod});
            A1212ContratoServicos_UnidadeContratada = T002U8_A1212ContratoServicos_UnidadeContratada[0];
            n1212ContratoServicos_UnidadeContratada = T002U8_n1212ContratoServicos_UnidadeContratada[0];
            A1095ContratoServicosPrazo_ServicoCod = T002U8_A1095ContratoServicosPrazo_ServicoCod[0];
            n1095ContratoServicosPrazo_ServicoCod = T002U8_n1095ContratoServicosPrazo_ServicoCod[0];
            pr_default.close(6);
            /* Using cursor T002U10 */
            pr_default.execute(8, new Object[] {n1212ContratoServicos_UnidadeContratada, A1212ContratoServicos_UnidadeContratada});
            A1712ContratoServicos_UndCntSgl = T002U10_A1712ContratoServicos_UndCntSgl[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1712ContratoServicos_UndCntSgl", A1712ContratoServicos_UndCntSgl);
            n1712ContratoServicos_UndCntSgl = T002U10_n1712ContratoServicos_UndCntSgl[0];
            pr_default.close(8);
            /* Using cursor T002U9 */
            pr_default.execute(7, new Object[] {n1095ContratoServicosPrazo_ServicoCod, A1095ContratoServicosPrazo_ServicoCod});
            A1096ContratoServicosPrazo_ServicoSigla = T002U9_A1096ContratoServicosPrazo_ServicoSigla[0];
            n1096ContratoServicosPrazo_ServicoSigla = T002U9_n1096ContratoServicosPrazo_ServicoSigla[0];
            pr_default.close(7);
            AV11Servico_Sigla = A1096ContratoServicosPrazo_ServicoSigla;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Servico_Sigla", AV11Servico_Sigla);
            /* Using cursor T002U12 */
            pr_default.execute(9, new Object[] {A903ContratoServicosPrazo_CntSrvCod});
            if ( (pr_default.getStatus(9) != 101) )
            {
               A910ContratoServicosPrazo_QtdRegras = T002U12_A910ContratoServicosPrazo_QtdRegras[0];
               n910ContratoServicosPrazo_QtdRegras = T002U12_n910ContratoServicosPrazo_QtdRegras[0];
            }
            else
            {
               A910ContratoServicosPrazo_QtdRegras = 0;
               n910ContratoServicosPrazo_QtdRegras = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A910ContratoServicosPrazo_QtdRegras", StringUtil.LTrim( StringUtil.Str( (decimal)(A910ContratoServicosPrazo_QtdRegras), 4, 0)));
            }
            O910ContratoServicosPrazo_QtdRegras = A910ContratoServicosPrazo_QtdRegras;
            n910ContratoServicosPrazo_QtdRegras = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A910ContratoServicosPrazo_QtdRegras", StringUtil.LTrim( StringUtil.Str( (decimal)(A910ContratoServicosPrazo_QtdRegras), 4, 0)));
            pr_default.close(9);
            /* Using cursor T002U14 */
            pr_default.execute(10, new Object[] {A903ContratoServicosPrazo_CntSrvCod});
            if ( (pr_default.getStatus(10) != 101) )
            {
               A1737ContratoServicosPrazo_QtdPlnj = T002U14_A1737ContratoServicosPrazo_QtdPlnj[0];
               n1737ContratoServicosPrazo_QtdPlnj = T002U14_n1737ContratoServicosPrazo_QtdPlnj[0];
            }
            else
            {
               A1737ContratoServicosPrazo_QtdPlnj = 0;
               n1737ContratoServicosPrazo_QtdPlnj = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1737ContratoServicosPrazo_QtdPlnj", StringUtil.LTrim( StringUtil.Str( (decimal)(A1737ContratoServicosPrazo_QtdPlnj), 4, 0)));
            }
            O1737ContratoServicosPrazo_QtdPlnj = A1737ContratoServicosPrazo_QtdPlnj;
            n1737ContratoServicosPrazo_QtdPlnj = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1737ContratoServicosPrazo_QtdPlnj", StringUtil.LTrim( StringUtil.Str( (decimal)(A1737ContratoServicosPrazo_QtdPlnj), 4, 0)));
            pr_default.close(10);
         }
      }

      protected void Load2U112( )
      {
         /* Using cursor T002U17 */
         pr_default.execute(11, new Object[] {A903ContratoServicosPrazo_CntSrvCod});
         if ( (pr_default.getStatus(11) != 101) )
         {
            RcdFound112 = 1;
            A1212ContratoServicos_UnidadeContratada = T002U17_A1212ContratoServicos_UnidadeContratada[0];
            n1212ContratoServicos_UnidadeContratada = T002U17_n1212ContratoServicos_UnidadeContratada[0];
            A905ContratoServicosPrazo_Dias = T002U17_A905ContratoServicosPrazo_Dias[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A905ContratoServicosPrazo_Dias", StringUtil.LTrim( StringUtil.Str( (decimal)(A905ContratoServicosPrazo_Dias), 3, 0)));
            n905ContratoServicosPrazo_Dias = T002U17_n905ContratoServicosPrazo_Dias[0];
            A904ContratoServicosPrazo_Tipo = T002U17_A904ContratoServicosPrazo_Tipo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A904ContratoServicosPrazo_Tipo", A904ContratoServicosPrazo_Tipo);
            A1456ContratoServicosPrazo_Cada = T002U17_A1456ContratoServicosPrazo_Cada[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1456ContratoServicosPrazo_Cada", StringUtil.LTrim( StringUtil.Str( A1456ContratoServicosPrazo_Cada, 14, 5)));
            n1456ContratoServicosPrazo_Cada = T002U17_n1456ContratoServicosPrazo_Cada[0];
            A1712ContratoServicos_UndCntSgl = T002U17_A1712ContratoServicos_UndCntSgl[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1712ContratoServicos_UndCntSgl", A1712ContratoServicos_UndCntSgl);
            n1712ContratoServicos_UndCntSgl = T002U17_n1712ContratoServicos_UndCntSgl[0];
            A1096ContratoServicosPrazo_ServicoSigla = T002U17_A1096ContratoServicosPrazo_ServicoSigla[0];
            n1096ContratoServicosPrazo_ServicoSigla = T002U17_n1096ContratoServicosPrazo_ServicoSigla[0];
            A1263ContratoServicosPrazo_DiasAlta = T002U17_A1263ContratoServicosPrazo_DiasAlta[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1263ContratoServicosPrazo_DiasAlta", StringUtil.LTrim( StringUtil.Str( (decimal)(A1263ContratoServicosPrazo_DiasAlta), 3, 0)));
            n1263ContratoServicosPrazo_DiasAlta = T002U17_n1263ContratoServicosPrazo_DiasAlta[0];
            A1264ContratoServicosPrazo_DiasMedia = T002U17_A1264ContratoServicosPrazo_DiasMedia[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1264ContratoServicosPrazo_DiasMedia", StringUtil.LTrim( StringUtil.Str( (decimal)(A1264ContratoServicosPrazo_DiasMedia), 3, 0)));
            n1264ContratoServicosPrazo_DiasMedia = T002U17_n1264ContratoServicosPrazo_DiasMedia[0];
            A1265ContratoServicosPrazo_DiasBaixa = T002U17_A1265ContratoServicosPrazo_DiasBaixa[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1265ContratoServicosPrazo_DiasBaixa", StringUtil.LTrim( StringUtil.Str( (decimal)(A1265ContratoServicosPrazo_DiasBaixa), 3, 0)));
            n1265ContratoServicosPrazo_DiasBaixa = T002U17_n1265ContratoServicosPrazo_DiasBaixa[0];
            A1823ContratoServicosPrazo_Momento = T002U17_A1823ContratoServicosPrazo_Momento[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1823ContratoServicosPrazo_Momento", StringUtil.LTrim( StringUtil.Str( (decimal)(A1823ContratoServicosPrazo_Momento), 4, 0)));
            n1823ContratoServicosPrazo_Momento = T002U17_n1823ContratoServicosPrazo_Momento[0];
            A1095ContratoServicosPrazo_ServicoCod = T002U17_A1095ContratoServicosPrazo_ServicoCod[0];
            n1095ContratoServicosPrazo_ServicoCod = T002U17_n1095ContratoServicosPrazo_ServicoCod[0];
            A910ContratoServicosPrazo_QtdRegras = T002U17_A910ContratoServicosPrazo_QtdRegras[0];
            n910ContratoServicosPrazo_QtdRegras = T002U17_n910ContratoServicosPrazo_QtdRegras[0];
            A1737ContratoServicosPrazo_QtdPlnj = T002U17_A1737ContratoServicosPrazo_QtdPlnj[0];
            n1737ContratoServicosPrazo_QtdPlnj = T002U17_n1737ContratoServicosPrazo_QtdPlnj[0];
            ZM2U112( -34) ;
         }
         pr_default.close(11);
         OnLoadActions2U112( ) ;
      }

      protected void OnLoadActions2U112( )
      {
         O1737ContratoServicosPrazo_QtdPlnj = A1737ContratoServicosPrazo_QtdPlnj;
         n1737ContratoServicosPrazo_QtdPlnj = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1737ContratoServicosPrazo_QtdPlnj", StringUtil.LTrim( StringUtil.Str( (decimal)(A1737ContratoServicosPrazo_QtdPlnj), 4, 0)));
         O910ContratoServicosPrazo_QtdRegras = A910ContratoServicosPrazo_QtdRegras;
         n910ContratoServicosPrazo_QtdRegras = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A910ContratoServicosPrazo_QtdRegras", StringUtil.LTrim( StringUtil.Str( (decimal)(A910ContratoServicosPrazo_QtdRegras), 4, 0)));
         AV11Servico_Sigla = A1096ContratoServicosPrazo_ServicoSigla;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Servico_Sigla", AV11Servico_Sigla);
         lblTextblockcontratoservicosprazo_dias_Visible = (((StringUtil.StrCmp(A904ContratoServicosPrazo_Tipo, "F")==0)||(StringUtil.StrCmp(A904ContratoServicosPrazo_Tipo, "E")==0)||(StringUtil.StrCmp(A904ContratoServicosPrazo_Tipo, "P")==0)) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockcontratoservicosprazo_dias_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockcontratoservicosprazo_dias_Visible), 5, 0)));
         edtContratoServicosPrazo_Dias_Visible = (((StringUtil.StrCmp(A904ContratoServicosPrazo_Tipo, "F")==0)||(StringUtil.StrCmp(A904ContratoServicosPrazo_Tipo, "E")==0)||(StringUtil.StrCmp(A904ContratoServicosPrazo_Tipo, "P")==0)) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosPrazo_Dias_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosPrazo_Dias_Visible), 5, 0)));
         lblTextblockcontratoservicosprazo_cada_Visible = (((StringUtil.StrCmp(A904ContratoServicosPrazo_Tipo, "E")==0)||(StringUtil.StrCmp(A904ContratoServicosPrazo_Tipo, "P")==0)) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockcontratoservicosprazo_cada_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockcontratoservicosprazo_cada_Visible), 5, 0)));
         edtContratoServicosPrazo_Cada_Visible = (((StringUtil.StrCmp(A904ContratoServicosPrazo_Tipo, "E")==0)||(StringUtil.StrCmp(A904ContratoServicosPrazo_Tipo, "P")==0)) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosPrazo_Cada_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosPrazo_Cada_Visible), 5, 0)));
         edtContratoServicos_UndCntSgl_Visible = (((StringUtil.StrCmp(A904ContratoServicosPrazo_Tipo, "E")==0)||(StringUtil.StrCmp(A904ContratoServicosPrazo_Tipo, "P")==0)) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicos_UndCntSgl_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicos_UndCntSgl_Visible), 5, 0)));
         tblTblregras_Visible = (((StringUtil.StrCmp(A904ContratoServicosPrazo_Tipo, "V")==0)) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTblregras_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTblregras_Visible), 5, 0)));
         tblTblcomplexidade_Visible = (((StringUtil.StrCmp(A904ContratoServicosPrazo_Tipo, "C")==0)) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTblcomplexidade_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTblcomplexidade_Visible), 5, 0)));
         if ( ! ( StringUtil.StrCmp(A904ContratoServicosPrazo_Tipo, "F") == 0 ) && ! ( StringUtil.StrCmp(A904ContratoServicosPrazo_Tipo, "E") == 0 ) && ! ( StringUtil.StrCmp(A904ContratoServicosPrazo_Tipo, "P") == 0 ) )
         {
            A905ContratoServicosPrazo_Dias = 0;
            n905ContratoServicosPrazo_Dias = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A905ContratoServicosPrazo_Dias", StringUtil.LTrim( StringUtil.Str( (decimal)(A905ContratoServicosPrazo_Dias), 3, 0)));
         }
      }

      protected void CheckExtendedTable2U112( )
      {
         Gx_BScreen = 1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         standaloneModal( ) ;
         /* Using cursor T002U8 */
         pr_default.execute(6, new Object[] {A903ContratoServicosPrazo_CntSrvCod});
         if ( (pr_default.getStatus(6) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato Servicos Prazos_Contrato Servicos'.", "ForeignKeyNotFound", 1, "CONTRATOSERVICOSPRAZO_CNTSRVCOD");
            AnyError = 1;
            GX_FocusControl = edtContratoServicosPrazo_CntSrvCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A1212ContratoServicos_UnidadeContratada = T002U8_A1212ContratoServicos_UnidadeContratada[0];
         n1212ContratoServicos_UnidadeContratada = T002U8_n1212ContratoServicos_UnidadeContratada[0];
         A1095ContratoServicosPrazo_ServicoCod = T002U8_A1095ContratoServicosPrazo_ServicoCod[0];
         n1095ContratoServicosPrazo_ServicoCod = T002U8_n1095ContratoServicosPrazo_ServicoCod[0];
         pr_default.close(6);
         /* Using cursor T002U10 */
         pr_default.execute(8, new Object[] {n1212ContratoServicos_UnidadeContratada, A1212ContratoServicos_UnidadeContratada});
         if ( (pr_default.getStatus(8) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato Servicos_Unidade Medicao'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A1712ContratoServicos_UndCntSgl = T002U10_A1712ContratoServicos_UndCntSgl[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1712ContratoServicos_UndCntSgl", A1712ContratoServicos_UndCntSgl);
         n1712ContratoServicos_UndCntSgl = T002U10_n1712ContratoServicos_UndCntSgl[0];
         pr_default.close(8);
         /* Using cursor T002U9 */
         pr_default.execute(7, new Object[] {n1095ContratoServicosPrazo_ServicoCod, A1095ContratoServicosPrazo_ServicoCod});
         if ( (pr_default.getStatus(7) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato Servicos Prazos_Contrato Servicos'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A1096ContratoServicosPrazo_ServicoSigla = T002U9_A1096ContratoServicosPrazo_ServicoSigla[0];
         n1096ContratoServicosPrazo_ServicoSigla = T002U9_n1096ContratoServicosPrazo_ServicoSigla[0];
         pr_default.close(7);
         AV11Servico_Sigla = A1096ContratoServicosPrazo_ServicoSigla;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Servico_Sigla", AV11Servico_Sigla);
         /* Using cursor T002U12 */
         pr_default.execute(9, new Object[] {A903ContratoServicosPrazo_CntSrvCod});
         if ( (pr_default.getStatus(9) != 101) )
         {
            A910ContratoServicosPrazo_QtdRegras = T002U12_A910ContratoServicosPrazo_QtdRegras[0];
            n910ContratoServicosPrazo_QtdRegras = T002U12_n910ContratoServicosPrazo_QtdRegras[0];
         }
         else
         {
            A910ContratoServicosPrazo_QtdRegras = 0;
            n910ContratoServicosPrazo_QtdRegras = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A910ContratoServicosPrazo_QtdRegras", StringUtil.LTrim( StringUtil.Str( (decimal)(A910ContratoServicosPrazo_QtdRegras), 4, 0)));
         }
         pr_default.close(9);
         /* Using cursor T002U14 */
         pr_default.execute(10, new Object[] {A903ContratoServicosPrazo_CntSrvCod});
         if ( (pr_default.getStatus(10) != 101) )
         {
            A1737ContratoServicosPrazo_QtdPlnj = T002U14_A1737ContratoServicosPrazo_QtdPlnj[0];
            n1737ContratoServicosPrazo_QtdPlnj = T002U14_n1737ContratoServicosPrazo_QtdPlnj[0];
         }
         else
         {
            A1737ContratoServicosPrazo_QtdPlnj = 0;
            n1737ContratoServicosPrazo_QtdPlnj = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1737ContratoServicosPrazo_QtdPlnj", StringUtil.LTrim( StringUtil.Str( (decimal)(A1737ContratoServicosPrazo_QtdPlnj), 4, 0)));
         }
         pr_default.close(10);
         if ( ! ( ( StringUtil.StrCmp(A904ContratoServicosPrazo_Tipo, "A") == 0 ) || ( StringUtil.StrCmp(A904ContratoServicosPrazo_Tipo, "C") == 0 ) || ( StringUtil.StrCmp(A904ContratoServicosPrazo_Tipo, "S") == 0 ) || ( StringUtil.StrCmp(A904ContratoServicosPrazo_Tipo, "F") == 0 ) || ( StringUtil.StrCmp(A904ContratoServicosPrazo_Tipo, "E") == 0 ) || ( StringUtil.StrCmp(A904ContratoServicosPrazo_Tipo, "V") == 0 ) || ( StringUtil.StrCmp(A904ContratoServicosPrazo_Tipo, "P") == 0 ) || ( StringUtil.StrCmp(A904ContratoServicosPrazo_Tipo, "O") == 0 ) ) )
         {
            GX_msglist.addItem("Campo Tipo fora do intervalo", "OutOfRange", 1, "CONTRATOSERVICOSPRAZO_TIPO");
            AnyError = 1;
            GX_FocusControl = cmbContratoServicosPrazo_Tipo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         lblTextblockcontratoservicosprazo_dias_Visible = (((StringUtil.StrCmp(A904ContratoServicosPrazo_Tipo, "F")==0)||(StringUtil.StrCmp(A904ContratoServicosPrazo_Tipo, "E")==0)||(StringUtil.StrCmp(A904ContratoServicosPrazo_Tipo, "P")==0)) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockcontratoservicosprazo_dias_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockcontratoservicosprazo_dias_Visible), 5, 0)));
         edtContratoServicosPrazo_Dias_Visible = (((StringUtil.StrCmp(A904ContratoServicosPrazo_Tipo, "F")==0)||(StringUtil.StrCmp(A904ContratoServicosPrazo_Tipo, "E")==0)||(StringUtil.StrCmp(A904ContratoServicosPrazo_Tipo, "P")==0)) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosPrazo_Dias_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosPrazo_Dias_Visible), 5, 0)));
         lblTextblockcontratoservicosprazo_cada_Visible = (((StringUtil.StrCmp(A904ContratoServicosPrazo_Tipo, "E")==0)||(StringUtil.StrCmp(A904ContratoServicosPrazo_Tipo, "P")==0)) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockcontratoservicosprazo_cada_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockcontratoservicosprazo_cada_Visible), 5, 0)));
         edtContratoServicosPrazo_Cada_Visible = (((StringUtil.StrCmp(A904ContratoServicosPrazo_Tipo, "E")==0)||(StringUtil.StrCmp(A904ContratoServicosPrazo_Tipo, "P")==0)) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosPrazo_Cada_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosPrazo_Cada_Visible), 5, 0)));
         edtContratoServicos_UndCntSgl_Visible = (((StringUtil.StrCmp(A904ContratoServicosPrazo_Tipo, "E")==0)||(StringUtil.StrCmp(A904ContratoServicosPrazo_Tipo, "P")==0)) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicos_UndCntSgl_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicos_UndCntSgl_Visible), 5, 0)));
         tblTblregras_Visible = (((StringUtil.StrCmp(A904ContratoServicosPrazo_Tipo, "V")==0)) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTblregras_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTblregras_Visible), 5, 0)));
         tblTblcomplexidade_Visible = (((StringUtil.StrCmp(A904ContratoServicosPrazo_Tipo, "C")==0)) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTblcomplexidade_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTblcomplexidade_Visible), 5, 0)));
         if ( ! ( StringUtil.StrCmp(A904ContratoServicosPrazo_Tipo, "F") == 0 ) && ! ( StringUtil.StrCmp(A904ContratoServicosPrazo_Tipo, "E") == 0 ) && ! ( StringUtil.StrCmp(A904ContratoServicosPrazo_Tipo, "P") == 0 ) )
         {
            A905ContratoServicosPrazo_Dias = 0;
            n905ContratoServicosPrazo_Dias = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A905ContratoServicosPrazo_Dias", StringUtil.LTrim( StringUtil.Str( (decimal)(A905ContratoServicosPrazo_Dias), 3, 0)));
         }
      }

      protected void CloseExtendedTableCursors2U112( )
      {
         pr_default.close(6);
         pr_default.close(8);
         pr_default.close(7);
         pr_default.close(9);
         pr_default.close(10);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_35( int A903ContratoServicosPrazo_CntSrvCod )
      {
         /* Using cursor T002U18 */
         pr_default.execute(12, new Object[] {A903ContratoServicosPrazo_CntSrvCod});
         if ( (pr_default.getStatus(12) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato Servicos Prazos_Contrato Servicos'.", "ForeignKeyNotFound", 1, "CONTRATOSERVICOSPRAZO_CNTSRVCOD");
            AnyError = 1;
            GX_FocusControl = edtContratoServicosPrazo_CntSrvCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A1212ContratoServicos_UnidadeContratada = T002U18_A1212ContratoServicos_UnidadeContratada[0];
         n1212ContratoServicos_UnidadeContratada = T002U18_n1212ContratoServicos_UnidadeContratada[0];
         A1095ContratoServicosPrazo_ServicoCod = T002U18_A1095ContratoServicosPrazo_ServicoCod[0];
         n1095ContratoServicosPrazo_ServicoCod = T002U18_n1095ContratoServicosPrazo_ServicoCod[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A1212ContratoServicos_UnidadeContratada), 6, 0, ".", "")))+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A1095ContratoServicosPrazo_ServicoCod), 6, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(12) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(12);
      }

      protected void gxLoad_37( int A1212ContratoServicos_UnidadeContratada )
      {
         /* Using cursor T002U19 */
         pr_default.execute(13, new Object[] {n1212ContratoServicos_UnidadeContratada, A1212ContratoServicos_UnidadeContratada});
         if ( (pr_default.getStatus(13) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato Servicos_Unidade Medicao'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A1712ContratoServicos_UndCntSgl = T002U19_A1712ContratoServicos_UndCntSgl[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1712ContratoServicos_UndCntSgl", A1712ContratoServicos_UndCntSgl);
         n1712ContratoServicos_UndCntSgl = T002U19_n1712ContratoServicos_UndCntSgl[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A1712ContratoServicos_UndCntSgl))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(13) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(13);
      }

      protected void gxLoad_36( int A1095ContratoServicosPrazo_ServicoCod )
      {
         /* Using cursor T002U20 */
         pr_default.execute(14, new Object[] {n1095ContratoServicosPrazo_ServicoCod, A1095ContratoServicosPrazo_ServicoCod});
         if ( (pr_default.getStatus(14) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato Servicos Prazos_Contrato Servicos'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A1096ContratoServicosPrazo_ServicoSigla = T002U20_A1096ContratoServicosPrazo_ServicoSigla[0];
         n1096ContratoServicosPrazo_ServicoSigla = T002U20_n1096ContratoServicosPrazo_ServicoSigla[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A1096ContratoServicosPrazo_ServicoSigla))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(14) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(14);
      }

      protected void gxLoad_38( int A903ContratoServicosPrazo_CntSrvCod )
      {
         /* Using cursor T002U22 */
         pr_default.execute(15, new Object[] {A903ContratoServicosPrazo_CntSrvCod});
         if ( (pr_default.getStatus(15) != 101) )
         {
            A910ContratoServicosPrazo_QtdRegras = T002U22_A910ContratoServicosPrazo_QtdRegras[0];
            n910ContratoServicosPrazo_QtdRegras = T002U22_n910ContratoServicosPrazo_QtdRegras[0];
         }
         else
         {
            A910ContratoServicosPrazo_QtdRegras = 0;
            n910ContratoServicosPrazo_QtdRegras = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A910ContratoServicosPrazo_QtdRegras", StringUtil.LTrim( StringUtil.Str( (decimal)(A910ContratoServicosPrazo_QtdRegras), 4, 0)));
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A910ContratoServicosPrazo_QtdRegras), 4, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(15) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(15);
      }

      protected void gxLoad_39( int A903ContratoServicosPrazo_CntSrvCod )
      {
         /* Using cursor T002U24 */
         pr_default.execute(16, new Object[] {A903ContratoServicosPrazo_CntSrvCod});
         if ( (pr_default.getStatus(16) != 101) )
         {
            A1737ContratoServicosPrazo_QtdPlnj = T002U24_A1737ContratoServicosPrazo_QtdPlnj[0];
            n1737ContratoServicosPrazo_QtdPlnj = T002U24_n1737ContratoServicosPrazo_QtdPlnj[0];
         }
         else
         {
            A1737ContratoServicosPrazo_QtdPlnj = 0;
            n1737ContratoServicosPrazo_QtdPlnj = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1737ContratoServicosPrazo_QtdPlnj", StringUtil.LTrim( StringUtil.Str( (decimal)(A1737ContratoServicosPrazo_QtdPlnj), 4, 0)));
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A1737ContratoServicosPrazo_QtdPlnj), 4, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(16) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(16);
      }

      protected void GetKey2U112( )
      {
         /* Using cursor T002U25 */
         pr_default.execute(17, new Object[] {A903ContratoServicosPrazo_CntSrvCod});
         if ( (pr_default.getStatus(17) != 101) )
         {
            RcdFound112 = 1;
         }
         else
         {
            RcdFound112 = 0;
         }
         pr_default.close(17);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T002U7 */
         pr_default.execute(5, new Object[] {A903ContratoServicosPrazo_CntSrvCod});
         if ( (pr_default.getStatus(5) != 101) )
         {
            ZM2U112( 34) ;
            RcdFound112 = 1;
            A905ContratoServicosPrazo_Dias = T002U7_A905ContratoServicosPrazo_Dias[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A905ContratoServicosPrazo_Dias", StringUtil.LTrim( StringUtil.Str( (decimal)(A905ContratoServicosPrazo_Dias), 3, 0)));
            n905ContratoServicosPrazo_Dias = T002U7_n905ContratoServicosPrazo_Dias[0];
            A904ContratoServicosPrazo_Tipo = T002U7_A904ContratoServicosPrazo_Tipo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A904ContratoServicosPrazo_Tipo", A904ContratoServicosPrazo_Tipo);
            A1456ContratoServicosPrazo_Cada = T002U7_A1456ContratoServicosPrazo_Cada[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1456ContratoServicosPrazo_Cada", StringUtil.LTrim( StringUtil.Str( A1456ContratoServicosPrazo_Cada, 14, 5)));
            n1456ContratoServicosPrazo_Cada = T002U7_n1456ContratoServicosPrazo_Cada[0];
            A1263ContratoServicosPrazo_DiasAlta = T002U7_A1263ContratoServicosPrazo_DiasAlta[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1263ContratoServicosPrazo_DiasAlta", StringUtil.LTrim( StringUtil.Str( (decimal)(A1263ContratoServicosPrazo_DiasAlta), 3, 0)));
            n1263ContratoServicosPrazo_DiasAlta = T002U7_n1263ContratoServicosPrazo_DiasAlta[0];
            A1264ContratoServicosPrazo_DiasMedia = T002U7_A1264ContratoServicosPrazo_DiasMedia[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1264ContratoServicosPrazo_DiasMedia", StringUtil.LTrim( StringUtil.Str( (decimal)(A1264ContratoServicosPrazo_DiasMedia), 3, 0)));
            n1264ContratoServicosPrazo_DiasMedia = T002U7_n1264ContratoServicosPrazo_DiasMedia[0];
            A1265ContratoServicosPrazo_DiasBaixa = T002U7_A1265ContratoServicosPrazo_DiasBaixa[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1265ContratoServicosPrazo_DiasBaixa", StringUtil.LTrim( StringUtil.Str( (decimal)(A1265ContratoServicosPrazo_DiasBaixa), 3, 0)));
            n1265ContratoServicosPrazo_DiasBaixa = T002U7_n1265ContratoServicosPrazo_DiasBaixa[0];
            A1823ContratoServicosPrazo_Momento = T002U7_A1823ContratoServicosPrazo_Momento[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1823ContratoServicosPrazo_Momento", StringUtil.LTrim( StringUtil.Str( (decimal)(A1823ContratoServicosPrazo_Momento), 4, 0)));
            n1823ContratoServicosPrazo_Momento = T002U7_n1823ContratoServicosPrazo_Momento[0];
            A903ContratoServicosPrazo_CntSrvCod = T002U7_A903ContratoServicosPrazo_CntSrvCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A903ContratoServicosPrazo_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A903ContratoServicosPrazo_CntSrvCod), 6, 0)));
            Z903ContratoServicosPrazo_CntSrvCod = A903ContratoServicosPrazo_CntSrvCod;
            sMode112 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load2U112( ) ;
            if ( AnyError == 1 )
            {
               RcdFound112 = 0;
               InitializeNonKey2U112( ) ;
            }
            Gx_mode = sMode112;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound112 = 0;
            InitializeNonKey2U112( ) ;
            sMode112 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal( ) ;
            Gx_mode = sMode112;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         pr_default.close(5);
      }

      protected void getEqualNoModal( )
      {
         GetKey2U112( ) ;
         if ( RcdFound112 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound112 = 0;
         /* Using cursor T002U26 */
         pr_default.execute(18, new Object[] {A903ContratoServicosPrazo_CntSrvCod});
         if ( (pr_default.getStatus(18) != 101) )
         {
            while ( (pr_default.getStatus(18) != 101) && ( ( T002U26_A903ContratoServicosPrazo_CntSrvCod[0] < A903ContratoServicosPrazo_CntSrvCod ) ) )
            {
               pr_default.readNext(18);
            }
            if ( (pr_default.getStatus(18) != 101) && ( ( T002U26_A903ContratoServicosPrazo_CntSrvCod[0] > A903ContratoServicosPrazo_CntSrvCod ) ) )
            {
               A903ContratoServicosPrazo_CntSrvCod = T002U26_A903ContratoServicosPrazo_CntSrvCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A903ContratoServicosPrazo_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A903ContratoServicosPrazo_CntSrvCod), 6, 0)));
               RcdFound112 = 1;
            }
         }
         pr_default.close(18);
      }

      protected void move_previous( )
      {
         RcdFound112 = 0;
         /* Using cursor T002U27 */
         pr_default.execute(19, new Object[] {A903ContratoServicosPrazo_CntSrvCod});
         if ( (pr_default.getStatus(19) != 101) )
         {
            while ( (pr_default.getStatus(19) != 101) && ( ( T002U27_A903ContratoServicosPrazo_CntSrvCod[0] > A903ContratoServicosPrazo_CntSrvCod ) ) )
            {
               pr_default.readNext(19);
            }
            if ( (pr_default.getStatus(19) != 101) && ( ( T002U27_A903ContratoServicosPrazo_CntSrvCod[0] < A903ContratoServicosPrazo_CntSrvCod ) ) )
            {
               A903ContratoServicosPrazo_CntSrvCod = T002U27_A903ContratoServicosPrazo_CntSrvCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A903ContratoServicosPrazo_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A903ContratoServicosPrazo_CntSrvCod), 6, 0)));
               RcdFound112 = 1;
            }
         }
         pr_default.close(19);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey2U112( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            A910ContratoServicosPrazo_QtdRegras = O910ContratoServicosPrazo_QtdRegras;
            n910ContratoServicosPrazo_QtdRegras = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A910ContratoServicosPrazo_QtdRegras", StringUtil.LTrim( StringUtil.Str( (decimal)(A910ContratoServicosPrazo_QtdRegras), 4, 0)));
            A1737ContratoServicosPrazo_QtdPlnj = O1737ContratoServicosPrazo_QtdPlnj;
            n1737ContratoServicosPrazo_QtdPlnj = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1737ContratoServicosPrazo_QtdPlnj", StringUtil.LTrim( StringUtil.Str( (decimal)(A1737ContratoServicosPrazo_QtdPlnj), 4, 0)));
            GX_FocusControl = cmbContratoServicosPrazo_Tipo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert2U112( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound112 == 1 )
            {
               if ( A903ContratoServicosPrazo_CntSrvCod != Z903ContratoServicosPrazo_CntSrvCod )
               {
                  A903ContratoServicosPrazo_CntSrvCod = Z903ContratoServicosPrazo_CntSrvCod;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A903ContratoServicosPrazo_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A903ContratoServicosPrazo_CntSrvCod), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "CONTRATOSERVICOSPRAZO_CNTSRVCOD");
                  AnyError = 1;
                  GX_FocusControl = edtContratoServicosPrazo_CntSrvCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  A910ContratoServicosPrazo_QtdRegras = O910ContratoServicosPrazo_QtdRegras;
                  n910ContratoServicosPrazo_QtdRegras = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A910ContratoServicosPrazo_QtdRegras", StringUtil.LTrim( StringUtil.Str( (decimal)(A910ContratoServicosPrazo_QtdRegras), 4, 0)));
                  A1737ContratoServicosPrazo_QtdPlnj = O1737ContratoServicosPrazo_QtdPlnj;
                  n1737ContratoServicosPrazo_QtdPlnj = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1737ContratoServicosPrazo_QtdPlnj", StringUtil.LTrim( StringUtil.Str( (decimal)(A1737ContratoServicosPrazo_QtdPlnj), 4, 0)));
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = cmbContratoServicosPrazo_Tipo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  A910ContratoServicosPrazo_QtdRegras = O910ContratoServicosPrazo_QtdRegras;
                  n910ContratoServicosPrazo_QtdRegras = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A910ContratoServicosPrazo_QtdRegras", StringUtil.LTrim( StringUtil.Str( (decimal)(A910ContratoServicosPrazo_QtdRegras), 4, 0)));
                  A1737ContratoServicosPrazo_QtdPlnj = O1737ContratoServicosPrazo_QtdPlnj;
                  n1737ContratoServicosPrazo_QtdPlnj = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1737ContratoServicosPrazo_QtdPlnj", StringUtil.LTrim( StringUtil.Str( (decimal)(A1737ContratoServicosPrazo_QtdPlnj), 4, 0)));
                  Update2U112( ) ;
                  GX_FocusControl = cmbContratoServicosPrazo_Tipo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A903ContratoServicosPrazo_CntSrvCod != Z903ContratoServicosPrazo_CntSrvCod )
               {
                  /* Insert record */
                  A910ContratoServicosPrazo_QtdRegras = O910ContratoServicosPrazo_QtdRegras;
                  n910ContratoServicosPrazo_QtdRegras = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A910ContratoServicosPrazo_QtdRegras", StringUtil.LTrim( StringUtil.Str( (decimal)(A910ContratoServicosPrazo_QtdRegras), 4, 0)));
                  A1737ContratoServicosPrazo_QtdPlnj = O1737ContratoServicosPrazo_QtdPlnj;
                  n1737ContratoServicosPrazo_QtdPlnj = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1737ContratoServicosPrazo_QtdPlnj", StringUtil.LTrim( StringUtil.Str( (decimal)(A1737ContratoServicosPrazo_QtdPlnj), 4, 0)));
                  GX_FocusControl = cmbContratoServicosPrazo_Tipo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert2U112( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "CONTRATOSERVICOSPRAZO_CNTSRVCOD");
                     AnyError = 1;
                     GX_FocusControl = edtContratoServicosPrazo_CntSrvCod_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     /* Insert record */
                     A910ContratoServicosPrazo_QtdRegras = O910ContratoServicosPrazo_QtdRegras;
                     n910ContratoServicosPrazo_QtdRegras = false;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A910ContratoServicosPrazo_QtdRegras", StringUtil.LTrim( StringUtil.Str( (decimal)(A910ContratoServicosPrazo_QtdRegras), 4, 0)));
                     A1737ContratoServicosPrazo_QtdPlnj = O1737ContratoServicosPrazo_QtdPlnj;
                     n1737ContratoServicosPrazo_QtdPlnj = false;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1737ContratoServicosPrazo_QtdPlnj", StringUtil.LTrim( StringUtil.Str( (decimal)(A1737ContratoServicosPrazo_QtdPlnj), 4, 0)));
                     GX_FocusControl = cmbContratoServicosPrazo_Tipo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert2U112( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( A903ContratoServicosPrazo_CntSrvCod != Z903ContratoServicosPrazo_CntSrvCod )
         {
            A903ContratoServicosPrazo_CntSrvCod = Z903ContratoServicosPrazo_CntSrvCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A903ContratoServicosPrazo_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A903ContratoServicosPrazo_CntSrvCod), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "CONTRATOSERVICOSPRAZO_CNTSRVCOD");
            AnyError = 1;
            GX_FocusControl = edtContratoServicosPrazo_CntSrvCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            A910ContratoServicosPrazo_QtdRegras = O910ContratoServicosPrazo_QtdRegras;
            n910ContratoServicosPrazo_QtdRegras = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A910ContratoServicosPrazo_QtdRegras", StringUtil.LTrim( StringUtil.Str( (decimal)(A910ContratoServicosPrazo_QtdRegras), 4, 0)));
            A1737ContratoServicosPrazo_QtdPlnj = O1737ContratoServicosPrazo_QtdPlnj;
            n1737ContratoServicosPrazo_QtdPlnj = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1737ContratoServicosPrazo_QtdPlnj", StringUtil.LTrim( StringUtil.Str( (decimal)(A1737ContratoServicosPrazo_QtdPlnj), 4, 0)));
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = cmbContratoServicosPrazo_Tipo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency2U112( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T002U6 */
            pr_default.execute(4, new Object[] {A903ContratoServicosPrazo_CntSrvCod});
            if ( (pr_default.getStatus(4) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContratoServicosPrazo"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            Gx_longc = false;
            if ( (pr_default.getStatus(4) == 101) || ( Z905ContratoServicosPrazo_Dias != T002U6_A905ContratoServicosPrazo_Dias[0] ) || ( StringUtil.StrCmp(Z904ContratoServicosPrazo_Tipo, T002U6_A904ContratoServicosPrazo_Tipo[0]) != 0 ) || ( Z1456ContratoServicosPrazo_Cada != T002U6_A1456ContratoServicosPrazo_Cada[0] ) || ( Z1263ContratoServicosPrazo_DiasAlta != T002U6_A1263ContratoServicosPrazo_DiasAlta[0] ) || ( Z1264ContratoServicosPrazo_DiasMedia != T002U6_A1264ContratoServicosPrazo_DiasMedia[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z1265ContratoServicosPrazo_DiasBaixa != T002U6_A1265ContratoServicosPrazo_DiasBaixa[0] ) || ( Z1823ContratoServicosPrazo_Momento != T002U6_A1823ContratoServicosPrazo_Momento[0] ) )
            {
               if ( Z905ContratoServicosPrazo_Dias != T002U6_A905ContratoServicosPrazo_Dias[0] )
               {
                  GXUtil.WriteLog("contratoservicosprazo:[seudo value changed for attri]"+"ContratoServicosPrazo_Dias");
                  GXUtil.WriteLogRaw("Old: ",Z905ContratoServicosPrazo_Dias);
                  GXUtil.WriteLogRaw("Current: ",T002U6_A905ContratoServicosPrazo_Dias[0]);
               }
               if ( StringUtil.StrCmp(Z904ContratoServicosPrazo_Tipo, T002U6_A904ContratoServicosPrazo_Tipo[0]) != 0 )
               {
                  GXUtil.WriteLog("contratoservicosprazo:[seudo value changed for attri]"+"ContratoServicosPrazo_Tipo");
                  GXUtil.WriteLogRaw("Old: ",Z904ContratoServicosPrazo_Tipo);
                  GXUtil.WriteLogRaw("Current: ",T002U6_A904ContratoServicosPrazo_Tipo[0]);
               }
               if ( Z1456ContratoServicosPrazo_Cada != T002U6_A1456ContratoServicosPrazo_Cada[0] )
               {
                  GXUtil.WriteLog("contratoservicosprazo:[seudo value changed for attri]"+"ContratoServicosPrazo_Cada");
                  GXUtil.WriteLogRaw("Old: ",Z1456ContratoServicosPrazo_Cada);
                  GXUtil.WriteLogRaw("Current: ",T002U6_A1456ContratoServicosPrazo_Cada[0]);
               }
               if ( Z1263ContratoServicosPrazo_DiasAlta != T002U6_A1263ContratoServicosPrazo_DiasAlta[0] )
               {
                  GXUtil.WriteLog("contratoservicosprazo:[seudo value changed for attri]"+"ContratoServicosPrazo_DiasAlta");
                  GXUtil.WriteLogRaw("Old: ",Z1263ContratoServicosPrazo_DiasAlta);
                  GXUtil.WriteLogRaw("Current: ",T002U6_A1263ContratoServicosPrazo_DiasAlta[0]);
               }
               if ( Z1264ContratoServicosPrazo_DiasMedia != T002U6_A1264ContratoServicosPrazo_DiasMedia[0] )
               {
                  GXUtil.WriteLog("contratoservicosprazo:[seudo value changed for attri]"+"ContratoServicosPrazo_DiasMedia");
                  GXUtil.WriteLogRaw("Old: ",Z1264ContratoServicosPrazo_DiasMedia);
                  GXUtil.WriteLogRaw("Current: ",T002U6_A1264ContratoServicosPrazo_DiasMedia[0]);
               }
               if ( Z1265ContratoServicosPrazo_DiasBaixa != T002U6_A1265ContratoServicosPrazo_DiasBaixa[0] )
               {
                  GXUtil.WriteLog("contratoservicosprazo:[seudo value changed for attri]"+"ContratoServicosPrazo_DiasBaixa");
                  GXUtil.WriteLogRaw("Old: ",Z1265ContratoServicosPrazo_DiasBaixa);
                  GXUtil.WriteLogRaw("Current: ",T002U6_A1265ContratoServicosPrazo_DiasBaixa[0]);
               }
               if ( Z1823ContratoServicosPrazo_Momento != T002U6_A1823ContratoServicosPrazo_Momento[0] )
               {
                  GXUtil.WriteLog("contratoservicosprazo:[seudo value changed for attri]"+"ContratoServicosPrazo_Momento");
                  GXUtil.WriteLogRaw("Old: ",Z1823ContratoServicosPrazo_Momento);
                  GXUtil.WriteLogRaw("Current: ",T002U6_A1823ContratoServicosPrazo_Momento[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"ContratoServicosPrazo"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert2U112( )
      {
         BeforeValidate2U112( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable2U112( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM2U112( 0) ;
            CheckOptimisticConcurrency2U112( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm2U112( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert2U112( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T002U28 */
                     pr_default.execute(20, new Object[] {n905ContratoServicosPrazo_Dias, A905ContratoServicosPrazo_Dias, A904ContratoServicosPrazo_Tipo, n1456ContratoServicosPrazo_Cada, A1456ContratoServicosPrazo_Cada, n1263ContratoServicosPrazo_DiasAlta, A1263ContratoServicosPrazo_DiasAlta, n1264ContratoServicosPrazo_DiasMedia, A1264ContratoServicosPrazo_DiasMedia, n1265ContratoServicosPrazo_DiasBaixa, A1265ContratoServicosPrazo_DiasBaixa, n1823ContratoServicosPrazo_Momento, A1823ContratoServicosPrazo_Momento, A903ContratoServicosPrazo_CntSrvCod});
                     pr_default.close(20);
                     dsDefault.SmartCacheProvider.SetUpdated("ContratoServicosPrazo") ;
                     if ( (pr_default.getStatus(20) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( level) rules */
                        bttBtn_trn_enter_Enabled = (((StringUtil.StrCmp(A904ContratoServicosPrazo_Tipo, "S")==0))||((StringUtil.StrCmp(A904ContratoServicosPrazo_Tipo, "O")==0))||((StringUtil.StrCmp(A904ContratoServicosPrazo_Tipo, "V")==0)&&(A910ContratoServicosPrazo_QtdRegras>0))||((StringUtil.StrCmp(A904ContratoServicosPrazo_Tipo, "F")==0)&&(A905ContratoServicosPrazo_Dias>0)) ? 1 : 0);
                        context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
                        /* End of After( level) rules */
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           ProcessLevel2U112( ) ;
                           if ( AnyError == 0 )
                           {
                              /* Save values for previous() function. */
                              GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                              ResetCaption2U0( ) ;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load2U112( ) ;
            }
            EndLevel2U112( ) ;
         }
         CloseExtendedTableCursors2U112( ) ;
      }

      protected void Update2U112( )
      {
         BeforeValidate2U112( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable2U112( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency2U112( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm2U112( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate2U112( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T002U29 */
                     pr_default.execute(21, new Object[] {n905ContratoServicosPrazo_Dias, A905ContratoServicosPrazo_Dias, A904ContratoServicosPrazo_Tipo, n1456ContratoServicosPrazo_Cada, A1456ContratoServicosPrazo_Cada, n1263ContratoServicosPrazo_DiasAlta, A1263ContratoServicosPrazo_DiasAlta, n1264ContratoServicosPrazo_DiasMedia, A1264ContratoServicosPrazo_DiasMedia, n1265ContratoServicosPrazo_DiasBaixa, A1265ContratoServicosPrazo_DiasBaixa, n1823ContratoServicosPrazo_Momento, A1823ContratoServicosPrazo_Momento, A903ContratoServicosPrazo_CntSrvCod});
                     pr_default.close(21);
                     dsDefault.SmartCacheProvider.SetUpdated("ContratoServicosPrazo") ;
                     if ( (pr_default.getStatus(21) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContratoServicosPrazo"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate2U112( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( level) rules */
                        bttBtn_trn_enter_Enabled = (((StringUtil.StrCmp(A904ContratoServicosPrazo_Tipo, "S")==0))||((StringUtil.StrCmp(A904ContratoServicosPrazo_Tipo, "O")==0))||((StringUtil.StrCmp(A904ContratoServicosPrazo_Tipo, "V")==0)&&(A910ContratoServicosPrazo_QtdRegras>0))||((StringUtil.StrCmp(A904ContratoServicosPrazo_Tipo, "F")==0)&&(A905ContratoServicosPrazo_Dias>0)) ? 1 : 0);
                        context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
                        /* End of After( level) rules */
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           ProcessLevel2U112( ) ;
                           if ( AnyError == 0 )
                           {
                              if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                              {
                                 if ( AnyError == 0 )
                                 {
                                    context.nUserReturn = 1;
                                 }
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel2U112( ) ;
         }
         CloseExtendedTableCursors2U112( ) ;
      }

      protected void DeferredUpdate2U112( )
      {
      }

      protected void delete( )
      {
         BeforeValidate2U112( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency2U112( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls2U112( ) ;
            AfterConfirm2U112( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete2U112( ) ;
               if ( AnyError == 0 )
               {
                  A1737ContratoServicosPrazo_QtdPlnj = O1737ContratoServicosPrazo_QtdPlnj;
                  n1737ContratoServicosPrazo_QtdPlnj = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1737ContratoServicosPrazo_QtdPlnj", StringUtil.LTrim( StringUtil.Str( (decimal)(A1737ContratoServicosPrazo_QtdPlnj), 4, 0)));
                  ScanStart2U188( ) ;
                  while ( RcdFound188 != 0 )
                  {
                     getByPrimaryKey2U188( ) ;
                     Delete2U188( ) ;
                     ScanNext2U188( ) ;
                     O1737ContratoServicosPrazo_QtdPlnj = A1737ContratoServicosPrazo_QtdPlnj;
                     n1737ContratoServicosPrazo_QtdPlnj = false;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1737ContratoServicosPrazo_QtdPlnj", StringUtil.LTrim( StringUtil.Str( (decimal)(A1737ContratoServicosPrazo_QtdPlnj), 4, 0)));
                  }
                  ScanEnd2U188( ) ;
                  A910ContratoServicosPrazo_QtdRegras = O910ContratoServicosPrazo_QtdRegras;
                  n910ContratoServicosPrazo_QtdRegras = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A910ContratoServicosPrazo_QtdRegras", StringUtil.LTrim( StringUtil.Str( (decimal)(A910ContratoServicosPrazo_QtdRegras), 4, 0)));
                  ScanStart2U113( ) ;
                  while ( RcdFound113 != 0 )
                  {
                     getByPrimaryKey2U113( ) ;
                     Delete2U113( ) ;
                     ScanNext2U113( ) ;
                     O910ContratoServicosPrazo_QtdRegras = A910ContratoServicosPrazo_QtdRegras;
                     n910ContratoServicosPrazo_QtdRegras = false;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A910ContratoServicosPrazo_QtdRegras", StringUtil.LTrim( StringUtil.Str( (decimal)(A910ContratoServicosPrazo_QtdRegras), 4, 0)));
                  }
                  ScanEnd2U113( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T002U30 */
                     pr_default.execute(22, new Object[] {A903ContratoServicosPrazo_CntSrvCod});
                     pr_default.close(22);
                     dsDefault.SmartCacheProvider.SetUpdated("ContratoServicosPrazo") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( level) rules */
                        bttBtn_trn_enter_Enabled = (((StringUtil.StrCmp(A904ContratoServicosPrazo_Tipo, "S")==0))||((StringUtil.StrCmp(A904ContratoServicosPrazo_Tipo, "O")==0))||((StringUtil.StrCmp(A904ContratoServicosPrazo_Tipo, "V")==0)&&(A910ContratoServicosPrazo_QtdRegras>0))||((StringUtil.StrCmp(A904ContratoServicosPrazo_Tipo, "F")==0)&&(A905ContratoServicosPrazo_Dias>0)) ? 1 : 0);
                        context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
                        /* End of After( level) rules */
                        /* Start of After( delete) rules */
                        /* End of After( delete) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
         }
         sMode112 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel2U112( ) ;
         Gx_mode = sMode112;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls2U112( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor T002U31 */
            pr_default.execute(23, new Object[] {A903ContratoServicosPrazo_CntSrvCod});
            A1212ContratoServicos_UnidadeContratada = T002U31_A1212ContratoServicos_UnidadeContratada[0];
            n1212ContratoServicos_UnidadeContratada = T002U31_n1212ContratoServicos_UnidadeContratada[0];
            A1095ContratoServicosPrazo_ServicoCod = T002U31_A1095ContratoServicosPrazo_ServicoCod[0];
            n1095ContratoServicosPrazo_ServicoCod = T002U31_n1095ContratoServicosPrazo_ServicoCod[0];
            pr_default.close(23);
            /* Using cursor T002U32 */
            pr_default.execute(24, new Object[] {n1212ContratoServicos_UnidadeContratada, A1212ContratoServicos_UnidadeContratada});
            A1712ContratoServicos_UndCntSgl = T002U32_A1712ContratoServicos_UndCntSgl[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1712ContratoServicos_UndCntSgl", A1712ContratoServicos_UndCntSgl);
            n1712ContratoServicos_UndCntSgl = T002U32_n1712ContratoServicos_UndCntSgl[0];
            pr_default.close(24);
            /* Using cursor T002U33 */
            pr_default.execute(25, new Object[] {n1095ContratoServicosPrazo_ServicoCod, A1095ContratoServicosPrazo_ServicoCod});
            A1096ContratoServicosPrazo_ServicoSigla = T002U33_A1096ContratoServicosPrazo_ServicoSigla[0];
            n1096ContratoServicosPrazo_ServicoSigla = T002U33_n1096ContratoServicosPrazo_ServicoSigla[0];
            pr_default.close(25);
            AV11Servico_Sigla = A1096ContratoServicosPrazo_ServicoSigla;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Servico_Sigla", AV11Servico_Sigla);
            /* Using cursor T002U35 */
            pr_default.execute(26, new Object[] {A903ContratoServicosPrazo_CntSrvCod});
            if ( (pr_default.getStatus(26) != 101) )
            {
               A910ContratoServicosPrazo_QtdRegras = T002U35_A910ContratoServicosPrazo_QtdRegras[0];
               n910ContratoServicosPrazo_QtdRegras = T002U35_n910ContratoServicosPrazo_QtdRegras[0];
            }
            else
            {
               A910ContratoServicosPrazo_QtdRegras = 0;
               n910ContratoServicosPrazo_QtdRegras = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A910ContratoServicosPrazo_QtdRegras", StringUtil.LTrim( StringUtil.Str( (decimal)(A910ContratoServicosPrazo_QtdRegras), 4, 0)));
            }
            pr_default.close(26);
            /* Using cursor T002U37 */
            pr_default.execute(27, new Object[] {A903ContratoServicosPrazo_CntSrvCod});
            if ( (pr_default.getStatus(27) != 101) )
            {
               A1737ContratoServicosPrazo_QtdPlnj = T002U37_A1737ContratoServicosPrazo_QtdPlnj[0];
               n1737ContratoServicosPrazo_QtdPlnj = T002U37_n1737ContratoServicosPrazo_QtdPlnj[0];
            }
            else
            {
               A1737ContratoServicosPrazo_QtdPlnj = 0;
               n1737ContratoServicosPrazo_QtdPlnj = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1737ContratoServicosPrazo_QtdPlnj", StringUtil.LTrim( StringUtil.Str( (decimal)(A1737ContratoServicosPrazo_QtdPlnj), 4, 0)));
            }
            pr_default.close(27);
            lblTextblockcontratoservicosprazo_dias_Visible = (((StringUtil.StrCmp(A904ContratoServicosPrazo_Tipo, "F")==0)||(StringUtil.StrCmp(A904ContratoServicosPrazo_Tipo, "E")==0)||(StringUtil.StrCmp(A904ContratoServicosPrazo_Tipo, "P")==0)) ? 1 : 0);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockcontratoservicosprazo_dias_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockcontratoservicosprazo_dias_Visible), 5, 0)));
            edtContratoServicosPrazo_Dias_Visible = (((StringUtil.StrCmp(A904ContratoServicosPrazo_Tipo, "F")==0)||(StringUtil.StrCmp(A904ContratoServicosPrazo_Tipo, "E")==0)||(StringUtil.StrCmp(A904ContratoServicosPrazo_Tipo, "P")==0)) ? 1 : 0);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosPrazo_Dias_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosPrazo_Dias_Visible), 5, 0)));
            lblTextblockcontratoservicosprazo_cada_Visible = (((StringUtil.StrCmp(A904ContratoServicosPrazo_Tipo, "E")==0)||(StringUtil.StrCmp(A904ContratoServicosPrazo_Tipo, "P")==0)) ? 1 : 0);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockcontratoservicosprazo_cada_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockcontratoservicosprazo_cada_Visible), 5, 0)));
            edtContratoServicosPrazo_Cada_Visible = (((StringUtil.StrCmp(A904ContratoServicosPrazo_Tipo, "E")==0)||(StringUtil.StrCmp(A904ContratoServicosPrazo_Tipo, "P")==0)) ? 1 : 0);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosPrazo_Cada_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosPrazo_Cada_Visible), 5, 0)));
            edtContratoServicos_UndCntSgl_Visible = (((StringUtil.StrCmp(A904ContratoServicosPrazo_Tipo, "E")==0)||(StringUtil.StrCmp(A904ContratoServicosPrazo_Tipo, "P")==0)) ? 1 : 0);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicos_UndCntSgl_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicos_UndCntSgl_Visible), 5, 0)));
            tblTblregras_Visible = (((StringUtil.StrCmp(A904ContratoServicosPrazo_Tipo, "V")==0)) ? 1 : 0);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTblregras_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTblregras_Visible), 5, 0)));
            tblTblcomplexidade_Visible = (((StringUtil.StrCmp(A904ContratoServicosPrazo_Tipo, "C")==0)) ? 1 : 0);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTblcomplexidade_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTblcomplexidade_Visible), 5, 0)));
         }
      }

      protected void ProcessNestedLevel2U113( )
      {
         s910ContratoServicosPrazo_QtdRegras = O910ContratoServicosPrazo_QtdRegras;
         n910ContratoServicosPrazo_QtdRegras = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A910ContratoServicosPrazo_QtdRegras", StringUtil.LTrim( StringUtil.Str( (decimal)(A910ContratoServicosPrazo_QtdRegras), 4, 0)));
         nGXsfl_97_idx = 0;
         while ( nGXsfl_97_idx < nRC_GXsfl_97 )
         {
            ReadRow2U113( ) ;
            if ( ( nRcdExists_113 != 0 ) || ( nIsMod_113 != 0 ) )
            {
               standaloneNotModal2U113( ) ;
               GetKey2U113( ) ;
               if ( ( nRcdExists_113 == 0 ) && ( nRcdDeleted_113 == 0 ) )
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  Insert2U113( ) ;
               }
               else
               {
                  if ( RcdFound113 != 0 )
                  {
                     if ( ( nRcdDeleted_113 != 0 ) && ( nRcdExists_113 != 0 ) )
                     {
                        Gx_mode = "DLT";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                        Delete2U113( ) ;
                     }
                     else
                     {
                        if ( ( nIsMod_113 != 0 ) && ( nRcdExists_113 != 0 ) )
                        {
                           Gx_mode = "UPD";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                           Update2U113( ) ;
                        }
                     }
                  }
                  else
                  {
                     if ( nRcdDeleted_113 == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "CONTRATOSERVICOSPRAZO_CNTSRVCOD");
                        AnyError = 1;
                        GX_FocusControl = edtContratoServicosPrazo_CntSrvCod_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
               O910ContratoServicosPrazo_QtdRegras = A910ContratoServicosPrazo_QtdRegras;
               n910ContratoServicosPrazo_QtdRegras = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A910ContratoServicosPrazo_QtdRegras", StringUtil.LTrim( StringUtil.Str( (decimal)(A910ContratoServicosPrazo_QtdRegras), 4, 0)));
            }
            ChangePostValue( edtContratoServicosPrazoRegra_Sequencial_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A906ContratoServicosPrazoRegra_Sequencial), 4, 0, ",", ""))) ;
            ChangePostValue( edtContratoServicosPrazoRegra_Inicio_Internalname, StringUtil.LTrim( StringUtil.NToC( A907ContratoServicosPrazoRegra_Inicio, 14, 5, ",", ""))) ;
            ChangePostValue( edtContratoServicosPrazoRegra_Fim_Internalname, StringUtil.LTrim( StringUtil.NToC( A908ContratoServicosPrazoRegra_Fim, 14, 5, ",", ""))) ;
            ChangePostValue( edtContratoServicosPrazoRegra_Dias_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A909ContratoServicosPrazoRegra_Dias), 3, 0, ",", ""))) ;
            ChangePostValue( "ZT_"+"Z906ContratoServicosPrazoRegra_Sequencial_"+sGXsfl_97_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(Z906ContratoServicosPrazoRegra_Sequencial), 4, 0, ",", ""))) ;
            ChangePostValue( "ZT_"+"Z907ContratoServicosPrazoRegra_Inicio_"+sGXsfl_97_idx, StringUtil.LTrim( StringUtil.NToC( Z907ContratoServicosPrazoRegra_Inicio, 14, 5, ",", ""))) ;
            ChangePostValue( "ZT_"+"Z908ContratoServicosPrazoRegra_Fim_"+sGXsfl_97_idx, StringUtil.LTrim( StringUtil.NToC( Z908ContratoServicosPrazoRegra_Fim, 14, 5, ",", ""))) ;
            ChangePostValue( "ZT_"+"Z909ContratoServicosPrazoRegra_Dias_"+sGXsfl_97_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(Z909ContratoServicosPrazoRegra_Dias), 3, 0, ",", ""))) ;
            ChangePostValue( "nRcdDeleted_113_"+sGXsfl_97_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdDeleted_113), 4, 0, ",", ""))) ;
            ChangePostValue( "nRcdExists_113_"+sGXsfl_97_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdExists_113), 4, 0, ",", ""))) ;
            ChangePostValue( "nIsMod_113_"+sGXsfl_97_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nIsMod_113), 4, 0, ",", ""))) ;
            if ( nIsMod_113 != 0 )
            {
               ChangePostValue( "CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_"+sGXsfl_97_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosPrazoRegra_Sequencial_Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "CONTRATOSERVICOSPRAZOREGRA_INICIO_"+sGXsfl_97_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosPrazoRegra_Inicio_Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "CONTRATOSERVICOSPRAZOREGRA_FIM_"+sGXsfl_97_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosPrazoRegra_Fim_Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "CONTRATOSERVICOSPRAZOREGRA_DIAS_"+sGXsfl_97_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosPrazoRegra_Dias_Enabled), 5, 0, ".", ""))) ;
            }
         }
         /* Start of After( level) rules */
         /* End of After( level) rules */
         InitAll2U113( ) ;
         if ( AnyError != 0 )
         {
            O910ContratoServicosPrazo_QtdRegras = s910ContratoServicosPrazo_QtdRegras;
            n910ContratoServicosPrazo_QtdRegras = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A910ContratoServicosPrazo_QtdRegras", StringUtil.LTrim( StringUtil.Str( (decimal)(A910ContratoServicosPrazo_QtdRegras), 4, 0)));
         }
         nRcdExists_113 = 0;
         nIsMod_113 = 0;
         nRcdDeleted_113 = 0;
      }

      protected void ProcessNestedLevel2U188( )
      {
         s1737ContratoServicosPrazo_QtdPlnj = O1737ContratoServicosPrazo_QtdPlnj;
         n1737ContratoServicosPrazo_QtdPlnj = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1737ContratoServicosPrazo_QtdPlnj", StringUtil.LTrim( StringUtil.Str( (decimal)(A1737ContratoServicosPrazo_QtdPlnj), 4, 0)));
         nGXsfl_112_idx = 0;
         while ( nGXsfl_112_idx < nRC_GXsfl_112 )
         {
            ReadRow2U188( ) ;
            if ( ( nRcdExists_188 != 0 ) || ( nIsMod_188 != 0 ) )
            {
               standaloneNotModal2U188( ) ;
               GetKey2U188( ) ;
               if ( ( nRcdExists_188 == 0 ) && ( nRcdDeleted_188 == 0 ) )
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  Insert2U188( ) ;
               }
               else
               {
                  if ( RcdFound188 != 0 )
                  {
                     if ( ( nRcdDeleted_188 != 0 ) && ( nRcdExists_188 != 0 ) )
                     {
                        Gx_mode = "DLT";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                        Delete2U188( ) ;
                     }
                     else
                     {
                        if ( ( nIsMod_188 != 0 ) && ( nRcdExists_188 != 0 ) )
                        {
                           Gx_mode = "UPD";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                           Update2U188( ) ;
                        }
                     }
                  }
                  else
                  {
                     if ( nRcdDeleted_188 == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "CONTRATOSERVICOSPRAZO_CNTSRVCOD");
                        AnyError = 1;
                        GX_FocusControl = edtContratoServicosPrazo_CntSrvCod_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
               O1737ContratoServicosPrazo_QtdPlnj = A1737ContratoServicosPrazo_QtdPlnj;
               n1737ContratoServicosPrazo_QtdPlnj = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1737ContratoServicosPrazo_QtdPlnj", StringUtil.LTrim( StringUtil.Str( (decimal)(A1737ContratoServicosPrazo_QtdPlnj), 4, 0)));
            }
            ChangePostValue( edtContratoServicosPrazoPlnj_Sequencial_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1724ContratoServicosPrazoPlnj_Sequencial), 3, 0, ",", ""))) ;
            ChangePostValue( edtContratoServicosPrazoPlnj_Inicio_Internalname, StringUtil.LTrim( StringUtil.NToC( A1734ContratoServicosPrazoPlnj_Inicio, 14, 5, ",", ""))) ;
            ChangePostValue( edtContratoServicosPrazoPlnj_Fim_Internalname, StringUtil.LTrim( StringUtil.NToC( A1735ContratoServicosPrazoPlnj_Fim, 14, 5, ",", ""))) ;
            ChangePostValue( edtContratoServicosPrazoPlnj_Dias_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1736ContratoServicosPrazoPlnj_Dias), 4, 0, ",", ""))) ;
            ChangePostValue( "ZT_"+"Z1724ContratoServicosPrazoPlnj_Sequencial_"+sGXsfl_112_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1724ContratoServicosPrazoPlnj_Sequencial), 3, 0, ",", ""))) ;
            ChangePostValue( "ZT_"+"Z1734ContratoServicosPrazoPlnj_Inicio_"+sGXsfl_112_idx, StringUtil.LTrim( StringUtil.NToC( Z1734ContratoServicosPrazoPlnj_Inicio, 14, 5, ",", ""))) ;
            ChangePostValue( "ZT_"+"Z1735ContratoServicosPrazoPlnj_Fim_"+sGXsfl_112_idx, StringUtil.LTrim( StringUtil.NToC( Z1735ContratoServicosPrazoPlnj_Fim, 14, 5, ",", ""))) ;
            ChangePostValue( "ZT_"+"Z1736ContratoServicosPrazoPlnj_Dias_"+sGXsfl_112_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1736ContratoServicosPrazoPlnj_Dias), 4, 0, ",", ""))) ;
            ChangePostValue( "nRcdDeleted_188_"+sGXsfl_112_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdDeleted_188), 4, 0, ",", ""))) ;
            ChangePostValue( "nRcdExists_188_"+sGXsfl_112_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdExists_188), 4, 0, ",", ""))) ;
            ChangePostValue( "nIsMod_188_"+sGXsfl_112_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nIsMod_188), 4, 0, ",", ""))) ;
            if ( nIsMod_188 != 0 )
            {
               ChangePostValue( "CONTRATOSERVICOSPRAZOPLNJ_SEQUENCIAL_"+sGXsfl_112_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosPrazoPlnj_Sequencial_Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "CONTRATOSERVICOSPRAZOPLNJ_INICIO_"+sGXsfl_112_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosPrazoPlnj_Inicio_Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "CONTRATOSERVICOSPRAZOPLNJ_FIM_"+sGXsfl_112_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosPrazoPlnj_Fim_Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "CONTRATOSERVICOSPRAZOPLNJ_DIAS_"+sGXsfl_112_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosPrazoPlnj_Dias_Enabled), 5, 0, ".", ""))) ;
            }
         }
         /* Start of After( level) rules */
         /* End of After( level) rules */
         InitAll2U188( ) ;
         if ( AnyError != 0 )
         {
            O1737ContratoServicosPrazo_QtdPlnj = s1737ContratoServicosPrazo_QtdPlnj;
            n1737ContratoServicosPrazo_QtdPlnj = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1737ContratoServicosPrazo_QtdPlnj", StringUtil.LTrim( StringUtil.Str( (decimal)(A1737ContratoServicosPrazo_QtdPlnj), 4, 0)));
         }
         nRcdExists_188 = 0;
         nIsMod_188 = 0;
         nRcdDeleted_188 = 0;
      }

      protected void ProcessLevel2U112( )
      {
         /* Save parent mode. */
         sMode112 = Gx_mode;
         ProcessNestedLevel2U113( ) ;
         ProcessNestedLevel2U188( ) ;
         if ( AnyError != 0 )
         {
            O910ContratoServicosPrazo_QtdRegras = s910ContratoServicosPrazo_QtdRegras;
            n910ContratoServicosPrazo_QtdRegras = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A910ContratoServicosPrazo_QtdRegras", StringUtil.LTrim( StringUtil.Str( (decimal)(A910ContratoServicosPrazo_QtdRegras), 4, 0)));
            O1737ContratoServicosPrazo_QtdPlnj = s1737ContratoServicosPrazo_QtdPlnj;
            n1737ContratoServicosPrazo_QtdPlnj = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1737ContratoServicosPrazo_QtdPlnj", StringUtil.LTrim( StringUtil.Str( (decimal)(A1737ContratoServicosPrazo_QtdPlnj), 4, 0)));
         }
         /* Restore parent mode. */
         Gx_mode = sMode112;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         /* ' Update level parameters */
      }

      protected void EndLevel2U112( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(4);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete2U112( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(5);
            pr_default.close(3);
            pr_default.close(2);
            pr_default.close(1);
            pr_default.close(0);
            pr_default.close(23);
            pr_default.close(25);
            pr_default.close(24);
            pr_default.close(26);
            pr_default.close(27);
            context.CommitDataStores( "ContratoServicosPrazo");
            if ( AnyError == 0 )
            {
               ConfirmValues2U0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(5);
            pr_default.close(3);
            pr_default.close(2);
            pr_default.close(1);
            pr_default.close(0);
            pr_default.close(23);
            pr_default.close(25);
            pr_default.close(24);
            pr_default.close(26);
            pr_default.close(27);
            context.RollbackDataStores( "ContratoServicosPrazo");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart2U112( )
      {
         /* Scan By routine */
         /* Using cursor T002U38 */
         pr_default.execute(28);
         RcdFound112 = 0;
         if ( (pr_default.getStatus(28) != 101) )
         {
            RcdFound112 = 1;
            A903ContratoServicosPrazo_CntSrvCod = T002U38_A903ContratoServicosPrazo_CntSrvCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A903ContratoServicosPrazo_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A903ContratoServicosPrazo_CntSrvCod), 6, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext2U112( )
      {
         /* Scan next routine */
         pr_default.readNext(28);
         RcdFound112 = 0;
         if ( (pr_default.getStatus(28) != 101) )
         {
            RcdFound112 = 1;
            A903ContratoServicosPrazo_CntSrvCod = T002U38_A903ContratoServicosPrazo_CntSrvCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A903ContratoServicosPrazo_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A903ContratoServicosPrazo_CntSrvCod), 6, 0)));
         }
      }

      protected void ScanEnd2U112( )
      {
         pr_default.close(28);
      }

      protected void AfterConfirm2U112( )
      {
         /* After Confirm Rules */
         if ( StringUtil.StrCmp(A904ContratoServicosPrazo_Tipo, "C") != 0 )
         {
            A1265ContratoServicosPrazo_DiasBaixa = 0;
            n1265ContratoServicosPrazo_DiasBaixa = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1265ContratoServicosPrazo_DiasBaixa", StringUtil.LTrim( StringUtil.Str( (decimal)(A1265ContratoServicosPrazo_DiasBaixa), 3, 0)));
            n1265ContratoServicosPrazo_DiasBaixa = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1265ContratoServicosPrazo_DiasBaixa", StringUtil.LTrim( StringUtil.Str( (decimal)(A1265ContratoServicosPrazo_DiasBaixa), 3, 0)));
         }
         if ( StringUtil.StrCmp(A904ContratoServicosPrazo_Tipo, "C") != 0 )
         {
            A1264ContratoServicosPrazo_DiasMedia = 0;
            n1264ContratoServicosPrazo_DiasMedia = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1264ContratoServicosPrazo_DiasMedia", StringUtil.LTrim( StringUtil.Str( (decimal)(A1264ContratoServicosPrazo_DiasMedia), 3, 0)));
            n1264ContratoServicosPrazo_DiasMedia = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1264ContratoServicosPrazo_DiasMedia", StringUtil.LTrim( StringUtil.Str( (decimal)(A1264ContratoServicosPrazo_DiasMedia), 3, 0)));
         }
         if ( StringUtil.StrCmp(A904ContratoServicosPrazo_Tipo, "C") != 0 )
         {
            A1263ContratoServicosPrazo_DiasAlta = 0;
            n1263ContratoServicosPrazo_DiasAlta = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1263ContratoServicosPrazo_DiasAlta", StringUtil.LTrim( StringUtil.Str( (decimal)(A1263ContratoServicosPrazo_DiasAlta), 3, 0)));
            n1263ContratoServicosPrazo_DiasAlta = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1263ContratoServicosPrazo_DiasAlta", StringUtil.LTrim( StringUtil.Str( (decimal)(A1263ContratoServicosPrazo_DiasAlta), 3, 0)));
         }
         if ( ( StringUtil.StrCmp(A904ContratoServicosPrazo_Tipo, "C") == 0 ) && ( (0==A1265ContratoServicosPrazo_DiasBaixa) || (0==A1264ContratoServicosPrazo_DiasMedia) || (0==A1263ContratoServicosPrazo_DiasAlta) ) )
         {
            GX_msglist.addItem("Indique pelo menos a uma quantidade de dias.!", 1, "CONTRATOSERVICOSPRAZO_TIPO");
            AnyError = 1;
            GX_FocusControl = cmbContratoServicosPrazo_Tipo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            return  ;
         }
      }

      protected void BeforeInsert2U112( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate2U112( )
      {
         /* Before Update Rules */
         new loadauditcontratoservicosprazo(context ).execute(  "Y", ref  AV12AuditingObject,  A903ContratoServicosPrazo_CntSrvCod,  Gx_mode) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A903ContratoServicosPrazo_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A903ContratoServicosPrazo_CntSrvCod), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void BeforeDelete2U112( )
      {
         /* Before Delete Rules */
         new loadauditcontratoservicosprazo(context ).execute(  "Y", ref  AV12AuditingObject,  A903ContratoServicosPrazo_CntSrvCod,  Gx_mode) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A903ContratoServicosPrazo_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A903ContratoServicosPrazo_CntSrvCod), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void BeforeComplete2U112( )
      {
         /* Before Complete Rules */
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            new loadauditcontratoservicosprazo(context ).execute(  "N", ref  AV12AuditingObject,  A903ContratoServicosPrazo_CntSrvCod,  Gx_mode) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A903ContratoServicosPrazo_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A903ContratoServicosPrazo_CntSrvCod), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )  )
         {
            new loadauditcontratoservicosprazo(context ).execute(  "N", ref  AV12AuditingObject,  A903ContratoServicosPrazo_CntSrvCod,  Gx_mode) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A903ContratoServicosPrazo_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A903ContratoServicosPrazo_CntSrvCod), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
      }

      protected void BeforeValidate2U112( )
      {
         /* Before Validate Rules */
         if ( ( ( StringUtil.StrCmp(A904ContratoServicosPrazo_Tipo, "F") == 0 ) || ( StringUtil.StrCmp(A904ContratoServicosPrazo_Tipo, "P") == 0 ) ) && (0==A905ContratoServicosPrazo_Dias) )
         {
            GX_msglist.addItem("Indique a quantidade de dias para o tipo de prazo escolhido!", 1, "CONTRATOSERVICOSPRAZO_TIPO");
            AnyError = 1;
            GX_FocusControl = cmbContratoServicosPrazo_Tipo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
      }

      protected void DisableAttributes2U112( )
      {
         edtavServico_sigla_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavServico_sigla_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavServico_sigla_Enabled), 5, 0)));
         cmbContratoServicosPrazo_Tipo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratoServicosPrazo_Tipo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbContratoServicosPrazo_Tipo.Enabled), 5, 0)));
         edtContratoServicosPrazo_Dias_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosPrazo_Dias_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosPrazo_Dias_Enabled), 5, 0)));
         cmbContratoServicosPrazo_Momento.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratoServicosPrazo_Momento_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbContratoServicosPrazo_Momento.Enabled), 5, 0)));
         edtContratoServicosPrazo_Cada_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosPrazo_Cada_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosPrazo_Cada_Enabled), 5, 0)));
         edtContratoServicos_UndCntSgl_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicos_UndCntSgl_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicos_UndCntSgl_Enabled), 5, 0)));
         edtContratoServicosPrazo_DiasBaixa_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosPrazo_DiasBaixa_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosPrazo_DiasBaixa_Enabled), 5, 0)));
         edtContratoServicosPrazo_DiasMedia_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosPrazo_DiasMedia_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosPrazo_DiasMedia_Enabled), 5, 0)));
         edtContratoServicosPrazo_DiasAlta_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosPrazo_DiasAlta_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosPrazo_DiasAlta_Enabled), 5, 0)));
         edtContratoServicosPrazo_CntSrvCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosPrazo_CntSrvCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosPrazo_CntSrvCod_Enabled), 5, 0)));
      }

      protected void ZM2U113( short GX_JID )
      {
         if ( ( GX_JID == 40 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z907ContratoServicosPrazoRegra_Inicio = T002U5_A907ContratoServicosPrazoRegra_Inicio[0];
               Z908ContratoServicosPrazoRegra_Fim = T002U5_A908ContratoServicosPrazoRegra_Fim[0];
               Z909ContratoServicosPrazoRegra_Dias = T002U5_A909ContratoServicosPrazoRegra_Dias[0];
            }
            else
            {
               Z907ContratoServicosPrazoRegra_Inicio = A907ContratoServicosPrazoRegra_Inicio;
               Z908ContratoServicosPrazoRegra_Fim = A908ContratoServicosPrazoRegra_Fim;
               Z909ContratoServicosPrazoRegra_Dias = A909ContratoServicosPrazoRegra_Dias;
            }
         }
         if ( GX_JID == -40 )
         {
            Z903ContratoServicosPrazo_CntSrvCod = A903ContratoServicosPrazo_CntSrvCod;
            Z906ContratoServicosPrazoRegra_Sequencial = A906ContratoServicosPrazoRegra_Sequencial;
            Z907ContratoServicosPrazoRegra_Inicio = A907ContratoServicosPrazoRegra_Inicio;
            Z908ContratoServicosPrazoRegra_Fim = A908ContratoServicosPrazoRegra_Fim;
            Z909ContratoServicosPrazoRegra_Dias = A909ContratoServicosPrazoRegra_Dias;
         }
      }

      protected void standaloneNotModal2U113( )
      {
         edtContratoServicosPrazoRegra_Sequencial_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosPrazoRegra_Sequencial_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosPrazoRegra_Sequencial_Enabled), 5, 0)));
      }

      protected void standaloneModal2U113( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            A910ContratoServicosPrazo_QtdRegras = (short)(O910ContratoServicosPrazo_QtdRegras+1);
            n910ContratoServicosPrazo_QtdRegras = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A910ContratoServicosPrazo_QtdRegras", StringUtil.LTrim( StringUtil.Str( (decimal)(A910ContratoServicosPrazo_QtdRegras), 4, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && ( Gx_BScreen == 1 ) )
         {
            A906ContratoServicosPrazoRegra_Sequencial = A910ContratoServicosPrazo_QtdRegras;
         }
      }

      protected void Load2U113( )
      {
         /* Using cursor T002U39 */
         pr_default.execute(29, new Object[] {A903ContratoServicosPrazo_CntSrvCod, A906ContratoServicosPrazoRegra_Sequencial});
         if ( (pr_default.getStatus(29) != 101) )
         {
            RcdFound113 = 1;
            A907ContratoServicosPrazoRegra_Inicio = T002U39_A907ContratoServicosPrazoRegra_Inicio[0];
            A908ContratoServicosPrazoRegra_Fim = T002U39_A908ContratoServicosPrazoRegra_Fim[0];
            A909ContratoServicosPrazoRegra_Dias = T002U39_A909ContratoServicosPrazoRegra_Dias[0];
            ZM2U113( -40) ;
         }
         pr_default.close(29);
         OnLoadActions2U113( ) ;
      }

      protected void OnLoadActions2U113( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            A910ContratoServicosPrazo_QtdRegras = (short)(O910ContratoServicosPrazo_QtdRegras+1);
            n910ContratoServicosPrazo_QtdRegras = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A910ContratoServicosPrazo_QtdRegras", StringUtil.LTrim( StringUtil.Str( (decimal)(A910ContratoServicosPrazo_QtdRegras), 4, 0)));
         }
         else
         {
            if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )  )
            {
               A910ContratoServicosPrazo_QtdRegras = O910ContratoServicosPrazo_QtdRegras;
               n910ContratoServicosPrazo_QtdRegras = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A910ContratoServicosPrazo_QtdRegras", StringUtil.LTrim( StringUtil.Str( (decimal)(A910ContratoServicosPrazo_QtdRegras), 4, 0)));
            }
            else
            {
               if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )  )
               {
                  A910ContratoServicosPrazo_QtdRegras = (short)(O910ContratoServicosPrazo_QtdRegras-1);
                  n910ContratoServicosPrazo_QtdRegras = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A910ContratoServicosPrazo_QtdRegras", StringUtil.LTrim( StringUtil.Str( (decimal)(A910ContratoServicosPrazo_QtdRegras), 4, 0)));
               }
            }
         }
      }

      protected void CheckExtendedTable2U113( )
      {
         Gx_BScreen = 1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         standaloneModal2U113( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            A910ContratoServicosPrazo_QtdRegras = (short)(O910ContratoServicosPrazo_QtdRegras+1);
            n910ContratoServicosPrazo_QtdRegras = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A910ContratoServicosPrazo_QtdRegras", StringUtil.LTrim( StringUtil.Str( (decimal)(A910ContratoServicosPrazo_QtdRegras), 4, 0)));
         }
         else
         {
            if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )  )
            {
               A910ContratoServicosPrazo_QtdRegras = O910ContratoServicosPrazo_QtdRegras;
               n910ContratoServicosPrazo_QtdRegras = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A910ContratoServicosPrazo_QtdRegras", StringUtil.LTrim( StringUtil.Str( (decimal)(A910ContratoServicosPrazo_QtdRegras), 4, 0)));
            }
            else
            {
               if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )  )
               {
                  A910ContratoServicosPrazo_QtdRegras = (short)(O910ContratoServicosPrazo_QtdRegras-1);
                  n910ContratoServicosPrazo_QtdRegras = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A910ContratoServicosPrazo_QtdRegras", StringUtil.LTrim( StringUtil.Str( (decimal)(A910ContratoServicosPrazo_QtdRegras), 4, 0)));
               }
            }
         }
      }

      protected void CloseExtendedTableCursors2U113( )
      {
      }

      protected void enableDisable2U113( )
      {
      }

      protected void GetKey2U113( )
      {
         /* Using cursor T002U40 */
         pr_default.execute(30, new Object[] {A903ContratoServicosPrazo_CntSrvCod, A906ContratoServicosPrazoRegra_Sequencial});
         if ( (pr_default.getStatus(30) != 101) )
         {
            RcdFound113 = 1;
         }
         else
         {
            RcdFound113 = 0;
         }
         pr_default.close(30);
      }

      protected void getByPrimaryKey2U113( )
      {
         /* Using cursor T002U5 */
         pr_default.execute(3, new Object[] {A903ContratoServicosPrazo_CntSrvCod, A906ContratoServicosPrazoRegra_Sequencial});
         if ( (pr_default.getStatus(3) != 101) )
         {
            ZM2U113( 40) ;
            RcdFound113 = 1;
            InitializeNonKey2U113( ) ;
            A906ContratoServicosPrazoRegra_Sequencial = T002U5_A906ContratoServicosPrazoRegra_Sequencial[0];
            A907ContratoServicosPrazoRegra_Inicio = T002U5_A907ContratoServicosPrazoRegra_Inicio[0];
            A908ContratoServicosPrazoRegra_Fim = T002U5_A908ContratoServicosPrazoRegra_Fim[0];
            A909ContratoServicosPrazoRegra_Dias = T002U5_A909ContratoServicosPrazoRegra_Dias[0];
            Z903ContratoServicosPrazo_CntSrvCod = A903ContratoServicosPrazo_CntSrvCod;
            Z906ContratoServicosPrazoRegra_Sequencial = A906ContratoServicosPrazoRegra_Sequencial;
            sMode113 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load2U113( ) ;
            Gx_mode = sMode113;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound113 = 0;
            InitializeNonKey2U113( ) ;
            sMode113 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal2U113( ) ;
            Gx_mode = sMode113;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            DisableAttributes2U113( ) ;
         }
         pr_default.close(3);
      }

      protected void CheckOptimisticConcurrency2U113( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T002U4 */
            pr_default.execute(2, new Object[] {A903ContratoServicosPrazo_CntSrvCod, A906ContratoServicosPrazoRegra_Sequencial});
            if ( (pr_default.getStatus(2) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContratoServicosPrazoContratoServicosPrazoRegra"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(2) == 101) || ( Z907ContratoServicosPrazoRegra_Inicio != T002U4_A907ContratoServicosPrazoRegra_Inicio[0] ) || ( Z908ContratoServicosPrazoRegra_Fim != T002U4_A908ContratoServicosPrazoRegra_Fim[0] ) || ( Z909ContratoServicosPrazoRegra_Dias != T002U4_A909ContratoServicosPrazoRegra_Dias[0] ) )
            {
               if ( Z907ContratoServicosPrazoRegra_Inicio != T002U4_A907ContratoServicosPrazoRegra_Inicio[0] )
               {
                  GXUtil.WriteLog("contratoservicosprazo:[seudo value changed for attri]"+"ContratoServicosPrazoRegra_Inicio");
                  GXUtil.WriteLogRaw("Old: ",Z907ContratoServicosPrazoRegra_Inicio);
                  GXUtil.WriteLogRaw("Current: ",T002U4_A907ContratoServicosPrazoRegra_Inicio[0]);
               }
               if ( Z908ContratoServicosPrazoRegra_Fim != T002U4_A908ContratoServicosPrazoRegra_Fim[0] )
               {
                  GXUtil.WriteLog("contratoservicosprazo:[seudo value changed for attri]"+"ContratoServicosPrazoRegra_Fim");
                  GXUtil.WriteLogRaw("Old: ",Z908ContratoServicosPrazoRegra_Fim);
                  GXUtil.WriteLogRaw("Current: ",T002U4_A908ContratoServicosPrazoRegra_Fim[0]);
               }
               if ( Z909ContratoServicosPrazoRegra_Dias != T002U4_A909ContratoServicosPrazoRegra_Dias[0] )
               {
                  GXUtil.WriteLog("contratoservicosprazo:[seudo value changed for attri]"+"ContratoServicosPrazoRegra_Dias");
                  GXUtil.WriteLogRaw("Old: ",Z909ContratoServicosPrazoRegra_Dias);
                  GXUtil.WriteLogRaw("Current: ",T002U4_A909ContratoServicosPrazoRegra_Dias[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"ContratoServicosPrazoContratoServicosPrazoRegra"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert2U113( )
      {
         BeforeValidate2U113( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable2U113( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM2U113( 0) ;
            CheckOptimisticConcurrency2U113( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm2U113( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert2U113( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T002U41 */
                     pr_default.execute(31, new Object[] {A903ContratoServicosPrazo_CntSrvCod, A906ContratoServicosPrazoRegra_Sequencial, A907ContratoServicosPrazoRegra_Inicio, A908ContratoServicosPrazoRegra_Fim, A909ContratoServicosPrazoRegra_Dias});
                     pr_default.close(31);
                     dsDefault.SmartCacheProvider.SetUpdated("ContratoServicosPrazoContratoServicosPrazoRegra") ;
                     if ( (pr_default.getStatus(31) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load2U113( ) ;
            }
            EndLevel2U113( ) ;
         }
         CloseExtendedTableCursors2U113( ) ;
      }

      protected void Update2U113( )
      {
         BeforeValidate2U113( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable2U113( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency2U113( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm2U113( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate2U113( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T002U42 */
                     pr_default.execute(32, new Object[] {A907ContratoServicosPrazoRegra_Inicio, A908ContratoServicosPrazoRegra_Fim, A909ContratoServicosPrazoRegra_Dias, A903ContratoServicosPrazo_CntSrvCod, A906ContratoServicosPrazoRegra_Sequencial});
                     pr_default.close(32);
                     dsDefault.SmartCacheProvider.SetUpdated("ContratoServicosPrazoContratoServicosPrazoRegra") ;
                     if ( (pr_default.getStatus(32) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContratoServicosPrazoContratoServicosPrazoRegra"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate2U113( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey2U113( ) ;
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel2U113( ) ;
         }
         CloseExtendedTableCursors2U113( ) ;
      }

      protected void DeferredUpdate2U113( )
      {
      }

      protected void Delete2U113( )
      {
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         BeforeValidate2U113( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency2U113( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls2U113( ) ;
            AfterConfirm2U113( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete2U113( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T002U43 */
                  pr_default.execute(33, new Object[] {A903ContratoServicosPrazo_CntSrvCod, A906ContratoServicosPrazoRegra_Sequencial});
                  pr_default.close(33);
                  dsDefault.SmartCacheProvider.SetUpdated("ContratoServicosPrazoContratoServicosPrazoRegra") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode113 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel2U113( ) ;
         Gx_mode = sMode113;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls2U113( )
      {
         standaloneModal2U113( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               A910ContratoServicosPrazo_QtdRegras = (short)(O910ContratoServicosPrazo_QtdRegras+1);
               n910ContratoServicosPrazo_QtdRegras = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A910ContratoServicosPrazo_QtdRegras", StringUtil.LTrim( StringUtil.Str( (decimal)(A910ContratoServicosPrazo_QtdRegras), 4, 0)));
            }
            else
            {
               if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )  )
               {
                  A910ContratoServicosPrazo_QtdRegras = O910ContratoServicosPrazo_QtdRegras;
                  n910ContratoServicosPrazo_QtdRegras = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A910ContratoServicosPrazo_QtdRegras", StringUtil.LTrim( StringUtil.Str( (decimal)(A910ContratoServicosPrazo_QtdRegras), 4, 0)));
               }
               else
               {
                  if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )  )
                  {
                     A910ContratoServicosPrazo_QtdRegras = (short)(O910ContratoServicosPrazo_QtdRegras-1);
                     n910ContratoServicosPrazo_QtdRegras = false;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A910ContratoServicosPrazo_QtdRegras", StringUtil.LTrim( StringUtil.Str( (decimal)(A910ContratoServicosPrazo_QtdRegras), 4, 0)));
                  }
               }
            }
         }
      }

      protected void EndLevel2U113( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(2);
         }
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart2U113( )
      {
         /* Scan By routine */
         /* Using cursor T002U44 */
         pr_default.execute(34, new Object[] {A903ContratoServicosPrazo_CntSrvCod});
         RcdFound113 = 0;
         if ( (pr_default.getStatus(34) != 101) )
         {
            RcdFound113 = 1;
            A906ContratoServicosPrazoRegra_Sequencial = T002U44_A906ContratoServicosPrazoRegra_Sequencial[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext2U113( )
      {
         /* Scan next routine */
         pr_default.readNext(34);
         RcdFound113 = 0;
         if ( (pr_default.getStatus(34) != 101) )
         {
            RcdFound113 = 1;
            A906ContratoServicosPrazoRegra_Sequencial = T002U44_A906ContratoServicosPrazoRegra_Sequencial[0];
         }
      }

      protected void ScanEnd2U113( )
      {
         pr_default.close(34);
      }

      protected void AfterConfirm2U113( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert2U113( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate2U113( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete2U113( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete2U113( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate2U113( )
      {
         /* Before Validate Rules */
         if ( ( StringUtil.StrCmp(A904ContratoServicosPrazo_Tipo, "V") == 0 ) && ( A909ContratoServicosPrazoRegra_Dias == 0 ) )
         {
            GXCCtl = "CONTRATOSERVICOSPRAZOREGRA_DIAS_" + sGXsfl_97_idx;
            GX_msglist.addItem("Indique pelo menos uma regra para o tipo de prazo escolhido!", 1, GXCCtl);
            AnyError = 1;
            GX_FocusControl = edtContratoServicosPrazoRegra_Dias_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
      }

      protected void DisableAttributes2U113( )
      {
         edtContratoServicosPrazoRegra_Sequencial_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosPrazoRegra_Sequencial_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosPrazoRegra_Sequencial_Enabled), 5, 0)));
         edtContratoServicosPrazoRegra_Inicio_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosPrazoRegra_Inicio_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosPrazoRegra_Inicio_Enabled), 5, 0)));
         edtContratoServicosPrazoRegra_Fim_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosPrazoRegra_Fim_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosPrazoRegra_Fim_Enabled), 5, 0)));
         edtContratoServicosPrazoRegra_Dias_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosPrazoRegra_Dias_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosPrazoRegra_Dias_Enabled), 5, 0)));
      }

      protected void ZM2U188( short GX_JID )
      {
         if ( ( GX_JID == 41 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z1734ContratoServicosPrazoPlnj_Inicio = T002U3_A1734ContratoServicosPrazoPlnj_Inicio[0];
               Z1735ContratoServicosPrazoPlnj_Fim = T002U3_A1735ContratoServicosPrazoPlnj_Fim[0];
               Z1736ContratoServicosPrazoPlnj_Dias = T002U3_A1736ContratoServicosPrazoPlnj_Dias[0];
            }
            else
            {
               Z1734ContratoServicosPrazoPlnj_Inicio = A1734ContratoServicosPrazoPlnj_Inicio;
               Z1735ContratoServicosPrazoPlnj_Fim = A1735ContratoServicosPrazoPlnj_Fim;
               Z1736ContratoServicosPrazoPlnj_Dias = A1736ContratoServicosPrazoPlnj_Dias;
            }
         }
         if ( GX_JID == -41 )
         {
            Z903ContratoServicosPrazo_CntSrvCod = A903ContratoServicosPrazo_CntSrvCod;
            Z1724ContratoServicosPrazoPlnj_Sequencial = A1724ContratoServicosPrazoPlnj_Sequencial;
            Z1734ContratoServicosPrazoPlnj_Inicio = A1734ContratoServicosPrazoPlnj_Inicio;
            Z1735ContratoServicosPrazoPlnj_Fim = A1735ContratoServicosPrazoPlnj_Fim;
            Z1736ContratoServicosPrazoPlnj_Dias = A1736ContratoServicosPrazoPlnj_Dias;
         }
      }

      protected void standaloneNotModal2U188( )
      {
         edtContratoServicosPrazoPlnj_Sequencial_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosPrazoPlnj_Sequencial_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosPrazoPlnj_Sequencial_Enabled), 5, 0)));
      }

      protected void standaloneModal2U188( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            A1737ContratoServicosPrazo_QtdPlnj = (short)(O1737ContratoServicosPrazo_QtdPlnj+1);
            n1737ContratoServicosPrazo_QtdPlnj = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1737ContratoServicosPrazo_QtdPlnj", StringUtil.LTrim( StringUtil.Str( (decimal)(A1737ContratoServicosPrazo_QtdPlnj), 4, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && ( Gx_BScreen == 1 ) )
         {
            A1724ContratoServicosPrazoPlnj_Sequencial = A1737ContratoServicosPrazo_QtdPlnj;
         }
      }

      protected void Load2U188( )
      {
         /* Using cursor T002U45 */
         pr_default.execute(35, new Object[] {A903ContratoServicosPrazo_CntSrvCod, A1724ContratoServicosPrazoPlnj_Sequencial});
         if ( (pr_default.getStatus(35) != 101) )
         {
            RcdFound188 = 1;
            A1734ContratoServicosPrazoPlnj_Inicio = T002U45_A1734ContratoServicosPrazoPlnj_Inicio[0];
            A1735ContratoServicosPrazoPlnj_Fim = T002U45_A1735ContratoServicosPrazoPlnj_Fim[0];
            A1736ContratoServicosPrazoPlnj_Dias = T002U45_A1736ContratoServicosPrazoPlnj_Dias[0];
            ZM2U188( -41) ;
         }
         pr_default.close(35);
         OnLoadActions2U188( ) ;
      }

      protected void OnLoadActions2U188( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            A1737ContratoServicosPrazo_QtdPlnj = (short)(O1737ContratoServicosPrazo_QtdPlnj+1);
            n1737ContratoServicosPrazo_QtdPlnj = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1737ContratoServicosPrazo_QtdPlnj", StringUtil.LTrim( StringUtil.Str( (decimal)(A1737ContratoServicosPrazo_QtdPlnj), 4, 0)));
         }
         else
         {
            if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )  )
            {
               A1737ContratoServicosPrazo_QtdPlnj = O1737ContratoServicosPrazo_QtdPlnj;
               n1737ContratoServicosPrazo_QtdPlnj = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1737ContratoServicosPrazo_QtdPlnj", StringUtil.LTrim( StringUtil.Str( (decimal)(A1737ContratoServicosPrazo_QtdPlnj), 4, 0)));
            }
            else
            {
               if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )  )
               {
                  A1737ContratoServicosPrazo_QtdPlnj = (short)(O1737ContratoServicosPrazo_QtdPlnj-1);
                  n1737ContratoServicosPrazo_QtdPlnj = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1737ContratoServicosPrazo_QtdPlnj", StringUtil.LTrim( StringUtil.Str( (decimal)(A1737ContratoServicosPrazo_QtdPlnj), 4, 0)));
               }
            }
         }
      }

      protected void CheckExtendedTable2U188( )
      {
         Gx_BScreen = 1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         standaloneModal2U188( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            A1737ContratoServicosPrazo_QtdPlnj = (short)(O1737ContratoServicosPrazo_QtdPlnj+1);
            n1737ContratoServicosPrazo_QtdPlnj = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1737ContratoServicosPrazo_QtdPlnj", StringUtil.LTrim( StringUtil.Str( (decimal)(A1737ContratoServicosPrazo_QtdPlnj), 4, 0)));
         }
         else
         {
            if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )  )
            {
               A1737ContratoServicosPrazo_QtdPlnj = O1737ContratoServicosPrazo_QtdPlnj;
               n1737ContratoServicosPrazo_QtdPlnj = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1737ContratoServicosPrazo_QtdPlnj", StringUtil.LTrim( StringUtil.Str( (decimal)(A1737ContratoServicosPrazo_QtdPlnj), 4, 0)));
            }
            else
            {
               if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )  )
               {
                  A1737ContratoServicosPrazo_QtdPlnj = (short)(O1737ContratoServicosPrazo_QtdPlnj-1);
                  n1737ContratoServicosPrazo_QtdPlnj = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1737ContratoServicosPrazo_QtdPlnj", StringUtil.LTrim( StringUtil.Str( (decimal)(A1737ContratoServicosPrazo_QtdPlnj), 4, 0)));
               }
            }
         }
      }

      protected void CloseExtendedTableCursors2U188( )
      {
      }

      protected void enableDisable2U188( )
      {
      }

      protected void GetKey2U188( )
      {
         /* Using cursor T002U46 */
         pr_default.execute(36, new Object[] {A903ContratoServicosPrazo_CntSrvCod, A1724ContratoServicosPrazoPlnj_Sequencial});
         if ( (pr_default.getStatus(36) != 101) )
         {
            RcdFound188 = 1;
         }
         else
         {
            RcdFound188 = 0;
         }
         pr_default.close(36);
      }

      protected void getByPrimaryKey2U188( )
      {
         /* Using cursor T002U3 */
         pr_default.execute(1, new Object[] {A903ContratoServicosPrazo_CntSrvCod, A1724ContratoServicosPrazoPlnj_Sequencial});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM2U188( 41) ;
            RcdFound188 = 1;
            InitializeNonKey2U188( ) ;
            A1724ContratoServicosPrazoPlnj_Sequencial = T002U3_A1724ContratoServicosPrazoPlnj_Sequencial[0];
            A1734ContratoServicosPrazoPlnj_Inicio = T002U3_A1734ContratoServicosPrazoPlnj_Inicio[0];
            A1735ContratoServicosPrazoPlnj_Fim = T002U3_A1735ContratoServicosPrazoPlnj_Fim[0];
            A1736ContratoServicosPrazoPlnj_Dias = T002U3_A1736ContratoServicosPrazoPlnj_Dias[0];
            Z903ContratoServicosPrazo_CntSrvCod = A903ContratoServicosPrazo_CntSrvCod;
            Z1724ContratoServicosPrazoPlnj_Sequencial = A1724ContratoServicosPrazoPlnj_Sequencial;
            sMode188 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load2U188( ) ;
            Gx_mode = sMode188;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound188 = 0;
            InitializeNonKey2U188( ) ;
            sMode188 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal2U188( ) ;
            Gx_mode = sMode188;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            DisableAttributes2U188( ) ;
         }
         pr_default.close(1);
      }

      protected void CheckOptimisticConcurrency2U188( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T002U2 */
            pr_default.execute(0, new Object[] {A903ContratoServicosPrazo_CntSrvCod, A1724ContratoServicosPrazoPlnj_Sequencial});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContratoServicosPrazoPrazoPlnj"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( Z1734ContratoServicosPrazoPlnj_Inicio != T002U2_A1734ContratoServicosPrazoPlnj_Inicio[0] ) || ( Z1735ContratoServicosPrazoPlnj_Fim != T002U2_A1735ContratoServicosPrazoPlnj_Fim[0] ) || ( Z1736ContratoServicosPrazoPlnj_Dias != T002U2_A1736ContratoServicosPrazoPlnj_Dias[0] ) )
            {
               if ( Z1734ContratoServicosPrazoPlnj_Inicio != T002U2_A1734ContratoServicosPrazoPlnj_Inicio[0] )
               {
                  GXUtil.WriteLog("contratoservicosprazo:[seudo value changed for attri]"+"ContratoServicosPrazoPlnj_Inicio");
                  GXUtil.WriteLogRaw("Old: ",Z1734ContratoServicosPrazoPlnj_Inicio);
                  GXUtil.WriteLogRaw("Current: ",T002U2_A1734ContratoServicosPrazoPlnj_Inicio[0]);
               }
               if ( Z1735ContratoServicosPrazoPlnj_Fim != T002U2_A1735ContratoServicosPrazoPlnj_Fim[0] )
               {
                  GXUtil.WriteLog("contratoservicosprazo:[seudo value changed for attri]"+"ContratoServicosPrazoPlnj_Fim");
                  GXUtil.WriteLogRaw("Old: ",Z1735ContratoServicosPrazoPlnj_Fim);
                  GXUtil.WriteLogRaw("Current: ",T002U2_A1735ContratoServicosPrazoPlnj_Fim[0]);
               }
               if ( Z1736ContratoServicosPrazoPlnj_Dias != T002U2_A1736ContratoServicosPrazoPlnj_Dias[0] )
               {
                  GXUtil.WriteLog("contratoservicosprazo:[seudo value changed for attri]"+"ContratoServicosPrazoPlnj_Dias");
                  GXUtil.WriteLogRaw("Old: ",Z1736ContratoServicosPrazoPlnj_Dias);
                  GXUtil.WriteLogRaw("Current: ",T002U2_A1736ContratoServicosPrazoPlnj_Dias[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"ContratoServicosPrazoPrazoPlnj"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert2U188( )
      {
         BeforeValidate2U188( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable2U188( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM2U188( 0) ;
            CheckOptimisticConcurrency2U188( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm2U188( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert2U188( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T002U47 */
                     pr_default.execute(37, new Object[] {A903ContratoServicosPrazo_CntSrvCod, A1724ContratoServicosPrazoPlnj_Sequencial, A1734ContratoServicosPrazoPlnj_Inicio, A1735ContratoServicosPrazoPlnj_Fim, A1736ContratoServicosPrazoPlnj_Dias});
                     pr_default.close(37);
                     dsDefault.SmartCacheProvider.SetUpdated("ContratoServicosPrazoPrazoPlnj") ;
                     if ( (pr_default.getStatus(37) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load2U188( ) ;
            }
            EndLevel2U188( ) ;
         }
         CloseExtendedTableCursors2U188( ) ;
      }

      protected void Update2U188( )
      {
         BeforeValidate2U188( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable2U188( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency2U188( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm2U188( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate2U188( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T002U48 */
                     pr_default.execute(38, new Object[] {A1734ContratoServicosPrazoPlnj_Inicio, A1735ContratoServicosPrazoPlnj_Fim, A1736ContratoServicosPrazoPlnj_Dias, A903ContratoServicosPrazo_CntSrvCod, A1724ContratoServicosPrazoPlnj_Sequencial});
                     pr_default.close(38);
                     dsDefault.SmartCacheProvider.SetUpdated("ContratoServicosPrazoPrazoPlnj") ;
                     if ( (pr_default.getStatus(38) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContratoServicosPrazoPrazoPlnj"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate2U188( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey2U188( ) ;
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel2U188( ) ;
         }
         CloseExtendedTableCursors2U188( ) ;
      }

      protected void DeferredUpdate2U188( )
      {
      }

      protected void Delete2U188( )
      {
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         BeforeValidate2U188( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency2U188( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls2U188( ) ;
            AfterConfirm2U188( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete2U188( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T002U49 */
                  pr_default.execute(39, new Object[] {A903ContratoServicosPrazo_CntSrvCod, A1724ContratoServicosPrazoPlnj_Sequencial});
                  pr_default.close(39);
                  dsDefault.SmartCacheProvider.SetUpdated("ContratoServicosPrazoPrazoPlnj") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode188 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel2U188( ) ;
         Gx_mode = sMode188;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls2U188( )
      {
         standaloneModal2U188( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               A1737ContratoServicosPrazo_QtdPlnj = (short)(O1737ContratoServicosPrazo_QtdPlnj+1);
               n1737ContratoServicosPrazo_QtdPlnj = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1737ContratoServicosPrazo_QtdPlnj", StringUtil.LTrim( StringUtil.Str( (decimal)(A1737ContratoServicosPrazo_QtdPlnj), 4, 0)));
            }
            else
            {
               if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )  )
               {
                  A1737ContratoServicosPrazo_QtdPlnj = O1737ContratoServicosPrazo_QtdPlnj;
                  n1737ContratoServicosPrazo_QtdPlnj = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1737ContratoServicosPrazo_QtdPlnj", StringUtil.LTrim( StringUtil.Str( (decimal)(A1737ContratoServicosPrazo_QtdPlnj), 4, 0)));
               }
               else
               {
                  if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )  )
                  {
                     A1737ContratoServicosPrazo_QtdPlnj = (short)(O1737ContratoServicosPrazo_QtdPlnj-1);
                     n1737ContratoServicosPrazo_QtdPlnj = false;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1737ContratoServicosPrazo_QtdPlnj", StringUtil.LTrim( StringUtil.Str( (decimal)(A1737ContratoServicosPrazo_QtdPlnj), 4, 0)));
                  }
               }
            }
         }
      }

      protected void EndLevel2U188( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart2U188( )
      {
         /* Scan By routine */
         /* Using cursor T002U50 */
         pr_default.execute(40, new Object[] {A903ContratoServicosPrazo_CntSrvCod});
         RcdFound188 = 0;
         if ( (pr_default.getStatus(40) != 101) )
         {
            RcdFound188 = 1;
            A1724ContratoServicosPrazoPlnj_Sequencial = T002U50_A1724ContratoServicosPrazoPlnj_Sequencial[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext2U188( )
      {
         /* Scan next routine */
         pr_default.readNext(40);
         RcdFound188 = 0;
         if ( (pr_default.getStatus(40) != 101) )
         {
            RcdFound188 = 1;
            A1724ContratoServicosPrazoPlnj_Sequencial = T002U50_A1724ContratoServicosPrazoPlnj_Sequencial[0];
         }
      }

      protected void ScanEnd2U188( )
      {
         pr_default.close(40);
      }

      protected void AfterConfirm2U188( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert2U188( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate2U188( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete2U188( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete2U188( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate2U188( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes2U188( )
      {
         edtContratoServicosPrazoPlnj_Sequencial_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosPrazoPlnj_Sequencial_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosPrazoPlnj_Sequencial_Enabled), 5, 0)));
         edtContratoServicosPrazoPlnj_Inicio_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosPrazoPlnj_Inicio_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosPrazoPlnj_Inicio_Enabled), 5, 0)));
         edtContratoServicosPrazoPlnj_Fim_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosPrazoPlnj_Fim_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosPrazoPlnj_Fim_Enabled), 5, 0)));
         edtContratoServicosPrazoPlnj_Dias_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosPrazoPlnj_Dias_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosPrazoPlnj_Dias_Enabled), 5, 0)));
      }

      protected void SubsflControlProps_97113( )
      {
         edtContratoServicosPrazoRegra_Sequencial_Internalname = "CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_"+sGXsfl_97_idx;
         edtContratoServicosPrazoRegra_Inicio_Internalname = "CONTRATOSERVICOSPRAZOREGRA_INICIO_"+sGXsfl_97_idx;
         edtContratoServicosPrazoRegra_Fim_Internalname = "CONTRATOSERVICOSPRAZOREGRA_FIM_"+sGXsfl_97_idx;
         edtContratoServicosPrazoRegra_Dias_Internalname = "CONTRATOSERVICOSPRAZOREGRA_DIAS_"+sGXsfl_97_idx;
      }

      protected void SubsflControlProps_fel_97113( )
      {
         edtContratoServicosPrazoRegra_Sequencial_Internalname = "CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_"+sGXsfl_97_fel_idx;
         edtContratoServicosPrazoRegra_Inicio_Internalname = "CONTRATOSERVICOSPRAZOREGRA_INICIO_"+sGXsfl_97_fel_idx;
         edtContratoServicosPrazoRegra_Fim_Internalname = "CONTRATOSERVICOSPRAZOREGRA_FIM_"+sGXsfl_97_fel_idx;
         edtContratoServicosPrazoRegra_Dias_Internalname = "CONTRATOSERVICOSPRAZOREGRA_DIAS_"+sGXsfl_97_fel_idx;
      }

      protected void AddRow2U113( )
      {
         nGXsfl_97_idx = (short)(nGXsfl_97_idx+1);
         sGXsfl_97_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_97_idx), 4, 0)), 4, "0");
         SubsflControlProps_97113( ) ;
         SendRow2U113( ) ;
      }

      protected void SendRow2U113( )
      {
         GridRow = GXWebRow.GetNew(context);
         if ( subGrid_Backcolorstyle == 0 )
         {
            /* None style subfile background logic. */
            subGrid_Backstyle = 0;
            if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
            {
               subGrid_Linesclass = subGrid_Class+"Odd";
            }
         }
         else if ( subGrid_Backcolorstyle == 1 )
         {
            /* Uniform style subfile background logic. */
            subGrid_Backstyle = 0;
            subGrid_Backcolor = subGrid_Allbackcolor;
            if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
            {
               subGrid_Linesclass = subGrid_Class+"Uniform";
            }
         }
         else if ( subGrid_Backcolorstyle == 2 )
         {
            /* Header style subfile background logic. */
            subGrid_Backstyle = 1;
            if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
            {
               subGrid_Linesclass = subGrid_Class+"Odd";
            }
            subGrid_Backcolor = (int)(0xFFFFFF);
         }
         else if ( subGrid_Backcolorstyle == 3 )
         {
            /* Report style subfile background logic. */
            subGrid_Backstyle = 1;
            if ( ((int)((nGXsfl_97_idx) % (2))) == 0 )
            {
               subGrid_Backcolor = (int)(0xFFFFFF);
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Even";
               }
            }
            else
            {
               subGrid_Backcolor = (int)(0xFFFFFF);
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
         }
         /* Subfile cell */
         /* Single line edit */
         ROClassString = "BootstrapAttribute";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoServicosPrazoRegra_Sequencial_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A906ContratoServicosPrazoRegra_Sequencial), 4, 0, ",", "")),((edtContratoServicosPrazoRegra_Sequencial_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A906ContratoServicosPrazoRegra_Sequencial), "ZZZ9")) : context.localUtil.Format( (decimal)(A906ContratoServicosPrazoRegra_Sequencial), "ZZZ9")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoServicosPrazoRegra_Sequencial_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(int)edtContratoServicosPrazoRegra_Sequencial_Enabled,(short)0,(String)"text",(String)"",(short)25,(String)"px",(short)17,(String)"px",(short)4,(short)0,(short)0,(short)97,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
         /* Subfile cell */
         /* Single line edit */
         TempTags = " gxoch1=\"gx.fn.setControlValue('nIsMod_113_" + sGXsfl_97_idx + "',1);\"  onfocus=\"gx.evt.onfocus(this, 99,'',false,'" + sGXsfl_97_idx + "',97)\"";
         ROClassString = "BootstrapAttribute";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoServicosPrazoRegra_Inicio_Internalname,StringUtil.LTrim( StringUtil.NToC( A907ContratoServicosPrazoRegra_Inicio, 14, 5, ",", "")),((edtContratoServicosPrazoRegra_Inicio_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A907ContratoServicosPrazoRegra_Inicio, "ZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( A907ContratoServicosPrazoRegra_Inicio, "ZZ,ZZZ,ZZ9.999")),TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,99);\"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoServicosPrazoRegra_Inicio_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtContratoServicosPrazoRegra_Inicio_Enabled,(short)0,(String)"text",(String)"",(short)94,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)97,(short)1,(short)-1,(short)0,(bool)true,(String)"PontosDeFuncao",(String)"right",(bool)false});
         /* Subfile cell */
         /* Single line edit */
         TempTags = " gxoch1=\"gx.fn.setControlValue('nIsMod_113_" + sGXsfl_97_idx + "',1);\"  onfocus=\"gx.evt.onfocus(this, 100,'',false,'" + sGXsfl_97_idx + "',97)\"";
         ROClassString = "BootstrapAttribute";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoServicosPrazoRegra_Fim_Internalname,StringUtil.LTrim( StringUtil.NToC( A908ContratoServicosPrazoRegra_Fim, 14, 5, ",", "")),((edtContratoServicosPrazoRegra_Fim_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A908ContratoServicosPrazoRegra_Fim, "ZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( A908ContratoServicosPrazoRegra_Fim, "ZZ,ZZZ,ZZ9.999")),TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,100);\"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoServicosPrazoRegra_Fim_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtContratoServicosPrazoRegra_Fim_Enabled,(short)0,(String)"text",(String)"",(short)94,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)97,(short)1,(short)-1,(short)0,(bool)true,(String)"PontosDeFuncao",(String)"right",(bool)false});
         /* Subfile cell */
         /* Single line edit */
         TempTags = " gxoch1=\"gx.fn.setControlValue('nIsMod_113_" + sGXsfl_97_idx + "',1);\"  onfocus=\"gx.evt.onfocus(this, 101,'',false,'" + sGXsfl_97_idx + "',97)\"";
         ROClassString = "BootstrapAttribute";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoServicosPrazoRegra_Dias_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A909ContratoServicosPrazoRegra_Dias), 3, 0, ",", "")),((edtContratoServicosPrazoRegra_Dias_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A909ContratoServicosPrazoRegra_Dias), "ZZ9")) : context.localUtil.Format( (decimal)(A909ContratoServicosPrazoRegra_Dias), "ZZ9")),TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,101);\"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoServicosPrazoRegra_Dias_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtContratoServicosPrazoRegra_Dias_Enabled,(short)0,(String)"text",(String)"",(short)28,(String)"px",(short)17,(String)"px",(short)3,(short)0,(short)0,(short)97,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
         context.httpAjaxContext.ajax_sending_grid_row(GridRow);
         GXCCtl = "Z906ContratoServicosPrazoRegra_Sequencial_" + sGXsfl_97_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(Z906ContratoServicosPrazoRegra_Sequencial), 4, 0, ",", "")));
         GXCCtl = "Z907ContratoServicosPrazoRegra_Inicio_" + sGXsfl_97_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( Z907ContratoServicosPrazoRegra_Inicio, 14, 5, ",", "")));
         GXCCtl = "Z908ContratoServicosPrazoRegra_Fim_" + sGXsfl_97_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( Z908ContratoServicosPrazoRegra_Fim, 14, 5, ",", "")));
         GXCCtl = "Z909ContratoServicosPrazoRegra_Dias_" + sGXsfl_97_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(Z909ContratoServicosPrazoRegra_Dias), 3, 0, ",", "")));
         GXCCtl = "nRcdDeleted_113_" + sGXsfl_97_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdDeleted_113), 4, 0, ",", "")));
         GXCCtl = "nRcdExists_113_" + sGXsfl_97_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdExists_113), 4, 0, ",", "")));
         GXCCtl = "nIsMod_113_" + sGXsfl_97_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(nIsMod_113), 4, 0, ",", "")));
         GXCCtl = "vAUDITINGOBJECT_" + sGXsfl_97_idx;
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, GXCCtl, AV12AuditingObject);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(GXCCtl, AV12AuditingObject);
         }
         GXCCtl = "vPGMNAME_" + sGXsfl_97_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.RTrim( AV14Pgmname));
         GXCCtl = "vMODE_" + sGXsfl_97_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.RTrim( Gx_mode));
         GXCCtl = "vTRNCONTEXT_" + sGXsfl_97_idx;
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, GXCCtl, AV9TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(GXCCtl, AV9TrnContext);
         }
         GXCCtl = "vCONTRATOSERVICOSPRAZO_CNTSRVCOD_" + sGXsfl_97_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7ContratoServicosPrazo_CntSrvCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_"+sGXsfl_97_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosPrazoRegra_Sequencial_Enabled), 5, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOSPRAZOREGRA_INICIO_"+sGXsfl_97_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosPrazoRegra_Inicio_Enabled), 5, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOSPRAZOREGRA_FIM_"+sGXsfl_97_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosPrazoRegra_Fim_Enabled), 5, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOSPRAZOREGRA_DIAS_"+sGXsfl_97_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosPrazoRegra_Dias_Enabled), 5, 0, ".", "")));
         context.httpAjaxContext.ajax_sending_grid_row(null);
         GridContainer.AddRow(GridRow);
      }

      protected void ReadRow2U113( )
      {
         nGXsfl_97_idx = (short)(nGXsfl_97_idx+1);
         sGXsfl_97_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_97_idx), 4, 0)), 4, "0");
         SubsflControlProps_97113( ) ;
         edtContratoServicosPrazoRegra_Sequencial_Enabled = (int)(context.localUtil.CToN( cgiGet( "CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL_"+sGXsfl_97_idx+"Enabled"), ",", "."));
         edtContratoServicosPrazoRegra_Inicio_Enabled = (int)(context.localUtil.CToN( cgiGet( "CONTRATOSERVICOSPRAZOREGRA_INICIO_"+sGXsfl_97_idx+"Enabled"), ",", "."));
         edtContratoServicosPrazoRegra_Fim_Enabled = (int)(context.localUtil.CToN( cgiGet( "CONTRATOSERVICOSPRAZOREGRA_FIM_"+sGXsfl_97_idx+"Enabled"), ",", "."));
         edtContratoServicosPrazoRegra_Dias_Enabled = (int)(context.localUtil.CToN( cgiGet( "CONTRATOSERVICOSPRAZOREGRA_DIAS_"+sGXsfl_97_idx+"Enabled"), ",", "."));
         A906ContratoServicosPrazoRegra_Sequencial = (short)(context.localUtil.CToN( cgiGet( edtContratoServicosPrazoRegra_Sequencial_Internalname), ",", "."));
         if ( ( ( context.localUtil.CToN( cgiGet( edtContratoServicosPrazoRegra_Inicio_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContratoServicosPrazoRegra_Inicio_Internalname), ",", ".") > 99999999.99999m ) ) )
         {
            GXCCtl = "CONTRATOSERVICOSPRAZOREGRA_INICIO_" + sGXsfl_97_idx;
            GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, GXCCtl);
            AnyError = 1;
            GX_FocusControl = edtContratoServicosPrazoRegra_Inicio_Internalname;
            wbErr = true;
            A907ContratoServicosPrazoRegra_Inicio = 0;
         }
         else
         {
            A907ContratoServicosPrazoRegra_Inicio = context.localUtil.CToN( cgiGet( edtContratoServicosPrazoRegra_Inicio_Internalname), ",", ".");
         }
         if ( ( ( context.localUtil.CToN( cgiGet( edtContratoServicosPrazoRegra_Fim_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContratoServicosPrazoRegra_Fim_Internalname), ",", ".") > 99999999.99999m ) ) )
         {
            GXCCtl = "CONTRATOSERVICOSPRAZOREGRA_FIM_" + sGXsfl_97_idx;
            GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, GXCCtl);
            AnyError = 1;
            GX_FocusControl = edtContratoServicosPrazoRegra_Fim_Internalname;
            wbErr = true;
            A908ContratoServicosPrazoRegra_Fim = 0;
         }
         else
         {
            A908ContratoServicosPrazoRegra_Fim = context.localUtil.CToN( cgiGet( edtContratoServicosPrazoRegra_Fim_Internalname), ",", ".");
         }
         if ( ( ( context.localUtil.CToN( cgiGet( edtContratoServicosPrazoRegra_Dias_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContratoServicosPrazoRegra_Dias_Internalname), ",", ".") > Convert.ToDecimal( 999 )) ) )
         {
            GXCCtl = "CONTRATOSERVICOSPRAZOREGRA_DIAS_" + sGXsfl_97_idx;
            GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, GXCCtl);
            AnyError = 1;
            GX_FocusControl = edtContratoServicosPrazoRegra_Dias_Internalname;
            wbErr = true;
            A909ContratoServicosPrazoRegra_Dias = 0;
         }
         else
         {
            A909ContratoServicosPrazoRegra_Dias = (short)(context.localUtil.CToN( cgiGet( edtContratoServicosPrazoRegra_Dias_Internalname), ",", "."));
         }
         GXCCtl = "Z906ContratoServicosPrazoRegra_Sequencial_" + sGXsfl_97_idx;
         Z906ContratoServicosPrazoRegra_Sequencial = (short)(context.localUtil.CToN( cgiGet( GXCCtl), ",", "."));
         GXCCtl = "Z907ContratoServicosPrazoRegra_Inicio_" + sGXsfl_97_idx;
         Z907ContratoServicosPrazoRegra_Inicio = context.localUtil.CToN( cgiGet( GXCCtl), ",", ".");
         GXCCtl = "Z908ContratoServicosPrazoRegra_Fim_" + sGXsfl_97_idx;
         Z908ContratoServicosPrazoRegra_Fim = context.localUtil.CToN( cgiGet( GXCCtl), ",", ".");
         GXCCtl = "Z909ContratoServicosPrazoRegra_Dias_" + sGXsfl_97_idx;
         Z909ContratoServicosPrazoRegra_Dias = (short)(context.localUtil.CToN( cgiGet( GXCCtl), ",", "."));
         GXCCtl = "nRcdDeleted_113_" + sGXsfl_97_idx;
         nRcdDeleted_113 = (short)(context.localUtil.CToN( cgiGet( GXCCtl), ",", "."));
         GXCCtl = "nRcdExists_113_" + sGXsfl_97_idx;
         nRcdExists_113 = (short)(context.localUtil.CToN( cgiGet( GXCCtl), ",", "."));
         GXCCtl = "nIsMod_113_" + sGXsfl_97_idx;
         nIsMod_113 = (short)(context.localUtil.CToN( cgiGet( GXCCtl), ",", "."));
      }

      protected void SubsflControlProps_112188( )
      {
         edtContratoServicosPrazoPlnj_Sequencial_Internalname = "CONTRATOSERVICOSPRAZOPLNJ_SEQUENCIAL_"+sGXsfl_112_idx;
         edtContratoServicosPrazoPlnj_Inicio_Internalname = "CONTRATOSERVICOSPRAZOPLNJ_INICIO_"+sGXsfl_112_idx;
         edtContratoServicosPrazoPlnj_Fim_Internalname = "CONTRATOSERVICOSPRAZOPLNJ_FIM_"+sGXsfl_112_idx;
         edtContratoServicosPrazoPlnj_Dias_Internalname = "CONTRATOSERVICOSPRAZOPLNJ_DIAS_"+sGXsfl_112_idx;
      }

      protected void SubsflControlProps_fel_112188( )
      {
         edtContratoServicosPrazoPlnj_Sequencial_Internalname = "CONTRATOSERVICOSPRAZOPLNJ_SEQUENCIAL_"+sGXsfl_112_fel_idx;
         edtContratoServicosPrazoPlnj_Inicio_Internalname = "CONTRATOSERVICOSPRAZOPLNJ_INICIO_"+sGXsfl_112_fel_idx;
         edtContratoServicosPrazoPlnj_Fim_Internalname = "CONTRATOSERVICOSPRAZOPLNJ_FIM_"+sGXsfl_112_fel_idx;
         edtContratoServicosPrazoPlnj_Dias_Internalname = "CONTRATOSERVICOSPRAZOPLNJ_DIAS_"+sGXsfl_112_fel_idx;
      }

      protected void AddRow2U188( )
      {
         nGXsfl_112_idx = (short)(nGXsfl_112_idx+1);
         sGXsfl_112_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_112_idx), 4, 0)), 4, "0");
         SubsflControlProps_112188( ) ;
         SendRow2U188( ) ;
      }

      protected void SendRow2U188( )
      {
         Grid2Row = GXWebRow.GetNew(context);
         if ( subGrid2_Backcolorstyle == 0 )
         {
            /* None style subfile background logic. */
            subGrid2_Backstyle = 0;
            if ( StringUtil.StrCmp(subGrid2_Class, "") != 0 )
            {
               subGrid2_Linesclass = subGrid2_Class+"Odd";
            }
         }
         else if ( subGrid2_Backcolorstyle == 1 )
         {
            /* Uniform style subfile background logic. */
            subGrid2_Backstyle = 0;
            subGrid2_Backcolor = subGrid2_Allbackcolor;
            if ( StringUtil.StrCmp(subGrid2_Class, "") != 0 )
            {
               subGrid2_Linesclass = subGrid2_Class+"Uniform";
            }
         }
         else if ( subGrid2_Backcolorstyle == 2 )
         {
            /* Header style subfile background logic. */
            subGrid2_Backstyle = 1;
            if ( StringUtil.StrCmp(subGrid2_Class, "") != 0 )
            {
               subGrid2_Linesclass = subGrid2_Class+"Odd";
            }
            subGrid2_Backcolor = (int)(0xFFFFFF);
         }
         else if ( subGrid2_Backcolorstyle == 3 )
         {
            /* Report style subfile background logic. */
            subGrid2_Backstyle = 1;
            if ( ((int)((nGXsfl_112_idx) % (2))) == 0 )
            {
               subGrid2_Backcolor = (int)(0xFFFFFF);
               if ( StringUtil.StrCmp(subGrid2_Class, "") != 0 )
               {
                  subGrid2_Linesclass = subGrid2_Class+"Even";
               }
            }
            else
            {
               subGrid2_Backcolor = (int)(0xFFFFFF);
               if ( StringUtil.StrCmp(subGrid2_Class, "") != 0 )
               {
                  subGrid2_Linesclass = subGrid2_Class+"Odd";
               }
            }
         }
         /* Subfile cell */
         /* Single line edit */
         ROClassString = "BootstrapAttribute";
         Grid2Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoServicosPrazoPlnj_Sequencial_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1724ContratoServicosPrazoPlnj_Sequencial), 3, 0, ",", "")),((edtContratoServicosPrazoPlnj_Sequencial_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1724ContratoServicosPrazoPlnj_Sequencial), "ZZ9")) : context.localUtil.Format( (decimal)(A1724ContratoServicosPrazoPlnj_Sequencial), "ZZ9")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoServicosPrazoPlnj_Sequencial_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(int)edtContratoServicosPrazoPlnj_Sequencial_Enabled,(short)0,(String)"text",(String)"",(short)25,(String)"px",(short)17,(String)"px",(short)3,(short)0,(short)0,(short)112,(short)1,(short)-1,(short)0,(bool)true,(String)"Sequencial",(String)"right",(bool)false});
         /* Subfile cell */
         /* Single line edit */
         TempTags = " gxoch1=\"gx.fn.setControlValue('nIsMod_188_" + sGXsfl_112_idx + "',1);\"  onfocus=\"gx.evt.onfocus(this, 114,'',false,'" + sGXsfl_112_idx + "',112)\"";
         ROClassString = "BootstrapAttribute";
         Grid2Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoServicosPrazoPlnj_Inicio_Internalname,StringUtil.LTrim( StringUtil.NToC( A1734ContratoServicosPrazoPlnj_Inicio, 14, 5, ",", "")),((edtContratoServicosPrazoPlnj_Inicio_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A1734ContratoServicosPrazoPlnj_Inicio, "ZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( A1734ContratoServicosPrazoPlnj_Inicio, "ZZ,ZZZ,ZZ9.999")),TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,114);\"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoServicosPrazoPlnj_Inicio_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtContratoServicosPrazoPlnj_Inicio_Enabled,(short)0,(String)"text",(String)"",(short)94,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)112,(short)1,(short)-1,(short)0,(bool)true,(String)"PontosDeFuncao",(String)"right",(bool)false});
         /* Subfile cell */
         /* Single line edit */
         TempTags = " gxoch1=\"gx.fn.setControlValue('nIsMod_188_" + sGXsfl_112_idx + "',1);\"  onfocus=\"gx.evt.onfocus(this, 115,'',false,'" + sGXsfl_112_idx + "',112)\"";
         ROClassString = "BootstrapAttribute";
         Grid2Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoServicosPrazoPlnj_Fim_Internalname,StringUtil.LTrim( StringUtil.NToC( A1735ContratoServicosPrazoPlnj_Fim, 14, 5, ",", "")),((edtContratoServicosPrazoPlnj_Fim_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A1735ContratoServicosPrazoPlnj_Fim, "ZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( A1735ContratoServicosPrazoPlnj_Fim, "ZZ,ZZZ,ZZ9.999")),TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,115);\"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoServicosPrazoPlnj_Fim_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtContratoServicosPrazoPlnj_Fim_Enabled,(short)0,(String)"text",(String)"",(short)94,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)112,(short)1,(short)-1,(short)0,(bool)true,(String)"PontosDeFuncao",(String)"right",(bool)false});
         /* Subfile cell */
         /* Single line edit */
         TempTags = " gxoch1=\"gx.fn.setControlValue('nIsMod_188_" + sGXsfl_112_idx + "',1);\"  onfocus=\"gx.evt.onfocus(this, 116,'',false,'" + sGXsfl_112_idx + "',112)\"";
         ROClassString = "BootstrapAttribute";
         Grid2Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoServicosPrazoPlnj_Dias_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1736ContratoServicosPrazoPlnj_Dias), 4, 0, ",", "")),((edtContratoServicosPrazoPlnj_Dias_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1736ContratoServicosPrazoPlnj_Dias), "ZZZ9")) : context.localUtil.Format( (decimal)(A1736ContratoServicosPrazoPlnj_Dias), "ZZZ9")),TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,116);\"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoServicosPrazoPlnj_Dias_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtContratoServicosPrazoPlnj_Dias_Enabled,(short)0,(String)"text",(String)"",(short)34,(String)"px",(short)17,(String)"px",(short)4,(short)0,(short)0,(short)112,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
         context.httpAjaxContext.ajax_sending_grid_row(Grid2Row);
         GXCCtl = "Z1724ContratoServicosPrazoPlnj_Sequencial_" + sGXsfl_112_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1724ContratoServicosPrazoPlnj_Sequencial), 3, 0, ",", "")));
         GXCCtl = "Z1734ContratoServicosPrazoPlnj_Inicio_" + sGXsfl_112_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( Z1734ContratoServicosPrazoPlnj_Inicio, 14, 5, ",", "")));
         GXCCtl = "Z1735ContratoServicosPrazoPlnj_Fim_" + sGXsfl_112_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( Z1735ContratoServicosPrazoPlnj_Fim, 14, 5, ",", "")));
         GXCCtl = "Z1736ContratoServicosPrazoPlnj_Dias_" + sGXsfl_112_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1736ContratoServicosPrazoPlnj_Dias), 4, 0, ",", "")));
         GXCCtl = "nRcdDeleted_188_" + sGXsfl_112_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdDeleted_188), 4, 0, ",", "")));
         GXCCtl = "nRcdExists_188_" + sGXsfl_112_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdExists_188), 4, 0, ",", "")));
         GXCCtl = "nIsMod_188_" + sGXsfl_112_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(nIsMod_188), 4, 0, ",", "")));
         GXCCtl = "vAUDITINGOBJECT_" + sGXsfl_112_idx;
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, GXCCtl, AV12AuditingObject);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(GXCCtl, AV12AuditingObject);
         }
         GXCCtl = "vPGMNAME_" + sGXsfl_112_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.RTrim( AV14Pgmname));
         GXCCtl = "vMODE_" + sGXsfl_112_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.RTrim( Gx_mode));
         GXCCtl = "vTRNCONTEXT_" + sGXsfl_112_idx;
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, GXCCtl, AV9TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(GXCCtl, AV9TrnContext);
         }
         GXCCtl = "vCONTRATOSERVICOSPRAZO_CNTSRVCOD_" + sGXsfl_112_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7ContratoServicosPrazo_CntSrvCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOSPRAZOPLNJ_SEQUENCIAL_"+sGXsfl_112_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosPrazoPlnj_Sequencial_Enabled), 5, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOSPRAZOPLNJ_INICIO_"+sGXsfl_112_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosPrazoPlnj_Inicio_Enabled), 5, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOSPRAZOPLNJ_FIM_"+sGXsfl_112_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosPrazoPlnj_Fim_Enabled), 5, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOSPRAZOPLNJ_DIAS_"+sGXsfl_112_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosPrazoPlnj_Dias_Enabled), 5, 0, ".", "")));
         context.httpAjaxContext.ajax_sending_grid_row(null);
         Grid2Container.AddRow(Grid2Row);
      }

      protected void ReadRow2U188( )
      {
         nGXsfl_112_idx = (short)(nGXsfl_112_idx+1);
         sGXsfl_112_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_112_idx), 4, 0)), 4, "0");
         SubsflControlProps_112188( ) ;
         edtContratoServicosPrazoPlnj_Sequencial_Enabled = (int)(context.localUtil.CToN( cgiGet( "CONTRATOSERVICOSPRAZOPLNJ_SEQUENCIAL_"+sGXsfl_112_idx+"Enabled"), ",", "."));
         edtContratoServicosPrazoPlnj_Inicio_Enabled = (int)(context.localUtil.CToN( cgiGet( "CONTRATOSERVICOSPRAZOPLNJ_INICIO_"+sGXsfl_112_idx+"Enabled"), ",", "."));
         edtContratoServicosPrazoPlnj_Fim_Enabled = (int)(context.localUtil.CToN( cgiGet( "CONTRATOSERVICOSPRAZOPLNJ_FIM_"+sGXsfl_112_idx+"Enabled"), ",", "."));
         edtContratoServicosPrazoPlnj_Dias_Enabled = (int)(context.localUtil.CToN( cgiGet( "CONTRATOSERVICOSPRAZOPLNJ_DIAS_"+sGXsfl_112_idx+"Enabled"), ",", "."));
         A1724ContratoServicosPrazoPlnj_Sequencial = (short)(context.localUtil.CToN( cgiGet( edtContratoServicosPrazoPlnj_Sequencial_Internalname), ",", "."));
         if ( ( ( context.localUtil.CToN( cgiGet( edtContratoServicosPrazoPlnj_Inicio_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContratoServicosPrazoPlnj_Inicio_Internalname), ",", ".") > 99999999.99999m ) ) )
         {
            GXCCtl = "CONTRATOSERVICOSPRAZOPLNJ_INICIO_" + sGXsfl_112_idx;
            GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, GXCCtl);
            AnyError = 1;
            GX_FocusControl = edtContratoServicosPrazoPlnj_Inicio_Internalname;
            wbErr = true;
            A1734ContratoServicosPrazoPlnj_Inicio = 0;
         }
         else
         {
            A1734ContratoServicosPrazoPlnj_Inicio = context.localUtil.CToN( cgiGet( edtContratoServicosPrazoPlnj_Inicio_Internalname), ",", ".");
         }
         if ( ( ( context.localUtil.CToN( cgiGet( edtContratoServicosPrazoPlnj_Fim_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContratoServicosPrazoPlnj_Fim_Internalname), ",", ".") > 99999999.99999m ) ) )
         {
            GXCCtl = "CONTRATOSERVICOSPRAZOPLNJ_FIM_" + sGXsfl_112_idx;
            GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, GXCCtl);
            AnyError = 1;
            GX_FocusControl = edtContratoServicosPrazoPlnj_Fim_Internalname;
            wbErr = true;
            A1735ContratoServicosPrazoPlnj_Fim = 0;
         }
         else
         {
            A1735ContratoServicosPrazoPlnj_Fim = context.localUtil.CToN( cgiGet( edtContratoServicosPrazoPlnj_Fim_Internalname), ",", ".");
         }
         if ( ( ( context.localUtil.CToN( cgiGet( edtContratoServicosPrazoPlnj_Dias_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContratoServicosPrazoPlnj_Dias_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
         {
            GXCCtl = "CONTRATOSERVICOSPRAZOPLNJ_DIAS_" + sGXsfl_112_idx;
            GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, GXCCtl);
            AnyError = 1;
            GX_FocusControl = edtContratoServicosPrazoPlnj_Dias_Internalname;
            wbErr = true;
            A1736ContratoServicosPrazoPlnj_Dias = 0;
         }
         else
         {
            A1736ContratoServicosPrazoPlnj_Dias = (short)(context.localUtil.CToN( cgiGet( edtContratoServicosPrazoPlnj_Dias_Internalname), ",", "."));
         }
         GXCCtl = "Z1724ContratoServicosPrazoPlnj_Sequencial_" + sGXsfl_112_idx;
         Z1724ContratoServicosPrazoPlnj_Sequencial = (short)(context.localUtil.CToN( cgiGet( GXCCtl), ",", "."));
         GXCCtl = "Z1734ContratoServicosPrazoPlnj_Inicio_" + sGXsfl_112_idx;
         Z1734ContratoServicosPrazoPlnj_Inicio = context.localUtil.CToN( cgiGet( GXCCtl), ",", ".");
         GXCCtl = "Z1735ContratoServicosPrazoPlnj_Fim_" + sGXsfl_112_idx;
         Z1735ContratoServicosPrazoPlnj_Fim = context.localUtil.CToN( cgiGet( GXCCtl), ",", ".");
         GXCCtl = "Z1736ContratoServicosPrazoPlnj_Dias_" + sGXsfl_112_idx;
         Z1736ContratoServicosPrazoPlnj_Dias = (short)(context.localUtil.CToN( cgiGet( GXCCtl), ",", "."));
         GXCCtl = "nRcdDeleted_188_" + sGXsfl_112_idx;
         nRcdDeleted_188 = (short)(context.localUtil.CToN( cgiGet( GXCCtl), ",", "."));
         GXCCtl = "nRcdExists_188_" + sGXsfl_112_idx;
         nRcdExists_188 = (short)(context.localUtil.CToN( cgiGet( GXCCtl), ",", "."));
         GXCCtl = "nIsMod_188_" + sGXsfl_112_idx;
         nIsMod_188 = (short)(context.localUtil.CToN( cgiGet( GXCCtl), ",", "."));
      }

      protected void assign_properties_default( )
      {
         defedtContratoServicosPrazoRegra_Sequencial_Enabled = edtContratoServicosPrazoRegra_Sequencial_Enabled;
         defedtContratoServicosPrazoPlnj_Sequencial_Enabled = edtContratoServicosPrazoPlnj_Sequencial_Enabled;
      }

      protected void ConfirmValues2U0( )
      {
         nGXsfl_97_idx = 0;
         sGXsfl_97_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_97_idx), 4, 0)), 4, "0");
         SubsflControlProps_97113( ) ;
         while ( nGXsfl_97_idx < nRC_GXsfl_97 )
         {
            nGXsfl_97_idx = (short)(nGXsfl_97_idx+1);
            sGXsfl_97_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_97_idx), 4, 0)), 4, "0");
            SubsflControlProps_97113( ) ;
            ChangePostValue( "Z906ContratoServicosPrazoRegra_Sequencial_"+sGXsfl_97_idx, cgiGet( "ZT_"+"Z906ContratoServicosPrazoRegra_Sequencial_"+sGXsfl_97_idx)) ;
            DeletePostValue( "ZT_"+"Z906ContratoServicosPrazoRegra_Sequencial_"+sGXsfl_97_idx) ;
            ChangePostValue( "Z907ContratoServicosPrazoRegra_Inicio_"+sGXsfl_97_idx, cgiGet( "ZT_"+"Z907ContratoServicosPrazoRegra_Inicio_"+sGXsfl_97_idx)) ;
            DeletePostValue( "ZT_"+"Z907ContratoServicosPrazoRegra_Inicio_"+sGXsfl_97_idx) ;
            ChangePostValue( "Z908ContratoServicosPrazoRegra_Fim_"+sGXsfl_97_idx, cgiGet( "ZT_"+"Z908ContratoServicosPrazoRegra_Fim_"+sGXsfl_97_idx)) ;
            DeletePostValue( "ZT_"+"Z908ContratoServicosPrazoRegra_Fim_"+sGXsfl_97_idx) ;
            ChangePostValue( "Z909ContratoServicosPrazoRegra_Dias_"+sGXsfl_97_idx, cgiGet( "ZT_"+"Z909ContratoServicosPrazoRegra_Dias_"+sGXsfl_97_idx)) ;
            DeletePostValue( "ZT_"+"Z909ContratoServicosPrazoRegra_Dias_"+sGXsfl_97_idx) ;
         }
         nGXsfl_112_idx = 0;
         sGXsfl_112_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_112_idx), 4, 0)), 4, "0");
         SubsflControlProps_112188( ) ;
         while ( nGXsfl_112_idx < nRC_GXsfl_112 )
         {
            nGXsfl_112_idx = (short)(nGXsfl_112_idx+1);
            sGXsfl_112_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_112_idx), 4, 0)), 4, "0");
            SubsflControlProps_112188( ) ;
            ChangePostValue( "Z1724ContratoServicosPrazoPlnj_Sequencial_"+sGXsfl_112_idx, cgiGet( "ZT_"+"Z1724ContratoServicosPrazoPlnj_Sequencial_"+sGXsfl_112_idx)) ;
            DeletePostValue( "ZT_"+"Z1724ContratoServicosPrazoPlnj_Sequencial_"+sGXsfl_112_idx) ;
            ChangePostValue( "Z1734ContratoServicosPrazoPlnj_Inicio_"+sGXsfl_112_idx, cgiGet( "ZT_"+"Z1734ContratoServicosPrazoPlnj_Inicio_"+sGXsfl_112_idx)) ;
            DeletePostValue( "ZT_"+"Z1734ContratoServicosPrazoPlnj_Inicio_"+sGXsfl_112_idx) ;
            ChangePostValue( "Z1735ContratoServicosPrazoPlnj_Fim_"+sGXsfl_112_idx, cgiGet( "ZT_"+"Z1735ContratoServicosPrazoPlnj_Fim_"+sGXsfl_112_idx)) ;
            DeletePostValue( "ZT_"+"Z1735ContratoServicosPrazoPlnj_Fim_"+sGXsfl_112_idx) ;
            ChangePostValue( "Z1736ContratoServicosPrazoPlnj_Dias_"+sGXsfl_112_idx, cgiGet( "ZT_"+"Z1736ContratoServicosPrazoPlnj_Dias_"+sGXsfl_112_idx)) ;
            DeletePostValue( "ZT_"+"Z1736ContratoServicosPrazoPlnj_Dias_"+sGXsfl_112_idx) ;
         }
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20205299303673");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("contratoservicosprazo.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7ContratoServicosPrazo_CntSrvCod)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z903ContratoServicosPrazo_CntSrvCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z903ContratoServicosPrazo_CntSrvCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z905ContratoServicosPrazo_Dias", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z905ContratoServicosPrazo_Dias), 3, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z904ContratoServicosPrazo_Tipo", StringUtil.RTrim( Z904ContratoServicosPrazo_Tipo));
         GxWebStd.gx_hidden_field( context, "Z1456ContratoServicosPrazo_Cada", StringUtil.LTrim( StringUtil.NToC( Z1456ContratoServicosPrazo_Cada, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1263ContratoServicosPrazo_DiasAlta", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1263ContratoServicosPrazo_DiasAlta), 3, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1264ContratoServicosPrazo_DiasMedia", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1264ContratoServicosPrazo_DiasMedia), 3, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1265ContratoServicosPrazo_DiasBaixa", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1265ContratoServicosPrazo_DiasBaixa), 3, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1823ContratoServicosPrazo_Momento", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1823ContratoServicosPrazo_Momento), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "O1737ContratoServicosPrazo_QtdPlnj", StringUtil.LTrim( StringUtil.NToC( (decimal)(O1737ContratoServicosPrazo_QtdPlnj), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "O910ContratoServicosPrazo_QtdRegras", StringUtil.LTrim( StringUtil.NToC( (decimal)(O910ContratoServicosPrazo_QtdRegras), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_97", StringUtil.LTrim( StringUtil.NToC( (decimal)(nGXsfl_97_idx), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_112", StringUtil.LTrim( StringUtil.NToC( (decimal)(nGXsfl_112_idx), 4, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV9TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV9TrnContext);
         }
         GxWebStd.gx_hidden_field( context, "vCONTRATOSERVICOSPRAZO_CNTSRVCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7ContratoServicosPrazo_CntSrvCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOSPRAZO_SERVICOSIGLA", StringUtil.RTrim( A1096ContratoServicosPrazo_ServicoSigla));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOSPRAZO_QTDREGRAS", StringUtil.LTrim( StringUtil.NToC( (decimal)(A910ContratoServicosPrazo_QtdRegras), 4, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vAUDITINGOBJECT", AV12AuditingObject);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vAUDITINGOBJECT", AV12AuditingObject);
         }
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOS_UNIDADECONTRATADA", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1212ContratoServicos_UnidadeContratada), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOSPRAZO_SERVICOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1095ContratoServicosPrazo_ServicoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOSPRAZO_QTDPLNJ", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1737ContratoServicosPrazo_QtdPlnj), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV14Pgmname));
         GxWebStd.gx_hidden_field( context, "vGXBSCREEN", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gx_BScreen), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTRATOSERVICOSPRAZO_CNTSRVCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7ContratoServicosPrazo_CntSrvCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Width", StringUtil.RTrim( Dvpanel_tableattributes_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Cls", StringUtil.RTrim( Dvpanel_tableattributes_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Title", StringUtil.RTrim( Dvpanel_tableattributes_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsible", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsed", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Enabled", StringUtil.BoolToStr( Dvpanel_tableattributes_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autowidth", StringUtil.BoolToStr( Dvpanel_tableattributes_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoheight", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tableattributes_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Iconposition", StringUtil.RTrim( Dvpanel_tableattributes_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoscroll", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoscroll));
         GxWebStd.gx_hidden_field( context, "DVPANEL_COMPLEXIDADE_Width", StringUtil.RTrim( Dvpanel_complexidade_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_COMPLEXIDADE_Cls", StringUtil.RTrim( Dvpanel_complexidade_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_COMPLEXIDADE_Title", StringUtil.RTrim( Dvpanel_complexidade_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_COMPLEXIDADE_Collapsible", StringUtil.BoolToStr( Dvpanel_complexidade_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_COMPLEXIDADE_Collapsed", StringUtil.BoolToStr( Dvpanel_complexidade_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_COMPLEXIDADE_Enabled", StringUtil.BoolToStr( Dvpanel_complexidade_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_COMPLEXIDADE_Autowidth", StringUtil.BoolToStr( Dvpanel_complexidade_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_COMPLEXIDADE_Autoheight", StringUtil.BoolToStr( Dvpanel_complexidade_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_COMPLEXIDADE_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_complexidade_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_COMPLEXIDADE_Iconposition", StringUtil.RTrim( Dvpanel_complexidade_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_COMPLEXIDADE_Autoscroll", StringUtil.BoolToStr( Dvpanel_complexidade_Autoscroll));
         GxWebStd.gx_hidden_field( context, "DVPANEL_REGRAS_Width", StringUtil.RTrim( Dvpanel_regras_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_REGRAS_Cls", StringUtil.RTrim( Dvpanel_regras_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_REGRAS_Title", StringUtil.RTrim( Dvpanel_regras_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_REGRAS_Collapsible", StringUtil.BoolToStr( Dvpanel_regras_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_REGRAS_Collapsed", StringUtil.BoolToStr( Dvpanel_regras_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_REGRAS_Enabled", StringUtil.BoolToStr( Dvpanel_regras_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_REGRAS_Autowidth", StringUtil.BoolToStr( Dvpanel_regras_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_REGRAS_Autoheight", StringUtil.BoolToStr( Dvpanel_regras_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_REGRAS_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_regras_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_REGRAS_Iconposition", StringUtil.RTrim( Dvpanel_regras_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_REGRAS_Autoscroll", StringUtil.BoolToStr( Dvpanel_regras_Autoscroll));
         GxWebStd.gx_hidden_field( context, "DVPANEL_PLANEJAMENTO_Width", StringUtil.RTrim( Dvpanel_planejamento_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_PLANEJAMENTO_Cls", StringUtil.RTrim( Dvpanel_planejamento_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_PLANEJAMENTO_Title", StringUtil.RTrim( Dvpanel_planejamento_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_PLANEJAMENTO_Collapsible", StringUtil.BoolToStr( Dvpanel_planejamento_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_PLANEJAMENTO_Collapsed", StringUtil.BoolToStr( Dvpanel_planejamento_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_PLANEJAMENTO_Enabled", StringUtil.BoolToStr( Dvpanel_planejamento_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_PLANEJAMENTO_Autowidth", StringUtil.BoolToStr( Dvpanel_planejamento_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_PLANEJAMENTO_Autoheight", StringUtil.BoolToStr( Dvpanel_planejamento_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_PLANEJAMENTO_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_planejamento_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_PLANEJAMENTO_Iconposition", StringUtil.RTrim( Dvpanel_planejamento_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_PLANEJAMENTO_Autoscroll", StringUtil.BoolToStr( Dvpanel_planejamento_Autoscroll));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "ContratoServicosPrazo";
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( AV14Pgmname, ""));
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("contratoservicosprazo:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
         GXUtil.WriteLog("contratoservicosprazo:[SendSecurityCheck value for]"+"Pgmname:"+StringUtil.RTrim( context.localUtil.Format( AV14Pgmname, "")));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("contratoservicosprazo.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7ContratoServicosPrazo_CntSrvCod) ;
      }

      public override String GetPgmname( )
      {
         return "ContratoServicosPrazo" ;
      }

      public override String GetPgmdesc( )
      {
         return "Prazo" ;
      }

      protected void InitializeNonKey2U112( )
      {
         A1212ContratoServicos_UnidadeContratada = 0;
         n1212ContratoServicos_UnidadeContratada = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1212ContratoServicos_UnidadeContratada", StringUtil.LTrim( StringUtil.Str( (decimal)(A1212ContratoServicos_UnidadeContratada), 6, 0)));
         AV12AuditingObject = new wwpbaseobjects.SdtAuditingObject(context);
         AV11Servico_Sigla = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Servico_Sigla", AV11Servico_Sigla);
         A905ContratoServicosPrazo_Dias = 0;
         n905ContratoServicosPrazo_Dias = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A905ContratoServicosPrazo_Dias", StringUtil.LTrim( StringUtil.Str( (decimal)(A905ContratoServicosPrazo_Dias), 3, 0)));
         n905ContratoServicosPrazo_Dias = ((0==A905ContratoServicosPrazo_Dias) ? true : false);
         A904ContratoServicosPrazo_Tipo = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A904ContratoServicosPrazo_Tipo", A904ContratoServicosPrazo_Tipo);
         A1456ContratoServicosPrazo_Cada = 0;
         n1456ContratoServicosPrazo_Cada = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1456ContratoServicosPrazo_Cada", StringUtil.LTrim( StringUtil.Str( A1456ContratoServicosPrazo_Cada, 14, 5)));
         n1456ContratoServicosPrazo_Cada = ((Convert.ToDecimal(0)==A1456ContratoServicosPrazo_Cada) ? true : false);
         A1712ContratoServicos_UndCntSgl = "";
         n1712ContratoServicos_UndCntSgl = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1712ContratoServicos_UndCntSgl", A1712ContratoServicos_UndCntSgl);
         A910ContratoServicosPrazo_QtdRegras = 0;
         n910ContratoServicosPrazo_QtdRegras = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A910ContratoServicosPrazo_QtdRegras", StringUtil.LTrim( StringUtil.Str( (decimal)(A910ContratoServicosPrazo_QtdRegras), 4, 0)));
         A1737ContratoServicosPrazo_QtdPlnj = 0;
         n1737ContratoServicosPrazo_QtdPlnj = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1737ContratoServicosPrazo_QtdPlnj", StringUtil.LTrim( StringUtil.Str( (decimal)(A1737ContratoServicosPrazo_QtdPlnj), 4, 0)));
         A1095ContratoServicosPrazo_ServicoCod = 0;
         n1095ContratoServicosPrazo_ServicoCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1095ContratoServicosPrazo_ServicoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1095ContratoServicosPrazo_ServicoCod), 6, 0)));
         A1096ContratoServicosPrazo_ServicoSigla = "";
         n1096ContratoServicosPrazo_ServicoSigla = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1096ContratoServicosPrazo_ServicoSigla", A1096ContratoServicosPrazo_ServicoSigla);
         A1263ContratoServicosPrazo_DiasAlta = 0;
         n1263ContratoServicosPrazo_DiasAlta = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1263ContratoServicosPrazo_DiasAlta", StringUtil.LTrim( StringUtil.Str( (decimal)(A1263ContratoServicosPrazo_DiasAlta), 3, 0)));
         n1263ContratoServicosPrazo_DiasAlta = ((0==A1263ContratoServicosPrazo_DiasAlta) ? true : false);
         A1264ContratoServicosPrazo_DiasMedia = 0;
         n1264ContratoServicosPrazo_DiasMedia = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1264ContratoServicosPrazo_DiasMedia", StringUtil.LTrim( StringUtil.Str( (decimal)(A1264ContratoServicosPrazo_DiasMedia), 3, 0)));
         n1264ContratoServicosPrazo_DiasMedia = ((0==A1264ContratoServicosPrazo_DiasMedia) ? true : false);
         A1265ContratoServicosPrazo_DiasBaixa = 0;
         n1265ContratoServicosPrazo_DiasBaixa = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1265ContratoServicosPrazo_DiasBaixa", StringUtil.LTrim( StringUtil.Str( (decimal)(A1265ContratoServicosPrazo_DiasBaixa), 3, 0)));
         n1265ContratoServicosPrazo_DiasBaixa = ((0==A1265ContratoServicosPrazo_DiasBaixa) ? true : false);
         A1823ContratoServicosPrazo_Momento = 0;
         n1823ContratoServicosPrazo_Momento = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1823ContratoServicosPrazo_Momento", StringUtil.LTrim( StringUtil.Str( (decimal)(A1823ContratoServicosPrazo_Momento), 4, 0)));
         n1823ContratoServicosPrazo_Momento = ((0==A1823ContratoServicosPrazo_Momento) ? true : false);
         O1737ContratoServicosPrazo_QtdPlnj = A1737ContratoServicosPrazo_QtdPlnj;
         n1737ContratoServicosPrazo_QtdPlnj = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1737ContratoServicosPrazo_QtdPlnj", StringUtil.LTrim( StringUtil.Str( (decimal)(A1737ContratoServicosPrazo_QtdPlnj), 4, 0)));
         O910ContratoServicosPrazo_QtdRegras = A910ContratoServicosPrazo_QtdRegras;
         n910ContratoServicosPrazo_QtdRegras = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A910ContratoServicosPrazo_QtdRegras", StringUtil.LTrim( StringUtil.Str( (decimal)(A910ContratoServicosPrazo_QtdRegras), 4, 0)));
         Z905ContratoServicosPrazo_Dias = 0;
         Z904ContratoServicosPrazo_Tipo = "";
         Z1456ContratoServicosPrazo_Cada = 0;
         Z1263ContratoServicosPrazo_DiasAlta = 0;
         Z1264ContratoServicosPrazo_DiasMedia = 0;
         Z1265ContratoServicosPrazo_DiasBaixa = 0;
         Z1823ContratoServicosPrazo_Momento = 0;
      }

      protected void InitAll2U112( )
      {
         A903ContratoServicosPrazo_CntSrvCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A903ContratoServicosPrazo_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A903ContratoServicosPrazo_CntSrvCod), 6, 0)));
         InitializeNonKey2U112( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void InitializeNonKey2U113( )
      {
         A907ContratoServicosPrazoRegra_Inicio = 0;
         A908ContratoServicosPrazoRegra_Fim = 0;
         A909ContratoServicosPrazoRegra_Dias = 0;
         Z907ContratoServicosPrazoRegra_Inicio = 0;
         Z908ContratoServicosPrazoRegra_Fim = 0;
         Z909ContratoServicosPrazoRegra_Dias = 0;
      }

      protected void InitAll2U113( )
      {
         A906ContratoServicosPrazoRegra_Sequencial = 0;
         InitializeNonKey2U113( ) ;
      }

      protected void StandaloneModalInsert2U113( )
      {
         A910ContratoServicosPrazo_QtdRegras = i910ContratoServicosPrazo_QtdRegras;
         n910ContratoServicosPrazo_QtdRegras = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A910ContratoServicosPrazo_QtdRegras", StringUtil.LTrim( StringUtil.Str( (decimal)(A910ContratoServicosPrazo_QtdRegras), 4, 0)));
      }

      protected void InitializeNonKey2U188( )
      {
         A1734ContratoServicosPrazoPlnj_Inicio = 0;
         A1735ContratoServicosPrazoPlnj_Fim = 0;
         A1736ContratoServicosPrazoPlnj_Dias = 0;
         Z1734ContratoServicosPrazoPlnj_Inicio = 0;
         Z1735ContratoServicosPrazoPlnj_Fim = 0;
         Z1736ContratoServicosPrazoPlnj_Dias = 0;
      }

      protected void InitAll2U188( )
      {
         A1724ContratoServicosPrazoPlnj_Sequencial = 0;
         InitializeNonKey2U188( ) ;
      }

      protected void StandaloneModalInsert2U188( )
      {
         A1737ContratoServicosPrazo_QtdPlnj = i1737ContratoServicosPrazo_QtdPlnj;
         n1737ContratoServicosPrazo_QtdPlnj = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1737ContratoServicosPrazo_QtdPlnj", StringUtil.LTrim( StringUtil.Str( (decimal)(A1737ContratoServicosPrazo_QtdPlnj), 4, 0)));
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20205299303763");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("contratoservicosprazo.js", "?20205299303763");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_level_properties113( )
      {
         edtContratoServicosPrazoRegra_Sequencial_Enabled = defedtContratoServicosPrazoRegra_Sequencial_Enabled;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosPrazoRegra_Sequencial_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosPrazoRegra_Sequencial_Enabled), 5, 0)));
      }

      protected void init_level_properties188( )
      {
         edtContratoServicosPrazoPlnj_Sequencial_Enabled = defedtContratoServicosPrazoPlnj_Sequencial_Enabled;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosPrazoPlnj_Sequencial_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosPrazoPlnj_Sequencial_Enabled), 5, 0)));
      }

      protected void init_default_properties( )
      {
         lblViewtitle_Internalname = "VIEWTITLE";
         edtavServico_sigla_Internalname = "vSERVICO_SIGLA";
         tblTablemergedviewtitle_Internalname = "TABLEMERGEDVIEWTITLE";
         lblTextblockcontratoservicosprazo_tipo_Internalname = "TEXTBLOCKCONTRATOSERVICOSPRAZO_TIPO";
         cmbContratoServicosPrazo_Tipo_Internalname = "CONTRATOSERVICOSPRAZO_TIPO";
         lblTextblockcontratoservicosprazo_dias_Internalname = "TEXTBLOCKCONTRATOSERVICOSPRAZO_DIAS";
         edtContratoServicosPrazo_Dias_Internalname = "CONTRATOSERVICOSPRAZO_DIAS";
         lblTextblockcontratoservicosprazo_momento_Internalname = "TEXTBLOCKCONTRATOSERVICOSPRAZO_MOMENTO";
         cmbContratoServicosPrazo_Momento_Internalname = "CONTRATOSERVICOSPRAZO_MOMENTO";
         lblTextblockcontratoservicosprazo_cada_Internalname = "TEXTBLOCKCONTRATOSERVICOSPRAZO_CADA";
         edtContratoServicosPrazo_Cada_Internalname = "CONTRATOSERVICOSPRAZO_CADA";
         edtContratoServicos_UndCntSgl_Internalname = "CONTRATOSERVICOS_UNDCNTSGL";
         tblTablemergedcontratoservicosprazo_cada_Internalname = "TABLEMERGEDCONTRATOSERVICOSPRAZO_CADA";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         Dvpanel_tableattributes_Internalname = "DVPANEL_TABLEATTRIBUTES";
         lblTextblockcontratoservicosprazo_diasbaixa_Internalname = "TEXTBLOCKCONTRATOSERVICOSPRAZO_DIASBAIXA";
         edtContratoServicosPrazo_DiasBaixa_Internalname = "CONTRATOSERVICOSPRAZO_DIASBAIXA";
         lblContratoservicosprazo_diasbaixa_righttext_Internalname = "CONTRATOSERVICOSPRAZO_DIASBAIXA_RIGHTTEXT";
         tblTablemergedcontratoservicosprazo_diasbaixa_Internalname = "TABLEMERGEDCONTRATOSERVICOSPRAZO_DIASBAIXA";
         lblTextblockcontratoservicosprazo_diasmedia_Internalname = "TEXTBLOCKCONTRATOSERVICOSPRAZO_DIASMEDIA";
         edtContratoServicosPrazo_DiasMedia_Internalname = "CONTRATOSERVICOSPRAZO_DIASMEDIA";
         lblContratoservicosprazo_diasmedia_righttext_Internalname = "CONTRATOSERVICOSPRAZO_DIASMEDIA_RIGHTTEXT";
         tblTablemergedcontratoservicosprazo_diasmedia_Internalname = "TABLEMERGEDCONTRATOSERVICOSPRAZO_DIASMEDIA";
         lblTextblockcontratoservicosprazo_diasalta_Internalname = "TEXTBLOCKCONTRATOSERVICOSPRAZO_DIASALTA";
         edtContratoServicosPrazo_DiasAlta_Internalname = "CONTRATOSERVICOSPRAZO_DIASALTA";
         lblContratoservicosprazo_diasalta_righttext_Internalname = "CONTRATOSERVICOSPRAZO_DIASALTA_RIGHTTEXT";
         tblTablemergedcontratoservicosprazo_diasalta_Internalname = "TABLEMERGEDCONTRATOSERVICOSPRAZO_DIASALTA";
         tblComplexidade_Internalname = "COMPLEXIDADE";
         Dvpanel_complexidade_Internalname = "DVPANEL_COMPLEXIDADE";
         tblTblcomplexidade_Internalname = "TBLCOMPLEXIDADE";
         tblTablecontent_Internalname = "TABLECONTENT";
         edtContratoServicosPrazoRegra_Sequencial_Internalname = "CONTRATOSERVICOSPRAZOREGRA_SEQUENCIAL";
         edtContratoServicosPrazoRegra_Inicio_Internalname = "CONTRATOSERVICOSPRAZOREGRA_INICIO";
         edtContratoServicosPrazoRegra_Fim_Internalname = "CONTRATOSERVICOSPRAZOREGRA_FIM";
         edtContratoServicosPrazoRegra_Dias_Internalname = "CONTRATOSERVICOSPRAZOREGRA_DIAS";
         tblRegras_Internalname = "REGRAS";
         Dvpanel_regras_Internalname = "DVPANEL_REGRAS";
         tblTblregras_Internalname = "TBLREGRAS";
         edtContratoServicosPrazoPlnj_Sequencial_Internalname = "CONTRATOSERVICOSPRAZOPLNJ_SEQUENCIAL";
         edtContratoServicosPrazoPlnj_Inicio_Internalname = "CONTRATOSERVICOSPRAZOPLNJ_INICIO";
         edtContratoServicosPrazoPlnj_Fim_Internalname = "CONTRATOSERVICOSPRAZOPLNJ_FIM";
         edtContratoServicosPrazoPlnj_Dias_Internalname = "CONTRATOSERVICOSPRAZOPLNJ_DIAS";
         tblPlanejamento_Internalname = "PLANEJAMENTO";
         Dvpanel_planejamento_Internalname = "DVPANEL_PLANEJAMENTO";
         tblTblplanejamento_Internalname = "TBLPLANEJAMENTO";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         bttBtn_trn_delete_Internalname = "BTN_TRN_DELETE";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         edtContratoServicosPrazo_CntSrvCod_Internalname = "CONTRATOSERVICOSPRAZO_CNTSRVCOD";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
         subGrid2_Internalname = "GRID2";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Dvpanel_planejamento_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_planejamento_Iconposition = "left";
         Dvpanel_planejamento_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_planejamento_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_planejamento_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_planejamento_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_planejamento_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_planejamento_Title = "SLA Planejamento";
         Dvpanel_planejamento_Cls = "GXUI-DVelop-Panel";
         Dvpanel_planejamento_Width = "100%";
         Dvpanel_regras_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_regras_Iconposition = "left";
         Dvpanel_regras_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_regras_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_regras_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_regras_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_regras_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_regras_Title = "SLA Execu��o";
         Dvpanel_regras_Cls = "GXUI-DVelop-Panel";
         Dvpanel_regras_Width = "100%";
         Dvpanel_complexidade_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_complexidade_Iconposition = "left";
         Dvpanel_complexidade_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_complexidade_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_complexidade_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_complexidade_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_complexidade_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_complexidade_Title = "Complexidade";
         Dvpanel_complexidade_Cls = "GXUI-DVelop-Panel";
         Dvpanel_complexidade_Width = "100%";
         Dvpanel_tableattributes_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Iconposition = "left";
         Dvpanel_tableattributes_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tableattributes_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Title = "Prazo";
         Dvpanel_tableattributes_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tableattributes_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Prazo";
         edtContratoServicosPrazoPlnj_Dias_Jsonclick = "";
         edtContratoServicosPrazoPlnj_Fim_Jsonclick = "";
         edtContratoServicosPrazoPlnj_Inicio_Jsonclick = "";
         edtContratoServicosPrazoPlnj_Sequencial_Jsonclick = "";
         subGrid2_Class = "WorkWithBorder WorkWith";
         edtContratoServicosPrazoRegra_Dias_Jsonclick = "";
         edtContratoServicosPrazoRegra_Fim_Jsonclick = "";
         edtContratoServicosPrazoRegra_Inicio_Jsonclick = "";
         edtContratoServicosPrazoRegra_Sequencial_Jsonclick = "";
         subGrid_Class = "WorkWithBorder WorkWith";
         edtavServico_sigla_Jsonclick = "";
         edtavServico_sigla_Enabled = 0;
         edtContratoServicos_UndCntSgl_Jsonclick = "";
         edtContratoServicos_UndCntSgl_Enabled = 0;
         edtContratoServicos_UndCntSgl_Visible = 1;
         edtContratoServicosPrazo_Cada_Jsonclick = "";
         edtContratoServicosPrazo_Cada_Enabled = 1;
         edtContratoServicosPrazo_Cada_Visible = 1;
         lblTextblockcontratoservicosprazo_cada_Visible = 1;
         cmbContratoServicosPrazo_Momento_Jsonclick = "";
         cmbContratoServicosPrazo_Momento.Enabled = 1;
         edtContratoServicosPrazo_Dias_Jsonclick = "";
         edtContratoServicosPrazo_Dias_Enabled = 1;
         edtContratoServicosPrazo_Dias_Visible = 1;
         lblTextblockcontratoservicosprazo_dias_Visible = 1;
         cmbContratoServicosPrazo_Tipo_Jsonclick = "";
         cmbContratoServicosPrazo_Tipo.Enabled = 1;
         edtContratoServicosPrazo_DiasBaixa_Jsonclick = "";
         edtContratoServicosPrazo_DiasBaixa_Enabled = 1;
         edtContratoServicosPrazo_DiasMedia_Jsonclick = "";
         edtContratoServicosPrazo_DiasMedia_Enabled = 1;
         edtContratoServicosPrazo_DiasAlta_Jsonclick = "";
         edtContratoServicosPrazo_DiasAlta_Enabled = 1;
         tblTblcomplexidade_Visible = 1;
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtContratoServicosPrazoRegra_Dias_Enabled = 1;
         edtContratoServicosPrazoRegra_Fim_Enabled = 1;
         edtContratoServicosPrazoRegra_Inicio_Enabled = 1;
         edtContratoServicosPrazoRegra_Sequencial_Enabled = 0;
         subGrid_Backcolorstyle = 3;
         tblTblregras_Visible = 1;
         subGrid2_Allowcollapsing = 0;
         subGrid2_Allowselection = 0;
         edtContratoServicosPrazoPlnj_Dias_Enabled = 1;
         edtContratoServicosPrazoPlnj_Fim_Enabled = 1;
         edtContratoServicosPrazoPlnj_Inicio_Enabled = 1;
         edtContratoServicosPrazoPlnj_Sequencial_Enabled = 0;
         subGrid2_Backcolorstyle = 3;
         bttBtn_trn_delete_Enabled = 0;
         bttBtn_trn_delete_Visible = 1;
         bttBtn_trn_cancel_Visible = 1;
         bttBtn_trn_enter_Enabled = 1;
         bttBtn_trn_enter_Visible = 1;
         edtContratoServicosPrazo_CntSrvCod_Jsonclick = "";
         edtContratoServicosPrazo_CntSrvCod_Enabled = 1;
         edtContratoServicosPrazo_CntSrvCod_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void XC_19_2U112( wwpbaseobjects.SdtAuditingObject AV12AuditingObject ,
                                  int A903ContratoServicosPrazo_CntSrvCod ,
                                  String Gx_mode )
      {
         new loadauditcontratoservicosprazo(context ).execute(  "Y", ref  AV12AuditingObject,  A903ContratoServicosPrazo_CntSrvCod,  Gx_mode) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A903ContratoServicosPrazo_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A903ContratoServicosPrazo_CntSrvCod), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.EncodeString( AV12AuditingObject.ToXml(false, true, "AuditingObject", "GxEv3Up14_MeetrikaVs3")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      protected void XC_20_2U112( wwpbaseobjects.SdtAuditingObject AV12AuditingObject ,
                                  int A903ContratoServicosPrazo_CntSrvCod ,
                                  String Gx_mode )
      {
         new loadauditcontratoservicosprazo(context ).execute(  "Y", ref  AV12AuditingObject,  A903ContratoServicosPrazo_CntSrvCod,  Gx_mode) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A903ContratoServicosPrazo_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A903ContratoServicosPrazo_CntSrvCod), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.EncodeString( AV12AuditingObject.ToXml(false, true, "AuditingObject", "GxEv3Up14_MeetrikaVs3")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      protected void XC_21_2U112( String Gx_mode ,
                                  wwpbaseobjects.SdtAuditingObject AV12AuditingObject ,
                                  int A903ContratoServicosPrazo_CntSrvCod )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            new loadauditcontratoservicosprazo(context ).execute(  "N", ref  AV12AuditingObject,  A903ContratoServicosPrazo_CntSrvCod,  Gx_mode) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A903ContratoServicosPrazo_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A903ContratoServicosPrazo_CntSrvCod), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.EncodeString( AV12AuditingObject.ToXml(false, true, "AuditingObject", "GxEv3Up14_MeetrikaVs3")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      protected void XC_22_2U112( String Gx_mode ,
                                  wwpbaseobjects.SdtAuditingObject AV12AuditingObject ,
                                  int A903ContratoServicosPrazo_CntSrvCod )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )  )
         {
            new loadauditcontratoservicosprazo(context ).execute(  "N", ref  AV12AuditingObject,  A903ContratoServicosPrazo_CntSrvCod,  Gx_mode) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A903ContratoServicosPrazo_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A903ContratoServicosPrazo_CntSrvCod), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.EncodeString( AV12AuditingObject.ToXml(false, true, "AuditingObject", "GxEv3Up14_MeetrikaVs3")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         Gx_mode = "INS";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         SubsflControlProps_97113( ) ;
         while ( nGXsfl_97_idx <= nRC_GXsfl_97 )
         {
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            standaloneNotModal2U113( ) ;
            standaloneModal2U113( ) ;
            cmbContratoServicosPrazo_Tipo.Name = "CONTRATOSERVICOSPRAZO_TIPO";
            cmbContratoServicosPrazo_Tipo.WebTags = "";
            cmbContratoServicosPrazo_Tipo.addItem("", "(Nenhum)", 0);
            cmbContratoServicosPrazo_Tipo.addItem("A", "Acumulado", 0);
            cmbContratoServicosPrazo_Tipo.addItem("C", "Complexidade", 0);
            cmbContratoServicosPrazo_Tipo.addItem("S", "Solicitado", 0);
            cmbContratoServicosPrazo_Tipo.addItem("F", "Fixo", 0);
            cmbContratoServicosPrazo_Tipo.addItem("E", "El�stico", 0);
            cmbContratoServicosPrazo_Tipo.addItem("V", "Vari�vel", 0);
            cmbContratoServicosPrazo_Tipo.addItem("P", "Progress�o Aritm�tica", 0);
            cmbContratoServicosPrazo_Tipo.addItem("O", "Combinado", 0);
            if ( cmbContratoServicosPrazo_Tipo.ItemCount > 0 )
            {
               A904ContratoServicosPrazo_Tipo = cmbContratoServicosPrazo_Tipo.getValidValue(A904ContratoServicosPrazo_Tipo);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A904ContratoServicosPrazo_Tipo", A904ContratoServicosPrazo_Tipo);
            }
            cmbContratoServicosPrazo_Momento.Name = "CONTRATOSERVICOSPRAZO_MOMENTO";
            cmbContratoServicosPrazo_Momento.WebTags = "";
            cmbContratoServicosPrazo_Momento.addItem("0", "Sempre", 0);
            cmbContratoServicosPrazo_Momento.addItem("1", "Exceto na Solicita��o", 0);
            if ( cmbContratoServicosPrazo_Momento.ItemCount > 0 )
            {
               A1823ContratoServicosPrazo_Momento = (short)(NumberUtil.Val( cmbContratoServicosPrazo_Momento.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1823ContratoServicosPrazo_Momento), 4, 0))), "."));
               n1823ContratoServicosPrazo_Momento = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1823ContratoServicosPrazo_Momento", StringUtil.LTrim( StringUtil.Str( (decimal)(A1823ContratoServicosPrazo_Momento), 4, 0)));
            }
            dynload_actions( ) ;
            SendRow2U113( ) ;
            nGXsfl_97_idx = (short)(nGXsfl_97_idx+1);
            sGXsfl_97_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_97_idx), 4, 0)), 4, "0");
            SubsflControlProps_97113( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxnrGrid2_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         Gx_mode = "INS";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         SubsflControlProps_112188( ) ;
         while ( nGXsfl_112_idx <= nRC_GXsfl_112 )
         {
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            standaloneNotModal2U188( ) ;
            standaloneModal2U188( ) ;
            cmbContratoServicosPrazo_Tipo.Name = "CONTRATOSERVICOSPRAZO_TIPO";
            cmbContratoServicosPrazo_Tipo.WebTags = "";
            cmbContratoServicosPrazo_Tipo.addItem("", "(Nenhum)", 0);
            cmbContratoServicosPrazo_Tipo.addItem("A", "Acumulado", 0);
            cmbContratoServicosPrazo_Tipo.addItem("C", "Complexidade", 0);
            cmbContratoServicosPrazo_Tipo.addItem("S", "Solicitado", 0);
            cmbContratoServicosPrazo_Tipo.addItem("F", "Fixo", 0);
            cmbContratoServicosPrazo_Tipo.addItem("E", "El�stico", 0);
            cmbContratoServicosPrazo_Tipo.addItem("V", "Vari�vel", 0);
            cmbContratoServicosPrazo_Tipo.addItem("P", "Progress�o Aritm�tica", 0);
            cmbContratoServicosPrazo_Tipo.addItem("O", "Combinado", 0);
            if ( cmbContratoServicosPrazo_Tipo.ItemCount > 0 )
            {
               A904ContratoServicosPrazo_Tipo = cmbContratoServicosPrazo_Tipo.getValidValue(A904ContratoServicosPrazo_Tipo);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A904ContratoServicosPrazo_Tipo", A904ContratoServicosPrazo_Tipo);
            }
            cmbContratoServicosPrazo_Momento.Name = "CONTRATOSERVICOSPRAZO_MOMENTO";
            cmbContratoServicosPrazo_Momento.WebTags = "";
            cmbContratoServicosPrazo_Momento.addItem("0", "Sempre", 0);
            cmbContratoServicosPrazo_Momento.addItem("1", "Exceto na Solicita��o", 0);
            if ( cmbContratoServicosPrazo_Momento.ItemCount > 0 )
            {
               A1823ContratoServicosPrazo_Momento = (short)(NumberUtil.Val( cmbContratoServicosPrazo_Momento.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1823ContratoServicosPrazo_Momento), 4, 0))), "."));
               n1823ContratoServicosPrazo_Momento = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1823ContratoServicosPrazo_Momento", StringUtil.LTrim( StringUtil.Str( (decimal)(A1823ContratoServicosPrazo_Momento), 4, 0)));
            }
            dynload_actions( ) ;
            SendRow2U188( ) ;
            nGXsfl_112_idx = (short)(nGXsfl_112_idx+1);
            sGXsfl_112_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_112_idx), 4, 0)), 4, "0");
            SubsflControlProps_112188( ) ;
         }
         context.GX_webresponse.AddString(Grid2Container.ToJavascriptSource());
         /* End function gxnrGrid2_newrow */
      }

      public void Valid_Contratoservicosprazo_cntsrvcod( int GX_Parm1 ,
                                                         int GX_Parm2 ,
                                                         int GX_Parm3 ,
                                                         String GX_Parm4 ,
                                                         String GX_Parm5 ,
                                                         String GX_Parm6 ,
                                                         short GX_Parm7 ,
                                                         short GX_Parm8 )
      {
         A903ContratoServicosPrazo_CntSrvCod = GX_Parm1;
         A1212ContratoServicos_UnidadeContratada = GX_Parm2;
         n1212ContratoServicos_UnidadeContratada = false;
         A1095ContratoServicosPrazo_ServicoCod = GX_Parm3;
         n1095ContratoServicosPrazo_ServicoCod = false;
         A1096ContratoServicosPrazo_ServicoSigla = GX_Parm4;
         n1096ContratoServicosPrazo_ServicoSigla = false;
         A1712ContratoServicos_UndCntSgl = GX_Parm5;
         n1712ContratoServicos_UndCntSgl = false;
         AV11Servico_Sigla = GX_Parm6;
         A910ContratoServicosPrazo_QtdRegras = GX_Parm7;
         n910ContratoServicosPrazo_QtdRegras = false;
         A1737ContratoServicosPrazo_QtdPlnj = GX_Parm8;
         n1737ContratoServicosPrazo_QtdPlnj = false;
         /* Using cursor T002U31 */
         pr_default.execute(23, new Object[] {A903ContratoServicosPrazo_CntSrvCod});
         if ( (pr_default.getStatus(23) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato Servicos Prazos_Contrato Servicos'.", "ForeignKeyNotFound", 1, "CONTRATOSERVICOSPRAZO_CNTSRVCOD");
            AnyError = 1;
            GX_FocusControl = edtContratoServicosPrazo_CntSrvCod_Internalname;
         }
         A1212ContratoServicos_UnidadeContratada = T002U31_A1212ContratoServicos_UnidadeContratada[0];
         n1212ContratoServicos_UnidadeContratada = T002U31_n1212ContratoServicos_UnidadeContratada[0];
         A1095ContratoServicosPrazo_ServicoCod = T002U31_A1095ContratoServicosPrazo_ServicoCod[0];
         n1095ContratoServicosPrazo_ServicoCod = T002U31_n1095ContratoServicosPrazo_ServicoCod[0];
         pr_default.close(23);
         /* Using cursor T002U32 */
         pr_default.execute(24, new Object[] {n1212ContratoServicos_UnidadeContratada, A1212ContratoServicos_UnidadeContratada});
         if ( (pr_default.getStatus(24) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato Servicos_Unidade Medicao'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A1712ContratoServicos_UndCntSgl = T002U32_A1712ContratoServicos_UndCntSgl[0];
         n1712ContratoServicos_UndCntSgl = T002U32_n1712ContratoServicos_UndCntSgl[0];
         pr_default.close(24);
         /* Using cursor T002U33 */
         pr_default.execute(25, new Object[] {n1095ContratoServicosPrazo_ServicoCod, A1095ContratoServicosPrazo_ServicoCod});
         if ( (pr_default.getStatus(25) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato Servicos Prazos_Contrato Servicos'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A1096ContratoServicosPrazo_ServicoSigla = T002U33_A1096ContratoServicosPrazo_ServicoSigla[0];
         n1096ContratoServicosPrazo_ServicoSigla = T002U33_n1096ContratoServicosPrazo_ServicoSigla[0];
         pr_default.close(25);
         AV11Servico_Sigla = A1096ContratoServicosPrazo_ServicoSigla;
         /* Using cursor T002U35 */
         pr_default.execute(26, new Object[] {A903ContratoServicosPrazo_CntSrvCod});
         if ( (pr_default.getStatus(26) != 101) )
         {
            A910ContratoServicosPrazo_QtdRegras = T002U35_A910ContratoServicosPrazo_QtdRegras[0];
            n910ContratoServicosPrazo_QtdRegras = T002U35_n910ContratoServicosPrazo_QtdRegras[0];
         }
         else
         {
            A910ContratoServicosPrazo_QtdRegras = 0;
            n910ContratoServicosPrazo_QtdRegras = false;
         }
         pr_default.close(26);
         /* Using cursor T002U37 */
         pr_default.execute(27, new Object[] {A903ContratoServicosPrazo_CntSrvCod});
         if ( (pr_default.getStatus(27) != 101) )
         {
            A1737ContratoServicosPrazo_QtdPlnj = T002U37_A1737ContratoServicosPrazo_QtdPlnj[0];
            n1737ContratoServicosPrazo_QtdPlnj = T002U37_n1737ContratoServicosPrazo_QtdPlnj[0];
         }
         else
         {
            A1737ContratoServicosPrazo_QtdPlnj = 0;
            n1737ContratoServicosPrazo_QtdPlnj = false;
         }
         pr_default.close(27);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A1212ContratoServicos_UnidadeContratada = 0;
            n1212ContratoServicos_UnidadeContratada = false;
            A1095ContratoServicosPrazo_ServicoCod = 0;
            n1095ContratoServicosPrazo_ServicoCod = false;
            A1712ContratoServicos_UndCntSgl = "";
            n1712ContratoServicos_UndCntSgl = false;
            A1096ContratoServicosPrazo_ServicoSigla = "";
            n1096ContratoServicosPrazo_ServicoSigla = false;
            A910ContratoServicosPrazo_QtdRegras = 0;
            n910ContratoServicosPrazo_QtdRegras = false;
            A1737ContratoServicosPrazo_QtdPlnj = 0;
            n1737ContratoServicosPrazo_QtdPlnj = false;
         }
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A1212ContratoServicos_UnidadeContratada), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A1095ContratoServicosPrazo_ServicoCod), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.RTrim( A1712ContratoServicos_UndCntSgl));
         isValidOutput.Add(StringUtil.RTrim( A1096ContratoServicosPrazo_ServicoSigla));
         isValidOutput.Add(StringUtil.RTrim( AV11Servico_Sigla));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A910ContratoServicosPrazo_QtdRegras), 4, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A1737ContratoServicosPrazo_QtdPlnj), 4, 0, ".", "")));
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV7ContratoServicosPrazo_CntSrvCod',fld:'vCONTRATOSERVICOSPRAZO_CNTSRVCOD',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E132U2',iparms:[{av:'AV12AuditingObject',fld:'vAUDITINGOBJECT',pic:'',nv:null},{av:'AV14Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV9TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null}],oparms:[]}");
         setEventMetadata("CONTRATOSERVICOSPRAZO_TIPO.CLICK","{handler:'E112U112',iparms:[{av:'A904ContratoServicosPrazo_Tipo',fld:'CONTRATOSERVICOSPRAZO_TIPO',pic:'',nv:''}],oparms:[{av:'lblTextblockcontratoservicosprazo_dias_Visible',ctrl:'TEXTBLOCKCONTRATOSERVICOSPRAZO_DIAS',prop:'Visible'},{av:'edtContratoServicosPrazo_Dias_Visible',ctrl:'CONTRATOSERVICOSPRAZO_DIAS',prop:'Visible'},{av:'lblTextblockcontratoservicosprazo_cada_Visible',ctrl:'TEXTBLOCKCONTRATOSERVICOSPRAZO_CADA',prop:'Visible'},{av:'edtContratoServicosPrazo_Cada_Visible',ctrl:'CONTRATOSERVICOSPRAZO_CADA',prop:'Visible'},{av:'edtContratoServicos_UndCntSgl_Visible',ctrl:'CONTRATOSERVICOS_UNDCNTSGL',prop:'Visible'},{av:'tblTblregras_Visible',ctrl:'TBLREGRAS',prop:'Visible'},{av:'tblTblcomplexidade_Visible',ctrl:'TBLCOMPLEXIDADE',prop:'Visible'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(3);
         pr_default.close(5);
         pr_default.close(23);
         pr_default.close(25);
         pr_default.close(24);
         pr_default.close(26);
         pr_default.close(27);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         Z904ContratoServicosPrazo_Tipo = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         A904ContratoServicosPrazo_Tipo = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         TempTags = "";
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         bttBtn_trn_delete_Jsonclick = "";
         Grid2Container = new GXWebGrid( context);
         Grid2Column = new GXWebColumn();
         sMode188 = "";
         GridContainer = new GXWebGrid( context);
         GridColumn = new GXWebColumn();
         sMode113 = "";
         lblTextblockcontratoservicosprazo_diasbaixa_Jsonclick = "";
         lblTextblockcontratoservicosprazo_diasmedia_Jsonclick = "";
         lblTextblockcontratoservicosprazo_diasalta_Jsonclick = "";
         lblContratoservicosprazo_diasalta_righttext_Jsonclick = "";
         lblContratoservicosprazo_diasmedia_righttext_Jsonclick = "";
         lblContratoservicosprazo_diasbaixa_righttext_Jsonclick = "";
         lblTextblockcontratoservicosprazo_tipo_Jsonclick = "";
         lblTextblockcontratoservicosprazo_dias_Jsonclick = "";
         lblTextblockcontratoservicosprazo_momento_Jsonclick = "";
         lblTextblockcontratoservicosprazo_cada_Jsonclick = "";
         A1712ContratoServicos_UndCntSgl = "";
         lblViewtitle_Jsonclick = "";
         AV11Servico_Sigla = "";
         A1096ContratoServicosPrazo_ServicoSigla = "";
         AV12AuditingObject = new wwpbaseobjects.SdtAuditingObject(context);
         AV14Pgmname = "";
         Dvpanel_tableattributes_Height = "";
         Dvpanel_tableattributes_Class = "";
         Dvpanel_complexidade_Height = "";
         Dvpanel_complexidade_Class = "";
         Dvpanel_regras_Height = "";
         Dvpanel_regras_Class = "";
         Dvpanel_planejamento_Height = "";
         Dvpanel_planejamento_Class = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode112 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         Z1712ContratoServicos_UndCntSgl = "";
         Z1096ContratoServicosPrazo_ServicoSigla = "";
         T002U8_A1212ContratoServicos_UnidadeContratada = new int[1] ;
         T002U8_n1212ContratoServicos_UnidadeContratada = new bool[] {false} ;
         T002U8_A1095ContratoServicosPrazo_ServicoCod = new int[1] ;
         T002U8_n1095ContratoServicosPrazo_ServicoCod = new bool[] {false} ;
         T002U10_A1712ContratoServicos_UndCntSgl = new String[] {""} ;
         T002U10_n1712ContratoServicos_UndCntSgl = new bool[] {false} ;
         T002U9_A1096ContratoServicosPrazo_ServicoSigla = new String[] {""} ;
         T002U9_n1096ContratoServicosPrazo_ServicoSigla = new bool[] {false} ;
         T002U12_A910ContratoServicosPrazo_QtdRegras = new short[1] ;
         T002U12_n910ContratoServicosPrazo_QtdRegras = new bool[] {false} ;
         T002U14_A1737ContratoServicosPrazo_QtdPlnj = new short[1] ;
         T002U14_n1737ContratoServicosPrazo_QtdPlnj = new bool[] {false} ;
         T002U17_A1212ContratoServicos_UnidadeContratada = new int[1] ;
         T002U17_n1212ContratoServicos_UnidadeContratada = new bool[] {false} ;
         T002U17_A905ContratoServicosPrazo_Dias = new short[1] ;
         T002U17_n905ContratoServicosPrazo_Dias = new bool[] {false} ;
         T002U17_A904ContratoServicosPrazo_Tipo = new String[] {""} ;
         T002U17_A1456ContratoServicosPrazo_Cada = new decimal[1] ;
         T002U17_n1456ContratoServicosPrazo_Cada = new bool[] {false} ;
         T002U17_A1712ContratoServicos_UndCntSgl = new String[] {""} ;
         T002U17_n1712ContratoServicos_UndCntSgl = new bool[] {false} ;
         T002U17_A1096ContratoServicosPrazo_ServicoSigla = new String[] {""} ;
         T002U17_n1096ContratoServicosPrazo_ServicoSigla = new bool[] {false} ;
         T002U17_A1263ContratoServicosPrazo_DiasAlta = new short[1] ;
         T002U17_n1263ContratoServicosPrazo_DiasAlta = new bool[] {false} ;
         T002U17_A1264ContratoServicosPrazo_DiasMedia = new short[1] ;
         T002U17_n1264ContratoServicosPrazo_DiasMedia = new bool[] {false} ;
         T002U17_A1265ContratoServicosPrazo_DiasBaixa = new short[1] ;
         T002U17_n1265ContratoServicosPrazo_DiasBaixa = new bool[] {false} ;
         T002U17_A1823ContratoServicosPrazo_Momento = new short[1] ;
         T002U17_n1823ContratoServicosPrazo_Momento = new bool[] {false} ;
         T002U17_A903ContratoServicosPrazo_CntSrvCod = new int[1] ;
         T002U17_A1095ContratoServicosPrazo_ServicoCod = new int[1] ;
         T002U17_n1095ContratoServicosPrazo_ServicoCod = new bool[] {false} ;
         T002U17_A910ContratoServicosPrazo_QtdRegras = new short[1] ;
         T002U17_n910ContratoServicosPrazo_QtdRegras = new bool[] {false} ;
         T002U17_A1737ContratoServicosPrazo_QtdPlnj = new short[1] ;
         T002U17_n1737ContratoServicosPrazo_QtdPlnj = new bool[] {false} ;
         T002U18_A1212ContratoServicos_UnidadeContratada = new int[1] ;
         T002U18_n1212ContratoServicos_UnidadeContratada = new bool[] {false} ;
         T002U18_A1095ContratoServicosPrazo_ServicoCod = new int[1] ;
         T002U18_n1095ContratoServicosPrazo_ServicoCod = new bool[] {false} ;
         T002U19_A1712ContratoServicos_UndCntSgl = new String[] {""} ;
         T002U19_n1712ContratoServicos_UndCntSgl = new bool[] {false} ;
         T002U20_A1096ContratoServicosPrazo_ServicoSigla = new String[] {""} ;
         T002U20_n1096ContratoServicosPrazo_ServicoSigla = new bool[] {false} ;
         T002U22_A910ContratoServicosPrazo_QtdRegras = new short[1] ;
         T002U22_n910ContratoServicosPrazo_QtdRegras = new bool[] {false} ;
         T002U24_A1737ContratoServicosPrazo_QtdPlnj = new short[1] ;
         T002U24_n1737ContratoServicosPrazo_QtdPlnj = new bool[] {false} ;
         T002U25_A903ContratoServicosPrazo_CntSrvCod = new int[1] ;
         T002U7_A905ContratoServicosPrazo_Dias = new short[1] ;
         T002U7_n905ContratoServicosPrazo_Dias = new bool[] {false} ;
         T002U7_A904ContratoServicosPrazo_Tipo = new String[] {""} ;
         T002U7_A1456ContratoServicosPrazo_Cada = new decimal[1] ;
         T002U7_n1456ContratoServicosPrazo_Cada = new bool[] {false} ;
         T002U7_A1263ContratoServicosPrazo_DiasAlta = new short[1] ;
         T002U7_n1263ContratoServicosPrazo_DiasAlta = new bool[] {false} ;
         T002U7_A1264ContratoServicosPrazo_DiasMedia = new short[1] ;
         T002U7_n1264ContratoServicosPrazo_DiasMedia = new bool[] {false} ;
         T002U7_A1265ContratoServicosPrazo_DiasBaixa = new short[1] ;
         T002U7_n1265ContratoServicosPrazo_DiasBaixa = new bool[] {false} ;
         T002U7_A1823ContratoServicosPrazo_Momento = new short[1] ;
         T002U7_n1823ContratoServicosPrazo_Momento = new bool[] {false} ;
         T002U7_A903ContratoServicosPrazo_CntSrvCod = new int[1] ;
         T002U26_A903ContratoServicosPrazo_CntSrvCod = new int[1] ;
         T002U27_A903ContratoServicosPrazo_CntSrvCod = new int[1] ;
         T002U6_A905ContratoServicosPrazo_Dias = new short[1] ;
         T002U6_n905ContratoServicosPrazo_Dias = new bool[] {false} ;
         T002U6_A904ContratoServicosPrazo_Tipo = new String[] {""} ;
         T002U6_A1456ContratoServicosPrazo_Cada = new decimal[1] ;
         T002U6_n1456ContratoServicosPrazo_Cada = new bool[] {false} ;
         T002U6_A1263ContratoServicosPrazo_DiasAlta = new short[1] ;
         T002U6_n1263ContratoServicosPrazo_DiasAlta = new bool[] {false} ;
         T002U6_A1264ContratoServicosPrazo_DiasMedia = new short[1] ;
         T002U6_n1264ContratoServicosPrazo_DiasMedia = new bool[] {false} ;
         T002U6_A1265ContratoServicosPrazo_DiasBaixa = new short[1] ;
         T002U6_n1265ContratoServicosPrazo_DiasBaixa = new bool[] {false} ;
         T002U6_A1823ContratoServicosPrazo_Momento = new short[1] ;
         T002U6_n1823ContratoServicosPrazo_Momento = new bool[] {false} ;
         T002U6_A903ContratoServicosPrazo_CntSrvCod = new int[1] ;
         T002U31_A1212ContratoServicos_UnidadeContratada = new int[1] ;
         T002U31_n1212ContratoServicos_UnidadeContratada = new bool[] {false} ;
         T002U31_A1095ContratoServicosPrazo_ServicoCod = new int[1] ;
         T002U31_n1095ContratoServicosPrazo_ServicoCod = new bool[] {false} ;
         T002U32_A1712ContratoServicos_UndCntSgl = new String[] {""} ;
         T002U32_n1712ContratoServicos_UndCntSgl = new bool[] {false} ;
         T002U33_A1096ContratoServicosPrazo_ServicoSigla = new String[] {""} ;
         T002U33_n1096ContratoServicosPrazo_ServicoSigla = new bool[] {false} ;
         T002U35_A910ContratoServicosPrazo_QtdRegras = new short[1] ;
         T002U35_n910ContratoServicosPrazo_QtdRegras = new bool[] {false} ;
         T002U37_A1737ContratoServicosPrazo_QtdPlnj = new short[1] ;
         T002U37_n1737ContratoServicosPrazo_QtdPlnj = new bool[] {false} ;
         T002U38_A903ContratoServicosPrazo_CntSrvCod = new int[1] ;
         T002U39_A903ContratoServicosPrazo_CntSrvCod = new int[1] ;
         T002U39_A906ContratoServicosPrazoRegra_Sequencial = new short[1] ;
         T002U39_A907ContratoServicosPrazoRegra_Inicio = new decimal[1] ;
         T002U39_A908ContratoServicosPrazoRegra_Fim = new decimal[1] ;
         T002U39_A909ContratoServicosPrazoRegra_Dias = new short[1] ;
         T002U40_A903ContratoServicosPrazo_CntSrvCod = new int[1] ;
         T002U40_A906ContratoServicosPrazoRegra_Sequencial = new short[1] ;
         T002U5_A903ContratoServicosPrazo_CntSrvCod = new int[1] ;
         T002U5_A906ContratoServicosPrazoRegra_Sequencial = new short[1] ;
         T002U5_A907ContratoServicosPrazoRegra_Inicio = new decimal[1] ;
         T002U5_A908ContratoServicosPrazoRegra_Fim = new decimal[1] ;
         T002U5_A909ContratoServicosPrazoRegra_Dias = new short[1] ;
         T002U4_A903ContratoServicosPrazo_CntSrvCod = new int[1] ;
         T002U4_A906ContratoServicosPrazoRegra_Sequencial = new short[1] ;
         T002U4_A907ContratoServicosPrazoRegra_Inicio = new decimal[1] ;
         T002U4_A908ContratoServicosPrazoRegra_Fim = new decimal[1] ;
         T002U4_A909ContratoServicosPrazoRegra_Dias = new short[1] ;
         T002U44_A903ContratoServicosPrazo_CntSrvCod = new int[1] ;
         T002U44_A906ContratoServicosPrazoRegra_Sequencial = new short[1] ;
         GXCCtl = "";
         T002U45_A903ContratoServicosPrazo_CntSrvCod = new int[1] ;
         T002U45_A1724ContratoServicosPrazoPlnj_Sequencial = new short[1] ;
         T002U45_A1734ContratoServicosPrazoPlnj_Inicio = new decimal[1] ;
         T002U45_A1735ContratoServicosPrazoPlnj_Fim = new decimal[1] ;
         T002U45_A1736ContratoServicosPrazoPlnj_Dias = new short[1] ;
         T002U46_A903ContratoServicosPrazo_CntSrvCod = new int[1] ;
         T002U46_A1724ContratoServicosPrazoPlnj_Sequencial = new short[1] ;
         T002U3_A903ContratoServicosPrazo_CntSrvCod = new int[1] ;
         T002U3_A1724ContratoServicosPrazoPlnj_Sequencial = new short[1] ;
         T002U3_A1734ContratoServicosPrazoPlnj_Inicio = new decimal[1] ;
         T002U3_A1735ContratoServicosPrazoPlnj_Fim = new decimal[1] ;
         T002U3_A1736ContratoServicosPrazoPlnj_Dias = new short[1] ;
         T002U2_A903ContratoServicosPrazo_CntSrvCod = new int[1] ;
         T002U2_A1724ContratoServicosPrazoPlnj_Sequencial = new short[1] ;
         T002U2_A1734ContratoServicosPrazoPlnj_Inicio = new decimal[1] ;
         T002U2_A1735ContratoServicosPrazoPlnj_Fim = new decimal[1] ;
         T002U2_A1736ContratoServicosPrazoPlnj_Dias = new short[1] ;
         T002U50_A903ContratoServicosPrazo_CntSrvCod = new int[1] ;
         T002U50_A1724ContratoServicosPrazoPlnj_Sequencial = new short[1] ;
         GridRow = new GXWebRow();
         subGrid_Linesclass = "";
         ROClassString = "";
         Grid2Row = new GXWebRow();
         subGrid2_Linesclass = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         isValidOutput = new GxUnknownObjectCollection();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contratoservicosprazo__default(),
            new Object[][] {
                new Object[] {
               T002U2_A903ContratoServicosPrazo_CntSrvCod, T002U2_A1724ContratoServicosPrazoPlnj_Sequencial, T002U2_A1734ContratoServicosPrazoPlnj_Inicio, T002U2_A1735ContratoServicosPrazoPlnj_Fim, T002U2_A1736ContratoServicosPrazoPlnj_Dias
               }
               , new Object[] {
               T002U3_A903ContratoServicosPrazo_CntSrvCod, T002U3_A1724ContratoServicosPrazoPlnj_Sequencial, T002U3_A1734ContratoServicosPrazoPlnj_Inicio, T002U3_A1735ContratoServicosPrazoPlnj_Fim, T002U3_A1736ContratoServicosPrazoPlnj_Dias
               }
               , new Object[] {
               T002U4_A903ContratoServicosPrazo_CntSrvCod, T002U4_A906ContratoServicosPrazoRegra_Sequencial, T002U4_A907ContratoServicosPrazoRegra_Inicio, T002U4_A908ContratoServicosPrazoRegra_Fim, T002U4_A909ContratoServicosPrazoRegra_Dias
               }
               , new Object[] {
               T002U5_A903ContratoServicosPrazo_CntSrvCod, T002U5_A906ContratoServicosPrazoRegra_Sequencial, T002U5_A907ContratoServicosPrazoRegra_Inicio, T002U5_A908ContratoServicosPrazoRegra_Fim, T002U5_A909ContratoServicosPrazoRegra_Dias
               }
               , new Object[] {
               T002U6_A905ContratoServicosPrazo_Dias, T002U6_n905ContratoServicosPrazo_Dias, T002U6_A904ContratoServicosPrazo_Tipo, T002U6_A1456ContratoServicosPrazo_Cada, T002U6_n1456ContratoServicosPrazo_Cada, T002U6_A1263ContratoServicosPrazo_DiasAlta, T002U6_n1263ContratoServicosPrazo_DiasAlta, T002U6_A1264ContratoServicosPrazo_DiasMedia, T002U6_n1264ContratoServicosPrazo_DiasMedia, T002U6_A1265ContratoServicosPrazo_DiasBaixa,
               T002U6_n1265ContratoServicosPrazo_DiasBaixa, T002U6_A1823ContratoServicosPrazo_Momento, T002U6_n1823ContratoServicosPrazo_Momento, T002U6_A903ContratoServicosPrazo_CntSrvCod
               }
               , new Object[] {
               T002U7_A905ContratoServicosPrazo_Dias, T002U7_n905ContratoServicosPrazo_Dias, T002U7_A904ContratoServicosPrazo_Tipo, T002U7_A1456ContratoServicosPrazo_Cada, T002U7_n1456ContratoServicosPrazo_Cada, T002U7_A1263ContratoServicosPrazo_DiasAlta, T002U7_n1263ContratoServicosPrazo_DiasAlta, T002U7_A1264ContratoServicosPrazo_DiasMedia, T002U7_n1264ContratoServicosPrazo_DiasMedia, T002U7_A1265ContratoServicosPrazo_DiasBaixa,
               T002U7_n1265ContratoServicosPrazo_DiasBaixa, T002U7_A1823ContratoServicosPrazo_Momento, T002U7_n1823ContratoServicosPrazo_Momento, T002U7_A903ContratoServicosPrazo_CntSrvCod
               }
               , new Object[] {
               T002U8_A1212ContratoServicos_UnidadeContratada, T002U8_n1212ContratoServicos_UnidadeContratada, T002U8_A1095ContratoServicosPrazo_ServicoCod, T002U8_n1095ContratoServicosPrazo_ServicoCod
               }
               , new Object[] {
               T002U9_A1096ContratoServicosPrazo_ServicoSigla, T002U9_n1096ContratoServicosPrazo_ServicoSigla
               }
               , new Object[] {
               T002U10_A1712ContratoServicos_UndCntSgl, T002U10_n1712ContratoServicos_UndCntSgl
               }
               , new Object[] {
               T002U12_A910ContratoServicosPrazo_QtdRegras, T002U12_n910ContratoServicosPrazo_QtdRegras
               }
               , new Object[] {
               T002U14_A1737ContratoServicosPrazo_QtdPlnj, T002U14_n1737ContratoServicosPrazo_QtdPlnj
               }
               , new Object[] {
               T002U17_A1212ContratoServicos_UnidadeContratada, T002U17_n1212ContratoServicos_UnidadeContratada, T002U17_A905ContratoServicosPrazo_Dias, T002U17_n905ContratoServicosPrazo_Dias, T002U17_A904ContratoServicosPrazo_Tipo, T002U17_A1456ContratoServicosPrazo_Cada, T002U17_n1456ContratoServicosPrazo_Cada, T002U17_A1712ContratoServicos_UndCntSgl, T002U17_n1712ContratoServicos_UndCntSgl, T002U17_A1096ContratoServicosPrazo_ServicoSigla,
               T002U17_n1096ContratoServicosPrazo_ServicoSigla, T002U17_A1263ContratoServicosPrazo_DiasAlta, T002U17_n1263ContratoServicosPrazo_DiasAlta, T002U17_A1264ContratoServicosPrazo_DiasMedia, T002U17_n1264ContratoServicosPrazo_DiasMedia, T002U17_A1265ContratoServicosPrazo_DiasBaixa, T002U17_n1265ContratoServicosPrazo_DiasBaixa, T002U17_A1823ContratoServicosPrazo_Momento, T002U17_n1823ContratoServicosPrazo_Momento, T002U17_A903ContratoServicosPrazo_CntSrvCod,
               T002U17_A1095ContratoServicosPrazo_ServicoCod, T002U17_n1095ContratoServicosPrazo_ServicoCod, T002U17_A910ContratoServicosPrazo_QtdRegras, T002U17_n910ContratoServicosPrazo_QtdRegras, T002U17_A1737ContratoServicosPrazo_QtdPlnj, T002U17_n1737ContratoServicosPrazo_QtdPlnj
               }
               , new Object[] {
               T002U18_A1212ContratoServicos_UnidadeContratada, T002U18_n1212ContratoServicos_UnidadeContratada, T002U18_A1095ContratoServicosPrazo_ServicoCod, T002U18_n1095ContratoServicosPrazo_ServicoCod
               }
               , new Object[] {
               T002U19_A1712ContratoServicos_UndCntSgl, T002U19_n1712ContratoServicos_UndCntSgl
               }
               , new Object[] {
               T002U20_A1096ContratoServicosPrazo_ServicoSigla, T002U20_n1096ContratoServicosPrazo_ServicoSigla
               }
               , new Object[] {
               T002U22_A910ContratoServicosPrazo_QtdRegras, T002U22_n910ContratoServicosPrazo_QtdRegras
               }
               , new Object[] {
               T002U24_A1737ContratoServicosPrazo_QtdPlnj, T002U24_n1737ContratoServicosPrazo_QtdPlnj
               }
               , new Object[] {
               T002U25_A903ContratoServicosPrazo_CntSrvCod
               }
               , new Object[] {
               T002U26_A903ContratoServicosPrazo_CntSrvCod
               }
               , new Object[] {
               T002U27_A903ContratoServicosPrazo_CntSrvCod
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T002U31_A1212ContratoServicos_UnidadeContratada, T002U31_n1212ContratoServicos_UnidadeContratada, T002U31_A1095ContratoServicosPrazo_ServicoCod, T002U31_n1095ContratoServicosPrazo_ServicoCod
               }
               , new Object[] {
               T002U32_A1712ContratoServicos_UndCntSgl, T002U32_n1712ContratoServicos_UndCntSgl
               }
               , new Object[] {
               T002U33_A1096ContratoServicosPrazo_ServicoSigla, T002U33_n1096ContratoServicosPrazo_ServicoSigla
               }
               , new Object[] {
               T002U35_A910ContratoServicosPrazo_QtdRegras, T002U35_n910ContratoServicosPrazo_QtdRegras
               }
               , new Object[] {
               T002U37_A1737ContratoServicosPrazo_QtdPlnj, T002U37_n1737ContratoServicosPrazo_QtdPlnj
               }
               , new Object[] {
               T002U38_A903ContratoServicosPrazo_CntSrvCod
               }
               , new Object[] {
               T002U39_A903ContratoServicosPrazo_CntSrvCod, T002U39_A906ContratoServicosPrazoRegra_Sequencial, T002U39_A907ContratoServicosPrazoRegra_Inicio, T002U39_A908ContratoServicosPrazoRegra_Fim, T002U39_A909ContratoServicosPrazoRegra_Dias
               }
               , new Object[] {
               T002U40_A903ContratoServicosPrazo_CntSrvCod, T002U40_A906ContratoServicosPrazoRegra_Sequencial
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T002U44_A903ContratoServicosPrazo_CntSrvCod, T002U44_A906ContratoServicosPrazoRegra_Sequencial
               }
               , new Object[] {
               T002U45_A903ContratoServicosPrazo_CntSrvCod, T002U45_A1724ContratoServicosPrazoPlnj_Sequencial, T002U45_A1734ContratoServicosPrazoPlnj_Inicio, T002U45_A1735ContratoServicosPrazoPlnj_Fim, T002U45_A1736ContratoServicosPrazoPlnj_Dias
               }
               , new Object[] {
               T002U46_A903ContratoServicosPrazo_CntSrvCod, T002U46_A1724ContratoServicosPrazoPlnj_Sequencial
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T002U50_A903ContratoServicosPrazo_CntSrvCod, T002U50_A1724ContratoServicosPrazoPlnj_Sequencial
               }
            }
         );
         AV14Pgmname = "ContratoServicosPrazo";
      }

      private short Z905ContratoServicosPrazo_Dias ;
      private short Z1263ContratoServicosPrazo_DiasAlta ;
      private short Z1264ContratoServicosPrazo_DiasMedia ;
      private short Z1265ContratoServicosPrazo_DiasBaixa ;
      private short Z1823ContratoServicosPrazo_Momento ;
      private short O1737ContratoServicosPrazo_QtdPlnj ;
      private short O910ContratoServicosPrazo_QtdRegras ;
      private short nRC_GXsfl_97 ;
      private short nGXsfl_97_idx=1 ;
      private short nRC_GXsfl_112 ;
      private short nGXsfl_112_idx=1 ;
      private short Z906ContratoServicosPrazoRegra_Sequencial ;
      private short Z909ContratoServicosPrazoRegra_Dias ;
      private short nRcdDeleted_113 ;
      private short nRcdExists_113 ;
      private short nIsMod_113 ;
      private short Z1724ContratoServicosPrazoPlnj_Sequencial ;
      private short Z1736ContratoServicosPrazoPlnj_Dias ;
      private short nRcdDeleted_188 ;
      private short nRcdExists_188 ;
      private short nIsMod_188 ;
      private short GxWebError ;
      private short gxcookieaux ;
      private short A1823ContratoServicosPrazo_Momento ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short subGrid2_Backcolorstyle ;
      private short A1724ContratoServicosPrazoPlnj_Sequencial ;
      private short A1736ContratoServicosPrazoPlnj_Dias ;
      private short subGrid2_Allowselection ;
      private short subGrid2_Allowhovering ;
      private short subGrid2_Allowcollapsing ;
      private short subGrid2_Collapsed ;
      private short nBlankRcdCount188 ;
      private short RcdFound188 ;
      private short B1737ContratoServicosPrazo_QtdPlnj ;
      private short A1737ContratoServicosPrazo_QtdPlnj ;
      private short B910ContratoServicosPrazo_QtdRegras ;
      private short A910ContratoServicosPrazo_QtdRegras ;
      private short nBlankRcdUsr188 ;
      private short subGrid_Backcolorstyle ;
      private short A906ContratoServicosPrazoRegra_Sequencial ;
      private short A909ContratoServicosPrazoRegra_Dias ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nBlankRcdCount113 ;
      private short RcdFound113 ;
      private short nBlankRcdUsr113 ;
      private short A1263ContratoServicosPrazo_DiasAlta ;
      private short A1264ContratoServicosPrazo_DiasMedia ;
      private short A1265ContratoServicosPrazo_DiasBaixa ;
      private short A905ContratoServicosPrazo_Dias ;
      private short Gx_BScreen ;
      private short RcdFound112 ;
      private short s1737ContratoServicosPrazo_QtdPlnj ;
      private short s910ContratoServicosPrazo_QtdRegras ;
      private short GX_JID ;
      private short Z910ContratoServicosPrazo_QtdRegras ;
      private short Z1737ContratoServicosPrazo_QtdPlnj ;
      private short subGrid_Backstyle ;
      private short subGrid2_Backstyle ;
      private short gxajaxcallmode ;
      private short i910ContratoServicosPrazo_QtdRegras ;
      private short i1737ContratoServicosPrazo_QtdPlnj ;
      private short wbTemp ;
      private int wcpOAV7ContratoServicosPrazo_CntSrvCod ;
      private int Z903ContratoServicosPrazo_CntSrvCod ;
      private int A903ContratoServicosPrazo_CntSrvCod ;
      private int A1212ContratoServicos_UnidadeContratada ;
      private int A1095ContratoServicosPrazo_ServicoCod ;
      private int AV7ContratoServicosPrazo_CntSrvCod ;
      private int trnEnded ;
      private int edtContratoServicosPrazo_CntSrvCod_Visible ;
      private int edtContratoServicosPrazo_CntSrvCod_Enabled ;
      private int bttBtn_trn_enter_Visible ;
      private int bttBtn_trn_enter_Enabled ;
      private int bttBtn_trn_cancel_Visible ;
      private int bttBtn_trn_delete_Visible ;
      private int bttBtn_trn_delete_Enabled ;
      private int edtContratoServicosPrazoPlnj_Sequencial_Enabled ;
      private int edtContratoServicosPrazoPlnj_Inicio_Enabled ;
      private int edtContratoServicosPrazoPlnj_Fim_Enabled ;
      private int edtContratoServicosPrazoPlnj_Dias_Enabled ;
      private int subGrid2_Selectioncolor ;
      private int subGrid2_Hoveringcolor ;
      private int fRowAdded ;
      private int tblTblregras_Visible ;
      private int edtContratoServicosPrazoRegra_Sequencial_Enabled ;
      private int edtContratoServicosPrazoRegra_Inicio_Enabled ;
      private int edtContratoServicosPrazoRegra_Fim_Enabled ;
      private int edtContratoServicosPrazoRegra_Dias_Enabled ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int tblTblcomplexidade_Visible ;
      private int edtContratoServicosPrazo_DiasAlta_Enabled ;
      private int edtContratoServicosPrazo_DiasMedia_Enabled ;
      private int edtContratoServicosPrazo_DiasBaixa_Enabled ;
      private int lblTextblockcontratoservicosprazo_dias_Visible ;
      private int edtContratoServicosPrazo_Dias_Enabled ;
      private int edtContratoServicosPrazo_Dias_Visible ;
      private int lblTextblockcontratoservicosprazo_cada_Visible ;
      private int edtContratoServicosPrazo_Cada_Enabled ;
      private int edtContratoServicosPrazo_Cada_Visible ;
      private int edtContratoServicos_UndCntSgl_Visible ;
      private int edtContratoServicos_UndCntSgl_Enabled ;
      private int edtavServico_sigla_Enabled ;
      private int Z1212ContratoServicos_UnidadeContratada ;
      private int Z1095ContratoServicosPrazo_ServicoCod ;
      private int subGrid_Backcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid2_Backcolor ;
      private int subGrid2_Allbackcolor ;
      private int defedtContratoServicosPrazoRegra_Sequencial_Enabled ;
      private int defedtContratoServicosPrazoPlnj_Sequencial_Enabled ;
      private int idxLst ;
      private long GRID_nFirstRecordOnPage ;
      private long GRID2_nFirstRecordOnPage ;
      private decimal Z1456ContratoServicosPrazo_Cada ;
      private decimal Z907ContratoServicosPrazoRegra_Inicio ;
      private decimal Z908ContratoServicosPrazoRegra_Fim ;
      private decimal Z1734ContratoServicosPrazoPlnj_Inicio ;
      private decimal Z1735ContratoServicosPrazoPlnj_Fim ;
      private decimal A1734ContratoServicosPrazoPlnj_Inicio ;
      private decimal A1735ContratoServicosPrazoPlnj_Fim ;
      private decimal A907ContratoServicosPrazoRegra_Inicio ;
      private decimal A908ContratoServicosPrazoRegra_Fim ;
      private decimal A1456ContratoServicosPrazo_Cada ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String Z904ContratoServicosPrazo_Tipo ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_97_idx="0001" ;
      private String sGXsfl_112_idx="0001" ;
      private String Gx_mode ;
      private String GXKey ;
      private String A904ContratoServicosPrazo_Tipo ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String cmbContratoServicosPrazo_Tipo_Internalname ;
      private String TempTags ;
      private String edtContratoServicosPrazo_CntSrvCod_Internalname ;
      private String edtContratoServicosPrazo_CntSrvCod_Jsonclick ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTableactions_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String bttBtn_trn_delete_Internalname ;
      private String bttBtn_trn_delete_Jsonclick ;
      private String tblTblplanejamento_Internalname ;
      private String tblPlanejamento_Internalname ;
      private String sMode188 ;
      private String edtContratoServicosPrazoPlnj_Sequencial_Internalname ;
      private String edtContratoServicosPrazoPlnj_Inicio_Internalname ;
      private String edtContratoServicosPrazoPlnj_Fim_Internalname ;
      private String edtContratoServicosPrazoPlnj_Dias_Internalname ;
      private String tblTblregras_Internalname ;
      private String tblRegras_Internalname ;
      private String sMode113 ;
      private String edtContratoServicosPrazoRegra_Sequencial_Internalname ;
      private String edtContratoServicosPrazoRegra_Inicio_Internalname ;
      private String edtContratoServicosPrazoRegra_Fim_Internalname ;
      private String edtContratoServicosPrazoRegra_Dias_Internalname ;
      private String tblTablecontent_Internalname ;
      private String tblTblcomplexidade_Internalname ;
      private String tblComplexidade_Internalname ;
      private String lblTextblockcontratoservicosprazo_diasbaixa_Internalname ;
      private String lblTextblockcontratoservicosprazo_diasbaixa_Jsonclick ;
      private String lblTextblockcontratoservicosprazo_diasmedia_Internalname ;
      private String lblTextblockcontratoservicosprazo_diasmedia_Jsonclick ;
      private String lblTextblockcontratoservicosprazo_diasalta_Internalname ;
      private String lblTextblockcontratoservicosprazo_diasalta_Jsonclick ;
      private String tblTablemergedcontratoservicosprazo_diasalta_Internalname ;
      private String edtContratoServicosPrazo_DiasAlta_Internalname ;
      private String edtContratoServicosPrazo_DiasAlta_Jsonclick ;
      private String lblContratoservicosprazo_diasalta_righttext_Internalname ;
      private String lblContratoservicosprazo_diasalta_righttext_Jsonclick ;
      private String tblTablemergedcontratoservicosprazo_diasmedia_Internalname ;
      private String edtContratoServicosPrazo_DiasMedia_Internalname ;
      private String edtContratoServicosPrazo_DiasMedia_Jsonclick ;
      private String lblContratoservicosprazo_diasmedia_righttext_Internalname ;
      private String lblContratoservicosprazo_diasmedia_righttext_Jsonclick ;
      private String tblTablemergedcontratoservicosprazo_diasbaixa_Internalname ;
      private String edtContratoServicosPrazo_DiasBaixa_Internalname ;
      private String edtContratoServicosPrazo_DiasBaixa_Jsonclick ;
      private String lblContratoservicosprazo_diasbaixa_righttext_Internalname ;
      private String lblContratoservicosprazo_diasbaixa_righttext_Jsonclick ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockcontratoservicosprazo_tipo_Internalname ;
      private String lblTextblockcontratoservicosprazo_tipo_Jsonclick ;
      private String cmbContratoServicosPrazo_Tipo_Jsonclick ;
      private String lblTextblockcontratoservicosprazo_dias_Internalname ;
      private String lblTextblockcontratoservicosprazo_dias_Jsonclick ;
      private String edtContratoServicosPrazo_Dias_Internalname ;
      private String edtContratoServicosPrazo_Dias_Jsonclick ;
      private String lblTextblockcontratoservicosprazo_momento_Internalname ;
      private String lblTextblockcontratoservicosprazo_momento_Jsonclick ;
      private String cmbContratoServicosPrazo_Momento_Internalname ;
      private String cmbContratoServicosPrazo_Momento_Jsonclick ;
      private String lblTextblockcontratoservicosprazo_cada_Internalname ;
      private String lblTextblockcontratoservicosprazo_cada_Jsonclick ;
      private String tblTablemergedcontratoservicosprazo_cada_Internalname ;
      private String edtContratoServicosPrazo_Cada_Internalname ;
      private String edtContratoServicosPrazo_Cada_Jsonclick ;
      private String edtContratoServicos_UndCntSgl_Internalname ;
      private String A1712ContratoServicos_UndCntSgl ;
      private String edtContratoServicos_UndCntSgl_Jsonclick ;
      private String tblTablemergedviewtitle_Internalname ;
      private String lblViewtitle_Internalname ;
      private String lblViewtitle_Jsonclick ;
      private String edtavServico_sigla_Internalname ;
      private String AV11Servico_Sigla ;
      private String edtavServico_sigla_Jsonclick ;
      private String A1096ContratoServicosPrazo_ServicoSigla ;
      private String AV14Pgmname ;
      private String Dvpanel_tableattributes_Width ;
      private String Dvpanel_tableattributes_Height ;
      private String Dvpanel_tableattributes_Cls ;
      private String Dvpanel_tableattributes_Title ;
      private String Dvpanel_tableattributes_Class ;
      private String Dvpanel_tableattributes_Iconposition ;
      private String Dvpanel_complexidade_Width ;
      private String Dvpanel_complexidade_Height ;
      private String Dvpanel_complexidade_Cls ;
      private String Dvpanel_complexidade_Title ;
      private String Dvpanel_complexidade_Class ;
      private String Dvpanel_complexidade_Iconposition ;
      private String Dvpanel_regras_Width ;
      private String Dvpanel_regras_Height ;
      private String Dvpanel_regras_Cls ;
      private String Dvpanel_regras_Title ;
      private String Dvpanel_regras_Class ;
      private String Dvpanel_regras_Iconposition ;
      private String Dvpanel_planejamento_Width ;
      private String Dvpanel_planejamento_Height ;
      private String Dvpanel_planejamento_Cls ;
      private String Dvpanel_planejamento_Title ;
      private String Dvpanel_planejamento_Class ;
      private String Dvpanel_planejamento_Iconposition ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode112 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String Z1712ContratoServicos_UndCntSgl ;
      private String Z1096ContratoServicosPrazo_ServicoSigla ;
      private String GXCCtl ;
      private String sGXsfl_97_fel_idx="0001" ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String ROClassString ;
      private String edtContratoServicosPrazoRegra_Sequencial_Jsonclick ;
      private String edtContratoServicosPrazoRegra_Inicio_Jsonclick ;
      private String edtContratoServicosPrazoRegra_Fim_Jsonclick ;
      private String edtContratoServicosPrazoRegra_Dias_Jsonclick ;
      private String sGXsfl_112_fel_idx="0001" ;
      private String subGrid2_Class ;
      private String subGrid2_Linesclass ;
      private String edtContratoServicosPrazoPlnj_Sequencial_Jsonclick ;
      private String edtContratoServicosPrazoPlnj_Inicio_Jsonclick ;
      private String edtContratoServicosPrazoPlnj_Fim_Jsonclick ;
      private String edtContratoServicosPrazoPlnj_Dias_Jsonclick ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Dvpanel_tableattributes_Internalname ;
      private String Dvpanel_complexidade_Internalname ;
      private String Dvpanel_regras_Internalname ;
      private String Dvpanel_planejamento_Internalname ;
      private String subGrid_Internalname ;
      private String subGrid2_Internalname ;
      private bool entryPointCalled ;
      private bool n1212ContratoServicos_UnidadeContratada ;
      private bool n1095ContratoServicosPrazo_ServicoCod ;
      private bool toggleJsOutput ;
      private bool n1823ContratoServicosPrazo_Momento ;
      private bool wbErr ;
      private bool n1737ContratoServicosPrazo_QtdPlnj ;
      private bool n910ContratoServicosPrazo_QtdRegras ;
      private bool n905ContratoServicosPrazo_Dias ;
      private bool n1456ContratoServicosPrazo_Cada ;
      private bool n1712ContratoServicos_UndCntSgl ;
      private bool n1265ContratoServicosPrazo_DiasBaixa ;
      private bool n1264ContratoServicosPrazo_DiasMedia ;
      private bool n1263ContratoServicosPrazo_DiasAlta ;
      private bool n1096ContratoServicosPrazo_ServicoSigla ;
      private bool Dvpanel_tableattributes_Collapsible ;
      private bool Dvpanel_tableattributes_Collapsed ;
      private bool Dvpanel_tableattributes_Enabled ;
      private bool Dvpanel_tableattributes_Autowidth ;
      private bool Dvpanel_tableattributes_Autoheight ;
      private bool Dvpanel_tableattributes_Showheader ;
      private bool Dvpanel_tableattributes_Showcollapseicon ;
      private bool Dvpanel_tableattributes_Autoscroll ;
      private bool Dvpanel_tableattributes_Visible ;
      private bool Dvpanel_complexidade_Collapsible ;
      private bool Dvpanel_complexidade_Collapsed ;
      private bool Dvpanel_complexidade_Enabled ;
      private bool Dvpanel_complexidade_Autowidth ;
      private bool Dvpanel_complexidade_Autoheight ;
      private bool Dvpanel_complexidade_Showheader ;
      private bool Dvpanel_complexidade_Showcollapseicon ;
      private bool Dvpanel_complexidade_Autoscroll ;
      private bool Dvpanel_complexidade_Visible ;
      private bool Dvpanel_regras_Collapsible ;
      private bool Dvpanel_regras_Collapsed ;
      private bool Dvpanel_regras_Enabled ;
      private bool Dvpanel_regras_Autowidth ;
      private bool Dvpanel_regras_Autoheight ;
      private bool Dvpanel_regras_Showheader ;
      private bool Dvpanel_regras_Showcollapseicon ;
      private bool Dvpanel_regras_Autoscroll ;
      private bool Dvpanel_regras_Visible ;
      private bool Dvpanel_planejamento_Collapsible ;
      private bool Dvpanel_planejamento_Collapsed ;
      private bool Dvpanel_planejamento_Enabled ;
      private bool Dvpanel_planejamento_Autowidth ;
      private bool Dvpanel_planejamento_Autoheight ;
      private bool Dvpanel_planejamento_Showheader ;
      private bool Dvpanel_planejamento_Showcollapseicon ;
      private bool Dvpanel_planejamento_Autoscroll ;
      private bool Dvpanel_planejamento_Visible ;
      private bool returnInSub ;
      private bool Gx_longc ;
      private IGxSession AV10WebSession ;
      private GxUnknownObjectCollection isValidOutput ;
      private GXWebGrid Grid2Container ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebRow Grid2Row ;
      private GXWebColumn Grid2Column ;
      private GXWebColumn GridColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbContratoServicosPrazo_Tipo ;
      private GXCombobox cmbContratoServicosPrazo_Momento ;
      private IDataStoreProvider pr_default ;
      private int[] T002U8_A1212ContratoServicos_UnidadeContratada ;
      private bool[] T002U8_n1212ContratoServicos_UnidadeContratada ;
      private int[] T002U8_A1095ContratoServicosPrazo_ServicoCod ;
      private bool[] T002U8_n1095ContratoServicosPrazo_ServicoCod ;
      private String[] T002U10_A1712ContratoServicos_UndCntSgl ;
      private bool[] T002U10_n1712ContratoServicos_UndCntSgl ;
      private String[] T002U9_A1096ContratoServicosPrazo_ServicoSigla ;
      private bool[] T002U9_n1096ContratoServicosPrazo_ServicoSigla ;
      private short[] T002U12_A910ContratoServicosPrazo_QtdRegras ;
      private bool[] T002U12_n910ContratoServicosPrazo_QtdRegras ;
      private short[] T002U14_A1737ContratoServicosPrazo_QtdPlnj ;
      private bool[] T002U14_n1737ContratoServicosPrazo_QtdPlnj ;
      private int[] T002U17_A1212ContratoServicos_UnidadeContratada ;
      private bool[] T002U17_n1212ContratoServicos_UnidadeContratada ;
      private short[] T002U17_A905ContratoServicosPrazo_Dias ;
      private bool[] T002U17_n905ContratoServicosPrazo_Dias ;
      private String[] T002U17_A904ContratoServicosPrazo_Tipo ;
      private decimal[] T002U17_A1456ContratoServicosPrazo_Cada ;
      private bool[] T002U17_n1456ContratoServicosPrazo_Cada ;
      private String[] T002U17_A1712ContratoServicos_UndCntSgl ;
      private bool[] T002U17_n1712ContratoServicos_UndCntSgl ;
      private String[] T002U17_A1096ContratoServicosPrazo_ServicoSigla ;
      private bool[] T002U17_n1096ContratoServicosPrazo_ServicoSigla ;
      private short[] T002U17_A1263ContratoServicosPrazo_DiasAlta ;
      private bool[] T002U17_n1263ContratoServicosPrazo_DiasAlta ;
      private short[] T002U17_A1264ContratoServicosPrazo_DiasMedia ;
      private bool[] T002U17_n1264ContratoServicosPrazo_DiasMedia ;
      private short[] T002U17_A1265ContratoServicosPrazo_DiasBaixa ;
      private bool[] T002U17_n1265ContratoServicosPrazo_DiasBaixa ;
      private short[] T002U17_A1823ContratoServicosPrazo_Momento ;
      private bool[] T002U17_n1823ContratoServicosPrazo_Momento ;
      private int[] T002U17_A903ContratoServicosPrazo_CntSrvCod ;
      private int[] T002U17_A1095ContratoServicosPrazo_ServicoCod ;
      private bool[] T002U17_n1095ContratoServicosPrazo_ServicoCod ;
      private short[] T002U17_A910ContratoServicosPrazo_QtdRegras ;
      private bool[] T002U17_n910ContratoServicosPrazo_QtdRegras ;
      private short[] T002U17_A1737ContratoServicosPrazo_QtdPlnj ;
      private bool[] T002U17_n1737ContratoServicosPrazo_QtdPlnj ;
      private int[] T002U18_A1212ContratoServicos_UnidadeContratada ;
      private bool[] T002U18_n1212ContratoServicos_UnidadeContratada ;
      private int[] T002U18_A1095ContratoServicosPrazo_ServicoCod ;
      private bool[] T002U18_n1095ContratoServicosPrazo_ServicoCod ;
      private String[] T002U19_A1712ContratoServicos_UndCntSgl ;
      private bool[] T002U19_n1712ContratoServicos_UndCntSgl ;
      private String[] T002U20_A1096ContratoServicosPrazo_ServicoSigla ;
      private bool[] T002U20_n1096ContratoServicosPrazo_ServicoSigla ;
      private short[] T002U22_A910ContratoServicosPrazo_QtdRegras ;
      private bool[] T002U22_n910ContratoServicosPrazo_QtdRegras ;
      private short[] T002U24_A1737ContratoServicosPrazo_QtdPlnj ;
      private bool[] T002U24_n1737ContratoServicosPrazo_QtdPlnj ;
      private int[] T002U25_A903ContratoServicosPrazo_CntSrvCod ;
      private short[] T002U7_A905ContratoServicosPrazo_Dias ;
      private bool[] T002U7_n905ContratoServicosPrazo_Dias ;
      private String[] T002U7_A904ContratoServicosPrazo_Tipo ;
      private decimal[] T002U7_A1456ContratoServicosPrazo_Cada ;
      private bool[] T002U7_n1456ContratoServicosPrazo_Cada ;
      private short[] T002U7_A1263ContratoServicosPrazo_DiasAlta ;
      private bool[] T002U7_n1263ContratoServicosPrazo_DiasAlta ;
      private short[] T002U7_A1264ContratoServicosPrazo_DiasMedia ;
      private bool[] T002U7_n1264ContratoServicosPrazo_DiasMedia ;
      private short[] T002U7_A1265ContratoServicosPrazo_DiasBaixa ;
      private bool[] T002U7_n1265ContratoServicosPrazo_DiasBaixa ;
      private short[] T002U7_A1823ContratoServicosPrazo_Momento ;
      private bool[] T002U7_n1823ContratoServicosPrazo_Momento ;
      private int[] T002U7_A903ContratoServicosPrazo_CntSrvCod ;
      private int[] T002U26_A903ContratoServicosPrazo_CntSrvCod ;
      private int[] T002U27_A903ContratoServicosPrazo_CntSrvCod ;
      private short[] T002U6_A905ContratoServicosPrazo_Dias ;
      private bool[] T002U6_n905ContratoServicosPrazo_Dias ;
      private String[] T002U6_A904ContratoServicosPrazo_Tipo ;
      private decimal[] T002U6_A1456ContratoServicosPrazo_Cada ;
      private bool[] T002U6_n1456ContratoServicosPrazo_Cada ;
      private short[] T002U6_A1263ContratoServicosPrazo_DiasAlta ;
      private bool[] T002U6_n1263ContratoServicosPrazo_DiasAlta ;
      private short[] T002U6_A1264ContratoServicosPrazo_DiasMedia ;
      private bool[] T002U6_n1264ContratoServicosPrazo_DiasMedia ;
      private short[] T002U6_A1265ContratoServicosPrazo_DiasBaixa ;
      private bool[] T002U6_n1265ContratoServicosPrazo_DiasBaixa ;
      private short[] T002U6_A1823ContratoServicosPrazo_Momento ;
      private bool[] T002U6_n1823ContratoServicosPrazo_Momento ;
      private int[] T002U6_A903ContratoServicosPrazo_CntSrvCod ;
      private int[] T002U31_A1212ContratoServicos_UnidadeContratada ;
      private bool[] T002U31_n1212ContratoServicos_UnidadeContratada ;
      private int[] T002U31_A1095ContratoServicosPrazo_ServicoCod ;
      private bool[] T002U31_n1095ContratoServicosPrazo_ServicoCod ;
      private String[] T002U32_A1712ContratoServicos_UndCntSgl ;
      private bool[] T002U32_n1712ContratoServicos_UndCntSgl ;
      private String[] T002U33_A1096ContratoServicosPrazo_ServicoSigla ;
      private bool[] T002U33_n1096ContratoServicosPrazo_ServicoSigla ;
      private short[] T002U35_A910ContratoServicosPrazo_QtdRegras ;
      private bool[] T002U35_n910ContratoServicosPrazo_QtdRegras ;
      private short[] T002U37_A1737ContratoServicosPrazo_QtdPlnj ;
      private bool[] T002U37_n1737ContratoServicosPrazo_QtdPlnj ;
      private int[] T002U38_A903ContratoServicosPrazo_CntSrvCod ;
      private int[] T002U39_A903ContratoServicosPrazo_CntSrvCod ;
      private short[] T002U39_A906ContratoServicosPrazoRegra_Sequencial ;
      private decimal[] T002U39_A907ContratoServicosPrazoRegra_Inicio ;
      private decimal[] T002U39_A908ContratoServicosPrazoRegra_Fim ;
      private short[] T002U39_A909ContratoServicosPrazoRegra_Dias ;
      private int[] T002U40_A903ContratoServicosPrazo_CntSrvCod ;
      private short[] T002U40_A906ContratoServicosPrazoRegra_Sequencial ;
      private int[] T002U5_A903ContratoServicosPrazo_CntSrvCod ;
      private short[] T002U5_A906ContratoServicosPrazoRegra_Sequencial ;
      private decimal[] T002U5_A907ContratoServicosPrazoRegra_Inicio ;
      private decimal[] T002U5_A908ContratoServicosPrazoRegra_Fim ;
      private short[] T002U5_A909ContratoServicosPrazoRegra_Dias ;
      private int[] T002U4_A903ContratoServicosPrazo_CntSrvCod ;
      private short[] T002U4_A906ContratoServicosPrazoRegra_Sequencial ;
      private decimal[] T002U4_A907ContratoServicosPrazoRegra_Inicio ;
      private decimal[] T002U4_A908ContratoServicosPrazoRegra_Fim ;
      private short[] T002U4_A909ContratoServicosPrazoRegra_Dias ;
      private int[] T002U44_A903ContratoServicosPrazo_CntSrvCod ;
      private short[] T002U44_A906ContratoServicosPrazoRegra_Sequencial ;
      private int[] T002U45_A903ContratoServicosPrazo_CntSrvCod ;
      private short[] T002U45_A1724ContratoServicosPrazoPlnj_Sequencial ;
      private decimal[] T002U45_A1734ContratoServicosPrazoPlnj_Inicio ;
      private decimal[] T002U45_A1735ContratoServicosPrazoPlnj_Fim ;
      private short[] T002U45_A1736ContratoServicosPrazoPlnj_Dias ;
      private int[] T002U46_A903ContratoServicosPrazo_CntSrvCod ;
      private short[] T002U46_A1724ContratoServicosPrazoPlnj_Sequencial ;
      private int[] T002U3_A903ContratoServicosPrazo_CntSrvCod ;
      private short[] T002U3_A1724ContratoServicosPrazoPlnj_Sequencial ;
      private decimal[] T002U3_A1734ContratoServicosPrazoPlnj_Inicio ;
      private decimal[] T002U3_A1735ContratoServicosPrazoPlnj_Fim ;
      private short[] T002U3_A1736ContratoServicosPrazoPlnj_Dias ;
      private int[] T002U2_A903ContratoServicosPrazo_CntSrvCod ;
      private short[] T002U2_A1724ContratoServicosPrazoPlnj_Sequencial ;
      private decimal[] T002U2_A1734ContratoServicosPrazoPlnj_Inicio ;
      private decimal[] T002U2_A1735ContratoServicosPrazoPlnj_Fim ;
      private short[] T002U2_A1736ContratoServicosPrazoPlnj_Dias ;
      private int[] T002U50_A903ContratoServicosPrazo_CntSrvCod ;
      private short[] T002U50_A1724ContratoServicosPrazoPlnj_Sequencial ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
      private wwpbaseobjects.SdtAuditingObject AV12AuditingObject ;
   }

   public class contratoservicosprazo__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
         ,new ForEachCursor(def[16])
         ,new ForEachCursor(def[17])
         ,new ForEachCursor(def[18])
         ,new ForEachCursor(def[19])
         ,new UpdateCursor(def[20])
         ,new UpdateCursor(def[21])
         ,new UpdateCursor(def[22])
         ,new ForEachCursor(def[23])
         ,new ForEachCursor(def[24])
         ,new ForEachCursor(def[25])
         ,new ForEachCursor(def[26])
         ,new ForEachCursor(def[27])
         ,new ForEachCursor(def[28])
         ,new ForEachCursor(def[29])
         ,new ForEachCursor(def[30])
         ,new UpdateCursor(def[31])
         ,new UpdateCursor(def[32])
         ,new UpdateCursor(def[33])
         ,new ForEachCursor(def[34])
         ,new ForEachCursor(def[35])
         ,new ForEachCursor(def[36])
         ,new UpdateCursor(def[37])
         ,new UpdateCursor(def[38])
         ,new UpdateCursor(def[39])
         ,new ForEachCursor(def[40])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT002U17 ;
          prmT002U17 = new Object[] {
          new Object[] {"@ContratoServicosPrazo_CntSrvCod",SqlDbType.Int,6,0}
          } ;
          String cmdBufferT002U17 ;
          cmdBufferT002U17=" SELECT T2.[ContratoServicos_UnidadeContratada] AS ContratoServicos_UnidadeContratada, TM1.[ContratoServicosPrazo_Dias], TM1.[ContratoServicosPrazo_Tipo], TM1.[ContratoServicosPrazo_Cada], T3.[UnidadeMedicao_Sigla] AS ContratoServicos_UndCntSgl, T4.[Servico_Sigla] AS ContratoServicosPrazo_ServicoSigla, TM1.[ContratoServicosPrazo_DiasAlta], TM1.[ContratoServicosPrazo_DiasMedia], TM1.[ContratoServicosPrazo_DiasBaixa], TM1.[ContratoServicosPrazo_Momento], TM1.[ContratoServicosPrazo_CntSrvCod] AS ContratoServicosPrazo_CntSrvCod, T2.[Servico_Codigo] AS ContratoServicosPrazo_ServicoCod, COALESCE( T5.[ContratoServicosPrazo_QtdRegras], 0) AS ContratoServicosPrazo_QtdRegras, COALESCE( T6.[ContratoServicosPrazo_QtdPlnj], 0) AS ContratoServicosPrazo_QtdPlnj FROM ((((([ContratoServicosPrazo] TM1 WITH (NOLOCK) INNER JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = TM1.[ContratoServicosPrazo_CntSrvCod]) LEFT JOIN [UnidadeMedicao] T3 WITH (NOLOCK) ON T3.[UnidadeMedicao_Codigo] = T2.[ContratoServicos_UnidadeContratada]) LEFT JOIN [Servico] T4 WITH (NOLOCK) ON T4.[Servico_Codigo] = T2.[Servico_Codigo]) LEFT JOIN (SELECT COUNT(*) AS ContratoServicosPrazo_QtdRegras, [ContratoServicosPrazo_CntSrvCod] AS ContratoServicosPrazo_CntSrvCod FROM [ContratoServicosPrazoContratoServicosPrazoRegra] WITH (NOLOCK) GROUP BY [ContratoServicosPrazo_CntSrvCod] ) T5 ON T5.[ContratoServicosPrazo_CntSrvCod] = TM1.[ContratoServicosPrazo_CntSrvCod]) LEFT JOIN (SELECT COUNT(*) AS ContratoServicosPrazo_QtdPlnj, [ContratoServicosPrazo_CntSrvCod] AS ContratoServicosPrazo_CntSrvCod FROM [ContratoServicosPrazoPrazoPlnj] WITH (NOLOCK) GROUP BY [ContratoServicosPrazo_CntSrvCod] ) T6 ON T6.[ContratoServicosPrazo_CntSrvCod] = TM1.[ContratoServicosPrazo_CntSrvCod]) WHERE TM1.[ContratoServicosPrazo_CntSrvCod] "
          + " = @ContratoServicosPrazo_CntSrvCod ORDER BY TM1.[ContratoServicosPrazo_CntSrvCod]  OPTION (FAST 100)" ;
          Object[] prmT002U8 ;
          prmT002U8 = new Object[] {
          new Object[] {"@ContratoServicosPrazo_CntSrvCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002U10 ;
          prmT002U10 = new Object[] {
          new Object[] {"@ContratoServicos_UnidadeContratada",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002U9 ;
          prmT002U9 = new Object[] {
          new Object[] {"@ContratoServicosPrazo_ServicoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002U12 ;
          prmT002U12 = new Object[] {
          new Object[] {"@ContratoServicosPrazo_CntSrvCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002U14 ;
          prmT002U14 = new Object[] {
          new Object[] {"@ContratoServicosPrazo_CntSrvCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002U18 ;
          prmT002U18 = new Object[] {
          new Object[] {"@ContratoServicosPrazo_CntSrvCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002U19 ;
          prmT002U19 = new Object[] {
          new Object[] {"@ContratoServicos_UnidadeContratada",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002U20 ;
          prmT002U20 = new Object[] {
          new Object[] {"@ContratoServicosPrazo_ServicoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002U22 ;
          prmT002U22 = new Object[] {
          new Object[] {"@ContratoServicosPrazo_CntSrvCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002U24 ;
          prmT002U24 = new Object[] {
          new Object[] {"@ContratoServicosPrazo_CntSrvCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002U25 ;
          prmT002U25 = new Object[] {
          new Object[] {"@ContratoServicosPrazo_CntSrvCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002U7 ;
          prmT002U7 = new Object[] {
          new Object[] {"@ContratoServicosPrazo_CntSrvCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002U26 ;
          prmT002U26 = new Object[] {
          new Object[] {"@ContratoServicosPrazo_CntSrvCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002U27 ;
          prmT002U27 = new Object[] {
          new Object[] {"@ContratoServicosPrazo_CntSrvCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002U6 ;
          prmT002U6 = new Object[] {
          new Object[] {"@ContratoServicosPrazo_CntSrvCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002U28 ;
          prmT002U28 = new Object[] {
          new Object[] {"@ContratoServicosPrazo_Dias",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@ContratoServicosPrazo_Tipo",SqlDbType.Char,20,0} ,
          new Object[] {"@ContratoServicosPrazo_Cada",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContratoServicosPrazo_DiasAlta",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@ContratoServicosPrazo_DiasMedia",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@ContratoServicosPrazo_DiasBaixa",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@ContratoServicosPrazo_Momento",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContratoServicosPrazo_CntSrvCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002U29 ;
          prmT002U29 = new Object[] {
          new Object[] {"@ContratoServicosPrazo_Dias",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@ContratoServicosPrazo_Tipo",SqlDbType.Char,20,0} ,
          new Object[] {"@ContratoServicosPrazo_Cada",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContratoServicosPrazo_DiasAlta",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@ContratoServicosPrazo_DiasMedia",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@ContratoServicosPrazo_DiasBaixa",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@ContratoServicosPrazo_Momento",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContratoServicosPrazo_CntSrvCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002U30 ;
          prmT002U30 = new Object[] {
          new Object[] {"@ContratoServicosPrazo_CntSrvCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002U38 ;
          prmT002U38 = new Object[] {
          } ;
          Object[] prmT002U39 ;
          prmT002U39 = new Object[] {
          new Object[] {"@ContratoServicosPrazo_CntSrvCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosPrazoRegra_Sequencial",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT002U40 ;
          prmT002U40 = new Object[] {
          new Object[] {"@ContratoServicosPrazo_CntSrvCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosPrazoRegra_Sequencial",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT002U5 ;
          prmT002U5 = new Object[] {
          new Object[] {"@ContratoServicosPrazo_CntSrvCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosPrazoRegra_Sequencial",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT002U4 ;
          prmT002U4 = new Object[] {
          new Object[] {"@ContratoServicosPrazo_CntSrvCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosPrazoRegra_Sequencial",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT002U41 ;
          prmT002U41 = new Object[] {
          new Object[] {"@ContratoServicosPrazo_CntSrvCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosPrazoRegra_Sequencial",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContratoServicosPrazoRegra_Inicio",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContratoServicosPrazoRegra_Fim",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContratoServicosPrazoRegra_Dias",SqlDbType.SmallInt,3,0}
          } ;
          Object[] prmT002U42 ;
          prmT002U42 = new Object[] {
          new Object[] {"@ContratoServicosPrazoRegra_Inicio",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContratoServicosPrazoRegra_Fim",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContratoServicosPrazoRegra_Dias",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@ContratoServicosPrazo_CntSrvCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosPrazoRegra_Sequencial",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT002U43 ;
          prmT002U43 = new Object[] {
          new Object[] {"@ContratoServicosPrazo_CntSrvCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosPrazoRegra_Sequencial",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT002U44 ;
          prmT002U44 = new Object[] {
          new Object[] {"@ContratoServicosPrazo_CntSrvCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002U45 ;
          prmT002U45 = new Object[] {
          new Object[] {"@ContratoServicosPrazo_CntSrvCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosPrazoPlnj_Sequencial",SqlDbType.SmallInt,3,0}
          } ;
          Object[] prmT002U46 ;
          prmT002U46 = new Object[] {
          new Object[] {"@ContratoServicosPrazo_CntSrvCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosPrazoPlnj_Sequencial",SqlDbType.SmallInt,3,0}
          } ;
          Object[] prmT002U3 ;
          prmT002U3 = new Object[] {
          new Object[] {"@ContratoServicosPrazo_CntSrvCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosPrazoPlnj_Sequencial",SqlDbType.SmallInt,3,0}
          } ;
          Object[] prmT002U2 ;
          prmT002U2 = new Object[] {
          new Object[] {"@ContratoServicosPrazo_CntSrvCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosPrazoPlnj_Sequencial",SqlDbType.SmallInt,3,0}
          } ;
          Object[] prmT002U47 ;
          prmT002U47 = new Object[] {
          new Object[] {"@ContratoServicosPrazo_CntSrvCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosPrazoPlnj_Sequencial",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@ContratoServicosPrazoPlnj_Inicio",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContratoServicosPrazoPlnj_Fim",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContratoServicosPrazoPlnj_Dias",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT002U48 ;
          prmT002U48 = new Object[] {
          new Object[] {"@ContratoServicosPrazoPlnj_Inicio",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContratoServicosPrazoPlnj_Fim",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContratoServicosPrazoPlnj_Dias",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContratoServicosPrazo_CntSrvCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosPrazoPlnj_Sequencial",SqlDbType.SmallInt,3,0}
          } ;
          Object[] prmT002U49 ;
          prmT002U49 = new Object[] {
          new Object[] {"@ContratoServicosPrazo_CntSrvCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosPrazoPlnj_Sequencial",SqlDbType.SmallInt,3,0}
          } ;
          Object[] prmT002U50 ;
          prmT002U50 = new Object[] {
          new Object[] {"@ContratoServicosPrazo_CntSrvCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002U31 ;
          prmT002U31 = new Object[] {
          new Object[] {"@ContratoServicosPrazo_CntSrvCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002U32 ;
          prmT002U32 = new Object[] {
          new Object[] {"@ContratoServicos_UnidadeContratada",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002U33 ;
          prmT002U33 = new Object[] {
          new Object[] {"@ContratoServicosPrazo_ServicoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002U35 ;
          prmT002U35 = new Object[] {
          new Object[] {"@ContratoServicosPrazo_CntSrvCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002U37 ;
          prmT002U37 = new Object[] {
          new Object[] {"@ContratoServicosPrazo_CntSrvCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T002U2", "SELECT [ContratoServicosPrazo_CntSrvCod] AS ContratoServicosPrazo_CntSrvCod, [ContratoServicosPrazoPlnj_Sequencial], [ContratoServicosPrazoPlnj_Inicio], [ContratoServicosPrazoPlnj_Fim], [ContratoServicosPrazoPlnj_Dias] FROM [ContratoServicosPrazoPrazoPlnj] WITH (UPDLOCK) WHERE [ContratoServicosPrazo_CntSrvCod] = @ContratoServicosPrazo_CntSrvCod AND [ContratoServicosPrazoPlnj_Sequencial] = @ContratoServicosPrazoPlnj_Sequencial ",true, GxErrorMask.GX_NOMASK, false, this,prmT002U2,1,0,true,false )
             ,new CursorDef("T002U3", "SELECT [ContratoServicosPrazo_CntSrvCod] AS ContratoServicosPrazo_CntSrvCod, [ContratoServicosPrazoPlnj_Sequencial], [ContratoServicosPrazoPlnj_Inicio], [ContratoServicosPrazoPlnj_Fim], [ContratoServicosPrazoPlnj_Dias] FROM [ContratoServicosPrazoPrazoPlnj] WITH (NOLOCK) WHERE [ContratoServicosPrazo_CntSrvCod] = @ContratoServicosPrazo_CntSrvCod AND [ContratoServicosPrazoPlnj_Sequencial] = @ContratoServicosPrazoPlnj_Sequencial ",true, GxErrorMask.GX_NOMASK, false, this,prmT002U3,1,0,true,false )
             ,new CursorDef("T002U4", "SELECT [ContratoServicosPrazo_CntSrvCod] AS ContratoServicosPrazo_CntSrvCod, [ContratoServicosPrazoRegra_Sequencial], [ContratoServicosPrazoRegra_Inicio], [ContratoServicosPrazoRegra_Fim], [ContratoServicosPrazoRegra_Dias] FROM [ContratoServicosPrazoContratoServicosPrazoRegra] WITH (UPDLOCK) WHERE [ContratoServicosPrazo_CntSrvCod] = @ContratoServicosPrazo_CntSrvCod AND [ContratoServicosPrazoRegra_Sequencial] = @ContratoServicosPrazoRegra_Sequencial ",true, GxErrorMask.GX_NOMASK, false, this,prmT002U4,1,0,true,false )
             ,new CursorDef("T002U5", "SELECT [ContratoServicosPrazo_CntSrvCod] AS ContratoServicosPrazo_CntSrvCod, [ContratoServicosPrazoRegra_Sequencial], [ContratoServicosPrazoRegra_Inicio], [ContratoServicosPrazoRegra_Fim], [ContratoServicosPrazoRegra_Dias] FROM [ContratoServicosPrazoContratoServicosPrazoRegra] WITH (NOLOCK) WHERE [ContratoServicosPrazo_CntSrvCod] = @ContratoServicosPrazo_CntSrvCod AND [ContratoServicosPrazoRegra_Sequencial] = @ContratoServicosPrazoRegra_Sequencial ",true, GxErrorMask.GX_NOMASK, false, this,prmT002U5,1,0,true,false )
             ,new CursorDef("T002U6", "SELECT [ContratoServicosPrazo_Dias], [ContratoServicosPrazo_Tipo], [ContratoServicosPrazo_Cada], [ContratoServicosPrazo_DiasAlta], [ContratoServicosPrazo_DiasMedia], [ContratoServicosPrazo_DiasBaixa], [ContratoServicosPrazo_Momento], [ContratoServicosPrazo_CntSrvCod] AS ContratoServicosPrazo_CntSrvCod FROM [ContratoServicosPrazo] WITH (UPDLOCK) WHERE [ContratoServicosPrazo_CntSrvCod] = @ContratoServicosPrazo_CntSrvCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002U6,1,0,true,false )
             ,new CursorDef("T002U7", "SELECT [ContratoServicosPrazo_Dias], [ContratoServicosPrazo_Tipo], [ContratoServicosPrazo_Cada], [ContratoServicosPrazo_DiasAlta], [ContratoServicosPrazo_DiasMedia], [ContratoServicosPrazo_DiasBaixa], [ContratoServicosPrazo_Momento], [ContratoServicosPrazo_CntSrvCod] AS ContratoServicosPrazo_CntSrvCod FROM [ContratoServicosPrazo] WITH (NOLOCK) WHERE [ContratoServicosPrazo_CntSrvCod] = @ContratoServicosPrazo_CntSrvCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002U7,1,0,true,false )
             ,new CursorDef("T002U8", "SELECT [ContratoServicos_UnidadeContratada] AS ContratoServicos_UnidadeContratada, [Servico_Codigo] AS ContratoServicosPrazo_ServicoCod FROM [ContratoServicos] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @ContratoServicosPrazo_CntSrvCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002U8,1,0,true,false )
             ,new CursorDef("T002U9", "SELECT [Servico_Sigla] AS ContratoServicosPrazo_ServicoSigla FROM [Servico] WITH (NOLOCK) WHERE [Servico_Codigo] = @ContratoServicosPrazo_ServicoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002U9,1,0,true,false )
             ,new CursorDef("T002U10", "SELECT [UnidadeMedicao_Sigla] AS ContratoServicos_UndCntSgl FROM [UnidadeMedicao] WITH (NOLOCK) WHERE [UnidadeMedicao_Codigo] = @ContratoServicos_UnidadeContratada ",true, GxErrorMask.GX_NOMASK, false, this,prmT002U10,1,0,true,false )
             ,new CursorDef("T002U12", "SELECT COALESCE( T1.[ContratoServicosPrazo_QtdRegras], 0) AS ContratoServicosPrazo_QtdRegras FROM (SELECT COUNT(*) AS ContratoServicosPrazo_QtdRegras, [ContratoServicosPrazo_CntSrvCod] AS ContratoServicosPrazo_CntSrvCod FROM [ContratoServicosPrazoContratoServicosPrazoRegra] WITH (UPDLOCK) GROUP BY [ContratoServicosPrazo_CntSrvCod] ) T1 WHERE T1.[ContratoServicosPrazo_CntSrvCod] = @ContratoServicosPrazo_CntSrvCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002U12,1,0,true,false )
             ,new CursorDef("T002U14", "SELECT COALESCE( T1.[ContratoServicosPrazo_QtdPlnj], 0) AS ContratoServicosPrazo_QtdPlnj FROM (SELECT COUNT(*) AS ContratoServicosPrazo_QtdPlnj, [ContratoServicosPrazo_CntSrvCod] AS ContratoServicosPrazo_CntSrvCod FROM [ContratoServicosPrazoPrazoPlnj] WITH (UPDLOCK) GROUP BY [ContratoServicosPrazo_CntSrvCod] ) T1 WHERE T1.[ContratoServicosPrazo_CntSrvCod] = @ContratoServicosPrazo_CntSrvCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002U14,1,0,true,false )
             ,new CursorDef("T002U17", cmdBufferT002U17,true, GxErrorMask.GX_NOMASK, false, this,prmT002U17,100,0,true,false )
             ,new CursorDef("T002U18", "SELECT [ContratoServicos_UnidadeContratada] AS ContratoServicos_UnidadeContratada, [Servico_Codigo] AS ContratoServicosPrazo_ServicoCod FROM [ContratoServicos] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @ContratoServicosPrazo_CntSrvCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002U18,1,0,true,false )
             ,new CursorDef("T002U19", "SELECT [UnidadeMedicao_Sigla] AS ContratoServicos_UndCntSgl FROM [UnidadeMedicao] WITH (NOLOCK) WHERE [UnidadeMedicao_Codigo] = @ContratoServicos_UnidadeContratada ",true, GxErrorMask.GX_NOMASK, false, this,prmT002U19,1,0,true,false )
             ,new CursorDef("T002U20", "SELECT [Servico_Sigla] AS ContratoServicosPrazo_ServicoSigla FROM [Servico] WITH (NOLOCK) WHERE [Servico_Codigo] = @ContratoServicosPrazo_ServicoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002U20,1,0,true,false )
             ,new CursorDef("T002U22", "SELECT COALESCE( T1.[ContratoServicosPrazo_QtdRegras], 0) AS ContratoServicosPrazo_QtdRegras FROM (SELECT COUNT(*) AS ContratoServicosPrazo_QtdRegras, [ContratoServicosPrazo_CntSrvCod] AS ContratoServicosPrazo_CntSrvCod FROM [ContratoServicosPrazoContratoServicosPrazoRegra] WITH (UPDLOCK) GROUP BY [ContratoServicosPrazo_CntSrvCod] ) T1 WHERE T1.[ContratoServicosPrazo_CntSrvCod] = @ContratoServicosPrazo_CntSrvCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002U22,1,0,true,false )
             ,new CursorDef("T002U24", "SELECT COALESCE( T1.[ContratoServicosPrazo_QtdPlnj], 0) AS ContratoServicosPrazo_QtdPlnj FROM (SELECT COUNT(*) AS ContratoServicosPrazo_QtdPlnj, [ContratoServicosPrazo_CntSrvCod] AS ContratoServicosPrazo_CntSrvCod FROM [ContratoServicosPrazoPrazoPlnj] WITH (UPDLOCK) GROUP BY [ContratoServicosPrazo_CntSrvCod] ) T1 WHERE T1.[ContratoServicosPrazo_CntSrvCod] = @ContratoServicosPrazo_CntSrvCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002U24,1,0,true,false )
             ,new CursorDef("T002U25", "SELECT [ContratoServicosPrazo_CntSrvCod] AS ContratoServicosPrazo_CntSrvCod FROM [ContratoServicosPrazo] WITH (NOLOCK) WHERE [ContratoServicosPrazo_CntSrvCod] = @ContratoServicosPrazo_CntSrvCod  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT002U25,1,0,true,false )
             ,new CursorDef("T002U26", "SELECT TOP 1 [ContratoServicosPrazo_CntSrvCod] AS ContratoServicosPrazo_CntSrvCod FROM [ContratoServicosPrazo] WITH (NOLOCK) WHERE ( [ContratoServicosPrazo_CntSrvCod] > @ContratoServicosPrazo_CntSrvCod) ORDER BY [ContratoServicosPrazo_CntSrvCod]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT002U26,1,0,true,true )
             ,new CursorDef("T002U27", "SELECT TOP 1 [ContratoServicosPrazo_CntSrvCod] AS ContratoServicosPrazo_CntSrvCod FROM [ContratoServicosPrazo] WITH (NOLOCK) WHERE ( [ContratoServicosPrazo_CntSrvCod] < @ContratoServicosPrazo_CntSrvCod) ORDER BY [ContratoServicosPrazo_CntSrvCod] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT002U27,1,0,true,true )
             ,new CursorDef("T002U28", "INSERT INTO [ContratoServicosPrazo]([ContratoServicosPrazo_Dias], [ContratoServicosPrazo_Tipo], [ContratoServicosPrazo_Cada], [ContratoServicosPrazo_DiasAlta], [ContratoServicosPrazo_DiasMedia], [ContratoServicosPrazo_DiasBaixa], [ContratoServicosPrazo_Momento], [ContratoServicosPrazo_CntSrvCod]) VALUES(@ContratoServicosPrazo_Dias, @ContratoServicosPrazo_Tipo, @ContratoServicosPrazo_Cada, @ContratoServicosPrazo_DiasAlta, @ContratoServicosPrazo_DiasMedia, @ContratoServicosPrazo_DiasBaixa, @ContratoServicosPrazo_Momento, @ContratoServicosPrazo_CntSrvCod)", GxErrorMask.GX_NOMASK,prmT002U28)
             ,new CursorDef("T002U29", "UPDATE [ContratoServicosPrazo] SET [ContratoServicosPrazo_Dias]=@ContratoServicosPrazo_Dias, [ContratoServicosPrazo_Tipo]=@ContratoServicosPrazo_Tipo, [ContratoServicosPrazo_Cada]=@ContratoServicosPrazo_Cada, [ContratoServicosPrazo_DiasAlta]=@ContratoServicosPrazo_DiasAlta, [ContratoServicosPrazo_DiasMedia]=@ContratoServicosPrazo_DiasMedia, [ContratoServicosPrazo_DiasBaixa]=@ContratoServicosPrazo_DiasBaixa, [ContratoServicosPrazo_Momento]=@ContratoServicosPrazo_Momento  WHERE [ContratoServicosPrazo_CntSrvCod] = @ContratoServicosPrazo_CntSrvCod", GxErrorMask.GX_NOMASK,prmT002U29)
             ,new CursorDef("T002U30", "DELETE FROM [ContratoServicosPrazo]  WHERE [ContratoServicosPrazo_CntSrvCod] = @ContratoServicosPrazo_CntSrvCod", GxErrorMask.GX_NOMASK,prmT002U30)
             ,new CursorDef("T002U31", "SELECT [ContratoServicos_UnidadeContratada] AS ContratoServicos_UnidadeContratada, [Servico_Codigo] AS ContratoServicosPrazo_ServicoCod FROM [ContratoServicos] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @ContratoServicosPrazo_CntSrvCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002U31,1,0,true,false )
             ,new CursorDef("T002U32", "SELECT [UnidadeMedicao_Sigla] AS ContratoServicos_UndCntSgl FROM [UnidadeMedicao] WITH (NOLOCK) WHERE [UnidadeMedicao_Codigo] = @ContratoServicos_UnidadeContratada ",true, GxErrorMask.GX_NOMASK, false, this,prmT002U32,1,0,true,false )
             ,new CursorDef("T002U33", "SELECT [Servico_Sigla] AS ContratoServicosPrazo_ServicoSigla FROM [Servico] WITH (NOLOCK) WHERE [Servico_Codigo] = @ContratoServicosPrazo_ServicoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002U33,1,0,true,false )
             ,new CursorDef("T002U35", "SELECT COALESCE( T1.[ContratoServicosPrazo_QtdRegras], 0) AS ContratoServicosPrazo_QtdRegras FROM (SELECT COUNT(*) AS ContratoServicosPrazo_QtdRegras, [ContratoServicosPrazo_CntSrvCod] AS ContratoServicosPrazo_CntSrvCod FROM [ContratoServicosPrazoContratoServicosPrazoRegra] WITH (UPDLOCK) GROUP BY [ContratoServicosPrazo_CntSrvCod] ) T1 WHERE T1.[ContratoServicosPrazo_CntSrvCod] = @ContratoServicosPrazo_CntSrvCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002U35,1,0,true,false )
             ,new CursorDef("T002U37", "SELECT COALESCE( T1.[ContratoServicosPrazo_QtdPlnj], 0) AS ContratoServicosPrazo_QtdPlnj FROM (SELECT COUNT(*) AS ContratoServicosPrazo_QtdPlnj, [ContratoServicosPrazo_CntSrvCod] AS ContratoServicosPrazo_CntSrvCod FROM [ContratoServicosPrazoPrazoPlnj] WITH (UPDLOCK) GROUP BY [ContratoServicosPrazo_CntSrvCod] ) T1 WHERE T1.[ContratoServicosPrazo_CntSrvCod] = @ContratoServicosPrazo_CntSrvCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002U37,1,0,true,false )
             ,new CursorDef("T002U38", "SELECT [ContratoServicosPrazo_CntSrvCod] AS ContratoServicosPrazo_CntSrvCod FROM [ContratoServicosPrazo] WITH (NOLOCK) ORDER BY [ContratoServicosPrazo_CntSrvCod]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT002U38,100,0,true,false )
             ,new CursorDef("T002U39", "SELECT [ContratoServicosPrazo_CntSrvCod] AS ContratoServicosPrazo_CntSrvCod, [ContratoServicosPrazoRegra_Sequencial], [ContratoServicosPrazoRegra_Inicio], [ContratoServicosPrazoRegra_Fim], [ContratoServicosPrazoRegra_Dias] FROM [ContratoServicosPrazoContratoServicosPrazoRegra] WITH (NOLOCK) WHERE [ContratoServicosPrazo_CntSrvCod] = @ContratoServicosPrazo_CntSrvCod and [ContratoServicosPrazoRegra_Sequencial] = @ContratoServicosPrazoRegra_Sequencial ORDER BY [ContratoServicosPrazo_CntSrvCod], [ContratoServicosPrazoRegra_Sequencial] ",true, GxErrorMask.GX_NOMASK, false, this,prmT002U39,11,0,true,false )
             ,new CursorDef("T002U40", "SELECT [ContratoServicosPrazo_CntSrvCod] AS ContratoServicosPrazo_CntSrvCod, [ContratoServicosPrazoRegra_Sequencial] FROM [ContratoServicosPrazoContratoServicosPrazoRegra] WITH (NOLOCK) WHERE [ContratoServicosPrazo_CntSrvCod] = @ContratoServicosPrazo_CntSrvCod AND [ContratoServicosPrazoRegra_Sequencial] = @ContratoServicosPrazoRegra_Sequencial ",true, GxErrorMask.GX_NOMASK, false, this,prmT002U40,1,0,true,false )
             ,new CursorDef("T002U41", "INSERT INTO [ContratoServicosPrazoContratoServicosPrazoRegra]([ContratoServicosPrazo_CntSrvCod], [ContratoServicosPrazoRegra_Sequencial], [ContratoServicosPrazoRegra_Inicio], [ContratoServicosPrazoRegra_Fim], [ContratoServicosPrazoRegra_Dias]) VALUES(@ContratoServicosPrazo_CntSrvCod, @ContratoServicosPrazoRegra_Sequencial, @ContratoServicosPrazoRegra_Inicio, @ContratoServicosPrazoRegra_Fim, @ContratoServicosPrazoRegra_Dias)", GxErrorMask.GX_NOMASK,prmT002U41)
             ,new CursorDef("T002U42", "UPDATE [ContratoServicosPrazoContratoServicosPrazoRegra] SET [ContratoServicosPrazoRegra_Inicio]=@ContratoServicosPrazoRegra_Inicio, [ContratoServicosPrazoRegra_Fim]=@ContratoServicosPrazoRegra_Fim, [ContratoServicosPrazoRegra_Dias]=@ContratoServicosPrazoRegra_Dias  WHERE [ContratoServicosPrazo_CntSrvCod] = @ContratoServicosPrazo_CntSrvCod AND [ContratoServicosPrazoRegra_Sequencial] = @ContratoServicosPrazoRegra_Sequencial", GxErrorMask.GX_NOMASK,prmT002U42)
             ,new CursorDef("T002U43", "DELETE FROM [ContratoServicosPrazoContratoServicosPrazoRegra]  WHERE [ContratoServicosPrazo_CntSrvCod] = @ContratoServicosPrazo_CntSrvCod AND [ContratoServicosPrazoRegra_Sequencial] = @ContratoServicosPrazoRegra_Sequencial", GxErrorMask.GX_NOMASK,prmT002U43)
             ,new CursorDef("T002U44", "SELECT [ContratoServicosPrazo_CntSrvCod] AS ContratoServicosPrazo_CntSrvCod, [ContratoServicosPrazoRegra_Sequencial] FROM [ContratoServicosPrazoContratoServicosPrazoRegra] WITH (NOLOCK) WHERE [ContratoServicosPrazo_CntSrvCod] = @ContratoServicosPrazo_CntSrvCod ORDER BY [ContratoServicosPrazo_CntSrvCod], [ContratoServicosPrazoRegra_Sequencial] ",true, GxErrorMask.GX_NOMASK, false, this,prmT002U44,11,0,true,false )
             ,new CursorDef("T002U45", "SELECT [ContratoServicosPrazo_CntSrvCod] AS ContratoServicosPrazo_CntSrvCod, [ContratoServicosPrazoPlnj_Sequencial], [ContratoServicosPrazoPlnj_Inicio], [ContratoServicosPrazoPlnj_Fim], [ContratoServicosPrazoPlnj_Dias] FROM [ContratoServicosPrazoPrazoPlnj] WITH (NOLOCK) WHERE [ContratoServicosPrazo_CntSrvCod] = @ContratoServicosPrazo_CntSrvCod and [ContratoServicosPrazoPlnj_Sequencial] = @ContratoServicosPrazoPlnj_Sequencial ORDER BY [ContratoServicosPrazo_CntSrvCod], [ContratoServicosPrazoPlnj_Sequencial] ",true, GxErrorMask.GX_NOMASK, false, this,prmT002U45,11,0,true,false )
             ,new CursorDef("T002U46", "SELECT [ContratoServicosPrazo_CntSrvCod] AS ContratoServicosPrazo_CntSrvCod, [ContratoServicosPrazoPlnj_Sequencial] FROM [ContratoServicosPrazoPrazoPlnj] WITH (NOLOCK) WHERE [ContratoServicosPrazo_CntSrvCod] = @ContratoServicosPrazo_CntSrvCod AND [ContratoServicosPrazoPlnj_Sequencial] = @ContratoServicosPrazoPlnj_Sequencial ",true, GxErrorMask.GX_NOMASK, false, this,prmT002U46,1,0,true,false )
             ,new CursorDef("T002U47", "INSERT INTO [ContratoServicosPrazoPrazoPlnj]([ContratoServicosPrazo_CntSrvCod], [ContratoServicosPrazoPlnj_Sequencial], [ContratoServicosPrazoPlnj_Inicio], [ContratoServicosPrazoPlnj_Fim], [ContratoServicosPrazoPlnj_Dias]) VALUES(@ContratoServicosPrazo_CntSrvCod, @ContratoServicosPrazoPlnj_Sequencial, @ContratoServicosPrazoPlnj_Inicio, @ContratoServicosPrazoPlnj_Fim, @ContratoServicosPrazoPlnj_Dias)", GxErrorMask.GX_NOMASK,prmT002U47)
             ,new CursorDef("T002U48", "UPDATE [ContratoServicosPrazoPrazoPlnj] SET [ContratoServicosPrazoPlnj_Inicio]=@ContratoServicosPrazoPlnj_Inicio, [ContratoServicosPrazoPlnj_Fim]=@ContratoServicosPrazoPlnj_Fim, [ContratoServicosPrazoPlnj_Dias]=@ContratoServicosPrazoPlnj_Dias  WHERE [ContratoServicosPrazo_CntSrvCod] = @ContratoServicosPrazo_CntSrvCod AND [ContratoServicosPrazoPlnj_Sequencial] = @ContratoServicosPrazoPlnj_Sequencial", GxErrorMask.GX_NOMASK,prmT002U48)
             ,new CursorDef("T002U49", "DELETE FROM [ContratoServicosPrazoPrazoPlnj]  WHERE [ContratoServicosPrazo_CntSrvCod] = @ContratoServicosPrazo_CntSrvCod AND [ContratoServicosPrazoPlnj_Sequencial] = @ContratoServicosPrazoPlnj_Sequencial", GxErrorMask.GX_NOMASK,prmT002U49)
             ,new CursorDef("T002U50", "SELECT [ContratoServicosPrazo_CntSrvCod] AS ContratoServicosPrazo_CntSrvCod, [ContratoServicosPrazoPlnj_Sequencial] FROM [ContratoServicosPrazoPrazoPlnj] WITH (NOLOCK) WHERE [ContratoServicosPrazo_CntSrvCod] = @ContratoServicosPrazo_CntSrvCod ORDER BY [ContratoServicosPrazo_CntSrvCod], [ContratoServicosPrazoPlnj_Sequencial] ",true, GxErrorMask.GX_NOMASK, false, this,prmT002U50,11,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                ((decimal[]) buf[3])[0] = rslt.getDecimal(4) ;
                ((short[]) buf[4])[0] = rslt.getShort(5) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                ((decimal[]) buf[3])[0] = rslt.getDecimal(4) ;
                ((short[]) buf[4])[0] = rslt.getShort(5) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                ((decimal[]) buf[3])[0] = rslt.getDecimal(4) ;
                ((short[]) buf[4])[0] = rslt.getShort(5) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                ((decimal[]) buf[3])[0] = rslt.getDecimal(4) ;
                ((short[]) buf[4])[0] = rslt.getShort(5) ;
                return;
             case 4 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 20) ;
                ((decimal[]) buf[3])[0] = rslt.getDecimal(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((short[]) buf[5])[0] = rslt.getShort(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((short[]) buf[7])[0] = rslt.getShort(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((short[]) buf[9])[0] = rslt.getShort(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((short[]) buf[11])[0] = rslt.getShort(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((int[]) buf[13])[0] = rslt.getInt(8) ;
                return;
             case 5 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 20) ;
                ((decimal[]) buf[3])[0] = rslt.getDecimal(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((short[]) buf[5])[0] = rslt.getShort(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((short[]) buf[7])[0] = rslt.getShort(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((short[]) buf[9])[0] = rslt.getShort(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((short[]) buf[11])[0] = rslt.getShort(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((int[]) buf[13])[0] = rslt.getInt(8) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 7 :
                ((String[]) buf[0])[0] = rslt.getString(1, 15) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 8 :
                ((String[]) buf[0])[0] = rslt.getString(1, 15) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 9 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 10 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((short[]) buf[2])[0] = rslt.getShort(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getString(3, 20) ;
                ((decimal[]) buf[5])[0] = rslt.getDecimal(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getString(5, 15) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getString(6, 15) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((short[]) buf[11])[0] = rslt.getShort(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((short[]) buf[13])[0] = rslt.getShort(8) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((short[]) buf[15])[0] = rslt.getShort(9) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                ((short[]) buf[17])[0] = rslt.getShort(10) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(10);
                ((int[]) buf[19])[0] = rslt.getInt(11) ;
                ((int[]) buf[20])[0] = rslt.getInt(12) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(12);
                ((short[]) buf[22])[0] = rslt.getShort(13) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(13);
                ((short[]) buf[24])[0] = rslt.getShort(14) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(14);
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 13 :
                ((String[]) buf[0])[0] = rslt.getString(1, 15) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 14 :
                ((String[]) buf[0])[0] = rslt.getString(1, 15) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 15 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 16 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 17 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 18 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 19 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 23 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 24 :
                ((String[]) buf[0])[0] = rslt.getString(1, 15) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 25 :
                ((String[]) buf[0])[0] = rslt.getString(1, 15) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 26 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 27 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 28 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 29 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                ((decimal[]) buf[3])[0] = rslt.getDecimal(4) ;
                ((short[]) buf[4])[0] = rslt.getShort(5) ;
                return;
       }
       getresults30( cursor, rslt, buf) ;
    }

    public void getresults30( int cursor ,
                              IFieldGetter rslt ,
                              Object[] buf )
    {
       switch ( cursor )
       {
             case 30 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                return;
             case 34 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                return;
             case 35 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                ((decimal[]) buf[3])[0] = rslt.getDecimal(4) ;
                ((short[]) buf[4])[0] = rslt.getShort(5) ;
                return;
             case 36 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                return;
             case 40 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 8 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 13 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 14 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 15 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 16 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 17 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 18 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 19 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 20 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(1, (short)parms[1]);
                }
                stmt.SetParameter(2, (String)parms[2]);
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 3 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(3, (decimal)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 4 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(4, (short)parms[6]);
                }
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 5 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(5, (short)parms[8]);
                }
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 6 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(6, (short)parms[10]);
                }
                if ( (bool)parms[11] )
                {
                   stmt.setNull( 7 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(7, (short)parms[12]);
                }
                stmt.SetParameter(8, (int)parms[13]);
                return;
             case 21 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(1, (short)parms[1]);
                }
                stmt.SetParameter(2, (String)parms[2]);
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 3 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(3, (decimal)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 4 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(4, (short)parms[6]);
                }
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 5 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(5, (short)parms[8]);
                }
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 6 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(6, (short)parms[10]);
                }
                if ( (bool)parms[11] )
                {
                   stmt.setNull( 7 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(7, (short)parms[12]);
                }
                stmt.SetParameter(8, (int)parms[13]);
                return;
             case 22 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 23 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 24 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 25 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 26 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 27 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 29 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                return;
       }
       setparameters30( cursor, stmt, parms) ;
    }

    public void setparameters30( int cursor ,
                                 IFieldSetter stmt ,
                                 Object[] parms )
    {
       switch ( cursor )
       {
             case 30 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                return;
             case 31 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                stmt.SetParameter(3, (decimal)parms[2]);
                stmt.SetParameter(4, (decimal)parms[3]);
                stmt.SetParameter(5, (short)parms[4]);
                return;
             case 32 :
                stmt.SetParameter(1, (decimal)parms[0]);
                stmt.SetParameter(2, (decimal)parms[1]);
                stmt.SetParameter(3, (short)parms[2]);
                stmt.SetParameter(4, (int)parms[3]);
                stmt.SetParameter(5, (short)parms[4]);
                return;
             case 33 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                return;
             case 34 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 35 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                return;
             case 36 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                return;
             case 37 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                stmt.SetParameter(3, (decimal)parms[2]);
                stmt.SetParameter(4, (decimal)parms[3]);
                stmt.SetParameter(5, (short)parms[4]);
                return;
             case 38 :
                stmt.SetParameter(1, (decimal)parms[0]);
                stmt.SetParameter(2, (decimal)parms[1]);
                stmt.SetParameter(3, (short)parms[2]);
                stmt.SetParameter(4, (int)parms[3]);
                stmt.SetParameter(5, (short)parms[4]);
                return;
             case 39 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                return;
             case 40 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
