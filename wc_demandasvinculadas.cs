/*
               File: WC_DemandasVinculadas
        Description: Demandas Vinculadas
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:15:43.71
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wc_demandasvinculadas : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public wc_demandasvinculadas( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public wc_demandasvinculadas( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ContagemResultado_Codigo ,
                           int aP1_ContagemResultado_OSVinculada )
      {
         this.AV7ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         this.AV6ContagemResultado_OSVinculada = aP1_ContagemResultado_OSVinculada;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
         cmbContagemResultado_Baseline = new GXCombobox();
         cmbContagemResultado_StatusDmn = new GXCombobox();
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  AV7ContagemResultado_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContagemResultado_Codigo), 6, 0)));
                  AV6ContagemResultado_OSVinculada = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV6ContagemResultado_OSVinculada", StringUtil.LTrim( StringUtil.Str( (decimal)(AV6ContagemResultado_OSVinculada), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)AV7ContagemResultado_Codigo,(int)AV6ContagemResultado_OSVinculada});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
               {
                  nRC_GXsfl_8 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  nGXsfl_8_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  sGXsfl_8_idx = GetNextPar( );
                  sPrefix = GetNextPar( );
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxnrGrid_newrow( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
               {
                  AV7ContagemResultado_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContagemResultado_Codigo), 6, 0)));
                  AV6ContagemResultado_OSVinculada = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV6ContagemResultado_OSVinculada", StringUtil.LTrim( StringUtil.Str( (decimal)(AV6ContagemResultado_OSVinculada), 6, 0)));
                  ajax_req_read_hidden_sdt(GetNextPar( ), AV11WWPContext);
                  A803ContagemResultado_ContratadaSigla = GetNextPar( );
                  n803ContagemResultado_ContratadaSigla = false;
                  A457ContagemResultado_Demanda = GetNextPar( );
                  n457ContagemResultado_Demanda = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A457ContagemResultado_Demanda", A457ContagemResultado_Demanda);
                  AV15SS = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15SS", AV15SS);
                  AV16StatusSS = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16StatusSS", AV16StatusSS);
                  A484ContagemResultado_StatusDmn = GetNextPar( );
                  n484ContagemResultado_StatusDmn = false;
                  A490ContagemResultado_ContratadaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  n490ContagemResultado_ContratadaCod = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A490ContagemResultado_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A490ContagemResultado_ContratadaCod), 6, 0)));
                  A456ContagemResultado_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  A682ContagemResultado_PFBFMUltima = NumberUtil.Val( GetNextPar( ), ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A682ContagemResultado_PFBFMUltima", StringUtil.LTrim( StringUtil.Str( A682ContagemResultado_PFBFMUltima, 14, 5)));
                  A684ContagemResultado_PFBFSUltima = NumberUtil.Val( GetNextPar( ), ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A684ContagemResultado_PFBFSUltima", StringUtil.LTrim( StringUtil.Str( A684ContagemResultado_PFBFSUltima, 14, 5)));
                  A683ContagemResultado_PFLFMUltima = NumberUtil.Val( GetNextPar( ), ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A683ContagemResultado_PFLFMUltima", StringUtil.LTrim( StringUtil.Str( A683ContagemResultado_PFLFMUltima, 14, 5)));
                  A685ContagemResultado_PFLFSUltima = NumberUtil.Val( GetNextPar( ), ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A685ContagemResultado_PFLFSUltima", StringUtil.LTrim( StringUtil.Str( A685ContagemResultado_PFLFSUltima, 14, 5)));
                  A574ContagemResultado_PFFinal = NumberUtil.Val( GetNextPar( ), ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A574ContagemResultado_PFFinal", StringUtil.LTrim( StringUtil.Str( A574ContagemResultado_PFFinal, 14, 5)));
                  sPrefix = GetNextPar( );
                  init_default_properties( ) ;
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxgrGrid_refresh( AV7ContagemResultado_Codigo, AV6ContagemResultado_OSVinculada, AV11WWPContext, A803ContagemResultado_ContratadaSigla, A457ContagemResultado_Demanda, AV15SS, AV16StatusSS, A484ContagemResultado_StatusDmn, A490ContagemResultado_ContratadaCod, A456ContagemResultado_Codigo, A682ContagemResultado_PFBFMUltima, A684ContagemResultado_PFBFSUltima, A683ContagemResultado_PFLFMUltima, A685ContagemResultado_PFLFSUltima, A574ContagemResultado_PFFinal, sPrefix) ;
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
                  return  ;
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PAEF2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               context.Gx_err = 0;
               edtavContratadasigla_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContratadasigla_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratadasigla_Enabled), 5, 0)));
               edtavDemanda_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDemanda_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDemanda_Enabled), 5, 0)));
               edtavPfbfs_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavPfbfs_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPfbfs_Enabled), 5, 0)));
               edtavPflfs_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavPflfs_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPflfs_Enabled), 5, 0)));
               edtavPfbfm_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavPfbfm_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPfbfm_Enabled), 5, 0)));
               edtavPflfm_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavPflfm_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPflfm_Enabled), 5, 0)));
               edtavPffinal_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavPffinal_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPffinal_Enabled), 5, 0)));
               WSEF2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Demandas Vinculadas") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20206216154674");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/ConfirmPanel/BootstrapConfirmPanelRender.js", "");
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wc_demandasvinculadas.aspx") + "?" + UrlEncode("" +AV7ContagemResultado_Codigo) + "," + UrlEncode("" +AV6ContagemResultado_OSVinculada)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"nRC_GXsfl_8", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_8), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOAV7ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOAV7ContagemResultado_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOAV6ContagemResultado_OSVinculada", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOAV6ContagemResultado_OSVinculada), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vCONTAGEMRESULTADO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7ContagemResultado_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vCONTAGEMRESULTADO_OSVINCULADA", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV6ContagemResultado_OSVinculada), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vWWPCONTEXT", AV11WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vWWPCONTEXT", AV11WWPContext);
         }
         GxWebStd.gx_boolean_hidden_field( context, sPrefix+"vSS", AV15SS);
         GxWebStd.gx_hidden_field( context, sPrefix+"vSTATUSSS", StringUtil.RTrim( AV16StatusSS));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADO_CONTRATADACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A490ContagemResultado_ContratadaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADO_PFBFMULTIMA", StringUtil.LTrim( StringUtil.NToC( A682ContagemResultado_PFBFMUltima, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADO_PFBFSULTIMA", StringUtil.LTrim( StringUtil.NToC( A684ContagemResultado_PFBFSUltima, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADO_PFLFMULTIMA", StringUtil.LTrim( StringUtil.NToC( A683ContagemResultado_PFLFMUltima, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADO_PFLFSULTIMA", StringUtil.LTrim( StringUtil.NToC( A685ContagemResultado_PFLFSUltima, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vCODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vNOVOSTATUS", StringUtil.RTrim( AV19NovoStatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADO_DEMANDA", A457ContagemResultado_Demanda);
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADO_DEMANDAFM", A493ContagemResultado_DemandaFM);
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADO_PFFINAL", StringUtil.LTrim( StringUtil.NToC( A574ContagemResultado_PFFinal, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONFIRMPANEL_Title", StringUtil.RTrim( Confirmpanel_Title));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONFIRMPANEL_Confirmationtext", StringUtil.RTrim( Confirmpanel_Confirmationtext));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONFIRMPANEL_Yesbuttoncaption", StringUtil.RTrim( Confirmpanel_Yesbuttoncaption));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONFIRMPANEL_Nobuttoncaption", StringUtil.RTrim( Confirmpanel_Nobuttoncaption));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONFIRMPANEL_Yesbuttonposition", StringUtil.RTrim( Confirmpanel_Yesbuttonposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONFIRMPANEL_Result", StringUtil.RTrim( Confirmpanel_Result));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormEF2( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("wc_demandasvinculadas.js", "?20206216154689");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "WC_DemandasVinculadas" ;
      }

      public override String GetPgmdesc( )
      {
         return "Demandas Vinculadas" ;
      }

      protected void WBEF0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "wc_demandasvinculadas.aspx");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/ConfirmPanel/BootstrapConfirmPanelRender.js", "");
            }
            wb_table1_2_EF2( true) ;
         }
         else
         {
            wb_table1_2_EF2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_EF2e( bool wbgen )
      {
         if ( wbgen )
         {
            wb_table2_31_EF2( true) ;
         }
         else
         {
            wb_table2_31_EF2( false) ;
         }
         return  ;
      }

      protected void wb_table2_31_EF2e( bool wbgen )
      {
         if ( wbgen )
         {
            wb_table3_34_EF2( true) ;
         }
         else
         {
            wb_table3_34_EF2( false) ;
         }
         return  ;
      }

      protected void wb_table3_34_EF2e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void STARTEF2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Demandas Vinculadas", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPEF0( ) ;
            }
         }
      }

      protected void WSEF2( )
      {
         STARTEF2( ) ;
         EVTEF2( ) ;
      }

      protected void EVTEF2( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPEF0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "CONFIRMPANEL.CLOSE") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPEF0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11EF2 */
                                    E11EF2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'VINCULARDEMANDA'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPEF0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12EF2 */
                                    E12EF2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPEF0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    GX_FocusControl = edtavContratadasigla_Internalname;
                                    context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 4), "LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 14), "VIGUALAR.CLICK") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 14), "VIGUALAR.CLICK") == 0 ) )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPEF0( ) ;
                              }
                              nGXsfl_8_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_8_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_8_idx), 4, 0)), 4, "0");
                              SubsflControlProps_82( ) ;
                              AV9Desvincular = cgiGet( edtavDesvincular_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDesvincular_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV9Desvincular)) ? AV30Desvincular_GXI : context.convertURL( context.PathToRelativeUrl( AV9Desvincular))));
                              A456ContagemResultado_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContagemResultado_Codigo_Internalname), ",", "."));
                              A602ContagemResultado_OSVinculada = (int)(context.localUtil.CToN( cgiGet( edtContagemResultado_OSVinculada_Internalname), ",", "."));
                              n602ContagemResultado_OSVinculada = false;
                              AV17ContratadaSigla = StringUtil.Upper( cgiGet( edtavContratadasigla_Internalname));
                              context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavContratadasigla_Internalname, AV17ContratadaSigla);
                              context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vCONTRATADASIGLA"+"_"+sGXsfl_8_idx, GetSecureSignedToken( sPrefix+sGXsfl_8_idx, StringUtil.RTrim( context.localUtil.Format( AV17ContratadaSigla, "@!"))));
                              AV18Demanda = StringUtil.Upper( cgiGet( edtavDemanda_Internalname));
                              context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavDemanda_Internalname, AV18Demanda);
                              context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vDEMANDA"+"_"+sGXsfl_8_idx, GetSecureSignedToken( sPrefix+sGXsfl_8_idx, StringUtil.RTrim( context.localUtil.Format( AV18Demanda, "@!"))));
                              A471ContagemResultado_DataDmn = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtContagemResultado_DataDmn_Internalname), 0));
                              A501ContagemResultado_OsFsOsFm = cgiGet( edtContagemResultado_OsFsOsFm_Internalname);
                              A803ContagemResultado_ContratadaSigla = StringUtil.Upper( cgiGet( edtContagemResultado_ContratadaSigla_Internalname));
                              n803ContagemResultado_ContratadaSigla = false;
                              A866ContagemResultado_ContratadaOrigemSigla = StringUtil.Upper( cgiGet( edtContagemResultado_ContratadaOrigemSigla_Internalname));
                              n866ContagemResultado_ContratadaOrigemSigla = false;
                              A801ContagemResultado_ServicoSigla = StringUtil.Upper( cgiGet( edtContagemResultado_ServicoSigla_Internalname));
                              n801ContagemResultado_ServicoSigla = false;
                              A509ContagemrResultado_SistemaSigla = StringUtil.Upper( cgiGet( edtContagemrResultado_SistemaSigla_Internalname));
                              n509ContagemrResultado_SistemaSigla = false;
                              cmbContagemResultado_Baseline.Name = cmbContagemResultado_Baseline_Internalname;
                              cmbContagemResultado_Baseline.CurrentValue = cgiGet( cmbContagemResultado_Baseline_Internalname);
                              A598ContagemResultado_Baseline = StringUtil.StrToBool( cgiGet( cmbContagemResultado_Baseline_Internalname));
                              n598ContagemResultado_Baseline = false;
                              AV22PFBFS = cgiGet( edtavPfbfs_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavPfbfs_Internalname, AV22PFBFS);
                              context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vPFBFS"+"_"+sGXsfl_8_idx, GetSecureSignedToken( sPrefix+sGXsfl_8_idx, StringUtil.RTrim( context.localUtil.Format( AV22PFBFS, ""))));
                              AV23PFLFS = cgiGet( edtavPflfs_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavPflfs_Internalname, AV23PFLFS);
                              context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vPFLFS"+"_"+sGXsfl_8_idx, GetSecureSignedToken( sPrefix+sGXsfl_8_idx, StringUtil.RTrim( context.localUtil.Format( AV23PFLFS, ""))));
                              AV24PFBFM = cgiGet( edtavPfbfm_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavPfbfm_Internalname, AV24PFBFM);
                              context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vPFBFM"+"_"+sGXsfl_8_idx, GetSecureSignedToken( sPrefix+sGXsfl_8_idx, StringUtil.RTrim( context.localUtil.Format( AV24PFBFM, ""))));
                              AV25PFLFM = cgiGet( edtavPflfm_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavPflfm_Internalname, AV25PFLFM);
                              context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vPFLFM"+"_"+sGXsfl_8_idx, GetSecureSignedToken( sPrefix+sGXsfl_8_idx, StringUtil.RTrim( context.localUtil.Format( AV25PFLFM, ""))));
                              AV27PFFINAL = cgiGet( edtavPffinal_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavPffinal_Internalname, AV27PFFINAL);
                              context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vPFFINAL"+"_"+sGXsfl_8_idx, GetSecureSignedToken( sPrefix+sGXsfl_8_idx, StringUtil.RTrim( context.localUtil.Format( AV27PFFINAL, ""))));
                              cmbContagemResultado_StatusDmn.Name = cmbContagemResultado_StatusDmn_Internalname;
                              cmbContagemResultado_StatusDmn.CurrentValue = cgiGet( cmbContagemResultado_StatusDmn_Internalname);
                              A484ContagemResultado_StatusDmn = cgiGet( cmbContagemResultado_StatusDmn_Internalname);
                              n484ContagemResultado_StatusDmn = false;
                              AV14Igualar = cgiGet( edtavIgualar_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavIgualar_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV14Igualar)) ? AV31Igualar_GXI : context.convertURL( context.PathToRelativeUrl( AV14Igualar))));
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavContratadasigla_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E13EF2 */
                                          E13EF2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavContratadasigla_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E14EF2 */
                                          E14EF2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavContratadasigla_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E15EF2 */
                                          E15EF2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "VIGUALAR.CLICK") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavContratadasigla_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E16EF2 */
                                          E16EF2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          if ( ! wbErr )
                                          {
                                             Rfr0gs = false;
                                             if ( ! Rfr0gs )
                                             {
                                             }
                                             dynload_actions( ) ;
                                          }
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavContratadasigla_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                                    {
                                       STRUPEF0( ) ;
                                    }
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavContratadasigla_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEEF2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormEF2( ) ;
            }
         }
      }

      protected void PAEF2( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            GXCCtl = "CONTAGEMRESULTADO_BASELINE_" + sGXsfl_8_idx;
            cmbContagemResultado_Baseline.Name = GXCCtl;
            cmbContagemResultado_Baseline.WebTags = "";
            cmbContagemResultado_Baseline.addItem(StringUtil.BoolToStr( true), "*", 0);
            cmbContagemResultado_Baseline.addItem(StringUtil.BoolToStr( false), "", 0);
            if ( cmbContagemResultado_Baseline.ItemCount > 0 )
            {
               A598ContagemResultado_Baseline = StringUtil.StrToBool( cmbContagemResultado_Baseline.getValidValue(StringUtil.BoolToStr( A598ContagemResultado_Baseline)));
               n598ContagemResultado_Baseline = false;
            }
            GXCCtl = "CONTAGEMRESULTADO_STATUSDMN_" + sGXsfl_8_idx;
            cmbContagemResultado_StatusDmn.Name = GXCCtl;
            cmbContagemResultado_StatusDmn.WebTags = "";
            cmbContagemResultado_StatusDmn.addItem("B", "Stand by", 0);
            cmbContagemResultado_StatusDmn.addItem("S", "Solicitada", 0);
            cmbContagemResultado_StatusDmn.addItem("E", "Em An�lise", 0);
            cmbContagemResultado_StatusDmn.addItem("A", "Em execu��o", 0);
            cmbContagemResultado_StatusDmn.addItem("R", "Resolvida", 0);
            cmbContagemResultado_StatusDmn.addItem("C", "Conferida", 0);
            cmbContagemResultado_StatusDmn.addItem("D", "Retornada", 0);
            cmbContagemResultado_StatusDmn.addItem("H", "Homologada", 0);
            cmbContagemResultado_StatusDmn.addItem("O", "Aceite", 0);
            cmbContagemResultado_StatusDmn.addItem("P", "A Pagar", 0);
            cmbContagemResultado_StatusDmn.addItem("L", "Liquidada", 0);
            cmbContagemResultado_StatusDmn.addItem("X", "Cancelada", 0);
            cmbContagemResultado_StatusDmn.addItem("N", "N�o Faturada", 0);
            cmbContagemResultado_StatusDmn.addItem("J", "Planejamento", 0);
            cmbContagemResultado_StatusDmn.addItem("I", "An�lise Planejamento", 0);
            cmbContagemResultado_StatusDmn.addItem("T", "Validacao T�cnica", 0);
            cmbContagemResultado_StatusDmn.addItem("Q", "Validacao Qualidade", 0);
            cmbContagemResultado_StatusDmn.addItem("G", "Em Homologa��o", 0);
            cmbContagemResultado_StatusDmn.addItem("M", "Valida��o Mensura��o", 0);
            cmbContagemResultado_StatusDmn.addItem("U", "Rascunho", 0);
            if ( cmbContagemResultado_StatusDmn.ItemCount > 0 )
            {
               A484ContagemResultado_StatusDmn = cmbContagemResultado_StatusDmn.getValidValue(A484ContagemResultado_StatusDmn);
               n484ContagemResultado_StatusDmn = false;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_82( ) ;
         while ( nGXsfl_8_idx <= nRC_GXsfl_8 )
         {
            sendrow_82( ) ;
            nGXsfl_8_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_8_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_8_idx+1));
            sGXsfl_8_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_8_idx), 4, 0)), 4, "0");
            SubsflControlProps_82( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int AV7ContagemResultado_Codigo ,
                                       int AV6ContagemResultado_OSVinculada ,
                                       wwpbaseobjects.SdtWWPContext AV11WWPContext ,
                                       String A803ContagemResultado_ContratadaSigla ,
                                       String A457ContagemResultado_Demanda ,
                                       bool AV15SS ,
                                       String AV16StatusSS ,
                                       String A484ContagemResultado_StatusDmn ,
                                       int A490ContagemResultado_ContratadaCod ,
                                       int A456ContagemResultado_Codigo ,
                                       decimal A682ContagemResultado_PFBFMUltima ,
                                       decimal A684ContagemResultado_PFBFSUltima ,
                                       decimal A683ContagemResultado_PFLFMUltima ,
                                       decimal A685ContagemResultado_PFLFSUltima ,
                                       decimal A574ContagemResultado_PFFinal ,
                                       String sPrefix )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GRID_nCurrentRecord = 0;
         RFEF2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADO_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A456ContagemResultado_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A456ContagemResultado_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADO_OSVINCULADA", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A602ContagemResultado_OSVinculada), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADO_OSVINCULADA", StringUtil.LTrim( StringUtil.NToC( (decimal)(A602ContagemResultado_OSVinculada), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_vCONTRATADASIGLA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( AV17ContratadaSigla, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"vCONTRATADASIGLA", StringUtil.RTrim( AV17ContratadaSigla));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_vDEMANDA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( AV18Demanda, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"vDEMANDA", AV18Demanda);
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADO_DATADMN", GetSecureSignedToken( sPrefix, A471ContagemResultado_DataDmn));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADO_DATADMN", context.localUtil.Format(A471ContagemResultado_DataDmn, "99/99/99"));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADO_OSFSOSFM", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A501ContagemResultado_OsFsOsFm, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADO_OSFSOSFM", A501ContagemResultado_OsFsOsFm);
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADO_BASELINE", GetSecureSignedToken( sPrefix, A598ContagemResultado_Baseline));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADO_BASELINE", StringUtil.BoolToStr( A598ContagemResultado_Baseline));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_vPFBFS", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( AV22PFBFS, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"vPFBFS", StringUtil.RTrim( AV22PFBFS));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_vPFLFS", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( AV23PFLFS, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"vPFLFS", StringUtil.RTrim( AV23PFLFS));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_vPFBFM", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( AV24PFBFM, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"vPFBFM", StringUtil.RTrim( AV24PFBFM));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_vPFLFM", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( AV25PFLFM, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"vPFLFM", StringUtil.RTrim( AV25PFLFM));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_vPFFINAL", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( AV27PFFINAL, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"vPFFINAL", StringUtil.RTrim( AV27PFFINAL));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADO_STATUSDMN", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A484ContagemResultado_StatusDmn, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADO_STATUSDMN", StringUtil.RTrim( A484ContagemResultado_StatusDmn));
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFEF2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavContratadasigla_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContratadasigla_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratadasigla_Enabled), 5, 0)));
         edtavDemanda_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDemanda_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDemanda_Enabled), 5, 0)));
         edtavPfbfs_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavPfbfs_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPfbfs_Enabled), 5, 0)));
         edtavPflfs_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavPflfs_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPflfs_Enabled), 5, 0)));
         edtavPfbfm_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavPfbfm_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPfbfm_Enabled), 5, 0)));
         edtavPflfm_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavPflfm_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPflfm_Enabled), 5, 0)));
         edtavPffinal_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavPffinal_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPffinal_Enabled), 5, 0)));
      }

      protected void RFEF2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 8;
         /* Execute user event: E14EF2 */
         E14EF2 ();
         nGXsfl_8_idx = 1;
         sGXsfl_8_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_8_idx), 4, 0)), 4, "0");
         SubsflControlProps_82( ) ;
         nGXsfl_8_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", sPrefix);
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_82( ) ;
            /* Using cursor H00EF3 */
            pr_default.execute(0, new Object[] {AV6ContagemResultado_OSVinculada, AV7ContagemResultado_Codigo});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A805ContagemResultado_ContratadaOrigemCod = H00EF3_A805ContagemResultado_ContratadaOrigemCod[0];
               n805ContagemResultado_ContratadaOrigemCod = H00EF3_n805ContagemResultado_ContratadaOrigemCod[0];
               A489ContagemResultado_SistemaCod = H00EF3_A489ContagemResultado_SistemaCod[0];
               n489ContagemResultado_SistemaCod = H00EF3_n489ContagemResultado_SistemaCod[0];
               A1553ContagemResultado_CntSrvCod = H00EF3_A1553ContagemResultado_CntSrvCod[0];
               n1553ContagemResultado_CntSrvCod = H00EF3_n1553ContagemResultado_CntSrvCod[0];
               A601ContagemResultado_Servico = H00EF3_A601ContagemResultado_Servico[0];
               n601ContagemResultado_Servico = H00EF3_n601ContagemResultado_Servico[0];
               A490ContagemResultado_ContratadaCod = H00EF3_A490ContagemResultado_ContratadaCod[0];
               n490ContagemResultado_ContratadaCod = H00EF3_n490ContagemResultado_ContratadaCod[0];
               A484ContagemResultado_StatusDmn = H00EF3_A484ContagemResultado_StatusDmn[0];
               n484ContagemResultado_StatusDmn = H00EF3_n484ContagemResultado_StatusDmn[0];
               A598ContagemResultado_Baseline = H00EF3_A598ContagemResultado_Baseline[0];
               n598ContagemResultado_Baseline = H00EF3_n598ContagemResultado_Baseline[0];
               A509ContagemrResultado_SistemaSigla = H00EF3_A509ContagemrResultado_SistemaSigla[0];
               n509ContagemrResultado_SistemaSigla = H00EF3_n509ContagemrResultado_SistemaSigla[0];
               A801ContagemResultado_ServicoSigla = H00EF3_A801ContagemResultado_ServicoSigla[0];
               n801ContagemResultado_ServicoSigla = H00EF3_n801ContagemResultado_ServicoSigla[0];
               A866ContagemResultado_ContratadaOrigemSigla = H00EF3_A866ContagemResultado_ContratadaOrigemSigla[0];
               n866ContagemResultado_ContratadaOrigemSigla = H00EF3_n866ContagemResultado_ContratadaOrigemSigla[0];
               A803ContagemResultado_ContratadaSigla = H00EF3_A803ContagemResultado_ContratadaSigla[0];
               n803ContagemResultado_ContratadaSigla = H00EF3_n803ContagemResultado_ContratadaSigla[0];
               A471ContagemResultado_DataDmn = H00EF3_A471ContagemResultado_DataDmn[0];
               A602ContagemResultado_OSVinculada = H00EF3_A602ContagemResultado_OSVinculada[0];
               n602ContagemResultado_OSVinculada = H00EF3_n602ContagemResultado_OSVinculada[0];
               A682ContagemResultado_PFBFMUltima = H00EF3_A682ContagemResultado_PFBFMUltima[0];
               A684ContagemResultado_PFBFSUltima = H00EF3_A684ContagemResultado_PFBFSUltima[0];
               A683ContagemResultado_PFLFMUltima = H00EF3_A683ContagemResultado_PFLFMUltima[0];
               A685ContagemResultado_PFLFSUltima = H00EF3_A685ContagemResultado_PFLFSUltima[0];
               A493ContagemResultado_DemandaFM = H00EF3_A493ContagemResultado_DemandaFM[0];
               n493ContagemResultado_DemandaFM = H00EF3_n493ContagemResultado_DemandaFM[0];
               A457ContagemResultado_Demanda = H00EF3_A457ContagemResultado_Demanda[0];
               n457ContagemResultado_Demanda = H00EF3_n457ContagemResultado_Demanda[0];
               A456ContagemResultado_Codigo = H00EF3_A456ContagemResultado_Codigo[0];
               A866ContagemResultado_ContratadaOrigemSigla = H00EF3_A866ContagemResultado_ContratadaOrigemSigla[0];
               n866ContagemResultado_ContratadaOrigemSigla = H00EF3_n866ContagemResultado_ContratadaOrigemSigla[0];
               A509ContagemrResultado_SistemaSigla = H00EF3_A509ContagemrResultado_SistemaSigla[0];
               n509ContagemrResultado_SistemaSigla = H00EF3_n509ContagemrResultado_SistemaSigla[0];
               A601ContagemResultado_Servico = H00EF3_A601ContagemResultado_Servico[0];
               n601ContagemResultado_Servico = H00EF3_n601ContagemResultado_Servico[0];
               A801ContagemResultado_ServicoSigla = H00EF3_A801ContagemResultado_ServicoSigla[0];
               n801ContagemResultado_ServicoSigla = H00EF3_n801ContagemResultado_ServicoSigla[0];
               A803ContagemResultado_ContratadaSigla = H00EF3_A803ContagemResultado_ContratadaSigla[0];
               n803ContagemResultado_ContratadaSigla = H00EF3_n803ContagemResultado_ContratadaSigla[0];
               A682ContagemResultado_PFBFMUltima = H00EF3_A682ContagemResultado_PFBFMUltima[0];
               A684ContagemResultado_PFBFSUltima = H00EF3_A684ContagemResultado_PFBFSUltima[0];
               A683ContagemResultado_PFLFMUltima = H00EF3_A683ContagemResultado_PFLFMUltima[0];
               A685ContagemResultado_PFLFSUltima = H00EF3_A685ContagemResultado_PFLFSUltima[0];
               GXt_decimal1 = A574ContagemResultado_PFFinal;
               new prc_contagemresultado_pffinal(context ).execute(  A456ContagemResultado_Codigo, out  GXt_decimal1) ;
               A574ContagemResultado_PFFinal = GXt_decimal1;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A574ContagemResultado_PFFinal", StringUtil.LTrim( StringUtil.Str( A574ContagemResultado_PFFinal, 14, 5)));
               A501ContagemResultado_OsFsOsFm = StringUtil.Trim( A457ContagemResultado_Demanda) + (String.IsNullOrEmpty(StringUtil.RTrim( A493ContagemResultado_DemandaFM)) ? "" : "|"+StringUtil.Trim( A493ContagemResultado_DemandaFM));
               /* Execute user event: E15EF2 */
               E15EF2 ();
               pr_default.readNext(0);
            }
            pr_default.close(0);
            wbEnd = 8;
            WBEF0( ) ;
         }
         nGXsfl_8_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         return (int)(-1) ;
      }

      protected int subGrid_Recordcount( )
      {
         return (int)(-1) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         return (int)(-1) ;
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(-1) ;
      }

      protected void STRUPEF0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         edtavContratadasigla_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContratadasigla_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratadasigla_Enabled), 5, 0)));
         edtavDemanda_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDemanda_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDemanda_Enabled), 5, 0)));
         edtavPfbfs_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavPfbfs_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPfbfs_Enabled), 5, 0)));
         edtavPflfs_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavPflfs_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPflfs_Enabled), 5, 0)));
         edtavPfbfm_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavPfbfm_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPfbfm_Enabled), 5, 0)));
         edtavPflfm_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavPflfm_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPflfm_Enabled), 5, 0)));
         edtavPffinal_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavPffinal_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPffinal_Enabled), 5, 0)));
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E13EF2 */
         E13EF2 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            /* Read saved values. */
            nRC_GXsfl_8 = (short)(context.localUtil.CToN( cgiGet( sPrefix+"nRC_GXsfl_8"), ",", "."));
            wcpOAV7ContagemResultado_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV7ContagemResultado_Codigo"), ",", "."));
            wcpOAV6ContagemResultado_OSVinculada = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV6ContagemResultado_OSVinculada"), ",", "."));
            AV7ContagemResultado_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"vCONTAGEMRESULTADO_CODIGO"), ",", "."));
            AV13Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"vCODIGO"), ",", "."));
            Confirmpanel_Title = cgiGet( sPrefix+"CONFIRMPANEL_Title");
            Confirmpanel_Confirmationtext = cgiGet( sPrefix+"CONFIRMPANEL_Confirmationtext");
            Confirmpanel_Yesbuttoncaption = cgiGet( sPrefix+"CONFIRMPANEL_Yesbuttoncaption");
            Confirmpanel_Nobuttoncaption = cgiGet( sPrefix+"CONFIRMPANEL_Nobuttoncaption");
            Confirmpanel_Yesbuttonposition = cgiGet( sPrefix+"CONFIRMPANEL_Yesbuttonposition");
            Confirmpanel_Result = cgiGet( sPrefix+"CONFIRMPANEL_Result");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E13EF2 */
         E13EF2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E13EF2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV11WWPContext) ;
      }

      protected void E14EF2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         bttBtnvincular_Enabled = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtnvincular_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnvincular_Enabled), 5, 0)));
         AV10ContagemResultado.Load(AV7ContagemResultado_Codigo);
         AV15SS = (bool)(((AV10ContagemResultado.gxTpr_Contagemresultado_servicoss>0)));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15SS", AV15SS);
         AV16StatusSS = AV10ContagemResultado.gxTpr_Contagemresultado_statusdmn;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16StatusSS", AV16StatusSS);
         if ( AV10ContagemResultado.gxTpr_Contagemresultado_osvinculada > 0 )
         {
            bttBtnvincular_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtnvincular_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnvincular_Enabled), 5, 0)));
            GXt_char2 = "";
            GXt_int3 = AV10ContagemResultado.gxTpr_Contagemresultado_osvinculada;
            new prc_servicosigla(context ).execute( ref  GXt_int3, out  GXt_char2) ;
            AV10ContagemResultado.gxTpr_Contagemresultado_osvinculada = GXt_int3;
            bttBtnvincular_Tooltiptext = "J� vinculada � OS "+StringUtil.Trim( AV10ContagemResultado.gxTpr_Contagemresultado_dmnvinculada)+" de "+GXt_char2;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtnvincular_Internalname, "Tooltiptext", bttBtnvincular_Tooltiptext);
         }
         if ( AV11WWPContext.gxTpr_Update )
         {
            edtavDesvincular_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDesvincular_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDesvincular_Enabled), 5, 0)));
            edtavIgualar_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavIgualar_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavIgualar_Enabled), 5, 0)));
         }
         else
         {
            bttBtnvincular_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtnvincular_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnvincular_Enabled), 5, 0)));
            edtavDesvincular_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDesvincular_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDesvincular_Enabled), 5, 0)));
            edtavIgualar_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavIgualar_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavIgualar_Enabled), 5, 0)));
            bttBtnvincular_Tooltiptext = "A��o n�o permitida ao seu usu�rio";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtnvincular_Internalname, "Tooltiptext", bttBtnvincular_Tooltiptext);
            edtavDesvincular_Tooltiptext = "A��o n�o permitida ao seu usu�rio";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDesvincular_Internalname, "Tooltiptext", edtavDesvincular_Tooltiptext);
            edtavIgualar_Tooltiptext = "A��o n�o permitida ao seu usu�rio";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavIgualar_Internalname, "Tooltiptext", edtavIgualar_Tooltiptext);
         }
         edtContagemResultado_OsFsOsFm_Linktarget = "_blank";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContagemResultado_OsFsOsFm_Internalname, "Linktarget", edtContagemResultado_OsFsOsFm_Linktarget);
      }

      private void E15EF2( )
      {
         /* Load Routine */
         AV17ContratadaSigla = A803ContagemResultado_ContratadaSigla;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavContratadasigla_Internalname, AV17ContratadaSigla);
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vCONTRATADASIGLA"+"_"+sGXsfl_8_idx, GetSecureSignedToken( sPrefix+sGXsfl_8_idx, StringUtil.RTrim( context.localUtil.Format( AV17ContratadaSigla, "@!"))));
         AV18Demanda = A457ContagemResultado_Demanda;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavDemanda_Internalname, AV18Demanda);
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vDEMANDA"+"_"+sGXsfl_8_idx, GetSecureSignedToken( sPrefix+sGXsfl_8_idx, StringUtil.RTrim( context.localUtil.Format( AV18Demanda, "@!"))));
         AV9Desvincular = context.GetImagePath( "ee240869-8f14-4640-bb08-f1db2cef8c25", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavDesvincular_Internalname, AV9Desvincular);
         AV30Desvincular_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "ee240869-8f14-4640-bb08-f1db2cef8c25", "", context.GetTheme( )));
         AV14Igualar = context.GetImagePath( "cc99b486-d697-4469-92b6-2667f7bbc318", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavIgualar_Internalname, AV14Igualar);
         AV31Igualar_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "cc99b486-d697-4469-92b6-2667f7bbc318", "", context.GetTheme( )));
         edtavIgualar_Visible = (AV15SS&&((StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "R")==0)||(StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "H")==0)||(StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "O")==0)||(StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "P")==0)||(StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "L")==0))&&(StringUtil.StrCmp(AV16StatusSS, A484ContagemResultado_StatusDmn)!=0) ? 1 : 0);
         if ( AV11WWPContext.gxTpr_Update )
         {
            edtavDesvincular_Tooltiptext = "Desvincular desta "+(AV15SS ? "SS" : "OS");
            edtavIgualar_Tooltiptext = "Igualar o status desta SS ao status da OS vinculada ("+gxdomainstatusdemanda.getDescription(context,A484ContagemResultado_StatusDmn)+")";
         }
         if ( ( A490ContagemResultado_ContratadaCod == AV11WWPContext.gxTpr_Contratada_codigo ) || AV11WWPContext.gxTpr_Userehcontratante || AV11WWPContext.gxTpr_Userehadministradorgam )
         {
            edtContagemResultado_OsFsOsFm_Link = formatLink("viewcontagemresultado.aspx") + "?" + UrlEncode("" +A456ContagemResultado_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
            AV24PFBFM = StringUtil.Str( A682ContagemResultado_PFBFMUltima, 14, 3);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavPfbfm_Internalname, AV24PFBFM);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vPFBFM"+"_"+sGXsfl_8_idx, GetSecureSignedToken( sPrefix+sGXsfl_8_idx, StringUtil.RTrim( context.localUtil.Format( AV24PFBFM, ""))));
            AV22PFBFS = StringUtil.Str( A684ContagemResultado_PFBFSUltima, 14, 3);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavPfbfs_Internalname, AV22PFBFS);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vPFBFS"+"_"+sGXsfl_8_idx, GetSecureSignedToken( sPrefix+sGXsfl_8_idx, StringUtil.RTrim( context.localUtil.Format( AV22PFBFS, ""))));
            AV25PFLFM = StringUtil.Str( A683ContagemResultado_PFLFMUltima, 14, 3);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavPflfm_Internalname, AV25PFLFM);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vPFLFM"+"_"+sGXsfl_8_idx, GetSecureSignedToken( sPrefix+sGXsfl_8_idx, StringUtil.RTrim( context.localUtil.Format( AV25PFLFM, ""))));
            AV23PFLFS = StringUtil.Str( A685ContagemResultado_PFLFSUltima, 14, 3);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavPflfs_Internalname, AV23PFLFS);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vPFLFS"+"_"+sGXsfl_8_idx, GetSecureSignedToken( sPrefix+sGXsfl_8_idx, StringUtil.RTrim( context.localUtil.Format( AV23PFLFS, ""))));
            AV27PFFINAL = StringUtil.Str( A574ContagemResultado_PFFinal, 14, 3);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavPffinal_Internalname, AV27PFFINAL);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vPFFINAL"+"_"+sGXsfl_8_idx, GetSecureSignedToken( sPrefix+sGXsfl_8_idx, StringUtil.RTrim( context.localUtil.Format( AV27PFFINAL, ""))));
         }
         else
         {
            edtContagemResultado_OsFsOsFm_Link = "";
            AV24PFBFM = "";
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavPfbfm_Internalname, AV24PFBFM);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vPFBFM"+"_"+sGXsfl_8_idx, GetSecureSignedToken( sPrefix+sGXsfl_8_idx, StringUtil.RTrim( context.localUtil.Format( AV24PFBFM, ""))));
            AV22PFBFS = "";
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavPfbfs_Internalname, AV22PFBFS);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vPFBFS"+"_"+sGXsfl_8_idx, GetSecureSignedToken( sPrefix+sGXsfl_8_idx, StringUtil.RTrim( context.localUtil.Format( AV22PFBFS, ""))));
            AV25PFLFM = "";
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavPflfm_Internalname, AV25PFLFM);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vPFLFM"+"_"+sGXsfl_8_idx, GetSecureSignedToken( sPrefix+sGXsfl_8_idx, StringUtil.RTrim( context.localUtil.Format( AV25PFLFM, ""))));
            AV23PFLFS = "";
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavPflfs_Internalname, AV23PFLFS);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vPFLFS"+"_"+sGXsfl_8_idx, GetSecureSignedToken( sPrefix+sGXsfl_8_idx, StringUtil.RTrim( context.localUtil.Format( AV23PFLFS, ""))));
            AV27PFFINAL = "";
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavPffinal_Internalname, AV27PFFINAL);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vPFFINAL"+"_"+sGXsfl_8_idx, GetSecureSignedToken( sPrefix+sGXsfl_8_idx, StringUtil.RTrim( context.localUtil.Format( AV27PFFINAL, ""))));
         }
         sendrow_82( ) ;
         if ( isFullAjaxMode( ) && ( nGXsfl_8_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(8, GridRow);
         }
      }

      protected void E11EF2( )
      {
         /* Confirmpanel_Close Routine */
         if ( StringUtil.StrCmp(Confirmpanel_Result, "Yes") == 0 )
         {
            new prc_desvincularos(context ).execute(  AV13Codigo) ;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13Codigo), 6, 0)));
            /* Execute user subroutine: 'RECARREGAR' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
      }

      protected void E12EF2( )
      {
         /* 'VincularDemanda' Routine */
         context.PopUp(formatLink("wp_vincularos.aspx") + "?" + UrlEncode("" +AV7ContagemResultado_Codigo) + "," + UrlEncode(StringUtil.RTrim("")), new Object[] {"AV7ContagemResultado_Codigo","AV12Confirmado"});
         /* Execute user subroutine: 'RECARREGAR' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E16EF2( )
      {
         /* Igualar_Click Routine */
         AV19NovoStatus = A484ContagemResultado_StatusDmn;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19NovoStatus", AV19NovoStatus);
         /* Execute user subroutine: 'IGUALARSTATUS' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S122( )
      {
         /* 'IGUALARSTATUS' Routine */
         AV10ContagemResultado.Load(AV7ContagemResultado_Codigo);
         AV10ContagemResultado.gxTpr_Contagemresultado_statusdmn = AV19NovoStatus;
         AV10ContagemResultado.Save();
         if ( AV10ContagemResultado.Success() )
         {
            new prc_inslogresponsavel(context ).execute( ref  AV7ContagemResultado_Codigo,  0,  "U",  "D",  AV11WWPContext.gxTpr_Userid,  0,  AV16StatusSS,  AV19NovoStatus,  "Status igualado ao da OS "+StringUtil.Trim( AV18Demanda)+" da Prestadora "+StringUtil.Trim( AV17ContratadaSigla),  AV10ContagemResultado.gxTpr_Contagemresultado_dataprevista,  false) ;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContagemResultado_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16StatusSS", AV16StatusSS);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19NovoStatus", AV19NovoStatus);
            context.CommitDataStores( "WC_DemandasVinculadas");
            /* Execute user subroutine: 'RECARREGAR' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         else
         {
            AV33GXV2 = 1;
            AV32GXV1 = AV10ContagemResultado.GetMessages();
            while ( AV33GXV2 <= AV32GXV1.Count )
            {
               AV20message = ((SdtMessages_Message)AV32GXV1.Item(AV33GXV2));
               GX_msglist.addItem(AV20message.gxTpr_Description);
               AV33GXV2 = (int)(AV33GXV2+1);
            }
         }
      }

      protected void S112( )
      {
         /* 'RECARREGAR' Routine */
         context.wjLoc = formatLink("viewcontagemresultado.aspx") + "?" + UrlEncode("" +AV7ContagemResultado_Codigo) + "," + UrlEncode(StringUtil.RTrim("Vinculadas"));
         context.wjLocDisableFrm = 1;
      }

      protected void wb_table3_34_EF2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable3_Internalname, tblTable3_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"CONFIRMPANELContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+sPrefix+"CONFIRMPANELContainer"+"Body"+"\" style=\"display:none;\">") ;
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_34_EF2e( true) ;
         }
         else
         {
            wb_table3_34_EF2e( false) ;
         }
      }

      protected void wb_table2_31_EF2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable2_Internalname, tblTable2_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_31_EF2e( true) ;
         }
         else
         {
            wb_table2_31_EF2e( false) ;
         }
      }

      protected void wb_table1_2_EF2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"DivS\" data-gxgridid=\"8\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Contagem Resultado_Codigo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Contagem Resultado_Codigo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Contratada Sigla") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Demanda") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(70), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Demanda") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "OS FS/FM") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Prestadora") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Origem") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Servi�o") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Sistema") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "BS") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "PFB FS") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "PFL FS") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "PFB FM") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "PFL FM") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "PF Final") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Status Dmn") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavIgualar_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", sPrefix);
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV9Desvincular));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDesvincular_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDesvincular_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A456ContagemResultado_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A602ContagemResultado_OSVinculada), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( AV17ContratadaSigla));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavContratadasigla_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", AV18Demanda);
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDemanda_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.Format(A471ContagemResultado_DataDmn, "99/99/99"));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A501ContagemResultado_OsFsOsFm);
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtContagemResultado_OsFsOsFm_Link));
               GridColumn.AddObjectProperty("Linktarget", StringUtil.RTrim( edtContagemResultado_OsFsOsFm_Linktarget));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A803ContagemResultado_ContratadaSigla));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A866ContagemResultado_ContratadaOrigemSigla));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A801ContagemResultado_ServicoSigla));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A509ContagemrResultado_SistemaSigla));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.BoolToStr( A598ContagemResultado_Baseline));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( AV22PFBFS));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavPfbfs_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( AV23PFLFS));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavPflfs_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( AV24PFBFM));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavPfbfm_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( AV25PFLFM));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavPflfm_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( AV27PFFINAL));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavPffinal_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A484ContagemResultado_StatusDmn));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV14Igualar));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavIgualar_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavIgualar_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavIgualar_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 8 )
         {
            wbEnd = 0;
            nRC_GXsfl_8 = (short)(nGXsfl_8_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid(sPrefix+"_"+"Grid", GridContainer);
               if ( ! isAjaxCallMode( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+sPrefix+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 30,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Button";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnvincular_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(8), 1, 0)+","+"null"+");", "Vincular com outra OS", bttBtnvincular_Jsonclick, 5, bttBtnvincular_Tooltiptext, "", StyleString, ClassString, 1, bttBtnvincular_Enabled, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'VINCULARDEMANDA\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_WC_DemandasVinculadas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_EF2e( true) ;
         }
         else
         {
            wb_table1_2_EF2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7ContagemResultado_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContagemResultado_Codigo), 6, 0)));
         AV6ContagemResultado_OSVinculada = Convert.ToInt32(getParm(obj,1));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV6ContagemResultado_OSVinculada", StringUtil.LTrim( StringUtil.Str( (decimal)(AV6ContagemResultado_OSVinculada), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAEF2( ) ;
         WSEF2( ) ;
         WEEF2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlAV7ContagemResultado_Codigo = (String)((String)getParm(obj,0));
         sCtrlAV6ContagemResultado_OSVinculada = (String)((String)getParm(obj,1));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PAEF2( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "wc_demandasvinculadas");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PAEF2( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            AV7ContagemResultado_Codigo = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContagemResultado_Codigo), 6, 0)));
            AV6ContagemResultado_OSVinculada = Convert.ToInt32(getParm(obj,3));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV6ContagemResultado_OSVinculada", StringUtil.LTrim( StringUtil.Str( (decimal)(AV6ContagemResultado_OSVinculada), 6, 0)));
         }
         wcpOAV7ContagemResultado_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV7ContagemResultado_Codigo"), ",", "."));
         wcpOAV6ContagemResultado_OSVinculada = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV6ContagemResultado_OSVinculada"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( AV7ContagemResultado_Codigo != wcpOAV7ContagemResultado_Codigo ) || ( AV6ContagemResultado_OSVinculada != wcpOAV6ContagemResultado_OSVinculada ) ) )
         {
            setjustcreated();
         }
         wcpOAV7ContagemResultado_Codigo = AV7ContagemResultado_Codigo;
         wcpOAV6ContagemResultado_OSVinculada = AV6ContagemResultado_OSVinculada;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlAV7ContagemResultado_Codigo = cgiGet( sPrefix+"AV7ContagemResultado_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlAV7ContagemResultado_Codigo) > 0 )
         {
            AV7ContagemResultado_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlAV7ContagemResultado_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContagemResultado_Codigo), 6, 0)));
         }
         else
         {
            AV7ContagemResultado_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"AV7ContagemResultado_Codigo_PARM"), ",", "."));
         }
         sCtrlAV6ContagemResultado_OSVinculada = cgiGet( sPrefix+"AV6ContagemResultado_OSVinculada_CTRL");
         if ( StringUtil.Len( sCtrlAV6ContagemResultado_OSVinculada) > 0 )
         {
            AV6ContagemResultado_OSVinculada = (int)(context.localUtil.CToN( cgiGet( sCtrlAV6ContagemResultado_OSVinculada), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV6ContagemResultado_OSVinculada", StringUtil.LTrim( StringUtil.Str( (decimal)(AV6ContagemResultado_OSVinculada), 6, 0)));
         }
         else
         {
            AV6ContagemResultado_OSVinculada = (int)(context.localUtil.CToN( cgiGet( sPrefix+"AV6ContagemResultado_OSVinculada_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PAEF2( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSEF2( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSEF2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"AV7ContagemResultado_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7ContagemResultado_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlAV7ContagemResultado_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"AV7ContagemResultado_Codigo_CTRL", StringUtil.RTrim( sCtrlAV7ContagemResultado_Codigo));
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"AV6ContagemResultado_OSVinculada_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV6ContagemResultado_OSVinculada), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlAV6ContagemResultado_OSVinculada)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"AV6ContagemResultado_OSVinculada_CTRL", StringUtil.RTrim( sCtrlAV6ContagemResultado_OSVinculada));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WEEF2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020621615483");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("wc_demandasvinculadas.js", "?2020621615483");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/ConfirmPanel/BootstrapConfirmPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_82( )
      {
         edtavDesvincular_Internalname = sPrefix+"vDESVINCULAR_"+sGXsfl_8_idx;
         edtContagemResultado_Codigo_Internalname = sPrefix+"CONTAGEMRESULTADO_CODIGO_"+sGXsfl_8_idx;
         edtContagemResultado_OSVinculada_Internalname = sPrefix+"CONTAGEMRESULTADO_OSVINCULADA_"+sGXsfl_8_idx;
         edtavContratadasigla_Internalname = sPrefix+"vCONTRATADASIGLA_"+sGXsfl_8_idx;
         edtavDemanda_Internalname = sPrefix+"vDEMANDA_"+sGXsfl_8_idx;
         edtContagemResultado_DataDmn_Internalname = sPrefix+"CONTAGEMRESULTADO_DATADMN_"+sGXsfl_8_idx;
         edtContagemResultado_OsFsOsFm_Internalname = sPrefix+"CONTAGEMRESULTADO_OSFSOSFM_"+sGXsfl_8_idx;
         edtContagemResultado_ContratadaSigla_Internalname = sPrefix+"CONTAGEMRESULTADO_CONTRATADASIGLA_"+sGXsfl_8_idx;
         edtContagemResultado_ContratadaOrigemSigla_Internalname = sPrefix+"CONTAGEMRESULTADO_CONTRATADAORIGEMSIGLA_"+sGXsfl_8_idx;
         edtContagemResultado_ServicoSigla_Internalname = sPrefix+"CONTAGEMRESULTADO_SERVICOSIGLA_"+sGXsfl_8_idx;
         edtContagemrResultado_SistemaSigla_Internalname = sPrefix+"CONTAGEMRRESULTADO_SISTEMASIGLA_"+sGXsfl_8_idx;
         cmbContagemResultado_Baseline_Internalname = sPrefix+"CONTAGEMRESULTADO_BASELINE_"+sGXsfl_8_idx;
         edtavPfbfs_Internalname = sPrefix+"vPFBFS_"+sGXsfl_8_idx;
         edtavPflfs_Internalname = sPrefix+"vPFLFS_"+sGXsfl_8_idx;
         edtavPfbfm_Internalname = sPrefix+"vPFBFM_"+sGXsfl_8_idx;
         edtavPflfm_Internalname = sPrefix+"vPFLFM_"+sGXsfl_8_idx;
         edtavPffinal_Internalname = sPrefix+"vPFFINAL_"+sGXsfl_8_idx;
         cmbContagemResultado_StatusDmn_Internalname = sPrefix+"CONTAGEMRESULTADO_STATUSDMN_"+sGXsfl_8_idx;
         edtavIgualar_Internalname = sPrefix+"vIGUALAR_"+sGXsfl_8_idx;
      }

      protected void SubsflControlProps_fel_82( )
      {
         edtavDesvincular_Internalname = sPrefix+"vDESVINCULAR_"+sGXsfl_8_fel_idx;
         edtContagemResultado_Codigo_Internalname = sPrefix+"CONTAGEMRESULTADO_CODIGO_"+sGXsfl_8_fel_idx;
         edtContagemResultado_OSVinculada_Internalname = sPrefix+"CONTAGEMRESULTADO_OSVINCULADA_"+sGXsfl_8_fel_idx;
         edtavContratadasigla_Internalname = sPrefix+"vCONTRATADASIGLA_"+sGXsfl_8_fel_idx;
         edtavDemanda_Internalname = sPrefix+"vDEMANDA_"+sGXsfl_8_fel_idx;
         edtContagemResultado_DataDmn_Internalname = sPrefix+"CONTAGEMRESULTADO_DATADMN_"+sGXsfl_8_fel_idx;
         edtContagemResultado_OsFsOsFm_Internalname = sPrefix+"CONTAGEMRESULTADO_OSFSOSFM_"+sGXsfl_8_fel_idx;
         edtContagemResultado_ContratadaSigla_Internalname = sPrefix+"CONTAGEMRESULTADO_CONTRATADASIGLA_"+sGXsfl_8_fel_idx;
         edtContagemResultado_ContratadaOrigemSigla_Internalname = sPrefix+"CONTAGEMRESULTADO_CONTRATADAORIGEMSIGLA_"+sGXsfl_8_fel_idx;
         edtContagemResultado_ServicoSigla_Internalname = sPrefix+"CONTAGEMRESULTADO_SERVICOSIGLA_"+sGXsfl_8_fel_idx;
         edtContagemrResultado_SistemaSigla_Internalname = sPrefix+"CONTAGEMRRESULTADO_SISTEMASIGLA_"+sGXsfl_8_fel_idx;
         cmbContagemResultado_Baseline_Internalname = sPrefix+"CONTAGEMRESULTADO_BASELINE_"+sGXsfl_8_fel_idx;
         edtavPfbfs_Internalname = sPrefix+"vPFBFS_"+sGXsfl_8_fel_idx;
         edtavPflfs_Internalname = sPrefix+"vPFLFS_"+sGXsfl_8_fel_idx;
         edtavPfbfm_Internalname = sPrefix+"vPFBFM_"+sGXsfl_8_fel_idx;
         edtavPflfm_Internalname = sPrefix+"vPFLFM_"+sGXsfl_8_fel_idx;
         edtavPffinal_Internalname = sPrefix+"vPFFINAL_"+sGXsfl_8_fel_idx;
         cmbContagemResultado_StatusDmn_Internalname = sPrefix+"CONTAGEMRESULTADO_STATUSDMN_"+sGXsfl_8_fel_idx;
         edtavIgualar_Internalname = sPrefix+"vIGUALAR_"+sGXsfl_8_fel_idx;
      }

      protected void sendrow_82( )
      {
         SubsflControlProps_82( ) ;
         WBEF0( ) ;
         GridRow = GXWebRow.GetNew(context,GridContainer);
         if ( subGrid_Backcolorstyle == 0 )
         {
            /* None style subfile background logic. */
            subGrid_Backstyle = 0;
            if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
            {
               subGrid_Linesclass = subGrid_Class+"Odd";
            }
         }
         else if ( subGrid_Backcolorstyle == 1 )
         {
            /* Uniform style subfile background logic. */
            subGrid_Backstyle = 0;
            subGrid_Backcolor = subGrid_Allbackcolor;
            if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
            {
               subGrid_Linesclass = subGrid_Class+"Uniform";
            }
         }
         else if ( subGrid_Backcolorstyle == 2 )
         {
            /* Header style subfile background logic. */
            subGrid_Backstyle = 1;
            if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
            {
               subGrid_Linesclass = subGrid_Class+"Odd";
            }
            subGrid_Backcolor = (int)(0xFFFFFF);
         }
         else if ( subGrid_Backcolorstyle == 3 )
         {
            /* Report style subfile background logic. */
            subGrid_Backstyle = 1;
            if ( ((int)((nGXsfl_8_idx) % (2))) == 0 )
            {
               subGrid_Backcolor = (int)(0xFFFFFF);
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Even";
               }
            }
            else
            {
               subGrid_Backcolor = (int)(0xFFFFFF);
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
         }
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<tr ") ;
            context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
            context.WriteHtmlText( " gxrow=\""+sGXsfl_8_idx+"\">") ;
         }
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
         }
         /* Active Bitmap Variable */
         TempTags = " " + ((edtavDesvincular_Enabled!=0)&&(edtavDesvincular_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 9,'"+sPrefix+"',false,'',8)\"" : " ");
         ClassString = "Image";
         StyleString = "";
         AV9Desvincular_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV9Desvincular))&&String.IsNullOrEmpty(StringUtil.RTrim( AV30Desvincular_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV9Desvincular)));
         GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDesvincular_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV9Desvincular)) ? AV30Desvincular_GXI : context.PathToRelativeUrl( AV9Desvincular)),(String)"",(String)"",(String)"",context.GetTheme( ),(short)-1,(int)edtavDesvincular_Enabled,(String)"",(String)edtavDesvincular_Tooltiptext,(short)0,(short)-1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)7,(String)edtavDesvincular_Jsonclick,(String)"'"+sPrefix+"'"+",false,"+"'"+"e17ef2_client"+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV9Desvincular_IsBlob,(bool)false});
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultado_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A456ContagemResultado_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A456ContagemResultado_Codigo), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultado_Codigo_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)8,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
         }
         /* Single line edit */
         ROClassString = "BootstrapAttribute";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultado_OSVinculada_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A602ContagemResultado_OSVinculada), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A602ContagemResultado_OSVinculada), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultado_OSVinculada_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)8,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+"display:none;"+"\">") ;
         }
         /* Single line edit */
         TempTags = " " + ((edtavContratadasigla_Enabled!=0)&&(edtavContratadasigla_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 12,'"+sPrefix+"',false,'"+sGXsfl_8_idx+"',8)\"" : " ");
         ROClassString = "Attribute";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavContratadasigla_Internalname,StringUtil.RTrim( AV17ContratadaSigla),StringUtil.RTrim( context.localUtil.Format( AV17ContratadaSigla, "@!")),TempTags+((edtavContratadasigla_Enabled!=0)&&(edtavContratadasigla_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavContratadasigla_Enabled!=0)&&(edtavContratadasigla_Visible!=0) ? " onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,12);\"" : " "),(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavContratadasigla_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)0,(int)edtavContratadasigla_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)15,(short)0,(short)0,(short)8,(short)1,(short)-1,(short)-1,(bool)true,(String)"Sigla",(String)"left",(bool)true});
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+"display:none;"+"\">") ;
         }
         /* Single line edit */
         TempTags = " " + ((edtavDemanda_Enabled!=0)&&(edtavDemanda_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 13,'"+sPrefix+"',false,'"+sGXsfl_8_idx+"',8)\"" : " ");
         ROClassString = "Attribute";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavDemanda_Internalname,(String)AV18Demanda,StringUtil.RTrim( context.localUtil.Format( AV18Demanda, "@!")),TempTags+((edtavDemanda_Enabled!=0)&&(edtavDemanda_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavDemanda_Enabled!=0)&&(edtavDemanda_Visible!=0) ? " onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,13);\"" : " "),(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavDemanda_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)0,(int)edtavDemanda_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)30,(short)0,(short)0,(short)8,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         ROClassString = "BootstrapAttribute";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultado_DataDmn_Internalname,context.localUtil.Format(A471ContagemResultado_DataDmn, "99/99/99"),context.localUtil.Format( A471ContagemResultado_DataDmn, "99/99/99"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultado_DataDmn_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)70,(String)"px",(short)17,(String)"px",(short)8,(short)0,(short)0,(short)8,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         ROClassString = "BootstrapAttribute";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultado_OsFsOsFm_Internalname,(String)A501ContagemResultado_OsFsOsFm,(String)"",(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)edtContagemResultado_OsFsOsFm_Link,(String)edtContagemResultado_OsFsOsFm_Linktarget,(String)"",(String)"",(String)edtContagemResultado_OsFsOsFm_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)30,(short)0,(short)0,(short)8,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultado_ContratadaSigla_Internalname,StringUtil.RTrim( A803ContagemResultado_ContratadaSigla),StringUtil.RTrim( context.localUtil.Format( A803ContagemResultado_ContratadaSigla, "@!")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultado_ContratadaSigla_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)15,(short)0,(short)0,(short)8,(short)1,(short)-1,(short)-1,(bool)true,(String)"Sigla",(String)"left",(bool)true});
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         ROClassString = "BootstrapAttribute";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultado_ContratadaOrigemSigla_Internalname,StringUtil.RTrim( A866ContagemResultado_ContratadaOrigemSigla),StringUtil.RTrim( context.localUtil.Format( A866ContagemResultado_ContratadaOrigemSigla, "@!")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultado_ContratadaOrigemSigla_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)15,(short)0,(short)0,(short)8,(short)1,(short)-1,(short)-1,(bool)true,(String)"Sigla",(String)"left",(bool)true});
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultado_ServicoSigla_Internalname,StringUtil.RTrim( A801ContagemResultado_ServicoSigla),StringUtil.RTrim( context.localUtil.Format( A801ContagemResultado_ServicoSigla, "@!")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultado_ServicoSigla_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)15,(short)0,(short)0,(short)8,(short)1,(short)-1,(short)-1,(bool)true,(String)"Sigla",(String)"left",(bool)true});
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         ROClassString = "BootstrapAttribute";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemrResultado_SistemaSigla_Internalname,StringUtil.RTrim( A509ContagemrResultado_SistemaSigla),StringUtil.RTrim( context.localUtil.Format( A509ContagemrResultado_SistemaSigla, "@!")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemrResultado_SistemaSigla_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)25,(short)0,(short)0,(short)8,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
         }
         if ( ( nGXsfl_8_idx == 1 ) && isAjaxCallMode( ) )
         {
            GXCCtl = "CONTAGEMRESULTADO_BASELINE_" + sGXsfl_8_idx;
            cmbContagemResultado_Baseline.Name = GXCCtl;
            cmbContagemResultado_Baseline.WebTags = "";
            cmbContagemResultado_Baseline.addItem(StringUtil.BoolToStr( true), "*", 0);
            cmbContagemResultado_Baseline.addItem(StringUtil.BoolToStr( false), "", 0);
            if ( cmbContagemResultado_Baseline.ItemCount > 0 )
            {
               A598ContagemResultado_Baseline = StringUtil.StrToBool( cmbContagemResultado_Baseline.getValidValue(StringUtil.BoolToStr( A598ContagemResultado_Baseline)));
               n598ContagemResultado_Baseline = false;
            }
         }
         /* ComboBox */
         GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbContagemResultado_Baseline,(String)cmbContagemResultado_Baseline_Internalname,StringUtil.BoolToStr( A598ContagemResultado_Baseline),(short)1,(String)cmbContagemResultado_Baseline_Jsonclick,(short)0,(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"boolean",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
         cmbContagemResultado_Baseline.CurrentValue = StringUtil.BoolToStr( A598ContagemResultado_Baseline);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbContagemResultado_Baseline_Internalname, "Values", (String)(cmbContagemResultado_Baseline.ToJavascriptSource()));
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         TempTags = " " + ((edtavPfbfs_Enabled!=0)&&(edtavPfbfs_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 21,'"+sPrefix+"',false,'"+sGXsfl_8_idx+"',8)\"" : " ");
         ROClassString = "Attribute";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavPfbfs_Internalname,StringUtil.RTrim( AV22PFBFS),(String)"",TempTags+((edtavPfbfs_Enabled!=0)&&(edtavPfbfs_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavPfbfs_Enabled!=0)&&(edtavPfbfs_Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,21);\"" : " "),(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavPfbfs_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavPfbfs_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)8,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"right",(bool)true});
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         TempTags = " " + ((edtavPflfs_Enabled!=0)&&(edtavPflfs_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 22,'"+sPrefix+"',false,'"+sGXsfl_8_idx+"',8)\"" : " ");
         ROClassString = "Attribute";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavPflfs_Internalname,StringUtil.RTrim( AV23PFLFS),(String)"",TempTags+((edtavPflfs_Enabled!=0)&&(edtavPflfs_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavPflfs_Enabled!=0)&&(edtavPflfs_Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,22);\"" : " "),(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavPflfs_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavPflfs_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)8,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"right",(bool)true});
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         TempTags = " " + ((edtavPfbfm_Enabled!=0)&&(edtavPfbfm_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 23,'"+sPrefix+"',false,'"+sGXsfl_8_idx+"',8)\"" : " ");
         ROClassString = "Attribute";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavPfbfm_Internalname,StringUtil.RTrim( AV24PFBFM),(String)"",TempTags+((edtavPfbfm_Enabled!=0)&&(edtavPfbfm_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavPfbfm_Enabled!=0)&&(edtavPfbfm_Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,23);\"" : " "),(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavPfbfm_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavPfbfm_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)8,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"right",(bool)true});
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         TempTags = " " + ((edtavPflfm_Enabled!=0)&&(edtavPflfm_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 24,'"+sPrefix+"',false,'"+sGXsfl_8_idx+"',8)\"" : " ");
         ROClassString = "Attribute";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavPflfm_Internalname,StringUtil.RTrim( AV25PFLFM),(String)"",TempTags+((edtavPflfm_Enabled!=0)&&(edtavPflfm_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavPflfm_Enabled!=0)&&(edtavPflfm_Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,24);\"" : " "),(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavPflfm_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavPflfm_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)8,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"right",(bool)true});
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         TempTags = " " + ((edtavPffinal_Enabled!=0)&&(edtavPffinal_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 25,'"+sPrefix+"',false,'"+sGXsfl_8_idx+"',8)\"" : " ");
         ROClassString = "Attribute";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavPffinal_Internalname,StringUtil.RTrim( AV27PFFINAL),(String)"",TempTags+((edtavPffinal_Enabled!=0)&&(edtavPffinal_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavPffinal_Enabled!=0)&&(edtavPffinal_Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,25);\"" : " "),(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavPffinal_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavPffinal_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)8,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"right",(bool)true});
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
         }
         if ( ( nGXsfl_8_idx == 1 ) && isAjaxCallMode( ) )
         {
            GXCCtl = "CONTAGEMRESULTADO_STATUSDMN_" + sGXsfl_8_idx;
            cmbContagemResultado_StatusDmn.Name = GXCCtl;
            cmbContagemResultado_StatusDmn.WebTags = "";
            cmbContagemResultado_StatusDmn.addItem("B", "Stand by", 0);
            cmbContagemResultado_StatusDmn.addItem("S", "Solicitada", 0);
            cmbContagemResultado_StatusDmn.addItem("E", "Em An�lise", 0);
            cmbContagemResultado_StatusDmn.addItem("A", "Em execu��o", 0);
            cmbContagemResultado_StatusDmn.addItem("R", "Resolvida", 0);
            cmbContagemResultado_StatusDmn.addItem("C", "Conferida", 0);
            cmbContagemResultado_StatusDmn.addItem("D", "Retornada", 0);
            cmbContagemResultado_StatusDmn.addItem("H", "Homologada", 0);
            cmbContagemResultado_StatusDmn.addItem("O", "Aceite", 0);
            cmbContagemResultado_StatusDmn.addItem("P", "A Pagar", 0);
            cmbContagemResultado_StatusDmn.addItem("L", "Liquidada", 0);
            cmbContagemResultado_StatusDmn.addItem("X", "Cancelada", 0);
            cmbContagemResultado_StatusDmn.addItem("N", "N�o Faturada", 0);
            cmbContagemResultado_StatusDmn.addItem("J", "Planejamento", 0);
            cmbContagemResultado_StatusDmn.addItem("I", "An�lise Planejamento", 0);
            cmbContagemResultado_StatusDmn.addItem("T", "Validacao T�cnica", 0);
            cmbContagemResultado_StatusDmn.addItem("Q", "Validacao Qualidade", 0);
            cmbContagemResultado_StatusDmn.addItem("G", "Em Homologa��o", 0);
            cmbContagemResultado_StatusDmn.addItem("M", "Valida��o Mensura��o", 0);
            cmbContagemResultado_StatusDmn.addItem("U", "Rascunho", 0);
            if ( cmbContagemResultado_StatusDmn.ItemCount > 0 )
            {
               A484ContagemResultado_StatusDmn = cmbContagemResultado_StatusDmn.getValidValue(A484ContagemResultado_StatusDmn);
               n484ContagemResultado_StatusDmn = false;
            }
         }
         /* ComboBox */
         GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbContagemResultado_StatusDmn,(String)cmbContagemResultado_StatusDmn_Internalname,StringUtil.RTrim( A484ContagemResultado_StatusDmn),(short)1,(String)cmbContagemResultado_StatusDmn_Jsonclick,(short)0,(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"char",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
         cmbContagemResultado_StatusDmn.CurrentValue = StringUtil.RTrim( A484ContagemResultado_StatusDmn);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbContagemResultado_StatusDmn_Internalname, "Values", (String)(cmbContagemResultado_StatusDmn.ToJavascriptSource()));
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavIgualar_Visible==0) ? "display:none;" : "")+"\">") ;
         }
         /* Active Bitmap Variable */
         TempTags = " " + ((edtavIgualar_Enabled!=0)&&(edtavIgualar_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 27,'"+sPrefix+"',false,'',8)\"" : " ");
         ClassString = "Image";
         StyleString = "";
         AV14Igualar_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV14Igualar))&&String.IsNullOrEmpty(StringUtil.RTrim( AV31Igualar_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV14Igualar)));
         GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavIgualar_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV14Igualar)) ? AV31Igualar_GXI : context.PathToRelativeUrl( AV14Igualar)),(String)"",(String)"",(String)"",context.GetTheme( ),(int)edtavIgualar_Visible,(int)edtavIgualar_Enabled,(String)"",(String)edtavIgualar_Tooltiptext,(short)0,(short)-1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)5,(String)edtavIgualar_Jsonclick,"'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVIGUALAR.CLICK."+sGXsfl_8_idx+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV14Igualar_IsBlob,(bool)false});
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADO_CODIGO"+"_"+sGXsfl_8_idx, GetSecureSignedToken( sPrefix+sGXsfl_8_idx, context.localUtil.Format( (decimal)(A456ContagemResultado_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADO_OSVINCULADA"+"_"+sGXsfl_8_idx, GetSecureSignedToken( sPrefix+sGXsfl_8_idx, context.localUtil.Format( (decimal)(A602ContagemResultado_OSVinculada), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_vCONTRATADASIGLA"+"_"+sGXsfl_8_idx, GetSecureSignedToken( sPrefix+sGXsfl_8_idx, StringUtil.RTrim( context.localUtil.Format( AV17ContratadaSigla, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_vDEMANDA"+"_"+sGXsfl_8_idx, GetSecureSignedToken( sPrefix+sGXsfl_8_idx, StringUtil.RTrim( context.localUtil.Format( AV18Demanda, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADO_DATADMN"+"_"+sGXsfl_8_idx, GetSecureSignedToken( sPrefix+sGXsfl_8_idx, A471ContagemResultado_DataDmn));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADO_OSFSOSFM"+"_"+sGXsfl_8_idx, GetSecureSignedToken( sPrefix+sGXsfl_8_idx, StringUtil.RTrim( context.localUtil.Format( A501ContagemResultado_OsFsOsFm, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADO_BASELINE"+"_"+sGXsfl_8_idx, GetSecureSignedToken( sPrefix+sGXsfl_8_idx, A598ContagemResultado_Baseline));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_vPFBFS"+"_"+sGXsfl_8_idx, GetSecureSignedToken( sPrefix+sGXsfl_8_idx, StringUtil.RTrim( context.localUtil.Format( AV22PFBFS, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_vPFLFS"+"_"+sGXsfl_8_idx, GetSecureSignedToken( sPrefix+sGXsfl_8_idx, StringUtil.RTrim( context.localUtil.Format( AV23PFLFS, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_vPFBFM"+"_"+sGXsfl_8_idx, GetSecureSignedToken( sPrefix+sGXsfl_8_idx, StringUtil.RTrim( context.localUtil.Format( AV24PFBFM, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_vPFLFM"+"_"+sGXsfl_8_idx, GetSecureSignedToken( sPrefix+sGXsfl_8_idx, StringUtil.RTrim( context.localUtil.Format( AV25PFLFM, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_vPFFINAL"+"_"+sGXsfl_8_idx, GetSecureSignedToken( sPrefix+sGXsfl_8_idx, StringUtil.RTrim( context.localUtil.Format( AV27PFFINAL, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADO_STATUSDMN"+"_"+sGXsfl_8_idx, GetSecureSignedToken( sPrefix+sGXsfl_8_idx, StringUtil.RTrim( context.localUtil.Format( A484ContagemResultado_StatusDmn, ""))));
         GridContainer.AddRow(GridRow);
         nGXsfl_8_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_8_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_8_idx+1));
         sGXsfl_8_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_8_idx), 4, 0)), 4, "0");
         SubsflControlProps_82( ) ;
         /* End function sendrow_82 */
      }

      protected void init_default_properties( )
      {
         edtavDesvincular_Internalname = sPrefix+"vDESVINCULAR";
         edtContagemResultado_Codigo_Internalname = sPrefix+"CONTAGEMRESULTADO_CODIGO";
         edtContagemResultado_OSVinculada_Internalname = sPrefix+"CONTAGEMRESULTADO_OSVINCULADA";
         edtavContratadasigla_Internalname = sPrefix+"vCONTRATADASIGLA";
         edtavDemanda_Internalname = sPrefix+"vDEMANDA";
         edtContagemResultado_DataDmn_Internalname = sPrefix+"CONTAGEMRESULTADO_DATADMN";
         edtContagemResultado_OsFsOsFm_Internalname = sPrefix+"CONTAGEMRESULTADO_OSFSOSFM";
         edtContagemResultado_ContratadaSigla_Internalname = sPrefix+"CONTAGEMRESULTADO_CONTRATADASIGLA";
         edtContagemResultado_ContratadaOrigemSigla_Internalname = sPrefix+"CONTAGEMRESULTADO_CONTRATADAORIGEMSIGLA";
         edtContagemResultado_ServicoSigla_Internalname = sPrefix+"CONTAGEMRESULTADO_SERVICOSIGLA";
         edtContagemrResultado_SistemaSigla_Internalname = sPrefix+"CONTAGEMRRESULTADO_SISTEMASIGLA";
         cmbContagemResultado_Baseline_Internalname = sPrefix+"CONTAGEMRESULTADO_BASELINE";
         edtavPfbfs_Internalname = sPrefix+"vPFBFS";
         edtavPflfs_Internalname = sPrefix+"vPFLFS";
         edtavPfbfm_Internalname = sPrefix+"vPFBFM";
         edtavPflfm_Internalname = sPrefix+"vPFLFM";
         edtavPffinal_Internalname = sPrefix+"vPFFINAL";
         cmbContagemResultado_StatusDmn_Internalname = sPrefix+"CONTAGEMRESULTADO_STATUSDMN";
         edtavIgualar_Internalname = sPrefix+"vIGUALAR";
         bttBtnvincular_Internalname = sPrefix+"BTNVINCULAR";
         tblTable1_Internalname = sPrefix+"TABLE1";
         tblTable2_Internalname = sPrefix+"TABLE2";
         Confirmpanel_Internalname = sPrefix+"CONFIRMPANEL";
         tblTable3_Internalname = sPrefix+"TABLE3";
         Form.Internalname = sPrefix+"FORM";
         subGrid_Internalname = sPrefix+"GRID";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtavIgualar_Jsonclick = "";
         cmbContagemResultado_StatusDmn_Jsonclick = "";
         edtavPffinal_Jsonclick = "";
         edtavPffinal_Visible = -1;
         edtavPflfm_Jsonclick = "";
         edtavPflfm_Visible = -1;
         edtavPfbfm_Jsonclick = "";
         edtavPfbfm_Visible = -1;
         edtavPflfs_Jsonclick = "";
         edtavPflfs_Visible = -1;
         edtavPfbfs_Jsonclick = "";
         edtavPfbfs_Visible = -1;
         cmbContagemResultado_Baseline_Jsonclick = "";
         edtContagemrResultado_SistemaSigla_Jsonclick = "";
         edtContagemResultado_ServicoSigla_Jsonclick = "";
         edtContagemResultado_ContratadaOrigemSigla_Jsonclick = "";
         edtContagemResultado_ContratadaSigla_Jsonclick = "";
         edtContagemResultado_OsFsOsFm_Jsonclick = "";
         edtContagemResultado_DataDmn_Jsonclick = "";
         edtavDemanda_Jsonclick = "";
         edtavDemanda_Visible = 0;
         edtavContratadasigla_Jsonclick = "";
         edtavContratadasigla_Visible = 0;
         edtContagemResultado_OSVinculada_Jsonclick = "";
         edtContagemResultado_Codigo_Jsonclick = "";
         edtavDesvincular_Jsonclick = "";
         edtavDesvincular_Visible = -1;
         bttBtnvincular_Enabled = 1;
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavPffinal_Enabled = 1;
         edtavPflfm_Enabled = 1;
         edtavPfbfm_Enabled = 1;
         edtavPflfs_Enabled = 1;
         edtavPfbfs_Enabled = 1;
         edtContagemResultado_OsFsOsFm_Link = "";
         edtavDemanda_Enabled = 1;
         edtavContratadasigla_Enabled = 1;
         edtavIgualar_Visible = -1;
         subGrid_Class = "WorkWith";
         edtContagemResultado_OsFsOsFm_Linktarget = "";
         edtavIgualar_Tooltiptext = "Igular SS com o status de esta OS";
         edtavDesvincular_Tooltiptext = "";
         edtavIgualar_Enabled = 1;
         edtavDesvincular_Enabled = 1;
         bttBtnvincular_Tooltiptext = "Vincular com outra OS";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         Confirmpanel_Yesbuttonposition = "left";
         Confirmpanel_Nobuttoncaption = "N�o";
         Confirmpanel_Yesbuttoncaption = "Sim";
         Confirmpanel_Confirmationtext = "Confirma eliminar o v�nculo com essa demanda?";
         Confirmpanel_Title = "Confirma��o";
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'AV6ContagemResultado_OSVinculada',fld:'vCONTAGEMRESULTADO_OSVINCULADA',pic:'ZZZZZ9',nv:0},{av:'A803ContagemResultado_ContratadaSigla',fld:'CONTAGEMRESULTADO_CONTRATADASIGLA',pic:'@!',nv:''},{av:'A457ContagemResultado_Demanda',fld:'CONTAGEMRESULTADO_DEMANDA',pic:'@!',nv:''},{av:'AV15SS',fld:'vSS',pic:'',nv:false},{av:'AV16StatusSS',fld:'vSTATUSSS',pic:'',nv:''},{av:'A484ContagemResultado_StatusDmn',fld:'CONTAGEMRESULTADO_STATUSDMN',pic:'',hsh:true,nv:''},{av:'A490ContagemResultado_ContratadaCod',fld:'CONTAGEMRESULTADO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A682ContagemResultado_PFBFMUltima',fld:'CONTAGEMRESULTADO_PFBFMULTIMA',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A684ContagemResultado_PFBFSUltima',fld:'CONTAGEMRESULTADO_PFBFSULTIMA',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A683ContagemResultado_PFLFMUltima',fld:'CONTAGEMRESULTADO_PFLFMULTIMA',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A685ContagemResultado_PFLFSUltima',fld:'CONTAGEMRESULTADO_PFLFSULTIMA',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A574ContagemResultado_PFFinal',fld:'CONTAGEMRESULTADO_PFFINAL',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'sPrefix',nv:''},{av:'AV7ContagemResultado_Codigo',fld:'vCONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV11WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null}],oparms:[{ctrl:'BTNVINCULAR',prop:'Enabled'},{av:'AV15SS',fld:'vSS',pic:'',nv:false},{av:'AV16StatusSS',fld:'vSTATUSSS',pic:'',nv:''},{ctrl:'BTNVINCULAR',prop:'Tooltiptext'},{av:'edtavDesvincular_Tooltiptext',ctrl:'vDESVINCULAR',prop:'Tooltiptext'},{av:'edtavIgualar_Tooltiptext',ctrl:'vIGUALAR',prop:'Tooltiptext'},{av:'edtavDesvincular_Enabled',ctrl:'vDESVINCULAR',prop:'Enabled'},{av:'edtavIgualar_Enabled',ctrl:'vIGUALAR',prop:'Enabled'},{av:'edtContagemResultado_OsFsOsFm_Linktarget',ctrl:'CONTAGEMRESULTADO_OSFSOSFM',prop:'Linktarget'}]}");
         setEventMetadata("VDESVINCULAR.CLICK","{handler:'E17EF2',iparms:[{av:'A602ContagemResultado_OSVinculada',fld:'CONTAGEMRESULTADO_OSVINCULADA',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV7ContagemResultado_Codigo',fld:'vCONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV13Codigo',fld:'vCODIGO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("CONFIRMPANEL.CLOSE","{handler:'E11EF2',iparms:[{av:'Confirmpanel_Result',ctrl:'CONFIRMPANEL',prop:'Result'},{av:'AV13Codigo',fld:'vCODIGO',pic:'ZZZZZ9',nv:0},{av:'AV7ContagemResultado_Codigo',fld:'vCONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("'VINCULARDEMANDA'","{handler:'E12EF2',iparms:[{av:'AV7ContagemResultado_Codigo',fld:'vCONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV7ContagemResultado_Codigo',fld:'vCONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("VIGUALAR.CLICK","{handler:'E16EF2',iparms:[{av:'A484ContagemResultado_StatusDmn',fld:'CONTAGEMRESULTADO_STATUSDMN',pic:'',hsh:true,nv:''},{av:'AV7ContagemResultado_Codigo',fld:'vCONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV19NovoStatus',fld:'vNOVOSTATUS',pic:'',nv:''},{av:'AV13Codigo',fld:'vCODIGO',pic:'ZZZZZ9',nv:0},{av:'AV11WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV16StatusSS',fld:'vSTATUSSS',pic:'',nv:''},{av:'AV18Demanda',fld:'vDEMANDA',pic:'@!',hsh:true,nv:''},{av:'AV17ContratadaSigla',fld:'vCONTRATADASIGLA',pic:'@!',hsh:true,nv:''}],oparms:[{av:'AV19NovoStatus',fld:'vNOVOSTATUS',pic:'',nv:''},{av:'AV7ContagemResultado_Codigo',fld:'vCONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Confirmpanel_Result = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV11WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         A803ContagemResultado_ContratadaSigla = "";
         A457ContagemResultado_Demanda = "";
         AV16StatusSS = "";
         A484ContagemResultado_StatusDmn = "";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV19NovoStatus = "";
         A493ContagemResultado_DemandaFM = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV9Desvincular = "";
         AV30Desvincular_GXI = "";
         AV17ContratadaSigla = "";
         AV18Demanda = "";
         A471ContagemResultado_DataDmn = DateTime.MinValue;
         A501ContagemResultado_OsFsOsFm = "";
         A866ContagemResultado_ContratadaOrigemSigla = "";
         A801ContagemResultado_ServicoSigla = "";
         A509ContagemrResultado_SistemaSigla = "";
         AV22PFBFS = "";
         AV23PFLFS = "";
         AV24PFBFM = "";
         AV25PFLFM = "";
         AV27PFFINAL = "";
         AV14Igualar = "";
         AV31Igualar_GXI = "";
         GXCCtl = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         H00EF3_A805ContagemResultado_ContratadaOrigemCod = new int[1] ;
         H00EF3_n805ContagemResultado_ContratadaOrigemCod = new bool[] {false} ;
         H00EF3_A489ContagemResultado_SistemaCod = new int[1] ;
         H00EF3_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         H00EF3_A1553ContagemResultado_CntSrvCod = new int[1] ;
         H00EF3_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         H00EF3_A601ContagemResultado_Servico = new int[1] ;
         H00EF3_n601ContagemResultado_Servico = new bool[] {false} ;
         H00EF3_A490ContagemResultado_ContratadaCod = new int[1] ;
         H00EF3_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         H00EF3_A484ContagemResultado_StatusDmn = new String[] {""} ;
         H00EF3_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         H00EF3_A598ContagemResultado_Baseline = new bool[] {false} ;
         H00EF3_n598ContagemResultado_Baseline = new bool[] {false} ;
         H00EF3_A509ContagemrResultado_SistemaSigla = new String[] {""} ;
         H00EF3_n509ContagemrResultado_SistemaSigla = new bool[] {false} ;
         H00EF3_A801ContagemResultado_ServicoSigla = new String[] {""} ;
         H00EF3_n801ContagemResultado_ServicoSigla = new bool[] {false} ;
         H00EF3_A866ContagemResultado_ContratadaOrigemSigla = new String[] {""} ;
         H00EF3_n866ContagemResultado_ContratadaOrigemSigla = new bool[] {false} ;
         H00EF3_A803ContagemResultado_ContratadaSigla = new String[] {""} ;
         H00EF3_n803ContagemResultado_ContratadaSigla = new bool[] {false} ;
         H00EF3_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         H00EF3_A602ContagemResultado_OSVinculada = new int[1] ;
         H00EF3_n602ContagemResultado_OSVinculada = new bool[] {false} ;
         H00EF3_A682ContagemResultado_PFBFMUltima = new decimal[1] ;
         H00EF3_A684ContagemResultado_PFBFSUltima = new decimal[1] ;
         H00EF3_A683ContagemResultado_PFLFMUltima = new decimal[1] ;
         H00EF3_A685ContagemResultado_PFLFSUltima = new decimal[1] ;
         H00EF3_A493ContagemResultado_DemandaFM = new String[] {""} ;
         H00EF3_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         H00EF3_A457ContagemResultado_Demanda = new String[] {""} ;
         H00EF3_n457ContagemResultado_Demanda = new bool[] {false} ;
         H00EF3_A456ContagemResultado_Codigo = new int[1] ;
         AV10ContagemResultado = new SdtContagemResultado(context);
         GXt_char2 = "";
         GridRow = new GXWebRow();
         AV32GXV1 = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs");
         AV20message = new SdtMessages_Message(context);
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         TempTags = "";
         bttBtnvincular_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlAV7ContagemResultado_Codigo = "";
         sCtrlAV6ContagemResultado_OSVinculada = "";
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wc_demandasvinculadas__default(),
            new Object[][] {
                new Object[] {
               H00EF3_A805ContagemResultado_ContratadaOrigemCod, H00EF3_n805ContagemResultado_ContratadaOrigemCod, H00EF3_A489ContagemResultado_SistemaCod, H00EF3_n489ContagemResultado_SistemaCod, H00EF3_A1553ContagemResultado_CntSrvCod, H00EF3_n1553ContagemResultado_CntSrvCod, H00EF3_A601ContagemResultado_Servico, H00EF3_n601ContagemResultado_Servico, H00EF3_A490ContagemResultado_ContratadaCod, H00EF3_n490ContagemResultado_ContratadaCod,
               H00EF3_A484ContagemResultado_StatusDmn, H00EF3_n484ContagemResultado_StatusDmn, H00EF3_A598ContagemResultado_Baseline, H00EF3_n598ContagemResultado_Baseline, H00EF3_A509ContagemrResultado_SistemaSigla, H00EF3_n509ContagemrResultado_SistemaSigla, H00EF3_A801ContagemResultado_ServicoSigla, H00EF3_n801ContagemResultado_ServicoSigla, H00EF3_A866ContagemResultado_ContratadaOrigemSigla, H00EF3_n866ContagemResultado_ContratadaOrigemSigla,
               H00EF3_A803ContagemResultado_ContratadaSigla, H00EF3_n803ContagemResultado_ContratadaSigla, H00EF3_A471ContagemResultado_DataDmn, H00EF3_A602ContagemResultado_OSVinculada, H00EF3_n602ContagemResultado_OSVinculada, H00EF3_A682ContagemResultado_PFBFMUltima, H00EF3_A684ContagemResultado_PFBFSUltima, H00EF3_A683ContagemResultado_PFLFMUltima, H00EF3_A685ContagemResultado_PFLFSUltima, H00EF3_A493ContagemResultado_DemandaFM,
               H00EF3_n493ContagemResultado_DemandaFM, H00EF3_A457ContagemResultado_Demanda, H00EF3_n457ContagemResultado_Demanda, H00EF3_A456ContagemResultado_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavContratadasigla_Enabled = 0;
         edtavDemanda_Enabled = 0;
         edtavPfbfs_Enabled = 0;
         edtavPflfs_Enabled = 0;
         edtavPfbfm_Enabled = 0;
         edtavPflfm_Enabled = 0;
         edtavPffinal_Enabled = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short nRC_GXsfl_8 ;
      private short nGXsfl_8_idx=1 ;
      private short initialized ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_8_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short GRID_nEOF ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV7ContagemResultado_Codigo ;
      private int AV6ContagemResultado_OSVinculada ;
      private int wcpOAV7ContagemResultado_Codigo ;
      private int wcpOAV6ContagemResultado_OSVinculada ;
      private int A490ContagemResultado_ContratadaCod ;
      private int A456ContagemResultado_Codigo ;
      private int edtavContratadasigla_Enabled ;
      private int edtavDemanda_Enabled ;
      private int edtavPfbfs_Enabled ;
      private int edtavPflfs_Enabled ;
      private int edtavPfbfm_Enabled ;
      private int edtavPflfm_Enabled ;
      private int edtavPffinal_Enabled ;
      private int AV13Codigo ;
      private int A602ContagemResultado_OSVinculada ;
      private int subGrid_Islastpage ;
      private int A805ContagemResultado_ContratadaOrigemCod ;
      private int A489ContagemResultado_SistemaCod ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A601ContagemResultado_Servico ;
      private int bttBtnvincular_Enabled ;
      private int GXt_int3 ;
      private int edtavDesvincular_Enabled ;
      private int edtavIgualar_Enabled ;
      private int edtavIgualar_Visible ;
      private int AV33GXV2 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private int edtavDesvincular_Visible ;
      private int edtavContratadasigla_Visible ;
      private int edtavDemanda_Visible ;
      private int edtavPfbfs_Visible ;
      private int edtavPflfs_Visible ;
      private int edtavPfbfm_Visible ;
      private int edtavPflfm_Visible ;
      private int edtavPffinal_Visible ;
      private long GRID_nCurrentRecord ;
      private long GRID_nFirstRecordOnPage ;
      private decimal A682ContagemResultado_PFBFMUltima ;
      private decimal A684ContagemResultado_PFBFSUltima ;
      private decimal A683ContagemResultado_PFLFMUltima ;
      private decimal A685ContagemResultado_PFLFSUltima ;
      private decimal A574ContagemResultado_PFFinal ;
      private decimal GXt_decimal1 ;
      private String Confirmpanel_Result ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String sGXsfl_8_idx="0001" ;
      private String A803ContagemResultado_ContratadaSigla ;
      private String AV16StatusSS ;
      private String A484ContagemResultado_StatusDmn ;
      private String GXKey ;
      private String edtavContratadasigla_Internalname ;
      private String edtavDemanda_Internalname ;
      private String edtavPfbfs_Internalname ;
      private String edtavPflfs_Internalname ;
      private String edtavPfbfm_Internalname ;
      private String edtavPflfm_Internalname ;
      private String edtavPffinal_Internalname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String AV19NovoStatus ;
      private String Confirmpanel_Title ;
      private String Confirmpanel_Confirmationtext ;
      private String Confirmpanel_Yesbuttoncaption ;
      private String Confirmpanel_Nobuttoncaption ;
      private String Confirmpanel_Yesbuttonposition ;
      private String GX_FocusControl ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavDesvincular_Internalname ;
      private String edtContagemResultado_Codigo_Internalname ;
      private String edtContagemResultado_OSVinculada_Internalname ;
      private String AV17ContratadaSigla ;
      private String edtContagemResultado_DataDmn_Internalname ;
      private String edtContagemResultado_OsFsOsFm_Internalname ;
      private String edtContagemResultado_ContratadaSigla_Internalname ;
      private String A866ContagemResultado_ContratadaOrigemSigla ;
      private String edtContagemResultado_ContratadaOrigemSigla_Internalname ;
      private String A801ContagemResultado_ServicoSigla ;
      private String edtContagemResultado_ServicoSigla_Internalname ;
      private String A509ContagemrResultado_SistemaSigla ;
      private String edtContagemrResultado_SistemaSigla_Internalname ;
      private String cmbContagemResultado_Baseline_Internalname ;
      private String AV22PFBFS ;
      private String AV23PFLFS ;
      private String AV24PFBFM ;
      private String AV25PFLFM ;
      private String AV27PFFINAL ;
      private String cmbContagemResultado_StatusDmn_Internalname ;
      private String edtavIgualar_Internalname ;
      private String GXCCtl ;
      private String scmdbuf ;
      private String bttBtnvincular_Internalname ;
      private String bttBtnvincular_Tooltiptext ;
      private String GXt_char2 ;
      private String edtavDesvincular_Tooltiptext ;
      private String edtavIgualar_Tooltiptext ;
      private String edtContagemResultado_OsFsOsFm_Linktarget ;
      private String edtContagemResultado_OsFsOsFm_Link ;
      private String sStyleString ;
      private String tblTable3_Internalname ;
      private String tblTable2_Internalname ;
      private String tblTable1_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String subGrid_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String TempTags ;
      private String bttBtnvincular_Jsonclick ;
      private String sCtrlAV7ContagemResultado_Codigo ;
      private String sCtrlAV6ContagemResultado_OSVinculada ;
      private String sGXsfl_8_fel_idx="0001" ;
      private String edtavDesvincular_Jsonclick ;
      private String ROClassString ;
      private String edtContagemResultado_Codigo_Jsonclick ;
      private String edtContagemResultado_OSVinculada_Jsonclick ;
      private String edtavContratadasigla_Jsonclick ;
      private String edtavDemanda_Jsonclick ;
      private String edtContagemResultado_DataDmn_Jsonclick ;
      private String edtContagemResultado_OsFsOsFm_Jsonclick ;
      private String edtContagemResultado_ContratadaSigla_Jsonclick ;
      private String edtContagemResultado_ContratadaOrigemSigla_Jsonclick ;
      private String edtContagemResultado_ServicoSigla_Jsonclick ;
      private String edtContagemrResultado_SistemaSigla_Jsonclick ;
      private String cmbContagemResultado_Baseline_Jsonclick ;
      private String edtavPfbfs_Jsonclick ;
      private String edtavPflfs_Jsonclick ;
      private String edtavPfbfm_Jsonclick ;
      private String edtavPflfm_Jsonclick ;
      private String edtavPffinal_Jsonclick ;
      private String cmbContagemResultado_StatusDmn_Jsonclick ;
      private String edtavIgualar_Jsonclick ;
      private String Confirmpanel_Internalname ;
      private DateTime A471ContagemResultado_DataDmn ;
      private bool entryPointCalled ;
      private bool n803ContagemResultado_ContratadaSigla ;
      private bool n457ContagemResultado_Demanda ;
      private bool AV15SS ;
      private bool n484ContagemResultado_StatusDmn ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n602ContagemResultado_OSVinculada ;
      private bool n866ContagemResultado_ContratadaOrigemSigla ;
      private bool n801ContagemResultado_ServicoSigla ;
      private bool n509ContagemrResultado_SistemaSigla ;
      private bool A598ContagemResultado_Baseline ;
      private bool n598ContagemResultado_Baseline ;
      private bool n805ContagemResultado_ContratadaOrigemCod ;
      private bool n489ContagemResultado_SistemaCod ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n601ContagemResultado_Servico ;
      private bool n493ContagemResultado_DemandaFM ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV9Desvincular_IsBlob ;
      private bool AV14Igualar_IsBlob ;
      private String A457ContagemResultado_Demanda ;
      private String A493ContagemResultado_DemandaFM ;
      private String AV30Desvincular_GXI ;
      private String AV18Demanda ;
      private String A501ContagemResultado_OsFsOsFm ;
      private String AV31Igualar_GXI ;
      private String AV9Desvincular ;
      private String AV14Igualar ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbContagemResultado_Baseline ;
      private GXCombobox cmbContagemResultado_StatusDmn ;
      private IDataStoreProvider pr_default ;
      private int[] H00EF3_A805ContagemResultado_ContratadaOrigemCod ;
      private bool[] H00EF3_n805ContagemResultado_ContratadaOrigemCod ;
      private int[] H00EF3_A489ContagemResultado_SistemaCod ;
      private bool[] H00EF3_n489ContagemResultado_SistemaCod ;
      private int[] H00EF3_A1553ContagemResultado_CntSrvCod ;
      private bool[] H00EF3_n1553ContagemResultado_CntSrvCod ;
      private int[] H00EF3_A601ContagemResultado_Servico ;
      private bool[] H00EF3_n601ContagemResultado_Servico ;
      private int[] H00EF3_A490ContagemResultado_ContratadaCod ;
      private bool[] H00EF3_n490ContagemResultado_ContratadaCod ;
      private String[] H00EF3_A484ContagemResultado_StatusDmn ;
      private bool[] H00EF3_n484ContagemResultado_StatusDmn ;
      private bool[] H00EF3_A598ContagemResultado_Baseline ;
      private bool[] H00EF3_n598ContagemResultado_Baseline ;
      private String[] H00EF3_A509ContagemrResultado_SistemaSigla ;
      private bool[] H00EF3_n509ContagemrResultado_SistemaSigla ;
      private String[] H00EF3_A801ContagemResultado_ServicoSigla ;
      private bool[] H00EF3_n801ContagemResultado_ServicoSigla ;
      private String[] H00EF3_A866ContagemResultado_ContratadaOrigemSigla ;
      private bool[] H00EF3_n866ContagemResultado_ContratadaOrigemSigla ;
      private String[] H00EF3_A803ContagemResultado_ContratadaSigla ;
      private bool[] H00EF3_n803ContagemResultado_ContratadaSigla ;
      private DateTime[] H00EF3_A471ContagemResultado_DataDmn ;
      private int[] H00EF3_A602ContagemResultado_OSVinculada ;
      private bool[] H00EF3_n602ContagemResultado_OSVinculada ;
      private decimal[] H00EF3_A682ContagemResultado_PFBFMUltima ;
      private decimal[] H00EF3_A684ContagemResultado_PFBFSUltima ;
      private decimal[] H00EF3_A683ContagemResultado_PFLFMUltima ;
      private decimal[] H00EF3_A685ContagemResultado_PFLFSUltima ;
      private String[] H00EF3_A493ContagemResultado_DemandaFM ;
      private bool[] H00EF3_n493ContagemResultado_DemandaFM ;
      private String[] H00EF3_A457ContagemResultado_Demanda ;
      private bool[] H00EF3_n457ContagemResultado_Demanda ;
      private int[] H00EF3_A456ContagemResultado_Codigo ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      [ObjectCollection(ItemType=typeof( SdtMessages_Message ))]
      private IGxCollection AV32GXV1 ;
      private wwpbaseobjects.SdtWWPContext AV11WWPContext ;
      private SdtContagemResultado AV10ContagemResultado ;
      private SdtMessages_Message AV20message ;
   }

   public class wc_demandasvinculadas__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00EF3 ;
          prmH00EF3 = new Object[] {
          new Object[] {"@AV6ContagemResultado_OSVinculada",SqlDbType.Int,6,0} ,
          new Object[] {"@AV7ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          String cmdBufferH00EF3 ;
          cmdBufferH00EF3=" SELECT T1.[ContagemResultado_ContratadaOrigemCod] AS ContagemResultado_ContratadaOr, T1.[ContagemResultado_SistemaCod] AS ContagemResultado_SistemaCod, T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T4.[Servico_Codigo] AS ContagemResultado_Servico, T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_Baseline], T3.[Sistema_Sigla] AS ContagemrResultado_SistemaSigla, T5.[Servico_Sigla] AS ContagemResultado_ServicoSigla, T2.[Contratada_Sigla] AS ContagemResultado_ContratadaOr, T6.[Contratada_Sigla] AS ContagemResultado_ContratadaSigla, T1.[ContagemResultado_DataDmn], T1.[ContagemResultado_OSVinculada], COALESCE( T7.[ContagemResultado_PFBFMUltima], 0) AS ContagemResultado_PFBFMUltima, COALESCE( T7.[ContagemResultado_PFBFSUltima], 0) AS ContagemResultado_PFBFSUltima, COALESCE( T7.[ContagemResultado_PFLFMUltima], 0) AS ContagemResultado_PFLFMUltima, COALESCE( T7.[ContagemResultado_PFLFSUltima], 0) AS ContagemResultado_PFLFSUltima, T1.[ContagemResultado_DemandaFM], T1.[ContagemResultado_Demanda], T1.[ContagemResultado_Codigo] FROM (((((([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaOrigemCod]) LEFT JOIN [Sistema] T3 WITH (NOLOCK) ON T3.[Sistema_Codigo] = T1.[ContagemResultado_SistemaCod]) LEFT JOIN [ContratoServicos] T4 WITH (NOLOCK) ON T4.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [Servico] T5 WITH (NOLOCK) ON T5.[Servico_Codigo] = T4.[Servico_Codigo]) LEFT JOIN [Contratada] T6 WITH (NOLOCK) ON T6.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod]) LEFT JOIN (SELECT MIN([ContagemResultado_PFBFM]) AS ContagemResultado_PFBFMUltima, [ContagemResultado_Codigo], MIN([ContagemResultado_PFBFS]) "
          + " AS ContagemResultado_PFBFSUltima, MIN([ContagemResultado_PFLFM]) AS ContagemResultado_PFLFMUltima, MIN([ContagemResultado_PFLFS]) AS ContagemResultado_PFLFSUltima FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T7 ON T7.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) WHERE T1.[ContagemResultado_Codigo] = @AV6ContagemResultado_OSVinculada or T1.[ContagemResultado_OSVinculada] = @AV7ContagemResultado_Codigo ORDER BY T1.[ContagemResultado_Codigo]" ;
          def= new CursorDef[] {
              new CursorDef("H00EF3", cmdBufferH00EF3,false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00EF3,11,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((int[]) buf[8])[0] = rslt.getInt(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((String[]) buf[10])[0] = rslt.getString(6, 1) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((bool[]) buf[12])[0] = rslt.getBool(7) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(7);
                ((String[]) buf[14])[0] = rslt.getString(8, 25) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(8);
                ((String[]) buf[16])[0] = rslt.getString(9, 15) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(9);
                ((String[]) buf[18])[0] = rslt.getString(10, 15) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(10);
                ((String[]) buf[20])[0] = rslt.getString(11, 15) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(11);
                ((DateTime[]) buf[22])[0] = rslt.getGXDate(12) ;
                ((int[]) buf[23])[0] = rslt.getInt(13) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(13);
                ((decimal[]) buf[25])[0] = rslt.getDecimal(14) ;
                ((decimal[]) buf[26])[0] = rslt.getDecimal(15) ;
                ((decimal[]) buf[27])[0] = rslt.getDecimal(16) ;
                ((decimal[]) buf[28])[0] = rslt.getDecimal(17) ;
                ((String[]) buf[29])[0] = rslt.getVarchar(18) ;
                ((bool[]) buf[30])[0] = rslt.wasNull(18);
                ((String[]) buf[31])[0] = rslt.getVarchar(19) ;
                ((bool[]) buf[32])[0] = rslt.wasNull(19);
                ((int[]) buf[33])[0] = rslt.getInt(20) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
       }
    }

 }

}
