/*
               File: PRC_AlterarOrdem
        Description: Alterar Ordem
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:55:1.14
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_alterarordem : GXProcedure
   {
      public prc_alterarordem( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_alterarordem( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_OS_Codigo ,
                           short aP1_Ordem ,
                           short aP2_NewOrdem ,
                           String aP3_Tabela ,
                           String aP4_UpDown )
      {
         this.AV8OS_Codigo = aP0_OS_Codigo;
         this.AV10Ordem = aP1_Ordem;
         this.AV13NewOrdem = aP2_NewOrdem;
         this.AV11Tabela = aP3_Tabela;
         this.AV12UpDown = aP4_UpDown;
         initialize();
         executePrivate();
      }

      public void executeSubmit( int aP0_OS_Codigo ,
                                 short aP1_Ordem ,
                                 short aP2_NewOrdem ,
                                 String aP3_Tabela ,
                                 String aP4_UpDown )
      {
         prc_alterarordem objprc_alterarordem;
         objprc_alterarordem = new prc_alterarordem();
         objprc_alterarordem.AV8OS_Codigo = aP0_OS_Codigo;
         objprc_alterarordem.AV10Ordem = aP1_Ordem;
         objprc_alterarordem.AV13NewOrdem = aP2_NewOrdem;
         objprc_alterarordem.AV11Tabela = aP3_Tabela;
         objprc_alterarordem.AV12UpDown = aP4_UpDown;
         objprc_alterarordem.context.SetSubmitInitialConfig(context);
         objprc_alterarordem.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_alterarordem);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_alterarordem)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         if ( AV13NewOrdem == AV10Ordem )
         {
            this.cleanup();
            if (true) return;
         }
         AV9Incremento = 1;
         if ( StringUtil.StrCmp(AV12UpDown, "-") == 0 )
         {
            AV9Incremento = -1;
         }
         if ( StringUtil.StrCmp(AV11Tabela, "Req") == 0 )
         {
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV13NewOrdem ,
                                                 AV12UpDown ,
                                                 AV10Ordem ,
                                                 A1931Requisito_Ordem ,
                                                 AV8OS_Codigo ,
                                                 A2003ContagemResultadoRequisito_OSCod },
                                                 new int[] {
                                                 TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                                 }
            });
            /* Using cursor P00TP2 */
            pr_default.execute(0, new Object[] {AV8OS_Codigo, AV13NewOrdem, AV10Ordem, AV13NewOrdem, AV10Ordem});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A2004ContagemResultadoRequisito_ReqCod = P00TP2_A2004ContagemResultadoRequisito_ReqCod[0];
               A2003ContagemResultadoRequisito_OSCod = P00TP2_A2003ContagemResultadoRequisito_OSCod[0];
               A1931Requisito_Ordem = P00TP2_A1931Requisito_Ordem[0];
               n1931Requisito_Ordem = P00TP2_n1931Requisito_Ordem[0];
               A2005ContagemResultadoRequisito_Codigo = P00TP2_A2005ContagemResultadoRequisito_Codigo[0];
               A1931Requisito_Ordem = P00TP2_A1931Requisito_Ordem[0];
               n1931Requisito_Ordem = P00TP2_n1931Requisito_Ordem[0];
               A1931Requisito_Ordem = (short)(A1931Requisito_Ordem+AV9Incremento);
               n1931Requisito_Ordem = false;
               BatchSize = 100;
               pr_default.initializeBatch( 1, BatchSize, this, "Executebatchp00tp3");
               /* Using cursor P00TP3 */
               pr_default.addRecord(1, new Object[] {n1931Requisito_Ordem, A1931Requisito_Ordem, A2004ContagemResultadoRequisito_ReqCod});
               if ( pr_default.recordCount(1) == pr_default.getBatchSize(1) )
               {
                  Executebatchp00tp3( ) ;
               }
               dsDefault.SmartCacheProvider.SetUpdated("Requisito") ;
               pr_default.readNext(0);
            }
            if ( pr_default.getBatchSize(1) > 0 )
            {
               Executebatchp00tp3( ) ;
            }
            pr_default.close(0);
         }
         this.cleanup();
      }

      protected void Executebatchp00tp3( )
      {
         /* Using cursor P00TP3 */
         pr_default.executeBatch(1);
         pr_default.close(1);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00TP2_A2004ContagemResultadoRequisito_ReqCod = new int[1] ;
         P00TP2_A2003ContagemResultadoRequisito_OSCod = new int[1] ;
         P00TP2_A1931Requisito_Ordem = new short[1] ;
         P00TP2_n1931Requisito_Ordem = new bool[] {false} ;
         P00TP2_A2005ContagemResultadoRequisito_Codigo = new int[1] ;
         P00TP3_A1931Requisito_Ordem = new short[1] ;
         P00TP3_n1931Requisito_Ordem = new bool[] {false} ;
         P00TP3_A2004ContagemResultadoRequisito_ReqCod = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_alterarordem__default(),
            new Object[][] {
                new Object[] {
               P00TP2_A2004ContagemResultadoRequisito_ReqCod, P00TP2_A2003ContagemResultadoRequisito_OSCod, P00TP2_A1931Requisito_Ordem, P00TP2_n1931Requisito_Ordem, P00TP2_A2005ContagemResultadoRequisito_Codigo
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV10Ordem ;
      private short AV13NewOrdem ;
      private short AV9Incremento ;
      private short A1931Requisito_Ordem ;
      private int AV8OS_Codigo ;
      private int A2003ContagemResultadoRequisito_OSCod ;
      private int A2004ContagemResultadoRequisito_ReqCod ;
      private int A2005ContagemResultadoRequisito_Codigo ;
      private int BatchSize ;
      private String AV11Tabela ;
      private String AV12UpDown ;
      private String scmdbuf ;
      private bool n1931Requisito_Ordem ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00TP2_A2004ContagemResultadoRequisito_ReqCod ;
      private int[] P00TP2_A2003ContagemResultadoRequisito_OSCod ;
      private short[] P00TP2_A1931Requisito_Ordem ;
      private bool[] P00TP2_n1931Requisito_Ordem ;
      private int[] P00TP2_A2005ContagemResultadoRequisito_Codigo ;
      private short[] P00TP3_A1931Requisito_Ordem ;
      private bool[] P00TP3_n1931Requisito_Ordem ;
      private int[] P00TP3_A2004ContagemResultadoRequisito_ReqCod ;
   }

   public class prc_alterarordem__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00TP2( IGxContext context ,
                                             short AV13NewOrdem ,
                                             String AV12UpDown ,
                                             short AV10Ordem ,
                                             short A1931Requisito_Ordem ,
                                             int AV8OS_Codigo ,
                                             int A2003ContagemResultadoRequisito_OSCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [5] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContagemResultadoRequisito_ReqCod] AS ContagemResultadoRequisito_ReqCod, T1.[ContagemResultadoRequisito_OSCod], T2.[Requisito_Ordem], T1.[ContagemResultadoRequisito_Codigo] FROM ([ContagemResultadoRequisito] T1 WITH (NOLOCK) INNER JOIN [Requisito] T2 WITH (UPDLOCK) ON T2.[Requisito_Codigo] = T1.[ContagemResultadoRequisito_ReqCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[ContagemResultadoRequisito_OSCod] = @AV8OS_Codigo)";
         if ( ( AV13NewOrdem > 0 ) && ( StringUtil.StrCmp(AV12UpDown, "+") == 0 ) )
         {
            sWhereString = sWhereString + " and (T2.[Requisito_Ordem] >= @AV13NewOrdem)";
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ( AV10Ordem > 0 ) && ( StringUtil.StrCmp(AV12UpDown, "+") == 0 ) )
         {
            sWhereString = sWhereString + " and (T2.[Requisito_Ordem] < @AV10Ordem)";
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( ( AV13NewOrdem > 0 ) && ( StringUtil.StrCmp(AV12UpDown, "-") == 0 ) )
         {
            sWhereString = sWhereString + " and (T2.[Requisito_Ordem] <= @AV13NewOrdem)";
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( ( AV10Ordem > 0 ) && ( StringUtil.StrCmp(AV12UpDown, "-") == 0 ) )
         {
            sWhereString = sWhereString + " and (T2.[Requisito_Ordem] > @AV10Ordem)";
         }
         else
         {
            GXv_int1[4] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultadoRequisito_OSCod], T2.[Requisito_Ordem]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00TP2(context, (short)dynConstraints[0] , (String)dynConstraints[1] , (short)dynConstraints[2] , (short)dynConstraints[3] , (int)dynConstraints[4] , (int)dynConstraints[5] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new BatchUpdateCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00TP3 ;
          prmP00TP3 = new Object[] {
          new Object[] {"@Requisito_Ordem",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@ContagemResultadoRequisito_ReqCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00TP2 ;
          prmP00TP2 = new Object[] {
          new Object[] {"@AV8OS_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV13NewOrdem",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@AV10Ordem",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@AV13NewOrdem",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@AV10Ordem",SqlDbType.SmallInt,3,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00TP2", "scmdbuf",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00TP2,1,0,true,false )
             ,new CursorDef("P00TP3", "UPDATE [Requisito] SET [Requisito_Ordem]=@Requisito_Ordem  WHERE [Requisito_Codigo] = @ContagemResultadoRequisito_ReqCod", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00TP3)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[5]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[6]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[7]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[8]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[9]);
                }
                return;
             case 1 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(1, (short)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
       }
    }

 }

}
