/*
               File: type_SdtSDT_WS_ConsultarAutorizacaoUsuario
        Description: SDT_WS_ConsultarAutorizacaoUsuario
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/29/2020 0:30:8.19
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "AutorizacaoUsuario" )]
   [XmlType(TypeName =  "AutorizacaoUsuario" , Namespace = "AutorizacaoUsuario" )]
   [System.Xml.Serialization.XmlInclude( typeof( SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho ))]
   [Serializable]
   public class SdtSDT_WS_ConsultarAutorizacaoUsuario : GxUserType
   {
      public SdtSDT_WS_ConsultarAutorizacaoUsuario( )
      {
         /* Constructor for serialization */
         gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_Pessoa_nome = "";
      }

      public SdtSDT_WS_ConsultarAutorizacaoUsuario( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         xmls = new XmlSerializer(this.GetType(), sNameSpace);
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtSDT_WS_ConsultarAutorizacaoUsuario deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "AutorizacaoUsuario" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtSDT_WS_ConsultarAutorizacaoUsuario)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtSDT_WS_ConsultarAutorizacaoUsuario obj ;
         obj = this;
         obj.gxTpr_Pessoa_codigo = deserialized.gxTpr_Pessoa_codigo;
         obj.gxTpr_Pessoa_nome = deserialized.gxTpr_Pessoa_nome;
         obj.gxTpr_Areatrabalho = deserialized.gxTpr_Areatrabalho;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "Pessoa_Codigo") )
               {
                  gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_Pessoa_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Pessoa_Nome") )
               {
                  gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_Pessoa_nome = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "AreaTrabalho") )
               {
                  if ( gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_Areatrabalho == null )
                  {
                     gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_Areatrabalho = new GxObjectCollection( context, "SDT_WS_ConsultarAutorizacaoUsuario.AreaTrabalho", "AutorizacaoUsuario", "SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho", "GeneXus.Programs");
                  }
                  if ( oReader.IsSimple == 0 )
                  {
                     GXSoapError = gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_Areatrabalho.readxmlcollection(oReader, "AreaTrabalho", "AreaTrabalho");
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "AutorizacaoUsuario";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "AutorizacaoUsuario";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("Pessoa_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_Pessoa_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "AutorizacaoUsuario") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "AutorizacaoUsuario");
         }
         oWriter.WriteElement("Pessoa_Nome", StringUtil.RTrim( gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_Pessoa_nome));
         if ( StringUtil.StrCmp(sNameSpace, "AutorizacaoUsuario") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "AutorizacaoUsuario");
         }
         if ( gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_Areatrabalho != null )
         {
            String sNameSpace1 ;
            if ( StringUtil.StrCmp(sNameSpace, "AutorizacaoUsuario") == 0 )
            {
               sNameSpace1 = "[*:nosend]" + "AutorizacaoUsuario";
            }
            else
            {
               sNameSpace1 = "AutorizacaoUsuario";
            }
            gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_Areatrabalho.writexmlcollection(oWriter, "AreaTrabalho", sNameSpace1, "AreaTrabalho", sNameSpace1);
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("Pessoa_Codigo", gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_Pessoa_codigo, false);
         AddObjectProperty("Pessoa_Nome", gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_Pessoa_nome, false);
         if ( gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_Areatrabalho != null )
         {
            AddObjectProperty("AreaTrabalho", gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_Areatrabalho, false);
         }
         return  ;
      }

      [  SoapElement( ElementName = "Pessoa_Codigo" )]
      [  XmlElement( ElementName = "Pessoa_Codigo"   )]
      public int gxTpr_Pessoa_codigo
      {
         get {
            return gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_Pessoa_codigo ;
         }

         set {
            gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_Pessoa_codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Pessoa_Nome" )]
      [  XmlElement( ElementName = "Pessoa_Nome"   )]
      public String gxTpr_Pessoa_nome
      {
         get {
            return gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_Pessoa_nome ;
         }

         set {
            gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_Pessoa_nome = (String)(value);
         }

      }

      public class gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_Areatrabalho_SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_80compatibility:SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho {}
      [  SoapElement( ElementName = "AreaTrabalho" )]
      [  XmlArray( ElementName = "AreaTrabalho"  )]
      [  XmlArrayItemAttribute( Type= typeof( SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho ), ElementName= "AreaTrabalho"  , IsNullable=false)]
      [  XmlArrayItemAttribute( Type= typeof( gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_Areatrabalho_SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_80compatibility ), ElementName= "SDT_WS_ConsultarAutorizacaoUsuario.AreaTrabalho"  , IsNullable=false)]
      public GxObjectCollection gxTpr_Areatrabalho_GxObjectCollection
      {
         get {
            if ( gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_Areatrabalho == null )
            {
               gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_Areatrabalho = new GxObjectCollection( context, "SDT_WS_ConsultarAutorizacaoUsuario.AreaTrabalho", "AutorizacaoUsuario", "SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho", "GeneXus.Programs");
            }
            return (GxObjectCollection)gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_Areatrabalho ;
         }

         set {
            if ( gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_Areatrabalho == null )
            {
               gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_Areatrabalho = new GxObjectCollection( context, "SDT_WS_ConsultarAutorizacaoUsuario.AreaTrabalho", "AutorizacaoUsuario", "SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho", "GeneXus.Programs");
            }
            gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_Areatrabalho = (GxObjectCollection) value;
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public IGxCollection gxTpr_Areatrabalho
      {
         get {
            if ( gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_Areatrabalho == null )
            {
               gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_Areatrabalho = new GxObjectCollection( context, "SDT_WS_ConsultarAutorizacaoUsuario.AreaTrabalho", "AutorizacaoUsuario", "SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho", "GeneXus.Programs");
            }
            return gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_Areatrabalho ;
         }

         set {
            gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_Areatrabalho = value;
         }

      }

      public void gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_Areatrabalho_SetNull( )
      {
         gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_Areatrabalho = null;
         return  ;
      }

      public bool gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_Areatrabalho_IsNull( )
      {
         if ( gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_Areatrabalho == null )
         {
            return true ;
         }
         return false ;
      }

      public void initialize( )
      {
         gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_Pessoa_nome = "";
         sTagName = "";
         return  ;
      }

      protected short readOk ;
      protected short nOutParmCount ;
      protected int gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_Pessoa_codigo ;
      protected String gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_Pessoa_nome ;
      protected String sTagName ;
      [ObjectCollection(ItemType=typeof( SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho ))]
      protected IGxCollection gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_Areatrabalho=null ;
   }

   [DataContract(Name = @"SDT_WS_ConsultarAutorizacaoUsuario", Namespace = "AutorizacaoUsuario")]
   public class SdtSDT_WS_ConsultarAutorizacaoUsuario_RESTInterface : GxGenericCollectionItem<SdtSDT_WS_ConsultarAutorizacaoUsuario>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtSDT_WS_ConsultarAutorizacaoUsuario_RESTInterface( ) : base()
      {
      }

      public SdtSDT_WS_ConsultarAutorizacaoUsuario_RESTInterface( SdtSDT_WS_ConsultarAutorizacaoUsuario psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "Pessoa_Codigo" , Order = 0 )]
      public Nullable<int> gxTpr_Pessoa_codigo
      {
         get {
            return sdt.gxTpr_Pessoa_codigo ;
         }

         set {
            sdt.gxTpr_Pessoa_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Pessoa_Nome" , Order = 1 )]
      public String gxTpr_Pessoa_nome
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Pessoa_nome) ;
         }

         set {
            sdt.gxTpr_Pessoa_nome = (String)(value);
         }

      }

      [DataMember( Name = "AreaTrabalho" , Order = 2 )]
      public GxGenericCollection<SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_RESTInterface> gxTpr_Areatrabalho
      {
         get {
            return new GxGenericCollection<SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_RESTInterface>(sdt.gxTpr_Areatrabalho) ;
         }

         set {
            value.LoadCollection(sdt.gxTpr_Areatrabalho);
         }

      }

      public SdtSDT_WS_ConsultarAutorizacaoUsuario sdt
      {
         get {
            return (SdtSDT_WS_ConsultarAutorizacaoUsuario)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtSDT_WS_ConsultarAutorizacaoUsuario() ;
         }
      }

   }

}
