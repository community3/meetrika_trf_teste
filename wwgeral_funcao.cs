/*
               File: WWGeral_Funcao
        Description:  Funcao
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/29/2020 9:37:36.30
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wwgeral_funcao : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wwgeral_funcao( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wwgeral_funcao( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         chkFuncao_Ativo = new GXCheckbox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_76 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_76_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_76_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               AV15DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
               AV16Funcao_Nome1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16Funcao_Nome1", AV16Funcao_Nome1);
               AV29Funcao_UONom1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29Funcao_UONom1", AV29Funcao_UONom1);
               AV18DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersSelector2", AV18DynamicFiltersSelector2);
               AV19Funcao_Nome2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Funcao_Nome2", AV19Funcao_Nome2);
               AV31Funcao_UONom2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31Funcao_UONom2", AV31Funcao_UONom2);
               AV21DynamicFiltersSelector3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector3", AV21DynamicFiltersSelector3);
               AV22Funcao_Nome3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Funcao_Nome3", AV22Funcao_Nome3);
               AV33Funcao_UONom3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33Funcao_UONom3", AV33Funcao_UONom3);
               AV17DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersEnabled2", AV17DynamicFiltersEnabled2);
               AV20DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled3", AV20DynamicFiltersEnabled3);
               AV37TFFuncao_Nome = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37TFFuncao_Nome", AV37TFFuncao_Nome);
               AV38TFFuncao_Nome_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFFuncao_Nome_Sel", AV38TFFuncao_Nome_Sel);
               AV41TFGrupoFuncao_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41TFGrupoFuncao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV41TFGrupoFuncao_Codigo), 6, 0)));
               AV42TFGrupoFuncao_Codigo_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42TFGrupoFuncao_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV42TFGrupoFuncao_Codigo_To), 6, 0)));
               AV45TFFuncao_UONom = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFFuncao_UONom", AV45TFFuncao_UONom);
               AV46TFFuncao_UONom_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFFuncao_UONom_Sel", AV46TFFuncao_UONom_Sel);
               AV49TFFuncao_Ativo_Sel = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFFuncao_Ativo_Sel", StringUtil.Str( (decimal)(AV49TFFuncao_Ativo_Sel), 1, 0));
               AV39ddo_Funcao_NomeTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39ddo_Funcao_NomeTitleControlIdToReplace", AV39ddo_Funcao_NomeTitleControlIdToReplace);
               AV43ddo_GrupoFuncao_CodigoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43ddo_GrupoFuncao_CodigoTitleControlIdToReplace", AV43ddo_GrupoFuncao_CodigoTitleControlIdToReplace);
               AV47ddo_Funcao_UONomTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47ddo_Funcao_UONomTitleControlIdToReplace", AV47ddo_Funcao_UONomTitleControlIdToReplace);
               AV50ddo_Funcao_AtivoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50ddo_Funcao_AtivoTitleControlIdToReplace", AV50ddo_Funcao_AtivoTitleControlIdToReplace);
               ajax_req_read_hidden_sdt(GetNextPar( ), AV6WWPContext);
               AV77Pgmname = GetNextPar( );
               ajax_req_read_hidden_sdt(GetNextPar( ), AV10GridState);
               AV24DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersIgnoreFirst", AV24DynamicFiltersIgnoreFirst);
               AV23DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersRemoving", AV23DynamicFiltersRemoving);
               A621Funcao_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               A635Funcao_UOCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               n635Funcao_UOCod = false;
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16Funcao_Nome1, AV29Funcao_UONom1, AV18DynamicFiltersSelector2, AV19Funcao_Nome2, AV31Funcao_UONom2, AV21DynamicFiltersSelector3, AV22Funcao_Nome3, AV33Funcao_UONom3, AV17DynamicFiltersEnabled2, AV20DynamicFiltersEnabled3, AV37TFFuncao_Nome, AV38TFFuncao_Nome_Sel, AV41TFGrupoFuncao_Codigo, AV42TFGrupoFuncao_Codigo_To, AV45TFFuncao_UONom, AV46TFFuncao_UONom_Sel, AV49TFFuncao_Ativo_Sel, AV39ddo_Funcao_NomeTitleControlIdToReplace, AV43ddo_GrupoFuncao_CodigoTitleControlIdToReplace, AV47ddo_Funcao_UONomTitleControlIdToReplace, AV50ddo_Funcao_AtivoTitleControlIdToReplace, AV6WWPContext, AV77Pgmname, AV10GridState, AV24DynamicFiltersIgnoreFirst, AV23DynamicFiltersRemoving, A621Funcao_Codigo, A635Funcao_UOCod) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("workwithplusbootstrapmasterpage", "GeneXus.Programs.workwithplusbootstrapmasterpage", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PACI2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTCI2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20205299373660");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wwgeral_funcao.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV15DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vFUNCAO_NOME1", AV16Funcao_Nome1);
         GxWebStd.gx_hidden_field( context, "GXH_vFUNCAO_UONOM1", StringUtil.RTrim( AV29Funcao_UONom1));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV18DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vFUNCAO_NOME2", AV19Funcao_Nome2);
         GxWebStd.gx_hidden_field( context, "GXH_vFUNCAO_UONOM2", StringUtil.RTrim( AV31Funcao_UONom2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR3", AV21DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, "GXH_vFUNCAO_NOME3", AV22Funcao_Nome3);
         GxWebStd.gx_hidden_field( context, "GXH_vFUNCAO_UONOM3", StringUtil.RTrim( AV33Funcao_UONom3));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV17DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV20DynamicFiltersEnabled3));
         GxWebStd.gx_hidden_field( context, "GXH_vTFFUNCAO_NOME", AV37TFFuncao_Nome);
         GxWebStd.gx_hidden_field( context, "GXH_vTFFUNCAO_NOME_SEL", AV38TFFuncao_Nome_Sel);
         GxWebStd.gx_hidden_field( context, "GXH_vTFGRUPOFUNCAO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV41TFGrupoFuncao_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFGRUPOFUNCAO_CODIGO_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV42TFGrupoFuncao_Codigo_To), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFFUNCAO_UONOM", StringUtil.RTrim( AV45TFFuncao_UONom));
         GxWebStd.gx_hidden_field( context, "GXH_vTFFUNCAO_UONOM_SEL", StringUtil.RTrim( AV46TFFuncao_UONom_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFFUNCAO_ATIVO_SEL", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV49TFFuncao_Ativo_Sel), 1, 0, ",", "")));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_76", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_76), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV53GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV54GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV51DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV51DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vFUNCAO_NOMETITLEFILTERDATA", AV36Funcao_NomeTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vFUNCAO_NOMETITLEFILTERDATA", AV36Funcao_NomeTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRUPOFUNCAO_CODIGOTITLEFILTERDATA", AV40GrupoFuncao_CodigoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRUPOFUNCAO_CODIGOTITLEFILTERDATA", AV40GrupoFuncao_CodigoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vFUNCAO_UONOMTITLEFILTERDATA", AV44Funcao_UONomTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vFUNCAO_UONOMTITLEFILTERDATA", AV44Funcao_UONomTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vFUNCAO_ATIVOTITLEFILTERDATA", AV48Funcao_AtivoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vFUNCAO_ATIVOTITLEFILTERDATA", AV48Funcao_AtivoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vWWPCONTEXT", AV6WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vWWPCONTEXT", AV6WWPContext);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV77Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV24DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV23DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAO_NOME_Caption", StringUtil.RTrim( Ddo_funcao_nome_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAO_NOME_Tooltip", StringUtil.RTrim( Ddo_funcao_nome_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAO_NOME_Cls", StringUtil.RTrim( Ddo_funcao_nome_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAO_NOME_Filteredtext_set", StringUtil.RTrim( Ddo_funcao_nome_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAO_NOME_Selectedvalue_set", StringUtil.RTrim( Ddo_funcao_nome_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAO_NOME_Dropdownoptionstype", StringUtil.RTrim( Ddo_funcao_nome_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAO_NOME_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_funcao_nome_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAO_NOME_Includesortasc", StringUtil.BoolToStr( Ddo_funcao_nome_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAO_NOME_Includesortdsc", StringUtil.BoolToStr( Ddo_funcao_nome_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAO_NOME_Sortedstatus", StringUtil.RTrim( Ddo_funcao_nome_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAO_NOME_Includefilter", StringUtil.BoolToStr( Ddo_funcao_nome_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAO_NOME_Filtertype", StringUtil.RTrim( Ddo_funcao_nome_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAO_NOME_Filterisrange", StringUtil.BoolToStr( Ddo_funcao_nome_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAO_NOME_Includedatalist", StringUtil.BoolToStr( Ddo_funcao_nome_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAO_NOME_Datalisttype", StringUtil.RTrim( Ddo_funcao_nome_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAO_NOME_Datalistproc", StringUtil.RTrim( Ddo_funcao_nome_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAO_NOME_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_funcao_nome_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAO_NOME_Sortasc", StringUtil.RTrim( Ddo_funcao_nome_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAO_NOME_Sortdsc", StringUtil.RTrim( Ddo_funcao_nome_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAO_NOME_Loadingdata", StringUtil.RTrim( Ddo_funcao_nome_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAO_NOME_Cleanfilter", StringUtil.RTrim( Ddo_funcao_nome_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAO_NOME_Noresultsfound", StringUtil.RTrim( Ddo_funcao_nome_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAO_NOME_Searchbuttontext", StringUtil.RTrim( Ddo_funcao_nome_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_GRUPOFUNCAO_CODIGO_Caption", StringUtil.RTrim( Ddo_grupofuncao_codigo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_GRUPOFUNCAO_CODIGO_Tooltip", StringUtil.RTrim( Ddo_grupofuncao_codigo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_GRUPOFUNCAO_CODIGO_Cls", StringUtil.RTrim( Ddo_grupofuncao_codigo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_GRUPOFUNCAO_CODIGO_Filteredtext_set", StringUtil.RTrim( Ddo_grupofuncao_codigo_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_GRUPOFUNCAO_CODIGO_Filteredtextto_set", StringUtil.RTrim( Ddo_grupofuncao_codigo_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_GRUPOFUNCAO_CODIGO_Dropdownoptionstype", StringUtil.RTrim( Ddo_grupofuncao_codigo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_GRUPOFUNCAO_CODIGO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_grupofuncao_codigo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_GRUPOFUNCAO_CODIGO_Includesortasc", StringUtil.BoolToStr( Ddo_grupofuncao_codigo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_GRUPOFUNCAO_CODIGO_Includesortdsc", StringUtil.BoolToStr( Ddo_grupofuncao_codigo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_GRUPOFUNCAO_CODIGO_Sortedstatus", StringUtil.RTrim( Ddo_grupofuncao_codigo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_GRUPOFUNCAO_CODIGO_Includefilter", StringUtil.BoolToStr( Ddo_grupofuncao_codigo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_GRUPOFUNCAO_CODIGO_Filtertype", StringUtil.RTrim( Ddo_grupofuncao_codigo_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_GRUPOFUNCAO_CODIGO_Filterisrange", StringUtil.BoolToStr( Ddo_grupofuncao_codigo_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_GRUPOFUNCAO_CODIGO_Includedatalist", StringUtil.BoolToStr( Ddo_grupofuncao_codigo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_GRUPOFUNCAO_CODIGO_Sortasc", StringUtil.RTrim( Ddo_grupofuncao_codigo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_GRUPOFUNCAO_CODIGO_Sortdsc", StringUtil.RTrim( Ddo_grupofuncao_codigo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_GRUPOFUNCAO_CODIGO_Cleanfilter", StringUtil.RTrim( Ddo_grupofuncao_codigo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_GRUPOFUNCAO_CODIGO_Rangefilterfrom", StringUtil.RTrim( Ddo_grupofuncao_codigo_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_GRUPOFUNCAO_CODIGO_Rangefilterto", StringUtil.RTrim( Ddo_grupofuncao_codigo_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_GRUPOFUNCAO_CODIGO_Searchbuttontext", StringUtil.RTrim( Ddo_grupofuncao_codigo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAO_UONOM_Caption", StringUtil.RTrim( Ddo_funcao_uonom_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAO_UONOM_Tooltip", StringUtil.RTrim( Ddo_funcao_uonom_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAO_UONOM_Cls", StringUtil.RTrim( Ddo_funcao_uonom_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAO_UONOM_Filteredtext_set", StringUtil.RTrim( Ddo_funcao_uonom_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAO_UONOM_Selectedvalue_set", StringUtil.RTrim( Ddo_funcao_uonom_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAO_UONOM_Dropdownoptionstype", StringUtil.RTrim( Ddo_funcao_uonom_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAO_UONOM_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_funcao_uonom_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAO_UONOM_Includesortasc", StringUtil.BoolToStr( Ddo_funcao_uonom_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAO_UONOM_Includesortdsc", StringUtil.BoolToStr( Ddo_funcao_uonom_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAO_UONOM_Sortedstatus", StringUtil.RTrim( Ddo_funcao_uonom_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAO_UONOM_Includefilter", StringUtil.BoolToStr( Ddo_funcao_uonom_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAO_UONOM_Filtertype", StringUtil.RTrim( Ddo_funcao_uonom_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAO_UONOM_Filterisrange", StringUtil.BoolToStr( Ddo_funcao_uonom_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAO_UONOM_Includedatalist", StringUtil.BoolToStr( Ddo_funcao_uonom_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAO_UONOM_Datalisttype", StringUtil.RTrim( Ddo_funcao_uonom_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAO_UONOM_Datalistproc", StringUtil.RTrim( Ddo_funcao_uonom_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAO_UONOM_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_funcao_uonom_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAO_UONOM_Sortasc", StringUtil.RTrim( Ddo_funcao_uonom_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAO_UONOM_Sortdsc", StringUtil.RTrim( Ddo_funcao_uonom_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAO_UONOM_Loadingdata", StringUtil.RTrim( Ddo_funcao_uonom_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAO_UONOM_Cleanfilter", StringUtil.RTrim( Ddo_funcao_uonom_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAO_UONOM_Noresultsfound", StringUtil.RTrim( Ddo_funcao_uonom_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAO_UONOM_Searchbuttontext", StringUtil.RTrim( Ddo_funcao_uonom_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAO_ATIVO_Caption", StringUtil.RTrim( Ddo_funcao_ativo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAO_ATIVO_Tooltip", StringUtil.RTrim( Ddo_funcao_ativo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAO_ATIVO_Cls", StringUtil.RTrim( Ddo_funcao_ativo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAO_ATIVO_Selectedvalue_set", StringUtil.RTrim( Ddo_funcao_ativo_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAO_ATIVO_Dropdownoptionstype", StringUtil.RTrim( Ddo_funcao_ativo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAO_ATIVO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_funcao_ativo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAO_ATIVO_Includesortasc", StringUtil.BoolToStr( Ddo_funcao_ativo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAO_ATIVO_Includesortdsc", StringUtil.BoolToStr( Ddo_funcao_ativo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAO_ATIVO_Sortedstatus", StringUtil.RTrim( Ddo_funcao_ativo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAO_ATIVO_Includefilter", StringUtil.BoolToStr( Ddo_funcao_ativo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAO_ATIVO_Includedatalist", StringUtil.BoolToStr( Ddo_funcao_ativo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAO_ATIVO_Datalisttype", StringUtil.RTrim( Ddo_funcao_ativo_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAO_ATIVO_Datalistfixedvalues", StringUtil.RTrim( Ddo_funcao_ativo_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAO_ATIVO_Sortasc", StringUtil.RTrim( Ddo_funcao_ativo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAO_ATIVO_Sortdsc", StringUtil.RTrim( Ddo_funcao_ativo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAO_ATIVO_Cleanfilter", StringUtil.RTrim( Ddo_funcao_ativo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAO_ATIVO_Searchbuttontext", StringUtil.RTrim( Ddo_funcao_ativo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAO_NOME_Activeeventkey", StringUtil.RTrim( Ddo_funcao_nome_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAO_NOME_Filteredtext_get", StringUtil.RTrim( Ddo_funcao_nome_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAO_NOME_Selectedvalue_get", StringUtil.RTrim( Ddo_funcao_nome_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_GRUPOFUNCAO_CODIGO_Activeeventkey", StringUtil.RTrim( Ddo_grupofuncao_codigo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_GRUPOFUNCAO_CODIGO_Filteredtext_get", StringUtil.RTrim( Ddo_grupofuncao_codigo_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_GRUPOFUNCAO_CODIGO_Filteredtextto_get", StringUtil.RTrim( Ddo_grupofuncao_codigo_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAO_UONOM_Activeeventkey", StringUtil.RTrim( Ddo_funcao_uonom_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAO_UONOM_Filteredtext_get", StringUtil.RTrim( Ddo_funcao_uonom_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAO_UONOM_Selectedvalue_get", StringUtil.RTrim( Ddo_funcao_uonom_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAO_ATIVO_Activeeventkey", StringUtil.RTrim( Ddo_funcao_ativo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAO_ATIVO_Selectedvalue_get", StringUtil.RTrim( Ddo_funcao_ativo_Selectedvalue_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WECI2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTCI2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wwgeral_funcao.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WWGeral_Funcao" ;
      }

      public override String GetPgmdesc( )
      {
         return " Funcao" ;
      }

      protected void WBCI0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_CI2( true) ;
         }
         else
         {
            wb_table1_2_CI2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_CI2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 89,'',false,'" + sGXsfl_76_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV17DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(89, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,89);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 90,'',false,'" + sGXsfl_76_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV20DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(90, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,90);\"");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 91,'',false,'" + sGXsfl_76_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTffuncao_nome_Internalname, AV37TFFuncao_Nome, StringUtil.RTrim( context.localUtil.Format( AV37TFFuncao_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,91);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTffuncao_nome_Jsonclick, 0, "Attribute", "", "", "", edtavTffuncao_nome_Visible, 1, 0, "text", "", 380, "px", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWGeral_Funcao.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 92,'',false,'" + sGXsfl_76_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTffuncao_nome_sel_Internalname, AV38TFFuncao_Nome_Sel, StringUtil.RTrim( context.localUtil.Format( AV38TFFuncao_Nome_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,92);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTffuncao_nome_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTffuncao_nome_sel_Visible, 1, 0, "text", "", 380, "px", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWGeral_Funcao.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 93,'',false,'" + sGXsfl_76_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfgrupofuncao_codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV41TFGrupoFuncao_Codigo), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV41TFGrupoFuncao_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,93);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfgrupofuncao_codigo_Jsonclick, 0, "Attribute", "", "", "", edtavTfgrupofuncao_codigo_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWGeral_Funcao.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 94,'',false,'" + sGXsfl_76_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfgrupofuncao_codigo_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV42TFGrupoFuncao_Codigo_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV42TFGrupoFuncao_Codigo_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,94);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfgrupofuncao_codigo_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfgrupofuncao_codigo_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWGeral_Funcao.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 95,'',false,'" + sGXsfl_76_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTffuncao_uonom_Internalname, StringUtil.RTrim( AV45TFFuncao_UONom), StringUtil.RTrim( context.localUtil.Format( AV45TFFuncao_UONom, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,95);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTffuncao_uonom_Jsonclick, 0, "Attribute", "", "", "", edtavTffuncao_uonom_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWGeral_Funcao.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 96,'',false,'" + sGXsfl_76_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTffuncao_uonom_sel_Internalname, StringUtil.RTrim( AV46TFFuncao_UONom_Sel), StringUtil.RTrim( context.localUtil.Format( AV46TFFuncao_UONom_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,96);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTffuncao_uonom_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTffuncao_uonom_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWGeral_Funcao.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 97,'',false,'" + sGXsfl_76_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTffuncao_ativo_sel_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV49TFFuncao_Ativo_Sel), 1, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV49TFFuncao_Ativo_Sel), "9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,97);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTffuncao_ativo_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTffuncao_ativo_sel_Visible, 1, 0, "text", "", 1, "chr", 1, "row", 1, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWGeral_Funcao.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_FUNCAO_NOMEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 99,'',false,'" + sGXsfl_76_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_funcao_nometitlecontrolidtoreplace_Internalname, AV39ddo_Funcao_NomeTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,99);\"", 0, edtavDdo_funcao_nometitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWGeral_Funcao.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_GRUPOFUNCAO_CODIGOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 101,'',false,'" + sGXsfl_76_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_grupofuncao_codigotitlecontrolidtoreplace_Internalname, AV43ddo_GrupoFuncao_CodigoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,101);\"", 0, edtavDdo_grupofuncao_codigotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWGeral_Funcao.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_FUNCAO_UONOMContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 103,'',false,'" + sGXsfl_76_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_funcao_uonomtitlecontrolidtoreplace_Internalname, AV47ddo_Funcao_UONomTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,103);\"", 0, edtavDdo_funcao_uonomtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWGeral_Funcao.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_FUNCAO_ATIVOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 105,'',false,'" + sGXsfl_76_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_funcao_ativotitlecontrolidtoreplace_Internalname, AV50ddo_Funcao_AtivoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,105);\"", 0, edtavDdo_funcao_ativotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWGeral_Funcao.htm");
         }
         wbLoad = true;
      }

      protected void STARTCI2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", " Funcao", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPCI0( ) ;
      }

      protected void WSCI2( )
      {
         STARTCI2( ) ;
         EVTCI2( ) ;
      }

      protected void EVTCI2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11CI2 */
                              E11CI2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_FUNCAO_NOME.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12CI2 */
                              E12CI2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_GRUPOFUNCAO_CODIGO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E13CI2 */
                              E13CI2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_FUNCAO_UONOM.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E14CI2 */
                              E14CI2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_FUNCAO_ATIVO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E15CI2 */
                              E15CI2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E16CI2 */
                              E16CI2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E17CI2 */
                              E17CI2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E18CI2 */
                              E18CI2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E19CI2 */
                              E19CI2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E20CI2 */
                              E20CI2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOINSERT'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E21CI2 */
                              E21CI2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E22CI2 */
                              E22CI2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E23CI2 */
                              E23CI2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E24CI2 */
                              E24CI2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E25CI2 */
                              E25CI2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR3.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E26CI2 */
                              E26CI2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              nGXsfl_76_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_76_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_76_idx), 4, 0)), 4, "0");
                              SubsflControlProps_762( ) ;
                              AV25Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV25Update)) ? AV75Update_GXI : context.convertURL( context.PathToRelativeUrl( AV25Update))));
                              AV26Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV26Delete)) ? AV76Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV26Delete))));
                              A621Funcao_Codigo = (int)(context.localUtil.CToN( cgiGet( edtFuncao_Codigo_Internalname), ",", "."));
                              A622Funcao_Nome = StringUtil.Upper( cgiGet( edtFuncao_Nome_Internalname));
                              A619GrupoFuncao_Codigo = (int)(context.localUtil.CToN( cgiGet( edtGrupoFuncao_Codigo_Internalname), ",", "."));
                              A635Funcao_UOCod = (int)(context.localUtil.CToN( cgiGet( edtFuncao_UOCod_Internalname), ",", "."));
                              n635Funcao_UOCod = false;
                              A636Funcao_UONom = StringUtil.Upper( cgiGet( edtFuncao_UONom_Internalname));
                              n636Funcao_UONom = false;
                              A630Funcao_Ativo = StringUtil.StrToBool( cgiGet( chkFuncao_Ativo_Internalname));
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E27CI2 */
                                    E27CI2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E28CI2 */
                                    E28CI2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E29CI2 */
                                    E29CI2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       /* Set Refresh If Orderedby Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Ordereddsc Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Funcao_nome1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vFUNCAO_NOME1"), AV16Funcao_Nome1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Funcao_uonom1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vFUNCAO_UONOM1"), AV29Funcao_UONom1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV18DynamicFiltersSelector2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Funcao_nome2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vFUNCAO_NOME2"), AV19Funcao_Nome2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Funcao_uonom2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vFUNCAO_UONOM2"), AV31Funcao_UONom2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV21DynamicFiltersSelector3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Funcao_nome3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vFUNCAO_NOME3"), AV22Funcao_Nome3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Funcao_uonom3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vFUNCAO_UONOM3"), AV33Funcao_UONom3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV17DynamicFiltersEnabled2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV20DynamicFiltersEnabled3 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tffuncao_nome Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFFUNCAO_NOME"), AV37TFFuncao_Nome) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tffuncao_nome_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFFUNCAO_NOME_SEL"), AV38TFFuncao_Nome_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfgrupofuncao_codigo Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFGRUPOFUNCAO_CODIGO"), ",", ".") != Convert.ToDecimal( AV41TFGrupoFuncao_Codigo )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfgrupofuncao_codigo_to Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFGRUPOFUNCAO_CODIGO_TO"), ",", ".") != Convert.ToDecimal( AV42TFGrupoFuncao_Codigo_To )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tffuncao_uonom Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFFUNCAO_UONOM"), AV45TFFuncao_UONom) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tffuncao_uonom_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFFUNCAO_UONOM_SEL"), AV46TFFuncao_UONom_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tffuncao_ativo_sel Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFFUNCAO_ATIVO_SEL"), ",", ".") != Convert.ToDecimal( AV49TFFuncao_Ativo_Sel )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WECI2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PACI2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("FUNCAO_NOME", "Nome", 0);
            cmbavDynamicfiltersselector1.addItem("FUNCAO_UONOM", "Unidade Organizacional", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("FUNCAO_NOME", "Nome", 0);
            cmbavDynamicfiltersselector2.addItem("FUNCAO_UONOM", "Unidade Organizacional", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV18DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV18DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersSelector2", AV18DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("FUNCAO_NOME", "Nome", 0);
            cmbavDynamicfiltersselector3.addItem("FUNCAO_UONOM", "Unidade Organizacional", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV21DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV21DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector3", AV21DynamicFiltersSelector3);
            }
            GXCCtl = "FUNCAO_ATIVO_" + sGXsfl_76_idx;
            chkFuncao_Ativo.Name = GXCCtl;
            chkFuncao_Ativo.WebTags = "";
            chkFuncao_Ativo.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkFuncao_Ativo_Internalname, "TitleCaption", chkFuncao_Ativo.Caption);
            chkFuncao_Ativo.CheckedValue = "false";
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_762( ) ;
         while ( nGXsfl_76_idx <= nRC_GXsfl_76 )
         {
            sendrow_762( ) ;
            nGXsfl_76_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_76_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_76_idx+1));
            sGXsfl_76_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_76_idx), 4, 0)), 4, "0");
            SubsflControlProps_762( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       String AV15DynamicFiltersSelector1 ,
                                       String AV16Funcao_Nome1 ,
                                       String AV29Funcao_UONom1 ,
                                       String AV18DynamicFiltersSelector2 ,
                                       String AV19Funcao_Nome2 ,
                                       String AV31Funcao_UONom2 ,
                                       String AV21DynamicFiltersSelector3 ,
                                       String AV22Funcao_Nome3 ,
                                       String AV33Funcao_UONom3 ,
                                       bool AV17DynamicFiltersEnabled2 ,
                                       bool AV20DynamicFiltersEnabled3 ,
                                       String AV37TFFuncao_Nome ,
                                       String AV38TFFuncao_Nome_Sel ,
                                       int AV41TFGrupoFuncao_Codigo ,
                                       int AV42TFGrupoFuncao_Codigo_To ,
                                       String AV45TFFuncao_UONom ,
                                       String AV46TFFuncao_UONom_Sel ,
                                       short AV49TFFuncao_Ativo_Sel ,
                                       String AV39ddo_Funcao_NomeTitleControlIdToReplace ,
                                       String AV43ddo_GrupoFuncao_CodigoTitleControlIdToReplace ,
                                       String AV47ddo_Funcao_UONomTitleControlIdToReplace ,
                                       String AV50ddo_Funcao_AtivoTitleControlIdToReplace ,
                                       wwpbaseobjects.SdtWWPContext AV6WWPContext ,
                                       String AV77Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV10GridState ,
                                       bool AV24DynamicFiltersIgnoreFirst ,
                                       bool AV23DynamicFiltersRemoving ,
                                       int A621Funcao_Codigo ,
                                       int A635Funcao_UOCod )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFCI2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_FUNCAO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A621Funcao_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "FUNCAO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A621Funcao_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_FUNCAO_NOME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A622Funcao_Nome, "@!"))));
         GxWebStd.gx_hidden_field( context, "FUNCAO_NOME", A622Funcao_Nome);
         GxWebStd.gx_hidden_field( context, "gxhash_GRUPOFUNCAO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A619GrupoFuncao_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "GRUPOFUNCAO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A619GrupoFuncao_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_FUNCAO_UOCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A635Funcao_UOCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "FUNCAO_UOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A635Funcao_UOCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_FUNCAO_ATIVO", GetSecureSignedToken( "", A630Funcao_Ativo));
         GxWebStd.gx_hidden_field( context, "FUNCAO_ATIVO", StringUtil.BoolToStr( A630Funcao_Ativo));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV18DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV18DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersSelector2", AV18DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV21DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV21DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector3", AV21DynamicFiltersSelector3);
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFCI2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV77Pgmname = "WWGeral_Funcao";
         context.Gx_err = 0;
      }

      protected void RFCI2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 76;
         /* Execute user event: E28CI2 */
         E28CI2 ();
         nGXsfl_76_idx = 1;
         sGXsfl_76_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_76_idx), 4, 0)), 4, "0");
         SubsflControlProps_762( ) ;
         nGXsfl_76_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_762( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV57WWGeral_FuncaoDS_1_Dynamicfiltersselector1 ,
                                                 AV58WWGeral_FuncaoDS_2_Funcao_nome1 ,
                                                 AV59WWGeral_FuncaoDS_3_Funcao_uonom1 ,
                                                 AV60WWGeral_FuncaoDS_4_Dynamicfiltersenabled2 ,
                                                 AV61WWGeral_FuncaoDS_5_Dynamicfiltersselector2 ,
                                                 AV62WWGeral_FuncaoDS_6_Funcao_nome2 ,
                                                 AV63WWGeral_FuncaoDS_7_Funcao_uonom2 ,
                                                 AV64WWGeral_FuncaoDS_8_Dynamicfiltersenabled3 ,
                                                 AV65WWGeral_FuncaoDS_9_Dynamicfiltersselector3 ,
                                                 AV66WWGeral_FuncaoDS_10_Funcao_nome3 ,
                                                 AV67WWGeral_FuncaoDS_11_Funcao_uonom3 ,
                                                 AV69WWGeral_FuncaoDS_13_Tffuncao_nome_sel ,
                                                 AV68WWGeral_FuncaoDS_12_Tffuncao_nome ,
                                                 AV70WWGeral_FuncaoDS_14_Tfgrupofuncao_codigo ,
                                                 AV71WWGeral_FuncaoDS_15_Tfgrupofuncao_codigo_to ,
                                                 AV73WWGeral_FuncaoDS_17_Tffuncao_uonom_sel ,
                                                 AV72WWGeral_FuncaoDS_16_Tffuncao_uonom ,
                                                 AV74WWGeral_FuncaoDS_18_Tffuncao_ativo_sel ,
                                                 A622Funcao_Nome ,
                                                 A636Funcao_UONom ,
                                                 A619GrupoFuncao_Codigo ,
                                                 A630Funcao_Ativo ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING,
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING,
                                                 TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                                 }
            });
            lV58WWGeral_FuncaoDS_2_Funcao_nome1 = StringUtil.Concat( StringUtil.RTrim( AV58WWGeral_FuncaoDS_2_Funcao_nome1), "%", "");
            lV59WWGeral_FuncaoDS_3_Funcao_uonom1 = StringUtil.PadR( StringUtil.RTrim( AV59WWGeral_FuncaoDS_3_Funcao_uonom1), 50, "%");
            lV62WWGeral_FuncaoDS_6_Funcao_nome2 = StringUtil.Concat( StringUtil.RTrim( AV62WWGeral_FuncaoDS_6_Funcao_nome2), "%", "");
            lV63WWGeral_FuncaoDS_7_Funcao_uonom2 = StringUtil.PadR( StringUtil.RTrim( AV63WWGeral_FuncaoDS_7_Funcao_uonom2), 50, "%");
            lV66WWGeral_FuncaoDS_10_Funcao_nome3 = StringUtil.Concat( StringUtil.RTrim( AV66WWGeral_FuncaoDS_10_Funcao_nome3), "%", "");
            lV67WWGeral_FuncaoDS_11_Funcao_uonom3 = StringUtil.PadR( StringUtil.RTrim( AV67WWGeral_FuncaoDS_11_Funcao_uonom3), 50, "%");
            lV68WWGeral_FuncaoDS_12_Tffuncao_nome = StringUtil.Concat( StringUtil.RTrim( AV68WWGeral_FuncaoDS_12_Tffuncao_nome), "%", "");
            lV72WWGeral_FuncaoDS_16_Tffuncao_uonom = StringUtil.PadR( StringUtil.RTrim( AV72WWGeral_FuncaoDS_16_Tffuncao_uonom), 50, "%");
            /* Using cursor H00CI2 */
            pr_default.execute(0, new Object[] {lV58WWGeral_FuncaoDS_2_Funcao_nome1, lV59WWGeral_FuncaoDS_3_Funcao_uonom1, lV62WWGeral_FuncaoDS_6_Funcao_nome2, lV63WWGeral_FuncaoDS_7_Funcao_uonom2, lV66WWGeral_FuncaoDS_10_Funcao_nome3, lV67WWGeral_FuncaoDS_11_Funcao_uonom3, lV68WWGeral_FuncaoDS_12_Tffuncao_nome, AV69WWGeral_FuncaoDS_13_Tffuncao_nome_sel, AV70WWGeral_FuncaoDS_14_Tfgrupofuncao_codigo, AV71WWGeral_FuncaoDS_15_Tfgrupofuncao_codigo_to, lV72WWGeral_FuncaoDS_16_Tffuncao_uonom, AV73WWGeral_FuncaoDS_17_Tffuncao_uonom_sel, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_76_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A630Funcao_Ativo = H00CI2_A630Funcao_Ativo[0];
               A636Funcao_UONom = H00CI2_A636Funcao_UONom[0];
               n636Funcao_UONom = H00CI2_n636Funcao_UONom[0];
               A635Funcao_UOCod = H00CI2_A635Funcao_UOCod[0];
               n635Funcao_UOCod = H00CI2_n635Funcao_UOCod[0];
               A619GrupoFuncao_Codigo = H00CI2_A619GrupoFuncao_Codigo[0];
               A622Funcao_Nome = H00CI2_A622Funcao_Nome[0];
               A621Funcao_Codigo = H00CI2_A621Funcao_Codigo[0];
               A636Funcao_UONom = H00CI2_A636Funcao_UONom[0];
               n636Funcao_UONom = H00CI2_n636Funcao_UONom[0];
               /* Execute user event: E29CI2 */
               E29CI2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 76;
            WBCI0( ) ;
         }
         nGXsfl_76_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         AV57WWGeral_FuncaoDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV58WWGeral_FuncaoDS_2_Funcao_nome1 = AV16Funcao_Nome1;
         AV59WWGeral_FuncaoDS_3_Funcao_uonom1 = AV29Funcao_UONom1;
         AV60WWGeral_FuncaoDS_4_Dynamicfiltersenabled2 = AV17DynamicFiltersEnabled2;
         AV61WWGeral_FuncaoDS_5_Dynamicfiltersselector2 = AV18DynamicFiltersSelector2;
         AV62WWGeral_FuncaoDS_6_Funcao_nome2 = AV19Funcao_Nome2;
         AV63WWGeral_FuncaoDS_7_Funcao_uonom2 = AV31Funcao_UONom2;
         AV64WWGeral_FuncaoDS_8_Dynamicfiltersenabled3 = AV20DynamicFiltersEnabled3;
         AV65WWGeral_FuncaoDS_9_Dynamicfiltersselector3 = AV21DynamicFiltersSelector3;
         AV66WWGeral_FuncaoDS_10_Funcao_nome3 = AV22Funcao_Nome3;
         AV67WWGeral_FuncaoDS_11_Funcao_uonom3 = AV33Funcao_UONom3;
         AV68WWGeral_FuncaoDS_12_Tffuncao_nome = AV37TFFuncao_Nome;
         AV69WWGeral_FuncaoDS_13_Tffuncao_nome_sel = AV38TFFuncao_Nome_Sel;
         AV70WWGeral_FuncaoDS_14_Tfgrupofuncao_codigo = AV41TFGrupoFuncao_Codigo;
         AV71WWGeral_FuncaoDS_15_Tfgrupofuncao_codigo_to = AV42TFGrupoFuncao_Codigo_To;
         AV72WWGeral_FuncaoDS_16_Tffuncao_uonom = AV45TFFuncao_UONom;
         AV73WWGeral_FuncaoDS_17_Tffuncao_uonom_sel = AV46TFFuncao_UONom_Sel;
         AV74WWGeral_FuncaoDS_18_Tffuncao_ativo_sel = AV49TFFuncao_Ativo_Sel;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV57WWGeral_FuncaoDS_1_Dynamicfiltersselector1 ,
                                              AV58WWGeral_FuncaoDS_2_Funcao_nome1 ,
                                              AV59WWGeral_FuncaoDS_3_Funcao_uonom1 ,
                                              AV60WWGeral_FuncaoDS_4_Dynamicfiltersenabled2 ,
                                              AV61WWGeral_FuncaoDS_5_Dynamicfiltersselector2 ,
                                              AV62WWGeral_FuncaoDS_6_Funcao_nome2 ,
                                              AV63WWGeral_FuncaoDS_7_Funcao_uonom2 ,
                                              AV64WWGeral_FuncaoDS_8_Dynamicfiltersenabled3 ,
                                              AV65WWGeral_FuncaoDS_9_Dynamicfiltersselector3 ,
                                              AV66WWGeral_FuncaoDS_10_Funcao_nome3 ,
                                              AV67WWGeral_FuncaoDS_11_Funcao_uonom3 ,
                                              AV69WWGeral_FuncaoDS_13_Tffuncao_nome_sel ,
                                              AV68WWGeral_FuncaoDS_12_Tffuncao_nome ,
                                              AV70WWGeral_FuncaoDS_14_Tfgrupofuncao_codigo ,
                                              AV71WWGeral_FuncaoDS_15_Tfgrupofuncao_codigo_to ,
                                              AV73WWGeral_FuncaoDS_17_Tffuncao_uonom_sel ,
                                              AV72WWGeral_FuncaoDS_16_Tffuncao_uonom ,
                                              AV74WWGeral_FuncaoDS_18_Tffuncao_ativo_sel ,
                                              A622Funcao_Nome ,
                                              A636Funcao_UONom ,
                                              A619GrupoFuncao_Codigo ,
                                              A630Funcao_Ativo ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                              }
         });
         lV58WWGeral_FuncaoDS_2_Funcao_nome1 = StringUtil.Concat( StringUtil.RTrim( AV58WWGeral_FuncaoDS_2_Funcao_nome1), "%", "");
         lV59WWGeral_FuncaoDS_3_Funcao_uonom1 = StringUtil.PadR( StringUtil.RTrim( AV59WWGeral_FuncaoDS_3_Funcao_uonom1), 50, "%");
         lV62WWGeral_FuncaoDS_6_Funcao_nome2 = StringUtil.Concat( StringUtil.RTrim( AV62WWGeral_FuncaoDS_6_Funcao_nome2), "%", "");
         lV63WWGeral_FuncaoDS_7_Funcao_uonom2 = StringUtil.PadR( StringUtil.RTrim( AV63WWGeral_FuncaoDS_7_Funcao_uonom2), 50, "%");
         lV66WWGeral_FuncaoDS_10_Funcao_nome3 = StringUtil.Concat( StringUtil.RTrim( AV66WWGeral_FuncaoDS_10_Funcao_nome3), "%", "");
         lV67WWGeral_FuncaoDS_11_Funcao_uonom3 = StringUtil.PadR( StringUtil.RTrim( AV67WWGeral_FuncaoDS_11_Funcao_uonom3), 50, "%");
         lV68WWGeral_FuncaoDS_12_Tffuncao_nome = StringUtil.Concat( StringUtil.RTrim( AV68WWGeral_FuncaoDS_12_Tffuncao_nome), "%", "");
         lV72WWGeral_FuncaoDS_16_Tffuncao_uonom = StringUtil.PadR( StringUtil.RTrim( AV72WWGeral_FuncaoDS_16_Tffuncao_uonom), 50, "%");
         /* Using cursor H00CI3 */
         pr_default.execute(1, new Object[] {lV58WWGeral_FuncaoDS_2_Funcao_nome1, lV59WWGeral_FuncaoDS_3_Funcao_uonom1, lV62WWGeral_FuncaoDS_6_Funcao_nome2, lV63WWGeral_FuncaoDS_7_Funcao_uonom2, lV66WWGeral_FuncaoDS_10_Funcao_nome3, lV67WWGeral_FuncaoDS_11_Funcao_uonom3, lV68WWGeral_FuncaoDS_12_Tffuncao_nome, AV69WWGeral_FuncaoDS_13_Tffuncao_nome_sel, AV70WWGeral_FuncaoDS_14_Tfgrupofuncao_codigo, AV71WWGeral_FuncaoDS_15_Tfgrupofuncao_codigo_to, lV72WWGeral_FuncaoDS_16_Tffuncao_uonom, AV73WWGeral_FuncaoDS_17_Tffuncao_uonom_sel});
         GRID_nRecordCount = H00CI3_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         AV57WWGeral_FuncaoDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV58WWGeral_FuncaoDS_2_Funcao_nome1 = AV16Funcao_Nome1;
         AV59WWGeral_FuncaoDS_3_Funcao_uonom1 = AV29Funcao_UONom1;
         AV60WWGeral_FuncaoDS_4_Dynamicfiltersenabled2 = AV17DynamicFiltersEnabled2;
         AV61WWGeral_FuncaoDS_5_Dynamicfiltersselector2 = AV18DynamicFiltersSelector2;
         AV62WWGeral_FuncaoDS_6_Funcao_nome2 = AV19Funcao_Nome2;
         AV63WWGeral_FuncaoDS_7_Funcao_uonom2 = AV31Funcao_UONom2;
         AV64WWGeral_FuncaoDS_8_Dynamicfiltersenabled3 = AV20DynamicFiltersEnabled3;
         AV65WWGeral_FuncaoDS_9_Dynamicfiltersselector3 = AV21DynamicFiltersSelector3;
         AV66WWGeral_FuncaoDS_10_Funcao_nome3 = AV22Funcao_Nome3;
         AV67WWGeral_FuncaoDS_11_Funcao_uonom3 = AV33Funcao_UONom3;
         AV68WWGeral_FuncaoDS_12_Tffuncao_nome = AV37TFFuncao_Nome;
         AV69WWGeral_FuncaoDS_13_Tffuncao_nome_sel = AV38TFFuncao_Nome_Sel;
         AV70WWGeral_FuncaoDS_14_Tfgrupofuncao_codigo = AV41TFGrupoFuncao_Codigo;
         AV71WWGeral_FuncaoDS_15_Tfgrupofuncao_codigo_to = AV42TFGrupoFuncao_Codigo_To;
         AV72WWGeral_FuncaoDS_16_Tffuncao_uonom = AV45TFFuncao_UONom;
         AV73WWGeral_FuncaoDS_17_Tffuncao_uonom_sel = AV46TFFuncao_UONom_Sel;
         AV74WWGeral_FuncaoDS_18_Tffuncao_ativo_sel = AV49TFFuncao_Ativo_Sel;
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16Funcao_Nome1, AV29Funcao_UONom1, AV18DynamicFiltersSelector2, AV19Funcao_Nome2, AV31Funcao_UONom2, AV21DynamicFiltersSelector3, AV22Funcao_Nome3, AV33Funcao_UONom3, AV17DynamicFiltersEnabled2, AV20DynamicFiltersEnabled3, AV37TFFuncao_Nome, AV38TFFuncao_Nome_Sel, AV41TFGrupoFuncao_Codigo, AV42TFGrupoFuncao_Codigo_To, AV45TFFuncao_UONom, AV46TFFuncao_UONom_Sel, AV49TFFuncao_Ativo_Sel, AV39ddo_Funcao_NomeTitleControlIdToReplace, AV43ddo_GrupoFuncao_CodigoTitleControlIdToReplace, AV47ddo_Funcao_UONomTitleControlIdToReplace, AV50ddo_Funcao_AtivoTitleControlIdToReplace, AV6WWPContext, AV77Pgmname, AV10GridState, AV24DynamicFiltersIgnoreFirst, AV23DynamicFiltersRemoving, A621Funcao_Codigo, A635Funcao_UOCod) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         AV57WWGeral_FuncaoDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV58WWGeral_FuncaoDS_2_Funcao_nome1 = AV16Funcao_Nome1;
         AV59WWGeral_FuncaoDS_3_Funcao_uonom1 = AV29Funcao_UONom1;
         AV60WWGeral_FuncaoDS_4_Dynamicfiltersenabled2 = AV17DynamicFiltersEnabled2;
         AV61WWGeral_FuncaoDS_5_Dynamicfiltersselector2 = AV18DynamicFiltersSelector2;
         AV62WWGeral_FuncaoDS_6_Funcao_nome2 = AV19Funcao_Nome2;
         AV63WWGeral_FuncaoDS_7_Funcao_uonom2 = AV31Funcao_UONom2;
         AV64WWGeral_FuncaoDS_8_Dynamicfiltersenabled3 = AV20DynamicFiltersEnabled3;
         AV65WWGeral_FuncaoDS_9_Dynamicfiltersselector3 = AV21DynamicFiltersSelector3;
         AV66WWGeral_FuncaoDS_10_Funcao_nome3 = AV22Funcao_Nome3;
         AV67WWGeral_FuncaoDS_11_Funcao_uonom3 = AV33Funcao_UONom3;
         AV68WWGeral_FuncaoDS_12_Tffuncao_nome = AV37TFFuncao_Nome;
         AV69WWGeral_FuncaoDS_13_Tffuncao_nome_sel = AV38TFFuncao_Nome_Sel;
         AV70WWGeral_FuncaoDS_14_Tfgrupofuncao_codigo = AV41TFGrupoFuncao_Codigo;
         AV71WWGeral_FuncaoDS_15_Tfgrupofuncao_codigo_to = AV42TFGrupoFuncao_Codigo_To;
         AV72WWGeral_FuncaoDS_16_Tffuncao_uonom = AV45TFFuncao_UONom;
         AV73WWGeral_FuncaoDS_17_Tffuncao_uonom_sel = AV46TFFuncao_UONom_Sel;
         AV74WWGeral_FuncaoDS_18_Tffuncao_ativo_sel = AV49TFFuncao_Ativo_Sel;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16Funcao_Nome1, AV29Funcao_UONom1, AV18DynamicFiltersSelector2, AV19Funcao_Nome2, AV31Funcao_UONom2, AV21DynamicFiltersSelector3, AV22Funcao_Nome3, AV33Funcao_UONom3, AV17DynamicFiltersEnabled2, AV20DynamicFiltersEnabled3, AV37TFFuncao_Nome, AV38TFFuncao_Nome_Sel, AV41TFGrupoFuncao_Codigo, AV42TFGrupoFuncao_Codigo_To, AV45TFFuncao_UONom, AV46TFFuncao_UONom_Sel, AV49TFFuncao_Ativo_Sel, AV39ddo_Funcao_NomeTitleControlIdToReplace, AV43ddo_GrupoFuncao_CodigoTitleControlIdToReplace, AV47ddo_Funcao_UONomTitleControlIdToReplace, AV50ddo_Funcao_AtivoTitleControlIdToReplace, AV6WWPContext, AV77Pgmname, AV10GridState, AV24DynamicFiltersIgnoreFirst, AV23DynamicFiltersRemoving, A621Funcao_Codigo, A635Funcao_UOCod) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         AV57WWGeral_FuncaoDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV58WWGeral_FuncaoDS_2_Funcao_nome1 = AV16Funcao_Nome1;
         AV59WWGeral_FuncaoDS_3_Funcao_uonom1 = AV29Funcao_UONom1;
         AV60WWGeral_FuncaoDS_4_Dynamicfiltersenabled2 = AV17DynamicFiltersEnabled2;
         AV61WWGeral_FuncaoDS_5_Dynamicfiltersselector2 = AV18DynamicFiltersSelector2;
         AV62WWGeral_FuncaoDS_6_Funcao_nome2 = AV19Funcao_Nome2;
         AV63WWGeral_FuncaoDS_7_Funcao_uonom2 = AV31Funcao_UONom2;
         AV64WWGeral_FuncaoDS_8_Dynamicfiltersenabled3 = AV20DynamicFiltersEnabled3;
         AV65WWGeral_FuncaoDS_9_Dynamicfiltersselector3 = AV21DynamicFiltersSelector3;
         AV66WWGeral_FuncaoDS_10_Funcao_nome3 = AV22Funcao_Nome3;
         AV67WWGeral_FuncaoDS_11_Funcao_uonom3 = AV33Funcao_UONom3;
         AV68WWGeral_FuncaoDS_12_Tffuncao_nome = AV37TFFuncao_Nome;
         AV69WWGeral_FuncaoDS_13_Tffuncao_nome_sel = AV38TFFuncao_Nome_Sel;
         AV70WWGeral_FuncaoDS_14_Tfgrupofuncao_codigo = AV41TFGrupoFuncao_Codigo;
         AV71WWGeral_FuncaoDS_15_Tfgrupofuncao_codigo_to = AV42TFGrupoFuncao_Codigo_To;
         AV72WWGeral_FuncaoDS_16_Tffuncao_uonom = AV45TFFuncao_UONom;
         AV73WWGeral_FuncaoDS_17_Tffuncao_uonom_sel = AV46TFFuncao_UONom_Sel;
         AV74WWGeral_FuncaoDS_18_Tffuncao_ativo_sel = AV49TFFuncao_Ativo_Sel;
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16Funcao_Nome1, AV29Funcao_UONom1, AV18DynamicFiltersSelector2, AV19Funcao_Nome2, AV31Funcao_UONom2, AV21DynamicFiltersSelector3, AV22Funcao_Nome3, AV33Funcao_UONom3, AV17DynamicFiltersEnabled2, AV20DynamicFiltersEnabled3, AV37TFFuncao_Nome, AV38TFFuncao_Nome_Sel, AV41TFGrupoFuncao_Codigo, AV42TFGrupoFuncao_Codigo_To, AV45TFFuncao_UONom, AV46TFFuncao_UONom_Sel, AV49TFFuncao_Ativo_Sel, AV39ddo_Funcao_NomeTitleControlIdToReplace, AV43ddo_GrupoFuncao_CodigoTitleControlIdToReplace, AV47ddo_Funcao_UONomTitleControlIdToReplace, AV50ddo_Funcao_AtivoTitleControlIdToReplace, AV6WWPContext, AV77Pgmname, AV10GridState, AV24DynamicFiltersIgnoreFirst, AV23DynamicFiltersRemoving, A621Funcao_Codigo, A635Funcao_UOCod) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         AV57WWGeral_FuncaoDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV58WWGeral_FuncaoDS_2_Funcao_nome1 = AV16Funcao_Nome1;
         AV59WWGeral_FuncaoDS_3_Funcao_uonom1 = AV29Funcao_UONom1;
         AV60WWGeral_FuncaoDS_4_Dynamicfiltersenabled2 = AV17DynamicFiltersEnabled2;
         AV61WWGeral_FuncaoDS_5_Dynamicfiltersselector2 = AV18DynamicFiltersSelector2;
         AV62WWGeral_FuncaoDS_6_Funcao_nome2 = AV19Funcao_Nome2;
         AV63WWGeral_FuncaoDS_7_Funcao_uonom2 = AV31Funcao_UONom2;
         AV64WWGeral_FuncaoDS_8_Dynamicfiltersenabled3 = AV20DynamicFiltersEnabled3;
         AV65WWGeral_FuncaoDS_9_Dynamicfiltersselector3 = AV21DynamicFiltersSelector3;
         AV66WWGeral_FuncaoDS_10_Funcao_nome3 = AV22Funcao_Nome3;
         AV67WWGeral_FuncaoDS_11_Funcao_uonom3 = AV33Funcao_UONom3;
         AV68WWGeral_FuncaoDS_12_Tffuncao_nome = AV37TFFuncao_Nome;
         AV69WWGeral_FuncaoDS_13_Tffuncao_nome_sel = AV38TFFuncao_Nome_Sel;
         AV70WWGeral_FuncaoDS_14_Tfgrupofuncao_codigo = AV41TFGrupoFuncao_Codigo;
         AV71WWGeral_FuncaoDS_15_Tfgrupofuncao_codigo_to = AV42TFGrupoFuncao_Codigo_To;
         AV72WWGeral_FuncaoDS_16_Tffuncao_uonom = AV45TFFuncao_UONom;
         AV73WWGeral_FuncaoDS_17_Tffuncao_uonom_sel = AV46TFFuncao_UONom_Sel;
         AV74WWGeral_FuncaoDS_18_Tffuncao_ativo_sel = AV49TFFuncao_Ativo_Sel;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16Funcao_Nome1, AV29Funcao_UONom1, AV18DynamicFiltersSelector2, AV19Funcao_Nome2, AV31Funcao_UONom2, AV21DynamicFiltersSelector3, AV22Funcao_Nome3, AV33Funcao_UONom3, AV17DynamicFiltersEnabled2, AV20DynamicFiltersEnabled3, AV37TFFuncao_Nome, AV38TFFuncao_Nome_Sel, AV41TFGrupoFuncao_Codigo, AV42TFGrupoFuncao_Codigo_To, AV45TFFuncao_UONom, AV46TFFuncao_UONom_Sel, AV49TFFuncao_Ativo_Sel, AV39ddo_Funcao_NomeTitleControlIdToReplace, AV43ddo_GrupoFuncao_CodigoTitleControlIdToReplace, AV47ddo_Funcao_UONomTitleControlIdToReplace, AV50ddo_Funcao_AtivoTitleControlIdToReplace, AV6WWPContext, AV77Pgmname, AV10GridState, AV24DynamicFiltersIgnoreFirst, AV23DynamicFiltersRemoving, A621Funcao_Codigo, A635Funcao_UOCod) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         AV57WWGeral_FuncaoDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV58WWGeral_FuncaoDS_2_Funcao_nome1 = AV16Funcao_Nome1;
         AV59WWGeral_FuncaoDS_3_Funcao_uonom1 = AV29Funcao_UONom1;
         AV60WWGeral_FuncaoDS_4_Dynamicfiltersenabled2 = AV17DynamicFiltersEnabled2;
         AV61WWGeral_FuncaoDS_5_Dynamicfiltersselector2 = AV18DynamicFiltersSelector2;
         AV62WWGeral_FuncaoDS_6_Funcao_nome2 = AV19Funcao_Nome2;
         AV63WWGeral_FuncaoDS_7_Funcao_uonom2 = AV31Funcao_UONom2;
         AV64WWGeral_FuncaoDS_8_Dynamicfiltersenabled3 = AV20DynamicFiltersEnabled3;
         AV65WWGeral_FuncaoDS_9_Dynamicfiltersselector3 = AV21DynamicFiltersSelector3;
         AV66WWGeral_FuncaoDS_10_Funcao_nome3 = AV22Funcao_Nome3;
         AV67WWGeral_FuncaoDS_11_Funcao_uonom3 = AV33Funcao_UONom3;
         AV68WWGeral_FuncaoDS_12_Tffuncao_nome = AV37TFFuncao_Nome;
         AV69WWGeral_FuncaoDS_13_Tffuncao_nome_sel = AV38TFFuncao_Nome_Sel;
         AV70WWGeral_FuncaoDS_14_Tfgrupofuncao_codigo = AV41TFGrupoFuncao_Codigo;
         AV71WWGeral_FuncaoDS_15_Tfgrupofuncao_codigo_to = AV42TFGrupoFuncao_Codigo_To;
         AV72WWGeral_FuncaoDS_16_Tffuncao_uonom = AV45TFFuncao_UONom;
         AV73WWGeral_FuncaoDS_17_Tffuncao_uonom_sel = AV46TFFuncao_UONom_Sel;
         AV74WWGeral_FuncaoDS_18_Tffuncao_ativo_sel = AV49TFFuncao_Ativo_Sel;
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16Funcao_Nome1, AV29Funcao_UONom1, AV18DynamicFiltersSelector2, AV19Funcao_Nome2, AV31Funcao_UONom2, AV21DynamicFiltersSelector3, AV22Funcao_Nome3, AV33Funcao_UONom3, AV17DynamicFiltersEnabled2, AV20DynamicFiltersEnabled3, AV37TFFuncao_Nome, AV38TFFuncao_Nome_Sel, AV41TFGrupoFuncao_Codigo, AV42TFGrupoFuncao_Codigo_To, AV45TFFuncao_UONom, AV46TFFuncao_UONom_Sel, AV49TFFuncao_Ativo_Sel, AV39ddo_Funcao_NomeTitleControlIdToReplace, AV43ddo_GrupoFuncao_CodigoTitleControlIdToReplace, AV47ddo_Funcao_UONomTitleControlIdToReplace, AV50ddo_Funcao_AtivoTitleControlIdToReplace, AV6WWPContext, AV77Pgmname, AV10GridState, AV24DynamicFiltersIgnoreFirst, AV23DynamicFiltersRemoving, A621Funcao_Codigo, A635Funcao_UOCod) ;
         }
         return (int)(0) ;
      }

      protected void STRUPCI0( )
      {
         /* Before Start, stand alone formulas. */
         AV77Pgmname = "WWGeral_Funcao";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E27CI2 */
         E27CI2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV51DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vFUNCAO_NOMETITLEFILTERDATA"), AV36Funcao_NomeTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vGRUPOFUNCAO_CODIGOTITLEFILTERDATA"), AV40GrupoFuncao_CodigoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vFUNCAO_UONOMTITLEFILTERDATA"), AV44Funcao_UONomTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vFUNCAO_ATIVOTITLEFILTERDATA"), AV48Funcao_AtivoTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV13OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV15DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            AV16Funcao_Nome1 = StringUtil.Upper( cgiGet( edtavFuncao_nome1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16Funcao_Nome1", AV16Funcao_Nome1);
            AV29Funcao_UONom1 = StringUtil.Upper( cgiGet( edtavFuncao_uonom1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29Funcao_UONom1", AV29Funcao_UONom1);
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV18DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersSelector2", AV18DynamicFiltersSelector2);
            AV19Funcao_Nome2 = StringUtil.Upper( cgiGet( edtavFuncao_nome2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Funcao_Nome2", AV19Funcao_Nome2);
            AV31Funcao_UONom2 = StringUtil.Upper( cgiGet( edtavFuncao_uonom2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31Funcao_UONom2", AV31Funcao_UONom2);
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV21DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector3", AV21DynamicFiltersSelector3);
            AV22Funcao_Nome3 = StringUtil.Upper( cgiGet( edtavFuncao_nome3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Funcao_Nome3", AV22Funcao_Nome3);
            AV33Funcao_UONom3 = StringUtil.Upper( cgiGet( edtavFuncao_uonom3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33Funcao_UONom3", AV33Funcao_UONom3);
            AV17DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersEnabled2", AV17DynamicFiltersEnabled2);
            AV20DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled3", AV20DynamicFiltersEnabled3);
            AV37TFFuncao_Nome = StringUtil.Upper( cgiGet( edtavTffuncao_nome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37TFFuncao_Nome", AV37TFFuncao_Nome);
            AV38TFFuncao_Nome_Sel = StringUtil.Upper( cgiGet( edtavTffuncao_nome_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFFuncao_Nome_Sel", AV38TFFuncao_Nome_Sel);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfgrupofuncao_codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfgrupofuncao_codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFGRUPOFUNCAO_CODIGO");
               GX_FocusControl = edtavTfgrupofuncao_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV41TFGrupoFuncao_Codigo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41TFGrupoFuncao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV41TFGrupoFuncao_Codigo), 6, 0)));
            }
            else
            {
               AV41TFGrupoFuncao_Codigo = (int)(context.localUtil.CToN( cgiGet( edtavTfgrupofuncao_codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41TFGrupoFuncao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV41TFGrupoFuncao_Codigo), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfgrupofuncao_codigo_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfgrupofuncao_codigo_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFGRUPOFUNCAO_CODIGO_TO");
               GX_FocusControl = edtavTfgrupofuncao_codigo_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV42TFGrupoFuncao_Codigo_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42TFGrupoFuncao_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV42TFGrupoFuncao_Codigo_To), 6, 0)));
            }
            else
            {
               AV42TFGrupoFuncao_Codigo_To = (int)(context.localUtil.CToN( cgiGet( edtavTfgrupofuncao_codigo_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42TFGrupoFuncao_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV42TFGrupoFuncao_Codigo_To), 6, 0)));
            }
            AV45TFFuncao_UONom = StringUtil.Upper( cgiGet( edtavTffuncao_uonom_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFFuncao_UONom", AV45TFFuncao_UONom);
            AV46TFFuncao_UONom_Sel = StringUtil.Upper( cgiGet( edtavTffuncao_uonom_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFFuncao_UONom_Sel", AV46TFFuncao_UONom_Sel);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTffuncao_ativo_sel_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTffuncao_ativo_sel_Internalname), ",", ".") > Convert.ToDecimal( 9 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFFUNCAO_ATIVO_SEL");
               GX_FocusControl = edtavTffuncao_ativo_sel_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV49TFFuncao_Ativo_Sel = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFFuncao_Ativo_Sel", StringUtil.Str( (decimal)(AV49TFFuncao_Ativo_Sel), 1, 0));
            }
            else
            {
               AV49TFFuncao_Ativo_Sel = (short)(context.localUtil.CToN( cgiGet( edtavTffuncao_ativo_sel_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFFuncao_Ativo_Sel", StringUtil.Str( (decimal)(AV49TFFuncao_Ativo_Sel), 1, 0));
            }
            AV39ddo_Funcao_NomeTitleControlIdToReplace = cgiGet( edtavDdo_funcao_nometitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39ddo_Funcao_NomeTitleControlIdToReplace", AV39ddo_Funcao_NomeTitleControlIdToReplace);
            AV43ddo_GrupoFuncao_CodigoTitleControlIdToReplace = cgiGet( edtavDdo_grupofuncao_codigotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43ddo_GrupoFuncao_CodigoTitleControlIdToReplace", AV43ddo_GrupoFuncao_CodigoTitleControlIdToReplace);
            AV47ddo_Funcao_UONomTitleControlIdToReplace = cgiGet( edtavDdo_funcao_uonomtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47ddo_Funcao_UONomTitleControlIdToReplace", AV47ddo_Funcao_UONomTitleControlIdToReplace);
            AV50ddo_Funcao_AtivoTitleControlIdToReplace = cgiGet( edtavDdo_funcao_ativotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50ddo_Funcao_AtivoTitleControlIdToReplace", AV50ddo_Funcao_AtivoTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_76 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_76"), ",", "."));
            AV53GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV54GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_funcao_nome_Caption = cgiGet( "DDO_FUNCAO_NOME_Caption");
            Ddo_funcao_nome_Tooltip = cgiGet( "DDO_FUNCAO_NOME_Tooltip");
            Ddo_funcao_nome_Cls = cgiGet( "DDO_FUNCAO_NOME_Cls");
            Ddo_funcao_nome_Filteredtext_set = cgiGet( "DDO_FUNCAO_NOME_Filteredtext_set");
            Ddo_funcao_nome_Selectedvalue_set = cgiGet( "DDO_FUNCAO_NOME_Selectedvalue_set");
            Ddo_funcao_nome_Dropdownoptionstype = cgiGet( "DDO_FUNCAO_NOME_Dropdownoptionstype");
            Ddo_funcao_nome_Titlecontrolidtoreplace = cgiGet( "DDO_FUNCAO_NOME_Titlecontrolidtoreplace");
            Ddo_funcao_nome_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_FUNCAO_NOME_Includesortasc"));
            Ddo_funcao_nome_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_FUNCAO_NOME_Includesortdsc"));
            Ddo_funcao_nome_Sortedstatus = cgiGet( "DDO_FUNCAO_NOME_Sortedstatus");
            Ddo_funcao_nome_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_FUNCAO_NOME_Includefilter"));
            Ddo_funcao_nome_Filtertype = cgiGet( "DDO_FUNCAO_NOME_Filtertype");
            Ddo_funcao_nome_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_FUNCAO_NOME_Filterisrange"));
            Ddo_funcao_nome_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_FUNCAO_NOME_Includedatalist"));
            Ddo_funcao_nome_Datalisttype = cgiGet( "DDO_FUNCAO_NOME_Datalisttype");
            Ddo_funcao_nome_Datalistproc = cgiGet( "DDO_FUNCAO_NOME_Datalistproc");
            Ddo_funcao_nome_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_FUNCAO_NOME_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_funcao_nome_Sortasc = cgiGet( "DDO_FUNCAO_NOME_Sortasc");
            Ddo_funcao_nome_Sortdsc = cgiGet( "DDO_FUNCAO_NOME_Sortdsc");
            Ddo_funcao_nome_Loadingdata = cgiGet( "DDO_FUNCAO_NOME_Loadingdata");
            Ddo_funcao_nome_Cleanfilter = cgiGet( "DDO_FUNCAO_NOME_Cleanfilter");
            Ddo_funcao_nome_Noresultsfound = cgiGet( "DDO_FUNCAO_NOME_Noresultsfound");
            Ddo_funcao_nome_Searchbuttontext = cgiGet( "DDO_FUNCAO_NOME_Searchbuttontext");
            Ddo_grupofuncao_codigo_Caption = cgiGet( "DDO_GRUPOFUNCAO_CODIGO_Caption");
            Ddo_grupofuncao_codigo_Tooltip = cgiGet( "DDO_GRUPOFUNCAO_CODIGO_Tooltip");
            Ddo_grupofuncao_codigo_Cls = cgiGet( "DDO_GRUPOFUNCAO_CODIGO_Cls");
            Ddo_grupofuncao_codigo_Filteredtext_set = cgiGet( "DDO_GRUPOFUNCAO_CODIGO_Filteredtext_set");
            Ddo_grupofuncao_codigo_Filteredtextto_set = cgiGet( "DDO_GRUPOFUNCAO_CODIGO_Filteredtextto_set");
            Ddo_grupofuncao_codigo_Dropdownoptionstype = cgiGet( "DDO_GRUPOFUNCAO_CODIGO_Dropdownoptionstype");
            Ddo_grupofuncao_codigo_Titlecontrolidtoreplace = cgiGet( "DDO_GRUPOFUNCAO_CODIGO_Titlecontrolidtoreplace");
            Ddo_grupofuncao_codigo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_GRUPOFUNCAO_CODIGO_Includesortasc"));
            Ddo_grupofuncao_codigo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_GRUPOFUNCAO_CODIGO_Includesortdsc"));
            Ddo_grupofuncao_codigo_Sortedstatus = cgiGet( "DDO_GRUPOFUNCAO_CODIGO_Sortedstatus");
            Ddo_grupofuncao_codigo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_GRUPOFUNCAO_CODIGO_Includefilter"));
            Ddo_grupofuncao_codigo_Filtertype = cgiGet( "DDO_GRUPOFUNCAO_CODIGO_Filtertype");
            Ddo_grupofuncao_codigo_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_GRUPOFUNCAO_CODIGO_Filterisrange"));
            Ddo_grupofuncao_codigo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_GRUPOFUNCAO_CODIGO_Includedatalist"));
            Ddo_grupofuncao_codigo_Sortasc = cgiGet( "DDO_GRUPOFUNCAO_CODIGO_Sortasc");
            Ddo_grupofuncao_codigo_Sortdsc = cgiGet( "DDO_GRUPOFUNCAO_CODIGO_Sortdsc");
            Ddo_grupofuncao_codigo_Cleanfilter = cgiGet( "DDO_GRUPOFUNCAO_CODIGO_Cleanfilter");
            Ddo_grupofuncao_codigo_Rangefilterfrom = cgiGet( "DDO_GRUPOFUNCAO_CODIGO_Rangefilterfrom");
            Ddo_grupofuncao_codigo_Rangefilterto = cgiGet( "DDO_GRUPOFUNCAO_CODIGO_Rangefilterto");
            Ddo_grupofuncao_codigo_Searchbuttontext = cgiGet( "DDO_GRUPOFUNCAO_CODIGO_Searchbuttontext");
            Ddo_funcao_uonom_Caption = cgiGet( "DDO_FUNCAO_UONOM_Caption");
            Ddo_funcao_uonom_Tooltip = cgiGet( "DDO_FUNCAO_UONOM_Tooltip");
            Ddo_funcao_uonom_Cls = cgiGet( "DDO_FUNCAO_UONOM_Cls");
            Ddo_funcao_uonom_Filteredtext_set = cgiGet( "DDO_FUNCAO_UONOM_Filteredtext_set");
            Ddo_funcao_uonom_Selectedvalue_set = cgiGet( "DDO_FUNCAO_UONOM_Selectedvalue_set");
            Ddo_funcao_uonom_Dropdownoptionstype = cgiGet( "DDO_FUNCAO_UONOM_Dropdownoptionstype");
            Ddo_funcao_uonom_Titlecontrolidtoreplace = cgiGet( "DDO_FUNCAO_UONOM_Titlecontrolidtoreplace");
            Ddo_funcao_uonom_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_FUNCAO_UONOM_Includesortasc"));
            Ddo_funcao_uonom_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_FUNCAO_UONOM_Includesortdsc"));
            Ddo_funcao_uonom_Sortedstatus = cgiGet( "DDO_FUNCAO_UONOM_Sortedstatus");
            Ddo_funcao_uonom_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_FUNCAO_UONOM_Includefilter"));
            Ddo_funcao_uonom_Filtertype = cgiGet( "DDO_FUNCAO_UONOM_Filtertype");
            Ddo_funcao_uonom_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_FUNCAO_UONOM_Filterisrange"));
            Ddo_funcao_uonom_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_FUNCAO_UONOM_Includedatalist"));
            Ddo_funcao_uonom_Datalisttype = cgiGet( "DDO_FUNCAO_UONOM_Datalisttype");
            Ddo_funcao_uonom_Datalistproc = cgiGet( "DDO_FUNCAO_UONOM_Datalistproc");
            Ddo_funcao_uonom_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_FUNCAO_UONOM_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_funcao_uonom_Sortasc = cgiGet( "DDO_FUNCAO_UONOM_Sortasc");
            Ddo_funcao_uonom_Sortdsc = cgiGet( "DDO_FUNCAO_UONOM_Sortdsc");
            Ddo_funcao_uonom_Loadingdata = cgiGet( "DDO_FUNCAO_UONOM_Loadingdata");
            Ddo_funcao_uonom_Cleanfilter = cgiGet( "DDO_FUNCAO_UONOM_Cleanfilter");
            Ddo_funcao_uonom_Noresultsfound = cgiGet( "DDO_FUNCAO_UONOM_Noresultsfound");
            Ddo_funcao_uonom_Searchbuttontext = cgiGet( "DDO_FUNCAO_UONOM_Searchbuttontext");
            Ddo_funcao_ativo_Caption = cgiGet( "DDO_FUNCAO_ATIVO_Caption");
            Ddo_funcao_ativo_Tooltip = cgiGet( "DDO_FUNCAO_ATIVO_Tooltip");
            Ddo_funcao_ativo_Cls = cgiGet( "DDO_FUNCAO_ATIVO_Cls");
            Ddo_funcao_ativo_Selectedvalue_set = cgiGet( "DDO_FUNCAO_ATIVO_Selectedvalue_set");
            Ddo_funcao_ativo_Dropdownoptionstype = cgiGet( "DDO_FUNCAO_ATIVO_Dropdownoptionstype");
            Ddo_funcao_ativo_Titlecontrolidtoreplace = cgiGet( "DDO_FUNCAO_ATIVO_Titlecontrolidtoreplace");
            Ddo_funcao_ativo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_FUNCAO_ATIVO_Includesortasc"));
            Ddo_funcao_ativo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_FUNCAO_ATIVO_Includesortdsc"));
            Ddo_funcao_ativo_Sortedstatus = cgiGet( "DDO_FUNCAO_ATIVO_Sortedstatus");
            Ddo_funcao_ativo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_FUNCAO_ATIVO_Includefilter"));
            Ddo_funcao_ativo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_FUNCAO_ATIVO_Includedatalist"));
            Ddo_funcao_ativo_Datalisttype = cgiGet( "DDO_FUNCAO_ATIVO_Datalisttype");
            Ddo_funcao_ativo_Datalistfixedvalues = cgiGet( "DDO_FUNCAO_ATIVO_Datalistfixedvalues");
            Ddo_funcao_ativo_Sortasc = cgiGet( "DDO_FUNCAO_ATIVO_Sortasc");
            Ddo_funcao_ativo_Sortdsc = cgiGet( "DDO_FUNCAO_ATIVO_Sortdsc");
            Ddo_funcao_ativo_Cleanfilter = cgiGet( "DDO_FUNCAO_ATIVO_Cleanfilter");
            Ddo_funcao_ativo_Searchbuttontext = cgiGet( "DDO_FUNCAO_ATIVO_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_funcao_nome_Activeeventkey = cgiGet( "DDO_FUNCAO_NOME_Activeeventkey");
            Ddo_funcao_nome_Filteredtext_get = cgiGet( "DDO_FUNCAO_NOME_Filteredtext_get");
            Ddo_funcao_nome_Selectedvalue_get = cgiGet( "DDO_FUNCAO_NOME_Selectedvalue_get");
            Ddo_grupofuncao_codigo_Activeeventkey = cgiGet( "DDO_GRUPOFUNCAO_CODIGO_Activeeventkey");
            Ddo_grupofuncao_codigo_Filteredtext_get = cgiGet( "DDO_GRUPOFUNCAO_CODIGO_Filteredtext_get");
            Ddo_grupofuncao_codigo_Filteredtextto_get = cgiGet( "DDO_GRUPOFUNCAO_CODIGO_Filteredtextto_get");
            Ddo_funcao_uonom_Activeeventkey = cgiGet( "DDO_FUNCAO_UONOM_Activeeventkey");
            Ddo_funcao_uonom_Filteredtext_get = cgiGet( "DDO_FUNCAO_UONOM_Filteredtext_get");
            Ddo_funcao_uonom_Selectedvalue_get = cgiGet( "DDO_FUNCAO_UONOM_Selectedvalue_get");
            Ddo_funcao_ativo_Activeeventkey = cgiGet( "DDO_FUNCAO_ATIVO_Activeeventkey");
            Ddo_funcao_ativo_Selectedvalue_get = cgiGet( "DDO_FUNCAO_ATIVO_Selectedvalue_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vFUNCAO_NOME1"), AV16Funcao_Nome1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vFUNCAO_UONOM1"), AV29Funcao_UONom1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV18DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vFUNCAO_NOME2"), AV19Funcao_Nome2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vFUNCAO_UONOM2"), AV31Funcao_UONom2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV21DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vFUNCAO_NOME3"), AV22Funcao_Nome3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vFUNCAO_UONOM3"), AV33Funcao_UONom3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV17DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV20DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFFUNCAO_NOME"), AV37TFFuncao_Nome) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFFUNCAO_NOME_SEL"), AV38TFFuncao_Nome_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFGRUPOFUNCAO_CODIGO"), ",", ".") != Convert.ToDecimal( AV41TFGrupoFuncao_Codigo )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFGRUPOFUNCAO_CODIGO_TO"), ",", ".") != Convert.ToDecimal( AV42TFGrupoFuncao_Codigo_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFFUNCAO_UONOM"), AV45TFFuncao_UONom) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFFUNCAO_UONOM_SEL"), AV46TFFuncao_UONom_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFFUNCAO_ATIVO_SEL"), ",", ".") != Convert.ToDecimal( AV49TFFuncao_Ativo_Sel )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E27CI2 */
         E27CI2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E27CI2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV15DynamicFiltersSelector1 = "FUNCAO_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV18DynamicFiltersSelector2 = "FUNCAO_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersSelector2", AV18DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV21DynamicFiltersSelector3 = "FUNCAO_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector3", AV21DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         edtavTffuncao_nome_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTffuncao_nome_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncao_nome_Visible), 5, 0)));
         edtavTffuncao_nome_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTffuncao_nome_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncao_nome_sel_Visible), 5, 0)));
         edtavTfgrupofuncao_codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfgrupofuncao_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfgrupofuncao_codigo_Visible), 5, 0)));
         edtavTfgrupofuncao_codigo_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfgrupofuncao_codigo_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfgrupofuncao_codigo_to_Visible), 5, 0)));
         edtavTffuncao_uonom_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTffuncao_uonom_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncao_uonom_Visible), 5, 0)));
         edtavTffuncao_uonom_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTffuncao_uonom_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncao_uonom_sel_Visible), 5, 0)));
         edtavTffuncao_ativo_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTffuncao_ativo_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncao_ativo_sel_Visible), 5, 0)));
         Ddo_funcao_nome_Titlecontrolidtoreplace = subGrid_Internalname+"_Funcao_Nome";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcao_nome_Internalname, "TitleControlIdToReplace", Ddo_funcao_nome_Titlecontrolidtoreplace);
         AV39ddo_Funcao_NomeTitleControlIdToReplace = Ddo_funcao_nome_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39ddo_Funcao_NomeTitleControlIdToReplace", AV39ddo_Funcao_NomeTitleControlIdToReplace);
         edtavDdo_funcao_nometitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_funcao_nometitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_funcao_nometitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_grupofuncao_codigo_Titlecontrolidtoreplace = subGrid_Internalname+"_GrupoFuncao_Codigo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_grupofuncao_codigo_Internalname, "TitleControlIdToReplace", Ddo_grupofuncao_codigo_Titlecontrolidtoreplace);
         AV43ddo_GrupoFuncao_CodigoTitleControlIdToReplace = Ddo_grupofuncao_codigo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43ddo_GrupoFuncao_CodigoTitleControlIdToReplace", AV43ddo_GrupoFuncao_CodigoTitleControlIdToReplace);
         edtavDdo_grupofuncao_codigotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_grupofuncao_codigotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_grupofuncao_codigotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_funcao_uonom_Titlecontrolidtoreplace = subGrid_Internalname+"_Funcao_UONom";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcao_uonom_Internalname, "TitleControlIdToReplace", Ddo_funcao_uonom_Titlecontrolidtoreplace);
         AV47ddo_Funcao_UONomTitleControlIdToReplace = Ddo_funcao_uonom_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47ddo_Funcao_UONomTitleControlIdToReplace", AV47ddo_Funcao_UONomTitleControlIdToReplace);
         edtavDdo_funcao_uonomtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_funcao_uonomtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_funcao_uonomtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_funcao_ativo_Titlecontrolidtoreplace = subGrid_Internalname+"_Funcao_Ativo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcao_ativo_Internalname, "TitleControlIdToReplace", Ddo_funcao_ativo_Titlecontrolidtoreplace);
         AV50ddo_Funcao_AtivoTitleControlIdToReplace = Ddo_funcao_ativo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50ddo_Funcao_AtivoTitleControlIdToReplace", AV50ddo_Funcao_AtivoTitleControlIdToReplace);
         edtavDdo_funcao_ativotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_funcao_ativotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_funcao_ativotitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = " Funcao";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Fun��o", 0);
         cmbavOrderedby.addItem("2", "Grupo", 0);
         cmbavOrderedby.addItem("3", "Unidade Organizacional", 0);
         cmbavOrderedby.addItem("4", "Ativa?", 0);
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV51DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV51DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E28CI2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV36Funcao_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV40GrupoFuncao_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV44Funcao_UONomTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV48Funcao_AtivoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'CHECKSECURITYFORACTIONS' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtFuncao_Nome_Titleformat = 2;
         edtFuncao_Nome_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Nome", AV39ddo_Funcao_NomeTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncao_Nome_Internalname, "Title", edtFuncao_Nome_Title);
         edtGrupoFuncao_Codigo_Titleformat = 2;
         edtGrupoFuncao_Codigo_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Grupo", AV43ddo_GrupoFuncao_CodigoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtGrupoFuncao_Codigo_Internalname, "Title", edtGrupoFuncao_Codigo_Title);
         edtFuncao_UONom_Titleformat = 2;
         edtFuncao_UONom_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Unidade Organizacional", AV47ddo_Funcao_UONomTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncao_UONom_Internalname, "Title", edtFuncao_UONom_Title);
         chkFuncao_Ativo_Titleformat = 2;
         chkFuncao_Ativo.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Ativa?", AV50ddo_Funcao_AtivoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkFuncao_Ativo_Internalname, "Title", chkFuncao_Ativo.Title.Text);
         AV53GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV53GridCurrentPage), 10, 0)));
         AV54GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV54GridPageCount), 10, 0)));
         AV57WWGeral_FuncaoDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV58WWGeral_FuncaoDS_2_Funcao_nome1 = AV16Funcao_Nome1;
         AV59WWGeral_FuncaoDS_3_Funcao_uonom1 = AV29Funcao_UONom1;
         AV60WWGeral_FuncaoDS_4_Dynamicfiltersenabled2 = AV17DynamicFiltersEnabled2;
         AV61WWGeral_FuncaoDS_5_Dynamicfiltersselector2 = AV18DynamicFiltersSelector2;
         AV62WWGeral_FuncaoDS_6_Funcao_nome2 = AV19Funcao_Nome2;
         AV63WWGeral_FuncaoDS_7_Funcao_uonom2 = AV31Funcao_UONom2;
         AV64WWGeral_FuncaoDS_8_Dynamicfiltersenabled3 = AV20DynamicFiltersEnabled3;
         AV65WWGeral_FuncaoDS_9_Dynamicfiltersselector3 = AV21DynamicFiltersSelector3;
         AV66WWGeral_FuncaoDS_10_Funcao_nome3 = AV22Funcao_Nome3;
         AV67WWGeral_FuncaoDS_11_Funcao_uonom3 = AV33Funcao_UONom3;
         AV68WWGeral_FuncaoDS_12_Tffuncao_nome = AV37TFFuncao_Nome;
         AV69WWGeral_FuncaoDS_13_Tffuncao_nome_sel = AV38TFFuncao_Nome_Sel;
         AV70WWGeral_FuncaoDS_14_Tfgrupofuncao_codigo = AV41TFGrupoFuncao_Codigo;
         AV71WWGeral_FuncaoDS_15_Tfgrupofuncao_codigo_to = AV42TFGrupoFuncao_Codigo_To;
         AV72WWGeral_FuncaoDS_16_Tffuncao_uonom = AV45TFFuncao_UONom;
         AV73WWGeral_FuncaoDS_17_Tffuncao_uonom_sel = AV46TFFuncao_UONom_Sel;
         AV74WWGeral_FuncaoDS_18_Tffuncao_ativo_sel = AV49TFFuncao_Ativo_Sel;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV36Funcao_NomeTitleFilterData", AV36Funcao_NomeTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV40GrupoFuncao_CodigoTitleFilterData", AV40GrupoFuncao_CodigoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV44Funcao_UONomTitleFilterData", AV44Funcao_UONomTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV48Funcao_AtivoTitleFilterData", AV48Funcao_AtivoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV6WWPContext", AV6WWPContext);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void E11CI2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV52PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV52PageToGo) ;
         }
      }

      protected void E12CI2( )
      {
         /* Ddo_funcao_nome_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_funcao_nome_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_funcao_nome_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcao_nome_Internalname, "SortedStatus", Ddo_funcao_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_funcao_nome_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_funcao_nome_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcao_nome_Internalname, "SortedStatus", Ddo_funcao_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_funcao_nome_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV37TFFuncao_Nome = Ddo_funcao_nome_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37TFFuncao_Nome", AV37TFFuncao_Nome);
            AV38TFFuncao_Nome_Sel = Ddo_funcao_nome_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFFuncao_Nome_Sel", AV38TFFuncao_Nome_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E13CI2( )
      {
         /* Ddo_grupofuncao_codigo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_grupofuncao_codigo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_grupofuncao_codigo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_grupofuncao_codigo_Internalname, "SortedStatus", Ddo_grupofuncao_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_grupofuncao_codigo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_grupofuncao_codigo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_grupofuncao_codigo_Internalname, "SortedStatus", Ddo_grupofuncao_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_grupofuncao_codigo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV41TFGrupoFuncao_Codigo = (int)(NumberUtil.Val( Ddo_grupofuncao_codigo_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41TFGrupoFuncao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV41TFGrupoFuncao_Codigo), 6, 0)));
            AV42TFGrupoFuncao_Codigo_To = (int)(NumberUtil.Val( Ddo_grupofuncao_codigo_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42TFGrupoFuncao_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV42TFGrupoFuncao_Codigo_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E14CI2( )
      {
         /* Ddo_funcao_uonom_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_funcao_uonom_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_funcao_uonom_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcao_uonom_Internalname, "SortedStatus", Ddo_funcao_uonom_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_funcao_uonom_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_funcao_uonom_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcao_uonom_Internalname, "SortedStatus", Ddo_funcao_uonom_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_funcao_uonom_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV45TFFuncao_UONom = Ddo_funcao_uonom_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFFuncao_UONom", AV45TFFuncao_UONom);
            AV46TFFuncao_UONom_Sel = Ddo_funcao_uonom_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFFuncao_UONom_Sel", AV46TFFuncao_UONom_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E15CI2( )
      {
         /* Ddo_funcao_ativo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_funcao_ativo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_funcao_ativo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcao_ativo_Internalname, "SortedStatus", Ddo_funcao_ativo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_funcao_ativo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_funcao_ativo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcao_ativo_Internalname, "SortedStatus", Ddo_funcao_ativo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_funcao_ativo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV49TFFuncao_Ativo_Sel = (short)(NumberUtil.Val( Ddo_funcao_ativo_Selectedvalue_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFFuncao_Ativo_Sel", StringUtil.Str( (decimal)(AV49TFFuncao_Ativo_Sel), 1, 0));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      private void E29CI2( )
      {
         /* Grid_Load Routine */
         edtavUpdate_Tooltiptext = "Modifica";
         if ( ( AV6WWPContext.gxTpr_Insert && AV6WWPContext.gxTpr_Update && AV6WWPContext.gxTpr_Delete ) || AV6WWPContext.gxTpr_Userehadministradorgam )
         {
            edtavUpdate_Link = formatLink("geral_funcao.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A621Funcao_Codigo);
            AV25Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUpdate_Internalname, AV25Update);
            AV75Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
            edtavUpdate_Enabled = 1;
         }
         else
         {
            edtavUpdate_Link = "";
            AV25Update = context.GetImagePath( "0211baea-a1e2-4bb5-849c-f3afb69b09ee", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUpdate_Internalname, AV25Update);
            AV75Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "0211baea-a1e2-4bb5-849c-f3afb69b09ee", "", context.GetTheme( )));
            edtavUpdate_Enabled = 0;
         }
         edtavDelete_Tooltiptext = "Eliminar";
         if ( ( AV6WWPContext.gxTpr_Insert && AV6WWPContext.gxTpr_Update && AV6WWPContext.gxTpr_Delete ) || AV6WWPContext.gxTpr_Userehadministradorgam )
         {
            edtavDelete_Link = formatLink("geral_funcao.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A621Funcao_Codigo);
            AV26Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDelete_Internalname, AV26Delete);
            AV76Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
            edtavDelete_Enabled = 1;
         }
         else
         {
            edtavDelete_Link = "";
            AV26Delete = context.GetImagePath( "138fdfa8-ae9b-4664-8007-e04d47468418", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDelete_Internalname, AV26Delete);
            AV76Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "138fdfa8-ae9b-4664-8007-e04d47468418", "", context.GetTheme( )));
            edtavDelete_Enabled = 0;
         }
         edtFuncao_Nome_Link = formatLink("viewgeral_funcao.aspx") + "?" + UrlEncode("" +A621Funcao_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         edtFuncao_UONom_Link = formatLink("viewgeral_unidadeorganizacional.aspx") + "?" + UrlEncode("" +A635Funcao_UOCod) + "," + UrlEncode(StringUtil.RTrim(""));
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 76;
         }
         sendrow_762( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_76_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(76, GridRow);
         }
      }

      protected void E16CI2( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E22CI2( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV17DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersEnabled2", AV17DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
      }

      protected void E17CI2( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV23DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersRemoving", AV23DynamicFiltersRemoving);
         AV24DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersIgnoreFirst", AV24DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV23DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersRemoving", AV23DynamicFiltersRemoving);
         AV24DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersIgnoreFirst", AV24DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16Funcao_Nome1, AV29Funcao_UONom1, AV18DynamicFiltersSelector2, AV19Funcao_Nome2, AV31Funcao_UONom2, AV21DynamicFiltersSelector3, AV22Funcao_Nome3, AV33Funcao_UONom3, AV17DynamicFiltersEnabled2, AV20DynamicFiltersEnabled3, AV37TFFuncao_Nome, AV38TFFuncao_Nome_Sel, AV41TFGrupoFuncao_Codigo, AV42TFGrupoFuncao_Codigo_To, AV45TFFuncao_UONom, AV46TFFuncao_UONom_Sel, AV49TFFuncao_Ativo_Sel, AV39ddo_Funcao_NomeTitleControlIdToReplace, AV43ddo_GrupoFuncao_CodigoTitleControlIdToReplace, AV47ddo_Funcao_UONomTitleControlIdToReplace, AV50ddo_Funcao_AtivoTitleControlIdToReplace, AV6WWPContext, AV77Pgmname, AV10GridState, AV24DynamicFiltersIgnoreFirst, AV23DynamicFiltersRemoving, A621Funcao_Codigo, A635Funcao_UOCod) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV18DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E23CI2( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E24CI2( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV20DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled3", AV20DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
      }

      protected void E18CI2( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV23DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersRemoving", AV23DynamicFiltersRemoving);
         AV17DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersEnabled2", AV17DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV23DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersRemoving", AV23DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16Funcao_Nome1, AV29Funcao_UONom1, AV18DynamicFiltersSelector2, AV19Funcao_Nome2, AV31Funcao_UONom2, AV21DynamicFiltersSelector3, AV22Funcao_Nome3, AV33Funcao_UONom3, AV17DynamicFiltersEnabled2, AV20DynamicFiltersEnabled3, AV37TFFuncao_Nome, AV38TFFuncao_Nome_Sel, AV41TFGrupoFuncao_Codigo, AV42TFGrupoFuncao_Codigo_To, AV45TFFuncao_UONom, AV46TFFuncao_UONom_Sel, AV49TFFuncao_Ativo_Sel, AV39ddo_Funcao_NomeTitleControlIdToReplace, AV43ddo_GrupoFuncao_CodigoTitleControlIdToReplace, AV47ddo_Funcao_UONomTitleControlIdToReplace, AV50ddo_Funcao_AtivoTitleControlIdToReplace, AV6WWPContext, AV77Pgmname, AV10GridState, AV24DynamicFiltersIgnoreFirst, AV23DynamicFiltersRemoving, A621Funcao_Codigo, A635Funcao_UOCod) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV18DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E25CI2( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E19CI2( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV23DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersRemoving", AV23DynamicFiltersRemoving);
         AV20DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled3", AV20DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV23DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersRemoving", AV23DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16Funcao_Nome1, AV29Funcao_UONom1, AV18DynamicFiltersSelector2, AV19Funcao_Nome2, AV31Funcao_UONom2, AV21DynamicFiltersSelector3, AV22Funcao_Nome3, AV33Funcao_UONom3, AV17DynamicFiltersEnabled2, AV20DynamicFiltersEnabled3, AV37TFFuncao_Nome, AV38TFFuncao_Nome_Sel, AV41TFGrupoFuncao_Codigo, AV42TFGrupoFuncao_Codigo_To, AV45TFFuncao_UONom, AV46TFFuncao_UONom_Sel, AV49TFFuncao_Ativo_Sel, AV39ddo_Funcao_NomeTitleControlIdToReplace, AV43ddo_GrupoFuncao_CodigoTitleControlIdToReplace, AV47ddo_Funcao_UONomTitleControlIdToReplace, AV50ddo_Funcao_AtivoTitleControlIdToReplace, AV6WWPContext, AV77Pgmname, AV10GridState, AV24DynamicFiltersIgnoreFirst, AV23DynamicFiltersRemoving, A621Funcao_Codigo, A635Funcao_UOCod) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV18DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E26CI2( )
      {
         /* Dynamicfiltersselector3_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E20CI2( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S232 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefresh();
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV18DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
      }

      protected void E21CI2( )
      {
         /* 'DoInsert' Routine */
         context.wjLoc = formatLink("geral_funcao.aspx") + "?" + UrlEncode(StringUtil.RTrim("INS")) + "," + UrlEncode("" +0);
         context.wjLocDisableFrm = 1;
      }

      protected void S192( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_funcao_nome_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcao_nome_Internalname, "SortedStatus", Ddo_funcao_nome_Sortedstatus);
         Ddo_grupofuncao_codigo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_grupofuncao_codigo_Internalname, "SortedStatus", Ddo_grupofuncao_codigo_Sortedstatus);
         Ddo_funcao_uonom_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcao_uonom_Internalname, "SortedStatus", Ddo_funcao_uonom_Sortedstatus);
         Ddo_funcao_ativo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcao_ativo_Internalname, "SortedStatus", Ddo_funcao_ativo_Sortedstatus);
      }

      protected void S162( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 1 )
         {
            Ddo_funcao_nome_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcao_nome_Internalname, "SortedStatus", Ddo_funcao_nome_Sortedstatus);
         }
         else if ( AV13OrderedBy == 2 )
         {
            Ddo_grupofuncao_codigo_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_grupofuncao_codigo_Internalname, "SortedStatus", Ddo_grupofuncao_codigo_Sortedstatus);
         }
         else if ( AV13OrderedBy == 3 )
         {
            Ddo_funcao_uonom_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcao_uonom_Internalname, "SortedStatus", Ddo_funcao_uonom_Sortedstatus);
         }
         else if ( AV13OrderedBy == 4 )
         {
            Ddo_funcao_ativo_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcao_ativo_Internalname, "SortedStatus", Ddo_funcao_ativo_Sortedstatus);
         }
      }

      protected void S172( )
      {
         /* 'CHECKSECURITYFORACTIONS' Routine */
         if ( ! ( ( AV6WWPContext.gxTpr_Insert && AV6WWPContext.gxTpr_Update && AV6WWPContext.gxTpr_Delete ) || AV6WWPContext.gxTpr_Userehadministradorgam ) )
         {
            imgInsert_Link = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgInsert_Internalname, "Link", imgInsert_Link);
            imgInsert_Bitmap = context.GetImagePath( "13d28f37-c579-4dd9-a404-ba51ab71d1e3", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgInsert_Internalname, "Bitmap", context.convertURL( context.PathToRelativeUrl( imgInsert_Bitmap)));
            imgInsert_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgInsert_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgInsert_Enabled), 5, 0)));
         }
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavFuncao_nome1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncao_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncao_nome1_Visible), 5, 0)));
         edtavFuncao_uonom1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncao_uonom1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncao_uonom1_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "FUNCAO_NOME") == 0 )
         {
            edtavFuncao_nome1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncao_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncao_nome1_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "FUNCAO_UONOM") == 0 )
         {
            edtavFuncao_uonom1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncao_uonom1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncao_uonom1_Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         edtavFuncao_nome2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncao_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncao_nome2_Visible), 5, 0)));
         edtavFuncao_uonom2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncao_uonom2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncao_uonom2_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV18DynamicFiltersSelector2, "FUNCAO_NOME") == 0 )
         {
            edtavFuncao_nome2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncao_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncao_nome2_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV18DynamicFiltersSelector2, "FUNCAO_UONOM") == 0 )
         {
            edtavFuncao_uonom2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncao_uonom2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncao_uonom2_Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         edtavFuncao_nome3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncao_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncao_nome3_Visible), 5, 0)));
         edtavFuncao_uonom3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncao_uonom3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncao_uonom3_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV21DynamicFiltersSelector3, "FUNCAO_NOME") == 0 )
         {
            edtavFuncao_nome3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncao_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncao_nome3_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector3, "FUNCAO_UONOM") == 0 )
         {
            edtavFuncao_uonom3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncao_uonom3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncao_uonom3_Visible), 5, 0)));
         }
      }

      protected void S212( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV17DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersEnabled2", AV17DynamicFiltersEnabled2);
         AV18DynamicFiltersSelector2 = "FUNCAO_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersSelector2", AV18DynamicFiltersSelector2);
         AV19Funcao_Nome2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Funcao_Nome2", AV19Funcao_Nome2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV20DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled3", AV20DynamicFiltersEnabled3);
         AV21DynamicFiltersSelector3 = "FUNCAO_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector3", AV21DynamicFiltersSelector3);
         AV22Funcao_Nome3 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Funcao_Nome3", AV22Funcao_Nome3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S232( )
      {
         /* 'CLEANFILTERS' Routine */
         AV37TFFuncao_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37TFFuncao_Nome", AV37TFFuncao_Nome);
         Ddo_funcao_nome_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcao_nome_Internalname, "FilteredText_set", Ddo_funcao_nome_Filteredtext_set);
         AV38TFFuncao_Nome_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFFuncao_Nome_Sel", AV38TFFuncao_Nome_Sel);
         Ddo_funcao_nome_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcao_nome_Internalname, "SelectedValue_set", Ddo_funcao_nome_Selectedvalue_set);
         AV41TFGrupoFuncao_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41TFGrupoFuncao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV41TFGrupoFuncao_Codigo), 6, 0)));
         Ddo_grupofuncao_codigo_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_grupofuncao_codigo_Internalname, "FilteredText_set", Ddo_grupofuncao_codigo_Filteredtext_set);
         AV42TFGrupoFuncao_Codigo_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42TFGrupoFuncao_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV42TFGrupoFuncao_Codigo_To), 6, 0)));
         Ddo_grupofuncao_codigo_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_grupofuncao_codigo_Internalname, "FilteredTextTo_set", Ddo_grupofuncao_codigo_Filteredtextto_set);
         AV45TFFuncao_UONom = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFFuncao_UONom", AV45TFFuncao_UONom);
         Ddo_funcao_uonom_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcao_uonom_Internalname, "FilteredText_set", Ddo_funcao_uonom_Filteredtext_set);
         AV46TFFuncao_UONom_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFFuncao_UONom_Sel", AV46TFFuncao_UONom_Sel);
         Ddo_funcao_uonom_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcao_uonom_Internalname, "SelectedValue_set", Ddo_funcao_uonom_Selectedvalue_set);
         AV49TFFuncao_Ativo_Sel = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFFuncao_Ativo_Sel", StringUtil.Str( (decimal)(AV49TFFuncao_Ativo_Sel), 1, 0));
         Ddo_funcao_ativo_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcao_ativo_Internalname, "SelectedValue_set", Ddo_funcao_ativo_Selectedvalue_set);
         AV15DynamicFiltersSelector1 = "FUNCAO_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         AV16Funcao_Nome1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16Funcao_Nome1", AV16Funcao_Nome1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S152( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV27Session.Get(AV77Pgmname+"GridState"), "") == 0 )
         {
            AV10GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV77Pgmname+"GridState"), "");
         }
         else
         {
            AV10GridState.FromXml(AV27Session.Get(AV77Pgmname+"GridState"), "");
         }
         AV13OrderedBy = AV10GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         AV14OrderedDsc = AV10GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADREGFILTERSSTATE' */
         S242 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_gotopage( AV10GridState.gxTpr_Currentpage) ;
      }

      protected void S242( )
      {
         /* 'LOADREGFILTERSSTATE' Routine */
         AV78GXV1 = 1;
         while ( AV78GXV1 <= AV10GridState.gxTpr_Filtervalues.Count )
         {
            AV11GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV10GridState.gxTpr_Filtervalues.Item(AV78GXV1));
            if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFFUNCAO_NOME") == 0 )
            {
               AV37TFFuncao_Nome = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37TFFuncao_Nome", AV37TFFuncao_Nome);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV37TFFuncao_Nome)) )
               {
                  Ddo_funcao_nome_Filteredtext_set = AV37TFFuncao_Nome;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcao_nome_Internalname, "FilteredText_set", Ddo_funcao_nome_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFFUNCAO_NOME_SEL") == 0 )
            {
               AV38TFFuncao_Nome_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFFuncao_Nome_Sel", AV38TFFuncao_Nome_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV38TFFuncao_Nome_Sel)) )
               {
                  Ddo_funcao_nome_Selectedvalue_set = AV38TFFuncao_Nome_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcao_nome_Internalname, "SelectedValue_set", Ddo_funcao_nome_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFGRUPOFUNCAO_CODIGO") == 0 )
            {
               AV41TFGrupoFuncao_Codigo = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41TFGrupoFuncao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV41TFGrupoFuncao_Codigo), 6, 0)));
               AV42TFGrupoFuncao_Codigo_To = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Valueto, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42TFGrupoFuncao_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV42TFGrupoFuncao_Codigo_To), 6, 0)));
               if ( ! (0==AV41TFGrupoFuncao_Codigo) )
               {
                  Ddo_grupofuncao_codigo_Filteredtext_set = StringUtil.Str( (decimal)(AV41TFGrupoFuncao_Codigo), 6, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_grupofuncao_codigo_Internalname, "FilteredText_set", Ddo_grupofuncao_codigo_Filteredtext_set);
               }
               if ( ! (0==AV42TFGrupoFuncao_Codigo_To) )
               {
                  Ddo_grupofuncao_codigo_Filteredtextto_set = StringUtil.Str( (decimal)(AV42TFGrupoFuncao_Codigo_To), 6, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_grupofuncao_codigo_Internalname, "FilteredTextTo_set", Ddo_grupofuncao_codigo_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFFUNCAO_UONOM") == 0 )
            {
               AV45TFFuncao_UONom = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFFuncao_UONom", AV45TFFuncao_UONom);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV45TFFuncao_UONom)) )
               {
                  Ddo_funcao_uonom_Filteredtext_set = AV45TFFuncao_UONom;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcao_uonom_Internalname, "FilteredText_set", Ddo_funcao_uonom_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFFUNCAO_UONOM_SEL") == 0 )
            {
               AV46TFFuncao_UONom_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFFuncao_UONom_Sel", AV46TFFuncao_UONom_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV46TFFuncao_UONom_Sel)) )
               {
                  Ddo_funcao_uonom_Selectedvalue_set = AV46TFFuncao_UONom_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcao_uonom_Internalname, "SelectedValue_set", Ddo_funcao_uonom_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFFUNCAO_ATIVO_SEL") == 0 )
            {
               AV49TFFuncao_Ativo_Sel = (short)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFFuncao_Ativo_Sel", StringUtil.Str( (decimal)(AV49TFFuncao_Ativo_Sel), 1, 0));
               if ( ! (0==AV49TFFuncao_Ativo_Sel) )
               {
                  Ddo_funcao_ativo_Selectedvalue_set = StringUtil.Str( (decimal)(AV49TFFuncao_Ativo_Sel), 1, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcao_ativo_Internalname, "SelectedValue_set", Ddo_funcao_ativo_Selectedvalue_set);
               }
            }
            AV78GXV1 = (int)(AV78GXV1+1);
         }
      }

      protected void S222( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV15DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "FUNCAO_NOME") == 0 )
            {
               AV16Funcao_Nome1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16Funcao_Nome1", AV16Funcao_Nome1);
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "FUNCAO_UONOM") == 0 )
            {
               AV29Funcao_UONom1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29Funcao_UONom1", AV29Funcao_UONom1);
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV17DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersEnabled2", AV17DynamicFiltersEnabled2);
               AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(2));
               AV18DynamicFiltersSelector2 = AV12GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersSelector2", AV18DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV18DynamicFiltersSelector2, "FUNCAO_NOME") == 0 )
               {
                  AV19Funcao_Nome2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Funcao_Nome2", AV19Funcao_Nome2);
               }
               else if ( StringUtil.StrCmp(AV18DynamicFiltersSelector2, "FUNCAO_UONOM") == 0 )
               {
                  AV31Funcao_UONom2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31Funcao_UONom2", AV31Funcao_UONom2);
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV20DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled3", AV20DynamicFiltersEnabled3);
                  AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(3));
                  AV21DynamicFiltersSelector3 = AV12GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector3", AV21DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV21DynamicFiltersSelector3, "FUNCAO_NOME") == 0 )
                  {
                     AV22Funcao_Nome3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Funcao_Nome3", AV22Funcao_Nome3);
                  }
                  else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector3, "FUNCAO_UONOM") == 0 )
                  {
                     AV33Funcao_UONom3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33Funcao_UONom3", AV33Funcao_UONom3);
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S132 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV23DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S182( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV10GridState.FromXml(AV27Session.Get(AV77Pgmname+"GridState"), "");
         AV10GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV10GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV10GridState.gxTpr_Filtervalues.Clear();
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV37TFFuncao_Nome)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFFUNCAO_NOME";
            AV11GridStateFilterValue.gxTpr_Value = AV37TFFuncao_Nome;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV38TFFuncao_Nome_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFFUNCAO_NOME_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV38TFFuncao_Nome_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (0==AV41TFGrupoFuncao_Codigo) && (0==AV42TFGrupoFuncao_Codigo_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFGRUPOFUNCAO_CODIGO";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV41TFGrupoFuncao_Codigo), 6, 0);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV42TFGrupoFuncao_Codigo_To), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV45TFFuncao_UONom)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFFUNCAO_UONOM";
            AV11GridStateFilterValue.gxTpr_Value = AV45TFFuncao_UONom;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV46TFFuncao_UONom_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFFUNCAO_UONOM_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV46TFFuncao_UONom_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! (0==AV49TFFuncao_Ativo_Sel) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFFUNCAO_ATIVO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV49TFFuncao_Ativo_Sel), 1, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Currentpage = (short)(subGrid_Currentpage( ));
         new wwpbaseobjects.savegridstate(context ).execute(  AV77Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_MeetrikaVs3")) ;
      }

      protected void S202( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV24DynamicFiltersIgnoreFirst )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV15DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "FUNCAO_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV16Funcao_Nome1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV16Funcao_Nome1;
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "FUNCAO_UONOM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV29Funcao_UONom1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV29Funcao_UONom1;
            }
            if ( AV23DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV17DynamicFiltersEnabled2 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV18DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV18DynamicFiltersSelector2, "FUNCAO_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV19Funcao_Nome2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV19Funcao_Nome2;
            }
            else if ( ( StringUtil.StrCmp(AV18DynamicFiltersSelector2, "FUNCAO_UONOM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV31Funcao_UONom2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV31Funcao_UONom2;
            }
            if ( AV23DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV20DynamicFiltersEnabled3 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV21DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector3, "FUNCAO_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV22Funcao_Nome3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV22Funcao_Nome3;
            }
            else if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector3, "FUNCAO_UONOM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV33Funcao_UONom3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV33Funcao_UONom3;
            }
            if ( AV23DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
      }

      protected void S142( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV77Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = true;
         AV8TrnContext.gxTpr_Callerurl = AV7HTTPRequest.ScriptName+"?"+AV7HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "Geral_Funcao";
         AV27Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_MeetrikaVs3"));
      }

      protected void wb_table1_2_CI2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_CI2( true) ;
         }
         else
         {
            wb_table2_8_CI2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_CI2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_70_CI2( true) ;
         }
         else
         {
            wb_table3_70_CI2( false) ;
         }
         return  ;
      }

      protected void wb_table3_70_CI2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_CI2e( true) ;
         }
         else
         {
            wb_table1_2_CI2e( false) ;
         }
      }

      protected void wb_table3_70_CI2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_73_CI2( true) ;
         }
         else
         {
            wb_table4_73_CI2( false) ;
         }
         return  ;
      }

      protected void wb_table4_73_CI2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_70_CI2e( true) ;
         }
         else
         {
            wb_table3_70_CI2e( false) ;
         }
      }

      protected void wb_table4_73_CI2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"76\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Fun��o") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(410), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtFuncao_Nome_Titleformat == 0 )
               {
                  context.SendWebValue( edtFuncao_Nome_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtFuncao_Nome_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtGrupoFuncao_Codigo_Titleformat == 0 )
               {
                  context.SendWebValue( edtGrupoFuncao_Codigo_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtGrupoFuncao_Codigo_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Unidade Organizacional") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtFuncao_UONom_Titleformat == 0 )
               {
                  context.SendWebValue( edtFuncao_UONom_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtFuncao_UONom_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( chkFuncao_Ativo_Titleformat == 0 )
               {
                  context.SendWebValue( chkFuncao_Ativo.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( chkFuncao_Ativo.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV25Update));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavUpdate_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavUpdate_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV26Delete));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDelete_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDelete_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A621Funcao_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A622Funcao_Nome);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtFuncao_Nome_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtFuncao_Nome_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtFuncao_Nome_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A619GrupoFuncao_Codigo), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtGrupoFuncao_Codigo_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtGrupoFuncao_Codigo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A635Funcao_UOCod), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A636Funcao_UONom));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtFuncao_UONom_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtFuncao_UONom_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtFuncao_UONom_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.BoolToStr( A630Funcao_Ativo));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( chkFuncao_Ativo.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(chkFuncao_Ativo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 76 )
         {
            wbEnd = 0;
            nRC_GXsfl_76 = (short)(nGXsfl_76_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_73_CI2e( true) ;
         }
         else
         {
            wb_table4_73_CI2e( false) ;
         }
      }

      protected void wb_table2_8_CI2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableheader_Internalname, tblTableheader_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='GridHeaderCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblGeral_funcaotitle_Internalname, "Fun��es", "", "", lblGeral_funcaotitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WWGeral_Funcao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table5_13_CI2( true) ;
         }
         else
         {
            wb_table5_13_CI2( false) ;
         }
         return  ;
      }

      protected void wb_table5_13_CI2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_WWGeral_Funcao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'" + sGXsfl_76_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,20);\"", "", true, "HLP_WWGeral_Funcao.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'" + sGXsfl_76_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,21);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_WWGeral_Funcao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table6_23_CI2( true) ;
         }
         else
         {
            wb_table6_23_CI2( false) ;
         }
         return  ;
      }

      protected void wb_table6_23_CI2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_CI2e( true) ;
         }
         else
         {
            wb_table2_8_CI2e( false) ;
         }
      }

      protected void wb_table6_23_CI2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 26,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWGeral_Funcao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_28_CI2( true) ;
         }
         else
         {
            wb_table7_28_CI2( false) ;
         }
         return  ;
      }

      protected void wb_table7_28_CI2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_WWGeral_Funcao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_23_CI2e( true) ;
         }
         else
         {
            wb_table6_23_CI2e( false) ;
         }
      }

      protected void wb_table7_28_CI2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWGeral_Funcao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'" + sGXsfl_76_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV15DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,33);\"", "", true, "HLP_WWGeral_Funcao.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWGeral_Funcao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 37,'',false,'" + sGXsfl_76_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavFuncao_nome1_Internalname, AV16Funcao_Nome1, StringUtil.RTrim( context.localUtil.Format( AV16Funcao_Nome1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,37);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavFuncao_nome1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavFuncao_nome1_Visible, 1, 0, "text", "", 380, "px", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWGeral_Funcao.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'',false,'" + sGXsfl_76_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavFuncao_uonom1_Internalname, StringUtil.RTrim( AV29Funcao_UONom1), StringUtil.RTrim( context.localUtil.Format( AV29Funcao_UONom1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,38);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavFuncao_uonom1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavFuncao_uonom1_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWGeral_Funcao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWGeral_Funcao.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 41,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWGeral_Funcao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWGeral_Funcao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'',false,'" + sGXsfl_76_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV18DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,46);\"", "", true, "HLP_WWGeral_Funcao.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV18DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWGeral_Funcao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 50,'',false,'" + sGXsfl_76_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavFuncao_nome2_Internalname, AV19Funcao_Nome2, StringUtil.RTrim( context.localUtil.Format( AV19Funcao_Nome2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,50);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavFuncao_nome2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavFuncao_nome2_Visible, 1, 0, "text", "", 380, "px", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWGeral_Funcao.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 51,'',false,'" + sGXsfl_76_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavFuncao_uonom2_Internalname, StringUtil.RTrim( AV31Funcao_UONom2), StringUtil.RTrim( context.localUtil.Format( AV31Funcao_UONom2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,51);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavFuncao_uonom2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavFuncao_uonom2_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWGeral_Funcao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 53,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWGeral_Funcao.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWGeral_Funcao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWGeral_Funcao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'" + sGXsfl_76_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV21DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR3.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,59);\"", "", true, "HLP_WWGeral_Funcao.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWGeral_Funcao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 63,'',false,'" + sGXsfl_76_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavFuncao_nome3_Internalname, AV22Funcao_Nome3, StringUtil.RTrim( context.localUtil.Format( AV22Funcao_Nome3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,63);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavFuncao_nome3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavFuncao_nome3_Visible, 1, 0, "text", "", 380, "px", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWGeral_Funcao.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 64,'',false,'" + sGXsfl_76_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavFuncao_uonom3_Internalname, StringUtil.RTrim( AV33Funcao_UONom3), StringUtil.RTrim( context.localUtil.Format( AV33Funcao_UONom3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,64);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavFuncao_uonom3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavFuncao_uonom3_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWGeral_Funcao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 66,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWGeral_Funcao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_28_CI2e( true) ;
         }
         else
         {
            wb_table7_28_CI2e( false) ;
         }
      }

      protected void wb_table5_13_CI2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgInsert_Internalname, imgInsert_Bitmap, "", "", "", context.GetTheme( ), 1, imgInsert_Enabled, "", "Inserir", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgInsert_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOINSERT\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWGeral_Funcao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_13_CI2e( true) ;
         }
         else
         {
            wb_table5_13_CI2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PACI2( ) ;
         WSCI2( ) ;
         WECI2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20205299374183");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wwgeral_funcao.js", "?20205299374183");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_762( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_76_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_76_idx;
         edtFuncao_Codigo_Internalname = "FUNCAO_CODIGO_"+sGXsfl_76_idx;
         edtFuncao_Nome_Internalname = "FUNCAO_NOME_"+sGXsfl_76_idx;
         edtGrupoFuncao_Codigo_Internalname = "GRUPOFUNCAO_CODIGO_"+sGXsfl_76_idx;
         edtFuncao_UOCod_Internalname = "FUNCAO_UOCOD_"+sGXsfl_76_idx;
         edtFuncao_UONom_Internalname = "FUNCAO_UONOM_"+sGXsfl_76_idx;
         chkFuncao_Ativo_Internalname = "FUNCAO_ATIVO_"+sGXsfl_76_idx;
      }

      protected void SubsflControlProps_fel_762( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_76_fel_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_76_fel_idx;
         edtFuncao_Codigo_Internalname = "FUNCAO_CODIGO_"+sGXsfl_76_fel_idx;
         edtFuncao_Nome_Internalname = "FUNCAO_NOME_"+sGXsfl_76_fel_idx;
         edtGrupoFuncao_Codigo_Internalname = "GRUPOFUNCAO_CODIGO_"+sGXsfl_76_fel_idx;
         edtFuncao_UOCod_Internalname = "FUNCAO_UOCOD_"+sGXsfl_76_fel_idx;
         edtFuncao_UONom_Internalname = "FUNCAO_UONOM_"+sGXsfl_76_fel_idx;
         chkFuncao_Ativo_Internalname = "FUNCAO_ATIVO_"+sGXsfl_76_fel_idx;
      }

      protected void sendrow_762( )
      {
         SubsflControlProps_762( ) ;
         WBCI0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_76_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_76_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_76_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV25Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV25Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV75Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV25Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV25Update)) ? AV75Update_GXI : context.PathToRelativeUrl( AV25Update)),(String)edtavUpdate_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(int)edtavUpdate_Enabled,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV25Update_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV26Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV26Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV76Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV26Delete)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV26Delete)) ? AV76Delete_GXI : context.PathToRelativeUrl( AV26Delete)),(String)edtavDelete_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(int)edtavDelete_Enabled,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV26Delete_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFuncao_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A621Funcao_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A621Funcao_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtFuncao_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)76,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFuncao_Nome_Internalname,(String)A622Funcao_Nome,StringUtil.RTrim( context.localUtil.Format( A622Funcao_Nome, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)edtFuncao_Nome_Link,(String)"",(String)"",(String)"",(String)edtFuncao_Nome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)410,(String)"px",(short)17,(String)"px",(short)100,(short)0,(short)0,(short)76,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtGrupoFuncao_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A619GrupoFuncao_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A619GrupoFuncao_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtGrupoFuncao_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)76,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFuncao_UOCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A635Funcao_UOCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A635Funcao_UOCod), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtFuncao_UOCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)76,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFuncao_UONom_Internalname,StringUtil.RTrim( A636Funcao_UONom),StringUtil.RTrim( context.localUtil.Format( A636Funcao_UONom, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)edtFuncao_UONom_Link,(String)"",(String)"",(String)"",(String)edtFuncao_UONom_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)76,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Check box */
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GridRow.AddColumnProperties("checkbox", 1, isAjaxCallMode( ), new Object[] {(String)chkFuncao_Ativo_Internalname,StringUtil.BoolToStr( A630Funcao_Ativo),(String)"",(String)"",(short)-1,(short)0,(String)"true",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)""});
            GxWebStd.gx_hidden_field( context, "gxhash_FUNCAO_CODIGO"+"_"+sGXsfl_76_idx, GetSecureSignedToken( sGXsfl_76_idx, context.localUtil.Format( (decimal)(A621Funcao_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_FUNCAO_NOME"+"_"+sGXsfl_76_idx, GetSecureSignedToken( sGXsfl_76_idx, StringUtil.RTrim( context.localUtil.Format( A622Funcao_Nome, "@!"))));
            GxWebStd.gx_hidden_field( context, "gxhash_GRUPOFUNCAO_CODIGO"+"_"+sGXsfl_76_idx, GetSecureSignedToken( sGXsfl_76_idx, context.localUtil.Format( (decimal)(A619GrupoFuncao_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_FUNCAO_UOCOD"+"_"+sGXsfl_76_idx, GetSecureSignedToken( sGXsfl_76_idx, context.localUtil.Format( (decimal)(A635Funcao_UOCod), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_FUNCAO_ATIVO"+"_"+sGXsfl_76_idx, GetSecureSignedToken( sGXsfl_76_idx, A630Funcao_Ativo));
            GridContainer.AddRow(GridRow);
            nGXsfl_76_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_76_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_76_idx+1));
            sGXsfl_76_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_76_idx), 4, 0)), 4, "0");
            SubsflControlProps_762( ) ;
         }
         /* End function sendrow_762 */
      }

      protected void init_default_properties( )
      {
         lblGeral_funcaotitle_Internalname = "GERAL_FUNCAOTITLE";
         imgInsert_Internalname = "INSERT";
         tblTableactions_Internalname = "TABLEACTIONS";
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         imgCleanfilters_Internalname = "CLEANFILTERS";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         edtavFuncao_nome1_Internalname = "vFUNCAO_NOME1";
         edtavFuncao_uonom1_Internalname = "vFUNCAO_UONOM1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         edtavFuncao_nome2_Internalname = "vFUNCAO_NOME2";
         edtavFuncao_uonom2_Internalname = "vFUNCAO_UONOM2";
         imgAdddynamicfilters2_Internalname = "ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = "DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = "vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = "DYNAMICFILTERSMIDDLE3";
         edtavFuncao_nome3_Internalname = "vFUNCAO_NOME3";
         edtavFuncao_uonom3_Internalname = "vFUNCAO_UONOM3";
         imgRemovedynamicfilters3_Internalname = "REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTableheader_Internalname = "TABLEHEADER";
         edtavUpdate_Internalname = "vUPDATE";
         edtavDelete_Internalname = "vDELETE";
         edtFuncao_Codigo_Internalname = "FUNCAO_CODIGO";
         edtFuncao_Nome_Internalname = "FUNCAO_NOME";
         edtGrupoFuncao_Codigo_Internalname = "GRUPOFUNCAO_CODIGO";
         edtFuncao_UOCod_Internalname = "FUNCAO_UOCOD";
         edtFuncao_UONom_Internalname = "FUNCAO_UONOM";
         chkFuncao_Ativo_Internalname = "FUNCAO_ATIVO";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = "vDYNAMICFILTERSENABLED3";
         edtavTffuncao_nome_Internalname = "vTFFUNCAO_NOME";
         edtavTffuncao_nome_sel_Internalname = "vTFFUNCAO_NOME_SEL";
         edtavTfgrupofuncao_codigo_Internalname = "vTFGRUPOFUNCAO_CODIGO";
         edtavTfgrupofuncao_codigo_to_Internalname = "vTFGRUPOFUNCAO_CODIGO_TO";
         edtavTffuncao_uonom_Internalname = "vTFFUNCAO_UONOM";
         edtavTffuncao_uonom_sel_Internalname = "vTFFUNCAO_UONOM_SEL";
         edtavTffuncao_ativo_sel_Internalname = "vTFFUNCAO_ATIVO_SEL";
         Ddo_funcao_nome_Internalname = "DDO_FUNCAO_NOME";
         edtavDdo_funcao_nometitlecontrolidtoreplace_Internalname = "vDDO_FUNCAO_NOMETITLECONTROLIDTOREPLACE";
         Ddo_grupofuncao_codigo_Internalname = "DDO_GRUPOFUNCAO_CODIGO";
         edtavDdo_grupofuncao_codigotitlecontrolidtoreplace_Internalname = "vDDO_GRUPOFUNCAO_CODIGOTITLECONTROLIDTOREPLACE";
         Ddo_funcao_uonom_Internalname = "DDO_FUNCAO_UONOM";
         edtavDdo_funcao_uonomtitlecontrolidtoreplace_Internalname = "vDDO_FUNCAO_UONOMTITLECONTROLIDTOREPLACE";
         Ddo_funcao_ativo_Internalname = "DDO_FUNCAO_ATIVO";
         edtavDdo_funcao_ativotitlecontrolidtoreplace_Internalname = "vDDO_FUNCAO_ATIVOTITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtFuncao_UONom_Jsonclick = "";
         edtFuncao_UOCod_Jsonclick = "";
         edtGrupoFuncao_Codigo_Jsonclick = "";
         edtFuncao_Nome_Jsonclick = "";
         edtFuncao_Codigo_Jsonclick = "";
         imgInsert_Enabled = 1;
         imgInsert_Bitmap = (String)(context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( )));
         edtavFuncao_uonom3_Jsonclick = "";
         edtavFuncao_nome3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         edtavFuncao_uonom2_Jsonclick = "";
         edtavFuncao_nome2_Jsonclick = "";
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         edtavFuncao_uonom1_Jsonclick = "";
         edtavFuncao_nome1_Jsonclick = "";
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtFuncao_UONom_Link = "";
         edtFuncao_Nome_Link = "";
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = "";
         edtavDelete_Enabled = 1;
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = "";
         edtavUpdate_Enabled = 1;
         chkFuncao_Ativo_Titleformat = 0;
         edtFuncao_UONom_Titleformat = 0;
         edtGrupoFuncao_Codigo_Titleformat = 0;
         edtFuncao_Nome_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         edtavFuncao_uonom3_Visible = 1;
         edtavFuncao_nome3_Visible = 1;
         edtavFuncao_uonom2_Visible = 1;
         edtavFuncao_nome2_Visible = 1;
         edtavFuncao_uonom1_Visible = 1;
         edtavFuncao_nome1_Visible = 1;
         chkFuncao_Ativo.Title.Text = "Ativa?";
         edtFuncao_UONom_Title = "Unidade Organizacional";
         edtGrupoFuncao_Codigo_Title = "Grupo";
         edtFuncao_Nome_Title = "Nome";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         chkFuncao_Ativo.Caption = "";
         edtavDdo_funcao_ativotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_funcao_uonomtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_grupofuncao_codigotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_funcao_nometitlecontrolidtoreplace_Visible = 1;
         edtavTffuncao_ativo_sel_Jsonclick = "";
         edtavTffuncao_ativo_sel_Visible = 1;
         edtavTffuncao_uonom_sel_Jsonclick = "";
         edtavTffuncao_uonom_sel_Visible = 1;
         edtavTffuncao_uonom_Jsonclick = "";
         edtavTffuncao_uonom_Visible = 1;
         edtavTfgrupofuncao_codigo_to_Jsonclick = "";
         edtavTfgrupofuncao_codigo_to_Visible = 1;
         edtavTfgrupofuncao_codigo_Jsonclick = "";
         edtavTfgrupofuncao_codigo_Visible = 1;
         edtavTffuncao_nome_sel_Jsonclick = "";
         edtavTffuncao_nome_sel_Visible = 1;
         edtavTffuncao_nome_Jsonclick = "";
         edtavTffuncao_nome_Visible = 1;
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         Ddo_funcao_ativo_Searchbuttontext = "Pesquisar";
         Ddo_funcao_ativo_Cleanfilter = "Limpar pesquisa";
         Ddo_funcao_ativo_Sortdsc = "Ordenar de Z � A";
         Ddo_funcao_ativo_Sortasc = "Ordenar de A � Z";
         Ddo_funcao_ativo_Datalistfixedvalues = "1:Marcado,2:Desmarcado";
         Ddo_funcao_ativo_Datalisttype = "FixedValues";
         Ddo_funcao_ativo_Includedatalist = Convert.ToBoolean( -1);
         Ddo_funcao_ativo_Includefilter = Convert.ToBoolean( 0);
         Ddo_funcao_ativo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_funcao_ativo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_funcao_ativo_Titlecontrolidtoreplace = "";
         Ddo_funcao_ativo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_funcao_ativo_Cls = "ColumnSettings";
         Ddo_funcao_ativo_Tooltip = "Op��es";
         Ddo_funcao_ativo_Caption = "";
         Ddo_funcao_uonom_Searchbuttontext = "Pesquisar";
         Ddo_funcao_uonom_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_funcao_uonom_Cleanfilter = "Limpar pesquisa";
         Ddo_funcao_uonom_Loadingdata = "Carregando dados...";
         Ddo_funcao_uonom_Sortdsc = "Ordenar de Z � A";
         Ddo_funcao_uonom_Sortasc = "Ordenar de A � Z";
         Ddo_funcao_uonom_Datalistupdateminimumcharacters = 0;
         Ddo_funcao_uonom_Datalistproc = "GetWWGeral_FuncaoFilterData";
         Ddo_funcao_uonom_Datalisttype = "Dynamic";
         Ddo_funcao_uonom_Includedatalist = Convert.ToBoolean( -1);
         Ddo_funcao_uonom_Filterisrange = Convert.ToBoolean( 0);
         Ddo_funcao_uonom_Filtertype = "Character";
         Ddo_funcao_uonom_Includefilter = Convert.ToBoolean( -1);
         Ddo_funcao_uonom_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_funcao_uonom_Includesortasc = Convert.ToBoolean( -1);
         Ddo_funcao_uonom_Titlecontrolidtoreplace = "";
         Ddo_funcao_uonom_Dropdownoptionstype = "GridTitleSettings";
         Ddo_funcao_uonom_Cls = "ColumnSettings";
         Ddo_funcao_uonom_Tooltip = "Op��es";
         Ddo_funcao_uonom_Caption = "";
         Ddo_grupofuncao_codigo_Searchbuttontext = "Pesquisar";
         Ddo_grupofuncao_codigo_Rangefilterto = "At�";
         Ddo_grupofuncao_codigo_Rangefilterfrom = "Desde";
         Ddo_grupofuncao_codigo_Cleanfilter = "Limpar pesquisa";
         Ddo_grupofuncao_codigo_Sortdsc = "Ordenar de Z � A";
         Ddo_grupofuncao_codigo_Sortasc = "Ordenar de A � Z";
         Ddo_grupofuncao_codigo_Includedatalist = Convert.ToBoolean( 0);
         Ddo_grupofuncao_codigo_Filterisrange = Convert.ToBoolean( -1);
         Ddo_grupofuncao_codigo_Filtertype = "Numeric";
         Ddo_grupofuncao_codigo_Includefilter = Convert.ToBoolean( -1);
         Ddo_grupofuncao_codigo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_grupofuncao_codigo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_grupofuncao_codigo_Titlecontrolidtoreplace = "";
         Ddo_grupofuncao_codigo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_grupofuncao_codigo_Cls = "ColumnSettings";
         Ddo_grupofuncao_codigo_Tooltip = "Op��es";
         Ddo_grupofuncao_codigo_Caption = "";
         Ddo_funcao_nome_Searchbuttontext = "Pesquisar";
         Ddo_funcao_nome_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_funcao_nome_Cleanfilter = "Limpar pesquisa";
         Ddo_funcao_nome_Loadingdata = "Carregando dados...";
         Ddo_funcao_nome_Sortdsc = "Ordenar de Z � A";
         Ddo_funcao_nome_Sortasc = "Ordenar de A � Z";
         Ddo_funcao_nome_Datalistupdateminimumcharacters = 0;
         Ddo_funcao_nome_Datalistproc = "GetWWGeral_FuncaoFilterData";
         Ddo_funcao_nome_Datalisttype = "Dynamic";
         Ddo_funcao_nome_Includedatalist = Convert.ToBoolean( -1);
         Ddo_funcao_nome_Filterisrange = Convert.ToBoolean( 0);
         Ddo_funcao_nome_Filtertype = "Character";
         Ddo_funcao_nome_Includefilter = Convert.ToBoolean( -1);
         Ddo_funcao_nome_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_funcao_nome_Includesortasc = Convert.ToBoolean( -1);
         Ddo_funcao_nome_Titlecontrolidtoreplace = "";
         Ddo_funcao_nome_Dropdownoptionstype = "GridTitleSettings";
         Ddo_funcao_nome_Cls = "ColumnSettings";
         Ddo_funcao_nome_Tooltip = "Op��es";
         Ddo_funcao_nome_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = " Funcao";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A621Funcao_Codigo',fld:'FUNCAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A635Funcao_UOCod',fld:'FUNCAO_UOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV39ddo_Funcao_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_GrupoFuncao_CodigoTitleControlIdToReplace',fld:'vDDO_GRUPOFUNCAO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_Funcao_UONomTitleControlIdToReplace',fld:'vDDO_FUNCAO_UONOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_Funcao_AtivoTitleControlIdToReplace',fld:'vDDO_FUNCAO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16Funcao_Nome1',fld:'vFUNCAO_NOME1',pic:'@!',nv:''},{av:'AV29Funcao_UONom1',fld:'vFUNCAO_UONOM1',pic:'@!',nv:''},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV19Funcao_Nome2',fld:'vFUNCAO_NOME2',pic:'@!',nv:''},{av:'AV31Funcao_UONom2',fld:'vFUNCAO_UONOM2',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV22Funcao_Nome3',fld:'vFUNCAO_NOME3',pic:'@!',nv:''},{av:'AV33Funcao_UONom3',fld:'vFUNCAO_UONOM3',pic:'@!',nv:''},{av:'AV37TFFuncao_Nome',fld:'vTFFUNCAO_NOME',pic:'@!',nv:''},{av:'AV38TFFuncao_Nome_Sel',fld:'vTFFUNCAO_NOME_SEL',pic:'@!',nv:''},{av:'AV41TFGrupoFuncao_Codigo',fld:'vTFGRUPOFUNCAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV42TFGrupoFuncao_Codigo_To',fld:'vTFGRUPOFUNCAO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV45TFFuncao_UONom',fld:'vTFFUNCAO_UONOM',pic:'@!',nv:''},{av:'AV46TFFuncao_UONom_Sel',fld:'vTFFUNCAO_UONOM_SEL',pic:'@!',nv:''},{av:'AV49TFFuncao_Ativo_Sel',fld:'vTFFUNCAO_ATIVO_SEL',pic:'9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV77Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV36Funcao_NomeTitleFilterData',fld:'vFUNCAO_NOMETITLEFILTERDATA',pic:'',nv:null},{av:'AV40GrupoFuncao_CodigoTitleFilterData',fld:'vGRUPOFUNCAO_CODIGOTITLEFILTERDATA',pic:'',nv:null},{av:'AV44Funcao_UONomTitleFilterData',fld:'vFUNCAO_UONOMTITLEFILTERDATA',pic:'',nv:null},{av:'AV48Funcao_AtivoTitleFilterData',fld:'vFUNCAO_ATIVOTITLEFILTERDATA',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'edtFuncao_Nome_Titleformat',ctrl:'FUNCAO_NOME',prop:'Titleformat'},{av:'edtFuncao_Nome_Title',ctrl:'FUNCAO_NOME',prop:'Title'},{av:'edtGrupoFuncao_Codigo_Titleformat',ctrl:'GRUPOFUNCAO_CODIGO',prop:'Titleformat'},{av:'edtGrupoFuncao_Codigo_Title',ctrl:'GRUPOFUNCAO_CODIGO',prop:'Title'},{av:'edtFuncao_UONom_Titleformat',ctrl:'FUNCAO_UONOM',prop:'Titleformat'},{av:'edtFuncao_UONom_Title',ctrl:'FUNCAO_UONOM',prop:'Title'},{av:'chkFuncao_Ativo_Titleformat',ctrl:'FUNCAO_ATIVO',prop:'Titleformat'},{av:'chkFuncao_Ativo.Title.Text',ctrl:'FUNCAO_ATIVO',prop:'Title'},{av:'AV53GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV54GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'imgInsert_Link',ctrl:'INSERT',prop:'Link'},{av:'imgInsert_Enabled',ctrl:'INSERT',prop:'Enabled'},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11CI2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16Funcao_Nome1',fld:'vFUNCAO_NOME1',pic:'@!',nv:''},{av:'AV29Funcao_UONom1',fld:'vFUNCAO_UONOM1',pic:'@!',nv:''},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV19Funcao_Nome2',fld:'vFUNCAO_NOME2',pic:'@!',nv:''},{av:'AV31Funcao_UONom2',fld:'vFUNCAO_UONOM2',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV22Funcao_Nome3',fld:'vFUNCAO_NOME3',pic:'@!',nv:''},{av:'AV33Funcao_UONom3',fld:'vFUNCAO_UONOM3',pic:'@!',nv:''},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV37TFFuncao_Nome',fld:'vTFFUNCAO_NOME',pic:'@!',nv:''},{av:'AV38TFFuncao_Nome_Sel',fld:'vTFFUNCAO_NOME_SEL',pic:'@!',nv:''},{av:'AV41TFGrupoFuncao_Codigo',fld:'vTFGRUPOFUNCAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV42TFGrupoFuncao_Codigo_To',fld:'vTFGRUPOFUNCAO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV45TFFuncao_UONom',fld:'vTFFUNCAO_UONOM',pic:'@!',nv:''},{av:'AV46TFFuncao_UONom_Sel',fld:'vTFFUNCAO_UONOM_SEL',pic:'@!',nv:''},{av:'AV49TFFuncao_Ativo_Sel',fld:'vTFFUNCAO_ATIVO_SEL',pic:'9',nv:0},{av:'AV39ddo_Funcao_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_GrupoFuncao_CodigoTitleControlIdToReplace',fld:'vDDO_GRUPOFUNCAO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_Funcao_UONomTitleControlIdToReplace',fld:'vDDO_FUNCAO_UONOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_Funcao_AtivoTitleControlIdToReplace',fld:'vDDO_FUNCAO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV77Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A621Funcao_Codigo',fld:'FUNCAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A635Funcao_UOCod',fld:'FUNCAO_UOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_FUNCAO_NOME.ONOPTIONCLICKED","{handler:'E12CI2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16Funcao_Nome1',fld:'vFUNCAO_NOME1',pic:'@!',nv:''},{av:'AV29Funcao_UONom1',fld:'vFUNCAO_UONOM1',pic:'@!',nv:''},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV19Funcao_Nome2',fld:'vFUNCAO_NOME2',pic:'@!',nv:''},{av:'AV31Funcao_UONom2',fld:'vFUNCAO_UONOM2',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV22Funcao_Nome3',fld:'vFUNCAO_NOME3',pic:'@!',nv:''},{av:'AV33Funcao_UONom3',fld:'vFUNCAO_UONOM3',pic:'@!',nv:''},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV37TFFuncao_Nome',fld:'vTFFUNCAO_NOME',pic:'@!',nv:''},{av:'AV38TFFuncao_Nome_Sel',fld:'vTFFUNCAO_NOME_SEL',pic:'@!',nv:''},{av:'AV41TFGrupoFuncao_Codigo',fld:'vTFGRUPOFUNCAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV42TFGrupoFuncao_Codigo_To',fld:'vTFGRUPOFUNCAO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV45TFFuncao_UONom',fld:'vTFFUNCAO_UONOM',pic:'@!',nv:''},{av:'AV46TFFuncao_UONom_Sel',fld:'vTFFUNCAO_UONOM_SEL',pic:'@!',nv:''},{av:'AV49TFFuncao_Ativo_Sel',fld:'vTFFUNCAO_ATIVO_SEL',pic:'9',nv:0},{av:'AV39ddo_Funcao_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_GrupoFuncao_CodigoTitleControlIdToReplace',fld:'vDDO_GRUPOFUNCAO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_Funcao_UONomTitleControlIdToReplace',fld:'vDDO_FUNCAO_UONOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_Funcao_AtivoTitleControlIdToReplace',fld:'vDDO_FUNCAO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV77Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A621Funcao_Codigo',fld:'FUNCAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A635Funcao_UOCod',fld:'FUNCAO_UOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_funcao_nome_Activeeventkey',ctrl:'DDO_FUNCAO_NOME',prop:'ActiveEventKey'},{av:'Ddo_funcao_nome_Filteredtext_get',ctrl:'DDO_FUNCAO_NOME',prop:'FilteredText_get'},{av:'Ddo_funcao_nome_Selectedvalue_get',ctrl:'DDO_FUNCAO_NOME',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_funcao_nome_Sortedstatus',ctrl:'DDO_FUNCAO_NOME',prop:'SortedStatus'},{av:'AV37TFFuncao_Nome',fld:'vTFFUNCAO_NOME',pic:'@!',nv:''},{av:'AV38TFFuncao_Nome_Sel',fld:'vTFFUNCAO_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_grupofuncao_codigo_Sortedstatus',ctrl:'DDO_GRUPOFUNCAO_CODIGO',prop:'SortedStatus'},{av:'Ddo_funcao_uonom_Sortedstatus',ctrl:'DDO_FUNCAO_UONOM',prop:'SortedStatus'},{av:'Ddo_funcao_ativo_Sortedstatus',ctrl:'DDO_FUNCAO_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_GRUPOFUNCAO_CODIGO.ONOPTIONCLICKED","{handler:'E13CI2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16Funcao_Nome1',fld:'vFUNCAO_NOME1',pic:'@!',nv:''},{av:'AV29Funcao_UONom1',fld:'vFUNCAO_UONOM1',pic:'@!',nv:''},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV19Funcao_Nome2',fld:'vFUNCAO_NOME2',pic:'@!',nv:''},{av:'AV31Funcao_UONom2',fld:'vFUNCAO_UONOM2',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV22Funcao_Nome3',fld:'vFUNCAO_NOME3',pic:'@!',nv:''},{av:'AV33Funcao_UONom3',fld:'vFUNCAO_UONOM3',pic:'@!',nv:''},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV37TFFuncao_Nome',fld:'vTFFUNCAO_NOME',pic:'@!',nv:''},{av:'AV38TFFuncao_Nome_Sel',fld:'vTFFUNCAO_NOME_SEL',pic:'@!',nv:''},{av:'AV41TFGrupoFuncao_Codigo',fld:'vTFGRUPOFUNCAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV42TFGrupoFuncao_Codigo_To',fld:'vTFGRUPOFUNCAO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV45TFFuncao_UONom',fld:'vTFFUNCAO_UONOM',pic:'@!',nv:''},{av:'AV46TFFuncao_UONom_Sel',fld:'vTFFUNCAO_UONOM_SEL',pic:'@!',nv:''},{av:'AV49TFFuncao_Ativo_Sel',fld:'vTFFUNCAO_ATIVO_SEL',pic:'9',nv:0},{av:'AV39ddo_Funcao_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_GrupoFuncao_CodigoTitleControlIdToReplace',fld:'vDDO_GRUPOFUNCAO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_Funcao_UONomTitleControlIdToReplace',fld:'vDDO_FUNCAO_UONOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_Funcao_AtivoTitleControlIdToReplace',fld:'vDDO_FUNCAO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV77Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A621Funcao_Codigo',fld:'FUNCAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A635Funcao_UOCod',fld:'FUNCAO_UOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_grupofuncao_codigo_Activeeventkey',ctrl:'DDO_GRUPOFUNCAO_CODIGO',prop:'ActiveEventKey'},{av:'Ddo_grupofuncao_codigo_Filteredtext_get',ctrl:'DDO_GRUPOFUNCAO_CODIGO',prop:'FilteredText_get'},{av:'Ddo_grupofuncao_codigo_Filteredtextto_get',ctrl:'DDO_GRUPOFUNCAO_CODIGO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_grupofuncao_codigo_Sortedstatus',ctrl:'DDO_GRUPOFUNCAO_CODIGO',prop:'SortedStatus'},{av:'AV41TFGrupoFuncao_Codigo',fld:'vTFGRUPOFUNCAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV42TFGrupoFuncao_Codigo_To',fld:'vTFGRUPOFUNCAO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_funcao_nome_Sortedstatus',ctrl:'DDO_FUNCAO_NOME',prop:'SortedStatus'},{av:'Ddo_funcao_uonom_Sortedstatus',ctrl:'DDO_FUNCAO_UONOM',prop:'SortedStatus'},{av:'Ddo_funcao_ativo_Sortedstatus',ctrl:'DDO_FUNCAO_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_FUNCAO_UONOM.ONOPTIONCLICKED","{handler:'E14CI2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16Funcao_Nome1',fld:'vFUNCAO_NOME1',pic:'@!',nv:''},{av:'AV29Funcao_UONom1',fld:'vFUNCAO_UONOM1',pic:'@!',nv:''},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV19Funcao_Nome2',fld:'vFUNCAO_NOME2',pic:'@!',nv:''},{av:'AV31Funcao_UONom2',fld:'vFUNCAO_UONOM2',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV22Funcao_Nome3',fld:'vFUNCAO_NOME3',pic:'@!',nv:''},{av:'AV33Funcao_UONom3',fld:'vFUNCAO_UONOM3',pic:'@!',nv:''},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV37TFFuncao_Nome',fld:'vTFFUNCAO_NOME',pic:'@!',nv:''},{av:'AV38TFFuncao_Nome_Sel',fld:'vTFFUNCAO_NOME_SEL',pic:'@!',nv:''},{av:'AV41TFGrupoFuncao_Codigo',fld:'vTFGRUPOFUNCAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV42TFGrupoFuncao_Codigo_To',fld:'vTFGRUPOFUNCAO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV45TFFuncao_UONom',fld:'vTFFUNCAO_UONOM',pic:'@!',nv:''},{av:'AV46TFFuncao_UONom_Sel',fld:'vTFFUNCAO_UONOM_SEL',pic:'@!',nv:''},{av:'AV49TFFuncao_Ativo_Sel',fld:'vTFFUNCAO_ATIVO_SEL',pic:'9',nv:0},{av:'AV39ddo_Funcao_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_GrupoFuncao_CodigoTitleControlIdToReplace',fld:'vDDO_GRUPOFUNCAO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_Funcao_UONomTitleControlIdToReplace',fld:'vDDO_FUNCAO_UONOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_Funcao_AtivoTitleControlIdToReplace',fld:'vDDO_FUNCAO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV77Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A621Funcao_Codigo',fld:'FUNCAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A635Funcao_UOCod',fld:'FUNCAO_UOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_funcao_uonom_Activeeventkey',ctrl:'DDO_FUNCAO_UONOM',prop:'ActiveEventKey'},{av:'Ddo_funcao_uonom_Filteredtext_get',ctrl:'DDO_FUNCAO_UONOM',prop:'FilteredText_get'},{av:'Ddo_funcao_uonom_Selectedvalue_get',ctrl:'DDO_FUNCAO_UONOM',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_funcao_uonom_Sortedstatus',ctrl:'DDO_FUNCAO_UONOM',prop:'SortedStatus'},{av:'AV45TFFuncao_UONom',fld:'vTFFUNCAO_UONOM',pic:'@!',nv:''},{av:'AV46TFFuncao_UONom_Sel',fld:'vTFFUNCAO_UONOM_SEL',pic:'@!',nv:''},{av:'Ddo_funcao_nome_Sortedstatus',ctrl:'DDO_FUNCAO_NOME',prop:'SortedStatus'},{av:'Ddo_grupofuncao_codigo_Sortedstatus',ctrl:'DDO_GRUPOFUNCAO_CODIGO',prop:'SortedStatus'},{av:'Ddo_funcao_ativo_Sortedstatus',ctrl:'DDO_FUNCAO_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_FUNCAO_ATIVO.ONOPTIONCLICKED","{handler:'E15CI2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16Funcao_Nome1',fld:'vFUNCAO_NOME1',pic:'@!',nv:''},{av:'AV29Funcao_UONom1',fld:'vFUNCAO_UONOM1',pic:'@!',nv:''},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV19Funcao_Nome2',fld:'vFUNCAO_NOME2',pic:'@!',nv:''},{av:'AV31Funcao_UONom2',fld:'vFUNCAO_UONOM2',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV22Funcao_Nome3',fld:'vFUNCAO_NOME3',pic:'@!',nv:''},{av:'AV33Funcao_UONom3',fld:'vFUNCAO_UONOM3',pic:'@!',nv:''},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV37TFFuncao_Nome',fld:'vTFFUNCAO_NOME',pic:'@!',nv:''},{av:'AV38TFFuncao_Nome_Sel',fld:'vTFFUNCAO_NOME_SEL',pic:'@!',nv:''},{av:'AV41TFGrupoFuncao_Codigo',fld:'vTFGRUPOFUNCAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV42TFGrupoFuncao_Codigo_To',fld:'vTFGRUPOFUNCAO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV45TFFuncao_UONom',fld:'vTFFUNCAO_UONOM',pic:'@!',nv:''},{av:'AV46TFFuncao_UONom_Sel',fld:'vTFFUNCAO_UONOM_SEL',pic:'@!',nv:''},{av:'AV49TFFuncao_Ativo_Sel',fld:'vTFFUNCAO_ATIVO_SEL',pic:'9',nv:0},{av:'AV39ddo_Funcao_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_GrupoFuncao_CodigoTitleControlIdToReplace',fld:'vDDO_GRUPOFUNCAO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_Funcao_UONomTitleControlIdToReplace',fld:'vDDO_FUNCAO_UONOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_Funcao_AtivoTitleControlIdToReplace',fld:'vDDO_FUNCAO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV77Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A621Funcao_Codigo',fld:'FUNCAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A635Funcao_UOCod',fld:'FUNCAO_UOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_funcao_ativo_Activeeventkey',ctrl:'DDO_FUNCAO_ATIVO',prop:'ActiveEventKey'},{av:'Ddo_funcao_ativo_Selectedvalue_get',ctrl:'DDO_FUNCAO_ATIVO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_funcao_ativo_Sortedstatus',ctrl:'DDO_FUNCAO_ATIVO',prop:'SortedStatus'},{av:'AV49TFFuncao_Ativo_Sel',fld:'vTFFUNCAO_ATIVO_SEL',pic:'9',nv:0},{av:'Ddo_funcao_nome_Sortedstatus',ctrl:'DDO_FUNCAO_NOME',prop:'SortedStatus'},{av:'Ddo_grupofuncao_codigo_Sortedstatus',ctrl:'DDO_GRUPOFUNCAO_CODIGO',prop:'SortedStatus'},{av:'Ddo_funcao_uonom_Sortedstatus',ctrl:'DDO_FUNCAO_UONOM',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E29CI2',iparms:[{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A621Funcao_Codigo',fld:'FUNCAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A635Funcao_UOCod',fld:'FUNCAO_UOCOD',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'edtavUpdate_Link',ctrl:'vUPDATE',prop:'Link'},{av:'AV25Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Enabled',ctrl:'vUPDATE',prop:'Enabled'},{av:'edtavDelete_Tooltiptext',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'edtavDelete_Link',ctrl:'vDELETE',prop:'Link'},{av:'AV26Delete',fld:'vDELETE',pic:'',nv:''},{av:'edtavDelete_Enabled',ctrl:'vDELETE',prop:'Enabled'},{av:'edtFuncao_Nome_Link',ctrl:'FUNCAO_NOME',prop:'Link'},{av:'edtFuncao_UONom_Link',ctrl:'FUNCAO_UONOM',prop:'Link'}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E16CI2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16Funcao_Nome1',fld:'vFUNCAO_NOME1',pic:'@!',nv:''},{av:'AV29Funcao_UONom1',fld:'vFUNCAO_UONOM1',pic:'@!',nv:''},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV19Funcao_Nome2',fld:'vFUNCAO_NOME2',pic:'@!',nv:''},{av:'AV31Funcao_UONom2',fld:'vFUNCAO_UONOM2',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV22Funcao_Nome3',fld:'vFUNCAO_NOME3',pic:'@!',nv:''},{av:'AV33Funcao_UONom3',fld:'vFUNCAO_UONOM3',pic:'@!',nv:''},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV37TFFuncao_Nome',fld:'vTFFUNCAO_NOME',pic:'@!',nv:''},{av:'AV38TFFuncao_Nome_Sel',fld:'vTFFUNCAO_NOME_SEL',pic:'@!',nv:''},{av:'AV41TFGrupoFuncao_Codigo',fld:'vTFGRUPOFUNCAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV42TFGrupoFuncao_Codigo_To',fld:'vTFGRUPOFUNCAO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV45TFFuncao_UONom',fld:'vTFFUNCAO_UONOM',pic:'@!',nv:''},{av:'AV46TFFuncao_UONom_Sel',fld:'vTFFUNCAO_UONOM_SEL',pic:'@!',nv:''},{av:'AV49TFFuncao_Ativo_Sel',fld:'vTFFUNCAO_ATIVO_SEL',pic:'9',nv:0},{av:'AV39ddo_Funcao_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_GrupoFuncao_CodigoTitleControlIdToReplace',fld:'vDDO_GRUPOFUNCAO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_Funcao_UONomTitleControlIdToReplace',fld:'vDDO_FUNCAO_UONOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_Funcao_AtivoTitleControlIdToReplace',fld:'vDDO_FUNCAO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV77Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A621Funcao_Codigo',fld:'FUNCAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A635Funcao_UOCod',fld:'FUNCAO_UOCOD',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E22CI2',iparms:[],oparms:[{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E17CI2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16Funcao_Nome1',fld:'vFUNCAO_NOME1',pic:'@!',nv:''},{av:'AV29Funcao_UONom1',fld:'vFUNCAO_UONOM1',pic:'@!',nv:''},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV19Funcao_Nome2',fld:'vFUNCAO_NOME2',pic:'@!',nv:''},{av:'AV31Funcao_UONom2',fld:'vFUNCAO_UONOM2',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV22Funcao_Nome3',fld:'vFUNCAO_NOME3',pic:'@!',nv:''},{av:'AV33Funcao_UONom3',fld:'vFUNCAO_UONOM3',pic:'@!',nv:''},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV37TFFuncao_Nome',fld:'vTFFUNCAO_NOME',pic:'@!',nv:''},{av:'AV38TFFuncao_Nome_Sel',fld:'vTFFUNCAO_NOME_SEL',pic:'@!',nv:''},{av:'AV41TFGrupoFuncao_Codigo',fld:'vTFGRUPOFUNCAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV42TFGrupoFuncao_Codigo_To',fld:'vTFGRUPOFUNCAO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV45TFFuncao_UONom',fld:'vTFFUNCAO_UONOM',pic:'@!',nv:''},{av:'AV46TFFuncao_UONom_Sel',fld:'vTFFUNCAO_UONOM_SEL',pic:'@!',nv:''},{av:'AV49TFFuncao_Ativo_Sel',fld:'vTFFUNCAO_ATIVO_SEL',pic:'9',nv:0},{av:'AV39ddo_Funcao_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_GrupoFuncao_CodigoTitleControlIdToReplace',fld:'vDDO_GRUPOFUNCAO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_Funcao_UONomTitleControlIdToReplace',fld:'vDDO_FUNCAO_UONOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_Funcao_AtivoTitleControlIdToReplace',fld:'vDDO_FUNCAO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV77Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A621Funcao_Codigo',fld:'FUNCAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A635Funcao_UOCod',fld:'FUNCAO_UOCOD',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV19Funcao_Nome2',fld:'vFUNCAO_NOME2',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV22Funcao_Nome3',fld:'vFUNCAO_NOME3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16Funcao_Nome1',fld:'vFUNCAO_NOME1',pic:'@!',nv:''},{av:'AV29Funcao_UONom1',fld:'vFUNCAO_UONOM1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV31Funcao_UONom2',fld:'vFUNCAO_UONOM2',pic:'@!',nv:''},{av:'AV33Funcao_UONom3',fld:'vFUNCAO_UONOM3',pic:'@!',nv:''},{av:'edtavFuncao_nome2_Visible',ctrl:'vFUNCAO_NOME2',prop:'Visible'},{av:'edtavFuncao_uonom2_Visible',ctrl:'vFUNCAO_UONOM2',prop:'Visible'},{av:'edtavFuncao_nome3_Visible',ctrl:'vFUNCAO_NOME3',prop:'Visible'},{av:'edtavFuncao_uonom3_Visible',ctrl:'vFUNCAO_UONOM3',prop:'Visible'},{av:'edtavFuncao_nome1_Visible',ctrl:'vFUNCAO_NOME1',prop:'Visible'},{av:'edtavFuncao_uonom1_Visible',ctrl:'vFUNCAO_UONOM1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E23CI2',iparms:[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'edtavFuncao_nome1_Visible',ctrl:'vFUNCAO_NOME1',prop:'Visible'},{av:'edtavFuncao_uonom1_Visible',ctrl:'vFUNCAO_UONOM1',prop:'Visible'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E24CI2',iparms:[],oparms:[{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E18CI2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16Funcao_Nome1',fld:'vFUNCAO_NOME1',pic:'@!',nv:''},{av:'AV29Funcao_UONom1',fld:'vFUNCAO_UONOM1',pic:'@!',nv:''},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV19Funcao_Nome2',fld:'vFUNCAO_NOME2',pic:'@!',nv:''},{av:'AV31Funcao_UONom2',fld:'vFUNCAO_UONOM2',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV22Funcao_Nome3',fld:'vFUNCAO_NOME3',pic:'@!',nv:''},{av:'AV33Funcao_UONom3',fld:'vFUNCAO_UONOM3',pic:'@!',nv:''},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV37TFFuncao_Nome',fld:'vTFFUNCAO_NOME',pic:'@!',nv:''},{av:'AV38TFFuncao_Nome_Sel',fld:'vTFFUNCAO_NOME_SEL',pic:'@!',nv:''},{av:'AV41TFGrupoFuncao_Codigo',fld:'vTFGRUPOFUNCAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV42TFGrupoFuncao_Codigo_To',fld:'vTFGRUPOFUNCAO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV45TFFuncao_UONom',fld:'vTFFUNCAO_UONOM',pic:'@!',nv:''},{av:'AV46TFFuncao_UONom_Sel',fld:'vTFFUNCAO_UONOM_SEL',pic:'@!',nv:''},{av:'AV49TFFuncao_Ativo_Sel',fld:'vTFFUNCAO_ATIVO_SEL',pic:'9',nv:0},{av:'AV39ddo_Funcao_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_GrupoFuncao_CodigoTitleControlIdToReplace',fld:'vDDO_GRUPOFUNCAO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_Funcao_UONomTitleControlIdToReplace',fld:'vDDO_FUNCAO_UONOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_Funcao_AtivoTitleControlIdToReplace',fld:'vDDO_FUNCAO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV77Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A621Funcao_Codigo',fld:'FUNCAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A635Funcao_UOCod',fld:'FUNCAO_UOCOD',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV19Funcao_Nome2',fld:'vFUNCAO_NOME2',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV22Funcao_Nome3',fld:'vFUNCAO_NOME3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16Funcao_Nome1',fld:'vFUNCAO_NOME1',pic:'@!',nv:''},{av:'AV29Funcao_UONom1',fld:'vFUNCAO_UONOM1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV31Funcao_UONom2',fld:'vFUNCAO_UONOM2',pic:'@!',nv:''},{av:'AV33Funcao_UONom3',fld:'vFUNCAO_UONOM3',pic:'@!',nv:''},{av:'edtavFuncao_nome2_Visible',ctrl:'vFUNCAO_NOME2',prop:'Visible'},{av:'edtavFuncao_uonom2_Visible',ctrl:'vFUNCAO_UONOM2',prop:'Visible'},{av:'edtavFuncao_nome3_Visible',ctrl:'vFUNCAO_NOME3',prop:'Visible'},{av:'edtavFuncao_uonom3_Visible',ctrl:'vFUNCAO_UONOM3',prop:'Visible'},{av:'edtavFuncao_nome1_Visible',ctrl:'vFUNCAO_NOME1',prop:'Visible'},{av:'edtavFuncao_uonom1_Visible',ctrl:'vFUNCAO_UONOM1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E25CI2',iparms:[{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'edtavFuncao_nome2_Visible',ctrl:'vFUNCAO_NOME2',prop:'Visible'},{av:'edtavFuncao_uonom2_Visible',ctrl:'vFUNCAO_UONOM2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E19CI2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16Funcao_Nome1',fld:'vFUNCAO_NOME1',pic:'@!',nv:''},{av:'AV29Funcao_UONom1',fld:'vFUNCAO_UONOM1',pic:'@!',nv:''},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV19Funcao_Nome2',fld:'vFUNCAO_NOME2',pic:'@!',nv:''},{av:'AV31Funcao_UONom2',fld:'vFUNCAO_UONOM2',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV22Funcao_Nome3',fld:'vFUNCAO_NOME3',pic:'@!',nv:''},{av:'AV33Funcao_UONom3',fld:'vFUNCAO_UONOM3',pic:'@!',nv:''},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV37TFFuncao_Nome',fld:'vTFFUNCAO_NOME',pic:'@!',nv:''},{av:'AV38TFFuncao_Nome_Sel',fld:'vTFFUNCAO_NOME_SEL',pic:'@!',nv:''},{av:'AV41TFGrupoFuncao_Codigo',fld:'vTFGRUPOFUNCAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV42TFGrupoFuncao_Codigo_To',fld:'vTFGRUPOFUNCAO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV45TFFuncao_UONom',fld:'vTFFUNCAO_UONOM',pic:'@!',nv:''},{av:'AV46TFFuncao_UONom_Sel',fld:'vTFFUNCAO_UONOM_SEL',pic:'@!',nv:''},{av:'AV49TFFuncao_Ativo_Sel',fld:'vTFFUNCAO_ATIVO_SEL',pic:'9',nv:0},{av:'AV39ddo_Funcao_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_GrupoFuncao_CodigoTitleControlIdToReplace',fld:'vDDO_GRUPOFUNCAO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_Funcao_UONomTitleControlIdToReplace',fld:'vDDO_FUNCAO_UONOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_Funcao_AtivoTitleControlIdToReplace',fld:'vDDO_FUNCAO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV77Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A621Funcao_Codigo',fld:'FUNCAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A635Funcao_UOCod',fld:'FUNCAO_UOCOD',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV19Funcao_Nome2',fld:'vFUNCAO_NOME2',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV22Funcao_Nome3',fld:'vFUNCAO_NOME3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16Funcao_Nome1',fld:'vFUNCAO_NOME1',pic:'@!',nv:''},{av:'AV29Funcao_UONom1',fld:'vFUNCAO_UONOM1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV31Funcao_UONom2',fld:'vFUNCAO_UONOM2',pic:'@!',nv:''},{av:'AV33Funcao_UONom3',fld:'vFUNCAO_UONOM3',pic:'@!',nv:''},{av:'edtavFuncao_nome2_Visible',ctrl:'vFUNCAO_NOME2',prop:'Visible'},{av:'edtavFuncao_uonom2_Visible',ctrl:'vFUNCAO_UONOM2',prop:'Visible'},{av:'edtavFuncao_nome3_Visible',ctrl:'vFUNCAO_NOME3',prop:'Visible'},{av:'edtavFuncao_uonom3_Visible',ctrl:'vFUNCAO_UONOM3',prop:'Visible'},{av:'edtavFuncao_nome1_Visible',ctrl:'vFUNCAO_NOME1',prop:'Visible'},{av:'edtavFuncao_uonom1_Visible',ctrl:'vFUNCAO_UONOM1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E26CI2',iparms:[{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'edtavFuncao_nome3_Visible',ctrl:'vFUNCAO_NOME3',prop:'Visible'},{av:'edtavFuncao_uonom3_Visible',ctrl:'vFUNCAO_UONOM3',prop:'Visible'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E20CI2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16Funcao_Nome1',fld:'vFUNCAO_NOME1',pic:'@!',nv:''},{av:'AV29Funcao_UONom1',fld:'vFUNCAO_UONOM1',pic:'@!',nv:''},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV19Funcao_Nome2',fld:'vFUNCAO_NOME2',pic:'@!',nv:''},{av:'AV31Funcao_UONom2',fld:'vFUNCAO_UONOM2',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV22Funcao_Nome3',fld:'vFUNCAO_NOME3',pic:'@!',nv:''},{av:'AV33Funcao_UONom3',fld:'vFUNCAO_UONOM3',pic:'@!',nv:''},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV37TFFuncao_Nome',fld:'vTFFUNCAO_NOME',pic:'@!',nv:''},{av:'AV38TFFuncao_Nome_Sel',fld:'vTFFUNCAO_NOME_SEL',pic:'@!',nv:''},{av:'AV41TFGrupoFuncao_Codigo',fld:'vTFGRUPOFUNCAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV42TFGrupoFuncao_Codigo_To',fld:'vTFGRUPOFUNCAO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV45TFFuncao_UONom',fld:'vTFFUNCAO_UONOM',pic:'@!',nv:''},{av:'AV46TFFuncao_UONom_Sel',fld:'vTFFUNCAO_UONOM_SEL',pic:'@!',nv:''},{av:'AV49TFFuncao_Ativo_Sel',fld:'vTFFUNCAO_ATIVO_SEL',pic:'9',nv:0},{av:'AV39ddo_Funcao_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_GrupoFuncao_CodigoTitleControlIdToReplace',fld:'vDDO_GRUPOFUNCAO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_Funcao_UONomTitleControlIdToReplace',fld:'vDDO_FUNCAO_UONOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_Funcao_AtivoTitleControlIdToReplace',fld:'vDDO_FUNCAO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV77Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV24DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A621Funcao_Codigo',fld:'FUNCAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A635Funcao_UOCod',fld:'FUNCAO_UOCOD',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV37TFFuncao_Nome',fld:'vTFFUNCAO_NOME',pic:'@!',nv:''},{av:'Ddo_funcao_nome_Filteredtext_set',ctrl:'DDO_FUNCAO_NOME',prop:'FilteredText_set'},{av:'AV38TFFuncao_Nome_Sel',fld:'vTFFUNCAO_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_funcao_nome_Selectedvalue_set',ctrl:'DDO_FUNCAO_NOME',prop:'SelectedValue_set'},{av:'AV41TFGrupoFuncao_Codigo',fld:'vTFGRUPOFUNCAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Ddo_grupofuncao_codigo_Filteredtext_set',ctrl:'DDO_GRUPOFUNCAO_CODIGO',prop:'FilteredText_set'},{av:'AV42TFGrupoFuncao_Codigo_To',fld:'vTFGRUPOFUNCAO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_grupofuncao_codigo_Filteredtextto_set',ctrl:'DDO_GRUPOFUNCAO_CODIGO',prop:'FilteredTextTo_set'},{av:'AV45TFFuncao_UONom',fld:'vTFFUNCAO_UONOM',pic:'@!',nv:''},{av:'Ddo_funcao_uonom_Filteredtext_set',ctrl:'DDO_FUNCAO_UONOM',prop:'FilteredText_set'},{av:'AV46TFFuncao_UONom_Sel',fld:'vTFFUNCAO_UONOM_SEL',pic:'@!',nv:''},{av:'Ddo_funcao_uonom_Selectedvalue_set',ctrl:'DDO_FUNCAO_UONOM',prop:'SelectedValue_set'},{av:'AV49TFFuncao_Ativo_Sel',fld:'vTFFUNCAO_ATIVO_SEL',pic:'9',nv:0},{av:'Ddo_funcao_ativo_Selectedvalue_set',ctrl:'DDO_FUNCAO_ATIVO',prop:'SelectedValue_set'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16Funcao_Nome1',fld:'vFUNCAO_NOME1',pic:'@!',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'edtavFuncao_nome1_Visible',ctrl:'vFUNCAO_NOME1',prop:'Visible'},{av:'edtavFuncao_uonom1_Visible',ctrl:'vFUNCAO_UONOM1',prop:'Visible'},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV18DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV19Funcao_Nome2',fld:'vFUNCAO_NOME2',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV21DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV22Funcao_Nome3',fld:'vFUNCAO_NOME3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV29Funcao_UONom1',fld:'vFUNCAO_UONOM1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV31Funcao_UONom2',fld:'vFUNCAO_UONOM2',pic:'@!',nv:''},{av:'AV33Funcao_UONom3',fld:'vFUNCAO_UONOM3',pic:'@!',nv:''},{av:'edtavFuncao_nome2_Visible',ctrl:'vFUNCAO_NOME2',prop:'Visible'},{av:'edtavFuncao_uonom2_Visible',ctrl:'vFUNCAO_UONOM2',prop:'Visible'},{av:'edtavFuncao_nome3_Visible',ctrl:'vFUNCAO_NOME3',prop:'Visible'},{av:'edtavFuncao_uonom3_Visible',ctrl:'vFUNCAO_UONOM3',prop:'Visible'}]}");
         setEventMetadata("'DOINSERT'","{handler:'E21CI2',iparms:[{av:'A621Funcao_Codigo',fld:'FUNCAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_funcao_nome_Activeeventkey = "";
         Ddo_funcao_nome_Filteredtext_get = "";
         Ddo_funcao_nome_Selectedvalue_get = "";
         Ddo_grupofuncao_codigo_Activeeventkey = "";
         Ddo_grupofuncao_codigo_Filteredtext_get = "";
         Ddo_grupofuncao_codigo_Filteredtextto_get = "";
         Ddo_funcao_uonom_Activeeventkey = "";
         Ddo_funcao_uonom_Filteredtext_get = "";
         Ddo_funcao_uonom_Selectedvalue_get = "";
         Ddo_funcao_ativo_Activeeventkey = "";
         Ddo_funcao_ativo_Selectedvalue_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV15DynamicFiltersSelector1 = "";
         AV16Funcao_Nome1 = "";
         AV29Funcao_UONom1 = "";
         AV18DynamicFiltersSelector2 = "";
         AV19Funcao_Nome2 = "";
         AV31Funcao_UONom2 = "";
         AV21DynamicFiltersSelector3 = "";
         AV22Funcao_Nome3 = "";
         AV33Funcao_UONom3 = "";
         AV37TFFuncao_Nome = "";
         AV38TFFuncao_Nome_Sel = "";
         AV45TFFuncao_UONom = "";
         AV46TFFuncao_UONom_Sel = "";
         AV39ddo_Funcao_NomeTitleControlIdToReplace = "";
         AV43ddo_GrupoFuncao_CodigoTitleControlIdToReplace = "";
         AV47ddo_Funcao_UONomTitleControlIdToReplace = "";
         AV50ddo_Funcao_AtivoTitleControlIdToReplace = "";
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV77Pgmname = "";
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV51DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV36Funcao_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV40GrupoFuncao_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV44Funcao_UONomTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV48Funcao_AtivoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_funcao_nome_Filteredtext_set = "";
         Ddo_funcao_nome_Selectedvalue_set = "";
         Ddo_funcao_nome_Sortedstatus = "";
         Ddo_grupofuncao_codigo_Filteredtext_set = "";
         Ddo_grupofuncao_codigo_Filteredtextto_set = "";
         Ddo_grupofuncao_codigo_Sortedstatus = "";
         Ddo_funcao_uonom_Filteredtext_set = "";
         Ddo_funcao_uonom_Selectedvalue_set = "";
         Ddo_funcao_uonom_Sortedstatus = "";
         Ddo_funcao_ativo_Selectedvalue_set = "";
         Ddo_funcao_ativo_Sortedstatus = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV25Update = "";
         AV75Update_GXI = "";
         AV26Delete = "";
         AV76Delete_GXI = "";
         A622Funcao_Nome = "";
         A636Funcao_UONom = "";
         GXCCtl = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV58WWGeral_FuncaoDS_2_Funcao_nome1 = "";
         lV59WWGeral_FuncaoDS_3_Funcao_uonom1 = "";
         lV62WWGeral_FuncaoDS_6_Funcao_nome2 = "";
         lV63WWGeral_FuncaoDS_7_Funcao_uonom2 = "";
         lV66WWGeral_FuncaoDS_10_Funcao_nome3 = "";
         lV67WWGeral_FuncaoDS_11_Funcao_uonom3 = "";
         lV68WWGeral_FuncaoDS_12_Tffuncao_nome = "";
         lV72WWGeral_FuncaoDS_16_Tffuncao_uonom = "";
         AV57WWGeral_FuncaoDS_1_Dynamicfiltersselector1 = "";
         AV58WWGeral_FuncaoDS_2_Funcao_nome1 = "";
         AV59WWGeral_FuncaoDS_3_Funcao_uonom1 = "";
         AV61WWGeral_FuncaoDS_5_Dynamicfiltersselector2 = "";
         AV62WWGeral_FuncaoDS_6_Funcao_nome2 = "";
         AV63WWGeral_FuncaoDS_7_Funcao_uonom2 = "";
         AV65WWGeral_FuncaoDS_9_Dynamicfiltersselector3 = "";
         AV66WWGeral_FuncaoDS_10_Funcao_nome3 = "";
         AV67WWGeral_FuncaoDS_11_Funcao_uonom3 = "";
         AV69WWGeral_FuncaoDS_13_Tffuncao_nome_sel = "";
         AV68WWGeral_FuncaoDS_12_Tffuncao_nome = "";
         AV73WWGeral_FuncaoDS_17_Tffuncao_uonom_sel = "";
         AV72WWGeral_FuncaoDS_16_Tffuncao_uonom = "";
         H00CI2_A630Funcao_Ativo = new bool[] {false} ;
         H00CI2_A636Funcao_UONom = new String[] {""} ;
         H00CI2_n636Funcao_UONom = new bool[] {false} ;
         H00CI2_A635Funcao_UOCod = new int[1] ;
         H00CI2_n635Funcao_UOCod = new bool[] {false} ;
         H00CI2_A619GrupoFuncao_Codigo = new int[1] ;
         H00CI2_A622Funcao_Nome = new String[] {""} ;
         H00CI2_A621Funcao_Codigo = new int[1] ;
         H00CI3_AGRID_nRecordCount = new long[1] ;
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         GridRow = new GXWebRow();
         imgInsert_Link = "";
         AV27Session = context.GetSession();
         AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV7HTTPRequest = new GxHttpRequest( context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblGeral_funcaotitle_Jsonclick = "";
         lblOrderedtext_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         imgInsert_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wwgeral_funcao__default(),
            new Object[][] {
                new Object[] {
               H00CI2_A630Funcao_Ativo, H00CI2_A636Funcao_UONom, H00CI2_n636Funcao_UONom, H00CI2_A635Funcao_UOCod, H00CI2_n635Funcao_UOCod, H00CI2_A619GrupoFuncao_Codigo, H00CI2_A622Funcao_Nome, H00CI2_A621Funcao_Codigo
               }
               , new Object[] {
               H00CI3_AGRID_nRecordCount
               }
            }
         );
         AV77Pgmname = "WWGeral_Funcao";
         /* GeneXus formulas. */
         AV77Pgmname = "WWGeral_Funcao";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_76 ;
      private short nGXsfl_76_idx=1 ;
      private short AV13OrderedBy ;
      private short AV49TFFuncao_Ativo_Sel ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_76_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short AV74WWGeral_FuncaoDS_18_Tffuncao_ativo_sel ;
      private short edtFuncao_Nome_Titleformat ;
      private short edtGrupoFuncao_Codigo_Titleformat ;
      private short edtFuncao_UONom_Titleformat ;
      private short chkFuncao_Ativo_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int subGrid_Rows ;
      private int AV41TFGrupoFuncao_Codigo ;
      private int AV42TFGrupoFuncao_Codigo_To ;
      private int A621Funcao_Codigo ;
      private int A635Funcao_UOCod ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_funcao_nome_Datalistupdateminimumcharacters ;
      private int Ddo_funcao_uonom_Datalistupdateminimumcharacters ;
      private int edtavTffuncao_nome_Visible ;
      private int edtavTffuncao_nome_sel_Visible ;
      private int edtavTfgrupofuncao_codigo_Visible ;
      private int edtavTfgrupofuncao_codigo_to_Visible ;
      private int edtavTffuncao_uonom_Visible ;
      private int edtavTffuncao_uonom_sel_Visible ;
      private int edtavTffuncao_ativo_sel_Visible ;
      private int edtavDdo_funcao_nometitlecontrolidtoreplace_Visible ;
      private int edtavDdo_grupofuncao_codigotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_funcao_uonomtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_funcao_ativotitlecontrolidtoreplace_Visible ;
      private int A619GrupoFuncao_Codigo ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int AV70WWGeral_FuncaoDS_14_Tfgrupofuncao_codigo ;
      private int AV71WWGeral_FuncaoDS_15_Tfgrupofuncao_codigo_to ;
      private int edtavOrdereddsc_Visible ;
      private int AV52PageToGo ;
      private int edtavUpdate_Enabled ;
      private int edtavDelete_Enabled ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int imgInsert_Enabled ;
      private int edtavFuncao_nome1_Visible ;
      private int edtavFuncao_uonom1_Visible ;
      private int edtavFuncao_nome2_Visible ;
      private int edtavFuncao_uonom2_Visible ;
      private int edtavFuncao_nome3_Visible ;
      private int edtavFuncao_uonom3_Visible ;
      private int AV78GXV1 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long AV53GridCurrentPage ;
      private long AV54GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_funcao_nome_Activeeventkey ;
      private String Ddo_funcao_nome_Filteredtext_get ;
      private String Ddo_funcao_nome_Selectedvalue_get ;
      private String Ddo_grupofuncao_codigo_Activeeventkey ;
      private String Ddo_grupofuncao_codigo_Filteredtext_get ;
      private String Ddo_grupofuncao_codigo_Filteredtextto_get ;
      private String Ddo_funcao_uonom_Activeeventkey ;
      private String Ddo_funcao_uonom_Filteredtext_get ;
      private String Ddo_funcao_uonom_Selectedvalue_get ;
      private String Ddo_funcao_ativo_Activeeventkey ;
      private String Ddo_funcao_ativo_Selectedvalue_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_76_idx="0001" ;
      private String AV29Funcao_UONom1 ;
      private String AV31Funcao_UONom2 ;
      private String AV33Funcao_UONom3 ;
      private String AV45TFFuncao_UONom ;
      private String AV46TFFuncao_UONom_Sel ;
      private String AV77Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_funcao_nome_Caption ;
      private String Ddo_funcao_nome_Tooltip ;
      private String Ddo_funcao_nome_Cls ;
      private String Ddo_funcao_nome_Filteredtext_set ;
      private String Ddo_funcao_nome_Selectedvalue_set ;
      private String Ddo_funcao_nome_Dropdownoptionstype ;
      private String Ddo_funcao_nome_Titlecontrolidtoreplace ;
      private String Ddo_funcao_nome_Sortedstatus ;
      private String Ddo_funcao_nome_Filtertype ;
      private String Ddo_funcao_nome_Datalisttype ;
      private String Ddo_funcao_nome_Datalistproc ;
      private String Ddo_funcao_nome_Sortasc ;
      private String Ddo_funcao_nome_Sortdsc ;
      private String Ddo_funcao_nome_Loadingdata ;
      private String Ddo_funcao_nome_Cleanfilter ;
      private String Ddo_funcao_nome_Noresultsfound ;
      private String Ddo_funcao_nome_Searchbuttontext ;
      private String Ddo_grupofuncao_codigo_Caption ;
      private String Ddo_grupofuncao_codigo_Tooltip ;
      private String Ddo_grupofuncao_codigo_Cls ;
      private String Ddo_grupofuncao_codigo_Filteredtext_set ;
      private String Ddo_grupofuncao_codigo_Filteredtextto_set ;
      private String Ddo_grupofuncao_codigo_Dropdownoptionstype ;
      private String Ddo_grupofuncao_codigo_Titlecontrolidtoreplace ;
      private String Ddo_grupofuncao_codigo_Sortedstatus ;
      private String Ddo_grupofuncao_codigo_Filtertype ;
      private String Ddo_grupofuncao_codigo_Sortasc ;
      private String Ddo_grupofuncao_codigo_Sortdsc ;
      private String Ddo_grupofuncao_codigo_Cleanfilter ;
      private String Ddo_grupofuncao_codigo_Rangefilterfrom ;
      private String Ddo_grupofuncao_codigo_Rangefilterto ;
      private String Ddo_grupofuncao_codigo_Searchbuttontext ;
      private String Ddo_funcao_uonom_Caption ;
      private String Ddo_funcao_uonom_Tooltip ;
      private String Ddo_funcao_uonom_Cls ;
      private String Ddo_funcao_uonom_Filteredtext_set ;
      private String Ddo_funcao_uonom_Selectedvalue_set ;
      private String Ddo_funcao_uonom_Dropdownoptionstype ;
      private String Ddo_funcao_uonom_Titlecontrolidtoreplace ;
      private String Ddo_funcao_uonom_Sortedstatus ;
      private String Ddo_funcao_uonom_Filtertype ;
      private String Ddo_funcao_uonom_Datalisttype ;
      private String Ddo_funcao_uonom_Datalistproc ;
      private String Ddo_funcao_uonom_Sortasc ;
      private String Ddo_funcao_uonom_Sortdsc ;
      private String Ddo_funcao_uonom_Loadingdata ;
      private String Ddo_funcao_uonom_Cleanfilter ;
      private String Ddo_funcao_uonom_Noresultsfound ;
      private String Ddo_funcao_uonom_Searchbuttontext ;
      private String Ddo_funcao_ativo_Caption ;
      private String Ddo_funcao_ativo_Tooltip ;
      private String Ddo_funcao_ativo_Cls ;
      private String Ddo_funcao_ativo_Selectedvalue_set ;
      private String Ddo_funcao_ativo_Dropdownoptionstype ;
      private String Ddo_funcao_ativo_Titlecontrolidtoreplace ;
      private String Ddo_funcao_ativo_Sortedstatus ;
      private String Ddo_funcao_ativo_Datalisttype ;
      private String Ddo_funcao_ativo_Datalistfixedvalues ;
      private String Ddo_funcao_ativo_Sortasc ;
      private String Ddo_funcao_ativo_Sortdsc ;
      private String Ddo_funcao_ativo_Cleanfilter ;
      private String Ddo_funcao_ativo_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String edtavTffuncao_nome_Internalname ;
      private String edtavTffuncao_nome_Jsonclick ;
      private String edtavTffuncao_nome_sel_Internalname ;
      private String edtavTffuncao_nome_sel_Jsonclick ;
      private String edtavTfgrupofuncao_codigo_Internalname ;
      private String edtavTfgrupofuncao_codigo_Jsonclick ;
      private String edtavTfgrupofuncao_codigo_to_Internalname ;
      private String edtavTfgrupofuncao_codigo_to_Jsonclick ;
      private String edtavTffuncao_uonom_Internalname ;
      private String edtavTffuncao_uonom_Jsonclick ;
      private String edtavTffuncao_uonom_sel_Internalname ;
      private String edtavTffuncao_uonom_sel_Jsonclick ;
      private String edtavTffuncao_ativo_sel_Internalname ;
      private String edtavTffuncao_ativo_sel_Jsonclick ;
      private String edtavDdo_funcao_nometitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_grupofuncao_codigotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_funcao_uonomtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_funcao_ativotitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavUpdate_Internalname ;
      private String edtavDelete_Internalname ;
      private String edtFuncao_Codigo_Internalname ;
      private String edtFuncao_Nome_Internalname ;
      private String edtGrupoFuncao_Codigo_Internalname ;
      private String edtFuncao_UOCod_Internalname ;
      private String A636Funcao_UONom ;
      private String edtFuncao_UONom_Internalname ;
      private String chkFuncao_Ativo_Internalname ;
      private String GXCCtl ;
      private String cmbavOrderedby_Internalname ;
      private String scmdbuf ;
      private String lV59WWGeral_FuncaoDS_3_Funcao_uonom1 ;
      private String lV63WWGeral_FuncaoDS_7_Funcao_uonom2 ;
      private String lV67WWGeral_FuncaoDS_11_Funcao_uonom3 ;
      private String lV72WWGeral_FuncaoDS_16_Tffuncao_uonom ;
      private String AV59WWGeral_FuncaoDS_3_Funcao_uonom1 ;
      private String AV63WWGeral_FuncaoDS_7_Funcao_uonom2 ;
      private String AV67WWGeral_FuncaoDS_11_Funcao_uonom3 ;
      private String AV73WWGeral_FuncaoDS_17_Tffuncao_uonom_sel ;
      private String AV72WWGeral_FuncaoDS_16_Tffuncao_uonom ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String edtavFuncao_nome1_Internalname ;
      private String edtavFuncao_uonom1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String edtavFuncao_nome2_Internalname ;
      private String edtavFuncao_uonom2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String edtavFuncao_nome3_Internalname ;
      private String edtavFuncao_uonom3_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_funcao_nome_Internalname ;
      private String Ddo_grupofuncao_codigo_Internalname ;
      private String Ddo_funcao_uonom_Internalname ;
      private String Ddo_funcao_ativo_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String edtFuncao_Nome_Title ;
      private String edtGrupoFuncao_Codigo_Title ;
      private String edtFuncao_UONom_Title ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavUpdate_Link ;
      private String edtavDelete_Tooltiptext ;
      private String edtavDelete_Link ;
      private String edtFuncao_Nome_Link ;
      private String edtFuncao_UONom_Link ;
      private String imgInsert_Link ;
      private String imgInsert_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTableheader_Internalname ;
      private String lblGeral_funcaotitle_Internalname ;
      private String lblGeral_funcaotitle_Jsonclick ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String edtavFuncao_nome1_Jsonclick ;
      private String edtavFuncao_uonom1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String edtavFuncao_nome2_Jsonclick ;
      private String edtavFuncao_uonom2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String edtavFuncao_nome3_Jsonclick ;
      private String edtavFuncao_uonom3_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String imgInsert_Jsonclick ;
      private String sGXsfl_76_fel_idx="0001" ;
      private String ROClassString ;
      private String edtFuncao_Codigo_Jsonclick ;
      private String edtFuncao_Nome_Jsonclick ;
      private String edtGrupoFuncao_Codigo_Jsonclick ;
      private String edtFuncao_UOCod_Jsonclick ;
      private String edtFuncao_UONom_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool AV17DynamicFiltersEnabled2 ;
      private bool AV20DynamicFiltersEnabled3 ;
      private bool AV24DynamicFiltersIgnoreFirst ;
      private bool AV23DynamicFiltersRemoving ;
      private bool n635Funcao_UOCod ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_funcao_nome_Includesortasc ;
      private bool Ddo_funcao_nome_Includesortdsc ;
      private bool Ddo_funcao_nome_Includefilter ;
      private bool Ddo_funcao_nome_Filterisrange ;
      private bool Ddo_funcao_nome_Includedatalist ;
      private bool Ddo_grupofuncao_codigo_Includesortasc ;
      private bool Ddo_grupofuncao_codigo_Includesortdsc ;
      private bool Ddo_grupofuncao_codigo_Includefilter ;
      private bool Ddo_grupofuncao_codigo_Filterisrange ;
      private bool Ddo_grupofuncao_codigo_Includedatalist ;
      private bool Ddo_funcao_uonom_Includesortasc ;
      private bool Ddo_funcao_uonom_Includesortdsc ;
      private bool Ddo_funcao_uonom_Includefilter ;
      private bool Ddo_funcao_uonom_Filterisrange ;
      private bool Ddo_funcao_uonom_Includedatalist ;
      private bool Ddo_funcao_ativo_Includesortasc ;
      private bool Ddo_funcao_ativo_Includesortdsc ;
      private bool Ddo_funcao_ativo_Includefilter ;
      private bool Ddo_funcao_ativo_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n636Funcao_UONom ;
      private bool A630Funcao_Ativo ;
      private bool AV60WWGeral_FuncaoDS_4_Dynamicfiltersenabled2 ;
      private bool AV64WWGeral_FuncaoDS_8_Dynamicfiltersenabled3 ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV25Update_IsBlob ;
      private bool AV26Delete_IsBlob ;
      private String AV15DynamicFiltersSelector1 ;
      private String AV16Funcao_Nome1 ;
      private String AV18DynamicFiltersSelector2 ;
      private String AV19Funcao_Nome2 ;
      private String AV21DynamicFiltersSelector3 ;
      private String AV22Funcao_Nome3 ;
      private String AV37TFFuncao_Nome ;
      private String AV38TFFuncao_Nome_Sel ;
      private String AV39ddo_Funcao_NomeTitleControlIdToReplace ;
      private String AV43ddo_GrupoFuncao_CodigoTitleControlIdToReplace ;
      private String AV47ddo_Funcao_UONomTitleControlIdToReplace ;
      private String AV50ddo_Funcao_AtivoTitleControlIdToReplace ;
      private String AV75Update_GXI ;
      private String AV76Delete_GXI ;
      private String A622Funcao_Nome ;
      private String lV58WWGeral_FuncaoDS_2_Funcao_nome1 ;
      private String lV62WWGeral_FuncaoDS_6_Funcao_nome2 ;
      private String lV66WWGeral_FuncaoDS_10_Funcao_nome3 ;
      private String lV68WWGeral_FuncaoDS_12_Tffuncao_nome ;
      private String AV57WWGeral_FuncaoDS_1_Dynamicfiltersselector1 ;
      private String AV58WWGeral_FuncaoDS_2_Funcao_nome1 ;
      private String AV61WWGeral_FuncaoDS_5_Dynamicfiltersselector2 ;
      private String AV62WWGeral_FuncaoDS_6_Funcao_nome2 ;
      private String AV65WWGeral_FuncaoDS_9_Dynamicfiltersselector3 ;
      private String AV66WWGeral_FuncaoDS_10_Funcao_nome3 ;
      private String AV69WWGeral_FuncaoDS_13_Tffuncao_nome_sel ;
      private String AV68WWGeral_FuncaoDS_12_Tffuncao_nome ;
      private String AV25Update ;
      private String AV26Delete ;
      private String imgInsert_Bitmap ;
      private IGxSession AV27Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCheckbox chkFuncao_Ativo ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private bool[] H00CI2_A630Funcao_Ativo ;
      private String[] H00CI2_A636Funcao_UONom ;
      private bool[] H00CI2_n636Funcao_UONom ;
      private int[] H00CI2_A635Funcao_UOCod ;
      private bool[] H00CI2_n635Funcao_UOCod ;
      private int[] H00CI2_A619GrupoFuncao_Codigo ;
      private String[] H00CI2_A622Funcao_Nome ;
      private int[] H00CI2_A621Funcao_Codigo ;
      private long[] H00CI3_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV7HTTPRequest ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV36Funcao_NomeTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV40GrupoFuncao_CodigoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV44Funcao_UONomTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV48Funcao_AtivoTitleFilterData ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV11GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV51DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class wwgeral_funcao__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00CI2( IGxContext context ,
                                             String AV57WWGeral_FuncaoDS_1_Dynamicfiltersselector1 ,
                                             String AV58WWGeral_FuncaoDS_2_Funcao_nome1 ,
                                             String AV59WWGeral_FuncaoDS_3_Funcao_uonom1 ,
                                             bool AV60WWGeral_FuncaoDS_4_Dynamicfiltersenabled2 ,
                                             String AV61WWGeral_FuncaoDS_5_Dynamicfiltersselector2 ,
                                             String AV62WWGeral_FuncaoDS_6_Funcao_nome2 ,
                                             String AV63WWGeral_FuncaoDS_7_Funcao_uonom2 ,
                                             bool AV64WWGeral_FuncaoDS_8_Dynamicfiltersenabled3 ,
                                             String AV65WWGeral_FuncaoDS_9_Dynamicfiltersselector3 ,
                                             String AV66WWGeral_FuncaoDS_10_Funcao_nome3 ,
                                             String AV67WWGeral_FuncaoDS_11_Funcao_uonom3 ,
                                             String AV69WWGeral_FuncaoDS_13_Tffuncao_nome_sel ,
                                             String AV68WWGeral_FuncaoDS_12_Tffuncao_nome ,
                                             int AV70WWGeral_FuncaoDS_14_Tfgrupofuncao_codigo ,
                                             int AV71WWGeral_FuncaoDS_15_Tfgrupofuncao_codigo_to ,
                                             String AV73WWGeral_FuncaoDS_17_Tffuncao_uonom_sel ,
                                             String AV72WWGeral_FuncaoDS_16_Tffuncao_uonom ,
                                             short AV74WWGeral_FuncaoDS_18_Tffuncao_ativo_sel ,
                                             String A622Funcao_Nome ,
                                             String A636Funcao_UONom ,
                                             int A619GrupoFuncao_Codigo ,
                                             bool A630Funcao_Ativo ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [17] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T1.[Funcao_Ativo], T2.[UnidadeOrganizacional_Nome] AS Funcao_UONom, T1.[Funcao_UOCod] AS Funcao_UOCod, T1.[GrupoFuncao_Codigo], T1.[Funcao_Nome], T1.[Funcao_Codigo]";
         sFromString = " FROM ([Geral_Funcao] T1 WITH (NOLOCK) LEFT JOIN [Geral_UnidadeOrganizacional] T2 WITH (NOLOCK) ON T2.[UnidadeOrganizacional_Codigo] = T1.[Funcao_UOCod])";
         sOrderString = "";
         if ( ( StringUtil.StrCmp(AV57WWGeral_FuncaoDS_1_Dynamicfiltersselector1, "FUNCAO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV58WWGeral_FuncaoDS_2_Funcao_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Funcao_Nome] like '%' + @lV58WWGeral_FuncaoDS_2_Funcao_nome1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Funcao_Nome] like '%' + @lV58WWGeral_FuncaoDS_2_Funcao_nome1 + '%')";
            }
         }
         else
         {
            GXv_int2[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV57WWGeral_FuncaoDS_1_Dynamicfiltersselector1, "FUNCAO_UONOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59WWGeral_FuncaoDS_3_Funcao_uonom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[UnidadeOrganizacional_Nome] like @lV59WWGeral_FuncaoDS_3_Funcao_uonom1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[UnidadeOrganizacional_Nome] like @lV59WWGeral_FuncaoDS_3_Funcao_uonom1 + '%')";
            }
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( AV60WWGeral_FuncaoDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV61WWGeral_FuncaoDS_5_Dynamicfiltersselector2, "FUNCAO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62WWGeral_FuncaoDS_6_Funcao_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Funcao_Nome] like '%' + @lV62WWGeral_FuncaoDS_6_Funcao_nome2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Funcao_Nome] like '%' + @lV62WWGeral_FuncaoDS_6_Funcao_nome2 + '%')";
            }
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( AV60WWGeral_FuncaoDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV61WWGeral_FuncaoDS_5_Dynamicfiltersselector2, "FUNCAO_UONOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63WWGeral_FuncaoDS_7_Funcao_uonom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[UnidadeOrganizacional_Nome] like @lV63WWGeral_FuncaoDS_7_Funcao_uonom2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[UnidadeOrganizacional_Nome] like @lV63WWGeral_FuncaoDS_7_Funcao_uonom2 + '%')";
            }
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( AV64WWGeral_FuncaoDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV65WWGeral_FuncaoDS_9_Dynamicfiltersselector3, "FUNCAO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66WWGeral_FuncaoDS_10_Funcao_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Funcao_Nome] like '%' + @lV66WWGeral_FuncaoDS_10_Funcao_nome3 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Funcao_Nome] like '%' + @lV66WWGeral_FuncaoDS_10_Funcao_nome3 + '%')";
            }
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( AV64WWGeral_FuncaoDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV65WWGeral_FuncaoDS_9_Dynamicfiltersselector3, "FUNCAO_UONOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67WWGeral_FuncaoDS_11_Funcao_uonom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[UnidadeOrganizacional_Nome] like @lV67WWGeral_FuncaoDS_11_Funcao_uonom3 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[UnidadeOrganizacional_Nome] like @lV67WWGeral_FuncaoDS_11_Funcao_uonom3 + '%')";
            }
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV69WWGeral_FuncaoDS_13_Tffuncao_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV68WWGeral_FuncaoDS_12_Tffuncao_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Funcao_Nome] like @lV68WWGeral_FuncaoDS_12_Tffuncao_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Funcao_Nome] like @lV68WWGeral_FuncaoDS_12_Tffuncao_nome)";
            }
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV69WWGeral_FuncaoDS_13_Tffuncao_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Funcao_Nome] = @AV69WWGeral_FuncaoDS_13_Tffuncao_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Funcao_Nome] = @AV69WWGeral_FuncaoDS_13_Tffuncao_nome_sel)";
            }
         }
         else
         {
            GXv_int2[7] = 1;
         }
         if ( ! (0==AV70WWGeral_FuncaoDS_14_Tfgrupofuncao_codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[GrupoFuncao_Codigo] >= @AV70WWGeral_FuncaoDS_14_Tfgrupofuncao_codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[GrupoFuncao_Codigo] >= @AV70WWGeral_FuncaoDS_14_Tfgrupofuncao_codigo)";
            }
         }
         else
         {
            GXv_int2[8] = 1;
         }
         if ( ! (0==AV71WWGeral_FuncaoDS_15_Tfgrupofuncao_codigo_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[GrupoFuncao_Codigo] <= @AV71WWGeral_FuncaoDS_15_Tfgrupofuncao_codigo_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[GrupoFuncao_Codigo] <= @AV71WWGeral_FuncaoDS_15_Tfgrupofuncao_codigo_to)";
            }
         }
         else
         {
            GXv_int2[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV73WWGeral_FuncaoDS_17_Tffuncao_uonom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV72WWGeral_FuncaoDS_16_Tffuncao_uonom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[UnidadeOrganizacional_Nome] like @lV72WWGeral_FuncaoDS_16_Tffuncao_uonom)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[UnidadeOrganizacional_Nome] like @lV72WWGeral_FuncaoDS_16_Tffuncao_uonom)";
            }
         }
         else
         {
            GXv_int2[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73WWGeral_FuncaoDS_17_Tffuncao_uonom_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[UnidadeOrganizacional_Nome] = @AV73WWGeral_FuncaoDS_17_Tffuncao_uonom_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[UnidadeOrganizacional_Nome] = @AV73WWGeral_FuncaoDS_17_Tffuncao_uonom_sel)";
            }
         }
         else
         {
            GXv_int2[11] = 1;
         }
         if ( AV74WWGeral_FuncaoDS_18_Tffuncao_ativo_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Funcao_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Funcao_Ativo] = 1)";
            }
         }
         if ( AV74WWGeral_FuncaoDS_18_Tffuncao_ativo_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Funcao_Ativo] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Funcao_Ativo] = 0)";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            sWhereString = " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Funcao_Nome]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Funcao_Nome] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[GrupoFuncao_Codigo]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[GrupoFuncao_Codigo] DESC";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T2.[UnidadeOrganizacional_Nome]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T2.[UnidadeOrganizacional_Nome] DESC";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Funcao_Ativo]";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Funcao_Ativo] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Funcao_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H00CI3( IGxContext context ,
                                             String AV57WWGeral_FuncaoDS_1_Dynamicfiltersselector1 ,
                                             String AV58WWGeral_FuncaoDS_2_Funcao_nome1 ,
                                             String AV59WWGeral_FuncaoDS_3_Funcao_uonom1 ,
                                             bool AV60WWGeral_FuncaoDS_4_Dynamicfiltersenabled2 ,
                                             String AV61WWGeral_FuncaoDS_5_Dynamicfiltersselector2 ,
                                             String AV62WWGeral_FuncaoDS_6_Funcao_nome2 ,
                                             String AV63WWGeral_FuncaoDS_7_Funcao_uonom2 ,
                                             bool AV64WWGeral_FuncaoDS_8_Dynamicfiltersenabled3 ,
                                             String AV65WWGeral_FuncaoDS_9_Dynamicfiltersselector3 ,
                                             String AV66WWGeral_FuncaoDS_10_Funcao_nome3 ,
                                             String AV67WWGeral_FuncaoDS_11_Funcao_uonom3 ,
                                             String AV69WWGeral_FuncaoDS_13_Tffuncao_nome_sel ,
                                             String AV68WWGeral_FuncaoDS_12_Tffuncao_nome ,
                                             int AV70WWGeral_FuncaoDS_14_Tfgrupofuncao_codigo ,
                                             int AV71WWGeral_FuncaoDS_15_Tfgrupofuncao_codigo_to ,
                                             String AV73WWGeral_FuncaoDS_17_Tffuncao_uonom_sel ,
                                             String AV72WWGeral_FuncaoDS_16_Tffuncao_uonom ,
                                             short AV74WWGeral_FuncaoDS_18_Tffuncao_ativo_sel ,
                                             String A622Funcao_Nome ,
                                             String A636Funcao_UONom ,
                                             int A619GrupoFuncao_Codigo ,
                                             bool A630Funcao_Ativo ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [12] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM ([Geral_Funcao] T1 WITH (NOLOCK) LEFT JOIN [Geral_UnidadeOrganizacional] T2 WITH (NOLOCK) ON T2.[UnidadeOrganizacional_Codigo] = T1.[Funcao_UOCod])";
         if ( ( StringUtil.StrCmp(AV57WWGeral_FuncaoDS_1_Dynamicfiltersselector1, "FUNCAO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV58WWGeral_FuncaoDS_2_Funcao_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Funcao_Nome] like '%' + @lV58WWGeral_FuncaoDS_2_Funcao_nome1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Funcao_Nome] like '%' + @lV58WWGeral_FuncaoDS_2_Funcao_nome1 + '%')";
            }
         }
         else
         {
            GXv_int4[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV57WWGeral_FuncaoDS_1_Dynamicfiltersselector1, "FUNCAO_UONOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59WWGeral_FuncaoDS_3_Funcao_uonom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[UnidadeOrganizacional_Nome] like @lV59WWGeral_FuncaoDS_3_Funcao_uonom1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[UnidadeOrganizacional_Nome] like @lV59WWGeral_FuncaoDS_3_Funcao_uonom1 + '%')";
            }
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( AV60WWGeral_FuncaoDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV61WWGeral_FuncaoDS_5_Dynamicfiltersselector2, "FUNCAO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62WWGeral_FuncaoDS_6_Funcao_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Funcao_Nome] like '%' + @lV62WWGeral_FuncaoDS_6_Funcao_nome2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Funcao_Nome] like '%' + @lV62WWGeral_FuncaoDS_6_Funcao_nome2 + '%')";
            }
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( AV60WWGeral_FuncaoDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV61WWGeral_FuncaoDS_5_Dynamicfiltersselector2, "FUNCAO_UONOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63WWGeral_FuncaoDS_7_Funcao_uonom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[UnidadeOrganizacional_Nome] like @lV63WWGeral_FuncaoDS_7_Funcao_uonom2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[UnidadeOrganizacional_Nome] like @lV63WWGeral_FuncaoDS_7_Funcao_uonom2 + '%')";
            }
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( AV64WWGeral_FuncaoDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV65WWGeral_FuncaoDS_9_Dynamicfiltersselector3, "FUNCAO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66WWGeral_FuncaoDS_10_Funcao_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Funcao_Nome] like '%' + @lV66WWGeral_FuncaoDS_10_Funcao_nome3 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Funcao_Nome] like '%' + @lV66WWGeral_FuncaoDS_10_Funcao_nome3 + '%')";
            }
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( AV64WWGeral_FuncaoDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV65WWGeral_FuncaoDS_9_Dynamicfiltersselector3, "FUNCAO_UONOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67WWGeral_FuncaoDS_11_Funcao_uonom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[UnidadeOrganizacional_Nome] like @lV67WWGeral_FuncaoDS_11_Funcao_uonom3 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[UnidadeOrganizacional_Nome] like @lV67WWGeral_FuncaoDS_11_Funcao_uonom3 + '%')";
            }
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV69WWGeral_FuncaoDS_13_Tffuncao_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV68WWGeral_FuncaoDS_12_Tffuncao_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Funcao_Nome] like @lV68WWGeral_FuncaoDS_12_Tffuncao_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Funcao_Nome] like @lV68WWGeral_FuncaoDS_12_Tffuncao_nome)";
            }
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV69WWGeral_FuncaoDS_13_Tffuncao_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Funcao_Nome] = @AV69WWGeral_FuncaoDS_13_Tffuncao_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Funcao_Nome] = @AV69WWGeral_FuncaoDS_13_Tffuncao_nome_sel)";
            }
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( ! (0==AV70WWGeral_FuncaoDS_14_Tfgrupofuncao_codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[GrupoFuncao_Codigo] >= @AV70WWGeral_FuncaoDS_14_Tfgrupofuncao_codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[GrupoFuncao_Codigo] >= @AV70WWGeral_FuncaoDS_14_Tfgrupofuncao_codigo)";
            }
         }
         else
         {
            GXv_int4[8] = 1;
         }
         if ( ! (0==AV71WWGeral_FuncaoDS_15_Tfgrupofuncao_codigo_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[GrupoFuncao_Codigo] <= @AV71WWGeral_FuncaoDS_15_Tfgrupofuncao_codigo_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[GrupoFuncao_Codigo] <= @AV71WWGeral_FuncaoDS_15_Tfgrupofuncao_codigo_to)";
            }
         }
         else
         {
            GXv_int4[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV73WWGeral_FuncaoDS_17_Tffuncao_uonom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV72WWGeral_FuncaoDS_16_Tffuncao_uonom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[UnidadeOrganizacional_Nome] like @lV72WWGeral_FuncaoDS_16_Tffuncao_uonom)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[UnidadeOrganizacional_Nome] like @lV72WWGeral_FuncaoDS_16_Tffuncao_uonom)";
            }
         }
         else
         {
            GXv_int4[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73WWGeral_FuncaoDS_17_Tffuncao_uonom_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[UnidadeOrganizacional_Nome] = @AV73WWGeral_FuncaoDS_17_Tffuncao_uonom_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[UnidadeOrganizacional_Nome] = @AV73WWGeral_FuncaoDS_17_Tffuncao_uonom_sel)";
            }
         }
         else
         {
            GXv_int4[11] = 1;
         }
         if ( AV74WWGeral_FuncaoDS_18_Tffuncao_ativo_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Funcao_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Funcao_Ativo] = 1)";
            }
         }
         if ( AV74WWGeral_FuncaoDS_18_Tffuncao_ativo_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Funcao_Ativo] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Funcao_Ativo] = 0)";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00CI2(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (int)dynConstraints[13] , (int)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (short)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (int)dynConstraints[20] , (bool)dynConstraints[21] , (short)dynConstraints[22] , (bool)dynConstraints[23] );
               case 1 :
                     return conditional_H00CI3(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (int)dynConstraints[13] , (int)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (short)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (int)dynConstraints[20] , (bool)dynConstraints[21] , (short)dynConstraints[22] , (bool)dynConstraints[23] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00CI2 ;
          prmH00CI2 = new Object[] {
          new Object[] {"@lV58WWGeral_FuncaoDS_2_Funcao_nome1",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV59WWGeral_FuncaoDS_3_Funcao_uonom1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV62WWGeral_FuncaoDS_6_Funcao_nome2",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV63WWGeral_FuncaoDS_7_Funcao_uonom2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV66WWGeral_FuncaoDS_10_Funcao_nome3",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV67WWGeral_FuncaoDS_11_Funcao_uonom3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV68WWGeral_FuncaoDS_12_Tffuncao_nome",SqlDbType.VarChar,100,0} ,
          new Object[] {"@AV69WWGeral_FuncaoDS_13_Tffuncao_nome_sel",SqlDbType.VarChar,100,0} ,
          new Object[] {"@AV70WWGeral_FuncaoDS_14_Tfgrupofuncao_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV71WWGeral_FuncaoDS_15_Tfgrupofuncao_codigo_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV72WWGeral_FuncaoDS_16_Tffuncao_uonom",SqlDbType.Char,50,0} ,
          new Object[] {"@AV73WWGeral_FuncaoDS_17_Tffuncao_uonom_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00CI3 ;
          prmH00CI3 = new Object[] {
          new Object[] {"@lV58WWGeral_FuncaoDS_2_Funcao_nome1",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV59WWGeral_FuncaoDS_3_Funcao_uonom1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV62WWGeral_FuncaoDS_6_Funcao_nome2",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV63WWGeral_FuncaoDS_7_Funcao_uonom2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV66WWGeral_FuncaoDS_10_Funcao_nome3",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV67WWGeral_FuncaoDS_11_Funcao_uonom3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV68WWGeral_FuncaoDS_12_Tffuncao_nome",SqlDbType.VarChar,100,0} ,
          new Object[] {"@AV69WWGeral_FuncaoDS_13_Tffuncao_nome_sel",SqlDbType.VarChar,100,0} ,
          new Object[] {"@AV70WWGeral_FuncaoDS_14_Tfgrupofuncao_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV71WWGeral_FuncaoDS_15_Tfgrupofuncao_codigo_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV72WWGeral_FuncaoDS_16_Tffuncao_uonom",SqlDbType.Char,50,0} ,
          new Object[] {"@AV73WWGeral_FuncaoDS_17_Tffuncao_uonom_sel",SqlDbType.Char,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00CI2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00CI2,11,0,true,false )
             ,new CursorDef("H00CI3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00CI3,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((String[]) buf[6])[0] = rslt.getVarchar(5) ;
                ((int[]) buf[7])[0] = rslt.getInt(6) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[25]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[26]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[29]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[30]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[31]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[32]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[33]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[20]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[21]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                return;
       }
    }

 }

}
