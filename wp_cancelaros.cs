/*
               File: WP_CancelarOS
        Description: Cancelar solicita��o da OS
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 23:46:25.89
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_cancelaros : GXHttpHandler, System.Web.SessionState.IRequiresSessionState
   {
      public wp_cancelaros( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_cancelaros( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Codigo ,
                           String aP1_OS ,
                           int aP2_UserId )
      {
         this.AV8Codigo = aP0_Codigo;
         this.AV35OS = aP1_OS;
         this.AV36UserId = aP2_UserId;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         dynavNaoconformidade_codigo = new GXCombobox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vNAOCONFORMIDADE_CODIGO") == 0 )
            {
               ajax_req_read_hidden_sdt(GetNextPar( ), AV34WWPContext);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvNAOCONFORMIDADE_CODIGOJX2( AV34WWPContext) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV8Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV8Codigo), "ZZZZZ9")));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV35OS = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35OS", AV35OS);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vOS", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV35OS, "@!"))));
                  AV36UserId = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36UserId", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36UserId), 6, 0)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vUSERID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV36UserId), "ZZZZZ9")));
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            ValidateSpaRequest();
            PAJX2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               context.Gx_err = 0;
               WSJX2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  WEJX2( ) ;
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202042823462593");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wp_cancelaros.aspx") + "?" + UrlEncode("" +AV8Codigo) + "," + UrlEncode(StringUtil.RTrim(AV35OS)) + "," + UrlEncode("" +AV36UserId)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "vCODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV8Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vUSERID", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV36UserId), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vOS", AV35OS);
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vWWPCONTEXT", AV34WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vWWPCONTEXT", AV34WWPContext);
         }
         GxWebStd.gx_hidden_field( context, "gxhash_vCODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV8Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vOS", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV35OS, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vUSERID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV36UserId), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vCODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV8Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vOS", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV35OS, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vUSERID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV36UserId), "ZZZZZ9")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormJX2( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
         context.WriteHtmlTextNl( "</body>") ;
         context.WriteHtmlTextNl( "</html>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
      }

      public override String GetPgmname( )
      {
         return "WP_CancelarOS" ;
      }

      public override String GetPgmdesc( )
      {
         return "Cancelar solicita��o da OS" ;
      }

      protected void WBJX0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            RenderHtmlHeaders( ) ;
            RenderHtmlOpenForm( ) ;
            wb_table1_2_JX2( true) ;
         }
         else
         {
            wb_table1_2_JX2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_JX2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbjava_Internalname, lblTbjava_Caption, "", "", lblTbjava_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", lblTbjava_Visible, 1, 1, "HLP_WP_CancelarOS.htm");
         }
         wbLoad = true;
      }

      protected void STARTJX2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Cancelar solicita��o da OS", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPJX0( ) ;
      }

      protected void WSJX2( )
      {
         STARTJX2( ) ;
         EVTJX2( ) ;
      }

      protected void EVTJX2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E11JX2 */
                           E11JX2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E12JX2 */
                           E12JX2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( ! wbErr )
                           {
                              Rfr0gs = false;
                              if ( ! Rfr0gs )
                              {
                                 /* Execute user event: E13JX2 */
                                 E13JX2 ();
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DOFECHAR'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E14JX2 */
                           E14JX2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E15JX2 */
                           E15JX2 ();
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           dynload_actions( ) ;
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void WEJX2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormJX2( ) ;
            }
         }
      }

      protected void PAJX2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            dynavNaoconformidade_codigo.Name = "vNAOCONFORMIDADE_CODIGO";
            dynavNaoconformidade_codigo.WebTags = "";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavObservacao_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLVvNAOCONFORMIDADE_CODIGOJX2( wwpbaseobjects.SdtWWPContext AV34WWPContext )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvNAOCONFORMIDADE_CODIGO_dataJX2( AV34WWPContext) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvNAOCONFORMIDADE_CODIGO_htmlJX2( wwpbaseobjects.SdtWWPContext AV34WWPContext )
      {
         int gxdynajaxvalue ;
         GXDLVvNAOCONFORMIDADE_CODIGO_dataJX2( AV34WWPContext) ;
         gxdynajaxindex = 1;
         dynavNaoconformidade_codigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavNaoconformidade_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavNaoconformidade_codigo.ItemCount > 0 )
         {
            AV39NaoConformidade_Codigo = (int)(NumberUtil.Val( dynavNaoconformidade_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV39NaoConformidade_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39NaoConformidade_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39NaoConformidade_Codigo), 6, 0)));
         }
      }

      protected void GXDLVvNAOCONFORMIDADE_CODIGO_dataJX2( wwpbaseobjects.SdtWWPContext AV34WWPContext )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhuma)");
         /* Using cursor H00JX2 */
         pr_default.execute(0, new Object[] {AV34WWPContext.gxTpr_Areatrabalho_codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00JX2_A426NaoConformidade_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00JX2_A427NaoConformidade_Nome[0]));
            pr_default.readNext(0);
         }
         pr_default.close(0);
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( dynavNaoconformidade_codigo.ItemCount > 0 )
         {
            AV39NaoConformidade_Codigo = (int)(NumberUtil.Val( dynavNaoconformidade_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV39NaoConformidade_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39NaoConformidade_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39NaoConformidade_Codigo), 6, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFJX2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      protected void RFJX2( )
      {
         initialize_formulas( ) ;
         /* Execute user event: E12JX2 */
         E12JX2 ();
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Execute user event: E15JX2 */
            E15JX2 ();
            WBJX0( ) ;
         }
      }

      protected void STRUPJX0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11JX2 */
         E11JX2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         GXVvNAOCONFORMIDADE_CODIGO_htmlJX2( AV34WWPContext) ;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            AV38Observacao = cgiGet( edtavObservacao_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38Observacao", AV38Observacao);
            dynavNaoconformidade_codigo.CurrentValue = cgiGet( dynavNaoconformidade_codigo_Internalname);
            AV39NaoConformidade_Codigo = (int)(NumberUtil.Val( cgiGet( dynavNaoconformidade_codigo_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39NaoConformidade_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39NaoConformidade_Codigo), 6, 0)));
            /* Read saved values. */
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            GXVvNAOCONFORMIDADE_CODIGO_htmlJX2( AV34WWPContext) ;
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11JX2 */
         E11JX2 ();
         if (returnInSub) return;
      }

      protected void E11JX2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV34WWPContext) ;
         if ( StringUtil.StringSearch( AV35OS, "(", 1) == 0 )
         {
            if ( StringUtil.StringSearch( AV35OS, "Rascunho", 1) == 0 )
            {
               Form.Caption = "Cancelar SS "+StringUtil.Trim( AV35OS);
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
            }
            else
            {
               Form.Caption = "Cancelar "+StringUtil.Trim( AV35OS);
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
            }
         }
         else
         {
            Form.Caption = "Cancelar execu��o da OS "+StringUtil.Trim( AV35OS);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         }
         lblTbjava_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbjava_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTbjava_Visible), 5, 0)));
         bttBtnenter_Visible = (AV34WWPContext.gxTpr_Insert ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnenter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnenter_Visible), 5, 0)));
      }

      protected void E12JX2( )
      {
         /* Refresh Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV34WWPContext) ;
         bttBtnenter_Visible = (AV34WWPContext.gxTpr_Insert ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnenter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnenter_Visible), 5, 0)));
      }

      public void GXEnter( )
      {
         /* Execute user event: E13JX2 */
         E13JX2 ();
         if (returnInSub) return;
      }

      protected void E13JX2( )
      {
         /* Enter Routine */
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV38Observacao)) )
         {
            GX_msglist.addItem("Informe a causa do cancelamento!");
         }
         else
         {
            new prc_cancelaros(context ).execute(  AV8Codigo,  AV36UserId,  AV38Observacao,  AV39NaoConformidade_Codigo) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV8Codigo), "ZZZZZ9")));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36UserId", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36UserId), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vUSERID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV36UserId), "ZZZZZ9")));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38Observacao", AV38Observacao);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39NaoConformidade_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39NaoConformidade_Codigo), 6, 0)));
            lblTbjava_Caption = "<script language=\"javascript\" type=\"javascript\">parent.location.replace(parent.location.href); </script>";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbjava_Internalname, "Caption", lblTbjava_Caption);
         }
      }

      protected void E14JX2( )
      {
         /* 'DoFechar' Routine */
         context.setWebReturnParms(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void nextLoad( )
      {
      }

      protected void E15JX2( )
      {
         /* Load Routine */
      }

      protected void wb_table1_2_JX2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_JX2( true) ;
         }
         else
         {
            wb_table2_8_JX2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_JX2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\">") ;
            wb_table3_21_JX2( true) ;
         }
         else
         {
            wb_table3_21_JX2( false) ;
         }
         return  ;
      }

      protected void wb_table3_21_JX2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_JX2e( true) ;
         }
         else
         {
            wb_table1_2_JX2e( false) ;
         }
      }

      protected void wb_table3_21_JX2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable1_Internalname, tblUnnamedtable1_Internalname, "", "Table", 0, "", "", 1, 15, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnenter_Internalname, "", "Confirmar", bttBtnenter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtnenter_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_CancelarOS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 26,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnfechar_Internalname, "", "Fechar", bttBtnfechar_Jsonclick, 5, "Fechar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'DOFECHAR\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_CancelarOS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_21_JX2e( true) ;
         }
         else
         {
            wb_table3_21_JX2e( false) ;
         }
      }

      protected void wb_table2_8_JX2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_parecertcn_Internalname, "Causa", "", "", lblTextblockcontagemresultado_parecertcn_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_CancelarOS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"3\"  class='RequiredDataContentCell'>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 13,'',false,'',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavObservacao_Internalname, AV38Observacao, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,13);\"", 0, 1, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "500", 1, "", "", -1, true, "DescricaoLonga", "HLP_WP_CancelarOS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_parecertcn2_Internalname, "N�o conformidade", "", "", lblTextblockcontagemresultado_parecertcn2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_CancelarOS.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"3\"  class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavNaoconformidade_codigo, dynavNaoconformidade_codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV39NaoConformidade_Codigo), 6, 0)), 1, dynavNaoconformidade_codigo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,18);\"", "", true, "HLP_WP_CancelarOS.htm");
            dynavNaoconformidade_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV39NaoConformidade_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavNaoconformidade_codigo_Internalname, "Values", (String)(dynavNaoconformidade_codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_JX2e( true) ;
         }
         else
         {
            wb_table2_8_JX2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV8Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV8Codigo), "ZZZZZ9")));
         AV35OS = (String)getParm(obj,1);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35OS", AV35OS);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vOS", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV35OS, "@!"))));
         AV36UserId = Convert.ToInt32(getParm(obj,2));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36UserId", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36UserId), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vUSERID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV36UserId), "ZZZZZ9")));
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAJX2( ) ;
         WSJX2( ) ;
         WEJX2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?2249787");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20204282346269");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wp_cancelaros.js", "?20204282346269");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockcontagemresultado_parecertcn_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_PARECERTCN";
         edtavObservacao_Internalname = "vOBSERVACAO";
         lblTextblockcontagemresultado_parecertcn2_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_PARECERTCN2";
         dynavNaoconformidade_codigo_Internalname = "vNAOCONFORMIDADE_CODIGO";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtnenter_Internalname = "BTNENTER";
         bttBtnfechar_Internalname = "BTNFECHAR";
         tblUnnamedtable1_Internalname = "UNNAMEDTABLE1";
         tblTablemain_Internalname = "TABLEMAIN";
         lblTbjava_Internalname = "TBJAVA";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         dynavNaoconformidade_codigo_Jsonclick = "";
         bttBtnenter_Visible = 1;
         lblTbjava_Caption = "tbJava";
         lblTbjava_Visible = 1;
         Form.Caption = "Cancelar solicita��o da OS";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[{ctrl:'BTNENTER',prop:'Visible'}]}");
         setEventMetadata("ENTER","{handler:'E13JX2',iparms:[{av:'AV38Observacao',fld:'vOBSERVACAO',pic:'',nv:''},{av:'AV8Codigo',fld:'vCODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV36UserId',fld:'vUSERID',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV39NaoConformidade_Codigo',fld:'vNAOCONFORMIDADE_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'lblTbjava_Caption',ctrl:'TBJAVA',prop:'Caption'}]}");
         setEventMetadata("'DOFECHAR'","{handler:'E14JX2',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         wcpOAV35OS = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV34WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GXKey = "";
         GX_FocusControl = "";
         sPrefix = "";
         lblTbjava_Jsonclick = "";
         Form = new GXWebForm();
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         scmdbuf = "";
         H00JX2_A426NaoConformidade_Codigo = new int[1] ;
         H00JX2_A427NaoConformidade_Nome = new String[] {""} ;
         H00JX2_A428NaoConformidade_AreaTrabalhoCod = new int[1] ;
         H00JX2_A429NaoConformidade_Tipo = new short[1] ;
         AV38Observacao = "";
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtnenter_Jsonclick = "";
         bttBtnfechar_Jsonclick = "";
         lblTextblockcontagemresultado_parecertcn_Jsonclick = "";
         lblTextblockcontagemresultado_parecertcn2_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wp_cancelaros__default(),
            new Object[][] {
                new Object[] {
               H00JX2_A426NaoConformidade_Codigo, H00JX2_A427NaoConformidade_Nome, H00JX2_A428NaoConformidade_AreaTrabalhoCod, H00JX2_A429NaoConformidade_Tipo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short initialized ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXWrapped ;
      private int AV8Codigo ;
      private int AV36UserId ;
      private int wcpOAV8Codigo ;
      private int wcpOAV36UserId ;
      private int lblTbjava_Visible ;
      private int gxdynajaxindex ;
      private int AV39NaoConformidade_Codigo ;
      private int bttBtnenter_Visible ;
      private int idxLst ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String lblTbjava_Internalname ;
      private String lblTbjava_Caption ;
      private String lblTbjava_Jsonclick ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavObservacao_Internalname ;
      private String gxwrpcisep ;
      private String scmdbuf ;
      private String dynavNaoconformidade_codigo_Internalname ;
      private String bttBtnenter_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String tblUnnamedtable1_Internalname ;
      private String TempTags ;
      private String bttBtnenter_Jsonclick ;
      private String bttBtnfechar_Internalname ;
      private String bttBtnfechar_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String lblTextblockcontagemresultado_parecertcn_Internalname ;
      private String lblTextblockcontagemresultado_parecertcn_Jsonclick ;
      private String lblTextblockcontagemresultado_parecertcn2_Internalname ;
      private String lblTextblockcontagemresultado_parecertcn2_Jsonclick ;
      private String dynavNaoconformidade_codigo_Jsonclick ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private String AV38Observacao ;
      private String AV35OS ;
      private String wcpOAV35OS ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox dynavNaoconformidade_codigo ;
      private IDataStoreProvider pr_default ;
      private int[] H00JX2_A426NaoConformidade_Codigo ;
      private String[] H00JX2_A427NaoConformidade_Nome ;
      private int[] H00JX2_A428NaoConformidade_AreaTrabalhoCod ;
      private short[] H00JX2_A429NaoConformidade_Tipo ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private wwpbaseobjects.SdtWWPContext AV34WWPContext ;
   }

   public class wp_cancelaros__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00JX2 ;
          prmH00JX2 = new Object[] {
          new Object[] {"@AV34WWPC_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00JX2", "SELECT [NaoConformidade_Codigo], [NaoConformidade_Nome], [NaoConformidade_AreaTrabalhoCod], [NaoConformidade_Tipo] FROM [NaoConformidade] WITH (NOLOCK) WHERE ([NaoConformidade_AreaTrabalhoCod] = @AV34WWPC_1Areatrabalho_codigo) AND ([NaoConformidade_Tipo] = 1) ORDER BY [NaoConformidade_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00JX2,0,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((short[]) buf[3])[0] = rslt.getShort(4) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
