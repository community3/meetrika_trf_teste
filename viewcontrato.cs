/*
               File: ViewContrato
        Description: View Contrato
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/3/2020 19:3:55.70
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class viewcontrato : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public viewcontrato( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public viewcontrato( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Contrato_Codigo ,
                           String aP1_TabCode )
      {
         this.AV9Contrato_Codigo = aP0_Contrato_Codigo;
         this.AV7TabCode = aP1_TabCode;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV9Contrato_Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9Contrato_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTRATO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV9Contrato_Codigo), "ZZZZZ9")));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV7TabCode = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7TabCode", AV7TabCode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vTABCODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV7TabCode, ""))));
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PA6G2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            START6G2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020531935586");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("viewcontrato.aspx") + "?" + UrlEncode("" +AV9Contrato_Codigo) + "," + UrlEncode(StringUtil.RTrim(AV7TabCode))+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "vCONTRATO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV9Contrato_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vTABCODE", StringUtil.RTrim( AV7TabCode));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATO_NUMERO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A77Contrato_Numero, ""))));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTRATO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV9Contrato_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vTABCODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV7TabCode, ""))));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTRATO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV9Contrato_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vTABCODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV7TabCode, ""))));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
         if ( ! ( WebComp_Tabbedview == null ) )
         {
            WebComp_Tabbedview.componentjscripts();
         }
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WE6G2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVT6G2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("viewcontrato.aspx") + "?" + UrlEncode("" +AV9Contrato_Codigo) + "," + UrlEncode(StringUtil.RTrim(AV7TabCode)) ;
      }

      public override String GetPgmname( )
      {
         return "ViewContrato" ;
      }

      public override String GetPgmdesc( )
      {
         return "View Contrato" ;
      }

      protected void WB6G0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", "", "false");
            wb_table1_2_6G2( true) ;
         }
         else
         {
            wb_table1_2_6G2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_6G2e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void START6G2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "View Contrato", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUP6G0( ) ;
      }

      protected void WS6G2( )
      {
         START6G2( ) ;
         EVT6G2( ) ;
      }

      protected void EVT6G2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E116G2 */
                              E116G2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E126G2 */
                              E126G2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ! wbErr )
                              {
                                 Rfr0gs = false;
                                 if ( ! Rfr0gs )
                                 {
                                 }
                                 dynload_actions( ) ;
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     else if ( StringUtil.StrCmp(sEvtType, "W") == 0 )
                     {
                        sEvtType = StringUtil.Left( sEvt, 4);
                        sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-4));
                        nCmpId = (short)(NumberUtil.Val( sEvtType, "."));
                        if ( nCmpId == 16 )
                        {
                           OldTabbedview = cgiGet( "W0016");
                           if ( ( StringUtil.Len( OldTabbedview) == 0 ) || ( StringUtil.StrCmp(OldTabbedview, WebComp_Tabbedview_Component) != 0 ) )
                           {
                              WebComp_Tabbedview = getWebComponent(GetType(), "GeneXus.Programs", OldTabbedview, new Object[] {context} );
                              WebComp_Tabbedview.ComponentInit();
                              WebComp_Tabbedview.Name = "OldTabbedview";
                              WebComp_Tabbedview_Component = OldTabbedview;
                           }
                           if ( StringUtil.Len( WebComp_Tabbedview_Component) != 0 )
                           {
                              WebComp_Tabbedview.componentprocess("W0016", "", sEvt);
                           }
                           WebComp_Tabbedview_Component = OldTabbedview;
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE6G2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PA6G2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF6G2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      protected void RF6G2( )
      {
         initialize_formulas( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            if ( 1 != 0 )
            {
               if ( StringUtil.Len( WebComp_Tabbedview_Component) != 0 )
               {
                  WebComp_Tabbedview.componentstart();
               }
            }
         }
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Using cursor H006G2 */
            pr_default.execute(0, new Object[] {AV9Contrato_Codigo});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A74Contrato_Codigo = H006G2_A74Contrato_Codigo[0];
               A77Contrato_Numero = H006G2_A77Contrato_Numero[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A77Contrato_Numero", A77Contrato_Numero);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_CONTRATO_NUMERO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A77Contrato_Numero, ""))));
               /* Execute user event: E126G2 */
               E126G2 ();
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(0);
            WB6G0( ) ;
         }
      }

      protected void STRUP6G0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E116G2 */
         E116G2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            A77Contrato_Numero = cgiGet( edtContrato_Numero_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A77Contrato_Numero", A77Contrato_Numero);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_CONTRATO_NUMERO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A77Contrato_Numero, ""))));
            /* Read saved values. */
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E116G2 */
         E116G2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E116G2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         lblWorkwithlink_Link = formatLink("wwcontrato.aspx") ;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblWorkwithlink_Internalname, "Link", lblWorkwithlink_Link);
         AV15GXLvl9 = 0;
         /* Using cursor H006G3 */
         pr_default.execute(1, new Object[] {AV9Contrato_Codigo});
         while ( (pr_default.getStatus(1) != 101) )
         {
            A74Contrato_Codigo = H006G3_A74Contrato_Codigo[0];
            AV15GXLvl9 = 1;
            Form.Caption = "Contrato";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
            AV8Exists = true;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(1);
         if ( AV15GXLvl9 == 0 )
         {
            Form.Caption = "Registro n�o encontrado ";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
            AV8Exists = false;
         }
         if ( AV8Exists )
         {
            /* Execute user subroutine: 'LOADTABS' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            /* Object Property */
            if ( StringUtil.StrCmp(StringUtil.Lower( WebComp_Tabbedview_Component), StringUtil.Lower( "WWPBaseObjects.WWPTabbedView")) != 0 )
            {
               WebComp_Tabbedview = getWebComponent(GetType(), "GeneXus.Programs", "wwpbaseobjects.wwptabbedview", new Object[] {context} );
               WebComp_Tabbedview.ComponentInit();
               WebComp_Tabbedview.Name = "WWPBaseObjects.WWPTabbedView";
               WebComp_Tabbedview_Component = "WWPBaseObjects.WWPTabbedView";
            }
            if ( StringUtil.Len( WebComp_Tabbedview_Component) != 0 )
            {
               WebComp_Tabbedview.setjustcreated();
               WebComp_Tabbedview.componentprepare(new Object[] {(String)"W0016",(String)"",(IGxCollection)AV10Tabs,(String)AV7TabCode});
               WebComp_Tabbedview.componentbind(new Object[] {(String)"",(String)""});
            }
         }
         lblWorkwithlink_Link = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblWorkwithlink_Internalname, "Link", lblWorkwithlink_Link);
         lblWorkwithlink_Jsonclick = "window.history.back()";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblWorkwithlink_Internalname, "Jsonclick", lblWorkwithlink_Jsonclick);
      }

      protected void nextLoad( )
      {
      }

      protected void E126G2( )
      {
         /* Load Routine */
      }

      protected void S112( )
      {
         /* 'LOADTABS' Routine */
         AV10Tabs = new GxObjectCollection( context, "WWPTabOptions.TabOptionsItem", "GxEv3Up14_MeetrikaVs3", "wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem", "GeneXus.Programs");
         AV11Tab = new wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem(context);
         AV11Tab.gxTpr_Code = "General";
         AV11Tab.gxTpr_Description = "Contrato";
         AV11Tab.gxTpr_Webcomponent = formatLink("contratogeneral.aspx") + "?" + UrlEncode("" +AV9Contrato_Codigo);
         AV11Tab.gxTpr_Link = formatLink("viewcontrato.aspx") + "?" + UrlEncode("" +AV9Contrato_Codigo) + "," + UrlEncode(StringUtil.RTrim(AV11Tab.gxTpr_Code));
         AV11Tab.gxTpr_Includeinpanel = 0;
         AV11Tab.gxTpr_Collapsable = true;
         AV11Tab.gxTpr_Collapsedbydefault = false;
         AV10Tabs.Add(AV11Tab, 0);
         AV11Tab = new wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem(context);
         AV11Tab.gxTpr_Code = "ContratoClausulas";
         AV11Tab.gxTpr_Description = "Cl�usulas";
         AV11Tab.gxTpr_Webcomponent = formatLink("contratocontratoclausulaswc.aspx") + "?" + UrlEncode("" +AV9Contrato_Codigo);
         AV11Tab.gxTpr_Link = formatLink("viewcontrato.aspx") + "?" + UrlEncode("" +AV9Contrato_Codigo) + "," + UrlEncode(StringUtil.RTrim(AV11Tab.gxTpr_Code));
         AV11Tab.gxTpr_Includeinpanel = 1;
         AV11Tab.gxTpr_Collapsable = true;
         AV11Tab.gxTpr_Collapsedbydefault = true;
         AV10Tabs.Add(AV11Tab, 0);
         AV11Tab = new wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem(context);
         AV11Tab.gxTpr_Code = "ContratoUnidades";
         AV11Tab.gxTpr_Description = "USC";
         AV11Tab.gxTpr_Webcomponent = formatLink("contrato_unidadesmedicaowc.aspx") + "?" + UrlEncode("" +AV9Contrato_Codigo);
         AV11Tab.gxTpr_Link = formatLink("viewcontrato.aspx") + "?" + UrlEncode("" +AV9Contrato_Codigo) + "," + UrlEncode(StringUtil.RTrim(AV11Tab.gxTpr_Code));
         AV11Tab.gxTpr_Includeinpanel = 1;
         AV11Tab.gxTpr_Collapsable = true;
         AV11Tab.gxTpr_Collapsedbydefault = false;
         AV10Tabs.Add(AV11Tab, 0);
         AV11Tab = new wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem(context);
         AV11Tab.gxTpr_Code = "ContratoServicos";
         AV11Tab.gxTpr_Description = "Servi�os";
         AV11Tab.gxTpr_Webcomponent = formatLink("contratocontratoservicoswc.aspx") + "?" + UrlEncode("" +AV9Contrato_Codigo);
         AV11Tab.gxTpr_Link = formatLink("viewcontrato.aspx") + "?" + UrlEncode("" +AV9Contrato_Codigo) + "," + UrlEncode(StringUtil.RTrim(AV11Tab.gxTpr_Code));
         AV11Tab.gxTpr_Includeinpanel = 1;
         AV11Tab.gxTpr_Collapsable = true;
         AV11Tab.gxTpr_Collapsedbydefault = true;
         AV10Tabs.Add(AV11Tab, 0);
         AV11Tab = new wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem(context);
         AV11Tab.gxTpr_Code = "ContratoGarantia";
         AV11Tab.gxTpr_Description = "Garantias";
         AV11Tab.gxTpr_Webcomponent = formatLink("contratocontratogarantiawc.aspx") + "?" + UrlEncode("" +AV9Contrato_Codigo);
         AV11Tab.gxTpr_Link = formatLink("viewcontrato.aspx") + "?" + UrlEncode("" +AV9Contrato_Codigo) + "," + UrlEncode(StringUtil.RTrim(AV11Tab.gxTpr_Code));
         AV11Tab.gxTpr_Includeinpanel = 1;
         AV11Tab.gxTpr_Collapsable = true;
         AV11Tab.gxTpr_Collapsedbydefault = true;
         AV10Tabs.Add(AV11Tab, 0);
         AV11Tab = new wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem(context);
         AV11Tab.gxTpr_Code = "ContratoArquivosAnexos";
         AV11Tab.gxTpr_Description = "Anexos";
         AV11Tab.gxTpr_Webcomponent = formatLink("contratocontratoarquivosanexoswc.aspx") + "?" + UrlEncode("" +AV9Contrato_Codigo);
         AV11Tab.gxTpr_Link = formatLink("viewcontrato.aspx") + "?" + UrlEncode("" +AV9Contrato_Codigo) + "," + UrlEncode(StringUtil.RTrim(AV11Tab.gxTpr_Code));
         AV11Tab.gxTpr_Includeinpanel = 1;
         AV11Tab.gxTpr_Collapsable = true;
         AV11Tab.gxTpr_Collapsedbydefault = true;
         AV10Tabs.Add(AV11Tab, 0);
         AV11Tab = new wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem(context);
         AV11Tab.gxTpr_Code = "ContratoDadosCertame";
         AV11Tab.gxTpr_Description = "Dados Certame";
         AV11Tab.gxTpr_Webcomponent = formatLink("contratocontratodadoscertamewc.aspx") + "?" + UrlEncode("" +AV9Contrato_Codigo);
         AV11Tab.gxTpr_Link = formatLink("viewcontrato.aspx") + "?" + UrlEncode("" +AV9Contrato_Codigo) + "," + UrlEncode(StringUtil.RTrim(AV11Tab.gxTpr_Code));
         AV11Tab.gxTpr_Includeinpanel = 1;
         AV11Tab.gxTpr_Collapsable = true;
         AV11Tab.gxTpr_Collapsedbydefault = true;
         AV10Tabs.Add(AV11Tab, 0);
         AV11Tab = new wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem(context);
         AV11Tab.gxTpr_Code = "ContratoTermoAditivo";
         AV11Tab.gxTpr_Description = "Termo Aditivo";
         AV11Tab.gxTpr_Webcomponent = formatLink("contratocontratotermoaditivowc.aspx") + "?" + UrlEncode("" +AV9Contrato_Codigo);
         AV11Tab.gxTpr_Link = formatLink("viewcontrato.aspx") + "?" + UrlEncode("" +AV9Contrato_Codigo) + "," + UrlEncode(StringUtil.RTrim(AV11Tab.gxTpr_Code));
         AV11Tab.gxTpr_Includeinpanel = 1;
         AV11Tab.gxTpr_Collapsable = true;
         AV11Tab.gxTpr_Collapsedbydefault = true;
         AV10Tabs.Add(AV11Tab, 0);
         AV11Tab = new wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem(context);
         AV11Tab.gxTpr_Code = "ContratoOcorrencia";
         AV11Tab.gxTpr_Description = "Ocorr�ncias";
         AV11Tab.gxTpr_Webcomponent = formatLink("contratocontratoocorrenciawc.aspx") + "?" + UrlEncode("" +AV9Contrato_Codigo);
         AV11Tab.gxTpr_Link = formatLink("viewcontrato.aspx") + "?" + UrlEncode("" +AV9Contrato_Codigo) + "," + UrlEncode(StringUtil.RTrim(AV11Tab.gxTpr_Code));
         AV11Tab.gxTpr_Includeinpanel = 1;
         AV11Tab.gxTpr_Collapsable = true;
         AV11Tab.gxTpr_Collapsedbydefault = true;
         AV10Tabs.Add(AV11Tab, 0);
         AV11Tab = new wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem(context);
         AV11Tab.gxTpr_Code = "ContratoObrigacao";
         AV11Tab.gxTpr_Description = "Obriga��es";
         AV11Tab.gxTpr_Webcomponent = formatLink("contratocontratoobrigacaowc.aspx") + "?" + UrlEncode("" +AV9Contrato_Codigo);
         AV11Tab.gxTpr_Link = formatLink("viewcontrato.aspx") + "?" + UrlEncode("" +AV9Contrato_Codigo) + "," + UrlEncode(StringUtil.RTrim(AV11Tab.gxTpr_Code));
         AV11Tab.gxTpr_Includeinpanel = 1;
         AV11Tab.gxTpr_Collapsable = true;
         AV11Tab.gxTpr_Collapsedbydefault = true;
         AV10Tabs.Add(AV11Tab, 0);
         AV11Tab = new wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem(context);
         AV11Tab.gxTpr_Code = "ContratoNotaEmpenho";
         AV11Tab.gxTpr_Description = "Notas de Empenho";
         AV11Tab.gxTpr_Webcomponent = formatLink("contratocontratonotaempenhowc.aspx") + "?" + UrlEncode("" +AV9Contrato_Codigo);
         AV11Tab.gxTpr_Link = formatLink("viewcontrato.aspx") + "?" + UrlEncode("" +AV9Contrato_Codigo) + "," + UrlEncode(StringUtil.RTrim(AV11Tab.gxTpr_Code));
         AV11Tab.gxTpr_Includeinpanel = 1;
         AV11Tab.gxTpr_Collapsable = true;
         AV11Tab.gxTpr_Collapsedbydefault = true;
         AV10Tabs.Add(AV11Tab, 0);
         AV11Tab = new wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem(context);
         AV11Tab.gxTpr_Code = "ContratoSaldoContrato";
         AV11Tab.gxTpr_Description = "Saldo";
         AV11Tab.gxTpr_Webcomponent = formatLink("contratocontratosaldocontratowc.aspx") + "?" + UrlEncode("" +AV9Contrato_Codigo);
         AV11Tab.gxTpr_Link = formatLink("viewcontrato.aspx") + "?" + UrlEncode("" +AV9Contrato_Codigo) + "," + UrlEncode(StringUtil.RTrim(AV11Tab.gxTpr_Code));
         AV11Tab.gxTpr_Includeinpanel = 1;
         AV11Tab.gxTpr_Collapsable = true;
         AV11Tab.gxTpr_Collapsedbydefault = true;
         AV10Tabs.Add(AV11Tab, 0);
         AV11Tab = new wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem(context);
         AV11Tab.gxTpr_Code = "WC_ContratoHistoricoConsumo";
         AV11Tab.gxTpr_Description = "Historico Consumo";
         AV11Tab.gxTpr_Webcomponent = formatLink("wc_contratohistoricoconsumo.aspx") + "?" + UrlEncode("" +AV9Contrato_Codigo);
         AV11Tab.gxTpr_Link = formatLink("viewcontrato.aspx") + "?" + UrlEncode("" +AV9Contrato_Codigo) + "," + UrlEncode(StringUtil.RTrim(AV11Tab.gxTpr_Code));
         AV11Tab.gxTpr_Includeinpanel = 1;
         AV11Tab.gxTpr_Collapsable = true;
         AV11Tab.gxTpr_Collapsedbydefault = false;
         AV10Tabs.Add(AV11Tab, 0);
         AV11Tab = new wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem(context);
         AV11Tab.gxTpr_Code = "ContratoAutorizacaoConsumo";
         AV11Tab.gxTpr_Description = "Autoriza��o de Consumo";
         AV11Tab.gxTpr_Webcomponent = formatLink("contratoautorizacaoconsumowc.aspx") + "?" + UrlEncode("" +AV9Contrato_Codigo);
         AV11Tab.gxTpr_Link = formatLink("viewcontrato.aspx") + "?" + UrlEncode("" +AV9Contrato_Codigo) + "," + UrlEncode(StringUtil.RTrim(AV11Tab.gxTpr_Code));
         AV11Tab.gxTpr_Includeinpanel = 1;
         AV11Tab.gxTpr_Collapsable = true;
         AV11Tab.gxTpr_Collapsedbydefault = false;
         AV10Tabs.Add(AV11Tab, 0);
         AV11Tab = new wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem(context);
         AV11Tab.gxTpr_Code = "ContratoCaixaEntrada";
         AV11Tab.gxTpr_Description = "Configura��es da Caixa de Entrada";
         AV11Tab.gxTpr_Webcomponent = formatLink("contratocontratocaixaentradawc.aspx") + "?" + UrlEncode("" +AV9Contrato_Codigo);
         AV11Tab.gxTpr_Link = formatLink("viewcontrato.aspx") + "?" + UrlEncode("" +AV9Contrato_Codigo) + "," + UrlEncode(StringUtil.RTrim(AV11Tab.gxTpr_Code));
         AV11Tab.gxTpr_Includeinpanel = 1;
         AV11Tab.gxTpr_Collapsable = true;
         AV11Tab.gxTpr_Collapsedbydefault = true;
         AV10Tabs.Add(AV11Tab, 0);
         /* Using cursor H006G4 */
         pr_default.execute(2, new Object[] {AV9Contrato_Codigo});
         while ( (pr_default.getStatus(2) != 101) )
         {
            A74Contrato_Codigo = H006G4_A74Contrato_Codigo[0];
            AV12Count = 0;
            /* Using cursor H006G5 */
            pr_default.execute(3, new Object[] {A74Contrato_Codigo});
            while ( (pr_default.getStatus(3) != 101) )
            {
               AV12Count = (short)(AV12Count+1);
               ((wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem)AV10Tabs.Item(2)).gxTpr_Description = "Cl�usulas ("+StringUtil.Trim( StringUtil.Str( (decimal)(AV12Count), 4, 0))+")";
               pr_default.readNext(3);
            }
            pr_default.close(3);
            AV12Count = 0;
            /* Using cursor H006G6 */
            pr_default.execute(4, new Object[] {A74Contrato_Codigo});
            while ( (pr_default.getStatus(4) != 101) )
            {
               A1207ContratoUnidades_ContratoCod = H006G6_A1207ContratoUnidades_ContratoCod[0];
               AV12Count = (short)(AV12Count+1);
               ((wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem)AV10Tabs.Item(3)).gxTpr_Description = "USC ("+StringUtil.Trim( StringUtil.Str( (decimal)(AV12Count), 4, 0))+")";
               pr_default.readNext(4);
            }
            pr_default.close(4);
            AV12Count = 0;
            /* Using cursor H006G7 */
            pr_default.execute(5, new Object[] {A74Contrato_Codigo});
            while ( (pr_default.getStatus(5) != 101) )
            {
               AV12Count = (short)(AV12Count+1);
               ((wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem)AV10Tabs.Item(4)).gxTpr_Description = "Servi�os ("+StringUtil.Trim( StringUtil.Str( (decimal)(AV12Count), 4, 0))+")";
               pr_default.readNext(5);
            }
            pr_default.close(5);
            AV12Count = 0;
            /* Using cursor H006G8 */
            pr_default.execute(6, new Object[] {A74Contrato_Codigo});
            while ( (pr_default.getStatus(6) != 101) )
            {
               AV12Count = (short)(AV12Count+1);
               ((wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem)AV10Tabs.Item(5)).gxTpr_Description = "Garantias ("+StringUtil.Trim( StringUtil.Str( (decimal)(AV12Count), 4, 0))+")";
               pr_default.readNext(6);
            }
            pr_default.close(6);
            AV12Count = 0;
            /* Using cursor H006G9 */
            pr_default.execute(7, new Object[] {A74Contrato_Codigo});
            while ( (pr_default.getStatus(7) != 101) )
            {
               AV12Count = (short)(AV12Count+1);
               ((wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem)AV10Tabs.Item(6)).gxTpr_Description = "Anexos ("+StringUtil.Trim( StringUtil.Str( (decimal)(AV12Count), 4, 0))+")";
               pr_default.readNext(7);
            }
            pr_default.close(7);
            AV12Count = 0;
            /* Using cursor H006G10 */
            pr_default.execute(8, new Object[] {A74Contrato_Codigo});
            while ( (pr_default.getStatus(8) != 101) )
            {
               AV12Count = (short)(AV12Count+1);
               ((wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem)AV10Tabs.Item(7)).gxTpr_Description = "Dados Certame ("+StringUtil.Trim( StringUtil.Str( (decimal)(AV12Count), 4, 0))+")";
               pr_default.readNext(8);
            }
            pr_default.close(8);
            AV12Count = 0;
            /* Using cursor H006G11 */
            pr_default.execute(9, new Object[] {A74Contrato_Codigo});
            while ( (pr_default.getStatus(9) != 101) )
            {
               AV12Count = (short)(AV12Count+1);
               ((wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem)AV10Tabs.Item(8)).gxTpr_Description = "Termo Aditivo ("+StringUtil.Trim( StringUtil.Str( (decimal)(AV12Count), 4, 0))+")";
               pr_default.readNext(9);
            }
            pr_default.close(9);
            AV12Count = 0;
            /* Using cursor H006G12 */
            pr_default.execute(10, new Object[] {A74Contrato_Codigo});
            while ( (pr_default.getStatus(10) != 101) )
            {
               AV12Count = (short)(AV12Count+1);
               ((wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem)AV10Tabs.Item(9)).gxTpr_Description = "Ocorr�ncias ("+StringUtil.Trim( StringUtil.Str( (decimal)(AV12Count), 4, 0))+")";
               pr_default.readNext(10);
            }
            pr_default.close(10);
            AV12Count = 0;
            /* Using cursor H006G13 */
            pr_default.execute(11, new Object[] {A74Contrato_Codigo});
            while ( (pr_default.getStatus(11) != 101) )
            {
               AV12Count = (short)(AV12Count+1);
               ((wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem)AV10Tabs.Item(10)).gxTpr_Description = "Obriga��es ("+StringUtil.Trim( StringUtil.Str( (decimal)(AV12Count), 4, 0))+")";
               pr_default.readNext(11);
            }
            pr_default.close(11);
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(2);
      }

      protected void wb_table1_2_6G2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableContentNoMargin", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TextBlockTitleCell'>") ;
            wb_table2_5_6G2( true) ;
         }
         else
         {
            wb_table2_5_6G2( false) ;
         }
         return  ;
      }

      protected void wb_table2_5_6G2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"right\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-right;text-align:-moz-right;text-align:-webkit-right")+"\" class='TableTitleCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblWorkwithlink_Internalname, " Contrato", lblWorkwithlink_Link, "", lblWorkwithlink_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockLink", 0, "", 1, 1, 0, "HLP_ViewContrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"3\" >") ;
            if ( ! isFullAjaxMode( ) )
            {
               /* WebComponent */
               GxWebStd.gx_hidden_field( context, "W0016"+"", StringUtil.RTrim( WebComp_Tabbedview_Component));
               context.WriteHtmlText( "<div") ;
               GxWebStd.ClassAttribute( context, "gxwebcomponent");
               context.WriteHtmlText( " id=\""+"gxHTMLWrpW0016"+""+"\""+"") ;
               context.WriteHtmlText( ">") ;
               if ( StringUtil.Len( WebComp_Tabbedview_Component) != 0 )
               {
                  if ( StringUtil.StrCmp(StringUtil.Lower( OldTabbedview), StringUtil.Lower( WebComp_Tabbedview_Component)) != 0 )
                  {
                     context.httpAjaxContext.ajax_rspStartCmp("gxHTMLWrpW0016"+"");
                  }
                  WebComp_Tabbedview.componentdraw();
                  if ( StringUtil.StrCmp(StringUtil.Lower( OldTabbedview), StringUtil.Lower( WebComp_Tabbedview_Component)) != 0 )
                  {
                     context.httpAjaxContext.ajax_rspEndCmp();
                  }
               }
               context.WriteHtmlText( "</div>") ;
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_6G2e( true) ;
         }
         else
         {
            wb_table1_2_6G2e( false) ;
         }
      }

      protected void wb_table2_5_6G2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedviewtitle_Internalname, tblTablemergedviewtitle_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblViewtitle_Internalname, "Contrato :: ", "", "", lblViewtitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_ViewContrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContrato_Numero_Internalname, StringUtil.RTrim( A77Contrato_Numero), StringUtil.RTrim( context.localUtil.Format( A77Contrato_Numero, "")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContrato_Numero_Jsonclick, 0, "AttributeTitleWWP", "", "", "", 1, 0, 0, "text", "", 15, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "NumeroContrato", "left", true, "HLP_ViewContrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_6G2e( true) ;
         }
         else
         {
            wb_table2_5_6G2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV9Contrato_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9Contrato_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTRATO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV9Contrato_Codigo), "ZZZZZ9")));
         AV7TabCode = (String)getParm(obj,1);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7TabCode", AV7TabCode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vTABCODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV7TabCode, ""))));
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA6G2( ) ;
         WS6G2( ) ;
         WE6G2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?2249787");
         if ( ! ( WebComp_Tabbedview == null ) )
         {
            if ( StringUtil.Len( WebComp_Tabbedview_Component) != 0 )
            {
               WebComp_Tabbedview.componentthemes();
            }
         }
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020531935654");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("viewcontrato.js", "?2020531935654");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblViewtitle_Internalname = "VIEWTITLE";
         edtContrato_Numero_Internalname = "CONTRATO_NUMERO";
         tblTablemergedviewtitle_Internalname = "TABLEMERGEDVIEWTITLE";
         lblWorkwithlink_Internalname = "WORKWITHLINK";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtContrato_Numero_Jsonclick = "";
         lblWorkwithlink_Link = "";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "View Contrato";
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         wcpOAV7TabCode = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         A77Contrato_Numero = "";
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         OldTabbedview = "";
         WebComp_Tabbedview_Component = "";
         scmdbuf = "";
         H006G2_A74Contrato_Codigo = new int[1] ;
         H006G2_A77Contrato_Numero = new String[] {""} ;
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         H006G3_A74Contrato_Codigo = new int[1] ;
         AV10Tabs = new GxObjectCollection( context, "WWPTabOptions.TabOptionsItem", "GxEv3Up14_MeetrikaVs3", "wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem", "GeneXus.Programs");
         lblWorkwithlink_Jsonclick = "";
         AV11Tab = new wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem(context);
         H006G4_A74Contrato_Codigo = new int[1] ;
         H006G5_A152ContratoClausulas_Codigo = new int[1] ;
         H006G5_A74Contrato_Codigo = new int[1] ;
         H006G6_A1204ContratoUnidades_UndMedCod = new int[1] ;
         H006G6_A1207ContratoUnidades_ContratoCod = new int[1] ;
         H006G7_A160ContratoServicos_Codigo = new int[1] ;
         H006G7_A74Contrato_Codigo = new int[1] ;
         H006G8_A101ContratoGarantia_Codigo = new int[1] ;
         H006G8_A74Contrato_Codigo = new int[1] ;
         H006G9_A108ContratoArquivosAnexos_Codigo = new int[1] ;
         H006G9_A74Contrato_Codigo = new int[1] ;
         H006G10_A314ContratoDadosCertame_Codigo = new int[1] ;
         H006G10_A74Contrato_Codigo = new int[1] ;
         H006G11_A315ContratoTermoAditivo_Codigo = new int[1] ;
         H006G11_A74Contrato_Codigo = new int[1] ;
         H006G12_A294ContratoOcorrencia_Codigo = new int[1] ;
         H006G12_A74Contrato_Codigo = new int[1] ;
         H006G13_A321ContratoObrigacao_Codigo = new int[1] ;
         H006G13_A74Contrato_Codigo = new int[1] ;
         sStyleString = "";
         lblViewtitle_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.viewcontrato__default(),
            new Object[][] {
                new Object[] {
               H006G2_A74Contrato_Codigo, H006G2_A77Contrato_Numero
               }
               , new Object[] {
               H006G3_A74Contrato_Codigo
               }
               , new Object[] {
               H006G4_A74Contrato_Codigo
               }
               , new Object[] {
               H006G5_A152ContratoClausulas_Codigo, H006G5_A74Contrato_Codigo
               }
               , new Object[] {
               H006G6_A1204ContratoUnidades_UndMedCod, H006G6_A1207ContratoUnidades_ContratoCod
               }
               , new Object[] {
               H006G7_A160ContratoServicos_Codigo, H006G7_A74Contrato_Codigo
               }
               , new Object[] {
               H006G8_A101ContratoGarantia_Codigo, H006G8_A74Contrato_Codigo
               }
               , new Object[] {
               H006G9_A108ContratoArquivosAnexos_Codigo, H006G9_A74Contrato_Codigo
               }
               , new Object[] {
               H006G10_A314ContratoDadosCertame_Codigo, H006G10_A74Contrato_Codigo
               }
               , new Object[] {
               H006G11_A315ContratoTermoAditivo_Codigo, H006G11_A74Contrato_Codigo
               }
               , new Object[] {
               H006G12_A294ContratoOcorrencia_Codigo, H006G12_A74Contrato_Codigo
               }
               , new Object[] {
               H006G13_A321ContratoObrigacao_Codigo, H006G13_A74Contrato_Codigo
               }
            }
         );
         WebComp_Tabbedview = new GeneXus.Http.GXNullWebComponent();
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short wbEnd ;
      private short wbStart ;
      private short nCmpId ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short AV15GXLvl9 ;
      private short AV12Count ;
      private short nGXWrapped ;
      private int AV9Contrato_Codigo ;
      private int wcpOAV9Contrato_Codigo ;
      private int A74Contrato_Codigo ;
      private int A1207ContratoUnidades_ContratoCod ;
      private int idxLst ;
      private String AV7TabCode ;
      private String wcpOAV7TabCode ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String A77Contrato_Numero ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String OldTabbedview ;
      private String WebComp_Tabbedview_Component ;
      private String scmdbuf ;
      private String edtContrato_Numero_Internalname ;
      private String lblWorkwithlink_Link ;
      private String lblWorkwithlink_Internalname ;
      private String lblWorkwithlink_Jsonclick ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablemergedviewtitle_Internalname ;
      private String lblViewtitle_Internalname ;
      private String lblViewtitle_Jsonclick ;
      private String edtContrato_Numero_Jsonclick ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private bool AV8Exists ;
      private GXWebComponent WebComp_Tabbedview ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] H006G2_A74Contrato_Codigo ;
      private String[] H006G2_A77Contrato_Numero ;
      private int[] H006G3_A74Contrato_Codigo ;
      private int[] H006G4_A74Contrato_Codigo ;
      private int[] H006G5_A152ContratoClausulas_Codigo ;
      private int[] H006G5_A74Contrato_Codigo ;
      private int[] H006G6_A1204ContratoUnidades_UndMedCod ;
      private int[] H006G6_A1207ContratoUnidades_ContratoCod ;
      private int[] H006G7_A160ContratoServicos_Codigo ;
      private int[] H006G7_A74Contrato_Codigo ;
      private int[] H006G8_A101ContratoGarantia_Codigo ;
      private int[] H006G8_A74Contrato_Codigo ;
      private int[] H006G9_A108ContratoArquivosAnexos_Codigo ;
      private int[] H006G9_A74Contrato_Codigo ;
      private int[] H006G10_A314ContratoDadosCertame_Codigo ;
      private int[] H006G10_A74Contrato_Codigo ;
      private int[] H006G11_A315ContratoTermoAditivo_Codigo ;
      private int[] H006G11_A74Contrato_Codigo ;
      private int[] H006G12_A294ContratoOcorrencia_Codigo ;
      private int[] H006G12_A74Contrato_Codigo ;
      private int[] H006G13_A321ContratoObrigacao_Codigo ;
      private int[] H006G13_A74Contrato_Codigo ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem ))]
      private IGxCollection AV10Tabs ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem AV11Tab ;
   }

   public class viewcontrato__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH006G2 ;
          prmH006G2 = new Object[] {
          new Object[] {"@AV9Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH006G3 ;
          prmH006G3 = new Object[] {
          new Object[] {"@AV9Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH006G4 ;
          prmH006G4 = new Object[] {
          new Object[] {"@AV9Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH006G5 ;
          prmH006G5 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH006G6 ;
          prmH006G6 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH006G7 ;
          prmH006G7 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH006G8 ;
          prmH006G8 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH006G9 ;
          prmH006G9 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH006G10 ;
          prmH006G10 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH006G11 ;
          prmH006G11 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH006G12 ;
          prmH006G12 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH006G13 ;
          prmH006G13 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H006G2", "SELECT [Contrato_Codigo], [Contrato_Numero] FROM [Contrato] WITH (NOLOCK) WHERE [Contrato_Codigo] = @AV9Contrato_Codigo ORDER BY [Contrato_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH006G2,1,0,true,true )
             ,new CursorDef("H006G3", "SELECT [Contrato_Codigo] FROM [Contrato] WITH (NOLOCK) WHERE [Contrato_Codigo] = @AV9Contrato_Codigo ORDER BY [Contrato_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH006G3,1,0,false,true )
             ,new CursorDef("H006G4", "SELECT [Contrato_Codigo] FROM [Contrato] WITH (NOLOCK) WHERE [Contrato_Codigo] = @AV9Contrato_Codigo ORDER BY [Contrato_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH006G4,1,0,true,true )
             ,new CursorDef("H006G5", "SELECT [ContratoClausulas_Codigo], [Contrato_Codigo] FROM [ContratoClausulas] WITH (NOLOCK) WHERE [Contrato_Codigo] = @Contrato_Codigo ORDER BY [Contrato_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH006G5,100,0,false,false )
             ,new CursorDef("H006G6", "SELECT [ContratoUnidades_UndMedCod], [ContratoUnidades_ContratoCod] FROM [ContratoUnidades] WITH (NOLOCK) WHERE [ContratoUnidades_ContratoCod] = @Contrato_Codigo ORDER BY [ContratoUnidades_ContratoCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH006G6,100,0,false,false )
             ,new CursorDef("H006G7", "SELECT [ContratoServicos_Codigo], [Contrato_Codigo] FROM [ContratoServicos] WITH (NOLOCK) WHERE [Contrato_Codigo] = @Contrato_Codigo ORDER BY [Contrato_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH006G7,100,0,false,false )
             ,new CursorDef("H006G8", "SELECT [ContratoGarantia_Codigo], [Contrato_Codigo] FROM [ContratoGarantia] WITH (NOLOCK) WHERE [Contrato_Codigo] = @Contrato_Codigo ORDER BY [Contrato_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH006G8,100,0,false,false )
             ,new CursorDef("H006G9", "SELECT [ContratoArquivosAnexos_Codigo], [Contrato_Codigo] FROM [ContratoArquivosAnexos] WITH (NOLOCK) WHERE [Contrato_Codigo] = @Contrato_Codigo ORDER BY [Contrato_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH006G9,100,0,false,false )
             ,new CursorDef("H006G10", "SELECT [ContratoDadosCertame_Codigo], [Contrato_Codigo] FROM [ContratoDadosCertame] WITH (NOLOCK) WHERE [Contrato_Codigo] = @Contrato_Codigo ORDER BY [Contrato_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH006G10,100,0,false,false )
             ,new CursorDef("H006G11", "SELECT [ContratoTermoAditivo_Codigo], [Contrato_Codigo] FROM [ContratoTermoAditivo] WITH (NOLOCK) WHERE [Contrato_Codigo] = @Contrato_Codigo ORDER BY [Contrato_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH006G11,100,0,false,false )
             ,new CursorDef("H006G12", "SELECT [ContratoOcorrencia_Codigo], [Contrato_Codigo] FROM [ContratoOcorrencia] WITH (NOLOCK) WHERE [Contrato_Codigo] = @Contrato_Codigo ORDER BY [Contrato_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH006G12,100,0,false,false )
             ,new CursorDef("H006G13", "SELECT [ContratoObrigacao_Codigo], [Contrato_Codigo] FROM [ContratoObrigacao] WITH (NOLOCK) WHERE [Contrato_Codigo] = @Contrato_Codigo ORDER BY [Contrato_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH006G13,100,0,false,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 20) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
