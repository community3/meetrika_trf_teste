/*
               File: FuncaoAPFPreImp
        Description: Funcao APF Pre Impprta��o
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/3/2020 22:40:34.96
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class funcaoapfpreimp : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_3") == 0 )
         {
            A1260FuncaoAPFPreImp_FnApfCodigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1260FuncaoAPFPreImp_FnApfCodigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1260FuncaoAPFPreImp_FnApfCodigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_3( A1260FuncaoAPFPreImp_FnApfCodigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         cmbFuncaoAPFPreImp_Tipo.Name = "FUNCAOAPFPREIMP_TIPO";
         cmbFuncaoAPFPreImp_Tipo.WebTags = "";
         cmbFuncaoAPFPreImp_Tipo.addItem("EE", "EE", 0);
         cmbFuncaoAPFPreImp_Tipo.addItem("SE", "SE", 0);
         cmbFuncaoAPFPreImp_Tipo.addItem("CE", "CE", 0);
         cmbFuncaoAPFPreImp_Tipo.addItem("NM", "NM", 0);
         if ( cmbFuncaoAPFPreImp_Tipo.ItemCount > 0 )
         {
            A1248FuncaoAPFPreImp_Tipo = cmbFuncaoAPFPreImp_Tipo.getValidValue(A1248FuncaoAPFPreImp_Tipo);
            n1248FuncaoAPFPreImp_Tipo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1248FuncaoAPFPreImp_Tipo", A1248FuncaoAPFPreImp_Tipo);
         }
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Funcao APF Pre Impprta��o", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtFuncaoAPFPreImp_FnApfCodigo_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public funcaoapfpreimp( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public funcaoapfpreimp( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbFuncaoAPFPreImp_Tipo = new GXCombobox();
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbFuncaoAPFPreImp_Tipo.ItemCount > 0 )
         {
            A1248FuncaoAPFPreImp_Tipo = cmbFuncaoAPFPreImp_Tipo.getValidValue(A1248FuncaoAPFPreImp_Tipo);
            n1248FuncaoAPFPreImp_Tipo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1248FuncaoAPFPreImp_Tipo", A1248FuncaoAPFPreImp_Tipo);
         }
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_3F153( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_3F153e( bool wbgen )
      {
         if ( wbgen )
         {
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_3F153( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableBorder100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_3F153( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_3F153e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Control Group */
            GxWebStd.gx_group_start( context, grpGroupdata_Internalname, "Funcao APF Pre Impprta��o", 1, 0, "px", 0, "px", "Group", " "+"style=\"-moz-border-radius: 3pt;;\""+" ", "HLP_FuncaoAPFPreImp.htm");
            wb_table3_28_3F153( true) ;
         }
         return  ;
      }

      protected void wb_table3_28_3F153e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</fieldset>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_3F153e( true) ;
         }
         else
         {
            wb_table1_2_3F153e( false) ;
         }
      }

      protected void wb_table3_28_3F153( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_34_3F153( true) ;
         }
         return  ;
      }

      protected void wb_table4_34_3F153e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 67,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_enter_Internalname, "", "Confirmar", bttBtn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_enter_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_FuncaoAPFPreImp.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 68,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_cancel_Internalname, "", "Fechar", bttBtn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_FuncaoAPFPreImp.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'',false,'',0)\"";
            ClassString = "BtnDelete";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_delete_Internalname, "", "Eliminar", bttBtn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_delete_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_FuncaoAPFPreImp.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_28_3F153e( true) ;
         }
         else
         {
            wb_table3_28_3F153e( false) ;
         }
      }

      protected void wb_table4_34_3F153( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable2_Internalname, tblTable2_Internalname, "", "Container", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaoapfpreimp_fnapfcodigo_Internalname, "Apf Codigo", "", "", lblTextblockfuncaoapfpreimp_fnapfcodigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_FuncaoAPFPreImp.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtFuncaoAPFPreImp_FnApfCodigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1260FuncaoAPFPreImp_FnApfCodigo), 6, 0, ",", "")), ((edtFuncaoAPFPreImp_FnApfCodigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1260FuncaoAPFPreImp_FnApfCodigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A1260FuncaoAPFPreImp_FnApfCodigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,39);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtFuncaoAPFPreImp_FnApfCodigo_Jsonclick, 0, "Attribute", "", "", "", 1, edtFuncaoAPFPreImp_FnApfCodigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_FuncaoAPFPreImp.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaoapfpreimp_sequencial_Internalname, "APFPre Imp_Sequencial", "", "", lblTextblockfuncaoapfpreimp_sequencial_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_FuncaoAPFPreImp.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtFuncaoAPFPreImp_Sequencial_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1257FuncaoAPFPreImp_Sequencial), 3, 0, ",", "")), ((edtFuncaoAPFPreImp_Sequencial_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1257FuncaoAPFPreImp_Sequencial), "ZZ9")) : context.localUtil.Format( (decimal)(A1257FuncaoAPFPreImp_Sequencial), "ZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,44);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtFuncaoAPFPreImp_Sequencial_Jsonclick, 0, "Attribute", "", "", "", 1, edtFuncaoAPFPreImp_Sequencial_Enabled, 0, "text", "", 3, "chr", 1, "row", 3, 0, 0, 0, 1, -1, 0, true, "Sequencial", "right", false, "HLP_FuncaoAPFPreImp.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaoapfpreimp_tipo_Internalname, "APFPre Imp_Tipo", "", "", lblTextblockfuncaoapfpreimp_tipo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_FuncaoAPFPreImp.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbFuncaoAPFPreImp_Tipo, cmbFuncaoAPFPreImp_Tipo_Internalname, StringUtil.RTrim( A1248FuncaoAPFPreImp_Tipo), 1, cmbFuncaoAPFPreImp_Tipo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, cmbFuncaoAPFPreImp_Tipo.Enabled, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,49);\"", "", true, "HLP_FuncaoAPFPreImp.htm");
            cmbFuncaoAPFPreImp_Tipo.CurrentValue = StringUtil.RTrim( A1248FuncaoAPFPreImp_Tipo);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbFuncaoAPFPreImp_Tipo_Internalname, "Values", (String)(cmbFuncaoAPFPreImp_Tipo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaoapfpreimp_derimp_Internalname, "APFPre Imp_DERImp", "", "", lblTextblockfuncaoapfpreimp_derimp_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_FuncaoAPFPreImp.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtFuncaoAPFPreImp_DERImp_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1249FuncaoAPFPreImp_DERImp), 4, 0, ",", "")), ((edtFuncaoAPFPreImp_DERImp_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1249FuncaoAPFPreImp_DERImp), "ZZZ9")) : context.localUtil.Format( (decimal)(A1249FuncaoAPFPreImp_DERImp), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,54);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtFuncaoAPFPreImp_DERImp_Jsonclick, 0, "Attribute", "", "", "", 1, edtFuncaoAPFPreImp_DERImp_Enabled, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_FuncaoAPFPreImp.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaoapfpreimp_raimp_Internalname, "APFPre Imp_RAImp", "", "", lblTextblockfuncaoapfpreimp_raimp_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_FuncaoAPFPreImp.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtFuncaoAPFPreImp_RAImp_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1250FuncaoAPFPreImp_RAImp), 4, 0, ",", "")), ((edtFuncaoAPFPreImp_RAImp_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1250FuncaoAPFPreImp_RAImp), "ZZZ9")) : context.localUtil.Format( (decimal)(A1250FuncaoAPFPreImp_RAImp), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,59);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtFuncaoAPFPreImp_RAImp_Jsonclick, 0, "Attribute", "", "", "", 1, edtFuncaoAPFPreImp_RAImp_Enabled, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_FuncaoAPFPreImp.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaoapfpreimp_observacao_Internalname, "APFPre Imp_Observacao", "", "", lblTextblockfuncaoapfpreimp_observacao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_FuncaoAPFPreImp.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"FUNCAOAPFPREIMP_OBSERVACAOContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_34_3F153e( true) ;
         }
         else
         {
            wb_table4_34_3F153e( false) ;
         }
      }

      protected void wb_table2_5_3F153( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabletoolbar_Internalname, tblTabletoolbar_Internalname, "", "ViewTable", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divSectiontoolbar_Internalname, 1, 0, "px", 0, "px", "ToolbarMain", "left", "top", "", "WHITE-SPACE: nowrap;", "div");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 9,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_first_Internalname, context.GetImagePath( "b9e06284-17ac-4c88-8937-5dbd84ad5d80", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_first_Visible, 1, "", "Primeiro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_first_Jsonclick, "'"+""+"'"+",false,"+"'"+"EFIRST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_FuncaoAPFPreImp.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_first_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_first_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_first_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EFIRST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_FuncaoAPFPreImp.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_previous_Internalname, context.GetImagePath( "7d212604-db7b-4785-9c0d-5faffe71aa33", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_previous_Visible, 1, "", "Anterior", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_previous_Jsonclick, "'"+""+"'"+",false,"+"'"+"EPREVIOUS."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_FuncaoAPFPreImp.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 12,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_previous_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_previous_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_previous_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EPREVIOUS."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_FuncaoAPFPreImp.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 13,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_next_Internalname, context.GetImagePath( "1ae947cf-1354-41a9-8d59-d91daebf554f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_next_Visible, 1, "", "Pr�ximo", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_next_Jsonclick, "'"+""+"'"+",false,"+"'"+"ENEXT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_FuncaoAPFPreImp.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 14,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_next_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_next_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_next_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ENEXT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_FuncaoAPFPreImp.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 15,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_last_Internalname, context.GetImagePath( "29211874-e613-48e5-9011-8017d984217e", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_last_Visible, 1, "", "�ltimo", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_last_Jsonclick, "'"+""+"'"+",false,"+"'"+"ELAST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_FuncaoAPFPreImp.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_last_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_last_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_last_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ELAST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_FuncaoAPFPreImp.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_select_Internalname, context.GetImagePath( "1ca03f75-9947-4d2c-90a4-e8ab9c5cedea", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_select_Visible, 1, "", "Selecionar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_select_Jsonclick, "'"+""+"'"+",false,"+"'"+"ESELECT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_FuncaoAPFPreImp.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_select_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_select_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_select_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ESELECT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_FuncaoAPFPreImp.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_enter2_Internalname, context.GetImagePath( "2061cf2c-bd33-4433-a13e-34af954142e9", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_enter2_Visible, imgBtn_enter2_Enabled, "", "Confirmar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_enter2_Jsonclick, "'"+""+"'"+",false,"+"'"+"EENTER."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_FuncaoAPFPreImp.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_enter2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_enter2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_enter2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EENTER."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_FuncaoAPFPreImp.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_cancel2_Internalname, context.GetImagePath( "0e94ced8-bc34-47ff-9a53-bc683736a686", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_cancel2_Visible, 1, "", "Fechar", 0, 0, 0, "px", 0, "px", 0, 0, 1, imgBtn_cancel2_Jsonclick, "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_FuncaoAPFPreImp.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 22,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_cancel2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_cancel2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 1, imgBtn_cancel2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_FuncaoAPFPreImp.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_delete2_Internalname, context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_delete2_Visible, imgBtn_delete2_Enabled, "", "Eliminar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_delete2_Jsonclick, "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_FuncaoAPFPreImp.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_delete2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_delete2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_delete2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_FuncaoAPFPreImp.htm");
            GxWebStd.gx_div_end( context, "left", "top");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_3F153e( true) ;
         }
         else
         {
            wb_table2_5_3F153e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               if ( ( ( context.localUtil.CToN( cgiGet( edtFuncaoAPFPreImp_FnApfCodigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtFuncaoAPFPreImp_FnApfCodigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "FUNCAOAPFPREIMP_FNAPFCODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtFuncaoAPFPreImp_FnApfCodigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1260FuncaoAPFPreImp_FnApfCodigo = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1260FuncaoAPFPreImp_FnApfCodigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1260FuncaoAPFPreImp_FnApfCodigo), 6, 0)));
               }
               else
               {
                  A1260FuncaoAPFPreImp_FnApfCodigo = (int)(context.localUtil.CToN( cgiGet( edtFuncaoAPFPreImp_FnApfCodigo_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1260FuncaoAPFPreImp_FnApfCodigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1260FuncaoAPFPreImp_FnApfCodigo), 6, 0)));
               }
               if ( ( ( context.localUtil.CToN( cgiGet( edtFuncaoAPFPreImp_Sequencial_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtFuncaoAPFPreImp_Sequencial_Internalname), ",", ".") > Convert.ToDecimal( 999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "FUNCAOAPFPREIMP_SEQUENCIAL");
                  AnyError = 1;
                  GX_FocusControl = edtFuncaoAPFPreImp_Sequencial_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1257FuncaoAPFPreImp_Sequencial = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1257FuncaoAPFPreImp_Sequencial", StringUtil.LTrim( StringUtil.Str( (decimal)(A1257FuncaoAPFPreImp_Sequencial), 3, 0)));
               }
               else
               {
                  A1257FuncaoAPFPreImp_Sequencial = (short)(context.localUtil.CToN( cgiGet( edtFuncaoAPFPreImp_Sequencial_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1257FuncaoAPFPreImp_Sequencial", StringUtil.LTrim( StringUtil.Str( (decimal)(A1257FuncaoAPFPreImp_Sequencial), 3, 0)));
               }
               cmbFuncaoAPFPreImp_Tipo.CurrentValue = cgiGet( cmbFuncaoAPFPreImp_Tipo_Internalname);
               A1248FuncaoAPFPreImp_Tipo = cgiGet( cmbFuncaoAPFPreImp_Tipo_Internalname);
               n1248FuncaoAPFPreImp_Tipo = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1248FuncaoAPFPreImp_Tipo", A1248FuncaoAPFPreImp_Tipo);
               n1248FuncaoAPFPreImp_Tipo = (String.IsNullOrEmpty(StringUtil.RTrim( A1248FuncaoAPFPreImp_Tipo)) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtFuncaoAPFPreImp_DERImp_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtFuncaoAPFPreImp_DERImp_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "FUNCAOAPFPREIMP_DERIMP");
                  AnyError = 1;
                  GX_FocusControl = edtFuncaoAPFPreImp_DERImp_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1249FuncaoAPFPreImp_DERImp = 0;
                  n1249FuncaoAPFPreImp_DERImp = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1249FuncaoAPFPreImp_DERImp", StringUtil.LTrim( StringUtil.Str( (decimal)(A1249FuncaoAPFPreImp_DERImp), 4, 0)));
               }
               else
               {
                  A1249FuncaoAPFPreImp_DERImp = (short)(context.localUtil.CToN( cgiGet( edtFuncaoAPFPreImp_DERImp_Internalname), ",", "."));
                  n1249FuncaoAPFPreImp_DERImp = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1249FuncaoAPFPreImp_DERImp", StringUtil.LTrim( StringUtil.Str( (decimal)(A1249FuncaoAPFPreImp_DERImp), 4, 0)));
               }
               n1249FuncaoAPFPreImp_DERImp = ((0==A1249FuncaoAPFPreImp_DERImp) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtFuncaoAPFPreImp_RAImp_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtFuncaoAPFPreImp_RAImp_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "FUNCAOAPFPREIMP_RAIMP");
                  AnyError = 1;
                  GX_FocusControl = edtFuncaoAPFPreImp_RAImp_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1250FuncaoAPFPreImp_RAImp = 0;
                  n1250FuncaoAPFPreImp_RAImp = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1250FuncaoAPFPreImp_RAImp", StringUtil.LTrim( StringUtil.Str( (decimal)(A1250FuncaoAPFPreImp_RAImp), 4, 0)));
               }
               else
               {
                  A1250FuncaoAPFPreImp_RAImp = (short)(context.localUtil.CToN( cgiGet( edtFuncaoAPFPreImp_RAImp_Internalname), ",", "."));
                  n1250FuncaoAPFPreImp_RAImp = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1250FuncaoAPFPreImp_RAImp", StringUtil.LTrim( StringUtil.Str( (decimal)(A1250FuncaoAPFPreImp_RAImp), 4, 0)));
               }
               n1250FuncaoAPFPreImp_RAImp = ((0==A1250FuncaoAPFPreImp_RAImp) ? true : false);
               /* Read saved values. */
               Z1260FuncaoAPFPreImp_FnApfCodigo = (int)(context.localUtil.CToN( cgiGet( "Z1260FuncaoAPFPreImp_FnApfCodigo"), ",", "."));
               Z1257FuncaoAPFPreImp_Sequencial = (short)(context.localUtil.CToN( cgiGet( "Z1257FuncaoAPFPreImp_Sequencial"), ",", "."));
               Z1248FuncaoAPFPreImp_Tipo = cgiGet( "Z1248FuncaoAPFPreImp_Tipo");
               n1248FuncaoAPFPreImp_Tipo = (String.IsNullOrEmpty(StringUtil.RTrim( A1248FuncaoAPFPreImp_Tipo)) ? true : false);
               Z1249FuncaoAPFPreImp_DERImp = (short)(context.localUtil.CToN( cgiGet( "Z1249FuncaoAPFPreImp_DERImp"), ",", "."));
               n1249FuncaoAPFPreImp_DERImp = ((0==A1249FuncaoAPFPreImp_DERImp) ? true : false);
               Z1250FuncaoAPFPreImp_RAImp = (short)(context.localUtil.CToN( cgiGet( "Z1250FuncaoAPFPreImp_RAImp"), ",", "."));
               n1250FuncaoAPFPreImp_RAImp = ((0==A1250FuncaoAPFPreImp_RAImp) ? true : false);
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               A1251FuncaoAPFPreImp_Observacao = cgiGet( "FUNCAOAPFPREIMP_OBSERVACAO");
               n1251FuncaoAPFPreImp_Observacao = false;
               n1251FuncaoAPFPreImp_Observacao = (String.IsNullOrEmpty(StringUtil.RTrim( A1251FuncaoAPFPreImp_Observacao)) ? true : false);
               Gx_mode = cgiGet( "vMODE");
               Funcaoapfpreimp_observacao_Width = cgiGet( "FUNCAOAPFPREIMP_OBSERVACAO_Width");
               Funcaoapfpreimp_observacao_Height = cgiGet( "FUNCAOAPFPREIMP_OBSERVACAO_Height");
               Funcaoapfpreimp_observacao_Skin = cgiGet( "FUNCAOAPFPREIMP_OBSERVACAO_Skin");
               Funcaoapfpreimp_observacao_Toolbar = cgiGet( "FUNCAOAPFPREIMP_OBSERVACAO_Toolbar");
               Funcaoapfpreimp_observacao_Color = (int)(context.localUtil.CToN( cgiGet( "FUNCAOAPFPREIMP_OBSERVACAO_Color"), ",", "."));
               Funcaoapfpreimp_observacao_Enabled = StringUtil.StrToBool( cgiGet( "FUNCAOAPFPREIMP_OBSERVACAO_Enabled"));
               Funcaoapfpreimp_observacao_Class = cgiGet( "FUNCAOAPFPREIMP_OBSERVACAO_Class");
               Funcaoapfpreimp_observacao_Customtoolbar = cgiGet( "FUNCAOAPFPREIMP_OBSERVACAO_Customtoolbar");
               Funcaoapfpreimp_observacao_Customconfiguration = cgiGet( "FUNCAOAPFPREIMP_OBSERVACAO_Customconfiguration");
               Funcaoapfpreimp_observacao_Toolbarcancollapse = StringUtil.StrToBool( cgiGet( "FUNCAOAPFPREIMP_OBSERVACAO_Toolbarcancollapse"));
               Funcaoapfpreimp_observacao_Toolbarexpanded = StringUtil.StrToBool( cgiGet( "FUNCAOAPFPREIMP_OBSERVACAO_Toolbarexpanded"));
               Funcaoapfpreimp_observacao_Buttonpressedid = cgiGet( "FUNCAOAPFPREIMP_OBSERVACAO_Buttonpressedid");
               Funcaoapfpreimp_observacao_Captionvalue = cgiGet( "FUNCAOAPFPREIMP_OBSERVACAO_Captionvalue");
               Funcaoapfpreimp_observacao_Captionclass = cgiGet( "FUNCAOAPFPREIMP_OBSERVACAO_Captionclass");
               Funcaoapfpreimp_observacao_Captionposition = cgiGet( "FUNCAOAPFPREIMP_OBSERVACAO_Captionposition");
               Funcaoapfpreimp_observacao_Coltitle = cgiGet( "FUNCAOAPFPREIMP_OBSERVACAO_Coltitle");
               Funcaoapfpreimp_observacao_Coltitlefont = cgiGet( "FUNCAOAPFPREIMP_OBSERVACAO_Coltitlefont");
               Funcaoapfpreimp_observacao_Coltitlecolor = (int)(context.localUtil.CToN( cgiGet( "FUNCAOAPFPREIMP_OBSERVACAO_Coltitlecolor"), ",", "."));
               Funcaoapfpreimp_observacao_Usercontroliscolumn = StringUtil.StrToBool( cgiGet( "FUNCAOAPFPREIMP_OBSERVACAO_Usercontroliscolumn"));
               Funcaoapfpreimp_observacao_Visible = StringUtil.StrToBool( cgiGet( "FUNCAOAPFPREIMP_OBSERVACAO_Visible"));
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  A1260FuncaoAPFPreImp_FnApfCodigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1260FuncaoAPFPreImp_FnApfCodigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1260FuncaoAPFPreImp_FnApfCodigo), 6, 0)));
                  A1257FuncaoAPFPreImp_Sequencial = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1257FuncaoAPFPreImp_Sequencial", StringUtil.LTrim( StringUtil.Str( (decimal)(A1257FuncaoAPFPreImp_Sequencial), 3, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  disable_std_buttons_dsp( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  standaloneModal( ) ;
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_enter( ) ;
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                        else if ( StringUtil.StrCmp(sEvt, "FIRST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_first( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "PREVIOUS") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_previous( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "NEXT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_next( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LAST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_last( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "SELECT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_select( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DELETE") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_delete( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           AfterKeyLoadScreen( ) ;
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll3F153( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            imgBtn_delete2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
      }

      protected void disable_std_buttons_dsp( )
      {
         imgBtn_delete2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Visible), 5, 0)));
         imgBtn_delete2_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_separator_Visible), 5, 0)));
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)));
         imgBtn_first_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_first_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_first_Visible), 5, 0)));
         imgBtn_first_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_first_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_first_separator_Visible), 5, 0)));
         imgBtn_previous_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_previous_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_previous_Visible), 5, 0)));
         imgBtn_previous_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_previous_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_previous_separator_Visible), 5, 0)));
         imgBtn_next_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_next_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_next_Visible), 5, 0)));
         imgBtn_next_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_next_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_next_separator_Visible), 5, 0)));
         imgBtn_last_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_last_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_last_Visible), 5, 0)));
         imgBtn_last_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_last_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_last_separator_Visible), 5, 0)));
         imgBtn_select_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_select_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_select_Visible), 5, 0)));
         imgBtn_select_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_select_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_select_separator_Visible), 5, 0)));
         imgBtn_delete2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Visible), 5, 0)));
         imgBtn_delete2_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_separator_Visible), 5, 0)));
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)));
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            imgBtn_enter2_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Visible), 5, 0)));
            imgBtn_enter2_separator_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_separator_Visible), 5, 0)));
            bttBtn_enter_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_enter_Visible), 5, 0)));
         }
         DisableAttributes3F153( ) ;
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void ResetCaption3F0( )
      {
      }

      protected void ZM3F153( short GX_JID )
      {
         if ( ( GX_JID == 2 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z1248FuncaoAPFPreImp_Tipo = T003F3_A1248FuncaoAPFPreImp_Tipo[0];
               Z1249FuncaoAPFPreImp_DERImp = T003F3_A1249FuncaoAPFPreImp_DERImp[0];
               Z1250FuncaoAPFPreImp_RAImp = T003F3_A1250FuncaoAPFPreImp_RAImp[0];
            }
            else
            {
               Z1248FuncaoAPFPreImp_Tipo = A1248FuncaoAPFPreImp_Tipo;
               Z1249FuncaoAPFPreImp_DERImp = A1249FuncaoAPFPreImp_DERImp;
               Z1250FuncaoAPFPreImp_RAImp = A1250FuncaoAPFPreImp_RAImp;
            }
         }
         if ( GX_JID == -2 )
         {
            Z1257FuncaoAPFPreImp_Sequencial = A1257FuncaoAPFPreImp_Sequencial;
            Z1248FuncaoAPFPreImp_Tipo = A1248FuncaoAPFPreImp_Tipo;
            Z1249FuncaoAPFPreImp_DERImp = A1249FuncaoAPFPreImp_DERImp;
            Z1250FuncaoAPFPreImp_RAImp = A1250FuncaoAPFPreImp_RAImp;
            Z1251FuncaoAPFPreImp_Observacao = A1251FuncaoAPFPreImp_Observacao;
            Z1260FuncaoAPFPreImp_FnApfCodigo = A1260FuncaoAPFPreImp_FnApfCodigo;
         }
      }

      protected void standaloneNotModal( )
      {
      }

      protected void standaloneModal( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            imgBtn_delete2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
         else
         {
            imgBtn_delete2_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            imgBtn_enter2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Enabled), 5, 0)));
         }
         else
         {
            imgBtn_enter2_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Enabled), 5, 0)));
         }
      }

      protected void Load3F153( )
      {
         /* Using cursor T003F5 */
         pr_default.execute(3, new Object[] {A1260FuncaoAPFPreImp_FnApfCodigo, A1257FuncaoAPFPreImp_Sequencial});
         if ( (pr_default.getStatus(3) != 101) )
         {
            RcdFound153 = 1;
            A1248FuncaoAPFPreImp_Tipo = T003F5_A1248FuncaoAPFPreImp_Tipo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1248FuncaoAPFPreImp_Tipo", A1248FuncaoAPFPreImp_Tipo);
            n1248FuncaoAPFPreImp_Tipo = T003F5_n1248FuncaoAPFPreImp_Tipo[0];
            A1249FuncaoAPFPreImp_DERImp = T003F5_A1249FuncaoAPFPreImp_DERImp[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1249FuncaoAPFPreImp_DERImp", StringUtil.LTrim( StringUtil.Str( (decimal)(A1249FuncaoAPFPreImp_DERImp), 4, 0)));
            n1249FuncaoAPFPreImp_DERImp = T003F5_n1249FuncaoAPFPreImp_DERImp[0];
            A1250FuncaoAPFPreImp_RAImp = T003F5_A1250FuncaoAPFPreImp_RAImp[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1250FuncaoAPFPreImp_RAImp", StringUtil.LTrim( StringUtil.Str( (decimal)(A1250FuncaoAPFPreImp_RAImp), 4, 0)));
            n1250FuncaoAPFPreImp_RAImp = T003F5_n1250FuncaoAPFPreImp_RAImp[0];
            A1251FuncaoAPFPreImp_Observacao = T003F5_A1251FuncaoAPFPreImp_Observacao[0];
            n1251FuncaoAPFPreImp_Observacao = T003F5_n1251FuncaoAPFPreImp_Observacao[0];
            ZM3F153( -2) ;
         }
         pr_default.close(3);
         OnLoadActions3F153( ) ;
      }

      protected void OnLoadActions3F153( )
      {
      }

      protected void CheckExtendedTable3F153( )
      {
         Gx_BScreen = 1;
         standaloneModal( ) ;
         /* Using cursor T003F4 */
         pr_default.execute(2, new Object[] {A1260FuncaoAPFPreImp_FnApfCodigo});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Funcao APFPre Imp_Funcao APF'.", "ForeignKeyNotFound", 1, "FUNCAOAPFPREIMP_FNAPFCODIGO");
            AnyError = 1;
            GX_FocusControl = edtFuncaoAPFPreImp_FnApfCodigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         pr_default.close(2);
         if ( ! ( ( StringUtil.StrCmp(A1248FuncaoAPFPreImp_Tipo, "EE") == 0 ) || ( StringUtil.StrCmp(A1248FuncaoAPFPreImp_Tipo, "SE") == 0 ) || ( StringUtil.StrCmp(A1248FuncaoAPFPreImp_Tipo, "CE") == 0 ) || ( StringUtil.StrCmp(A1248FuncaoAPFPreImp_Tipo, "NM") == 0 ) || String.IsNullOrEmpty(StringUtil.RTrim( A1248FuncaoAPFPreImp_Tipo)) ) )
         {
            GX_msglist.addItem("Campo Funcao APFPre Imp_Tipo fora do intervalo", "OutOfRange", 1, "FUNCAOAPFPREIMP_TIPO");
            AnyError = 1;
            GX_FocusControl = cmbFuncaoAPFPreImp_Tipo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
      }

      protected void CloseExtendedTableCursors3F153( )
      {
         pr_default.close(2);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_3( int A1260FuncaoAPFPreImp_FnApfCodigo )
      {
         /* Using cursor T003F6 */
         pr_default.execute(4, new Object[] {A1260FuncaoAPFPreImp_FnApfCodigo});
         if ( (pr_default.getStatus(4) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Funcao APFPre Imp_Funcao APF'.", "ForeignKeyNotFound", 1, "FUNCAOAPFPREIMP_FNAPFCODIGO");
            AnyError = 1;
            GX_FocusControl = edtFuncaoAPFPreImp_FnApfCodigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(4) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(4);
      }

      protected void GetKey3F153( )
      {
         /* Using cursor T003F7 */
         pr_default.execute(5, new Object[] {A1260FuncaoAPFPreImp_FnApfCodigo, A1257FuncaoAPFPreImp_Sequencial});
         if ( (pr_default.getStatus(5) != 101) )
         {
            RcdFound153 = 1;
         }
         else
         {
            RcdFound153 = 0;
         }
         pr_default.close(5);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T003F3 */
         pr_default.execute(1, new Object[] {A1260FuncaoAPFPreImp_FnApfCodigo, A1257FuncaoAPFPreImp_Sequencial});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM3F153( 2) ;
            RcdFound153 = 1;
            A1257FuncaoAPFPreImp_Sequencial = T003F3_A1257FuncaoAPFPreImp_Sequencial[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1257FuncaoAPFPreImp_Sequencial", StringUtil.LTrim( StringUtil.Str( (decimal)(A1257FuncaoAPFPreImp_Sequencial), 3, 0)));
            A1248FuncaoAPFPreImp_Tipo = T003F3_A1248FuncaoAPFPreImp_Tipo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1248FuncaoAPFPreImp_Tipo", A1248FuncaoAPFPreImp_Tipo);
            n1248FuncaoAPFPreImp_Tipo = T003F3_n1248FuncaoAPFPreImp_Tipo[0];
            A1249FuncaoAPFPreImp_DERImp = T003F3_A1249FuncaoAPFPreImp_DERImp[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1249FuncaoAPFPreImp_DERImp", StringUtil.LTrim( StringUtil.Str( (decimal)(A1249FuncaoAPFPreImp_DERImp), 4, 0)));
            n1249FuncaoAPFPreImp_DERImp = T003F3_n1249FuncaoAPFPreImp_DERImp[0];
            A1250FuncaoAPFPreImp_RAImp = T003F3_A1250FuncaoAPFPreImp_RAImp[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1250FuncaoAPFPreImp_RAImp", StringUtil.LTrim( StringUtil.Str( (decimal)(A1250FuncaoAPFPreImp_RAImp), 4, 0)));
            n1250FuncaoAPFPreImp_RAImp = T003F3_n1250FuncaoAPFPreImp_RAImp[0];
            A1251FuncaoAPFPreImp_Observacao = T003F3_A1251FuncaoAPFPreImp_Observacao[0];
            n1251FuncaoAPFPreImp_Observacao = T003F3_n1251FuncaoAPFPreImp_Observacao[0];
            A1260FuncaoAPFPreImp_FnApfCodigo = T003F3_A1260FuncaoAPFPreImp_FnApfCodigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1260FuncaoAPFPreImp_FnApfCodigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1260FuncaoAPFPreImp_FnApfCodigo), 6, 0)));
            Z1260FuncaoAPFPreImp_FnApfCodigo = A1260FuncaoAPFPreImp_FnApfCodigo;
            Z1257FuncaoAPFPreImp_Sequencial = A1257FuncaoAPFPreImp_Sequencial;
            sMode153 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Load3F153( ) ;
            if ( AnyError == 1 )
            {
               RcdFound153 = 0;
               InitializeNonKey3F153( ) ;
            }
            Gx_mode = sMode153;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            RcdFound153 = 0;
            InitializeNonKey3F153( ) ;
            sMode153 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Gx_mode = sMode153;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey3F153( ) ;
         if ( RcdFound153 == 0 )
         {
            Gx_mode = "INS";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound153 = 0;
         /* Using cursor T003F8 */
         pr_default.execute(6, new Object[] {A1260FuncaoAPFPreImp_FnApfCodigo, A1257FuncaoAPFPreImp_Sequencial});
         if ( (pr_default.getStatus(6) != 101) )
         {
            while ( (pr_default.getStatus(6) != 101) && ( ( T003F8_A1260FuncaoAPFPreImp_FnApfCodigo[0] < A1260FuncaoAPFPreImp_FnApfCodigo ) || ( T003F8_A1260FuncaoAPFPreImp_FnApfCodigo[0] == A1260FuncaoAPFPreImp_FnApfCodigo ) && ( T003F8_A1257FuncaoAPFPreImp_Sequencial[0] < A1257FuncaoAPFPreImp_Sequencial ) ) )
            {
               pr_default.readNext(6);
            }
            if ( (pr_default.getStatus(6) != 101) && ( ( T003F8_A1260FuncaoAPFPreImp_FnApfCodigo[0] > A1260FuncaoAPFPreImp_FnApfCodigo ) || ( T003F8_A1260FuncaoAPFPreImp_FnApfCodigo[0] == A1260FuncaoAPFPreImp_FnApfCodigo ) && ( T003F8_A1257FuncaoAPFPreImp_Sequencial[0] > A1257FuncaoAPFPreImp_Sequencial ) ) )
            {
               A1260FuncaoAPFPreImp_FnApfCodigo = T003F8_A1260FuncaoAPFPreImp_FnApfCodigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1260FuncaoAPFPreImp_FnApfCodigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1260FuncaoAPFPreImp_FnApfCodigo), 6, 0)));
               A1257FuncaoAPFPreImp_Sequencial = T003F8_A1257FuncaoAPFPreImp_Sequencial[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1257FuncaoAPFPreImp_Sequencial", StringUtil.LTrim( StringUtil.Str( (decimal)(A1257FuncaoAPFPreImp_Sequencial), 3, 0)));
               RcdFound153 = 1;
            }
         }
         pr_default.close(6);
      }

      protected void move_previous( )
      {
         RcdFound153 = 0;
         /* Using cursor T003F9 */
         pr_default.execute(7, new Object[] {A1260FuncaoAPFPreImp_FnApfCodigo, A1257FuncaoAPFPreImp_Sequencial});
         if ( (pr_default.getStatus(7) != 101) )
         {
            while ( (pr_default.getStatus(7) != 101) && ( ( T003F9_A1260FuncaoAPFPreImp_FnApfCodigo[0] > A1260FuncaoAPFPreImp_FnApfCodigo ) || ( T003F9_A1260FuncaoAPFPreImp_FnApfCodigo[0] == A1260FuncaoAPFPreImp_FnApfCodigo ) && ( T003F9_A1257FuncaoAPFPreImp_Sequencial[0] > A1257FuncaoAPFPreImp_Sequencial ) ) )
            {
               pr_default.readNext(7);
            }
            if ( (pr_default.getStatus(7) != 101) && ( ( T003F9_A1260FuncaoAPFPreImp_FnApfCodigo[0] < A1260FuncaoAPFPreImp_FnApfCodigo ) || ( T003F9_A1260FuncaoAPFPreImp_FnApfCodigo[0] == A1260FuncaoAPFPreImp_FnApfCodigo ) && ( T003F9_A1257FuncaoAPFPreImp_Sequencial[0] < A1257FuncaoAPFPreImp_Sequencial ) ) )
            {
               A1260FuncaoAPFPreImp_FnApfCodigo = T003F9_A1260FuncaoAPFPreImp_FnApfCodigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1260FuncaoAPFPreImp_FnApfCodigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1260FuncaoAPFPreImp_FnApfCodigo), 6, 0)));
               A1257FuncaoAPFPreImp_Sequencial = T003F9_A1257FuncaoAPFPreImp_Sequencial[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1257FuncaoAPFPreImp_Sequencial", StringUtil.LTrim( StringUtil.Str( (decimal)(A1257FuncaoAPFPreImp_Sequencial), 3, 0)));
               RcdFound153 = 1;
            }
         }
         pr_default.close(7);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey3F153( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtFuncaoAPFPreImp_FnApfCodigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert3F153( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound153 == 1 )
            {
               if ( ( A1260FuncaoAPFPreImp_FnApfCodigo != Z1260FuncaoAPFPreImp_FnApfCodigo ) || ( A1257FuncaoAPFPreImp_Sequencial != Z1257FuncaoAPFPreImp_Sequencial ) )
               {
                  A1260FuncaoAPFPreImp_FnApfCodigo = Z1260FuncaoAPFPreImp_FnApfCodigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1260FuncaoAPFPreImp_FnApfCodigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1260FuncaoAPFPreImp_FnApfCodigo), 6, 0)));
                  A1257FuncaoAPFPreImp_Sequencial = Z1257FuncaoAPFPreImp_Sequencial;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1257FuncaoAPFPreImp_Sequencial", StringUtil.LTrim( StringUtil.Str( (decimal)(A1257FuncaoAPFPreImp_Sequencial), 3, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "FUNCAOAPFPREIMP_FNAPFCODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtFuncaoAPFPreImp_FnApfCodigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtFuncaoAPFPreImp_FnApfCodigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  Gx_mode = "UPD";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Update record */
                  Update3F153( ) ;
                  GX_FocusControl = edtFuncaoAPFPreImp_FnApfCodigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( ( A1260FuncaoAPFPreImp_FnApfCodigo != Z1260FuncaoAPFPreImp_FnApfCodigo ) || ( A1257FuncaoAPFPreImp_Sequencial != Z1257FuncaoAPFPreImp_Sequencial ) )
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Insert record */
                  GX_FocusControl = edtFuncaoAPFPreImp_FnApfCodigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert3F153( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "FUNCAOAPFPREIMP_FNAPFCODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtFuncaoAPFPreImp_FnApfCodigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     Gx_mode = "INS";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     /* Insert record */
                     GX_FocusControl = edtFuncaoAPFPreImp_FnApfCodigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert3F153( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
      }

      protected void btn_delete( )
      {
         if ( ( A1260FuncaoAPFPreImp_FnApfCodigo != Z1260FuncaoAPFPreImp_FnApfCodigo ) || ( A1257FuncaoAPFPreImp_Sequencial != Z1257FuncaoAPFPreImp_Sequencial ) )
         {
            A1260FuncaoAPFPreImp_FnApfCodigo = Z1260FuncaoAPFPreImp_FnApfCodigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1260FuncaoAPFPreImp_FnApfCodigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1260FuncaoAPFPreImp_FnApfCodigo), 6, 0)));
            A1257FuncaoAPFPreImp_Sequencial = Z1257FuncaoAPFPreImp_Sequencial;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1257FuncaoAPFPreImp_Sequencial", StringUtil.LTrim( StringUtil.Str( (decimal)(A1257FuncaoAPFPreImp_Sequencial), 3, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "FUNCAOAPFPREIMP_FNAPFCODIGO");
            AnyError = 1;
            GX_FocusControl = edtFuncaoAPFPreImp_FnApfCodigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtFuncaoAPFPreImp_FnApfCodigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            getByPrimaryKey( ) ;
         }
         CloseOpenCursors();
      }

      protected void btn_get( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         if ( RcdFound153 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "FUNCAOAPFPREIMP_FNAPFCODIGO");
            AnyError = 1;
            GX_FocusControl = edtFuncaoAPFPreImp_FnApfCodigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GX_FocusControl = cmbFuncaoAPFPreImp_Tipo_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_first( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart3F153( ) ;
         if ( RcdFound153 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = cmbFuncaoAPFPreImp_Tipo_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         ScanEnd3F153( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_previous( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_previous( ) ;
         if ( RcdFound153 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = cmbFuncaoAPFPreImp_Tipo_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_next( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_next( ) ;
         if ( RcdFound153 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = cmbFuncaoAPFPreImp_Tipo_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_last( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart3F153( ) ;
         if ( RcdFound153 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            while ( RcdFound153 != 0 )
            {
               ScanNext3F153( ) ;
            }
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = cmbFuncaoAPFPreImp_Tipo_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         ScanEnd3F153( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_select( )
      {
         getEqualNoModal( ) ;
      }

      protected void CheckOptimisticConcurrency3F153( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T003F2 */
            pr_default.execute(0, new Object[] {A1260FuncaoAPFPreImp_FnApfCodigo, A1257FuncaoAPFPreImp_Sequencial});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"FuncaoAPFPreImp"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z1248FuncaoAPFPreImp_Tipo, T003F2_A1248FuncaoAPFPreImp_Tipo[0]) != 0 ) || ( Z1249FuncaoAPFPreImp_DERImp != T003F2_A1249FuncaoAPFPreImp_DERImp[0] ) || ( Z1250FuncaoAPFPreImp_RAImp != T003F2_A1250FuncaoAPFPreImp_RAImp[0] ) )
            {
               if ( StringUtil.StrCmp(Z1248FuncaoAPFPreImp_Tipo, T003F2_A1248FuncaoAPFPreImp_Tipo[0]) != 0 )
               {
                  GXUtil.WriteLog("funcaoapfpreimp:[seudo value changed for attri]"+"FuncaoAPFPreImp_Tipo");
                  GXUtil.WriteLogRaw("Old: ",Z1248FuncaoAPFPreImp_Tipo);
                  GXUtil.WriteLogRaw("Current: ",T003F2_A1248FuncaoAPFPreImp_Tipo[0]);
               }
               if ( Z1249FuncaoAPFPreImp_DERImp != T003F2_A1249FuncaoAPFPreImp_DERImp[0] )
               {
                  GXUtil.WriteLog("funcaoapfpreimp:[seudo value changed for attri]"+"FuncaoAPFPreImp_DERImp");
                  GXUtil.WriteLogRaw("Old: ",Z1249FuncaoAPFPreImp_DERImp);
                  GXUtil.WriteLogRaw("Current: ",T003F2_A1249FuncaoAPFPreImp_DERImp[0]);
               }
               if ( Z1250FuncaoAPFPreImp_RAImp != T003F2_A1250FuncaoAPFPreImp_RAImp[0] )
               {
                  GXUtil.WriteLog("funcaoapfpreimp:[seudo value changed for attri]"+"FuncaoAPFPreImp_RAImp");
                  GXUtil.WriteLogRaw("Old: ",Z1250FuncaoAPFPreImp_RAImp);
                  GXUtil.WriteLogRaw("Current: ",T003F2_A1250FuncaoAPFPreImp_RAImp[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"FuncaoAPFPreImp"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert3F153( )
      {
         BeforeValidate3F153( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable3F153( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM3F153( 0) ;
            CheckOptimisticConcurrency3F153( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm3F153( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert3F153( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T003F10 */
                     pr_default.execute(8, new Object[] {A1257FuncaoAPFPreImp_Sequencial, n1248FuncaoAPFPreImp_Tipo, A1248FuncaoAPFPreImp_Tipo, n1249FuncaoAPFPreImp_DERImp, A1249FuncaoAPFPreImp_DERImp, n1250FuncaoAPFPreImp_RAImp, A1250FuncaoAPFPreImp_RAImp, n1251FuncaoAPFPreImp_Observacao, A1251FuncaoAPFPreImp_Observacao, A1260FuncaoAPFPreImp_FnApfCodigo});
                     pr_default.close(8);
                     dsDefault.SmartCacheProvider.SetUpdated("FuncaoAPFPreImp") ;
                     if ( (pr_default.getStatus(8) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption3F0( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load3F153( ) ;
            }
            EndLevel3F153( ) ;
         }
         CloseExtendedTableCursors3F153( ) ;
      }

      protected void Update3F153( )
      {
         BeforeValidate3F153( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable3F153( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency3F153( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm3F153( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate3F153( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T003F11 */
                     pr_default.execute(9, new Object[] {n1248FuncaoAPFPreImp_Tipo, A1248FuncaoAPFPreImp_Tipo, n1249FuncaoAPFPreImp_DERImp, A1249FuncaoAPFPreImp_DERImp, n1250FuncaoAPFPreImp_RAImp, A1250FuncaoAPFPreImp_RAImp, n1251FuncaoAPFPreImp_Observacao, A1251FuncaoAPFPreImp_Observacao, A1260FuncaoAPFPreImp_FnApfCodigo, A1257FuncaoAPFPreImp_Sequencial});
                     pr_default.close(9);
                     dsDefault.SmartCacheProvider.SetUpdated("FuncaoAPFPreImp") ;
                     if ( (pr_default.getStatus(9) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"FuncaoAPFPreImp"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate3F153( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                           ResetCaption3F0( ) ;
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel3F153( ) ;
         }
         CloseExtendedTableCursors3F153( ) ;
      }

      protected void DeferredUpdate3F153( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         BeforeValidate3F153( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency3F153( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls3F153( ) ;
            AfterConfirm3F153( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete3F153( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T003F12 */
                  pr_default.execute(10, new Object[] {A1260FuncaoAPFPreImp_FnApfCodigo, A1257FuncaoAPFPreImp_Sequencial});
                  pr_default.close(10);
                  dsDefault.SmartCacheProvider.SetUpdated("FuncaoAPFPreImp") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        move_next( ) ;
                        if ( RcdFound153 == 0 )
                        {
                           InitAll3F153( ) ;
                           Gx_mode = "INS";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        }
                        else
                        {
                           getByPrimaryKey( ) ;
                           Gx_mode = "UPD";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        }
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                        ResetCaption3F0( ) ;
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode153 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         EndLevel3F153( ) ;
         Gx_mode = sMode153;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
      }

      protected void OnDeleteControls3F153( )
      {
         standaloneModal( ) ;
         /* No delete mode formulas found. */
      }

      protected void EndLevel3F153( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete3F153( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            context.CommitDataStores( "FuncaoAPFPreImp");
            if ( AnyError == 0 )
            {
               ConfirmValues3F0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            context.RollbackDataStores( "FuncaoAPFPreImp");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart3F153( )
      {
         /* Using cursor T003F13 */
         pr_default.execute(11);
         RcdFound153 = 0;
         if ( (pr_default.getStatus(11) != 101) )
         {
            RcdFound153 = 1;
            A1260FuncaoAPFPreImp_FnApfCodigo = T003F13_A1260FuncaoAPFPreImp_FnApfCodigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1260FuncaoAPFPreImp_FnApfCodigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1260FuncaoAPFPreImp_FnApfCodigo), 6, 0)));
            A1257FuncaoAPFPreImp_Sequencial = T003F13_A1257FuncaoAPFPreImp_Sequencial[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1257FuncaoAPFPreImp_Sequencial", StringUtil.LTrim( StringUtil.Str( (decimal)(A1257FuncaoAPFPreImp_Sequencial), 3, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext3F153( )
      {
         /* Scan next routine */
         pr_default.readNext(11);
         RcdFound153 = 0;
         if ( (pr_default.getStatus(11) != 101) )
         {
            RcdFound153 = 1;
            A1260FuncaoAPFPreImp_FnApfCodigo = T003F13_A1260FuncaoAPFPreImp_FnApfCodigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1260FuncaoAPFPreImp_FnApfCodigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1260FuncaoAPFPreImp_FnApfCodigo), 6, 0)));
            A1257FuncaoAPFPreImp_Sequencial = T003F13_A1257FuncaoAPFPreImp_Sequencial[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1257FuncaoAPFPreImp_Sequencial", StringUtil.LTrim( StringUtil.Str( (decimal)(A1257FuncaoAPFPreImp_Sequencial), 3, 0)));
         }
      }

      protected void ScanEnd3F153( )
      {
         pr_default.close(11);
      }

      protected void AfterConfirm3F153( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert3F153( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate3F153( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete3F153( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete3F153( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate3F153( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes3F153( )
      {
         edtFuncaoAPFPreImp_FnApfCodigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoAPFPreImp_FnApfCodigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncaoAPFPreImp_FnApfCodigo_Enabled), 5, 0)));
         edtFuncaoAPFPreImp_Sequencial_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoAPFPreImp_Sequencial_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncaoAPFPreImp_Sequencial_Enabled), 5, 0)));
         cmbFuncaoAPFPreImp_Tipo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbFuncaoAPFPreImp_Tipo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbFuncaoAPFPreImp_Tipo.Enabled), 5, 0)));
         edtFuncaoAPFPreImp_DERImp_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoAPFPreImp_DERImp_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncaoAPFPreImp_DERImp_Enabled), 5, 0)));
         edtFuncaoAPFPreImp_RAImp_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoAPFPreImp_RAImp_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncaoAPFPreImp_RAImp_Enabled), 5, 0)));
         Funcaoapfpreimp_observacao_Enabled = Convert.ToBoolean( 0);
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Funcaoapfpreimp_observacao_Internalname, "Enabled", StringUtil.BoolToStr( Funcaoapfpreimp_observacao_Enabled));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues3F0( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20205322403581");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("CKEditor/ckeditor/ckeditor.js", "");
         context.AddJavascriptSource("CKEditor/CKEditorRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("funcaoapfpreimp.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z1260FuncaoAPFPreImp_FnApfCodigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1260FuncaoAPFPreImp_FnApfCodigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1257FuncaoAPFPreImp_Sequencial", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1257FuncaoAPFPreImp_Sequencial), 3, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1248FuncaoAPFPreImp_Tipo", StringUtil.RTrim( Z1248FuncaoAPFPreImp_Tipo));
         GxWebStd.gx_hidden_field( context, "Z1249FuncaoAPFPreImp_DERImp", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1249FuncaoAPFPreImp_DERImp), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1250FuncaoAPFPreImp_RAImp", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1250FuncaoAPFPreImp_RAImp), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "FUNCAOAPFPREIMP_OBSERVACAO", A1251FuncaoAPFPreImp_Observacao);
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "FUNCAOAPFPREIMP_OBSERVACAO_Enabled", StringUtil.BoolToStr( Funcaoapfpreimp_observacao_Enabled));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("funcaoapfpreimp.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "FuncaoAPFPreImp" ;
      }

      public override String GetPgmdesc( )
      {
         return "Funcao APF Pre Impprta��o" ;
      }

      protected void InitializeNonKey3F153( )
      {
         A1248FuncaoAPFPreImp_Tipo = "";
         n1248FuncaoAPFPreImp_Tipo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1248FuncaoAPFPreImp_Tipo", A1248FuncaoAPFPreImp_Tipo);
         n1248FuncaoAPFPreImp_Tipo = (String.IsNullOrEmpty(StringUtil.RTrim( A1248FuncaoAPFPreImp_Tipo)) ? true : false);
         A1249FuncaoAPFPreImp_DERImp = 0;
         n1249FuncaoAPFPreImp_DERImp = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1249FuncaoAPFPreImp_DERImp", StringUtil.LTrim( StringUtil.Str( (decimal)(A1249FuncaoAPFPreImp_DERImp), 4, 0)));
         n1249FuncaoAPFPreImp_DERImp = ((0==A1249FuncaoAPFPreImp_DERImp) ? true : false);
         A1250FuncaoAPFPreImp_RAImp = 0;
         n1250FuncaoAPFPreImp_RAImp = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1250FuncaoAPFPreImp_RAImp", StringUtil.LTrim( StringUtil.Str( (decimal)(A1250FuncaoAPFPreImp_RAImp), 4, 0)));
         n1250FuncaoAPFPreImp_RAImp = ((0==A1250FuncaoAPFPreImp_RAImp) ? true : false);
         A1251FuncaoAPFPreImp_Observacao = "";
         n1251FuncaoAPFPreImp_Observacao = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1251FuncaoAPFPreImp_Observacao", A1251FuncaoAPFPreImp_Observacao);
         Z1248FuncaoAPFPreImp_Tipo = "";
         Z1249FuncaoAPFPreImp_DERImp = 0;
         Z1250FuncaoAPFPreImp_RAImp = 0;
      }

      protected void InitAll3F153( )
      {
         A1260FuncaoAPFPreImp_FnApfCodigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1260FuncaoAPFPreImp_FnApfCodigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1260FuncaoAPFPreImp_FnApfCodigo), 6, 0)));
         A1257FuncaoAPFPreImp_Sequencial = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1257FuncaoAPFPreImp_Sequencial", StringUtil.LTrim( StringUtil.Str( (decimal)(A1257FuncaoAPFPreImp_Sequencial), 3, 0)));
         InitializeNonKey3F153( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?2249787");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20205322403585");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("funcaoapfpreimp.js", "?20205322403585");
         context.AddJavascriptSource("CKEditor/ckeditor/ckeditor.js", "");
         context.AddJavascriptSource("CKEditor/CKEditorRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         imgBtn_first_Internalname = "BTN_FIRST";
         imgBtn_first_separator_Internalname = "BTN_FIRST_SEPARATOR";
         imgBtn_previous_Internalname = "BTN_PREVIOUS";
         imgBtn_previous_separator_Internalname = "BTN_PREVIOUS_SEPARATOR";
         imgBtn_next_Internalname = "BTN_NEXT";
         imgBtn_next_separator_Internalname = "BTN_NEXT_SEPARATOR";
         imgBtn_last_Internalname = "BTN_LAST";
         imgBtn_last_separator_Internalname = "BTN_LAST_SEPARATOR";
         imgBtn_select_Internalname = "BTN_SELECT";
         imgBtn_select_separator_Internalname = "BTN_SELECT_SEPARATOR";
         imgBtn_enter2_Internalname = "BTN_ENTER2";
         imgBtn_enter2_separator_Internalname = "BTN_ENTER2_SEPARATOR";
         imgBtn_cancel2_Internalname = "BTN_CANCEL2";
         imgBtn_cancel2_separator_Internalname = "BTN_CANCEL2_SEPARATOR";
         imgBtn_delete2_Internalname = "BTN_DELETE2";
         imgBtn_delete2_separator_Internalname = "BTN_DELETE2_SEPARATOR";
         divSectiontoolbar_Internalname = "SECTIONTOOLBAR";
         tblTabletoolbar_Internalname = "TABLETOOLBAR";
         lblTextblockfuncaoapfpreimp_fnapfcodigo_Internalname = "TEXTBLOCKFUNCAOAPFPREIMP_FNAPFCODIGO";
         edtFuncaoAPFPreImp_FnApfCodigo_Internalname = "FUNCAOAPFPREIMP_FNAPFCODIGO";
         lblTextblockfuncaoapfpreimp_sequencial_Internalname = "TEXTBLOCKFUNCAOAPFPREIMP_SEQUENCIAL";
         edtFuncaoAPFPreImp_Sequencial_Internalname = "FUNCAOAPFPREIMP_SEQUENCIAL";
         lblTextblockfuncaoapfpreimp_tipo_Internalname = "TEXTBLOCKFUNCAOAPFPREIMP_TIPO";
         cmbFuncaoAPFPreImp_Tipo_Internalname = "FUNCAOAPFPREIMP_TIPO";
         lblTextblockfuncaoapfpreimp_derimp_Internalname = "TEXTBLOCKFUNCAOAPFPREIMP_DERIMP";
         edtFuncaoAPFPreImp_DERImp_Internalname = "FUNCAOAPFPREIMP_DERIMP";
         lblTextblockfuncaoapfpreimp_raimp_Internalname = "TEXTBLOCKFUNCAOAPFPREIMP_RAIMP";
         edtFuncaoAPFPreImp_RAImp_Internalname = "FUNCAOAPFPREIMP_RAIMP";
         lblTextblockfuncaoapfpreimp_observacao_Internalname = "TEXTBLOCKFUNCAOAPFPREIMP_OBSERVACAO";
         Funcaoapfpreimp_observacao_Internalname = "FUNCAOAPFPREIMP_OBSERVACAO";
         tblTable2_Internalname = "TABLE2";
         bttBtn_enter_Internalname = "BTN_ENTER";
         bttBtn_cancel_Internalname = "BTN_CANCEL";
         bttBtn_delete_Internalname = "BTN_DELETE";
         tblTable1_Internalname = "TABLE1";
         grpGroupdata_Internalname = "GROUPDATA";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Funcao APF Pre Impprta��o";
         imgBtn_delete2_separator_Visible = 1;
         imgBtn_delete2_Enabled = 1;
         imgBtn_delete2_Visible = 1;
         imgBtn_cancel2_separator_Visible = 1;
         imgBtn_cancel2_Visible = 1;
         imgBtn_enter2_separator_Visible = 1;
         imgBtn_enter2_Enabled = 1;
         imgBtn_enter2_Visible = 1;
         imgBtn_select_separator_Visible = 1;
         imgBtn_select_Visible = 1;
         imgBtn_last_separator_Visible = 1;
         imgBtn_last_Visible = 1;
         imgBtn_next_separator_Visible = 1;
         imgBtn_next_Visible = 1;
         imgBtn_previous_separator_Visible = 1;
         imgBtn_previous_Visible = 1;
         imgBtn_first_separator_Visible = 1;
         imgBtn_first_Visible = 1;
         Funcaoapfpreimp_observacao_Enabled = Convert.ToBoolean( 1);
         edtFuncaoAPFPreImp_RAImp_Jsonclick = "";
         edtFuncaoAPFPreImp_RAImp_Enabled = 1;
         edtFuncaoAPFPreImp_DERImp_Jsonclick = "";
         edtFuncaoAPFPreImp_DERImp_Enabled = 1;
         cmbFuncaoAPFPreImp_Tipo_Jsonclick = "";
         cmbFuncaoAPFPreImp_Tipo.Enabled = 1;
         edtFuncaoAPFPreImp_Sequencial_Jsonclick = "";
         edtFuncaoAPFPreImp_Sequencial_Enabled = 1;
         edtFuncaoAPFPreImp_FnApfCodigo_Jsonclick = "";
         edtFuncaoAPFPreImp_FnApfCodigo_Enabled = 1;
         bttBtn_delete_Visible = 1;
         bttBtn_cancel_Visible = 1;
         bttBtn_enter_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void AfterKeyLoadScreen( )
      {
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         /* Using cursor T003F14 */
         pr_default.execute(12, new Object[] {A1260FuncaoAPFPreImp_FnApfCodigo});
         if ( (pr_default.getStatus(12) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Funcao APFPre Imp_Funcao APF'.", "ForeignKeyNotFound", 1, "FUNCAOAPFPREIMP_FNAPFCODIGO");
            AnyError = 1;
            GX_FocusControl = edtFuncaoAPFPreImp_FnApfCodigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         pr_default.close(12);
         GX_FocusControl = cmbFuncaoAPFPreImp_Tipo_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         standaloneNotModal( ) ;
         standaloneModal( ) ;
         /* End function AfterKeyLoadScreen */
      }

      public void Valid_Funcaoapfpreimp_fnapfcodigo( int GX_Parm1 )
      {
         A1260FuncaoAPFPreImp_FnApfCodigo = GX_Parm1;
         /* Using cursor T003F14 */
         pr_default.execute(12, new Object[] {A1260FuncaoAPFPreImp_FnApfCodigo});
         if ( (pr_default.getStatus(12) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Funcao APFPre Imp_Funcao APF'.", "ForeignKeyNotFound", 1, "FUNCAOAPFPREIMP_FNAPFCODIGO");
            AnyError = 1;
            GX_FocusControl = edtFuncaoAPFPreImp_FnApfCodigo_Internalname;
         }
         pr_default.close(12);
         dynload_actions( ) ;
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Funcaoapfpreimp_sequencial( int GX_Parm1 ,
                                                    short GX_Parm2 ,
                                                    GXCombobox cmbGX_Parm3 ,
                                                    short GX_Parm4 ,
                                                    short GX_Parm5 ,
                                                    String GX_Parm6 )
      {
         A1260FuncaoAPFPreImp_FnApfCodigo = GX_Parm1;
         A1257FuncaoAPFPreImp_Sequencial = GX_Parm2;
         cmbFuncaoAPFPreImp_Tipo = cmbGX_Parm3;
         A1248FuncaoAPFPreImp_Tipo = cmbFuncaoAPFPreImp_Tipo.CurrentValue;
         n1248FuncaoAPFPreImp_Tipo = false;
         cmbFuncaoAPFPreImp_Tipo.CurrentValue = A1248FuncaoAPFPreImp_Tipo;
         A1249FuncaoAPFPreImp_DERImp = GX_Parm4;
         n1249FuncaoAPFPreImp_DERImp = false;
         A1250FuncaoAPFPreImp_RAImp = GX_Parm5;
         n1250FuncaoAPFPreImp_RAImp = false;
         A1251FuncaoAPFPreImp_Observacao = GX_Parm6;
         n1251FuncaoAPFPreImp_Observacao = false;
         context.wbHandled = 1;
         AfterKeyLoadScreen( ) ;
         Draw( ) ;
         SendCloseFormHiddens( ) ;
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
         }
         cmbFuncaoAPFPreImp_Tipo.CurrentValue = A1248FuncaoAPFPreImp_Tipo;
         isValidOutput.Add(cmbFuncaoAPFPreImp_Tipo);
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A1249FuncaoAPFPreImp_DERImp), 4, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A1250FuncaoAPFPreImp_RAImp), 4, 0, ".", "")));
         isValidOutput.Add(A1251FuncaoAPFPreImp_Observacao);
         isValidOutput.Add(StringUtil.RTrim( Gx_mode));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1260FuncaoAPFPreImp_FnApfCodigo), 6, 0, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1257FuncaoAPFPreImp_Sequencial), 3, 0, ",", "")));
         isValidOutput.Add(StringUtil.RTrim( Z1248FuncaoAPFPreImp_Tipo));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1249FuncaoAPFPreImp_DERImp), 4, 0, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1250FuncaoAPFPreImp_RAImp), 4, 0, ",", "")));
         isValidOutput.Add(Z1251FuncaoAPFPreImp_Observacao);
         isValidOutput.Add(imgBtn_delete2_Enabled);
         isValidOutput.Add(imgBtn_enter2_Enabled);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(12);
      }

      public override void initialize( )
      {
         sPrefix = "";
         Z1248FuncaoAPFPreImp_Tipo = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         A1248FuncaoAPFPreImp_Tipo = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtn_enter_Jsonclick = "";
         bttBtn_cancel_Jsonclick = "";
         bttBtn_delete_Jsonclick = "";
         lblTextblockfuncaoapfpreimp_fnapfcodigo_Jsonclick = "";
         lblTextblockfuncaoapfpreimp_sequencial_Jsonclick = "";
         lblTextblockfuncaoapfpreimp_tipo_Jsonclick = "";
         lblTextblockfuncaoapfpreimp_derimp_Jsonclick = "";
         lblTextblockfuncaoapfpreimp_raimp_Jsonclick = "";
         lblTextblockfuncaoapfpreimp_observacao_Jsonclick = "";
         imgBtn_first_Jsonclick = "";
         imgBtn_first_separator_Jsonclick = "";
         imgBtn_previous_Jsonclick = "";
         imgBtn_previous_separator_Jsonclick = "";
         imgBtn_next_Jsonclick = "";
         imgBtn_next_separator_Jsonclick = "";
         imgBtn_last_Jsonclick = "";
         imgBtn_last_separator_Jsonclick = "";
         imgBtn_select_Jsonclick = "";
         imgBtn_select_separator_Jsonclick = "";
         imgBtn_enter2_Jsonclick = "";
         imgBtn_enter2_separator_Jsonclick = "";
         imgBtn_cancel2_Jsonclick = "";
         imgBtn_cancel2_separator_Jsonclick = "";
         imgBtn_delete2_Jsonclick = "";
         imgBtn_delete2_separator_Jsonclick = "";
         Gx_mode = "";
         A1251FuncaoAPFPreImp_Observacao = "";
         Funcaoapfpreimp_observacao_Width = "";
         Funcaoapfpreimp_observacao_Height = "";
         Funcaoapfpreimp_observacao_Skin = "";
         Funcaoapfpreimp_observacao_Toolbar = "";
         Funcaoapfpreimp_observacao_Class = "";
         Funcaoapfpreimp_observacao_Customtoolbar = "";
         Funcaoapfpreimp_observacao_Customconfiguration = "";
         Funcaoapfpreimp_observacao_Buttonpressedid = "";
         Funcaoapfpreimp_observacao_Captionvalue = "";
         Funcaoapfpreimp_observacao_Captionclass = "";
         Funcaoapfpreimp_observacao_Captionposition = "";
         Funcaoapfpreimp_observacao_Coltitle = "";
         Funcaoapfpreimp_observacao_Coltitlefont = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         Z1251FuncaoAPFPreImp_Observacao = "";
         T003F5_A1257FuncaoAPFPreImp_Sequencial = new short[1] ;
         T003F5_A1248FuncaoAPFPreImp_Tipo = new String[] {""} ;
         T003F5_n1248FuncaoAPFPreImp_Tipo = new bool[] {false} ;
         T003F5_A1249FuncaoAPFPreImp_DERImp = new short[1] ;
         T003F5_n1249FuncaoAPFPreImp_DERImp = new bool[] {false} ;
         T003F5_A1250FuncaoAPFPreImp_RAImp = new short[1] ;
         T003F5_n1250FuncaoAPFPreImp_RAImp = new bool[] {false} ;
         T003F5_A1251FuncaoAPFPreImp_Observacao = new String[] {""} ;
         T003F5_n1251FuncaoAPFPreImp_Observacao = new bool[] {false} ;
         T003F5_A1260FuncaoAPFPreImp_FnApfCodigo = new int[1] ;
         T003F4_A1260FuncaoAPFPreImp_FnApfCodigo = new int[1] ;
         T003F6_A1260FuncaoAPFPreImp_FnApfCodigo = new int[1] ;
         T003F7_A1260FuncaoAPFPreImp_FnApfCodigo = new int[1] ;
         T003F7_A1257FuncaoAPFPreImp_Sequencial = new short[1] ;
         T003F3_A1257FuncaoAPFPreImp_Sequencial = new short[1] ;
         T003F3_A1248FuncaoAPFPreImp_Tipo = new String[] {""} ;
         T003F3_n1248FuncaoAPFPreImp_Tipo = new bool[] {false} ;
         T003F3_A1249FuncaoAPFPreImp_DERImp = new short[1] ;
         T003F3_n1249FuncaoAPFPreImp_DERImp = new bool[] {false} ;
         T003F3_A1250FuncaoAPFPreImp_RAImp = new short[1] ;
         T003F3_n1250FuncaoAPFPreImp_RAImp = new bool[] {false} ;
         T003F3_A1251FuncaoAPFPreImp_Observacao = new String[] {""} ;
         T003F3_n1251FuncaoAPFPreImp_Observacao = new bool[] {false} ;
         T003F3_A1260FuncaoAPFPreImp_FnApfCodigo = new int[1] ;
         sMode153 = "";
         T003F8_A1260FuncaoAPFPreImp_FnApfCodigo = new int[1] ;
         T003F8_A1257FuncaoAPFPreImp_Sequencial = new short[1] ;
         T003F9_A1260FuncaoAPFPreImp_FnApfCodigo = new int[1] ;
         T003F9_A1257FuncaoAPFPreImp_Sequencial = new short[1] ;
         T003F2_A1257FuncaoAPFPreImp_Sequencial = new short[1] ;
         T003F2_A1248FuncaoAPFPreImp_Tipo = new String[] {""} ;
         T003F2_n1248FuncaoAPFPreImp_Tipo = new bool[] {false} ;
         T003F2_A1249FuncaoAPFPreImp_DERImp = new short[1] ;
         T003F2_n1249FuncaoAPFPreImp_DERImp = new bool[] {false} ;
         T003F2_A1250FuncaoAPFPreImp_RAImp = new short[1] ;
         T003F2_n1250FuncaoAPFPreImp_RAImp = new bool[] {false} ;
         T003F2_A1251FuncaoAPFPreImp_Observacao = new String[] {""} ;
         T003F2_n1251FuncaoAPFPreImp_Observacao = new bool[] {false} ;
         T003F2_A1260FuncaoAPFPreImp_FnApfCodigo = new int[1] ;
         T003F13_A1260FuncaoAPFPreImp_FnApfCodigo = new int[1] ;
         T003F13_A1257FuncaoAPFPreImp_Sequencial = new short[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         T003F14_A1260FuncaoAPFPreImp_FnApfCodigo = new int[1] ;
         isValidOutput = new GxUnknownObjectCollection();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.funcaoapfpreimp__default(),
            new Object[][] {
                new Object[] {
               T003F2_A1257FuncaoAPFPreImp_Sequencial, T003F2_A1248FuncaoAPFPreImp_Tipo, T003F2_n1248FuncaoAPFPreImp_Tipo, T003F2_A1249FuncaoAPFPreImp_DERImp, T003F2_n1249FuncaoAPFPreImp_DERImp, T003F2_A1250FuncaoAPFPreImp_RAImp, T003F2_n1250FuncaoAPFPreImp_RAImp, T003F2_A1251FuncaoAPFPreImp_Observacao, T003F2_n1251FuncaoAPFPreImp_Observacao, T003F2_A1260FuncaoAPFPreImp_FnApfCodigo
               }
               , new Object[] {
               T003F3_A1257FuncaoAPFPreImp_Sequencial, T003F3_A1248FuncaoAPFPreImp_Tipo, T003F3_n1248FuncaoAPFPreImp_Tipo, T003F3_A1249FuncaoAPFPreImp_DERImp, T003F3_n1249FuncaoAPFPreImp_DERImp, T003F3_A1250FuncaoAPFPreImp_RAImp, T003F3_n1250FuncaoAPFPreImp_RAImp, T003F3_A1251FuncaoAPFPreImp_Observacao, T003F3_n1251FuncaoAPFPreImp_Observacao, T003F3_A1260FuncaoAPFPreImp_FnApfCodigo
               }
               , new Object[] {
               T003F4_A1260FuncaoAPFPreImp_FnApfCodigo
               }
               , new Object[] {
               T003F5_A1257FuncaoAPFPreImp_Sequencial, T003F5_A1248FuncaoAPFPreImp_Tipo, T003F5_n1248FuncaoAPFPreImp_Tipo, T003F5_A1249FuncaoAPFPreImp_DERImp, T003F5_n1249FuncaoAPFPreImp_DERImp, T003F5_A1250FuncaoAPFPreImp_RAImp, T003F5_n1250FuncaoAPFPreImp_RAImp, T003F5_A1251FuncaoAPFPreImp_Observacao, T003F5_n1251FuncaoAPFPreImp_Observacao, T003F5_A1260FuncaoAPFPreImp_FnApfCodigo
               }
               , new Object[] {
               T003F6_A1260FuncaoAPFPreImp_FnApfCodigo
               }
               , new Object[] {
               T003F7_A1260FuncaoAPFPreImp_FnApfCodigo, T003F7_A1257FuncaoAPFPreImp_Sequencial
               }
               , new Object[] {
               T003F8_A1260FuncaoAPFPreImp_FnApfCodigo, T003F8_A1257FuncaoAPFPreImp_Sequencial
               }
               , new Object[] {
               T003F9_A1260FuncaoAPFPreImp_FnApfCodigo, T003F9_A1257FuncaoAPFPreImp_Sequencial
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T003F13_A1260FuncaoAPFPreImp_FnApfCodigo, T003F13_A1257FuncaoAPFPreImp_Sequencial
               }
               , new Object[] {
               T003F14_A1260FuncaoAPFPreImp_FnApfCodigo
               }
            }
         );
      }

      private short Z1257FuncaoAPFPreImp_Sequencial ;
      private short Z1249FuncaoAPFPreImp_DERImp ;
      private short Z1250FuncaoAPFPreImp_RAImp ;
      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short A1257FuncaoAPFPreImp_Sequencial ;
      private short A1249FuncaoAPFPreImp_DERImp ;
      private short A1250FuncaoAPFPreImp_RAImp ;
      private short GX_JID ;
      private short RcdFound153 ;
      private short Gx_BScreen ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int Z1260FuncaoAPFPreImp_FnApfCodigo ;
      private int A1260FuncaoAPFPreImp_FnApfCodigo ;
      private int trnEnded ;
      private int bttBtn_enter_Visible ;
      private int bttBtn_cancel_Visible ;
      private int bttBtn_delete_Visible ;
      private int edtFuncaoAPFPreImp_FnApfCodigo_Enabled ;
      private int edtFuncaoAPFPreImp_Sequencial_Enabled ;
      private int edtFuncaoAPFPreImp_DERImp_Enabled ;
      private int edtFuncaoAPFPreImp_RAImp_Enabled ;
      private int imgBtn_first_Visible ;
      private int imgBtn_first_separator_Visible ;
      private int imgBtn_previous_Visible ;
      private int imgBtn_previous_separator_Visible ;
      private int imgBtn_next_Visible ;
      private int imgBtn_next_separator_Visible ;
      private int imgBtn_last_Visible ;
      private int imgBtn_last_separator_Visible ;
      private int imgBtn_select_Visible ;
      private int imgBtn_select_separator_Visible ;
      private int imgBtn_enter2_Visible ;
      private int imgBtn_enter2_Enabled ;
      private int imgBtn_enter2_separator_Visible ;
      private int imgBtn_cancel2_Visible ;
      private int imgBtn_cancel2_separator_Visible ;
      private int imgBtn_delete2_Visible ;
      private int imgBtn_delete2_Enabled ;
      private int imgBtn_delete2_separator_Visible ;
      private int Funcaoapfpreimp_observacao_Color ;
      private int Funcaoapfpreimp_observacao_Coltitlecolor ;
      private int idxLst ;
      private String sPrefix ;
      private String Z1248FuncaoAPFPreImp_Tipo ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String GXKey ;
      private String A1248FuncaoAPFPreImp_Tipo ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtFuncaoAPFPreImp_FnApfCodigo_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String grpGroupdata_Internalname ;
      private String tblTable1_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String TempTags ;
      private String bttBtn_enter_Internalname ;
      private String bttBtn_enter_Jsonclick ;
      private String bttBtn_cancel_Internalname ;
      private String bttBtn_cancel_Jsonclick ;
      private String bttBtn_delete_Internalname ;
      private String bttBtn_delete_Jsonclick ;
      private String tblTable2_Internalname ;
      private String lblTextblockfuncaoapfpreimp_fnapfcodigo_Internalname ;
      private String lblTextblockfuncaoapfpreimp_fnapfcodigo_Jsonclick ;
      private String edtFuncaoAPFPreImp_FnApfCodigo_Jsonclick ;
      private String lblTextblockfuncaoapfpreimp_sequencial_Internalname ;
      private String lblTextblockfuncaoapfpreimp_sequencial_Jsonclick ;
      private String edtFuncaoAPFPreImp_Sequencial_Internalname ;
      private String edtFuncaoAPFPreImp_Sequencial_Jsonclick ;
      private String lblTextblockfuncaoapfpreimp_tipo_Internalname ;
      private String lblTextblockfuncaoapfpreimp_tipo_Jsonclick ;
      private String cmbFuncaoAPFPreImp_Tipo_Internalname ;
      private String cmbFuncaoAPFPreImp_Tipo_Jsonclick ;
      private String lblTextblockfuncaoapfpreimp_derimp_Internalname ;
      private String lblTextblockfuncaoapfpreimp_derimp_Jsonclick ;
      private String edtFuncaoAPFPreImp_DERImp_Internalname ;
      private String edtFuncaoAPFPreImp_DERImp_Jsonclick ;
      private String lblTextblockfuncaoapfpreimp_raimp_Internalname ;
      private String lblTextblockfuncaoapfpreimp_raimp_Jsonclick ;
      private String edtFuncaoAPFPreImp_RAImp_Internalname ;
      private String edtFuncaoAPFPreImp_RAImp_Jsonclick ;
      private String lblTextblockfuncaoapfpreimp_observacao_Internalname ;
      private String lblTextblockfuncaoapfpreimp_observacao_Jsonclick ;
      private String tblTabletoolbar_Internalname ;
      private String divSectiontoolbar_Internalname ;
      private String imgBtn_first_Internalname ;
      private String imgBtn_first_Jsonclick ;
      private String imgBtn_first_separator_Internalname ;
      private String imgBtn_first_separator_Jsonclick ;
      private String imgBtn_previous_Internalname ;
      private String imgBtn_previous_Jsonclick ;
      private String imgBtn_previous_separator_Internalname ;
      private String imgBtn_previous_separator_Jsonclick ;
      private String imgBtn_next_Internalname ;
      private String imgBtn_next_Jsonclick ;
      private String imgBtn_next_separator_Internalname ;
      private String imgBtn_next_separator_Jsonclick ;
      private String imgBtn_last_Internalname ;
      private String imgBtn_last_Jsonclick ;
      private String imgBtn_last_separator_Internalname ;
      private String imgBtn_last_separator_Jsonclick ;
      private String imgBtn_select_Internalname ;
      private String imgBtn_select_Jsonclick ;
      private String imgBtn_select_separator_Internalname ;
      private String imgBtn_select_separator_Jsonclick ;
      private String imgBtn_enter2_Internalname ;
      private String imgBtn_enter2_Jsonclick ;
      private String imgBtn_enter2_separator_Internalname ;
      private String imgBtn_enter2_separator_Jsonclick ;
      private String imgBtn_cancel2_Internalname ;
      private String imgBtn_cancel2_Jsonclick ;
      private String imgBtn_cancel2_separator_Internalname ;
      private String imgBtn_cancel2_separator_Jsonclick ;
      private String imgBtn_delete2_Internalname ;
      private String imgBtn_delete2_Jsonclick ;
      private String imgBtn_delete2_separator_Internalname ;
      private String imgBtn_delete2_separator_Jsonclick ;
      private String Gx_mode ;
      private String Funcaoapfpreimp_observacao_Width ;
      private String Funcaoapfpreimp_observacao_Height ;
      private String Funcaoapfpreimp_observacao_Skin ;
      private String Funcaoapfpreimp_observacao_Toolbar ;
      private String Funcaoapfpreimp_observacao_Class ;
      private String Funcaoapfpreimp_observacao_Customtoolbar ;
      private String Funcaoapfpreimp_observacao_Customconfiguration ;
      private String Funcaoapfpreimp_observacao_Buttonpressedid ;
      private String Funcaoapfpreimp_observacao_Captionvalue ;
      private String Funcaoapfpreimp_observacao_Captionclass ;
      private String Funcaoapfpreimp_observacao_Captionposition ;
      private String Funcaoapfpreimp_observacao_Coltitle ;
      private String Funcaoapfpreimp_observacao_Coltitlefont ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String sMode153 ;
      private String Funcaoapfpreimp_observacao_Internalname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool n1248FuncaoAPFPreImp_Tipo ;
      private bool wbErr ;
      private bool n1249FuncaoAPFPreImp_DERImp ;
      private bool n1250FuncaoAPFPreImp_RAImp ;
      private bool n1251FuncaoAPFPreImp_Observacao ;
      private bool Funcaoapfpreimp_observacao_Enabled ;
      private bool Funcaoapfpreimp_observacao_Toolbarcancollapse ;
      private bool Funcaoapfpreimp_observacao_Toolbarexpanded ;
      private bool Funcaoapfpreimp_observacao_Usercontroliscolumn ;
      private bool Funcaoapfpreimp_observacao_Visible ;
      private String A1251FuncaoAPFPreImp_Observacao ;
      private String Z1251FuncaoAPFPreImp_Observacao ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbFuncaoAPFPreImp_Tipo ;
      private IDataStoreProvider pr_default ;
      private short[] T003F5_A1257FuncaoAPFPreImp_Sequencial ;
      private String[] T003F5_A1248FuncaoAPFPreImp_Tipo ;
      private bool[] T003F5_n1248FuncaoAPFPreImp_Tipo ;
      private short[] T003F5_A1249FuncaoAPFPreImp_DERImp ;
      private bool[] T003F5_n1249FuncaoAPFPreImp_DERImp ;
      private short[] T003F5_A1250FuncaoAPFPreImp_RAImp ;
      private bool[] T003F5_n1250FuncaoAPFPreImp_RAImp ;
      private String[] T003F5_A1251FuncaoAPFPreImp_Observacao ;
      private bool[] T003F5_n1251FuncaoAPFPreImp_Observacao ;
      private int[] T003F5_A1260FuncaoAPFPreImp_FnApfCodigo ;
      private int[] T003F4_A1260FuncaoAPFPreImp_FnApfCodigo ;
      private int[] T003F6_A1260FuncaoAPFPreImp_FnApfCodigo ;
      private int[] T003F7_A1260FuncaoAPFPreImp_FnApfCodigo ;
      private short[] T003F7_A1257FuncaoAPFPreImp_Sequencial ;
      private short[] T003F3_A1257FuncaoAPFPreImp_Sequencial ;
      private String[] T003F3_A1248FuncaoAPFPreImp_Tipo ;
      private bool[] T003F3_n1248FuncaoAPFPreImp_Tipo ;
      private short[] T003F3_A1249FuncaoAPFPreImp_DERImp ;
      private bool[] T003F3_n1249FuncaoAPFPreImp_DERImp ;
      private short[] T003F3_A1250FuncaoAPFPreImp_RAImp ;
      private bool[] T003F3_n1250FuncaoAPFPreImp_RAImp ;
      private String[] T003F3_A1251FuncaoAPFPreImp_Observacao ;
      private bool[] T003F3_n1251FuncaoAPFPreImp_Observacao ;
      private int[] T003F3_A1260FuncaoAPFPreImp_FnApfCodigo ;
      private int[] T003F8_A1260FuncaoAPFPreImp_FnApfCodigo ;
      private short[] T003F8_A1257FuncaoAPFPreImp_Sequencial ;
      private int[] T003F9_A1260FuncaoAPFPreImp_FnApfCodigo ;
      private short[] T003F9_A1257FuncaoAPFPreImp_Sequencial ;
      private short[] T003F2_A1257FuncaoAPFPreImp_Sequencial ;
      private String[] T003F2_A1248FuncaoAPFPreImp_Tipo ;
      private bool[] T003F2_n1248FuncaoAPFPreImp_Tipo ;
      private short[] T003F2_A1249FuncaoAPFPreImp_DERImp ;
      private bool[] T003F2_n1249FuncaoAPFPreImp_DERImp ;
      private short[] T003F2_A1250FuncaoAPFPreImp_RAImp ;
      private bool[] T003F2_n1250FuncaoAPFPreImp_RAImp ;
      private String[] T003F2_A1251FuncaoAPFPreImp_Observacao ;
      private bool[] T003F2_n1251FuncaoAPFPreImp_Observacao ;
      private int[] T003F2_A1260FuncaoAPFPreImp_FnApfCodigo ;
      private int[] T003F13_A1260FuncaoAPFPreImp_FnApfCodigo ;
      private short[] T003F13_A1257FuncaoAPFPreImp_Sequencial ;
      private int[] T003F14_A1260FuncaoAPFPreImp_FnApfCodigo ;
      private GXWebForm Form ;
   }

   public class funcaoapfpreimp__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new UpdateCursor(def[8])
         ,new UpdateCursor(def[9])
         ,new UpdateCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT003F5 ;
          prmT003F5 = new Object[] {
          new Object[] {"@FuncaoAPFPreImp_FnApfCodigo",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoAPFPreImp_Sequencial",SqlDbType.SmallInt,3,0}
          } ;
          Object[] prmT003F4 ;
          prmT003F4 = new Object[] {
          new Object[] {"@FuncaoAPFPreImp_FnApfCodigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003F6 ;
          prmT003F6 = new Object[] {
          new Object[] {"@FuncaoAPFPreImp_FnApfCodigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003F7 ;
          prmT003F7 = new Object[] {
          new Object[] {"@FuncaoAPFPreImp_FnApfCodigo",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoAPFPreImp_Sequencial",SqlDbType.SmallInt,3,0}
          } ;
          Object[] prmT003F3 ;
          prmT003F3 = new Object[] {
          new Object[] {"@FuncaoAPFPreImp_FnApfCodigo",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoAPFPreImp_Sequencial",SqlDbType.SmallInt,3,0}
          } ;
          Object[] prmT003F8 ;
          prmT003F8 = new Object[] {
          new Object[] {"@FuncaoAPFPreImp_FnApfCodigo",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoAPFPreImp_Sequencial",SqlDbType.SmallInt,3,0}
          } ;
          Object[] prmT003F9 ;
          prmT003F9 = new Object[] {
          new Object[] {"@FuncaoAPFPreImp_FnApfCodigo",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoAPFPreImp_Sequencial",SqlDbType.SmallInt,3,0}
          } ;
          Object[] prmT003F2 ;
          prmT003F2 = new Object[] {
          new Object[] {"@FuncaoAPFPreImp_FnApfCodigo",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoAPFPreImp_Sequencial",SqlDbType.SmallInt,3,0}
          } ;
          Object[] prmT003F10 ;
          prmT003F10 = new Object[] {
          new Object[] {"@FuncaoAPFPreImp_Sequencial",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@FuncaoAPFPreImp_Tipo",SqlDbType.Char,3,0} ,
          new Object[] {"@FuncaoAPFPreImp_DERImp",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@FuncaoAPFPreImp_RAImp",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@FuncaoAPFPreImp_Observacao",SqlDbType.VarChar,1000,0} ,
          new Object[] {"@FuncaoAPFPreImp_FnApfCodigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003F11 ;
          prmT003F11 = new Object[] {
          new Object[] {"@FuncaoAPFPreImp_Tipo",SqlDbType.Char,3,0} ,
          new Object[] {"@FuncaoAPFPreImp_DERImp",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@FuncaoAPFPreImp_RAImp",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@FuncaoAPFPreImp_Observacao",SqlDbType.VarChar,1000,0} ,
          new Object[] {"@FuncaoAPFPreImp_FnApfCodigo",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoAPFPreImp_Sequencial",SqlDbType.SmallInt,3,0}
          } ;
          Object[] prmT003F12 ;
          prmT003F12 = new Object[] {
          new Object[] {"@FuncaoAPFPreImp_FnApfCodigo",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoAPFPreImp_Sequencial",SqlDbType.SmallInt,3,0}
          } ;
          Object[] prmT003F13 ;
          prmT003F13 = new Object[] {
          } ;
          Object[] prmT003F14 ;
          prmT003F14 = new Object[] {
          new Object[] {"@FuncaoAPFPreImp_FnApfCodigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T003F2", "SELECT [FuncaoAPFPreImp_Sequencial], [FuncaoAPFPreImp_Tipo], [FuncaoAPFPreImp_DERImp], [FuncaoAPFPreImp_RAImp], [FuncaoAPFPreImp_Observacao], [FuncaoAPFPreImp_FnApfCodigo] AS FuncaoAPFPreImp_FnApfCodigo FROM [FuncaoAPFPreImp] WITH (UPDLOCK) WHERE [FuncaoAPFPreImp_FnApfCodigo] = @FuncaoAPFPreImp_FnApfCodigo AND [FuncaoAPFPreImp_Sequencial] = @FuncaoAPFPreImp_Sequencial ",true, GxErrorMask.GX_NOMASK, false, this,prmT003F2,1,0,true,false )
             ,new CursorDef("T003F3", "SELECT [FuncaoAPFPreImp_Sequencial], [FuncaoAPFPreImp_Tipo], [FuncaoAPFPreImp_DERImp], [FuncaoAPFPreImp_RAImp], [FuncaoAPFPreImp_Observacao], [FuncaoAPFPreImp_FnApfCodigo] AS FuncaoAPFPreImp_FnApfCodigo FROM [FuncaoAPFPreImp] WITH (NOLOCK) WHERE [FuncaoAPFPreImp_FnApfCodigo] = @FuncaoAPFPreImp_FnApfCodigo AND [FuncaoAPFPreImp_Sequencial] = @FuncaoAPFPreImp_Sequencial ",true, GxErrorMask.GX_NOMASK, false, this,prmT003F3,1,0,true,false )
             ,new CursorDef("T003F4", "SELECT [FuncaoAPF_Codigo] AS FuncaoAPFPreImp_FnApfCodigo FROM [FuncoesAPF] WITH (NOLOCK) WHERE [FuncaoAPF_Codigo] = @FuncaoAPFPreImp_FnApfCodigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003F4,1,0,true,false )
             ,new CursorDef("T003F5", "SELECT TM1.[FuncaoAPFPreImp_Sequencial], TM1.[FuncaoAPFPreImp_Tipo], TM1.[FuncaoAPFPreImp_DERImp], TM1.[FuncaoAPFPreImp_RAImp], TM1.[FuncaoAPFPreImp_Observacao], TM1.[FuncaoAPFPreImp_FnApfCodigo] AS FuncaoAPFPreImp_FnApfCodigo FROM [FuncaoAPFPreImp] TM1 WITH (NOLOCK) WHERE TM1.[FuncaoAPFPreImp_FnApfCodigo] = @FuncaoAPFPreImp_FnApfCodigo and TM1.[FuncaoAPFPreImp_Sequencial] = @FuncaoAPFPreImp_Sequencial ORDER BY TM1.[FuncaoAPFPreImp_FnApfCodigo], TM1.[FuncaoAPFPreImp_Sequencial]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT003F5,100,0,true,false )
             ,new CursorDef("T003F6", "SELECT [FuncaoAPF_Codigo] AS FuncaoAPFPreImp_FnApfCodigo FROM [FuncoesAPF] WITH (NOLOCK) WHERE [FuncaoAPF_Codigo] = @FuncaoAPFPreImp_FnApfCodigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003F6,1,0,true,false )
             ,new CursorDef("T003F7", "SELECT [FuncaoAPFPreImp_FnApfCodigo] AS FuncaoAPFPreImp_FnApfCodigo, [FuncaoAPFPreImp_Sequencial] FROM [FuncaoAPFPreImp] WITH (NOLOCK) WHERE [FuncaoAPFPreImp_FnApfCodigo] = @FuncaoAPFPreImp_FnApfCodigo AND [FuncaoAPFPreImp_Sequencial] = @FuncaoAPFPreImp_Sequencial  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT003F7,1,0,true,false )
             ,new CursorDef("T003F8", "SELECT TOP 1 [FuncaoAPFPreImp_FnApfCodigo] AS FuncaoAPFPreImp_FnApfCodigo, [FuncaoAPFPreImp_Sequencial] FROM [FuncaoAPFPreImp] WITH (NOLOCK) WHERE ( [FuncaoAPFPreImp_FnApfCodigo] > @FuncaoAPFPreImp_FnApfCodigo or [FuncaoAPFPreImp_FnApfCodigo] = @FuncaoAPFPreImp_FnApfCodigo and [FuncaoAPFPreImp_Sequencial] > @FuncaoAPFPreImp_Sequencial) ORDER BY [FuncaoAPFPreImp_FnApfCodigo], [FuncaoAPFPreImp_Sequencial]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT003F8,1,0,true,true )
             ,new CursorDef("T003F9", "SELECT TOP 1 [FuncaoAPFPreImp_FnApfCodigo] AS FuncaoAPFPreImp_FnApfCodigo, [FuncaoAPFPreImp_Sequencial] FROM [FuncaoAPFPreImp] WITH (NOLOCK) WHERE ( [FuncaoAPFPreImp_FnApfCodigo] < @FuncaoAPFPreImp_FnApfCodigo or [FuncaoAPFPreImp_FnApfCodigo] = @FuncaoAPFPreImp_FnApfCodigo and [FuncaoAPFPreImp_Sequencial] < @FuncaoAPFPreImp_Sequencial) ORDER BY [FuncaoAPFPreImp_FnApfCodigo] DESC, [FuncaoAPFPreImp_Sequencial] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT003F9,1,0,true,true )
             ,new CursorDef("T003F10", "INSERT INTO [FuncaoAPFPreImp]([FuncaoAPFPreImp_Sequencial], [FuncaoAPFPreImp_Tipo], [FuncaoAPFPreImp_DERImp], [FuncaoAPFPreImp_RAImp], [FuncaoAPFPreImp_Observacao], [FuncaoAPFPreImp_FnApfCodigo]) VALUES(@FuncaoAPFPreImp_Sequencial, @FuncaoAPFPreImp_Tipo, @FuncaoAPFPreImp_DERImp, @FuncaoAPFPreImp_RAImp, @FuncaoAPFPreImp_Observacao, @FuncaoAPFPreImp_FnApfCodigo)", GxErrorMask.GX_NOMASK,prmT003F10)
             ,new CursorDef("T003F11", "UPDATE [FuncaoAPFPreImp] SET [FuncaoAPFPreImp_Tipo]=@FuncaoAPFPreImp_Tipo, [FuncaoAPFPreImp_DERImp]=@FuncaoAPFPreImp_DERImp, [FuncaoAPFPreImp_RAImp]=@FuncaoAPFPreImp_RAImp, [FuncaoAPFPreImp_Observacao]=@FuncaoAPFPreImp_Observacao  WHERE [FuncaoAPFPreImp_FnApfCodigo] = @FuncaoAPFPreImp_FnApfCodigo AND [FuncaoAPFPreImp_Sequencial] = @FuncaoAPFPreImp_Sequencial", GxErrorMask.GX_NOMASK,prmT003F11)
             ,new CursorDef("T003F12", "DELETE FROM [FuncaoAPFPreImp]  WHERE [FuncaoAPFPreImp_FnApfCodigo] = @FuncaoAPFPreImp_FnApfCodigo AND [FuncaoAPFPreImp_Sequencial] = @FuncaoAPFPreImp_Sequencial", GxErrorMask.GX_NOMASK,prmT003F12)
             ,new CursorDef("T003F13", "SELECT [FuncaoAPFPreImp_FnApfCodigo] AS FuncaoAPFPreImp_FnApfCodigo, [FuncaoAPFPreImp_Sequencial] FROM [FuncaoAPFPreImp] WITH (NOLOCK) ORDER BY [FuncaoAPFPreImp_FnApfCodigo], [FuncaoAPFPreImp_Sequencial]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT003F13,100,0,true,false )
             ,new CursorDef("T003F14", "SELECT [FuncaoAPF_Codigo] AS FuncaoAPFPreImp_FnApfCodigo FROM [FuncoesAPF] WITH (NOLOCK) WHERE [FuncaoAPF_Codigo] = @FuncaoAPFPreImp_FnApfCodigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003F14,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 3) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((short[]) buf[3])[0] = rslt.getShort(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((short[]) buf[5])[0] = rslt.getShort(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getLongVarchar(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                return;
             case 1 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 3) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((short[]) buf[3])[0] = rslt.getShort(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((short[]) buf[5])[0] = rslt.getShort(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getLongVarchar(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 3 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 3) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((short[]) buf[3])[0] = rslt.getShort(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((short[]) buf[5])[0] = rslt.getShort(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getLongVarchar(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                return;
             case 8 :
                stmt.SetParameter(1, (short)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[2]);
                }
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 3 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(3, (short)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 4 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(4, (short)parms[6]);
                }
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 5 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(5, (String)parms[8]);
                }
                stmt.SetParameter(6, (int)parms[9]);
                return;
             case 9 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(2, (short)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(3, (short)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 4 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[7]);
                }
                stmt.SetParameter(5, (int)parms[8]);
                stmt.SetParameter(6, (short)parms[9]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
