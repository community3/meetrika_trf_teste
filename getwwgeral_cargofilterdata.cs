/*
               File: GetWWGeral_CargoFilterData
        Description: Get WWGeral_Cargo Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/21/2020 18:4:48.98
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getwwgeral_cargofilterdata : GXProcedure
   {
      public getwwgeral_cargofilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getwwgeral_cargofilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV19DDOName = aP0_DDOName;
         this.AV17SearchTxt = aP1_SearchTxt;
         this.AV18SearchTxtTo = aP2_SearchTxtTo;
         this.AV23OptionsJson = "" ;
         this.AV26OptionsDescJson = "" ;
         this.AV28OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV23OptionsJson;
         aP4_OptionsDescJson=this.AV26OptionsDescJson;
         aP5_OptionIndexesJson=this.AV28OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV19DDOName = aP0_DDOName;
         this.AV17SearchTxt = aP1_SearchTxt;
         this.AV18SearchTxtTo = aP2_SearchTxtTo;
         this.AV23OptionsJson = "" ;
         this.AV26OptionsDescJson = "" ;
         this.AV28OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV23OptionsJson;
         aP4_OptionsDescJson=this.AV26OptionsDescJson;
         aP5_OptionIndexesJson=this.AV28OptionIndexesJson;
         return AV28OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getwwgeral_cargofilterdata objgetwwgeral_cargofilterdata;
         objgetwwgeral_cargofilterdata = new getwwgeral_cargofilterdata();
         objgetwwgeral_cargofilterdata.AV19DDOName = aP0_DDOName;
         objgetwwgeral_cargofilterdata.AV17SearchTxt = aP1_SearchTxt;
         objgetwwgeral_cargofilterdata.AV18SearchTxtTo = aP2_SearchTxtTo;
         objgetwwgeral_cargofilterdata.AV23OptionsJson = "" ;
         objgetwwgeral_cargofilterdata.AV26OptionsDescJson = "" ;
         objgetwwgeral_cargofilterdata.AV28OptionIndexesJson = "" ;
         objgetwwgeral_cargofilterdata.context.SetSubmitInitialConfig(context);
         objgetwwgeral_cargofilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetwwgeral_cargofilterdata);
         aP3_OptionsJson=this.AV23OptionsJson;
         aP4_OptionsDescJson=this.AV26OptionsDescJson;
         aP5_OptionIndexesJson=this.AV28OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getwwgeral_cargofilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV22Options = (IGxCollection)(new GxSimpleCollection());
         AV25OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV27OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV19DDOName), "DDO_CARGO_NOME") == 0 )
         {
            /* Execute user subroutine: 'LOADCARGO_NOMEOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV19DDOName), "DDO_CARGO_UONOM") == 0 )
         {
            /* Execute user subroutine: 'LOADCARGO_UONOMOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV23OptionsJson = AV22Options.ToJSonString(false);
         AV26OptionsDescJson = AV25OptionsDesc.ToJSonString(false);
         AV28OptionIndexesJson = AV27OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV30Session.Get("WWGeral_CargoGridState"), "") == 0 )
         {
            AV32GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "WWGeral_CargoGridState"), "");
         }
         else
         {
            AV32GridState.FromXml(AV30Session.Get("WWGeral_CargoGridState"), "");
         }
         AV51GXV1 = 1;
         while ( AV51GXV1 <= AV32GridState.gxTpr_Filtervalues.Count )
         {
            AV33GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV32GridState.gxTpr_Filtervalues.Item(AV51GXV1));
            if ( StringUtil.StrCmp(AV33GridStateFilterValue.gxTpr_Name, "TFCARGO_NOME") == 0 )
            {
               AV10TFCargo_Nome = AV33GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV33GridStateFilterValue.gxTpr_Name, "TFCARGO_NOME_SEL") == 0 )
            {
               AV11TFCargo_Nome_Sel = AV33GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV33GridStateFilterValue.gxTpr_Name, "TFGRUPOCARGO_CODIGO") == 0 )
            {
               AV12TFGrupoCargo_Codigo = (int)(NumberUtil.Val( AV33GridStateFilterValue.gxTpr_Value, "."));
               AV13TFGrupoCargo_Codigo_To = (int)(NumberUtil.Val( AV33GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV33GridStateFilterValue.gxTpr_Name, "TFCARGO_UONOM") == 0 )
            {
               AV14TFCargo_UONom = AV33GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV33GridStateFilterValue.gxTpr_Name, "TFCARGO_UONOM_SEL") == 0 )
            {
               AV15TFCargo_UONom_Sel = AV33GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV33GridStateFilterValue.gxTpr_Name, "TFCARGO_ATIVO_SEL") == 0 )
            {
               AV16TFCargo_Ativo_Sel = (short)(NumberUtil.Val( AV33GridStateFilterValue.gxTpr_Value, "."));
            }
            AV51GXV1 = (int)(AV51GXV1+1);
         }
         if ( AV32GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV34GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV32GridState.gxTpr_Dynamicfilters.Item(1));
            AV35DynamicFiltersSelector1 = AV34GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV35DynamicFiltersSelector1, "CARGO_NOME") == 0 )
            {
               AV36Cargo_Nome1 = AV34GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV35DynamicFiltersSelector1, "GRUPOCARGO_CODIGO") == 0 )
            {
               AV37GrupoCargo_Codigo1 = (int)(NumberUtil.Val( AV34GridStateDynamicFilter.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV35DynamicFiltersSelector1, "CARGO_UOCOD") == 0 )
            {
               AV38Cargo_UOCod1 = (int)(NumberUtil.Val( AV34GridStateDynamicFilter.gxTpr_Value, "."));
            }
            if ( AV32GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV39DynamicFiltersEnabled2 = true;
               AV34GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV32GridState.gxTpr_Dynamicfilters.Item(2));
               AV40DynamicFiltersSelector2 = AV34GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV40DynamicFiltersSelector2, "CARGO_NOME") == 0 )
               {
                  AV41Cargo_Nome2 = AV34GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV40DynamicFiltersSelector2, "GRUPOCARGO_CODIGO") == 0 )
               {
                  AV42GrupoCargo_Codigo2 = (int)(NumberUtil.Val( AV34GridStateDynamicFilter.gxTpr_Value, "."));
               }
               else if ( StringUtil.StrCmp(AV40DynamicFiltersSelector2, "CARGO_UOCOD") == 0 )
               {
                  AV43Cargo_UOCod2 = (int)(NumberUtil.Val( AV34GridStateDynamicFilter.gxTpr_Value, "."));
               }
               if ( AV32GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV44DynamicFiltersEnabled3 = true;
                  AV34GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV32GridState.gxTpr_Dynamicfilters.Item(3));
                  AV45DynamicFiltersSelector3 = AV34GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV45DynamicFiltersSelector3, "CARGO_NOME") == 0 )
                  {
                     AV46Cargo_Nome3 = AV34GridStateDynamicFilter.gxTpr_Value;
                  }
                  else if ( StringUtil.StrCmp(AV45DynamicFiltersSelector3, "GRUPOCARGO_CODIGO") == 0 )
                  {
                     AV47GrupoCargo_Codigo3 = (int)(NumberUtil.Val( AV34GridStateDynamicFilter.gxTpr_Value, "."));
                  }
                  else if ( StringUtil.StrCmp(AV45DynamicFiltersSelector3, "CARGO_UOCOD") == 0 )
                  {
                     AV48Cargo_UOCod3 = (int)(NumberUtil.Val( AV34GridStateDynamicFilter.gxTpr_Value, "."));
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADCARGO_NOMEOPTIONS' Routine */
         AV10TFCargo_Nome = AV17SearchTxt;
         AV11TFCargo_Nome_Sel = "";
         AV53WWGeral_CargoDS_1_Dynamicfiltersselector1 = AV35DynamicFiltersSelector1;
         AV54WWGeral_CargoDS_2_Cargo_nome1 = AV36Cargo_Nome1;
         AV55WWGeral_CargoDS_3_Grupocargo_codigo1 = AV37GrupoCargo_Codigo1;
         AV56WWGeral_CargoDS_4_Cargo_uocod1 = AV38Cargo_UOCod1;
         AV57WWGeral_CargoDS_5_Dynamicfiltersenabled2 = AV39DynamicFiltersEnabled2;
         AV58WWGeral_CargoDS_6_Dynamicfiltersselector2 = AV40DynamicFiltersSelector2;
         AV59WWGeral_CargoDS_7_Cargo_nome2 = AV41Cargo_Nome2;
         AV60WWGeral_CargoDS_8_Grupocargo_codigo2 = AV42GrupoCargo_Codigo2;
         AV61WWGeral_CargoDS_9_Cargo_uocod2 = AV43Cargo_UOCod2;
         AV62WWGeral_CargoDS_10_Dynamicfiltersenabled3 = AV44DynamicFiltersEnabled3;
         AV63WWGeral_CargoDS_11_Dynamicfiltersselector3 = AV45DynamicFiltersSelector3;
         AV64WWGeral_CargoDS_12_Cargo_nome3 = AV46Cargo_Nome3;
         AV65WWGeral_CargoDS_13_Grupocargo_codigo3 = AV47GrupoCargo_Codigo3;
         AV66WWGeral_CargoDS_14_Cargo_uocod3 = AV48Cargo_UOCod3;
         AV67WWGeral_CargoDS_15_Tfcargo_nome = AV10TFCargo_Nome;
         AV68WWGeral_CargoDS_16_Tfcargo_nome_sel = AV11TFCargo_Nome_Sel;
         AV69WWGeral_CargoDS_17_Tfgrupocargo_codigo = AV12TFGrupoCargo_Codigo;
         AV70WWGeral_CargoDS_18_Tfgrupocargo_codigo_to = AV13TFGrupoCargo_Codigo_To;
         AV71WWGeral_CargoDS_19_Tfcargo_uonom = AV14TFCargo_UONom;
         AV72WWGeral_CargoDS_20_Tfcargo_uonom_sel = AV15TFCargo_UONom_Sel;
         AV73WWGeral_CargoDS_21_Tfcargo_ativo_sel = AV16TFCargo_Ativo_Sel;
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV53WWGeral_CargoDS_1_Dynamicfiltersselector1 ,
                                              AV54WWGeral_CargoDS_2_Cargo_nome1 ,
                                              AV55WWGeral_CargoDS_3_Grupocargo_codigo1 ,
                                              AV56WWGeral_CargoDS_4_Cargo_uocod1 ,
                                              AV57WWGeral_CargoDS_5_Dynamicfiltersenabled2 ,
                                              AV58WWGeral_CargoDS_6_Dynamicfiltersselector2 ,
                                              AV59WWGeral_CargoDS_7_Cargo_nome2 ,
                                              AV60WWGeral_CargoDS_8_Grupocargo_codigo2 ,
                                              AV61WWGeral_CargoDS_9_Cargo_uocod2 ,
                                              AV62WWGeral_CargoDS_10_Dynamicfiltersenabled3 ,
                                              AV63WWGeral_CargoDS_11_Dynamicfiltersselector3 ,
                                              AV64WWGeral_CargoDS_12_Cargo_nome3 ,
                                              AV65WWGeral_CargoDS_13_Grupocargo_codigo3 ,
                                              AV66WWGeral_CargoDS_14_Cargo_uocod3 ,
                                              AV68WWGeral_CargoDS_16_Tfcargo_nome_sel ,
                                              AV67WWGeral_CargoDS_15_Tfcargo_nome ,
                                              AV69WWGeral_CargoDS_17_Tfgrupocargo_codigo ,
                                              AV70WWGeral_CargoDS_18_Tfgrupocargo_codigo_to ,
                                              AV72WWGeral_CargoDS_20_Tfcargo_uonom_sel ,
                                              AV71WWGeral_CargoDS_19_Tfcargo_uonom ,
                                              AV73WWGeral_CargoDS_21_Tfcargo_ativo_sel ,
                                              A618Cargo_Nome ,
                                              A615GrupoCargo_Codigo ,
                                              A1002Cargo_UOCod ,
                                              A1003Cargo_UONom ,
                                              A628Cargo_Ativo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN
                                              }
         });
         lV54WWGeral_CargoDS_2_Cargo_nome1 = StringUtil.Concat( StringUtil.RTrim( AV54WWGeral_CargoDS_2_Cargo_nome1), "%", "");
         lV59WWGeral_CargoDS_7_Cargo_nome2 = StringUtil.Concat( StringUtil.RTrim( AV59WWGeral_CargoDS_7_Cargo_nome2), "%", "");
         lV64WWGeral_CargoDS_12_Cargo_nome3 = StringUtil.Concat( StringUtil.RTrim( AV64WWGeral_CargoDS_12_Cargo_nome3), "%", "");
         lV67WWGeral_CargoDS_15_Tfcargo_nome = StringUtil.Concat( StringUtil.RTrim( AV67WWGeral_CargoDS_15_Tfcargo_nome), "%", "");
         lV71WWGeral_CargoDS_19_Tfcargo_uonom = StringUtil.PadR( StringUtil.RTrim( AV71WWGeral_CargoDS_19_Tfcargo_uonom), 50, "%");
         /* Using cursor P00NH2 */
         pr_default.execute(0, new Object[] {lV54WWGeral_CargoDS_2_Cargo_nome1, AV55WWGeral_CargoDS_3_Grupocargo_codigo1, AV56WWGeral_CargoDS_4_Cargo_uocod1, lV59WWGeral_CargoDS_7_Cargo_nome2, AV60WWGeral_CargoDS_8_Grupocargo_codigo2, AV61WWGeral_CargoDS_9_Cargo_uocod2, lV64WWGeral_CargoDS_12_Cargo_nome3, AV65WWGeral_CargoDS_13_Grupocargo_codigo3, AV66WWGeral_CargoDS_14_Cargo_uocod3, lV67WWGeral_CargoDS_15_Tfcargo_nome, AV68WWGeral_CargoDS_16_Tfcargo_nome_sel, AV69WWGeral_CargoDS_17_Tfgrupocargo_codigo, AV70WWGeral_CargoDS_18_Tfgrupocargo_codigo_to, lV71WWGeral_CargoDS_19_Tfcargo_uonom, AV72WWGeral_CargoDS_20_Tfcargo_uonom_sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKNH2 = false;
            A618Cargo_Nome = P00NH2_A618Cargo_Nome[0];
            A628Cargo_Ativo = P00NH2_A628Cargo_Ativo[0];
            A1003Cargo_UONom = P00NH2_A1003Cargo_UONom[0];
            n1003Cargo_UONom = P00NH2_n1003Cargo_UONom[0];
            A1002Cargo_UOCod = P00NH2_A1002Cargo_UOCod[0];
            n1002Cargo_UOCod = P00NH2_n1002Cargo_UOCod[0];
            A615GrupoCargo_Codigo = P00NH2_A615GrupoCargo_Codigo[0];
            A617Cargo_Codigo = P00NH2_A617Cargo_Codigo[0];
            A1003Cargo_UONom = P00NH2_A1003Cargo_UONom[0];
            n1003Cargo_UONom = P00NH2_n1003Cargo_UONom[0];
            AV29count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00NH2_A618Cargo_Nome[0], A618Cargo_Nome) == 0 ) )
            {
               BRKNH2 = false;
               A617Cargo_Codigo = P00NH2_A617Cargo_Codigo[0];
               AV29count = (long)(AV29count+1);
               BRKNH2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A618Cargo_Nome)) )
            {
               AV21Option = A618Cargo_Nome;
               AV24OptionDesc = StringUtil.Trim( StringUtil.RTrim( context.localUtil.Format( A618Cargo_Nome, "@!")));
               AV22Options.Add(AV21Option, 0);
               AV25OptionsDesc.Add(AV24OptionDesc, 0);
               AV27OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV29count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV22Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKNH2 )
            {
               BRKNH2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADCARGO_UONOMOPTIONS' Routine */
         AV14TFCargo_UONom = AV17SearchTxt;
         AV15TFCargo_UONom_Sel = "";
         AV53WWGeral_CargoDS_1_Dynamicfiltersselector1 = AV35DynamicFiltersSelector1;
         AV54WWGeral_CargoDS_2_Cargo_nome1 = AV36Cargo_Nome1;
         AV55WWGeral_CargoDS_3_Grupocargo_codigo1 = AV37GrupoCargo_Codigo1;
         AV56WWGeral_CargoDS_4_Cargo_uocod1 = AV38Cargo_UOCod1;
         AV57WWGeral_CargoDS_5_Dynamicfiltersenabled2 = AV39DynamicFiltersEnabled2;
         AV58WWGeral_CargoDS_6_Dynamicfiltersselector2 = AV40DynamicFiltersSelector2;
         AV59WWGeral_CargoDS_7_Cargo_nome2 = AV41Cargo_Nome2;
         AV60WWGeral_CargoDS_8_Grupocargo_codigo2 = AV42GrupoCargo_Codigo2;
         AV61WWGeral_CargoDS_9_Cargo_uocod2 = AV43Cargo_UOCod2;
         AV62WWGeral_CargoDS_10_Dynamicfiltersenabled3 = AV44DynamicFiltersEnabled3;
         AV63WWGeral_CargoDS_11_Dynamicfiltersselector3 = AV45DynamicFiltersSelector3;
         AV64WWGeral_CargoDS_12_Cargo_nome3 = AV46Cargo_Nome3;
         AV65WWGeral_CargoDS_13_Grupocargo_codigo3 = AV47GrupoCargo_Codigo3;
         AV66WWGeral_CargoDS_14_Cargo_uocod3 = AV48Cargo_UOCod3;
         AV67WWGeral_CargoDS_15_Tfcargo_nome = AV10TFCargo_Nome;
         AV68WWGeral_CargoDS_16_Tfcargo_nome_sel = AV11TFCargo_Nome_Sel;
         AV69WWGeral_CargoDS_17_Tfgrupocargo_codigo = AV12TFGrupoCargo_Codigo;
         AV70WWGeral_CargoDS_18_Tfgrupocargo_codigo_to = AV13TFGrupoCargo_Codigo_To;
         AV71WWGeral_CargoDS_19_Tfcargo_uonom = AV14TFCargo_UONom;
         AV72WWGeral_CargoDS_20_Tfcargo_uonom_sel = AV15TFCargo_UONom_Sel;
         AV73WWGeral_CargoDS_21_Tfcargo_ativo_sel = AV16TFCargo_Ativo_Sel;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV53WWGeral_CargoDS_1_Dynamicfiltersselector1 ,
                                              AV54WWGeral_CargoDS_2_Cargo_nome1 ,
                                              AV55WWGeral_CargoDS_3_Grupocargo_codigo1 ,
                                              AV56WWGeral_CargoDS_4_Cargo_uocod1 ,
                                              AV57WWGeral_CargoDS_5_Dynamicfiltersenabled2 ,
                                              AV58WWGeral_CargoDS_6_Dynamicfiltersselector2 ,
                                              AV59WWGeral_CargoDS_7_Cargo_nome2 ,
                                              AV60WWGeral_CargoDS_8_Grupocargo_codigo2 ,
                                              AV61WWGeral_CargoDS_9_Cargo_uocod2 ,
                                              AV62WWGeral_CargoDS_10_Dynamicfiltersenabled3 ,
                                              AV63WWGeral_CargoDS_11_Dynamicfiltersselector3 ,
                                              AV64WWGeral_CargoDS_12_Cargo_nome3 ,
                                              AV65WWGeral_CargoDS_13_Grupocargo_codigo3 ,
                                              AV66WWGeral_CargoDS_14_Cargo_uocod3 ,
                                              AV68WWGeral_CargoDS_16_Tfcargo_nome_sel ,
                                              AV67WWGeral_CargoDS_15_Tfcargo_nome ,
                                              AV69WWGeral_CargoDS_17_Tfgrupocargo_codigo ,
                                              AV70WWGeral_CargoDS_18_Tfgrupocargo_codigo_to ,
                                              AV72WWGeral_CargoDS_20_Tfcargo_uonom_sel ,
                                              AV71WWGeral_CargoDS_19_Tfcargo_uonom ,
                                              AV73WWGeral_CargoDS_21_Tfcargo_ativo_sel ,
                                              A618Cargo_Nome ,
                                              A615GrupoCargo_Codigo ,
                                              A1002Cargo_UOCod ,
                                              A1003Cargo_UONom ,
                                              A628Cargo_Ativo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN
                                              }
         });
         lV54WWGeral_CargoDS_2_Cargo_nome1 = StringUtil.Concat( StringUtil.RTrim( AV54WWGeral_CargoDS_2_Cargo_nome1), "%", "");
         lV59WWGeral_CargoDS_7_Cargo_nome2 = StringUtil.Concat( StringUtil.RTrim( AV59WWGeral_CargoDS_7_Cargo_nome2), "%", "");
         lV64WWGeral_CargoDS_12_Cargo_nome3 = StringUtil.Concat( StringUtil.RTrim( AV64WWGeral_CargoDS_12_Cargo_nome3), "%", "");
         lV67WWGeral_CargoDS_15_Tfcargo_nome = StringUtil.Concat( StringUtil.RTrim( AV67WWGeral_CargoDS_15_Tfcargo_nome), "%", "");
         lV71WWGeral_CargoDS_19_Tfcargo_uonom = StringUtil.PadR( StringUtil.RTrim( AV71WWGeral_CargoDS_19_Tfcargo_uonom), 50, "%");
         /* Using cursor P00NH3 */
         pr_default.execute(1, new Object[] {lV54WWGeral_CargoDS_2_Cargo_nome1, AV55WWGeral_CargoDS_3_Grupocargo_codigo1, AV56WWGeral_CargoDS_4_Cargo_uocod1, lV59WWGeral_CargoDS_7_Cargo_nome2, AV60WWGeral_CargoDS_8_Grupocargo_codigo2, AV61WWGeral_CargoDS_9_Cargo_uocod2, lV64WWGeral_CargoDS_12_Cargo_nome3, AV65WWGeral_CargoDS_13_Grupocargo_codigo3, AV66WWGeral_CargoDS_14_Cargo_uocod3, lV67WWGeral_CargoDS_15_Tfcargo_nome, AV68WWGeral_CargoDS_16_Tfcargo_nome_sel, AV69WWGeral_CargoDS_17_Tfgrupocargo_codigo, AV70WWGeral_CargoDS_18_Tfgrupocargo_codigo_to, lV71WWGeral_CargoDS_19_Tfcargo_uonom, AV72WWGeral_CargoDS_20_Tfcargo_uonom_sel});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKNH4 = false;
            A1002Cargo_UOCod = P00NH3_A1002Cargo_UOCod[0];
            n1002Cargo_UOCod = P00NH3_n1002Cargo_UOCod[0];
            A628Cargo_Ativo = P00NH3_A628Cargo_Ativo[0];
            A1003Cargo_UONom = P00NH3_A1003Cargo_UONom[0];
            n1003Cargo_UONom = P00NH3_n1003Cargo_UONom[0];
            A615GrupoCargo_Codigo = P00NH3_A615GrupoCargo_Codigo[0];
            A618Cargo_Nome = P00NH3_A618Cargo_Nome[0];
            A617Cargo_Codigo = P00NH3_A617Cargo_Codigo[0];
            A1003Cargo_UONom = P00NH3_A1003Cargo_UONom[0];
            n1003Cargo_UONom = P00NH3_n1003Cargo_UONom[0];
            AV29count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( P00NH3_A1002Cargo_UOCod[0] == A1002Cargo_UOCod ) )
            {
               BRKNH4 = false;
               A617Cargo_Codigo = P00NH3_A617Cargo_Codigo[0];
               AV29count = (long)(AV29count+1);
               BRKNH4 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A1003Cargo_UONom)) )
            {
               AV21Option = A1003Cargo_UONom;
               AV20InsertIndex = 1;
               while ( ( AV20InsertIndex <= AV22Options.Count ) && ( StringUtil.StrCmp(((String)AV22Options.Item(AV20InsertIndex)), AV21Option) < 0 ) )
               {
                  AV20InsertIndex = (int)(AV20InsertIndex+1);
               }
               AV22Options.Add(AV21Option, AV20InsertIndex);
               AV27OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV29count), "Z,ZZZ,ZZZ,ZZ9")), AV20InsertIndex);
            }
            if ( AV22Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKNH4 )
            {
               BRKNH4 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV22Options = new GxSimpleCollection();
         AV25OptionsDesc = new GxSimpleCollection();
         AV27OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV30Session = context.GetSession();
         AV32GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV33GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFCargo_Nome = "";
         AV11TFCargo_Nome_Sel = "";
         AV14TFCargo_UONom = "";
         AV15TFCargo_UONom_Sel = "";
         AV34GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV35DynamicFiltersSelector1 = "";
         AV36Cargo_Nome1 = "";
         AV40DynamicFiltersSelector2 = "";
         AV41Cargo_Nome2 = "";
         AV45DynamicFiltersSelector3 = "";
         AV46Cargo_Nome3 = "";
         AV53WWGeral_CargoDS_1_Dynamicfiltersselector1 = "";
         AV54WWGeral_CargoDS_2_Cargo_nome1 = "";
         AV58WWGeral_CargoDS_6_Dynamicfiltersselector2 = "";
         AV59WWGeral_CargoDS_7_Cargo_nome2 = "";
         AV63WWGeral_CargoDS_11_Dynamicfiltersselector3 = "";
         AV64WWGeral_CargoDS_12_Cargo_nome3 = "";
         AV67WWGeral_CargoDS_15_Tfcargo_nome = "";
         AV68WWGeral_CargoDS_16_Tfcargo_nome_sel = "";
         AV71WWGeral_CargoDS_19_Tfcargo_uonom = "";
         AV72WWGeral_CargoDS_20_Tfcargo_uonom_sel = "";
         scmdbuf = "";
         lV54WWGeral_CargoDS_2_Cargo_nome1 = "";
         lV59WWGeral_CargoDS_7_Cargo_nome2 = "";
         lV64WWGeral_CargoDS_12_Cargo_nome3 = "";
         lV67WWGeral_CargoDS_15_Tfcargo_nome = "";
         lV71WWGeral_CargoDS_19_Tfcargo_uonom = "";
         A618Cargo_Nome = "";
         A1003Cargo_UONom = "";
         P00NH2_A618Cargo_Nome = new String[] {""} ;
         P00NH2_A628Cargo_Ativo = new bool[] {false} ;
         P00NH2_A1003Cargo_UONom = new String[] {""} ;
         P00NH2_n1003Cargo_UONom = new bool[] {false} ;
         P00NH2_A1002Cargo_UOCod = new int[1] ;
         P00NH2_n1002Cargo_UOCod = new bool[] {false} ;
         P00NH2_A615GrupoCargo_Codigo = new int[1] ;
         P00NH2_A617Cargo_Codigo = new int[1] ;
         AV21Option = "";
         AV24OptionDesc = "";
         P00NH3_A1002Cargo_UOCod = new int[1] ;
         P00NH3_n1002Cargo_UOCod = new bool[] {false} ;
         P00NH3_A628Cargo_Ativo = new bool[] {false} ;
         P00NH3_A1003Cargo_UONom = new String[] {""} ;
         P00NH3_n1003Cargo_UONom = new bool[] {false} ;
         P00NH3_A615GrupoCargo_Codigo = new int[1] ;
         P00NH3_A618Cargo_Nome = new String[] {""} ;
         P00NH3_A617Cargo_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getwwgeral_cargofilterdata__default(),
            new Object[][] {
                new Object[] {
               P00NH2_A618Cargo_Nome, P00NH2_A628Cargo_Ativo, P00NH2_A1003Cargo_UONom, P00NH2_n1003Cargo_UONom, P00NH2_A1002Cargo_UOCod, P00NH2_n1002Cargo_UOCod, P00NH2_A615GrupoCargo_Codigo, P00NH2_A617Cargo_Codigo
               }
               , new Object[] {
               P00NH3_A1002Cargo_UOCod, P00NH3_n1002Cargo_UOCod, P00NH3_A628Cargo_Ativo, P00NH3_A1003Cargo_UONom, P00NH3_n1003Cargo_UONom, P00NH3_A615GrupoCargo_Codigo, P00NH3_A618Cargo_Nome, P00NH3_A617Cargo_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV16TFCargo_Ativo_Sel ;
      private short AV73WWGeral_CargoDS_21_Tfcargo_ativo_sel ;
      private int AV51GXV1 ;
      private int AV12TFGrupoCargo_Codigo ;
      private int AV13TFGrupoCargo_Codigo_To ;
      private int AV37GrupoCargo_Codigo1 ;
      private int AV38Cargo_UOCod1 ;
      private int AV42GrupoCargo_Codigo2 ;
      private int AV43Cargo_UOCod2 ;
      private int AV47GrupoCargo_Codigo3 ;
      private int AV48Cargo_UOCod3 ;
      private int AV55WWGeral_CargoDS_3_Grupocargo_codigo1 ;
      private int AV56WWGeral_CargoDS_4_Cargo_uocod1 ;
      private int AV60WWGeral_CargoDS_8_Grupocargo_codigo2 ;
      private int AV61WWGeral_CargoDS_9_Cargo_uocod2 ;
      private int AV65WWGeral_CargoDS_13_Grupocargo_codigo3 ;
      private int AV66WWGeral_CargoDS_14_Cargo_uocod3 ;
      private int AV69WWGeral_CargoDS_17_Tfgrupocargo_codigo ;
      private int AV70WWGeral_CargoDS_18_Tfgrupocargo_codigo_to ;
      private int A615GrupoCargo_Codigo ;
      private int A1002Cargo_UOCod ;
      private int A617Cargo_Codigo ;
      private int AV20InsertIndex ;
      private long AV29count ;
      private String AV14TFCargo_UONom ;
      private String AV15TFCargo_UONom_Sel ;
      private String AV71WWGeral_CargoDS_19_Tfcargo_uonom ;
      private String AV72WWGeral_CargoDS_20_Tfcargo_uonom_sel ;
      private String scmdbuf ;
      private String lV71WWGeral_CargoDS_19_Tfcargo_uonom ;
      private String A1003Cargo_UONom ;
      private bool returnInSub ;
      private bool AV39DynamicFiltersEnabled2 ;
      private bool AV44DynamicFiltersEnabled3 ;
      private bool AV57WWGeral_CargoDS_5_Dynamicfiltersenabled2 ;
      private bool AV62WWGeral_CargoDS_10_Dynamicfiltersenabled3 ;
      private bool A628Cargo_Ativo ;
      private bool BRKNH2 ;
      private bool n1003Cargo_UONom ;
      private bool n1002Cargo_UOCod ;
      private bool BRKNH4 ;
      private String AV28OptionIndexesJson ;
      private String AV23OptionsJson ;
      private String AV26OptionsDescJson ;
      private String AV19DDOName ;
      private String AV17SearchTxt ;
      private String AV18SearchTxtTo ;
      private String AV10TFCargo_Nome ;
      private String AV11TFCargo_Nome_Sel ;
      private String AV35DynamicFiltersSelector1 ;
      private String AV36Cargo_Nome1 ;
      private String AV40DynamicFiltersSelector2 ;
      private String AV41Cargo_Nome2 ;
      private String AV45DynamicFiltersSelector3 ;
      private String AV46Cargo_Nome3 ;
      private String AV53WWGeral_CargoDS_1_Dynamicfiltersselector1 ;
      private String AV54WWGeral_CargoDS_2_Cargo_nome1 ;
      private String AV58WWGeral_CargoDS_6_Dynamicfiltersselector2 ;
      private String AV59WWGeral_CargoDS_7_Cargo_nome2 ;
      private String AV63WWGeral_CargoDS_11_Dynamicfiltersselector3 ;
      private String AV64WWGeral_CargoDS_12_Cargo_nome3 ;
      private String AV67WWGeral_CargoDS_15_Tfcargo_nome ;
      private String AV68WWGeral_CargoDS_16_Tfcargo_nome_sel ;
      private String lV54WWGeral_CargoDS_2_Cargo_nome1 ;
      private String lV59WWGeral_CargoDS_7_Cargo_nome2 ;
      private String lV64WWGeral_CargoDS_12_Cargo_nome3 ;
      private String lV67WWGeral_CargoDS_15_Tfcargo_nome ;
      private String A618Cargo_Nome ;
      private String AV21Option ;
      private String AV24OptionDesc ;
      private IGxSession AV30Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] P00NH2_A618Cargo_Nome ;
      private bool[] P00NH2_A628Cargo_Ativo ;
      private String[] P00NH2_A1003Cargo_UONom ;
      private bool[] P00NH2_n1003Cargo_UONom ;
      private int[] P00NH2_A1002Cargo_UOCod ;
      private bool[] P00NH2_n1002Cargo_UOCod ;
      private int[] P00NH2_A615GrupoCargo_Codigo ;
      private int[] P00NH2_A617Cargo_Codigo ;
      private int[] P00NH3_A1002Cargo_UOCod ;
      private bool[] P00NH3_n1002Cargo_UOCod ;
      private bool[] P00NH3_A628Cargo_Ativo ;
      private String[] P00NH3_A1003Cargo_UONom ;
      private bool[] P00NH3_n1003Cargo_UONom ;
      private int[] P00NH3_A615GrupoCargo_Codigo ;
      private String[] P00NH3_A618Cargo_Nome ;
      private int[] P00NH3_A617Cargo_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV22Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV25OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV27OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV32GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV33GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV34GridStateDynamicFilter ;
   }

   public class getwwgeral_cargofilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00NH2( IGxContext context ,
                                             String AV53WWGeral_CargoDS_1_Dynamicfiltersselector1 ,
                                             String AV54WWGeral_CargoDS_2_Cargo_nome1 ,
                                             int AV55WWGeral_CargoDS_3_Grupocargo_codigo1 ,
                                             int AV56WWGeral_CargoDS_4_Cargo_uocod1 ,
                                             bool AV57WWGeral_CargoDS_5_Dynamicfiltersenabled2 ,
                                             String AV58WWGeral_CargoDS_6_Dynamicfiltersselector2 ,
                                             String AV59WWGeral_CargoDS_7_Cargo_nome2 ,
                                             int AV60WWGeral_CargoDS_8_Grupocargo_codigo2 ,
                                             int AV61WWGeral_CargoDS_9_Cargo_uocod2 ,
                                             bool AV62WWGeral_CargoDS_10_Dynamicfiltersenabled3 ,
                                             String AV63WWGeral_CargoDS_11_Dynamicfiltersselector3 ,
                                             String AV64WWGeral_CargoDS_12_Cargo_nome3 ,
                                             int AV65WWGeral_CargoDS_13_Grupocargo_codigo3 ,
                                             int AV66WWGeral_CargoDS_14_Cargo_uocod3 ,
                                             String AV68WWGeral_CargoDS_16_Tfcargo_nome_sel ,
                                             String AV67WWGeral_CargoDS_15_Tfcargo_nome ,
                                             int AV69WWGeral_CargoDS_17_Tfgrupocargo_codigo ,
                                             int AV70WWGeral_CargoDS_18_Tfgrupocargo_codigo_to ,
                                             String AV72WWGeral_CargoDS_20_Tfcargo_uonom_sel ,
                                             String AV71WWGeral_CargoDS_19_Tfcargo_uonom ,
                                             short AV73WWGeral_CargoDS_21_Tfcargo_ativo_sel ,
                                             String A618Cargo_Nome ,
                                             int A615GrupoCargo_Codigo ,
                                             int A1002Cargo_UOCod ,
                                             String A1003Cargo_UONom ,
                                             bool A628Cargo_Ativo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [15] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[Cargo_Nome], T1.[Cargo_Ativo], T2.[UnidadeOrganizacional_Nome] AS Cargo_UONom, T1.[Cargo_UOCod] AS Cargo_UOCod, T1.[GrupoCargo_Codigo], T1.[Cargo_Codigo] FROM ([Geral_Cargo] T1 WITH (NOLOCK) LEFT JOIN [Geral_UnidadeOrganizacional] T2 WITH (NOLOCK) ON T2.[UnidadeOrganizacional_Codigo] = T1.[Cargo_UOCod])";
         if ( ( StringUtil.StrCmp(AV53WWGeral_CargoDS_1_Dynamicfiltersselector1, "CARGO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54WWGeral_CargoDS_2_Cargo_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Cargo_Nome] like '%' + @lV54WWGeral_CargoDS_2_Cargo_nome1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Cargo_Nome] like '%' + @lV54WWGeral_CargoDS_2_Cargo_nome1 + '%')";
            }
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV53WWGeral_CargoDS_1_Dynamicfiltersselector1, "GRUPOCARGO_CODIGO") == 0 ) && ( ! (0==AV55WWGeral_CargoDS_3_Grupocargo_codigo1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[GrupoCargo_Codigo] = @AV55WWGeral_CargoDS_3_Grupocargo_codigo1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[GrupoCargo_Codigo] = @AV55WWGeral_CargoDS_3_Grupocargo_codigo1)";
            }
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV53WWGeral_CargoDS_1_Dynamicfiltersselector1, "CARGO_UOCOD") == 0 ) && ( ! (0==AV56WWGeral_CargoDS_4_Cargo_uocod1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Cargo_UOCod] = @AV56WWGeral_CargoDS_4_Cargo_uocod1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Cargo_UOCod] = @AV56WWGeral_CargoDS_4_Cargo_uocod1)";
            }
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( AV57WWGeral_CargoDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV58WWGeral_CargoDS_6_Dynamicfiltersselector2, "CARGO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59WWGeral_CargoDS_7_Cargo_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Cargo_Nome] like '%' + @lV59WWGeral_CargoDS_7_Cargo_nome2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Cargo_Nome] like '%' + @lV59WWGeral_CargoDS_7_Cargo_nome2 + '%')";
            }
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( AV57WWGeral_CargoDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV58WWGeral_CargoDS_6_Dynamicfiltersselector2, "GRUPOCARGO_CODIGO") == 0 ) && ( ! (0==AV60WWGeral_CargoDS_8_Grupocargo_codigo2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[GrupoCargo_Codigo] = @AV60WWGeral_CargoDS_8_Grupocargo_codigo2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[GrupoCargo_Codigo] = @AV60WWGeral_CargoDS_8_Grupocargo_codigo2)";
            }
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( AV57WWGeral_CargoDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV58WWGeral_CargoDS_6_Dynamicfiltersselector2, "CARGO_UOCOD") == 0 ) && ( ! (0==AV61WWGeral_CargoDS_9_Cargo_uocod2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Cargo_UOCod] = @AV61WWGeral_CargoDS_9_Cargo_uocod2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Cargo_UOCod] = @AV61WWGeral_CargoDS_9_Cargo_uocod2)";
            }
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( AV62WWGeral_CargoDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV63WWGeral_CargoDS_11_Dynamicfiltersselector3, "CARGO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64WWGeral_CargoDS_12_Cargo_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Cargo_Nome] like '%' + @lV64WWGeral_CargoDS_12_Cargo_nome3 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Cargo_Nome] like '%' + @lV64WWGeral_CargoDS_12_Cargo_nome3 + '%')";
            }
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( AV62WWGeral_CargoDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV63WWGeral_CargoDS_11_Dynamicfiltersselector3, "GRUPOCARGO_CODIGO") == 0 ) && ( ! (0==AV65WWGeral_CargoDS_13_Grupocargo_codigo3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[GrupoCargo_Codigo] = @AV65WWGeral_CargoDS_13_Grupocargo_codigo3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[GrupoCargo_Codigo] = @AV65WWGeral_CargoDS_13_Grupocargo_codigo3)";
            }
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( AV62WWGeral_CargoDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV63WWGeral_CargoDS_11_Dynamicfiltersselector3, "CARGO_UOCOD") == 0 ) && ( ! (0==AV66WWGeral_CargoDS_14_Cargo_uocod3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Cargo_UOCod] = @AV66WWGeral_CargoDS_14_Cargo_uocod3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Cargo_UOCod] = @AV66WWGeral_CargoDS_14_Cargo_uocod3)";
            }
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV68WWGeral_CargoDS_16_Tfcargo_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67WWGeral_CargoDS_15_Tfcargo_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Cargo_Nome] like @lV67WWGeral_CargoDS_15_Tfcargo_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Cargo_Nome] like @lV67WWGeral_CargoDS_15_Tfcargo_nome)";
            }
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV68WWGeral_CargoDS_16_Tfcargo_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Cargo_Nome] = @AV68WWGeral_CargoDS_16_Tfcargo_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Cargo_Nome] = @AV68WWGeral_CargoDS_16_Tfcargo_nome_sel)";
            }
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( ! (0==AV69WWGeral_CargoDS_17_Tfgrupocargo_codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[GrupoCargo_Codigo] >= @AV69WWGeral_CargoDS_17_Tfgrupocargo_codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[GrupoCargo_Codigo] >= @AV69WWGeral_CargoDS_17_Tfgrupocargo_codigo)";
            }
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( ! (0==AV70WWGeral_CargoDS_18_Tfgrupocargo_codigo_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[GrupoCargo_Codigo] <= @AV70WWGeral_CargoDS_18_Tfgrupocargo_codigo_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[GrupoCargo_Codigo] <= @AV70WWGeral_CargoDS_18_Tfgrupocargo_codigo_to)";
            }
         }
         else
         {
            GXv_int1[12] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV72WWGeral_CargoDS_20_Tfcargo_uonom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV71WWGeral_CargoDS_19_Tfcargo_uonom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[UnidadeOrganizacional_Nome] like @lV71WWGeral_CargoDS_19_Tfcargo_uonom)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[UnidadeOrganizacional_Nome] like @lV71WWGeral_CargoDS_19_Tfcargo_uonom)";
            }
         }
         else
         {
            GXv_int1[13] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV72WWGeral_CargoDS_20_Tfcargo_uonom_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[UnidadeOrganizacional_Nome] = @AV72WWGeral_CargoDS_20_Tfcargo_uonom_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[UnidadeOrganizacional_Nome] = @AV72WWGeral_CargoDS_20_Tfcargo_uonom_sel)";
            }
         }
         else
         {
            GXv_int1[14] = 1;
         }
         if ( AV73WWGeral_CargoDS_21_Tfcargo_ativo_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Cargo_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Cargo_Ativo] = 1)";
            }
         }
         if ( AV73WWGeral_CargoDS_21_Tfcargo_ativo_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Cargo_Ativo] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Cargo_Ativo] = 0)";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[Cargo_Nome]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00NH3( IGxContext context ,
                                             String AV53WWGeral_CargoDS_1_Dynamicfiltersselector1 ,
                                             String AV54WWGeral_CargoDS_2_Cargo_nome1 ,
                                             int AV55WWGeral_CargoDS_3_Grupocargo_codigo1 ,
                                             int AV56WWGeral_CargoDS_4_Cargo_uocod1 ,
                                             bool AV57WWGeral_CargoDS_5_Dynamicfiltersenabled2 ,
                                             String AV58WWGeral_CargoDS_6_Dynamicfiltersselector2 ,
                                             String AV59WWGeral_CargoDS_7_Cargo_nome2 ,
                                             int AV60WWGeral_CargoDS_8_Grupocargo_codigo2 ,
                                             int AV61WWGeral_CargoDS_9_Cargo_uocod2 ,
                                             bool AV62WWGeral_CargoDS_10_Dynamicfiltersenabled3 ,
                                             String AV63WWGeral_CargoDS_11_Dynamicfiltersselector3 ,
                                             String AV64WWGeral_CargoDS_12_Cargo_nome3 ,
                                             int AV65WWGeral_CargoDS_13_Grupocargo_codigo3 ,
                                             int AV66WWGeral_CargoDS_14_Cargo_uocod3 ,
                                             String AV68WWGeral_CargoDS_16_Tfcargo_nome_sel ,
                                             String AV67WWGeral_CargoDS_15_Tfcargo_nome ,
                                             int AV69WWGeral_CargoDS_17_Tfgrupocargo_codigo ,
                                             int AV70WWGeral_CargoDS_18_Tfgrupocargo_codigo_to ,
                                             String AV72WWGeral_CargoDS_20_Tfcargo_uonom_sel ,
                                             String AV71WWGeral_CargoDS_19_Tfcargo_uonom ,
                                             short AV73WWGeral_CargoDS_21_Tfcargo_ativo_sel ,
                                             String A618Cargo_Nome ,
                                             int A615GrupoCargo_Codigo ,
                                             int A1002Cargo_UOCod ,
                                             String A1003Cargo_UONom ,
                                             bool A628Cargo_Ativo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [15] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT T1.[Cargo_UOCod] AS Cargo_UOCod, T1.[Cargo_Ativo], T2.[UnidadeOrganizacional_Nome] AS Cargo_UONom, T1.[GrupoCargo_Codigo], T1.[Cargo_Nome], T1.[Cargo_Codigo] FROM ([Geral_Cargo] T1 WITH (NOLOCK) LEFT JOIN [Geral_UnidadeOrganizacional] T2 WITH (NOLOCK) ON T2.[UnidadeOrganizacional_Codigo] = T1.[Cargo_UOCod])";
         if ( ( StringUtil.StrCmp(AV53WWGeral_CargoDS_1_Dynamicfiltersselector1, "CARGO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54WWGeral_CargoDS_2_Cargo_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Cargo_Nome] like '%' + @lV54WWGeral_CargoDS_2_Cargo_nome1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Cargo_Nome] like '%' + @lV54WWGeral_CargoDS_2_Cargo_nome1 + '%')";
            }
         }
         else
         {
            GXv_int3[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV53WWGeral_CargoDS_1_Dynamicfiltersselector1, "GRUPOCARGO_CODIGO") == 0 ) && ( ! (0==AV55WWGeral_CargoDS_3_Grupocargo_codigo1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[GrupoCargo_Codigo] = @AV55WWGeral_CargoDS_3_Grupocargo_codigo1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[GrupoCargo_Codigo] = @AV55WWGeral_CargoDS_3_Grupocargo_codigo1)";
            }
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV53WWGeral_CargoDS_1_Dynamicfiltersselector1, "CARGO_UOCOD") == 0 ) && ( ! (0==AV56WWGeral_CargoDS_4_Cargo_uocod1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Cargo_UOCod] = @AV56WWGeral_CargoDS_4_Cargo_uocod1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Cargo_UOCod] = @AV56WWGeral_CargoDS_4_Cargo_uocod1)";
            }
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( AV57WWGeral_CargoDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV58WWGeral_CargoDS_6_Dynamicfiltersselector2, "CARGO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59WWGeral_CargoDS_7_Cargo_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Cargo_Nome] like '%' + @lV59WWGeral_CargoDS_7_Cargo_nome2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Cargo_Nome] like '%' + @lV59WWGeral_CargoDS_7_Cargo_nome2 + '%')";
            }
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( AV57WWGeral_CargoDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV58WWGeral_CargoDS_6_Dynamicfiltersselector2, "GRUPOCARGO_CODIGO") == 0 ) && ( ! (0==AV60WWGeral_CargoDS_8_Grupocargo_codigo2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[GrupoCargo_Codigo] = @AV60WWGeral_CargoDS_8_Grupocargo_codigo2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[GrupoCargo_Codigo] = @AV60WWGeral_CargoDS_8_Grupocargo_codigo2)";
            }
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( AV57WWGeral_CargoDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV58WWGeral_CargoDS_6_Dynamicfiltersselector2, "CARGO_UOCOD") == 0 ) && ( ! (0==AV61WWGeral_CargoDS_9_Cargo_uocod2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Cargo_UOCod] = @AV61WWGeral_CargoDS_9_Cargo_uocod2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Cargo_UOCod] = @AV61WWGeral_CargoDS_9_Cargo_uocod2)";
            }
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( AV62WWGeral_CargoDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV63WWGeral_CargoDS_11_Dynamicfiltersselector3, "CARGO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64WWGeral_CargoDS_12_Cargo_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Cargo_Nome] like '%' + @lV64WWGeral_CargoDS_12_Cargo_nome3 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Cargo_Nome] like '%' + @lV64WWGeral_CargoDS_12_Cargo_nome3 + '%')";
            }
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( AV62WWGeral_CargoDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV63WWGeral_CargoDS_11_Dynamicfiltersselector3, "GRUPOCARGO_CODIGO") == 0 ) && ( ! (0==AV65WWGeral_CargoDS_13_Grupocargo_codigo3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[GrupoCargo_Codigo] = @AV65WWGeral_CargoDS_13_Grupocargo_codigo3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[GrupoCargo_Codigo] = @AV65WWGeral_CargoDS_13_Grupocargo_codigo3)";
            }
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( AV62WWGeral_CargoDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV63WWGeral_CargoDS_11_Dynamicfiltersselector3, "CARGO_UOCOD") == 0 ) && ( ! (0==AV66WWGeral_CargoDS_14_Cargo_uocod3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Cargo_UOCod] = @AV66WWGeral_CargoDS_14_Cargo_uocod3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Cargo_UOCod] = @AV66WWGeral_CargoDS_14_Cargo_uocod3)";
            }
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV68WWGeral_CargoDS_16_Tfcargo_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67WWGeral_CargoDS_15_Tfcargo_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Cargo_Nome] like @lV67WWGeral_CargoDS_15_Tfcargo_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Cargo_Nome] like @lV67WWGeral_CargoDS_15_Tfcargo_nome)";
            }
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV68WWGeral_CargoDS_16_Tfcargo_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Cargo_Nome] = @AV68WWGeral_CargoDS_16_Tfcargo_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Cargo_Nome] = @AV68WWGeral_CargoDS_16_Tfcargo_nome_sel)";
            }
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( ! (0==AV69WWGeral_CargoDS_17_Tfgrupocargo_codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[GrupoCargo_Codigo] >= @AV69WWGeral_CargoDS_17_Tfgrupocargo_codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[GrupoCargo_Codigo] >= @AV69WWGeral_CargoDS_17_Tfgrupocargo_codigo)";
            }
         }
         else
         {
            GXv_int3[11] = 1;
         }
         if ( ! (0==AV70WWGeral_CargoDS_18_Tfgrupocargo_codigo_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[GrupoCargo_Codigo] <= @AV70WWGeral_CargoDS_18_Tfgrupocargo_codigo_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[GrupoCargo_Codigo] <= @AV70WWGeral_CargoDS_18_Tfgrupocargo_codigo_to)";
            }
         }
         else
         {
            GXv_int3[12] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV72WWGeral_CargoDS_20_Tfcargo_uonom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV71WWGeral_CargoDS_19_Tfcargo_uonom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[UnidadeOrganizacional_Nome] like @lV71WWGeral_CargoDS_19_Tfcargo_uonom)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[UnidadeOrganizacional_Nome] like @lV71WWGeral_CargoDS_19_Tfcargo_uonom)";
            }
         }
         else
         {
            GXv_int3[13] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV72WWGeral_CargoDS_20_Tfcargo_uonom_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[UnidadeOrganizacional_Nome] = @AV72WWGeral_CargoDS_20_Tfcargo_uonom_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[UnidadeOrganizacional_Nome] = @AV72WWGeral_CargoDS_20_Tfcargo_uonom_sel)";
            }
         }
         else
         {
            GXv_int3[14] = 1;
         }
         if ( AV73WWGeral_CargoDS_21_Tfcargo_ativo_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Cargo_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Cargo_Ativo] = 1)";
            }
         }
         if ( AV73WWGeral_CargoDS_21_Tfcargo_ativo_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Cargo_Ativo] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Cargo_Ativo] = 0)";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[Cargo_UOCod]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00NH2(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (int)dynConstraints[2] , (int)dynConstraints[3] , (bool)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (int)dynConstraints[7] , (int)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (int)dynConstraints[12] , (int)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (int)dynConstraints[16] , (int)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (short)dynConstraints[20] , (String)dynConstraints[21] , (int)dynConstraints[22] , (int)dynConstraints[23] , (String)dynConstraints[24] , (bool)dynConstraints[25] );
               case 1 :
                     return conditional_P00NH3(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (int)dynConstraints[2] , (int)dynConstraints[3] , (bool)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (int)dynConstraints[7] , (int)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (int)dynConstraints[12] , (int)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (int)dynConstraints[16] , (int)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (short)dynConstraints[20] , (String)dynConstraints[21] , (int)dynConstraints[22] , (int)dynConstraints[23] , (String)dynConstraints[24] , (bool)dynConstraints[25] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00NH2 ;
          prmP00NH2 = new Object[] {
          new Object[] {"@lV54WWGeral_CargoDS_2_Cargo_nome1",SqlDbType.VarChar,80,0} ,
          new Object[] {"@AV55WWGeral_CargoDS_3_Grupocargo_codigo1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV56WWGeral_CargoDS_4_Cargo_uocod1",SqlDbType.Int,6,0} ,
          new Object[] {"@lV59WWGeral_CargoDS_7_Cargo_nome2",SqlDbType.VarChar,80,0} ,
          new Object[] {"@AV60WWGeral_CargoDS_8_Grupocargo_codigo2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV61WWGeral_CargoDS_9_Cargo_uocod2",SqlDbType.Int,6,0} ,
          new Object[] {"@lV64WWGeral_CargoDS_12_Cargo_nome3",SqlDbType.VarChar,80,0} ,
          new Object[] {"@AV65WWGeral_CargoDS_13_Grupocargo_codigo3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV66WWGeral_CargoDS_14_Cargo_uocod3",SqlDbType.Int,6,0} ,
          new Object[] {"@lV67WWGeral_CargoDS_15_Tfcargo_nome",SqlDbType.VarChar,80,0} ,
          new Object[] {"@AV68WWGeral_CargoDS_16_Tfcargo_nome_sel",SqlDbType.VarChar,80,0} ,
          new Object[] {"@AV69WWGeral_CargoDS_17_Tfgrupocargo_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV70WWGeral_CargoDS_18_Tfgrupocargo_codigo_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV71WWGeral_CargoDS_19_Tfcargo_uonom",SqlDbType.Char,50,0} ,
          new Object[] {"@AV72WWGeral_CargoDS_20_Tfcargo_uonom_sel",SqlDbType.Char,50,0}
          } ;
          Object[] prmP00NH3 ;
          prmP00NH3 = new Object[] {
          new Object[] {"@lV54WWGeral_CargoDS_2_Cargo_nome1",SqlDbType.VarChar,80,0} ,
          new Object[] {"@AV55WWGeral_CargoDS_3_Grupocargo_codigo1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV56WWGeral_CargoDS_4_Cargo_uocod1",SqlDbType.Int,6,0} ,
          new Object[] {"@lV59WWGeral_CargoDS_7_Cargo_nome2",SqlDbType.VarChar,80,0} ,
          new Object[] {"@AV60WWGeral_CargoDS_8_Grupocargo_codigo2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV61WWGeral_CargoDS_9_Cargo_uocod2",SqlDbType.Int,6,0} ,
          new Object[] {"@lV64WWGeral_CargoDS_12_Cargo_nome3",SqlDbType.VarChar,80,0} ,
          new Object[] {"@AV65WWGeral_CargoDS_13_Grupocargo_codigo3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV66WWGeral_CargoDS_14_Cargo_uocod3",SqlDbType.Int,6,0} ,
          new Object[] {"@lV67WWGeral_CargoDS_15_Tfcargo_nome",SqlDbType.VarChar,80,0} ,
          new Object[] {"@AV68WWGeral_CargoDS_16_Tfcargo_nome_sel",SqlDbType.VarChar,80,0} ,
          new Object[] {"@AV69WWGeral_CargoDS_17_Tfgrupocargo_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV70WWGeral_CargoDS_18_Tfgrupocargo_codigo_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV71WWGeral_CargoDS_19_Tfcargo_uonom",SqlDbType.Char,50,0} ,
          new Object[] {"@AV72WWGeral_CargoDS_20_Tfcargo_uonom_sel",SqlDbType.Char,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00NH2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00NH2,100,0,true,false )
             ,new CursorDef("P00NH3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00NH3,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 50) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((int[]) buf[7])[0] = rslt.getInt(6) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((bool[]) buf[2])[0] = rslt.getBool(2) ;
                ((String[]) buf[3])[0] = rslt.getString(3, 50) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((String[]) buf[6])[0] = rslt.getVarchar(5) ;
                ((int[]) buf[7])[0] = rslt.getInt(6) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[16]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[17]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[19]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[20]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[22]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[23]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[26]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[27]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[16]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[17]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[19]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[20]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[22]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[23]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[26]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[27]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getwwgeral_cargofilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getwwgeral_cargofilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getwwgeral_cargofilterdata") )
          {
             return  ;
          }
          getwwgeral_cargofilterdata worker = new getwwgeral_cargofilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
