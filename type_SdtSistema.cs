/*
               File: type_SdtSistema
        Description: Sistema
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:16:32.70
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "Sistema" )]
   [XmlType(TypeName =  "Sistema" , Namespace = "GxEv3Up14_MeetrikaVs3" )]
   [System.Xml.Serialization.XmlInclude( typeof( SdtSistema_Tecnologia ))]
   [Serializable]
   public class SdtSistema : GxSilentTrnSdt, System.Web.SessionState.IRequiresSessionState
   {
      public SdtSistema( )
      {
         /* Constructor for serialization */
         gxTv_SdtSistema_Sistema_nome = "";
         gxTv_SdtSistema_Sistema_descricao = "";
         gxTv_SdtSistema_Sistema_sigla = "";
         gxTv_SdtSistema_Sistema_coordenacao = "";
         gxTv_SdtSistema_Sistema_areatrabalhodes = "";
         gxTv_SdtSistema_Ambientetecnologico_descricao = "";
         gxTv_SdtSistema_Metodologia_descricao = "";
         gxTv_SdtSistema_Sistema_tipo = "";
         gxTv_SdtSistema_Sistema_tiposigla = "";
         gxTv_SdtSistema_Sistema_tecnica = "";
         gxTv_SdtSistema_Sistema_impdata = (DateTime)(DateTime.MinValue);
         gxTv_SdtSistema_Sistema_impuserpesnom = "";
         gxTv_SdtSistema_Sistemaversao_id = "";
         gxTv_SdtSistema_Sistema_repositorio = "";
         gxTv_SdtSistema_Mode = "";
         gxTv_SdtSistema_Sistema_nome_Z = "";
         gxTv_SdtSistema_Sistema_sigla_Z = "";
         gxTv_SdtSistema_Sistema_coordenacao_Z = "";
         gxTv_SdtSistema_Sistema_areatrabalhodes_Z = "";
         gxTv_SdtSistema_Ambientetecnologico_descricao_Z = "";
         gxTv_SdtSistema_Metodologia_descricao_Z = "";
         gxTv_SdtSistema_Sistema_tipo_Z = "";
         gxTv_SdtSistema_Sistema_tiposigla_Z = "";
         gxTv_SdtSistema_Sistema_tecnica_Z = "";
         gxTv_SdtSistema_Sistema_impdata_Z = (DateTime)(DateTime.MinValue);
         gxTv_SdtSistema_Sistema_impuserpesnom_Z = "";
         gxTv_SdtSistema_Sistemaversao_id_Z = "";
         gxTv_SdtSistema_Sistema_repositorio_Z = "";
      }

      public SdtSistema( IGxContext context )
      {
         this.context = context;
         constructorCallingAssembly = Assembly.GetCallingAssembly();
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public void Load( int AV127Sistema_Codigo )
      {
         IGxSilentTrn obj ;
         obj = getTransaction();
         obj.LoadKey(new Object[] {(int)AV127Sistema_Codigo});
         return  ;
      }

      public override Object[][] GetBCKey( )
      {
         return (Object[][])(new Object[][]{new Object[]{"Sistema_Codigo", typeof(int)}}) ;
      }

      public override IGxCollection GetMessages( )
      {
         short item = 1 ;
         IGxCollection msgs = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs") ;
         SdtMessages_Message m1 ;
         IGxSilentTrn trn = getTransaction() ;
         msglist msgList = trn.GetMessages() ;
         while ( item <= msgList.ItemCount )
         {
            m1 = new SdtMessages_Message(context);
            m1.gxTpr_Id = msgList.getItemValue(item);
            m1.gxTpr_Description = msgList.getItemText(item);
            m1.gxTpr_Type = msgList.getItemType(item);
            msgs.Add(m1, 0);
            item = (short)(item+1);
         }
         return msgs ;
      }

      public override GXProperties GetMetadata( )
      {
         GXProperties metadata = new GXProperties() ;
         metadata.Set("Name", "Sistema");
         metadata.Set("BT", "Sistema");
         metadata.Set("PK", "[ \"Sistema_Codigo\" ]");
         metadata.Set("PKAssigned", "[ \"Sistema_Codigo\" ]");
         metadata.Set("Levels", "[ \"Tecnologia\" ]");
         metadata.Set("Serial", "[ [ \"Same\",\"Sistema\",\"Sistema_Ultimo\",\"SistemaTecnologiaLinha\",\"Sistema_Codigo\",\"Sistema_Codigo\" ] ]");
         metadata.Set("FKList", "[ { \"FK\":[ \"AmbienteTecnologico_Codigo\" ],\"FKMap\":[  ] },{ \"FK\":[ \"AreaTrabalho_Codigo\" ],\"FKMap\":[ \"Sistema_AreaTrabalhoCod-AreaTrabalho_Codigo\" ] },{ \"FK\":[ \"GpoObjCtrl_Codigo\" ],\"FKMap\":[ \"Sistema_GpoObjCtrlCod-GpoObjCtrl_Codigo\" ] },{ \"FK\":[ \"Metodologia_Codigo\" ],\"FKMap\":[  ] },{ \"FK\":[ \"SistemaVersao_Codigo\" ],\"FKMap\":[  ] },{ \"FK\":[ \"Usuario_Codigo\" ],\"FKMap\":[ \"Sistema_ImpUserCod-Usuario_Codigo\" ] } ]");
         metadata.Set("AllowInsert", "True");
         metadata.Set("AllowUpdate", "True");
         metadata.Set("AllowDelete", "True");
         return metadata ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         if ( ! includeState )
         {
            XmlAttributeOverrides ov = new XmlAttributeOverrides();
            XmlAttributes attrs = new XmlAttributes();
            attrs.XmlIgnore = true;
            ov.Add(this.GetType(),  "gxTpr_Tecnologia_GxSilentTrnGridCollection" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Mode" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Initialized" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Sistema_codigo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Sistema_nome_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Sistema_sigla_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Sistema_coordenacao_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Sistema_areatrabalhocod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Sistema_areatrabalhodes_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Ambientetecnologico_codigo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Ambientetecnologico_descricao_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Metodologia_codigo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Metodologia_descricao_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Sistema_pf_Z_double" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Sistema_pfa_Z_double" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Sistema_tipo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Sistema_tiposigla_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Sistema_tecnica_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Sistema_fatorajuste_Z_double" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Sistema_custo_Z_double" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Sistema_prazo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Sistema_esforco_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Sistema_impdata_Z_Nullable" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Sistema_impusercod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Sistema_impuserpescod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Sistema_impuserpesnom_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Sistema_responsavel_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Sistema_gpoobjctrlcod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Sistema_gpoobjctrlrsp_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Sistemaversao_codigo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Sistemaversao_id_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Sistema_ativo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Sistema_repositorio_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Sistema_ultimo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Sistema_descricao_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Sistema_coordenacao_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Sistema_areatrabalhodes_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Ambientetecnologico_codigo_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Metodologia_codigo_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Sistema_tipo_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Sistema_tecnica_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Sistema_fatorajuste_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Sistema_custo_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Sistema_prazo_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Sistema_esforco_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Sistema_impdata_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Sistema_impusercod_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Sistema_impuserpescod_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Sistema_impuserpesnom_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Sistema_responsavel_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Sistema_gpoobjctrlcod_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Sistemaversao_codigo_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Sistema_repositorio_N" , attrs);
            xmls = new XmlSerializer(this.GetType(), ov, new Type[0], null, sNameSpace);
         }
         else
         {
            xmls = new XmlSerializer(this.GetType(), sNameSpace);
         }
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtSistema deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_MeetrikaVs3" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtSistema)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtSistema obj ;
         obj = this;
         obj.gxTpr_Sistema_codigo = deserialized.gxTpr_Sistema_codigo;
         obj.gxTpr_Sistema_nome = deserialized.gxTpr_Sistema_nome;
         obj.gxTpr_Sistema_descricao = deserialized.gxTpr_Sistema_descricao;
         obj.gxTpr_Sistema_sigla = deserialized.gxTpr_Sistema_sigla;
         obj.gxTpr_Sistema_coordenacao = deserialized.gxTpr_Sistema_coordenacao;
         obj.gxTpr_Sistema_areatrabalhocod = deserialized.gxTpr_Sistema_areatrabalhocod;
         obj.gxTpr_Sistema_areatrabalhodes = deserialized.gxTpr_Sistema_areatrabalhodes;
         obj.gxTpr_Ambientetecnologico_codigo = deserialized.gxTpr_Ambientetecnologico_codigo;
         obj.gxTpr_Ambientetecnologico_descricao = deserialized.gxTpr_Ambientetecnologico_descricao;
         obj.gxTpr_Metodologia_codigo = deserialized.gxTpr_Metodologia_codigo;
         obj.gxTpr_Metodologia_descricao = deserialized.gxTpr_Metodologia_descricao;
         obj.gxTpr_Sistema_pf = deserialized.gxTpr_Sistema_pf;
         obj.gxTpr_Sistema_pfa = deserialized.gxTpr_Sistema_pfa;
         obj.gxTpr_Sistema_tipo = deserialized.gxTpr_Sistema_tipo;
         obj.gxTpr_Sistema_tiposigla = deserialized.gxTpr_Sistema_tiposigla;
         obj.gxTpr_Sistema_tecnica = deserialized.gxTpr_Sistema_tecnica;
         obj.gxTpr_Sistema_fatorajuste = deserialized.gxTpr_Sistema_fatorajuste;
         obj.gxTpr_Sistema_custo = deserialized.gxTpr_Sistema_custo;
         obj.gxTpr_Sistema_prazo = deserialized.gxTpr_Sistema_prazo;
         obj.gxTpr_Sistema_esforco = deserialized.gxTpr_Sistema_esforco;
         obj.gxTpr_Sistema_impdata = deserialized.gxTpr_Sistema_impdata;
         obj.gxTpr_Sistema_impusercod = deserialized.gxTpr_Sistema_impusercod;
         obj.gxTpr_Sistema_impuserpescod = deserialized.gxTpr_Sistema_impuserpescod;
         obj.gxTpr_Sistema_impuserpesnom = deserialized.gxTpr_Sistema_impuserpesnom;
         obj.gxTpr_Sistema_responsavel = deserialized.gxTpr_Sistema_responsavel;
         obj.gxTpr_Sistema_gpoobjctrlcod = deserialized.gxTpr_Sistema_gpoobjctrlcod;
         obj.gxTpr_Sistema_gpoobjctrlrsp = deserialized.gxTpr_Sistema_gpoobjctrlrsp;
         obj.gxTpr_Sistemaversao_codigo = deserialized.gxTpr_Sistemaversao_codigo;
         obj.gxTpr_Sistemaversao_id = deserialized.gxTpr_Sistemaversao_id;
         obj.gxTpr_Sistema_ativo = deserialized.gxTpr_Sistema_ativo;
         obj.gxTpr_Sistema_repositorio = deserialized.gxTpr_Sistema_repositorio;
         obj.gxTpr_Sistema_ultimo = deserialized.gxTpr_Sistema_ultimo;
         obj.gxTpr_Tecnologia = deserialized.gxTpr_Tecnologia;
         obj.gxTpr_Mode = deserialized.gxTpr_Mode;
         obj.gxTpr_Initialized = deserialized.gxTpr_Initialized;
         obj.gxTpr_Sistema_codigo_Z = deserialized.gxTpr_Sistema_codigo_Z;
         obj.gxTpr_Sistema_nome_Z = deserialized.gxTpr_Sistema_nome_Z;
         obj.gxTpr_Sistema_sigla_Z = deserialized.gxTpr_Sistema_sigla_Z;
         obj.gxTpr_Sistema_coordenacao_Z = deserialized.gxTpr_Sistema_coordenacao_Z;
         obj.gxTpr_Sistema_areatrabalhocod_Z = deserialized.gxTpr_Sistema_areatrabalhocod_Z;
         obj.gxTpr_Sistema_areatrabalhodes_Z = deserialized.gxTpr_Sistema_areatrabalhodes_Z;
         obj.gxTpr_Ambientetecnologico_codigo_Z = deserialized.gxTpr_Ambientetecnologico_codigo_Z;
         obj.gxTpr_Ambientetecnologico_descricao_Z = deserialized.gxTpr_Ambientetecnologico_descricao_Z;
         obj.gxTpr_Metodologia_codigo_Z = deserialized.gxTpr_Metodologia_codigo_Z;
         obj.gxTpr_Metodologia_descricao_Z = deserialized.gxTpr_Metodologia_descricao_Z;
         obj.gxTpr_Sistema_pf_Z = deserialized.gxTpr_Sistema_pf_Z;
         obj.gxTpr_Sistema_pfa_Z = deserialized.gxTpr_Sistema_pfa_Z;
         obj.gxTpr_Sistema_tipo_Z = deserialized.gxTpr_Sistema_tipo_Z;
         obj.gxTpr_Sistema_tiposigla_Z = deserialized.gxTpr_Sistema_tiposigla_Z;
         obj.gxTpr_Sistema_tecnica_Z = deserialized.gxTpr_Sistema_tecnica_Z;
         obj.gxTpr_Sistema_fatorajuste_Z = deserialized.gxTpr_Sistema_fatorajuste_Z;
         obj.gxTpr_Sistema_custo_Z = deserialized.gxTpr_Sistema_custo_Z;
         obj.gxTpr_Sistema_prazo_Z = deserialized.gxTpr_Sistema_prazo_Z;
         obj.gxTpr_Sistema_esforco_Z = deserialized.gxTpr_Sistema_esforco_Z;
         obj.gxTpr_Sistema_impdata_Z = deserialized.gxTpr_Sistema_impdata_Z;
         obj.gxTpr_Sistema_impusercod_Z = deserialized.gxTpr_Sistema_impusercod_Z;
         obj.gxTpr_Sistema_impuserpescod_Z = deserialized.gxTpr_Sistema_impuserpescod_Z;
         obj.gxTpr_Sistema_impuserpesnom_Z = deserialized.gxTpr_Sistema_impuserpesnom_Z;
         obj.gxTpr_Sistema_responsavel_Z = deserialized.gxTpr_Sistema_responsavel_Z;
         obj.gxTpr_Sistema_gpoobjctrlcod_Z = deserialized.gxTpr_Sistema_gpoobjctrlcod_Z;
         obj.gxTpr_Sistema_gpoobjctrlrsp_Z = deserialized.gxTpr_Sistema_gpoobjctrlrsp_Z;
         obj.gxTpr_Sistemaversao_codigo_Z = deserialized.gxTpr_Sistemaversao_codigo_Z;
         obj.gxTpr_Sistemaversao_id_Z = deserialized.gxTpr_Sistemaversao_id_Z;
         obj.gxTpr_Sistema_ativo_Z = deserialized.gxTpr_Sistema_ativo_Z;
         obj.gxTpr_Sistema_repositorio_Z = deserialized.gxTpr_Sistema_repositorio_Z;
         obj.gxTpr_Sistema_ultimo_Z = deserialized.gxTpr_Sistema_ultimo_Z;
         obj.gxTpr_Sistema_descricao_N = deserialized.gxTpr_Sistema_descricao_N;
         obj.gxTpr_Sistema_coordenacao_N = deserialized.gxTpr_Sistema_coordenacao_N;
         obj.gxTpr_Sistema_areatrabalhodes_N = deserialized.gxTpr_Sistema_areatrabalhodes_N;
         obj.gxTpr_Ambientetecnologico_codigo_N = deserialized.gxTpr_Ambientetecnologico_codigo_N;
         obj.gxTpr_Metodologia_codigo_N = deserialized.gxTpr_Metodologia_codigo_N;
         obj.gxTpr_Sistema_tipo_N = deserialized.gxTpr_Sistema_tipo_N;
         obj.gxTpr_Sistema_tecnica_N = deserialized.gxTpr_Sistema_tecnica_N;
         obj.gxTpr_Sistema_fatorajuste_N = deserialized.gxTpr_Sistema_fatorajuste_N;
         obj.gxTpr_Sistema_custo_N = deserialized.gxTpr_Sistema_custo_N;
         obj.gxTpr_Sistema_prazo_N = deserialized.gxTpr_Sistema_prazo_N;
         obj.gxTpr_Sistema_esforco_N = deserialized.gxTpr_Sistema_esforco_N;
         obj.gxTpr_Sistema_impdata_N = deserialized.gxTpr_Sistema_impdata_N;
         obj.gxTpr_Sistema_impusercod_N = deserialized.gxTpr_Sistema_impusercod_N;
         obj.gxTpr_Sistema_impuserpescod_N = deserialized.gxTpr_Sistema_impuserpescod_N;
         obj.gxTpr_Sistema_impuserpesnom_N = deserialized.gxTpr_Sistema_impuserpesnom_N;
         obj.gxTpr_Sistema_responsavel_N = deserialized.gxTpr_Sistema_responsavel_N;
         obj.gxTpr_Sistema_gpoobjctrlcod_N = deserialized.gxTpr_Sistema_gpoobjctrlcod_N;
         obj.gxTpr_Sistemaversao_codigo_N = deserialized.gxTpr_Sistemaversao_codigo_N;
         obj.gxTpr_Sistema_repositorio_N = deserialized.gxTpr_Sistema_repositorio_N;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "Sistema_Codigo") )
               {
                  gxTv_SdtSistema_Sistema_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Sistema_Nome") )
               {
                  gxTv_SdtSistema_Sistema_nome = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Sistema_Descricao") )
               {
                  gxTv_SdtSistema_Sistema_descricao = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Sistema_Sigla") )
               {
                  gxTv_SdtSistema_Sistema_sigla = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Sistema_Coordenacao") )
               {
                  gxTv_SdtSistema_Sistema_coordenacao = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Sistema_AreaTrabalhoCod") )
               {
                  gxTv_SdtSistema_Sistema_areatrabalhocod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Sistema_AreaTrabalhoDes") )
               {
                  gxTv_SdtSistema_Sistema_areatrabalhodes = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "AmbienteTecnologico_Codigo") )
               {
                  gxTv_SdtSistema_Ambientetecnologico_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "AmbienteTecnologico_Descricao") )
               {
                  gxTv_SdtSistema_Ambientetecnologico_descricao = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Metodologia_Codigo") )
               {
                  gxTv_SdtSistema_Metodologia_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Metodologia_Descricao") )
               {
                  gxTv_SdtSistema_Metodologia_descricao = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Sistema_PF") )
               {
                  gxTv_SdtSistema_Sistema_pf = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Sistema_PFA") )
               {
                  gxTv_SdtSistema_Sistema_pfa = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Sistema_Tipo") )
               {
                  gxTv_SdtSistema_Sistema_tipo = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Sistema_TipoSigla") )
               {
                  gxTv_SdtSistema_Sistema_tiposigla = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Sistema_Tecnica") )
               {
                  gxTv_SdtSistema_Sistema_tecnica = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Sistema_FatorAjuste") )
               {
                  gxTv_SdtSistema_Sistema_fatorajuste = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Sistema_Custo") )
               {
                  gxTv_SdtSistema_Sistema_custo = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Sistema_Prazo") )
               {
                  gxTv_SdtSistema_Sistema_prazo = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Sistema_Esforco") )
               {
                  gxTv_SdtSistema_Sistema_esforco = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Sistema_ImpData") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00T00:00:00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtSistema_Sistema_impdata = (DateTime)(DateTime.MinValue);
                  }
                  else
                  {
                     gxTv_SdtSistema_Sistema_impdata = context.localUtil.YMDHMSToT( (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 12, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 15, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 18, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Sistema_ImpUserCod") )
               {
                  gxTv_SdtSistema_Sistema_impusercod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Sistema_ImpUserPesCod") )
               {
                  gxTv_SdtSistema_Sistema_impuserpescod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Sistema_ImpUserPesNom") )
               {
                  gxTv_SdtSistema_Sistema_impuserpesnom = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Sistema_Responsavel") )
               {
                  gxTv_SdtSistema_Sistema_responsavel = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Sistema_GpoObjCtrlCod") )
               {
                  gxTv_SdtSistema_Sistema_gpoobjctrlcod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Sistema_GpoObjCtrlRsp") )
               {
                  gxTv_SdtSistema_Sistema_gpoobjctrlrsp = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "SistemaVersao_Codigo") )
               {
                  gxTv_SdtSistema_Sistemaversao_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "SistemaVersao_Id") )
               {
                  gxTv_SdtSistema_Sistemaversao_id = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Sistema_Ativo") )
               {
                  gxTv_SdtSistema_Sistema_ativo = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Sistema_Repositorio") )
               {
                  gxTv_SdtSistema_Sistema_repositorio = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Sistema_Ultimo") )
               {
                  gxTv_SdtSistema_Sistema_ultimo = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Tecnologia") )
               {
                  if ( gxTv_SdtSistema_Tecnologia == null )
                  {
                     gxTv_SdtSistema_Tecnologia = new GxSilentTrnGridCollection( context, "Sistema.Tecnologia", "GxEv3Up14_MeetrikaVs3", "SdtSistema_Tecnologia", "GeneXus.Programs");
                  }
                  if ( ( oReader.IsSimple == 0 ) || ( oReader.AttributeCount > 0 ) )
                  {
                     GXSoapError = gxTv_SdtSistema_Tecnologia.readxml(oReader, "Tecnologia");
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Mode") )
               {
                  gxTv_SdtSistema_Mode = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Initialized") )
               {
                  gxTv_SdtSistema_Initialized = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Sistema_Codigo_Z") )
               {
                  gxTv_SdtSistema_Sistema_codigo_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Sistema_Nome_Z") )
               {
                  gxTv_SdtSistema_Sistema_nome_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Sistema_Sigla_Z") )
               {
                  gxTv_SdtSistema_Sistema_sigla_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Sistema_Coordenacao_Z") )
               {
                  gxTv_SdtSistema_Sistema_coordenacao_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Sistema_AreaTrabalhoCod_Z") )
               {
                  gxTv_SdtSistema_Sistema_areatrabalhocod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Sistema_AreaTrabalhoDes_Z") )
               {
                  gxTv_SdtSistema_Sistema_areatrabalhodes_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "AmbienteTecnologico_Codigo_Z") )
               {
                  gxTv_SdtSistema_Ambientetecnologico_codigo_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "AmbienteTecnologico_Descricao_Z") )
               {
                  gxTv_SdtSistema_Ambientetecnologico_descricao_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Metodologia_Codigo_Z") )
               {
                  gxTv_SdtSistema_Metodologia_codigo_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Metodologia_Descricao_Z") )
               {
                  gxTv_SdtSistema_Metodologia_descricao_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Sistema_PF_Z") )
               {
                  gxTv_SdtSistema_Sistema_pf_Z = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Sistema_PFA_Z") )
               {
                  gxTv_SdtSistema_Sistema_pfa_Z = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Sistema_Tipo_Z") )
               {
                  gxTv_SdtSistema_Sistema_tipo_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Sistema_TipoSigla_Z") )
               {
                  gxTv_SdtSistema_Sistema_tiposigla_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Sistema_Tecnica_Z") )
               {
                  gxTv_SdtSistema_Sistema_tecnica_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Sistema_FatorAjuste_Z") )
               {
                  gxTv_SdtSistema_Sistema_fatorajuste_Z = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Sistema_Custo_Z") )
               {
                  gxTv_SdtSistema_Sistema_custo_Z = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Sistema_Prazo_Z") )
               {
                  gxTv_SdtSistema_Sistema_prazo_Z = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Sistema_Esforco_Z") )
               {
                  gxTv_SdtSistema_Sistema_esforco_Z = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Sistema_ImpData_Z") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00T00:00:00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtSistema_Sistema_impdata_Z = (DateTime)(DateTime.MinValue);
                  }
                  else
                  {
                     gxTv_SdtSistema_Sistema_impdata_Z = context.localUtil.YMDHMSToT( (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 12, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 15, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 18, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Sistema_ImpUserCod_Z") )
               {
                  gxTv_SdtSistema_Sistema_impusercod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Sistema_ImpUserPesCod_Z") )
               {
                  gxTv_SdtSistema_Sistema_impuserpescod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Sistema_ImpUserPesNom_Z") )
               {
                  gxTv_SdtSistema_Sistema_impuserpesnom_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Sistema_Responsavel_Z") )
               {
                  gxTv_SdtSistema_Sistema_responsavel_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Sistema_GpoObjCtrlCod_Z") )
               {
                  gxTv_SdtSistema_Sistema_gpoobjctrlcod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Sistema_GpoObjCtrlRsp_Z") )
               {
                  gxTv_SdtSistema_Sistema_gpoobjctrlrsp_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "SistemaVersao_Codigo_Z") )
               {
                  gxTv_SdtSistema_Sistemaversao_codigo_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "SistemaVersao_Id_Z") )
               {
                  gxTv_SdtSistema_Sistemaversao_id_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Sistema_Ativo_Z") )
               {
                  gxTv_SdtSistema_Sistema_ativo_Z = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Sistema_Repositorio_Z") )
               {
                  gxTv_SdtSistema_Sistema_repositorio_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Sistema_Ultimo_Z") )
               {
                  gxTv_SdtSistema_Sistema_ultimo_Z = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Sistema_Descricao_N") )
               {
                  gxTv_SdtSistema_Sistema_descricao_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Sistema_Coordenacao_N") )
               {
                  gxTv_SdtSistema_Sistema_coordenacao_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Sistema_AreaTrabalhoDes_N") )
               {
                  gxTv_SdtSistema_Sistema_areatrabalhodes_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "AmbienteTecnologico_Codigo_N") )
               {
                  gxTv_SdtSistema_Ambientetecnologico_codigo_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Metodologia_Codigo_N") )
               {
                  gxTv_SdtSistema_Metodologia_codigo_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Sistema_Tipo_N") )
               {
                  gxTv_SdtSistema_Sistema_tipo_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Sistema_Tecnica_N") )
               {
                  gxTv_SdtSistema_Sistema_tecnica_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Sistema_FatorAjuste_N") )
               {
                  gxTv_SdtSistema_Sistema_fatorajuste_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Sistema_Custo_N") )
               {
                  gxTv_SdtSistema_Sistema_custo_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Sistema_Prazo_N") )
               {
                  gxTv_SdtSistema_Sistema_prazo_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Sistema_Esforco_N") )
               {
                  gxTv_SdtSistema_Sistema_esforco_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Sistema_ImpData_N") )
               {
                  gxTv_SdtSistema_Sistema_impdata_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Sistema_ImpUserCod_N") )
               {
                  gxTv_SdtSistema_Sistema_impusercod_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Sistema_ImpUserPesCod_N") )
               {
                  gxTv_SdtSistema_Sistema_impuserpescod_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Sistema_ImpUserPesNom_N") )
               {
                  gxTv_SdtSistema_Sistema_impuserpesnom_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Sistema_Responsavel_N") )
               {
                  gxTv_SdtSistema_Sistema_responsavel_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Sistema_GpoObjCtrlCod_N") )
               {
                  gxTv_SdtSistema_Sistema_gpoobjctrlcod_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "SistemaVersao_Codigo_N") )
               {
                  gxTv_SdtSistema_Sistemaversao_codigo_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Sistema_Repositorio_N") )
               {
                  gxTv_SdtSistema_Sistema_repositorio_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "Sistema";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "GxEv3Up14_MeetrikaVs3";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("Sistema_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSistema_Sistema_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Sistema_Nome", StringUtil.RTrim( gxTv_SdtSistema_Sistema_nome));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Sistema_Descricao", StringUtil.RTrim( gxTv_SdtSistema_Sistema_descricao));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Sistema_Sigla", StringUtil.RTrim( gxTv_SdtSistema_Sistema_sigla));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Sistema_Coordenacao", StringUtil.RTrim( gxTv_SdtSistema_Sistema_coordenacao));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Sistema_AreaTrabalhoCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSistema_Sistema_areatrabalhocod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Sistema_AreaTrabalhoDes", StringUtil.RTrim( gxTv_SdtSistema_Sistema_areatrabalhodes));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("AmbienteTecnologico_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSistema_Ambientetecnologico_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("AmbienteTecnologico_Descricao", StringUtil.RTrim( gxTv_SdtSistema_Ambientetecnologico_descricao));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Metodologia_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSistema_Metodologia_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Metodologia_Descricao", StringUtil.RTrim( gxTv_SdtSistema_Metodologia_descricao));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Sistema_PF", StringUtil.Trim( StringUtil.Str( gxTv_SdtSistema_Sistema_pf, 14, 5)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Sistema_PFA", StringUtil.Trim( StringUtil.Str( gxTv_SdtSistema_Sistema_pfa, 14, 5)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Sistema_Tipo", StringUtil.RTrim( gxTv_SdtSistema_Sistema_tipo));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Sistema_TipoSigla", StringUtil.RTrim( gxTv_SdtSistema_Sistema_tiposigla));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Sistema_Tecnica", StringUtil.RTrim( gxTv_SdtSistema_Sistema_tecnica));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Sistema_FatorAjuste", StringUtil.Trim( StringUtil.Str( gxTv_SdtSistema_Sistema_fatorajuste, 6, 2)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Sistema_Custo", StringUtil.Trim( StringUtil.Str( gxTv_SdtSistema_Sistema_custo, 18, 5)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Sistema_Prazo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSistema_Sistema_prazo), 4, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Sistema_Esforco", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSistema_Sistema_esforco), 4, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         if ( (DateTime.MinValue==gxTv_SdtSistema_Sistema_impdata) )
         {
            oWriter.WriteStartElement("Sistema_ImpData");
            oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            oWriter.WriteAttribute("xsi:nil", "true");
            oWriter.WriteEndElement();
         }
         else
         {
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtSistema_Sistema_impdata)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtSistema_Sistema_impdata)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtSistema_Sistema_impdata)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "T";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( gxTv_SdtSistema_Sistema_impdata)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( gxTv_SdtSistema_Sistema_impdata)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( gxTv_SdtSistema_Sistema_impdata)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            oWriter.WriteElement("Sistema_ImpData", sDateCnv);
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
         }
         oWriter.WriteElement("Sistema_ImpUserCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSistema_Sistema_impusercod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Sistema_ImpUserPesCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSistema_Sistema_impuserpescod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Sistema_ImpUserPesNom", StringUtil.RTrim( gxTv_SdtSistema_Sistema_impuserpesnom));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Sistema_Responsavel", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSistema_Sistema_responsavel), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Sistema_GpoObjCtrlCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSistema_Sistema_gpoobjctrlcod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Sistema_GpoObjCtrlRsp", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSistema_Sistema_gpoobjctrlrsp), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("SistemaVersao_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSistema_Sistemaversao_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("SistemaVersao_Id", StringUtil.RTrim( gxTv_SdtSistema_Sistemaversao_id));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Sistema_Ativo", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtSistema_Sistema_ativo)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Sistema_Repositorio", StringUtil.RTrim( gxTv_SdtSistema_Sistema_repositorio));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Sistema_Ultimo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSistema_Sistema_ultimo), 4, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         if ( sIncludeState )
         {
            if ( gxTv_SdtSistema_Tecnologia != null )
            {
               String sNameSpace1 ;
               if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") == 0 )
               {
                  sNameSpace1 = "[*:nosend]" + "GxEv3Up14_MeetrikaVs3";
               }
               else
               {
                  sNameSpace1 = "GxEv3Up14_MeetrikaVs3";
               }
               gxTv_SdtSistema_Tecnologia.writexml(oWriter, "Tecnologia", sNameSpace1);
            }
            oWriter.WriteElement("Mode", StringUtil.RTrim( gxTv_SdtSistema_Mode));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Initialized", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSistema_Initialized), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Sistema_Codigo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSistema_Sistema_codigo_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Sistema_Nome_Z", StringUtil.RTrim( gxTv_SdtSistema_Sistema_nome_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Sistema_Sigla_Z", StringUtil.RTrim( gxTv_SdtSistema_Sistema_sigla_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Sistema_Coordenacao_Z", StringUtil.RTrim( gxTv_SdtSistema_Sistema_coordenacao_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Sistema_AreaTrabalhoCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSistema_Sistema_areatrabalhocod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Sistema_AreaTrabalhoDes_Z", StringUtil.RTrim( gxTv_SdtSistema_Sistema_areatrabalhodes_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("AmbienteTecnologico_Codigo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSistema_Ambientetecnologico_codigo_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("AmbienteTecnologico_Descricao_Z", StringUtil.RTrim( gxTv_SdtSistema_Ambientetecnologico_descricao_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Metodologia_Codigo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSistema_Metodologia_codigo_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Metodologia_Descricao_Z", StringUtil.RTrim( gxTv_SdtSistema_Metodologia_descricao_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Sistema_PF_Z", StringUtil.Trim( StringUtil.Str( gxTv_SdtSistema_Sistema_pf_Z, 14, 5)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Sistema_PFA_Z", StringUtil.Trim( StringUtil.Str( gxTv_SdtSistema_Sistema_pfa_Z, 14, 5)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Sistema_Tipo_Z", StringUtil.RTrim( gxTv_SdtSistema_Sistema_tipo_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Sistema_TipoSigla_Z", StringUtil.RTrim( gxTv_SdtSistema_Sistema_tiposigla_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Sistema_Tecnica_Z", StringUtil.RTrim( gxTv_SdtSistema_Sistema_tecnica_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Sistema_FatorAjuste_Z", StringUtil.Trim( StringUtil.Str( gxTv_SdtSistema_Sistema_fatorajuste_Z, 6, 2)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Sistema_Custo_Z", StringUtil.Trim( StringUtil.Str( gxTv_SdtSistema_Sistema_custo_Z, 18, 5)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Sistema_Prazo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSistema_Sistema_prazo_Z), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Sistema_Esforco_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSistema_Sistema_esforco_Z), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            if ( (DateTime.MinValue==gxTv_SdtSistema_Sistema_impdata_Z) )
            {
               oWriter.WriteStartElement("Sistema_ImpData_Z");
               oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
               oWriter.WriteAttribute("xsi:nil", "true");
               oWriter.WriteEndElement();
            }
            else
            {
               sDateCnv = "";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtSistema_Sistema_impdata_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "-";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtSistema_Sistema_impdata_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "-";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtSistema_Sistema_impdata_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "T";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( gxTv_SdtSistema_Sistema_impdata_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + ":";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( gxTv_SdtSistema_Sistema_impdata_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + ":";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( gxTv_SdtSistema_Sistema_impdata_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               oWriter.WriteElement("Sistema_ImpData_Z", sDateCnv);
               if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
               {
                  oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
               }
            }
            oWriter.WriteElement("Sistema_ImpUserCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSistema_Sistema_impusercod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Sistema_ImpUserPesCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSistema_Sistema_impuserpescod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Sistema_ImpUserPesNom_Z", StringUtil.RTrim( gxTv_SdtSistema_Sistema_impuserpesnom_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Sistema_Responsavel_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSistema_Sistema_responsavel_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Sistema_GpoObjCtrlCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSistema_Sistema_gpoobjctrlcod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Sistema_GpoObjCtrlRsp_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSistema_Sistema_gpoobjctrlrsp_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("SistemaVersao_Codigo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSistema_Sistemaversao_codigo_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("SistemaVersao_Id_Z", StringUtil.RTrim( gxTv_SdtSistema_Sistemaversao_id_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Sistema_Ativo_Z", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtSistema_Sistema_ativo_Z)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Sistema_Repositorio_Z", StringUtil.RTrim( gxTv_SdtSistema_Sistema_repositorio_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Sistema_Ultimo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSistema_Sistema_ultimo_Z), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Sistema_Descricao_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSistema_Sistema_descricao_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Sistema_Coordenacao_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSistema_Sistema_coordenacao_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Sistema_AreaTrabalhoDes_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSistema_Sistema_areatrabalhodes_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("AmbienteTecnologico_Codigo_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSistema_Ambientetecnologico_codigo_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Metodologia_Codigo_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSistema_Metodologia_codigo_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Sistema_Tipo_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSistema_Sistema_tipo_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Sistema_Tecnica_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSistema_Sistema_tecnica_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Sistema_FatorAjuste_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSistema_Sistema_fatorajuste_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Sistema_Custo_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSistema_Sistema_custo_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Sistema_Prazo_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSistema_Sistema_prazo_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Sistema_Esforco_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSistema_Sistema_esforco_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Sistema_ImpData_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSistema_Sistema_impdata_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Sistema_ImpUserCod_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSistema_Sistema_impusercod_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Sistema_ImpUserPesCod_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSistema_Sistema_impuserpescod_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Sistema_ImpUserPesNom_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSistema_Sistema_impuserpesnom_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Sistema_Responsavel_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSistema_Sistema_responsavel_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Sistema_GpoObjCtrlCod_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSistema_Sistema_gpoobjctrlcod_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("SistemaVersao_Codigo_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSistema_Sistemaversao_codigo_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Sistema_Repositorio_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSistema_Sistema_repositorio_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("Sistema_Codigo", gxTv_SdtSistema_Sistema_codigo, false);
         AddObjectProperty("Sistema_Nome", gxTv_SdtSistema_Sistema_nome, false);
         AddObjectProperty("Sistema_Descricao", gxTv_SdtSistema_Sistema_descricao, false);
         AddObjectProperty("Sistema_Sigla", gxTv_SdtSistema_Sistema_sigla, false);
         AddObjectProperty("Sistema_Coordenacao", gxTv_SdtSistema_Sistema_coordenacao, false);
         AddObjectProperty("Sistema_AreaTrabalhoCod", gxTv_SdtSistema_Sistema_areatrabalhocod, false);
         AddObjectProperty("Sistema_AreaTrabalhoDes", gxTv_SdtSistema_Sistema_areatrabalhodes, false);
         AddObjectProperty("AmbienteTecnologico_Codigo", gxTv_SdtSistema_Ambientetecnologico_codigo, false);
         AddObjectProperty("AmbienteTecnologico_Descricao", gxTv_SdtSistema_Ambientetecnologico_descricao, false);
         AddObjectProperty("Metodologia_Codigo", gxTv_SdtSistema_Metodologia_codigo, false);
         AddObjectProperty("Metodologia_Descricao", gxTv_SdtSistema_Metodologia_descricao, false);
         AddObjectProperty("Sistema_PF", gxTv_SdtSistema_Sistema_pf, false);
         AddObjectProperty("Sistema_PFA", gxTv_SdtSistema_Sistema_pfa, false);
         AddObjectProperty("Sistema_Tipo", gxTv_SdtSistema_Sistema_tipo, false);
         AddObjectProperty("Sistema_TipoSigla", gxTv_SdtSistema_Sistema_tiposigla, false);
         AddObjectProperty("Sistema_Tecnica", gxTv_SdtSistema_Sistema_tecnica, false);
         AddObjectProperty("Sistema_FatorAjuste", gxTv_SdtSistema_Sistema_fatorajuste, false);
         AddObjectProperty("Sistema_Custo", StringUtil.LTrim( StringUtil.Str( gxTv_SdtSistema_Sistema_custo, 18, 5)), false);
         AddObjectProperty("Sistema_Prazo", gxTv_SdtSistema_Sistema_prazo, false);
         AddObjectProperty("Sistema_Esforco", gxTv_SdtSistema_Sistema_esforco, false);
         datetime_STZ = gxTv_SdtSistema_Sistema_impdata;
         sDateCnv = "";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "T";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + ":";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + ":";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         AddObjectProperty("Sistema_ImpData", sDateCnv, false);
         AddObjectProperty("Sistema_ImpUserCod", gxTv_SdtSistema_Sistema_impusercod, false);
         AddObjectProperty("Sistema_ImpUserPesCod", gxTv_SdtSistema_Sistema_impuserpescod, false);
         AddObjectProperty("Sistema_ImpUserPesNom", gxTv_SdtSistema_Sistema_impuserpesnom, false);
         AddObjectProperty("Sistema_Responsavel", gxTv_SdtSistema_Sistema_responsavel, false);
         AddObjectProperty("Sistema_GpoObjCtrlCod", gxTv_SdtSistema_Sistema_gpoobjctrlcod, false);
         AddObjectProperty("Sistema_GpoObjCtrlRsp", gxTv_SdtSistema_Sistema_gpoobjctrlrsp, false);
         AddObjectProperty("SistemaVersao_Codigo", gxTv_SdtSistema_Sistemaversao_codigo, false);
         AddObjectProperty("SistemaVersao_Id", gxTv_SdtSistema_Sistemaversao_id, false);
         AddObjectProperty("Sistema_Ativo", gxTv_SdtSistema_Sistema_ativo, false);
         AddObjectProperty("Sistema_Repositorio", gxTv_SdtSistema_Sistema_repositorio, false);
         AddObjectProperty("Sistema_Ultimo", gxTv_SdtSistema_Sistema_ultimo, false);
         if ( gxTv_SdtSistema_Tecnologia != null )
         {
            AddObjectProperty("Tecnologia", gxTv_SdtSistema_Tecnologia, includeState);
         }
         if ( includeState )
         {
            AddObjectProperty("Mode", gxTv_SdtSistema_Mode, false);
            AddObjectProperty("Initialized", gxTv_SdtSistema_Initialized, false);
            AddObjectProperty("Sistema_Codigo_Z", gxTv_SdtSistema_Sistema_codigo_Z, false);
            AddObjectProperty("Sistema_Nome_Z", gxTv_SdtSistema_Sistema_nome_Z, false);
            AddObjectProperty("Sistema_Sigla_Z", gxTv_SdtSistema_Sistema_sigla_Z, false);
            AddObjectProperty("Sistema_Coordenacao_Z", gxTv_SdtSistema_Sistema_coordenacao_Z, false);
            AddObjectProperty("Sistema_AreaTrabalhoCod_Z", gxTv_SdtSistema_Sistema_areatrabalhocod_Z, false);
            AddObjectProperty("Sistema_AreaTrabalhoDes_Z", gxTv_SdtSistema_Sistema_areatrabalhodes_Z, false);
            AddObjectProperty("AmbienteTecnologico_Codigo_Z", gxTv_SdtSistema_Ambientetecnologico_codigo_Z, false);
            AddObjectProperty("AmbienteTecnologico_Descricao_Z", gxTv_SdtSistema_Ambientetecnologico_descricao_Z, false);
            AddObjectProperty("Metodologia_Codigo_Z", gxTv_SdtSistema_Metodologia_codigo_Z, false);
            AddObjectProperty("Metodologia_Descricao_Z", gxTv_SdtSistema_Metodologia_descricao_Z, false);
            AddObjectProperty("Sistema_PF_Z", gxTv_SdtSistema_Sistema_pf_Z, false);
            AddObjectProperty("Sistema_PFA_Z", gxTv_SdtSistema_Sistema_pfa_Z, false);
            AddObjectProperty("Sistema_Tipo_Z", gxTv_SdtSistema_Sistema_tipo_Z, false);
            AddObjectProperty("Sistema_TipoSigla_Z", gxTv_SdtSistema_Sistema_tiposigla_Z, false);
            AddObjectProperty("Sistema_Tecnica_Z", gxTv_SdtSistema_Sistema_tecnica_Z, false);
            AddObjectProperty("Sistema_FatorAjuste_Z", gxTv_SdtSistema_Sistema_fatorajuste_Z, false);
            AddObjectProperty("Sistema_Custo_Z", StringUtil.LTrim( StringUtil.Str( gxTv_SdtSistema_Sistema_custo_Z, 18, 5)), false);
            AddObjectProperty("Sistema_Prazo_Z", gxTv_SdtSistema_Sistema_prazo_Z, false);
            AddObjectProperty("Sistema_Esforco_Z", gxTv_SdtSistema_Sistema_esforco_Z, false);
            datetime_STZ = gxTv_SdtSistema_Sistema_impdata_Z;
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "T";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            AddObjectProperty("Sistema_ImpData_Z", sDateCnv, false);
            AddObjectProperty("Sistema_ImpUserCod_Z", gxTv_SdtSistema_Sistema_impusercod_Z, false);
            AddObjectProperty("Sistema_ImpUserPesCod_Z", gxTv_SdtSistema_Sistema_impuserpescod_Z, false);
            AddObjectProperty("Sistema_ImpUserPesNom_Z", gxTv_SdtSistema_Sistema_impuserpesnom_Z, false);
            AddObjectProperty("Sistema_Responsavel_Z", gxTv_SdtSistema_Sistema_responsavel_Z, false);
            AddObjectProperty("Sistema_GpoObjCtrlCod_Z", gxTv_SdtSistema_Sistema_gpoobjctrlcod_Z, false);
            AddObjectProperty("Sistema_GpoObjCtrlRsp_Z", gxTv_SdtSistema_Sistema_gpoobjctrlrsp_Z, false);
            AddObjectProperty("SistemaVersao_Codigo_Z", gxTv_SdtSistema_Sistemaversao_codigo_Z, false);
            AddObjectProperty("SistemaVersao_Id_Z", gxTv_SdtSistema_Sistemaversao_id_Z, false);
            AddObjectProperty("Sistema_Ativo_Z", gxTv_SdtSistema_Sistema_ativo_Z, false);
            AddObjectProperty("Sistema_Repositorio_Z", gxTv_SdtSistema_Sistema_repositorio_Z, false);
            AddObjectProperty("Sistema_Ultimo_Z", gxTv_SdtSistema_Sistema_ultimo_Z, false);
            AddObjectProperty("Sistema_Descricao_N", gxTv_SdtSistema_Sistema_descricao_N, false);
            AddObjectProperty("Sistema_Coordenacao_N", gxTv_SdtSistema_Sistema_coordenacao_N, false);
            AddObjectProperty("Sistema_AreaTrabalhoDes_N", gxTv_SdtSistema_Sistema_areatrabalhodes_N, false);
            AddObjectProperty("AmbienteTecnologico_Codigo_N", gxTv_SdtSistema_Ambientetecnologico_codigo_N, false);
            AddObjectProperty("Metodologia_Codigo_N", gxTv_SdtSistema_Metodologia_codigo_N, false);
            AddObjectProperty("Sistema_Tipo_N", gxTv_SdtSistema_Sistema_tipo_N, false);
            AddObjectProperty("Sistema_Tecnica_N", gxTv_SdtSistema_Sistema_tecnica_N, false);
            AddObjectProperty("Sistema_FatorAjuste_N", gxTv_SdtSistema_Sistema_fatorajuste_N, false);
            AddObjectProperty("Sistema_Custo_N", gxTv_SdtSistema_Sistema_custo_N, false);
            AddObjectProperty("Sistema_Prazo_N", gxTv_SdtSistema_Sistema_prazo_N, false);
            AddObjectProperty("Sistema_Esforco_N", gxTv_SdtSistema_Sistema_esforco_N, false);
            AddObjectProperty("Sistema_ImpData_N", gxTv_SdtSistema_Sistema_impdata_N, false);
            AddObjectProperty("Sistema_ImpUserCod_N", gxTv_SdtSistema_Sistema_impusercod_N, false);
            AddObjectProperty("Sistema_ImpUserPesCod_N", gxTv_SdtSistema_Sistema_impuserpescod_N, false);
            AddObjectProperty("Sistema_ImpUserPesNom_N", gxTv_SdtSistema_Sistema_impuserpesnom_N, false);
            AddObjectProperty("Sistema_Responsavel_N", gxTv_SdtSistema_Sistema_responsavel_N, false);
            AddObjectProperty("Sistema_GpoObjCtrlCod_N", gxTv_SdtSistema_Sistema_gpoobjctrlcod_N, false);
            AddObjectProperty("SistemaVersao_Codigo_N", gxTv_SdtSistema_Sistemaversao_codigo_N, false);
            AddObjectProperty("Sistema_Repositorio_N", gxTv_SdtSistema_Sistema_repositorio_N, false);
         }
         return  ;
      }

      [  SoapElement( ElementName = "Sistema_Codigo" )]
      [  XmlElement( ElementName = "Sistema_Codigo"   )]
      public int gxTpr_Sistema_codigo
      {
         get {
            return gxTv_SdtSistema_Sistema_codigo ;
         }

         set {
            if ( gxTv_SdtSistema_Sistema_codigo != value )
            {
               gxTv_SdtSistema_Mode = "INS";
               this.gxTv_SdtSistema_Sistema_codigo_Z_SetNull( );
               this.gxTv_SdtSistema_Sistema_nome_Z_SetNull( );
               this.gxTv_SdtSistema_Sistema_codigo_Z_SetNull( );
               this.gxTv_SdtSistema_Sistema_nome_Z_SetNull( );
               this.gxTv_SdtSistema_Sistema_sigla_Z_SetNull( );
               this.gxTv_SdtSistema_Sistema_coordenacao_Z_SetNull( );
               this.gxTv_SdtSistema_Sistema_areatrabalhocod_Z_SetNull( );
               this.gxTv_SdtSistema_Sistema_areatrabalhodes_Z_SetNull( );
               this.gxTv_SdtSistema_Ambientetecnologico_codigo_Z_SetNull( );
               this.gxTv_SdtSistema_Ambientetecnologico_descricao_Z_SetNull( );
               this.gxTv_SdtSistema_Metodologia_codigo_Z_SetNull( );
               this.gxTv_SdtSistema_Metodologia_descricao_Z_SetNull( );
               this.gxTv_SdtSistema_Sistema_pf_Z_SetNull( );
               this.gxTv_SdtSistema_Sistema_pfa_Z_SetNull( );
               this.gxTv_SdtSistema_Sistema_tipo_Z_SetNull( );
               this.gxTv_SdtSistema_Sistema_tiposigla_Z_SetNull( );
               this.gxTv_SdtSistema_Sistema_tecnica_Z_SetNull( );
               this.gxTv_SdtSistema_Sistema_fatorajuste_Z_SetNull( );
               this.gxTv_SdtSistema_Sistema_custo_Z_SetNull( );
               this.gxTv_SdtSistema_Sistema_prazo_Z_SetNull( );
               this.gxTv_SdtSistema_Sistema_esforco_Z_SetNull( );
               this.gxTv_SdtSistema_Sistema_impdata_Z_SetNull( );
               this.gxTv_SdtSistema_Sistema_impusercod_Z_SetNull( );
               this.gxTv_SdtSistema_Sistema_impuserpescod_Z_SetNull( );
               this.gxTv_SdtSistema_Sistema_impuserpesnom_Z_SetNull( );
               this.gxTv_SdtSistema_Sistema_responsavel_Z_SetNull( );
               this.gxTv_SdtSistema_Sistema_gpoobjctrlcod_Z_SetNull( );
               this.gxTv_SdtSistema_Sistema_gpoobjctrlrsp_Z_SetNull( );
               this.gxTv_SdtSistema_Sistemaversao_codigo_Z_SetNull( );
               this.gxTv_SdtSistema_Sistemaversao_id_Z_SetNull( );
               this.gxTv_SdtSistema_Sistema_ativo_Z_SetNull( );
               this.gxTv_SdtSistema_Sistema_repositorio_Z_SetNull( );
               this.gxTv_SdtSistema_Sistema_ultimo_Z_SetNull( );
               if ( gxTv_SdtSistema_Tecnologia != null )
               {
                  GxSilentTrnGridCollection collectionTecnologia = gxTv_SdtSistema_Tecnologia ;
                  SdtSistema_Tecnologia currItemTecnologia ;
                  short idx = 1 ;
                  while ( idx <= collectionTecnologia.Count )
                  {
                     currItemTecnologia = ((SdtSistema_Tecnologia)collectionTecnologia.Item(idx));
                     currItemTecnologia.gxTpr_Mode = "INS";
                     currItemTecnologia.gxTpr_Modified = 1;
                     idx = (short)(idx+1);
                  }
               }
            }
            gxTv_SdtSistema_Sistema_codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Sistema_Nome" )]
      [  XmlElement( ElementName = "Sistema_Nome"   )]
      public String gxTpr_Sistema_nome
      {
         get {
            return gxTv_SdtSistema_Sistema_nome ;
         }

         set {
            gxTv_SdtSistema_Sistema_nome = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Sistema_Descricao" )]
      [  XmlElement( ElementName = "Sistema_Descricao"   )]
      public String gxTpr_Sistema_descricao
      {
         get {
            return gxTv_SdtSistema_Sistema_descricao ;
         }

         set {
            gxTv_SdtSistema_Sistema_descricao_N = 0;
            gxTv_SdtSistema_Sistema_descricao = (String)(value);
         }

      }

      public void gxTv_SdtSistema_Sistema_descricao_SetNull( )
      {
         gxTv_SdtSistema_Sistema_descricao_N = 1;
         gxTv_SdtSistema_Sistema_descricao = "";
         return  ;
      }

      public bool gxTv_SdtSistema_Sistema_descricao_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Sistema_Sigla" )]
      [  XmlElement( ElementName = "Sistema_Sigla"   )]
      public String gxTpr_Sistema_sigla
      {
         get {
            return gxTv_SdtSistema_Sistema_sigla ;
         }

         set {
            gxTv_SdtSistema_Sistema_sigla = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Sistema_Coordenacao" )]
      [  XmlElement( ElementName = "Sistema_Coordenacao"   )]
      public String gxTpr_Sistema_coordenacao
      {
         get {
            return gxTv_SdtSistema_Sistema_coordenacao ;
         }

         set {
            gxTv_SdtSistema_Sistema_coordenacao_N = 0;
            gxTv_SdtSistema_Sistema_coordenacao = (String)(value);
         }

      }

      public void gxTv_SdtSistema_Sistema_coordenacao_SetNull( )
      {
         gxTv_SdtSistema_Sistema_coordenacao_N = 1;
         gxTv_SdtSistema_Sistema_coordenacao = "";
         return  ;
      }

      public bool gxTv_SdtSistema_Sistema_coordenacao_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Sistema_AreaTrabalhoCod" )]
      [  XmlElement( ElementName = "Sistema_AreaTrabalhoCod"   )]
      public int gxTpr_Sistema_areatrabalhocod
      {
         get {
            return gxTv_SdtSistema_Sistema_areatrabalhocod ;
         }

         set {
            gxTv_SdtSistema_Sistema_areatrabalhocod = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Sistema_AreaTrabalhoDes" )]
      [  XmlElement( ElementName = "Sistema_AreaTrabalhoDes"   )]
      public String gxTpr_Sistema_areatrabalhodes
      {
         get {
            return gxTv_SdtSistema_Sistema_areatrabalhodes ;
         }

         set {
            gxTv_SdtSistema_Sistema_areatrabalhodes_N = 0;
            gxTv_SdtSistema_Sistema_areatrabalhodes = (String)(value);
         }

      }

      public void gxTv_SdtSistema_Sistema_areatrabalhodes_SetNull( )
      {
         gxTv_SdtSistema_Sistema_areatrabalhodes_N = 1;
         gxTv_SdtSistema_Sistema_areatrabalhodes = "";
         return  ;
      }

      public bool gxTv_SdtSistema_Sistema_areatrabalhodes_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "AmbienteTecnologico_Codigo" )]
      [  XmlElement( ElementName = "AmbienteTecnologico_Codigo"   )]
      public int gxTpr_Ambientetecnologico_codigo
      {
         get {
            return gxTv_SdtSistema_Ambientetecnologico_codigo ;
         }

         set {
            gxTv_SdtSistema_Ambientetecnologico_codigo_N = 0;
            gxTv_SdtSistema_Ambientetecnologico_codigo = (int)(value);
         }

      }

      public void gxTv_SdtSistema_Ambientetecnologico_codigo_SetNull( )
      {
         gxTv_SdtSistema_Ambientetecnologico_codigo_N = 1;
         gxTv_SdtSistema_Ambientetecnologico_codigo = 0;
         return  ;
      }

      public bool gxTv_SdtSistema_Ambientetecnologico_codigo_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "AmbienteTecnologico_Descricao" )]
      [  XmlElement( ElementName = "AmbienteTecnologico_Descricao"   )]
      public String gxTpr_Ambientetecnologico_descricao
      {
         get {
            return gxTv_SdtSistema_Ambientetecnologico_descricao ;
         }

         set {
            gxTv_SdtSistema_Ambientetecnologico_descricao = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Metodologia_Codigo" )]
      [  XmlElement( ElementName = "Metodologia_Codigo"   )]
      public int gxTpr_Metodologia_codigo
      {
         get {
            return gxTv_SdtSistema_Metodologia_codigo ;
         }

         set {
            gxTv_SdtSistema_Metodologia_codigo_N = 0;
            gxTv_SdtSistema_Metodologia_codigo = (int)(value);
         }

      }

      public void gxTv_SdtSistema_Metodologia_codigo_SetNull( )
      {
         gxTv_SdtSistema_Metodologia_codigo_N = 1;
         gxTv_SdtSistema_Metodologia_codigo = 0;
         return  ;
      }

      public bool gxTv_SdtSistema_Metodologia_codigo_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Metodologia_Descricao" )]
      [  XmlElement( ElementName = "Metodologia_Descricao"   )]
      public String gxTpr_Metodologia_descricao
      {
         get {
            return gxTv_SdtSistema_Metodologia_descricao ;
         }

         set {
            gxTv_SdtSistema_Metodologia_descricao = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Sistema_PF" )]
      [  XmlElement( ElementName = "Sistema_PF"   )]
      public double gxTpr_Sistema_pf_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtSistema_Sistema_pf) ;
         }

         set {
            gxTv_SdtSistema_Sistema_pf = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Sistema_pf
      {
         get {
            return gxTv_SdtSistema_Sistema_pf ;
         }

         set {
            gxTv_SdtSistema_Sistema_pf = (decimal)(value);
         }

      }

      public void gxTv_SdtSistema_Sistema_pf_SetNull( )
      {
         gxTv_SdtSistema_Sistema_pf = 0;
         return  ;
      }

      public bool gxTv_SdtSistema_Sistema_pf_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Sistema_PFA" )]
      [  XmlElement( ElementName = "Sistema_PFA"   )]
      public double gxTpr_Sistema_pfa_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtSistema_Sistema_pfa) ;
         }

         set {
            gxTv_SdtSistema_Sistema_pfa = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Sistema_pfa
      {
         get {
            return gxTv_SdtSistema_Sistema_pfa ;
         }

         set {
            gxTv_SdtSistema_Sistema_pfa = (decimal)(value);
         }

      }

      public void gxTv_SdtSistema_Sistema_pfa_SetNull( )
      {
         gxTv_SdtSistema_Sistema_pfa = 0;
         return  ;
      }

      public bool gxTv_SdtSistema_Sistema_pfa_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Sistema_Tipo" )]
      [  XmlElement( ElementName = "Sistema_Tipo"   )]
      public String gxTpr_Sistema_tipo
      {
         get {
            return gxTv_SdtSistema_Sistema_tipo ;
         }

         set {
            gxTv_SdtSistema_Sistema_tipo_N = 0;
            gxTv_SdtSistema_Sistema_tipo = (String)(value);
         }

      }

      public void gxTv_SdtSistema_Sistema_tipo_SetNull( )
      {
         gxTv_SdtSistema_Sistema_tipo_N = 1;
         gxTv_SdtSistema_Sistema_tipo = "";
         return  ;
      }

      public bool gxTv_SdtSistema_Sistema_tipo_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Sistema_TipoSigla" )]
      [  XmlElement( ElementName = "Sistema_TipoSigla"   )]
      public String gxTpr_Sistema_tiposigla
      {
         get {
            return gxTv_SdtSistema_Sistema_tiposigla ;
         }

         set {
            gxTv_SdtSistema_Sistema_tiposigla = (String)(value);
         }

      }

      public void gxTv_SdtSistema_Sistema_tiposigla_SetNull( )
      {
         gxTv_SdtSistema_Sistema_tiposigla = "";
         return  ;
      }

      public bool gxTv_SdtSistema_Sistema_tiposigla_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Sistema_Tecnica" )]
      [  XmlElement( ElementName = "Sistema_Tecnica"   )]
      public String gxTpr_Sistema_tecnica
      {
         get {
            return gxTv_SdtSistema_Sistema_tecnica ;
         }

         set {
            gxTv_SdtSistema_Sistema_tecnica_N = 0;
            gxTv_SdtSistema_Sistema_tecnica = (String)(value);
         }

      }

      public void gxTv_SdtSistema_Sistema_tecnica_SetNull( )
      {
         gxTv_SdtSistema_Sistema_tecnica_N = 1;
         gxTv_SdtSistema_Sistema_tecnica = "";
         return  ;
      }

      public bool gxTv_SdtSistema_Sistema_tecnica_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Sistema_FatorAjuste" )]
      [  XmlElement( ElementName = "Sistema_FatorAjuste"   )]
      public double gxTpr_Sistema_fatorajuste_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtSistema_Sistema_fatorajuste) ;
         }

         set {
            gxTv_SdtSistema_Sistema_fatorajuste_N = 0;
            gxTv_SdtSistema_Sistema_fatorajuste = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Sistema_fatorajuste
      {
         get {
            return gxTv_SdtSistema_Sistema_fatorajuste ;
         }

         set {
            gxTv_SdtSistema_Sistema_fatorajuste_N = 0;
            gxTv_SdtSistema_Sistema_fatorajuste = (decimal)(value);
         }

      }

      public void gxTv_SdtSistema_Sistema_fatorajuste_SetNull( )
      {
         gxTv_SdtSistema_Sistema_fatorajuste_N = 1;
         gxTv_SdtSistema_Sistema_fatorajuste = 0;
         return  ;
      }

      public bool gxTv_SdtSistema_Sistema_fatorajuste_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Sistema_Custo" )]
      [  XmlElement( ElementName = "Sistema_Custo"   )]
      public double gxTpr_Sistema_custo_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtSistema_Sistema_custo) ;
         }

         set {
            gxTv_SdtSistema_Sistema_custo_N = 0;
            gxTv_SdtSistema_Sistema_custo = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Sistema_custo
      {
         get {
            return gxTv_SdtSistema_Sistema_custo ;
         }

         set {
            gxTv_SdtSistema_Sistema_custo_N = 0;
            gxTv_SdtSistema_Sistema_custo = (decimal)(value);
         }

      }

      public void gxTv_SdtSistema_Sistema_custo_SetNull( )
      {
         gxTv_SdtSistema_Sistema_custo_N = 1;
         gxTv_SdtSistema_Sistema_custo = 0;
         return  ;
      }

      public bool gxTv_SdtSistema_Sistema_custo_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Sistema_Prazo" )]
      [  XmlElement( ElementName = "Sistema_Prazo"   )]
      public short gxTpr_Sistema_prazo
      {
         get {
            return gxTv_SdtSistema_Sistema_prazo ;
         }

         set {
            gxTv_SdtSistema_Sistema_prazo_N = 0;
            gxTv_SdtSistema_Sistema_prazo = (short)(value);
         }

      }

      public void gxTv_SdtSistema_Sistema_prazo_SetNull( )
      {
         gxTv_SdtSistema_Sistema_prazo_N = 1;
         gxTv_SdtSistema_Sistema_prazo = 0;
         return  ;
      }

      public bool gxTv_SdtSistema_Sistema_prazo_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Sistema_Esforco" )]
      [  XmlElement( ElementName = "Sistema_Esforco"   )]
      public short gxTpr_Sistema_esforco
      {
         get {
            return gxTv_SdtSistema_Sistema_esforco ;
         }

         set {
            gxTv_SdtSistema_Sistema_esforco_N = 0;
            gxTv_SdtSistema_Sistema_esforco = (short)(value);
         }

      }

      public void gxTv_SdtSistema_Sistema_esforco_SetNull( )
      {
         gxTv_SdtSistema_Sistema_esforco_N = 1;
         gxTv_SdtSistema_Sistema_esforco = 0;
         return  ;
      }

      public bool gxTv_SdtSistema_Sistema_esforco_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Sistema_ImpData" )]
      [  XmlElement( ElementName = "Sistema_ImpData"  , IsNullable=true )]
      public string gxTpr_Sistema_impdata_Nullable
      {
         get {
            if ( gxTv_SdtSistema_Sistema_impdata == DateTime.MinValue)
               return null;
            return new GxDatetimeString(gxTv_SdtSistema_Sistema_impdata).value ;
         }

         set {
            gxTv_SdtSistema_Sistema_impdata_N = 0;
            if (value == null || value == GxDatetimeString.NullValue )
               gxTv_SdtSistema_Sistema_impdata = DateTime.MinValue;
            else
               gxTv_SdtSistema_Sistema_impdata = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Sistema_impdata
      {
         get {
            return gxTv_SdtSistema_Sistema_impdata ;
         }

         set {
            gxTv_SdtSistema_Sistema_impdata_N = 0;
            gxTv_SdtSistema_Sistema_impdata = (DateTime)(value);
         }

      }

      public void gxTv_SdtSistema_Sistema_impdata_SetNull( )
      {
         gxTv_SdtSistema_Sistema_impdata_N = 1;
         gxTv_SdtSistema_Sistema_impdata = (DateTime)(DateTime.MinValue);
         return  ;
      }

      public bool gxTv_SdtSistema_Sistema_impdata_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Sistema_ImpUserCod" )]
      [  XmlElement( ElementName = "Sistema_ImpUserCod"   )]
      public int gxTpr_Sistema_impusercod
      {
         get {
            return gxTv_SdtSistema_Sistema_impusercod ;
         }

         set {
            gxTv_SdtSistema_Sistema_impusercod_N = 0;
            gxTv_SdtSistema_Sistema_impusercod = (int)(value);
         }

      }

      public void gxTv_SdtSistema_Sistema_impusercod_SetNull( )
      {
         gxTv_SdtSistema_Sistema_impusercod_N = 1;
         gxTv_SdtSistema_Sistema_impusercod = 0;
         return  ;
      }

      public bool gxTv_SdtSistema_Sistema_impusercod_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Sistema_ImpUserPesCod" )]
      [  XmlElement( ElementName = "Sistema_ImpUserPesCod"   )]
      public int gxTpr_Sistema_impuserpescod
      {
         get {
            return gxTv_SdtSistema_Sistema_impuserpescod ;
         }

         set {
            gxTv_SdtSistema_Sistema_impuserpescod_N = 0;
            gxTv_SdtSistema_Sistema_impuserpescod = (int)(value);
         }

      }

      public void gxTv_SdtSistema_Sistema_impuserpescod_SetNull( )
      {
         gxTv_SdtSistema_Sistema_impuserpescod_N = 1;
         gxTv_SdtSistema_Sistema_impuserpescod = 0;
         return  ;
      }

      public bool gxTv_SdtSistema_Sistema_impuserpescod_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Sistema_ImpUserPesNom" )]
      [  XmlElement( ElementName = "Sistema_ImpUserPesNom"   )]
      public String gxTpr_Sistema_impuserpesnom
      {
         get {
            return gxTv_SdtSistema_Sistema_impuserpesnom ;
         }

         set {
            gxTv_SdtSistema_Sistema_impuserpesnom_N = 0;
            gxTv_SdtSistema_Sistema_impuserpesnom = (String)(value);
         }

      }

      public void gxTv_SdtSistema_Sistema_impuserpesnom_SetNull( )
      {
         gxTv_SdtSistema_Sistema_impuserpesnom_N = 1;
         gxTv_SdtSistema_Sistema_impuserpesnom = "";
         return  ;
      }

      public bool gxTv_SdtSistema_Sistema_impuserpesnom_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Sistema_Responsavel" )]
      [  XmlElement( ElementName = "Sistema_Responsavel"   )]
      public int gxTpr_Sistema_responsavel
      {
         get {
            return gxTv_SdtSistema_Sistema_responsavel ;
         }

         set {
            gxTv_SdtSistema_Sistema_responsavel_N = 0;
            gxTv_SdtSistema_Sistema_responsavel = (int)(value);
         }

      }

      public void gxTv_SdtSistema_Sistema_responsavel_SetNull( )
      {
         gxTv_SdtSistema_Sistema_responsavel_N = 1;
         gxTv_SdtSistema_Sistema_responsavel = 0;
         return  ;
      }

      public bool gxTv_SdtSistema_Sistema_responsavel_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Sistema_GpoObjCtrlCod" )]
      [  XmlElement( ElementName = "Sistema_GpoObjCtrlCod"   )]
      public int gxTpr_Sistema_gpoobjctrlcod
      {
         get {
            return gxTv_SdtSistema_Sistema_gpoobjctrlcod ;
         }

         set {
            gxTv_SdtSistema_Sistema_gpoobjctrlcod_N = 0;
            gxTv_SdtSistema_Sistema_gpoobjctrlcod = (int)(value);
         }

      }

      public void gxTv_SdtSistema_Sistema_gpoobjctrlcod_SetNull( )
      {
         gxTv_SdtSistema_Sistema_gpoobjctrlcod_N = 1;
         gxTv_SdtSistema_Sistema_gpoobjctrlcod = 0;
         return  ;
      }

      public bool gxTv_SdtSistema_Sistema_gpoobjctrlcod_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Sistema_GpoObjCtrlRsp" )]
      [  XmlElement( ElementName = "Sistema_GpoObjCtrlRsp"   )]
      public int gxTpr_Sistema_gpoobjctrlrsp
      {
         get {
            return gxTv_SdtSistema_Sistema_gpoobjctrlrsp ;
         }

         set {
            gxTv_SdtSistema_Sistema_gpoobjctrlrsp = (int)(value);
         }

      }

      public void gxTv_SdtSistema_Sistema_gpoobjctrlrsp_SetNull( )
      {
         gxTv_SdtSistema_Sistema_gpoobjctrlrsp = 0;
         return  ;
      }

      public bool gxTv_SdtSistema_Sistema_gpoobjctrlrsp_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "SistemaVersao_Codigo" )]
      [  XmlElement( ElementName = "SistemaVersao_Codigo"   )]
      public int gxTpr_Sistemaversao_codigo
      {
         get {
            return gxTv_SdtSistema_Sistemaversao_codigo ;
         }

         set {
            gxTv_SdtSistema_Sistemaversao_codigo_N = 0;
            gxTv_SdtSistema_Sistemaversao_codigo = (int)(value);
         }

      }

      public void gxTv_SdtSistema_Sistemaversao_codigo_SetNull( )
      {
         gxTv_SdtSistema_Sistemaversao_codigo_N = 1;
         gxTv_SdtSistema_Sistemaversao_codigo = 0;
         return  ;
      }

      public bool gxTv_SdtSistema_Sistemaversao_codigo_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "SistemaVersao_Id" )]
      [  XmlElement( ElementName = "SistemaVersao_Id"   )]
      public String gxTpr_Sistemaversao_id
      {
         get {
            return gxTv_SdtSistema_Sistemaversao_id ;
         }

         set {
            gxTv_SdtSistema_Sistemaversao_id = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Sistema_Ativo" )]
      [  XmlElement( ElementName = "Sistema_Ativo"   )]
      public bool gxTpr_Sistema_ativo
      {
         get {
            return gxTv_SdtSistema_Sistema_ativo ;
         }

         set {
            gxTv_SdtSistema_Sistema_ativo = value;
         }

      }

      [  SoapElement( ElementName = "Sistema_Repositorio" )]
      [  XmlElement( ElementName = "Sistema_Repositorio"   )]
      public String gxTpr_Sistema_repositorio
      {
         get {
            return gxTv_SdtSistema_Sistema_repositorio ;
         }

         set {
            gxTv_SdtSistema_Sistema_repositorio_N = 0;
            gxTv_SdtSistema_Sistema_repositorio = (String)(value);
         }

      }

      public void gxTv_SdtSistema_Sistema_repositorio_SetNull( )
      {
         gxTv_SdtSistema_Sistema_repositorio_N = 1;
         gxTv_SdtSistema_Sistema_repositorio = "";
         return  ;
      }

      public bool gxTv_SdtSistema_Sistema_repositorio_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Sistema_Ultimo" )]
      [  XmlElement( ElementName = "Sistema_Ultimo"   )]
      public short gxTpr_Sistema_ultimo
      {
         get {
            return gxTv_SdtSistema_Sistema_ultimo ;
         }

         set {
            gxTv_SdtSistema_Sistema_ultimo = (short)(value);
         }

      }

      public class gxTv_SdtSistema_Tecnologia_SdtSistema_Tecnologia_80compatibility:SdtSistema_Tecnologia {}
      [  SoapElement( ElementName = "Tecnologia" )]
      [  XmlArray( ElementName = "Tecnologia"  )]
      [  XmlArrayItemAttribute( Type= typeof( SdtSistema_Tecnologia ), ElementName= "Sistema.Tecnologia"  , IsNullable=false)]
      public GxSilentTrnGridCollection gxTpr_Tecnologia_GxSilentTrnGridCollection
      {
         get {
            if ( gxTv_SdtSistema_Tecnologia == null )
            {
               gxTv_SdtSistema_Tecnologia = new GxSilentTrnGridCollection( context, "Sistema.Tecnologia", "GxEv3Up14_MeetrikaVs3", "SdtSistema_Tecnologia", "GeneXus.Programs");
            }
            return (GxSilentTrnGridCollection)gxTv_SdtSistema_Tecnologia ;
         }

         set {
            if ( gxTv_SdtSistema_Tecnologia == null )
            {
               gxTv_SdtSistema_Tecnologia = new GxSilentTrnGridCollection( context, "Sistema.Tecnologia", "GxEv3Up14_MeetrikaVs3", "SdtSistema_Tecnologia", "GeneXus.Programs");
            }
            gxTv_SdtSistema_Tecnologia = (GxSilentTrnGridCollection) value;
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public GxSilentTrnGridCollection gxTpr_Tecnologia
      {
         get {
            if ( gxTv_SdtSistema_Tecnologia == null )
            {
               gxTv_SdtSistema_Tecnologia = new GxSilentTrnGridCollection( context, "Sistema.Tecnologia", "GxEv3Up14_MeetrikaVs3", "SdtSistema_Tecnologia", "GeneXus.Programs");
            }
            return gxTv_SdtSistema_Tecnologia ;
         }

         set {
            gxTv_SdtSistema_Tecnologia = value;
         }

      }

      public void gxTv_SdtSistema_Tecnologia_SetNull( )
      {
         gxTv_SdtSistema_Tecnologia = null;
         return  ;
      }

      public bool gxTv_SdtSistema_Tecnologia_IsNull( )
      {
         if ( gxTv_SdtSistema_Tecnologia == null )
         {
            return true ;
         }
         return false ;
      }

      [  SoapElement( ElementName = "Mode" )]
      [  XmlElement( ElementName = "Mode"   )]
      public String gxTpr_Mode
      {
         get {
            return gxTv_SdtSistema_Mode ;
         }

         set {
            gxTv_SdtSistema_Mode = (String)(value);
         }

      }

      public void gxTv_SdtSistema_Mode_SetNull( )
      {
         gxTv_SdtSistema_Mode = "";
         return  ;
      }

      public bool gxTv_SdtSistema_Mode_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Initialized" )]
      [  XmlElement( ElementName = "Initialized"   )]
      public short gxTpr_Initialized
      {
         get {
            return gxTv_SdtSistema_Initialized ;
         }

         set {
            gxTv_SdtSistema_Initialized = (short)(value);
         }

      }

      public void gxTv_SdtSistema_Initialized_SetNull( )
      {
         gxTv_SdtSistema_Initialized = 0;
         return  ;
      }

      public bool gxTv_SdtSistema_Initialized_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Sistema_Codigo_Z" )]
      [  XmlElement( ElementName = "Sistema_Codigo_Z"   )]
      public int gxTpr_Sistema_codigo_Z
      {
         get {
            return gxTv_SdtSistema_Sistema_codigo_Z ;
         }

         set {
            gxTv_SdtSistema_Sistema_codigo_Z = (int)(value);
         }

      }

      public void gxTv_SdtSistema_Sistema_codigo_Z_SetNull( )
      {
         gxTv_SdtSistema_Sistema_codigo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtSistema_Sistema_codigo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Sistema_Nome_Z" )]
      [  XmlElement( ElementName = "Sistema_Nome_Z"   )]
      public String gxTpr_Sistema_nome_Z
      {
         get {
            return gxTv_SdtSistema_Sistema_nome_Z ;
         }

         set {
            gxTv_SdtSistema_Sistema_nome_Z = (String)(value);
         }

      }

      public void gxTv_SdtSistema_Sistema_nome_Z_SetNull( )
      {
         gxTv_SdtSistema_Sistema_nome_Z = "";
         return  ;
      }

      public bool gxTv_SdtSistema_Sistema_nome_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Sistema_Sigla_Z" )]
      [  XmlElement( ElementName = "Sistema_Sigla_Z"   )]
      public String gxTpr_Sistema_sigla_Z
      {
         get {
            return gxTv_SdtSistema_Sistema_sigla_Z ;
         }

         set {
            gxTv_SdtSistema_Sistema_sigla_Z = (String)(value);
         }

      }

      public void gxTv_SdtSistema_Sistema_sigla_Z_SetNull( )
      {
         gxTv_SdtSistema_Sistema_sigla_Z = "";
         return  ;
      }

      public bool gxTv_SdtSistema_Sistema_sigla_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Sistema_Coordenacao_Z" )]
      [  XmlElement( ElementName = "Sistema_Coordenacao_Z"   )]
      public String gxTpr_Sistema_coordenacao_Z
      {
         get {
            return gxTv_SdtSistema_Sistema_coordenacao_Z ;
         }

         set {
            gxTv_SdtSistema_Sistema_coordenacao_Z = (String)(value);
         }

      }

      public void gxTv_SdtSistema_Sistema_coordenacao_Z_SetNull( )
      {
         gxTv_SdtSistema_Sistema_coordenacao_Z = "";
         return  ;
      }

      public bool gxTv_SdtSistema_Sistema_coordenacao_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Sistema_AreaTrabalhoCod_Z" )]
      [  XmlElement( ElementName = "Sistema_AreaTrabalhoCod_Z"   )]
      public int gxTpr_Sistema_areatrabalhocod_Z
      {
         get {
            return gxTv_SdtSistema_Sistema_areatrabalhocod_Z ;
         }

         set {
            gxTv_SdtSistema_Sistema_areatrabalhocod_Z = (int)(value);
         }

      }

      public void gxTv_SdtSistema_Sistema_areatrabalhocod_Z_SetNull( )
      {
         gxTv_SdtSistema_Sistema_areatrabalhocod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtSistema_Sistema_areatrabalhocod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Sistema_AreaTrabalhoDes_Z" )]
      [  XmlElement( ElementName = "Sistema_AreaTrabalhoDes_Z"   )]
      public String gxTpr_Sistema_areatrabalhodes_Z
      {
         get {
            return gxTv_SdtSistema_Sistema_areatrabalhodes_Z ;
         }

         set {
            gxTv_SdtSistema_Sistema_areatrabalhodes_Z = (String)(value);
         }

      }

      public void gxTv_SdtSistema_Sistema_areatrabalhodes_Z_SetNull( )
      {
         gxTv_SdtSistema_Sistema_areatrabalhodes_Z = "";
         return  ;
      }

      public bool gxTv_SdtSistema_Sistema_areatrabalhodes_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "AmbienteTecnologico_Codigo_Z" )]
      [  XmlElement( ElementName = "AmbienteTecnologico_Codigo_Z"   )]
      public int gxTpr_Ambientetecnologico_codigo_Z
      {
         get {
            return gxTv_SdtSistema_Ambientetecnologico_codigo_Z ;
         }

         set {
            gxTv_SdtSistema_Ambientetecnologico_codigo_Z = (int)(value);
         }

      }

      public void gxTv_SdtSistema_Ambientetecnologico_codigo_Z_SetNull( )
      {
         gxTv_SdtSistema_Ambientetecnologico_codigo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtSistema_Ambientetecnologico_codigo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "AmbienteTecnologico_Descricao_Z" )]
      [  XmlElement( ElementName = "AmbienteTecnologico_Descricao_Z"   )]
      public String gxTpr_Ambientetecnologico_descricao_Z
      {
         get {
            return gxTv_SdtSistema_Ambientetecnologico_descricao_Z ;
         }

         set {
            gxTv_SdtSistema_Ambientetecnologico_descricao_Z = (String)(value);
         }

      }

      public void gxTv_SdtSistema_Ambientetecnologico_descricao_Z_SetNull( )
      {
         gxTv_SdtSistema_Ambientetecnologico_descricao_Z = "";
         return  ;
      }

      public bool gxTv_SdtSistema_Ambientetecnologico_descricao_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Metodologia_Codigo_Z" )]
      [  XmlElement( ElementName = "Metodologia_Codigo_Z"   )]
      public int gxTpr_Metodologia_codigo_Z
      {
         get {
            return gxTv_SdtSistema_Metodologia_codigo_Z ;
         }

         set {
            gxTv_SdtSistema_Metodologia_codigo_Z = (int)(value);
         }

      }

      public void gxTv_SdtSistema_Metodologia_codigo_Z_SetNull( )
      {
         gxTv_SdtSistema_Metodologia_codigo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtSistema_Metodologia_codigo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Metodologia_Descricao_Z" )]
      [  XmlElement( ElementName = "Metodologia_Descricao_Z"   )]
      public String gxTpr_Metodologia_descricao_Z
      {
         get {
            return gxTv_SdtSistema_Metodologia_descricao_Z ;
         }

         set {
            gxTv_SdtSistema_Metodologia_descricao_Z = (String)(value);
         }

      }

      public void gxTv_SdtSistema_Metodologia_descricao_Z_SetNull( )
      {
         gxTv_SdtSistema_Metodologia_descricao_Z = "";
         return  ;
      }

      public bool gxTv_SdtSistema_Metodologia_descricao_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Sistema_PF_Z" )]
      [  XmlElement( ElementName = "Sistema_PF_Z"   )]
      public double gxTpr_Sistema_pf_Z_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtSistema_Sistema_pf_Z) ;
         }

         set {
            gxTv_SdtSistema_Sistema_pf_Z = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Sistema_pf_Z
      {
         get {
            return gxTv_SdtSistema_Sistema_pf_Z ;
         }

         set {
            gxTv_SdtSistema_Sistema_pf_Z = (decimal)(value);
         }

      }

      public void gxTv_SdtSistema_Sistema_pf_Z_SetNull( )
      {
         gxTv_SdtSistema_Sistema_pf_Z = 0;
         return  ;
      }

      public bool gxTv_SdtSistema_Sistema_pf_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Sistema_PFA_Z" )]
      [  XmlElement( ElementName = "Sistema_PFA_Z"   )]
      public double gxTpr_Sistema_pfa_Z_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtSistema_Sistema_pfa_Z) ;
         }

         set {
            gxTv_SdtSistema_Sistema_pfa_Z = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Sistema_pfa_Z
      {
         get {
            return gxTv_SdtSistema_Sistema_pfa_Z ;
         }

         set {
            gxTv_SdtSistema_Sistema_pfa_Z = (decimal)(value);
         }

      }

      public void gxTv_SdtSistema_Sistema_pfa_Z_SetNull( )
      {
         gxTv_SdtSistema_Sistema_pfa_Z = 0;
         return  ;
      }

      public bool gxTv_SdtSistema_Sistema_pfa_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Sistema_Tipo_Z" )]
      [  XmlElement( ElementName = "Sistema_Tipo_Z"   )]
      public String gxTpr_Sistema_tipo_Z
      {
         get {
            return gxTv_SdtSistema_Sistema_tipo_Z ;
         }

         set {
            gxTv_SdtSistema_Sistema_tipo_Z = (String)(value);
         }

      }

      public void gxTv_SdtSistema_Sistema_tipo_Z_SetNull( )
      {
         gxTv_SdtSistema_Sistema_tipo_Z = "";
         return  ;
      }

      public bool gxTv_SdtSistema_Sistema_tipo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Sistema_TipoSigla_Z" )]
      [  XmlElement( ElementName = "Sistema_TipoSigla_Z"   )]
      public String gxTpr_Sistema_tiposigla_Z
      {
         get {
            return gxTv_SdtSistema_Sistema_tiposigla_Z ;
         }

         set {
            gxTv_SdtSistema_Sistema_tiposigla_Z = (String)(value);
         }

      }

      public void gxTv_SdtSistema_Sistema_tiposigla_Z_SetNull( )
      {
         gxTv_SdtSistema_Sistema_tiposigla_Z = "";
         return  ;
      }

      public bool gxTv_SdtSistema_Sistema_tiposigla_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Sistema_Tecnica_Z" )]
      [  XmlElement( ElementName = "Sistema_Tecnica_Z"   )]
      public String gxTpr_Sistema_tecnica_Z
      {
         get {
            return gxTv_SdtSistema_Sistema_tecnica_Z ;
         }

         set {
            gxTv_SdtSistema_Sistema_tecnica_Z = (String)(value);
         }

      }

      public void gxTv_SdtSistema_Sistema_tecnica_Z_SetNull( )
      {
         gxTv_SdtSistema_Sistema_tecnica_Z = "";
         return  ;
      }

      public bool gxTv_SdtSistema_Sistema_tecnica_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Sistema_FatorAjuste_Z" )]
      [  XmlElement( ElementName = "Sistema_FatorAjuste_Z"   )]
      public double gxTpr_Sistema_fatorajuste_Z_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtSistema_Sistema_fatorajuste_Z) ;
         }

         set {
            gxTv_SdtSistema_Sistema_fatorajuste_Z = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Sistema_fatorajuste_Z
      {
         get {
            return gxTv_SdtSistema_Sistema_fatorajuste_Z ;
         }

         set {
            gxTv_SdtSistema_Sistema_fatorajuste_Z = (decimal)(value);
         }

      }

      public void gxTv_SdtSistema_Sistema_fatorajuste_Z_SetNull( )
      {
         gxTv_SdtSistema_Sistema_fatorajuste_Z = 0;
         return  ;
      }

      public bool gxTv_SdtSistema_Sistema_fatorajuste_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Sistema_Custo_Z" )]
      [  XmlElement( ElementName = "Sistema_Custo_Z"   )]
      public double gxTpr_Sistema_custo_Z_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtSistema_Sistema_custo_Z) ;
         }

         set {
            gxTv_SdtSistema_Sistema_custo_Z = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Sistema_custo_Z
      {
         get {
            return gxTv_SdtSistema_Sistema_custo_Z ;
         }

         set {
            gxTv_SdtSistema_Sistema_custo_Z = (decimal)(value);
         }

      }

      public void gxTv_SdtSistema_Sistema_custo_Z_SetNull( )
      {
         gxTv_SdtSistema_Sistema_custo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtSistema_Sistema_custo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Sistema_Prazo_Z" )]
      [  XmlElement( ElementName = "Sistema_Prazo_Z"   )]
      public short gxTpr_Sistema_prazo_Z
      {
         get {
            return gxTv_SdtSistema_Sistema_prazo_Z ;
         }

         set {
            gxTv_SdtSistema_Sistema_prazo_Z = (short)(value);
         }

      }

      public void gxTv_SdtSistema_Sistema_prazo_Z_SetNull( )
      {
         gxTv_SdtSistema_Sistema_prazo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtSistema_Sistema_prazo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Sistema_Esforco_Z" )]
      [  XmlElement( ElementName = "Sistema_Esforco_Z"   )]
      public short gxTpr_Sistema_esforco_Z
      {
         get {
            return gxTv_SdtSistema_Sistema_esforco_Z ;
         }

         set {
            gxTv_SdtSistema_Sistema_esforco_Z = (short)(value);
         }

      }

      public void gxTv_SdtSistema_Sistema_esforco_Z_SetNull( )
      {
         gxTv_SdtSistema_Sistema_esforco_Z = 0;
         return  ;
      }

      public bool gxTv_SdtSistema_Sistema_esforco_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Sistema_ImpData_Z" )]
      [  XmlElement( ElementName = "Sistema_ImpData_Z"  , IsNullable=true )]
      public string gxTpr_Sistema_impdata_Z_Nullable
      {
         get {
            if ( gxTv_SdtSistema_Sistema_impdata_Z == DateTime.MinValue)
               return null;
            return new GxDatetimeString(gxTv_SdtSistema_Sistema_impdata_Z).value ;
         }

         set {
            if (value == null || value == GxDatetimeString.NullValue )
               gxTv_SdtSistema_Sistema_impdata_Z = DateTime.MinValue;
            else
               gxTv_SdtSistema_Sistema_impdata_Z = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Sistema_impdata_Z
      {
         get {
            return gxTv_SdtSistema_Sistema_impdata_Z ;
         }

         set {
            gxTv_SdtSistema_Sistema_impdata_Z = (DateTime)(value);
         }

      }

      public void gxTv_SdtSistema_Sistema_impdata_Z_SetNull( )
      {
         gxTv_SdtSistema_Sistema_impdata_Z = (DateTime)(DateTime.MinValue);
         return  ;
      }

      public bool gxTv_SdtSistema_Sistema_impdata_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Sistema_ImpUserCod_Z" )]
      [  XmlElement( ElementName = "Sistema_ImpUserCod_Z"   )]
      public int gxTpr_Sistema_impusercod_Z
      {
         get {
            return gxTv_SdtSistema_Sistema_impusercod_Z ;
         }

         set {
            gxTv_SdtSistema_Sistema_impusercod_Z = (int)(value);
         }

      }

      public void gxTv_SdtSistema_Sistema_impusercod_Z_SetNull( )
      {
         gxTv_SdtSistema_Sistema_impusercod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtSistema_Sistema_impusercod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Sistema_ImpUserPesCod_Z" )]
      [  XmlElement( ElementName = "Sistema_ImpUserPesCod_Z"   )]
      public int gxTpr_Sistema_impuserpescod_Z
      {
         get {
            return gxTv_SdtSistema_Sistema_impuserpescod_Z ;
         }

         set {
            gxTv_SdtSistema_Sistema_impuserpescod_Z = (int)(value);
         }

      }

      public void gxTv_SdtSistema_Sistema_impuserpescod_Z_SetNull( )
      {
         gxTv_SdtSistema_Sistema_impuserpescod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtSistema_Sistema_impuserpescod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Sistema_ImpUserPesNom_Z" )]
      [  XmlElement( ElementName = "Sistema_ImpUserPesNom_Z"   )]
      public String gxTpr_Sistema_impuserpesnom_Z
      {
         get {
            return gxTv_SdtSistema_Sistema_impuserpesnom_Z ;
         }

         set {
            gxTv_SdtSistema_Sistema_impuserpesnom_Z = (String)(value);
         }

      }

      public void gxTv_SdtSistema_Sistema_impuserpesnom_Z_SetNull( )
      {
         gxTv_SdtSistema_Sistema_impuserpesnom_Z = "";
         return  ;
      }

      public bool gxTv_SdtSistema_Sistema_impuserpesnom_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Sistema_Responsavel_Z" )]
      [  XmlElement( ElementName = "Sistema_Responsavel_Z"   )]
      public int gxTpr_Sistema_responsavel_Z
      {
         get {
            return gxTv_SdtSistema_Sistema_responsavel_Z ;
         }

         set {
            gxTv_SdtSistema_Sistema_responsavel_Z = (int)(value);
         }

      }

      public void gxTv_SdtSistema_Sistema_responsavel_Z_SetNull( )
      {
         gxTv_SdtSistema_Sistema_responsavel_Z = 0;
         return  ;
      }

      public bool gxTv_SdtSistema_Sistema_responsavel_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Sistema_GpoObjCtrlCod_Z" )]
      [  XmlElement( ElementName = "Sistema_GpoObjCtrlCod_Z"   )]
      public int gxTpr_Sistema_gpoobjctrlcod_Z
      {
         get {
            return gxTv_SdtSistema_Sistema_gpoobjctrlcod_Z ;
         }

         set {
            gxTv_SdtSistema_Sistema_gpoobjctrlcod_Z = (int)(value);
         }

      }

      public void gxTv_SdtSistema_Sistema_gpoobjctrlcod_Z_SetNull( )
      {
         gxTv_SdtSistema_Sistema_gpoobjctrlcod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtSistema_Sistema_gpoobjctrlcod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Sistema_GpoObjCtrlRsp_Z" )]
      [  XmlElement( ElementName = "Sistema_GpoObjCtrlRsp_Z"   )]
      public int gxTpr_Sistema_gpoobjctrlrsp_Z
      {
         get {
            return gxTv_SdtSistema_Sistema_gpoobjctrlrsp_Z ;
         }

         set {
            gxTv_SdtSistema_Sistema_gpoobjctrlrsp_Z = (int)(value);
         }

      }

      public void gxTv_SdtSistema_Sistema_gpoobjctrlrsp_Z_SetNull( )
      {
         gxTv_SdtSistema_Sistema_gpoobjctrlrsp_Z = 0;
         return  ;
      }

      public bool gxTv_SdtSistema_Sistema_gpoobjctrlrsp_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "SistemaVersao_Codigo_Z" )]
      [  XmlElement( ElementName = "SistemaVersao_Codigo_Z"   )]
      public int gxTpr_Sistemaversao_codigo_Z
      {
         get {
            return gxTv_SdtSistema_Sistemaversao_codigo_Z ;
         }

         set {
            gxTv_SdtSistema_Sistemaversao_codigo_Z = (int)(value);
         }

      }

      public void gxTv_SdtSistema_Sistemaversao_codigo_Z_SetNull( )
      {
         gxTv_SdtSistema_Sistemaversao_codigo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtSistema_Sistemaversao_codigo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "SistemaVersao_Id_Z" )]
      [  XmlElement( ElementName = "SistemaVersao_Id_Z"   )]
      public String gxTpr_Sistemaversao_id_Z
      {
         get {
            return gxTv_SdtSistema_Sistemaversao_id_Z ;
         }

         set {
            gxTv_SdtSistema_Sistemaversao_id_Z = (String)(value);
         }

      }

      public void gxTv_SdtSistema_Sistemaversao_id_Z_SetNull( )
      {
         gxTv_SdtSistema_Sistemaversao_id_Z = "";
         return  ;
      }

      public bool gxTv_SdtSistema_Sistemaversao_id_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Sistema_Ativo_Z" )]
      [  XmlElement( ElementName = "Sistema_Ativo_Z"   )]
      public bool gxTpr_Sistema_ativo_Z
      {
         get {
            return gxTv_SdtSistema_Sistema_ativo_Z ;
         }

         set {
            gxTv_SdtSistema_Sistema_ativo_Z = value;
         }

      }

      public void gxTv_SdtSistema_Sistema_ativo_Z_SetNull( )
      {
         gxTv_SdtSistema_Sistema_ativo_Z = false;
         return  ;
      }

      public bool gxTv_SdtSistema_Sistema_ativo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Sistema_Repositorio_Z" )]
      [  XmlElement( ElementName = "Sistema_Repositorio_Z"   )]
      public String gxTpr_Sistema_repositorio_Z
      {
         get {
            return gxTv_SdtSistema_Sistema_repositorio_Z ;
         }

         set {
            gxTv_SdtSistema_Sistema_repositorio_Z = (String)(value);
         }

      }

      public void gxTv_SdtSistema_Sistema_repositorio_Z_SetNull( )
      {
         gxTv_SdtSistema_Sistema_repositorio_Z = "";
         return  ;
      }

      public bool gxTv_SdtSistema_Sistema_repositorio_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Sistema_Ultimo_Z" )]
      [  XmlElement( ElementName = "Sistema_Ultimo_Z"   )]
      public short gxTpr_Sistema_ultimo_Z
      {
         get {
            return gxTv_SdtSistema_Sistema_ultimo_Z ;
         }

         set {
            gxTv_SdtSistema_Sistema_ultimo_Z = (short)(value);
         }

      }

      public void gxTv_SdtSistema_Sistema_ultimo_Z_SetNull( )
      {
         gxTv_SdtSistema_Sistema_ultimo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtSistema_Sistema_ultimo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Sistema_Descricao_N" )]
      [  XmlElement( ElementName = "Sistema_Descricao_N"   )]
      public short gxTpr_Sistema_descricao_N
      {
         get {
            return gxTv_SdtSistema_Sistema_descricao_N ;
         }

         set {
            gxTv_SdtSistema_Sistema_descricao_N = (short)(value);
         }

      }

      public void gxTv_SdtSistema_Sistema_descricao_N_SetNull( )
      {
         gxTv_SdtSistema_Sistema_descricao_N = 0;
         return  ;
      }

      public bool gxTv_SdtSistema_Sistema_descricao_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Sistema_Coordenacao_N" )]
      [  XmlElement( ElementName = "Sistema_Coordenacao_N"   )]
      public short gxTpr_Sistema_coordenacao_N
      {
         get {
            return gxTv_SdtSistema_Sistema_coordenacao_N ;
         }

         set {
            gxTv_SdtSistema_Sistema_coordenacao_N = (short)(value);
         }

      }

      public void gxTv_SdtSistema_Sistema_coordenacao_N_SetNull( )
      {
         gxTv_SdtSistema_Sistema_coordenacao_N = 0;
         return  ;
      }

      public bool gxTv_SdtSistema_Sistema_coordenacao_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Sistema_AreaTrabalhoDes_N" )]
      [  XmlElement( ElementName = "Sistema_AreaTrabalhoDes_N"   )]
      public short gxTpr_Sistema_areatrabalhodes_N
      {
         get {
            return gxTv_SdtSistema_Sistema_areatrabalhodes_N ;
         }

         set {
            gxTv_SdtSistema_Sistema_areatrabalhodes_N = (short)(value);
         }

      }

      public void gxTv_SdtSistema_Sistema_areatrabalhodes_N_SetNull( )
      {
         gxTv_SdtSistema_Sistema_areatrabalhodes_N = 0;
         return  ;
      }

      public bool gxTv_SdtSistema_Sistema_areatrabalhodes_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "AmbienteTecnologico_Codigo_N" )]
      [  XmlElement( ElementName = "AmbienteTecnologico_Codigo_N"   )]
      public short gxTpr_Ambientetecnologico_codigo_N
      {
         get {
            return gxTv_SdtSistema_Ambientetecnologico_codigo_N ;
         }

         set {
            gxTv_SdtSistema_Ambientetecnologico_codigo_N = (short)(value);
         }

      }

      public void gxTv_SdtSistema_Ambientetecnologico_codigo_N_SetNull( )
      {
         gxTv_SdtSistema_Ambientetecnologico_codigo_N = 0;
         return  ;
      }

      public bool gxTv_SdtSistema_Ambientetecnologico_codigo_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Metodologia_Codigo_N" )]
      [  XmlElement( ElementName = "Metodologia_Codigo_N"   )]
      public short gxTpr_Metodologia_codigo_N
      {
         get {
            return gxTv_SdtSistema_Metodologia_codigo_N ;
         }

         set {
            gxTv_SdtSistema_Metodologia_codigo_N = (short)(value);
         }

      }

      public void gxTv_SdtSistema_Metodologia_codigo_N_SetNull( )
      {
         gxTv_SdtSistema_Metodologia_codigo_N = 0;
         return  ;
      }

      public bool gxTv_SdtSistema_Metodologia_codigo_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Sistema_Tipo_N" )]
      [  XmlElement( ElementName = "Sistema_Tipo_N"   )]
      public short gxTpr_Sistema_tipo_N
      {
         get {
            return gxTv_SdtSistema_Sistema_tipo_N ;
         }

         set {
            gxTv_SdtSistema_Sistema_tipo_N = (short)(value);
         }

      }

      public void gxTv_SdtSistema_Sistema_tipo_N_SetNull( )
      {
         gxTv_SdtSistema_Sistema_tipo_N = 0;
         return  ;
      }

      public bool gxTv_SdtSistema_Sistema_tipo_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Sistema_Tecnica_N" )]
      [  XmlElement( ElementName = "Sistema_Tecnica_N"   )]
      public short gxTpr_Sistema_tecnica_N
      {
         get {
            return gxTv_SdtSistema_Sistema_tecnica_N ;
         }

         set {
            gxTv_SdtSistema_Sistema_tecnica_N = (short)(value);
         }

      }

      public void gxTv_SdtSistema_Sistema_tecnica_N_SetNull( )
      {
         gxTv_SdtSistema_Sistema_tecnica_N = 0;
         return  ;
      }

      public bool gxTv_SdtSistema_Sistema_tecnica_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Sistema_FatorAjuste_N" )]
      [  XmlElement( ElementName = "Sistema_FatorAjuste_N"   )]
      public short gxTpr_Sistema_fatorajuste_N
      {
         get {
            return gxTv_SdtSistema_Sistema_fatorajuste_N ;
         }

         set {
            gxTv_SdtSistema_Sistema_fatorajuste_N = (short)(value);
         }

      }

      public void gxTv_SdtSistema_Sistema_fatorajuste_N_SetNull( )
      {
         gxTv_SdtSistema_Sistema_fatorajuste_N = 0;
         return  ;
      }

      public bool gxTv_SdtSistema_Sistema_fatorajuste_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Sistema_Custo_N" )]
      [  XmlElement( ElementName = "Sistema_Custo_N"   )]
      public short gxTpr_Sistema_custo_N
      {
         get {
            return gxTv_SdtSistema_Sistema_custo_N ;
         }

         set {
            gxTv_SdtSistema_Sistema_custo_N = (short)(value);
         }

      }

      public void gxTv_SdtSistema_Sistema_custo_N_SetNull( )
      {
         gxTv_SdtSistema_Sistema_custo_N = 0;
         return  ;
      }

      public bool gxTv_SdtSistema_Sistema_custo_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Sistema_Prazo_N" )]
      [  XmlElement( ElementName = "Sistema_Prazo_N"   )]
      public short gxTpr_Sistema_prazo_N
      {
         get {
            return gxTv_SdtSistema_Sistema_prazo_N ;
         }

         set {
            gxTv_SdtSistema_Sistema_prazo_N = (short)(value);
         }

      }

      public void gxTv_SdtSistema_Sistema_prazo_N_SetNull( )
      {
         gxTv_SdtSistema_Sistema_prazo_N = 0;
         return  ;
      }

      public bool gxTv_SdtSistema_Sistema_prazo_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Sistema_Esforco_N" )]
      [  XmlElement( ElementName = "Sistema_Esforco_N"   )]
      public short gxTpr_Sistema_esforco_N
      {
         get {
            return gxTv_SdtSistema_Sistema_esforco_N ;
         }

         set {
            gxTv_SdtSistema_Sistema_esforco_N = (short)(value);
         }

      }

      public void gxTv_SdtSistema_Sistema_esforco_N_SetNull( )
      {
         gxTv_SdtSistema_Sistema_esforco_N = 0;
         return  ;
      }

      public bool gxTv_SdtSistema_Sistema_esforco_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Sistema_ImpData_N" )]
      [  XmlElement( ElementName = "Sistema_ImpData_N"   )]
      public short gxTpr_Sistema_impdata_N
      {
         get {
            return gxTv_SdtSistema_Sistema_impdata_N ;
         }

         set {
            gxTv_SdtSistema_Sistema_impdata_N = (short)(value);
         }

      }

      public void gxTv_SdtSistema_Sistema_impdata_N_SetNull( )
      {
         gxTv_SdtSistema_Sistema_impdata_N = 0;
         return  ;
      }

      public bool gxTv_SdtSistema_Sistema_impdata_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Sistema_ImpUserCod_N" )]
      [  XmlElement( ElementName = "Sistema_ImpUserCod_N"   )]
      public short gxTpr_Sistema_impusercod_N
      {
         get {
            return gxTv_SdtSistema_Sistema_impusercod_N ;
         }

         set {
            gxTv_SdtSistema_Sistema_impusercod_N = (short)(value);
         }

      }

      public void gxTv_SdtSistema_Sistema_impusercod_N_SetNull( )
      {
         gxTv_SdtSistema_Sistema_impusercod_N = 0;
         return  ;
      }

      public bool gxTv_SdtSistema_Sistema_impusercod_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Sistema_ImpUserPesCod_N" )]
      [  XmlElement( ElementName = "Sistema_ImpUserPesCod_N"   )]
      public short gxTpr_Sistema_impuserpescod_N
      {
         get {
            return gxTv_SdtSistema_Sistema_impuserpescod_N ;
         }

         set {
            gxTv_SdtSistema_Sistema_impuserpescod_N = (short)(value);
         }

      }

      public void gxTv_SdtSistema_Sistema_impuserpescod_N_SetNull( )
      {
         gxTv_SdtSistema_Sistema_impuserpescod_N = 0;
         return  ;
      }

      public bool gxTv_SdtSistema_Sistema_impuserpescod_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Sistema_ImpUserPesNom_N" )]
      [  XmlElement( ElementName = "Sistema_ImpUserPesNom_N"   )]
      public short gxTpr_Sistema_impuserpesnom_N
      {
         get {
            return gxTv_SdtSistema_Sistema_impuserpesnom_N ;
         }

         set {
            gxTv_SdtSistema_Sistema_impuserpesnom_N = (short)(value);
         }

      }

      public void gxTv_SdtSistema_Sistema_impuserpesnom_N_SetNull( )
      {
         gxTv_SdtSistema_Sistema_impuserpesnom_N = 0;
         return  ;
      }

      public bool gxTv_SdtSistema_Sistema_impuserpesnom_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Sistema_Responsavel_N" )]
      [  XmlElement( ElementName = "Sistema_Responsavel_N"   )]
      public short gxTpr_Sistema_responsavel_N
      {
         get {
            return gxTv_SdtSistema_Sistema_responsavel_N ;
         }

         set {
            gxTv_SdtSistema_Sistema_responsavel_N = (short)(value);
         }

      }

      public void gxTv_SdtSistema_Sistema_responsavel_N_SetNull( )
      {
         gxTv_SdtSistema_Sistema_responsavel_N = 0;
         return  ;
      }

      public bool gxTv_SdtSistema_Sistema_responsavel_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Sistema_GpoObjCtrlCod_N" )]
      [  XmlElement( ElementName = "Sistema_GpoObjCtrlCod_N"   )]
      public short gxTpr_Sistema_gpoobjctrlcod_N
      {
         get {
            return gxTv_SdtSistema_Sistema_gpoobjctrlcod_N ;
         }

         set {
            gxTv_SdtSistema_Sistema_gpoobjctrlcod_N = (short)(value);
         }

      }

      public void gxTv_SdtSistema_Sistema_gpoobjctrlcod_N_SetNull( )
      {
         gxTv_SdtSistema_Sistema_gpoobjctrlcod_N = 0;
         return  ;
      }

      public bool gxTv_SdtSistema_Sistema_gpoobjctrlcod_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "SistemaVersao_Codigo_N" )]
      [  XmlElement( ElementName = "SistemaVersao_Codigo_N"   )]
      public short gxTpr_Sistemaversao_codigo_N
      {
         get {
            return gxTv_SdtSistema_Sistemaversao_codigo_N ;
         }

         set {
            gxTv_SdtSistema_Sistemaversao_codigo_N = (short)(value);
         }

      }

      public void gxTv_SdtSistema_Sistemaversao_codigo_N_SetNull( )
      {
         gxTv_SdtSistema_Sistemaversao_codigo_N = 0;
         return  ;
      }

      public bool gxTv_SdtSistema_Sistemaversao_codigo_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Sistema_Repositorio_N" )]
      [  XmlElement( ElementName = "Sistema_Repositorio_N"   )]
      public short gxTpr_Sistema_repositorio_N
      {
         get {
            return gxTv_SdtSistema_Sistema_repositorio_N ;
         }

         set {
            gxTv_SdtSistema_Sistema_repositorio_N = (short)(value);
         }

      }

      public void gxTv_SdtSistema_Sistema_repositorio_N_SetNull( )
      {
         gxTv_SdtSistema_Sistema_repositorio_N = 0;
         return  ;
      }

      public bool gxTv_SdtSistema_Sistema_repositorio_N_IsNull( )
      {
         return false ;
      }

      public void initialize( )
      {
         gxTv_SdtSistema_Sistema_nome = "";
         gxTv_SdtSistema_Sistema_descricao = "";
         gxTv_SdtSistema_Sistema_sigla = "";
         gxTv_SdtSistema_Sistema_coordenacao = "";
         gxTv_SdtSistema_Sistema_areatrabalhodes = "";
         gxTv_SdtSistema_Ambientetecnologico_descricao = "";
         gxTv_SdtSistema_Metodologia_descricao = "";
         gxTv_SdtSistema_Sistema_tipo = "A";
         gxTv_SdtSistema_Sistema_tiposigla = "";
         gxTv_SdtSistema_Sistema_tecnica = "";
         gxTv_SdtSistema_Sistema_impdata = (DateTime)(DateTime.MinValue);
         gxTv_SdtSistema_Sistema_impuserpesnom = "";
         gxTv_SdtSistema_Sistemaversao_id = "";
         gxTv_SdtSistema_Sistema_repositorio = "";
         gxTv_SdtSistema_Mode = "";
         gxTv_SdtSistema_Sistema_nome_Z = "";
         gxTv_SdtSistema_Sistema_sigla_Z = "";
         gxTv_SdtSistema_Sistema_coordenacao_Z = "";
         gxTv_SdtSistema_Sistema_areatrabalhodes_Z = "";
         gxTv_SdtSistema_Ambientetecnologico_descricao_Z = "";
         gxTv_SdtSistema_Metodologia_descricao_Z = "";
         gxTv_SdtSistema_Sistema_tipo_Z = "";
         gxTv_SdtSistema_Sistema_tiposigla_Z = "";
         gxTv_SdtSistema_Sistema_tecnica_Z = "";
         gxTv_SdtSistema_Sistema_impdata_Z = (DateTime)(DateTime.MinValue);
         gxTv_SdtSistema_Sistema_impuserpesnom_Z = "";
         gxTv_SdtSistema_Sistemaversao_id_Z = "";
         gxTv_SdtSistema_Sistema_repositorio_Z = "";
         gxTv_SdtSistema_Sistema_ativo = true;
         sTagName = "";
         sDateCnv = "";
         sNumToPad = "";
         datetime_STZ = (DateTime)(DateTime.MinValue);
         IGxSilentTrn obj ;
         obj = (IGxSilentTrn)ClassLoader.FindInstance( "sistema", "GeneXus.Programs.sistema_bc", new Object[] {context}, constructorCallingAssembly);
         obj.initialize();
         obj.SetSDT(this, 1);
         setTransaction( obj) ;
         obj.SetMode("INS");
         return  ;
      }

      private short gxTv_SdtSistema_Sistema_prazo ;
      private short gxTv_SdtSistema_Sistema_esforco ;
      private short gxTv_SdtSistema_Sistema_ultimo ;
      private short gxTv_SdtSistema_Initialized ;
      private short gxTv_SdtSistema_Sistema_prazo_Z ;
      private short gxTv_SdtSistema_Sistema_esforco_Z ;
      private short gxTv_SdtSistema_Sistema_ultimo_Z ;
      private short gxTv_SdtSistema_Sistema_descricao_N ;
      private short gxTv_SdtSistema_Sistema_coordenacao_N ;
      private short gxTv_SdtSistema_Sistema_areatrabalhodes_N ;
      private short gxTv_SdtSistema_Ambientetecnologico_codigo_N ;
      private short gxTv_SdtSistema_Metodologia_codigo_N ;
      private short gxTv_SdtSistema_Sistema_tipo_N ;
      private short gxTv_SdtSistema_Sistema_tecnica_N ;
      private short gxTv_SdtSistema_Sistema_fatorajuste_N ;
      private short gxTv_SdtSistema_Sistema_custo_N ;
      private short gxTv_SdtSistema_Sistema_prazo_N ;
      private short gxTv_SdtSistema_Sistema_esforco_N ;
      private short gxTv_SdtSistema_Sistema_impdata_N ;
      private short gxTv_SdtSistema_Sistema_impusercod_N ;
      private short gxTv_SdtSistema_Sistema_impuserpescod_N ;
      private short gxTv_SdtSistema_Sistema_impuserpesnom_N ;
      private short gxTv_SdtSistema_Sistema_responsavel_N ;
      private short gxTv_SdtSistema_Sistema_gpoobjctrlcod_N ;
      private short gxTv_SdtSistema_Sistemaversao_codigo_N ;
      private short gxTv_SdtSistema_Sistema_repositorio_N ;
      private short readOk ;
      private short nOutParmCount ;
      private int gxTv_SdtSistema_Sistema_codigo ;
      private int gxTv_SdtSistema_Sistema_areatrabalhocod ;
      private int gxTv_SdtSistema_Ambientetecnologico_codigo ;
      private int gxTv_SdtSistema_Metodologia_codigo ;
      private int gxTv_SdtSistema_Sistema_impusercod ;
      private int gxTv_SdtSistema_Sistema_impuserpescod ;
      private int gxTv_SdtSistema_Sistema_responsavel ;
      private int gxTv_SdtSistema_Sistema_gpoobjctrlcod ;
      private int gxTv_SdtSistema_Sistema_gpoobjctrlrsp ;
      private int gxTv_SdtSistema_Sistemaversao_codigo ;
      private int gxTv_SdtSistema_Sistema_codigo_Z ;
      private int gxTv_SdtSistema_Sistema_areatrabalhocod_Z ;
      private int gxTv_SdtSistema_Ambientetecnologico_codigo_Z ;
      private int gxTv_SdtSistema_Metodologia_codigo_Z ;
      private int gxTv_SdtSistema_Sistema_impusercod_Z ;
      private int gxTv_SdtSistema_Sistema_impuserpescod_Z ;
      private int gxTv_SdtSistema_Sistema_responsavel_Z ;
      private int gxTv_SdtSistema_Sistema_gpoobjctrlcod_Z ;
      private int gxTv_SdtSistema_Sistema_gpoobjctrlrsp_Z ;
      private int gxTv_SdtSistema_Sistemaversao_codigo_Z ;
      private decimal gxTv_SdtSistema_Sistema_pf ;
      private decimal gxTv_SdtSistema_Sistema_pfa ;
      private decimal gxTv_SdtSistema_Sistema_fatorajuste ;
      private decimal gxTv_SdtSistema_Sistema_custo ;
      private decimal gxTv_SdtSistema_Sistema_pf_Z ;
      private decimal gxTv_SdtSistema_Sistema_pfa_Z ;
      private decimal gxTv_SdtSistema_Sistema_fatorajuste_Z ;
      private decimal gxTv_SdtSistema_Sistema_custo_Z ;
      private String gxTv_SdtSistema_Sistema_sigla ;
      private String gxTv_SdtSistema_Sistema_tipo ;
      private String gxTv_SdtSistema_Sistema_tiposigla ;
      private String gxTv_SdtSistema_Sistema_tecnica ;
      private String gxTv_SdtSistema_Sistema_impuserpesnom ;
      private String gxTv_SdtSistema_Sistemaversao_id ;
      private String gxTv_SdtSistema_Mode ;
      private String gxTv_SdtSistema_Sistema_sigla_Z ;
      private String gxTv_SdtSistema_Sistema_tipo_Z ;
      private String gxTv_SdtSistema_Sistema_tiposigla_Z ;
      private String gxTv_SdtSistema_Sistema_tecnica_Z ;
      private String gxTv_SdtSistema_Sistema_impuserpesnom_Z ;
      private String gxTv_SdtSistema_Sistemaversao_id_Z ;
      private String sTagName ;
      private String sDateCnv ;
      private String sNumToPad ;
      private DateTime gxTv_SdtSistema_Sistema_impdata ;
      private DateTime gxTv_SdtSistema_Sistema_impdata_Z ;
      private DateTime datetime_STZ ;
      private bool gxTv_SdtSistema_Sistema_ativo ;
      private bool gxTv_SdtSistema_Sistema_ativo_Z ;
      private String gxTv_SdtSistema_Sistema_descricao ;
      private String gxTv_SdtSistema_Sistema_nome ;
      private String gxTv_SdtSistema_Sistema_coordenacao ;
      private String gxTv_SdtSistema_Sistema_areatrabalhodes ;
      private String gxTv_SdtSistema_Ambientetecnologico_descricao ;
      private String gxTv_SdtSistema_Metodologia_descricao ;
      private String gxTv_SdtSistema_Sistema_repositorio ;
      private String gxTv_SdtSistema_Sistema_nome_Z ;
      private String gxTv_SdtSistema_Sistema_coordenacao_Z ;
      private String gxTv_SdtSistema_Sistema_areatrabalhodes_Z ;
      private String gxTv_SdtSistema_Ambientetecnologico_descricao_Z ;
      private String gxTv_SdtSistema_Metodologia_descricao_Z ;
      private String gxTv_SdtSistema_Sistema_repositorio_Z ;
      private Assembly constructorCallingAssembly ;
      [ObjectCollection(ItemType=typeof( SdtSistema_Tecnologia ))]
      private GxSilentTrnGridCollection gxTv_SdtSistema_Tecnologia=null ;
   }

   [DataContract(Name = @"Sistema", Namespace = "GxEv3Up14_MeetrikaVs3")]
   public class SdtSistema_RESTInterface : GxGenericCollectionItem<SdtSistema>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtSistema_RESTInterface( ) : base()
      {
      }

      public SdtSistema_RESTInterface( SdtSistema psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "Sistema_Codigo" , Order = 0 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Sistema_codigo
      {
         get {
            return sdt.gxTpr_Sistema_codigo ;
         }

         set {
            sdt.gxTpr_Sistema_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Sistema_Nome" , Order = 1 )]
      [GxSeudo()]
      public String gxTpr_Sistema_nome
      {
         get {
            return sdt.gxTpr_Sistema_nome ;
         }

         set {
            sdt.gxTpr_Sistema_nome = (String)(value);
         }

      }

      [DataMember( Name = "Sistema_Descricao" , Order = 2 )]
      public String gxTpr_Sistema_descricao
      {
         get {
            return sdt.gxTpr_Sistema_descricao ;
         }

         set {
            sdt.gxTpr_Sistema_descricao = (String)(value);
         }

      }

      [DataMember( Name = "Sistema_Sigla" , Order = 3 )]
      [GxSeudo()]
      public String gxTpr_Sistema_sigla
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Sistema_sigla) ;
         }

         set {
            sdt.gxTpr_Sistema_sigla = (String)(value);
         }

      }

      [DataMember( Name = "Sistema_Coordenacao" , Order = 4 )]
      [GxSeudo()]
      public String gxTpr_Sistema_coordenacao
      {
         get {
            return sdt.gxTpr_Sistema_coordenacao ;
         }

         set {
            sdt.gxTpr_Sistema_coordenacao = (String)(value);
         }

      }

      [DataMember( Name = "Sistema_AreaTrabalhoCod" , Order = 5 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Sistema_areatrabalhocod
      {
         get {
            return sdt.gxTpr_Sistema_areatrabalhocod ;
         }

         set {
            sdt.gxTpr_Sistema_areatrabalhocod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Sistema_AreaTrabalhoDes" , Order = 6 )]
      [GxSeudo()]
      public String gxTpr_Sistema_areatrabalhodes
      {
         get {
            return sdt.gxTpr_Sistema_areatrabalhodes ;
         }

         set {
            sdt.gxTpr_Sistema_areatrabalhodes = (String)(value);
         }

      }

      [DataMember( Name = "AmbienteTecnologico_Codigo" , Order = 7 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Ambientetecnologico_codigo
      {
         get {
            return sdt.gxTpr_Ambientetecnologico_codigo ;
         }

         set {
            sdt.gxTpr_Ambientetecnologico_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "AmbienteTecnologico_Descricao" , Order = 8 )]
      [GxSeudo()]
      public String gxTpr_Ambientetecnologico_descricao
      {
         get {
            return sdt.gxTpr_Ambientetecnologico_descricao ;
         }

         set {
            sdt.gxTpr_Ambientetecnologico_descricao = (String)(value);
         }

      }

      [DataMember( Name = "Metodologia_Codigo" , Order = 9 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Metodologia_codigo
      {
         get {
            return sdt.gxTpr_Metodologia_codigo ;
         }

         set {
            sdt.gxTpr_Metodologia_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Metodologia_Descricao" , Order = 10 )]
      [GxSeudo()]
      public String gxTpr_Metodologia_descricao
      {
         get {
            return sdt.gxTpr_Metodologia_descricao ;
         }

         set {
            sdt.gxTpr_Metodologia_descricao = (String)(value);
         }

      }

      [DataMember( Name = "Sistema_PF" , Order = 11 )]
      [GxSeudo()]
      public String gxTpr_Sistema_pf
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( sdt.gxTpr_Sistema_pf, 14, 5)) ;
         }

         set {
            sdt.gxTpr_Sistema_pf = NumberUtil.Val( (String)(value), ".");
         }

      }

      [DataMember( Name = "Sistema_PFA" , Order = 12 )]
      [GxSeudo()]
      public String gxTpr_Sistema_pfa
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( sdt.gxTpr_Sistema_pfa, 14, 5)) ;
         }

         set {
            sdt.gxTpr_Sistema_pfa = NumberUtil.Val( (String)(value), ".");
         }

      }

      [DataMember( Name = "Sistema_Tipo" , Order = 13 )]
      [GxSeudo()]
      public String gxTpr_Sistema_tipo
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Sistema_tipo) ;
         }

         set {
            sdt.gxTpr_Sistema_tipo = (String)(value);
         }

      }

      [DataMember( Name = "Sistema_TipoSigla" , Order = 14 )]
      [GxSeudo()]
      public String gxTpr_Sistema_tiposigla
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Sistema_tiposigla) ;
         }

         set {
            sdt.gxTpr_Sistema_tiposigla = (String)(value);
         }

      }

      [DataMember( Name = "Sistema_Tecnica" , Order = 15 )]
      [GxSeudo()]
      public String gxTpr_Sistema_tecnica
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Sistema_tecnica) ;
         }

         set {
            sdt.gxTpr_Sistema_tecnica = (String)(value);
         }

      }

      [DataMember( Name = "Sistema_FatorAjuste" , Order = 16 )]
      [GxSeudo()]
      public Nullable<decimal> gxTpr_Sistema_fatorajuste
      {
         get {
            return sdt.gxTpr_Sistema_fatorajuste ;
         }

         set {
            sdt.gxTpr_Sistema_fatorajuste = (decimal)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Sistema_Custo" , Order = 17 )]
      [GxSeudo()]
      public String gxTpr_Sistema_custo
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( sdt.gxTpr_Sistema_custo, 18, 5)) ;
         }

         set {
            sdt.gxTpr_Sistema_custo = NumberUtil.Val( (String)(value), ".");
         }

      }

      [DataMember( Name = "Sistema_Prazo" , Order = 18 )]
      [GxSeudo()]
      public Nullable<short> gxTpr_Sistema_prazo
      {
         get {
            return sdt.gxTpr_Sistema_prazo ;
         }

         set {
            sdt.gxTpr_Sistema_prazo = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Sistema_Esforco" , Order = 19 )]
      [GxSeudo()]
      public Nullable<short> gxTpr_Sistema_esforco
      {
         get {
            return sdt.gxTpr_Sistema_esforco ;
         }

         set {
            sdt.gxTpr_Sistema_esforco = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Sistema_ImpData" , Order = 20 )]
      [GxSeudo()]
      public String gxTpr_Sistema_impdata
      {
         get {
            return DateTimeUtil.TToC2( sdt.gxTpr_Sistema_impdata) ;
         }

         set {
            sdt.gxTpr_Sistema_impdata = DateTimeUtil.CToT2( (String)(value));
         }

      }

      [DataMember( Name = "Sistema_ImpUserCod" , Order = 21 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Sistema_impusercod
      {
         get {
            return sdt.gxTpr_Sistema_impusercod ;
         }

         set {
            sdt.gxTpr_Sistema_impusercod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Sistema_ImpUserPesCod" , Order = 22 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Sistema_impuserpescod
      {
         get {
            return sdt.gxTpr_Sistema_impuserpescod ;
         }

         set {
            sdt.gxTpr_Sistema_impuserpescod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Sistema_ImpUserPesNom" , Order = 23 )]
      [GxSeudo()]
      public String gxTpr_Sistema_impuserpesnom
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Sistema_impuserpesnom) ;
         }

         set {
            sdt.gxTpr_Sistema_impuserpesnom = (String)(value);
         }

      }

      [DataMember( Name = "Sistema_Responsavel" , Order = 24 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Sistema_responsavel
      {
         get {
            return sdt.gxTpr_Sistema_responsavel ;
         }

         set {
            sdt.gxTpr_Sistema_responsavel = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Sistema_GpoObjCtrlCod" , Order = 25 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Sistema_gpoobjctrlcod
      {
         get {
            return sdt.gxTpr_Sistema_gpoobjctrlcod ;
         }

         set {
            sdt.gxTpr_Sistema_gpoobjctrlcod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Sistema_GpoObjCtrlRsp" , Order = 26 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Sistema_gpoobjctrlrsp
      {
         get {
            return sdt.gxTpr_Sistema_gpoobjctrlrsp ;
         }

         set {
            sdt.gxTpr_Sistema_gpoobjctrlrsp = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "SistemaVersao_Codigo" , Order = 27 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Sistemaversao_codigo
      {
         get {
            return sdt.gxTpr_Sistemaversao_codigo ;
         }

         set {
            sdt.gxTpr_Sistemaversao_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "SistemaVersao_Id" , Order = 28 )]
      [GxSeudo()]
      public String gxTpr_Sistemaversao_id
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Sistemaversao_id) ;
         }

         set {
            sdt.gxTpr_Sistemaversao_id = (String)(value);
         }

      }

      [DataMember( Name = "Sistema_Ativo" , Order = 29 )]
      [GxSeudo()]
      public bool gxTpr_Sistema_ativo
      {
         get {
            return sdt.gxTpr_Sistema_ativo ;
         }

         set {
            sdt.gxTpr_Sistema_ativo = value;
         }

      }

      [DataMember( Name = "Sistema_Repositorio" , Order = 30 )]
      [GxSeudo()]
      public String gxTpr_Sistema_repositorio
      {
         get {
            return sdt.gxTpr_Sistema_repositorio ;
         }

         set {
            sdt.gxTpr_Sistema_repositorio = (String)(value);
         }

      }

      [DataMember( Name = "Sistema_Ultimo" , Order = 31 )]
      [GxSeudo()]
      public Nullable<short> gxTpr_Sistema_ultimo
      {
         get {
            return sdt.gxTpr_Sistema_ultimo ;
         }

         set {
            sdt.gxTpr_Sistema_ultimo = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Tecnologia" , Order = 32 )]
      public GxGenericCollection<SdtSistema_Tecnologia_RESTInterface> gxTpr_Tecnologia
      {
         get {
            return new GxGenericCollection<SdtSistema_Tecnologia_RESTInterface>(sdt.gxTpr_Tecnologia) ;
         }

         set {
            value.LoadCollection(sdt.gxTpr_Tecnologia);
         }

      }

      public SdtSistema sdt
      {
         get {
            return (SdtSistema)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtSistema() ;
         }
      }

      [DataMember( Name = "gx_md5_hash", Order = 85 )]
      public string Hash
      {
         get {
            if ( StringUtil.StrCmp(md5Hash, null) == 0 )
            {
               md5Hash = (String)(getHash());
            }
            return md5Hash ;
         }

         set {
            md5Hash = value ;
         }

      }

      private String md5Hash ;
   }

}
