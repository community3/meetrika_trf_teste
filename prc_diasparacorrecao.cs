/*
               File: PRC_DiasParaCorrecao
        Description: Dias Para Corre�ao
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:12:45.24
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_diasparacorrecao : GXProcedure
   {
      public prc_diasparacorrecao( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_diasparacorrecao( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_ContratoServicos_Codigo ,
                           int aP1_Codigo ,
                           out short aP2_Dias )
      {
         this.AV13ContratoServicos_Codigo = aP0_ContratoServicos_Codigo;
         this.AV11Codigo = aP1_Codigo;
         this.AV9Dias = 0 ;
         initialize();
         executePrivate();
         aP0_ContratoServicos_Codigo=this.AV13ContratoServicos_Codigo;
         aP2_Dias=this.AV9Dias;
      }

      public short executeUdp( ref int aP0_ContratoServicos_Codigo ,
                               int aP1_Codigo )
      {
         this.AV13ContratoServicos_Codigo = aP0_ContratoServicos_Codigo;
         this.AV11Codigo = aP1_Codigo;
         this.AV9Dias = 0 ;
         initialize();
         executePrivate();
         aP0_ContratoServicos_Codigo=this.AV13ContratoServicos_Codigo;
         aP2_Dias=this.AV9Dias;
         return AV9Dias ;
      }

      public void executeSubmit( ref int aP0_ContratoServicos_Codigo ,
                                 int aP1_Codigo ,
                                 out short aP2_Dias )
      {
         prc_diasparacorrecao objprc_diasparacorrecao;
         objprc_diasparacorrecao = new prc_diasparacorrecao();
         objprc_diasparacorrecao.AV13ContratoServicos_Codigo = aP0_ContratoServicos_Codigo;
         objprc_diasparacorrecao.AV11Codigo = aP1_Codigo;
         objprc_diasparacorrecao.AV9Dias = 0 ;
         objprc_diasparacorrecao.context.SetSubmitInitialConfig(context);
         objprc_diasparacorrecao.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_diasparacorrecao);
         aP0_ContratoServicos_Codigo=this.AV13ContratoServicos_Codigo;
         aP2_Dias=this.AV9Dias;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_diasparacorrecao)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P009R2 */
         pr_default.execute(0, new Object[] {AV13ContratoServicos_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A160ContratoServicos_Codigo = P009R2_A160ContratoServicos_Codigo[0];
            A1225ContratoServicos_PrazoCorrecaoTipo = P009R2_A1225ContratoServicos_PrazoCorrecaoTipo[0];
            n1225ContratoServicos_PrazoCorrecaoTipo = P009R2_n1225ContratoServicos_PrazoCorrecaoTipo[0];
            A1224ContratoServicos_PrazoCorrecao = P009R2_A1224ContratoServicos_PrazoCorrecao[0];
            n1224ContratoServicos_PrazoCorrecao = P009R2_n1224ContratoServicos_PrazoCorrecao[0];
            if ( StringUtil.StrCmp(A1225ContratoServicos_PrazoCorrecaoTipo, "F") == 0 )
            {
               AV9Dias = A1224ContratoServicos_PrazoCorrecao;
            }
            else if ( ( StringUtil.StrCmp(A1225ContratoServicos_PrazoCorrecaoTipo, "I") == 0 ) || ( StringUtil.StrCmp(A1225ContratoServicos_PrazoCorrecaoTipo, "P") == 0 ) )
            {
               /* Execute user subroutine: 'GETPRAZOINICIAL' */
               S111 ();
               if ( returnInSub )
               {
                  pr_default.close(0);
                  this.cleanup();
                  if (true) return;
               }
               if ( StringUtil.StrCmp(A1225ContratoServicos_PrazoCorrecaoTipo, "I") == 0 )
               {
                  AV9Dias = AV12PrazoInicial;
               }
               else if ( StringUtil.StrCmp(A1225ContratoServicos_PrazoCorrecaoTipo, "P") == 0 )
               {
                  AV9Dias = (short)(AV12PrazoInicial*(A1224ContratoServicos_PrazoCorrecao/ (decimal)(100)));
                  if ( AV9Dias > NumberUtil.Int( AV9Dias) )
                  {
                     AV9Dias = (short)(AV9Dias+1);
                  }
               }
            }
            else if ( StringUtil.StrCmp(A1225ContratoServicos_PrazoCorrecaoTipo, "F") == 0 )
            {
            }
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'GETPRAZOINICIAL' Routine */
         /* Using cursor P009R3 */
         pr_default.execute(1, new Object[] {AV11Codigo});
         while ( (pr_default.getStatus(1) != 101) )
         {
            A456ContagemResultado_Codigo = P009R3_A456ContagemResultado_Codigo[0];
            A1227ContagemResultado_PrazoInicialDias = P009R3_A1227ContagemResultado_PrazoInicialDias[0];
            n1227ContagemResultado_PrazoInicialDias = P009R3_n1227ContagemResultado_PrazoInicialDias[0];
            AV12PrazoInicial = A1227ContagemResultado_PrazoInicialDias;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(1);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P009R2_A160ContratoServicos_Codigo = new int[1] ;
         P009R2_A1225ContratoServicos_PrazoCorrecaoTipo = new String[] {""} ;
         P009R2_n1225ContratoServicos_PrazoCorrecaoTipo = new bool[] {false} ;
         P009R2_A1224ContratoServicos_PrazoCorrecao = new short[1] ;
         P009R2_n1224ContratoServicos_PrazoCorrecao = new bool[] {false} ;
         A1225ContratoServicos_PrazoCorrecaoTipo = "";
         P009R3_A456ContagemResultado_Codigo = new int[1] ;
         P009R3_A1227ContagemResultado_PrazoInicialDias = new short[1] ;
         P009R3_n1227ContagemResultado_PrazoInicialDias = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_diasparacorrecao__default(),
            new Object[][] {
                new Object[] {
               P009R2_A160ContratoServicos_Codigo, P009R2_A1225ContratoServicos_PrazoCorrecaoTipo, P009R2_n1225ContratoServicos_PrazoCorrecaoTipo, P009R2_A1224ContratoServicos_PrazoCorrecao, P009R2_n1224ContratoServicos_PrazoCorrecao
               }
               , new Object[] {
               P009R3_A456ContagemResultado_Codigo, P009R3_A1227ContagemResultado_PrazoInicialDias, P009R3_n1227ContagemResultado_PrazoInicialDias
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV9Dias ;
      private short A1224ContratoServicos_PrazoCorrecao ;
      private short AV12PrazoInicial ;
      private short A1227ContagemResultado_PrazoInicialDias ;
      private int AV13ContratoServicos_Codigo ;
      private int AV11Codigo ;
      private int A160ContratoServicos_Codigo ;
      private int A456ContagemResultado_Codigo ;
      private String scmdbuf ;
      private String A1225ContratoServicos_PrazoCorrecaoTipo ;
      private bool n1225ContratoServicos_PrazoCorrecaoTipo ;
      private bool n1224ContratoServicos_PrazoCorrecao ;
      private bool returnInSub ;
      private bool n1227ContagemResultado_PrazoInicialDias ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_ContratoServicos_Codigo ;
      private IDataStoreProvider pr_default ;
      private int[] P009R2_A160ContratoServicos_Codigo ;
      private String[] P009R2_A1225ContratoServicos_PrazoCorrecaoTipo ;
      private bool[] P009R2_n1225ContratoServicos_PrazoCorrecaoTipo ;
      private short[] P009R2_A1224ContratoServicos_PrazoCorrecao ;
      private bool[] P009R2_n1224ContratoServicos_PrazoCorrecao ;
      private int[] P009R3_A456ContagemResultado_Codigo ;
      private short[] P009R3_A1227ContagemResultado_PrazoInicialDias ;
      private bool[] P009R3_n1227ContagemResultado_PrazoInicialDias ;
      private short aP2_Dias ;
   }

   public class prc_diasparacorrecao__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP009R2 ;
          prmP009R2 = new Object[] {
          new Object[] {"@AV13ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP009R3 ;
          prmP009R3 = new Object[] {
          new Object[] {"@AV11Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P009R2", "SELECT TOP 1 [ContratoServicos_Codigo], [ContratoServicos_PrazoCorrecaoTipo], [ContratoServicos_PrazoCorrecao] FROM [ContratoServicos] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @AV13ContratoServicos_Codigo ORDER BY [ContratoServicos_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP009R2,1,0,true,true )
             ,new CursorDef("P009R3", "SELECT TOP 1 [ContagemResultado_Codigo], [ContagemResultado_PrazoInicialDias] FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @AV11Codigo ORDER BY [ContagemResultado_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP009R3,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 1) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((short[]) buf[3])[0] = rslt.getShort(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
