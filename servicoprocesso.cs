/*
               File: ServicoProcesso
        Description: Processo do Servi�o
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:17:41.73
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class servicoprocesso : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_13") == 0 )
         {
            A631Servico_Vinculado = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n631Servico_Vinculado = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A631Servico_Vinculado", StringUtil.LTrim( StringUtil.Str( (decimal)(A631Servico_Vinculado), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_13( A631Servico_Vinculado) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV8Servico_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8Servico_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vSERVICO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV8Servico_Codigo), "ZZZZZ9")));
               AV7Ordem = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7Ordem", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Ordem), 3, 0)));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         radavManterordem.Name = "vMANTERORDEM";
         radavManterordem.WebTags = "";
         radavManterordem.addItem(StringUtil.BoolToStr( true), "Mesma ordem", 0);
         radavManterordem.addItem(StringUtil.BoolToStr( false), "Inserir antes", 0);
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Processo do Servi�o", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtServico_Nome_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public servicoprocesso( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public servicoprocesso( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           int aP1_Servico_Codigo ,
                           ref short aP2_Ordem )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV8Servico_Codigo = aP1_Servico_Codigo;
         this.AV7Ordem = aP2_Ordem;
         executePrivate();
         aP2_Ordem=this.AV7Ordem;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         radavManterordem = new GXRadio();
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("workwithplusbootstrapmasterpage", "GeneXus.Programs.workwithplusbootstrapmasterpage", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_3X130( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_3X130e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtServico_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A155Servico_Codigo), 6, 0, ",", "")), ((edtServico_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A155Servico_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A155Servico_Codigo), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtServico_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtServico_Codigo_Visible, edtServico_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ServicoProcesso.htm");
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtavServicofluxo_ordemoldvalue_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV10ServicoFluxo_OrdemOldValue), 3, 0, ",", "")), ((edtavServicofluxo_ordemoldvalue_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV10ServicoFluxo_OrdemOldValue), "ZZ9")) : context.localUtil.Format( (decimal)(AV10ServicoFluxo_OrdemOldValue), "ZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavServicofluxo_ordemoldvalue_Jsonclick, 0, "Attribute", "", "", "", edtavServicofluxo_ordemoldvalue_Visible, edtavServicofluxo_ordemoldvalue_Enabled, 0, "text", "", 50, "px", 1, "row", 3, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ServicoProcesso.htm");
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_3X130( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_3X130( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_3X130e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_74_3X130( true) ;
         }
         return  ;
      }

      protected void wb_table3_74_3X130e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_3X130e( true) ;
         }
         else
         {
            wb_table1_2_3X130e( false) ;
         }
      }

      protected void wb_table3_74_3X130( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 77,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", "Confirmar", bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_trn_enter_Visible, bttBtn_trn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_ServicoProcesso.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 79,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "", "Fechar", bttBtn_trn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_trn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_ServicoProcesso.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 81,'',false,'',0)\"";
            ClassString = "btn btn-danger";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_delete_Internalname, "", "Eliminar", bttBtn_trn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_trn_delete_Visible, bttBtn_trn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_ServicoProcesso.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_74_3X130e( true) ;
         }
         else
         {
            wb_table3_74_3X130e( false) ;
         }
      }

      protected void wb_table2_5_3X130( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbservico_Internalname, lblTbservico_Caption, "", "", lblTbservico_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_ServicoProcesso.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_14_3X130( true) ;
         }
         return  ;
      }

      protected void wb_table4_14_3X130e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_3X130e( true) ;
         }
         else
         {
            wb_table2_5_3X130e( false) ;
         }
      }

      protected void wb_table4_14_3X130( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableData", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCellLeft'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservicofluxo_ordem_Internalname, "Ordem", "", "", lblTextblockservicofluxo_ordem_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ServicoProcesso.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table5_19_3X130( true) ;
         }
         return  ;
      }

      protected void wb_table5_19_3X130e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCellLeft'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservico_nome_Internalname, "Nome", "", "", lblTextblockservico_nome_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ServicoProcesso.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 31,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtServico_Nome_Internalname, StringUtil.RTrim( A608Servico_Nome), StringUtil.RTrim( context.localUtil.Format( A608Servico_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,31);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtServico_Nome_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtServico_Nome_Enabled, 0, "text", "", 380, "px", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_ServicoProcesso.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCellLeft'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservico_sigla_Internalname, "Sigla", "", "", lblTextblockservico_sigla_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ServicoProcesso.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtServico_Sigla_Internalname, StringUtil.RTrim( A605Servico_Sigla), StringUtil.RTrim( context.localUtil.Format( A605Servico_Sigla, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,36);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtServico_Sigla_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtServico_Sigla_Enabled, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "Sigla", "left", true, "HLP_ServicoProcesso.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCellLeft'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservico_descricao_Internalname, "Descri��o", "", "", lblTextblockservico_descricao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ServicoProcesso.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 41,'',false,'',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtServico_Descricao_Internalname, A156Servico_Descricao, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,41);\"", 0, 1, edtServico_Descricao_Enabled, 0, 600, "px", 2, "row", StyleString, ClassString, "", "500", 1, "", "", -1, true, "DescricaoLonga", "HLP_ServicoProcesso.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCellLeft'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservico_perctmp_Internalname, "Perc. de Tempo", "", "", lblTextblockservico_perctmp_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ServicoProcesso.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table6_46_3X130( true) ;
         }
         return  ;
      }

      protected void wb_table6_46_3X130e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCellLeft'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservico_percpgm_Internalname, "Perc. de Pagamento", "", "", lblTextblockservico_percpgm_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ServicoProcesso.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table7_56_3X130( true) ;
         }
         return  ;
      }

      protected void wb_table7_56_3X130e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCellLeft'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservico_perccnc_Internalname, "Perc. de Cancelamento", "", "", lblTextblockservico_perccnc_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ServicoProcesso.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table8_66_3X130( true) ;
         }
         return  ;
      }

      protected void wb_table8_66_3X130e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_14_3X130e( true) ;
         }
         else
         {
            wb_table4_14_3X130e( false) ;
         }
      }

      protected void wb_table8_66_3X130( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedservico_perccnc_Internalname, tblTablemergedservico_perccnc_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtServico_PercCnc_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1536Servico_PercCnc), 4, 0, ",", "")), ((edtServico_PercCnc_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1536Servico_PercCnc), "ZZZ9")) : context.localUtil.Format( (decimal)(A1536Servico_PercCnc), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,69);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtServico_PercCnc_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtServico_PercCnc_Enabled, 0, "text", "", 40, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "center", false, "HLP_ServicoProcesso.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblServico_perccnc_righttext_Internalname, "�%", "", "", lblServico_perccnc_righttext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ServicoProcesso.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_66_3X130e( true) ;
         }
         else
         {
            wb_table8_66_3X130e( false) ;
         }
      }

      protected void wb_table7_56_3X130( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedservico_percpgm_Internalname, tblTablemergedservico_percpgm_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtServico_PercPgm_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1535Servico_PercPgm), 4, 0, ",", "")), ((edtServico_PercPgm_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1535Servico_PercPgm), "ZZZ9")) : context.localUtil.Format( (decimal)(A1535Servico_PercPgm), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,59);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtServico_PercPgm_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtServico_PercPgm_Enabled, 0, "text", "", 40, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "center", false, "HLP_ServicoProcesso.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblServico_percpgm_righttext_Internalname, "�%", "", "", lblServico_percpgm_righttext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ServicoProcesso.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_56_3X130e( true) ;
         }
         else
         {
            wb_table7_56_3X130e( false) ;
         }
      }

      protected void wb_table6_46_3X130( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedservico_perctmp_Internalname, tblTablemergedservico_perctmp_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtServico_PercTmp_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1534Servico_PercTmp), 4, 0, ",", "")), ((edtServico_PercTmp_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1534Servico_PercTmp), "ZZZ9")) : context.localUtil.Format( (decimal)(A1534Servico_PercTmp), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,49);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtServico_PercTmp_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtServico_PercTmp_Enabled, 0, "text", "", 40, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "center", false, "HLP_ServicoProcesso.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblServico_perctmp_righttext_Internalname, "�%", "", "", lblServico_perctmp_righttext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ServicoProcesso.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_46_3X130e( true) ;
         }
         else
         {
            wb_table6_46_3X130e( false) ;
         }
      }

      protected void wb_table5_19_3X130( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedservicofluxo_ordem_Internalname, tblTablemergedservicofluxo_ordem_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 22,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavServicofluxo_ordem_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV9ServicoFluxo_Ordem), 3, 0, ",", "")), ((edtavServicofluxo_ordem_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV9ServicoFluxo_Ordem), "ZZ9")) : context.localUtil.Format( (decimal)(AV9ServicoFluxo_Ordem), "ZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,22);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavServicofluxo_ordem_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtavServicofluxo_ordem_Enabled, 0, "text", "", 50, "px", 1, "row", 3, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ServicoProcesso.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockmanterordem_Internalname, "", "", "", lblTextblockmanterordem_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ServicoProcesso.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Radio button */
            ClassString = "BootstrapAttributeGrayCheckBox";
            StyleString = "";
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 26,'',false,'',0)\"";
            GxWebStd.gx_radio_ctrl( context, radavManterordem, radavManterordem_Internalname, StringUtil.BoolToStr( AV14ManterOrdem), "", radavManterordem.Visible, radavManterordem.Enabled, 0, 0, StyleString, ClassString, "", 0, radavManterordem_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", TempTags+" onclick=\"gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,26);\"", "HLP_ServicoProcesso.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_19_3X130e( true) ;
         }
         else
         {
            wb_table5_19_3X130e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E113X2 */
         E113X2 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               if ( ( ( context.localUtil.CToN( cgiGet( edtavServicofluxo_ordem_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavServicofluxo_ordem_Internalname), ",", ".") > Convert.ToDecimal( 999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vSERVICOFLUXO_ORDEM");
                  AnyError = 1;
                  GX_FocusControl = edtavServicofluxo_ordem_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  AV9ServicoFluxo_Ordem = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9ServicoFluxo_Ordem", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9ServicoFluxo_Ordem), 3, 0)));
               }
               else
               {
                  AV9ServicoFluxo_Ordem = (short)(context.localUtil.CToN( cgiGet( edtavServicofluxo_ordem_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9ServicoFluxo_Ordem", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9ServicoFluxo_Ordem), 3, 0)));
               }
               AV14ManterOrdem = StringUtil.StrToBool( cgiGet( radavManterordem_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14ManterOrdem", AV14ManterOrdem);
               A608Servico_Nome = StringUtil.Upper( cgiGet( edtServico_Nome_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A608Servico_Nome", A608Servico_Nome);
               A605Servico_Sigla = StringUtil.Upper( cgiGet( edtServico_Sigla_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A605Servico_Sigla", A605Servico_Sigla);
               A156Servico_Descricao = cgiGet( edtServico_Descricao_Internalname);
               n156Servico_Descricao = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A156Servico_Descricao", A156Servico_Descricao);
               n156Servico_Descricao = (String.IsNullOrEmpty(StringUtil.RTrim( A156Servico_Descricao)) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtServico_PercTmp_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtServico_PercTmp_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "SERVICO_PERCTMP");
                  AnyError = 1;
                  GX_FocusControl = edtServico_PercTmp_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1534Servico_PercTmp = 0;
                  n1534Servico_PercTmp = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1534Servico_PercTmp", StringUtil.LTrim( StringUtil.Str( (decimal)(A1534Servico_PercTmp), 4, 0)));
               }
               else
               {
                  A1534Servico_PercTmp = (short)(context.localUtil.CToN( cgiGet( edtServico_PercTmp_Internalname), ",", "."));
                  n1534Servico_PercTmp = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1534Servico_PercTmp", StringUtil.LTrim( StringUtil.Str( (decimal)(A1534Servico_PercTmp), 4, 0)));
               }
               n1534Servico_PercTmp = ((0==A1534Servico_PercTmp) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtServico_PercPgm_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtServico_PercPgm_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "SERVICO_PERCPGM");
                  AnyError = 1;
                  GX_FocusControl = edtServico_PercPgm_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1535Servico_PercPgm = 0;
                  n1535Servico_PercPgm = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1535Servico_PercPgm", StringUtil.LTrim( StringUtil.Str( (decimal)(A1535Servico_PercPgm), 4, 0)));
               }
               else
               {
                  A1535Servico_PercPgm = (short)(context.localUtil.CToN( cgiGet( edtServico_PercPgm_Internalname), ",", "."));
                  n1535Servico_PercPgm = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1535Servico_PercPgm", StringUtil.LTrim( StringUtil.Str( (decimal)(A1535Servico_PercPgm), 4, 0)));
               }
               n1535Servico_PercPgm = ((0==A1535Servico_PercPgm) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtServico_PercCnc_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtServico_PercCnc_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "SERVICO_PERCCNC");
                  AnyError = 1;
                  GX_FocusControl = edtServico_PercCnc_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1536Servico_PercCnc = 0;
                  n1536Servico_PercCnc = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1536Servico_PercCnc", StringUtil.LTrim( StringUtil.Str( (decimal)(A1536Servico_PercCnc), 4, 0)));
               }
               else
               {
                  A1536Servico_PercCnc = (short)(context.localUtil.CToN( cgiGet( edtServico_PercCnc_Internalname), ",", "."));
                  n1536Servico_PercCnc = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1536Servico_PercCnc", StringUtil.LTrim( StringUtil.Str( (decimal)(A1536Servico_PercCnc), 4, 0)));
               }
               n1536Servico_PercCnc = ((0==A1536Servico_PercCnc) ? true : false);
               A155Servico_Codigo = (int)(context.localUtil.CToN( cgiGet( edtServico_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
               AV10ServicoFluxo_OrdemOldValue = (short)(context.localUtil.CToN( cgiGet( edtavServicofluxo_ordemoldvalue_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10ServicoFluxo_OrdemOldValue", StringUtil.LTrim( StringUtil.Str( (decimal)(AV10ServicoFluxo_OrdemOldValue), 3, 0)));
               /* Read saved values. */
               Z155Servico_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z155Servico_Codigo"), ",", "."));
               Z608Servico_Nome = cgiGet( "Z608Servico_Nome");
               Z605Servico_Sigla = cgiGet( "Z605Servico_Sigla");
               Z1534Servico_PercTmp = (short)(context.localUtil.CToN( cgiGet( "Z1534Servico_PercTmp"), ",", "."));
               n1534Servico_PercTmp = ((0==A1534Servico_PercTmp) ? true : false);
               Z1535Servico_PercPgm = (short)(context.localUtil.CToN( cgiGet( "Z1535Servico_PercPgm"), ",", "."));
               n1535Servico_PercPgm = ((0==A1535Servico_PercPgm) ? true : false);
               Z1536Servico_PercCnc = (short)(context.localUtil.CToN( cgiGet( "Z1536Servico_PercCnc"), ",", "."));
               n1536Servico_PercCnc = ((0==A1536Servico_PercCnc) ? true : false);
               Z2131Servico_PausaSLA = StringUtil.StrToBool( cgiGet( "Z2131Servico_PausaSLA"));
               Z1635Servico_IsPublico = StringUtil.StrToBool( cgiGet( "Z1635Servico_IsPublico"));
               Z1530Servico_TipoHierarquia = (short)(context.localUtil.CToN( cgiGet( "Z1530Servico_TipoHierarquia"), ",", "."));
               n1530Servico_TipoHierarquia = ((0==A1530Servico_TipoHierarquia) ? true : false);
               Z632Servico_Ativo = StringUtil.StrToBool( cgiGet( "Z632Servico_Ativo"));
               Z631Servico_Vinculado = (int)(context.localUtil.CToN( cgiGet( "Z631Servico_Vinculado"), ",", "."));
               n631Servico_Vinculado = ((0==A631Servico_Vinculado) ? true : false);
               A2131Servico_PausaSLA = StringUtil.StrToBool( cgiGet( "Z2131Servico_PausaSLA"));
               A1635Servico_IsPublico = StringUtil.StrToBool( cgiGet( "Z1635Servico_IsPublico"));
               A1530Servico_TipoHierarquia = (short)(context.localUtil.CToN( cgiGet( "Z1530Servico_TipoHierarquia"), ",", "."));
               n1530Servico_TipoHierarquia = false;
               n1530Servico_TipoHierarquia = ((0==A1530Servico_TipoHierarquia) ? true : false);
               A632Servico_Ativo = StringUtil.StrToBool( cgiGet( "Z632Servico_Ativo"));
               A631Servico_Vinculado = (int)(context.localUtil.CToN( cgiGet( "Z631Servico_Vinculado"), ",", "."));
               n631Servico_Vinculado = false;
               n631Servico_Vinculado = ((0==A631Servico_Vinculado) ? true : false);
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               N631Servico_Vinculado = (int)(context.localUtil.CToN( cgiGet( "N631Servico_Vinculado"), ",", "."));
               n631Servico_Vinculado = ((0==A631Servico_Vinculado) ? true : false);
               AV8Servico_Codigo = (int)(context.localUtil.CToN( cgiGet( "vSERVICO_CODIGO"), ",", "."));
               AV16Insert_Servico_Vinculado = (int)(context.localUtil.CToN( cgiGet( "vINSERT_SERVICO_VINCULADO"), ",", "."));
               A631Servico_Vinculado = (int)(context.localUtil.CToN( cgiGet( "SERVICO_VINCULADO"), ",", "."));
               n631Servico_Vinculado = ((0==A631Servico_Vinculado) ? true : false);
               A1557Servico_VincSigla = cgiGet( "SERVICO_VINCSIGLA");
               n1557Servico_VincSigla = false;
               Gx_BScreen = (short)(context.localUtil.CToN( cgiGet( "vGXBSCREEN"), ",", "."));
               A2131Servico_PausaSLA = StringUtil.StrToBool( cgiGet( "SERVICO_PAUSASLA"));
               A1635Servico_IsPublico = StringUtil.StrToBool( cgiGet( "SERVICO_ISPUBLICO"));
               A1530Servico_TipoHierarquia = (short)(context.localUtil.CToN( cgiGet( "SERVICO_TIPOHIERARQUIA"), ",", "."));
               n1530Servico_TipoHierarquia = ((0==A1530Servico_TipoHierarquia) ? true : false);
               A632Servico_Ativo = StringUtil.StrToBool( cgiGet( "SERVICO_ATIVO"));
               AV18Pgmname = cgiGet( "vPGMNAME");
               Gx_mode = cgiGet( "vMODE");
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "ServicoProcesso";
               AV10ServicoFluxo_OrdemOldValue = (short)(context.localUtil.CToN( cgiGet( edtavServicofluxo_ordemoldvalue_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10ServicoFluxo_OrdemOldValue", StringUtil.LTrim( StringUtil.Str( (decimal)(AV10ServicoFluxo_OrdemOldValue), 3, 0)));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(AV10ServicoFluxo_OrdemOldValue), "ZZ9");
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A631Servico_Vinculado), "ZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + StringUtil.BoolToStr( A2131Servico_PausaSLA);
               forbiddenHiddens = forbiddenHiddens + StringUtil.BoolToStr( A1635Servico_IsPublico);
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1530Servico_TipoHierarquia), "ZZZ9");
               forbiddenHiddens = forbiddenHiddens + StringUtil.BoolToStr( A632Servico_Ativo);
               hsh = cgiGet( "hsh");
               if ( ( ! ( ( A155Servico_Codigo != Z155Servico_Codigo ) ) || ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ) && ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("servicoprocesso:[SecurityCheckFailed value for]"+"ServicoFluxo_OrdemOldValue:"+context.localUtil.Format( (decimal)(AV10ServicoFluxo_OrdemOldValue), "ZZ9"));
                  GXUtil.WriteLog("servicoprocesso:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GXUtil.WriteLog("servicoprocesso:[SecurityCheckFailed value for]"+"Servico_Vinculado:"+context.localUtil.Format( (decimal)(A631Servico_Vinculado), "ZZZZZ9"));
                  GXUtil.WriteLog("servicoprocesso:[SecurityCheckFailed value for]"+"Servico_PausaSLA:"+StringUtil.BoolToStr( A2131Servico_PausaSLA));
                  GXUtil.WriteLog("servicoprocesso:[SecurityCheckFailed value for]"+"Servico_IsPublico:"+StringUtil.BoolToStr( A1635Servico_IsPublico));
                  GXUtil.WriteLog("servicoprocesso:[SecurityCheckFailed value for]"+"Servico_TipoHierarquia:"+context.localUtil.Format( (decimal)(A1530Servico_TipoHierarquia), "ZZZ9"));
                  GXUtil.WriteLog("servicoprocesso:[SecurityCheckFailed value for]"+"Servico_Ativo:"+StringUtil.BoolToStr( A632Servico_Ativo));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  A155Servico_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode130 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     Gx_mode = sMode130;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound130 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_3X0( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_trn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "SERVICO_CODIGO");
                        AnyError = 1;
                        GX_FocusControl = edtServico_Codigo_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E113X2 */
                           E113X2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E123X2 */
                           E123X2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VSERVICOFLUXO_ORDEM.ISVALID") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E133X2 */
                           E133X2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E123X2 */
            E123X2 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll3X130( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         bttBtn_trn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtn_trn_delete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_trn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
            }
            DisableAttributes3X130( ) ;
         }
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavServicofluxo_ordem_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavServicofluxo_ordem_Enabled), 5, 0)));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, radavManterordem_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(radavManterordem.Enabled), 5, 0)));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavServicofluxo_ordemoldvalue_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavServicofluxo_ordemoldvalue_Enabled), 5, 0)));
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_3X0( )
      {
         BeforeValidate3X130( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls3X130( ) ;
            }
            else
            {
               CheckExtendedTable3X130( ) ;
               CloseExtendedTableCursors3X130( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         }
      }

      protected void ResetCaption3X0( )
      {
      }

      protected void E113X2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV13WWPContext) ;
         AV11TrnContext.FromXml(AV12WebSession.Get("TrnContext"), "");
         if ( ( StringUtil.StrCmp(AV11TrnContext.gxTpr_Transactionname, AV18Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV19GXV1 = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV19GXV1), 8, 0)));
            while ( AV19GXV1 <= AV11TrnContext.gxTpr_Attributes.Count )
            {
               AV17TrnContextAtt = ((wwpbaseobjects.SdtWWPTransactionContext_Attribute)AV11TrnContext.gxTpr_Attributes.Item(AV19GXV1));
               if ( StringUtil.StrCmp(AV17TrnContextAtt.gxTpr_Attributename, "Servico_Vinculado") == 0 )
               {
                  AV16Insert_Servico_Vinculado = (int)(NumberUtil.Val( AV17TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16Insert_Servico_Vinculado", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16Insert_Servico_Vinculado), 6, 0)));
               }
               AV19GXV1 = (int)(AV19GXV1+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV19GXV1), 8, 0)));
            }
         }
         edtServico_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtServico_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtServico_Codigo_Visible), 5, 0)));
         edtavServicofluxo_ordemoldvalue_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavServicofluxo_ordemoldvalue_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavServicofluxo_ordemoldvalue_Visible), 5, 0)));
         GX_FocusControl = bttBtn_trn_cancel_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         context.DoAjaxSetFocus(GX_FocusControl);
         AV9ServicoFluxo_Ordem = AV7Ordem;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9ServicoFluxo_Ordem", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9ServicoFluxo_Ordem), 3, 0)));
         AV10ServicoFluxo_OrdemOldValue = AV9ServicoFluxo_Ordem;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10ServicoFluxo_OrdemOldValue", StringUtil.LTrim( StringUtil.Str( (decimal)(AV10ServicoFluxo_OrdemOldValue), 3, 0)));
         radavManterordem.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, radavManterordem_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(radavManterordem.Visible), 5, 0)));
      }

      protected void E123X2( )
      {
         /* After Trn Routine */
         if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
         {
            if ( AV9ServicoFluxo_Ordem != AV10ServicoFluxo_OrdemOldValue )
            {
               if ( ! AV14ManterOrdem )
               {
                  if ( AV9ServicoFluxo_Ordem > AV10ServicoFluxo_OrdemOldValue )
                  {
                     new prc_reordenafluxo(context ).execute(  "S",  A155Servico_Codigo,  AV10ServicoFluxo_OrdemOldValue,  AV9ServicoFluxo_Ordem,  "+") ;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10ServicoFluxo_OrdemOldValue", StringUtil.LTrim( StringUtil.Str( (decimal)(AV10ServicoFluxo_OrdemOldValue), 3, 0)));
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9ServicoFluxo_Ordem", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9ServicoFluxo_Ordem), 3, 0)));
                  }
                  else
                  {
                     new prc_reordenafluxo(context ).execute(  "S",  A155Servico_Codigo,  AV9ServicoFluxo_Ordem,  AV10ServicoFluxo_OrdemOldValue,  "-") ;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9ServicoFluxo_Ordem", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9ServicoFluxo_Ordem), 3, 0)));
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10ServicoFluxo_OrdemOldValue", StringUtil.LTrim( StringUtil.Str( (decimal)(AV10ServicoFluxo_OrdemOldValue), 3, 0)));
                  }
               }
            }
         }
         if ( false )
         {
            context.setWebReturnParms(new Object[] {(short)AV7Ordem});
            context.wjLocDisableFrm = 1;
            context.nUserReturn = 1;
            returnInSub = true;
            if (true) return;
         }
         else
         {
            context.setWebReturnParms(new Object[] {(short)AV7Ordem});
            context.wjLocDisableFrm = 1;
            context.nUserReturn = 1;
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E133X2( )
      {
         /* Servicofluxo_ordem_Isvalid Routine */
         new prc_servicosdaordem(context ).execute( ref  A155Servico_Codigo, ref  AV9ServicoFluxo_Ordem, out  AV15TemOutros) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9ServicoFluxo_Ordem", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9ServicoFluxo_Ordem), 3, 0)));
         radavManterordem.Visible = (AV15TemOutros ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, radavManterordem_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(radavManterordem.Visible), 5, 0)));
      }

      protected void ZM3X130( short GX_JID )
      {
         if ( ( GX_JID == 12 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z608Servico_Nome = T003X3_A608Servico_Nome[0];
               Z605Servico_Sigla = T003X3_A605Servico_Sigla[0];
               Z1534Servico_PercTmp = T003X3_A1534Servico_PercTmp[0];
               Z1535Servico_PercPgm = T003X3_A1535Servico_PercPgm[0];
               Z1536Servico_PercCnc = T003X3_A1536Servico_PercCnc[0];
               Z2131Servico_PausaSLA = T003X3_A2131Servico_PausaSLA[0];
               Z1635Servico_IsPublico = T003X3_A1635Servico_IsPublico[0];
               Z1530Servico_TipoHierarquia = T003X3_A1530Servico_TipoHierarquia[0];
               Z632Servico_Ativo = T003X3_A632Servico_Ativo[0];
               Z631Servico_Vinculado = T003X3_A631Servico_Vinculado[0];
            }
            else
            {
               Z608Servico_Nome = A608Servico_Nome;
               Z605Servico_Sigla = A605Servico_Sigla;
               Z1534Servico_PercTmp = A1534Servico_PercTmp;
               Z1535Servico_PercPgm = A1535Servico_PercPgm;
               Z1536Servico_PercCnc = A1536Servico_PercCnc;
               Z2131Servico_PausaSLA = A2131Servico_PausaSLA;
               Z1635Servico_IsPublico = A1635Servico_IsPublico;
               Z1530Servico_TipoHierarquia = A1530Servico_TipoHierarquia;
               Z632Servico_Ativo = A632Servico_Ativo;
               Z631Servico_Vinculado = A631Servico_Vinculado;
            }
         }
         if ( GX_JID == -12 )
         {
            Z155Servico_Codigo = A155Servico_Codigo;
            Z608Servico_Nome = A608Servico_Nome;
            Z605Servico_Sigla = A605Servico_Sigla;
            Z156Servico_Descricao = A156Servico_Descricao;
            Z1534Servico_PercTmp = A1534Servico_PercTmp;
            Z1535Servico_PercPgm = A1535Servico_PercPgm;
            Z1536Servico_PercCnc = A1536Servico_PercCnc;
            Z2131Servico_PausaSLA = A2131Servico_PausaSLA;
            Z1635Servico_IsPublico = A1635Servico_IsPublico;
            Z1530Servico_TipoHierarquia = A1530Servico_TipoHierarquia;
            Z632Servico_Ativo = A632Servico_Ativo;
            Z631Servico_Vinculado = A631Servico_Vinculado;
            Z1557Servico_VincSigla = A1557Servico_VincSigla;
         }
      }

      protected void standaloneNotModal( )
      {
         edtServico_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtServico_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtServico_Codigo_Enabled), 5, 0)));
         Gx_BScreen = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         AV18Pgmname = "ServicoProcesso";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Pgmname", AV18Pgmname);
         edtServico_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtServico_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtServico_Codigo_Enabled), 5, 0)));
         bttBtn_trn_delete_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         if ( ! (0==AV8Servico_Codigo) )
         {
            A155Servico_Codigo = AV8Servico_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
         }
      }

      protected void standaloneModal( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV16Insert_Servico_Vinculado) )
         {
            A631Servico_Vinculado = AV16Insert_Servico_Vinculado;
            n631Servico_Vinculado = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A631Servico_Vinculado", StringUtil.LTrim( StringUtil.Str( (decimal)(A631Servico_Vinculado), 6, 0)));
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (false==A2131Servico_PausaSLA) && ( Gx_BScreen == 0 ) )
         {
            A2131Servico_PausaSLA = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2131Servico_PausaSLA", A2131Servico_PausaSLA);
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (false==A1635Servico_IsPublico) && ( Gx_BScreen == 0 ) )
         {
            A1635Servico_IsPublico = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1635Servico_IsPublico", A1635Servico_IsPublico);
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (0==A1530Servico_TipoHierarquia) && ( Gx_BScreen == 0 ) )
         {
            A1530Servico_TipoHierarquia = 1;
            n1530Servico_TipoHierarquia = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1530Servico_TipoHierarquia", StringUtil.LTrim( StringUtil.Str( (decimal)(A1530Servico_TipoHierarquia), 4, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (false==A632Servico_Ativo) && ( Gx_BScreen == 0 ) )
         {
            A632Servico_Ativo = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A632Servico_Ativo", A632Servico_Ativo);
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
            /* Using cursor T003X4 */
            pr_default.execute(2, new Object[] {n631Servico_Vinculado, A631Servico_Vinculado});
            A1557Servico_VincSigla = T003X4_A1557Servico_VincSigla[0];
            n1557Servico_VincSigla = T003X4_n1557Servico_VincSigla[0];
            pr_default.close(2);
            lblTbservico_Caption = "��"+A1557Servico_VincSigla;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbservico_Internalname, "Caption", lblTbservico_Caption);
         }
      }

      protected void Load3X130( )
      {
         /* Using cursor T003X5 */
         pr_default.execute(3, new Object[] {A155Servico_Codigo});
         if ( (pr_default.getStatus(3) != 101) )
         {
            RcdFound130 = 1;
            A608Servico_Nome = T003X5_A608Servico_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A608Servico_Nome", A608Servico_Nome);
            A605Servico_Sigla = T003X5_A605Servico_Sigla[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A605Servico_Sigla", A605Servico_Sigla);
            A156Servico_Descricao = T003X5_A156Servico_Descricao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A156Servico_Descricao", A156Servico_Descricao);
            n156Servico_Descricao = T003X5_n156Servico_Descricao[0];
            A1534Servico_PercTmp = T003X5_A1534Servico_PercTmp[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1534Servico_PercTmp", StringUtil.LTrim( StringUtil.Str( (decimal)(A1534Servico_PercTmp), 4, 0)));
            n1534Servico_PercTmp = T003X5_n1534Servico_PercTmp[0];
            A1535Servico_PercPgm = T003X5_A1535Servico_PercPgm[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1535Servico_PercPgm", StringUtil.LTrim( StringUtil.Str( (decimal)(A1535Servico_PercPgm), 4, 0)));
            n1535Servico_PercPgm = T003X5_n1535Servico_PercPgm[0];
            A1536Servico_PercCnc = T003X5_A1536Servico_PercCnc[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1536Servico_PercCnc", StringUtil.LTrim( StringUtil.Str( (decimal)(A1536Servico_PercCnc), 4, 0)));
            n1536Servico_PercCnc = T003X5_n1536Servico_PercCnc[0];
            A1557Servico_VincSigla = T003X5_A1557Servico_VincSigla[0];
            n1557Servico_VincSigla = T003X5_n1557Servico_VincSigla[0];
            A2131Servico_PausaSLA = T003X5_A2131Servico_PausaSLA[0];
            A1635Servico_IsPublico = T003X5_A1635Servico_IsPublico[0];
            A1530Servico_TipoHierarquia = T003X5_A1530Servico_TipoHierarquia[0];
            n1530Servico_TipoHierarquia = T003X5_n1530Servico_TipoHierarquia[0];
            A632Servico_Ativo = T003X5_A632Servico_Ativo[0];
            A631Servico_Vinculado = T003X5_A631Servico_Vinculado[0];
            n631Servico_Vinculado = T003X5_n631Servico_Vinculado[0];
            ZM3X130( -12) ;
         }
         pr_default.close(3);
         OnLoadActions3X130( ) ;
      }

      protected void OnLoadActions3X130( )
      {
         lblTbservico_Caption = "��"+A1557Servico_VincSigla;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbservico_Internalname, "Caption", lblTbservico_Caption);
      }

      protected void CheckExtendedTable3X130( )
      {
         Gx_BScreen = 1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         standaloneModal( ) ;
         /* Using cursor T003X4 */
         pr_default.execute(2, new Object[] {n631Servico_Vinculado, A631Servico_Vinculado});
         if ( (pr_default.getStatus(2) == 101) )
         {
            if ( ! ( (0==A631Servico_Vinculado) ) )
            {
               GX_msglist.addItem("N�o existe 'Servico_Servico Vinculado'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         A1557Servico_VincSigla = T003X4_A1557Servico_VincSigla[0];
         n1557Servico_VincSigla = T003X4_n1557Servico_VincSigla[0];
         pr_default.close(2);
         lblTbservico_Caption = "��"+A1557Servico_VincSigla;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbservico_Internalname, "Caption", lblTbservico_Caption);
      }

      protected void CloseExtendedTableCursors3X130( )
      {
         pr_default.close(2);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_13( int A631Servico_Vinculado )
      {
         /* Using cursor T003X6 */
         pr_default.execute(4, new Object[] {n631Servico_Vinculado, A631Servico_Vinculado});
         if ( (pr_default.getStatus(4) == 101) )
         {
            if ( ! ( (0==A631Servico_Vinculado) ) )
            {
               GX_msglist.addItem("N�o existe 'Servico_Servico Vinculado'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         A1557Servico_VincSigla = T003X6_A1557Servico_VincSigla[0];
         n1557Servico_VincSigla = T003X6_n1557Servico_VincSigla[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A1557Servico_VincSigla))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(4) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(4);
      }

      protected void GetKey3X130( )
      {
         /* Using cursor T003X7 */
         pr_default.execute(5, new Object[] {A155Servico_Codigo});
         if ( (pr_default.getStatus(5) != 101) )
         {
            RcdFound130 = 1;
         }
         else
         {
            RcdFound130 = 0;
         }
         pr_default.close(5);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T003X3 */
         pr_default.execute(1, new Object[] {A155Servico_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM3X130( 12) ;
            RcdFound130 = 1;
            A155Servico_Codigo = T003X3_A155Servico_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
            A608Servico_Nome = T003X3_A608Servico_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A608Servico_Nome", A608Servico_Nome);
            A605Servico_Sigla = T003X3_A605Servico_Sigla[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A605Servico_Sigla", A605Servico_Sigla);
            A156Servico_Descricao = T003X3_A156Servico_Descricao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A156Servico_Descricao", A156Servico_Descricao);
            n156Servico_Descricao = T003X3_n156Servico_Descricao[0];
            A1534Servico_PercTmp = T003X3_A1534Servico_PercTmp[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1534Servico_PercTmp", StringUtil.LTrim( StringUtil.Str( (decimal)(A1534Servico_PercTmp), 4, 0)));
            n1534Servico_PercTmp = T003X3_n1534Servico_PercTmp[0];
            A1535Servico_PercPgm = T003X3_A1535Servico_PercPgm[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1535Servico_PercPgm", StringUtil.LTrim( StringUtil.Str( (decimal)(A1535Servico_PercPgm), 4, 0)));
            n1535Servico_PercPgm = T003X3_n1535Servico_PercPgm[0];
            A1536Servico_PercCnc = T003X3_A1536Servico_PercCnc[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1536Servico_PercCnc", StringUtil.LTrim( StringUtil.Str( (decimal)(A1536Servico_PercCnc), 4, 0)));
            n1536Servico_PercCnc = T003X3_n1536Servico_PercCnc[0];
            A2131Servico_PausaSLA = T003X3_A2131Servico_PausaSLA[0];
            A1635Servico_IsPublico = T003X3_A1635Servico_IsPublico[0];
            A1530Servico_TipoHierarquia = T003X3_A1530Servico_TipoHierarquia[0];
            n1530Servico_TipoHierarquia = T003X3_n1530Servico_TipoHierarquia[0];
            A632Servico_Ativo = T003X3_A632Servico_Ativo[0];
            A631Servico_Vinculado = T003X3_A631Servico_Vinculado[0];
            n631Servico_Vinculado = T003X3_n631Servico_Vinculado[0];
            Z155Servico_Codigo = A155Servico_Codigo;
            sMode130 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load3X130( ) ;
            if ( AnyError == 1 )
            {
               RcdFound130 = 0;
               InitializeNonKey3X130( ) ;
            }
            Gx_mode = sMode130;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound130 = 0;
            InitializeNonKey3X130( ) ;
            sMode130 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal( ) ;
            Gx_mode = sMode130;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey3X130( ) ;
         if ( RcdFound130 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound130 = 0;
         /* Using cursor T003X8 */
         pr_default.execute(6, new Object[] {A155Servico_Codigo});
         if ( (pr_default.getStatus(6) != 101) )
         {
            while ( (pr_default.getStatus(6) != 101) && ( ( T003X8_A155Servico_Codigo[0] < A155Servico_Codigo ) ) )
            {
               pr_default.readNext(6);
            }
            if ( (pr_default.getStatus(6) != 101) && ( ( T003X8_A155Servico_Codigo[0] > A155Servico_Codigo ) ) )
            {
               A155Servico_Codigo = T003X8_A155Servico_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
               RcdFound130 = 1;
            }
         }
         pr_default.close(6);
      }

      protected void move_previous( )
      {
         RcdFound130 = 0;
         /* Using cursor T003X9 */
         pr_default.execute(7, new Object[] {A155Servico_Codigo});
         if ( (pr_default.getStatus(7) != 101) )
         {
            while ( (pr_default.getStatus(7) != 101) && ( ( T003X9_A155Servico_Codigo[0] > A155Servico_Codigo ) ) )
            {
               pr_default.readNext(7);
            }
            if ( (pr_default.getStatus(7) != 101) && ( ( T003X9_A155Servico_Codigo[0] < A155Servico_Codigo ) ) )
            {
               A155Servico_Codigo = T003X9_A155Servico_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
               RcdFound130 = 1;
            }
         }
         pr_default.close(7);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey3X130( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtServico_Nome_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert3X130( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound130 == 1 )
            {
               if ( A155Servico_Codigo != Z155Servico_Codigo )
               {
                  A155Servico_Codigo = Z155Servico_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "SERVICO_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtServico_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtServico_Nome_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  Update3X130( ) ;
                  GX_FocusControl = edtServico_Nome_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A155Servico_Codigo != Z155Servico_Codigo )
               {
                  /* Insert record */
                  GX_FocusControl = edtServico_Nome_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert3X130( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "SERVICO_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtServico_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     /* Insert record */
                     GX_FocusControl = edtServico_Nome_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert3X130( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( A155Servico_Codigo != Z155Servico_Codigo )
         {
            A155Servico_Codigo = Z155Servico_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "SERVICO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtServico_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtServico_Nome_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency3X130( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T003X2 */
            pr_default.execute(0, new Object[] {A155Servico_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Servico"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            Gx_longc = false;
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z608Servico_Nome, T003X2_A608Servico_Nome[0]) != 0 ) || ( StringUtil.StrCmp(Z605Servico_Sigla, T003X2_A605Servico_Sigla[0]) != 0 ) || ( Z1534Servico_PercTmp != T003X2_A1534Servico_PercTmp[0] ) || ( Z1535Servico_PercPgm != T003X2_A1535Servico_PercPgm[0] ) || ( Z1536Servico_PercCnc != T003X2_A1536Servico_PercCnc[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z2131Servico_PausaSLA != T003X2_A2131Servico_PausaSLA[0] ) || ( Z1635Servico_IsPublico != T003X2_A1635Servico_IsPublico[0] ) || ( Z1530Servico_TipoHierarquia != T003X2_A1530Servico_TipoHierarquia[0] ) || ( Z632Servico_Ativo != T003X2_A632Servico_Ativo[0] ) || ( Z631Servico_Vinculado != T003X2_A631Servico_Vinculado[0] ) )
            {
               if ( StringUtil.StrCmp(Z608Servico_Nome, T003X2_A608Servico_Nome[0]) != 0 )
               {
                  GXUtil.WriteLog("servicoprocesso:[seudo value changed for attri]"+"Servico_Nome");
                  GXUtil.WriteLogRaw("Old: ",Z608Servico_Nome);
                  GXUtil.WriteLogRaw("Current: ",T003X2_A608Servico_Nome[0]);
               }
               if ( StringUtil.StrCmp(Z605Servico_Sigla, T003X2_A605Servico_Sigla[0]) != 0 )
               {
                  GXUtil.WriteLog("servicoprocesso:[seudo value changed for attri]"+"Servico_Sigla");
                  GXUtil.WriteLogRaw("Old: ",Z605Servico_Sigla);
                  GXUtil.WriteLogRaw("Current: ",T003X2_A605Servico_Sigla[0]);
               }
               if ( Z1534Servico_PercTmp != T003X2_A1534Servico_PercTmp[0] )
               {
                  GXUtil.WriteLog("servicoprocesso:[seudo value changed for attri]"+"Servico_PercTmp");
                  GXUtil.WriteLogRaw("Old: ",Z1534Servico_PercTmp);
                  GXUtil.WriteLogRaw("Current: ",T003X2_A1534Servico_PercTmp[0]);
               }
               if ( Z1535Servico_PercPgm != T003X2_A1535Servico_PercPgm[0] )
               {
                  GXUtil.WriteLog("servicoprocesso:[seudo value changed for attri]"+"Servico_PercPgm");
                  GXUtil.WriteLogRaw("Old: ",Z1535Servico_PercPgm);
                  GXUtil.WriteLogRaw("Current: ",T003X2_A1535Servico_PercPgm[0]);
               }
               if ( Z1536Servico_PercCnc != T003X2_A1536Servico_PercCnc[0] )
               {
                  GXUtil.WriteLog("servicoprocesso:[seudo value changed for attri]"+"Servico_PercCnc");
                  GXUtil.WriteLogRaw("Old: ",Z1536Servico_PercCnc);
                  GXUtil.WriteLogRaw("Current: ",T003X2_A1536Servico_PercCnc[0]);
               }
               if ( Z2131Servico_PausaSLA != T003X2_A2131Servico_PausaSLA[0] )
               {
                  GXUtil.WriteLog("servicoprocesso:[seudo value changed for attri]"+"Servico_PausaSLA");
                  GXUtil.WriteLogRaw("Old: ",Z2131Servico_PausaSLA);
                  GXUtil.WriteLogRaw("Current: ",T003X2_A2131Servico_PausaSLA[0]);
               }
               if ( Z1635Servico_IsPublico != T003X2_A1635Servico_IsPublico[0] )
               {
                  GXUtil.WriteLog("servicoprocesso:[seudo value changed for attri]"+"Servico_IsPublico");
                  GXUtil.WriteLogRaw("Old: ",Z1635Servico_IsPublico);
                  GXUtil.WriteLogRaw("Current: ",T003X2_A1635Servico_IsPublico[0]);
               }
               if ( Z1530Servico_TipoHierarquia != T003X2_A1530Servico_TipoHierarquia[0] )
               {
                  GXUtil.WriteLog("servicoprocesso:[seudo value changed for attri]"+"Servico_TipoHierarquia");
                  GXUtil.WriteLogRaw("Old: ",Z1530Servico_TipoHierarquia);
                  GXUtil.WriteLogRaw("Current: ",T003X2_A1530Servico_TipoHierarquia[0]);
               }
               if ( Z632Servico_Ativo != T003X2_A632Servico_Ativo[0] )
               {
                  GXUtil.WriteLog("servicoprocesso:[seudo value changed for attri]"+"Servico_Ativo");
                  GXUtil.WriteLogRaw("Old: ",Z632Servico_Ativo);
                  GXUtil.WriteLogRaw("Current: ",T003X2_A632Servico_Ativo[0]);
               }
               if ( Z631Servico_Vinculado != T003X2_A631Servico_Vinculado[0] )
               {
                  GXUtil.WriteLog("servicoprocesso:[seudo value changed for attri]"+"Servico_Vinculado");
                  GXUtil.WriteLogRaw("Old: ",Z631Servico_Vinculado);
                  GXUtil.WriteLogRaw("Current: ",T003X2_A631Servico_Vinculado[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"Servico"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert3X130( )
      {
         BeforeValidate3X130( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable3X130( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM3X130( 0) ;
            CheckOptimisticConcurrency3X130( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm3X130( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert3X130( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T003X10 */
                     pr_default.execute(8, new Object[] {A608Servico_Nome, A605Servico_Sigla, n156Servico_Descricao, A156Servico_Descricao, n1534Servico_PercTmp, A1534Servico_PercTmp, n1535Servico_PercPgm, A1535Servico_PercPgm, n1536Servico_PercCnc, A1536Servico_PercCnc, A2131Servico_PausaSLA, A1635Servico_IsPublico, n1530Servico_TipoHierarquia, A1530Servico_TipoHierarquia, A632Servico_Ativo, n631Servico_Vinculado, A631Servico_Vinculado});
                     A155Servico_Codigo = T003X10_A155Servico_Codigo[0];
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
                     pr_default.close(8);
                     dsDefault.SmartCacheProvider.SetUpdated("Servico") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption3X0( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load3X130( ) ;
            }
            EndLevel3X130( ) ;
         }
         CloseExtendedTableCursors3X130( ) ;
      }

      protected void Update3X130( )
      {
         BeforeValidate3X130( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable3X130( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency3X130( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm3X130( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate3X130( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T003X11 */
                     pr_default.execute(9, new Object[] {A608Servico_Nome, A605Servico_Sigla, n156Servico_Descricao, A156Servico_Descricao, n1534Servico_PercTmp, A1534Servico_PercTmp, n1535Servico_PercPgm, A1535Servico_PercPgm, n1536Servico_PercCnc, A1536Servico_PercCnc, A2131Servico_PausaSLA, A1635Servico_IsPublico, n1530Servico_TipoHierarquia, A1530Servico_TipoHierarquia, A632Servico_Ativo, n631Servico_Vinculado, A631Servico_Vinculado, A155Servico_Codigo});
                     pr_default.close(9);
                     dsDefault.SmartCacheProvider.SetUpdated("Servico") ;
                     if ( (pr_default.getStatus(9) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Servico"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate3X130( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel3X130( ) ;
         }
         CloseExtendedTableCursors3X130( ) ;
      }

      protected void DeferredUpdate3X130( )
      {
      }

      protected void delete( )
      {
         BeforeValidate3X130( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency3X130( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls3X130( ) ;
            AfterConfirm3X130( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete3X130( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T003X12 */
                  pr_default.execute(10, new Object[] {A155Servico_Codigo});
                  pr_default.close(10);
                  dsDefault.SmartCacheProvider.SetUpdated("Servico") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                        {
                           if ( AnyError == 0 )
                           {
                              context.nUserReturn = 1;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode130 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel3X130( ) ;
         Gx_mode = sMode130;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls3X130( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor T003X13 */
            pr_default.execute(11, new Object[] {n631Servico_Vinculado, A631Servico_Vinculado});
            A1557Servico_VincSigla = T003X13_A1557Servico_VincSigla[0];
            n1557Servico_VincSigla = T003X13_n1557Servico_VincSigla[0];
            pr_default.close(11);
            lblTbservico_Caption = "��"+A1557Servico_VincSigla;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbservico_Internalname, "Caption", lblTbservico_Caption);
         }
         if ( AnyError == 0 )
         {
            /* Using cursor T003X14 */
            pr_default.execute(12, new Object[] {A155Servico_Codigo});
            if ( (pr_default.getStatus(12) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Servico"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(12);
            /* Using cursor T003X15 */
            pr_default.execute(13, new Object[] {A155Servico_Codigo});
            if ( (pr_default.getStatus(13) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Servico Fluxo"+" ("+"Servico Fluxo_Servico Posterior"+")"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(13);
            /* Using cursor T003X16 */
            pr_default.execute(14, new Object[] {A155Servico_Codigo});
            if ( (pr_default.getStatus(14) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Servico Fluxo"+" ("+"Servico_Servico Fluxo"+")"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(14);
            /* Using cursor T003X17 */
            pr_default.execute(15, new Object[] {A155Servico_Codigo});
            if ( (pr_default.getStatus(15) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Prioridades padr�o"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(15);
            /* Using cursor T003X18 */
            pr_default.execute(16, new Object[] {A155Servico_Codigo});
            if ( (pr_default.getStatus(16) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Sistemas do Servi�o"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(16);
            /* Using cursor T003X19 */
            pr_default.execute(17, new Object[] {A155Servico_Codigo});
            if ( (pr_default.getStatus(17) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Area de Trabalho"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(17);
            /* Using cursor T003X20 */
            pr_default.execute(18, new Object[] {A155Servico_Codigo});
            if ( (pr_default.getStatus(18) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Usuario Servicos"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(18);
            /* Using cursor T003X21 */
            pr_default.execute(19, new Object[] {A155Servico_Codigo});
            if ( (pr_default.getStatus(19) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Resultado das Contagens"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(19);
            /* Using cursor T003X22 */
            pr_default.execute(20, new Object[] {A155Servico_Codigo});
            if ( (pr_default.getStatus(20) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Solicitacoes"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(20);
            /* Using cursor T003X23 */
            pr_default.execute(21, new Object[] {A155Servico_Codigo});
            if ( (pr_default.getStatus(21) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Servico Ans"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(21);
            /* Using cursor T003X24 */
            pr_default.execute(22, new Object[] {A155Servico_Codigo});
            if ( (pr_default.getStatus(22) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contrato Servicos"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(22);
            /* Using cursor T003X25 */
            pr_default.execute(23, new Object[] {A155Servico_Codigo});
            if ( (pr_default.getStatus(23) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Check Lists"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(23);
            /* Using cursor T003X26 */
            pr_default.execute(24, new Object[] {A155Servico_Codigo});
            if ( (pr_default.getStatus(24) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Artefatos"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(24);
         }
      }

      protected void EndLevel3X130( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete3X130( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            pr_default.close(11);
            context.CommitDataStores( "ServicoProcesso");
            if ( AnyError == 0 )
            {
               ConfirmValues3X0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            pr_default.close(11);
            context.RollbackDataStores( "ServicoProcesso");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart3X130( )
      {
         /* Scan By routine */
         /* Using cursor T003X27 */
         pr_default.execute(25);
         RcdFound130 = 0;
         if ( (pr_default.getStatus(25) != 101) )
         {
            RcdFound130 = 1;
            A155Servico_Codigo = T003X27_A155Servico_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext3X130( )
      {
         /* Scan next routine */
         pr_default.readNext(25);
         RcdFound130 = 0;
         if ( (pr_default.getStatus(25) != 101) )
         {
            RcdFound130 = 1;
            A155Servico_Codigo = T003X27_A155Servico_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
         }
      }

      protected void ScanEnd3X130( )
      {
         pr_default.close(25);
      }

      protected void AfterConfirm3X130( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert3X130( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate3X130( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete3X130( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete3X130( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate3X130( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes3X130( )
      {
         edtavServicofluxo_ordem_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavServicofluxo_ordem_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavServicofluxo_ordem_Enabled), 5, 0)));
         radavManterordem.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, radavManterordem_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(radavManterordem.Enabled), 5, 0)));
         edtServico_Nome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtServico_Nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtServico_Nome_Enabled), 5, 0)));
         edtServico_Sigla_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtServico_Sigla_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtServico_Sigla_Enabled), 5, 0)));
         edtServico_Descricao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtServico_Descricao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtServico_Descricao_Enabled), 5, 0)));
         edtServico_PercTmp_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtServico_PercTmp_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtServico_PercTmp_Enabled), 5, 0)));
         edtServico_PercPgm_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtServico_PercPgm_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtServico_PercPgm_Enabled), 5, 0)));
         edtServico_PercCnc_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtServico_PercCnc_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtServico_PercCnc_Enabled), 5, 0)));
         edtServico_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtServico_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtServico_Codigo_Enabled), 5, 0)));
         edtavServicofluxo_ordemoldvalue_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavServicofluxo_ordemoldvalue_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavServicofluxo_ordemoldvalue_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues3X0( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20206216174339");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("servicoprocesso.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV8Servico_Codigo) + "," + UrlEncode("" +AV7Ordem)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z155Servico_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z155Servico_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z608Servico_Nome", StringUtil.RTrim( Z608Servico_Nome));
         GxWebStd.gx_hidden_field( context, "Z605Servico_Sigla", StringUtil.RTrim( Z605Servico_Sigla));
         GxWebStd.gx_hidden_field( context, "Z1534Servico_PercTmp", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1534Servico_PercTmp), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1535Servico_PercPgm", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1535Servico_PercPgm), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1536Servico_PercCnc", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1536Servico_PercCnc), 4, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "Z2131Servico_PausaSLA", Z2131Servico_PausaSLA);
         GxWebStd.gx_boolean_hidden_field( context, "Z1635Servico_IsPublico", Z1635Servico_IsPublico);
         GxWebStd.gx_hidden_field( context, "Z1530Servico_TipoHierarquia", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1530Servico_TipoHierarquia), 4, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "Z632Servico_Ativo", Z632Servico_Ativo);
         GxWebStd.gx_hidden_field( context, "Z631Servico_Vinculado", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z631Servico_Vinculado), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "N631Servico_Vinculado", StringUtil.LTrim( StringUtil.NToC( (decimal)(A631Servico_Vinculado), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vORDEM", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7Ordem), 3, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vSERVICO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV8Servico_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_SERVICO_VINCULADO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV16Insert_Servico_Vinculado), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "SERVICO_VINCULADO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A631Servico_Vinculado), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "SERVICO_VINCSIGLA", StringUtil.RTrim( A1557Servico_VincSigla));
         GxWebStd.gx_hidden_field( context, "vGXBSCREEN", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gx_BScreen), 1, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "SERVICO_PAUSASLA", A2131Servico_PausaSLA);
         GxWebStd.gx_boolean_hidden_field( context, "SERVICO_ISPUBLICO", A1635Servico_IsPublico);
         GxWebStd.gx_hidden_field( context, "SERVICO_TIPOHIERARQUIA", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1530Servico_TipoHierarquia), 4, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "SERVICO_ATIVO", A632Servico_Ativo);
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV18Pgmname));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vSERVICO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV8Servico_Codigo), "ZZZZZ9")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "ServicoProcesso";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(AV10ServicoFluxo_OrdemOldValue), "ZZ9");
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A631Servico_Vinculado), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + StringUtil.BoolToStr( A2131Servico_PausaSLA);
         forbiddenHiddens = forbiddenHiddens + StringUtil.BoolToStr( A1635Servico_IsPublico);
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1530Servico_TipoHierarquia), "ZZZ9");
         forbiddenHiddens = forbiddenHiddens + StringUtil.BoolToStr( A632Servico_Ativo);
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("servicoprocesso:[SendSecurityCheck value for]"+"ServicoFluxo_OrdemOldValue:"+context.localUtil.Format( (decimal)(AV10ServicoFluxo_OrdemOldValue), "ZZ9"));
         GXUtil.WriteLog("servicoprocesso:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
         GXUtil.WriteLog("servicoprocesso:[SendSecurityCheck value for]"+"Servico_Vinculado:"+context.localUtil.Format( (decimal)(A631Servico_Vinculado), "ZZZZZ9"));
         GXUtil.WriteLog("servicoprocesso:[SendSecurityCheck value for]"+"Servico_PausaSLA:"+StringUtil.BoolToStr( A2131Servico_PausaSLA));
         GXUtil.WriteLog("servicoprocesso:[SendSecurityCheck value for]"+"Servico_IsPublico:"+StringUtil.BoolToStr( A1635Servico_IsPublico));
         GXUtil.WriteLog("servicoprocesso:[SendSecurityCheck value for]"+"Servico_TipoHierarquia:"+context.localUtil.Format( (decimal)(A1530Servico_TipoHierarquia), "ZZZ9"));
         GXUtil.WriteLog("servicoprocesso:[SendSecurityCheck value for]"+"Servico_Ativo:"+StringUtil.BoolToStr( A632Servico_Ativo));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("servicoprocesso.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV8Servico_Codigo) + "," + UrlEncode("" +AV7Ordem) ;
      }

      public override String GetPgmname( )
      {
         return "ServicoProcesso" ;
      }

      public override String GetPgmdesc( )
      {
         return "Processo do Servi�o" ;
      }

      protected void InitializeNonKey3X130( )
      {
         A631Servico_Vinculado = 0;
         n631Servico_Vinculado = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A631Servico_Vinculado", StringUtil.LTrim( StringUtil.Str( (decimal)(A631Servico_Vinculado), 6, 0)));
         AV14ManterOrdem = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14ManterOrdem", AV14ManterOrdem);
         A608Servico_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A608Servico_Nome", A608Servico_Nome);
         A605Servico_Sigla = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A605Servico_Sigla", A605Servico_Sigla);
         A156Servico_Descricao = "";
         n156Servico_Descricao = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A156Servico_Descricao", A156Servico_Descricao);
         n156Servico_Descricao = (String.IsNullOrEmpty(StringUtil.RTrim( A156Servico_Descricao)) ? true : false);
         A1534Servico_PercTmp = 0;
         n1534Servico_PercTmp = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1534Servico_PercTmp", StringUtil.LTrim( StringUtil.Str( (decimal)(A1534Servico_PercTmp), 4, 0)));
         n1534Servico_PercTmp = ((0==A1534Servico_PercTmp) ? true : false);
         A1535Servico_PercPgm = 0;
         n1535Servico_PercPgm = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1535Servico_PercPgm", StringUtil.LTrim( StringUtil.Str( (decimal)(A1535Servico_PercPgm), 4, 0)));
         n1535Servico_PercPgm = ((0==A1535Servico_PercPgm) ? true : false);
         A1536Servico_PercCnc = 0;
         n1536Servico_PercCnc = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1536Servico_PercCnc", StringUtil.LTrim( StringUtil.Str( (decimal)(A1536Servico_PercCnc), 4, 0)));
         n1536Servico_PercCnc = ((0==A1536Servico_PercCnc) ? true : false);
         A1557Servico_VincSigla = "";
         n1557Servico_VincSigla = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1557Servico_VincSigla", A1557Servico_VincSigla);
         A2131Servico_PausaSLA = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2131Servico_PausaSLA", A2131Servico_PausaSLA);
         A1635Servico_IsPublico = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1635Servico_IsPublico", A1635Servico_IsPublico);
         A1530Servico_TipoHierarquia = 1;
         n1530Servico_TipoHierarquia = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1530Servico_TipoHierarquia", StringUtil.LTrim( StringUtil.Str( (decimal)(A1530Servico_TipoHierarquia), 4, 0)));
         A632Servico_Ativo = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A632Servico_Ativo", A632Servico_Ativo);
         Z608Servico_Nome = "";
         Z605Servico_Sigla = "";
         Z1534Servico_PercTmp = 0;
         Z1535Servico_PercPgm = 0;
         Z1536Servico_PercCnc = 0;
         Z2131Servico_PausaSLA = false;
         Z1635Servico_IsPublico = false;
         Z1530Servico_TipoHierarquia = 0;
         Z632Servico_Ativo = false;
         Z631Servico_Vinculado = 0;
      }

      protected void InitAll3X130( )
      {
         A155Servico_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A155Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0)));
         InitializeNonKey3X130( ) ;
      }

      protected void StandaloneModalInsert( )
      {
         A2131Servico_PausaSLA = i2131Servico_PausaSLA;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2131Servico_PausaSLA", A2131Servico_PausaSLA);
         A1635Servico_IsPublico = i1635Servico_IsPublico;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1635Servico_IsPublico", A1635Servico_IsPublico);
         A1530Servico_TipoHierarquia = i1530Servico_TipoHierarquia;
         n1530Servico_TipoHierarquia = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1530Servico_TipoHierarquia", StringUtil.LTrim( StringUtil.Str( (decimal)(A1530Servico_TipoHierarquia), 4, 0)));
         A632Servico_Ativo = i632Servico_Ativo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A632Servico_Ativo", A632Servico_Ativo);
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20206216174353");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("servicoprocesso.js", "?20206216174353");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTbservico_Internalname = "TBSERVICO";
         lblTextblockservicofluxo_ordem_Internalname = "TEXTBLOCKSERVICOFLUXO_ORDEM";
         edtavServicofluxo_ordem_Internalname = "vSERVICOFLUXO_ORDEM";
         lblTextblockmanterordem_Internalname = "TEXTBLOCKMANTERORDEM";
         radavManterordem_Internalname = "vMANTERORDEM";
         tblTablemergedservicofluxo_ordem_Internalname = "TABLEMERGEDSERVICOFLUXO_ORDEM";
         lblTextblockservico_nome_Internalname = "TEXTBLOCKSERVICO_NOME";
         edtServico_Nome_Internalname = "SERVICO_NOME";
         lblTextblockservico_sigla_Internalname = "TEXTBLOCKSERVICO_SIGLA";
         edtServico_Sigla_Internalname = "SERVICO_SIGLA";
         lblTextblockservico_descricao_Internalname = "TEXTBLOCKSERVICO_DESCRICAO";
         edtServico_Descricao_Internalname = "SERVICO_DESCRICAO";
         lblTextblockservico_perctmp_Internalname = "TEXTBLOCKSERVICO_PERCTMP";
         edtServico_PercTmp_Internalname = "SERVICO_PERCTMP";
         lblServico_perctmp_righttext_Internalname = "SERVICO_PERCTMP_RIGHTTEXT";
         tblTablemergedservico_perctmp_Internalname = "TABLEMERGEDSERVICO_PERCTMP";
         lblTextblockservico_percpgm_Internalname = "TEXTBLOCKSERVICO_PERCPGM";
         edtServico_PercPgm_Internalname = "SERVICO_PERCPGM";
         lblServico_percpgm_righttext_Internalname = "SERVICO_PERCPGM_RIGHTTEXT";
         tblTablemergedservico_percpgm_Internalname = "TABLEMERGEDSERVICO_PERCPGM";
         lblTextblockservico_perccnc_Internalname = "TEXTBLOCKSERVICO_PERCCNC";
         edtServico_PercCnc_Internalname = "SERVICO_PERCCNC";
         lblServico_perccnc_righttext_Internalname = "SERVICO_PERCCNC_RIGHTTEXT";
         tblTablemergedservico_perccnc_Internalname = "TABLEMERGEDSERVICO_PERCCNC";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         bttBtn_trn_delete_Internalname = "BTN_TRN_DELETE";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         edtServico_Codigo_Internalname = "SERVICO_CODIGO";
         edtavServicofluxo_ordemoldvalue_Internalname = "vSERVICOFLUXO_ORDEMOLDVALUE";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Processo do Servi�o";
         radavManterordem_Jsonclick = "";
         radavManterordem.Enabled = 1;
         radavManterordem.Visible = 1;
         edtavServicofluxo_ordem_Jsonclick = "";
         edtavServicofluxo_ordem_Enabled = 1;
         edtServico_PercTmp_Jsonclick = "";
         edtServico_PercTmp_Enabled = 1;
         edtServico_PercPgm_Jsonclick = "";
         edtServico_PercPgm_Enabled = 1;
         edtServico_PercCnc_Jsonclick = "";
         edtServico_PercCnc_Enabled = 1;
         edtServico_Descricao_Enabled = 1;
         edtServico_Sigla_Jsonclick = "";
         edtServico_Sigla_Enabled = 1;
         edtServico_Nome_Jsonclick = "";
         edtServico_Nome_Enabled = 1;
         lblTbservico_Caption = "Servi�o";
         bttBtn_trn_delete_Enabled = 0;
         bttBtn_trn_delete_Visible = 1;
         bttBtn_trn_cancel_Visible = 1;
         bttBtn_trn_enter_Enabled = 1;
         bttBtn_trn_enter_Visible = 1;
         edtavServicofluxo_ordemoldvalue_Jsonclick = "";
         edtavServicofluxo_ordemoldvalue_Enabled = 0;
         edtavServicofluxo_ordemoldvalue_Visible = 1;
         edtServico_Codigo_Jsonclick = "";
         edtServico_Codigo_Enabled = 0;
         edtServico_Codigo_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV8Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV7Ordem',fld:'vORDEM',pic:'ZZ9',nv:0}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E123X2',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV9ServicoFluxo_Ordem',fld:'vSERVICOFLUXO_ORDEM',pic:'ZZ9',nv:0},{av:'AV10ServicoFluxo_OrdemOldValue',fld:'vSERVICOFLUXO_ORDEMOLDVALUE',pic:'ZZ9',nv:0},{av:'AV14ManterOrdem',fld:'vMANTERORDEM',pic:'',nv:false},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("VSERVICOFLUXO_ORDEM.ISVALID","{handler:'E133X2',iparms:[{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV9ServicoFluxo_Ordem',fld:'vSERVICOFLUXO_ORDEM',pic:'ZZ9',nv:0}],oparms:[{av:'AV9ServicoFluxo_Ordem',fld:'vSERVICOFLUXO_ORDEM',pic:'ZZ9',nv:0},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'radavManterordem'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(11);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         Z608Servico_Nome = "";
         Z605Servico_Sigla = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         bttBtn_trn_delete_Jsonclick = "";
         lblTbservico_Jsonclick = "";
         lblTextblockservicofluxo_ordem_Jsonclick = "";
         lblTextblockservico_nome_Jsonclick = "";
         A608Servico_Nome = "";
         lblTextblockservico_sigla_Jsonclick = "";
         A605Servico_Sigla = "";
         lblTextblockservico_descricao_Jsonclick = "";
         A156Servico_Descricao = "";
         lblTextblockservico_perctmp_Jsonclick = "";
         lblTextblockservico_percpgm_Jsonclick = "";
         lblTextblockservico_perccnc_Jsonclick = "";
         lblServico_perccnc_righttext_Jsonclick = "";
         lblServico_percpgm_righttext_Jsonclick = "";
         lblServico_perctmp_righttext_Jsonclick = "";
         lblTextblockmanterordem_Jsonclick = "";
         A1557Servico_VincSigla = "";
         AV18Pgmname = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode130 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV13WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV11TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV12WebSession = context.GetSession();
         AV17TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         Z156Servico_Descricao = "";
         Z1557Servico_VincSigla = "";
         T003X4_A1557Servico_VincSigla = new String[] {""} ;
         T003X4_n1557Servico_VincSigla = new bool[] {false} ;
         T003X5_A155Servico_Codigo = new int[1] ;
         T003X5_A608Servico_Nome = new String[] {""} ;
         T003X5_A605Servico_Sigla = new String[] {""} ;
         T003X5_A156Servico_Descricao = new String[] {""} ;
         T003X5_n156Servico_Descricao = new bool[] {false} ;
         T003X5_A1534Servico_PercTmp = new short[1] ;
         T003X5_n1534Servico_PercTmp = new bool[] {false} ;
         T003X5_A1535Servico_PercPgm = new short[1] ;
         T003X5_n1535Servico_PercPgm = new bool[] {false} ;
         T003X5_A1536Servico_PercCnc = new short[1] ;
         T003X5_n1536Servico_PercCnc = new bool[] {false} ;
         T003X5_A1557Servico_VincSigla = new String[] {""} ;
         T003X5_n1557Servico_VincSigla = new bool[] {false} ;
         T003X5_A2131Servico_PausaSLA = new bool[] {false} ;
         T003X5_A1635Servico_IsPublico = new bool[] {false} ;
         T003X5_A1530Servico_TipoHierarquia = new short[1] ;
         T003X5_n1530Servico_TipoHierarquia = new bool[] {false} ;
         T003X5_A632Servico_Ativo = new bool[] {false} ;
         T003X5_A631Servico_Vinculado = new int[1] ;
         T003X5_n631Servico_Vinculado = new bool[] {false} ;
         T003X6_A1557Servico_VincSigla = new String[] {""} ;
         T003X6_n1557Servico_VincSigla = new bool[] {false} ;
         T003X7_A155Servico_Codigo = new int[1] ;
         T003X3_A155Servico_Codigo = new int[1] ;
         T003X3_A608Servico_Nome = new String[] {""} ;
         T003X3_A605Servico_Sigla = new String[] {""} ;
         T003X3_A156Servico_Descricao = new String[] {""} ;
         T003X3_n156Servico_Descricao = new bool[] {false} ;
         T003X3_A1534Servico_PercTmp = new short[1] ;
         T003X3_n1534Servico_PercTmp = new bool[] {false} ;
         T003X3_A1535Servico_PercPgm = new short[1] ;
         T003X3_n1535Servico_PercPgm = new bool[] {false} ;
         T003X3_A1536Servico_PercCnc = new short[1] ;
         T003X3_n1536Servico_PercCnc = new bool[] {false} ;
         T003X3_A2131Servico_PausaSLA = new bool[] {false} ;
         T003X3_A1635Servico_IsPublico = new bool[] {false} ;
         T003X3_A1530Servico_TipoHierarquia = new short[1] ;
         T003X3_n1530Servico_TipoHierarquia = new bool[] {false} ;
         T003X3_A632Servico_Ativo = new bool[] {false} ;
         T003X3_A631Servico_Vinculado = new int[1] ;
         T003X3_n631Servico_Vinculado = new bool[] {false} ;
         T003X8_A155Servico_Codigo = new int[1] ;
         T003X9_A155Servico_Codigo = new int[1] ;
         T003X2_A155Servico_Codigo = new int[1] ;
         T003X2_A608Servico_Nome = new String[] {""} ;
         T003X2_A605Servico_Sigla = new String[] {""} ;
         T003X2_A156Servico_Descricao = new String[] {""} ;
         T003X2_n156Servico_Descricao = new bool[] {false} ;
         T003X2_A1534Servico_PercTmp = new short[1] ;
         T003X2_n1534Servico_PercTmp = new bool[] {false} ;
         T003X2_A1535Servico_PercPgm = new short[1] ;
         T003X2_n1535Servico_PercPgm = new bool[] {false} ;
         T003X2_A1536Servico_PercCnc = new short[1] ;
         T003X2_n1536Servico_PercCnc = new bool[] {false} ;
         T003X2_A2131Servico_PausaSLA = new bool[] {false} ;
         T003X2_A1635Servico_IsPublico = new bool[] {false} ;
         T003X2_A1530Servico_TipoHierarquia = new short[1] ;
         T003X2_n1530Servico_TipoHierarquia = new bool[] {false} ;
         T003X2_A632Servico_Ativo = new bool[] {false} ;
         T003X2_A631Servico_Vinculado = new int[1] ;
         T003X2_n631Servico_Vinculado = new bool[] {false} ;
         T003X10_A155Servico_Codigo = new int[1] ;
         T003X13_A1557Servico_VincSigla = new String[] {""} ;
         T003X13_n1557Servico_VincSigla = new bool[] {false} ;
         T003X14_A631Servico_Vinculado = new int[1] ;
         T003X14_n631Servico_Vinculado = new bool[] {false} ;
         T003X15_A1528ServicoFluxo_Codigo = new int[1] ;
         T003X16_A1528ServicoFluxo_Codigo = new int[1] ;
         T003X17_A1440ServicoPrioridade_Codigo = new int[1] ;
         T003X18_A160ContratoServicos_Codigo = new int[1] ;
         T003X18_A1067ContratoServicosSistemas_ServicoCod = new int[1] ;
         T003X18_A1063ContratoServicosSistemas_SistemaCod = new int[1] ;
         T003X19_A5AreaTrabalho_Codigo = new int[1] ;
         T003X20_A828UsuarioServicos_UsuarioCod = new int[1] ;
         T003X20_A829UsuarioServicos_ServicoCod = new int[1] ;
         T003X21_A456ContagemResultado_Codigo = new int[1] ;
         T003X22_A439Solicitacoes_Codigo = new int[1] ;
         T003X23_A319ServicoAns_Codigo = new int[1] ;
         T003X24_A160ContratoServicos_Codigo = new int[1] ;
         T003X25_A155Servico_Codigo = new int[1] ;
         T003X25_A1839Check_Codigo = new int[1] ;
         T003X26_A155Servico_Codigo = new int[1] ;
         T003X26_A1766ServicoArtefato_ArtefatoCod = new int[1] ;
         T003X27_A155Servico_Codigo = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.servicoprocesso__default(),
            new Object[][] {
                new Object[] {
               T003X2_A155Servico_Codigo, T003X2_A608Servico_Nome, T003X2_A605Servico_Sigla, T003X2_A156Servico_Descricao, T003X2_n156Servico_Descricao, T003X2_A1534Servico_PercTmp, T003X2_n1534Servico_PercTmp, T003X2_A1535Servico_PercPgm, T003X2_n1535Servico_PercPgm, T003X2_A1536Servico_PercCnc,
               T003X2_n1536Servico_PercCnc, T003X2_A2131Servico_PausaSLA, T003X2_A1635Servico_IsPublico, T003X2_A1530Servico_TipoHierarquia, T003X2_n1530Servico_TipoHierarquia, T003X2_A632Servico_Ativo, T003X2_A631Servico_Vinculado, T003X2_n631Servico_Vinculado
               }
               , new Object[] {
               T003X3_A155Servico_Codigo, T003X3_A608Servico_Nome, T003X3_A605Servico_Sigla, T003X3_A156Servico_Descricao, T003X3_n156Servico_Descricao, T003X3_A1534Servico_PercTmp, T003X3_n1534Servico_PercTmp, T003X3_A1535Servico_PercPgm, T003X3_n1535Servico_PercPgm, T003X3_A1536Servico_PercCnc,
               T003X3_n1536Servico_PercCnc, T003X3_A2131Servico_PausaSLA, T003X3_A1635Servico_IsPublico, T003X3_A1530Servico_TipoHierarquia, T003X3_n1530Servico_TipoHierarquia, T003X3_A632Servico_Ativo, T003X3_A631Servico_Vinculado, T003X3_n631Servico_Vinculado
               }
               , new Object[] {
               T003X4_A1557Servico_VincSigla, T003X4_n1557Servico_VincSigla
               }
               , new Object[] {
               T003X5_A155Servico_Codigo, T003X5_A608Servico_Nome, T003X5_A605Servico_Sigla, T003X5_A156Servico_Descricao, T003X5_n156Servico_Descricao, T003X5_A1534Servico_PercTmp, T003X5_n1534Servico_PercTmp, T003X5_A1535Servico_PercPgm, T003X5_n1535Servico_PercPgm, T003X5_A1536Servico_PercCnc,
               T003X5_n1536Servico_PercCnc, T003X5_A1557Servico_VincSigla, T003X5_n1557Servico_VincSigla, T003X5_A2131Servico_PausaSLA, T003X5_A1635Servico_IsPublico, T003X5_A1530Servico_TipoHierarquia, T003X5_n1530Servico_TipoHierarquia, T003X5_A632Servico_Ativo, T003X5_A631Servico_Vinculado, T003X5_n631Servico_Vinculado
               }
               , new Object[] {
               T003X6_A1557Servico_VincSigla, T003X6_n1557Servico_VincSigla
               }
               , new Object[] {
               T003X7_A155Servico_Codigo
               }
               , new Object[] {
               T003X8_A155Servico_Codigo
               }
               , new Object[] {
               T003X9_A155Servico_Codigo
               }
               , new Object[] {
               T003X10_A155Servico_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T003X13_A1557Servico_VincSigla, T003X13_n1557Servico_VincSigla
               }
               , new Object[] {
               T003X14_A631Servico_Vinculado
               }
               , new Object[] {
               T003X15_A1528ServicoFluxo_Codigo
               }
               , new Object[] {
               T003X16_A1528ServicoFluxo_Codigo
               }
               , new Object[] {
               T003X17_A1440ServicoPrioridade_Codigo
               }
               , new Object[] {
               T003X18_A160ContratoServicos_Codigo, T003X18_A1067ContratoServicosSistemas_ServicoCod, T003X18_A1063ContratoServicosSistemas_SistemaCod
               }
               , new Object[] {
               T003X19_A5AreaTrabalho_Codigo
               }
               , new Object[] {
               T003X20_A828UsuarioServicos_UsuarioCod, T003X20_A829UsuarioServicos_ServicoCod
               }
               , new Object[] {
               T003X21_A456ContagemResultado_Codigo
               }
               , new Object[] {
               T003X22_A439Solicitacoes_Codigo
               }
               , new Object[] {
               T003X23_A319ServicoAns_Codigo
               }
               , new Object[] {
               T003X24_A160ContratoServicos_Codigo
               }
               , new Object[] {
               T003X25_A155Servico_Codigo, T003X25_A1839Check_Codigo
               }
               , new Object[] {
               T003X26_A155Servico_Codigo, T003X26_A1766ServicoArtefato_ArtefatoCod
               }
               , new Object[] {
               T003X27_A155Servico_Codigo
               }
            }
         );
         Z2131Servico_PausaSLA = false;
         A2131Servico_PausaSLA = false;
         i2131Servico_PausaSLA = false;
         Z1635Servico_IsPublico = false;
         A1635Servico_IsPublico = false;
         i1635Servico_IsPublico = false;
         Z1530Servico_TipoHierarquia = 1;
         n1530Servico_TipoHierarquia = false;
         A1530Servico_TipoHierarquia = 1;
         n1530Servico_TipoHierarquia = false;
         i1530Servico_TipoHierarquia = 1;
         n1530Servico_TipoHierarquia = false;
         Z632Servico_Ativo = true;
         A632Servico_Ativo = true;
         i632Servico_Ativo = true;
         AV18Pgmname = "ServicoProcesso";
      }

      private short wcpOAV7Ordem ;
      private short Z1534Servico_PercTmp ;
      private short Z1535Servico_PercPgm ;
      private short Z1536Servico_PercCnc ;
      private short Z1530Servico_TipoHierarquia ;
      private short GxWebError ;
      private short AV7Ordem ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short AV10ServicoFluxo_OrdemOldValue ;
      private short A1536Servico_PercCnc ;
      private short A1535Servico_PercPgm ;
      private short A1534Servico_PercTmp ;
      private short AV9ServicoFluxo_Ordem ;
      private short A1530Servico_TipoHierarquia ;
      private short Gx_BScreen ;
      private short RcdFound130 ;
      private short GX_JID ;
      private short gxajaxcallmode ;
      private short i1530Servico_TipoHierarquia ;
      private int wcpOAV8Servico_Codigo ;
      private int Z155Servico_Codigo ;
      private int Z631Servico_Vinculado ;
      private int N631Servico_Vinculado ;
      private int A631Servico_Vinculado ;
      private int AV8Servico_Codigo ;
      private int trnEnded ;
      private int A155Servico_Codigo ;
      private int edtServico_Codigo_Enabled ;
      private int edtServico_Codigo_Visible ;
      private int edtavServicofluxo_ordemoldvalue_Enabled ;
      private int edtavServicofluxo_ordemoldvalue_Visible ;
      private int bttBtn_trn_enter_Visible ;
      private int bttBtn_trn_enter_Enabled ;
      private int bttBtn_trn_cancel_Visible ;
      private int bttBtn_trn_delete_Visible ;
      private int bttBtn_trn_delete_Enabled ;
      private int edtServico_Nome_Enabled ;
      private int edtServico_Sigla_Enabled ;
      private int edtServico_Descricao_Enabled ;
      private int edtServico_PercCnc_Enabled ;
      private int edtServico_PercPgm_Enabled ;
      private int edtServico_PercTmp_Enabled ;
      private int edtavServicofluxo_ordem_Enabled ;
      private int AV16Insert_Servico_Vinculado ;
      private int AV19GXV1 ;
      private int idxLst ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String Z608Servico_Nome ;
      private String Z605Servico_Sigla ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String Gx_mode ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtServico_Nome_Internalname ;
      private String edtServico_Codigo_Internalname ;
      private String edtServico_Codigo_Jsonclick ;
      private String edtavServicofluxo_ordemoldvalue_Internalname ;
      private String edtavServicofluxo_ordemoldvalue_Jsonclick ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String bttBtn_trn_delete_Internalname ;
      private String bttBtn_trn_delete_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String lblTbservico_Internalname ;
      private String lblTbservico_Caption ;
      private String lblTbservico_Jsonclick ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockservicofluxo_ordem_Internalname ;
      private String lblTextblockservicofluxo_ordem_Jsonclick ;
      private String lblTextblockservico_nome_Internalname ;
      private String lblTextblockservico_nome_Jsonclick ;
      private String A608Servico_Nome ;
      private String edtServico_Nome_Jsonclick ;
      private String lblTextblockservico_sigla_Internalname ;
      private String lblTextblockservico_sigla_Jsonclick ;
      private String edtServico_Sigla_Internalname ;
      private String A605Servico_Sigla ;
      private String edtServico_Sigla_Jsonclick ;
      private String lblTextblockservico_descricao_Internalname ;
      private String lblTextblockservico_descricao_Jsonclick ;
      private String edtServico_Descricao_Internalname ;
      private String lblTextblockservico_perctmp_Internalname ;
      private String lblTextblockservico_perctmp_Jsonclick ;
      private String lblTextblockservico_percpgm_Internalname ;
      private String lblTextblockservico_percpgm_Jsonclick ;
      private String lblTextblockservico_perccnc_Internalname ;
      private String lblTextblockservico_perccnc_Jsonclick ;
      private String tblTablemergedservico_perccnc_Internalname ;
      private String edtServico_PercCnc_Internalname ;
      private String edtServico_PercCnc_Jsonclick ;
      private String lblServico_perccnc_righttext_Internalname ;
      private String lblServico_perccnc_righttext_Jsonclick ;
      private String tblTablemergedservico_percpgm_Internalname ;
      private String edtServico_PercPgm_Internalname ;
      private String edtServico_PercPgm_Jsonclick ;
      private String lblServico_percpgm_righttext_Internalname ;
      private String lblServico_percpgm_righttext_Jsonclick ;
      private String tblTablemergedservico_perctmp_Internalname ;
      private String edtServico_PercTmp_Internalname ;
      private String edtServico_PercTmp_Jsonclick ;
      private String lblServico_perctmp_righttext_Internalname ;
      private String lblServico_perctmp_righttext_Jsonclick ;
      private String tblTablemergedservicofluxo_ordem_Internalname ;
      private String edtavServicofluxo_ordem_Internalname ;
      private String edtavServicofluxo_ordem_Jsonclick ;
      private String lblTextblockmanterordem_Internalname ;
      private String lblTextblockmanterordem_Jsonclick ;
      private String radavManterordem_Internalname ;
      private String radavManterordem_Jsonclick ;
      private String A1557Servico_VincSigla ;
      private String AV18Pgmname ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode130 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String Z1557Servico_VincSigla ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private bool Z2131Servico_PausaSLA ;
      private bool Z1635Servico_IsPublico ;
      private bool Z632Servico_Ativo ;
      private bool entryPointCalled ;
      private bool n631Servico_Vinculado ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool AV14ManterOrdem ;
      private bool n156Servico_Descricao ;
      private bool n1534Servico_PercTmp ;
      private bool n1535Servico_PercPgm ;
      private bool n1536Servico_PercCnc ;
      private bool n1530Servico_TipoHierarquia ;
      private bool A2131Servico_PausaSLA ;
      private bool A1635Servico_IsPublico ;
      private bool A632Servico_Ativo ;
      private bool n1557Servico_VincSigla ;
      private bool returnInSub ;
      private bool AV15TemOutros ;
      private bool Gx_longc ;
      private bool i2131Servico_PausaSLA ;
      private bool i1635Servico_IsPublico ;
      private bool i632Servico_Ativo ;
      private String A156Servico_Descricao ;
      private String Z156Servico_Descricao ;
      private IGxSession AV12WebSession ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private short aP2_Ordem ;
      private GXRadio radavManterordem ;
      private IDataStoreProvider pr_default ;
      private String[] T003X4_A1557Servico_VincSigla ;
      private bool[] T003X4_n1557Servico_VincSigla ;
      private int[] T003X5_A155Servico_Codigo ;
      private String[] T003X5_A608Servico_Nome ;
      private String[] T003X5_A605Servico_Sigla ;
      private String[] T003X5_A156Servico_Descricao ;
      private bool[] T003X5_n156Servico_Descricao ;
      private short[] T003X5_A1534Servico_PercTmp ;
      private bool[] T003X5_n1534Servico_PercTmp ;
      private short[] T003X5_A1535Servico_PercPgm ;
      private bool[] T003X5_n1535Servico_PercPgm ;
      private short[] T003X5_A1536Servico_PercCnc ;
      private bool[] T003X5_n1536Servico_PercCnc ;
      private String[] T003X5_A1557Servico_VincSigla ;
      private bool[] T003X5_n1557Servico_VincSigla ;
      private bool[] T003X5_A2131Servico_PausaSLA ;
      private bool[] T003X5_A1635Servico_IsPublico ;
      private short[] T003X5_A1530Servico_TipoHierarquia ;
      private bool[] T003X5_n1530Servico_TipoHierarquia ;
      private bool[] T003X5_A632Servico_Ativo ;
      private int[] T003X5_A631Servico_Vinculado ;
      private bool[] T003X5_n631Servico_Vinculado ;
      private String[] T003X6_A1557Servico_VincSigla ;
      private bool[] T003X6_n1557Servico_VincSigla ;
      private int[] T003X7_A155Servico_Codigo ;
      private int[] T003X3_A155Servico_Codigo ;
      private String[] T003X3_A608Servico_Nome ;
      private String[] T003X3_A605Servico_Sigla ;
      private String[] T003X3_A156Servico_Descricao ;
      private bool[] T003X3_n156Servico_Descricao ;
      private short[] T003X3_A1534Servico_PercTmp ;
      private bool[] T003X3_n1534Servico_PercTmp ;
      private short[] T003X3_A1535Servico_PercPgm ;
      private bool[] T003X3_n1535Servico_PercPgm ;
      private short[] T003X3_A1536Servico_PercCnc ;
      private bool[] T003X3_n1536Servico_PercCnc ;
      private bool[] T003X3_A2131Servico_PausaSLA ;
      private bool[] T003X3_A1635Servico_IsPublico ;
      private short[] T003X3_A1530Servico_TipoHierarquia ;
      private bool[] T003X3_n1530Servico_TipoHierarquia ;
      private bool[] T003X3_A632Servico_Ativo ;
      private int[] T003X3_A631Servico_Vinculado ;
      private bool[] T003X3_n631Servico_Vinculado ;
      private int[] T003X8_A155Servico_Codigo ;
      private int[] T003X9_A155Servico_Codigo ;
      private int[] T003X2_A155Servico_Codigo ;
      private String[] T003X2_A608Servico_Nome ;
      private String[] T003X2_A605Servico_Sigla ;
      private String[] T003X2_A156Servico_Descricao ;
      private bool[] T003X2_n156Servico_Descricao ;
      private short[] T003X2_A1534Servico_PercTmp ;
      private bool[] T003X2_n1534Servico_PercTmp ;
      private short[] T003X2_A1535Servico_PercPgm ;
      private bool[] T003X2_n1535Servico_PercPgm ;
      private short[] T003X2_A1536Servico_PercCnc ;
      private bool[] T003X2_n1536Servico_PercCnc ;
      private bool[] T003X2_A2131Servico_PausaSLA ;
      private bool[] T003X2_A1635Servico_IsPublico ;
      private short[] T003X2_A1530Servico_TipoHierarquia ;
      private bool[] T003X2_n1530Servico_TipoHierarquia ;
      private bool[] T003X2_A632Servico_Ativo ;
      private int[] T003X2_A631Servico_Vinculado ;
      private bool[] T003X2_n631Servico_Vinculado ;
      private int[] T003X10_A155Servico_Codigo ;
      private String[] T003X13_A1557Servico_VincSigla ;
      private bool[] T003X13_n1557Servico_VincSigla ;
      private int[] T003X14_A631Servico_Vinculado ;
      private bool[] T003X14_n631Servico_Vinculado ;
      private int[] T003X15_A1528ServicoFluxo_Codigo ;
      private int[] T003X16_A1528ServicoFluxo_Codigo ;
      private int[] T003X17_A1440ServicoPrioridade_Codigo ;
      private int[] T003X18_A160ContratoServicos_Codigo ;
      private int[] T003X18_A1067ContratoServicosSistemas_ServicoCod ;
      private int[] T003X18_A1063ContratoServicosSistemas_SistemaCod ;
      private int[] T003X19_A5AreaTrabalho_Codigo ;
      private int[] T003X20_A828UsuarioServicos_UsuarioCod ;
      private int[] T003X20_A829UsuarioServicos_ServicoCod ;
      private int[] T003X21_A456ContagemResultado_Codigo ;
      private int[] T003X22_A439Solicitacoes_Codigo ;
      private int[] T003X23_A319ServicoAns_Codigo ;
      private int[] T003X24_A160ContratoServicos_Codigo ;
      private int[] T003X25_A155Servico_Codigo ;
      private int[] T003X25_A1839Check_Codigo ;
      private int[] T003X26_A155Servico_Codigo ;
      private int[] T003X26_A1766ServicoArtefato_ArtefatoCod ;
      private int[] T003X27_A155Servico_Codigo ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPTransactionContext AV11TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV17TrnContextAtt ;
      private wwpbaseobjects.SdtWWPContext AV13WWPContext ;
   }

   public class servicoprocesso__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new UpdateCursor(def[9])
         ,new UpdateCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
         ,new ForEachCursor(def[16])
         ,new ForEachCursor(def[17])
         ,new ForEachCursor(def[18])
         ,new ForEachCursor(def[19])
         ,new ForEachCursor(def[20])
         ,new ForEachCursor(def[21])
         ,new ForEachCursor(def[22])
         ,new ForEachCursor(def[23])
         ,new ForEachCursor(def[24])
         ,new ForEachCursor(def[25])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT003X5 ;
          prmT003X5 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003X4 ;
          prmT003X4 = new Object[] {
          new Object[] {"@Servico_Vinculado",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003X6 ;
          prmT003X6 = new Object[] {
          new Object[] {"@Servico_Vinculado",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003X7 ;
          prmT003X7 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003X3 ;
          prmT003X3 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003X8 ;
          prmT003X8 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003X9 ;
          prmT003X9 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003X2 ;
          prmT003X2 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003X10 ;
          prmT003X10 = new Object[] {
          new Object[] {"@Servico_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@Servico_Sigla",SqlDbType.Char,15,0} ,
          new Object[] {"@Servico_Descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@Servico_PercTmp",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Servico_PercPgm",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Servico_PercCnc",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Servico_PausaSLA",SqlDbType.Bit,4,0} ,
          new Object[] {"@Servico_IsPublico",SqlDbType.Bit,4,0} ,
          new Object[] {"@Servico_TipoHierarquia",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Servico_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@Servico_Vinculado",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003X11 ;
          prmT003X11 = new Object[] {
          new Object[] {"@Servico_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@Servico_Sigla",SqlDbType.Char,15,0} ,
          new Object[] {"@Servico_Descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@Servico_PercTmp",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Servico_PercPgm",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Servico_PercCnc",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Servico_PausaSLA",SqlDbType.Bit,4,0} ,
          new Object[] {"@Servico_IsPublico",SqlDbType.Bit,4,0} ,
          new Object[] {"@Servico_TipoHierarquia",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Servico_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@Servico_Vinculado",SqlDbType.Int,6,0} ,
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003X12 ;
          prmT003X12 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003X13 ;
          prmT003X13 = new Object[] {
          new Object[] {"@Servico_Vinculado",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003X14 ;
          prmT003X14 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003X15 ;
          prmT003X15 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003X16 ;
          prmT003X16 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003X17 ;
          prmT003X17 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003X18 ;
          prmT003X18 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003X19 ;
          prmT003X19 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003X20 ;
          prmT003X20 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003X21 ;
          prmT003X21 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003X22 ;
          prmT003X22 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003X23 ;
          prmT003X23 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003X24 ;
          prmT003X24 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003X25 ;
          prmT003X25 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003X26 ;
          prmT003X26 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003X27 ;
          prmT003X27 = new Object[] {
          } ;
          def= new CursorDef[] {
              new CursorDef("T003X2", "SELECT [Servico_Codigo], [Servico_Nome], [Servico_Sigla], [Servico_Descricao], [Servico_PercTmp], [Servico_PercPgm], [Servico_PercCnc], [Servico_PausaSLA], [Servico_IsPublico], [Servico_TipoHierarquia], [Servico_Ativo], [Servico_Vinculado] AS Servico_Vinculado FROM [Servico] WITH (UPDLOCK) WHERE [Servico_Codigo] = @Servico_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003X2,1,0,true,false )
             ,new CursorDef("T003X3", "SELECT [Servico_Codigo], [Servico_Nome], [Servico_Sigla], [Servico_Descricao], [Servico_PercTmp], [Servico_PercPgm], [Servico_PercCnc], [Servico_PausaSLA], [Servico_IsPublico], [Servico_TipoHierarquia], [Servico_Ativo], [Servico_Vinculado] AS Servico_Vinculado FROM [Servico] WITH (NOLOCK) WHERE [Servico_Codigo] = @Servico_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003X3,1,0,true,false )
             ,new CursorDef("T003X4", "SELECT [Servico_Sigla] AS Servico_VincSigla FROM [Servico] WITH (NOLOCK) WHERE [Servico_Codigo] = @Servico_Vinculado ",true, GxErrorMask.GX_NOMASK, false, this,prmT003X4,1,0,true,false )
             ,new CursorDef("T003X5", "SELECT TM1.[Servico_Codigo], TM1.[Servico_Nome], TM1.[Servico_Sigla], TM1.[Servico_Descricao], TM1.[Servico_PercTmp], TM1.[Servico_PercPgm], TM1.[Servico_PercCnc], T2.[Servico_Sigla] AS Servico_VincSigla, TM1.[Servico_PausaSLA], TM1.[Servico_IsPublico], TM1.[Servico_TipoHierarquia], TM1.[Servico_Ativo], TM1.[Servico_Vinculado] AS Servico_Vinculado FROM ([Servico] TM1 WITH (NOLOCK) LEFT JOIN [Servico] T2 WITH (NOLOCK) ON T2.[Servico_Codigo] = TM1.[Servico_Vinculado]) WHERE TM1.[Servico_Codigo] = @Servico_Codigo ORDER BY TM1.[Servico_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT003X5,100,0,true,false )
             ,new CursorDef("T003X6", "SELECT [Servico_Sigla] AS Servico_VincSigla FROM [Servico] WITH (NOLOCK) WHERE [Servico_Codigo] = @Servico_Vinculado ",true, GxErrorMask.GX_NOMASK, false, this,prmT003X6,1,0,true,false )
             ,new CursorDef("T003X7", "SELECT [Servico_Codigo] FROM [Servico] WITH (NOLOCK) WHERE [Servico_Codigo] = @Servico_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT003X7,1,0,true,false )
             ,new CursorDef("T003X8", "SELECT TOP 1 [Servico_Codigo] FROM [Servico] WITH (NOLOCK) WHERE ( [Servico_Codigo] > @Servico_Codigo) ORDER BY [Servico_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT003X8,1,0,true,true )
             ,new CursorDef("T003X9", "SELECT TOP 1 [Servico_Codigo] FROM [Servico] WITH (NOLOCK) WHERE ( [Servico_Codigo] < @Servico_Codigo) ORDER BY [Servico_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT003X9,1,0,true,true )
             ,new CursorDef("T003X10", "INSERT INTO [Servico]([Servico_Nome], [Servico_Sigla], [Servico_Descricao], [Servico_PercTmp], [Servico_PercPgm], [Servico_PercCnc], [Servico_PausaSLA], [Servico_IsPublico], [Servico_TipoHierarquia], [Servico_Ativo], [Servico_Vinculado], [ServicoGrupo_Codigo], [Servico_UO], [Servico_UORespExclusiva], [Servico_Terceriza], [Servico_Tela], [Servico_Atende], [Servico_ObrigaValores], [Servico_ObjetoControle], [Servico_Anterior], [Servico_Posterior], [Servico_LinNegCod], [Servico_IsOrigemReferencia]) VALUES(@Servico_Nome, @Servico_Sigla, @Servico_Descricao, @Servico_PercTmp, @Servico_PercPgm, @Servico_PercCnc, @Servico_PausaSLA, @Servico_IsPublico, @Servico_TipoHierarquia, @Servico_Ativo, @Servico_Vinculado, convert(int, 0), convert(int, 0), convert(bit, 0), convert(bit, 0), '', '', '', '', convert(int, 0), convert(int, 0), convert(int, 0), convert(bit, 0)); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT003X10)
             ,new CursorDef("T003X11", "UPDATE [Servico] SET [Servico_Nome]=@Servico_Nome, [Servico_Sigla]=@Servico_Sigla, [Servico_Descricao]=@Servico_Descricao, [Servico_PercTmp]=@Servico_PercTmp, [Servico_PercPgm]=@Servico_PercPgm, [Servico_PercCnc]=@Servico_PercCnc, [Servico_PausaSLA]=@Servico_PausaSLA, [Servico_IsPublico]=@Servico_IsPublico, [Servico_TipoHierarquia]=@Servico_TipoHierarquia, [Servico_Ativo]=@Servico_Ativo, [Servico_Vinculado]=@Servico_Vinculado  WHERE [Servico_Codigo] = @Servico_Codigo", GxErrorMask.GX_NOMASK,prmT003X11)
             ,new CursorDef("T003X12", "DELETE FROM [Servico]  WHERE [Servico_Codigo] = @Servico_Codigo", GxErrorMask.GX_NOMASK,prmT003X12)
             ,new CursorDef("T003X13", "SELECT [Servico_Sigla] AS Servico_VincSigla FROM [Servico] WITH (NOLOCK) WHERE [Servico_Codigo] = @Servico_Vinculado ",true, GxErrorMask.GX_NOMASK, false, this,prmT003X13,1,0,true,false )
             ,new CursorDef("T003X14", "SELECT TOP 1 [Servico_Codigo] AS Servico_Vinculado FROM [Servico] WITH (NOLOCK) WHERE [Servico_Vinculado] = @Servico_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003X14,1,0,true,true )
             ,new CursorDef("T003X15", "SELECT TOP 1 [ServicoFluxo_Codigo] FROM [ServicoFluxo] WITH (NOLOCK) WHERE [ServicoFluxo_ServicoPos] = @Servico_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003X15,1,0,true,true )
             ,new CursorDef("T003X16", "SELECT TOP 1 [ServicoFluxo_Codigo] FROM [ServicoFluxo] WITH (NOLOCK) WHERE [ServicoFluxo_ServicoCod] = @Servico_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003X16,1,0,true,true )
             ,new CursorDef("T003X17", "SELECT TOP 1 [ServicoPrioridade_Codigo] FROM [ServicoPrioridade] WITH (NOLOCK) WHERE [ServicoPrioridade_SrvCod] = @Servico_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003X17,1,0,true,true )
             ,new CursorDef("T003X18", "SELECT TOP 1 [ContratoServicos_Codigo], [ContratoServicosSistemas_ServicoCod], [ContratoServicosSistemas_SistemaCod] FROM [ContratoServicosSistemas] WITH (NOLOCK) WHERE [ContratoServicosSistemas_ServicoCod] = @Servico_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003X18,1,0,true,true )
             ,new CursorDef("T003X19", "SELECT TOP 1 [AreaTrabalho_Codigo] FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_ServicoPadrao] = @Servico_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003X19,1,0,true,true )
             ,new CursorDef("T003X20", "SELECT TOP 1 [UsuarioServicos_UsuarioCod], [UsuarioServicos_ServicoCod] FROM [UsuarioServicos] WITH (NOLOCK) WHERE [UsuarioServicos_ServicoCod] = @Servico_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003X20,1,0,true,true )
             ,new CursorDef("T003X21", "SELECT TOP 1 [ContagemResultado_Codigo] FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_ServicoSS] = @Servico_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003X21,1,0,true,true )
             ,new CursorDef("T003X22", "SELECT TOP 1 [Solicitacoes_Codigo] FROM [Solicitacoes] WITH (NOLOCK) WHERE [Servico_Codigo] = @Servico_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003X22,1,0,true,true )
             ,new CursorDef("T003X23", "SELECT TOP 1 [ServicoAns_Codigo] FROM [ServicoAns] WITH (NOLOCK) WHERE [Servico_Codigo] = @Servico_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003X23,1,0,true,true )
             ,new CursorDef("T003X24", "SELECT TOP 1 [ContratoServicos_Codigo] FROM [ContratoServicos] WITH (NOLOCK) WHERE [Servico_Codigo] = @Servico_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003X24,1,0,true,true )
             ,new CursorDef("T003X25", "SELECT TOP 1 [Servico_Codigo], [Check_Codigo] FROM [ServicoCheck] WITH (NOLOCK) WHERE [Servico_Codigo] = @Servico_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003X25,1,0,true,true )
             ,new CursorDef("T003X26", "SELECT TOP 1 [Servico_Codigo], [ServicoArtefato_ArtefatoCod] FROM [ServicoArtefatos] WITH (NOLOCK) WHERE [Servico_Codigo] = @Servico_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT003X26,1,0,true,true )
             ,new CursorDef("T003X27", "SELECT [Servico_Codigo] FROM [Servico] WITH (NOLOCK) ORDER BY [Servico_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT003X27,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 15) ;
                ((String[]) buf[3])[0] = rslt.getLongVarchar(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((short[]) buf[5])[0] = rslt.getShort(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((short[]) buf[7])[0] = rslt.getShort(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((short[]) buf[9])[0] = rslt.getShort(7) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((bool[]) buf[11])[0] = rslt.getBool(8) ;
                ((bool[]) buf[12])[0] = rslt.getBool(9) ;
                ((short[]) buf[13])[0] = rslt.getShort(10) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(10);
                ((bool[]) buf[15])[0] = rslt.getBool(11) ;
                ((int[]) buf[16])[0] = rslt.getInt(12) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(12);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 15) ;
                ((String[]) buf[3])[0] = rslt.getLongVarchar(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((short[]) buf[5])[0] = rslt.getShort(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((short[]) buf[7])[0] = rslt.getShort(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((short[]) buf[9])[0] = rslt.getShort(7) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((bool[]) buf[11])[0] = rslt.getBool(8) ;
                ((bool[]) buf[12])[0] = rslt.getBool(9) ;
                ((short[]) buf[13])[0] = rslt.getShort(10) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(10);
                ((bool[]) buf[15])[0] = rslt.getBool(11) ;
                ((int[]) buf[16])[0] = rslt.getInt(12) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(12);
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getString(1, 15) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 15) ;
                ((String[]) buf[3])[0] = rslt.getLongVarchar(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((short[]) buf[5])[0] = rslt.getShort(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((short[]) buf[7])[0] = rslt.getShort(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((short[]) buf[9])[0] = rslt.getShort(7) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((String[]) buf[11])[0] = rslt.getString(8, 15) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((bool[]) buf[13])[0] = rslt.getBool(9) ;
                ((bool[]) buf[14])[0] = rslt.getBool(10) ;
                ((short[]) buf[15])[0] = rslt.getShort(11) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(11);
                ((bool[]) buf[17])[0] = rslt.getBool(12) ;
                ((int[]) buf[18])[0] = rslt.getInt(13) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(13);
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getString(1, 15) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 11 :
                ((String[]) buf[0])[0] = rslt.getString(1, 15) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 13 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 14 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 15 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 16 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 17 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 18 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 19 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 20 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 21 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 22 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 23 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 24 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 25 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 3 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 4 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(4, (short)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 5 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(5, (short)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 6 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(6, (short)parms[9]);
                }
                stmt.SetParameter(7, (bool)parms[10]);
                stmt.SetParameter(8, (bool)parms[11]);
                if ( (bool)parms[12] )
                {
                   stmt.setNull( 9 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(9, (short)parms[13]);
                }
                stmt.SetParameter(10, (bool)parms[14]);
                if ( (bool)parms[15] )
                {
                   stmt.setNull( 11 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(11, (int)parms[16]);
                }
                return;
             case 9 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 3 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 4 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(4, (short)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 5 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(5, (short)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 6 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(6, (short)parms[9]);
                }
                stmt.SetParameter(7, (bool)parms[10]);
                stmt.SetParameter(8, (bool)parms[11]);
                if ( (bool)parms[12] )
                {
                   stmt.setNull( 9 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(9, (short)parms[13]);
                }
                stmt.SetParameter(10, (bool)parms[14]);
                if ( (bool)parms[15] )
                {
                   stmt.setNull( 11 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(11, (int)parms[16]);
                }
                stmt.SetParameter(12, (int)parms[17]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 11 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 14 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 15 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 16 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 17 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 18 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 19 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 20 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 21 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 22 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 23 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 24 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
