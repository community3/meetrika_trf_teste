/*
               File: InsertRegistros
        Description: Insert Registros
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 23:3:28.85
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class insertregistros : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Insert Registros", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtInsertRegistros_Codigo_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public insertregistros( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public insertregistros( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_2J99( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_2J99e( bool wbgen )
      {
         if ( wbgen )
         {
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_2J99( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableBorder100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_2J99( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_2J99e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Control Group */
            GxWebStd.gx_group_start( context, grpGroupdata_Internalname, "Insert Registros", 1, 0, "px", 0, "px", "Group", " "+"style=\"-moz-border-radius: 3pt;;\""+" ", "HLP_InsertRegistros.htm");
            wb_table3_28_2J99( true) ;
         }
         return  ;
      }

      protected void wb_table3_28_2J99e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</fieldset>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_2J99e( true) ;
         }
         else
         {
            wb_table1_2_2J99e( false) ;
         }
      }

      protected void wb_table3_28_2J99( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_34_2J99( true) ;
         }
         return  ;
      }

      protected void wb_table4_34_2J99e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 67,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_enter_Internalname, "", "Confirmar", bttBtn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_enter_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_InsertRegistros.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 68,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_cancel_Internalname, "", "Fechar", bttBtn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_InsertRegistros.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'',false,'',0)\"";
            ClassString = "BtnDelete";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_delete_Internalname, "", "Eliminar", bttBtn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_delete_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_InsertRegistros.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_28_2J99e( true) ;
         }
         else
         {
            wb_table3_28_2J99e( false) ;
         }
      }

      protected void wb_table4_34_2J99( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable2_Internalname, tblTable2_Internalname, "", "Container", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockinsertregistros_codigo_Internalname, "Codigo", "", "", lblTextblockinsertregistros_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_InsertRegistros.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtInsertRegistros_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A765InsertRegistros_Codigo), 6, 0, ",", "")), ((edtInsertRegistros_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A765InsertRegistros_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A765InsertRegistros_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,39);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtInsertRegistros_Codigo_Jsonclick, 0, "Attribute", "", "", "", 1, edtInsertRegistros_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_InsertRegistros.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockinsertregistros_nome_Internalname, "Nome", "", "", lblTextblockinsertregistros_nome_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_InsertRegistros.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtInsertRegistros_Nome_Internalname, StringUtil.RTrim( A766InsertRegistros_Nome), StringUtil.RTrim( context.localUtil.Format( A766InsertRegistros_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,44);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtInsertRegistros_Nome_Jsonclick, 0, "Attribute", "", "", "", 1, edtInsertRegistros_Nome_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_InsertRegistros.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockinsertregistros_painome_Internalname, "Pai", "", "", lblTextblockinsertregistros_painome_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_InsertRegistros.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtInsertRegistros_PaiNome_Internalname, StringUtil.RTrim( A767InsertRegistros_PaiNome), StringUtil.RTrim( context.localUtil.Format( A767InsertRegistros_PaiNome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,49);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtInsertRegistros_PaiNome_Jsonclick, 0, "Attribute", "", "", "", 1, edtInsertRegistros_PaiNome_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_InsertRegistros.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockinsertregistros_cmplchar1_Internalname, "character 1", "", "", lblTextblockinsertregistros_cmplchar1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_InsertRegistros.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtInsertRegistros_CmplChar1_Internalname, A768InsertRegistros_CmplChar1, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,54);\"", 0, 1, edtInsertRegistros_CmplChar1_Enabled, 0, 80, "chr", 5, "row", StyleString, ClassString, "", "400", -1, "", "", -1, true, "", "HLP_InsertRegistros.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockinsertregistros_cmplnum1_Internalname, "numeric 1", "", "", lblTextblockinsertregistros_cmplnum1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_InsertRegistros.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtInsertRegistros_CmplNum1_Internalname, StringUtil.LTrim( StringUtil.NToC( A769InsertRegistros_CmplNum1, 18, 3, ",", "")), ((edtInsertRegistros_CmplNum1_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A769InsertRegistros_CmplNum1, "ZZZZZZZZZZZZZ9.999")) : context.localUtil.Format( A769InsertRegistros_CmplNum1, "ZZZZZZZZZZZZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','3');"+";gx.evt.onblur(this,59);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtInsertRegistros_CmplNum1_Jsonclick, 0, "Attribute", "", "", "", 1, edtInsertRegistros_CmplNum1_Enabled, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_InsertRegistros.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockinsertregistros_tipo_Internalname, "Tipo", "", "", lblTextblockinsertregistros_tipo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_InsertRegistros.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 64,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtInsertRegistros_Tipo_Internalname, StringUtil.RTrim( A770InsertRegistros_Tipo), StringUtil.RTrim( context.localUtil.Format( A770InsertRegistros_Tipo, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,64);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtInsertRegistros_Tipo_Jsonclick, 0, "Attribute", "", "", "", 1, edtInsertRegistros_Tipo_Enabled, 0, "text", "", 1, "chr", 1, "row", 1, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_InsertRegistros.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_34_2J99e( true) ;
         }
         else
         {
            wb_table4_34_2J99e( false) ;
         }
      }

      protected void wb_table2_5_2J99( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabletoolbar_Internalname, tblTabletoolbar_Internalname, "", "ViewTable", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divSectiontoolbar_Internalname, 1, 0, "px", 0, "px", "ToolbarMain", "left", "top", "", "WHITE-SPACE: nowrap;", "div");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 9,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_first_Internalname, context.GetImagePath( "b9e06284-17ac-4c88-8937-5dbd84ad5d80", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_first_Visible, 1, "", "Primeiro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_first_Jsonclick, "'"+""+"'"+",false,"+"'"+"EFIRST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_InsertRegistros.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_first_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_first_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_first_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EFIRST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_InsertRegistros.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_previous_Internalname, context.GetImagePath( "7d212604-db7b-4785-9c0d-5faffe71aa33", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_previous_Visible, 1, "", "Anterior", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_previous_Jsonclick, "'"+""+"'"+",false,"+"'"+"EPREVIOUS."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_InsertRegistros.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 12,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_previous_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_previous_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_previous_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EPREVIOUS."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_InsertRegistros.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 13,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_next_Internalname, context.GetImagePath( "1ae947cf-1354-41a9-8d59-d91daebf554f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_next_Visible, 1, "", "Pr�ximo", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_next_Jsonclick, "'"+""+"'"+",false,"+"'"+"ENEXT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_InsertRegistros.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 14,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_next_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_next_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_next_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ENEXT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_InsertRegistros.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 15,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_last_Internalname, context.GetImagePath( "29211874-e613-48e5-9011-8017d984217e", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_last_Visible, 1, "", "�ltimo", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_last_Jsonclick, "'"+""+"'"+",false,"+"'"+"ELAST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_InsertRegistros.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_last_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_last_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_last_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ELAST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_InsertRegistros.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_select_Internalname, context.GetImagePath( "1ca03f75-9947-4d2c-90a4-e8ab9c5cedea", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_select_Visible, 1, "", "Selecionar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_select_Jsonclick, "'"+""+"'"+",false,"+"'"+"ESELECT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_InsertRegistros.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_select_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_select_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_select_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ESELECT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_InsertRegistros.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_enter2_Internalname, context.GetImagePath( "2061cf2c-bd33-4433-a13e-34af954142e9", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_enter2_Visible, imgBtn_enter2_Enabled, "", "Confirmar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_enter2_Jsonclick, "'"+""+"'"+",false,"+"'"+"EENTER."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_InsertRegistros.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_enter2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_enter2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_enter2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EENTER."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_InsertRegistros.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_cancel2_Internalname, context.GetImagePath( "0e94ced8-bc34-47ff-9a53-bc683736a686", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_cancel2_Visible, 1, "", "Fechar", 0, 0, 0, "px", 0, "px", 0, 0, 1, imgBtn_cancel2_Jsonclick, "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_InsertRegistros.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 22,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_cancel2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_cancel2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 1, imgBtn_cancel2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_InsertRegistros.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_delete2_Internalname, context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_delete2_Visible, imgBtn_delete2_Enabled, "", "Eliminar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_delete2_Jsonclick, "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_InsertRegistros.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_delete2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_delete2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_delete2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_InsertRegistros.htm");
            GxWebStd.gx_div_end( context, "left", "top");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_2J99e( true) ;
         }
         else
         {
            wb_table2_5_2J99e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               if ( ( ( context.localUtil.CToN( cgiGet( edtInsertRegistros_Codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtInsertRegistros_Codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "INSERTREGISTROS_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtInsertRegistros_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A765InsertRegistros_Codigo = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A765InsertRegistros_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A765InsertRegistros_Codigo), 6, 0)));
               }
               else
               {
                  A765InsertRegistros_Codigo = (int)(context.localUtil.CToN( cgiGet( edtInsertRegistros_Codigo_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A765InsertRegistros_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A765InsertRegistros_Codigo), 6, 0)));
               }
               A766InsertRegistros_Nome = StringUtil.Upper( cgiGet( edtInsertRegistros_Nome_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A766InsertRegistros_Nome", A766InsertRegistros_Nome);
               A767InsertRegistros_PaiNome = StringUtil.Upper( cgiGet( edtInsertRegistros_PaiNome_Internalname));
               n767InsertRegistros_PaiNome = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A767InsertRegistros_PaiNome", A767InsertRegistros_PaiNome);
               n767InsertRegistros_PaiNome = (String.IsNullOrEmpty(StringUtil.RTrim( A767InsertRegistros_PaiNome)) ? true : false);
               A768InsertRegistros_CmplChar1 = cgiGet( edtInsertRegistros_CmplChar1_Internalname);
               n768InsertRegistros_CmplChar1 = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A768InsertRegistros_CmplChar1", A768InsertRegistros_CmplChar1);
               n768InsertRegistros_CmplChar1 = (String.IsNullOrEmpty(StringUtil.RTrim( A768InsertRegistros_CmplChar1)) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtInsertRegistros_CmplNum1_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtInsertRegistros_CmplNum1_Internalname), ",", ".") > 99999999999999.999m ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "INSERTREGISTROS_CMPLNUM1");
                  AnyError = 1;
                  GX_FocusControl = edtInsertRegistros_CmplNum1_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A769InsertRegistros_CmplNum1 = 0;
                  n769InsertRegistros_CmplNum1 = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A769InsertRegistros_CmplNum1", StringUtil.LTrim( StringUtil.Str( A769InsertRegistros_CmplNum1, 18, 3)));
               }
               else
               {
                  A769InsertRegistros_CmplNum1 = context.localUtil.CToN( cgiGet( edtInsertRegistros_CmplNum1_Internalname), ",", ".");
                  n769InsertRegistros_CmplNum1 = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A769InsertRegistros_CmplNum1", StringUtil.LTrim( StringUtil.Str( A769InsertRegistros_CmplNum1, 18, 3)));
               }
               n769InsertRegistros_CmplNum1 = ((Convert.ToDecimal(0)==A769InsertRegistros_CmplNum1) ? true : false);
               A770InsertRegistros_Tipo = cgiGet( edtInsertRegistros_Tipo_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A770InsertRegistros_Tipo", A770InsertRegistros_Tipo);
               /* Read saved values. */
               Z765InsertRegistros_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z765InsertRegistros_Codigo"), ",", "."));
               Z766InsertRegistros_Nome = cgiGet( "Z766InsertRegistros_Nome");
               Z767InsertRegistros_PaiNome = cgiGet( "Z767InsertRegistros_PaiNome");
               n767InsertRegistros_PaiNome = (String.IsNullOrEmpty(StringUtil.RTrim( A767InsertRegistros_PaiNome)) ? true : false);
               Z768InsertRegistros_CmplChar1 = cgiGet( "Z768InsertRegistros_CmplChar1");
               n768InsertRegistros_CmplChar1 = (String.IsNullOrEmpty(StringUtil.RTrim( A768InsertRegistros_CmplChar1)) ? true : false);
               Z769InsertRegistros_CmplNum1 = context.localUtil.CToN( cgiGet( "Z769InsertRegistros_CmplNum1"), ",", ".");
               n769InsertRegistros_CmplNum1 = ((Convert.ToDecimal(0)==A769InsertRegistros_CmplNum1) ? true : false);
               Z770InsertRegistros_Tipo = cgiGet( "Z770InsertRegistros_Tipo");
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               Gx_mode = cgiGet( "vMODE");
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  A765InsertRegistros_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A765InsertRegistros_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A765InsertRegistros_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  disable_std_buttons_dsp( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  standaloneModal( ) ;
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_enter( ) ;
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                        else if ( StringUtil.StrCmp(sEvt, "FIRST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_first( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "PREVIOUS") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_previous( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "NEXT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_next( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LAST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_last( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "SELECT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_select( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DELETE") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_delete( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           AfterKeyLoadScreen( ) ;
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll2J99( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            imgBtn_delete2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
      }

      protected void disable_std_buttons_dsp( )
      {
         imgBtn_delete2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Visible), 5, 0)));
         imgBtn_delete2_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_separator_Visible), 5, 0)));
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)));
         imgBtn_first_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_first_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_first_Visible), 5, 0)));
         imgBtn_first_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_first_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_first_separator_Visible), 5, 0)));
         imgBtn_previous_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_previous_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_previous_Visible), 5, 0)));
         imgBtn_previous_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_previous_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_previous_separator_Visible), 5, 0)));
         imgBtn_next_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_next_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_next_Visible), 5, 0)));
         imgBtn_next_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_next_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_next_separator_Visible), 5, 0)));
         imgBtn_last_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_last_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_last_Visible), 5, 0)));
         imgBtn_last_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_last_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_last_separator_Visible), 5, 0)));
         imgBtn_select_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_select_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_select_Visible), 5, 0)));
         imgBtn_select_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_select_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_select_separator_Visible), 5, 0)));
         imgBtn_delete2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Visible), 5, 0)));
         imgBtn_delete2_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_separator_Visible), 5, 0)));
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)));
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            imgBtn_enter2_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Visible), 5, 0)));
            imgBtn_enter2_separator_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_separator_Visible), 5, 0)));
            bttBtn_enter_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_enter_Visible), 5, 0)));
         }
         DisableAttributes2J99( ) ;
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void ResetCaption2J0( )
      {
      }

      protected void ZM2J99( short GX_JID )
      {
         if ( ( GX_JID == 1 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z766InsertRegistros_Nome = T002J3_A766InsertRegistros_Nome[0];
               Z767InsertRegistros_PaiNome = T002J3_A767InsertRegistros_PaiNome[0];
               Z768InsertRegistros_CmplChar1 = T002J3_A768InsertRegistros_CmplChar1[0];
               Z769InsertRegistros_CmplNum1 = T002J3_A769InsertRegistros_CmplNum1[0];
               Z770InsertRegistros_Tipo = T002J3_A770InsertRegistros_Tipo[0];
            }
            else
            {
               Z766InsertRegistros_Nome = A766InsertRegistros_Nome;
               Z767InsertRegistros_PaiNome = A767InsertRegistros_PaiNome;
               Z768InsertRegistros_CmplChar1 = A768InsertRegistros_CmplChar1;
               Z769InsertRegistros_CmplNum1 = A769InsertRegistros_CmplNum1;
               Z770InsertRegistros_Tipo = A770InsertRegistros_Tipo;
            }
         }
         if ( GX_JID == -1 )
         {
            Z765InsertRegistros_Codigo = A765InsertRegistros_Codigo;
            Z766InsertRegistros_Nome = A766InsertRegistros_Nome;
            Z767InsertRegistros_PaiNome = A767InsertRegistros_PaiNome;
            Z768InsertRegistros_CmplChar1 = A768InsertRegistros_CmplChar1;
            Z769InsertRegistros_CmplNum1 = A769InsertRegistros_CmplNum1;
            Z770InsertRegistros_Tipo = A770InsertRegistros_Tipo;
         }
      }

      protected void standaloneNotModal( )
      {
      }

      protected void standaloneModal( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            imgBtn_delete2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
         else
         {
            imgBtn_delete2_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            imgBtn_enter2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Enabled), 5, 0)));
         }
         else
         {
            imgBtn_enter2_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Enabled), 5, 0)));
         }
      }

      protected void Load2J99( )
      {
         /* Using cursor T002J4 */
         pr_default.execute(2, new Object[] {A765InsertRegistros_Codigo});
         if ( (pr_default.getStatus(2) != 101) )
         {
            RcdFound99 = 1;
            A766InsertRegistros_Nome = T002J4_A766InsertRegistros_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A766InsertRegistros_Nome", A766InsertRegistros_Nome);
            A767InsertRegistros_PaiNome = T002J4_A767InsertRegistros_PaiNome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A767InsertRegistros_PaiNome", A767InsertRegistros_PaiNome);
            n767InsertRegistros_PaiNome = T002J4_n767InsertRegistros_PaiNome[0];
            A768InsertRegistros_CmplChar1 = T002J4_A768InsertRegistros_CmplChar1[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A768InsertRegistros_CmplChar1", A768InsertRegistros_CmplChar1);
            n768InsertRegistros_CmplChar1 = T002J4_n768InsertRegistros_CmplChar1[0];
            A769InsertRegistros_CmplNum1 = T002J4_A769InsertRegistros_CmplNum1[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A769InsertRegistros_CmplNum1", StringUtil.LTrim( StringUtil.Str( A769InsertRegistros_CmplNum1, 18, 3)));
            n769InsertRegistros_CmplNum1 = T002J4_n769InsertRegistros_CmplNum1[0];
            A770InsertRegistros_Tipo = T002J4_A770InsertRegistros_Tipo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A770InsertRegistros_Tipo", A770InsertRegistros_Tipo);
            ZM2J99( -1) ;
         }
         pr_default.close(2);
         OnLoadActions2J99( ) ;
      }

      protected void OnLoadActions2J99( )
      {
      }

      protected void CheckExtendedTable2J99( )
      {
         Gx_BScreen = 1;
         standaloneModal( ) ;
      }

      protected void CloseExtendedTableCursors2J99( )
      {
      }

      protected void enableDisable( )
      {
      }

      protected void GetKey2J99( )
      {
         /* Using cursor T002J5 */
         pr_default.execute(3, new Object[] {A765InsertRegistros_Codigo});
         if ( (pr_default.getStatus(3) != 101) )
         {
            RcdFound99 = 1;
         }
         else
         {
            RcdFound99 = 0;
         }
         pr_default.close(3);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T002J3 */
         pr_default.execute(1, new Object[] {A765InsertRegistros_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM2J99( 1) ;
            RcdFound99 = 1;
            A765InsertRegistros_Codigo = T002J3_A765InsertRegistros_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A765InsertRegistros_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A765InsertRegistros_Codigo), 6, 0)));
            A766InsertRegistros_Nome = T002J3_A766InsertRegistros_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A766InsertRegistros_Nome", A766InsertRegistros_Nome);
            A767InsertRegistros_PaiNome = T002J3_A767InsertRegistros_PaiNome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A767InsertRegistros_PaiNome", A767InsertRegistros_PaiNome);
            n767InsertRegistros_PaiNome = T002J3_n767InsertRegistros_PaiNome[0];
            A768InsertRegistros_CmplChar1 = T002J3_A768InsertRegistros_CmplChar1[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A768InsertRegistros_CmplChar1", A768InsertRegistros_CmplChar1);
            n768InsertRegistros_CmplChar1 = T002J3_n768InsertRegistros_CmplChar1[0];
            A769InsertRegistros_CmplNum1 = T002J3_A769InsertRegistros_CmplNum1[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A769InsertRegistros_CmplNum1", StringUtil.LTrim( StringUtil.Str( A769InsertRegistros_CmplNum1, 18, 3)));
            n769InsertRegistros_CmplNum1 = T002J3_n769InsertRegistros_CmplNum1[0];
            A770InsertRegistros_Tipo = T002J3_A770InsertRegistros_Tipo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A770InsertRegistros_Tipo", A770InsertRegistros_Tipo);
            Z765InsertRegistros_Codigo = A765InsertRegistros_Codigo;
            sMode99 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Load2J99( ) ;
            if ( AnyError == 1 )
            {
               RcdFound99 = 0;
               InitializeNonKey2J99( ) ;
            }
            Gx_mode = sMode99;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            RcdFound99 = 0;
            InitializeNonKey2J99( ) ;
            sMode99 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Gx_mode = sMode99;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey2J99( ) ;
         if ( RcdFound99 == 0 )
         {
            Gx_mode = "INS";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound99 = 0;
         /* Using cursor T002J6 */
         pr_default.execute(4, new Object[] {A765InsertRegistros_Codigo});
         if ( (pr_default.getStatus(4) != 101) )
         {
            while ( (pr_default.getStatus(4) != 101) && ( ( T002J6_A765InsertRegistros_Codigo[0] < A765InsertRegistros_Codigo ) ) )
            {
               pr_default.readNext(4);
            }
            if ( (pr_default.getStatus(4) != 101) && ( ( T002J6_A765InsertRegistros_Codigo[0] > A765InsertRegistros_Codigo ) ) )
            {
               A765InsertRegistros_Codigo = T002J6_A765InsertRegistros_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A765InsertRegistros_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A765InsertRegistros_Codigo), 6, 0)));
               RcdFound99 = 1;
            }
         }
         pr_default.close(4);
      }

      protected void move_previous( )
      {
         RcdFound99 = 0;
         /* Using cursor T002J7 */
         pr_default.execute(5, new Object[] {A765InsertRegistros_Codigo});
         if ( (pr_default.getStatus(5) != 101) )
         {
            while ( (pr_default.getStatus(5) != 101) && ( ( T002J7_A765InsertRegistros_Codigo[0] > A765InsertRegistros_Codigo ) ) )
            {
               pr_default.readNext(5);
            }
            if ( (pr_default.getStatus(5) != 101) && ( ( T002J7_A765InsertRegistros_Codigo[0] < A765InsertRegistros_Codigo ) ) )
            {
               A765InsertRegistros_Codigo = T002J7_A765InsertRegistros_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A765InsertRegistros_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A765InsertRegistros_Codigo), 6, 0)));
               RcdFound99 = 1;
            }
         }
         pr_default.close(5);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey2J99( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtInsertRegistros_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert2J99( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound99 == 1 )
            {
               if ( A765InsertRegistros_Codigo != Z765InsertRegistros_Codigo )
               {
                  A765InsertRegistros_Codigo = Z765InsertRegistros_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A765InsertRegistros_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A765InsertRegistros_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "INSERTREGISTROS_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtInsertRegistros_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtInsertRegistros_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  Gx_mode = "UPD";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Update record */
                  Update2J99( ) ;
                  GX_FocusControl = edtInsertRegistros_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A765InsertRegistros_Codigo != Z765InsertRegistros_Codigo )
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Insert record */
                  GX_FocusControl = edtInsertRegistros_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert2J99( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "INSERTREGISTROS_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtInsertRegistros_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     Gx_mode = "INS";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     /* Insert record */
                     GX_FocusControl = edtInsertRegistros_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert2J99( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
      }

      protected void btn_delete( )
      {
         if ( A765InsertRegistros_Codigo != Z765InsertRegistros_Codigo )
         {
            A765InsertRegistros_Codigo = Z765InsertRegistros_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A765InsertRegistros_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A765InsertRegistros_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "INSERTREGISTROS_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtInsertRegistros_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtInsertRegistros_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            getByPrimaryKey( ) ;
         }
         CloseOpenCursors();
      }

      protected void btn_get( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         if ( RcdFound99 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "INSERTREGISTROS_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtInsertRegistros_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GX_FocusControl = edtInsertRegistros_Nome_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_first( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart2J99( ) ;
         if ( RcdFound99 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtInsertRegistros_Nome_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         ScanEnd2J99( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_previous( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_previous( ) ;
         if ( RcdFound99 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtInsertRegistros_Nome_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_next( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_next( ) ;
         if ( RcdFound99 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtInsertRegistros_Nome_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_last( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart2J99( ) ;
         if ( RcdFound99 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            while ( RcdFound99 != 0 )
            {
               ScanNext2J99( ) ;
            }
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtInsertRegistros_Nome_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         ScanEnd2J99( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_select( )
      {
         getEqualNoModal( ) ;
      }

      protected void CheckOptimisticConcurrency2J99( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T002J2 */
            pr_default.execute(0, new Object[] {A765InsertRegistros_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"InsertRegistros"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z766InsertRegistros_Nome, T002J2_A766InsertRegistros_Nome[0]) != 0 ) || ( StringUtil.StrCmp(Z767InsertRegistros_PaiNome, T002J2_A767InsertRegistros_PaiNome[0]) != 0 ) || ( StringUtil.StrCmp(Z768InsertRegistros_CmplChar1, T002J2_A768InsertRegistros_CmplChar1[0]) != 0 ) || ( Z769InsertRegistros_CmplNum1 != T002J2_A769InsertRegistros_CmplNum1[0] ) || ( StringUtil.StrCmp(Z770InsertRegistros_Tipo, T002J2_A770InsertRegistros_Tipo[0]) != 0 ) )
            {
               if ( StringUtil.StrCmp(Z766InsertRegistros_Nome, T002J2_A766InsertRegistros_Nome[0]) != 0 )
               {
                  GXUtil.WriteLog("insertregistros:[seudo value changed for attri]"+"InsertRegistros_Nome");
                  GXUtil.WriteLogRaw("Old: ",Z766InsertRegistros_Nome);
                  GXUtil.WriteLogRaw("Current: ",T002J2_A766InsertRegistros_Nome[0]);
               }
               if ( StringUtil.StrCmp(Z767InsertRegistros_PaiNome, T002J2_A767InsertRegistros_PaiNome[0]) != 0 )
               {
                  GXUtil.WriteLog("insertregistros:[seudo value changed for attri]"+"InsertRegistros_PaiNome");
                  GXUtil.WriteLogRaw("Old: ",Z767InsertRegistros_PaiNome);
                  GXUtil.WriteLogRaw("Current: ",T002J2_A767InsertRegistros_PaiNome[0]);
               }
               if ( StringUtil.StrCmp(Z768InsertRegistros_CmplChar1, T002J2_A768InsertRegistros_CmplChar1[0]) != 0 )
               {
                  GXUtil.WriteLog("insertregistros:[seudo value changed for attri]"+"InsertRegistros_CmplChar1");
                  GXUtil.WriteLogRaw("Old: ",Z768InsertRegistros_CmplChar1);
                  GXUtil.WriteLogRaw("Current: ",T002J2_A768InsertRegistros_CmplChar1[0]);
               }
               if ( Z769InsertRegistros_CmplNum1 != T002J2_A769InsertRegistros_CmplNum1[0] )
               {
                  GXUtil.WriteLog("insertregistros:[seudo value changed for attri]"+"InsertRegistros_CmplNum1");
                  GXUtil.WriteLogRaw("Old: ",Z769InsertRegistros_CmplNum1);
                  GXUtil.WriteLogRaw("Current: ",T002J2_A769InsertRegistros_CmplNum1[0]);
               }
               if ( StringUtil.StrCmp(Z770InsertRegistros_Tipo, T002J2_A770InsertRegistros_Tipo[0]) != 0 )
               {
                  GXUtil.WriteLog("insertregistros:[seudo value changed for attri]"+"InsertRegistros_Tipo");
                  GXUtil.WriteLogRaw("Old: ",Z770InsertRegistros_Tipo);
                  GXUtil.WriteLogRaw("Current: ",T002J2_A770InsertRegistros_Tipo[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"InsertRegistros"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert2J99( )
      {
         BeforeValidate2J99( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable2J99( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM2J99( 0) ;
            CheckOptimisticConcurrency2J99( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm2J99( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert2J99( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T002J8 */
                     pr_default.execute(6, new Object[] {A766InsertRegistros_Nome, n767InsertRegistros_PaiNome, A767InsertRegistros_PaiNome, n768InsertRegistros_CmplChar1, A768InsertRegistros_CmplChar1, n769InsertRegistros_CmplNum1, A769InsertRegistros_CmplNum1, A770InsertRegistros_Tipo});
                     A765InsertRegistros_Codigo = T002J8_A765InsertRegistros_Codigo[0];
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A765InsertRegistros_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A765InsertRegistros_Codigo), 6, 0)));
                     pr_default.close(6);
                     dsDefault.SmartCacheProvider.SetUpdated("InsertRegistros") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption2J0( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load2J99( ) ;
            }
            EndLevel2J99( ) ;
         }
         CloseExtendedTableCursors2J99( ) ;
      }

      protected void Update2J99( )
      {
         BeforeValidate2J99( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable2J99( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency2J99( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm2J99( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate2J99( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T002J9 */
                     pr_default.execute(7, new Object[] {A766InsertRegistros_Nome, n767InsertRegistros_PaiNome, A767InsertRegistros_PaiNome, n768InsertRegistros_CmplChar1, A768InsertRegistros_CmplChar1, n769InsertRegistros_CmplNum1, A769InsertRegistros_CmplNum1, A770InsertRegistros_Tipo, A765InsertRegistros_Codigo});
                     pr_default.close(7);
                     dsDefault.SmartCacheProvider.SetUpdated("InsertRegistros") ;
                     if ( (pr_default.getStatus(7) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"InsertRegistros"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate2J99( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                           ResetCaption2J0( ) ;
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel2J99( ) ;
         }
         CloseExtendedTableCursors2J99( ) ;
      }

      protected void DeferredUpdate2J99( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         BeforeValidate2J99( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency2J99( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls2J99( ) ;
            AfterConfirm2J99( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete2J99( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T002J10 */
                  pr_default.execute(8, new Object[] {A765InsertRegistros_Codigo});
                  pr_default.close(8);
                  dsDefault.SmartCacheProvider.SetUpdated("InsertRegistros") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        move_next( ) ;
                        if ( RcdFound99 == 0 )
                        {
                           InitAll2J99( ) ;
                           Gx_mode = "INS";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        }
                        else
                        {
                           getByPrimaryKey( ) ;
                           Gx_mode = "UPD";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        }
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                        ResetCaption2J0( ) ;
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode99 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         EndLevel2J99( ) ;
         Gx_mode = sMode99;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
      }

      protected void OnDeleteControls2J99( )
      {
         standaloneModal( ) ;
         /* No delete mode formulas found. */
      }

      protected void EndLevel2J99( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete2J99( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            context.CommitDataStores( "InsertRegistros");
            if ( AnyError == 0 )
            {
               ConfirmValues2J0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            context.RollbackDataStores( "InsertRegistros");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart2J99( )
      {
         /* Using cursor T002J11 */
         pr_default.execute(9);
         RcdFound99 = 0;
         if ( (pr_default.getStatus(9) != 101) )
         {
            RcdFound99 = 1;
            A765InsertRegistros_Codigo = T002J11_A765InsertRegistros_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A765InsertRegistros_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A765InsertRegistros_Codigo), 6, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext2J99( )
      {
         /* Scan next routine */
         pr_default.readNext(9);
         RcdFound99 = 0;
         if ( (pr_default.getStatus(9) != 101) )
         {
            RcdFound99 = 1;
            A765InsertRegistros_Codigo = T002J11_A765InsertRegistros_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A765InsertRegistros_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A765InsertRegistros_Codigo), 6, 0)));
         }
      }

      protected void ScanEnd2J99( )
      {
         pr_default.close(9);
      }

      protected void AfterConfirm2J99( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert2J99( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate2J99( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete2J99( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete2J99( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate2J99( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes2J99( )
      {
         edtInsertRegistros_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtInsertRegistros_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtInsertRegistros_Codigo_Enabled), 5, 0)));
         edtInsertRegistros_Nome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtInsertRegistros_Nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtInsertRegistros_Nome_Enabled), 5, 0)));
         edtInsertRegistros_PaiNome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtInsertRegistros_PaiNome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtInsertRegistros_PaiNome_Enabled), 5, 0)));
         edtInsertRegistros_CmplChar1_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtInsertRegistros_CmplChar1_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtInsertRegistros_CmplChar1_Enabled), 5, 0)));
         edtInsertRegistros_CmplNum1_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtInsertRegistros_CmplNum1_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtInsertRegistros_CmplNum1_Enabled), 5, 0)));
         edtInsertRegistros_Tipo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtInsertRegistros_Tipo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtInsertRegistros_Tipo_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues2J0( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020428233305");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("insertregistros.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z765InsertRegistros_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z765InsertRegistros_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z766InsertRegistros_Nome", StringUtil.RTrim( Z766InsertRegistros_Nome));
         GxWebStd.gx_hidden_field( context, "Z767InsertRegistros_PaiNome", StringUtil.RTrim( Z767InsertRegistros_PaiNome));
         GxWebStd.gx_hidden_field( context, "Z768InsertRegistros_CmplChar1", Z768InsertRegistros_CmplChar1);
         GxWebStd.gx_hidden_field( context, "Z769InsertRegistros_CmplNum1", StringUtil.LTrim( StringUtil.NToC( Z769InsertRegistros_CmplNum1, 18, 3, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z770InsertRegistros_Tipo", StringUtil.RTrim( Z770InsertRegistros_Tipo));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("insertregistros.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "InsertRegistros" ;
      }

      public override String GetPgmdesc( )
      {
         return "Insert Registros" ;
      }

      protected void InitializeNonKey2J99( )
      {
         A766InsertRegistros_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A766InsertRegistros_Nome", A766InsertRegistros_Nome);
         A767InsertRegistros_PaiNome = "";
         n767InsertRegistros_PaiNome = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A767InsertRegistros_PaiNome", A767InsertRegistros_PaiNome);
         n767InsertRegistros_PaiNome = (String.IsNullOrEmpty(StringUtil.RTrim( A767InsertRegistros_PaiNome)) ? true : false);
         A768InsertRegistros_CmplChar1 = "";
         n768InsertRegistros_CmplChar1 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A768InsertRegistros_CmplChar1", A768InsertRegistros_CmplChar1);
         n768InsertRegistros_CmplChar1 = (String.IsNullOrEmpty(StringUtil.RTrim( A768InsertRegistros_CmplChar1)) ? true : false);
         A769InsertRegistros_CmplNum1 = 0;
         n769InsertRegistros_CmplNum1 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A769InsertRegistros_CmplNum1", StringUtil.LTrim( StringUtil.Str( A769InsertRegistros_CmplNum1, 18, 3)));
         n769InsertRegistros_CmplNum1 = ((Convert.ToDecimal(0)==A769InsertRegistros_CmplNum1) ? true : false);
         A770InsertRegistros_Tipo = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A770InsertRegistros_Tipo", A770InsertRegistros_Tipo);
         Z766InsertRegistros_Nome = "";
         Z767InsertRegistros_PaiNome = "";
         Z768InsertRegistros_CmplChar1 = "";
         Z769InsertRegistros_CmplNum1 = 0;
         Z770InsertRegistros_Tipo = "";
      }

      protected void InitAll2J99( )
      {
         A765InsertRegistros_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A765InsertRegistros_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A765InsertRegistros_Codigo), 6, 0)));
         InitializeNonKey2J99( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?2249787");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20204282333011");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxdec.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("insertregistros.js", "?20204282333011");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         imgBtn_first_Internalname = "BTN_FIRST";
         imgBtn_first_separator_Internalname = "BTN_FIRST_SEPARATOR";
         imgBtn_previous_Internalname = "BTN_PREVIOUS";
         imgBtn_previous_separator_Internalname = "BTN_PREVIOUS_SEPARATOR";
         imgBtn_next_Internalname = "BTN_NEXT";
         imgBtn_next_separator_Internalname = "BTN_NEXT_SEPARATOR";
         imgBtn_last_Internalname = "BTN_LAST";
         imgBtn_last_separator_Internalname = "BTN_LAST_SEPARATOR";
         imgBtn_select_Internalname = "BTN_SELECT";
         imgBtn_select_separator_Internalname = "BTN_SELECT_SEPARATOR";
         imgBtn_enter2_Internalname = "BTN_ENTER2";
         imgBtn_enter2_separator_Internalname = "BTN_ENTER2_SEPARATOR";
         imgBtn_cancel2_Internalname = "BTN_CANCEL2";
         imgBtn_cancel2_separator_Internalname = "BTN_CANCEL2_SEPARATOR";
         imgBtn_delete2_Internalname = "BTN_DELETE2";
         imgBtn_delete2_separator_Internalname = "BTN_DELETE2_SEPARATOR";
         divSectiontoolbar_Internalname = "SECTIONTOOLBAR";
         tblTabletoolbar_Internalname = "TABLETOOLBAR";
         lblTextblockinsertregistros_codigo_Internalname = "TEXTBLOCKINSERTREGISTROS_CODIGO";
         edtInsertRegistros_Codigo_Internalname = "INSERTREGISTROS_CODIGO";
         lblTextblockinsertregistros_nome_Internalname = "TEXTBLOCKINSERTREGISTROS_NOME";
         edtInsertRegistros_Nome_Internalname = "INSERTREGISTROS_NOME";
         lblTextblockinsertregistros_painome_Internalname = "TEXTBLOCKINSERTREGISTROS_PAINOME";
         edtInsertRegistros_PaiNome_Internalname = "INSERTREGISTROS_PAINOME";
         lblTextblockinsertregistros_cmplchar1_Internalname = "TEXTBLOCKINSERTREGISTROS_CMPLCHAR1";
         edtInsertRegistros_CmplChar1_Internalname = "INSERTREGISTROS_CMPLCHAR1";
         lblTextblockinsertregistros_cmplnum1_Internalname = "TEXTBLOCKINSERTREGISTROS_CMPLNUM1";
         edtInsertRegistros_CmplNum1_Internalname = "INSERTREGISTROS_CMPLNUM1";
         lblTextblockinsertregistros_tipo_Internalname = "TEXTBLOCKINSERTREGISTROS_TIPO";
         edtInsertRegistros_Tipo_Internalname = "INSERTREGISTROS_TIPO";
         tblTable2_Internalname = "TABLE2";
         bttBtn_enter_Internalname = "BTN_ENTER";
         bttBtn_cancel_Internalname = "BTN_CANCEL";
         bttBtn_delete_Internalname = "BTN_DELETE";
         tblTable1_Internalname = "TABLE1";
         grpGroupdata_Internalname = "GROUPDATA";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Insert Registros";
         imgBtn_delete2_separator_Visible = 1;
         imgBtn_delete2_Enabled = 1;
         imgBtn_delete2_Visible = 1;
         imgBtn_cancel2_separator_Visible = 1;
         imgBtn_cancel2_Visible = 1;
         imgBtn_enter2_separator_Visible = 1;
         imgBtn_enter2_Enabled = 1;
         imgBtn_enter2_Visible = 1;
         imgBtn_select_separator_Visible = 1;
         imgBtn_select_Visible = 1;
         imgBtn_last_separator_Visible = 1;
         imgBtn_last_Visible = 1;
         imgBtn_next_separator_Visible = 1;
         imgBtn_next_Visible = 1;
         imgBtn_previous_separator_Visible = 1;
         imgBtn_previous_Visible = 1;
         imgBtn_first_separator_Visible = 1;
         imgBtn_first_Visible = 1;
         edtInsertRegistros_Tipo_Jsonclick = "";
         edtInsertRegistros_Tipo_Enabled = 1;
         edtInsertRegistros_CmplNum1_Jsonclick = "";
         edtInsertRegistros_CmplNum1_Enabled = 1;
         edtInsertRegistros_CmplChar1_Enabled = 1;
         edtInsertRegistros_PaiNome_Jsonclick = "";
         edtInsertRegistros_PaiNome_Enabled = 1;
         edtInsertRegistros_Nome_Jsonclick = "";
         edtInsertRegistros_Nome_Enabled = 1;
         edtInsertRegistros_Codigo_Jsonclick = "";
         edtInsertRegistros_Codigo_Enabled = 1;
         bttBtn_delete_Visible = 1;
         bttBtn_cancel_Visible = 1;
         bttBtn_enter_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void AfterKeyLoadScreen( )
      {
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         GX_FocusControl = edtInsertRegistros_Nome_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         standaloneNotModal( ) ;
         standaloneModal( ) ;
         /* End function AfterKeyLoadScreen */
      }

      public void Valid_Insertregistros_codigo( int GX_Parm1 ,
                                                String GX_Parm2 ,
                                                String GX_Parm3 ,
                                                String GX_Parm4 ,
                                                decimal GX_Parm5 ,
                                                String GX_Parm6 )
      {
         A765InsertRegistros_Codigo = GX_Parm1;
         A766InsertRegistros_Nome = GX_Parm2;
         A767InsertRegistros_PaiNome = GX_Parm3;
         n767InsertRegistros_PaiNome = false;
         A768InsertRegistros_CmplChar1 = GX_Parm4;
         n768InsertRegistros_CmplChar1 = false;
         A769InsertRegistros_CmplNum1 = GX_Parm5;
         n769InsertRegistros_CmplNum1 = false;
         A770InsertRegistros_Tipo = GX_Parm6;
         context.wbHandled = 1;
         AfterKeyLoadScreen( ) ;
         Draw( ) ;
         SendCloseFormHiddens( ) ;
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
         }
         isValidOutput.Add(StringUtil.RTrim( A766InsertRegistros_Nome));
         isValidOutput.Add(StringUtil.RTrim( A767InsertRegistros_PaiNome));
         isValidOutput.Add(A768InsertRegistros_CmplChar1);
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( A769InsertRegistros_CmplNum1, 18, 3, ".", "")));
         isValidOutput.Add(StringUtil.RTrim( A770InsertRegistros_Tipo));
         isValidOutput.Add(StringUtil.RTrim( Gx_mode));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z765InsertRegistros_Codigo), 6, 0, ",", "")));
         isValidOutput.Add(StringUtil.RTrim( Z766InsertRegistros_Nome));
         isValidOutput.Add(StringUtil.RTrim( Z767InsertRegistros_PaiNome));
         isValidOutput.Add(Z768InsertRegistros_CmplChar1);
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( Z769InsertRegistros_CmplNum1, 18, 3, ",", "")));
         isValidOutput.Add(StringUtil.RTrim( Z770InsertRegistros_Tipo));
         isValidOutput.Add(imgBtn_delete2_Enabled);
         isValidOutput.Add(imgBtn_enter2_Enabled);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
      }

      public override void initialize( )
      {
         sPrefix = "";
         Z766InsertRegistros_Nome = "";
         Z767InsertRegistros_PaiNome = "";
         Z768InsertRegistros_CmplChar1 = "";
         Z770InsertRegistros_Tipo = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtn_enter_Jsonclick = "";
         bttBtn_cancel_Jsonclick = "";
         bttBtn_delete_Jsonclick = "";
         lblTextblockinsertregistros_codigo_Jsonclick = "";
         lblTextblockinsertregistros_nome_Jsonclick = "";
         A766InsertRegistros_Nome = "";
         lblTextblockinsertregistros_painome_Jsonclick = "";
         A767InsertRegistros_PaiNome = "";
         lblTextblockinsertregistros_cmplchar1_Jsonclick = "";
         A768InsertRegistros_CmplChar1 = "";
         lblTextblockinsertregistros_cmplnum1_Jsonclick = "";
         lblTextblockinsertregistros_tipo_Jsonclick = "";
         A770InsertRegistros_Tipo = "";
         imgBtn_first_Jsonclick = "";
         imgBtn_first_separator_Jsonclick = "";
         imgBtn_previous_Jsonclick = "";
         imgBtn_previous_separator_Jsonclick = "";
         imgBtn_next_Jsonclick = "";
         imgBtn_next_separator_Jsonclick = "";
         imgBtn_last_Jsonclick = "";
         imgBtn_last_separator_Jsonclick = "";
         imgBtn_select_Jsonclick = "";
         imgBtn_select_separator_Jsonclick = "";
         imgBtn_enter2_Jsonclick = "";
         imgBtn_enter2_separator_Jsonclick = "";
         imgBtn_cancel2_Jsonclick = "";
         imgBtn_cancel2_separator_Jsonclick = "";
         imgBtn_delete2_Jsonclick = "";
         imgBtn_delete2_separator_Jsonclick = "";
         Gx_mode = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         T002J4_A765InsertRegistros_Codigo = new int[1] ;
         T002J4_A766InsertRegistros_Nome = new String[] {""} ;
         T002J4_A767InsertRegistros_PaiNome = new String[] {""} ;
         T002J4_n767InsertRegistros_PaiNome = new bool[] {false} ;
         T002J4_A768InsertRegistros_CmplChar1 = new String[] {""} ;
         T002J4_n768InsertRegistros_CmplChar1 = new bool[] {false} ;
         T002J4_A769InsertRegistros_CmplNum1 = new decimal[1] ;
         T002J4_n769InsertRegistros_CmplNum1 = new bool[] {false} ;
         T002J4_A770InsertRegistros_Tipo = new String[] {""} ;
         T002J5_A765InsertRegistros_Codigo = new int[1] ;
         T002J3_A765InsertRegistros_Codigo = new int[1] ;
         T002J3_A766InsertRegistros_Nome = new String[] {""} ;
         T002J3_A767InsertRegistros_PaiNome = new String[] {""} ;
         T002J3_n767InsertRegistros_PaiNome = new bool[] {false} ;
         T002J3_A768InsertRegistros_CmplChar1 = new String[] {""} ;
         T002J3_n768InsertRegistros_CmplChar1 = new bool[] {false} ;
         T002J3_A769InsertRegistros_CmplNum1 = new decimal[1] ;
         T002J3_n769InsertRegistros_CmplNum1 = new bool[] {false} ;
         T002J3_A770InsertRegistros_Tipo = new String[] {""} ;
         sMode99 = "";
         T002J6_A765InsertRegistros_Codigo = new int[1] ;
         T002J7_A765InsertRegistros_Codigo = new int[1] ;
         T002J2_A765InsertRegistros_Codigo = new int[1] ;
         T002J2_A766InsertRegistros_Nome = new String[] {""} ;
         T002J2_A767InsertRegistros_PaiNome = new String[] {""} ;
         T002J2_n767InsertRegistros_PaiNome = new bool[] {false} ;
         T002J2_A768InsertRegistros_CmplChar1 = new String[] {""} ;
         T002J2_n768InsertRegistros_CmplChar1 = new bool[] {false} ;
         T002J2_A769InsertRegistros_CmplNum1 = new decimal[1] ;
         T002J2_n769InsertRegistros_CmplNum1 = new bool[] {false} ;
         T002J2_A770InsertRegistros_Tipo = new String[] {""} ;
         T002J8_A765InsertRegistros_Codigo = new int[1] ;
         T002J11_A765InsertRegistros_Codigo = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         isValidOutput = new GxUnknownObjectCollection();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.insertregistros__default(),
            new Object[][] {
                new Object[] {
               T002J2_A765InsertRegistros_Codigo, T002J2_A766InsertRegistros_Nome, T002J2_A767InsertRegistros_PaiNome, T002J2_n767InsertRegistros_PaiNome, T002J2_A768InsertRegistros_CmplChar1, T002J2_n768InsertRegistros_CmplChar1, T002J2_A769InsertRegistros_CmplNum1, T002J2_n769InsertRegistros_CmplNum1, T002J2_A770InsertRegistros_Tipo
               }
               , new Object[] {
               T002J3_A765InsertRegistros_Codigo, T002J3_A766InsertRegistros_Nome, T002J3_A767InsertRegistros_PaiNome, T002J3_n767InsertRegistros_PaiNome, T002J3_A768InsertRegistros_CmplChar1, T002J3_n768InsertRegistros_CmplChar1, T002J3_A769InsertRegistros_CmplNum1, T002J3_n769InsertRegistros_CmplNum1, T002J3_A770InsertRegistros_Tipo
               }
               , new Object[] {
               T002J4_A765InsertRegistros_Codigo, T002J4_A766InsertRegistros_Nome, T002J4_A767InsertRegistros_PaiNome, T002J4_n767InsertRegistros_PaiNome, T002J4_A768InsertRegistros_CmplChar1, T002J4_n768InsertRegistros_CmplChar1, T002J4_A769InsertRegistros_CmplNum1, T002J4_n769InsertRegistros_CmplNum1, T002J4_A770InsertRegistros_Tipo
               }
               , new Object[] {
               T002J5_A765InsertRegistros_Codigo
               }
               , new Object[] {
               T002J6_A765InsertRegistros_Codigo
               }
               , new Object[] {
               T002J7_A765InsertRegistros_Codigo
               }
               , new Object[] {
               T002J8_A765InsertRegistros_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T002J11_A765InsertRegistros_Codigo
               }
            }
         );
      }

      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short GX_JID ;
      private short RcdFound99 ;
      private short Gx_BScreen ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int Z765InsertRegistros_Codigo ;
      private int trnEnded ;
      private int bttBtn_enter_Visible ;
      private int bttBtn_cancel_Visible ;
      private int bttBtn_delete_Visible ;
      private int A765InsertRegistros_Codigo ;
      private int edtInsertRegistros_Codigo_Enabled ;
      private int edtInsertRegistros_Nome_Enabled ;
      private int edtInsertRegistros_PaiNome_Enabled ;
      private int edtInsertRegistros_CmplChar1_Enabled ;
      private int edtInsertRegistros_CmplNum1_Enabled ;
      private int edtInsertRegistros_Tipo_Enabled ;
      private int imgBtn_first_Visible ;
      private int imgBtn_first_separator_Visible ;
      private int imgBtn_previous_Visible ;
      private int imgBtn_previous_separator_Visible ;
      private int imgBtn_next_Visible ;
      private int imgBtn_next_separator_Visible ;
      private int imgBtn_last_Visible ;
      private int imgBtn_last_separator_Visible ;
      private int imgBtn_select_Visible ;
      private int imgBtn_select_separator_Visible ;
      private int imgBtn_enter2_Visible ;
      private int imgBtn_enter2_Enabled ;
      private int imgBtn_enter2_separator_Visible ;
      private int imgBtn_cancel2_Visible ;
      private int imgBtn_cancel2_separator_Visible ;
      private int imgBtn_delete2_Visible ;
      private int imgBtn_delete2_Enabled ;
      private int imgBtn_delete2_separator_Visible ;
      private int idxLst ;
      private decimal Z769InsertRegistros_CmplNum1 ;
      private decimal A769InsertRegistros_CmplNum1 ;
      private String sPrefix ;
      private String Z766InsertRegistros_Nome ;
      private String Z767InsertRegistros_PaiNome ;
      private String Z770InsertRegistros_Tipo ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtInsertRegistros_Codigo_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String grpGroupdata_Internalname ;
      private String tblTable1_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String TempTags ;
      private String bttBtn_enter_Internalname ;
      private String bttBtn_enter_Jsonclick ;
      private String bttBtn_cancel_Internalname ;
      private String bttBtn_cancel_Jsonclick ;
      private String bttBtn_delete_Internalname ;
      private String bttBtn_delete_Jsonclick ;
      private String tblTable2_Internalname ;
      private String lblTextblockinsertregistros_codigo_Internalname ;
      private String lblTextblockinsertregistros_codigo_Jsonclick ;
      private String edtInsertRegistros_Codigo_Jsonclick ;
      private String lblTextblockinsertregistros_nome_Internalname ;
      private String lblTextblockinsertregistros_nome_Jsonclick ;
      private String edtInsertRegistros_Nome_Internalname ;
      private String A766InsertRegistros_Nome ;
      private String edtInsertRegistros_Nome_Jsonclick ;
      private String lblTextblockinsertregistros_painome_Internalname ;
      private String lblTextblockinsertregistros_painome_Jsonclick ;
      private String edtInsertRegistros_PaiNome_Internalname ;
      private String A767InsertRegistros_PaiNome ;
      private String edtInsertRegistros_PaiNome_Jsonclick ;
      private String lblTextblockinsertregistros_cmplchar1_Internalname ;
      private String lblTextblockinsertregistros_cmplchar1_Jsonclick ;
      private String edtInsertRegistros_CmplChar1_Internalname ;
      private String lblTextblockinsertregistros_cmplnum1_Internalname ;
      private String lblTextblockinsertregistros_cmplnum1_Jsonclick ;
      private String edtInsertRegistros_CmplNum1_Internalname ;
      private String edtInsertRegistros_CmplNum1_Jsonclick ;
      private String lblTextblockinsertregistros_tipo_Internalname ;
      private String lblTextblockinsertregistros_tipo_Jsonclick ;
      private String edtInsertRegistros_Tipo_Internalname ;
      private String A770InsertRegistros_Tipo ;
      private String edtInsertRegistros_Tipo_Jsonclick ;
      private String tblTabletoolbar_Internalname ;
      private String divSectiontoolbar_Internalname ;
      private String imgBtn_first_Internalname ;
      private String imgBtn_first_Jsonclick ;
      private String imgBtn_first_separator_Internalname ;
      private String imgBtn_first_separator_Jsonclick ;
      private String imgBtn_previous_Internalname ;
      private String imgBtn_previous_Jsonclick ;
      private String imgBtn_previous_separator_Internalname ;
      private String imgBtn_previous_separator_Jsonclick ;
      private String imgBtn_next_Internalname ;
      private String imgBtn_next_Jsonclick ;
      private String imgBtn_next_separator_Internalname ;
      private String imgBtn_next_separator_Jsonclick ;
      private String imgBtn_last_Internalname ;
      private String imgBtn_last_Jsonclick ;
      private String imgBtn_last_separator_Internalname ;
      private String imgBtn_last_separator_Jsonclick ;
      private String imgBtn_select_Internalname ;
      private String imgBtn_select_Jsonclick ;
      private String imgBtn_select_separator_Internalname ;
      private String imgBtn_select_separator_Jsonclick ;
      private String imgBtn_enter2_Internalname ;
      private String imgBtn_enter2_Jsonclick ;
      private String imgBtn_enter2_separator_Internalname ;
      private String imgBtn_enter2_separator_Jsonclick ;
      private String imgBtn_cancel2_Internalname ;
      private String imgBtn_cancel2_Jsonclick ;
      private String imgBtn_cancel2_separator_Internalname ;
      private String imgBtn_cancel2_separator_Jsonclick ;
      private String imgBtn_delete2_Internalname ;
      private String imgBtn_delete2_Jsonclick ;
      private String imgBtn_delete2_separator_Internalname ;
      private String imgBtn_delete2_separator_Jsonclick ;
      private String Gx_mode ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String sMode99 ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool n767InsertRegistros_PaiNome ;
      private bool n768InsertRegistros_CmplChar1 ;
      private bool n769InsertRegistros_CmplNum1 ;
      private String Z768InsertRegistros_CmplChar1 ;
      private String A768InsertRegistros_CmplChar1 ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] T002J4_A765InsertRegistros_Codigo ;
      private String[] T002J4_A766InsertRegistros_Nome ;
      private String[] T002J4_A767InsertRegistros_PaiNome ;
      private bool[] T002J4_n767InsertRegistros_PaiNome ;
      private String[] T002J4_A768InsertRegistros_CmplChar1 ;
      private bool[] T002J4_n768InsertRegistros_CmplChar1 ;
      private decimal[] T002J4_A769InsertRegistros_CmplNum1 ;
      private bool[] T002J4_n769InsertRegistros_CmplNum1 ;
      private String[] T002J4_A770InsertRegistros_Tipo ;
      private int[] T002J5_A765InsertRegistros_Codigo ;
      private int[] T002J3_A765InsertRegistros_Codigo ;
      private String[] T002J3_A766InsertRegistros_Nome ;
      private String[] T002J3_A767InsertRegistros_PaiNome ;
      private bool[] T002J3_n767InsertRegistros_PaiNome ;
      private String[] T002J3_A768InsertRegistros_CmplChar1 ;
      private bool[] T002J3_n768InsertRegistros_CmplChar1 ;
      private decimal[] T002J3_A769InsertRegistros_CmplNum1 ;
      private bool[] T002J3_n769InsertRegistros_CmplNum1 ;
      private String[] T002J3_A770InsertRegistros_Tipo ;
      private int[] T002J6_A765InsertRegistros_Codigo ;
      private int[] T002J7_A765InsertRegistros_Codigo ;
      private int[] T002J2_A765InsertRegistros_Codigo ;
      private String[] T002J2_A766InsertRegistros_Nome ;
      private String[] T002J2_A767InsertRegistros_PaiNome ;
      private bool[] T002J2_n767InsertRegistros_PaiNome ;
      private String[] T002J2_A768InsertRegistros_CmplChar1 ;
      private bool[] T002J2_n768InsertRegistros_CmplChar1 ;
      private decimal[] T002J2_A769InsertRegistros_CmplNum1 ;
      private bool[] T002J2_n769InsertRegistros_CmplNum1 ;
      private String[] T002J2_A770InsertRegistros_Tipo ;
      private int[] T002J8_A765InsertRegistros_Codigo ;
      private int[] T002J11_A765InsertRegistros_Codigo ;
      private GXWebForm Form ;
   }

   public class insertregistros__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new UpdateCursor(def[7])
         ,new UpdateCursor(def[8])
         ,new ForEachCursor(def[9])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT002J4 ;
          prmT002J4 = new Object[] {
          new Object[] {"@InsertRegistros_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002J5 ;
          prmT002J5 = new Object[] {
          new Object[] {"@InsertRegistros_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002J3 ;
          prmT002J3 = new Object[] {
          new Object[] {"@InsertRegistros_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002J6 ;
          prmT002J6 = new Object[] {
          new Object[] {"@InsertRegistros_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002J7 ;
          prmT002J7 = new Object[] {
          new Object[] {"@InsertRegistros_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002J2 ;
          prmT002J2 = new Object[] {
          new Object[] {"@InsertRegistros_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002J8 ;
          prmT002J8 = new Object[] {
          new Object[] {"@InsertRegistros_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@InsertRegistros_PaiNome",SqlDbType.Char,50,0} ,
          new Object[] {"@InsertRegistros_CmplChar1",SqlDbType.VarChar,400,0} ,
          new Object[] {"@InsertRegistros_CmplNum1",SqlDbType.Decimal,18,3} ,
          new Object[] {"@InsertRegistros_Tipo",SqlDbType.Char,1,0}
          } ;
          Object[] prmT002J9 ;
          prmT002J9 = new Object[] {
          new Object[] {"@InsertRegistros_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@InsertRegistros_PaiNome",SqlDbType.Char,50,0} ,
          new Object[] {"@InsertRegistros_CmplChar1",SqlDbType.VarChar,400,0} ,
          new Object[] {"@InsertRegistros_CmplNum1",SqlDbType.Decimal,18,3} ,
          new Object[] {"@InsertRegistros_Tipo",SqlDbType.Char,1,0} ,
          new Object[] {"@InsertRegistros_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002J10 ;
          prmT002J10 = new Object[] {
          new Object[] {"@InsertRegistros_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002J11 ;
          prmT002J11 = new Object[] {
          } ;
          def= new CursorDef[] {
              new CursorDef("T002J2", "SELECT [InsertRegistros_Codigo], [InsertRegistros_Nome], [InsertRegistros_PaiNome], [InsertRegistros_CmplChar1], [InsertRegistros_CmplNum1], [InsertRegistros_Tipo] FROM [InsertRegistros] WITH (UPDLOCK) WHERE [InsertRegistros_Codigo] = @InsertRegistros_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002J2,1,0,true,false )
             ,new CursorDef("T002J3", "SELECT [InsertRegistros_Codigo], [InsertRegistros_Nome], [InsertRegistros_PaiNome], [InsertRegistros_CmplChar1], [InsertRegistros_CmplNum1], [InsertRegistros_Tipo] FROM [InsertRegistros] WITH (NOLOCK) WHERE [InsertRegistros_Codigo] = @InsertRegistros_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002J3,1,0,true,false )
             ,new CursorDef("T002J4", "SELECT TM1.[InsertRegistros_Codigo], TM1.[InsertRegistros_Nome], TM1.[InsertRegistros_PaiNome], TM1.[InsertRegistros_CmplChar1], TM1.[InsertRegistros_CmplNum1], TM1.[InsertRegistros_Tipo] FROM [InsertRegistros] TM1 WITH (NOLOCK) WHERE TM1.[InsertRegistros_Codigo] = @InsertRegistros_Codigo ORDER BY TM1.[InsertRegistros_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT002J4,100,0,true,false )
             ,new CursorDef("T002J5", "SELECT [InsertRegistros_Codigo] FROM [InsertRegistros] WITH (NOLOCK) WHERE [InsertRegistros_Codigo] = @InsertRegistros_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT002J5,1,0,true,false )
             ,new CursorDef("T002J6", "SELECT TOP 1 [InsertRegistros_Codigo] FROM [InsertRegistros] WITH (NOLOCK) WHERE ( [InsertRegistros_Codigo] > @InsertRegistros_Codigo) ORDER BY [InsertRegistros_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT002J6,1,0,true,true )
             ,new CursorDef("T002J7", "SELECT TOP 1 [InsertRegistros_Codigo] FROM [InsertRegistros] WITH (NOLOCK) WHERE ( [InsertRegistros_Codigo] < @InsertRegistros_Codigo) ORDER BY [InsertRegistros_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT002J7,1,0,true,true )
             ,new CursorDef("T002J8", "INSERT INTO [InsertRegistros]([InsertRegistros_Nome], [InsertRegistros_PaiNome], [InsertRegistros_CmplChar1], [InsertRegistros_CmplNum1], [InsertRegistros_Tipo]) VALUES(@InsertRegistros_Nome, @InsertRegistros_PaiNome, @InsertRegistros_CmplChar1, @InsertRegistros_CmplNum1, @InsertRegistros_Tipo); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT002J8)
             ,new CursorDef("T002J9", "UPDATE [InsertRegistros] SET [InsertRegistros_Nome]=@InsertRegistros_Nome, [InsertRegistros_PaiNome]=@InsertRegistros_PaiNome, [InsertRegistros_CmplChar1]=@InsertRegistros_CmplChar1, [InsertRegistros_CmplNum1]=@InsertRegistros_CmplNum1, [InsertRegistros_Tipo]=@InsertRegistros_Tipo  WHERE [InsertRegistros_Codigo] = @InsertRegistros_Codigo", GxErrorMask.GX_NOMASK,prmT002J9)
             ,new CursorDef("T002J10", "DELETE FROM [InsertRegistros]  WHERE [InsertRegistros_Codigo] = @InsertRegistros_Codigo", GxErrorMask.GX_NOMASK,prmT002J10)
             ,new CursorDef("T002J11", "SELECT [InsertRegistros_Codigo] FROM [InsertRegistros] WITH (NOLOCK) ORDER BY [InsertRegistros_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT002J11,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 50) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((decimal[]) buf[6])[0] = rslt.getDecimal(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getString(6, 1) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 50) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((decimal[]) buf[6])[0] = rslt.getDecimal(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getString(6, 1) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 50) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((decimal[]) buf[6])[0] = rslt.getDecimal(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getString(6, 1) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (String)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[2]);
                }
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 3 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 4 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(4, (decimal)parms[6]);
                }
                stmt.SetParameter(5, (String)parms[7]);
                return;
             case 7 :
                stmt.SetParameter(1, (String)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[2]);
                }
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 3 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 4 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(4, (decimal)parms[6]);
                }
                stmt.SetParameter(5, (String)parms[7]);
                stmt.SetParameter(6, (int)parms[8]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
