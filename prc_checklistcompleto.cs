/*
               File: PRC_CheckListCompleto
        Description: Check List Completo
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:51:30.14
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_checklistcompleto : GXProcedure
   {
      public prc_checklistcompleto( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_checklistcompleto( IGxContext context )
      {
         this.context = context;
         IsMain = false;
      }

      public void release( )
      {
      }

      public void execute( short aP0_Etapa ,
                           out bool aP1_Flag )
      {
         this.AV9Etapa = aP0_Etapa;
         this.AV8Flag = false ;
         initialize();
         executePrivate();
         aP1_Flag=this.AV8Flag;
      }

      public bool executeUdp( short aP0_Etapa )
      {
         this.AV9Etapa = aP0_Etapa;
         this.AV8Flag = false ;
         initialize();
         executePrivate();
         aP1_Flag=this.AV8Flag;
         return AV8Flag ;
      }

      public void executeSubmit( short aP0_Etapa ,
                                 out bool aP1_Flag )
      {
         prc_checklistcompleto objprc_checklistcompleto;
         objprc_checklistcompleto = new prc_checklistcompleto();
         objprc_checklistcompleto.AV9Etapa = aP0_Etapa;
         objprc_checklistcompleto.AV8Flag = false ;
         objprc_checklistcompleto.context.SetSubmitInitialConfig(context);
         objprc_checklistcompleto.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_checklistcompleto);
         aP1_Flag=this.AV8Flag;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_checklistcompleto)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV11SDT_CheckList.FromXml(AV10WebSession.Get("CheckList"), "");
         AV8Flag = true;
         AV15GXV1 = 1;
         while ( AV15GXV1 <= AV11SDT_CheckList.Count )
         {
            AV12SDT_Item = ((SdtSDT_CheckList_Item)AV11SDT_CheckList.Item(AV15GXV1));
            if ( ( AV9Etapa == 1 ) && ( StringUtil.StrCmp(StringUtil.Substring( AV12SDT_Item.gxTpr_Descricao, 1, 1), "1") == 0 ) )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( AV12SDT_Item.gxTpr_Cumpre)) || ( StringUtil.StrCmp(AV12SDT_Item.gxTpr_Cumpre, "N") == 0 ) )
               {
                  AV8Flag = false;
                  this.cleanup();
                  if (true) return;
               }
            }
            else if ( ( AV9Etapa == 2 ) && ( StringUtil.StrCmp(StringUtil.Substring( AV12SDT_Item.gxTpr_Descricao, 1, 1), "2") == 0 ) )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( AV12SDT_Item.gxTpr_Cumpre)) || ( StringUtil.StrCmp(AV12SDT_Item.gxTpr_Cumpre, "N") == 0 ) )
               {
                  AV8Flag = false;
                  this.cleanup();
                  if (true) return;
               }
            }
            AV15GXV1 = (int)(AV15GXV1+1);
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV11SDT_CheckList = new GxObjectCollection( context, "SDT_CheckList.Item", "GxEv3Up14_MeetrikaVs3", "SdtSDT_CheckList_Item", "GeneXus.Programs");
         AV10WebSession = context.GetSession();
         AV12SDT_Item = new SdtSDT_CheckList_Item(context);
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV9Etapa ;
      private int AV15GXV1 ;
      private bool AV8Flag ;
      private IGxSession AV10WebSession ;
      private bool aP1_Flag ;
      [ObjectCollection(ItemType=typeof( SdtSDT_CheckList_Item ))]
      private IGxCollection AV11SDT_CheckList ;
      private SdtSDT_CheckList_Item AV12SDT_Item ;
   }

}
