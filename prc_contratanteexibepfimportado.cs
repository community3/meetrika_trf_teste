/*
               File: PRC_ContratanteExibePFImportado
        Description: Verifca se a Contratante exibe o PF Bruto e L�quido
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/30/2020 21:26:51.40
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_contratanteexibepfimportado : GXProcedure
   {
      public prc_contratanteexibepfimportado( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_contratanteexibepfimportado( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_Contratante_Codigo ,
                           ref bool aP1_Seleciona )
      {
         this.A29Contratante_Codigo = aP0_Contratante_Codigo;
         this.AV8Seleciona = aP1_Seleciona;
         initialize();
         executePrivate();
         aP0_Contratante_Codigo=this.A29Contratante_Codigo;
         aP1_Seleciona=this.AV8Seleciona;
      }

      public bool executeUdp( ref int aP0_Contratante_Codigo )
      {
         this.A29Contratante_Codigo = aP0_Contratante_Codigo;
         this.AV8Seleciona = aP1_Seleciona;
         initialize();
         executePrivate();
         aP0_Contratante_Codigo=this.A29Contratante_Codigo;
         aP1_Seleciona=this.AV8Seleciona;
         return AV8Seleciona ;
      }

      public void executeSubmit( ref int aP0_Contratante_Codigo ,
                                 ref bool aP1_Seleciona )
      {
         prc_contratanteexibepfimportado objprc_contratanteexibepfimportado;
         objprc_contratanteexibepfimportado = new prc_contratanteexibepfimportado();
         objprc_contratanteexibepfimportado.A29Contratante_Codigo = aP0_Contratante_Codigo;
         objprc_contratanteexibepfimportado.AV8Seleciona = aP1_Seleciona;
         objprc_contratanteexibepfimportado.context.SetSubmitInitialConfig(context);
         objprc_contratanteexibepfimportado.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_contratanteexibepfimportado);
         aP0_Contratante_Codigo=this.A29Contratante_Codigo;
         aP1_Seleciona=this.AV8Seleciona;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_contratanteexibepfimportado)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00XP2 */
         pr_default.execute(0, new Object[] {A29Contratante_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A2090Contratante_ExibePF = P00XP2_A2090Contratante_ExibePF[0];
            AV8Seleciona = A2090Contratante_ExibePF;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00XP2_A29Contratante_Codigo = new int[1] ;
         P00XP2_A2090Contratante_ExibePF = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_contratanteexibepfimportado__default(),
            new Object[][] {
                new Object[] {
               P00XP2_A29Contratante_Codigo, P00XP2_A2090Contratante_ExibePF
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A29Contratante_Codigo ;
      private String scmdbuf ;
      private bool AV8Seleciona ;
      private bool A2090Contratante_ExibePF ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_Contratante_Codigo ;
      private bool aP1_Seleciona ;
      private IDataStoreProvider pr_default ;
      private int[] P00XP2_A29Contratante_Codigo ;
      private bool[] P00XP2_A2090Contratante_ExibePF ;
   }

   public class prc_contratanteexibepfimportado__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00XP2 ;
          prmP00XP2 = new Object[] {
          new Object[] {"@Contratante_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00XP2", "SELECT TOP 1 [Contratante_Codigo], [Contratante_ExibePF] FROM [Contratante] WITH (NOLOCK) WHERE [Contratante_Codigo] = @Contratante_Codigo ORDER BY [Contratante_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00XP2,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
