/*
               File: type_SdtContratanteUsuario
        Description: Contratante Usuario
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/30/2020 21:29:22.5
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "ContratanteUsuario" )]
   [XmlType(TypeName =  "ContratanteUsuario" , Namespace = "GxEv3Up14_MeetrikaVs3" )]
   [Serializable]
   public class SdtContratanteUsuario : GxSilentTrnSdt, System.Web.SessionState.IRequiresSessionState
   {
      public SdtContratanteUsuario( )
      {
         /* Constructor for serialization */
         gxTv_SdtContratanteUsuario_Contratanteusuario_contratantefan = "";
         gxTv_SdtContratanteUsuario_Contratanteusuario_contratanteraz = "";
         gxTv_SdtContratanteUsuario_Usuario_nome = "";
         gxTv_SdtContratanteUsuario_Contratanteusuario_usuariopessoanom = "";
         gxTv_SdtContratanteUsuario_Contratanteusuario_usuariopessoadoc = "";
         gxTv_SdtContratanteUsuario_Usuario_usergamguid = "";
         gxTv_SdtContratanteUsuario_Mode = "";
         gxTv_SdtContratanteUsuario_Contratanteusuario_contratantefan_Z = "";
         gxTv_SdtContratanteUsuario_Contratanteusuario_contratanteraz_Z = "";
         gxTv_SdtContratanteUsuario_Usuario_nome_Z = "";
         gxTv_SdtContratanteUsuario_Contratanteusuario_usuariopessoanom_Z = "";
         gxTv_SdtContratanteUsuario_Contratanteusuario_usuariopessoadoc_Z = "";
         gxTv_SdtContratanteUsuario_Usuario_usergamguid_Z = "";
      }

      public SdtContratanteUsuario( IGxContext context )
      {
         this.context = context;
         constructorCallingAssembly = Assembly.GetCallingAssembly();
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public void Load( int AV63ContratanteUsuario_ContratanteCod ,
                        int AV60ContratanteUsuario_UsuarioCod )
      {
         IGxSilentTrn obj ;
         obj = getTransaction();
         obj.LoadKey(new Object[] {(int)AV63ContratanteUsuario_ContratanteCod,(int)AV60ContratanteUsuario_UsuarioCod});
         return  ;
      }

      public override Object[][] GetBCKey( )
      {
         return (Object[][])(new Object[][]{new Object[]{"ContratanteUsuario_ContratanteCod", typeof(int)}, new Object[]{"ContratanteUsuario_UsuarioCod", typeof(int)}}) ;
      }

      public override IGxCollection GetMessages( )
      {
         short item = 1 ;
         IGxCollection msgs = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs") ;
         SdtMessages_Message m1 ;
         IGxSilentTrn trn = getTransaction() ;
         msglist msgList = trn.GetMessages() ;
         while ( item <= msgList.ItemCount )
         {
            m1 = new SdtMessages_Message(context);
            m1.gxTpr_Id = msgList.getItemValue(item);
            m1.gxTpr_Description = msgList.getItemText(item);
            m1.gxTpr_Type = msgList.getItemType(item);
            msgs.Add(m1, 0);
            item = (short)(item+1);
         }
         return msgs ;
      }

      public override GXProperties GetMetadata( )
      {
         GXProperties metadata = new GXProperties() ;
         metadata.Set("Name", "ContratanteUsuario");
         metadata.Set("BT", "ContratanteUsuario");
         metadata.Set("PK", "[ \"ContratanteUsuario_ContratanteCod\",\"ContratanteUsuario_UsuarioCod\" ]");
         metadata.Set("PKAssigned", "[ \"ContratanteUsuario_ContratanteCod\",\"ContratanteUsuario_UsuarioCod\" ]");
         metadata.Set("FKList", "[ { \"FK\":[ \"Contratante_Codigo\" ],\"FKMap\":[ \"ContratanteUsuario_ContratanteCod-Contratante_Codigo\" ] },{ \"FK\":[ \"Usuario_Codigo\" ],\"FKMap\":[ \"ContratanteUsuario_UsuarioCod-Usuario_Codigo\" ] } ]");
         metadata.Set("AllowInsert", "True");
         metadata.Set("AllowUpdate", "True");
         metadata.Set("AllowDelete", "True");
         return metadata ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         if ( ! includeState )
         {
            XmlAttributeOverrides ov = new XmlAttributeOverrides();
            XmlAttributes attrs = new XmlAttributes();
            attrs.XmlIgnore = true;
            ov.Add(this.GetType(),  "gxTpr_Mode" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Initialized" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratanteusuario_contratantecod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratanteusuario_contratantepescod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratanteusuario_contratantefan_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratanteusuario_contratanteraz_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratanteusuario_usuariocod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Usuario_nome_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratanteusuario_usuariopessoacod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratanteusuario_usuariopessoanom_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratanteusuario_usuariopessoadoc_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratanteusuario_usuarioehcontratante_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratanteusuario_areatrabalhocod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Usuario_usergamguid_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratanteusuario_ehfiscal_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratanteusuario_contratantepescod_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratanteusuario_contratantefan_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratanteusuario_contratanteraz_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Usuario_nome_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratanteusuario_usuariopessoacod_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratanteusuario_usuariopessoanom_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratanteusuario_usuariopessoadoc_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratanteusuario_usuarioehcontratante_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratanteusuario_areatrabalhocod_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Usuario_usergamguid_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratanteusuario_ehfiscal_N" , attrs);
            xmls = new XmlSerializer(this.GetType(), ov, new Type[0], null, sNameSpace);
         }
         else
         {
            xmls = new XmlSerializer(this.GetType(), sNameSpace);
         }
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtContratanteUsuario deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_MeetrikaVs3" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtContratanteUsuario)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtContratanteUsuario obj ;
         obj = this;
         obj.gxTpr_Contratanteusuario_contratantecod = deserialized.gxTpr_Contratanteusuario_contratantecod;
         obj.gxTpr_Contratanteusuario_contratantepescod = deserialized.gxTpr_Contratanteusuario_contratantepescod;
         obj.gxTpr_Contratanteusuario_contratantefan = deserialized.gxTpr_Contratanteusuario_contratantefan;
         obj.gxTpr_Contratanteusuario_contratanteraz = deserialized.gxTpr_Contratanteusuario_contratanteraz;
         obj.gxTpr_Contratanteusuario_usuariocod = deserialized.gxTpr_Contratanteusuario_usuariocod;
         obj.gxTpr_Usuario_nome = deserialized.gxTpr_Usuario_nome;
         obj.gxTpr_Contratanteusuario_usuariopessoacod = deserialized.gxTpr_Contratanteusuario_usuariopessoacod;
         obj.gxTpr_Contratanteusuario_usuariopessoanom = deserialized.gxTpr_Contratanteusuario_usuariopessoanom;
         obj.gxTpr_Contratanteusuario_usuariopessoadoc = deserialized.gxTpr_Contratanteusuario_usuariopessoadoc;
         obj.gxTpr_Contratanteusuario_usuarioehcontratante = deserialized.gxTpr_Contratanteusuario_usuarioehcontratante;
         obj.gxTpr_Contratanteusuario_areatrabalhocod = deserialized.gxTpr_Contratanteusuario_areatrabalhocod;
         obj.gxTpr_Usuario_usergamguid = deserialized.gxTpr_Usuario_usergamguid;
         obj.gxTpr_Contratanteusuario_ehfiscal = deserialized.gxTpr_Contratanteusuario_ehfiscal;
         obj.gxTpr_Mode = deserialized.gxTpr_Mode;
         obj.gxTpr_Initialized = deserialized.gxTpr_Initialized;
         obj.gxTpr_Contratanteusuario_contratantecod_Z = deserialized.gxTpr_Contratanteusuario_contratantecod_Z;
         obj.gxTpr_Contratanteusuario_contratantepescod_Z = deserialized.gxTpr_Contratanteusuario_contratantepescod_Z;
         obj.gxTpr_Contratanteusuario_contratantefan_Z = deserialized.gxTpr_Contratanteusuario_contratantefan_Z;
         obj.gxTpr_Contratanteusuario_contratanteraz_Z = deserialized.gxTpr_Contratanteusuario_contratanteraz_Z;
         obj.gxTpr_Contratanteusuario_usuariocod_Z = deserialized.gxTpr_Contratanteusuario_usuariocod_Z;
         obj.gxTpr_Usuario_nome_Z = deserialized.gxTpr_Usuario_nome_Z;
         obj.gxTpr_Contratanteusuario_usuariopessoacod_Z = deserialized.gxTpr_Contratanteusuario_usuariopessoacod_Z;
         obj.gxTpr_Contratanteusuario_usuariopessoanom_Z = deserialized.gxTpr_Contratanteusuario_usuariopessoanom_Z;
         obj.gxTpr_Contratanteusuario_usuariopessoadoc_Z = deserialized.gxTpr_Contratanteusuario_usuariopessoadoc_Z;
         obj.gxTpr_Contratanteusuario_usuarioehcontratante_Z = deserialized.gxTpr_Contratanteusuario_usuarioehcontratante_Z;
         obj.gxTpr_Contratanteusuario_areatrabalhocod_Z = deserialized.gxTpr_Contratanteusuario_areatrabalhocod_Z;
         obj.gxTpr_Usuario_usergamguid_Z = deserialized.gxTpr_Usuario_usergamguid_Z;
         obj.gxTpr_Contratanteusuario_ehfiscal_Z = deserialized.gxTpr_Contratanteusuario_ehfiscal_Z;
         obj.gxTpr_Contratanteusuario_contratantepescod_N = deserialized.gxTpr_Contratanteusuario_contratantepescod_N;
         obj.gxTpr_Contratanteusuario_contratantefan_N = deserialized.gxTpr_Contratanteusuario_contratantefan_N;
         obj.gxTpr_Contratanteusuario_contratanteraz_N = deserialized.gxTpr_Contratanteusuario_contratanteraz_N;
         obj.gxTpr_Usuario_nome_N = deserialized.gxTpr_Usuario_nome_N;
         obj.gxTpr_Contratanteusuario_usuariopessoacod_N = deserialized.gxTpr_Contratanteusuario_usuariopessoacod_N;
         obj.gxTpr_Contratanteusuario_usuariopessoanom_N = deserialized.gxTpr_Contratanteusuario_usuariopessoanom_N;
         obj.gxTpr_Contratanteusuario_usuariopessoadoc_N = deserialized.gxTpr_Contratanteusuario_usuariopessoadoc_N;
         obj.gxTpr_Contratanteusuario_usuarioehcontratante_N = deserialized.gxTpr_Contratanteusuario_usuarioehcontratante_N;
         obj.gxTpr_Contratanteusuario_areatrabalhocod_N = deserialized.gxTpr_Contratanteusuario_areatrabalhocod_N;
         obj.gxTpr_Usuario_usergamguid_N = deserialized.gxTpr_Usuario_usergamguid_N;
         obj.gxTpr_Contratanteusuario_ehfiscal_N = deserialized.gxTpr_Contratanteusuario_ehfiscal_N;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratanteUsuario_ContratanteCod") )
               {
                  gxTv_SdtContratanteUsuario_Contratanteusuario_contratantecod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratanteUsuario_ContratantePesCod") )
               {
                  gxTv_SdtContratanteUsuario_Contratanteusuario_contratantepescod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratanteUsuario_ContratanteFan") )
               {
                  gxTv_SdtContratanteUsuario_Contratanteusuario_contratantefan = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratanteUsuario_ContratanteRaz") )
               {
                  gxTv_SdtContratanteUsuario_Contratanteusuario_contratanteraz = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratanteUsuario_UsuarioCod") )
               {
                  gxTv_SdtContratanteUsuario_Contratanteusuario_usuariocod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_Nome") )
               {
                  gxTv_SdtContratanteUsuario_Usuario_nome = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratanteUsuario_UsuarioPessoaCod") )
               {
                  gxTv_SdtContratanteUsuario_Contratanteusuario_usuariopessoacod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratanteUsuario_UsuarioPessoaNom") )
               {
                  gxTv_SdtContratanteUsuario_Contratanteusuario_usuariopessoanom = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratanteUsuario_UsuarioPessoaDoc") )
               {
                  gxTv_SdtContratanteUsuario_Contratanteusuario_usuariopessoadoc = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratanteUsuario_UsuarioEhContratante") )
               {
                  gxTv_SdtContratanteUsuario_Contratanteusuario_usuarioehcontratante = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratanteUsuario_AreaTrabalhoCod") )
               {
                  gxTv_SdtContratanteUsuario_Contratanteusuario_areatrabalhocod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_UserGamGuid") )
               {
                  gxTv_SdtContratanteUsuario_Usuario_usergamguid = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratanteUsuario_EhFiscal") )
               {
                  gxTv_SdtContratanteUsuario_Contratanteusuario_ehfiscal = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Mode") )
               {
                  gxTv_SdtContratanteUsuario_Mode = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Initialized") )
               {
                  gxTv_SdtContratanteUsuario_Initialized = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratanteUsuario_ContratanteCod_Z") )
               {
                  gxTv_SdtContratanteUsuario_Contratanteusuario_contratantecod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratanteUsuario_ContratantePesCod_Z") )
               {
                  gxTv_SdtContratanteUsuario_Contratanteusuario_contratantepescod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratanteUsuario_ContratanteFan_Z") )
               {
                  gxTv_SdtContratanteUsuario_Contratanteusuario_contratantefan_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratanteUsuario_ContratanteRaz_Z") )
               {
                  gxTv_SdtContratanteUsuario_Contratanteusuario_contratanteraz_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratanteUsuario_UsuarioCod_Z") )
               {
                  gxTv_SdtContratanteUsuario_Contratanteusuario_usuariocod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_Nome_Z") )
               {
                  gxTv_SdtContratanteUsuario_Usuario_nome_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratanteUsuario_UsuarioPessoaCod_Z") )
               {
                  gxTv_SdtContratanteUsuario_Contratanteusuario_usuariopessoacod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratanteUsuario_UsuarioPessoaNom_Z") )
               {
                  gxTv_SdtContratanteUsuario_Contratanteusuario_usuariopessoanom_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratanteUsuario_UsuarioPessoaDoc_Z") )
               {
                  gxTv_SdtContratanteUsuario_Contratanteusuario_usuariopessoadoc_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratanteUsuario_UsuarioEhContratante_Z") )
               {
                  gxTv_SdtContratanteUsuario_Contratanteusuario_usuarioehcontratante_Z = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratanteUsuario_AreaTrabalhoCod_Z") )
               {
                  gxTv_SdtContratanteUsuario_Contratanteusuario_areatrabalhocod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_UserGamGuid_Z") )
               {
                  gxTv_SdtContratanteUsuario_Usuario_usergamguid_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratanteUsuario_EhFiscal_Z") )
               {
                  gxTv_SdtContratanteUsuario_Contratanteusuario_ehfiscal_Z = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratanteUsuario_ContratantePesCod_N") )
               {
                  gxTv_SdtContratanteUsuario_Contratanteusuario_contratantepescod_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratanteUsuario_ContratanteFan_N") )
               {
                  gxTv_SdtContratanteUsuario_Contratanteusuario_contratantefan_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratanteUsuario_ContratanteRaz_N") )
               {
                  gxTv_SdtContratanteUsuario_Contratanteusuario_contratanteraz_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_Nome_N") )
               {
                  gxTv_SdtContratanteUsuario_Usuario_nome_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratanteUsuario_UsuarioPessoaCod_N") )
               {
                  gxTv_SdtContratanteUsuario_Contratanteusuario_usuariopessoacod_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratanteUsuario_UsuarioPessoaNom_N") )
               {
                  gxTv_SdtContratanteUsuario_Contratanteusuario_usuariopessoanom_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratanteUsuario_UsuarioPessoaDoc_N") )
               {
                  gxTv_SdtContratanteUsuario_Contratanteusuario_usuariopessoadoc_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratanteUsuario_UsuarioEhContratante_N") )
               {
                  gxTv_SdtContratanteUsuario_Contratanteusuario_usuarioehcontratante_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratanteUsuario_AreaTrabalhoCod_N") )
               {
                  gxTv_SdtContratanteUsuario_Contratanteusuario_areatrabalhocod_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_UserGamGuid_N") )
               {
                  gxTv_SdtContratanteUsuario_Usuario_usergamguid_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContratanteUsuario_EhFiscal_N") )
               {
                  gxTv_SdtContratanteUsuario_Contratanteusuario_ehfiscal_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "ContratanteUsuario";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "GxEv3Up14_MeetrikaVs3";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("ContratanteUsuario_ContratanteCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratanteUsuario_Contratanteusuario_contratantecod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContratanteUsuario_ContratantePesCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratanteUsuario_Contratanteusuario_contratantepescod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContratanteUsuario_ContratanteFan", StringUtil.RTrim( gxTv_SdtContratanteUsuario_Contratanteusuario_contratantefan));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContratanteUsuario_ContratanteRaz", StringUtil.RTrim( gxTv_SdtContratanteUsuario_Contratanteusuario_contratanteraz));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContratanteUsuario_UsuarioCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratanteUsuario_Contratanteusuario_usuariocod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Usuario_Nome", StringUtil.RTrim( gxTv_SdtContratanteUsuario_Usuario_nome));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContratanteUsuario_UsuarioPessoaCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratanteUsuario_Contratanteusuario_usuariopessoacod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContratanteUsuario_UsuarioPessoaNom", StringUtil.RTrim( gxTv_SdtContratanteUsuario_Contratanteusuario_usuariopessoanom));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContratanteUsuario_UsuarioPessoaDoc", StringUtil.RTrim( gxTv_SdtContratanteUsuario_Contratanteusuario_usuariopessoadoc));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContratanteUsuario_UsuarioEhContratante", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtContratanteUsuario_Contratanteusuario_usuarioehcontratante)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContratanteUsuario_AreaTrabalhoCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratanteUsuario_Contratanteusuario_areatrabalhocod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Usuario_UserGamGuid", StringUtil.RTrim( gxTv_SdtContratanteUsuario_Usuario_usergamguid));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContratanteUsuario_EhFiscal", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtContratanteUsuario_Contratanteusuario_ehfiscal)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         if ( sIncludeState )
         {
            oWriter.WriteElement("Mode", StringUtil.RTrim( gxTv_SdtContratanteUsuario_Mode));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Initialized", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratanteUsuario_Initialized), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratanteUsuario_ContratanteCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratanteUsuario_Contratanteusuario_contratantecod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratanteUsuario_ContratantePesCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratanteUsuario_Contratanteusuario_contratantepescod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratanteUsuario_ContratanteFan_Z", StringUtil.RTrim( gxTv_SdtContratanteUsuario_Contratanteusuario_contratantefan_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratanteUsuario_ContratanteRaz_Z", StringUtil.RTrim( gxTv_SdtContratanteUsuario_Contratanteusuario_contratanteraz_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratanteUsuario_UsuarioCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratanteUsuario_Contratanteusuario_usuariocod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Usuario_Nome_Z", StringUtil.RTrim( gxTv_SdtContratanteUsuario_Usuario_nome_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratanteUsuario_UsuarioPessoaCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratanteUsuario_Contratanteusuario_usuariopessoacod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratanteUsuario_UsuarioPessoaNom_Z", StringUtil.RTrim( gxTv_SdtContratanteUsuario_Contratanteusuario_usuariopessoanom_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratanteUsuario_UsuarioPessoaDoc_Z", StringUtil.RTrim( gxTv_SdtContratanteUsuario_Contratanteusuario_usuariopessoadoc_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratanteUsuario_UsuarioEhContratante_Z", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtContratanteUsuario_Contratanteusuario_usuarioehcontratante_Z)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratanteUsuario_AreaTrabalhoCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratanteUsuario_Contratanteusuario_areatrabalhocod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Usuario_UserGamGuid_Z", StringUtil.RTrim( gxTv_SdtContratanteUsuario_Usuario_usergamguid_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratanteUsuario_EhFiscal_Z", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtContratanteUsuario_Contratanteusuario_ehfiscal_Z)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratanteUsuario_ContratantePesCod_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratanteUsuario_Contratanteusuario_contratantepescod_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratanteUsuario_ContratanteFan_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratanteUsuario_Contratanteusuario_contratantefan_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratanteUsuario_ContratanteRaz_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratanteUsuario_Contratanteusuario_contratanteraz_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Usuario_Nome_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratanteUsuario_Usuario_nome_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratanteUsuario_UsuarioPessoaCod_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratanteUsuario_Contratanteusuario_usuariopessoacod_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratanteUsuario_UsuarioPessoaNom_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratanteUsuario_Contratanteusuario_usuariopessoanom_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratanteUsuario_UsuarioPessoaDoc_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratanteUsuario_Contratanteusuario_usuariopessoadoc_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratanteUsuario_UsuarioEhContratante_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratanteUsuario_Contratanteusuario_usuarioehcontratante_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratanteUsuario_AreaTrabalhoCod_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratanteUsuario_Contratanteusuario_areatrabalhocod_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Usuario_UserGamGuid_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratanteUsuario_Usuario_usergamguid_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContratanteUsuario_EhFiscal_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContratanteUsuario_Contratanteusuario_ehfiscal_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("ContratanteUsuario_ContratanteCod", gxTv_SdtContratanteUsuario_Contratanteusuario_contratantecod, false);
         AddObjectProperty("ContratanteUsuario_ContratantePesCod", gxTv_SdtContratanteUsuario_Contratanteusuario_contratantepescod, false);
         AddObjectProperty("ContratanteUsuario_ContratanteFan", gxTv_SdtContratanteUsuario_Contratanteusuario_contratantefan, false);
         AddObjectProperty("ContratanteUsuario_ContratanteRaz", gxTv_SdtContratanteUsuario_Contratanteusuario_contratanteraz, false);
         AddObjectProperty("ContratanteUsuario_UsuarioCod", gxTv_SdtContratanteUsuario_Contratanteusuario_usuariocod, false);
         AddObjectProperty("Usuario_Nome", gxTv_SdtContratanteUsuario_Usuario_nome, false);
         AddObjectProperty("ContratanteUsuario_UsuarioPessoaCod", gxTv_SdtContratanteUsuario_Contratanteusuario_usuariopessoacod, false);
         AddObjectProperty("ContratanteUsuario_UsuarioPessoaNom", gxTv_SdtContratanteUsuario_Contratanteusuario_usuariopessoanom, false);
         AddObjectProperty("ContratanteUsuario_UsuarioPessoaDoc", gxTv_SdtContratanteUsuario_Contratanteusuario_usuariopessoadoc, false);
         AddObjectProperty("ContratanteUsuario_UsuarioEhContratante", gxTv_SdtContratanteUsuario_Contratanteusuario_usuarioehcontratante, false);
         AddObjectProperty("ContratanteUsuario_AreaTrabalhoCod", gxTv_SdtContratanteUsuario_Contratanteusuario_areatrabalhocod, false);
         AddObjectProperty("Usuario_UserGamGuid", gxTv_SdtContratanteUsuario_Usuario_usergamguid, false);
         AddObjectProperty("ContratanteUsuario_EhFiscal", gxTv_SdtContratanteUsuario_Contratanteusuario_ehfiscal, false);
         if ( includeState )
         {
            AddObjectProperty("Mode", gxTv_SdtContratanteUsuario_Mode, false);
            AddObjectProperty("Initialized", gxTv_SdtContratanteUsuario_Initialized, false);
            AddObjectProperty("ContratanteUsuario_ContratanteCod_Z", gxTv_SdtContratanteUsuario_Contratanteusuario_contratantecod_Z, false);
            AddObjectProperty("ContratanteUsuario_ContratantePesCod_Z", gxTv_SdtContratanteUsuario_Contratanteusuario_contratantepescod_Z, false);
            AddObjectProperty("ContratanteUsuario_ContratanteFan_Z", gxTv_SdtContratanteUsuario_Contratanteusuario_contratantefan_Z, false);
            AddObjectProperty("ContratanteUsuario_ContratanteRaz_Z", gxTv_SdtContratanteUsuario_Contratanteusuario_contratanteraz_Z, false);
            AddObjectProperty("ContratanteUsuario_UsuarioCod_Z", gxTv_SdtContratanteUsuario_Contratanteusuario_usuariocod_Z, false);
            AddObjectProperty("Usuario_Nome_Z", gxTv_SdtContratanteUsuario_Usuario_nome_Z, false);
            AddObjectProperty("ContratanteUsuario_UsuarioPessoaCod_Z", gxTv_SdtContratanteUsuario_Contratanteusuario_usuariopessoacod_Z, false);
            AddObjectProperty("ContratanteUsuario_UsuarioPessoaNom_Z", gxTv_SdtContratanteUsuario_Contratanteusuario_usuariopessoanom_Z, false);
            AddObjectProperty("ContratanteUsuario_UsuarioPessoaDoc_Z", gxTv_SdtContratanteUsuario_Contratanteusuario_usuariopessoadoc_Z, false);
            AddObjectProperty("ContratanteUsuario_UsuarioEhContratante_Z", gxTv_SdtContratanteUsuario_Contratanteusuario_usuarioehcontratante_Z, false);
            AddObjectProperty("ContratanteUsuario_AreaTrabalhoCod_Z", gxTv_SdtContratanteUsuario_Contratanteusuario_areatrabalhocod_Z, false);
            AddObjectProperty("Usuario_UserGamGuid_Z", gxTv_SdtContratanteUsuario_Usuario_usergamguid_Z, false);
            AddObjectProperty("ContratanteUsuario_EhFiscal_Z", gxTv_SdtContratanteUsuario_Contratanteusuario_ehfiscal_Z, false);
            AddObjectProperty("ContratanteUsuario_ContratantePesCod_N", gxTv_SdtContratanteUsuario_Contratanteusuario_contratantepescod_N, false);
            AddObjectProperty("ContratanteUsuario_ContratanteFan_N", gxTv_SdtContratanteUsuario_Contratanteusuario_contratantefan_N, false);
            AddObjectProperty("ContratanteUsuario_ContratanteRaz_N", gxTv_SdtContratanteUsuario_Contratanteusuario_contratanteraz_N, false);
            AddObjectProperty("Usuario_Nome_N", gxTv_SdtContratanteUsuario_Usuario_nome_N, false);
            AddObjectProperty("ContratanteUsuario_UsuarioPessoaCod_N", gxTv_SdtContratanteUsuario_Contratanteusuario_usuariopessoacod_N, false);
            AddObjectProperty("ContratanteUsuario_UsuarioPessoaNom_N", gxTv_SdtContratanteUsuario_Contratanteusuario_usuariopessoanom_N, false);
            AddObjectProperty("ContratanteUsuario_UsuarioPessoaDoc_N", gxTv_SdtContratanteUsuario_Contratanteusuario_usuariopessoadoc_N, false);
            AddObjectProperty("ContratanteUsuario_UsuarioEhContratante_N", gxTv_SdtContratanteUsuario_Contratanteusuario_usuarioehcontratante_N, false);
            AddObjectProperty("ContratanteUsuario_AreaTrabalhoCod_N", gxTv_SdtContratanteUsuario_Contratanteusuario_areatrabalhocod_N, false);
            AddObjectProperty("Usuario_UserGamGuid_N", gxTv_SdtContratanteUsuario_Usuario_usergamguid_N, false);
            AddObjectProperty("ContratanteUsuario_EhFiscal_N", gxTv_SdtContratanteUsuario_Contratanteusuario_ehfiscal_N, false);
         }
         return  ;
      }

      [  SoapElement( ElementName = "ContratanteUsuario_ContratanteCod" )]
      [  XmlElement( ElementName = "ContratanteUsuario_ContratanteCod"   )]
      public int gxTpr_Contratanteusuario_contratantecod
      {
         get {
            return gxTv_SdtContratanteUsuario_Contratanteusuario_contratantecod ;
         }

         set {
            if ( gxTv_SdtContratanteUsuario_Contratanteusuario_contratantecod != value )
            {
               gxTv_SdtContratanteUsuario_Mode = "INS";
               this.gxTv_SdtContratanteUsuario_Contratanteusuario_contratantecod_Z_SetNull( );
               this.gxTv_SdtContratanteUsuario_Contratanteusuario_contratantepescod_Z_SetNull( );
               this.gxTv_SdtContratanteUsuario_Contratanteusuario_contratantefan_Z_SetNull( );
               this.gxTv_SdtContratanteUsuario_Contratanteusuario_contratanteraz_Z_SetNull( );
               this.gxTv_SdtContratanteUsuario_Contratanteusuario_usuariocod_Z_SetNull( );
               this.gxTv_SdtContratanteUsuario_Usuario_nome_Z_SetNull( );
               this.gxTv_SdtContratanteUsuario_Contratanteusuario_usuariopessoacod_Z_SetNull( );
               this.gxTv_SdtContratanteUsuario_Contratanteusuario_usuariopessoanom_Z_SetNull( );
               this.gxTv_SdtContratanteUsuario_Contratanteusuario_usuariopessoadoc_Z_SetNull( );
               this.gxTv_SdtContratanteUsuario_Contratanteusuario_usuarioehcontratante_Z_SetNull( );
               this.gxTv_SdtContratanteUsuario_Contratanteusuario_areatrabalhocod_Z_SetNull( );
               this.gxTv_SdtContratanteUsuario_Usuario_usergamguid_Z_SetNull( );
               this.gxTv_SdtContratanteUsuario_Contratanteusuario_ehfiscal_Z_SetNull( );
            }
            gxTv_SdtContratanteUsuario_Contratanteusuario_contratantecod = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ContratanteUsuario_ContratantePesCod" )]
      [  XmlElement( ElementName = "ContratanteUsuario_ContratantePesCod"   )]
      public int gxTpr_Contratanteusuario_contratantepescod
      {
         get {
            return gxTv_SdtContratanteUsuario_Contratanteusuario_contratantepescod ;
         }

         set {
            gxTv_SdtContratanteUsuario_Contratanteusuario_contratantepescod_N = 0;
            gxTv_SdtContratanteUsuario_Contratanteusuario_contratantepescod = (int)(value);
         }

      }

      public void gxTv_SdtContratanteUsuario_Contratanteusuario_contratantepescod_SetNull( )
      {
         gxTv_SdtContratanteUsuario_Contratanteusuario_contratantepescod_N = 1;
         gxTv_SdtContratanteUsuario_Contratanteusuario_contratantepescod = 0;
         return  ;
      }

      public bool gxTv_SdtContratanteUsuario_Contratanteusuario_contratantepescod_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratanteUsuario_ContratanteFan" )]
      [  XmlElement( ElementName = "ContratanteUsuario_ContratanteFan"   )]
      public String gxTpr_Contratanteusuario_contratantefan
      {
         get {
            return gxTv_SdtContratanteUsuario_Contratanteusuario_contratantefan ;
         }

         set {
            gxTv_SdtContratanteUsuario_Contratanteusuario_contratantefan_N = 0;
            gxTv_SdtContratanteUsuario_Contratanteusuario_contratantefan = (String)(value);
         }

      }

      public void gxTv_SdtContratanteUsuario_Contratanteusuario_contratantefan_SetNull( )
      {
         gxTv_SdtContratanteUsuario_Contratanteusuario_contratantefan_N = 1;
         gxTv_SdtContratanteUsuario_Contratanteusuario_contratantefan = "";
         return  ;
      }

      public bool gxTv_SdtContratanteUsuario_Contratanteusuario_contratantefan_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratanteUsuario_ContratanteRaz" )]
      [  XmlElement( ElementName = "ContratanteUsuario_ContratanteRaz"   )]
      public String gxTpr_Contratanteusuario_contratanteraz
      {
         get {
            return gxTv_SdtContratanteUsuario_Contratanteusuario_contratanteraz ;
         }

         set {
            gxTv_SdtContratanteUsuario_Contratanteusuario_contratanteraz_N = 0;
            gxTv_SdtContratanteUsuario_Contratanteusuario_contratanteraz = (String)(value);
         }

      }

      public void gxTv_SdtContratanteUsuario_Contratanteusuario_contratanteraz_SetNull( )
      {
         gxTv_SdtContratanteUsuario_Contratanteusuario_contratanteraz_N = 1;
         gxTv_SdtContratanteUsuario_Contratanteusuario_contratanteraz = "";
         return  ;
      }

      public bool gxTv_SdtContratanteUsuario_Contratanteusuario_contratanteraz_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratanteUsuario_UsuarioCod" )]
      [  XmlElement( ElementName = "ContratanteUsuario_UsuarioCod"   )]
      public int gxTpr_Contratanteusuario_usuariocod
      {
         get {
            return gxTv_SdtContratanteUsuario_Contratanteusuario_usuariocod ;
         }

         set {
            if ( gxTv_SdtContratanteUsuario_Contratanteusuario_usuariocod != value )
            {
               gxTv_SdtContratanteUsuario_Mode = "INS";
               this.gxTv_SdtContratanteUsuario_Contratanteusuario_contratantecod_Z_SetNull( );
               this.gxTv_SdtContratanteUsuario_Contratanteusuario_contratantepescod_Z_SetNull( );
               this.gxTv_SdtContratanteUsuario_Contratanteusuario_contratantefan_Z_SetNull( );
               this.gxTv_SdtContratanteUsuario_Contratanteusuario_contratanteraz_Z_SetNull( );
               this.gxTv_SdtContratanteUsuario_Contratanteusuario_usuariocod_Z_SetNull( );
               this.gxTv_SdtContratanteUsuario_Usuario_nome_Z_SetNull( );
               this.gxTv_SdtContratanteUsuario_Contratanteusuario_usuariopessoacod_Z_SetNull( );
               this.gxTv_SdtContratanteUsuario_Contratanteusuario_usuariopessoanom_Z_SetNull( );
               this.gxTv_SdtContratanteUsuario_Contratanteusuario_usuariopessoadoc_Z_SetNull( );
               this.gxTv_SdtContratanteUsuario_Contratanteusuario_usuarioehcontratante_Z_SetNull( );
               this.gxTv_SdtContratanteUsuario_Contratanteusuario_areatrabalhocod_Z_SetNull( );
               this.gxTv_SdtContratanteUsuario_Usuario_usergamguid_Z_SetNull( );
               this.gxTv_SdtContratanteUsuario_Contratanteusuario_ehfiscal_Z_SetNull( );
            }
            gxTv_SdtContratanteUsuario_Contratanteusuario_usuariocod = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Usuario_Nome" )]
      [  XmlElement( ElementName = "Usuario_Nome"   )]
      public String gxTpr_Usuario_nome
      {
         get {
            return gxTv_SdtContratanteUsuario_Usuario_nome ;
         }

         set {
            gxTv_SdtContratanteUsuario_Usuario_nome_N = 0;
            gxTv_SdtContratanteUsuario_Usuario_nome = (String)(value);
         }

      }

      public void gxTv_SdtContratanteUsuario_Usuario_nome_SetNull( )
      {
         gxTv_SdtContratanteUsuario_Usuario_nome_N = 1;
         gxTv_SdtContratanteUsuario_Usuario_nome = "";
         return  ;
      }

      public bool gxTv_SdtContratanteUsuario_Usuario_nome_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratanteUsuario_UsuarioPessoaCod" )]
      [  XmlElement( ElementName = "ContratanteUsuario_UsuarioPessoaCod"   )]
      public int gxTpr_Contratanteusuario_usuariopessoacod
      {
         get {
            return gxTv_SdtContratanteUsuario_Contratanteusuario_usuariopessoacod ;
         }

         set {
            gxTv_SdtContratanteUsuario_Contratanteusuario_usuariopessoacod_N = 0;
            gxTv_SdtContratanteUsuario_Contratanteusuario_usuariopessoacod = (int)(value);
         }

      }

      public void gxTv_SdtContratanteUsuario_Contratanteusuario_usuariopessoacod_SetNull( )
      {
         gxTv_SdtContratanteUsuario_Contratanteusuario_usuariopessoacod_N = 1;
         gxTv_SdtContratanteUsuario_Contratanteusuario_usuariopessoacod = 0;
         return  ;
      }

      public bool gxTv_SdtContratanteUsuario_Contratanteusuario_usuariopessoacod_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratanteUsuario_UsuarioPessoaNom" )]
      [  XmlElement( ElementName = "ContratanteUsuario_UsuarioPessoaNom"   )]
      public String gxTpr_Contratanteusuario_usuariopessoanom
      {
         get {
            return gxTv_SdtContratanteUsuario_Contratanteusuario_usuariopessoanom ;
         }

         set {
            gxTv_SdtContratanteUsuario_Contratanteusuario_usuariopessoanom_N = 0;
            gxTv_SdtContratanteUsuario_Contratanteusuario_usuariopessoanom = (String)(value);
         }

      }

      public void gxTv_SdtContratanteUsuario_Contratanteusuario_usuariopessoanom_SetNull( )
      {
         gxTv_SdtContratanteUsuario_Contratanteusuario_usuariopessoanom_N = 1;
         gxTv_SdtContratanteUsuario_Contratanteusuario_usuariopessoanom = "";
         return  ;
      }

      public bool gxTv_SdtContratanteUsuario_Contratanteusuario_usuariopessoanom_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratanteUsuario_UsuarioPessoaDoc" )]
      [  XmlElement( ElementName = "ContratanteUsuario_UsuarioPessoaDoc"   )]
      public String gxTpr_Contratanteusuario_usuariopessoadoc
      {
         get {
            return gxTv_SdtContratanteUsuario_Contratanteusuario_usuariopessoadoc ;
         }

         set {
            gxTv_SdtContratanteUsuario_Contratanteusuario_usuariopessoadoc_N = 0;
            gxTv_SdtContratanteUsuario_Contratanteusuario_usuariopessoadoc = (String)(value);
         }

      }

      public void gxTv_SdtContratanteUsuario_Contratanteusuario_usuariopessoadoc_SetNull( )
      {
         gxTv_SdtContratanteUsuario_Contratanteusuario_usuariopessoadoc_N = 1;
         gxTv_SdtContratanteUsuario_Contratanteusuario_usuariopessoadoc = "";
         return  ;
      }

      public bool gxTv_SdtContratanteUsuario_Contratanteusuario_usuariopessoadoc_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratanteUsuario_UsuarioEhContratante" )]
      [  XmlElement( ElementName = "ContratanteUsuario_UsuarioEhContratante"   )]
      public bool gxTpr_Contratanteusuario_usuarioehcontratante
      {
         get {
            return gxTv_SdtContratanteUsuario_Contratanteusuario_usuarioehcontratante ;
         }

         set {
            gxTv_SdtContratanteUsuario_Contratanteusuario_usuarioehcontratante_N = 0;
            gxTv_SdtContratanteUsuario_Contratanteusuario_usuarioehcontratante = value;
         }

      }

      public void gxTv_SdtContratanteUsuario_Contratanteusuario_usuarioehcontratante_SetNull( )
      {
         gxTv_SdtContratanteUsuario_Contratanteusuario_usuarioehcontratante_N = 1;
         gxTv_SdtContratanteUsuario_Contratanteusuario_usuarioehcontratante = false;
         return  ;
      }

      public bool gxTv_SdtContratanteUsuario_Contratanteusuario_usuarioehcontratante_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratanteUsuario_AreaTrabalhoCod" )]
      [  XmlElement( ElementName = "ContratanteUsuario_AreaTrabalhoCod"   )]
      public int gxTpr_Contratanteusuario_areatrabalhocod
      {
         get {
            return gxTv_SdtContratanteUsuario_Contratanteusuario_areatrabalhocod ;
         }

         set {
            gxTv_SdtContratanteUsuario_Contratanteusuario_areatrabalhocod_N = 0;
            gxTv_SdtContratanteUsuario_Contratanteusuario_areatrabalhocod = (int)(value);
         }

      }

      public void gxTv_SdtContratanteUsuario_Contratanteusuario_areatrabalhocod_SetNull( )
      {
         gxTv_SdtContratanteUsuario_Contratanteusuario_areatrabalhocod_N = 1;
         gxTv_SdtContratanteUsuario_Contratanteusuario_areatrabalhocod = 0;
         return  ;
      }

      public bool gxTv_SdtContratanteUsuario_Contratanteusuario_areatrabalhocod_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Usuario_UserGamGuid" )]
      [  XmlElement( ElementName = "Usuario_UserGamGuid"   )]
      public String gxTpr_Usuario_usergamguid
      {
         get {
            return gxTv_SdtContratanteUsuario_Usuario_usergamguid ;
         }

         set {
            gxTv_SdtContratanteUsuario_Usuario_usergamguid_N = 0;
            gxTv_SdtContratanteUsuario_Usuario_usergamguid = (String)(value);
         }

      }

      public void gxTv_SdtContratanteUsuario_Usuario_usergamguid_SetNull( )
      {
         gxTv_SdtContratanteUsuario_Usuario_usergamguid_N = 1;
         gxTv_SdtContratanteUsuario_Usuario_usergamguid = "";
         return  ;
      }

      public bool gxTv_SdtContratanteUsuario_Usuario_usergamguid_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratanteUsuario_EhFiscal" )]
      [  XmlElement( ElementName = "ContratanteUsuario_EhFiscal"   )]
      public bool gxTpr_Contratanteusuario_ehfiscal
      {
         get {
            return gxTv_SdtContratanteUsuario_Contratanteusuario_ehfiscal ;
         }

         set {
            gxTv_SdtContratanteUsuario_Contratanteusuario_ehfiscal_N = 0;
            gxTv_SdtContratanteUsuario_Contratanteusuario_ehfiscal = value;
         }

      }

      public void gxTv_SdtContratanteUsuario_Contratanteusuario_ehfiscal_SetNull( )
      {
         gxTv_SdtContratanteUsuario_Contratanteusuario_ehfiscal_N = 1;
         gxTv_SdtContratanteUsuario_Contratanteusuario_ehfiscal = false;
         return  ;
      }

      public bool gxTv_SdtContratanteUsuario_Contratanteusuario_ehfiscal_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Mode" )]
      [  XmlElement( ElementName = "Mode"   )]
      public String gxTpr_Mode
      {
         get {
            return gxTv_SdtContratanteUsuario_Mode ;
         }

         set {
            gxTv_SdtContratanteUsuario_Mode = (String)(value);
         }

      }

      public void gxTv_SdtContratanteUsuario_Mode_SetNull( )
      {
         gxTv_SdtContratanteUsuario_Mode = "";
         return  ;
      }

      public bool gxTv_SdtContratanteUsuario_Mode_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Initialized" )]
      [  XmlElement( ElementName = "Initialized"   )]
      public short gxTpr_Initialized
      {
         get {
            return gxTv_SdtContratanteUsuario_Initialized ;
         }

         set {
            gxTv_SdtContratanteUsuario_Initialized = (short)(value);
         }

      }

      public void gxTv_SdtContratanteUsuario_Initialized_SetNull( )
      {
         gxTv_SdtContratanteUsuario_Initialized = 0;
         return  ;
      }

      public bool gxTv_SdtContratanteUsuario_Initialized_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratanteUsuario_ContratanteCod_Z" )]
      [  XmlElement( ElementName = "ContratanteUsuario_ContratanteCod_Z"   )]
      public int gxTpr_Contratanteusuario_contratantecod_Z
      {
         get {
            return gxTv_SdtContratanteUsuario_Contratanteusuario_contratantecod_Z ;
         }

         set {
            gxTv_SdtContratanteUsuario_Contratanteusuario_contratantecod_Z = (int)(value);
         }

      }

      public void gxTv_SdtContratanteUsuario_Contratanteusuario_contratantecod_Z_SetNull( )
      {
         gxTv_SdtContratanteUsuario_Contratanteusuario_contratantecod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContratanteUsuario_Contratanteusuario_contratantecod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratanteUsuario_ContratantePesCod_Z" )]
      [  XmlElement( ElementName = "ContratanteUsuario_ContratantePesCod_Z"   )]
      public int gxTpr_Contratanteusuario_contratantepescod_Z
      {
         get {
            return gxTv_SdtContratanteUsuario_Contratanteusuario_contratantepescod_Z ;
         }

         set {
            gxTv_SdtContratanteUsuario_Contratanteusuario_contratantepescod_Z = (int)(value);
         }

      }

      public void gxTv_SdtContratanteUsuario_Contratanteusuario_contratantepescod_Z_SetNull( )
      {
         gxTv_SdtContratanteUsuario_Contratanteusuario_contratantepescod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContratanteUsuario_Contratanteusuario_contratantepescod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratanteUsuario_ContratanteFan_Z" )]
      [  XmlElement( ElementName = "ContratanteUsuario_ContratanteFan_Z"   )]
      public String gxTpr_Contratanteusuario_contratantefan_Z
      {
         get {
            return gxTv_SdtContratanteUsuario_Contratanteusuario_contratantefan_Z ;
         }

         set {
            gxTv_SdtContratanteUsuario_Contratanteusuario_contratantefan_Z = (String)(value);
         }

      }

      public void gxTv_SdtContratanteUsuario_Contratanteusuario_contratantefan_Z_SetNull( )
      {
         gxTv_SdtContratanteUsuario_Contratanteusuario_contratantefan_Z = "";
         return  ;
      }

      public bool gxTv_SdtContratanteUsuario_Contratanteusuario_contratantefan_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratanteUsuario_ContratanteRaz_Z" )]
      [  XmlElement( ElementName = "ContratanteUsuario_ContratanteRaz_Z"   )]
      public String gxTpr_Contratanteusuario_contratanteraz_Z
      {
         get {
            return gxTv_SdtContratanteUsuario_Contratanteusuario_contratanteraz_Z ;
         }

         set {
            gxTv_SdtContratanteUsuario_Contratanteusuario_contratanteraz_Z = (String)(value);
         }

      }

      public void gxTv_SdtContratanteUsuario_Contratanteusuario_contratanteraz_Z_SetNull( )
      {
         gxTv_SdtContratanteUsuario_Contratanteusuario_contratanteraz_Z = "";
         return  ;
      }

      public bool gxTv_SdtContratanteUsuario_Contratanteusuario_contratanteraz_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratanteUsuario_UsuarioCod_Z" )]
      [  XmlElement( ElementName = "ContratanteUsuario_UsuarioCod_Z"   )]
      public int gxTpr_Contratanteusuario_usuariocod_Z
      {
         get {
            return gxTv_SdtContratanteUsuario_Contratanteusuario_usuariocod_Z ;
         }

         set {
            gxTv_SdtContratanteUsuario_Contratanteusuario_usuariocod_Z = (int)(value);
         }

      }

      public void gxTv_SdtContratanteUsuario_Contratanteusuario_usuariocod_Z_SetNull( )
      {
         gxTv_SdtContratanteUsuario_Contratanteusuario_usuariocod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContratanteUsuario_Contratanteusuario_usuariocod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Usuario_Nome_Z" )]
      [  XmlElement( ElementName = "Usuario_Nome_Z"   )]
      public String gxTpr_Usuario_nome_Z
      {
         get {
            return gxTv_SdtContratanteUsuario_Usuario_nome_Z ;
         }

         set {
            gxTv_SdtContratanteUsuario_Usuario_nome_Z = (String)(value);
         }

      }

      public void gxTv_SdtContratanteUsuario_Usuario_nome_Z_SetNull( )
      {
         gxTv_SdtContratanteUsuario_Usuario_nome_Z = "";
         return  ;
      }

      public bool gxTv_SdtContratanteUsuario_Usuario_nome_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratanteUsuario_UsuarioPessoaCod_Z" )]
      [  XmlElement( ElementName = "ContratanteUsuario_UsuarioPessoaCod_Z"   )]
      public int gxTpr_Contratanteusuario_usuariopessoacod_Z
      {
         get {
            return gxTv_SdtContratanteUsuario_Contratanteusuario_usuariopessoacod_Z ;
         }

         set {
            gxTv_SdtContratanteUsuario_Contratanteusuario_usuariopessoacod_Z = (int)(value);
         }

      }

      public void gxTv_SdtContratanteUsuario_Contratanteusuario_usuariopessoacod_Z_SetNull( )
      {
         gxTv_SdtContratanteUsuario_Contratanteusuario_usuariopessoacod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContratanteUsuario_Contratanteusuario_usuariopessoacod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratanteUsuario_UsuarioPessoaNom_Z" )]
      [  XmlElement( ElementName = "ContratanteUsuario_UsuarioPessoaNom_Z"   )]
      public String gxTpr_Contratanteusuario_usuariopessoanom_Z
      {
         get {
            return gxTv_SdtContratanteUsuario_Contratanteusuario_usuariopessoanom_Z ;
         }

         set {
            gxTv_SdtContratanteUsuario_Contratanteusuario_usuariopessoanom_Z = (String)(value);
         }

      }

      public void gxTv_SdtContratanteUsuario_Contratanteusuario_usuariopessoanom_Z_SetNull( )
      {
         gxTv_SdtContratanteUsuario_Contratanteusuario_usuariopessoanom_Z = "";
         return  ;
      }

      public bool gxTv_SdtContratanteUsuario_Contratanteusuario_usuariopessoanom_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratanteUsuario_UsuarioPessoaDoc_Z" )]
      [  XmlElement( ElementName = "ContratanteUsuario_UsuarioPessoaDoc_Z"   )]
      public String gxTpr_Contratanteusuario_usuariopessoadoc_Z
      {
         get {
            return gxTv_SdtContratanteUsuario_Contratanteusuario_usuariopessoadoc_Z ;
         }

         set {
            gxTv_SdtContratanteUsuario_Contratanteusuario_usuariopessoadoc_Z = (String)(value);
         }

      }

      public void gxTv_SdtContratanteUsuario_Contratanteusuario_usuariopessoadoc_Z_SetNull( )
      {
         gxTv_SdtContratanteUsuario_Contratanteusuario_usuariopessoadoc_Z = "";
         return  ;
      }

      public bool gxTv_SdtContratanteUsuario_Contratanteusuario_usuariopessoadoc_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratanteUsuario_UsuarioEhContratante_Z" )]
      [  XmlElement( ElementName = "ContratanteUsuario_UsuarioEhContratante_Z"   )]
      public bool gxTpr_Contratanteusuario_usuarioehcontratante_Z
      {
         get {
            return gxTv_SdtContratanteUsuario_Contratanteusuario_usuarioehcontratante_Z ;
         }

         set {
            gxTv_SdtContratanteUsuario_Contratanteusuario_usuarioehcontratante_Z = value;
         }

      }

      public void gxTv_SdtContratanteUsuario_Contratanteusuario_usuarioehcontratante_Z_SetNull( )
      {
         gxTv_SdtContratanteUsuario_Contratanteusuario_usuarioehcontratante_Z = false;
         return  ;
      }

      public bool gxTv_SdtContratanteUsuario_Contratanteusuario_usuarioehcontratante_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratanteUsuario_AreaTrabalhoCod_Z" )]
      [  XmlElement( ElementName = "ContratanteUsuario_AreaTrabalhoCod_Z"   )]
      public int gxTpr_Contratanteusuario_areatrabalhocod_Z
      {
         get {
            return gxTv_SdtContratanteUsuario_Contratanteusuario_areatrabalhocod_Z ;
         }

         set {
            gxTv_SdtContratanteUsuario_Contratanteusuario_areatrabalhocod_Z = (int)(value);
         }

      }

      public void gxTv_SdtContratanteUsuario_Contratanteusuario_areatrabalhocod_Z_SetNull( )
      {
         gxTv_SdtContratanteUsuario_Contratanteusuario_areatrabalhocod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContratanteUsuario_Contratanteusuario_areatrabalhocod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Usuario_UserGamGuid_Z" )]
      [  XmlElement( ElementName = "Usuario_UserGamGuid_Z"   )]
      public String gxTpr_Usuario_usergamguid_Z
      {
         get {
            return gxTv_SdtContratanteUsuario_Usuario_usergamguid_Z ;
         }

         set {
            gxTv_SdtContratanteUsuario_Usuario_usergamguid_Z = (String)(value);
         }

      }

      public void gxTv_SdtContratanteUsuario_Usuario_usergamguid_Z_SetNull( )
      {
         gxTv_SdtContratanteUsuario_Usuario_usergamguid_Z = "";
         return  ;
      }

      public bool gxTv_SdtContratanteUsuario_Usuario_usergamguid_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratanteUsuario_EhFiscal_Z" )]
      [  XmlElement( ElementName = "ContratanteUsuario_EhFiscal_Z"   )]
      public bool gxTpr_Contratanteusuario_ehfiscal_Z
      {
         get {
            return gxTv_SdtContratanteUsuario_Contratanteusuario_ehfiscal_Z ;
         }

         set {
            gxTv_SdtContratanteUsuario_Contratanteusuario_ehfiscal_Z = value;
         }

      }

      public void gxTv_SdtContratanteUsuario_Contratanteusuario_ehfiscal_Z_SetNull( )
      {
         gxTv_SdtContratanteUsuario_Contratanteusuario_ehfiscal_Z = false;
         return  ;
      }

      public bool gxTv_SdtContratanteUsuario_Contratanteusuario_ehfiscal_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratanteUsuario_ContratantePesCod_N" )]
      [  XmlElement( ElementName = "ContratanteUsuario_ContratantePesCod_N"   )]
      public short gxTpr_Contratanteusuario_contratantepescod_N
      {
         get {
            return gxTv_SdtContratanteUsuario_Contratanteusuario_contratantepescod_N ;
         }

         set {
            gxTv_SdtContratanteUsuario_Contratanteusuario_contratantepescod_N = (short)(value);
         }

      }

      public void gxTv_SdtContratanteUsuario_Contratanteusuario_contratantepescod_N_SetNull( )
      {
         gxTv_SdtContratanteUsuario_Contratanteusuario_contratantepescod_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratanteUsuario_Contratanteusuario_contratantepescod_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratanteUsuario_ContratanteFan_N" )]
      [  XmlElement( ElementName = "ContratanteUsuario_ContratanteFan_N"   )]
      public short gxTpr_Contratanteusuario_contratantefan_N
      {
         get {
            return gxTv_SdtContratanteUsuario_Contratanteusuario_contratantefan_N ;
         }

         set {
            gxTv_SdtContratanteUsuario_Contratanteusuario_contratantefan_N = (short)(value);
         }

      }

      public void gxTv_SdtContratanteUsuario_Contratanteusuario_contratantefan_N_SetNull( )
      {
         gxTv_SdtContratanteUsuario_Contratanteusuario_contratantefan_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratanteUsuario_Contratanteusuario_contratantefan_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratanteUsuario_ContratanteRaz_N" )]
      [  XmlElement( ElementName = "ContratanteUsuario_ContratanteRaz_N"   )]
      public short gxTpr_Contratanteusuario_contratanteraz_N
      {
         get {
            return gxTv_SdtContratanteUsuario_Contratanteusuario_contratanteraz_N ;
         }

         set {
            gxTv_SdtContratanteUsuario_Contratanteusuario_contratanteraz_N = (short)(value);
         }

      }

      public void gxTv_SdtContratanteUsuario_Contratanteusuario_contratanteraz_N_SetNull( )
      {
         gxTv_SdtContratanteUsuario_Contratanteusuario_contratanteraz_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratanteUsuario_Contratanteusuario_contratanteraz_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Usuario_Nome_N" )]
      [  XmlElement( ElementName = "Usuario_Nome_N"   )]
      public short gxTpr_Usuario_nome_N
      {
         get {
            return gxTv_SdtContratanteUsuario_Usuario_nome_N ;
         }

         set {
            gxTv_SdtContratanteUsuario_Usuario_nome_N = (short)(value);
         }

      }

      public void gxTv_SdtContratanteUsuario_Usuario_nome_N_SetNull( )
      {
         gxTv_SdtContratanteUsuario_Usuario_nome_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratanteUsuario_Usuario_nome_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratanteUsuario_UsuarioPessoaCod_N" )]
      [  XmlElement( ElementName = "ContratanteUsuario_UsuarioPessoaCod_N"   )]
      public short gxTpr_Contratanteusuario_usuariopessoacod_N
      {
         get {
            return gxTv_SdtContratanteUsuario_Contratanteusuario_usuariopessoacod_N ;
         }

         set {
            gxTv_SdtContratanteUsuario_Contratanteusuario_usuariopessoacod_N = (short)(value);
         }

      }

      public void gxTv_SdtContratanteUsuario_Contratanteusuario_usuariopessoacod_N_SetNull( )
      {
         gxTv_SdtContratanteUsuario_Contratanteusuario_usuariopessoacod_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratanteUsuario_Contratanteusuario_usuariopessoacod_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratanteUsuario_UsuarioPessoaNom_N" )]
      [  XmlElement( ElementName = "ContratanteUsuario_UsuarioPessoaNom_N"   )]
      public short gxTpr_Contratanteusuario_usuariopessoanom_N
      {
         get {
            return gxTv_SdtContratanteUsuario_Contratanteusuario_usuariopessoanom_N ;
         }

         set {
            gxTv_SdtContratanteUsuario_Contratanteusuario_usuariopessoanom_N = (short)(value);
         }

      }

      public void gxTv_SdtContratanteUsuario_Contratanteusuario_usuariopessoanom_N_SetNull( )
      {
         gxTv_SdtContratanteUsuario_Contratanteusuario_usuariopessoanom_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratanteUsuario_Contratanteusuario_usuariopessoanom_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratanteUsuario_UsuarioPessoaDoc_N" )]
      [  XmlElement( ElementName = "ContratanteUsuario_UsuarioPessoaDoc_N"   )]
      public short gxTpr_Contratanteusuario_usuariopessoadoc_N
      {
         get {
            return gxTv_SdtContratanteUsuario_Contratanteusuario_usuariopessoadoc_N ;
         }

         set {
            gxTv_SdtContratanteUsuario_Contratanteusuario_usuariopessoadoc_N = (short)(value);
         }

      }

      public void gxTv_SdtContratanteUsuario_Contratanteusuario_usuariopessoadoc_N_SetNull( )
      {
         gxTv_SdtContratanteUsuario_Contratanteusuario_usuariopessoadoc_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratanteUsuario_Contratanteusuario_usuariopessoadoc_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratanteUsuario_UsuarioEhContratante_N" )]
      [  XmlElement( ElementName = "ContratanteUsuario_UsuarioEhContratante_N"   )]
      public short gxTpr_Contratanteusuario_usuarioehcontratante_N
      {
         get {
            return gxTv_SdtContratanteUsuario_Contratanteusuario_usuarioehcontratante_N ;
         }

         set {
            gxTv_SdtContratanteUsuario_Contratanteusuario_usuarioehcontratante_N = (short)(value);
         }

      }

      public void gxTv_SdtContratanteUsuario_Contratanteusuario_usuarioehcontratante_N_SetNull( )
      {
         gxTv_SdtContratanteUsuario_Contratanteusuario_usuarioehcontratante_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratanteUsuario_Contratanteusuario_usuarioehcontratante_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratanteUsuario_AreaTrabalhoCod_N" )]
      [  XmlElement( ElementName = "ContratanteUsuario_AreaTrabalhoCod_N"   )]
      public short gxTpr_Contratanteusuario_areatrabalhocod_N
      {
         get {
            return gxTv_SdtContratanteUsuario_Contratanteusuario_areatrabalhocod_N ;
         }

         set {
            gxTv_SdtContratanteUsuario_Contratanteusuario_areatrabalhocod_N = (short)(value);
         }

      }

      public void gxTv_SdtContratanteUsuario_Contratanteusuario_areatrabalhocod_N_SetNull( )
      {
         gxTv_SdtContratanteUsuario_Contratanteusuario_areatrabalhocod_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratanteUsuario_Contratanteusuario_areatrabalhocod_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Usuario_UserGamGuid_N" )]
      [  XmlElement( ElementName = "Usuario_UserGamGuid_N"   )]
      public short gxTpr_Usuario_usergamguid_N
      {
         get {
            return gxTv_SdtContratanteUsuario_Usuario_usergamguid_N ;
         }

         set {
            gxTv_SdtContratanteUsuario_Usuario_usergamguid_N = (short)(value);
         }

      }

      public void gxTv_SdtContratanteUsuario_Usuario_usergamguid_N_SetNull( )
      {
         gxTv_SdtContratanteUsuario_Usuario_usergamguid_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratanteUsuario_Usuario_usergamguid_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContratanteUsuario_EhFiscal_N" )]
      [  XmlElement( ElementName = "ContratanteUsuario_EhFiscal_N"   )]
      public short gxTpr_Contratanteusuario_ehfiscal_N
      {
         get {
            return gxTv_SdtContratanteUsuario_Contratanteusuario_ehfiscal_N ;
         }

         set {
            gxTv_SdtContratanteUsuario_Contratanteusuario_ehfiscal_N = (short)(value);
         }

      }

      public void gxTv_SdtContratanteUsuario_Contratanteusuario_ehfiscal_N_SetNull( )
      {
         gxTv_SdtContratanteUsuario_Contratanteusuario_ehfiscal_N = 0;
         return  ;
      }

      public bool gxTv_SdtContratanteUsuario_Contratanteusuario_ehfiscal_N_IsNull( )
      {
         return false ;
      }

      public void initialize( )
      {
         gxTv_SdtContratanteUsuario_Contratanteusuario_contratantefan = "";
         gxTv_SdtContratanteUsuario_Contratanteusuario_contratanteraz = "";
         gxTv_SdtContratanteUsuario_Usuario_nome = "";
         gxTv_SdtContratanteUsuario_Contratanteusuario_usuariopessoanom = "";
         gxTv_SdtContratanteUsuario_Contratanteusuario_usuariopessoadoc = "";
         gxTv_SdtContratanteUsuario_Usuario_usergamguid = "";
         gxTv_SdtContratanteUsuario_Mode = "";
         gxTv_SdtContratanteUsuario_Contratanteusuario_contratantefan_Z = "";
         gxTv_SdtContratanteUsuario_Contratanteusuario_contratanteraz_Z = "";
         gxTv_SdtContratanteUsuario_Usuario_nome_Z = "";
         gxTv_SdtContratanteUsuario_Contratanteusuario_usuariopessoanom_Z = "";
         gxTv_SdtContratanteUsuario_Contratanteusuario_usuariopessoadoc_Z = "";
         gxTv_SdtContratanteUsuario_Usuario_usergamguid_Z = "";
         sTagName = "";
         IGxSilentTrn obj ;
         obj = (IGxSilentTrn)ClassLoader.FindInstance( "contratanteusuario", "GeneXus.Programs.contratanteusuario_bc", new Object[] {context}, constructorCallingAssembly);
         obj.initialize();
         obj.SetSDT(this, 1);
         setTransaction( obj) ;
         obj.SetMode("INS");
         return  ;
      }

      private short gxTv_SdtContratanteUsuario_Initialized ;
      private short gxTv_SdtContratanteUsuario_Contratanteusuario_contratantepescod_N ;
      private short gxTv_SdtContratanteUsuario_Contratanteusuario_contratantefan_N ;
      private short gxTv_SdtContratanteUsuario_Contratanteusuario_contratanteraz_N ;
      private short gxTv_SdtContratanteUsuario_Usuario_nome_N ;
      private short gxTv_SdtContratanteUsuario_Contratanteusuario_usuariopessoacod_N ;
      private short gxTv_SdtContratanteUsuario_Contratanteusuario_usuariopessoanom_N ;
      private short gxTv_SdtContratanteUsuario_Contratanteusuario_usuariopessoadoc_N ;
      private short gxTv_SdtContratanteUsuario_Contratanteusuario_usuarioehcontratante_N ;
      private short gxTv_SdtContratanteUsuario_Contratanteusuario_areatrabalhocod_N ;
      private short gxTv_SdtContratanteUsuario_Usuario_usergamguid_N ;
      private short gxTv_SdtContratanteUsuario_Contratanteusuario_ehfiscal_N ;
      private short readOk ;
      private short nOutParmCount ;
      private int gxTv_SdtContratanteUsuario_Contratanteusuario_contratantecod ;
      private int gxTv_SdtContratanteUsuario_Contratanteusuario_contratantepescod ;
      private int gxTv_SdtContratanteUsuario_Contratanteusuario_usuariocod ;
      private int gxTv_SdtContratanteUsuario_Contratanteusuario_usuariopessoacod ;
      private int gxTv_SdtContratanteUsuario_Contratanteusuario_areatrabalhocod ;
      private int gxTv_SdtContratanteUsuario_Contratanteusuario_contratantecod_Z ;
      private int gxTv_SdtContratanteUsuario_Contratanteusuario_contratantepescod_Z ;
      private int gxTv_SdtContratanteUsuario_Contratanteusuario_usuariocod_Z ;
      private int gxTv_SdtContratanteUsuario_Contratanteusuario_usuariopessoacod_Z ;
      private int gxTv_SdtContratanteUsuario_Contratanteusuario_areatrabalhocod_Z ;
      private String gxTv_SdtContratanteUsuario_Contratanteusuario_contratantefan ;
      private String gxTv_SdtContratanteUsuario_Contratanteusuario_contratanteraz ;
      private String gxTv_SdtContratanteUsuario_Usuario_nome ;
      private String gxTv_SdtContratanteUsuario_Contratanteusuario_usuariopessoanom ;
      private String gxTv_SdtContratanteUsuario_Usuario_usergamguid ;
      private String gxTv_SdtContratanteUsuario_Mode ;
      private String gxTv_SdtContratanteUsuario_Contratanteusuario_contratantefan_Z ;
      private String gxTv_SdtContratanteUsuario_Contratanteusuario_contratanteraz_Z ;
      private String gxTv_SdtContratanteUsuario_Usuario_nome_Z ;
      private String gxTv_SdtContratanteUsuario_Contratanteusuario_usuariopessoanom_Z ;
      private String gxTv_SdtContratanteUsuario_Usuario_usergamguid_Z ;
      private String sTagName ;
      private bool gxTv_SdtContratanteUsuario_Contratanteusuario_usuarioehcontratante ;
      private bool gxTv_SdtContratanteUsuario_Contratanteusuario_ehfiscal ;
      private bool gxTv_SdtContratanteUsuario_Contratanteusuario_usuarioehcontratante_Z ;
      private bool gxTv_SdtContratanteUsuario_Contratanteusuario_ehfiscal_Z ;
      private String gxTv_SdtContratanteUsuario_Contratanteusuario_usuariopessoadoc ;
      private String gxTv_SdtContratanteUsuario_Contratanteusuario_usuariopessoadoc_Z ;
      private Assembly constructorCallingAssembly ;
   }

   [DataContract(Name = @"ContratanteUsuario", Namespace = "GxEv3Up14_MeetrikaVs3")]
   public class SdtContratanteUsuario_RESTInterface : GxGenericCollectionItem<SdtContratanteUsuario>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtContratanteUsuario_RESTInterface( ) : base()
      {
      }

      public SdtContratanteUsuario_RESTInterface( SdtContratanteUsuario psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "ContratanteUsuario_ContratanteCod" , Order = 0 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contratanteusuario_contratantecod
      {
         get {
            return sdt.gxTpr_Contratanteusuario_contratantecod ;
         }

         set {
            sdt.gxTpr_Contratanteusuario_contratantecod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContratanteUsuario_ContratantePesCod" , Order = 1 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contratanteusuario_contratantepescod
      {
         get {
            return sdt.gxTpr_Contratanteusuario_contratantepescod ;
         }

         set {
            sdt.gxTpr_Contratanteusuario_contratantepescod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContratanteUsuario_ContratanteFan" , Order = 2 )]
      [GxSeudo()]
      public String gxTpr_Contratanteusuario_contratantefan
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contratanteusuario_contratantefan) ;
         }

         set {
            sdt.gxTpr_Contratanteusuario_contratantefan = (String)(value);
         }

      }

      [DataMember( Name = "ContratanteUsuario_ContratanteRaz" , Order = 3 )]
      [GxSeudo()]
      public String gxTpr_Contratanteusuario_contratanteraz
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contratanteusuario_contratanteraz) ;
         }

         set {
            sdt.gxTpr_Contratanteusuario_contratanteraz = (String)(value);
         }

      }

      [DataMember( Name = "ContratanteUsuario_UsuarioCod" , Order = 4 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contratanteusuario_usuariocod
      {
         get {
            return sdt.gxTpr_Contratanteusuario_usuariocod ;
         }

         set {
            sdt.gxTpr_Contratanteusuario_usuariocod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Usuario_Nome" , Order = 5 )]
      [GxSeudo()]
      public String gxTpr_Usuario_nome
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Usuario_nome) ;
         }

         set {
            sdt.gxTpr_Usuario_nome = (String)(value);
         }

      }

      [DataMember( Name = "ContratanteUsuario_UsuarioPessoaCod" , Order = 6 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contratanteusuario_usuariopessoacod
      {
         get {
            return sdt.gxTpr_Contratanteusuario_usuariopessoacod ;
         }

         set {
            sdt.gxTpr_Contratanteusuario_usuariopessoacod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContratanteUsuario_UsuarioPessoaNom" , Order = 7 )]
      [GxSeudo()]
      public String gxTpr_Contratanteusuario_usuariopessoanom
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contratanteusuario_usuariopessoanom) ;
         }

         set {
            sdt.gxTpr_Contratanteusuario_usuariopessoanom = (String)(value);
         }

      }

      [DataMember( Name = "ContratanteUsuario_UsuarioPessoaDoc" , Order = 8 )]
      [GxSeudo()]
      public String gxTpr_Contratanteusuario_usuariopessoadoc
      {
         get {
            return sdt.gxTpr_Contratanteusuario_usuariopessoadoc ;
         }

         set {
            sdt.gxTpr_Contratanteusuario_usuariopessoadoc = (String)(value);
         }

      }

      [DataMember( Name = "ContratanteUsuario_UsuarioEhContratante" , Order = 9 )]
      [GxSeudo()]
      public bool gxTpr_Contratanteusuario_usuarioehcontratante
      {
         get {
            return sdt.gxTpr_Contratanteusuario_usuarioehcontratante ;
         }

         set {
            sdt.gxTpr_Contratanteusuario_usuarioehcontratante = value;
         }

      }

      [DataMember( Name = "ContratanteUsuario_AreaTrabalhoCod" , Order = 10 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contratanteusuario_areatrabalhocod
      {
         get {
            return sdt.gxTpr_Contratanteusuario_areatrabalhocod ;
         }

         set {
            sdt.gxTpr_Contratanteusuario_areatrabalhocod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Usuario_UserGamGuid" , Order = 11 )]
      [GxSeudo()]
      public String gxTpr_Usuario_usergamguid
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Usuario_usergamguid) ;
         }

         set {
            sdt.gxTpr_Usuario_usergamguid = (String)(value);
         }

      }

      [DataMember( Name = "ContratanteUsuario_EhFiscal" , Order = 12 )]
      [GxSeudo()]
      public bool gxTpr_Contratanteusuario_ehfiscal
      {
         get {
            return sdt.gxTpr_Contratanteusuario_ehfiscal ;
         }

         set {
            sdt.gxTpr_Contratanteusuario_ehfiscal = value;
         }

      }

      public SdtContratanteUsuario sdt
      {
         get {
            return (SdtContratanteUsuario)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtContratanteUsuario() ;
         }
      }

      [DataMember( Name = "gx_md5_hash", Order = 39 )]
      public string Hash
      {
         get {
            if ( StringUtil.StrCmp(md5Hash, null) == 0 )
            {
               md5Hash = (String)(getHash());
            }
            return md5Hash ;
         }

         set {
            md5Hash = value ;
         }

      }

      private String md5Hash ;
   }

}
