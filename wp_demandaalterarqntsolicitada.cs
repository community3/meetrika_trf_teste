/*
               File: WP_DemandaAlterarQntSolicitada
        Description: Alterar quantidade Solicitada
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/16/2020 20:49:43.93
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_demandaalterarqntsolicitada : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wp_demandaalterarqntsolicitada( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_demandaalterarqntsolicitada( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ContagemResultado_Codigo )
      {
         this.A456ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               A456ContagemResultado_Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_CONTAGEMRESULTADO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A456ContagemResultado_Codigo), "ZZZZZ9")));
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PATR2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTTR2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202061620494396");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/ConfirmPanel/BootstrapConfirmPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wp_demandaalterarqntsolicitada.aspx") + "?" + UrlEncode("" +A456ContagemResultado_Codigo)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A456ContagemResultado_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_DEMANDA", A457ContagemResultado_Demanda);
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_DEMANDAFM", A493ContagemResultado_DemandaFM);
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADO_OSFSOSFM", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A501ContagemResultado_OsFsOsFm, ""))));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADO_DESCRICAO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A494ContagemResultado_Descricao, ""))));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A456ContagemResultado_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTAGEMRESULTADO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A456ContagemResultado_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Width", StringUtil.RTrim( Dvpanel_tableattributes_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Cls", StringUtil.RTrim( Dvpanel_tableattributes_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Title", StringUtil.RTrim( Dvpanel_tableattributes_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsible", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsed", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autowidth", StringUtil.BoolToStr( Dvpanel_tableattributes_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoheight", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tableattributes_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Iconposition", StringUtil.RTrim( Dvpanel_tableattributes_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoscroll", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoscroll));
         GxWebStd.gx_hidden_field( context, "DVELOP_CONFIRMPANEL_Width", StringUtil.RTrim( Dvelop_confirmpanel_Width));
         GxWebStd.gx_hidden_field( context, "DVELOP_CONFIRMPANEL_Height", StringUtil.RTrim( Dvelop_confirmpanel_Height));
         GxWebStd.gx_hidden_field( context, "DVELOP_CONFIRMPANEL_Title", StringUtil.RTrim( Dvelop_confirmpanel_Title));
         GxWebStd.gx_hidden_field( context, "DVELOP_CONFIRMPANEL_Confirmationtext", StringUtil.RTrim( Dvelop_confirmpanel_Confirmationtext));
         GxWebStd.gx_hidden_field( context, "DVELOP_CONFIRMPANEL_Yesbuttoncaption", StringUtil.RTrim( Dvelop_confirmpanel_Yesbuttoncaption));
         GxWebStd.gx_hidden_field( context, "DVELOP_CONFIRMPANEL_Cancelbuttoncaption", StringUtil.RTrim( Dvelop_confirmpanel_Cancelbuttoncaption));
         GxWebStd.gx_hidden_field( context, "DVELOP_CONFIRMPANEL_Confirmtype", StringUtil.RTrim( Dvelop_confirmpanel_Confirmtype));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WETR2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTTR2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wp_demandaalterarqntsolicitada.aspx") + "?" + UrlEncode("" +A456ContagemResultado_Codigo) ;
      }

      public override String GetPgmname( )
      {
         return "WP_DemandaAlterarQntSolicitada" ;
      }

      public override String GetPgmdesc( )
      {
         return "Alterar quantidade Solicitada" ;
      }

      protected void WBTR0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_TR2( true) ;
         }
         else
         {
            wb_table1_2_TR2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_TR2e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void STARTTR2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Alterar quantidade Solicitada", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPTR0( ) ;
      }

      protected void WSTR2( )
      {
         STARTTR2( ) ;
         EVTTR2( ) ;
      }

      protected void EVTTR2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DVELOP_CONFIRMPANEL.CLOSE") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11TR2 */
                              E11TR2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12TR2 */
                              E12TR2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ! wbErr )
                              {
                                 Rfr0gs = false;
                                 if ( ! Rfr0gs )
                                 {
                                    /* Execute user event: E13TR2 */
                                    E13TR2 ();
                                 }
                                 dynload_actions( ) ;
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E14TR2 */
                              E14TR2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E15TR2 */
                              E15TR2 ();
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WETR2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PATR2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavContagemresultado_quantidadesolicitada_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFTR2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      protected void RFTR2( )
      {
         initialize_formulas( ) ;
         /* Execute user event: E14TR2 */
         E14TR2 ();
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Using cursor H00TR2 */
            pr_default.execute(0, new Object[] {A456ContagemResultado_Codigo});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A1553ContagemResultado_CntSrvCod = H00TR2_A1553ContagemResultado_CntSrvCod[0];
               n1553ContagemResultado_CntSrvCod = H00TR2_n1553ContagemResultado_CntSrvCod[0];
               A601ContagemResultado_Servico = H00TR2_A601ContagemResultado_Servico[0];
               n601ContagemResultado_Servico = H00TR2_n601ContagemResultado_Servico[0];
               A2133ContagemResultado_QuantidadeSolicitada = H00TR2_A2133ContagemResultado_QuantidadeSolicitada[0];
               n2133ContagemResultado_QuantidadeSolicitada = H00TR2_n2133ContagemResultado_QuantidadeSolicitada[0];
               A801ContagemResultado_ServicoSigla = H00TR2_A801ContagemResultado_ServicoSigla[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A801ContagemResultado_ServicoSigla", A801ContagemResultado_ServicoSigla);
               n801ContagemResultado_ServicoSigla = H00TR2_n801ContagemResultado_ServicoSigla[0];
               A494ContagemResultado_Descricao = H00TR2_A494ContagemResultado_Descricao[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A494ContagemResultado_Descricao", A494ContagemResultado_Descricao);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_CONTAGEMRESULTADO_DESCRICAO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A494ContagemResultado_Descricao, ""))));
               n494ContagemResultado_Descricao = H00TR2_n494ContagemResultado_Descricao[0];
               A493ContagemResultado_DemandaFM = H00TR2_A493ContagemResultado_DemandaFM[0];
               n493ContagemResultado_DemandaFM = H00TR2_n493ContagemResultado_DemandaFM[0];
               A457ContagemResultado_Demanda = H00TR2_A457ContagemResultado_Demanda[0];
               n457ContagemResultado_Demanda = H00TR2_n457ContagemResultado_Demanda[0];
               A601ContagemResultado_Servico = H00TR2_A601ContagemResultado_Servico[0];
               n601ContagemResultado_Servico = H00TR2_n601ContagemResultado_Servico[0];
               A801ContagemResultado_ServicoSigla = H00TR2_A801ContagemResultado_ServicoSigla[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A801ContagemResultado_ServicoSigla", A801ContagemResultado_ServicoSigla);
               n801ContagemResultado_ServicoSigla = H00TR2_n801ContagemResultado_ServicoSigla[0];
               A501ContagemResultado_OsFsOsFm = StringUtil.Trim( A457ContagemResultado_Demanda) + (String.IsNullOrEmpty(StringUtil.RTrim( A493ContagemResultado_DemandaFM)) ? "" : "|"+StringUtil.Trim( A493ContagemResultado_DemandaFM));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A501ContagemResultado_OsFsOsFm", A501ContagemResultado_OsFsOsFm);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_CONTAGEMRESULTADO_OSFSOSFM", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A501ContagemResultado_OsFsOsFm, ""))));
               /* Execute user event: E15TR2 */
               E15TR2 ();
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(0);
            WBTR0( ) ;
         }
      }

      protected void STRUPTR0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E12TR2 */
         E12TR2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            A501ContagemResultado_OsFsOsFm = cgiGet( edtContagemResultado_OsFsOsFm_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A501ContagemResultado_OsFsOsFm", A501ContagemResultado_OsFsOsFm);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_CONTAGEMRESULTADO_OSFSOSFM", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A501ContagemResultado_OsFsOsFm, ""))));
            A494ContagemResultado_Descricao = cgiGet( edtContagemResultado_Descricao_Internalname);
            n494ContagemResultado_Descricao = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A494ContagemResultado_Descricao", A494ContagemResultado_Descricao);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_CONTAGEMRESULTADO_DESCRICAO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A494ContagemResultado_Descricao, ""))));
            A801ContagemResultado_ServicoSigla = StringUtil.Upper( cgiGet( edtContagemResultado_ServicoSigla_Internalname));
            n801ContagemResultado_ServicoSigla = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A801ContagemResultado_ServicoSigla", A801ContagemResultado_ServicoSigla);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavContagemresultado_quantidadesolicitada_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavContagemresultado_quantidadesolicitada_Internalname), ",", ".") > 9999.9999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTAGEMRESULTADO_QUANTIDADESOLICITADA");
               GX_FocusControl = edtavContagemresultado_quantidadesolicitada_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV46ContagemResultado_QuantidadeSolicitada = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46ContagemResultado_QuantidadeSolicitada", StringUtil.LTrim( StringUtil.Str( AV46ContagemResultado_QuantidadeSolicitada, 9, 4)));
            }
            else
            {
               AV46ContagemResultado_QuantidadeSolicitada = context.localUtil.CToN( cgiGet( edtavContagemresultado_quantidadesolicitada_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46ContagemResultado_QuantidadeSolicitada", StringUtil.LTrim( StringUtil.Str( AV46ContagemResultado_QuantidadeSolicitada, 9, 4)));
            }
            /* Read saved values. */
            Dvpanel_tableattributes_Width = cgiGet( "DVPANEL_TABLEATTRIBUTES_Width");
            Dvpanel_tableattributes_Cls = cgiGet( "DVPANEL_TABLEATTRIBUTES_Cls");
            Dvpanel_tableattributes_Title = cgiGet( "DVPANEL_TABLEATTRIBUTES_Title");
            Dvpanel_tableattributes_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsible"));
            Dvpanel_tableattributes_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsed"));
            Dvpanel_tableattributes_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autowidth"));
            Dvpanel_tableattributes_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoheight"));
            Dvpanel_tableattributes_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showcollapseicon"));
            Dvpanel_tableattributes_Iconposition = cgiGet( "DVPANEL_TABLEATTRIBUTES_Iconposition");
            Dvpanel_tableattributes_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoscroll"));
            Dvelop_confirmpanel_Width = cgiGet( "DVELOP_CONFIRMPANEL_Width");
            Dvelop_confirmpanel_Height = cgiGet( "DVELOP_CONFIRMPANEL_Height");
            Dvelop_confirmpanel_Title = cgiGet( "DVELOP_CONFIRMPANEL_Title");
            Dvelop_confirmpanel_Confirmationtext = cgiGet( "DVELOP_CONFIRMPANEL_Confirmationtext");
            Dvelop_confirmpanel_Yesbuttoncaption = cgiGet( "DVELOP_CONFIRMPANEL_Yesbuttoncaption");
            Dvelop_confirmpanel_Cancelbuttoncaption = cgiGet( "DVELOP_CONFIRMPANEL_Cancelbuttoncaption");
            Dvelop_confirmpanel_Confirmtype = cgiGet( "DVELOP_CONFIRMPANEL_Confirmtype");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E12TR2 */
         E12TR2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E12TR2( )
      {
         /* Start Routine */
         Form.Meta.addItem("Versao", "1.0 - Data: 02/06/2020 22:00", 0) ;
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV20Context) ;
      }

      public void GXEnter( )
      {
         /* Execute user event: E13TR2 */
         E13TR2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E13TR2( )
      {
         /* Enter Routine */
         new prc_demandaalterarqntsolicitada(context ).execute(  A456ContagemResultado_Codigo,  AV46ContagemResultado_QuantidadeSolicitada) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_CONTAGEMRESULTADO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A456ContagemResultado_Codigo), "ZZZZZ9")));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46ContagemResultado_QuantidadeSolicitada", StringUtil.LTrim( StringUtil.Str( AV46ContagemResultado_QuantidadeSolicitada, 9, 4)));
         context.CommitDataStores( "WP_DemandaAlterarQntSolicitada");
         /* Execute user subroutine: 'EXIBE.MENSAGEM' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E14TR2( )
      {
         /* Refresh Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV20Context) ;
      }

      protected void E11TR2( )
      {
         /* Dvelop_confirmpanel_Close Routine */
         context.setWebReturnParms(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void nextLoad( )
      {
      }

      protected void E15TR2( )
      {
         /* Load Routine */
         AV46ContagemResultado_QuantidadeSolicitada = A2133ContagemResultado_QuantidadeSolicitada;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46ContagemResultado_QuantidadeSolicitada", StringUtil.LTrim( StringUtil.Str( AV46ContagemResultado_QuantidadeSolicitada, 9, 4)));
      }

      protected void S112( )
      {
         /* 'EXIBE.MENSAGEM' Routine */
         Dvelop_confirmpanel_Title = "Aten��o!";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Dvelop_confirmpanel_Internalname, "Title", Dvelop_confirmpanel_Title);
         Dvelop_confirmpanel_Confirmationtext = "Quantidade Solicitada atualizada!";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Dvelop_confirmpanel_Internalname, "ConfirmationText", Dvelop_confirmpanel_Confirmationtext);
         Dvelop_confirmpanel_Yesbuttoncaption = "Confirmar";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Dvelop_confirmpanel_Internalname, "YesButtonCaption", Dvelop_confirmpanel_Yesbuttoncaption);
         Dvelop_confirmpanel_Confirmtype = "YES";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Dvelop_confirmpanel_Internalname, "ConfirmType", Dvelop_confirmpanel_Confirmtype);
         this.executeUsercontrolMethod("", false, "DVELOP_CONFIRMPANELContainer", "Confirm", "", new Object[] {});
      }

      protected void wb_table1_2_TR2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableContent", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_TR2( true) ;
         }
         else
         {
            wb_table2_5_TR2( false) ;
         }
         return  ;
      }

      protected void wb_table2_5_TR2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"right\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-right;text-align:-moz-right;text-align:-webkit-right")+"\">") ;
            wb_table3_39_TR2( true) ;
         }
         else
         {
            wb_table3_39_TR2( false) ;
         }
         return  ;
      }

      protected void wb_table3_39_TR2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVELOP_CONFIRMPANELContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_TR2e( true) ;
         }
         else
         {
            wb_table1_2_TR2e( false) ;
         }
      }

      protected void wb_table3_39_TR2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 42,'',false,'',0)\"";
            ClassString = "btn btn-success";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnenter_Internalname, "", "Confirmar", bttBtnenter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_DemandaAlterarQntSolicitada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'',0)\"";
            ClassString = "btn btn-default";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_cancel_Internalname, "", "Fechar", bttBtn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_DemandaAlterarQntSolicitada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_39_TR2e( true) ;
         }
         else
         {
            wb_table3_39_TR2e( false) ;
         }
      }

      protected void wb_table2_5_TR2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table4_13_TR2( true) ;
         }
         else
         {
            wb_table4_13_TR2( false) ;
         }
         return  ;
      }

      protected void wb_table4_13_TR2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_TR2e( true) ;
         }
         else
         {
            wb_table2_5_TR2e( false) ;
         }
      }

      protected void wb_table4_13_TR2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " height: " + StringUtil.LTrim( StringUtil.Str( (decimal)(450), 10, 0)) + "px" + ";";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(800), 10, 0)) + "px" + ";";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableData", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table5_16_TR2( true) ;
         }
         else
         {
            wb_table5_16_TR2( false) ;
         }
         return  ;
      }

      protected void wb_table5_16_TR2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_13_TR2e( true) ;
         }
         else
         {
            wb_table4_13_TR2e( false) ;
         }
      }

      protected void wb_table5_16_TR2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable_Internalname, tblTable_Internalname, "", "TableContent", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_osfsosfm_Internalname, "OS Ref|OS", "", "", lblTextblockcontagemresultado_osfsosfm_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_DemandaAlterarQntSolicitada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemResultado_OsFsOsFm_Internalname, A501ContagemResultado_OsFsOsFm, StringUtil.RTrim( context.localUtil.Format( A501ContagemResultado_OsFsOsFm, "")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultado_OsFsOsFm_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 30, "chr", 1, "row", 30, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_DemandaAlterarQntSolicitada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_descricao_Internalname, "T�tulo", "", "", lblTextblockcontagemresultado_descricao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_DemandaAlterarQntSolicitada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemResultado_Descricao_Internalname, A494ContagemResultado_Descricao, StringUtil.RTrim( context.localUtil.Format( A494ContagemResultado_Descricao, "")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultado_Descricao_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 100, "%", 1, "row", 500, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_DemandaAlterarQntSolicitada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_servicosigla_Internalname, "Servi�o", "", "", lblTextblockcontagemresultado_servicosigla_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_DemandaAlterarQntSolicitada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemResultado_ServicoSigla_Internalname, StringUtil.RTrim( A801ContagemResultado_ServicoSigla), StringUtil.RTrim( context.localUtil.Format( A801ContagemResultado_ServicoSigla, "@!")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultado_ServicoSigla_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "Sigla", "left", true, "HLP_WP_DemandaAlterarQntSolicitada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_quantidadesolicitada_Internalname, "Quantidade  Solicitada", "", "", lblTextblockcontagemresultado_quantidadesolicitada_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_DemandaAlterarQntSolicitada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_quantidadesolicitada_Internalname, StringUtil.LTrim( StringUtil.NToC( AV46ContagemResultado_QuantidadeSolicitada, 9, 4, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV46ContagemResultado_QuantidadeSolicitada, "ZZZ9.9999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','4');"+";gx.evt.onblur(this,36);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_quantidadesolicitada_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 9, "chr", 1, "row", 9, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_DemandaAlterarQntSolicitada.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_16_TR2e( true) ;
         }
         else
         {
            wb_table5_16_TR2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         A456ContagemResultado_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_CONTAGEMRESULTADO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A456ContagemResultado_Codigo), "ZZZZZ9")));
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PATR2( ) ;
         WSTR2( ) ;
         WETR2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202061620494489");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxdec.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wp_demandaalterarqntsolicitada.js", "?202061620494489");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/ConfirmPanel/BootstrapConfirmPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockcontagemresultado_osfsosfm_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_OSFSOSFM";
         edtContagemResultado_OsFsOsFm_Internalname = "CONTAGEMRESULTADO_OSFSOSFM";
         lblTextblockcontagemresultado_descricao_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_DESCRICAO";
         edtContagemResultado_Descricao_Internalname = "CONTAGEMRESULTADO_DESCRICAO";
         lblTextblockcontagemresultado_servicosigla_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_SERVICOSIGLA";
         edtContagemResultado_ServicoSigla_Internalname = "CONTAGEMRESULTADO_SERVICOSIGLA";
         lblTextblockcontagemresultado_quantidadesolicitada_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_QUANTIDADESOLICITADA";
         edtavContagemresultado_quantidadesolicitada_Internalname = "vCONTAGEMRESULTADO_QUANTIDADESOLICITADA";
         tblTable_Internalname = "TABLE";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         Dvpanel_tableattributes_Internalname = "DVPANEL_TABLEATTRIBUTES";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtnenter_Internalname = "BTNENTER";
         bttBtn_cancel_Internalname = "BTN_CANCEL";
         tblTableactions_Internalname = "TABLEACTIONS";
         Dvelop_confirmpanel_Internalname = "DVELOP_CONFIRMPANEL";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtavContagemresultado_quantidadesolicitada_Jsonclick = "";
         edtContagemResultado_ServicoSigla_Jsonclick = "";
         edtContagemResultado_Descricao_Jsonclick = "";
         edtContagemResultado_OsFsOsFm_Jsonclick = "";
         Dvelop_confirmpanel_Confirmtype = "";
         Dvelop_confirmpanel_Cancelbuttoncaption = "Fechar";
         Dvelop_confirmpanel_Yesbuttoncaption = "Confirmar";
         Dvelop_confirmpanel_Confirmationtext = "Gera��o das Demandandas vinculadas conclu�da. Verifique o relat�rio geardo!";
         Dvelop_confirmpanel_Title = "Aten��o!";
         Dvelop_confirmpanel_Height = "200px";
         Dvelop_confirmpanel_Width = "250px";
         Dvpanel_tableattributes_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Iconposition = "left";
         Dvpanel_tableattributes_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tableattributes_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Title = "Informa��es";
         Dvpanel_tableattributes_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tableattributes_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Alterar quantidade Solicitada";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("ENTER","{handler:'E13TR2',iparms:[{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV46ContagemResultado_QuantidadeSolicitada',fld:'vCONTAGEMRESULTADO_QUANTIDADESOLICITADA',pic:'ZZZ9.9999',nv:0.0}],oparms:[{av:'Dvelop_confirmpanel_Title',ctrl:'DVELOP_CONFIRMPANEL',prop:'Title'},{av:'Dvelop_confirmpanel_Confirmationtext',ctrl:'DVELOP_CONFIRMPANEL',prop:'ConfirmationText'},{av:'Dvelop_confirmpanel_Yesbuttoncaption',ctrl:'DVELOP_CONFIRMPANEL',prop:'YesButtonCaption'},{av:'Dvelop_confirmpanel_Confirmtype',ctrl:'DVELOP_CONFIRMPANEL',prop:'ConfirmType'}]}");
         setEventMetadata("DVELOP_CONFIRMPANEL.CLOSE","{handler:'E11TR2',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         A457ContagemResultado_Demanda = "";
         A493ContagemResultado_DemandaFM = "";
         A501ContagemResultado_OsFsOsFm = "";
         A494ContagemResultado_Descricao = "";
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         scmdbuf = "";
         H00TR2_A1553ContagemResultado_CntSrvCod = new int[1] ;
         H00TR2_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         H00TR2_A601ContagemResultado_Servico = new int[1] ;
         H00TR2_n601ContagemResultado_Servico = new bool[] {false} ;
         H00TR2_A456ContagemResultado_Codigo = new int[1] ;
         H00TR2_A2133ContagemResultado_QuantidadeSolicitada = new decimal[1] ;
         H00TR2_n2133ContagemResultado_QuantidadeSolicitada = new bool[] {false} ;
         H00TR2_A801ContagemResultado_ServicoSigla = new String[] {""} ;
         H00TR2_n801ContagemResultado_ServicoSigla = new bool[] {false} ;
         H00TR2_A494ContagemResultado_Descricao = new String[] {""} ;
         H00TR2_n494ContagemResultado_Descricao = new bool[] {false} ;
         H00TR2_A493ContagemResultado_DemandaFM = new String[] {""} ;
         H00TR2_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         H00TR2_A457ContagemResultado_Demanda = new String[] {""} ;
         H00TR2_n457ContagemResultado_Demanda = new bool[] {false} ;
         A801ContagemResultado_ServicoSigla = "";
         AV20Context = new wwpbaseobjects.SdtWWPContext(context);
         sStyleString = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         bttBtnenter_Jsonclick = "";
         bttBtn_cancel_Jsonclick = "";
         lblTextblockcontagemresultado_osfsosfm_Jsonclick = "";
         lblTextblockcontagemresultado_descricao_Jsonclick = "";
         lblTextblockcontagemresultado_servicosigla_Jsonclick = "";
         lblTextblockcontagemresultado_quantidadesolicitada_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wp_demandaalterarqntsolicitada__default(),
            new Object[][] {
                new Object[] {
               H00TR2_A1553ContagemResultado_CntSrvCod, H00TR2_n1553ContagemResultado_CntSrvCod, H00TR2_A601ContagemResultado_Servico, H00TR2_n601ContagemResultado_Servico, H00TR2_A456ContagemResultado_Codigo, H00TR2_A2133ContagemResultado_QuantidadeSolicitada, H00TR2_n2133ContagemResultado_QuantidadeSolicitada, H00TR2_A801ContagemResultado_ServicoSigla, H00TR2_n801ContagemResultado_ServicoSigla, H00TR2_A494ContagemResultado_Descricao,
               H00TR2_n494ContagemResultado_Descricao, H00TR2_A493ContagemResultado_DemandaFM, H00TR2_n493ContagemResultado_DemandaFM, H00TR2_A457ContagemResultado_Demanda, H00TR2_n457ContagemResultado_Demanda
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXWrapped ;
      private int A456ContagemResultado_Codigo ;
      private int wcpOA456ContagemResultado_Codigo ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A601ContagemResultado_Servico ;
      private int idxLst ;
      private decimal A2133ContagemResultado_QuantidadeSolicitada ;
      private decimal AV46ContagemResultado_QuantidadeSolicitada ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Dvpanel_tableattributes_Width ;
      private String Dvpanel_tableattributes_Cls ;
      private String Dvpanel_tableattributes_Title ;
      private String Dvpanel_tableattributes_Iconposition ;
      private String Dvelop_confirmpanel_Width ;
      private String Dvelop_confirmpanel_Height ;
      private String Dvelop_confirmpanel_Title ;
      private String Dvelop_confirmpanel_Confirmationtext ;
      private String Dvelop_confirmpanel_Yesbuttoncaption ;
      private String Dvelop_confirmpanel_Cancelbuttoncaption ;
      private String Dvelop_confirmpanel_Confirmtype ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavContagemresultado_quantidadesolicitada_Internalname ;
      private String scmdbuf ;
      private String A801ContagemResultado_ServicoSigla ;
      private String edtContagemResultado_OsFsOsFm_Internalname ;
      private String edtContagemResultado_Descricao_Internalname ;
      private String edtContagemResultado_ServicoSigla_Internalname ;
      private String Dvelop_confirmpanel_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtnenter_Internalname ;
      private String bttBtnenter_Jsonclick ;
      private String bttBtn_cancel_Internalname ;
      private String bttBtn_cancel_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblTableattributes_Internalname ;
      private String tblTable_Internalname ;
      private String lblTextblockcontagemresultado_osfsosfm_Internalname ;
      private String lblTextblockcontagemresultado_osfsosfm_Jsonclick ;
      private String edtContagemResultado_OsFsOsFm_Jsonclick ;
      private String lblTextblockcontagemresultado_descricao_Internalname ;
      private String lblTextblockcontagemresultado_descricao_Jsonclick ;
      private String edtContagemResultado_Descricao_Jsonclick ;
      private String lblTextblockcontagemresultado_servicosigla_Internalname ;
      private String lblTextblockcontagemresultado_servicosigla_Jsonclick ;
      private String edtContagemResultado_ServicoSigla_Jsonclick ;
      private String lblTextblockcontagemresultado_quantidadesolicitada_Internalname ;
      private String lblTextblockcontagemresultado_quantidadesolicitada_Jsonclick ;
      private String edtavContagemresultado_quantidadesolicitada_Jsonclick ;
      private String Dvpanel_tableattributes_Internalname ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool Dvpanel_tableattributes_Collapsible ;
      private bool Dvpanel_tableattributes_Collapsed ;
      private bool Dvpanel_tableattributes_Autowidth ;
      private bool Dvpanel_tableattributes_Autoheight ;
      private bool Dvpanel_tableattributes_Showcollapseicon ;
      private bool Dvpanel_tableattributes_Autoscroll ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n601ContagemResultado_Servico ;
      private bool n2133ContagemResultado_QuantidadeSolicitada ;
      private bool n801ContagemResultado_ServicoSigla ;
      private bool n494ContagemResultado_Descricao ;
      private bool n493ContagemResultado_DemandaFM ;
      private bool n457ContagemResultado_Demanda ;
      private bool returnInSub ;
      private String A457ContagemResultado_Demanda ;
      private String A493ContagemResultado_DemandaFM ;
      private String A501ContagemResultado_OsFsOsFm ;
      private String A494ContagemResultado_Descricao ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] H00TR2_A1553ContagemResultado_CntSrvCod ;
      private bool[] H00TR2_n1553ContagemResultado_CntSrvCod ;
      private int[] H00TR2_A601ContagemResultado_Servico ;
      private bool[] H00TR2_n601ContagemResultado_Servico ;
      private int[] H00TR2_A456ContagemResultado_Codigo ;
      private decimal[] H00TR2_A2133ContagemResultado_QuantidadeSolicitada ;
      private bool[] H00TR2_n2133ContagemResultado_QuantidadeSolicitada ;
      private String[] H00TR2_A801ContagemResultado_ServicoSigla ;
      private bool[] H00TR2_n801ContagemResultado_ServicoSigla ;
      private String[] H00TR2_A494ContagemResultado_Descricao ;
      private bool[] H00TR2_n494ContagemResultado_Descricao ;
      private String[] H00TR2_A493ContagemResultado_DemandaFM ;
      private bool[] H00TR2_n493ContagemResultado_DemandaFM ;
      private String[] H00TR2_A457ContagemResultado_Demanda ;
      private bool[] H00TR2_n457ContagemResultado_Demanda ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV20Context ;
   }

   public class wp_demandaalterarqntsolicitada__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00TR2 ;
          prmH00TR2 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00TR2", "SELECT T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T2.[Servico_Codigo] AS ContagemResultado_Servico, T1.[ContagemResultado_Codigo], T1.[ContagemResultado_QuantidadeSolicitada], T3.[Servico_Sigla] AS ContagemResultado_ServicoSigla, T1.[ContagemResultado_Descricao], T1.[ContagemResultado_DemandaFM], T1.[ContagemResultado_Demanda] FROM (([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [Servico] T3 WITH (NOLOCK) ON T3.[Servico_Codigo] = T2.[Servico_Codigo]) WHERE T1.[ContagemResultado_Codigo] = @ContagemResultado_Codigo ORDER BY T1.[ContagemResultado_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00TR2,1,0,true,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((decimal[]) buf[5])[0] = rslt.getDecimal(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getString(5, 15) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((String[]) buf[11])[0] = rslt.getVarchar(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((String[]) buf[13])[0] = rslt.getVarchar(8) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
