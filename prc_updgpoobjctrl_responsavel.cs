/*
               File: PRC_UpdGpoObjCtrl_Responsavel
        Description: Upd Gpo Obj Ctrl_Responsavel
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:54:51.55
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_updgpoobjctrl_responsavel : GXProcedure
   {
      public prc_updgpoobjctrl_responsavel( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_updgpoobjctrl_responsavel( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           int aP1_Contratante_Codigo ,
                           int aP2_GpoObjCtrl_Codigo ,
                           int aP3_Usuario_Codigo )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV8Contratante_Codigo = aP1_Contratante_Codigo;
         this.AV9GpoObjCtrl_Codigo = aP2_GpoObjCtrl_Codigo;
         this.AV10Usuario_Codigo = aP3_Usuario_Codigo;
         initialize();
         executePrivate();
      }

      public void executeSubmit( String aP0_Gx_mode ,
                                 int aP1_Contratante_Codigo ,
                                 int aP2_GpoObjCtrl_Codigo ,
                                 int aP3_Usuario_Codigo )
      {
         prc_updgpoobjctrl_responsavel objprc_updgpoobjctrl_responsavel;
         objprc_updgpoobjctrl_responsavel = new prc_updgpoobjctrl_responsavel();
         objprc_updgpoobjctrl_responsavel.Gx_mode = aP0_Gx_mode;
         objprc_updgpoobjctrl_responsavel.AV8Contratante_Codigo = aP1_Contratante_Codigo;
         objprc_updgpoobjctrl_responsavel.AV9GpoObjCtrl_Codigo = aP2_GpoObjCtrl_Codigo;
         objprc_updgpoobjctrl_responsavel.AV10Usuario_Codigo = aP3_Usuario_Codigo;
         objprc_updgpoobjctrl_responsavel.context.SetSubmitInitialConfig(context);
         objprc_updgpoobjctrl_responsavel.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_updgpoobjctrl_responsavel);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_updgpoobjctrl_responsavel)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
         {
            AV14GXLvl6 = 0;
            /* Using cursor P00SZ2 */
            pr_default.execute(0, new Object[] {AV9GpoObjCtrl_Codigo, AV8Contratante_Codigo});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A1834GpoObjCtrlResponsavel_CteCteCod = P00SZ2_A1834GpoObjCtrlResponsavel_CteCteCod[0];
               n1834GpoObjCtrlResponsavel_CteCteCod = P00SZ2_n1834GpoObjCtrlResponsavel_CteCteCod[0];
               A1838GpoObjCtrlResponsavel_GpoCod = P00SZ2_A1838GpoObjCtrlResponsavel_GpoCod[0];
               n1838GpoObjCtrlResponsavel_GpoCod = P00SZ2_n1838GpoObjCtrlResponsavel_GpoCod[0];
               A1835GpoObjCtrlResponsavel_CteUsrCod = P00SZ2_A1835GpoObjCtrlResponsavel_CteUsrCod[0];
               n1835GpoObjCtrlResponsavel_CteUsrCod = P00SZ2_n1835GpoObjCtrlResponsavel_CteUsrCod[0];
               A1837GpoObjCtrlResponsavel_Codigo = P00SZ2_A1837GpoObjCtrlResponsavel_Codigo[0];
               AV14GXLvl6 = 1;
               if ( (0==AV10Usuario_Codigo) )
               {
                  A1835GpoObjCtrlResponsavel_CteUsrCod = 0;
                  n1835GpoObjCtrlResponsavel_CteUsrCod = false;
                  n1835GpoObjCtrlResponsavel_CteUsrCod = true;
               }
               else
               {
                  A1835GpoObjCtrlResponsavel_CteUsrCod = AV10Usuario_Codigo;
                  n1835GpoObjCtrlResponsavel_CteUsrCod = false;
               }
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               /* Using cursor P00SZ3 */
               pr_default.execute(1, new Object[] {n1835GpoObjCtrlResponsavel_CteUsrCod, A1835GpoObjCtrlResponsavel_CteUsrCod, A1837GpoObjCtrlResponsavel_Codigo});
               pr_default.close(1);
               dsDefault.SmartCacheProvider.SetUpdated("GpoObjCtrlResponsavel") ;
               if (true) break;
               /* Using cursor P00SZ4 */
               pr_default.execute(2, new Object[] {n1835GpoObjCtrlResponsavel_CteUsrCod, A1835GpoObjCtrlResponsavel_CteUsrCod, A1837GpoObjCtrlResponsavel_Codigo});
               pr_default.close(2);
               dsDefault.SmartCacheProvider.SetUpdated("GpoObjCtrlResponsavel") ;
               pr_default.readNext(0);
            }
            pr_default.close(0);
            if ( AV14GXLvl6 == 0 )
            {
               if ( AV10Usuario_Codigo > 0 )
               {
                  /*
                     INSERT RECORD ON TABLE GpoObjCtrlResponsavel

                  */
                  A1838GpoObjCtrlResponsavel_GpoCod = (short)(AV9GpoObjCtrl_Codigo);
                  n1838GpoObjCtrlResponsavel_GpoCod = false;
                  A1834GpoObjCtrlResponsavel_CteCteCod = AV8Contratante_Codigo;
                  n1834GpoObjCtrlResponsavel_CteCteCod = false;
                  A1835GpoObjCtrlResponsavel_CteUsrCod = AV10Usuario_Codigo;
                  n1835GpoObjCtrlResponsavel_CteUsrCod = false;
                  /* Using cursor P00SZ5 */
                  pr_default.execute(3, new Object[] {n1838GpoObjCtrlResponsavel_GpoCod, A1838GpoObjCtrlResponsavel_GpoCod, n1834GpoObjCtrlResponsavel_CteCteCod, A1834GpoObjCtrlResponsavel_CteCteCod, n1835GpoObjCtrlResponsavel_CteUsrCod, A1835GpoObjCtrlResponsavel_CteUsrCod});
                  A1837GpoObjCtrlResponsavel_Codigo = P00SZ5_A1837GpoObjCtrlResponsavel_Codigo[0];
                  pr_default.close(3);
                  dsDefault.SmartCacheProvider.SetUpdated("GpoObjCtrlResponsavel") ;
                  if ( (pr_default.getStatus(3) == 1) )
                  {
                     context.Gx_err = 1;
                     Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
                  }
                  else
                  {
                     context.Gx_err = 0;
                     Gx_emsg = "";
                  }
                  /* End Insert */
               }
            }
         }
         else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
         {
            /* Using cursor P00SZ6 */
            pr_default.execute(4, new Object[] {AV8Contratante_Codigo, AV10Usuario_Codigo, AV9GpoObjCtrl_Codigo});
            while ( (pr_default.getStatus(4) != 101) )
            {
               A1835GpoObjCtrlResponsavel_CteUsrCod = P00SZ6_A1835GpoObjCtrlResponsavel_CteUsrCod[0];
               n1835GpoObjCtrlResponsavel_CteUsrCod = P00SZ6_n1835GpoObjCtrlResponsavel_CteUsrCod[0];
               A1834GpoObjCtrlResponsavel_CteCteCod = P00SZ6_A1834GpoObjCtrlResponsavel_CteCteCod[0];
               n1834GpoObjCtrlResponsavel_CteCteCod = P00SZ6_n1834GpoObjCtrlResponsavel_CteCteCod[0];
               A1838GpoObjCtrlResponsavel_GpoCod = P00SZ6_A1838GpoObjCtrlResponsavel_GpoCod[0];
               n1838GpoObjCtrlResponsavel_GpoCod = P00SZ6_n1838GpoObjCtrlResponsavel_GpoCod[0];
               A1837GpoObjCtrlResponsavel_Codigo = P00SZ6_A1837GpoObjCtrlResponsavel_Codigo[0];
               /* Using cursor P00SZ7 */
               pr_default.execute(5, new Object[] {A1837GpoObjCtrlResponsavel_Codigo});
               pr_default.close(5);
               dsDefault.SmartCacheProvider.SetUpdated("GpoObjCtrlResponsavel") ;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               pr_default.readNext(4);
            }
            pr_default.close(4);
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "PRC_UpdGpoObjCtrl_Responsavel");
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00SZ2_A1834GpoObjCtrlResponsavel_CteCteCod = new int[1] ;
         P00SZ2_n1834GpoObjCtrlResponsavel_CteCteCod = new bool[] {false} ;
         P00SZ2_A1838GpoObjCtrlResponsavel_GpoCod = new short[1] ;
         P00SZ2_n1838GpoObjCtrlResponsavel_GpoCod = new bool[] {false} ;
         P00SZ2_A1835GpoObjCtrlResponsavel_CteUsrCod = new int[1] ;
         P00SZ2_n1835GpoObjCtrlResponsavel_CteUsrCod = new bool[] {false} ;
         P00SZ2_A1837GpoObjCtrlResponsavel_Codigo = new int[1] ;
         P00SZ5_A1837GpoObjCtrlResponsavel_Codigo = new int[1] ;
         Gx_emsg = "";
         P00SZ6_A1835GpoObjCtrlResponsavel_CteUsrCod = new int[1] ;
         P00SZ6_n1835GpoObjCtrlResponsavel_CteUsrCod = new bool[] {false} ;
         P00SZ6_A1834GpoObjCtrlResponsavel_CteCteCod = new int[1] ;
         P00SZ6_n1834GpoObjCtrlResponsavel_CteCteCod = new bool[] {false} ;
         P00SZ6_A1838GpoObjCtrlResponsavel_GpoCod = new short[1] ;
         P00SZ6_n1838GpoObjCtrlResponsavel_GpoCod = new bool[] {false} ;
         P00SZ6_A1837GpoObjCtrlResponsavel_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_updgpoobjctrl_responsavel__default(),
            new Object[][] {
                new Object[] {
               P00SZ2_A1834GpoObjCtrlResponsavel_CteCteCod, P00SZ2_n1834GpoObjCtrlResponsavel_CteCteCod, P00SZ2_A1838GpoObjCtrlResponsavel_GpoCod, P00SZ2_n1838GpoObjCtrlResponsavel_GpoCod, P00SZ2_A1835GpoObjCtrlResponsavel_CteUsrCod, P00SZ2_n1835GpoObjCtrlResponsavel_CteUsrCod, P00SZ2_A1837GpoObjCtrlResponsavel_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               P00SZ5_A1837GpoObjCtrlResponsavel_Codigo
               }
               , new Object[] {
               P00SZ6_A1835GpoObjCtrlResponsavel_CteUsrCod, P00SZ6_n1835GpoObjCtrlResponsavel_CteUsrCod, P00SZ6_A1834GpoObjCtrlResponsavel_CteCteCod, P00SZ6_n1834GpoObjCtrlResponsavel_CteCteCod, P00SZ6_A1838GpoObjCtrlResponsavel_GpoCod, P00SZ6_n1838GpoObjCtrlResponsavel_GpoCod, P00SZ6_A1837GpoObjCtrlResponsavel_Codigo
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV14GXLvl6 ;
      private short A1838GpoObjCtrlResponsavel_GpoCod ;
      private int AV8Contratante_Codigo ;
      private int AV9GpoObjCtrl_Codigo ;
      private int AV10Usuario_Codigo ;
      private int A1834GpoObjCtrlResponsavel_CteCteCod ;
      private int A1835GpoObjCtrlResponsavel_CteUsrCod ;
      private int A1837GpoObjCtrlResponsavel_Codigo ;
      private int GX_INS202 ;
      private String Gx_mode ;
      private String scmdbuf ;
      private String Gx_emsg ;
      private bool n1834GpoObjCtrlResponsavel_CteCteCod ;
      private bool n1838GpoObjCtrlResponsavel_GpoCod ;
      private bool n1835GpoObjCtrlResponsavel_CteUsrCod ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00SZ2_A1834GpoObjCtrlResponsavel_CteCteCod ;
      private bool[] P00SZ2_n1834GpoObjCtrlResponsavel_CteCteCod ;
      private short[] P00SZ2_A1838GpoObjCtrlResponsavel_GpoCod ;
      private bool[] P00SZ2_n1838GpoObjCtrlResponsavel_GpoCod ;
      private int[] P00SZ2_A1835GpoObjCtrlResponsavel_CteUsrCod ;
      private bool[] P00SZ2_n1835GpoObjCtrlResponsavel_CteUsrCod ;
      private int[] P00SZ2_A1837GpoObjCtrlResponsavel_Codigo ;
      private int[] P00SZ5_A1837GpoObjCtrlResponsavel_Codigo ;
      private int[] P00SZ6_A1835GpoObjCtrlResponsavel_CteUsrCod ;
      private bool[] P00SZ6_n1835GpoObjCtrlResponsavel_CteUsrCod ;
      private int[] P00SZ6_A1834GpoObjCtrlResponsavel_CteCteCod ;
      private bool[] P00SZ6_n1834GpoObjCtrlResponsavel_CteCteCod ;
      private short[] P00SZ6_A1838GpoObjCtrlResponsavel_GpoCod ;
      private bool[] P00SZ6_n1838GpoObjCtrlResponsavel_GpoCod ;
      private int[] P00SZ6_A1837GpoObjCtrlResponsavel_Codigo ;
   }

   public class prc_updgpoobjctrl_responsavel__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new UpdateCursor(def[1])
         ,new UpdateCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new UpdateCursor(def[5])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00SZ2 ;
          prmP00SZ2 = new Object[] {
          new Object[] {"@AV9GpoObjCtrl_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV8Contratante_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00SZ3 ;
          prmP00SZ3 = new Object[] {
          new Object[] {"@GpoObjCtrlResponsavel_CteUsrCod",SqlDbType.Int,6,0} ,
          new Object[] {"@GpoObjCtrlResponsavel_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00SZ4 ;
          prmP00SZ4 = new Object[] {
          new Object[] {"@GpoObjCtrlResponsavel_CteUsrCod",SqlDbType.Int,6,0} ,
          new Object[] {"@GpoObjCtrlResponsavel_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00SZ5 ;
          prmP00SZ5 = new Object[] {
          new Object[] {"@GpoObjCtrlResponsavel_GpoCod",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@GpoObjCtrlResponsavel_CteCteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@GpoObjCtrlResponsavel_CteUsrCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00SZ6 ;
          prmP00SZ6 = new Object[] {
          new Object[] {"@AV8Contratante_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV10Usuario_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV9GpoObjCtrl_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00SZ7 ;
          prmP00SZ7 = new Object[] {
          new Object[] {"@GpoObjCtrlResponsavel_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00SZ2", "SELECT TOP 1 [GpoObjCtrlResponsavel_CteCteCod], [GpoObjCtrlResponsavel_GpoCod], [GpoObjCtrlResponsavel_CteUsrCod], [GpoObjCtrlResponsavel_Codigo] FROM [GpoObjCtrlResponsavel] WITH (UPDLOCK) WHERE ([GpoObjCtrlResponsavel_GpoCod] = @AV9GpoObjCtrl_Codigo) AND ([GpoObjCtrlResponsavel_CteCteCod] = @AV8Contratante_Codigo) ORDER BY [GpoObjCtrlResponsavel_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00SZ2,1,0,true,true )
             ,new CursorDef("P00SZ3", "UPDATE [GpoObjCtrlResponsavel] SET [GpoObjCtrlResponsavel_CteUsrCod]=@GpoObjCtrlResponsavel_CteUsrCod  WHERE [GpoObjCtrlResponsavel_Codigo] = @GpoObjCtrlResponsavel_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00SZ3)
             ,new CursorDef("P00SZ4", "UPDATE [GpoObjCtrlResponsavel] SET [GpoObjCtrlResponsavel_CteUsrCod]=@GpoObjCtrlResponsavel_CteUsrCod  WHERE [GpoObjCtrlResponsavel_Codigo] = @GpoObjCtrlResponsavel_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00SZ4)
             ,new CursorDef("P00SZ5", "INSERT INTO [GpoObjCtrlResponsavel]([GpoObjCtrlResponsavel_GpoCod], [GpoObjCtrlResponsavel_CteCteCod], [GpoObjCtrlResponsavel_CteUsrCod]) VALUES(@GpoObjCtrlResponsavel_GpoCod, @GpoObjCtrlResponsavel_CteCteCod, @GpoObjCtrlResponsavel_CteUsrCod); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmP00SZ5)
             ,new CursorDef("P00SZ6", "SELECT [GpoObjCtrlResponsavel_CteUsrCod], [GpoObjCtrlResponsavel_CteCteCod], [GpoObjCtrlResponsavel_GpoCod], [GpoObjCtrlResponsavel_Codigo] FROM [GpoObjCtrlResponsavel] WITH (UPDLOCK) WHERE ([GpoObjCtrlResponsavel_CteCteCod] = @AV8Contratante_Codigo and [GpoObjCtrlResponsavel_CteUsrCod] = @AV10Usuario_Codigo) AND ([GpoObjCtrlResponsavel_GpoCod] = @AV9GpoObjCtrl_Codigo) ORDER BY [GpoObjCtrlResponsavel_CteCteCod], [GpoObjCtrlResponsavel_CteUsrCod] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00SZ6,1,0,true,false )
             ,new CursorDef("P00SZ7", "DELETE FROM [GpoObjCtrlResponsavel]  WHERE [GpoObjCtrlResponsavel_Codigo] = @GpoObjCtrlResponsavel_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00SZ7)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((short[]) buf[2])[0] = rslt.getShort(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((short[]) buf[4])[0] = rslt.getShort(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 1 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
             case 3 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(1, (short)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(2, (int)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(3, (int)parms[5]);
                }
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
