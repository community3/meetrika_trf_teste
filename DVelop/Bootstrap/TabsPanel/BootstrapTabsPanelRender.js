function BootstrapTabsPanel()
{
	this.DesignTimeTabs;
	this.SelectedTabIndex;
	this.Width;
	this.Height;
	this.AutoWidth;
	this.AutoHeight;
	this.AutoScroll;
	this.Cls;
	this.ActiveTabId;

	this.show = function()
	{
		///UserCodeRegionStart:[show] (do not remove this comment.)

		if(this.my_tabsPanel == undefined){
			this.my_tabsPanel = new DVelopBootstrapTabs(this);
			this.my_tabsPanel.render();	
			var n = $('#DVelopBootstrapTabsContent_' + this.ContainerName).get(0);
			while(n.parentElement.tagName != 'HTML')
			{
				$(n).css('height', '100%');
				n = n.parentElement;
			}
		}
















































		///UserCodeRegionEnd: (do not remove this comment.)
	}
	///UserCodeRegionStart:[User Functions] (do not remove this comment.)


	this.SetTabTitle = function(tabId, title) {
		if(this.my_tabsPanel != undefined){
			this.my_tabsPanel.setTabTitle(tabId, title);
		}
	}
	
	this.AddIFrameTab = function(url, title) {
		if(this.my_tabsPanel != undefined){
			this.my_tabsPanel.addIFrameTab(url, title);
		}
	}
	
	this.SelectTab = function(tabId) {
		if(this.my_tabsPanel != undefined){
			this.my_tabsPanel.SelectTab(tabId);
		}
	}
	
	this.ShowTab = function(tabId) {
		if(this.my_tabsPanel != undefined){
			this.my_tabsPanel.showTab(tabId);
		}
}

this.HideTabById = function (tabId) {
    if (this.my_tabsPanel != undefined) {
        this.my_tabsPanel.hideTabById(tabId);
    }
}

this.HideTab = function (tabIndex) {
    if (this.my_tabsPanel != undefined) {
        this.my_tabsPanel.hideTabByIndex(tabIndex);
    }
}
	














































	///UserCodeRegionEnd: (do not remove this comment.):
}
