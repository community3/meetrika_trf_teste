/*
               File: SolicitacaodeServico
        Description: Solicitacao de Servico
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/29/2020 9:35:29.75
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class solicitacaodeservico : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public solicitacaodeservico( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public solicitacaodeservico( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_Solicitacoes_Codigo )
      {
         this.AV17Solicitacoes_Codigo = aP0_Solicitacoes_Codigo;
         executePrivate();
         aP0_Solicitacoes_Codigo=this.AV17Solicitacoes_Codigo;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         dynavContratada_codigo = new GXCombobox();
         chkavSolicitacoes_novo_projeto = new GXCheckbox();
         dynavServico_codigo = new GXCombobox();
         dynavSistema_codigo = new GXCombobox();
         dynavFuncaousuario_codigo = new GXCombobox();
         chkavExcluir = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vCONTRATADA_CODIGO") == 0 )
            {
               ajax_req_read_hidden_sdt(GetNextPar( ), AV25WWPContext);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvCONTRATADA_CODIGOAZ2( AV25WWPContext) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vSERVICO_CODIGO") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvSERVICO_CODIGOAZ2( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vSISTEMA_CODIGO") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvSISTEMA_CODIGOAZ2( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vFUNCAOUSUARIO_CODIGO") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvFUNCAOUSUARIO_CODIGOAZ2( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid1") == 0 )
            {
               nRC_GXsfl_67 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_67_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_67_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid1_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid1") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid1_refresh( ) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV17Solicitacoes_Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Solicitacoes_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17Solicitacoes_Codigo), 6, 0)));
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PAAZ2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTAZ2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20205299352981");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("solicitacaodeservico.aspx") + "?" + UrlEncode("" +AV17Solicitacoes_Codigo)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "Sdtsolicitacao", AV12SDTSolicitacao);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("Sdtsolicitacao", AV12SDTSolicitacao);
         }
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_67", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_67), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vERRO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV28Erro), 4, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSDTSOLICITACAO", AV12SDTSolicitacao);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSDTSOLICITACAO", AV12SDTSolicitacao);
         }
         GxWebStd.gx_hidden_field( context, "vVRMSG", StringUtil.RTrim( AV27vrMsg));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSDTSOLICITACAOITEM", AV13SDTSolicitacaoItem);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSDTSOLICITACAOITEM", AV13SDTSolicitacaoItem);
         }
         GxWebStd.gx_hidden_field( context, "vFUNCAOUSUARIO_NOME", AV26FuncaoUsuario_Nome);
         GxWebStd.gx_hidden_field( context, "FUNCAOUSUARIO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A161FuncaoUsuario_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "FUNCAOUSUARIO_NOME", A162FuncaoUsuario_Nome);
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vWWPCONTEXT", AV25WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vWWPCONTEXT", AV25WWPContext);
         }
         GXCCtlgxBlob = "vNECESSIDADE_ARQUIVO" + "_gxBlob";
         GxWebStd.gx_hidden_field( context, GXCCtlgxBlob, AV10Necessidade_Arquivo);
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WEAZ2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTAZ2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("solicitacaodeservico.aspx") + "?" + UrlEncode("" +AV17Solicitacoes_Codigo) ;
      }

      public override String GetPgmname( )
      {
         return "SolicitacaodeServico" ;
      }

      public override String GetPgmdesc( )
      {
         return "Solicitacao de Servico" ;
      }

      protected void WBAZ0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_AZ2( true) ;
         }
         else
         {
            wb_table1_2_AZ2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_AZ2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtavSolicitacoes_codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV17Solicitacoes_Codigo), 6, 0, ",", "")), context.localUtil.Format( (decimal)(AV17Solicitacoes_Codigo), "ZZZZZ9"), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavSolicitacoes_codigo_Jsonclick, 0, "Attribute", "", "", "", edtavSolicitacoes_codigo_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_SolicitacaodeServico.htm");
         }
         wbLoad = true;
      }

      protected void STARTAZ2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Solicitacao de Servico", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPAZ0( ) ;
      }

      protected void WSAZ2( )
      {
         STARTAZ2( ) ;
         EVTAZ2( ) ;
      }

      protected void EVTAZ2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ! wbErr )
                              {
                                 Rfr0gs = false;
                                 if ( ! Rfr0gs )
                                 {
                                    /* Execute user event: E11AZ2 */
                                    E11AZ2 ();
                                 }
                                 dynload_actions( ) ;
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'PDF'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12AZ2 */
                              E12AZ2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'INCLUIR'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E13AZ2 */
                              E13AZ2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'SAIR'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E14AZ2 */
                              E14AZ2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 10), "GRID1.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 14), "VEXCLUIR.CLICK") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 14), "VEXCLUIR.CLICK") == 0 ) )
                           {
                              nGXsfl_67_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_67_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_67_idx), 4, 0)), 4, "0");
                              SubsflControlProps_672( ) ;
                              AV32GXV1 = nGXsfl_67_idx;
                              if ( ( AV12SDTSolicitacao.Count >= AV32GXV1 ) && ( AV32GXV1 > 0 ) )
                              {
                                 AV12SDTSolicitacao.CurrentItem = ((SdtSDTSolicitacao_SDTSolicitacaoItem)AV12SDTSolicitacao.Item(AV32GXV1));
                                 AV6Excluir = ((StringUtil.StrCmp(cgiGet( chkavExcluir_Internalname), "S")==0) ? "S" : "N");
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, chkavExcluir_Internalname, AV6Excluir);
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( AV10Necessidade_Arquivo)) )
                              {
                                 GXCCtl = "vNECESSIDADE_ARQUIVO_" + sGXsfl_67_idx;
                                 GXCCtlgxBlob = GXCCtl + "_gxBlob";
                                 AV10Necessidade_Arquivo = cgiGet( GXCCtlgxBlob);
                              }
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E15AZ2 */
                                    E15AZ2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E16AZ2 */
                                    E16AZ2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID1.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E17AZ2 */
                                    E17AZ2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "VEXCLUIR.CLICK") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E18AZ2 */
                                    E18AZ2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEAZ2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PAAZ2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            dynavContratada_codigo.Name = "vCONTRATADA_CODIGO";
            dynavContratada_codigo.WebTags = "";
            chkavSolicitacoes_novo_projeto.Name = "vSOLICITACOES_NOVO_PROJETO";
            chkavSolicitacoes_novo_projeto.WebTags = "";
            chkavSolicitacoes_novo_projeto.Caption = "Novo Projeto";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavSolicitacoes_novo_projeto_Internalname, "TitleCaption", chkavSolicitacoes_novo_projeto.Caption);
            chkavSolicitacoes_novo_projeto.CheckedValue = "0";
            dynavServico_codigo.Name = "vSERVICO_CODIGO";
            dynavServico_codigo.WebTags = "";
            dynavSistema_codigo.Name = "vSISTEMA_CODIGO";
            dynavSistema_codigo.WebTags = "";
            dynavFuncaousuario_codigo.Name = "vFUNCAOUSUARIO_CODIGO";
            dynavFuncaousuario_codigo.WebTags = "";
            GXCCtl = "vEXCLUIR_" + sGXsfl_67_idx;
            chkavExcluir.Name = GXCCtl;
            chkavExcluir.WebTags = "";
            chkavExcluir.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavExcluir_Internalname, "TitleCaption", chkavExcluir.Caption);
            chkavExcluir.CheckedValue = "N";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = dynavContratada_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLVvCONTRATADA_CODIGOAZ2( wwpbaseobjects.SdtWWPContext AV25WWPContext )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvCONTRATADA_CODIGO_dataAZ2( AV25WWPContext) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvCONTRATADA_CODIGO_htmlAZ2( wwpbaseobjects.SdtWWPContext AV25WWPContext )
      {
         int gxdynajaxvalue ;
         GXDLVvCONTRATADA_CODIGO_dataAZ2( AV25WWPContext) ;
         gxdynajaxindex = 1;
         dynavContratada_codigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavContratada_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavContratada_codigo.ItemCount > 0 )
         {
            AV5Contratada_codigo = (int)(NumberUtil.Val( dynavContratada_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV5Contratada_codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5Contratada_codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV5Contratada_codigo), 6, 0)));
         }
      }

      protected void GXDLVvCONTRATADA_CODIGO_dataAZ2( wwpbaseobjects.SdtWWPContext AV25WWPContext )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("Selecione");
         /* Using cursor H00AZ2 */
         pr_default.execute(0, new Object[] {AV25WWPContext.gxTpr_Areatrabalho_codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00AZ2_A39Contratada_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00AZ2_A41Contratada_PessoaNom[0]));
            pr_default.readNext(0);
         }
         pr_default.close(0);
      }

      protected void GXDLVvSERVICO_CODIGOAZ2( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvSERVICO_CODIGO_dataAZ2( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvSERVICO_CODIGO_htmlAZ2( )
      {
         int gxdynajaxvalue ;
         GXDLVvSERVICO_CODIGO_dataAZ2( ) ;
         gxdynajaxindex = 1;
         dynavServico_codigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavServico_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavServico_codigo.ItemCount > 0 )
         {
            AV14Servico_Codigo = (int)(NumberUtil.Val( dynavServico_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV14Servico_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14Servico_Codigo), 6, 0)));
         }
      }

      protected void GXDLVvSERVICO_CODIGO_dataAZ2( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("Selecione");
         /* Using cursor H00AZ3 */
         pr_default.execute(1);
         while ( (pr_default.getStatus(1) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00AZ3_A155Servico_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00AZ3_A608Servico_Nome[0]));
            pr_default.readNext(1);
         }
         pr_default.close(1);
      }

      protected void GXDLVvSISTEMA_CODIGOAZ2( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvSISTEMA_CODIGO_dataAZ2( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvSISTEMA_CODIGO_htmlAZ2( )
      {
         int gxdynajaxvalue ;
         GXDLVvSISTEMA_CODIGO_dataAZ2( ) ;
         gxdynajaxindex = 1;
         dynavSistema_codigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavSistema_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavSistema_codigo.ItemCount > 0 )
         {
            AV15Sistema_Codigo = (int)(NumberUtil.Val( dynavSistema_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV15Sistema_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15Sistema_Codigo), 6, 0)));
         }
      }

      protected void GXDLVvSISTEMA_CODIGO_dataAZ2( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("Selecione");
         /* Using cursor H00AZ4 */
         pr_default.execute(2);
         while ( (pr_default.getStatus(2) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00AZ4_A127Sistema_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(H00AZ4_A416Sistema_Nome[0]);
            pr_default.readNext(2);
         }
         pr_default.close(2);
      }

      protected void GXDLVvFUNCAOUSUARIO_CODIGOAZ2( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvFUNCAOUSUARIO_CODIGO_dataAZ2( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvFUNCAOUSUARIO_CODIGO_htmlAZ2( )
      {
         int gxdynajaxvalue ;
         GXDLVvFUNCAOUSUARIO_CODIGO_dataAZ2( ) ;
         gxdynajaxindex = 1;
         dynavFuncaousuario_codigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavFuncaousuario_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavFuncaousuario_codigo.ItemCount > 0 )
         {
            AV8FuncaoUsuario_Codigo = (int)(NumberUtil.Val( dynavFuncaousuario_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV8FuncaoUsuario_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8FuncaoUsuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8FuncaoUsuario_Codigo), 6, 0)));
         }
      }

      protected void GXDLVvFUNCAOUSUARIO_CODIGO_dataAZ2( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("Selecione");
         /* Using cursor H00AZ5 */
         pr_default.execute(3);
         while ( (pr_default.getStatus(3) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00AZ5_A161FuncaoUsuario_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(H00AZ5_A162FuncaoUsuario_Nome[0]);
            pr_default.readNext(3);
         }
         pr_default.close(3);
      }

      protected void gxnrGrid1_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_672( ) ;
         while ( nGXsfl_67_idx <= nRC_GXsfl_67 )
         {
            sendrow_672( ) ;
            nGXsfl_67_idx = (short)(((subGrid1_Islastpage==1)&&(nGXsfl_67_idx+1>subGrid1_Recordsperpage( )) ? 1 : nGXsfl_67_idx+1));
            sGXsfl_67_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_67_idx), 4, 0)), 4, "0");
            SubsflControlProps_672( ) ;
         }
         context.GX_webresponse.AddString(Grid1Container.ToJavascriptSource());
         /* End function gxnrGrid1_newrow */
      }

      protected void gxgrGrid1_refresh( )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GRID1_nCurrentRecord = 0;
         RFAZ2( ) ;
         /* End function gxgrGrid1_refresh */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( dynavContratada_codigo.ItemCount > 0 )
         {
            AV5Contratada_codigo = (int)(NumberUtil.Val( dynavContratada_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV5Contratada_codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5Contratada_codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV5Contratada_codigo), 6, 0)));
         }
         if ( dynavServico_codigo.ItemCount > 0 )
         {
            AV14Servico_Codigo = (int)(NumberUtil.Val( dynavServico_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV14Servico_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14Servico_Codigo), 6, 0)));
         }
         if ( dynavSistema_codigo.ItemCount > 0 )
         {
            AV15Sistema_Codigo = (int)(NumberUtil.Val( dynavSistema_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV15Sistema_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15Sistema_Codigo), 6, 0)));
         }
         if ( dynavFuncaousuario_codigo.ItemCount > 0 )
         {
            AV8FuncaoUsuario_Codigo = (int)(NumberUtil.Val( dynavFuncaousuario_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV8FuncaoUsuario_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8FuncaoUsuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8FuncaoUsuario_Codigo), 6, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFAZ2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavCtlfuncaousuario_codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtlfuncaousuario_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlfuncaousuario_codigo_Enabled), 5, 0)));
      }

      protected void RFAZ2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            Grid1Container.ClearRows();
         }
         wbStart = 67;
         /* Execute user event: E16AZ2 */
         E16AZ2 ();
         nGXsfl_67_idx = 1;
         sGXsfl_67_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_67_idx), 4, 0)), 4, "0");
         SubsflControlProps_672( ) ;
         nGXsfl_67_Refreshing = 1;
         Grid1Container.AddObjectProperty("GridName", "Grid1");
         Grid1Container.AddObjectProperty("CmpContext", "");
         Grid1Container.AddObjectProperty("InMasterPage", "false");
         Grid1Container.AddObjectProperty("Class", "Grid");
         Grid1Container.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
         Grid1Container.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
         Grid1Container.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Backcolorstyle), 1, 0, ".", "")));
         Grid1Container.AddObjectProperty("Titleforecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Titleforecolor), 9, 0, ".", "")));
         Grid1Container.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Visible), 5, 0, ".", "")));
         Grid1Container.PageSize = subGrid1_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_672( ) ;
            /* Execute user event: E17AZ2 */
            E17AZ2 ();
            wbEnd = 67;
            WBAZ0( ) ;
         }
         nGXsfl_67_Refreshing = 0;
      }

      protected int subGrid1_Pagecount( )
      {
         return (int)(-1) ;
      }

      protected int subGrid1_Recordcount( )
      {
         return (int)(-1) ;
      }

      protected int subGrid1_Recordsperpage( )
      {
         return (int)(-1) ;
      }

      protected int subGrid1_Currentpage( )
      {
         return (int)(-1) ;
      }

      protected void STRUPAZ0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         edtavCtlfuncaousuario_codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtlfuncaousuario_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlfuncaousuario_codigo_Enabled), 5, 0)));
         GXVvSERVICO_CODIGO_htmlAZ2( ) ;
         GXVvSISTEMA_CODIGO_htmlAZ2( ) ;
         GXVvFUNCAOUSUARIO_CODIGO_htmlAZ2( ) ;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E15AZ2 */
         E15AZ2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         GXVvCONTRATADA_CODIGO_htmlAZ2( AV25WWPContext) ;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "Sdtsolicitacao"), AV12SDTSolicitacao);
            /* Read variables values. */
            dynavContratada_codigo.Name = dynavContratada_codigo_Internalname;
            dynavContratada_codigo.CurrentValue = cgiGet( dynavContratada_codigo_Internalname);
            AV5Contratada_codigo = (int)(NumberUtil.Val( cgiGet( dynavContratada_codigo_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5Contratada_codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV5Contratada_codigo), 6, 0)));
            AV18Solicitacoes_Novo_Projeto = ((StringUtil.StrCmp(cgiGet( chkavSolicitacoes_novo_projeto_Internalname), "1")==0) ? "1" : "0");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Solicitacoes_Novo_Projeto", AV18Solicitacoes_Novo_Projeto);
            dynavServico_codigo.Name = dynavServico_codigo_Internalname;
            dynavServico_codigo.CurrentValue = cgiGet( dynavServico_codigo_Internalname);
            AV14Servico_Codigo = (int)(NumberUtil.Val( cgiGet( dynavServico_codigo_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14Servico_Codigo), 6, 0)));
            dynavSistema_codigo.Name = dynavSistema_codigo_Internalname;
            dynavSistema_codigo.CurrentValue = cgiGet( dynavSistema_codigo_Internalname);
            AV15Sistema_Codigo = (int)(NumberUtil.Val( cgiGet( dynavSistema_codigo_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15Sistema_Codigo), 6, 0)));
            AV16Sistema_Nome = StringUtil.Upper( cgiGet( edtavSistema_nome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16Sistema_Nome", AV16Sistema_Nome);
            AV19Solicitacoes_Objetivo = cgiGet( edtavSolicitacoes_objetivo_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Solicitacoes_Objetivo", AV19Solicitacoes_Objetivo);
            dynavFuncaousuario_codigo.Name = dynavFuncaousuario_codigo_Internalname;
            dynavFuncaousuario_codigo.CurrentValue = cgiGet( dynavFuncaousuario_codigo_Internalname);
            AV8FuncaoUsuario_Codigo = (int)(NumberUtil.Val( cgiGet( dynavFuncaousuario_codigo_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8FuncaoUsuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8FuncaoUsuario_Codigo), 6, 0)));
            AV11Necessidade_Descricao = cgiGet( edtavNecessidade_descricao_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Necessidade_Descricao", AV11Necessidade_Descricao);
            AV10Necessidade_Arquivo = cgiGet( edtavNecessidade_arquivo_Internalname);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavUsername_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavUsername_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vUSERNAME");
               GX_FocusControl = edtavUsername_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV24UserName = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24UserName", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24UserName), 6, 0)));
            }
            else
            {
               AV24UserName = (int)(context.localUtil.CToN( cgiGet( edtavUsername_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24UserName", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24UserName), 6, 0)));
            }
            AV17Solicitacoes_Codigo = (int)(context.localUtil.CToN( cgiGet( edtavSolicitacoes_codigo_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Solicitacoes_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17Solicitacoes_Codigo), 6, 0)));
            /* Read saved values. */
            nRC_GXsfl_67 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_67"), ",", "."));
            nRC_GXsfl_67 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_67"), ",", "."));
            nGXsfl_67_fel_idx = 0;
            while ( nGXsfl_67_fel_idx < nRC_GXsfl_67 )
            {
               nGXsfl_67_fel_idx = (short)(((subGrid1_Islastpage==1)&&(nGXsfl_67_fel_idx+1>subGrid1_Recordsperpage( )) ? 1 : nGXsfl_67_fel_idx+1));
               sGXsfl_67_fel_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_67_fel_idx), 4, 0)), 4, "0");
               SubsflControlProps_fel_672( ) ;
               AV32GXV1 = nGXsfl_67_fel_idx;
               if ( ( AV12SDTSolicitacao.Count >= AV32GXV1 ) && ( AV32GXV1 > 0 ) )
               {
                  AV12SDTSolicitacao.CurrentItem = ((SdtSDTSolicitacao_SDTSolicitacaoItem)AV12SDTSolicitacao.Item(AV32GXV1));
                  AV6Excluir = ((StringUtil.StrCmp(cgiGet( chkavExcluir_Internalname), "S")==0) ? "S" : "N");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( AV10Necessidade_Arquivo)) )
               {
                  GXCCtl = "vNECESSIDADE_ARQUIVO_" + sGXsfl_67_fel_idx;
                  GXCCtlgxBlob = GXCCtl + "_gxBlob";
                  AV10Necessidade_Arquivo = cgiGet( GXCCtlgxBlob);
               }
            }
            if ( nGXsfl_67_fel_idx == 0 )
            {
               nGXsfl_67_idx = 1;
               sGXsfl_67_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_67_idx), 4, 0)), 4, "0");
               SubsflControlProps_672( ) ;
            }
            nGXsfl_67_fel_idx = 1;
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            if ( String.IsNullOrEmpty(StringUtil.RTrim( AV10Necessidade_Arquivo)) )
            {
               GXCCtlgxBlob = "vNECESSIDADE_ARQUIVO" + "_gxBlob";
               AV10Necessidade_Arquivo = cgiGet( GXCCtlgxBlob);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E15AZ2 */
         E15AZ2 ();
         if (returnInSub) return;
      }

      protected void E15AZ2( )
      {
         /* Start Routine */
         dynavSistema_codigo.Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavSistema_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavSistema_codigo.Visible), 5, 0)));
         edtavSistema_nome_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSistema_nome_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSistema_nome_Visible), 5, 0)));
         lblTextblock3_Caption = "Sistema";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblock3_Internalname, "Caption", lblTextblock3_Caption);
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV25WWPContext) ;
         subGrid1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "Grid1ContainerDiv", "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(subGrid1_Visible), 5, 0)));
         AV24UserName = AV25WWPContext.gxTpr_Userid;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24UserName", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24UserName), 6, 0)));
         edtavUsername_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUsername_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUsername_Visible), 5, 0)));
         edtavSolicitacoes_codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSolicitacoes_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSolicitacoes_codigo_Visible), 5, 0)));
         AV28Erro = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28Erro", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28Erro), 4, 0)));
         if ( ! (0==AV17Solicitacoes_Codigo) )
         {
            /* Execute user subroutine: 'SOLICITACOES' */
            S112 ();
            if (returnInSub) return;
         }
      }

      protected void E16AZ2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
      }

      private void E17AZ2( )
      {
         /* Grid1_Load Routine */
         AV32GXV1 = 1;
         while ( AV32GXV1 <= AV12SDTSolicitacao.Count )
         {
            AV12SDTSolicitacao.CurrentItem = ((SdtSDTSolicitacao_SDTSolicitacaoItem)AV12SDTSolicitacao.Item(AV32GXV1));
            /* Load Method */
            if ( wbStart != -1 )
            {
               wbStart = 67;
            }
            sendrow_672( ) ;
            if ( isFullAjaxMode( ) && ( nGXsfl_67_Refreshing == 0 ) )
            {
               context.DoAjaxLoad(67, Grid1Row);
            }
            AV32GXV1 = (short)(AV32GXV1+1);
         }
      }

      public void GXEnter( )
      {
         /* Execute user event: E11AZ2 */
         E11AZ2 ();
         if (returnInSub) return;
      }

      protected void E11AZ2( )
      {
         AV32GXV1 = nGXsfl_67_idx;
         if ( AV12SDTSolicitacao.Count >= AV32GXV1 )
         {
            AV12SDTSolicitacao.CurrentItem = ((SdtSDTSolicitacao_SDTSolicitacaoItem)AV12SDTSolicitacao.Item(AV32GXV1));
         }
         /* Enter Routine */
         /* Execute user subroutine: 'ERROS' */
         S122 ();
         if (returnInSub) return;
         if ( AV28Erro == 0 )
         {
            new pgravasolicitacao(context ).execute(  AV5Contratada_codigo,  AV14Servico_Codigo,  AV15Sistema_Codigo,  AV19Solicitacoes_Objetivo,  AV18Solicitacoes_Novo_Projeto,  AV24UserName,  AV12SDTSolicitacao, out  AV27vrMsg) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5Contratada_codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV5Contratada_codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14Servico_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15Sistema_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Solicitacoes_Objetivo", AV19Solicitacoes_Objetivo);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Solicitacoes_Novo_Projeto", AV18Solicitacoes_Novo_Projeto);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24UserName", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24UserName), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27vrMsg", AV27vrMsg);
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV27vrMsg)) )
         {
            context.setWebReturnParms(new Object[] {(int)AV17Solicitacoes_Codigo});
            context.wjLocDisableFrm = 1;
            context.nUserReturn = 1;
            returnInSub = true;
            if (true) return;
         }
         else
         {
            GX_msglist.addItem(AV27vrMsg);
         }
      }

      protected void E12AZ2( )
      {
         /* 'PDF' Routine */
         new solicitacaoservicopdf(context ).execute( ref  AV17Solicitacoes_Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Solicitacoes_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17Solicitacoes_Codigo), 6, 0)));
      }

      protected void E13AZ2( )
      {
         AV32GXV1 = nGXsfl_67_idx;
         if ( AV12SDTSolicitacao.Count >= AV32GXV1 )
         {
            AV12SDTSolicitacao.CurrentItem = ((SdtSDTSolicitacao_SDTSolicitacaoItem)AV12SDTSolicitacao.Item(AV32GXV1));
         }
         /* 'Incluir' Routine */
         AV27vrMsg = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27vrMsg", AV27vrMsg);
         if ( AV8FuncaoUsuario_Codigo > 0 )
         {
            /* Execute user subroutine: 'BUSCA_NOME_FUNCAO' */
            S132 ();
            if (returnInSub) return;
            AV13SDTSolicitacaoItem.gxTpr_Funcaousuario_codigo = AV8FuncaoUsuario_Codigo;
            AV13SDTSolicitacaoItem.gxTpr_Funcaousuario_nome = AV26FuncaoUsuario_Nome;
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10Necessidade_Arquivo)) )
            {
               AV13SDTSolicitacaoItem.gxTpr_Solicitacoesitens_arquivo = AV10Necessidade_Arquivo;
            }
            AV13SDTSolicitacaoItem.gxTpr_Solicitacoesitens_descricao = AV11Necessidade_Descricao;
            AV12SDTSolicitacao.Add(AV13SDTSolicitacaoItem, 0);
            gx_BV67 = true;
            AV13SDTSolicitacaoItem = new SdtSDTSolicitacao_SDTSolicitacaoItem(context);
            AV11Necessidade_Descricao = "";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Necessidade_Descricao", AV11Necessidade_Descricao);
            AV8FuncaoUsuario_Codigo = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8FuncaoUsuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8FuncaoUsuario_Codigo), 6, 0)));
            AV10Necessidade_Arquivo = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavNecessidade_arquivo_Internalname, "URL", context.PathToRelativeUrl( AV10Necessidade_Arquivo));
         }
         else
         {
            AV27vrMsg = "Campo obrigat�rio Fun��o de Usu�rio.";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27vrMsg", AV27vrMsg);
            GX_msglist.addItem(AV27vrMsg);
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV13SDTSolicitacaoItem", AV13SDTSolicitacaoItem);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV12SDTSolicitacao", AV12SDTSolicitacao);
         nGXsfl_67_bak_idx = nGXsfl_67_idx;
         gxgrGrid1_refresh( ) ;
         nGXsfl_67_idx = nGXsfl_67_bak_idx;
         sGXsfl_67_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_67_idx), 4, 0)), 4, "0");
         SubsflControlProps_672( ) ;
         dynavFuncaousuario_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV8FuncaoUsuario_Codigo), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavFuncaousuario_codigo_Internalname, "Values", dynavFuncaousuario_codigo.ToJavascriptSource());
      }

      protected void E14AZ2( )
      {
         AV32GXV1 = nGXsfl_67_idx;
         if ( AV12SDTSolicitacao.Count >= AV32GXV1 )
         {
            AV12SDTSolicitacao.CurrentItem = ((SdtSDTSolicitacao_SDTSolicitacaoItem)AV12SDTSolicitacao.Item(AV32GXV1));
         }
         /* 'Sair' Routine */
         if ( AV17Solicitacoes_Codigo == 0 )
         {
            if ( AV12SDTSolicitacao.Count > 0 )
            {
               new pgravarascunho(context ).execute( ref  AV5Contratada_codigo, ref  AV14Servico_Codigo, ref  AV15Sistema_Codigo, ref  AV19Solicitacoes_Objetivo, ref  AV18Solicitacoes_Novo_Projeto, ref  AV24UserName, ref  AV12SDTSolicitacao, out  AV27vrMsg) ;
               gx_BV67 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5Contratada_codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV5Contratada_codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14Servico_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15Sistema_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Solicitacoes_Objetivo", AV19Solicitacoes_Objetivo);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Solicitacoes_Novo_Projeto", AV18Solicitacoes_Novo_Projeto);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24UserName", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24UserName), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27vrMsg", AV27vrMsg);
            }
            else
            {
            }
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV27vrMsg)) )
         {
            context.setWebReturnParms(new Object[] {(int)AV17Solicitacoes_Codigo});
            context.wjLocDisableFrm = 1;
            context.nUserReturn = 1;
            returnInSub = true;
            if (true) return;
         }
         else
         {
            GX_msglist.addItem(AV27vrMsg);
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV12SDTSolicitacao", AV12SDTSolicitacao);
         nGXsfl_67_bak_idx = nGXsfl_67_idx;
         gxgrGrid1_refresh( ) ;
         nGXsfl_67_idx = nGXsfl_67_bak_idx;
         sGXsfl_67_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_67_idx), 4, 0)), 4, "0");
         SubsflControlProps_672( ) ;
         dynavSistema_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV15Sistema_Codigo), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavSistema_codigo_Internalname, "Values", dynavSistema_codigo.ToJavascriptSource());
         dynavServico_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14Servico_Codigo), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavServico_codigo_Internalname, "Values", dynavServico_codigo.ToJavascriptSource());
         dynavContratada_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV5Contratada_codigo), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContratada_codigo_Internalname, "Values", dynavContratada_codigo.ToJavascriptSource());
      }

      protected void E18AZ2( )
      {
         AV32GXV1 = nGXsfl_67_idx;
         if ( AV12SDTSolicitacao.Count >= AV32GXV1 )
         {
            AV12SDTSolicitacao.CurrentItem = ((SdtSDTSolicitacao_SDTSolicitacaoItem)AV12SDTSolicitacao.Item(AV32GXV1));
         }
         /* Excluir_Click Routine */
         AV29Idx = 0;
         /* Start For Each Line */
         nRC_GXsfl_67 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_67"), ",", "."));
         nGXsfl_67_fel_idx = 0;
         while ( nGXsfl_67_fel_idx < nRC_GXsfl_67 )
         {
            nGXsfl_67_fel_idx = (short)(((subGrid1_Islastpage==1)&&(nGXsfl_67_fel_idx+1>subGrid1_Recordsperpage( )) ? 1 : nGXsfl_67_fel_idx+1));
            sGXsfl_67_fel_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_67_fel_idx), 4, 0)), 4, "0");
            SubsflControlProps_fel_672( ) ;
            AV32GXV1 = nGXsfl_67_fel_idx;
            if ( ( AV12SDTSolicitacao.Count >= AV32GXV1 ) && ( AV32GXV1 > 0 ) )
            {
               AV12SDTSolicitacao.CurrentItem = ((SdtSDTSolicitacao_SDTSolicitacaoItem)AV12SDTSolicitacao.Item(AV32GXV1));
               AV6Excluir = ((StringUtil.StrCmp(cgiGet( chkavExcluir_Internalname), "S")==0) ? "S" : "N");
            }
            if ( String.IsNullOrEmpty(StringUtil.RTrim( AV10Necessidade_Arquivo)) )
            {
               GXCCtl = "vNECESSIDADE_ARQUIVO_" + sGXsfl_67_fel_idx;
               GXCCtlgxBlob = GXCCtl + "_gxBlob";
               AV10Necessidade_Arquivo = cgiGet( GXCCtlgxBlob);
            }
            if ( StringUtil.StrCmp(AV6Excluir, "S") == 0 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            AV29Idx = (short)(AV29Idx+1);
            /* End For Each Line */
         }
         if ( nGXsfl_67_fel_idx == 0 )
         {
            nGXsfl_67_idx = 1;
            sGXsfl_67_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_67_idx), 4, 0)), 4, "0");
            SubsflControlProps_672( ) ;
         }
         nGXsfl_67_fel_idx = 1;
         AV36GXV2 = 1;
         while ( AV36GXV2 <= AV12SDTSolicitacao.Count )
         {
            AV13SDTSolicitacaoItem = ((SdtSDTSolicitacao_SDTSolicitacaoItem)AV12SDTSolicitacao.Item(AV36GXV2));
            AV12SDTSolicitacao.RemoveItem(AV29Idx);
            gx_BV67 = true;
            AV36GXV2 = (int)(AV36GXV2+1);
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV13SDTSolicitacaoItem", AV13SDTSolicitacaoItem);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV12SDTSolicitacao", AV12SDTSolicitacao);
         nGXsfl_67_bak_idx = nGXsfl_67_idx;
         gxgrGrid1_refresh( ) ;
         nGXsfl_67_idx = nGXsfl_67_bak_idx;
         sGXsfl_67_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_67_idx), 4, 0)), 4, "0");
         SubsflControlProps_672( ) ;
      }

      protected void S112( )
      {
         /* 'SOLICITACOES' Routine */
         chkavSolicitacoes_novo_projeto.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavSolicitacoes_novo_projeto_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavSolicitacoes_novo_projeto.Visible), 5, 0)));
         /* Using cursor H00AZ6 */
         pr_default.execute(4, new Object[] {AV17Solicitacoes_Codigo});
         while ( (pr_default.getStatus(4) != 101) )
         {
            A52Contratada_AreaTrabalhoCod = H00AZ6_A52Contratada_AreaTrabalhoCod[0];
            A439Solicitacoes_Codigo = H00AZ6_A439Solicitacoes_Codigo[0];
            A441Solicitacoes_Objetivo = H00AZ6_A441Solicitacoes_Objetivo[0];
            n441Solicitacoes_Objetivo = H00AZ6_n441Solicitacoes_Objetivo[0];
            A127Sistema_Codigo = H00AZ6_A127Sistema_Codigo[0];
            A155Servico_Codigo = H00AZ6_A155Servico_Codigo[0];
            A39Contratada_Codigo = H00AZ6_A39Contratada_Codigo[0];
            A52Contratada_AreaTrabalhoCod = H00AZ6_A52Contratada_AreaTrabalhoCod[0];
            AV19Solicitacoes_Objetivo = A441Solicitacoes_Objetivo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Solicitacoes_Objetivo", AV19Solicitacoes_Objetivo);
            AV15Sistema_Codigo = A127Sistema_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15Sistema_Codigo), 6, 0)));
            AV14Servico_Codigo = A155Servico_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14Servico_Codigo), 6, 0)));
            AV5Contratada_codigo = A39Contratada_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5Contratada_codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV5Contratada_codigo), 6, 0)));
            /* Using cursor H00AZ7 */
            pr_default.execute(5, new Object[] {AV17Solicitacoes_Codigo});
            while ( (pr_default.getStatus(5) != 101) )
            {
               A439Solicitacoes_Codigo = H00AZ7_A439Solicitacoes_Codigo[0];
               A161FuncaoUsuario_Codigo = H00AZ7_A161FuncaoUsuario_Codigo[0];
               A448SolicitacoesItens_descricao = H00AZ7_A448SolicitacoesItens_descricao[0];
               n448SolicitacoesItens_descricao = H00AZ7_n448SolicitacoesItens_descricao[0];
               A449SolicitacoesItens_Arquivo = H00AZ7_A449SolicitacoesItens_Arquivo[0];
               n449SolicitacoesItens_Arquivo = H00AZ7_n449SolicitacoesItens_Arquivo[0];
               AV8FuncaoUsuario_Codigo = A161FuncaoUsuario_Codigo;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8FuncaoUsuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8FuncaoUsuario_Codigo), 6, 0)));
               /* Execute user subroutine: 'BUSCA_NOME_FUNCAO' */
               S132 ();
               if ( returnInSub )
               {
                  pr_default.close(5);
                  returnInSub = true;
                  if (true) return;
               }
               AV13SDTSolicitacaoItem.gxTpr_Funcaousuario_codigo = AV8FuncaoUsuario_Codigo;
               AV13SDTSolicitacaoItem.gxTpr_Funcaousuario_nome = AV26FuncaoUsuario_Nome;
               AV13SDTSolicitacaoItem.gxTpr_Solicitacoesitens_arquivo = A449SolicitacoesItens_Arquivo;
               AV13SDTSolicitacaoItem.gxTpr_Solicitacoesitens_descricao = A448SolicitacoesItens_descricao;
               AV12SDTSolicitacao.Add(AV13SDTSolicitacaoItem, 0);
               gx_BV67 = true;
               AV13SDTSolicitacaoItem = new SdtSDTSolicitacao_SDTSolicitacaoItem(context);
               pr_default.readNext(5);
            }
            pr_default.close(5);
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(4);
      }

      protected void S132( )
      {
         /* 'BUSCA_NOME_FUNCAO' Routine */
         /* Using cursor H00AZ8 */
         pr_default.execute(6, new Object[] {AV8FuncaoUsuario_Codigo});
         while ( (pr_default.getStatus(6) != 101) )
         {
            A161FuncaoUsuario_Codigo = H00AZ8_A161FuncaoUsuario_Codigo[0];
            A162FuncaoUsuario_Nome = H00AZ8_A162FuncaoUsuario_Nome[0];
            AV26FuncaoUsuario_Nome = A162FuncaoUsuario_Nome;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26FuncaoUsuario_Nome", AV26FuncaoUsuario_Nome);
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(6);
      }

      protected void S122( )
      {
         /* 'ERROS' Routine */
         AV28Erro = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28Erro", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28Erro), 4, 0)));
         AV27vrMsg = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27vrMsg", AV27vrMsg);
         if ( AV5Contratada_codigo == 0 )
         {
            AV28Erro = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28Erro", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28Erro), 4, 0)));
            AV27vrMsg = "Campo obrigat�rio Contratada.";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27vrMsg", AV27vrMsg);
         }
         else
         {
            if ( ( AV15Sistema_Codigo == 0 ) && ( StringUtil.StrCmp(AV18Solicitacoes_Novo_Projeto, "N") == 0 ) )
            {
               AV28Erro = 1;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28Erro", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28Erro), 4, 0)));
               AV27vrMsg = "Campo obrigat�rio Sistema.";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27vrMsg", AV27vrMsg);
            }
            else
            {
               if ( AV14Servico_Codigo == 0 )
               {
                  AV28Erro = 1;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28Erro", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28Erro), 4, 0)));
                  AV27vrMsg = "Campo obrigat�rio Servi�o.";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27vrMsg", AV27vrMsg);
               }
            }
         }
      }

      protected void wb_table1_2_AZ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " height: " + StringUtil.LTrim( StringUtil.Str( (decimal)(178), 10, 0)) + "px" + ";";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(617), 10, 0)) + "px" + ";";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "center", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\" colspan=\"3\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\">") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "width:66px")+"\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock1_Internalname, "Contratada", "", "", lblTextblock1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_SolicitacaodeServico.htm");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock7_Internalname, "*", "", "", lblTextblock7_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "color:#FF0000;", "TextBlock", 0, "", 1, 1, 0, "HLP_SolicitacaodeServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "width:729px")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'',false,'" + sGXsfl_67_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavContratada_codigo, dynavContratada_codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV5Contratada_codigo), 6, 0)), 1, dynavContratada_codigo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,11);\"", "", true, "HLP_SolicitacaodeServico.htm");
            dynavContratada_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV5Contratada_codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContratada_codigo_Internalname, "Values", (String)(dynavContratada_codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "width:57px")+"\">") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 13,'',false,'" + sGXsfl_67_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavSolicitacoes_novo_projeto_Internalname, AV18Solicitacoes_Novo_Projeto, "", "", chkavSolicitacoes_novo_projeto.Visible, 1, "1", "Novo Projeto", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(13, this, '1', '0');gx.ajax.executeCliEvent('e19az1_client',this, event);gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,13);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock2_Internalname, "Servi�o", "", "", lblTextblock2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_SolicitacaodeServico.htm");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock8_Internalname, "*", "", "", lblTextblock8_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "color:#FF0000;", "TextBlock", 0, "", 1, 1, 0, "HLP_SolicitacaodeServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'',false,'" + sGXsfl_67_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavServico_codigo, dynavServico_codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV14Servico_Codigo), 6, 0)), 1, dynavServico_codigo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,19);\"", "", true, "HLP_SolicitacaodeServico.htm");
            dynavServico_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14Servico_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavServico_codigo_Internalname, "Values", (String)(dynavServico_codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock3_Internalname, lblTextblock3_Caption, "", "", lblTextblock3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_SolicitacaodeServico.htm");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock9_Internalname, "*", "", "", lblTextblock9_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "color:#FF0000;", "TextBlock", 0, "", 1, 1, 0, "HLP_SolicitacaodeServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 26,'',false,'" + sGXsfl_67_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavSistema_codigo, dynavSistema_codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV15Sistema_Codigo), 6, 0)), 1, dynavSistema_codigo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", dynavSistema_codigo.Visible, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,26);\"", "", true, "HLP_SolicitacaodeServico.htm");
            dynavSistema_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV15Sistema_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavSistema_codigo_Internalname, "Values", (String)(dynavSistema_codigo.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 27,'',false,'" + sGXsfl_67_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavSistema_nome_Internalname, StringUtil.RTrim( AV16Sistema_Nome), StringUtil.RTrim( context.localUtil.Format( AV16Sistema_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,27);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavSistema_nome_Jsonclick, 0, "Attribute", "", "", "", edtavSistema_nome_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_SolicitacaodeServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"3\" >") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock4_Internalname, "Objetivo Geral da Solicita��o", "", "", lblTextblock4_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_SolicitacaodeServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"3\" >") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 34,'',false,'" + sGXsfl_67_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavSolicitacoes_objetivo_Internalname, AV19Solicitacoes_Objetivo, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,34);\"", 0, 1, 1, 0, 80, "chr", 10, "row", StyleString, ClassString, "", "2097152", -1, "", "", -1, true, "", "HLP_SolicitacaodeServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock5_Internalname, "Fun��o de Usu�rio", "", "", lblTextblock5_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_SolicitacaodeServico.htm");
            context.WriteHtmlText( "&nbsp; ") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock10_Internalname, "*", "", "", lblTextblock10_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "color:#FF0000;", "TextBlock", 0, "", 1, 1, 0, "HLP_SolicitacaodeServico.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'" + sGXsfl_67_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavFuncaousuario_codigo, dynavFuncaousuario_codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV8FuncaoUsuario_Codigo), 6, 0)), 1, dynavFuncaousuario_codigo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,39);\"", "", true, "HLP_SolicitacaodeServico.htm");
            dynavFuncaousuario_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV8FuncaoUsuario_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavFuncaousuario_codigo_Internalname, "Values", (String)(dynavFuncaousuario_codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"3\" >") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock6_Internalname, "Necessidade", "", "", lblTextblock6_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_SolicitacaodeServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"3\" >") ;
            wb_table2_46_AZ2( true) ;
         }
         else
         {
            wb_table2_46_AZ2( false) ;
         }
         return  ;
      }

      protected void wb_table2_46_AZ2e( bool wbgen )
      {
         if ( wbgen )
         {
            wb_table3_53_AZ2( true) ;
         }
         else
         {
            wb_table3_53_AZ2( false) ;
         }
         return  ;
      }

      protected void wb_table3_53_AZ2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"3\" >") ;
            /*  Grid Control  */
            Grid1Container.SetWrapped(nGXWrapped);
            if ( Grid1Container.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"Grid1Container"+"DivS\" data-gxgridid=\"67\">") ;
               sStyleString = "";
               if ( subGrid1_Visible == 0 )
               {
                  sStyleString = sStyleString + "display:none;";
               }
               GxWebStd.gx_table_start( context, subGrid1_Internalname, subGrid1_Internalname, "", "Grid", 0, "", "", 1, 2, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid1_Backcolorstyle == 0 )
               {
                  subGrid1_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid1_Class) > 0 )
                  {
                     subGrid1_Linesclass = subGrid1_Class+"Title";
                  }
               }
               else
               {
                  subGrid1_Titlebackstyle = 1;
                  if ( subGrid1_Backcolorstyle == 1 )
                  {
                     subGrid1_Titlebackcolor = subGrid1_Allbackcolor;
                     if ( StringUtil.Len( subGrid1_Class) > 0 )
                     {
                        subGrid1_Linesclass = subGrid1_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid1_Class) > 0 )
                     {
                        subGrid1_Linesclass = subGrid1_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid1_Linesclass+"\" "+" style=\""+""+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid1_Linesclass+"\" "+" style=\""+"display:none;"+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "Fun��o de Usu�rio") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(410), 4, 0))+"px"+" class=\""+subGrid1_Linesclass+"\" "+" style=\""+""+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "Fun��o de Usu�rio") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               Grid1Container.AddObjectProperty("GridName", "Grid1");
            }
            else
            {
               Grid1Container.AddObjectProperty("GridName", "Grid1");
               Grid1Container.AddObjectProperty("Class", "Grid");
               Grid1Container.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
               Grid1Container.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
               Grid1Container.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Backcolorstyle), 1, 0, ".", "")));
               Grid1Container.AddObjectProperty("Titleforecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Titleforecolor), 9, 0, ".", "")));
               Grid1Container.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Visible), 5, 0, ".", "")));
               Grid1Container.AddObjectProperty("CmpContext", "");
               Grid1Container.AddObjectProperty("InMasterPage", "false");
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Column.AddObjectProperty("Value", StringUtil.RTrim( AV6Excluir));
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Column.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavCtlfuncaousuario_codigo_Enabled), 5, 0, ".", "")));
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Container.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Allowselection), 1, 0, ".", "")));
               Grid1Container.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Selectioncolor), 9, 0, ".", "")));
               Grid1Container.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Allowhovering), 1, 0, ".", "")));
               Grid1Container.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Hoveringcolor), 9, 0, ".", "")));
               Grid1Container.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Allowcollapsing), 1, 0, ".", "")));
               Grid1Container.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 67 )
         {
            wbEnd = 0;
            nRC_GXsfl_67 = (short)(nGXsfl_67_idx-1);
            if ( Grid1Container.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               AV32GXV1 = nGXsfl_67_idx;
               if ( subGrid1_Visible != 0 )
               {
                  sStyleString = "";
               }
               else
               {
                  sStyleString = " style=\"display:none;\"";
               }
               context.WriteHtmlText( "<div id=\""+"Grid1Container"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid1", Grid1Container);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "Grid1ContainerData", Grid1Container.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "Grid1ContainerData"+"V", Grid1Container.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"Grid1ContainerData"+"V"+"\" value='"+Grid1Container.GridValuesHidden()+"'/>") ;
               }
            }
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 73,'',false,'" + sGXsfl_67_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavUsername_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV24UserName), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV24UserName), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,73);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavUsername_Jsonclick, 0, "Attribute", "", "", "", edtavUsername_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_SolicitacaodeServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_AZ2e( true) ;
         }
         else
         {
            wb_table1_2_AZ2e( false) ;
         }
      }

      protected void wb_table3_53_AZ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " height: " + StringUtil.LTrim( StringUtil.Str( (decimal)(18), 10, 0)) + "px" + ";";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(546), 10, 0)) + "px" + ";";
            GxWebStd.gx_table_start( context, tblTable2_Internalname, tblTable2_Internalname, "", "Table", 0, "right", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 57,'',false,'',0)\"";
            ClassString = "Button";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttButton2_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(67), 2, 0)+","+"null"+");", "Incluir Necessidade", bttButton2_Jsonclick, 5, "Incluir Necessidade", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'INCLUIR\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_SolicitacaodeServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'',0)\"";
            ClassString = "Button";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttButton3_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(67), 2, 0)+","+"null"+");", "Salvar SS", bttButton3_Jsonclick, 5, "Salvar SS", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_SolicitacaodeServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 61,'',false,'',0)\"";
            ClassString = "Button";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttButton4_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(67), 2, 0)+","+"null"+");", "Gerar PDF", bttButton4_Jsonclick, 5, "Gerar PDF", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'PDF\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_SolicitacaodeServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 63,'',false,'',0)\"";
            ClassString = "Button";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttButton5_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(67), 2, 0)+","+"null"+");", "  Sair  ", bttButton5_Jsonclick, 5, "  Sair  ", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'SAIR\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_SolicitacaodeServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_53_AZ2e( true) ;
         }
         else
         {
            wb_table3_53_AZ2e( false) ;
         }
      }

      protected void wb_table2_46_AZ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable3_Internalname, tblTable3_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "width:509px")+"\">") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'" + sGXsfl_67_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavNecessidade_descricao_Internalname, AV11Necessidade_Descricao, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,49);\"", 0, 1, 1, 0, 80, "chr", 10, "row", StyleString, ClassString, "", "2097152", -1, "", "", -1, true, "", "HLP_SolicitacaodeServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "Image";
            StyleString = "";
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 52,'',false,'" + sGXsfl_67_idx + "',0)\"";
            edtavNecessidade_arquivo_Filetype = "tmp";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavNecessidade_arquivo_Internalname, "Filetype", edtavNecessidade_arquivo_Filetype);
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10Necessidade_Arquivo)) )
            {
               gxblobfileaux.Source = AV10Necessidade_Arquivo;
               if ( ! gxblobfileaux.HasExtension() || ( StringUtil.StrCmp(edtavNecessidade_arquivo_Filetype, "tmp") != 0 ) )
               {
                  gxblobfileaux.SetExtension(StringUtil.Trim( edtavNecessidade_arquivo_Filetype));
               }
               if ( gxblobfileaux.ErrCode == 0 )
               {
                  AV10Necessidade_Arquivo = gxblobfileaux.GetAbsoluteName();
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavNecessidade_arquivo_Internalname, "URL", context.PathToRelativeUrl( AV10Necessidade_Arquivo));
                  edtavNecessidade_arquivo_Filetype = gxblobfileaux.GetExtension();
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavNecessidade_arquivo_Internalname, "Filetype", edtavNecessidade_arquivo_Filetype);
               }
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavNecessidade_arquivo_Internalname, "URL", context.PathToRelativeUrl( AV10Necessidade_Arquivo));
            }
            GxWebStd.gx_blob_field( context, edtavNecessidade_arquivo_Internalname, StringUtil.RTrim( AV10Necessidade_Arquivo), context.PathToRelativeUrl( AV10Necessidade_Arquivo), (String.IsNullOrEmpty(StringUtil.RTrim( edtavNecessidade_arquivo_Contenttype)) ? context.GetContentType( (String.IsNullOrEmpty(StringUtil.RTrim( edtavNecessidade_arquivo_Filetype)) ? AV10Necessidade_Arquivo : edtavNecessidade_arquivo_Filetype)) : edtavNecessidade_arquivo_Contenttype), false, "", edtavNecessidade_arquivo_Parameters, 1, 1, 1, "", "", 0, -1, 250, "px", 60, "px", 0, 0, 0, edtavNecessidade_arquivo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", StyleString, ClassString, "", ""+TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,52);\"", "", "", "HLP_SolicitacaodeServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_46_AZ2e( true) ;
         }
         else
         {
            wb_table2_46_AZ2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV17Solicitacoes_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Solicitacoes_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17Solicitacoes_Codigo), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAAZ2( ) ;
         WSAZ2( ) ;
         WEAZ2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20205299353058");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("solicitacaodeservico.js", "?20205299353058");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_672( )
      {
         chkavExcluir_Internalname = "vEXCLUIR_"+sGXsfl_67_idx;
         edtavCtlfuncaousuario_codigo_Internalname = "CTLFUNCAOUSUARIO_CODIGO_"+sGXsfl_67_idx;
         edtavCtlfuncaousuario_nome_Internalname = "CTLFUNCAOUSUARIO_NOME_"+sGXsfl_67_idx;
      }

      protected void SubsflControlProps_fel_672( )
      {
         chkavExcluir_Internalname = "vEXCLUIR_"+sGXsfl_67_fel_idx;
         edtavCtlfuncaousuario_codigo_Internalname = "CTLFUNCAOUSUARIO_CODIGO_"+sGXsfl_67_fel_idx;
         edtavCtlfuncaousuario_nome_Internalname = "CTLFUNCAOUSUARIO_NOME_"+sGXsfl_67_fel_idx;
      }

      protected void sendrow_672( )
      {
         SubsflControlProps_672( ) ;
         WBAZ0( ) ;
         Grid1Row = GXWebRow.GetNew(context,Grid1Container);
         if ( subGrid1_Backcolorstyle == 0 )
         {
            /* None style subfile background logic. */
            subGrid1_Backstyle = 0;
            if ( StringUtil.StrCmp(subGrid1_Class, "") != 0 )
            {
               subGrid1_Linesclass = subGrid1_Class+"Odd";
            }
         }
         else if ( subGrid1_Backcolorstyle == 1 )
         {
            /* Uniform style subfile background logic. */
            subGrid1_Backstyle = 0;
            subGrid1_Backcolor = subGrid1_Allbackcolor;
            if ( StringUtil.StrCmp(subGrid1_Class, "") != 0 )
            {
               subGrid1_Linesclass = subGrid1_Class+"Uniform";
            }
         }
         else if ( subGrid1_Backcolorstyle == 2 )
         {
            /* Header style subfile background logic. */
            subGrid1_Backstyle = 1;
            if ( StringUtil.StrCmp(subGrid1_Class, "") != 0 )
            {
               subGrid1_Linesclass = subGrid1_Class+"Odd";
            }
            subGrid1_Backcolor = (int)(0x0);
         }
         else if ( subGrid1_Backcolorstyle == 3 )
         {
            /* Report style subfile background logic. */
            subGrid1_Backstyle = 1;
            if ( ((int)((nGXsfl_67_idx) % (2))) == 0 )
            {
               subGrid1_Backcolor = (int)(0x0);
               if ( StringUtil.StrCmp(subGrid1_Class, "") != 0 )
               {
                  subGrid1_Linesclass = subGrid1_Class+"Even";
               }
            }
            else
            {
               subGrid1_Backcolor = (int)(0x0);
               if ( StringUtil.StrCmp(subGrid1_Class, "") != 0 )
               {
                  subGrid1_Linesclass = subGrid1_Class+"Odd";
               }
            }
         }
         if ( Grid1Container.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<tr ") ;
            context.WriteHtmlText( " class=\""+subGrid1_Linesclass+"\" style=\""+""+"\"") ;
            context.WriteHtmlText( " gxrow=\""+sGXsfl_67_idx+"\">") ;
         }
         /* Subfile cell */
         if ( Grid1Container.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
         }
         /* Check box */
         TempTags = " " + ((chkavExcluir.Enabled!=0)&&(chkavExcluir.Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 68,'',false,'"+sGXsfl_67_idx+"',67)\"" : " ");
         ClassString = "Attribute";
         StyleString = "";
         Grid1Row.AddColumnProperties("checkbox", 1, isAjaxCallMode( ), new Object[] {(String)chkavExcluir_Internalname,(String)AV6Excluir,(String)"",(String)"",(short)-1,(short)1,(String)"S",(String)"",(String)StyleString,(String)ClassString,(String)"",TempTags+((chkavExcluir.Enabled!=0)&&(chkavExcluir.Visible!=0) ? " onclick=\"gx.fn.checkboxClick(68, this, 'S', 'N');gx.ajax.executeCliEvent('e18az2_client',this, event);gx.evt.onchange(this);\" " : " ")+((chkavExcluir.Enabled!=0)&&(chkavExcluir.Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,68);\"" : " ")});
         /* Subfile cell */
         if ( Grid1Container.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute";
         Grid1Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavCtlfuncaousuario_codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(((SdtSDTSolicitacao_SDTSolicitacaoItem)AV12SDTSolicitacao.Item(AV32GXV1)).gxTpr_Funcaousuario_codigo), 6, 0, ",", "")),((edtavCtlfuncaousuario_codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(((SdtSDTSolicitacao_SDTSolicitacaoItem)AV12SDTSolicitacao.Item(AV32GXV1)).gxTpr_Funcaousuario_codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(((SdtSDTSolicitacao_SDTSolicitacaoItem)AV12SDTSolicitacao.Item(AV32GXV1)).gxTpr_Funcaousuario_codigo), "ZZZZZ9")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavCtlfuncaousuario_codigo_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)0,(int)edtavCtlfuncaousuario_codigo_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)67,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
         /* Subfile cell */
         if ( Grid1Container.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         TempTags = " " + ((edtavCtlfuncaousuario_nome_Enabled!=0)&&(edtavCtlfuncaousuario_nome_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 70,'',false,'"+sGXsfl_67_idx+"',67)\"" : " ");
         ROClassString = "Attribute";
         Grid1Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavCtlfuncaousuario_nome_Internalname,((SdtSDTSolicitacao_SDTSolicitacaoItem)AV12SDTSolicitacao.Item(AV32GXV1)).gxTpr_Funcaousuario_nome,(String)"",TempTags+((edtavCtlfuncaousuario_nome_Enabled!=0)&&(edtavCtlfuncaousuario_nome_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavCtlfuncaousuario_nome_Enabled!=0)&&(edtavCtlfuncaousuario_nome_Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,70);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavCtlfuncaousuario_nome_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)1,(short)0,(String)"text",(String)"",(short)410,(String)"px",(short)17,(String)"px",(short)200,(short)0,(short)0,(short)67,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
         GXCCtl = "vNECESSIDADE_ARQUIVO_" + sGXsfl_67_idx;
         GXCCtlgxBlob = GXCCtl + "_gxBlob";
         GxWebStd.gx_hidden_field( context, GXCCtlgxBlob, AV10Necessidade_Arquivo);
         Grid1Container.AddRow(Grid1Row);
         nGXsfl_67_idx = (short)(((subGrid1_Islastpage==1)&&(nGXsfl_67_idx+1>subGrid1_Recordsperpage( )) ? 1 : nGXsfl_67_idx+1));
         sGXsfl_67_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_67_idx), 4, 0)), 4, "0");
         SubsflControlProps_672( ) ;
         /* End function sendrow_672 */
      }

      protected void init_default_properties( )
      {
         lblTextblock1_Internalname = "TEXTBLOCK1";
         lblTextblock7_Internalname = "TEXTBLOCK7";
         dynavContratada_codigo_Internalname = "vCONTRATADA_CODIGO";
         chkavSolicitacoes_novo_projeto_Internalname = "vSOLICITACOES_NOVO_PROJETO";
         lblTextblock2_Internalname = "TEXTBLOCK2";
         lblTextblock8_Internalname = "TEXTBLOCK8";
         dynavServico_codigo_Internalname = "vSERVICO_CODIGO";
         lblTextblock3_Internalname = "TEXTBLOCK3";
         lblTextblock9_Internalname = "TEXTBLOCK9";
         dynavSistema_codigo_Internalname = "vSISTEMA_CODIGO";
         edtavSistema_nome_Internalname = "vSISTEMA_NOME";
         lblTextblock4_Internalname = "TEXTBLOCK4";
         edtavSolicitacoes_objetivo_Internalname = "vSOLICITACOES_OBJETIVO";
         lblTextblock5_Internalname = "TEXTBLOCK5";
         lblTextblock10_Internalname = "TEXTBLOCK10";
         dynavFuncaousuario_codigo_Internalname = "vFUNCAOUSUARIO_CODIGO";
         lblTextblock6_Internalname = "TEXTBLOCK6";
         edtavNecessidade_descricao_Internalname = "vNECESSIDADE_DESCRICAO";
         edtavNecessidade_arquivo_Internalname = "vNECESSIDADE_ARQUIVO";
         tblTable3_Internalname = "TABLE3";
         bttButton2_Internalname = "BUTTON2";
         bttButton3_Internalname = "BUTTON3";
         bttButton4_Internalname = "BUTTON4";
         bttButton5_Internalname = "BUTTON5";
         tblTable2_Internalname = "TABLE2";
         chkavExcluir_Internalname = "vEXCLUIR";
         edtavCtlfuncaousuario_codigo_Internalname = "CTLFUNCAOUSUARIO_CODIGO";
         edtavCtlfuncaousuario_nome_Internalname = "CTLFUNCAOUSUARIO_NOME";
         edtavUsername_Internalname = "vUSERNAME";
         tblTablecontent_Internalname = "TABLECONTENT";
         edtavSolicitacoes_codigo_Internalname = "vSOLICITACOES_CODIGO";
         Form.Internalname = "FORM";
         subGrid1_Internalname = "GRID1";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtavCtlfuncaousuario_nome_Jsonclick = "";
         edtavCtlfuncaousuario_nome_Visible = -1;
         edtavCtlfuncaousuario_nome_Enabled = 1;
         edtavCtlfuncaousuario_codigo_Jsonclick = "";
         chkavExcluir.Visible = -1;
         chkavExcluir.Enabled = 1;
         edtavNecessidade_arquivo_Jsonclick = "";
         edtavNecessidade_arquivo_Parameters = "";
         edtavNecessidade_arquivo_Contenttype = "";
         edtavNecessidade_arquivo_Filetype = "";
         edtavUsername_Jsonclick = "";
         subGrid1_Allowcollapsing = 0;
         subGrid1_Allowselection = 0;
         edtavCtlfuncaousuario_codigo_Enabled = 0;
         subGrid1_Class = "Grid";
         subGrid1_Visible = 1;
         dynavFuncaousuario_codigo_Jsonclick = "";
         edtavSistema_nome_Jsonclick = "";
         dynavSistema_codigo_Jsonclick = "";
         dynavServico_codigo_Jsonclick = "";
         dynavContratada_codigo_Jsonclick = "";
         chkavSolicitacoes_novo_projeto.Visible = 1;
         edtavUsername_Visible = 1;
         lblTextblock3_Caption = "Sistema";
         edtavSistema_nome_Visible = 1;
         dynavSistema_codigo.Visible = 1;
         subGrid1_Titleforecolor = (int)(0x000000);
         subGrid1_Backcolorstyle = 0;
         edtavCtlfuncaousuario_codigo_Enabled = -1;
         chkavExcluir.Caption = "";
         chkavSolicitacoes_novo_projeto.Caption = "";
         edtavSolicitacoes_codigo_Jsonclick = "";
         edtavSolicitacoes_codigo_Visible = 1;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Solicitacao de Servico";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      /* * Property Necessidade_descricao not supported in */
      /* * Property Necessidade_arquivo not supported in */
      /* * Property Necessidade_descricao not supported in */
      /* * Property Necessidade_arquivo not supported in */
      /* * Property Necessidade_descricao not supported in */
      /* * Property Necessidade_arquivo not supported in */
      /* * Property Necessidade_descricao not supported in */
      /* * Property Necessidade_arquivo not supported in */
      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID1_nFirstRecordOnPage',nv:0},{av:'GRID1_nEOF',nv:0},{av:'AV12SDTSolicitacao',fld:'vSDTSOLICITACAO',pic:'',nv:null}],oparms:[]}");
         setEventMetadata("GRID1.LOAD","{handler:'E17AZ2',iparms:[],oparms:[]}");
         setEventMetadata("ENTER","{handler:'E11AZ2',iparms:[{av:'AV28Erro',fld:'vERRO',pic:'ZZZ9',nv:0},{av:'AV5Contratada_codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV14Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV15Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV19Solicitacoes_Objetivo',fld:'vSOLICITACOES_OBJETIVO',pic:'',nv:''},{av:'AV18Solicitacoes_Novo_Projeto',fld:'vSOLICITACOES_NOVO_PROJETO',pic:'',nv:''},{av:'AV24UserName',fld:'vUSERNAME',pic:'ZZZZZ9',nv:0},{av:'AV12SDTSolicitacao',fld:'vSDTSOLICITACAO',pic:'',nv:null},{av:'AV27vrMsg',fld:'vVRMSG',pic:'',nv:''},{av:'AV17Solicitacoes_Codigo',fld:'vSOLICITACOES_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV27vrMsg',fld:'vVRMSG',pic:'',nv:''},{av:'AV28Erro',fld:'vERRO',pic:'ZZZ9',nv:0}]}");
         setEventMetadata("'PDF'","{handler:'E12AZ2',iparms:[{av:'AV17Solicitacoes_Codigo',fld:'vSOLICITACOES_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV17Solicitacoes_Codigo',fld:'vSOLICITACOES_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("'INCLUIR'","{handler:'E13AZ2',iparms:[{av:'AV27vrMsg',fld:'vVRMSG',pic:'',nv:''},{av:'AV8FuncaoUsuario_Codigo',fld:'vFUNCAOUSUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV13SDTSolicitacaoItem',fld:'vSDTSOLICITACAOITEM',pic:'',nv:null},{av:'AV26FuncaoUsuario_Nome',fld:'vFUNCAOUSUARIO_NOME',pic:'',nv:''},{av:'AV10Necessidade_Arquivo',fld:'vNECESSIDADE_ARQUIVO',pic:'',nv:''},{av:'AV11Necessidade_Descricao',fld:'vNECESSIDADE_DESCRICAO',pic:'',nv:''},{av:'AV12SDTSolicitacao',fld:'vSDTSOLICITACAO',pic:'',nv:null},{av:'A161FuncaoUsuario_Codigo',fld:'FUNCAOUSUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A162FuncaoUsuario_Nome',fld:'FUNCAOUSUARIO_NOME',pic:'',nv:''},{av:'GRID1_nFirstRecordOnPage',nv:0},{av:'GRID1_nEOF',nv:0}],oparms:[{av:'AV27vrMsg',fld:'vVRMSG',pic:'',nv:''},{av:'AV13SDTSolicitacaoItem',fld:'vSDTSOLICITACAOITEM',pic:'',nv:null},{av:'AV12SDTSolicitacao',fld:'vSDTSOLICITACAO',pic:'',nv:null},{av:'AV11Necessidade_Descricao',fld:'vNECESSIDADE_DESCRICAO',pic:'',nv:''},{av:'AV8FuncaoUsuario_Codigo',fld:'vFUNCAOUSUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV10Necessidade_Arquivo',fld:'vNECESSIDADE_ARQUIVO',pic:'',nv:''},{av:'AV26FuncaoUsuario_Nome',fld:'vFUNCAOUSUARIO_NOME',pic:'',nv:''}]}");
         setEventMetadata("VSOLICITACOES_NOVO_PROJETO.CLICK","{handler:'E19AZ1',iparms:[{av:'AV18Solicitacoes_Novo_Projeto',fld:'vSOLICITACOES_NOVO_PROJETO',pic:'',nv:''}],oparms:[{av:'dynavSistema_codigo'},{av:'edtavSistema_nome_Visible',ctrl:'vSISTEMA_NOME',prop:'Visible'},{av:'lblTextblock3_Caption',ctrl:'TEXTBLOCK3',prop:'Caption'}]}");
         setEventMetadata("'SAIR'","{handler:'E14AZ2',iparms:[{av:'AV17Solicitacoes_Codigo',fld:'vSOLICITACOES_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV12SDTSolicitacao',fld:'vSDTSOLICITACAO',pic:'',nv:null},{av:'AV5Contratada_codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV14Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV15Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV19Solicitacoes_Objetivo',fld:'vSOLICITACOES_OBJETIVO',pic:'',nv:''},{av:'AV18Solicitacoes_Novo_Projeto',fld:'vSOLICITACOES_NOVO_PROJETO',pic:'',nv:''},{av:'AV24UserName',fld:'vUSERNAME',pic:'ZZZZZ9',nv:0},{av:'AV27vrMsg',fld:'vVRMSG',pic:'',nv:''},{av:'GRID1_nFirstRecordOnPage',nv:0},{av:'GRID1_nEOF',nv:0}],oparms:[{av:'AV27vrMsg',fld:'vVRMSG',pic:'',nv:''},{av:'AV12SDTSolicitacao',fld:'vSDTSOLICITACAO',pic:'',nv:null},{av:'AV24UserName',fld:'vUSERNAME',pic:'ZZZZZ9',nv:0},{av:'AV18Solicitacoes_Novo_Projeto',fld:'vSOLICITACOES_NOVO_PROJETO',pic:'',nv:''},{av:'AV19Solicitacoes_Objetivo',fld:'vSOLICITACOES_OBJETIVO',pic:'',nv:''},{av:'AV15Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV14Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV5Contratada_codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("VEXCLUIR.CLICK","{handler:'E18AZ2',iparms:[{av:'AV6Excluir',fld:'vEXCLUIR',grid:67,pic:'',nv:''},{av:'AV12SDTSolicitacao',fld:'vSDTSOLICITACAO',pic:'',nv:null},{av:'GRID1_nFirstRecordOnPage',nv:0},{av:'GRID1_nEOF',nv:0}],oparms:[{av:'AV13SDTSolicitacaoItem',fld:'vSDTSOLICITACAOITEM',pic:'',nv:null},{av:'AV12SDTSolicitacao',fld:'vSDTSOLICITACAO',pic:'',nv:null}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV25WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV12SDTSolicitacao = new GxObjectCollection( context, "SDTSolicitacao.SDTSolicitacaoItem", "GxEv3Up14_MeetrikaVs3", "SdtSDTSolicitacao_SDTSolicitacaoItem", "GeneXus.Programs");
         AV27vrMsg = "";
         AV13SDTSolicitacaoItem = new SdtSDTSolicitacao_SDTSolicitacaoItem(context);
         AV26FuncaoUsuario_Nome = "";
         A162FuncaoUsuario_Nome = "";
         GXCCtlgxBlob = "";
         AV10Necessidade_Arquivo = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV6Excluir = "";
         GXCCtl = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         scmdbuf = "";
         H00AZ2_A40Contratada_PessoaCod = new int[1] ;
         H00AZ2_A39Contratada_Codigo = new int[1] ;
         H00AZ2_A41Contratada_PessoaNom = new String[] {""} ;
         H00AZ2_n41Contratada_PessoaNom = new bool[] {false} ;
         H00AZ2_A52Contratada_AreaTrabalhoCod = new int[1] ;
         H00AZ3_A155Servico_Codigo = new int[1] ;
         H00AZ3_A608Servico_Nome = new String[] {""} ;
         H00AZ4_A127Sistema_Codigo = new int[1] ;
         H00AZ4_A416Sistema_Nome = new String[] {""} ;
         H00AZ5_A161FuncaoUsuario_Codigo = new int[1] ;
         H00AZ5_A162FuncaoUsuario_Nome = new String[] {""} ;
         Grid1Container = new GXWebGrid( context);
         AV18Solicitacoes_Novo_Projeto = "";
         AV16Sistema_Nome = "";
         AV19Solicitacoes_Objetivo = "";
         AV11Necessidade_Descricao = "";
         Grid1Row = new GXWebRow();
         H00AZ6_A52Contratada_AreaTrabalhoCod = new int[1] ;
         H00AZ6_A439Solicitacoes_Codigo = new int[1] ;
         H00AZ6_A441Solicitacoes_Objetivo = new String[] {""} ;
         H00AZ6_n441Solicitacoes_Objetivo = new bool[] {false} ;
         H00AZ6_A127Sistema_Codigo = new int[1] ;
         H00AZ6_A155Servico_Codigo = new int[1] ;
         H00AZ6_A39Contratada_Codigo = new int[1] ;
         A441Solicitacoes_Objetivo = "";
         H00AZ7_A447SolicitacoesItens_Codigo = new int[1] ;
         H00AZ7_A439Solicitacoes_Codigo = new int[1] ;
         H00AZ7_A161FuncaoUsuario_Codigo = new int[1] ;
         H00AZ7_A448SolicitacoesItens_descricao = new String[] {""} ;
         H00AZ7_n448SolicitacoesItens_descricao = new bool[] {false} ;
         H00AZ7_A449SolicitacoesItens_Arquivo = new String[] {""} ;
         H00AZ7_n449SolicitacoesItens_Arquivo = new bool[] {false} ;
         A448SolicitacoesItens_descricao = "";
         A449SolicitacoesItens_Arquivo = "";
         H00AZ8_A161FuncaoUsuario_Codigo = new int[1] ;
         H00AZ8_A162FuncaoUsuario_Nome = new String[] {""} ;
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         lblTextblock1_Jsonclick = "";
         lblTextblock7_Jsonclick = "";
         TempTags = "";
         lblTextblock2_Jsonclick = "";
         lblTextblock8_Jsonclick = "";
         lblTextblock3_Jsonclick = "";
         lblTextblock9_Jsonclick = "";
         lblTextblock4_Jsonclick = "";
         lblTextblock5_Jsonclick = "";
         lblTextblock10_Jsonclick = "";
         lblTextblock6_Jsonclick = "";
         subGrid1_Linesclass = "";
         Grid1Column = new GXWebColumn();
         bttButton2_Jsonclick = "";
         bttButton3_Jsonclick = "";
         bttButton4_Jsonclick = "";
         bttButton5_Jsonclick = "";
         gxblobfileaux = new GxFile(context.GetPhysicalPath());
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.solicitacaodeservico__default(),
            new Object[][] {
                new Object[] {
               H00AZ2_A40Contratada_PessoaCod, H00AZ2_A39Contratada_Codigo, H00AZ2_A41Contratada_PessoaNom, H00AZ2_n41Contratada_PessoaNom, H00AZ2_A52Contratada_AreaTrabalhoCod
               }
               , new Object[] {
               H00AZ3_A155Servico_Codigo, H00AZ3_A608Servico_Nome
               }
               , new Object[] {
               H00AZ4_A127Sistema_Codigo, H00AZ4_A416Sistema_Nome
               }
               , new Object[] {
               H00AZ5_A161FuncaoUsuario_Codigo, H00AZ5_A162FuncaoUsuario_Nome
               }
               , new Object[] {
               H00AZ6_A52Contratada_AreaTrabalhoCod, H00AZ6_A439Solicitacoes_Codigo, H00AZ6_A441Solicitacoes_Objetivo, H00AZ6_n441Solicitacoes_Objetivo, H00AZ6_A127Sistema_Codigo, H00AZ6_A155Servico_Codigo, H00AZ6_A39Contratada_Codigo
               }
               , new Object[] {
               H00AZ7_A447SolicitacoesItens_Codigo, H00AZ7_A439Solicitacoes_Codigo, H00AZ7_A161FuncaoUsuario_Codigo, H00AZ7_A448SolicitacoesItens_descricao, H00AZ7_n448SolicitacoesItens_descricao, H00AZ7_A449SolicitacoesItens_Arquivo, H00AZ7_n449SolicitacoesItens_Arquivo
               }
               , new Object[] {
               H00AZ8_A161FuncaoUsuario_Codigo, H00AZ8_A162FuncaoUsuario_Nome
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavCtlfuncaousuario_codigo_Enabled = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_67 ;
      private short nGXsfl_67_idx=1 ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short AV28Erro ;
      private short wbEnd ;
      private short wbStart ;
      private short AV32GXV1 ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_67_Refreshing=0 ;
      private short subGrid1_Backcolorstyle ;
      private short nGXsfl_67_fel_idx=1 ;
      private short GRID1_nEOF ;
      private short nGXsfl_67_bak_idx=1 ;
      private short AV29Idx ;
      private short subGrid1_Titlebackstyle ;
      private short subGrid1_Allowselection ;
      private short subGrid1_Allowhovering ;
      private short subGrid1_Allowcollapsing ;
      private short subGrid1_Collapsed ;
      private short nGXWrapped ;
      private short subGrid1_Backstyle ;
      private int AV17Solicitacoes_Codigo ;
      private int wcpOAV17Solicitacoes_Codigo ;
      private int A161FuncaoUsuario_Codigo ;
      private int edtavSolicitacoes_codigo_Visible ;
      private int gxdynajaxindex ;
      private int AV5Contratada_codigo ;
      private int AV14Servico_Codigo ;
      private int AV15Sistema_Codigo ;
      private int AV8FuncaoUsuario_Codigo ;
      private int subGrid1_Islastpage ;
      private int edtavCtlfuncaousuario_codigo_Enabled ;
      private int subGrid1_Titleforecolor ;
      private int subGrid1_Visible ;
      private int AV24UserName ;
      private int edtavSistema_nome_Visible ;
      private int edtavUsername_Visible ;
      private int AV36GXV2 ;
      private int A52Contratada_AreaTrabalhoCod ;
      private int A439Solicitacoes_Codigo ;
      private int A127Sistema_Codigo ;
      private int A155Servico_Codigo ;
      private int A39Contratada_Codigo ;
      private int subGrid1_Titlebackcolor ;
      private int subGrid1_Allbackcolor ;
      private int subGrid1_Selectioncolor ;
      private int subGrid1_Hoveringcolor ;
      private int idxLst ;
      private int subGrid1_Backcolor ;
      private int edtavCtlfuncaousuario_nome_Enabled ;
      private int edtavCtlfuncaousuario_nome_Visible ;
      private long GRID1_nCurrentRecord ;
      private long GRID1_nFirstRecordOnPage ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_67_idx="0001" ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String AV27vrMsg ;
      private String GXCCtlgxBlob ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String edtavSolicitacoes_codigo_Internalname ;
      private String edtavSolicitacoes_codigo_Jsonclick ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String AV6Excluir ;
      private String chkavExcluir_Internalname ;
      private String GXCCtl ;
      private String chkavSolicitacoes_novo_projeto_Internalname ;
      private String dynavContratada_codigo_Internalname ;
      private String gxwrpcisep ;
      private String scmdbuf ;
      private String edtavCtlfuncaousuario_codigo_Internalname ;
      private String AV18Solicitacoes_Novo_Projeto ;
      private String dynavServico_codigo_Internalname ;
      private String dynavSistema_codigo_Internalname ;
      private String AV16Sistema_Nome ;
      private String edtavSistema_nome_Internalname ;
      private String edtavSolicitacoes_objetivo_Internalname ;
      private String dynavFuncaousuario_codigo_Internalname ;
      private String edtavNecessidade_descricao_Internalname ;
      private String edtavNecessidade_arquivo_Internalname ;
      private String edtavUsername_Internalname ;
      private String sGXsfl_67_fel_idx="0001" ;
      private String lblTextblock3_Caption ;
      private String lblTextblock3_Internalname ;
      private String sStyleString ;
      private String tblTablecontent_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String lblTextblock1_Internalname ;
      private String lblTextblock1_Jsonclick ;
      private String lblTextblock7_Internalname ;
      private String lblTextblock7_Jsonclick ;
      private String TempTags ;
      private String dynavContratada_codigo_Jsonclick ;
      private String lblTextblock2_Internalname ;
      private String lblTextblock2_Jsonclick ;
      private String lblTextblock8_Internalname ;
      private String lblTextblock8_Jsonclick ;
      private String dynavServico_codigo_Jsonclick ;
      private String lblTextblock3_Jsonclick ;
      private String lblTextblock9_Internalname ;
      private String lblTextblock9_Jsonclick ;
      private String dynavSistema_codigo_Jsonclick ;
      private String edtavSistema_nome_Jsonclick ;
      private String lblTextblock4_Internalname ;
      private String lblTextblock4_Jsonclick ;
      private String lblTextblock5_Internalname ;
      private String lblTextblock5_Jsonclick ;
      private String lblTextblock10_Internalname ;
      private String lblTextblock10_Jsonclick ;
      private String dynavFuncaousuario_codigo_Jsonclick ;
      private String lblTextblock6_Internalname ;
      private String lblTextblock6_Jsonclick ;
      private String subGrid1_Internalname ;
      private String subGrid1_Class ;
      private String subGrid1_Linesclass ;
      private String edtavUsername_Jsonclick ;
      private String tblTable2_Internalname ;
      private String bttButton2_Internalname ;
      private String bttButton2_Jsonclick ;
      private String bttButton3_Internalname ;
      private String bttButton3_Jsonclick ;
      private String bttButton4_Internalname ;
      private String bttButton4_Jsonclick ;
      private String bttButton5_Internalname ;
      private String bttButton5_Jsonclick ;
      private String tblTable3_Internalname ;
      private String edtavNecessidade_arquivo_Filetype ;
      private String edtavNecessidade_arquivo_Contenttype ;
      private String edtavNecessidade_arquivo_Parameters ;
      private String edtavNecessidade_arquivo_Jsonclick ;
      private String edtavCtlfuncaousuario_nome_Internalname ;
      private String ROClassString ;
      private String edtavCtlfuncaousuario_codigo_Jsonclick ;
      private String edtavCtlfuncaousuario_nome_Jsonclick ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool gx_BV67 ;
      private bool n441Solicitacoes_Objetivo ;
      private bool n448SolicitacoesItens_descricao ;
      private bool n449SolicitacoesItens_Arquivo ;
      private String AV19Solicitacoes_Objetivo ;
      private String AV11Necessidade_Descricao ;
      private String A441Solicitacoes_Objetivo ;
      private String A448SolicitacoesItens_descricao ;
      private String AV26FuncaoUsuario_Nome ;
      private String A162FuncaoUsuario_Nome ;
      private String AV10Necessidade_Arquivo ;
      private String A449SolicitacoesItens_Arquivo ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GxFile gxblobfileaux ;
      private GXWebGrid Grid1Container ;
      private GXWebRow Grid1Row ;
      private GXWebColumn Grid1Column ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_Solicitacoes_Codigo ;
      private GXCombobox dynavContratada_codigo ;
      private GXCheckbox chkavSolicitacoes_novo_projeto ;
      private GXCombobox dynavServico_codigo ;
      private GXCombobox dynavSistema_codigo ;
      private GXCombobox dynavFuncaousuario_codigo ;
      private GXCheckbox chkavExcluir ;
      private IDataStoreProvider pr_default ;
      private int[] H00AZ2_A40Contratada_PessoaCod ;
      private int[] H00AZ2_A39Contratada_Codigo ;
      private String[] H00AZ2_A41Contratada_PessoaNom ;
      private bool[] H00AZ2_n41Contratada_PessoaNom ;
      private int[] H00AZ2_A52Contratada_AreaTrabalhoCod ;
      private int[] H00AZ3_A155Servico_Codigo ;
      private String[] H00AZ3_A608Servico_Nome ;
      private int[] H00AZ4_A127Sistema_Codigo ;
      private String[] H00AZ4_A416Sistema_Nome ;
      private int[] H00AZ5_A161FuncaoUsuario_Codigo ;
      private String[] H00AZ5_A162FuncaoUsuario_Nome ;
      private int[] H00AZ6_A52Contratada_AreaTrabalhoCod ;
      private int[] H00AZ6_A439Solicitacoes_Codigo ;
      private String[] H00AZ6_A441Solicitacoes_Objetivo ;
      private bool[] H00AZ6_n441Solicitacoes_Objetivo ;
      private int[] H00AZ6_A127Sistema_Codigo ;
      private int[] H00AZ6_A155Servico_Codigo ;
      private int[] H00AZ6_A39Contratada_Codigo ;
      private int[] H00AZ7_A447SolicitacoesItens_Codigo ;
      private int[] H00AZ7_A439Solicitacoes_Codigo ;
      private int[] H00AZ7_A161FuncaoUsuario_Codigo ;
      private String[] H00AZ7_A448SolicitacoesItens_descricao ;
      private bool[] H00AZ7_n448SolicitacoesItens_descricao ;
      private String[] H00AZ7_A449SolicitacoesItens_Arquivo ;
      private bool[] H00AZ7_n449SolicitacoesItens_Arquivo ;
      private int[] H00AZ8_A161FuncaoUsuario_Codigo ;
      private String[] H00AZ8_A162FuncaoUsuario_Nome ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      [ObjectCollection(ItemType=typeof( SdtSDTSolicitacao_SDTSolicitacaoItem ))]
      private IGxCollection AV12SDTSolicitacao ;
      private GXWebForm Form ;
      private SdtSDTSolicitacao_SDTSolicitacaoItem AV13SDTSolicitacaoItem ;
      private wwpbaseobjects.SdtWWPContext AV25WWPContext ;
   }

   public class solicitacaodeservico__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00AZ2 ;
          prmH00AZ2 = new Object[] {
          new Object[] {"@AV25WWPC_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00AZ3 ;
          prmH00AZ3 = new Object[] {
          } ;
          Object[] prmH00AZ4 ;
          prmH00AZ4 = new Object[] {
          } ;
          Object[] prmH00AZ5 ;
          prmH00AZ5 = new Object[] {
          } ;
          Object[] prmH00AZ6 ;
          prmH00AZ6 = new Object[] {
          new Object[] {"@AV17Solicitacoes_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00AZ7 ;
          prmH00AZ7 = new Object[] {
          new Object[] {"@AV17Solicitacoes_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00AZ8 ;
          prmH00AZ8 = new Object[] {
          new Object[] {"@AV8FuncaoUsuario_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00AZ2", "SELECT T2.[Pessoa_Codigo] AS Contratada_PessoaCod, T1.[Contratada_Codigo], T2.[Pessoa_Nome] AS Contratada_PessoaNom, T1.[Contratada_AreaTrabalhoCod] FROM ([Contratada] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Contratada_PessoaCod]) WHERE T1.[Contratada_AreaTrabalhoCod] = @AV25WWPC_1Areatrabalho_codigo ORDER BY T2.[Pessoa_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00AZ2,0,0,true,false )
             ,new CursorDef("H00AZ3", "SELECT [Servico_Codigo], [Servico_Nome] FROM [Servico] WITH (NOLOCK) ORDER BY [Servico_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00AZ3,0,0,true,false )
             ,new CursorDef("H00AZ4", "SELECT [Sistema_Codigo], [Sistema_Nome] FROM [Sistema] WITH (NOLOCK) ORDER BY [Sistema_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00AZ4,0,0,true,false )
             ,new CursorDef("H00AZ5", "SELECT [FuncaoUsuario_Codigo], [FuncaoUsuario_Nome] FROM [ModuloFuncoes] WITH (NOLOCK) ORDER BY [FuncaoUsuario_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00AZ5,0,0,true,false )
             ,new CursorDef("H00AZ6", "SELECT T2.[Contratada_AreaTrabalhoCod], T1.[Solicitacoes_Codigo], T1.[Solicitacoes_Objetivo], T1.[Sistema_Codigo], T1.[Servico_Codigo], T1.[Contratada_Codigo] FROM ([Solicitacoes] T1 WITH (NOLOCK) INNER JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[Contratada_Codigo]) WHERE T1.[Solicitacoes_Codigo] = @AV17Solicitacoes_Codigo ORDER BY T1.[Solicitacoes_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00AZ6,1,0,true,true )
             ,new CursorDef("H00AZ7", "SELECT [SolicitacoesItens_Codigo], [Solicitacoes_Codigo], [FuncaoUsuario_Codigo], [SolicitacoesItens_descricao], [SolicitacoesItens_Arquivo] FROM [SolicitacoesItens] WITH (NOLOCK) WHERE [Solicitacoes_Codigo] = @AV17Solicitacoes_Codigo ORDER BY [Solicitacoes_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00AZ7,100,0,true,false )
             ,new CursorDef("H00AZ8", "SELECT [FuncaoUsuario_Codigo], [FuncaoUsuario_Nome] FROM [ModuloFuncoes] WITH (NOLOCK) WHERE [FuncaoUsuario_Codigo] = @AV8FuncaoUsuario_Codigo ORDER BY [FuncaoUsuario_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00AZ8,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                ((int[]) buf[6])[0] = rslt.getInt(6) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((String[]) buf[3])[0] = rslt.getLongVarchar(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((String[]) buf[5])[0] = rslt.getBLOBFile(5, "tmp", "") ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
