/*
               File: ContagemResultado_TipoRegistro
        Description: ContagemResultado_TipoRegistro
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 21:56:34.73
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class gxdomaincontagemresultado_tiporegistro
   {
      private static Hashtable domain = new Hashtable();
      static gxdomaincontagemresultado_tiporegistro ()
      {
         domain[(short)1] = "Ordem de Servi�o";
         domain[(short)2] = "Solicita��o de Servi�o";
         domain[(short)3] = "Reuni�o";
      }

      public static string getDescription( IGxContext context ,
                                           short key )
      {
         return (string)domain[key] ;
      }

      public static GxSimpleCollection getValues( )
      {
         GxSimpleCollection value = new GxSimpleCollection();
         ArrayList aKeys = new ArrayList(domain.Keys);
         aKeys.Sort();
         foreach (short key in aKeys)
         {
            value.Add(key);
         }
         return value;
      }

   }

}
