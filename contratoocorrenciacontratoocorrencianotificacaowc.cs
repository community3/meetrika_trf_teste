/*
               File: ContratoOcorrenciaContratoOcorrenciaNotificacaoWC
        Description: Contrato Ocorrencia Contrato Ocorrencia Notificacao WC
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/18/2020 12:49:17.60
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contratoocorrenciacontratoocorrencianotificacaowc : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public contratoocorrenciacontratoocorrencianotificacaowc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public contratoocorrenciacontratoocorrencianotificacaowc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ContratoOcorrencia_Codigo )
      {
         this.AV7ContratoOcorrencia_Codigo = aP0_ContratoOcorrencia_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  AV7ContratoOcorrencia_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7ContratoOcorrencia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContratoOcorrencia_Codigo), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)AV7ContratoOcorrencia_Codigo});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
               {
                  nRC_GXsfl_84 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  nGXsfl_84_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  sGXsfl_84_idx = GetNextPar( );
                  sPrefix = GetNextPar( );
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxnrGrid_newrow( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
               {
                  subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV14OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
                  AV15OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
                  AV16DynamicFiltersSelector1 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
                  AV17ContratoOcorrenciaNotificacao_Data1 = context.localUtil.ParseDateParm( GetNextPar( ));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17ContratoOcorrenciaNotificacao_Data1", context.localUtil.Format(AV17ContratoOcorrenciaNotificacao_Data1, "99/99/99"));
                  AV18ContratoOcorrenciaNotificacao_Data_To1 = context.localUtil.ParseDateParm( GetNextPar( ));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18ContratoOcorrenciaNotificacao_Data_To1", context.localUtil.Format(AV18ContratoOcorrenciaNotificacao_Data_To1, "99/99/99"));
                  AV19ContratoOcorrenciaNotificacao_Prazo1 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19ContratoOcorrenciaNotificacao_Prazo1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV19ContratoOcorrenciaNotificacao_Prazo1), 4, 0)));
                  AV20ContratoOcorrenciaNotificacao_Prazo_To1 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20ContratoOcorrenciaNotificacao_Prazo_To1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20ContratoOcorrenciaNotificacao_Prazo_To1), 4, 0)));
                  AV22DynamicFiltersSelector2 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22DynamicFiltersSelector2", AV22DynamicFiltersSelector2);
                  AV23ContratoOcorrenciaNotificacao_Data2 = context.localUtil.ParseDateParm( GetNextPar( ));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23ContratoOcorrenciaNotificacao_Data2", context.localUtil.Format(AV23ContratoOcorrenciaNotificacao_Data2, "99/99/99"));
                  AV24ContratoOcorrenciaNotificacao_Data_To2 = context.localUtil.ParseDateParm( GetNextPar( ));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24ContratoOcorrenciaNotificacao_Data_To2", context.localUtil.Format(AV24ContratoOcorrenciaNotificacao_Data_To2, "99/99/99"));
                  AV25ContratoOcorrenciaNotificacao_Prazo2 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25ContratoOcorrenciaNotificacao_Prazo2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25ContratoOcorrenciaNotificacao_Prazo2), 4, 0)));
                  AV26ContratoOcorrenciaNotificacao_Prazo_To2 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26ContratoOcorrenciaNotificacao_Prazo_To2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26ContratoOcorrenciaNotificacao_Prazo_To2), 4, 0)));
                  AV21DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21DynamicFiltersEnabled2", AV21DynamicFiltersEnabled2);
                  AV39TFContratoOcorrenciaNotificacao_Data = context.localUtil.ParseDateParm( GetNextPar( ));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV39TFContratoOcorrenciaNotificacao_Data", context.localUtil.Format(AV39TFContratoOcorrenciaNotificacao_Data, "99/99/99"));
                  AV40TFContratoOcorrenciaNotificacao_Data_To = context.localUtil.ParseDateParm( GetNextPar( ));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV40TFContratoOcorrenciaNotificacao_Data_To", context.localUtil.Format(AV40TFContratoOcorrenciaNotificacao_Data_To, "99/99/99"));
                  AV45TFContratoOcorrenciaNotificacao_Prazo = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV45TFContratoOcorrenciaNotificacao_Prazo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV45TFContratoOcorrenciaNotificacao_Prazo), 4, 0)));
                  AV46TFContratoOcorrenciaNotificacao_Prazo_To = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV46TFContratoOcorrenciaNotificacao_Prazo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV46TFContratoOcorrenciaNotificacao_Prazo_To), 4, 0)));
                  AV49TFContratoOcorrenciaNotificacao_Descricao = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV49TFContratoOcorrenciaNotificacao_Descricao", AV49TFContratoOcorrenciaNotificacao_Descricao);
                  AV50TFContratoOcorrenciaNotificacao_Descricao_Sel = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV50TFContratoOcorrenciaNotificacao_Descricao_Sel", AV50TFContratoOcorrenciaNotificacao_Descricao_Sel);
                  AV53TFContratoOcorrenciaNotificacao_Cumprido = context.localUtil.ParseDateParm( GetNextPar( ));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV53TFContratoOcorrenciaNotificacao_Cumprido", context.localUtil.Format(AV53TFContratoOcorrenciaNotificacao_Cumprido, "99/99/99"));
                  AV54TFContratoOcorrenciaNotificacao_Cumprido_To = context.localUtil.ParseDateParm( GetNextPar( ));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV54TFContratoOcorrenciaNotificacao_Cumprido_To", context.localUtil.Format(AV54TFContratoOcorrenciaNotificacao_Cumprido_To, "99/99/99"));
                  AV59TFContratoOcorrenciaNotificacao_Protocolo = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV59TFContratoOcorrenciaNotificacao_Protocolo", AV59TFContratoOcorrenciaNotificacao_Protocolo);
                  AV60TFContratoOcorrenciaNotificacao_Protocolo_Sel = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV60TFContratoOcorrenciaNotificacao_Protocolo_Sel", AV60TFContratoOcorrenciaNotificacao_Protocolo_Sel);
                  AV63TFContratoOcorrenciaNotificacao_Responsavel = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV63TFContratoOcorrenciaNotificacao_Responsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(AV63TFContratoOcorrenciaNotificacao_Responsavel), 6, 0)));
                  AV64TFContratoOcorrenciaNotificacao_Responsavel_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV64TFContratoOcorrenciaNotificacao_Responsavel_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV64TFContratoOcorrenciaNotificacao_Responsavel_To), 6, 0)));
                  AV67TFContratoOcorrenciaNotificacao_Nome = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV67TFContratoOcorrenciaNotificacao_Nome", AV67TFContratoOcorrenciaNotificacao_Nome);
                  AV68TFContratoOcorrenciaNotificacao_Nome_Sel = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV68TFContratoOcorrenciaNotificacao_Nome_Sel", AV68TFContratoOcorrenciaNotificacao_Nome_Sel);
                  AV71TFContratoOcorrenciaNotificacao_Docto = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV71TFContratoOcorrenciaNotificacao_Docto", AV71TFContratoOcorrenciaNotificacao_Docto);
                  AV72TFContratoOcorrenciaNotificacao_Docto_Sel = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV72TFContratoOcorrenciaNotificacao_Docto_Sel", AV72TFContratoOcorrenciaNotificacao_Docto_Sel);
                  AV7ContratoOcorrencia_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7ContratoOcorrencia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContratoOcorrencia_Codigo), 6, 0)));
                  AV43ddo_ContratoOcorrenciaNotificacao_DataTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV43ddo_ContratoOcorrenciaNotificacao_DataTitleControlIdToReplace", AV43ddo_ContratoOcorrenciaNotificacao_DataTitleControlIdToReplace);
                  AV47ddo_ContratoOcorrenciaNotificacao_PrazoTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV47ddo_ContratoOcorrenciaNotificacao_PrazoTitleControlIdToReplace", AV47ddo_ContratoOcorrenciaNotificacao_PrazoTitleControlIdToReplace);
                  AV51ddo_ContratoOcorrenciaNotificacao_DescricaoTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV51ddo_ContratoOcorrenciaNotificacao_DescricaoTitleControlIdToReplace", AV51ddo_ContratoOcorrenciaNotificacao_DescricaoTitleControlIdToReplace);
                  AV57ddo_ContratoOcorrenciaNotificacao_CumpridoTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV57ddo_ContratoOcorrenciaNotificacao_CumpridoTitleControlIdToReplace", AV57ddo_ContratoOcorrenciaNotificacao_CumpridoTitleControlIdToReplace);
                  AV61ddo_ContratoOcorrenciaNotificacao_ProtocoloTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV61ddo_ContratoOcorrenciaNotificacao_ProtocoloTitleControlIdToReplace", AV61ddo_ContratoOcorrenciaNotificacao_ProtocoloTitleControlIdToReplace);
                  AV65ddo_ContratoOcorrenciaNotificacao_ResponsavelTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV65ddo_ContratoOcorrenciaNotificacao_ResponsavelTitleControlIdToReplace", AV65ddo_ContratoOcorrenciaNotificacao_ResponsavelTitleControlIdToReplace);
                  AV69ddo_ContratoOcorrenciaNotificacao_NomeTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV69ddo_ContratoOcorrenciaNotificacao_NomeTitleControlIdToReplace", AV69ddo_ContratoOcorrenciaNotificacao_NomeTitleControlIdToReplace);
                  AV73ddo_ContratoOcorrenciaNotificacao_DoctoTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV73ddo_ContratoOcorrenciaNotificacao_DoctoTitleControlIdToReplace", AV73ddo_ContratoOcorrenciaNotificacao_DoctoTitleControlIdToReplace);
                  AV82Pgmname = GetNextPar( );
                  ajax_req_read_hidden_sdt(GetNextPar( ), AV11GridState);
                  AV34DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV34DynamicFiltersIgnoreFirst", AV34DynamicFiltersIgnoreFirst);
                  AV33DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV33DynamicFiltersRemoving", AV33DynamicFiltersRemoving);
                  A297ContratoOcorrenciaNotificacao_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A297ContratoOcorrenciaNotificacao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A297ContratoOcorrenciaNotificacao_Codigo), 6, 0)));
                  A294ContratoOcorrencia_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A294ContratoOcorrencia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A294ContratoOcorrencia_Codigo), 6, 0)));
                  A303ContratoOcorrenciaNotificacao_Responsavel = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  sPrefix = GetNextPar( );
                  init_default_properties( ) ;
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17ContratoOcorrenciaNotificacao_Data1, AV18ContratoOcorrenciaNotificacao_Data_To1, AV19ContratoOcorrenciaNotificacao_Prazo1, AV20ContratoOcorrenciaNotificacao_Prazo_To1, AV22DynamicFiltersSelector2, AV23ContratoOcorrenciaNotificacao_Data2, AV24ContratoOcorrenciaNotificacao_Data_To2, AV25ContratoOcorrenciaNotificacao_Prazo2, AV26ContratoOcorrenciaNotificacao_Prazo_To2, AV21DynamicFiltersEnabled2, AV39TFContratoOcorrenciaNotificacao_Data, AV40TFContratoOcorrenciaNotificacao_Data_To, AV45TFContratoOcorrenciaNotificacao_Prazo, AV46TFContratoOcorrenciaNotificacao_Prazo_To, AV49TFContratoOcorrenciaNotificacao_Descricao, AV50TFContratoOcorrenciaNotificacao_Descricao_Sel, AV53TFContratoOcorrenciaNotificacao_Cumprido, AV54TFContratoOcorrenciaNotificacao_Cumprido_To, AV59TFContratoOcorrenciaNotificacao_Protocolo, AV60TFContratoOcorrenciaNotificacao_Protocolo_Sel, AV63TFContratoOcorrenciaNotificacao_Responsavel, AV64TFContratoOcorrenciaNotificacao_Responsavel_To, AV67TFContratoOcorrenciaNotificacao_Nome, AV68TFContratoOcorrenciaNotificacao_Nome_Sel, AV71TFContratoOcorrenciaNotificacao_Docto, AV72TFContratoOcorrenciaNotificacao_Docto_Sel, AV7ContratoOcorrencia_Codigo, AV43ddo_ContratoOcorrenciaNotificacao_DataTitleControlIdToReplace, AV47ddo_ContratoOcorrenciaNotificacao_PrazoTitleControlIdToReplace, AV51ddo_ContratoOcorrenciaNotificacao_DescricaoTitleControlIdToReplace, AV57ddo_ContratoOcorrenciaNotificacao_CumpridoTitleControlIdToReplace, AV61ddo_ContratoOcorrenciaNotificacao_ProtocoloTitleControlIdToReplace, AV65ddo_ContratoOcorrenciaNotificacao_ResponsavelTitleControlIdToReplace, AV69ddo_ContratoOcorrenciaNotificacao_NomeTitleControlIdToReplace, AV73ddo_ContratoOcorrenciaNotificacao_DoctoTitleControlIdToReplace, AV82Pgmname, AV11GridState, AV34DynamicFiltersIgnoreFirst, AV33DynamicFiltersRemoving, A297ContratoOcorrenciaNotificacao_Codigo, A294ContratoOcorrencia_Codigo, A303ContratoOcorrenciaNotificacao_Responsavel, sPrefix) ;
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  forbiddenHiddens = sPrefix + "hsh" + "ContratoOcorrenciaContratoOcorrenciaNotificacaoWC";
                  forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A294ContratoOcorrencia_Codigo), "ZZZZZ9");
                  GxWebStd.gx_hidden_field( context, sPrefix+"hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
                  GXUtil.WriteLog("contratoocorrenciacontratoocorrencianotificacaowc:[SendSecurityCheck value for]"+"ContratoOcorrencia_Codigo:"+context.localUtil.Format( (decimal)(A294ContratoOcorrencia_Codigo), "ZZZZZ9"));
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
                  return  ;
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PA7A2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV82Pgmname = "ContratoOcorrenciaContratoOcorrenciaNotificacaoWC";
               context.Gx_err = 0;
               WS7A2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Contrato Ocorrencia Contrato Ocorrencia Notificacao WC") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202051812491863");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("contratoocorrenciacontratoocorrencianotificacaowc.aspx") + "?" + UrlEncode("" +AV7ContratoOcorrencia_Codigo)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV14OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDDSC", StringUtil.BoolToStr( AV15OrderedDsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSSELECTOR1", AV16DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vCONTRATOOCORRENCIANOTIFICACAO_DATA1", context.localUtil.Format(AV17ContratoOcorrenciaNotificacao_Data1, "99/99/99"));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO1", context.localUtil.Format(AV18ContratoOcorrenciaNotificacao_Data_To1, "99/99/99"));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vCONTRATOOCORRENCIANOTIFICACAO_PRAZO1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV19ContratoOcorrenciaNotificacao_Prazo1), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vCONTRATOOCORRENCIANOTIFICACAO_PRAZO_TO1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV20ContratoOcorrenciaNotificacao_Prazo_To1), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSSELECTOR2", AV22DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vCONTRATOOCORRENCIANOTIFICACAO_DATA2", context.localUtil.Format(AV23ContratoOcorrenciaNotificacao_Data2, "99/99/99"));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO2", context.localUtil.Format(AV24ContratoOcorrenciaNotificacao_Data_To2, "99/99/99"));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vCONTRATOOCORRENCIANOTIFICACAO_PRAZO2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV25ContratoOcorrenciaNotificacao_Prazo2), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vCONTRATOOCORRENCIANOTIFICACAO_PRAZO_TO2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV26ContratoOcorrenciaNotificacao_Prazo_To2), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV21DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_DATA", context.localUtil.Format(AV39TFContratoOcorrenciaNotificacao_Data, "99/99/99"));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_DATA_TO", context.localUtil.Format(AV40TFContratoOcorrenciaNotificacao_Data_To, "99/99/99"));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV45TFContratoOcorrenciaNotificacao_Prazo), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV46TFContratoOcorrenciaNotificacao_Prazo_To), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO", AV49TFContratoOcorrenciaNotificacao_Descricao);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_SEL", AV50TFContratoOcorrenciaNotificacao_Descricao_Sel);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO", context.localUtil.Format(AV53TFContratoOcorrenciaNotificacao_Cumprido, "99/99/99"));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_TO", context.localUtil.Format(AV54TFContratoOcorrenciaNotificacao_Cumprido_To, "99/99/99"));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO", AV59TFContratoOcorrenciaNotificacao_Protocolo);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_SEL", AV60TFContratoOcorrenciaNotificacao_Protocolo_Sel);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV63TFContratoOcorrenciaNotificacao_Responsavel), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV64TFContratoOcorrenciaNotificacao_Responsavel_To), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_NOME", StringUtil.RTrim( AV67TFContratoOcorrenciaNotificacao_Nome));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_NOME_SEL", StringUtil.RTrim( AV68TFContratoOcorrenciaNotificacao_Nome_Sel));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_DOCTO", AV71TFContratoOcorrenciaNotificacao_Docto);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_DOCTO_SEL", AV72TFContratoOcorrenciaNotificacao_Docto_Sel);
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"nRC_GXsfl_84", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_84), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV76GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV77GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vDDO_TITLESETTINGSICONS", AV74DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vDDO_TITLESETTINGSICONS", AV74DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vCONTRATOOCORRENCIANOTIFICACAO_DATATITLEFILTERDATA", AV38ContratoOcorrenciaNotificacao_DataTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vCONTRATOOCORRENCIANOTIFICACAO_DATATITLEFILTERDATA", AV38ContratoOcorrenciaNotificacao_DataTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vCONTRATOOCORRENCIANOTIFICACAO_PRAZOTITLEFILTERDATA", AV44ContratoOcorrenciaNotificacao_PrazoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vCONTRATOOCORRENCIANOTIFICACAO_PRAZOTITLEFILTERDATA", AV44ContratoOcorrenciaNotificacao_PrazoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vCONTRATOOCORRENCIANOTIFICACAO_DESCRICAOTITLEFILTERDATA", AV48ContratoOcorrenciaNotificacao_DescricaoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vCONTRATOOCORRENCIANOTIFICACAO_DESCRICAOTITLEFILTERDATA", AV48ContratoOcorrenciaNotificacao_DescricaoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDOTITLEFILTERDATA", AV52ContratoOcorrenciaNotificacao_CumpridoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDOTITLEFILTERDATA", AV52ContratoOcorrenciaNotificacao_CumpridoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLOTITLEFILTERDATA", AV58ContratoOcorrenciaNotificacao_ProtocoloTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLOTITLEFILTERDATA", AV58ContratoOcorrenciaNotificacao_ProtocoloTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vCONTRATOOCORRENCIANOTIFICACAO_RESPONSAVELTITLEFILTERDATA", AV62ContratoOcorrenciaNotificacao_ResponsavelTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vCONTRATOOCORRENCIANOTIFICACAO_RESPONSAVELTITLEFILTERDATA", AV62ContratoOcorrenciaNotificacao_ResponsavelTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vCONTRATOOCORRENCIANOTIFICACAO_NOMETITLEFILTERDATA", AV66ContratoOcorrenciaNotificacao_NomeTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vCONTRATOOCORRENCIANOTIFICACAO_NOMETITLEFILTERDATA", AV66ContratoOcorrenciaNotificacao_NomeTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vCONTRATOOCORRENCIANOTIFICACAO_DOCTOTITLEFILTERDATA", AV70ContratoOcorrenciaNotificacao_DoctoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vCONTRATOOCORRENCIANOTIFICACAO_DOCTOTITLEFILTERDATA", AV70ContratoOcorrenciaNotificacao_DoctoTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOAV7ContratoOcorrencia_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOAV7ContratoOcorrencia_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vCONTRATOOCORRENCIA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7ContratoOcorrencia_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vPGMNAME", StringUtil.RTrim( AV82Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vGRIDSTATE", AV11GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vGRIDSTATE", AV11GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, sPrefix+"vDYNAMICFILTERSIGNOREFIRST", AV34DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, sPrefix+"vDYNAMICFILTERSREMOVING", AV33DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATOOCORRENCIANOTIFICACAO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A297ContratoOcorrenciaNotificacao_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Caption", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_data_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Tooltip", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_data_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Cls", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_data_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Filteredtext_set", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_data_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Filteredtextto_set", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_data_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_data_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_data_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Includesortasc", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_data_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Includesortdsc", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_data_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Sortedstatus", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_data_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Includefilter", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_data_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Filtertype", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_data_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Filterisrange", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_data_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Includedatalist", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_data_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Sortasc", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_data_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Sortdsc", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_data_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Cleanfilter", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_data_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Rangefilterfrom", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_data_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Rangefilterto", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_data_Rangefilterto));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Searchbuttontext", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_data_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Caption", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_prazo_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Tooltip", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_prazo_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Cls", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_prazo_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Filteredtext_set", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_prazo_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Filteredtextto_set", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_prazo_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_prazo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_prazo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Includesortasc", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_prazo_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Includesortdsc", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_prazo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Sortedstatus", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_prazo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Includefilter", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_prazo_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Filtertype", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_prazo_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Filterisrange", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_prazo_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Includedatalist", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_prazo_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Sortasc", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_prazo_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Sortdsc", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_prazo_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Cleanfilter", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_prazo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Rangefilterfrom", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_prazo_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Rangefilterto", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_prazo_Rangefilterto));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Searchbuttontext", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_prazo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Caption", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_descricao_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Tooltip", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_descricao_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Cls", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_descricao_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Filteredtext_set", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_descricao_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Selectedvalue_set", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_descricao_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_descricao_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_descricao_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Includesortasc", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_descricao_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Includesortdsc", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_descricao_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Sortedstatus", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_descricao_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Includefilter", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_descricao_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Filtertype", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_descricao_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Filterisrange", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_descricao_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Includedatalist", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_descricao_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Datalisttype", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_descricao_Datalisttype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Datalistproc", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_descricao_Datalistproc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contratoocorrencianotificacao_descricao_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Sortasc", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_descricao_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Sortdsc", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_descricao_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Loadingdata", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_descricao_Loadingdata));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Cleanfilter", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_descricao_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Noresultsfound", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_descricao_Noresultsfound));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Searchbuttontext", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_descricao_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Caption", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_cumprido_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Tooltip", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_cumprido_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Cls", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_cumprido_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Filteredtext_set", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_cumprido_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Filteredtextto_set", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_cumprido_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_cumprido_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_cumprido_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Includesortasc", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_cumprido_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Includesortdsc", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_cumprido_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Sortedstatus", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_cumprido_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Includefilter", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_cumprido_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Filtertype", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_cumprido_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Filterisrange", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_cumprido_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Includedatalist", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_cumprido_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Sortasc", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_cumprido_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Sortdsc", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_cumprido_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Cleanfilter", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_cumprido_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Rangefilterfrom", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_cumprido_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Rangefilterto", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_cumprido_Rangefilterto));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Searchbuttontext", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_cumprido_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Caption", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_protocolo_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Tooltip", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_protocolo_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Cls", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_protocolo_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Filteredtext_set", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_protocolo_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Selectedvalue_set", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_protocolo_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_protocolo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_protocolo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Includesortasc", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_protocolo_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Includesortdsc", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_protocolo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Sortedstatus", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_protocolo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Includefilter", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_protocolo_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Filtertype", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_protocolo_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Filterisrange", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_protocolo_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Includedatalist", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_protocolo_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Datalisttype", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_protocolo_Datalisttype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Datalistproc", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_protocolo_Datalistproc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contratoocorrencianotificacao_protocolo_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Sortasc", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_protocolo_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Sortdsc", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_protocolo_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Loadingdata", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_protocolo_Loadingdata));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Cleanfilter", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_protocolo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Noresultsfound", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_protocolo_Noresultsfound));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Searchbuttontext", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_protocolo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_Caption", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_responsavel_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_Tooltip", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_responsavel_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_Cls", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_responsavel_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_Filteredtext_set", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_responsavel_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_Filteredtextto_set", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_responsavel_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_responsavel_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_responsavel_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_Includesortasc", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_responsavel_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_Includesortdsc", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_responsavel_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_Sortedstatus", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_responsavel_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_Includefilter", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_responsavel_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_Filtertype", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_responsavel_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_Filterisrange", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_responsavel_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_Includedatalist", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_responsavel_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_Sortasc", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_responsavel_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_Sortdsc", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_responsavel_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_Cleanfilter", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_responsavel_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_Rangefilterfrom", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_responsavel_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_Rangefilterto", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_responsavel_Rangefilterto));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_Searchbuttontext", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_responsavel_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Caption", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_nome_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Tooltip", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_nome_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Cls", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_nome_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Filteredtext_set", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_nome_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Selectedvalue_set", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_nome_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_nome_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_nome_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Includesortasc", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_nome_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Includesortdsc", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_nome_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Sortedstatus", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_nome_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Includefilter", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_nome_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Filtertype", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_nome_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Filterisrange", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_nome_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Includedatalist", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_nome_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Datalisttype", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_nome_Datalisttype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Datalistproc", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_nome_Datalistproc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contratoocorrencianotificacao_nome_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Sortasc", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_nome_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Sortdsc", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_nome_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Loadingdata", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_nome_Loadingdata));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Cleanfilter", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_nome_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Noresultsfound", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_nome_Noresultsfound));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Searchbuttontext", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_nome_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO_Caption", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_docto_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO_Tooltip", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_docto_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO_Cls", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_docto_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO_Filteredtext_set", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_docto_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO_Selectedvalue_set", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_docto_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_docto_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_docto_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO_Includesortasc", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_docto_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO_Includesortdsc", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_docto_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO_Sortedstatus", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_docto_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO_Includefilter", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_docto_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO_Filtertype", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_docto_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO_Filterisrange", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_docto_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO_Includedatalist", StringUtil.BoolToStr( Ddo_contratoocorrencianotificacao_docto_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO_Datalisttype", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_docto_Datalisttype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO_Datalistproc", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_docto_Datalistproc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contratoocorrencianotificacao_docto_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO_Sortasc", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_docto_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO_Sortdsc", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_docto_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO_Loadingdata", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_docto_Loadingdata));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO_Cleanfilter", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_docto_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO_Noresultsfound", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_docto_Noresultsfound));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO_Searchbuttontext", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_docto_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Activeeventkey", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_data_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Filteredtext_get", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_data_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Filteredtextto_get", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_data_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Activeeventkey", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_prazo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Filteredtext_get", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_prazo_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Filteredtextto_get", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_prazo_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Activeeventkey", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_descricao_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Filteredtext_get", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_descricao_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Selectedvalue_get", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_descricao_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Activeeventkey", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_cumprido_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Filteredtext_get", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_cumprido_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Filteredtextto_get", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_cumprido_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Activeeventkey", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_protocolo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Filteredtext_get", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_protocolo_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Selectedvalue_get", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_protocolo_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_Activeeventkey", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_responsavel_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_Filteredtext_get", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_responsavel_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_Filteredtextto_get", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_responsavel_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Activeeventkey", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_nome_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Filteredtext_get", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_nome_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Selectedvalue_get", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_nome_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO_Activeeventkey", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_docto_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO_Filteredtext_get", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_docto_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO_Selectedvalue_get", StringUtil.RTrim( Ddo_contratoocorrencianotificacao_docto_Selectedvalue_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = sPrefix + "hsh" + "ContratoOcorrenciaContratoOcorrenciaNotificacaoWC";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A294ContratoOcorrencia_Codigo), "ZZZZZ9");
         GxWebStd.gx_hidden_field( context, sPrefix+"hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("contratoocorrenciacontratoocorrencianotificacaowc:[SendSecurityCheck value for]"+"ContratoOcorrencia_Codigo:"+context.localUtil.Format( (decimal)(A294ContratoOcorrencia_Codigo), "ZZZZZ9"));
      }

      protected void RenderHtmlCloseForm7A2( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("contratoocorrenciacontratoocorrencianotificacaowc.js", "?20205181249245");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "ContratoOcorrenciaContratoOcorrenciaNotificacaoWC" ;
      }

      public override String GetPgmdesc( )
      {
         return "Contrato Ocorrencia Contrato Ocorrencia Notificacao WC" ;
      }

      protected void WB7A0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "contratoocorrenciacontratoocorrencianotificacaowc.aspx");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
               context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
            }
            wb_table1_2_7A2( true) ;
         }
         else
         {
            wb_table1_2_7A2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_7A2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratoOcorrencia_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A294ContratoOcorrencia_Codigo), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A294ContratoOcorrencia_Codigo), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoOcorrencia_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtContratoOcorrencia_Codigo_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContratoOcorrenciaContratoOcorrenciaNotificacaoWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 100,'" + sPrefix + "',false,'" + sGXsfl_84_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV21DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(100, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,100);\"");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 101,'" + sPrefix + "',false,'" + sGXsfl_84_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfcontratoocorrencianotificacao_data_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoocorrencianotificacao_data_Internalname, context.localUtil.Format(AV39TFContratoOcorrenciaNotificacao_Data, "99/99/99"), context.localUtil.Format( AV39TFContratoOcorrenciaNotificacao_Data, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,101);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoocorrencianotificacao_data_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoocorrencianotificacao_data_Visible, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoOcorrenciaContratoOcorrenciaNotificacaoWC.htm");
            GxWebStd.gx_bitmap( context, edtavTfcontratoocorrencianotificacao_data_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfcontratoocorrencianotificacao_data_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ContratoOcorrenciaContratoOcorrenciaNotificacaoWC.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 102,'" + sPrefix + "',false,'" + sGXsfl_84_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfcontratoocorrencianotificacao_data_to_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoocorrencianotificacao_data_to_Internalname, context.localUtil.Format(AV40TFContratoOcorrenciaNotificacao_Data_To, "99/99/99"), context.localUtil.Format( AV40TFContratoOcorrenciaNotificacao_Data_To, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,102);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoocorrencianotificacao_data_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoocorrencianotificacao_data_to_Visible, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoOcorrenciaContratoOcorrenciaNotificacaoWC.htm");
            GxWebStd.gx_bitmap( context, edtavTfcontratoocorrencianotificacao_data_to_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfcontratoocorrencianotificacao_data_to_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ContratoOcorrenciaContratoOcorrenciaNotificacaoWC.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divDdo_contratoocorrencianotificacao_dataauxdates_Internalname, 1, 0, "px", 0, "px", "Invisible", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 104,'" + sPrefix + "',false,'" + sGXsfl_84_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_contratoocorrencianotificacao_dataauxdate_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_contratoocorrencianotificacao_dataauxdate_Internalname, context.localUtil.Format(AV41DDO_ContratoOcorrenciaNotificacao_DataAuxDate, "99/99/99"), context.localUtil.Format( AV41DDO_ContratoOcorrenciaNotificacao_DataAuxDate, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,104);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_contratoocorrencianotificacao_dataauxdate_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoOcorrenciaContratoOcorrenciaNotificacaoWC.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_contratoocorrencianotificacao_dataauxdate_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ContratoOcorrenciaContratoOcorrenciaNotificacaoWC.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 105,'" + sPrefix + "',false,'" + sGXsfl_84_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_contratoocorrencianotificacao_dataauxdateto_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_contratoocorrencianotificacao_dataauxdateto_Internalname, context.localUtil.Format(AV42DDO_ContratoOcorrenciaNotificacao_DataAuxDateTo, "99/99/99"), context.localUtil.Format( AV42DDO_ContratoOcorrenciaNotificacao_DataAuxDateTo, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,105);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_contratoocorrencianotificacao_dataauxdateto_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoOcorrenciaContratoOcorrenciaNotificacaoWC.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_contratoocorrencianotificacao_dataauxdateto_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ContratoOcorrenciaContratoOcorrenciaNotificacaoWC.htm");
            context.WriteHtmlTextNl( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 106,'" + sPrefix + "',false,'" + sGXsfl_84_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoocorrencianotificacao_prazo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV45TFContratoOcorrenciaNotificacao_Prazo), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV45TFContratoOcorrenciaNotificacao_Prazo), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,106);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoocorrencianotificacao_prazo_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoocorrencianotificacao_prazo_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoOcorrenciaContratoOcorrenciaNotificacaoWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 107,'" + sPrefix + "',false,'" + sGXsfl_84_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoocorrencianotificacao_prazo_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV46TFContratoOcorrenciaNotificacao_Prazo_To), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV46TFContratoOcorrenciaNotificacao_Prazo_To), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,107);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoocorrencianotificacao_prazo_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoocorrencianotificacao_prazo_to_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoOcorrenciaContratoOcorrenciaNotificacaoWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 108,'" + sPrefix + "',false,'" + sGXsfl_84_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoocorrencianotificacao_descricao_Internalname, AV49TFContratoOcorrenciaNotificacao_Descricao, StringUtil.RTrim( context.localUtil.Format( AV49TFContratoOcorrenciaNotificacao_Descricao, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,108);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoocorrencianotificacao_descricao_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoocorrencianotificacao_descricao_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContratoOcorrenciaContratoOcorrenciaNotificacaoWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 109,'" + sPrefix + "',false,'" + sGXsfl_84_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoocorrencianotificacao_descricao_sel_Internalname, AV50TFContratoOcorrenciaNotificacao_Descricao_Sel, StringUtil.RTrim( context.localUtil.Format( AV50TFContratoOcorrenciaNotificacao_Descricao_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,109);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoocorrencianotificacao_descricao_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoocorrencianotificacao_descricao_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContratoOcorrenciaContratoOcorrenciaNotificacaoWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 110,'" + sPrefix + "',false,'" + sGXsfl_84_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfcontratoocorrencianotificacao_cumprido_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoocorrencianotificacao_cumprido_Internalname, context.localUtil.Format(AV53TFContratoOcorrenciaNotificacao_Cumprido, "99/99/99"), context.localUtil.Format( AV53TFContratoOcorrenciaNotificacao_Cumprido, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,110);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoocorrencianotificacao_cumprido_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoocorrencianotificacao_cumprido_Visible, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoOcorrenciaContratoOcorrenciaNotificacaoWC.htm");
            GxWebStd.gx_bitmap( context, edtavTfcontratoocorrencianotificacao_cumprido_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfcontratoocorrencianotificacao_cumprido_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ContratoOcorrenciaContratoOcorrenciaNotificacaoWC.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 111,'" + sPrefix + "',false,'" + sGXsfl_84_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfcontratoocorrencianotificacao_cumprido_to_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoocorrencianotificacao_cumprido_to_Internalname, context.localUtil.Format(AV54TFContratoOcorrenciaNotificacao_Cumprido_To, "99/99/99"), context.localUtil.Format( AV54TFContratoOcorrenciaNotificacao_Cumprido_To, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,111);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoocorrencianotificacao_cumprido_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoocorrencianotificacao_cumprido_to_Visible, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoOcorrenciaContratoOcorrenciaNotificacaoWC.htm");
            GxWebStd.gx_bitmap( context, edtavTfcontratoocorrencianotificacao_cumprido_to_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfcontratoocorrencianotificacao_cumprido_to_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ContratoOcorrenciaContratoOcorrenciaNotificacaoWC.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divDdo_contratoocorrencianotificacao_cumpridoauxdates_Internalname, 1, 0, "px", 0, "px", "Invisible", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 113,'" + sPrefix + "',false,'" + sGXsfl_84_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_contratoocorrencianotificacao_cumpridoauxdate_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_contratoocorrencianotificacao_cumpridoauxdate_Internalname, context.localUtil.Format(AV55DDO_ContratoOcorrenciaNotificacao_CumpridoAuxDate, "99/99/99"), context.localUtil.Format( AV55DDO_ContratoOcorrenciaNotificacao_CumpridoAuxDate, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,113);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_contratoocorrencianotificacao_cumpridoauxdate_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoOcorrenciaContratoOcorrenciaNotificacaoWC.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_contratoocorrencianotificacao_cumpridoauxdate_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ContratoOcorrenciaContratoOcorrenciaNotificacaoWC.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 114,'" + sPrefix + "',false,'" + sGXsfl_84_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_contratoocorrencianotificacao_cumpridoauxdateto_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_contratoocorrencianotificacao_cumpridoauxdateto_Internalname, context.localUtil.Format(AV56DDO_ContratoOcorrenciaNotificacao_CumpridoAuxDateTo, "99/99/99"), context.localUtil.Format( AV56DDO_ContratoOcorrenciaNotificacao_CumpridoAuxDateTo, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,114);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_contratoocorrencianotificacao_cumpridoauxdateto_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoOcorrenciaContratoOcorrenciaNotificacaoWC.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_contratoocorrencianotificacao_cumpridoauxdateto_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ContratoOcorrenciaContratoOcorrenciaNotificacaoWC.htm");
            context.WriteHtmlTextNl( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 115,'" + sPrefix + "',false,'" + sGXsfl_84_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoocorrencianotificacao_protocolo_Internalname, AV59TFContratoOcorrenciaNotificacao_Protocolo, StringUtil.RTrim( context.localUtil.Format( AV59TFContratoOcorrenciaNotificacao_Protocolo, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,115);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoocorrencianotificacao_protocolo_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoocorrencianotificacao_protocolo_Visible, 1, 0, "text", "", 20, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContratoOcorrenciaContratoOcorrenciaNotificacaoWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 116,'" + sPrefix + "',false,'" + sGXsfl_84_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoocorrencianotificacao_protocolo_sel_Internalname, AV60TFContratoOcorrenciaNotificacao_Protocolo_Sel, StringUtil.RTrim( context.localUtil.Format( AV60TFContratoOcorrenciaNotificacao_Protocolo_Sel, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,116);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoocorrencianotificacao_protocolo_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoocorrencianotificacao_protocolo_sel_Visible, 1, 0, "text", "", 20, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContratoOcorrenciaContratoOcorrenciaNotificacaoWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 117,'" + sPrefix + "',false,'" + sGXsfl_84_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoocorrencianotificacao_responsavel_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV63TFContratoOcorrenciaNotificacao_Responsavel), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV63TFContratoOcorrenciaNotificacao_Responsavel), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,117);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoocorrencianotificacao_responsavel_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoocorrencianotificacao_responsavel_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoOcorrenciaContratoOcorrenciaNotificacaoWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 118,'" + sPrefix + "',false,'" + sGXsfl_84_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoocorrencianotificacao_responsavel_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV64TFContratoOcorrenciaNotificacao_Responsavel_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV64TFContratoOcorrenciaNotificacao_Responsavel_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,118);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoocorrencianotificacao_responsavel_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoocorrencianotificacao_responsavel_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoOcorrenciaContratoOcorrenciaNotificacaoWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 119,'" + sPrefix + "',false,'" + sGXsfl_84_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoocorrencianotificacao_nome_Internalname, StringUtil.RTrim( AV67TFContratoOcorrenciaNotificacao_Nome), StringUtil.RTrim( context.localUtil.Format( AV67TFContratoOcorrenciaNotificacao_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,119);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoocorrencianotificacao_nome_Jsonclick, 0, "BootstrapAttribute100", "", "", "", edtavTfcontratoocorrencianotificacao_nome_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContratoOcorrenciaContratoOcorrenciaNotificacaoWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 120,'" + sPrefix + "',false,'" + sGXsfl_84_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoocorrencianotificacao_nome_sel_Internalname, StringUtil.RTrim( AV68TFContratoOcorrenciaNotificacao_Nome_Sel), StringUtil.RTrim( context.localUtil.Format( AV68TFContratoOcorrenciaNotificacao_Nome_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,120);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoocorrencianotificacao_nome_sel_Jsonclick, 0, "BootstrapAttribute100", "", "", "", edtavTfcontratoocorrencianotificacao_nome_sel_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContratoOcorrenciaContratoOcorrenciaNotificacaoWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 121,'" + sPrefix + "',false,'" + sGXsfl_84_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoocorrencianotificacao_docto_Internalname, AV71TFContratoOcorrenciaNotificacao_Docto, StringUtil.RTrim( context.localUtil.Format( AV71TFContratoOcorrenciaNotificacao_Docto, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,121);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoocorrencianotificacao_docto_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoocorrencianotificacao_docto_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContratoOcorrenciaContratoOcorrenciaNotificacaoWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 122,'" + sPrefix + "',false,'" + sGXsfl_84_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoocorrencianotificacao_docto_sel_Internalname, AV72TFContratoOcorrenciaNotificacao_Docto_Sel, StringUtil.RTrim( context.localUtil.Format( AV72TFContratoOcorrenciaNotificacao_Docto_Sel, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,122);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoocorrencianotificacao_docto_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoocorrencianotificacao_docto_sel_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContratoOcorrenciaContratoOcorrenciaNotificacaoWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DATAContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 124,'" + sPrefix + "',false,'" + sGXsfl_84_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratoocorrencianotificacao_datatitlecontrolidtoreplace_Internalname, AV43ddo_ContratoOcorrenciaNotificacao_DataTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,124);\"", 0, edtavDdo_contratoocorrencianotificacao_datatitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_ContratoOcorrenciaContratoOcorrenciaNotificacaoWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 126,'" + sPrefix + "',false,'" + sGXsfl_84_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratoocorrencianotificacao_prazotitlecontrolidtoreplace_Internalname, AV47ddo_ContratoOcorrenciaNotificacao_PrazoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,126);\"", 0, edtavDdo_contratoocorrencianotificacao_prazotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_ContratoOcorrenciaContratoOcorrenciaNotificacaoWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 128,'" + sPrefix + "',false,'" + sGXsfl_84_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratoocorrencianotificacao_descricaotitlecontrolidtoreplace_Internalname, AV51ddo_ContratoOcorrenciaNotificacao_DescricaoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,128);\"", 0, edtavDdo_contratoocorrencianotificacao_descricaotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_ContratoOcorrenciaContratoOcorrenciaNotificacaoWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 130,'" + sPrefix + "',false,'" + sGXsfl_84_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratoocorrencianotificacao_cumpridotitlecontrolidtoreplace_Internalname, AV57ddo_ContratoOcorrenciaNotificacao_CumpridoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,130);\"", 0, edtavDdo_contratoocorrencianotificacao_cumpridotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_ContratoOcorrenciaContratoOcorrenciaNotificacaoWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 132,'" + sPrefix + "',false,'" + sGXsfl_84_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratoocorrencianotificacao_protocolotitlecontrolidtoreplace_Internalname, AV61ddo_ContratoOcorrenciaNotificacao_ProtocoloTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,132);\"", 0, edtavDdo_contratoocorrencianotificacao_protocolotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_ContratoOcorrenciaContratoOcorrenciaNotificacaoWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVELContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 134,'" + sPrefix + "',false,'" + sGXsfl_84_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratoocorrencianotificacao_responsaveltitlecontrolidtoreplace_Internalname, AV65ddo_ContratoOcorrenciaNotificacao_ResponsavelTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,134);\"", 0, edtavDdo_contratoocorrencianotificacao_responsaveltitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_ContratoOcorrenciaContratoOcorrenciaNotificacaoWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_NOMEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 136,'" + sPrefix + "',false,'" + sGXsfl_84_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratoocorrencianotificacao_nometitlecontrolidtoreplace_Internalname, AV69ddo_ContratoOcorrenciaNotificacao_NomeTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,136);\"", 0, edtavDdo_contratoocorrencianotificacao_nometitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_ContratoOcorrenciaContratoOcorrenciaNotificacaoWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 138,'" + sPrefix + "',false,'" + sGXsfl_84_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratoocorrencianotificacao_doctotitlecontrolidtoreplace_Internalname, AV73ddo_ContratoOcorrenciaNotificacao_DoctoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,138);\"", 0, edtavDdo_contratoocorrencianotificacao_doctotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_ContratoOcorrenciaContratoOcorrenciaNotificacaoWC.htm");
         }
         wbLoad = true;
      }

      protected void START7A2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Contrato Ocorrencia Contrato Ocorrencia Notificacao WC", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUP7A0( ) ;
            }
         }
      }

      protected void WS7A2( )
      {
         START7A2( ) ;
         EVT7A2( ) ;
      }

      protected void EVT7A2( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP7A0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP7A0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E117A2 */
                                    E117A2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP7A0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E127A2 */
                                    E127A2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP7A0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E137A2 */
                                    E137A2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP7A0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E147A2 */
                                    E147A2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP7A0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E157A2 */
                                    E157A2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP7A0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E167A2 */
                                    E167A2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP7A0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E177A2 */
                                    E177A2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP7A0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E187A2 */
                                    E187A2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP7A0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E197A2 */
                                    E197A2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP7A0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E207A2 */
                                    E207A2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP7A0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E217A2 */
                                    E217A2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP7A0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E227A2 */
                                    E227A2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOINSERT'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP7A0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E237A2 */
                                    E237A2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP7A0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E247A2 */
                                    E247A2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP7A0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E257A2 */
                                    E257A2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP7A0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E267A2 */
                                    E267A2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP7A0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    GX_FocusControl = cmbavOrderedby_Internalname;
                                    context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP7A0( ) ;
                              }
                              nGXsfl_84_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_84_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_84_idx), 4, 0)), 4, "0");
                              SubsflControlProps_842( ) ;
                              AV35Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV35Delete)) ? AV80Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV35Delete))));
                              AV36Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV36Update)) ? AV81Update_GXI : context.convertURL( context.PathToRelativeUrl( AV36Update))));
                              A298ContratoOcorrenciaNotificacao_Data = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtContratoOcorrenciaNotificacao_Data_Internalname), 0));
                              A299ContratoOcorrenciaNotificacao_Prazo = (short)(context.localUtil.CToN( cgiGet( edtContratoOcorrenciaNotificacao_Prazo_Internalname), ",", "."));
                              A300ContratoOcorrenciaNotificacao_Descricao = StringUtil.Upper( cgiGet( edtContratoOcorrenciaNotificacao_Descricao_Internalname));
                              A301ContratoOcorrenciaNotificacao_Cumprido = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtContratoOcorrenciaNotificacao_Cumprido_Internalname), 0));
                              n301ContratoOcorrenciaNotificacao_Cumprido = false;
                              A302ContratoOcorrenciaNotificacao_Protocolo = cgiGet( edtContratoOcorrenciaNotificacao_Protocolo_Internalname);
                              n302ContratoOcorrenciaNotificacao_Protocolo = false;
                              A303ContratoOcorrenciaNotificacao_Responsavel = (int)(context.localUtil.CToN( cgiGet( edtContratoOcorrenciaNotificacao_Responsavel_Internalname), ",", "."));
                              A304ContratoOcorrenciaNotificacao_Nome = StringUtil.Upper( cgiGet( edtContratoOcorrenciaNotificacao_Nome_Internalname));
                              n304ContratoOcorrenciaNotificacao_Nome = false;
                              A306ContratoOcorrenciaNotificacao_Docto = cgiGet( edtContratoOcorrenciaNotificacao_Docto_Internalname);
                              n306ContratoOcorrenciaNotificacao_Docto = false;
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E277A2 */
                                          E277A2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E287A2 */
                                          E287A2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E297A2 */
                                          E297A2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          if ( ! wbErr )
                                          {
                                             Rfr0gs = false;
                                             /* Set Refresh If Orderedby Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV14OrderedBy )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Ordereddsc Changed */
                                             if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV15OrderedDsc )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersselector1 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR1"), AV16DynamicFiltersSelector1) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Contratoocorrencianotificacao_data1 Changed */
                                             if ( context.localUtil.CToT( cgiGet( sPrefix+"GXH_vCONTRATOOCORRENCIANOTIFICACAO_DATA1"), 0) != AV17ContratoOcorrenciaNotificacao_Data1 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Contratoocorrencianotificacao_data_to1 Changed */
                                             if ( context.localUtil.CToT( cgiGet( sPrefix+"GXH_vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO1"), 0) != AV18ContratoOcorrenciaNotificacao_Data_To1 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Contratoocorrencianotificacao_prazo1 Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vCONTRATOOCORRENCIANOTIFICACAO_PRAZO1"), ",", ".") != Convert.ToDecimal( AV19ContratoOcorrenciaNotificacao_Prazo1 )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Contratoocorrencianotificacao_prazo_to1 Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vCONTRATOOCORRENCIANOTIFICACAO_PRAZO_TO1"), ",", ".") != Convert.ToDecimal( AV20ContratoOcorrenciaNotificacao_Prazo_To1 )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersselector2 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR2"), AV22DynamicFiltersSelector2) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Contratoocorrencianotificacao_data2 Changed */
                                             if ( context.localUtil.CToT( cgiGet( sPrefix+"GXH_vCONTRATOOCORRENCIANOTIFICACAO_DATA2"), 0) != AV23ContratoOcorrenciaNotificacao_Data2 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Contratoocorrencianotificacao_data_to2 Changed */
                                             if ( context.localUtil.CToT( cgiGet( sPrefix+"GXH_vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO2"), 0) != AV24ContratoOcorrenciaNotificacao_Data_To2 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Contratoocorrencianotificacao_prazo2 Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vCONTRATOOCORRENCIANOTIFICACAO_PRAZO2"), ",", ".") != Convert.ToDecimal( AV25ContratoOcorrenciaNotificacao_Prazo2 )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Contratoocorrencianotificacao_prazo_to2 Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vCONTRATOOCORRENCIANOTIFICACAO_PRAZO_TO2"), ",", ".") != Convert.ToDecimal( AV26ContratoOcorrenciaNotificacao_Prazo_To2 )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                             if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSENABLED2")) != AV21DynamicFiltersEnabled2 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontratoocorrencianotificacao_data Changed */
                                             if ( context.localUtil.CToT( cgiGet( sPrefix+"GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_DATA"), 0) != AV39TFContratoOcorrenciaNotificacao_Data )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontratoocorrencianotificacao_data_to Changed */
                                             if ( context.localUtil.CToT( cgiGet( sPrefix+"GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_DATA_TO"), 0) != AV40TFContratoOcorrenciaNotificacao_Data_To )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontratoocorrencianotificacao_prazo Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO"), ",", ".") != Convert.ToDecimal( AV45TFContratoOcorrenciaNotificacao_Prazo )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontratoocorrencianotificacao_prazo_to Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO_TO"), ",", ".") != Convert.ToDecimal( AV46TFContratoOcorrenciaNotificacao_Prazo_To )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontratoocorrencianotificacao_descricao Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO"), AV49TFContratoOcorrenciaNotificacao_Descricao) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontratoocorrencianotificacao_descricao_sel Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_SEL"), AV50TFContratoOcorrenciaNotificacao_Descricao_Sel) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontratoocorrencianotificacao_cumprido Changed */
                                             if ( context.localUtil.CToT( cgiGet( sPrefix+"GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO"), 0) != AV53TFContratoOcorrenciaNotificacao_Cumprido )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontratoocorrencianotificacao_cumprido_to Changed */
                                             if ( context.localUtil.CToT( cgiGet( sPrefix+"GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_TO"), 0) != AV54TFContratoOcorrenciaNotificacao_Cumprido_To )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontratoocorrencianotificacao_protocolo Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO"), AV59TFContratoOcorrenciaNotificacao_Protocolo) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontratoocorrencianotificacao_protocolo_sel Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_SEL"), AV60TFContratoOcorrenciaNotificacao_Protocolo_Sel) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontratoocorrencianotificacao_responsavel Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL"), ",", ".") != Convert.ToDecimal( AV63TFContratoOcorrenciaNotificacao_Responsavel )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontratoocorrencianotificacao_responsavel_to Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_TO"), ",", ".") != Convert.ToDecimal( AV64TFContratoOcorrenciaNotificacao_Responsavel_To )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontratoocorrencianotificacao_nome Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_NOME"), AV67TFContratoOcorrenciaNotificacao_Nome) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontratoocorrencianotificacao_nome_sel Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_NOME_SEL"), AV68TFContratoOcorrenciaNotificacao_Nome_Sel) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontratoocorrencianotificacao_docto Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_DOCTO"), AV71TFContratoOcorrenciaNotificacao_Docto) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontratoocorrencianotificacao_docto_sel Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_DOCTO_SEL"), AV72TFContratoOcorrenciaNotificacao_Docto_Sel) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             if ( ! Rfr0gs )
                                             {
                                             }
                                             dynload_actions( ) ;
                                          }
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                                    {
                                       STRUP7A0( ) ;
                                    }
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE7A2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseForm7A2( ) ;
            }
         }
      }

      protected void PA7A2( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV14OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("CONTRATOOCORRENCIANOTIFICACAO_DATA", "de emiss�o", 0);
            cmbavDynamicfiltersselector1.addItem("CONTRATOOCORRENCIANOTIFICACAO_PRAZO", "de cumprimento", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV16DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV16DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("CONTRATOOCORRENCIANOTIFICACAO_DATA", "de emiss�o", 0);
            cmbavDynamicfiltersselector2.addItem("CONTRATOOCORRENCIANOTIFICACAO_PRAZO", "de cumprimento", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV22DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV22DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22DynamicFiltersSelector2", AV22DynamicFiltersSelector2);
            }
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_842( ) ;
         while ( nGXsfl_84_idx <= nRC_GXsfl_84 )
         {
            sendrow_842( ) ;
            nGXsfl_84_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_84_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_84_idx+1));
            sGXsfl_84_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_84_idx), 4, 0)), 4, "0");
            SubsflControlProps_842( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV14OrderedBy ,
                                       bool AV15OrderedDsc ,
                                       String AV16DynamicFiltersSelector1 ,
                                       DateTime AV17ContratoOcorrenciaNotificacao_Data1 ,
                                       DateTime AV18ContratoOcorrenciaNotificacao_Data_To1 ,
                                       short AV19ContratoOcorrenciaNotificacao_Prazo1 ,
                                       short AV20ContratoOcorrenciaNotificacao_Prazo_To1 ,
                                       String AV22DynamicFiltersSelector2 ,
                                       DateTime AV23ContratoOcorrenciaNotificacao_Data2 ,
                                       DateTime AV24ContratoOcorrenciaNotificacao_Data_To2 ,
                                       short AV25ContratoOcorrenciaNotificacao_Prazo2 ,
                                       short AV26ContratoOcorrenciaNotificacao_Prazo_To2 ,
                                       bool AV21DynamicFiltersEnabled2 ,
                                       DateTime AV39TFContratoOcorrenciaNotificacao_Data ,
                                       DateTime AV40TFContratoOcorrenciaNotificacao_Data_To ,
                                       short AV45TFContratoOcorrenciaNotificacao_Prazo ,
                                       short AV46TFContratoOcorrenciaNotificacao_Prazo_To ,
                                       String AV49TFContratoOcorrenciaNotificacao_Descricao ,
                                       String AV50TFContratoOcorrenciaNotificacao_Descricao_Sel ,
                                       DateTime AV53TFContratoOcorrenciaNotificacao_Cumprido ,
                                       DateTime AV54TFContratoOcorrenciaNotificacao_Cumprido_To ,
                                       String AV59TFContratoOcorrenciaNotificacao_Protocolo ,
                                       String AV60TFContratoOcorrenciaNotificacao_Protocolo_Sel ,
                                       int AV63TFContratoOcorrenciaNotificacao_Responsavel ,
                                       int AV64TFContratoOcorrenciaNotificacao_Responsavel_To ,
                                       String AV67TFContratoOcorrenciaNotificacao_Nome ,
                                       String AV68TFContratoOcorrenciaNotificacao_Nome_Sel ,
                                       String AV71TFContratoOcorrenciaNotificacao_Docto ,
                                       String AV72TFContratoOcorrenciaNotificacao_Docto_Sel ,
                                       int AV7ContratoOcorrencia_Codigo ,
                                       String AV43ddo_ContratoOcorrenciaNotificacao_DataTitleControlIdToReplace ,
                                       String AV47ddo_ContratoOcorrenciaNotificacao_PrazoTitleControlIdToReplace ,
                                       String AV51ddo_ContratoOcorrenciaNotificacao_DescricaoTitleControlIdToReplace ,
                                       String AV57ddo_ContratoOcorrenciaNotificacao_CumpridoTitleControlIdToReplace ,
                                       String AV61ddo_ContratoOcorrenciaNotificacao_ProtocoloTitleControlIdToReplace ,
                                       String AV65ddo_ContratoOcorrenciaNotificacao_ResponsavelTitleControlIdToReplace ,
                                       String AV69ddo_ContratoOcorrenciaNotificacao_NomeTitleControlIdToReplace ,
                                       String AV73ddo_ContratoOcorrenciaNotificacao_DoctoTitleControlIdToReplace ,
                                       String AV82Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV11GridState ,
                                       bool AV34DynamicFiltersIgnoreFirst ,
                                       bool AV33DynamicFiltersRemoving ,
                                       int A297ContratoOcorrenciaNotificacao_Codigo ,
                                       int A294ContratoOcorrencia_Codigo ,
                                       int A303ContratoOcorrenciaNotificacao_Responsavel ,
                                       String sPrefix )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RF7A2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOOCORRENCIANOTIFICACAO_DATA", GetSecureSignedToken( sPrefix, A298ContratoOcorrenciaNotificacao_Data));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATOOCORRENCIANOTIFICACAO_DATA", context.localUtil.Format(A298ContratoOcorrenciaNotificacao_Data, "99/99/99"));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOOCORRENCIANOTIFICACAO_PRAZO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A299ContratoOcorrenciaNotificacao_Prazo), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATOOCORRENCIANOTIFICACAO_PRAZO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A299ContratoOcorrenciaNotificacao_Prazo), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A300ContratoOcorrenciaNotificacao_Descricao, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO", A300ContratoOcorrenciaNotificacao_Descricao);
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO", GetSecureSignedToken( sPrefix, A301ContratoOcorrenciaNotificacao_Cumprido));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO", context.localUtil.Format(A301ContratoOcorrenciaNotificacao_Cumprido, "99/99/99"));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A302ContratoOcorrenciaNotificacao_Protocolo, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO", A302ContratoOcorrenciaNotificacao_Protocolo);
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A303ContratoOcorrenciaNotificacao_Responsavel), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL", StringUtil.LTrim( StringUtil.NToC( (decimal)(A303ContratoOcorrenciaNotificacao_Responsavel), 6, 0, ".", "")));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV14OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV16DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV16DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV22DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV22DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22DynamicFiltersSelector2", AV22DynamicFiltersSelector2);
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF7A2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV82Pgmname = "ContratoOcorrenciaContratoOcorrenciaNotificacaoWC";
         context.Gx_err = 0;
      }

      protected void RF7A2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 84;
         /* Execute user event: E287A2 */
         E287A2 ();
         nGXsfl_84_idx = 1;
         sGXsfl_84_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_84_idx), 4, 0)), 4, "0");
         SubsflControlProps_842( ) ;
         nGXsfl_84_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", sPrefix);
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_842( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV16DynamicFiltersSelector1 ,
                                                 AV17ContratoOcorrenciaNotificacao_Data1 ,
                                                 AV18ContratoOcorrenciaNotificacao_Data_To1 ,
                                                 AV19ContratoOcorrenciaNotificacao_Prazo1 ,
                                                 AV20ContratoOcorrenciaNotificacao_Prazo_To1 ,
                                                 AV21DynamicFiltersEnabled2 ,
                                                 AV22DynamicFiltersSelector2 ,
                                                 AV23ContratoOcorrenciaNotificacao_Data2 ,
                                                 AV24ContratoOcorrenciaNotificacao_Data_To2 ,
                                                 AV25ContratoOcorrenciaNotificacao_Prazo2 ,
                                                 AV26ContratoOcorrenciaNotificacao_Prazo_To2 ,
                                                 AV39TFContratoOcorrenciaNotificacao_Data ,
                                                 AV40TFContratoOcorrenciaNotificacao_Data_To ,
                                                 AV45TFContratoOcorrenciaNotificacao_Prazo ,
                                                 AV46TFContratoOcorrenciaNotificacao_Prazo_To ,
                                                 AV50TFContratoOcorrenciaNotificacao_Descricao_Sel ,
                                                 AV49TFContratoOcorrenciaNotificacao_Descricao ,
                                                 AV53TFContratoOcorrenciaNotificacao_Cumprido ,
                                                 AV54TFContratoOcorrenciaNotificacao_Cumprido_To ,
                                                 AV60TFContratoOcorrenciaNotificacao_Protocolo_Sel ,
                                                 AV59TFContratoOcorrenciaNotificacao_Protocolo ,
                                                 AV63TFContratoOcorrenciaNotificacao_Responsavel ,
                                                 AV64TFContratoOcorrenciaNotificacao_Responsavel_To ,
                                                 AV68TFContratoOcorrenciaNotificacao_Nome_Sel ,
                                                 AV67TFContratoOcorrenciaNotificacao_Nome ,
                                                 AV72TFContratoOcorrenciaNotificacao_Docto_Sel ,
                                                 AV71TFContratoOcorrenciaNotificacao_Docto ,
                                                 A298ContratoOcorrenciaNotificacao_Data ,
                                                 A299ContratoOcorrenciaNotificacao_Prazo ,
                                                 A300ContratoOcorrenciaNotificacao_Descricao ,
                                                 A301ContratoOcorrenciaNotificacao_Cumprido ,
                                                 A302ContratoOcorrenciaNotificacao_Protocolo ,
                                                 A303ContratoOcorrenciaNotificacao_Responsavel ,
                                                 A304ContratoOcorrenciaNotificacao_Nome ,
                                                 A306ContratoOcorrenciaNotificacao_Docto ,
                                                 AV14OrderedBy ,
                                                 AV15OrderedDsc ,
                                                 A294ContratoOcorrencia_Codigo ,
                                                 AV7ContratoOcorrencia_Codigo },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT,
                                                 TypeConstants.SHORT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING,
                                                 TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.STRING,
                                                 TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT,
                                                 TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                                 }
            });
            lV49TFContratoOcorrenciaNotificacao_Descricao = StringUtil.Concat( StringUtil.RTrim( AV49TFContratoOcorrenciaNotificacao_Descricao), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV49TFContratoOcorrenciaNotificacao_Descricao", AV49TFContratoOcorrenciaNotificacao_Descricao);
            lV59TFContratoOcorrenciaNotificacao_Protocolo = StringUtil.Concat( StringUtil.RTrim( AV59TFContratoOcorrenciaNotificacao_Protocolo), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV59TFContratoOcorrenciaNotificacao_Protocolo", AV59TFContratoOcorrenciaNotificacao_Protocolo);
            lV67TFContratoOcorrenciaNotificacao_Nome = StringUtil.PadR( StringUtil.RTrim( AV67TFContratoOcorrenciaNotificacao_Nome), 100, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV67TFContratoOcorrenciaNotificacao_Nome", AV67TFContratoOcorrenciaNotificacao_Nome);
            lV71TFContratoOcorrenciaNotificacao_Docto = StringUtil.Concat( StringUtil.RTrim( AV71TFContratoOcorrenciaNotificacao_Docto), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV71TFContratoOcorrenciaNotificacao_Docto", AV71TFContratoOcorrenciaNotificacao_Docto);
            /* Using cursor H007A2 */
            pr_default.execute(0, new Object[] {AV7ContratoOcorrencia_Codigo, AV17ContratoOcorrenciaNotificacao_Data1, AV18ContratoOcorrenciaNotificacao_Data_To1, AV19ContratoOcorrenciaNotificacao_Prazo1, AV20ContratoOcorrenciaNotificacao_Prazo_To1, AV23ContratoOcorrenciaNotificacao_Data2, AV24ContratoOcorrenciaNotificacao_Data_To2, AV25ContratoOcorrenciaNotificacao_Prazo2, AV26ContratoOcorrenciaNotificacao_Prazo_To2, AV39TFContratoOcorrenciaNotificacao_Data, AV40TFContratoOcorrenciaNotificacao_Data_To, AV45TFContratoOcorrenciaNotificacao_Prazo, AV46TFContratoOcorrenciaNotificacao_Prazo_To, lV49TFContratoOcorrenciaNotificacao_Descricao, AV50TFContratoOcorrenciaNotificacao_Descricao_Sel, AV53TFContratoOcorrenciaNotificacao_Cumprido, AV54TFContratoOcorrenciaNotificacao_Cumprido_To, lV59TFContratoOcorrenciaNotificacao_Protocolo, AV60TFContratoOcorrenciaNotificacao_Protocolo_Sel, AV63TFContratoOcorrenciaNotificacao_Responsavel, AV64TFContratoOcorrenciaNotificacao_Responsavel_To, lV67TFContratoOcorrenciaNotificacao_Nome, AV68TFContratoOcorrenciaNotificacao_Nome_Sel, lV71TFContratoOcorrenciaNotificacao_Docto, AV72TFContratoOcorrenciaNotificacao_Docto_Sel, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_84_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A295ContratoOcorrencia_Data = H007A2_A295ContratoOcorrencia_Data[0];
               A297ContratoOcorrenciaNotificacao_Codigo = H007A2_A297ContratoOcorrenciaNotificacao_Codigo[0];
               A294ContratoOcorrencia_Codigo = H007A2_A294ContratoOcorrencia_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A294ContratoOcorrencia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A294ContratoOcorrencia_Codigo), 6, 0)));
               A306ContratoOcorrenciaNotificacao_Docto = H007A2_A306ContratoOcorrenciaNotificacao_Docto[0];
               n306ContratoOcorrenciaNotificacao_Docto = H007A2_n306ContratoOcorrenciaNotificacao_Docto[0];
               A304ContratoOcorrenciaNotificacao_Nome = H007A2_A304ContratoOcorrenciaNotificacao_Nome[0];
               n304ContratoOcorrenciaNotificacao_Nome = H007A2_n304ContratoOcorrenciaNotificacao_Nome[0];
               A303ContratoOcorrenciaNotificacao_Responsavel = H007A2_A303ContratoOcorrenciaNotificacao_Responsavel[0];
               A302ContratoOcorrenciaNotificacao_Protocolo = H007A2_A302ContratoOcorrenciaNotificacao_Protocolo[0];
               n302ContratoOcorrenciaNotificacao_Protocolo = H007A2_n302ContratoOcorrenciaNotificacao_Protocolo[0];
               A301ContratoOcorrenciaNotificacao_Cumprido = H007A2_A301ContratoOcorrenciaNotificacao_Cumprido[0];
               n301ContratoOcorrenciaNotificacao_Cumprido = H007A2_n301ContratoOcorrenciaNotificacao_Cumprido[0];
               A300ContratoOcorrenciaNotificacao_Descricao = H007A2_A300ContratoOcorrenciaNotificacao_Descricao[0];
               A299ContratoOcorrenciaNotificacao_Prazo = H007A2_A299ContratoOcorrenciaNotificacao_Prazo[0];
               A298ContratoOcorrenciaNotificacao_Data = H007A2_A298ContratoOcorrenciaNotificacao_Data[0];
               A295ContratoOcorrencia_Data = H007A2_A295ContratoOcorrencia_Data[0];
               A306ContratoOcorrenciaNotificacao_Docto = H007A2_A306ContratoOcorrenciaNotificacao_Docto[0];
               n306ContratoOcorrenciaNotificacao_Docto = H007A2_n306ContratoOcorrenciaNotificacao_Docto[0];
               A304ContratoOcorrenciaNotificacao_Nome = H007A2_A304ContratoOcorrenciaNotificacao_Nome[0];
               n304ContratoOcorrenciaNotificacao_Nome = H007A2_n304ContratoOcorrenciaNotificacao_Nome[0];
               /* Execute user event: E297A2 */
               E297A2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 84;
            WB7A0( ) ;
         }
         nGXsfl_84_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV16DynamicFiltersSelector1 ,
                                              AV17ContratoOcorrenciaNotificacao_Data1 ,
                                              AV18ContratoOcorrenciaNotificacao_Data_To1 ,
                                              AV19ContratoOcorrenciaNotificacao_Prazo1 ,
                                              AV20ContratoOcorrenciaNotificacao_Prazo_To1 ,
                                              AV21DynamicFiltersEnabled2 ,
                                              AV22DynamicFiltersSelector2 ,
                                              AV23ContratoOcorrenciaNotificacao_Data2 ,
                                              AV24ContratoOcorrenciaNotificacao_Data_To2 ,
                                              AV25ContratoOcorrenciaNotificacao_Prazo2 ,
                                              AV26ContratoOcorrenciaNotificacao_Prazo_To2 ,
                                              AV39TFContratoOcorrenciaNotificacao_Data ,
                                              AV40TFContratoOcorrenciaNotificacao_Data_To ,
                                              AV45TFContratoOcorrenciaNotificacao_Prazo ,
                                              AV46TFContratoOcorrenciaNotificacao_Prazo_To ,
                                              AV50TFContratoOcorrenciaNotificacao_Descricao_Sel ,
                                              AV49TFContratoOcorrenciaNotificacao_Descricao ,
                                              AV53TFContratoOcorrenciaNotificacao_Cumprido ,
                                              AV54TFContratoOcorrenciaNotificacao_Cumprido_To ,
                                              AV60TFContratoOcorrenciaNotificacao_Protocolo_Sel ,
                                              AV59TFContratoOcorrenciaNotificacao_Protocolo ,
                                              AV63TFContratoOcorrenciaNotificacao_Responsavel ,
                                              AV64TFContratoOcorrenciaNotificacao_Responsavel_To ,
                                              AV68TFContratoOcorrenciaNotificacao_Nome_Sel ,
                                              AV67TFContratoOcorrenciaNotificacao_Nome ,
                                              AV72TFContratoOcorrenciaNotificacao_Docto_Sel ,
                                              AV71TFContratoOcorrenciaNotificacao_Docto ,
                                              A298ContratoOcorrenciaNotificacao_Data ,
                                              A299ContratoOcorrenciaNotificacao_Prazo ,
                                              A300ContratoOcorrenciaNotificacao_Descricao ,
                                              A301ContratoOcorrenciaNotificacao_Cumprido ,
                                              A302ContratoOcorrenciaNotificacao_Protocolo ,
                                              A303ContratoOcorrenciaNotificacao_Responsavel ,
                                              A304ContratoOcorrenciaNotificacao_Nome ,
                                              A306ContratoOcorrenciaNotificacao_Docto ,
                                              AV14OrderedBy ,
                                              AV15OrderedDsc ,
                                              A294ContratoOcorrencia_Codigo ,
                                              AV7ContratoOcorrencia_Codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT,
                                              TypeConstants.SHORT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.STRING,
                                              TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT,
                                              TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV49TFContratoOcorrenciaNotificacao_Descricao = StringUtil.Concat( StringUtil.RTrim( AV49TFContratoOcorrenciaNotificacao_Descricao), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV49TFContratoOcorrenciaNotificacao_Descricao", AV49TFContratoOcorrenciaNotificacao_Descricao);
         lV59TFContratoOcorrenciaNotificacao_Protocolo = StringUtil.Concat( StringUtil.RTrim( AV59TFContratoOcorrenciaNotificacao_Protocolo), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV59TFContratoOcorrenciaNotificacao_Protocolo", AV59TFContratoOcorrenciaNotificacao_Protocolo);
         lV67TFContratoOcorrenciaNotificacao_Nome = StringUtil.PadR( StringUtil.RTrim( AV67TFContratoOcorrenciaNotificacao_Nome), 100, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV67TFContratoOcorrenciaNotificacao_Nome", AV67TFContratoOcorrenciaNotificacao_Nome);
         lV71TFContratoOcorrenciaNotificacao_Docto = StringUtil.Concat( StringUtil.RTrim( AV71TFContratoOcorrenciaNotificacao_Docto), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV71TFContratoOcorrenciaNotificacao_Docto", AV71TFContratoOcorrenciaNotificacao_Docto);
         /* Using cursor H007A3 */
         pr_default.execute(1, new Object[] {AV7ContratoOcorrencia_Codigo, AV17ContratoOcorrenciaNotificacao_Data1, AV18ContratoOcorrenciaNotificacao_Data_To1, AV19ContratoOcorrenciaNotificacao_Prazo1, AV20ContratoOcorrenciaNotificacao_Prazo_To1, AV23ContratoOcorrenciaNotificacao_Data2, AV24ContratoOcorrenciaNotificacao_Data_To2, AV25ContratoOcorrenciaNotificacao_Prazo2, AV26ContratoOcorrenciaNotificacao_Prazo_To2, AV39TFContratoOcorrenciaNotificacao_Data, AV40TFContratoOcorrenciaNotificacao_Data_To, AV45TFContratoOcorrenciaNotificacao_Prazo, AV46TFContratoOcorrenciaNotificacao_Prazo_To, lV49TFContratoOcorrenciaNotificacao_Descricao, AV50TFContratoOcorrenciaNotificacao_Descricao_Sel, AV53TFContratoOcorrenciaNotificacao_Cumprido, AV54TFContratoOcorrenciaNotificacao_Cumprido_To, lV59TFContratoOcorrenciaNotificacao_Protocolo, AV60TFContratoOcorrenciaNotificacao_Protocolo_Sel, AV63TFContratoOcorrenciaNotificacao_Responsavel, AV64TFContratoOcorrenciaNotificacao_Responsavel_To, lV67TFContratoOcorrenciaNotificacao_Nome, AV68TFContratoOcorrenciaNotificacao_Nome_Sel, lV71TFContratoOcorrenciaNotificacao_Docto, AV72TFContratoOcorrenciaNotificacao_Docto_Sel});
         GRID_nRecordCount = H007A3_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17ContratoOcorrenciaNotificacao_Data1, AV18ContratoOcorrenciaNotificacao_Data_To1, AV19ContratoOcorrenciaNotificacao_Prazo1, AV20ContratoOcorrenciaNotificacao_Prazo_To1, AV22DynamicFiltersSelector2, AV23ContratoOcorrenciaNotificacao_Data2, AV24ContratoOcorrenciaNotificacao_Data_To2, AV25ContratoOcorrenciaNotificacao_Prazo2, AV26ContratoOcorrenciaNotificacao_Prazo_To2, AV21DynamicFiltersEnabled2, AV39TFContratoOcorrenciaNotificacao_Data, AV40TFContratoOcorrenciaNotificacao_Data_To, AV45TFContratoOcorrenciaNotificacao_Prazo, AV46TFContratoOcorrenciaNotificacao_Prazo_To, AV49TFContratoOcorrenciaNotificacao_Descricao, AV50TFContratoOcorrenciaNotificacao_Descricao_Sel, AV53TFContratoOcorrenciaNotificacao_Cumprido, AV54TFContratoOcorrenciaNotificacao_Cumprido_To, AV59TFContratoOcorrenciaNotificacao_Protocolo, AV60TFContratoOcorrenciaNotificacao_Protocolo_Sel, AV63TFContratoOcorrenciaNotificacao_Responsavel, AV64TFContratoOcorrenciaNotificacao_Responsavel_To, AV67TFContratoOcorrenciaNotificacao_Nome, AV68TFContratoOcorrenciaNotificacao_Nome_Sel, AV71TFContratoOcorrenciaNotificacao_Docto, AV72TFContratoOcorrenciaNotificacao_Docto_Sel, AV7ContratoOcorrencia_Codigo, AV43ddo_ContratoOcorrenciaNotificacao_DataTitleControlIdToReplace, AV47ddo_ContratoOcorrenciaNotificacao_PrazoTitleControlIdToReplace, AV51ddo_ContratoOcorrenciaNotificacao_DescricaoTitleControlIdToReplace, AV57ddo_ContratoOcorrenciaNotificacao_CumpridoTitleControlIdToReplace, AV61ddo_ContratoOcorrenciaNotificacao_ProtocoloTitleControlIdToReplace, AV65ddo_ContratoOcorrenciaNotificacao_ResponsavelTitleControlIdToReplace, AV69ddo_ContratoOcorrenciaNotificacao_NomeTitleControlIdToReplace, AV73ddo_ContratoOcorrenciaNotificacao_DoctoTitleControlIdToReplace, AV82Pgmname, AV11GridState, AV34DynamicFiltersIgnoreFirst, AV33DynamicFiltersRemoving, A297ContratoOcorrenciaNotificacao_Codigo, A294ContratoOcorrencia_Codigo, A303ContratoOcorrenciaNotificacao_Responsavel, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17ContratoOcorrenciaNotificacao_Data1, AV18ContratoOcorrenciaNotificacao_Data_To1, AV19ContratoOcorrenciaNotificacao_Prazo1, AV20ContratoOcorrenciaNotificacao_Prazo_To1, AV22DynamicFiltersSelector2, AV23ContratoOcorrenciaNotificacao_Data2, AV24ContratoOcorrenciaNotificacao_Data_To2, AV25ContratoOcorrenciaNotificacao_Prazo2, AV26ContratoOcorrenciaNotificacao_Prazo_To2, AV21DynamicFiltersEnabled2, AV39TFContratoOcorrenciaNotificacao_Data, AV40TFContratoOcorrenciaNotificacao_Data_To, AV45TFContratoOcorrenciaNotificacao_Prazo, AV46TFContratoOcorrenciaNotificacao_Prazo_To, AV49TFContratoOcorrenciaNotificacao_Descricao, AV50TFContratoOcorrenciaNotificacao_Descricao_Sel, AV53TFContratoOcorrenciaNotificacao_Cumprido, AV54TFContratoOcorrenciaNotificacao_Cumprido_To, AV59TFContratoOcorrenciaNotificacao_Protocolo, AV60TFContratoOcorrenciaNotificacao_Protocolo_Sel, AV63TFContratoOcorrenciaNotificacao_Responsavel, AV64TFContratoOcorrenciaNotificacao_Responsavel_To, AV67TFContratoOcorrenciaNotificacao_Nome, AV68TFContratoOcorrenciaNotificacao_Nome_Sel, AV71TFContratoOcorrenciaNotificacao_Docto, AV72TFContratoOcorrenciaNotificacao_Docto_Sel, AV7ContratoOcorrencia_Codigo, AV43ddo_ContratoOcorrenciaNotificacao_DataTitleControlIdToReplace, AV47ddo_ContratoOcorrenciaNotificacao_PrazoTitleControlIdToReplace, AV51ddo_ContratoOcorrenciaNotificacao_DescricaoTitleControlIdToReplace, AV57ddo_ContratoOcorrenciaNotificacao_CumpridoTitleControlIdToReplace, AV61ddo_ContratoOcorrenciaNotificacao_ProtocoloTitleControlIdToReplace, AV65ddo_ContratoOcorrenciaNotificacao_ResponsavelTitleControlIdToReplace, AV69ddo_ContratoOcorrenciaNotificacao_NomeTitleControlIdToReplace, AV73ddo_ContratoOcorrenciaNotificacao_DoctoTitleControlIdToReplace, AV82Pgmname, AV11GridState, AV34DynamicFiltersIgnoreFirst, AV33DynamicFiltersRemoving, A297ContratoOcorrenciaNotificacao_Codigo, A294ContratoOcorrencia_Codigo, A303ContratoOcorrenciaNotificacao_Responsavel, sPrefix) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17ContratoOcorrenciaNotificacao_Data1, AV18ContratoOcorrenciaNotificacao_Data_To1, AV19ContratoOcorrenciaNotificacao_Prazo1, AV20ContratoOcorrenciaNotificacao_Prazo_To1, AV22DynamicFiltersSelector2, AV23ContratoOcorrenciaNotificacao_Data2, AV24ContratoOcorrenciaNotificacao_Data_To2, AV25ContratoOcorrenciaNotificacao_Prazo2, AV26ContratoOcorrenciaNotificacao_Prazo_To2, AV21DynamicFiltersEnabled2, AV39TFContratoOcorrenciaNotificacao_Data, AV40TFContratoOcorrenciaNotificacao_Data_To, AV45TFContratoOcorrenciaNotificacao_Prazo, AV46TFContratoOcorrenciaNotificacao_Prazo_To, AV49TFContratoOcorrenciaNotificacao_Descricao, AV50TFContratoOcorrenciaNotificacao_Descricao_Sel, AV53TFContratoOcorrenciaNotificacao_Cumprido, AV54TFContratoOcorrenciaNotificacao_Cumprido_To, AV59TFContratoOcorrenciaNotificacao_Protocolo, AV60TFContratoOcorrenciaNotificacao_Protocolo_Sel, AV63TFContratoOcorrenciaNotificacao_Responsavel, AV64TFContratoOcorrenciaNotificacao_Responsavel_To, AV67TFContratoOcorrenciaNotificacao_Nome, AV68TFContratoOcorrenciaNotificacao_Nome_Sel, AV71TFContratoOcorrenciaNotificacao_Docto, AV72TFContratoOcorrenciaNotificacao_Docto_Sel, AV7ContratoOcorrencia_Codigo, AV43ddo_ContratoOcorrenciaNotificacao_DataTitleControlIdToReplace, AV47ddo_ContratoOcorrenciaNotificacao_PrazoTitleControlIdToReplace, AV51ddo_ContratoOcorrenciaNotificacao_DescricaoTitleControlIdToReplace, AV57ddo_ContratoOcorrenciaNotificacao_CumpridoTitleControlIdToReplace, AV61ddo_ContratoOcorrenciaNotificacao_ProtocoloTitleControlIdToReplace, AV65ddo_ContratoOcorrenciaNotificacao_ResponsavelTitleControlIdToReplace, AV69ddo_ContratoOcorrenciaNotificacao_NomeTitleControlIdToReplace, AV73ddo_ContratoOcorrenciaNotificacao_DoctoTitleControlIdToReplace, AV82Pgmname, AV11GridState, AV34DynamicFiltersIgnoreFirst, AV33DynamicFiltersRemoving, A297ContratoOcorrenciaNotificacao_Codigo, A294ContratoOcorrencia_Codigo, A303ContratoOcorrenciaNotificacao_Responsavel, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17ContratoOcorrenciaNotificacao_Data1, AV18ContratoOcorrenciaNotificacao_Data_To1, AV19ContratoOcorrenciaNotificacao_Prazo1, AV20ContratoOcorrenciaNotificacao_Prazo_To1, AV22DynamicFiltersSelector2, AV23ContratoOcorrenciaNotificacao_Data2, AV24ContratoOcorrenciaNotificacao_Data_To2, AV25ContratoOcorrenciaNotificacao_Prazo2, AV26ContratoOcorrenciaNotificacao_Prazo_To2, AV21DynamicFiltersEnabled2, AV39TFContratoOcorrenciaNotificacao_Data, AV40TFContratoOcorrenciaNotificacao_Data_To, AV45TFContratoOcorrenciaNotificacao_Prazo, AV46TFContratoOcorrenciaNotificacao_Prazo_To, AV49TFContratoOcorrenciaNotificacao_Descricao, AV50TFContratoOcorrenciaNotificacao_Descricao_Sel, AV53TFContratoOcorrenciaNotificacao_Cumprido, AV54TFContratoOcorrenciaNotificacao_Cumprido_To, AV59TFContratoOcorrenciaNotificacao_Protocolo, AV60TFContratoOcorrenciaNotificacao_Protocolo_Sel, AV63TFContratoOcorrenciaNotificacao_Responsavel, AV64TFContratoOcorrenciaNotificacao_Responsavel_To, AV67TFContratoOcorrenciaNotificacao_Nome, AV68TFContratoOcorrenciaNotificacao_Nome_Sel, AV71TFContratoOcorrenciaNotificacao_Docto, AV72TFContratoOcorrenciaNotificacao_Docto_Sel, AV7ContratoOcorrencia_Codigo, AV43ddo_ContratoOcorrenciaNotificacao_DataTitleControlIdToReplace, AV47ddo_ContratoOcorrenciaNotificacao_PrazoTitleControlIdToReplace, AV51ddo_ContratoOcorrenciaNotificacao_DescricaoTitleControlIdToReplace, AV57ddo_ContratoOcorrenciaNotificacao_CumpridoTitleControlIdToReplace, AV61ddo_ContratoOcorrenciaNotificacao_ProtocoloTitleControlIdToReplace, AV65ddo_ContratoOcorrenciaNotificacao_ResponsavelTitleControlIdToReplace, AV69ddo_ContratoOcorrenciaNotificacao_NomeTitleControlIdToReplace, AV73ddo_ContratoOcorrenciaNotificacao_DoctoTitleControlIdToReplace, AV82Pgmname, AV11GridState, AV34DynamicFiltersIgnoreFirst, AV33DynamicFiltersRemoving, A297ContratoOcorrenciaNotificacao_Codigo, A294ContratoOcorrencia_Codigo, A303ContratoOcorrenciaNotificacao_Responsavel, sPrefix) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17ContratoOcorrenciaNotificacao_Data1, AV18ContratoOcorrenciaNotificacao_Data_To1, AV19ContratoOcorrenciaNotificacao_Prazo1, AV20ContratoOcorrenciaNotificacao_Prazo_To1, AV22DynamicFiltersSelector2, AV23ContratoOcorrenciaNotificacao_Data2, AV24ContratoOcorrenciaNotificacao_Data_To2, AV25ContratoOcorrenciaNotificacao_Prazo2, AV26ContratoOcorrenciaNotificacao_Prazo_To2, AV21DynamicFiltersEnabled2, AV39TFContratoOcorrenciaNotificacao_Data, AV40TFContratoOcorrenciaNotificacao_Data_To, AV45TFContratoOcorrenciaNotificacao_Prazo, AV46TFContratoOcorrenciaNotificacao_Prazo_To, AV49TFContratoOcorrenciaNotificacao_Descricao, AV50TFContratoOcorrenciaNotificacao_Descricao_Sel, AV53TFContratoOcorrenciaNotificacao_Cumprido, AV54TFContratoOcorrenciaNotificacao_Cumprido_To, AV59TFContratoOcorrenciaNotificacao_Protocolo, AV60TFContratoOcorrenciaNotificacao_Protocolo_Sel, AV63TFContratoOcorrenciaNotificacao_Responsavel, AV64TFContratoOcorrenciaNotificacao_Responsavel_To, AV67TFContratoOcorrenciaNotificacao_Nome, AV68TFContratoOcorrenciaNotificacao_Nome_Sel, AV71TFContratoOcorrenciaNotificacao_Docto, AV72TFContratoOcorrenciaNotificacao_Docto_Sel, AV7ContratoOcorrencia_Codigo, AV43ddo_ContratoOcorrenciaNotificacao_DataTitleControlIdToReplace, AV47ddo_ContratoOcorrenciaNotificacao_PrazoTitleControlIdToReplace, AV51ddo_ContratoOcorrenciaNotificacao_DescricaoTitleControlIdToReplace, AV57ddo_ContratoOcorrenciaNotificacao_CumpridoTitleControlIdToReplace, AV61ddo_ContratoOcorrenciaNotificacao_ProtocoloTitleControlIdToReplace, AV65ddo_ContratoOcorrenciaNotificacao_ResponsavelTitleControlIdToReplace, AV69ddo_ContratoOcorrenciaNotificacao_NomeTitleControlIdToReplace, AV73ddo_ContratoOcorrenciaNotificacao_DoctoTitleControlIdToReplace, AV82Pgmname, AV11GridState, AV34DynamicFiltersIgnoreFirst, AV33DynamicFiltersRemoving, A297ContratoOcorrenciaNotificacao_Codigo, A294ContratoOcorrencia_Codigo, A303ContratoOcorrenciaNotificacao_Responsavel, sPrefix) ;
         }
         return (int)(0) ;
      }

      protected void STRUP7A0( )
      {
         /* Before Start, stand alone formulas. */
         AV82Pgmname = "ContratoOcorrenciaContratoOcorrenciaNotificacaoWC";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E277A2 */
         E277A2 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vDDO_TITLESETTINGSICONS"), AV74DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vCONTRATOOCORRENCIANOTIFICACAO_DATATITLEFILTERDATA"), AV38ContratoOcorrenciaNotificacao_DataTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vCONTRATOOCORRENCIANOTIFICACAO_PRAZOTITLEFILTERDATA"), AV44ContratoOcorrenciaNotificacao_PrazoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vCONTRATOOCORRENCIANOTIFICACAO_DESCRICAOTITLEFILTERDATA"), AV48ContratoOcorrenciaNotificacao_DescricaoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDOTITLEFILTERDATA"), AV52ContratoOcorrenciaNotificacao_CumpridoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLOTITLEFILTERDATA"), AV58ContratoOcorrenciaNotificacao_ProtocoloTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vCONTRATOOCORRENCIANOTIFICACAO_RESPONSAVELTITLEFILTERDATA"), AV62ContratoOcorrenciaNotificacao_ResponsavelTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vCONTRATOOCORRENCIANOTIFICACAO_NOMETITLEFILTERDATA"), AV66ContratoOcorrenciaNotificacao_NomeTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vCONTRATOOCORRENCIANOTIFICACAO_DOCTOTITLEFILTERDATA"), AV70ContratoOcorrenciaNotificacao_DoctoTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV14OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV16DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
            if ( context.localUtil.VCDateTime( cgiGet( edtavContratoocorrencianotificacao_data1_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contrato Ocorrencia Notificacao_Data1"}), 1, "vCONTRATOOCORRENCIANOTIFICACAO_DATA1");
               GX_FocusControl = edtavContratoocorrencianotificacao_data1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV17ContratoOcorrenciaNotificacao_Data1 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17ContratoOcorrenciaNotificacao_Data1", context.localUtil.Format(AV17ContratoOcorrenciaNotificacao_Data1, "99/99/99"));
            }
            else
            {
               AV17ContratoOcorrenciaNotificacao_Data1 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContratoocorrencianotificacao_data1_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17ContratoOcorrenciaNotificacao_Data1", context.localUtil.Format(AV17ContratoOcorrenciaNotificacao_Data1, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContratoocorrencianotificacao_data_to1_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contrato Ocorrencia Notificacao_Data_To1"}), 1, "vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO1");
               GX_FocusControl = edtavContratoocorrencianotificacao_data_to1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV18ContratoOcorrenciaNotificacao_Data_To1 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18ContratoOcorrenciaNotificacao_Data_To1", context.localUtil.Format(AV18ContratoOcorrenciaNotificacao_Data_To1, "99/99/99"));
            }
            else
            {
               AV18ContratoOcorrenciaNotificacao_Data_To1 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContratoocorrencianotificacao_data_to1_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18ContratoOcorrenciaNotificacao_Data_To1", context.localUtil.Format(AV18ContratoOcorrenciaNotificacao_Data_To1, "99/99/99"));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavContratoocorrencianotificacao_prazo1_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavContratoocorrencianotificacao_prazo1_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTRATOOCORRENCIANOTIFICACAO_PRAZO1");
               GX_FocusControl = edtavContratoocorrencianotificacao_prazo1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV19ContratoOcorrenciaNotificacao_Prazo1 = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19ContratoOcorrenciaNotificacao_Prazo1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV19ContratoOcorrenciaNotificacao_Prazo1), 4, 0)));
            }
            else
            {
               AV19ContratoOcorrenciaNotificacao_Prazo1 = (short)(context.localUtil.CToN( cgiGet( edtavContratoocorrencianotificacao_prazo1_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19ContratoOcorrenciaNotificacao_Prazo1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV19ContratoOcorrenciaNotificacao_Prazo1), 4, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavContratoocorrencianotificacao_prazo_to1_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavContratoocorrencianotificacao_prazo_to1_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTRATOOCORRENCIANOTIFICACAO_PRAZO_TO1");
               GX_FocusControl = edtavContratoocorrencianotificacao_prazo_to1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV20ContratoOcorrenciaNotificacao_Prazo_To1 = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20ContratoOcorrenciaNotificacao_Prazo_To1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20ContratoOcorrenciaNotificacao_Prazo_To1), 4, 0)));
            }
            else
            {
               AV20ContratoOcorrenciaNotificacao_Prazo_To1 = (short)(context.localUtil.CToN( cgiGet( edtavContratoocorrencianotificacao_prazo_to1_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20ContratoOcorrenciaNotificacao_Prazo_To1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20ContratoOcorrenciaNotificacao_Prazo_To1), 4, 0)));
            }
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV22DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22DynamicFiltersSelector2", AV22DynamicFiltersSelector2);
            if ( context.localUtil.VCDateTime( cgiGet( edtavContratoocorrencianotificacao_data2_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contrato Ocorrencia Notificacao_Data2"}), 1, "vCONTRATOOCORRENCIANOTIFICACAO_DATA2");
               GX_FocusControl = edtavContratoocorrencianotificacao_data2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV23ContratoOcorrenciaNotificacao_Data2 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23ContratoOcorrenciaNotificacao_Data2", context.localUtil.Format(AV23ContratoOcorrenciaNotificacao_Data2, "99/99/99"));
            }
            else
            {
               AV23ContratoOcorrenciaNotificacao_Data2 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContratoocorrencianotificacao_data2_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23ContratoOcorrenciaNotificacao_Data2", context.localUtil.Format(AV23ContratoOcorrenciaNotificacao_Data2, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContratoocorrencianotificacao_data_to2_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contrato Ocorrencia Notificacao_Data_To2"}), 1, "vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO2");
               GX_FocusControl = edtavContratoocorrencianotificacao_data_to2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV24ContratoOcorrenciaNotificacao_Data_To2 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24ContratoOcorrenciaNotificacao_Data_To2", context.localUtil.Format(AV24ContratoOcorrenciaNotificacao_Data_To2, "99/99/99"));
            }
            else
            {
               AV24ContratoOcorrenciaNotificacao_Data_To2 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContratoocorrencianotificacao_data_to2_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24ContratoOcorrenciaNotificacao_Data_To2", context.localUtil.Format(AV24ContratoOcorrenciaNotificacao_Data_To2, "99/99/99"));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavContratoocorrencianotificacao_prazo2_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavContratoocorrencianotificacao_prazo2_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTRATOOCORRENCIANOTIFICACAO_PRAZO2");
               GX_FocusControl = edtavContratoocorrencianotificacao_prazo2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV25ContratoOcorrenciaNotificacao_Prazo2 = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25ContratoOcorrenciaNotificacao_Prazo2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25ContratoOcorrenciaNotificacao_Prazo2), 4, 0)));
            }
            else
            {
               AV25ContratoOcorrenciaNotificacao_Prazo2 = (short)(context.localUtil.CToN( cgiGet( edtavContratoocorrencianotificacao_prazo2_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25ContratoOcorrenciaNotificacao_Prazo2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25ContratoOcorrenciaNotificacao_Prazo2), 4, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavContratoocorrencianotificacao_prazo_to2_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavContratoocorrencianotificacao_prazo_to2_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTRATOOCORRENCIANOTIFICACAO_PRAZO_TO2");
               GX_FocusControl = edtavContratoocorrencianotificacao_prazo_to2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV26ContratoOcorrenciaNotificacao_Prazo_To2 = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26ContratoOcorrenciaNotificacao_Prazo_To2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26ContratoOcorrenciaNotificacao_Prazo_To2), 4, 0)));
            }
            else
            {
               AV26ContratoOcorrenciaNotificacao_Prazo_To2 = (short)(context.localUtil.CToN( cgiGet( edtavContratoocorrencianotificacao_prazo_to2_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26ContratoOcorrenciaNotificacao_Prazo_To2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26ContratoOcorrenciaNotificacao_Prazo_To2), 4, 0)));
            }
            A294ContratoOcorrencia_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContratoOcorrencia_Codigo_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A294ContratoOcorrencia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A294ContratoOcorrencia_Codigo), 6, 0)));
            AV21DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21DynamicFiltersEnabled2", AV21DynamicFiltersEnabled2);
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfcontratoocorrencianotificacao_data_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"TFContrato Ocorrencia Notificacao_Data"}), 1, "vTFCONTRATOOCORRENCIANOTIFICACAO_DATA");
               GX_FocusControl = edtavTfcontratoocorrencianotificacao_data_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV39TFContratoOcorrenciaNotificacao_Data = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV39TFContratoOcorrenciaNotificacao_Data", context.localUtil.Format(AV39TFContratoOcorrenciaNotificacao_Data, "99/99/99"));
            }
            else
            {
               AV39TFContratoOcorrenciaNotificacao_Data = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavTfcontratoocorrencianotificacao_data_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV39TFContratoOcorrenciaNotificacao_Data", context.localUtil.Format(AV39TFContratoOcorrenciaNotificacao_Data, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfcontratoocorrencianotificacao_data_to_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"TFContrato Ocorrencia Notificacao_Data_To"}), 1, "vTFCONTRATOOCORRENCIANOTIFICACAO_DATA_TO");
               GX_FocusControl = edtavTfcontratoocorrencianotificacao_data_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV40TFContratoOcorrenciaNotificacao_Data_To = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV40TFContratoOcorrenciaNotificacao_Data_To", context.localUtil.Format(AV40TFContratoOcorrenciaNotificacao_Data_To, "99/99/99"));
            }
            else
            {
               AV40TFContratoOcorrenciaNotificacao_Data_To = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavTfcontratoocorrencianotificacao_data_to_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV40TFContratoOcorrenciaNotificacao_Data_To", context.localUtil.Format(AV40TFContratoOcorrenciaNotificacao_Data_To, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_contratoocorrencianotificacao_dataauxdate_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Contrato Ocorrencia Notificacao_Data Aux Date"}), 1, "vDDO_CONTRATOOCORRENCIANOTIFICACAO_DATAAUXDATE");
               GX_FocusControl = edtavDdo_contratoocorrencianotificacao_dataauxdate_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV41DDO_ContratoOcorrenciaNotificacao_DataAuxDate = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV41DDO_ContratoOcorrenciaNotificacao_DataAuxDate", context.localUtil.Format(AV41DDO_ContratoOcorrenciaNotificacao_DataAuxDate, "99/99/99"));
            }
            else
            {
               AV41DDO_ContratoOcorrenciaNotificacao_DataAuxDate = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_contratoocorrencianotificacao_dataauxdate_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV41DDO_ContratoOcorrenciaNotificacao_DataAuxDate", context.localUtil.Format(AV41DDO_ContratoOcorrenciaNotificacao_DataAuxDate, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_contratoocorrencianotificacao_dataauxdateto_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Contrato Ocorrencia Notificacao_Data Aux Date To"}), 1, "vDDO_CONTRATOOCORRENCIANOTIFICACAO_DATAAUXDATETO");
               GX_FocusControl = edtavDdo_contratoocorrencianotificacao_dataauxdateto_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV42DDO_ContratoOcorrenciaNotificacao_DataAuxDateTo = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV42DDO_ContratoOcorrenciaNotificacao_DataAuxDateTo", context.localUtil.Format(AV42DDO_ContratoOcorrenciaNotificacao_DataAuxDateTo, "99/99/99"));
            }
            else
            {
               AV42DDO_ContratoOcorrenciaNotificacao_DataAuxDateTo = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_contratoocorrencianotificacao_dataauxdateto_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV42DDO_ContratoOcorrenciaNotificacao_DataAuxDateTo", context.localUtil.Format(AV42DDO_ContratoOcorrenciaNotificacao_DataAuxDateTo, "99/99/99"));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoocorrencianotificacao_prazo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoocorrencianotificacao_prazo_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO");
               GX_FocusControl = edtavTfcontratoocorrencianotificacao_prazo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV45TFContratoOcorrenciaNotificacao_Prazo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV45TFContratoOcorrenciaNotificacao_Prazo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV45TFContratoOcorrenciaNotificacao_Prazo), 4, 0)));
            }
            else
            {
               AV45TFContratoOcorrenciaNotificacao_Prazo = (short)(context.localUtil.CToN( cgiGet( edtavTfcontratoocorrencianotificacao_prazo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV45TFContratoOcorrenciaNotificacao_Prazo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV45TFContratoOcorrenciaNotificacao_Prazo), 4, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoocorrencianotificacao_prazo_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoocorrencianotificacao_prazo_to_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO_TO");
               GX_FocusControl = edtavTfcontratoocorrencianotificacao_prazo_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV46TFContratoOcorrenciaNotificacao_Prazo_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV46TFContratoOcorrenciaNotificacao_Prazo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV46TFContratoOcorrenciaNotificacao_Prazo_To), 4, 0)));
            }
            else
            {
               AV46TFContratoOcorrenciaNotificacao_Prazo_To = (short)(context.localUtil.CToN( cgiGet( edtavTfcontratoocorrencianotificacao_prazo_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV46TFContratoOcorrenciaNotificacao_Prazo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV46TFContratoOcorrenciaNotificacao_Prazo_To), 4, 0)));
            }
            AV49TFContratoOcorrenciaNotificacao_Descricao = StringUtil.Upper( cgiGet( edtavTfcontratoocorrencianotificacao_descricao_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV49TFContratoOcorrenciaNotificacao_Descricao", AV49TFContratoOcorrenciaNotificacao_Descricao);
            AV50TFContratoOcorrenciaNotificacao_Descricao_Sel = StringUtil.Upper( cgiGet( edtavTfcontratoocorrencianotificacao_descricao_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV50TFContratoOcorrenciaNotificacao_Descricao_Sel", AV50TFContratoOcorrenciaNotificacao_Descricao_Sel);
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfcontratoocorrencianotificacao_cumprido_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"TFContrato Ocorrencia Notificacao_Cumprido"}), 1, "vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO");
               GX_FocusControl = edtavTfcontratoocorrencianotificacao_cumprido_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV53TFContratoOcorrenciaNotificacao_Cumprido = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV53TFContratoOcorrenciaNotificacao_Cumprido", context.localUtil.Format(AV53TFContratoOcorrenciaNotificacao_Cumprido, "99/99/99"));
            }
            else
            {
               AV53TFContratoOcorrenciaNotificacao_Cumprido = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavTfcontratoocorrencianotificacao_cumprido_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV53TFContratoOcorrenciaNotificacao_Cumprido", context.localUtil.Format(AV53TFContratoOcorrenciaNotificacao_Cumprido, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfcontratoocorrencianotificacao_cumprido_to_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"TFContrato Ocorrencia Notificacao_Cumprido_To"}), 1, "vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_TO");
               GX_FocusControl = edtavTfcontratoocorrencianotificacao_cumprido_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV54TFContratoOcorrenciaNotificacao_Cumprido_To = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV54TFContratoOcorrenciaNotificacao_Cumprido_To", context.localUtil.Format(AV54TFContratoOcorrenciaNotificacao_Cumprido_To, "99/99/99"));
            }
            else
            {
               AV54TFContratoOcorrenciaNotificacao_Cumprido_To = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavTfcontratoocorrencianotificacao_cumprido_to_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV54TFContratoOcorrenciaNotificacao_Cumprido_To", context.localUtil.Format(AV54TFContratoOcorrenciaNotificacao_Cumprido_To, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_contratoocorrencianotificacao_cumpridoauxdate_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Contrato Ocorrencia Notificacao_Cumprido Aux Date"}), 1, "vDDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDOAUXDATE");
               GX_FocusControl = edtavDdo_contratoocorrencianotificacao_cumpridoauxdate_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV55DDO_ContratoOcorrenciaNotificacao_CumpridoAuxDate = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV55DDO_ContratoOcorrenciaNotificacao_CumpridoAuxDate", context.localUtil.Format(AV55DDO_ContratoOcorrenciaNotificacao_CumpridoAuxDate, "99/99/99"));
            }
            else
            {
               AV55DDO_ContratoOcorrenciaNotificacao_CumpridoAuxDate = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_contratoocorrencianotificacao_cumpridoauxdate_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV55DDO_ContratoOcorrenciaNotificacao_CumpridoAuxDate", context.localUtil.Format(AV55DDO_ContratoOcorrenciaNotificacao_CumpridoAuxDate, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_contratoocorrencianotificacao_cumpridoauxdateto_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Contrato Ocorrencia Notificacao_Cumprido Aux Date To"}), 1, "vDDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDOAUXDATETO");
               GX_FocusControl = edtavDdo_contratoocorrencianotificacao_cumpridoauxdateto_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV56DDO_ContratoOcorrenciaNotificacao_CumpridoAuxDateTo = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV56DDO_ContratoOcorrenciaNotificacao_CumpridoAuxDateTo", context.localUtil.Format(AV56DDO_ContratoOcorrenciaNotificacao_CumpridoAuxDateTo, "99/99/99"));
            }
            else
            {
               AV56DDO_ContratoOcorrenciaNotificacao_CumpridoAuxDateTo = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_contratoocorrencianotificacao_cumpridoauxdateto_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV56DDO_ContratoOcorrenciaNotificacao_CumpridoAuxDateTo", context.localUtil.Format(AV56DDO_ContratoOcorrenciaNotificacao_CumpridoAuxDateTo, "99/99/99"));
            }
            AV59TFContratoOcorrenciaNotificacao_Protocolo = cgiGet( edtavTfcontratoocorrencianotificacao_protocolo_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV59TFContratoOcorrenciaNotificacao_Protocolo", AV59TFContratoOcorrenciaNotificacao_Protocolo);
            AV60TFContratoOcorrenciaNotificacao_Protocolo_Sel = cgiGet( edtavTfcontratoocorrencianotificacao_protocolo_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV60TFContratoOcorrenciaNotificacao_Protocolo_Sel", AV60TFContratoOcorrenciaNotificacao_Protocolo_Sel);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoocorrencianotificacao_responsavel_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoocorrencianotificacao_responsavel_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL");
               GX_FocusControl = edtavTfcontratoocorrencianotificacao_responsavel_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV63TFContratoOcorrenciaNotificacao_Responsavel = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV63TFContratoOcorrenciaNotificacao_Responsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(AV63TFContratoOcorrenciaNotificacao_Responsavel), 6, 0)));
            }
            else
            {
               AV63TFContratoOcorrenciaNotificacao_Responsavel = (int)(context.localUtil.CToN( cgiGet( edtavTfcontratoocorrencianotificacao_responsavel_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV63TFContratoOcorrenciaNotificacao_Responsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(AV63TFContratoOcorrenciaNotificacao_Responsavel), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoocorrencianotificacao_responsavel_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoocorrencianotificacao_responsavel_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_TO");
               GX_FocusControl = edtavTfcontratoocorrencianotificacao_responsavel_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV64TFContratoOcorrenciaNotificacao_Responsavel_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV64TFContratoOcorrenciaNotificacao_Responsavel_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV64TFContratoOcorrenciaNotificacao_Responsavel_To), 6, 0)));
            }
            else
            {
               AV64TFContratoOcorrenciaNotificacao_Responsavel_To = (int)(context.localUtil.CToN( cgiGet( edtavTfcontratoocorrencianotificacao_responsavel_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV64TFContratoOcorrenciaNotificacao_Responsavel_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV64TFContratoOcorrenciaNotificacao_Responsavel_To), 6, 0)));
            }
            AV67TFContratoOcorrenciaNotificacao_Nome = StringUtil.Upper( cgiGet( edtavTfcontratoocorrencianotificacao_nome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV67TFContratoOcorrenciaNotificacao_Nome", AV67TFContratoOcorrenciaNotificacao_Nome);
            AV68TFContratoOcorrenciaNotificacao_Nome_Sel = StringUtil.Upper( cgiGet( edtavTfcontratoocorrencianotificacao_nome_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV68TFContratoOcorrenciaNotificacao_Nome_Sel", AV68TFContratoOcorrenciaNotificacao_Nome_Sel);
            AV71TFContratoOcorrenciaNotificacao_Docto = cgiGet( edtavTfcontratoocorrencianotificacao_docto_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV71TFContratoOcorrenciaNotificacao_Docto", AV71TFContratoOcorrenciaNotificacao_Docto);
            AV72TFContratoOcorrenciaNotificacao_Docto_Sel = cgiGet( edtavTfcontratoocorrencianotificacao_docto_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV72TFContratoOcorrenciaNotificacao_Docto_Sel", AV72TFContratoOcorrenciaNotificacao_Docto_Sel);
            AV43ddo_ContratoOcorrenciaNotificacao_DataTitleControlIdToReplace = cgiGet( edtavDdo_contratoocorrencianotificacao_datatitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV43ddo_ContratoOcorrenciaNotificacao_DataTitleControlIdToReplace", AV43ddo_ContratoOcorrenciaNotificacao_DataTitleControlIdToReplace);
            AV47ddo_ContratoOcorrenciaNotificacao_PrazoTitleControlIdToReplace = cgiGet( edtavDdo_contratoocorrencianotificacao_prazotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV47ddo_ContratoOcorrenciaNotificacao_PrazoTitleControlIdToReplace", AV47ddo_ContratoOcorrenciaNotificacao_PrazoTitleControlIdToReplace);
            AV51ddo_ContratoOcorrenciaNotificacao_DescricaoTitleControlIdToReplace = cgiGet( edtavDdo_contratoocorrencianotificacao_descricaotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV51ddo_ContratoOcorrenciaNotificacao_DescricaoTitleControlIdToReplace", AV51ddo_ContratoOcorrenciaNotificacao_DescricaoTitleControlIdToReplace);
            AV57ddo_ContratoOcorrenciaNotificacao_CumpridoTitleControlIdToReplace = cgiGet( edtavDdo_contratoocorrencianotificacao_cumpridotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV57ddo_ContratoOcorrenciaNotificacao_CumpridoTitleControlIdToReplace", AV57ddo_ContratoOcorrenciaNotificacao_CumpridoTitleControlIdToReplace);
            AV61ddo_ContratoOcorrenciaNotificacao_ProtocoloTitleControlIdToReplace = cgiGet( edtavDdo_contratoocorrencianotificacao_protocolotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV61ddo_ContratoOcorrenciaNotificacao_ProtocoloTitleControlIdToReplace", AV61ddo_ContratoOcorrenciaNotificacao_ProtocoloTitleControlIdToReplace);
            AV65ddo_ContratoOcorrenciaNotificacao_ResponsavelTitleControlIdToReplace = cgiGet( edtavDdo_contratoocorrencianotificacao_responsaveltitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV65ddo_ContratoOcorrenciaNotificacao_ResponsavelTitleControlIdToReplace", AV65ddo_ContratoOcorrenciaNotificacao_ResponsavelTitleControlIdToReplace);
            AV69ddo_ContratoOcorrenciaNotificacao_NomeTitleControlIdToReplace = cgiGet( edtavDdo_contratoocorrencianotificacao_nometitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV69ddo_ContratoOcorrenciaNotificacao_NomeTitleControlIdToReplace", AV69ddo_ContratoOcorrenciaNotificacao_NomeTitleControlIdToReplace);
            AV73ddo_ContratoOcorrenciaNotificacao_DoctoTitleControlIdToReplace = cgiGet( edtavDdo_contratoocorrencianotificacao_doctotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV73ddo_ContratoOcorrenciaNotificacao_DoctoTitleControlIdToReplace", AV73ddo_ContratoOcorrenciaNotificacao_DoctoTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_84 = (short)(context.localUtil.CToN( cgiGet( sPrefix+"nRC_GXsfl_84"), ",", "."));
            AV76GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"vGRIDCURRENTPAGE"), ",", "."));
            AV77GridPageCount = (long)(context.localUtil.CToN( cgiGet( sPrefix+"vGRIDPAGECOUNT"), ",", "."));
            wcpOAV7ContratoOcorrencia_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV7ContratoOcorrencia_Codigo"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( sPrefix+"GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_contratoocorrencianotificacao_data_Caption = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Caption");
            Ddo_contratoocorrencianotificacao_data_Tooltip = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Tooltip");
            Ddo_contratoocorrencianotificacao_data_Cls = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Cls");
            Ddo_contratoocorrencianotificacao_data_Filteredtext_set = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Filteredtext_set");
            Ddo_contratoocorrencianotificacao_data_Filteredtextto_set = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Filteredtextto_set");
            Ddo_contratoocorrencianotificacao_data_Dropdownoptionstype = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Dropdownoptionstype");
            Ddo_contratoocorrencianotificacao_data_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Titlecontrolidtoreplace");
            Ddo_contratoocorrencianotificacao_data_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Includesortasc"));
            Ddo_contratoocorrencianotificacao_data_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Includesortdsc"));
            Ddo_contratoocorrencianotificacao_data_Sortedstatus = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Sortedstatus");
            Ddo_contratoocorrencianotificacao_data_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Includefilter"));
            Ddo_contratoocorrencianotificacao_data_Filtertype = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Filtertype");
            Ddo_contratoocorrencianotificacao_data_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Filterisrange"));
            Ddo_contratoocorrencianotificacao_data_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Includedatalist"));
            Ddo_contratoocorrencianotificacao_data_Sortasc = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Sortasc");
            Ddo_contratoocorrencianotificacao_data_Sortdsc = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Sortdsc");
            Ddo_contratoocorrencianotificacao_data_Cleanfilter = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Cleanfilter");
            Ddo_contratoocorrencianotificacao_data_Rangefilterfrom = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Rangefilterfrom");
            Ddo_contratoocorrencianotificacao_data_Rangefilterto = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Rangefilterto");
            Ddo_contratoocorrencianotificacao_data_Searchbuttontext = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Searchbuttontext");
            Ddo_contratoocorrencianotificacao_prazo_Caption = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Caption");
            Ddo_contratoocorrencianotificacao_prazo_Tooltip = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Tooltip");
            Ddo_contratoocorrencianotificacao_prazo_Cls = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Cls");
            Ddo_contratoocorrencianotificacao_prazo_Filteredtext_set = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Filteredtext_set");
            Ddo_contratoocorrencianotificacao_prazo_Filteredtextto_set = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Filteredtextto_set");
            Ddo_contratoocorrencianotificacao_prazo_Dropdownoptionstype = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Dropdownoptionstype");
            Ddo_contratoocorrencianotificacao_prazo_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Titlecontrolidtoreplace");
            Ddo_contratoocorrencianotificacao_prazo_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Includesortasc"));
            Ddo_contratoocorrencianotificacao_prazo_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Includesortdsc"));
            Ddo_contratoocorrencianotificacao_prazo_Sortedstatus = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Sortedstatus");
            Ddo_contratoocorrencianotificacao_prazo_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Includefilter"));
            Ddo_contratoocorrencianotificacao_prazo_Filtertype = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Filtertype");
            Ddo_contratoocorrencianotificacao_prazo_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Filterisrange"));
            Ddo_contratoocorrencianotificacao_prazo_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Includedatalist"));
            Ddo_contratoocorrencianotificacao_prazo_Sortasc = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Sortasc");
            Ddo_contratoocorrencianotificacao_prazo_Sortdsc = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Sortdsc");
            Ddo_contratoocorrencianotificacao_prazo_Cleanfilter = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Cleanfilter");
            Ddo_contratoocorrencianotificacao_prazo_Rangefilterfrom = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Rangefilterfrom");
            Ddo_contratoocorrencianotificacao_prazo_Rangefilterto = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Rangefilterto");
            Ddo_contratoocorrencianotificacao_prazo_Searchbuttontext = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Searchbuttontext");
            Ddo_contratoocorrencianotificacao_descricao_Caption = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Caption");
            Ddo_contratoocorrencianotificacao_descricao_Tooltip = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Tooltip");
            Ddo_contratoocorrencianotificacao_descricao_Cls = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Cls");
            Ddo_contratoocorrencianotificacao_descricao_Filteredtext_set = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Filteredtext_set");
            Ddo_contratoocorrencianotificacao_descricao_Selectedvalue_set = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Selectedvalue_set");
            Ddo_contratoocorrencianotificacao_descricao_Dropdownoptionstype = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Dropdownoptionstype");
            Ddo_contratoocorrencianotificacao_descricao_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Titlecontrolidtoreplace");
            Ddo_contratoocorrencianotificacao_descricao_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Includesortasc"));
            Ddo_contratoocorrencianotificacao_descricao_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Includesortdsc"));
            Ddo_contratoocorrencianotificacao_descricao_Sortedstatus = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Sortedstatus");
            Ddo_contratoocorrencianotificacao_descricao_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Includefilter"));
            Ddo_contratoocorrencianotificacao_descricao_Filtertype = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Filtertype");
            Ddo_contratoocorrencianotificacao_descricao_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Filterisrange"));
            Ddo_contratoocorrencianotificacao_descricao_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Includedatalist"));
            Ddo_contratoocorrencianotificacao_descricao_Datalisttype = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Datalisttype");
            Ddo_contratoocorrencianotificacao_descricao_Datalistproc = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Datalistproc");
            Ddo_contratoocorrencianotificacao_descricao_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contratoocorrencianotificacao_descricao_Sortasc = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Sortasc");
            Ddo_contratoocorrencianotificacao_descricao_Sortdsc = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Sortdsc");
            Ddo_contratoocorrencianotificacao_descricao_Loadingdata = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Loadingdata");
            Ddo_contratoocorrencianotificacao_descricao_Cleanfilter = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Cleanfilter");
            Ddo_contratoocorrencianotificacao_descricao_Noresultsfound = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Noresultsfound");
            Ddo_contratoocorrencianotificacao_descricao_Searchbuttontext = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Searchbuttontext");
            Ddo_contratoocorrencianotificacao_cumprido_Caption = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Caption");
            Ddo_contratoocorrencianotificacao_cumprido_Tooltip = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Tooltip");
            Ddo_contratoocorrencianotificacao_cumprido_Cls = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Cls");
            Ddo_contratoocorrencianotificacao_cumprido_Filteredtext_set = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Filteredtext_set");
            Ddo_contratoocorrencianotificacao_cumprido_Filteredtextto_set = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Filteredtextto_set");
            Ddo_contratoocorrencianotificacao_cumprido_Dropdownoptionstype = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Dropdownoptionstype");
            Ddo_contratoocorrencianotificacao_cumprido_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Titlecontrolidtoreplace");
            Ddo_contratoocorrencianotificacao_cumprido_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Includesortasc"));
            Ddo_contratoocorrencianotificacao_cumprido_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Includesortdsc"));
            Ddo_contratoocorrencianotificacao_cumprido_Sortedstatus = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Sortedstatus");
            Ddo_contratoocorrencianotificacao_cumprido_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Includefilter"));
            Ddo_contratoocorrencianotificacao_cumprido_Filtertype = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Filtertype");
            Ddo_contratoocorrencianotificacao_cumprido_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Filterisrange"));
            Ddo_contratoocorrencianotificacao_cumprido_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Includedatalist"));
            Ddo_contratoocorrencianotificacao_cumprido_Sortasc = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Sortasc");
            Ddo_contratoocorrencianotificacao_cumprido_Sortdsc = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Sortdsc");
            Ddo_contratoocorrencianotificacao_cumprido_Cleanfilter = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Cleanfilter");
            Ddo_contratoocorrencianotificacao_cumprido_Rangefilterfrom = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Rangefilterfrom");
            Ddo_contratoocorrencianotificacao_cumprido_Rangefilterto = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Rangefilterto");
            Ddo_contratoocorrencianotificacao_cumprido_Searchbuttontext = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Searchbuttontext");
            Ddo_contratoocorrencianotificacao_protocolo_Caption = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Caption");
            Ddo_contratoocorrencianotificacao_protocolo_Tooltip = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Tooltip");
            Ddo_contratoocorrencianotificacao_protocolo_Cls = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Cls");
            Ddo_contratoocorrencianotificacao_protocolo_Filteredtext_set = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Filteredtext_set");
            Ddo_contratoocorrencianotificacao_protocolo_Selectedvalue_set = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Selectedvalue_set");
            Ddo_contratoocorrencianotificacao_protocolo_Dropdownoptionstype = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Dropdownoptionstype");
            Ddo_contratoocorrencianotificacao_protocolo_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Titlecontrolidtoreplace");
            Ddo_contratoocorrencianotificacao_protocolo_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Includesortasc"));
            Ddo_contratoocorrencianotificacao_protocolo_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Includesortdsc"));
            Ddo_contratoocorrencianotificacao_protocolo_Sortedstatus = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Sortedstatus");
            Ddo_contratoocorrencianotificacao_protocolo_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Includefilter"));
            Ddo_contratoocorrencianotificacao_protocolo_Filtertype = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Filtertype");
            Ddo_contratoocorrencianotificacao_protocolo_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Filterisrange"));
            Ddo_contratoocorrencianotificacao_protocolo_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Includedatalist"));
            Ddo_contratoocorrencianotificacao_protocolo_Datalisttype = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Datalisttype");
            Ddo_contratoocorrencianotificacao_protocolo_Datalistproc = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Datalistproc");
            Ddo_contratoocorrencianotificacao_protocolo_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contratoocorrencianotificacao_protocolo_Sortasc = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Sortasc");
            Ddo_contratoocorrencianotificacao_protocolo_Sortdsc = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Sortdsc");
            Ddo_contratoocorrencianotificacao_protocolo_Loadingdata = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Loadingdata");
            Ddo_contratoocorrencianotificacao_protocolo_Cleanfilter = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Cleanfilter");
            Ddo_contratoocorrencianotificacao_protocolo_Noresultsfound = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Noresultsfound");
            Ddo_contratoocorrencianotificacao_protocolo_Searchbuttontext = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Searchbuttontext");
            Ddo_contratoocorrencianotificacao_responsavel_Caption = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_Caption");
            Ddo_contratoocorrencianotificacao_responsavel_Tooltip = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_Tooltip");
            Ddo_contratoocorrencianotificacao_responsavel_Cls = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_Cls");
            Ddo_contratoocorrencianotificacao_responsavel_Filteredtext_set = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_Filteredtext_set");
            Ddo_contratoocorrencianotificacao_responsavel_Filteredtextto_set = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_Filteredtextto_set");
            Ddo_contratoocorrencianotificacao_responsavel_Dropdownoptionstype = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_Dropdownoptionstype");
            Ddo_contratoocorrencianotificacao_responsavel_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_Titlecontrolidtoreplace");
            Ddo_contratoocorrencianotificacao_responsavel_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_Includesortasc"));
            Ddo_contratoocorrencianotificacao_responsavel_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_Includesortdsc"));
            Ddo_contratoocorrencianotificacao_responsavel_Sortedstatus = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_Sortedstatus");
            Ddo_contratoocorrencianotificacao_responsavel_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_Includefilter"));
            Ddo_contratoocorrencianotificacao_responsavel_Filtertype = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_Filtertype");
            Ddo_contratoocorrencianotificacao_responsavel_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_Filterisrange"));
            Ddo_contratoocorrencianotificacao_responsavel_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_Includedatalist"));
            Ddo_contratoocorrencianotificacao_responsavel_Sortasc = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_Sortasc");
            Ddo_contratoocorrencianotificacao_responsavel_Sortdsc = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_Sortdsc");
            Ddo_contratoocorrencianotificacao_responsavel_Cleanfilter = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_Cleanfilter");
            Ddo_contratoocorrencianotificacao_responsavel_Rangefilterfrom = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_Rangefilterfrom");
            Ddo_contratoocorrencianotificacao_responsavel_Rangefilterto = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_Rangefilterto");
            Ddo_contratoocorrencianotificacao_responsavel_Searchbuttontext = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_Searchbuttontext");
            Ddo_contratoocorrencianotificacao_nome_Caption = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Caption");
            Ddo_contratoocorrencianotificacao_nome_Tooltip = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Tooltip");
            Ddo_contratoocorrencianotificacao_nome_Cls = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Cls");
            Ddo_contratoocorrencianotificacao_nome_Filteredtext_set = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Filteredtext_set");
            Ddo_contratoocorrencianotificacao_nome_Selectedvalue_set = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Selectedvalue_set");
            Ddo_contratoocorrencianotificacao_nome_Dropdownoptionstype = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Dropdownoptionstype");
            Ddo_contratoocorrencianotificacao_nome_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Titlecontrolidtoreplace");
            Ddo_contratoocorrencianotificacao_nome_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Includesortasc"));
            Ddo_contratoocorrencianotificacao_nome_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Includesortdsc"));
            Ddo_contratoocorrencianotificacao_nome_Sortedstatus = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Sortedstatus");
            Ddo_contratoocorrencianotificacao_nome_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Includefilter"));
            Ddo_contratoocorrencianotificacao_nome_Filtertype = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Filtertype");
            Ddo_contratoocorrencianotificacao_nome_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Filterisrange"));
            Ddo_contratoocorrencianotificacao_nome_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Includedatalist"));
            Ddo_contratoocorrencianotificacao_nome_Datalisttype = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Datalisttype");
            Ddo_contratoocorrencianotificacao_nome_Datalistproc = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Datalistproc");
            Ddo_contratoocorrencianotificacao_nome_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contratoocorrencianotificacao_nome_Sortasc = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Sortasc");
            Ddo_contratoocorrencianotificacao_nome_Sortdsc = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Sortdsc");
            Ddo_contratoocorrencianotificacao_nome_Loadingdata = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Loadingdata");
            Ddo_contratoocorrencianotificacao_nome_Cleanfilter = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Cleanfilter");
            Ddo_contratoocorrencianotificacao_nome_Noresultsfound = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Noresultsfound");
            Ddo_contratoocorrencianotificacao_nome_Searchbuttontext = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Searchbuttontext");
            Ddo_contratoocorrencianotificacao_docto_Caption = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO_Caption");
            Ddo_contratoocorrencianotificacao_docto_Tooltip = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO_Tooltip");
            Ddo_contratoocorrencianotificacao_docto_Cls = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO_Cls");
            Ddo_contratoocorrencianotificacao_docto_Filteredtext_set = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO_Filteredtext_set");
            Ddo_contratoocorrencianotificacao_docto_Selectedvalue_set = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO_Selectedvalue_set");
            Ddo_contratoocorrencianotificacao_docto_Dropdownoptionstype = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO_Dropdownoptionstype");
            Ddo_contratoocorrencianotificacao_docto_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO_Titlecontrolidtoreplace");
            Ddo_contratoocorrencianotificacao_docto_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO_Includesortasc"));
            Ddo_contratoocorrencianotificacao_docto_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO_Includesortdsc"));
            Ddo_contratoocorrencianotificacao_docto_Sortedstatus = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO_Sortedstatus");
            Ddo_contratoocorrencianotificacao_docto_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO_Includefilter"));
            Ddo_contratoocorrencianotificacao_docto_Filtertype = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO_Filtertype");
            Ddo_contratoocorrencianotificacao_docto_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO_Filterisrange"));
            Ddo_contratoocorrencianotificacao_docto_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO_Includedatalist"));
            Ddo_contratoocorrencianotificacao_docto_Datalisttype = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO_Datalisttype");
            Ddo_contratoocorrencianotificacao_docto_Datalistproc = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO_Datalistproc");
            Ddo_contratoocorrencianotificacao_docto_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contratoocorrencianotificacao_docto_Sortasc = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO_Sortasc");
            Ddo_contratoocorrencianotificacao_docto_Sortdsc = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO_Sortdsc");
            Ddo_contratoocorrencianotificacao_docto_Loadingdata = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO_Loadingdata");
            Ddo_contratoocorrencianotificacao_docto_Cleanfilter = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO_Cleanfilter");
            Ddo_contratoocorrencianotificacao_docto_Noresultsfound = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO_Noresultsfound");
            Ddo_contratoocorrencianotificacao_docto_Searchbuttontext = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Selectedpage");
            Ddo_contratoocorrencianotificacao_data_Activeeventkey = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Activeeventkey");
            Ddo_contratoocorrencianotificacao_data_Filteredtext_get = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Filteredtext_get");
            Ddo_contratoocorrencianotificacao_data_Filteredtextto_get = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA_Filteredtextto_get");
            Ddo_contratoocorrencianotificacao_prazo_Activeeventkey = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Activeeventkey");
            Ddo_contratoocorrencianotificacao_prazo_Filteredtext_get = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Filteredtext_get");
            Ddo_contratoocorrencianotificacao_prazo_Filteredtextto_get = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO_Filteredtextto_get");
            Ddo_contratoocorrencianotificacao_descricao_Activeeventkey = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Activeeventkey");
            Ddo_contratoocorrencianotificacao_descricao_Filteredtext_get = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Filteredtext_get");
            Ddo_contratoocorrencianotificacao_descricao_Selectedvalue_get = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_Selectedvalue_get");
            Ddo_contratoocorrencianotificacao_cumprido_Activeeventkey = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Activeeventkey");
            Ddo_contratoocorrencianotificacao_cumprido_Filteredtext_get = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Filteredtext_get");
            Ddo_contratoocorrencianotificacao_cumprido_Filteredtextto_get = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_Filteredtextto_get");
            Ddo_contratoocorrencianotificacao_protocolo_Activeeventkey = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Activeeventkey");
            Ddo_contratoocorrencianotificacao_protocolo_Filteredtext_get = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Filteredtext_get");
            Ddo_contratoocorrencianotificacao_protocolo_Selectedvalue_get = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_Selectedvalue_get");
            Ddo_contratoocorrencianotificacao_responsavel_Activeeventkey = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_Activeeventkey");
            Ddo_contratoocorrencianotificacao_responsavel_Filteredtext_get = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_Filteredtext_get");
            Ddo_contratoocorrencianotificacao_responsavel_Filteredtextto_get = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_Filteredtextto_get");
            Ddo_contratoocorrencianotificacao_nome_Activeeventkey = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Activeeventkey");
            Ddo_contratoocorrencianotificacao_nome_Filteredtext_get = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Filteredtext_get");
            Ddo_contratoocorrencianotificacao_nome_Selectedvalue_get = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME_Selectedvalue_get");
            Ddo_contratoocorrencianotificacao_docto_Activeeventkey = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO_Activeeventkey");
            Ddo_contratoocorrencianotificacao_docto_Filteredtext_get = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO_Filteredtext_get");
            Ddo_contratoocorrencianotificacao_docto_Selectedvalue_get = cgiGet( sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO_Selectedvalue_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            forbiddenHiddens = sPrefix + "hsh" + "ContratoOcorrenciaContratoOcorrenciaNotificacaoWC";
            A294ContratoOcorrencia_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContratoOcorrencia_Codigo_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A294ContratoOcorrencia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A294ContratoOcorrencia_Codigo), 6, 0)));
            forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A294ContratoOcorrencia_Codigo), "ZZZZZ9");
            hsh = cgiGet( sPrefix+"hsh");
            if ( ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
            {
               GXUtil.WriteLog("contratoocorrenciacontratoocorrencianotificacaowc:[SecurityCheckFailed value for]"+"ContratoOcorrencia_Codigo:"+context.localUtil.Format( (decimal)(A294ContratoOcorrencia_Codigo), "ZZZZZ9"));
               GxWebError = 1;
               context.HttpContext.Response.StatusDescription = 403.ToString();
               context.HttpContext.Response.StatusCode = 403;
               context.WriteHtmlText( "<title>403 Forbidden</title>") ;
               context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
               context.WriteHtmlText( "<p /><hr />") ;
               GXUtil.WriteLog("send_http_error_code " + 403.ToString());
               return  ;
            }
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV14OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV15OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR1"), AV16DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( sPrefix+"GXH_vCONTRATOOCORRENCIANOTIFICACAO_DATA1"), 0) != AV17ContratoOcorrenciaNotificacao_Data1 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( sPrefix+"GXH_vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO1"), 0) != AV18ContratoOcorrenciaNotificacao_Data_To1 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vCONTRATOOCORRENCIANOTIFICACAO_PRAZO1"), ",", ".") != Convert.ToDecimal( AV19ContratoOcorrenciaNotificacao_Prazo1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vCONTRATOOCORRENCIANOTIFICACAO_PRAZO_TO1"), ",", ".") != Convert.ToDecimal( AV20ContratoOcorrenciaNotificacao_Prazo_To1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR2"), AV22DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( sPrefix+"GXH_vCONTRATOOCORRENCIANOTIFICACAO_DATA2"), 0) != AV23ContratoOcorrenciaNotificacao_Data2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( sPrefix+"GXH_vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO2"), 0) != AV24ContratoOcorrenciaNotificacao_Data_To2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vCONTRATOOCORRENCIANOTIFICACAO_PRAZO2"), ",", ".") != Convert.ToDecimal( AV25ContratoOcorrenciaNotificacao_Prazo2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vCONTRATOOCORRENCIANOTIFICACAO_PRAZO_TO2"), ",", ".") != Convert.ToDecimal( AV26ContratoOcorrenciaNotificacao_Prazo_To2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSENABLED2")) != AV21DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( sPrefix+"GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_DATA"), 0) != AV39TFContratoOcorrenciaNotificacao_Data )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( sPrefix+"GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_DATA_TO"), 0) != AV40TFContratoOcorrenciaNotificacao_Data_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO"), ",", ".") != Convert.ToDecimal( AV45TFContratoOcorrenciaNotificacao_Prazo )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO_TO"), ",", ".") != Convert.ToDecimal( AV46TFContratoOcorrenciaNotificacao_Prazo_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO"), AV49TFContratoOcorrenciaNotificacao_Descricao) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_SEL"), AV50TFContratoOcorrenciaNotificacao_Descricao_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( sPrefix+"GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO"), 0) != AV53TFContratoOcorrenciaNotificacao_Cumprido )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( sPrefix+"GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_TO"), 0) != AV54TFContratoOcorrenciaNotificacao_Cumprido_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO"), AV59TFContratoOcorrenciaNotificacao_Protocolo) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_SEL"), AV60TFContratoOcorrenciaNotificacao_Protocolo_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL"), ",", ".") != Convert.ToDecimal( AV63TFContratoOcorrenciaNotificacao_Responsavel )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_TO"), ",", ".") != Convert.ToDecimal( AV64TFContratoOcorrenciaNotificacao_Responsavel_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_NOME"), AV67TFContratoOcorrenciaNotificacao_Nome) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_NOME_SEL"), AV68TFContratoOcorrenciaNotificacao_Nome_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_DOCTO"), AV71TFContratoOcorrenciaNotificacao_Docto) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFCONTRATOOCORRENCIANOTIFICACAO_DOCTO_SEL"), AV72TFContratoOcorrenciaNotificacao_Docto_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E277A2 */
         E277A2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E277A2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         AV16DynamicFiltersSelector1 = "CONTRATOOCORRENCIANOTIFICACAO_DATA";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV22DynamicFiltersSelector2 = "CONTRATOOCORRENCIANOTIFICACAO_DATA";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22DynamicFiltersSelector2", AV22DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         edtavTfcontratoocorrencianotificacao_data_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontratoocorrencianotificacao_data_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoocorrencianotificacao_data_Visible), 5, 0)));
         edtavTfcontratoocorrencianotificacao_data_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontratoocorrencianotificacao_data_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoocorrencianotificacao_data_to_Visible), 5, 0)));
         edtavTfcontratoocorrencianotificacao_prazo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontratoocorrencianotificacao_prazo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoocorrencianotificacao_prazo_Visible), 5, 0)));
         edtavTfcontratoocorrencianotificacao_prazo_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontratoocorrencianotificacao_prazo_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoocorrencianotificacao_prazo_to_Visible), 5, 0)));
         edtavTfcontratoocorrencianotificacao_descricao_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontratoocorrencianotificacao_descricao_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoocorrencianotificacao_descricao_Visible), 5, 0)));
         edtavTfcontratoocorrencianotificacao_descricao_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontratoocorrencianotificacao_descricao_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoocorrencianotificacao_descricao_sel_Visible), 5, 0)));
         edtavTfcontratoocorrencianotificacao_cumprido_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontratoocorrencianotificacao_cumprido_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoocorrencianotificacao_cumprido_Visible), 5, 0)));
         edtavTfcontratoocorrencianotificacao_cumprido_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontratoocorrencianotificacao_cumprido_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoocorrencianotificacao_cumprido_to_Visible), 5, 0)));
         edtavTfcontratoocorrencianotificacao_protocolo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontratoocorrencianotificacao_protocolo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoocorrencianotificacao_protocolo_Visible), 5, 0)));
         edtavTfcontratoocorrencianotificacao_protocolo_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontratoocorrencianotificacao_protocolo_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoocorrencianotificacao_protocolo_sel_Visible), 5, 0)));
         edtavTfcontratoocorrencianotificacao_responsavel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontratoocorrencianotificacao_responsavel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoocorrencianotificacao_responsavel_Visible), 5, 0)));
         edtavTfcontratoocorrencianotificacao_responsavel_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontratoocorrencianotificacao_responsavel_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoocorrencianotificacao_responsavel_to_Visible), 5, 0)));
         edtavTfcontratoocorrencianotificacao_nome_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontratoocorrencianotificacao_nome_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoocorrencianotificacao_nome_Visible), 5, 0)));
         edtavTfcontratoocorrencianotificacao_nome_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontratoocorrencianotificacao_nome_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoocorrencianotificacao_nome_sel_Visible), 5, 0)));
         edtavTfcontratoocorrencianotificacao_docto_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontratoocorrencianotificacao_docto_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoocorrencianotificacao_docto_Visible), 5, 0)));
         edtavTfcontratoocorrencianotificacao_docto_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontratoocorrencianotificacao_docto_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoocorrencianotificacao_docto_sel_Visible), 5, 0)));
         Ddo_contratoocorrencianotificacao_data_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoOcorrenciaNotificacao_Data";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoocorrencianotificacao_data_Internalname, "TitleControlIdToReplace", Ddo_contratoocorrencianotificacao_data_Titlecontrolidtoreplace);
         AV43ddo_ContratoOcorrenciaNotificacao_DataTitleControlIdToReplace = Ddo_contratoocorrencianotificacao_data_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV43ddo_ContratoOcorrenciaNotificacao_DataTitleControlIdToReplace", AV43ddo_ContratoOcorrenciaNotificacao_DataTitleControlIdToReplace);
         edtavDdo_contratoocorrencianotificacao_datatitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_contratoocorrencianotificacao_datatitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratoocorrencianotificacao_datatitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratoocorrencianotificacao_prazo_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoOcorrenciaNotificacao_Prazo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoocorrencianotificacao_prazo_Internalname, "TitleControlIdToReplace", Ddo_contratoocorrencianotificacao_prazo_Titlecontrolidtoreplace);
         AV47ddo_ContratoOcorrenciaNotificacao_PrazoTitleControlIdToReplace = Ddo_contratoocorrencianotificacao_prazo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV47ddo_ContratoOcorrenciaNotificacao_PrazoTitleControlIdToReplace", AV47ddo_ContratoOcorrenciaNotificacao_PrazoTitleControlIdToReplace);
         edtavDdo_contratoocorrencianotificacao_prazotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_contratoocorrencianotificacao_prazotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratoocorrencianotificacao_prazotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratoocorrencianotificacao_descricao_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoOcorrenciaNotificacao_Descricao";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoocorrencianotificacao_descricao_Internalname, "TitleControlIdToReplace", Ddo_contratoocorrencianotificacao_descricao_Titlecontrolidtoreplace);
         AV51ddo_ContratoOcorrenciaNotificacao_DescricaoTitleControlIdToReplace = Ddo_contratoocorrencianotificacao_descricao_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV51ddo_ContratoOcorrenciaNotificacao_DescricaoTitleControlIdToReplace", AV51ddo_ContratoOcorrenciaNotificacao_DescricaoTitleControlIdToReplace);
         edtavDdo_contratoocorrencianotificacao_descricaotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_contratoocorrencianotificacao_descricaotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratoocorrencianotificacao_descricaotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratoocorrencianotificacao_cumprido_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoOcorrenciaNotificacao_Cumprido";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoocorrencianotificacao_cumprido_Internalname, "TitleControlIdToReplace", Ddo_contratoocorrencianotificacao_cumprido_Titlecontrolidtoreplace);
         AV57ddo_ContratoOcorrenciaNotificacao_CumpridoTitleControlIdToReplace = Ddo_contratoocorrencianotificacao_cumprido_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV57ddo_ContratoOcorrenciaNotificacao_CumpridoTitleControlIdToReplace", AV57ddo_ContratoOcorrenciaNotificacao_CumpridoTitleControlIdToReplace);
         edtavDdo_contratoocorrencianotificacao_cumpridotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_contratoocorrencianotificacao_cumpridotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratoocorrencianotificacao_cumpridotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratoocorrencianotificacao_protocolo_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoOcorrenciaNotificacao_Protocolo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoocorrencianotificacao_protocolo_Internalname, "TitleControlIdToReplace", Ddo_contratoocorrencianotificacao_protocolo_Titlecontrolidtoreplace);
         AV61ddo_ContratoOcorrenciaNotificacao_ProtocoloTitleControlIdToReplace = Ddo_contratoocorrencianotificacao_protocolo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV61ddo_ContratoOcorrenciaNotificacao_ProtocoloTitleControlIdToReplace", AV61ddo_ContratoOcorrenciaNotificacao_ProtocoloTitleControlIdToReplace);
         edtavDdo_contratoocorrencianotificacao_protocolotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_contratoocorrencianotificacao_protocolotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratoocorrencianotificacao_protocolotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratoocorrencianotificacao_responsavel_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoOcorrenciaNotificacao_Responsavel";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoocorrencianotificacao_responsavel_Internalname, "TitleControlIdToReplace", Ddo_contratoocorrencianotificacao_responsavel_Titlecontrolidtoreplace);
         AV65ddo_ContratoOcorrenciaNotificacao_ResponsavelTitleControlIdToReplace = Ddo_contratoocorrencianotificacao_responsavel_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV65ddo_ContratoOcorrenciaNotificacao_ResponsavelTitleControlIdToReplace", AV65ddo_ContratoOcorrenciaNotificacao_ResponsavelTitleControlIdToReplace);
         edtavDdo_contratoocorrencianotificacao_responsaveltitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_contratoocorrencianotificacao_responsaveltitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratoocorrencianotificacao_responsaveltitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratoocorrencianotificacao_nome_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoOcorrenciaNotificacao_Nome";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoocorrencianotificacao_nome_Internalname, "TitleControlIdToReplace", Ddo_contratoocorrencianotificacao_nome_Titlecontrolidtoreplace);
         AV69ddo_ContratoOcorrenciaNotificacao_NomeTitleControlIdToReplace = Ddo_contratoocorrencianotificacao_nome_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV69ddo_ContratoOcorrenciaNotificacao_NomeTitleControlIdToReplace", AV69ddo_ContratoOcorrenciaNotificacao_NomeTitleControlIdToReplace);
         edtavDdo_contratoocorrencianotificacao_nometitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_contratoocorrencianotificacao_nometitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratoocorrencianotificacao_nometitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratoocorrencianotificacao_docto_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoOcorrenciaNotificacao_Docto";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoocorrencianotificacao_docto_Internalname, "TitleControlIdToReplace", Ddo_contratoocorrencianotificacao_docto_Titlecontrolidtoreplace);
         AV73ddo_ContratoOcorrenciaNotificacao_DoctoTitleControlIdToReplace = Ddo_contratoocorrencianotificacao_docto_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV73ddo_ContratoOcorrenciaNotificacao_DoctoTitleControlIdToReplace", AV73ddo_ContratoOcorrenciaNotificacao_DoctoTitleControlIdToReplace);
         edtavDdo_contratoocorrencianotificacao_doctotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_contratoocorrencianotificacao_doctotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratoocorrencianotificacao_doctotitlecontrolidtoreplace_Visible), 5, 0)));
         edtContratoOcorrencia_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratoOcorrencia_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoOcorrencia_Codigo_Visible), 5, 0)));
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "da Garantia", 0);
         cmbavOrderedby.addItem("2", "Data", 0);
         cmbavOrderedby.addItem("3", "Prazo", 0);
         cmbavOrderedby.addItem("4", "Descri��o", 0);
         cmbavOrderedby.addItem("5", "D. Cumprimento", 0);
         cmbavOrderedby.addItem("6", "Protocolo", 0);
         cmbavOrderedby.addItem("7", "pessoa respons�vel", 0);
         cmbavOrderedby.addItem("8", "Respons�vel", 0);
         cmbavOrderedby.addItem("9", "do respons�vel", 0);
         if ( AV14OrderedBy < 1 )
         {
            AV14OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV74DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV74DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E287A2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV38ContratoOcorrenciaNotificacao_DataTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV44ContratoOcorrenciaNotificacao_PrazoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV48ContratoOcorrenciaNotificacao_DescricaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV52ContratoOcorrenciaNotificacao_CumpridoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV58ContratoOcorrenciaNotificacao_ProtocoloTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV62ContratoOcorrenciaNotificacao_ResponsavelTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV66ContratoOcorrenciaNotificacao_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV70ContratoOcorrenciaNotificacao_DoctoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtContratoOcorrenciaNotificacao_Data_Titleformat = 2;
         edtContratoOcorrenciaNotificacao_Data_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Data", AV43ddo_ContratoOcorrenciaNotificacao_DataTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratoOcorrenciaNotificacao_Data_Internalname, "Title", edtContratoOcorrenciaNotificacao_Data_Title);
         edtContratoOcorrenciaNotificacao_Prazo_Titleformat = 2;
         edtContratoOcorrenciaNotificacao_Prazo_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Prazo", AV47ddo_ContratoOcorrenciaNotificacao_PrazoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratoOcorrenciaNotificacao_Prazo_Internalname, "Title", edtContratoOcorrenciaNotificacao_Prazo_Title);
         edtContratoOcorrenciaNotificacao_Descricao_Titleformat = 2;
         edtContratoOcorrenciaNotificacao_Descricao_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Descri��o", AV51ddo_ContratoOcorrenciaNotificacao_DescricaoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratoOcorrenciaNotificacao_Descricao_Internalname, "Title", edtContratoOcorrenciaNotificacao_Descricao_Title);
         edtContratoOcorrenciaNotificacao_Cumprido_Titleformat = 2;
         edtContratoOcorrenciaNotificacao_Cumprido_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "D. Cumprimento", AV57ddo_ContratoOcorrenciaNotificacao_CumpridoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratoOcorrenciaNotificacao_Cumprido_Internalname, "Title", edtContratoOcorrenciaNotificacao_Cumprido_Title);
         edtContratoOcorrenciaNotificacao_Protocolo_Titleformat = 2;
         edtContratoOcorrenciaNotificacao_Protocolo_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Protocolo", AV61ddo_ContratoOcorrenciaNotificacao_ProtocoloTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratoOcorrenciaNotificacao_Protocolo_Internalname, "Title", edtContratoOcorrenciaNotificacao_Protocolo_Title);
         edtContratoOcorrenciaNotificacao_Responsavel_Titleformat = 2;
         edtContratoOcorrenciaNotificacao_Responsavel_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "pessoa respons�vel", AV65ddo_ContratoOcorrenciaNotificacao_ResponsavelTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratoOcorrenciaNotificacao_Responsavel_Internalname, "Title", edtContratoOcorrenciaNotificacao_Responsavel_Title);
         edtContratoOcorrenciaNotificacao_Nome_Titleformat = 2;
         edtContratoOcorrenciaNotificacao_Nome_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Respons�vel", AV69ddo_ContratoOcorrenciaNotificacao_NomeTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratoOcorrenciaNotificacao_Nome_Internalname, "Title", edtContratoOcorrenciaNotificacao_Nome_Title);
         edtContratoOcorrenciaNotificacao_Docto_Titleformat = 2;
         edtContratoOcorrenciaNotificacao_Docto_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "do respons�vel", AV73ddo_ContratoOcorrenciaNotificacao_DoctoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratoOcorrenciaNotificacao_Docto_Internalname, "Title", edtContratoOcorrenciaNotificacao_Docto_Title);
         AV76GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV76GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV76GridCurrentPage), 10, 0)));
         AV77GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV77GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV77GridPageCount), 10, 0)));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV38ContratoOcorrenciaNotificacao_DataTitleFilterData", AV38ContratoOcorrenciaNotificacao_DataTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV44ContratoOcorrenciaNotificacao_PrazoTitleFilterData", AV44ContratoOcorrenciaNotificacao_PrazoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV48ContratoOcorrenciaNotificacao_DescricaoTitleFilterData", AV48ContratoOcorrenciaNotificacao_DescricaoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV52ContratoOcorrenciaNotificacao_CumpridoTitleFilterData", AV52ContratoOcorrenciaNotificacao_CumpridoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV58ContratoOcorrenciaNotificacao_ProtocoloTitleFilterData", AV58ContratoOcorrenciaNotificacao_ProtocoloTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV62ContratoOcorrenciaNotificacao_ResponsavelTitleFilterData", AV62ContratoOcorrenciaNotificacao_ResponsavelTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV66ContratoOcorrenciaNotificacao_NomeTitleFilterData", AV66ContratoOcorrenciaNotificacao_NomeTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV70ContratoOcorrenciaNotificacao_DoctoTitleFilterData", AV70ContratoOcorrenciaNotificacao_DoctoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV11GridState", AV11GridState);
      }

      protected void E117A2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV75PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV75PageToGo) ;
         }
      }

      protected void E127A2( )
      {
         /* Ddo_contratoocorrencianotificacao_data_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratoocorrencianotificacao_data_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_contratoocorrencianotificacao_data_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoocorrencianotificacao_data_Internalname, "SortedStatus", Ddo_contratoocorrencianotificacao_data_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_contratoocorrencianotificacao_data_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_contratoocorrencianotificacao_data_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoocorrencianotificacao_data_Internalname, "SortedStatus", Ddo_contratoocorrencianotificacao_data_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_contratoocorrencianotificacao_data_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV39TFContratoOcorrenciaNotificacao_Data = context.localUtil.CToD( Ddo_contratoocorrencianotificacao_data_Filteredtext_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV39TFContratoOcorrenciaNotificacao_Data", context.localUtil.Format(AV39TFContratoOcorrenciaNotificacao_Data, "99/99/99"));
            AV40TFContratoOcorrenciaNotificacao_Data_To = context.localUtil.CToD( Ddo_contratoocorrencianotificacao_data_Filteredtextto_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV40TFContratoOcorrenciaNotificacao_Data_To", context.localUtil.Format(AV40TFContratoOcorrenciaNotificacao_Data_To, "99/99/99"));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E137A2( )
      {
         /* Ddo_contratoocorrencianotificacao_prazo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratoocorrencianotificacao_prazo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_contratoocorrencianotificacao_prazo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoocorrencianotificacao_prazo_Internalname, "SortedStatus", Ddo_contratoocorrencianotificacao_prazo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_contratoocorrencianotificacao_prazo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_contratoocorrencianotificacao_prazo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoocorrencianotificacao_prazo_Internalname, "SortedStatus", Ddo_contratoocorrencianotificacao_prazo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_contratoocorrencianotificacao_prazo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV45TFContratoOcorrenciaNotificacao_Prazo = (short)(NumberUtil.Val( Ddo_contratoocorrencianotificacao_prazo_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV45TFContratoOcorrenciaNotificacao_Prazo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV45TFContratoOcorrenciaNotificacao_Prazo), 4, 0)));
            AV46TFContratoOcorrenciaNotificacao_Prazo_To = (short)(NumberUtil.Val( Ddo_contratoocorrencianotificacao_prazo_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV46TFContratoOcorrenciaNotificacao_Prazo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV46TFContratoOcorrenciaNotificacao_Prazo_To), 4, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E147A2( )
      {
         /* Ddo_contratoocorrencianotificacao_descricao_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratoocorrencianotificacao_descricao_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_contratoocorrencianotificacao_descricao_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoocorrencianotificacao_descricao_Internalname, "SortedStatus", Ddo_contratoocorrencianotificacao_descricao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_contratoocorrencianotificacao_descricao_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_contratoocorrencianotificacao_descricao_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoocorrencianotificacao_descricao_Internalname, "SortedStatus", Ddo_contratoocorrencianotificacao_descricao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_contratoocorrencianotificacao_descricao_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV49TFContratoOcorrenciaNotificacao_Descricao = Ddo_contratoocorrencianotificacao_descricao_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV49TFContratoOcorrenciaNotificacao_Descricao", AV49TFContratoOcorrenciaNotificacao_Descricao);
            AV50TFContratoOcorrenciaNotificacao_Descricao_Sel = Ddo_contratoocorrencianotificacao_descricao_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV50TFContratoOcorrenciaNotificacao_Descricao_Sel", AV50TFContratoOcorrenciaNotificacao_Descricao_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E157A2( )
      {
         /* Ddo_contratoocorrencianotificacao_cumprido_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratoocorrencianotificacao_cumprido_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_contratoocorrencianotificacao_cumprido_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoocorrencianotificacao_cumprido_Internalname, "SortedStatus", Ddo_contratoocorrencianotificacao_cumprido_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_contratoocorrencianotificacao_cumprido_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_contratoocorrencianotificacao_cumprido_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoocorrencianotificacao_cumprido_Internalname, "SortedStatus", Ddo_contratoocorrencianotificacao_cumprido_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_contratoocorrencianotificacao_cumprido_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV53TFContratoOcorrenciaNotificacao_Cumprido = context.localUtil.CToD( Ddo_contratoocorrencianotificacao_cumprido_Filteredtext_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV53TFContratoOcorrenciaNotificacao_Cumprido", context.localUtil.Format(AV53TFContratoOcorrenciaNotificacao_Cumprido, "99/99/99"));
            AV54TFContratoOcorrenciaNotificacao_Cumprido_To = context.localUtil.CToD( Ddo_contratoocorrencianotificacao_cumprido_Filteredtextto_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV54TFContratoOcorrenciaNotificacao_Cumprido_To", context.localUtil.Format(AV54TFContratoOcorrenciaNotificacao_Cumprido_To, "99/99/99"));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E167A2( )
      {
         /* Ddo_contratoocorrencianotificacao_protocolo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratoocorrencianotificacao_protocolo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 6;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_contratoocorrencianotificacao_protocolo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoocorrencianotificacao_protocolo_Internalname, "SortedStatus", Ddo_contratoocorrencianotificacao_protocolo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_contratoocorrencianotificacao_protocolo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 6;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_contratoocorrencianotificacao_protocolo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoocorrencianotificacao_protocolo_Internalname, "SortedStatus", Ddo_contratoocorrencianotificacao_protocolo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_contratoocorrencianotificacao_protocolo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV59TFContratoOcorrenciaNotificacao_Protocolo = Ddo_contratoocorrencianotificacao_protocolo_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV59TFContratoOcorrenciaNotificacao_Protocolo", AV59TFContratoOcorrenciaNotificacao_Protocolo);
            AV60TFContratoOcorrenciaNotificacao_Protocolo_Sel = Ddo_contratoocorrencianotificacao_protocolo_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV60TFContratoOcorrenciaNotificacao_Protocolo_Sel", AV60TFContratoOcorrenciaNotificacao_Protocolo_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E177A2( )
      {
         /* Ddo_contratoocorrencianotificacao_responsavel_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratoocorrencianotificacao_responsavel_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 7;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_contratoocorrencianotificacao_responsavel_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoocorrencianotificacao_responsavel_Internalname, "SortedStatus", Ddo_contratoocorrencianotificacao_responsavel_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_contratoocorrencianotificacao_responsavel_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 7;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_contratoocorrencianotificacao_responsavel_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoocorrencianotificacao_responsavel_Internalname, "SortedStatus", Ddo_contratoocorrencianotificacao_responsavel_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_contratoocorrencianotificacao_responsavel_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV63TFContratoOcorrenciaNotificacao_Responsavel = (int)(NumberUtil.Val( Ddo_contratoocorrencianotificacao_responsavel_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV63TFContratoOcorrenciaNotificacao_Responsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(AV63TFContratoOcorrenciaNotificacao_Responsavel), 6, 0)));
            AV64TFContratoOcorrenciaNotificacao_Responsavel_To = (int)(NumberUtil.Val( Ddo_contratoocorrencianotificacao_responsavel_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV64TFContratoOcorrenciaNotificacao_Responsavel_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV64TFContratoOcorrenciaNotificacao_Responsavel_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E187A2( )
      {
         /* Ddo_contratoocorrencianotificacao_nome_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratoocorrencianotificacao_nome_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 8;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_contratoocorrencianotificacao_nome_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoocorrencianotificacao_nome_Internalname, "SortedStatus", Ddo_contratoocorrencianotificacao_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_contratoocorrencianotificacao_nome_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 8;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_contratoocorrencianotificacao_nome_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoocorrencianotificacao_nome_Internalname, "SortedStatus", Ddo_contratoocorrencianotificacao_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_contratoocorrencianotificacao_nome_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV67TFContratoOcorrenciaNotificacao_Nome = Ddo_contratoocorrencianotificacao_nome_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV67TFContratoOcorrenciaNotificacao_Nome", AV67TFContratoOcorrenciaNotificacao_Nome);
            AV68TFContratoOcorrenciaNotificacao_Nome_Sel = Ddo_contratoocorrencianotificacao_nome_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV68TFContratoOcorrenciaNotificacao_Nome_Sel", AV68TFContratoOcorrenciaNotificacao_Nome_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E197A2( )
      {
         /* Ddo_contratoocorrencianotificacao_docto_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratoocorrencianotificacao_docto_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 9;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_contratoocorrencianotificacao_docto_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoocorrencianotificacao_docto_Internalname, "SortedStatus", Ddo_contratoocorrencianotificacao_docto_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_contratoocorrencianotificacao_docto_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 9;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_contratoocorrencianotificacao_docto_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoocorrencianotificacao_docto_Internalname, "SortedStatus", Ddo_contratoocorrencianotificacao_docto_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_contratoocorrencianotificacao_docto_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV71TFContratoOcorrenciaNotificacao_Docto = Ddo_contratoocorrencianotificacao_docto_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV71TFContratoOcorrenciaNotificacao_Docto", AV71TFContratoOcorrenciaNotificacao_Docto);
            AV72TFContratoOcorrenciaNotificacao_Docto_Sel = Ddo_contratoocorrencianotificacao_docto_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV72TFContratoOcorrenciaNotificacao_Docto_Sel", AV72TFContratoOcorrenciaNotificacao_Docto_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      private void E297A2( )
      {
         /* Grid_Load Routine */
         AV35Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavDelete_Internalname, AV35Delete);
         AV80Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = formatLink("contratoocorrencianotificacao.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A297ContratoOcorrenciaNotificacao_Codigo) + "," + UrlEncode("" +A294ContratoOcorrencia_Codigo);
         AV36Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavUpdate_Internalname, AV36Update);
         AV81Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = formatLink("contratoocorrencianotificacao.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A297ContratoOcorrenciaNotificacao_Codigo) + "," + UrlEncode("" +A294ContratoOcorrencia_Codigo);
         edtContratoOcorrenciaNotificacao_Data_Link = formatLink("viewcontratoocorrencianotificacao.aspx") + "?" + UrlEncode("" +A297ContratoOcorrenciaNotificacao_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         edtContratoOcorrenciaNotificacao_Nome_Link = formatLink("viewpessoa.aspx") + "?" + UrlEncode("" +A303ContratoOcorrenciaNotificacao_Responsavel) + "," + UrlEncode(StringUtil.RTrim(""));
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 84;
         }
         sendrow_842( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_84_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(84, GridRow);
         }
      }

      protected void E207A2( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefreshCmp(sPrefix);
      }

      protected void E247A2( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV21DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21DynamicFiltersEnabled2", AV21DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
      }

      protected void E217A2( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV33DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV33DynamicFiltersRemoving", AV33DynamicFiltersRemoving);
         AV34DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV34DynamicFiltersIgnoreFirst", AV34DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV33DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV33DynamicFiltersRemoving", AV33DynamicFiltersRemoving);
         AV34DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV34DynamicFiltersIgnoreFirst", AV34DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17ContratoOcorrenciaNotificacao_Data1, AV18ContratoOcorrenciaNotificacao_Data_To1, AV19ContratoOcorrenciaNotificacao_Prazo1, AV20ContratoOcorrenciaNotificacao_Prazo_To1, AV22DynamicFiltersSelector2, AV23ContratoOcorrenciaNotificacao_Data2, AV24ContratoOcorrenciaNotificacao_Data_To2, AV25ContratoOcorrenciaNotificacao_Prazo2, AV26ContratoOcorrenciaNotificacao_Prazo_To2, AV21DynamicFiltersEnabled2, AV39TFContratoOcorrenciaNotificacao_Data, AV40TFContratoOcorrenciaNotificacao_Data_To, AV45TFContratoOcorrenciaNotificacao_Prazo, AV46TFContratoOcorrenciaNotificacao_Prazo_To, AV49TFContratoOcorrenciaNotificacao_Descricao, AV50TFContratoOcorrenciaNotificacao_Descricao_Sel, AV53TFContratoOcorrenciaNotificacao_Cumprido, AV54TFContratoOcorrenciaNotificacao_Cumprido_To, AV59TFContratoOcorrenciaNotificacao_Protocolo, AV60TFContratoOcorrenciaNotificacao_Protocolo_Sel, AV63TFContratoOcorrenciaNotificacao_Responsavel, AV64TFContratoOcorrenciaNotificacao_Responsavel_To, AV67TFContratoOcorrenciaNotificacao_Nome, AV68TFContratoOcorrenciaNotificacao_Nome_Sel, AV71TFContratoOcorrenciaNotificacao_Docto, AV72TFContratoOcorrenciaNotificacao_Docto_Sel, AV7ContratoOcorrencia_Codigo, AV43ddo_ContratoOcorrenciaNotificacao_DataTitleControlIdToReplace, AV47ddo_ContratoOcorrenciaNotificacao_PrazoTitleControlIdToReplace, AV51ddo_ContratoOcorrenciaNotificacao_DescricaoTitleControlIdToReplace, AV57ddo_ContratoOcorrenciaNotificacao_CumpridoTitleControlIdToReplace, AV61ddo_ContratoOcorrenciaNotificacao_ProtocoloTitleControlIdToReplace, AV65ddo_ContratoOcorrenciaNotificacao_ResponsavelTitleControlIdToReplace, AV69ddo_ContratoOcorrenciaNotificacao_NomeTitleControlIdToReplace, AV73ddo_ContratoOcorrenciaNotificacao_DoctoTitleControlIdToReplace, AV82Pgmname, AV11GridState, AV34DynamicFiltersIgnoreFirst, AV33DynamicFiltersRemoving, A297ContratoOcorrenciaNotificacao_Codigo, A294ContratoOcorrencia_Codigo, A303ContratoOcorrenciaNotificacao_Responsavel, sPrefix) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV11GridState", AV11GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV22DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E257A2( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E227A2( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV33DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV33DynamicFiltersRemoving", AV33DynamicFiltersRemoving);
         AV21DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21DynamicFiltersEnabled2", AV21DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV33DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV33DynamicFiltersRemoving", AV33DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17ContratoOcorrenciaNotificacao_Data1, AV18ContratoOcorrenciaNotificacao_Data_To1, AV19ContratoOcorrenciaNotificacao_Prazo1, AV20ContratoOcorrenciaNotificacao_Prazo_To1, AV22DynamicFiltersSelector2, AV23ContratoOcorrenciaNotificacao_Data2, AV24ContratoOcorrenciaNotificacao_Data_To2, AV25ContratoOcorrenciaNotificacao_Prazo2, AV26ContratoOcorrenciaNotificacao_Prazo_To2, AV21DynamicFiltersEnabled2, AV39TFContratoOcorrenciaNotificacao_Data, AV40TFContratoOcorrenciaNotificacao_Data_To, AV45TFContratoOcorrenciaNotificacao_Prazo, AV46TFContratoOcorrenciaNotificacao_Prazo_To, AV49TFContratoOcorrenciaNotificacao_Descricao, AV50TFContratoOcorrenciaNotificacao_Descricao_Sel, AV53TFContratoOcorrenciaNotificacao_Cumprido, AV54TFContratoOcorrenciaNotificacao_Cumprido_To, AV59TFContratoOcorrenciaNotificacao_Protocolo, AV60TFContratoOcorrenciaNotificacao_Protocolo_Sel, AV63TFContratoOcorrenciaNotificacao_Responsavel, AV64TFContratoOcorrenciaNotificacao_Responsavel_To, AV67TFContratoOcorrenciaNotificacao_Nome, AV68TFContratoOcorrenciaNotificacao_Nome_Sel, AV71TFContratoOcorrenciaNotificacao_Docto, AV72TFContratoOcorrenciaNotificacao_Docto_Sel, AV7ContratoOcorrencia_Codigo, AV43ddo_ContratoOcorrenciaNotificacao_DataTitleControlIdToReplace, AV47ddo_ContratoOcorrenciaNotificacao_PrazoTitleControlIdToReplace, AV51ddo_ContratoOcorrenciaNotificacao_DescricaoTitleControlIdToReplace, AV57ddo_ContratoOcorrenciaNotificacao_CumpridoTitleControlIdToReplace, AV61ddo_ContratoOcorrenciaNotificacao_ProtocoloTitleControlIdToReplace, AV65ddo_ContratoOcorrenciaNotificacao_ResponsavelTitleControlIdToReplace, AV69ddo_ContratoOcorrenciaNotificacao_NomeTitleControlIdToReplace, AV73ddo_ContratoOcorrenciaNotificacao_DoctoTitleControlIdToReplace, AV82Pgmname, AV11GridState, AV34DynamicFiltersIgnoreFirst, AV33DynamicFiltersRemoving, A297ContratoOcorrenciaNotificacao_Codigo, A294ContratoOcorrencia_Codigo, A303ContratoOcorrenciaNotificacao_Responsavel, sPrefix) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV11GridState", AV11GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV22DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E267A2( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E237A2( )
      {
         /* 'DoInsert' Routine */
         context.wjLoc = formatLink("contratoocorrencianotificacao.aspx") + "?" + UrlEncode(StringUtil.RTrim("INS")) + "," + UrlEncode("" +0) + "," + UrlEncode("" +AV7ContratoOcorrencia_Codigo);
         context.wjLocDisableFrm = 1;
      }

      protected void S172( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_contratoocorrencianotificacao_data_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoocorrencianotificacao_data_Internalname, "SortedStatus", Ddo_contratoocorrencianotificacao_data_Sortedstatus);
         Ddo_contratoocorrencianotificacao_prazo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoocorrencianotificacao_prazo_Internalname, "SortedStatus", Ddo_contratoocorrencianotificacao_prazo_Sortedstatus);
         Ddo_contratoocorrencianotificacao_descricao_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoocorrencianotificacao_descricao_Internalname, "SortedStatus", Ddo_contratoocorrencianotificacao_descricao_Sortedstatus);
         Ddo_contratoocorrencianotificacao_cumprido_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoocorrencianotificacao_cumprido_Internalname, "SortedStatus", Ddo_contratoocorrencianotificacao_cumprido_Sortedstatus);
         Ddo_contratoocorrencianotificacao_protocolo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoocorrencianotificacao_protocolo_Internalname, "SortedStatus", Ddo_contratoocorrencianotificacao_protocolo_Sortedstatus);
         Ddo_contratoocorrencianotificacao_responsavel_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoocorrencianotificacao_responsavel_Internalname, "SortedStatus", Ddo_contratoocorrencianotificacao_responsavel_Sortedstatus);
         Ddo_contratoocorrencianotificacao_nome_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoocorrencianotificacao_nome_Internalname, "SortedStatus", Ddo_contratoocorrencianotificacao_nome_Sortedstatus);
         Ddo_contratoocorrencianotificacao_docto_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoocorrencianotificacao_docto_Internalname, "SortedStatus", Ddo_contratoocorrencianotificacao_docto_Sortedstatus);
      }

      protected void S152( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV14OrderedBy == 2 )
         {
            Ddo_contratoocorrencianotificacao_data_Sortedstatus = (AV15OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoocorrencianotificacao_data_Internalname, "SortedStatus", Ddo_contratoocorrencianotificacao_data_Sortedstatus);
         }
         else if ( AV14OrderedBy == 3 )
         {
            Ddo_contratoocorrencianotificacao_prazo_Sortedstatus = (AV15OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoocorrencianotificacao_prazo_Internalname, "SortedStatus", Ddo_contratoocorrencianotificacao_prazo_Sortedstatus);
         }
         else if ( AV14OrderedBy == 4 )
         {
            Ddo_contratoocorrencianotificacao_descricao_Sortedstatus = (AV15OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoocorrencianotificacao_descricao_Internalname, "SortedStatus", Ddo_contratoocorrencianotificacao_descricao_Sortedstatus);
         }
         else if ( AV14OrderedBy == 5 )
         {
            Ddo_contratoocorrencianotificacao_cumprido_Sortedstatus = (AV15OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoocorrencianotificacao_cumprido_Internalname, "SortedStatus", Ddo_contratoocorrencianotificacao_cumprido_Sortedstatus);
         }
         else if ( AV14OrderedBy == 6 )
         {
            Ddo_contratoocorrencianotificacao_protocolo_Sortedstatus = (AV15OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoocorrencianotificacao_protocolo_Internalname, "SortedStatus", Ddo_contratoocorrencianotificacao_protocolo_Sortedstatus);
         }
         else if ( AV14OrderedBy == 7 )
         {
            Ddo_contratoocorrencianotificacao_responsavel_Sortedstatus = (AV15OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoocorrencianotificacao_responsavel_Internalname, "SortedStatus", Ddo_contratoocorrencianotificacao_responsavel_Sortedstatus);
         }
         else if ( AV14OrderedBy == 8 )
         {
            Ddo_contratoocorrencianotificacao_nome_Sortedstatus = (AV15OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoocorrencianotificacao_nome_Internalname, "SortedStatus", Ddo_contratoocorrencianotificacao_nome_Sortedstatus);
         }
         else if ( AV14OrderedBy == 9 )
         {
            Ddo_contratoocorrencianotificacao_docto_Sortedstatus = (AV15OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoocorrencianotificacao_docto_Internalname, "SortedStatus", Ddo_contratoocorrencianotificacao_docto_Sortedstatus);
         }
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         tblTablemergeddynamicfilterscontratoocorrencianotificacao_data1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, tblTablemergeddynamicfilterscontratoocorrencianotificacao_data1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontratoocorrencianotificacao_data1_Visible), 5, 0)));
         tblTablemergeddynamicfilterscontratoocorrencianotificacao_prazo1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, tblTablemergeddynamicfilterscontratoocorrencianotificacao_prazo1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontratoocorrencianotificacao_prazo1_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 )
         {
            tblTablemergeddynamicfilterscontratoocorrencianotificacao_data1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, tblTablemergeddynamicfilterscontratoocorrencianotificacao_data1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontratoocorrencianotificacao_data1_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTRATOOCORRENCIANOTIFICACAO_PRAZO") == 0 )
         {
            tblTablemergeddynamicfilterscontratoocorrencianotificacao_prazo1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, tblTablemergeddynamicfilterscontratoocorrencianotificacao_prazo1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontratoocorrencianotificacao_prazo1_Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         tblTablemergeddynamicfilterscontratoocorrencianotificacao_data2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, tblTablemergeddynamicfilterscontratoocorrencianotificacao_data2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontratoocorrencianotificacao_data2_Visible), 5, 0)));
         tblTablemergeddynamicfilterscontratoocorrencianotificacao_prazo2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, tblTablemergeddynamicfilterscontratoocorrencianotificacao_prazo2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontratoocorrencianotificacao_prazo2_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV22DynamicFiltersSelector2, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 )
         {
            tblTablemergeddynamicfilterscontratoocorrencianotificacao_data2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, tblTablemergeddynamicfilterscontratoocorrencianotificacao_data2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontratoocorrencianotificacao_data2_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV22DynamicFiltersSelector2, "CONTRATOOCORRENCIANOTIFICACAO_PRAZO") == 0 )
         {
            tblTablemergeddynamicfilterscontratoocorrencianotificacao_prazo2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, tblTablemergeddynamicfilterscontratoocorrencianotificacao_prazo2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontratoocorrencianotificacao_prazo2_Visible), 5, 0)));
         }
      }

      protected void S192( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV21DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21DynamicFiltersEnabled2", AV21DynamicFiltersEnabled2);
         AV22DynamicFiltersSelector2 = "CONTRATOOCORRENCIANOTIFICACAO_DATA";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22DynamicFiltersSelector2", AV22DynamicFiltersSelector2);
         AV23ContratoOcorrenciaNotificacao_Data2 = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23ContratoOcorrenciaNotificacao_Data2", context.localUtil.Format(AV23ContratoOcorrenciaNotificacao_Data2, "99/99/99"));
         AV24ContratoOcorrenciaNotificacao_Data_To2 = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24ContratoOcorrenciaNotificacao_Data_To2", context.localUtil.Format(AV24ContratoOcorrenciaNotificacao_Data_To2, "99/99/99"));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S142( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV37Session.Get(AV82Pgmname+"GridState"), "") == 0 )
         {
            AV11GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV82Pgmname+"GridState"), "");
         }
         else
         {
            AV11GridState.FromXml(AV37Session.Get(AV82Pgmname+"GridState"), "");
         }
         AV14OrderedBy = AV11GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
         AV15OrderedDsc = AV11GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV83GXV1 = 1;
         while ( AV83GXV1 <= AV11GridState.gxTpr_Filtervalues.Count )
         {
            AV12GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV11GridState.gxTpr_Filtervalues.Item(AV83GXV1));
            if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFCONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 )
            {
               AV39TFContratoOcorrenciaNotificacao_Data = context.localUtil.CToD( AV12GridStateFilterValue.gxTpr_Value, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV39TFContratoOcorrenciaNotificacao_Data", context.localUtil.Format(AV39TFContratoOcorrenciaNotificacao_Data, "99/99/99"));
               AV40TFContratoOcorrenciaNotificacao_Data_To = context.localUtil.CToD( AV12GridStateFilterValue.gxTpr_Valueto, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV40TFContratoOcorrenciaNotificacao_Data_To", context.localUtil.Format(AV40TFContratoOcorrenciaNotificacao_Data_To, "99/99/99"));
               if ( ! (DateTime.MinValue==AV39TFContratoOcorrenciaNotificacao_Data) )
               {
                  Ddo_contratoocorrencianotificacao_data_Filteredtext_set = context.localUtil.DToC( AV39TFContratoOcorrenciaNotificacao_Data, 2, "/");
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoocorrencianotificacao_data_Internalname, "FilteredText_set", Ddo_contratoocorrencianotificacao_data_Filteredtext_set);
               }
               if ( ! (DateTime.MinValue==AV40TFContratoOcorrenciaNotificacao_Data_To) )
               {
                  Ddo_contratoocorrencianotificacao_data_Filteredtextto_set = context.localUtil.DToC( AV40TFContratoOcorrenciaNotificacao_Data_To, 2, "/");
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoocorrencianotificacao_data_Internalname, "FilteredTextTo_set", Ddo_contratoocorrencianotificacao_data_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFCONTRATOOCORRENCIANOTIFICACAO_PRAZO") == 0 )
            {
               AV45TFContratoOcorrenciaNotificacao_Prazo = (short)(NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV45TFContratoOcorrenciaNotificacao_Prazo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV45TFContratoOcorrenciaNotificacao_Prazo), 4, 0)));
               AV46TFContratoOcorrenciaNotificacao_Prazo_To = (short)(NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Valueto, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV46TFContratoOcorrenciaNotificacao_Prazo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV46TFContratoOcorrenciaNotificacao_Prazo_To), 4, 0)));
               if ( ! (0==AV45TFContratoOcorrenciaNotificacao_Prazo) )
               {
                  Ddo_contratoocorrencianotificacao_prazo_Filteredtext_set = StringUtil.Str( (decimal)(AV45TFContratoOcorrenciaNotificacao_Prazo), 4, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoocorrencianotificacao_prazo_Internalname, "FilteredText_set", Ddo_contratoocorrencianotificacao_prazo_Filteredtext_set);
               }
               if ( ! (0==AV46TFContratoOcorrenciaNotificacao_Prazo_To) )
               {
                  Ddo_contratoocorrencianotificacao_prazo_Filteredtextto_set = StringUtil.Str( (decimal)(AV46TFContratoOcorrenciaNotificacao_Prazo_To), 4, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoocorrencianotificacao_prazo_Internalname, "FilteredTextTo_set", Ddo_contratoocorrencianotificacao_prazo_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO") == 0 )
            {
               AV49TFContratoOcorrenciaNotificacao_Descricao = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV49TFContratoOcorrenciaNotificacao_Descricao", AV49TFContratoOcorrenciaNotificacao_Descricao);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49TFContratoOcorrenciaNotificacao_Descricao)) )
               {
                  Ddo_contratoocorrencianotificacao_descricao_Filteredtext_set = AV49TFContratoOcorrenciaNotificacao_Descricao;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoocorrencianotificacao_descricao_Internalname, "FilteredText_set", Ddo_contratoocorrencianotificacao_descricao_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_SEL") == 0 )
            {
               AV50TFContratoOcorrenciaNotificacao_Descricao_Sel = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV50TFContratoOcorrenciaNotificacao_Descricao_Sel", AV50TFContratoOcorrenciaNotificacao_Descricao_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV50TFContratoOcorrenciaNotificacao_Descricao_Sel)) )
               {
                  Ddo_contratoocorrencianotificacao_descricao_Selectedvalue_set = AV50TFContratoOcorrenciaNotificacao_Descricao_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoocorrencianotificacao_descricao_Internalname, "SelectedValue_set", Ddo_contratoocorrencianotificacao_descricao_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO") == 0 )
            {
               AV53TFContratoOcorrenciaNotificacao_Cumprido = context.localUtil.CToD( AV12GridStateFilterValue.gxTpr_Value, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV53TFContratoOcorrenciaNotificacao_Cumprido", context.localUtil.Format(AV53TFContratoOcorrenciaNotificacao_Cumprido, "99/99/99"));
               AV54TFContratoOcorrenciaNotificacao_Cumprido_To = context.localUtil.CToD( AV12GridStateFilterValue.gxTpr_Valueto, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV54TFContratoOcorrenciaNotificacao_Cumprido_To", context.localUtil.Format(AV54TFContratoOcorrenciaNotificacao_Cumprido_To, "99/99/99"));
               if ( ! (DateTime.MinValue==AV53TFContratoOcorrenciaNotificacao_Cumprido) )
               {
                  Ddo_contratoocorrencianotificacao_cumprido_Filteredtext_set = context.localUtil.DToC( AV53TFContratoOcorrenciaNotificacao_Cumprido, 2, "/");
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoocorrencianotificacao_cumprido_Internalname, "FilteredText_set", Ddo_contratoocorrencianotificacao_cumprido_Filteredtext_set);
               }
               if ( ! (DateTime.MinValue==AV54TFContratoOcorrenciaNotificacao_Cumprido_To) )
               {
                  Ddo_contratoocorrencianotificacao_cumprido_Filteredtextto_set = context.localUtil.DToC( AV54TFContratoOcorrenciaNotificacao_Cumprido_To, 2, "/");
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoocorrencianotificacao_cumprido_Internalname, "FilteredTextTo_set", Ddo_contratoocorrencianotificacao_cumprido_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO") == 0 )
            {
               AV59TFContratoOcorrenciaNotificacao_Protocolo = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV59TFContratoOcorrenciaNotificacao_Protocolo", AV59TFContratoOcorrenciaNotificacao_Protocolo);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59TFContratoOcorrenciaNotificacao_Protocolo)) )
               {
                  Ddo_contratoocorrencianotificacao_protocolo_Filteredtext_set = AV59TFContratoOcorrenciaNotificacao_Protocolo;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoocorrencianotificacao_protocolo_Internalname, "FilteredText_set", Ddo_contratoocorrencianotificacao_protocolo_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_SEL") == 0 )
            {
               AV60TFContratoOcorrenciaNotificacao_Protocolo_Sel = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV60TFContratoOcorrenciaNotificacao_Protocolo_Sel", AV60TFContratoOcorrenciaNotificacao_Protocolo_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60TFContratoOcorrenciaNotificacao_Protocolo_Sel)) )
               {
                  Ddo_contratoocorrencianotificacao_protocolo_Selectedvalue_set = AV60TFContratoOcorrenciaNotificacao_Protocolo_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoocorrencianotificacao_protocolo_Internalname, "SelectedValue_set", Ddo_contratoocorrencianotificacao_protocolo_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFCONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL") == 0 )
            {
               AV63TFContratoOcorrenciaNotificacao_Responsavel = (int)(NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV63TFContratoOcorrenciaNotificacao_Responsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(AV63TFContratoOcorrenciaNotificacao_Responsavel), 6, 0)));
               AV64TFContratoOcorrenciaNotificacao_Responsavel_To = (int)(NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Valueto, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV64TFContratoOcorrenciaNotificacao_Responsavel_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV64TFContratoOcorrenciaNotificacao_Responsavel_To), 6, 0)));
               if ( ! (0==AV63TFContratoOcorrenciaNotificacao_Responsavel) )
               {
                  Ddo_contratoocorrencianotificacao_responsavel_Filteredtext_set = StringUtil.Str( (decimal)(AV63TFContratoOcorrenciaNotificacao_Responsavel), 6, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoocorrencianotificacao_responsavel_Internalname, "FilteredText_set", Ddo_contratoocorrencianotificacao_responsavel_Filteredtext_set);
               }
               if ( ! (0==AV64TFContratoOcorrenciaNotificacao_Responsavel_To) )
               {
                  Ddo_contratoocorrencianotificacao_responsavel_Filteredtextto_set = StringUtil.Str( (decimal)(AV64TFContratoOcorrenciaNotificacao_Responsavel_To), 6, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoocorrencianotificacao_responsavel_Internalname, "FilteredTextTo_set", Ddo_contratoocorrencianotificacao_responsavel_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFCONTRATOOCORRENCIANOTIFICACAO_NOME") == 0 )
            {
               AV67TFContratoOcorrenciaNotificacao_Nome = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV67TFContratoOcorrenciaNotificacao_Nome", AV67TFContratoOcorrenciaNotificacao_Nome);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67TFContratoOcorrenciaNotificacao_Nome)) )
               {
                  Ddo_contratoocorrencianotificacao_nome_Filteredtext_set = AV67TFContratoOcorrenciaNotificacao_Nome;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoocorrencianotificacao_nome_Internalname, "FilteredText_set", Ddo_contratoocorrencianotificacao_nome_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFCONTRATOOCORRENCIANOTIFICACAO_NOME_SEL") == 0 )
            {
               AV68TFContratoOcorrenciaNotificacao_Nome_Sel = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV68TFContratoOcorrenciaNotificacao_Nome_Sel", AV68TFContratoOcorrenciaNotificacao_Nome_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV68TFContratoOcorrenciaNotificacao_Nome_Sel)) )
               {
                  Ddo_contratoocorrencianotificacao_nome_Selectedvalue_set = AV68TFContratoOcorrenciaNotificacao_Nome_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoocorrencianotificacao_nome_Internalname, "SelectedValue_set", Ddo_contratoocorrencianotificacao_nome_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFCONTRATOOCORRENCIANOTIFICACAO_DOCTO") == 0 )
            {
               AV71TFContratoOcorrenciaNotificacao_Docto = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV71TFContratoOcorrenciaNotificacao_Docto", AV71TFContratoOcorrenciaNotificacao_Docto);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV71TFContratoOcorrenciaNotificacao_Docto)) )
               {
                  Ddo_contratoocorrencianotificacao_docto_Filteredtext_set = AV71TFContratoOcorrenciaNotificacao_Docto;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoocorrencianotificacao_docto_Internalname, "FilteredText_set", Ddo_contratoocorrencianotificacao_docto_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFCONTRATOOCORRENCIANOTIFICACAO_DOCTO_SEL") == 0 )
            {
               AV72TFContratoOcorrenciaNotificacao_Docto_Sel = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV72TFContratoOcorrenciaNotificacao_Docto_Sel", AV72TFContratoOcorrenciaNotificacao_Docto_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV72TFContratoOcorrenciaNotificacao_Docto_Sel)) )
               {
                  Ddo_contratoocorrencianotificacao_docto_Selectedvalue_set = AV72TFContratoOcorrenciaNotificacao_Docto_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoocorrencianotificacao_docto_Internalname, "SelectedValue_set", Ddo_contratoocorrencianotificacao_docto_Selectedvalue_set);
               }
            }
            AV83GXV1 = (int)(AV83GXV1+1);
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S202( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         if ( AV11GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV13GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV11GridState.gxTpr_Dynamicfilters.Item(1));
            AV16DynamicFiltersSelector1 = AV13GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 )
            {
               AV17ContratoOcorrenciaNotificacao_Data1 = context.localUtil.CToD( AV13GridStateDynamicFilter.gxTpr_Value, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17ContratoOcorrenciaNotificacao_Data1", context.localUtil.Format(AV17ContratoOcorrenciaNotificacao_Data1, "99/99/99"));
               AV18ContratoOcorrenciaNotificacao_Data_To1 = context.localUtil.CToD( AV13GridStateDynamicFilter.gxTpr_Valueto, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18ContratoOcorrenciaNotificacao_Data_To1", context.localUtil.Format(AV18ContratoOcorrenciaNotificacao_Data_To1, "99/99/99"));
            }
            else if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTRATOOCORRENCIANOTIFICACAO_PRAZO") == 0 )
            {
               AV19ContratoOcorrenciaNotificacao_Prazo1 = (short)(NumberUtil.Val( AV13GridStateDynamicFilter.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19ContratoOcorrenciaNotificacao_Prazo1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV19ContratoOcorrenciaNotificacao_Prazo1), 4, 0)));
               AV20ContratoOcorrenciaNotificacao_Prazo_To1 = (short)(NumberUtil.Val( AV13GridStateDynamicFilter.gxTpr_Valueto, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20ContratoOcorrenciaNotificacao_Prazo_To1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20ContratoOcorrenciaNotificacao_Prazo_To1), 4, 0)));
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV11GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV21DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21DynamicFiltersEnabled2", AV21DynamicFiltersEnabled2);
               AV13GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV11GridState.gxTpr_Dynamicfilters.Item(2));
               AV22DynamicFiltersSelector2 = AV13GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22DynamicFiltersSelector2", AV22DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV22DynamicFiltersSelector2, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 )
               {
                  AV23ContratoOcorrenciaNotificacao_Data2 = context.localUtil.CToD( AV13GridStateDynamicFilter.gxTpr_Value, 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23ContratoOcorrenciaNotificacao_Data2", context.localUtil.Format(AV23ContratoOcorrenciaNotificacao_Data2, "99/99/99"));
                  AV24ContratoOcorrenciaNotificacao_Data_To2 = context.localUtil.CToD( AV13GridStateDynamicFilter.gxTpr_Valueto, 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24ContratoOcorrenciaNotificacao_Data_To2", context.localUtil.Format(AV24ContratoOcorrenciaNotificacao_Data_To2, "99/99/99"));
               }
               else if ( StringUtil.StrCmp(AV22DynamicFiltersSelector2, "CONTRATOOCORRENCIANOTIFICACAO_PRAZO") == 0 )
               {
                  AV25ContratoOcorrenciaNotificacao_Prazo2 = (short)(NumberUtil.Val( AV13GridStateDynamicFilter.gxTpr_Value, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25ContratoOcorrenciaNotificacao_Prazo2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25ContratoOcorrenciaNotificacao_Prazo2), 4, 0)));
                  AV26ContratoOcorrenciaNotificacao_Prazo_To2 = (short)(NumberUtil.Val( AV13GridStateDynamicFilter.gxTpr_Valueto, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26ContratoOcorrenciaNotificacao_Prazo_To2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26ContratoOcorrenciaNotificacao_Prazo_To2), 4, 0)));
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV33DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S162( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV11GridState.FromXml(AV37Session.Get(AV82Pgmname+"GridState"), "");
         AV11GridState.gxTpr_Orderedby = AV14OrderedBy;
         AV11GridState.gxTpr_Ordereddsc = AV15OrderedDsc;
         AV11GridState.gxTpr_Filtervalues.Clear();
         if ( ! ( (DateTime.MinValue==AV39TFContratoOcorrenciaNotificacao_Data) && (DateTime.MinValue==AV40TFContratoOcorrenciaNotificacao_Data_To) ) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCONTRATOOCORRENCIANOTIFICACAO_DATA";
            AV12GridStateFilterValue.gxTpr_Value = context.localUtil.DToC( AV39TFContratoOcorrenciaNotificacao_Data, 2, "/");
            AV12GridStateFilterValue.gxTpr_Valueto = context.localUtil.DToC( AV40TFContratoOcorrenciaNotificacao_Data_To, 2, "/");
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! ( (0==AV45TFContratoOcorrenciaNotificacao_Prazo) && (0==AV46TFContratoOcorrenciaNotificacao_Prazo_To) ) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCONTRATOOCORRENCIANOTIFICACAO_PRAZO";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV45TFContratoOcorrenciaNotificacao_Prazo), 4, 0);
            AV12GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV46TFContratoOcorrenciaNotificacao_Prazo_To), 4, 0);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49TFContratoOcorrenciaNotificacao_Descricao)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO";
            AV12GridStateFilterValue.gxTpr_Value = AV49TFContratoOcorrenciaNotificacao_Descricao;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV50TFContratoOcorrenciaNotificacao_Descricao_Sel)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_SEL";
            AV12GridStateFilterValue.gxTpr_Value = AV50TFContratoOcorrenciaNotificacao_Descricao_Sel;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! ( (DateTime.MinValue==AV53TFContratoOcorrenciaNotificacao_Cumprido) && (DateTime.MinValue==AV54TFContratoOcorrenciaNotificacao_Cumprido_To) ) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO";
            AV12GridStateFilterValue.gxTpr_Value = context.localUtil.DToC( AV53TFContratoOcorrenciaNotificacao_Cumprido, 2, "/");
            AV12GridStateFilterValue.gxTpr_Valueto = context.localUtil.DToC( AV54TFContratoOcorrenciaNotificacao_Cumprido_To, 2, "/");
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59TFContratoOcorrenciaNotificacao_Protocolo)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO";
            AV12GridStateFilterValue.gxTpr_Value = AV59TFContratoOcorrenciaNotificacao_Protocolo;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60TFContratoOcorrenciaNotificacao_Protocolo_Sel)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_SEL";
            AV12GridStateFilterValue.gxTpr_Value = AV60TFContratoOcorrenciaNotificacao_Protocolo_Sel;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! ( (0==AV63TFContratoOcorrenciaNotificacao_Responsavel) && (0==AV64TFContratoOcorrenciaNotificacao_Responsavel_To) ) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV63TFContratoOcorrenciaNotificacao_Responsavel), 6, 0);
            AV12GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV64TFContratoOcorrenciaNotificacao_Responsavel_To), 6, 0);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67TFContratoOcorrenciaNotificacao_Nome)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCONTRATOOCORRENCIANOTIFICACAO_NOME";
            AV12GridStateFilterValue.gxTpr_Value = AV67TFContratoOcorrenciaNotificacao_Nome;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV68TFContratoOcorrenciaNotificacao_Nome_Sel)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCONTRATOOCORRENCIANOTIFICACAO_NOME_SEL";
            AV12GridStateFilterValue.gxTpr_Value = AV68TFContratoOcorrenciaNotificacao_Nome_Sel;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV71TFContratoOcorrenciaNotificacao_Docto)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCONTRATOOCORRENCIANOTIFICACAO_DOCTO";
            AV12GridStateFilterValue.gxTpr_Value = AV71TFContratoOcorrenciaNotificacao_Docto;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV72TFContratoOcorrenciaNotificacao_Docto_Sel)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCONTRATOOCORRENCIANOTIFICACAO_DOCTO_SEL";
            AV12GridStateFilterValue.gxTpr_Value = AV72TFContratoOcorrenciaNotificacao_Docto_Sel;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! (0==AV7ContratoOcorrencia_Codigo) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "PARM_&CONTRATOOCORRENCIA_CODIGO";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV7ContratoOcorrencia_Codigo), 6, 0);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         new wwpbaseobjects.savegridstate(context ).execute(  AV82Pgmname+"GridState",  AV11GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_MeetrikaVs3")) ;
      }

      protected void S182( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV11GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV34DynamicFiltersIgnoreFirst )
         {
            AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV13GridStateDynamicFilter.gxTpr_Selected = AV16DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 ) && ! ( (DateTime.MinValue==AV17ContratoOcorrenciaNotificacao_Data1) && (DateTime.MinValue==AV18ContratoOcorrenciaNotificacao_Data_To1) ) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = context.localUtil.DToC( AV17ContratoOcorrenciaNotificacao_Data1, 2, "/");
               AV13GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.DToC( AV18ContratoOcorrenciaNotificacao_Data_To1, 2, "/");
            }
            else if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTRATOOCORRENCIANOTIFICACAO_PRAZO") == 0 ) && ! ( (0==AV19ContratoOcorrenciaNotificacao_Prazo1) && (0==AV20ContratoOcorrenciaNotificacao_Prazo_To1) ) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV19ContratoOcorrenciaNotificacao_Prazo1), 4, 0);
               AV13GridStateDynamicFilter.gxTpr_Valueto = StringUtil.Str( (decimal)(AV20ContratoOcorrenciaNotificacao_Prazo_To1), 4, 0);
            }
            if ( AV33DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV13GridStateDynamicFilter.gxTpr_Value)) || ! String.IsNullOrEmpty(StringUtil.RTrim( AV13GridStateDynamicFilter.gxTpr_Valueto)) )
            {
               AV11GridState.gxTpr_Dynamicfilters.Add(AV13GridStateDynamicFilter, 0);
            }
         }
         if ( AV21DynamicFiltersEnabled2 )
         {
            AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV13GridStateDynamicFilter.gxTpr_Selected = AV22DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV22DynamicFiltersSelector2, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 ) && ! ( (DateTime.MinValue==AV23ContratoOcorrenciaNotificacao_Data2) && (DateTime.MinValue==AV24ContratoOcorrenciaNotificacao_Data_To2) ) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = context.localUtil.DToC( AV23ContratoOcorrenciaNotificacao_Data2, 2, "/");
               AV13GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.DToC( AV24ContratoOcorrenciaNotificacao_Data_To2, 2, "/");
            }
            else if ( ( StringUtil.StrCmp(AV22DynamicFiltersSelector2, "CONTRATOOCORRENCIANOTIFICACAO_PRAZO") == 0 ) && ! ( (0==AV25ContratoOcorrenciaNotificacao_Prazo2) && (0==AV26ContratoOcorrenciaNotificacao_Prazo_To2) ) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV25ContratoOcorrenciaNotificacao_Prazo2), 4, 0);
               AV13GridStateDynamicFilter.gxTpr_Valueto = StringUtil.Str( (decimal)(AV26ContratoOcorrenciaNotificacao_Prazo_To2), 4, 0);
            }
            if ( AV33DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV13GridStateDynamicFilter.gxTpr_Value)) || ! String.IsNullOrEmpty(StringUtil.RTrim( AV13GridStateDynamicFilter.gxTpr_Valueto)) )
            {
               AV11GridState.gxTpr_Dynamicfilters.Add(AV13GridStateDynamicFilter, 0);
            }
         }
      }

      protected void S132( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV9TrnContext.gxTpr_Callerobject = AV82Pgmname;
         AV9TrnContext.gxTpr_Callerondelete = true;
         AV9TrnContext.gxTpr_Callerurl = AV8HTTPRequest.ScriptName+"?"+AV8HTTPRequest.QueryString;
         AV9TrnContext.gxTpr_Transactionname = "ContratoOcorrenciaNotificacao";
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10TrnContextAtt.gxTpr_Attributename = "ContratoOcorrencia_Codigo";
         AV10TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7ContratoOcorrencia_Codigo), 6, 0);
         AV9TrnContext.gxTpr_Attributes.Add(AV10TrnContextAtt, 0);
         AV37Session.Set("TrnContext", AV9TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_MeetrikaVs3"));
      }

      protected void wb_table1_2_7A2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='TableSearchCell'>") ;
            wb_table2_8_7A2( true) ;
         }
         else
         {
            wb_table2_8_7A2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_7A2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_81_7A2( true) ;
         }
         else
         {
            wb_table3_81_7A2( false) ;
         }
         return  ;
      }

      protected void wb_table3_81_7A2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_7A2e( true) ;
         }
         else
         {
            wb_table1_2_7A2e( false) ;
         }
      }

      protected void wb_table3_81_7A2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"DivS\" data-gxgridid=\"84\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoOcorrenciaNotificacao_Data_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoOcorrenciaNotificacao_Data_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoOcorrenciaNotificacao_Data_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoOcorrenciaNotificacao_Prazo_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoOcorrenciaNotificacao_Prazo_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoOcorrenciaNotificacao_Prazo_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoOcorrenciaNotificacao_Descricao_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoOcorrenciaNotificacao_Descricao_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoOcorrenciaNotificacao_Descricao_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoOcorrenciaNotificacao_Cumprido_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoOcorrenciaNotificacao_Cumprido_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoOcorrenciaNotificacao_Cumprido_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoOcorrenciaNotificacao_Protocolo_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoOcorrenciaNotificacao_Protocolo_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoOcorrenciaNotificacao_Protocolo_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoOcorrenciaNotificacao_Responsavel_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoOcorrenciaNotificacao_Responsavel_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoOcorrenciaNotificacao_Responsavel_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoOcorrenciaNotificacao_Nome_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoOcorrenciaNotificacao_Nome_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoOcorrenciaNotificacao_Nome_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoOcorrenciaNotificacao_Docto_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoOcorrenciaNotificacao_Docto_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoOcorrenciaNotificacao_Docto_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", sPrefix);
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV35Delete));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDelete_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV36Update));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavUpdate_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.Format(A298ContratoOcorrenciaNotificacao_Data, "99/99/99"));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoOcorrenciaNotificacao_Data_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoOcorrenciaNotificacao_Data_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtContratoOcorrenciaNotificacao_Data_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A299ContratoOcorrenciaNotificacao_Prazo), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoOcorrenciaNotificacao_Prazo_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoOcorrenciaNotificacao_Prazo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A300ContratoOcorrenciaNotificacao_Descricao);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoOcorrenciaNotificacao_Descricao_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoOcorrenciaNotificacao_Descricao_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.Format(A301ContratoOcorrenciaNotificacao_Cumprido, "99/99/99"));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoOcorrenciaNotificacao_Cumprido_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoOcorrenciaNotificacao_Cumprido_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A302ContratoOcorrenciaNotificacao_Protocolo);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoOcorrenciaNotificacao_Protocolo_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoOcorrenciaNotificacao_Protocolo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A303ContratoOcorrenciaNotificacao_Responsavel), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoOcorrenciaNotificacao_Responsavel_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoOcorrenciaNotificacao_Responsavel_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A304ContratoOcorrenciaNotificacao_Nome));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoOcorrenciaNotificacao_Nome_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoOcorrenciaNotificacao_Nome_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtContratoOcorrenciaNotificacao_Nome_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A306ContratoOcorrenciaNotificacao_Docto);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoOcorrenciaNotificacao_Docto_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoOcorrenciaNotificacao_Docto_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 84 )
         {
            wbEnd = 0;
            nRC_GXsfl_84 = (short)(nGXsfl_84_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid(sPrefix+"_"+"Grid", GridContainer);
               if ( ! isAjaxCallMode( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+sPrefix+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_81_7A2e( true) ;
         }
         else
         {
            wb_table3_81_7A2e( false) ;
         }
      }

      protected void wb_table2_8_7A2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablesearch_Internalname, tblTablesearch_Internalname, "", "TableSearch", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='Width100'>") ;
            wb_table4_11_7A2( true) ;
         }
         else
         {
            wb_table4_11_7A2( false) ;
         }
         return  ;
      }

      protected void wb_table4_11_7A2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_ContratoOcorrenciaContratoOcorrenciaNotificacaoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'" + sPrefix + "',false,'" + sGXsfl_84_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,18);\"", "", true, "HLP_ContratoOcorrenciaContratoOcorrenciaNotificacaoWC.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'" + sPrefix + "',false,'" + sGXsfl_84_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV15OrderedDsc), StringUtil.BoolToStr( AV15OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,19);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_ContratoOcorrenciaContratoOcorrenciaNotificacaoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table5_21_7A2( true) ;
         }
         else
         {
            wb_table5_21_7A2( false) ;
         }
         return  ;
      }

      protected void wb_table5_21_7A2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_7A2e( true) ;
         }
         else
         {
            wb_table2_8_7A2e( false) ;
         }
      }

      protected void wb_table5_21_7A2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataFilterContentCell'>") ;
            wb_table6_24_7A2( true) ;
         }
         else
         {
            wb_table6_24_7A2( false) ;
         }
         return  ;
      }

      protected void wb_table6_24_7A2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_ContratoOcorrenciaContratoOcorrenciaNotificacaoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_21_7A2e( true) ;
         }
         else
         {
            wb_table5_21_7A2e( false) ;
         }
      }

      protected void wb_table6_24_7A2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ContratoOcorrenciaContratoOcorrenciaNotificacaoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 29,'" + sPrefix + "',false,'" + sGXsfl_84_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV16DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,29);\"", "", true, "HLP_ContratoOcorrenciaContratoOcorrenciaNotificacaoWC.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ContratoOcorrenciaContratoOcorrenciaNotificacaoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_33_7A2( true) ;
         }
         else
         {
            wb_table7_33_7A2( false) ;
         }
         return  ;
      }

      protected void wb_table7_33_7A2e( bool wbgen )
      {
         if ( wbgen )
         {
            wb_table8_41_7A2( true) ;
         }
         else
         {
            wb_table8_41_7A2( false) ;
         }
         return  ;
      }

      protected void wb_table8_41_7A2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 50,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContratoOcorrenciaContratoOcorrenciaNotificacaoWC.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 51,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContratoOcorrenciaContratoOcorrenciaNotificacaoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ContratoOcorrenciaContratoOcorrenciaNotificacaoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 56,'" + sPrefix + "',false,'" + sGXsfl_84_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV22DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,56);\"", "", true, "HLP_ContratoOcorrenciaContratoOcorrenciaNotificacaoWC.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV22DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ContratoOcorrenciaContratoOcorrenciaNotificacaoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table9_60_7A2( true) ;
         }
         else
         {
            wb_table9_60_7A2( false) ;
         }
         return  ;
      }

      protected void wb_table9_60_7A2e( bool wbgen )
      {
         if ( wbgen )
         {
            wb_table10_68_7A2( true) ;
         }
         else
         {
            wb_table10_68_7A2( false) ;
         }
         return  ;
      }

      protected void wb_table10_68_7A2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 77,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContratoOcorrenciaContratoOcorrenciaNotificacaoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_24_7A2e( true) ;
         }
         else
         {
            wb_table6_24_7A2e( false) ;
         }
      }

      protected void wb_table10_68_7A2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterscontratoocorrencianotificacao_prazo2_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterscontratoocorrencianotificacao_prazo2_Internalname, tblTablemergeddynamicfilterscontratoocorrencianotificacao_prazo2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 71,'" + sPrefix + "',false,'" + sGXsfl_84_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratoocorrencianotificacao_prazo2_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV25ContratoOcorrenciaNotificacao_Prazo2), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV25ContratoOcorrenciaNotificacao_Prazo2), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,71);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratoocorrencianotificacao_prazo2_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoOcorrenciaContratoOcorrenciaNotificacaoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfilterscontratoocorrencianotificacao_prazo_rangemiddletext2_Internalname, "at�", "", "", lblDynamicfilterscontratoocorrencianotificacao_prazo_rangemiddletext2_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ContratoOcorrenciaContratoOcorrenciaNotificacaoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 75,'" + sPrefix + "',false,'" + sGXsfl_84_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratoocorrencianotificacao_prazo_to2_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV26ContratoOcorrenciaNotificacao_Prazo_To2), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV26ContratoOcorrenciaNotificacao_Prazo_To2), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,75);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratoocorrencianotificacao_prazo_to2_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoOcorrenciaContratoOcorrenciaNotificacaoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table10_68_7A2e( true) ;
         }
         else
         {
            wb_table10_68_7A2e( false) ;
         }
      }

      protected void wb_table9_60_7A2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterscontratoocorrencianotificacao_data2_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterscontratoocorrencianotificacao_data2_Internalname, tblTablemergeddynamicfilterscontratoocorrencianotificacao_data2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 63,'" + sPrefix + "',false,'" + sGXsfl_84_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContratoocorrencianotificacao_data2_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContratoocorrencianotificacao_data2_Internalname, context.localUtil.Format(AV23ContratoOcorrenciaNotificacao_Data2, "99/99/99"), context.localUtil.Format( AV23ContratoOcorrenciaNotificacao_Data2, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,63);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratoocorrencianotificacao_data2_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoOcorrenciaContratoOcorrenciaNotificacaoWC.htm");
            GxWebStd.gx_bitmap( context, edtavContratoocorrencianotificacao_data2_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ContratoOcorrenciaContratoOcorrenciaNotificacaoWC.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfilterscontratoocorrencianotificacao_data_rangemiddletext2_Internalname, "at�", "", "", lblDynamicfilterscontratoocorrencianotificacao_data_rangemiddletext2_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ContratoOcorrenciaContratoOcorrenciaNotificacaoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 67,'" + sPrefix + "',false,'" + sGXsfl_84_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContratoocorrencianotificacao_data_to2_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContratoocorrencianotificacao_data_to2_Internalname, context.localUtil.Format(AV24ContratoOcorrenciaNotificacao_Data_To2, "99/99/99"), context.localUtil.Format( AV24ContratoOcorrenciaNotificacao_Data_To2, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,67);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratoocorrencianotificacao_data_to2_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoOcorrenciaContratoOcorrenciaNotificacaoWC.htm");
            GxWebStd.gx_bitmap( context, edtavContratoocorrencianotificacao_data_to2_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ContratoOcorrenciaContratoOcorrenciaNotificacaoWC.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_60_7A2e( true) ;
         }
         else
         {
            wb_table9_60_7A2e( false) ;
         }
      }

      protected void wb_table8_41_7A2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterscontratoocorrencianotificacao_prazo1_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterscontratoocorrencianotificacao_prazo1_Internalname, tblTablemergeddynamicfilterscontratoocorrencianotificacao_prazo1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'" + sPrefix + "',false,'" + sGXsfl_84_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratoocorrencianotificacao_prazo1_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV19ContratoOcorrenciaNotificacao_Prazo1), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV19ContratoOcorrenciaNotificacao_Prazo1), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,44);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratoocorrencianotificacao_prazo1_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoOcorrenciaContratoOcorrenciaNotificacaoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfilterscontratoocorrencianotificacao_prazo_rangemiddletext1_Internalname, "at�", "", "", lblDynamicfilterscontratoocorrencianotificacao_prazo_rangemiddletext1_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ContratoOcorrenciaContratoOcorrenciaNotificacaoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 48,'" + sPrefix + "',false,'" + sGXsfl_84_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratoocorrencianotificacao_prazo_to1_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV20ContratoOcorrenciaNotificacao_Prazo_To1), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV20ContratoOcorrenciaNotificacao_Prazo_To1), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,48);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratoocorrencianotificacao_prazo_to1_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoOcorrenciaContratoOcorrenciaNotificacaoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_41_7A2e( true) ;
         }
         else
         {
            wb_table8_41_7A2e( false) ;
         }
      }

      protected void wb_table7_33_7A2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterscontratoocorrencianotificacao_data1_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterscontratoocorrencianotificacao_data1_Internalname, tblTablemergeddynamicfilterscontratoocorrencianotificacao_data1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'" + sPrefix + "',false,'" + sGXsfl_84_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContratoocorrencianotificacao_data1_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContratoocorrencianotificacao_data1_Internalname, context.localUtil.Format(AV17ContratoOcorrenciaNotificacao_Data1, "99/99/99"), context.localUtil.Format( AV17ContratoOcorrenciaNotificacao_Data1, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,36);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratoocorrencianotificacao_data1_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoOcorrenciaContratoOcorrenciaNotificacaoWC.htm");
            GxWebStd.gx_bitmap( context, edtavContratoocorrencianotificacao_data1_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ContratoOcorrenciaContratoOcorrenciaNotificacaoWC.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfilterscontratoocorrencianotificacao_data_rangemiddletext1_Internalname, "at�", "", "", lblDynamicfilterscontratoocorrencianotificacao_data_rangemiddletext1_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_ContratoOcorrenciaContratoOcorrenciaNotificacaoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'" + sPrefix + "',false,'" + sGXsfl_84_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContratoocorrencianotificacao_data_to1_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContratoocorrencianotificacao_data_to1_Internalname, context.localUtil.Format(AV18ContratoOcorrenciaNotificacao_Data_To1, "99/99/99"), context.localUtil.Format( AV18ContratoOcorrenciaNotificacao_Data_To1, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,40);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratoocorrencianotificacao_data_to1_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoOcorrenciaContratoOcorrenciaNotificacaoWC.htm");
            GxWebStd.gx_bitmap( context, edtavContratoocorrencianotificacao_data_to1_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ContratoOcorrenciaContratoOcorrenciaNotificacaoWC.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_33_7A2e( true) ;
         }
         else
         {
            wb_table7_33_7A2e( false) ;
         }
      }

      protected void wb_table4_11_7A2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 14,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgInsert_Internalname, context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Inserir", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgInsert_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOINSERT\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContratoOcorrenciaContratoOcorrenciaNotificacaoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_11_7A2e( true) ;
         }
         else
         {
            wb_table4_11_7A2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7ContratoOcorrencia_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7ContratoOcorrencia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContratoOcorrencia_Codigo), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA7A2( ) ;
         WS7A2( ) ;
         WE7A2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlAV7ContratoOcorrencia_Codigo = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PA7A2( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "contratoocorrenciacontratoocorrencianotificacaowc");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PA7A2( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            AV7ContratoOcorrencia_Codigo = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7ContratoOcorrencia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContratoOcorrencia_Codigo), 6, 0)));
         }
         wcpOAV7ContratoOcorrencia_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV7ContratoOcorrencia_Codigo"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( AV7ContratoOcorrencia_Codigo != wcpOAV7ContratoOcorrencia_Codigo ) ) )
         {
            setjustcreated();
         }
         wcpOAV7ContratoOcorrencia_Codigo = AV7ContratoOcorrencia_Codigo;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlAV7ContratoOcorrencia_Codigo = cgiGet( sPrefix+"AV7ContratoOcorrencia_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlAV7ContratoOcorrencia_Codigo) > 0 )
         {
            AV7ContratoOcorrencia_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlAV7ContratoOcorrencia_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7ContratoOcorrencia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContratoOcorrencia_Codigo), 6, 0)));
         }
         else
         {
            AV7ContratoOcorrencia_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"AV7ContratoOcorrencia_Codigo_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PA7A2( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WS7A2( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WS7A2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"AV7ContratoOcorrencia_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7ContratoOcorrencia_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlAV7ContratoOcorrencia_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"AV7ContratoOcorrencia_Codigo_CTRL", StringUtil.RTrim( sCtrlAV7ContratoOcorrencia_Codigo));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WE7A2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202051812494677");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("contratoocorrenciacontratoocorrencianotificacaowc.js", "?202051812494678");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_842( )
      {
         edtavDelete_Internalname = sPrefix+"vDELETE_"+sGXsfl_84_idx;
         edtavUpdate_Internalname = sPrefix+"vUPDATE_"+sGXsfl_84_idx;
         edtContratoOcorrenciaNotificacao_Data_Internalname = sPrefix+"CONTRATOOCORRENCIANOTIFICACAO_DATA_"+sGXsfl_84_idx;
         edtContratoOcorrenciaNotificacao_Prazo_Internalname = sPrefix+"CONTRATOOCORRENCIANOTIFICACAO_PRAZO_"+sGXsfl_84_idx;
         edtContratoOcorrenciaNotificacao_Descricao_Internalname = sPrefix+"CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_"+sGXsfl_84_idx;
         edtContratoOcorrenciaNotificacao_Cumprido_Internalname = sPrefix+"CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_"+sGXsfl_84_idx;
         edtContratoOcorrenciaNotificacao_Protocolo_Internalname = sPrefix+"CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_"+sGXsfl_84_idx;
         edtContratoOcorrenciaNotificacao_Responsavel_Internalname = sPrefix+"CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_"+sGXsfl_84_idx;
         edtContratoOcorrenciaNotificacao_Nome_Internalname = sPrefix+"CONTRATOOCORRENCIANOTIFICACAO_NOME_"+sGXsfl_84_idx;
         edtContratoOcorrenciaNotificacao_Docto_Internalname = sPrefix+"CONTRATOOCORRENCIANOTIFICACAO_DOCTO_"+sGXsfl_84_idx;
      }

      protected void SubsflControlProps_fel_842( )
      {
         edtavDelete_Internalname = sPrefix+"vDELETE_"+sGXsfl_84_fel_idx;
         edtavUpdate_Internalname = sPrefix+"vUPDATE_"+sGXsfl_84_fel_idx;
         edtContratoOcorrenciaNotificacao_Data_Internalname = sPrefix+"CONTRATOOCORRENCIANOTIFICACAO_DATA_"+sGXsfl_84_fel_idx;
         edtContratoOcorrenciaNotificacao_Prazo_Internalname = sPrefix+"CONTRATOOCORRENCIANOTIFICACAO_PRAZO_"+sGXsfl_84_fel_idx;
         edtContratoOcorrenciaNotificacao_Descricao_Internalname = sPrefix+"CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_"+sGXsfl_84_fel_idx;
         edtContratoOcorrenciaNotificacao_Cumprido_Internalname = sPrefix+"CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_"+sGXsfl_84_fel_idx;
         edtContratoOcorrenciaNotificacao_Protocolo_Internalname = sPrefix+"CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_"+sGXsfl_84_fel_idx;
         edtContratoOcorrenciaNotificacao_Responsavel_Internalname = sPrefix+"CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_"+sGXsfl_84_fel_idx;
         edtContratoOcorrenciaNotificacao_Nome_Internalname = sPrefix+"CONTRATOOCORRENCIANOTIFICACAO_NOME_"+sGXsfl_84_fel_idx;
         edtContratoOcorrenciaNotificacao_Docto_Internalname = sPrefix+"CONTRATOOCORRENCIANOTIFICACAO_DOCTO_"+sGXsfl_84_fel_idx;
      }

      protected void sendrow_842( )
      {
         SubsflControlProps_842( ) ;
         WB7A0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_84_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_84_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_84_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV35Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV35Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV80Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV35Delete)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV35Delete)) ? AV80Delete_GXI : context.PathToRelativeUrl( AV35Delete)),(String)edtavDelete_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV35Delete_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV36Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV36Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV81Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV36Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV36Update)) ? AV81Update_GXI : context.PathToRelativeUrl( AV36Update)),(String)edtavUpdate_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV36Update_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoOcorrenciaNotificacao_Data_Internalname,context.localUtil.Format(A298ContratoOcorrenciaNotificacao_Data, "99/99/99"),context.localUtil.Format( A298ContratoOcorrenciaNotificacao_Data, "99/99/99"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)edtContratoOcorrenciaNotificacao_Data_Link,(String)"",(String)"",(String)"",(String)edtContratoOcorrenciaNotificacao_Data_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)8,(short)0,(short)0,(short)84,(short)1,(short)-1,(short)0,(bool)true,(String)"Data",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoOcorrenciaNotificacao_Prazo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A299ContratoOcorrenciaNotificacao_Prazo), 4, 0, ",", "")),context.localUtil.Format( (decimal)(A299ContratoOcorrenciaNotificacao_Prazo), "ZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoOcorrenciaNotificacao_Prazo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)4,(short)0,(short)0,(short)84,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoOcorrenciaNotificacao_Descricao_Internalname,(String)A300ContratoOcorrenciaNotificacao_Descricao,StringUtil.RTrim( context.localUtil.Format( A300ContratoOcorrenciaNotificacao_Descricao, "@!")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoOcorrenciaNotificacao_Descricao_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)84,(short)1,(short)-1,(short)-1,(bool)true,(String)"Descricao",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoOcorrenciaNotificacao_Cumprido_Internalname,context.localUtil.Format(A301ContratoOcorrenciaNotificacao_Cumprido, "99/99/99"),context.localUtil.Format( A301ContratoOcorrenciaNotificacao_Cumprido, "99/99/99"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoOcorrenciaNotificacao_Cumprido_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)8,(short)0,(short)0,(short)84,(short)1,(short)-1,(short)0,(bool)true,(String)"Data",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoOcorrenciaNotificacao_Protocolo_Internalname,(String)A302ContratoOcorrenciaNotificacao_Protocolo,(String)"",(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoOcorrenciaNotificacao_Protocolo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)20,(short)0,(short)0,(short)84,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoOcorrenciaNotificacao_Responsavel_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A303ContratoOcorrenciaNotificacao_Responsavel), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A303ContratoOcorrenciaNotificacao_Responsavel), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoOcorrenciaNotificacao_Responsavel_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)84,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoOcorrenciaNotificacao_Nome_Internalname,StringUtil.RTrim( A304ContratoOcorrenciaNotificacao_Nome),StringUtil.RTrim( context.localUtil.Format( A304ContratoOcorrenciaNotificacao_Nome, "@!")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)edtContratoOcorrenciaNotificacao_Nome_Link,(String)"",(String)"",(String)"",(String)edtContratoOcorrenciaNotificacao_Nome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)100,(short)0,(short)0,(short)84,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome100",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoOcorrenciaNotificacao_Docto_Internalname,(String)A306ContratoOcorrenciaNotificacao_Docto,(String)"",(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoOcorrenciaNotificacao_Docto_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)15,(short)0,(short)0,(short)84,(short)1,(short)-1,(short)-1,(bool)true,(String)"Docto",(String)"left",(bool)true});
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOOCORRENCIANOTIFICACAO_DATA"+"_"+sGXsfl_84_idx, GetSecureSignedToken( sPrefix+sGXsfl_84_idx, A298ContratoOcorrenciaNotificacao_Data));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOOCORRENCIANOTIFICACAO_PRAZO"+"_"+sGXsfl_84_idx, GetSecureSignedToken( sPrefix+sGXsfl_84_idx, context.localUtil.Format( (decimal)(A299ContratoOcorrenciaNotificacao_Prazo), "ZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO"+"_"+sGXsfl_84_idx, GetSecureSignedToken( sPrefix+sGXsfl_84_idx, StringUtil.RTrim( context.localUtil.Format( A300ContratoOcorrenciaNotificacao_Descricao, "@!"))));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO"+"_"+sGXsfl_84_idx, GetSecureSignedToken( sPrefix+sGXsfl_84_idx, A301ContratoOcorrenciaNotificacao_Cumprido));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO"+"_"+sGXsfl_84_idx, GetSecureSignedToken( sPrefix+sGXsfl_84_idx, StringUtil.RTrim( context.localUtil.Format( A302ContratoOcorrenciaNotificacao_Protocolo, ""))));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL"+"_"+sGXsfl_84_idx, GetSecureSignedToken( sPrefix+sGXsfl_84_idx, context.localUtil.Format( (decimal)(A303ContratoOcorrenciaNotificacao_Responsavel), "ZZZZZ9")));
            GridContainer.AddRow(GridRow);
            nGXsfl_84_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_84_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_84_idx+1));
            sGXsfl_84_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_84_idx), 4, 0)), 4, "0");
            SubsflControlProps_842( ) ;
         }
         /* End function sendrow_842 */
      }

      protected void init_default_properties( )
      {
         imgInsert_Internalname = sPrefix+"INSERT";
         tblTableactions_Internalname = sPrefix+"TABLEACTIONS";
         lblOrderedtext_Internalname = sPrefix+"ORDEREDTEXT";
         cmbavOrderedby_Internalname = sPrefix+"vORDEREDBY";
         edtavOrdereddsc_Internalname = sPrefix+"vORDEREDDSC";
         lblDynamicfiltersprefix1_Internalname = sPrefix+"DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = sPrefix+"vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = sPrefix+"DYNAMICFILTERSMIDDLE1";
         edtavContratoocorrencianotificacao_data1_Internalname = sPrefix+"vCONTRATOOCORRENCIANOTIFICACAO_DATA1";
         lblDynamicfilterscontratoocorrencianotificacao_data_rangemiddletext1_Internalname = sPrefix+"DYNAMICFILTERSCONTRATOOCORRENCIANOTIFICACAO_DATA_RANGEMIDDLETEXT1";
         edtavContratoocorrencianotificacao_data_to1_Internalname = sPrefix+"vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO1";
         tblTablemergeddynamicfilterscontratoocorrencianotificacao_data1_Internalname = sPrefix+"TABLEMERGEDDYNAMICFILTERSCONTRATOOCORRENCIANOTIFICACAO_DATA1";
         edtavContratoocorrencianotificacao_prazo1_Internalname = sPrefix+"vCONTRATOOCORRENCIANOTIFICACAO_PRAZO1";
         lblDynamicfilterscontratoocorrencianotificacao_prazo_rangemiddletext1_Internalname = sPrefix+"DYNAMICFILTERSCONTRATOOCORRENCIANOTIFICACAO_PRAZO_RANGEMIDDLETEXT1";
         edtavContratoocorrencianotificacao_prazo_to1_Internalname = sPrefix+"vCONTRATOOCORRENCIANOTIFICACAO_PRAZO_TO1";
         tblTablemergeddynamicfilterscontratoocorrencianotificacao_prazo1_Internalname = sPrefix+"TABLEMERGEDDYNAMICFILTERSCONTRATOOCORRENCIANOTIFICACAO_PRAZO1";
         imgAdddynamicfilters1_Internalname = sPrefix+"ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = sPrefix+"REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = sPrefix+"DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = sPrefix+"vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = sPrefix+"DYNAMICFILTERSMIDDLE2";
         edtavContratoocorrencianotificacao_data2_Internalname = sPrefix+"vCONTRATOOCORRENCIANOTIFICACAO_DATA2";
         lblDynamicfilterscontratoocorrencianotificacao_data_rangemiddletext2_Internalname = sPrefix+"DYNAMICFILTERSCONTRATOOCORRENCIANOTIFICACAO_DATA_RANGEMIDDLETEXT2";
         edtavContratoocorrencianotificacao_data_to2_Internalname = sPrefix+"vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO2";
         tblTablemergeddynamicfilterscontratoocorrencianotificacao_data2_Internalname = sPrefix+"TABLEMERGEDDYNAMICFILTERSCONTRATOOCORRENCIANOTIFICACAO_DATA2";
         edtavContratoocorrencianotificacao_prazo2_Internalname = sPrefix+"vCONTRATOOCORRENCIANOTIFICACAO_PRAZO2";
         lblDynamicfilterscontratoocorrencianotificacao_prazo_rangemiddletext2_Internalname = sPrefix+"DYNAMICFILTERSCONTRATOOCORRENCIANOTIFICACAO_PRAZO_RANGEMIDDLETEXT2";
         edtavContratoocorrencianotificacao_prazo_to2_Internalname = sPrefix+"vCONTRATOOCORRENCIANOTIFICACAO_PRAZO_TO2";
         tblTablemergeddynamicfilterscontratoocorrencianotificacao_prazo2_Internalname = sPrefix+"TABLEMERGEDDYNAMICFILTERSCONTRATOOCORRENCIANOTIFICACAO_PRAZO2";
         imgRemovedynamicfilters2_Internalname = sPrefix+"REMOVEDYNAMICFILTERS2";
         tblTabledynamicfilters_Internalname = sPrefix+"TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = sPrefix+"JSDYNAMICFILTERS";
         tblTablefilters_Internalname = sPrefix+"TABLEFILTERS";
         tblTablesearch_Internalname = sPrefix+"TABLESEARCH";
         edtavDelete_Internalname = sPrefix+"vDELETE";
         edtavUpdate_Internalname = sPrefix+"vUPDATE";
         edtContratoOcorrenciaNotificacao_Data_Internalname = sPrefix+"CONTRATOOCORRENCIANOTIFICACAO_DATA";
         edtContratoOcorrenciaNotificacao_Prazo_Internalname = sPrefix+"CONTRATOOCORRENCIANOTIFICACAO_PRAZO";
         edtContratoOcorrenciaNotificacao_Descricao_Internalname = sPrefix+"CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO";
         edtContratoOcorrenciaNotificacao_Cumprido_Internalname = sPrefix+"CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO";
         edtContratoOcorrenciaNotificacao_Protocolo_Internalname = sPrefix+"CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO";
         edtContratoOcorrenciaNotificacao_Responsavel_Internalname = sPrefix+"CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL";
         edtContratoOcorrenciaNotificacao_Nome_Internalname = sPrefix+"CONTRATOOCORRENCIANOTIFICACAO_NOME";
         edtContratoOcorrenciaNotificacao_Docto_Internalname = sPrefix+"CONTRATOOCORRENCIANOTIFICACAO_DOCTO";
         Gridpaginationbar_Internalname = sPrefix+"GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = sPrefix+"GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = sPrefix+"TABLEGRIDHEADER";
         edtContratoOcorrencia_Codigo_Internalname = sPrefix+"CONTRATOOCORRENCIA_CODIGO";
         Workwithplusutilities1_Internalname = sPrefix+"WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = sPrefix+"vDYNAMICFILTERSENABLED2";
         edtavTfcontratoocorrencianotificacao_data_Internalname = sPrefix+"vTFCONTRATOOCORRENCIANOTIFICACAO_DATA";
         edtavTfcontratoocorrencianotificacao_data_to_Internalname = sPrefix+"vTFCONTRATOOCORRENCIANOTIFICACAO_DATA_TO";
         edtavDdo_contratoocorrencianotificacao_dataauxdate_Internalname = sPrefix+"vDDO_CONTRATOOCORRENCIANOTIFICACAO_DATAAUXDATE";
         edtavDdo_contratoocorrencianotificacao_dataauxdateto_Internalname = sPrefix+"vDDO_CONTRATOOCORRENCIANOTIFICACAO_DATAAUXDATETO";
         divDdo_contratoocorrencianotificacao_dataauxdates_Internalname = sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DATAAUXDATES";
         edtavTfcontratoocorrencianotificacao_prazo_Internalname = sPrefix+"vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO";
         edtavTfcontratoocorrencianotificacao_prazo_to_Internalname = sPrefix+"vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO_TO";
         edtavTfcontratoocorrencianotificacao_descricao_Internalname = sPrefix+"vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO";
         edtavTfcontratoocorrencianotificacao_descricao_sel_Internalname = sPrefix+"vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_SEL";
         edtavTfcontratoocorrencianotificacao_cumprido_Internalname = sPrefix+"vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO";
         edtavTfcontratoocorrencianotificacao_cumprido_to_Internalname = sPrefix+"vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_TO";
         edtavDdo_contratoocorrencianotificacao_cumpridoauxdate_Internalname = sPrefix+"vDDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDOAUXDATE";
         edtavDdo_contratoocorrencianotificacao_cumpridoauxdateto_Internalname = sPrefix+"vDDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDOAUXDATETO";
         divDdo_contratoocorrencianotificacao_cumpridoauxdates_Internalname = sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDOAUXDATES";
         edtavTfcontratoocorrencianotificacao_protocolo_Internalname = sPrefix+"vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO";
         edtavTfcontratoocorrencianotificacao_protocolo_sel_Internalname = sPrefix+"vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_SEL";
         edtavTfcontratoocorrencianotificacao_responsavel_Internalname = sPrefix+"vTFCONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL";
         edtavTfcontratoocorrencianotificacao_responsavel_to_Internalname = sPrefix+"vTFCONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_TO";
         edtavTfcontratoocorrencianotificacao_nome_Internalname = sPrefix+"vTFCONTRATOOCORRENCIANOTIFICACAO_NOME";
         edtavTfcontratoocorrencianotificacao_nome_sel_Internalname = sPrefix+"vTFCONTRATOOCORRENCIANOTIFICACAO_NOME_SEL";
         edtavTfcontratoocorrencianotificacao_docto_Internalname = sPrefix+"vTFCONTRATOOCORRENCIANOTIFICACAO_DOCTO";
         edtavTfcontratoocorrencianotificacao_docto_sel_Internalname = sPrefix+"vTFCONTRATOOCORRENCIANOTIFICACAO_DOCTO_SEL";
         Ddo_contratoocorrencianotificacao_data_Internalname = sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA";
         edtavDdo_contratoocorrencianotificacao_datatitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_CONTRATOOCORRENCIANOTIFICACAO_DATATITLECONTROLIDTOREPLACE";
         Ddo_contratoocorrencianotificacao_prazo_Internalname = sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO";
         edtavDdo_contratoocorrencianotificacao_prazotitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZOTITLECONTROLIDTOREPLACE";
         Ddo_contratoocorrencianotificacao_descricao_Internalname = sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO";
         edtavDdo_contratoocorrencianotificacao_descricaotitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAOTITLECONTROLIDTOREPLACE";
         Ddo_contratoocorrencianotificacao_cumprido_Internalname = sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO";
         edtavDdo_contratoocorrencianotificacao_cumpridotitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDOTITLECONTROLIDTOREPLACE";
         Ddo_contratoocorrencianotificacao_protocolo_Internalname = sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO";
         edtavDdo_contratoocorrencianotificacao_protocolotitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLOTITLECONTROLIDTOREPLACE";
         Ddo_contratoocorrencianotificacao_responsavel_Internalname = sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL";
         edtavDdo_contratoocorrencianotificacao_responsaveltitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVELTITLECONTROLIDTOREPLACE";
         Ddo_contratoocorrencianotificacao_nome_Internalname = sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME";
         edtavDdo_contratoocorrencianotificacao_nometitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_CONTRATOOCORRENCIANOTIFICACAO_NOMETITLECONTROLIDTOREPLACE";
         Ddo_contratoocorrencianotificacao_docto_Internalname = sPrefix+"DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO";
         edtavDdo_contratoocorrencianotificacao_doctotitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTOTITLECONTROLIDTOREPLACE";
         Form.Internalname = sPrefix+"FORM";
         subGrid_Internalname = sPrefix+"GRID";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtContratoOcorrenciaNotificacao_Docto_Jsonclick = "";
         edtContratoOcorrenciaNotificacao_Nome_Jsonclick = "";
         edtContratoOcorrenciaNotificacao_Responsavel_Jsonclick = "";
         edtContratoOcorrenciaNotificacao_Protocolo_Jsonclick = "";
         edtContratoOcorrenciaNotificacao_Cumprido_Jsonclick = "";
         edtContratoOcorrenciaNotificacao_Descricao_Jsonclick = "";
         edtContratoOcorrenciaNotificacao_Prazo_Jsonclick = "";
         edtContratoOcorrenciaNotificacao_Data_Jsonclick = "";
         edtavContratoocorrencianotificacao_data_to1_Jsonclick = "";
         edtavContratoocorrencianotificacao_data1_Jsonclick = "";
         edtavContratoocorrencianotificacao_prazo_to1_Jsonclick = "";
         edtavContratoocorrencianotificacao_prazo1_Jsonclick = "";
         edtavContratoocorrencianotificacao_data_to2_Jsonclick = "";
         edtavContratoocorrencianotificacao_data2_Jsonclick = "";
         edtavContratoocorrencianotificacao_prazo_to2_Jsonclick = "";
         edtavContratoocorrencianotificacao_prazo2_Jsonclick = "";
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtContratoOcorrenciaNotificacao_Nome_Link = "";
         edtContratoOcorrenciaNotificacao_Data_Link = "";
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = "";
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = "";
         edtContratoOcorrenciaNotificacao_Docto_Titleformat = 0;
         edtContratoOcorrenciaNotificacao_Nome_Titleformat = 0;
         edtContratoOcorrenciaNotificacao_Responsavel_Titleformat = 0;
         edtContratoOcorrenciaNotificacao_Protocolo_Titleformat = 0;
         edtContratoOcorrenciaNotificacao_Cumprido_Titleformat = 0;
         edtContratoOcorrenciaNotificacao_Descricao_Titleformat = 0;
         edtContratoOcorrenciaNotificacao_Prazo_Titleformat = 0;
         edtContratoOcorrenciaNotificacao_Data_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         tblTablemergeddynamicfilterscontratoocorrencianotificacao_prazo2_Visible = 1;
         tblTablemergeddynamicfilterscontratoocorrencianotificacao_data2_Visible = 1;
         tblTablemergeddynamicfilterscontratoocorrencianotificacao_prazo1_Visible = 1;
         tblTablemergeddynamicfilterscontratoocorrencianotificacao_data1_Visible = 1;
         edtContratoOcorrenciaNotificacao_Docto_Title = "do respons�vel";
         edtContratoOcorrenciaNotificacao_Nome_Title = "Respons�vel";
         edtContratoOcorrenciaNotificacao_Responsavel_Title = "pessoa respons�vel";
         edtContratoOcorrenciaNotificacao_Protocolo_Title = "Protocolo";
         edtContratoOcorrenciaNotificacao_Cumprido_Title = "D. Cumprimento";
         edtContratoOcorrenciaNotificacao_Descricao_Title = "Descri��o";
         edtContratoOcorrenciaNotificacao_Prazo_Title = "Prazo";
         edtContratoOcorrenciaNotificacao_Data_Title = "Data";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled2.Caption = "";
         edtavDdo_contratoocorrencianotificacao_doctotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratoocorrencianotificacao_nometitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratoocorrencianotificacao_responsaveltitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratoocorrencianotificacao_protocolotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratoocorrencianotificacao_cumpridotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratoocorrencianotificacao_descricaotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratoocorrencianotificacao_prazotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratoocorrencianotificacao_datatitlecontrolidtoreplace_Visible = 1;
         edtavTfcontratoocorrencianotificacao_docto_sel_Jsonclick = "";
         edtavTfcontratoocorrencianotificacao_docto_sel_Visible = 1;
         edtavTfcontratoocorrencianotificacao_docto_Jsonclick = "";
         edtavTfcontratoocorrencianotificacao_docto_Visible = 1;
         edtavTfcontratoocorrencianotificacao_nome_sel_Jsonclick = "";
         edtavTfcontratoocorrencianotificacao_nome_sel_Visible = 1;
         edtavTfcontratoocorrencianotificacao_nome_Jsonclick = "";
         edtavTfcontratoocorrencianotificacao_nome_Visible = 1;
         edtavTfcontratoocorrencianotificacao_responsavel_to_Jsonclick = "";
         edtavTfcontratoocorrencianotificacao_responsavel_to_Visible = 1;
         edtavTfcontratoocorrencianotificacao_responsavel_Jsonclick = "";
         edtavTfcontratoocorrencianotificacao_responsavel_Visible = 1;
         edtavTfcontratoocorrencianotificacao_protocolo_sel_Jsonclick = "";
         edtavTfcontratoocorrencianotificacao_protocolo_sel_Visible = 1;
         edtavTfcontratoocorrencianotificacao_protocolo_Jsonclick = "";
         edtavTfcontratoocorrencianotificacao_protocolo_Visible = 1;
         edtavDdo_contratoocorrencianotificacao_cumpridoauxdateto_Jsonclick = "";
         edtavDdo_contratoocorrencianotificacao_cumpridoauxdate_Jsonclick = "";
         edtavTfcontratoocorrencianotificacao_cumprido_to_Jsonclick = "";
         edtavTfcontratoocorrencianotificacao_cumprido_to_Visible = 1;
         edtavTfcontratoocorrencianotificacao_cumprido_Jsonclick = "";
         edtavTfcontratoocorrencianotificacao_cumprido_Visible = 1;
         edtavTfcontratoocorrencianotificacao_descricao_sel_Jsonclick = "";
         edtavTfcontratoocorrencianotificacao_descricao_sel_Visible = 1;
         edtavTfcontratoocorrencianotificacao_descricao_Jsonclick = "";
         edtavTfcontratoocorrencianotificacao_descricao_Visible = 1;
         edtavTfcontratoocorrencianotificacao_prazo_to_Jsonclick = "";
         edtavTfcontratoocorrencianotificacao_prazo_to_Visible = 1;
         edtavTfcontratoocorrencianotificacao_prazo_Jsonclick = "";
         edtavTfcontratoocorrencianotificacao_prazo_Visible = 1;
         edtavDdo_contratoocorrencianotificacao_dataauxdateto_Jsonclick = "";
         edtavDdo_contratoocorrencianotificacao_dataauxdate_Jsonclick = "";
         edtavTfcontratoocorrencianotificacao_data_to_Jsonclick = "";
         edtavTfcontratoocorrencianotificacao_data_to_Visible = 1;
         edtavTfcontratoocorrencianotificacao_data_Jsonclick = "";
         edtavTfcontratoocorrencianotificacao_data_Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         edtContratoOcorrencia_Codigo_Jsonclick = "";
         edtContratoOcorrencia_Codigo_Visible = 1;
         Ddo_contratoocorrencianotificacao_docto_Searchbuttontext = "Pesquisar";
         Ddo_contratoocorrencianotificacao_docto_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contratoocorrencianotificacao_docto_Cleanfilter = "Limpar pesquisa";
         Ddo_contratoocorrencianotificacao_docto_Loadingdata = "Carregando dados...";
         Ddo_contratoocorrencianotificacao_docto_Sortdsc = "Ordenar de Z � A";
         Ddo_contratoocorrencianotificacao_docto_Sortasc = "Ordenar de A � Z";
         Ddo_contratoocorrencianotificacao_docto_Datalistupdateminimumcharacters = 0;
         Ddo_contratoocorrencianotificacao_docto_Datalistproc = "GetContratoOcorrenciaContratoOcorrenciaNotificacaoWCFilterData";
         Ddo_contratoocorrencianotificacao_docto_Datalisttype = "Dynamic";
         Ddo_contratoocorrencianotificacao_docto_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contratoocorrencianotificacao_docto_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contratoocorrencianotificacao_docto_Filtertype = "Character";
         Ddo_contratoocorrencianotificacao_docto_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratoocorrencianotificacao_docto_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratoocorrencianotificacao_docto_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratoocorrencianotificacao_docto_Titlecontrolidtoreplace = "";
         Ddo_contratoocorrencianotificacao_docto_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratoocorrencianotificacao_docto_Cls = "ColumnSettings";
         Ddo_contratoocorrencianotificacao_docto_Tooltip = "Op��es";
         Ddo_contratoocorrencianotificacao_docto_Caption = "";
         Ddo_contratoocorrencianotificacao_nome_Searchbuttontext = "Pesquisar";
         Ddo_contratoocorrencianotificacao_nome_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contratoocorrencianotificacao_nome_Cleanfilter = "Limpar pesquisa";
         Ddo_contratoocorrencianotificacao_nome_Loadingdata = "Carregando dados...";
         Ddo_contratoocorrencianotificacao_nome_Sortdsc = "Ordenar de Z � A";
         Ddo_contratoocorrencianotificacao_nome_Sortasc = "Ordenar de A � Z";
         Ddo_contratoocorrencianotificacao_nome_Datalistupdateminimumcharacters = 0;
         Ddo_contratoocorrencianotificacao_nome_Datalistproc = "GetContratoOcorrenciaContratoOcorrenciaNotificacaoWCFilterData";
         Ddo_contratoocorrencianotificacao_nome_Datalisttype = "Dynamic";
         Ddo_contratoocorrencianotificacao_nome_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contratoocorrencianotificacao_nome_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contratoocorrencianotificacao_nome_Filtertype = "Character";
         Ddo_contratoocorrencianotificacao_nome_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratoocorrencianotificacao_nome_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratoocorrencianotificacao_nome_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratoocorrencianotificacao_nome_Titlecontrolidtoreplace = "";
         Ddo_contratoocorrencianotificacao_nome_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratoocorrencianotificacao_nome_Cls = "ColumnSettings";
         Ddo_contratoocorrencianotificacao_nome_Tooltip = "Op��es";
         Ddo_contratoocorrencianotificacao_nome_Caption = "";
         Ddo_contratoocorrencianotificacao_responsavel_Searchbuttontext = "Pesquisar";
         Ddo_contratoocorrencianotificacao_responsavel_Rangefilterto = "At�";
         Ddo_contratoocorrencianotificacao_responsavel_Rangefilterfrom = "Desde";
         Ddo_contratoocorrencianotificacao_responsavel_Cleanfilter = "Limpar pesquisa";
         Ddo_contratoocorrencianotificacao_responsavel_Sortdsc = "Ordenar de Z � A";
         Ddo_contratoocorrencianotificacao_responsavel_Sortasc = "Ordenar de A � Z";
         Ddo_contratoocorrencianotificacao_responsavel_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contratoocorrencianotificacao_responsavel_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contratoocorrencianotificacao_responsavel_Filtertype = "Numeric";
         Ddo_contratoocorrencianotificacao_responsavel_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratoocorrencianotificacao_responsavel_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratoocorrencianotificacao_responsavel_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratoocorrencianotificacao_responsavel_Titlecontrolidtoreplace = "";
         Ddo_contratoocorrencianotificacao_responsavel_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratoocorrencianotificacao_responsavel_Cls = "ColumnSettings";
         Ddo_contratoocorrencianotificacao_responsavel_Tooltip = "Op��es";
         Ddo_contratoocorrencianotificacao_responsavel_Caption = "";
         Ddo_contratoocorrencianotificacao_protocolo_Searchbuttontext = "Pesquisar";
         Ddo_contratoocorrencianotificacao_protocolo_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contratoocorrencianotificacao_protocolo_Cleanfilter = "Limpar pesquisa";
         Ddo_contratoocorrencianotificacao_protocolo_Loadingdata = "Carregando dados...";
         Ddo_contratoocorrencianotificacao_protocolo_Sortdsc = "Ordenar de Z � A";
         Ddo_contratoocorrencianotificacao_protocolo_Sortasc = "Ordenar de A � Z";
         Ddo_contratoocorrencianotificacao_protocolo_Datalistupdateminimumcharacters = 0;
         Ddo_contratoocorrencianotificacao_protocolo_Datalistproc = "GetContratoOcorrenciaContratoOcorrenciaNotificacaoWCFilterData";
         Ddo_contratoocorrencianotificacao_protocolo_Datalisttype = "Dynamic";
         Ddo_contratoocorrencianotificacao_protocolo_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contratoocorrencianotificacao_protocolo_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contratoocorrencianotificacao_protocolo_Filtertype = "Character";
         Ddo_contratoocorrencianotificacao_protocolo_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratoocorrencianotificacao_protocolo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratoocorrencianotificacao_protocolo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratoocorrencianotificacao_protocolo_Titlecontrolidtoreplace = "";
         Ddo_contratoocorrencianotificacao_protocolo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratoocorrencianotificacao_protocolo_Cls = "ColumnSettings";
         Ddo_contratoocorrencianotificacao_protocolo_Tooltip = "Op��es";
         Ddo_contratoocorrencianotificacao_protocolo_Caption = "";
         Ddo_contratoocorrencianotificacao_cumprido_Searchbuttontext = "Pesquisar";
         Ddo_contratoocorrencianotificacao_cumprido_Rangefilterto = "At�";
         Ddo_contratoocorrencianotificacao_cumprido_Rangefilterfrom = "Desde";
         Ddo_contratoocorrencianotificacao_cumprido_Cleanfilter = "Limpar pesquisa";
         Ddo_contratoocorrencianotificacao_cumprido_Sortdsc = "Ordenar de Z � A";
         Ddo_contratoocorrencianotificacao_cumprido_Sortasc = "Ordenar de A � Z";
         Ddo_contratoocorrencianotificacao_cumprido_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contratoocorrencianotificacao_cumprido_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contratoocorrencianotificacao_cumprido_Filtertype = "Date";
         Ddo_contratoocorrencianotificacao_cumprido_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratoocorrencianotificacao_cumprido_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratoocorrencianotificacao_cumprido_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratoocorrencianotificacao_cumprido_Titlecontrolidtoreplace = "";
         Ddo_contratoocorrencianotificacao_cumprido_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratoocorrencianotificacao_cumprido_Cls = "ColumnSettings";
         Ddo_contratoocorrencianotificacao_cumprido_Tooltip = "Op��es";
         Ddo_contratoocorrencianotificacao_cumprido_Caption = "";
         Ddo_contratoocorrencianotificacao_descricao_Searchbuttontext = "Pesquisar";
         Ddo_contratoocorrencianotificacao_descricao_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contratoocorrencianotificacao_descricao_Cleanfilter = "Limpar pesquisa";
         Ddo_contratoocorrencianotificacao_descricao_Loadingdata = "Carregando dados...";
         Ddo_contratoocorrencianotificacao_descricao_Sortdsc = "Ordenar de Z � A";
         Ddo_contratoocorrencianotificacao_descricao_Sortasc = "Ordenar de A � Z";
         Ddo_contratoocorrencianotificacao_descricao_Datalistupdateminimumcharacters = 0;
         Ddo_contratoocorrencianotificacao_descricao_Datalistproc = "GetContratoOcorrenciaContratoOcorrenciaNotificacaoWCFilterData";
         Ddo_contratoocorrencianotificacao_descricao_Datalisttype = "Dynamic";
         Ddo_contratoocorrencianotificacao_descricao_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contratoocorrencianotificacao_descricao_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contratoocorrencianotificacao_descricao_Filtertype = "Character";
         Ddo_contratoocorrencianotificacao_descricao_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratoocorrencianotificacao_descricao_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratoocorrencianotificacao_descricao_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratoocorrencianotificacao_descricao_Titlecontrolidtoreplace = "";
         Ddo_contratoocorrencianotificacao_descricao_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratoocorrencianotificacao_descricao_Cls = "ColumnSettings";
         Ddo_contratoocorrencianotificacao_descricao_Tooltip = "Op��es";
         Ddo_contratoocorrencianotificacao_descricao_Caption = "";
         Ddo_contratoocorrencianotificacao_prazo_Searchbuttontext = "Pesquisar";
         Ddo_contratoocorrencianotificacao_prazo_Rangefilterto = "At�";
         Ddo_contratoocorrencianotificacao_prazo_Rangefilterfrom = "Desde";
         Ddo_contratoocorrencianotificacao_prazo_Cleanfilter = "Limpar pesquisa";
         Ddo_contratoocorrencianotificacao_prazo_Sortdsc = "Ordenar de Z � A";
         Ddo_contratoocorrencianotificacao_prazo_Sortasc = "Ordenar de A � Z";
         Ddo_contratoocorrencianotificacao_prazo_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contratoocorrencianotificacao_prazo_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contratoocorrencianotificacao_prazo_Filtertype = "Numeric";
         Ddo_contratoocorrencianotificacao_prazo_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratoocorrencianotificacao_prazo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratoocorrencianotificacao_prazo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratoocorrencianotificacao_prazo_Titlecontrolidtoreplace = "";
         Ddo_contratoocorrencianotificacao_prazo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratoocorrencianotificacao_prazo_Cls = "ColumnSettings";
         Ddo_contratoocorrencianotificacao_prazo_Tooltip = "Op��es";
         Ddo_contratoocorrencianotificacao_prazo_Caption = "";
         Ddo_contratoocorrencianotificacao_data_Searchbuttontext = "Pesquisar";
         Ddo_contratoocorrencianotificacao_data_Rangefilterto = "At�";
         Ddo_contratoocorrencianotificacao_data_Rangefilterfrom = "Desde";
         Ddo_contratoocorrencianotificacao_data_Cleanfilter = "Limpar pesquisa";
         Ddo_contratoocorrencianotificacao_data_Sortdsc = "Ordenar de Z � A";
         Ddo_contratoocorrencianotificacao_data_Sortasc = "Ordenar de A � Z";
         Ddo_contratoocorrencianotificacao_data_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contratoocorrencianotificacao_data_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contratoocorrencianotificacao_data_Filtertype = "Date";
         Ddo_contratoocorrencianotificacao_data_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratoocorrencianotificacao_data_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratoocorrencianotificacao_data_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratoocorrencianotificacao_data_Titlecontrolidtoreplace = "";
         Ddo_contratoocorrencianotificacao_data_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratoocorrencianotificacao_data_Cls = "ColumnSettings";
         Ddo_contratoocorrencianotificacao_data_Tooltip = "Op��es";
         Ddo_contratoocorrencianotificacao_data_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A297ContratoOcorrenciaNotificacao_Codigo',fld:'CONTRATOOCORRENCIANOTIFICACAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A294ContratoOcorrencia_Codigo',fld:'CONTRATOOCORRENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A303ContratoOcorrenciaNotificacao_Responsavel',fld:'CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL',pic:'ZZZZZ9',hsh:true,nv:0},{av:'sPrefix',nv:''},{av:'AV43ddo_ContratoOcorrenciaNotificacao_DataTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_ContratoOcorrenciaNotificacao_PrazoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_ContratoOcorrenciaNotificacao_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_ContratoOcorrenciaNotificacao_CumpridoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_ContratoOcorrenciaNotificacao_ProtocoloTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_ContratoOcorrenciaNotificacao_ResponsavelTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVELTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_ContratoOcorrenciaNotificacao_NomeTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_ContratoOcorrenciaNotificacao_DoctoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV82Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV39TFContratoOcorrenciaNotificacao_Data',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DATA',pic:'',nv:''},{av:'AV40TFContratoOcorrenciaNotificacao_Data_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DATA_TO',pic:'',nv:''},{av:'AV45TFContratoOcorrenciaNotificacao_Prazo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO',pic:'ZZZ9',nv:0},{av:'AV46TFContratoOcorrenciaNotificacao_Prazo_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO_TO',pic:'ZZZ9',nv:0},{av:'AV49TFContratoOcorrenciaNotificacao_Descricao',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO',pic:'@!',nv:''},{av:'AV50TFContratoOcorrenciaNotificacao_Descricao_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV53TFContratoOcorrenciaNotificacao_Cumprido',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO',pic:'',nv:''},{av:'AV54TFContratoOcorrenciaNotificacao_Cumprido_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_TO',pic:'',nv:''},{av:'AV59TFContratoOcorrenciaNotificacao_Protocolo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO',pic:'',nv:''},{av:'AV60TFContratoOcorrenciaNotificacao_Protocolo_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_SEL',pic:'',nv:''},{av:'AV63TFContratoOcorrenciaNotificacao_Responsavel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL',pic:'ZZZZZ9',nv:0},{av:'AV64TFContratoOcorrenciaNotificacao_Responsavel_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_TO',pic:'ZZZZZ9',nv:0},{av:'AV67TFContratoOcorrenciaNotificacao_Nome',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_NOME',pic:'@!',nv:''},{av:'AV68TFContratoOcorrenciaNotificacao_Nome_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_NOME_SEL',pic:'@!',nv:''},{av:'AV71TFContratoOcorrenciaNotificacao_Docto',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DOCTO',pic:'',nv:''},{av:'AV72TFContratoOcorrenciaNotificacao_Docto_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DOCTO_SEL',pic:'',nv:''},{av:'AV7ContratoOcorrencia_Codigo',fld:'vCONTRATOOCORRENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV34DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17ContratoOcorrenciaNotificacao_Data1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA1',pic:'',nv:''},{av:'AV18ContratoOcorrenciaNotificacao_Data_To1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO1',pic:'',nv:''},{av:'AV19ContratoOcorrenciaNotificacao_Prazo1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_PRAZO1',pic:'ZZZ9',nv:0},{av:'AV20ContratoOcorrenciaNotificacao_Prazo_To1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_PRAZO_TO1',pic:'ZZZ9',nv:0},{av:'AV33DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV21DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23ContratoOcorrenciaNotificacao_Data2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA2',pic:'',nv:''},{av:'AV24ContratoOcorrenciaNotificacao_Data_To2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO2',pic:'',nv:''},{av:'AV25ContratoOcorrenciaNotificacao_Prazo2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_PRAZO2',pic:'ZZZ9',nv:0},{av:'AV26ContratoOcorrenciaNotificacao_Prazo_To2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_PRAZO_TO2',pic:'ZZZ9',nv:0}],oparms:[{av:'AV38ContratoOcorrenciaNotificacao_DataTitleFilterData',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATATITLEFILTERDATA',pic:'',nv:null},{av:'AV44ContratoOcorrenciaNotificacao_PrazoTitleFilterData',fld:'vCONTRATOOCORRENCIANOTIFICACAO_PRAZOTITLEFILTERDATA',pic:'',nv:null},{av:'AV48ContratoOcorrenciaNotificacao_DescricaoTitleFilterData',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DESCRICAOTITLEFILTERDATA',pic:'',nv:null},{av:'AV52ContratoOcorrenciaNotificacao_CumpridoTitleFilterData',fld:'vCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDOTITLEFILTERDATA',pic:'',nv:null},{av:'AV58ContratoOcorrenciaNotificacao_ProtocoloTitleFilterData',fld:'vCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLOTITLEFILTERDATA',pic:'',nv:null},{av:'AV62ContratoOcorrenciaNotificacao_ResponsavelTitleFilterData',fld:'vCONTRATOOCORRENCIANOTIFICACAO_RESPONSAVELTITLEFILTERDATA',pic:'',nv:null},{av:'AV66ContratoOcorrenciaNotificacao_NomeTitleFilterData',fld:'vCONTRATOOCORRENCIANOTIFICACAO_NOMETITLEFILTERDATA',pic:'',nv:null},{av:'AV70ContratoOcorrenciaNotificacao_DoctoTitleFilterData',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DOCTOTITLEFILTERDATA',pic:'',nv:null},{av:'edtContratoOcorrenciaNotificacao_Data_Titleformat',ctrl:'CONTRATOOCORRENCIANOTIFICACAO_DATA',prop:'Titleformat'},{av:'edtContratoOcorrenciaNotificacao_Data_Title',ctrl:'CONTRATOOCORRENCIANOTIFICACAO_DATA',prop:'Title'},{av:'edtContratoOcorrenciaNotificacao_Prazo_Titleformat',ctrl:'CONTRATOOCORRENCIANOTIFICACAO_PRAZO',prop:'Titleformat'},{av:'edtContratoOcorrenciaNotificacao_Prazo_Title',ctrl:'CONTRATOOCORRENCIANOTIFICACAO_PRAZO',prop:'Title'},{av:'edtContratoOcorrenciaNotificacao_Descricao_Titleformat',ctrl:'CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO',prop:'Titleformat'},{av:'edtContratoOcorrenciaNotificacao_Descricao_Title',ctrl:'CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO',prop:'Title'},{av:'edtContratoOcorrenciaNotificacao_Cumprido_Titleformat',ctrl:'CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO',prop:'Titleformat'},{av:'edtContratoOcorrenciaNotificacao_Cumprido_Title',ctrl:'CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO',prop:'Title'},{av:'edtContratoOcorrenciaNotificacao_Protocolo_Titleformat',ctrl:'CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO',prop:'Titleformat'},{av:'edtContratoOcorrenciaNotificacao_Protocolo_Title',ctrl:'CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO',prop:'Title'},{av:'edtContratoOcorrenciaNotificacao_Responsavel_Titleformat',ctrl:'CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL',prop:'Titleformat'},{av:'edtContratoOcorrenciaNotificacao_Responsavel_Title',ctrl:'CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL',prop:'Title'},{av:'edtContratoOcorrenciaNotificacao_Nome_Titleformat',ctrl:'CONTRATOOCORRENCIANOTIFICACAO_NOME',prop:'Titleformat'},{av:'edtContratoOcorrenciaNotificacao_Nome_Title',ctrl:'CONTRATOOCORRENCIANOTIFICACAO_NOME',prop:'Title'},{av:'edtContratoOcorrenciaNotificacao_Docto_Titleformat',ctrl:'CONTRATOOCORRENCIANOTIFICACAO_DOCTO',prop:'Titleformat'},{av:'edtContratoOcorrenciaNotificacao_Docto_Title',ctrl:'CONTRATOOCORRENCIANOTIFICACAO_DOCTO',prop:'Title'},{av:'AV76GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV77GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E117A2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17ContratoOcorrenciaNotificacao_Data1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA1',pic:'',nv:''},{av:'AV18ContratoOcorrenciaNotificacao_Data_To1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO1',pic:'',nv:''},{av:'AV19ContratoOcorrenciaNotificacao_Prazo1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_PRAZO1',pic:'ZZZ9',nv:0},{av:'AV20ContratoOcorrenciaNotificacao_Prazo_To1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_PRAZO_TO1',pic:'ZZZ9',nv:0},{av:'AV22DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23ContratoOcorrenciaNotificacao_Data2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA2',pic:'',nv:''},{av:'AV24ContratoOcorrenciaNotificacao_Data_To2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO2',pic:'',nv:''},{av:'AV25ContratoOcorrenciaNotificacao_Prazo2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_PRAZO2',pic:'ZZZ9',nv:0},{av:'AV26ContratoOcorrenciaNotificacao_Prazo_To2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_PRAZO_TO2',pic:'ZZZ9',nv:0},{av:'AV21DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV39TFContratoOcorrenciaNotificacao_Data',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DATA',pic:'',nv:''},{av:'AV40TFContratoOcorrenciaNotificacao_Data_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DATA_TO',pic:'',nv:''},{av:'AV45TFContratoOcorrenciaNotificacao_Prazo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO',pic:'ZZZ9',nv:0},{av:'AV46TFContratoOcorrenciaNotificacao_Prazo_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO_TO',pic:'ZZZ9',nv:0},{av:'AV49TFContratoOcorrenciaNotificacao_Descricao',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO',pic:'@!',nv:''},{av:'AV50TFContratoOcorrenciaNotificacao_Descricao_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV53TFContratoOcorrenciaNotificacao_Cumprido',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO',pic:'',nv:''},{av:'AV54TFContratoOcorrenciaNotificacao_Cumprido_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_TO',pic:'',nv:''},{av:'AV59TFContratoOcorrenciaNotificacao_Protocolo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO',pic:'',nv:''},{av:'AV60TFContratoOcorrenciaNotificacao_Protocolo_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_SEL',pic:'',nv:''},{av:'AV63TFContratoOcorrenciaNotificacao_Responsavel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL',pic:'ZZZZZ9',nv:0},{av:'AV64TFContratoOcorrenciaNotificacao_Responsavel_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_TO',pic:'ZZZZZ9',nv:0},{av:'AV67TFContratoOcorrenciaNotificacao_Nome',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_NOME',pic:'@!',nv:''},{av:'AV68TFContratoOcorrenciaNotificacao_Nome_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_NOME_SEL',pic:'@!',nv:''},{av:'AV71TFContratoOcorrenciaNotificacao_Docto',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DOCTO',pic:'',nv:''},{av:'AV72TFContratoOcorrenciaNotificacao_Docto_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DOCTO_SEL',pic:'',nv:''},{av:'AV7ContratoOcorrencia_Codigo',fld:'vCONTRATOOCORRENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV43ddo_ContratoOcorrenciaNotificacao_DataTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_ContratoOcorrenciaNotificacao_PrazoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_ContratoOcorrenciaNotificacao_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_ContratoOcorrenciaNotificacao_CumpridoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_ContratoOcorrenciaNotificacao_ProtocoloTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_ContratoOcorrenciaNotificacao_ResponsavelTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVELTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_ContratoOcorrenciaNotificacao_NomeTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_ContratoOcorrenciaNotificacao_DoctoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV82Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV34DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV33DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A297ContratoOcorrenciaNotificacao_Codigo',fld:'CONTRATOOCORRENCIANOTIFICACAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A294ContratoOcorrencia_Codigo',fld:'CONTRATOOCORRENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A303ContratoOcorrenciaNotificacao_Responsavel',fld:'CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL',pic:'ZZZZZ9',hsh:true,nv:0},{av:'sPrefix',nv:''},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA.ONOPTIONCLICKED","{handler:'E127A2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17ContratoOcorrenciaNotificacao_Data1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA1',pic:'',nv:''},{av:'AV18ContratoOcorrenciaNotificacao_Data_To1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO1',pic:'',nv:''},{av:'AV19ContratoOcorrenciaNotificacao_Prazo1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_PRAZO1',pic:'ZZZ9',nv:0},{av:'AV20ContratoOcorrenciaNotificacao_Prazo_To1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_PRAZO_TO1',pic:'ZZZ9',nv:0},{av:'AV22DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23ContratoOcorrenciaNotificacao_Data2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA2',pic:'',nv:''},{av:'AV24ContratoOcorrenciaNotificacao_Data_To2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO2',pic:'',nv:''},{av:'AV25ContratoOcorrenciaNotificacao_Prazo2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_PRAZO2',pic:'ZZZ9',nv:0},{av:'AV26ContratoOcorrenciaNotificacao_Prazo_To2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_PRAZO_TO2',pic:'ZZZ9',nv:0},{av:'AV21DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV39TFContratoOcorrenciaNotificacao_Data',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DATA',pic:'',nv:''},{av:'AV40TFContratoOcorrenciaNotificacao_Data_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DATA_TO',pic:'',nv:''},{av:'AV45TFContratoOcorrenciaNotificacao_Prazo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO',pic:'ZZZ9',nv:0},{av:'AV46TFContratoOcorrenciaNotificacao_Prazo_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO_TO',pic:'ZZZ9',nv:0},{av:'AV49TFContratoOcorrenciaNotificacao_Descricao',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO',pic:'@!',nv:''},{av:'AV50TFContratoOcorrenciaNotificacao_Descricao_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV53TFContratoOcorrenciaNotificacao_Cumprido',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO',pic:'',nv:''},{av:'AV54TFContratoOcorrenciaNotificacao_Cumprido_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_TO',pic:'',nv:''},{av:'AV59TFContratoOcorrenciaNotificacao_Protocolo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO',pic:'',nv:''},{av:'AV60TFContratoOcorrenciaNotificacao_Protocolo_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_SEL',pic:'',nv:''},{av:'AV63TFContratoOcorrenciaNotificacao_Responsavel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL',pic:'ZZZZZ9',nv:0},{av:'AV64TFContratoOcorrenciaNotificacao_Responsavel_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_TO',pic:'ZZZZZ9',nv:0},{av:'AV67TFContratoOcorrenciaNotificacao_Nome',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_NOME',pic:'@!',nv:''},{av:'AV68TFContratoOcorrenciaNotificacao_Nome_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_NOME_SEL',pic:'@!',nv:''},{av:'AV71TFContratoOcorrenciaNotificacao_Docto',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DOCTO',pic:'',nv:''},{av:'AV72TFContratoOcorrenciaNotificacao_Docto_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DOCTO_SEL',pic:'',nv:''},{av:'AV7ContratoOcorrencia_Codigo',fld:'vCONTRATOOCORRENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV43ddo_ContratoOcorrenciaNotificacao_DataTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_ContratoOcorrenciaNotificacao_PrazoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_ContratoOcorrenciaNotificacao_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_ContratoOcorrenciaNotificacao_CumpridoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_ContratoOcorrenciaNotificacao_ProtocoloTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_ContratoOcorrenciaNotificacao_ResponsavelTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVELTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_ContratoOcorrenciaNotificacao_NomeTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_ContratoOcorrenciaNotificacao_DoctoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV82Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV34DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV33DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A297ContratoOcorrenciaNotificacao_Codigo',fld:'CONTRATOOCORRENCIANOTIFICACAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A294ContratoOcorrencia_Codigo',fld:'CONTRATOOCORRENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A303ContratoOcorrenciaNotificacao_Responsavel',fld:'CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL',pic:'ZZZZZ9',hsh:true,nv:0},{av:'sPrefix',nv:''},{av:'Ddo_contratoocorrencianotificacao_data_Activeeventkey',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA',prop:'ActiveEventKey'},{av:'Ddo_contratoocorrencianotificacao_data_Filteredtext_get',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA',prop:'FilteredText_get'},{av:'Ddo_contratoocorrencianotificacao_data_Filteredtextto_get',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA',prop:'FilteredTextTo_get'}],oparms:[{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratoocorrencianotificacao_data_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA',prop:'SortedStatus'},{av:'AV39TFContratoOcorrenciaNotificacao_Data',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DATA',pic:'',nv:''},{av:'AV40TFContratoOcorrenciaNotificacao_Data_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DATA_TO',pic:'',nv:''},{av:'Ddo_contratoocorrencianotificacao_prazo_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_descricao_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_cumprido_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_protocolo_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_responsavel_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_nome_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_docto_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO.ONOPTIONCLICKED","{handler:'E137A2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17ContratoOcorrenciaNotificacao_Data1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA1',pic:'',nv:''},{av:'AV18ContratoOcorrenciaNotificacao_Data_To1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO1',pic:'',nv:''},{av:'AV19ContratoOcorrenciaNotificacao_Prazo1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_PRAZO1',pic:'ZZZ9',nv:0},{av:'AV20ContratoOcorrenciaNotificacao_Prazo_To1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_PRAZO_TO1',pic:'ZZZ9',nv:0},{av:'AV22DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23ContratoOcorrenciaNotificacao_Data2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA2',pic:'',nv:''},{av:'AV24ContratoOcorrenciaNotificacao_Data_To2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO2',pic:'',nv:''},{av:'AV25ContratoOcorrenciaNotificacao_Prazo2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_PRAZO2',pic:'ZZZ9',nv:0},{av:'AV26ContratoOcorrenciaNotificacao_Prazo_To2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_PRAZO_TO2',pic:'ZZZ9',nv:0},{av:'AV21DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV39TFContratoOcorrenciaNotificacao_Data',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DATA',pic:'',nv:''},{av:'AV40TFContratoOcorrenciaNotificacao_Data_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DATA_TO',pic:'',nv:''},{av:'AV45TFContratoOcorrenciaNotificacao_Prazo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO',pic:'ZZZ9',nv:0},{av:'AV46TFContratoOcorrenciaNotificacao_Prazo_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO_TO',pic:'ZZZ9',nv:0},{av:'AV49TFContratoOcorrenciaNotificacao_Descricao',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO',pic:'@!',nv:''},{av:'AV50TFContratoOcorrenciaNotificacao_Descricao_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV53TFContratoOcorrenciaNotificacao_Cumprido',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO',pic:'',nv:''},{av:'AV54TFContratoOcorrenciaNotificacao_Cumprido_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_TO',pic:'',nv:''},{av:'AV59TFContratoOcorrenciaNotificacao_Protocolo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO',pic:'',nv:''},{av:'AV60TFContratoOcorrenciaNotificacao_Protocolo_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_SEL',pic:'',nv:''},{av:'AV63TFContratoOcorrenciaNotificacao_Responsavel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL',pic:'ZZZZZ9',nv:0},{av:'AV64TFContratoOcorrenciaNotificacao_Responsavel_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_TO',pic:'ZZZZZ9',nv:0},{av:'AV67TFContratoOcorrenciaNotificacao_Nome',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_NOME',pic:'@!',nv:''},{av:'AV68TFContratoOcorrenciaNotificacao_Nome_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_NOME_SEL',pic:'@!',nv:''},{av:'AV71TFContratoOcorrenciaNotificacao_Docto',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DOCTO',pic:'',nv:''},{av:'AV72TFContratoOcorrenciaNotificacao_Docto_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DOCTO_SEL',pic:'',nv:''},{av:'AV7ContratoOcorrencia_Codigo',fld:'vCONTRATOOCORRENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV43ddo_ContratoOcorrenciaNotificacao_DataTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_ContratoOcorrenciaNotificacao_PrazoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_ContratoOcorrenciaNotificacao_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_ContratoOcorrenciaNotificacao_CumpridoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_ContratoOcorrenciaNotificacao_ProtocoloTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_ContratoOcorrenciaNotificacao_ResponsavelTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVELTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_ContratoOcorrenciaNotificacao_NomeTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_ContratoOcorrenciaNotificacao_DoctoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV82Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV34DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV33DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A297ContratoOcorrenciaNotificacao_Codigo',fld:'CONTRATOOCORRENCIANOTIFICACAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A294ContratoOcorrencia_Codigo',fld:'CONTRATOOCORRENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A303ContratoOcorrenciaNotificacao_Responsavel',fld:'CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL',pic:'ZZZZZ9',hsh:true,nv:0},{av:'sPrefix',nv:''},{av:'Ddo_contratoocorrencianotificacao_prazo_Activeeventkey',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO',prop:'ActiveEventKey'},{av:'Ddo_contratoocorrencianotificacao_prazo_Filteredtext_get',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO',prop:'FilteredText_get'},{av:'Ddo_contratoocorrencianotificacao_prazo_Filteredtextto_get',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratoocorrencianotificacao_prazo_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO',prop:'SortedStatus'},{av:'AV45TFContratoOcorrenciaNotificacao_Prazo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO',pic:'ZZZ9',nv:0},{av:'AV46TFContratoOcorrenciaNotificacao_Prazo_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO_TO',pic:'ZZZ9',nv:0},{av:'Ddo_contratoocorrencianotificacao_data_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_descricao_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_cumprido_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_protocolo_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_responsavel_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_nome_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_docto_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO.ONOPTIONCLICKED","{handler:'E147A2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17ContratoOcorrenciaNotificacao_Data1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA1',pic:'',nv:''},{av:'AV18ContratoOcorrenciaNotificacao_Data_To1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO1',pic:'',nv:''},{av:'AV19ContratoOcorrenciaNotificacao_Prazo1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_PRAZO1',pic:'ZZZ9',nv:0},{av:'AV20ContratoOcorrenciaNotificacao_Prazo_To1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_PRAZO_TO1',pic:'ZZZ9',nv:0},{av:'AV22DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23ContratoOcorrenciaNotificacao_Data2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA2',pic:'',nv:''},{av:'AV24ContratoOcorrenciaNotificacao_Data_To2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO2',pic:'',nv:''},{av:'AV25ContratoOcorrenciaNotificacao_Prazo2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_PRAZO2',pic:'ZZZ9',nv:0},{av:'AV26ContratoOcorrenciaNotificacao_Prazo_To2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_PRAZO_TO2',pic:'ZZZ9',nv:0},{av:'AV21DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV39TFContratoOcorrenciaNotificacao_Data',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DATA',pic:'',nv:''},{av:'AV40TFContratoOcorrenciaNotificacao_Data_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DATA_TO',pic:'',nv:''},{av:'AV45TFContratoOcorrenciaNotificacao_Prazo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO',pic:'ZZZ9',nv:0},{av:'AV46TFContratoOcorrenciaNotificacao_Prazo_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO_TO',pic:'ZZZ9',nv:0},{av:'AV49TFContratoOcorrenciaNotificacao_Descricao',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO',pic:'@!',nv:''},{av:'AV50TFContratoOcorrenciaNotificacao_Descricao_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV53TFContratoOcorrenciaNotificacao_Cumprido',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO',pic:'',nv:''},{av:'AV54TFContratoOcorrenciaNotificacao_Cumprido_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_TO',pic:'',nv:''},{av:'AV59TFContratoOcorrenciaNotificacao_Protocolo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO',pic:'',nv:''},{av:'AV60TFContratoOcorrenciaNotificacao_Protocolo_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_SEL',pic:'',nv:''},{av:'AV63TFContratoOcorrenciaNotificacao_Responsavel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL',pic:'ZZZZZ9',nv:0},{av:'AV64TFContratoOcorrenciaNotificacao_Responsavel_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_TO',pic:'ZZZZZ9',nv:0},{av:'AV67TFContratoOcorrenciaNotificacao_Nome',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_NOME',pic:'@!',nv:''},{av:'AV68TFContratoOcorrenciaNotificacao_Nome_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_NOME_SEL',pic:'@!',nv:''},{av:'AV71TFContratoOcorrenciaNotificacao_Docto',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DOCTO',pic:'',nv:''},{av:'AV72TFContratoOcorrenciaNotificacao_Docto_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DOCTO_SEL',pic:'',nv:''},{av:'AV7ContratoOcorrencia_Codigo',fld:'vCONTRATOOCORRENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV43ddo_ContratoOcorrenciaNotificacao_DataTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_ContratoOcorrenciaNotificacao_PrazoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_ContratoOcorrenciaNotificacao_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_ContratoOcorrenciaNotificacao_CumpridoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_ContratoOcorrenciaNotificacao_ProtocoloTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_ContratoOcorrenciaNotificacao_ResponsavelTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVELTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_ContratoOcorrenciaNotificacao_NomeTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_ContratoOcorrenciaNotificacao_DoctoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV82Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV34DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV33DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A297ContratoOcorrenciaNotificacao_Codigo',fld:'CONTRATOOCORRENCIANOTIFICACAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A294ContratoOcorrencia_Codigo',fld:'CONTRATOOCORRENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A303ContratoOcorrenciaNotificacao_Responsavel',fld:'CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL',pic:'ZZZZZ9',hsh:true,nv:0},{av:'sPrefix',nv:''},{av:'Ddo_contratoocorrencianotificacao_descricao_Activeeventkey',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO',prop:'ActiveEventKey'},{av:'Ddo_contratoocorrencianotificacao_descricao_Filteredtext_get',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO',prop:'FilteredText_get'},{av:'Ddo_contratoocorrencianotificacao_descricao_Selectedvalue_get',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO',prop:'SelectedValue_get'}],oparms:[{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratoocorrencianotificacao_descricao_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO',prop:'SortedStatus'},{av:'AV49TFContratoOcorrenciaNotificacao_Descricao',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO',pic:'@!',nv:''},{av:'AV50TFContratoOcorrenciaNotificacao_Descricao_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'Ddo_contratoocorrencianotificacao_data_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_prazo_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_cumprido_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_protocolo_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_responsavel_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_nome_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_docto_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO.ONOPTIONCLICKED","{handler:'E157A2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17ContratoOcorrenciaNotificacao_Data1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA1',pic:'',nv:''},{av:'AV18ContratoOcorrenciaNotificacao_Data_To1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO1',pic:'',nv:''},{av:'AV19ContratoOcorrenciaNotificacao_Prazo1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_PRAZO1',pic:'ZZZ9',nv:0},{av:'AV20ContratoOcorrenciaNotificacao_Prazo_To1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_PRAZO_TO1',pic:'ZZZ9',nv:0},{av:'AV22DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23ContratoOcorrenciaNotificacao_Data2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA2',pic:'',nv:''},{av:'AV24ContratoOcorrenciaNotificacao_Data_To2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO2',pic:'',nv:''},{av:'AV25ContratoOcorrenciaNotificacao_Prazo2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_PRAZO2',pic:'ZZZ9',nv:0},{av:'AV26ContratoOcorrenciaNotificacao_Prazo_To2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_PRAZO_TO2',pic:'ZZZ9',nv:0},{av:'AV21DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV39TFContratoOcorrenciaNotificacao_Data',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DATA',pic:'',nv:''},{av:'AV40TFContratoOcorrenciaNotificacao_Data_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DATA_TO',pic:'',nv:''},{av:'AV45TFContratoOcorrenciaNotificacao_Prazo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO',pic:'ZZZ9',nv:0},{av:'AV46TFContratoOcorrenciaNotificacao_Prazo_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO_TO',pic:'ZZZ9',nv:0},{av:'AV49TFContratoOcorrenciaNotificacao_Descricao',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO',pic:'@!',nv:''},{av:'AV50TFContratoOcorrenciaNotificacao_Descricao_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV53TFContratoOcorrenciaNotificacao_Cumprido',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO',pic:'',nv:''},{av:'AV54TFContratoOcorrenciaNotificacao_Cumprido_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_TO',pic:'',nv:''},{av:'AV59TFContratoOcorrenciaNotificacao_Protocolo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO',pic:'',nv:''},{av:'AV60TFContratoOcorrenciaNotificacao_Protocolo_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_SEL',pic:'',nv:''},{av:'AV63TFContratoOcorrenciaNotificacao_Responsavel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL',pic:'ZZZZZ9',nv:0},{av:'AV64TFContratoOcorrenciaNotificacao_Responsavel_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_TO',pic:'ZZZZZ9',nv:0},{av:'AV67TFContratoOcorrenciaNotificacao_Nome',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_NOME',pic:'@!',nv:''},{av:'AV68TFContratoOcorrenciaNotificacao_Nome_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_NOME_SEL',pic:'@!',nv:''},{av:'AV71TFContratoOcorrenciaNotificacao_Docto',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DOCTO',pic:'',nv:''},{av:'AV72TFContratoOcorrenciaNotificacao_Docto_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DOCTO_SEL',pic:'',nv:''},{av:'AV7ContratoOcorrencia_Codigo',fld:'vCONTRATOOCORRENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV43ddo_ContratoOcorrenciaNotificacao_DataTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_ContratoOcorrenciaNotificacao_PrazoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_ContratoOcorrenciaNotificacao_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_ContratoOcorrenciaNotificacao_CumpridoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_ContratoOcorrenciaNotificacao_ProtocoloTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_ContratoOcorrenciaNotificacao_ResponsavelTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVELTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_ContratoOcorrenciaNotificacao_NomeTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_ContratoOcorrenciaNotificacao_DoctoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV82Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV34DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV33DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A297ContratoOcorrenciaNotificacao_Codigo',fld:'CONTRATOOCORRENCIANOTIFICACAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A294ContratoOcorrencia_Codigo',fld:'CONTRATOOCORRENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A303ContratoOcorrenciaNotificacao_Responsavel',fld:'CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL',pic:'ZZZZZ9',hsh:true,nv:0},{av:'sPrefix',nv:''},{av:'Ddo_contratoocorrencianotificacao_cumprido_Activeeventkey',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO',prop:'ActiveEventKey'},{av:'Ddo_contratoocorrencianotificacao_cumprido_Filteredtext_get',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO',prop:'FilteredText_get'},{av:'Ddo_contratoocorrencianotificacao_cumprido_Filteredtextto_get',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratoocorrencianotificacao_cumprido_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO',prop:'SortedStatus'},{av:'AV53TFContratoOcorrenciaNotificacao_Cumprido',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO',pic:'',nv:''},{av:'AV54TFContratoOcorrenciaNotificacao_Cumprido_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_TO',pic:'',nv:''},{av:'Ddo_contratoocorrencianotificacao_data_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_prazo_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_descricao_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_protocolo_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_responsavel_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_nome_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_docto_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO.ONOPTIONCLICKED","{handler:'E167A2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17ContratoOcorrenciaNotificacao_Data1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA1',pic:'',nv:''},{av:'AV18ContratoOcorrenciaNotificacao_Data_To1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO1',pic:'',nv:''},{av:'AV19ContratoOcorrenciaNotificacao_Prazo1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_PRAZO1',pic:'ZZZ9',nv:0},{av:'AV20ContratoOcorrenciaNotificacao_Prazo_To1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_PRAZO_TO1',pic:'ZZZ9',nv:0},{av:'AV22DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23ContratoOcorrenciaNotificacao_Data2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA2',pic:'',nv:''},{av:'AV24ContratoOcorrenciaNotificacao_Data_To2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO2',pic:'',nv:''},{av:'AV25ContratoOcorrenciaNotificacao_Prazo2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_PRAZO2',pic:'ZZZ9',nv:0},{av:'AV26ContratoOcorrenciaNotificacao_Prazo_To2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_PRAZO_TO2',pic:'ZZZ9',nv:0},{av:'AV21DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV39TFContratoOcorrenciaNotificacao_Data',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DATA',pic:'',nv:''},{av:'AV40TFContratoOcorrenciaNotificacao_Data_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DATA_TO',pic:'',nv:''},{av:'AV45TFContratoOcorrenciaNotificacao_Prazo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO',pic:'ZZZ9',nv:0},{av:'AV46TFContratoOcorrenciaNotificacao_Prazo_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO_TO',pic:'ZZZ9',nv:0},{av:'AV49TFContratoOcorrenciaNotificacao_Descricao',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO',pic:'@!',nv:''},{av:'AV50TFContratoOcorrenciaNotificacao_Descricao_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV53TFContratoOcorrenciaNotificacao_Cumprido',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO',pic:'',nv:''},{av:'AV54TFContratoOcorrenciaNotificacao_Cumprido_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_TO',pic:'',nv:''},{av:'AV59TFContratoOcorrenciaNotificacao_Protocolo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO',pic:'',nv:''},{av:'AV60TFContratoOcorrenciaNotificacao_Protocolo_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_SEL',pic:'',nv:''},{av:'AV63TFContratoOcorrenciaNotificacao_Responsavel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL',pic:'ZZZZZ9',nv:0},{av:'AV64TFContratoOcorrenciaNotificacao_Responsavel_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_TO',pic:'ZZZZZ9',nv:0},{av:'AV67TFContratoOcorrenciaNotificacao_Nome',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_NOME',pic:'@!',nv:''},{av:'AV68TFContratoOcorrenciaNotificacao_Nome_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_NOME_SEL',pic:'@!',nv:''},{av:'AV71TFContratoOcorrenciaNotificacao_Docto',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DOCTO',pic:'',nv:''},{av:'AV72TFContratoOcorrenciaNotificacao_Docto_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DOCTO_SEL',pic:'',nv:''},{av:'AV7ContratoOcorrencia_Codigo',fld:'vCONTRATOOCORRENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV43ddo_ContratoOcorrenciaNotificacao_DataTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_ContratoOcorrenciaNotificacao_PrazoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_ContratoOcorrenciaNotificacao_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_ContratoOcorrenciaNotificacao_CumpridoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_ContratoOcorrenciaNotificacao_ProtocoloTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_ContratoOcorrenciaNotificacao_ResponsavelTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVELTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_ContratoOcorrenciaNotificacao_NomeTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_ContratoOcorrenciaNotificacao_DoctoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV82Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV34DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV33DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A297ContratoOcorrenciaNotificacao_Codigo',fld:'CONTRATOOCORRENCIANOTIFICACAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A294ContratoOcorrencia_Codigo',fld:'CONTRATOOCORRENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A303ContratoOcorrenciaNotificacao_Responsavel',fld:'CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL',pic:'ZZZZZ9',hsh:true,nv:0},{av:'sPrefix',nv:''},{av:'Ddo_contratoocorrencianotificacao_protocolo_Activeeventkey',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO',prop:'ActiveEventKey'},{av:'Ddo_contratoocorrencianotificacao_protocolo_Filteredtext_get',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO',prop:'FilteredText_get'},{av:'Ddo_contratoocorrencianotificacao_protocolo_Selectedvalue_get',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO',prop:'SelectedValue_get'}],oparms:[{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratoocorrencianotificacao_protocolo_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO',prop:'SortedStatus'},{av:'AV59TFContratoOcorrenciaNotificacao_Protocolo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO',pic:'',nv:''},{av:'AV60TFContratoOcorrenciaNotificacao_Protocolo_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_SEL',pic:'',nv:''},{av:'Ddo_contratoocorrencianotificacao_data_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_prazo_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_descricao_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_cumprido_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_responsavel_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_nome_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_docto_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL.ONOPTIONCLICKED","{handler:'E177A2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17ContratoOcorrenciaNotificacao_Data1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA1',pic:'',nv:''},{av:'AV18ContratoOcorrenciaNotificacao_Data_To1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO1',pic:'',nv:''},{av:'AV19ContratoOcorrenciaNotificacao_Prazo1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_PRAZO1',pic:'ZZZ9',nv:0},{av:'AV20ContratoOcorrenciaNotificacao_Prazo_To1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_PRAZO_TO1',pic:'ZZZ9',nv:0},{av:'AV22DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23ContratoOcorrenciaNotificacao_Data2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA2',pic:'',nv:''},{av:'AV24ContratoOcorrenciaNotificacao_Data_To2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO2',pic:'',nv:''},{av:'AV25ContratoOcorrenciaNotificacao_Prazo2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_PRAZO2',pic:'ZZZ9',nv:0},{av:'AV26ContratoOcorrenciaNotificacao_Prazo_To2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_PRAZO_TO2',pic:'ZZZ9',nv:0},{av:'AV21DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV39TFContratoOcorrenciaNotificacao_Data',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DATA',pic:'',nv:''},{av:'AV40TFContratoOcorrenciaNotificacao_Data_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DATA_TO',pic:'',nv:''},{av:'AV45TFContratoOcorrenciaNotificacao_Prazo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO',pic:'ZZZ9',nv:0},{av:'AV46TFContratoOcorrenciaNotificacao_Prazo_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO_TO',pic:'ZZZ9',nv:0},{av:'AV49TFContratoOcorrenciaNotificacao_Descricao',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO',pic:'@!',nv:''},{av:'AV50TFContratoOcorrenciaNotificacao_Descricao_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV53TFContratoOcorrenciaNotificacao_Cumprido',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO',pic:'',nv:''},{av:'AV54TFContratoOcorrenciaNotificacao_Cumprido_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_TO',pic:'',nv:''},{av:'AV59TFContratoOcorrenciaNotificacao_Protocolo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO',pic:'',nv:''},{av:'AV60TFContratoOcorrenciaNotificacao_Protocolo_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_SEL',pic:'',nv:''},{av:'AV63TFContratoOcorrenciaNotificacao_Responsavel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL',pic:'ZZZZZ9',nv:0},{av:'AV64TFContratoOcorrenciaNotificacao_Responsavel_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_TO',pic:'ZZZZZ9',nv:0},{av:'AV67TFContratoOcorrenciaNotificacao_Nome',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_NOME',pic:'@!',nv:''},{av:'AV68TFContratoOcorrenciaNotificacao_Nome_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_NOME_SEL',pic:'@!',nv:''},{av:'AV71TFContratoOcorrenciaNotificacao_Docto',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DOCTO',pic:'',nv:''},{av:'AV72TFContratoOcorrenciaNotificacao_Docto_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DOCTO_SEL',pic:'',nv:''},{av:'AV7ContratoOcorrencia_Codigo',fld:'vCONTRATOOCORRENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV43ddo_ContratoOcorrenciaNotificacao_DataTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_ContratoOcorrenciaNotificacao_PrazoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_ContratoOcorrenciaNotificacao_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_ContratoOcorrenciaNotificacao_CumpridoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_ContratoOcorrenciaNotificacao_ProtocoloTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_ContratoOcorrenciaNotificacao_ResponsavelTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVELTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_ContratoOcorrenciaNotificacao_NomeTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_ContratoOcorrenciaNotificacao_DoctoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV82Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV34DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV33DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A297ContratoOcorrenciaNotificacao_Codigo',fld:'CONTRATOOCORRENCIANOTIFICACAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A294ContratoOcorrencia_Codigo',fld:'CONTRATOOCORRENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A303ContratoOcorrenciaNotificacao_Responsavel',fld:'CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL',pic:'ZZZZZ9',hsh:true,nv:0},{av:'sPrefix',nv:''},{av:'Ddo_contratoocorrencianotificacao_responsavel_Activeeventkey',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL',prop:'ActiveEventKey'},{av:'Ddo_contratoocorrencianotificacao_responsavel_Filteredtext_get',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL',prop:'FilteredText_get'},{av:'Ddo_contratoocorrencianotificacao_responsavel_Filteredtextto_get',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL',prop:'FilteredTextTo_get'}],oparms:[{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratoocorrencianotificacao_responsavel_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL',prop:'SortedStatus'},{av:'AV63TFContratoOcorrenciaNotificacao_Responsavel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL',pic:'ZZZZZ9',nv:0},{av:'AV64TFContratoOcorrenciaNotificacao_Responsavel_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contratoocorrencianotificacao_data_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_prazo_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_descricao_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_cumprido_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_protocolo_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_nome_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_docto_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME.ONOPTIONCLICKED","{handler:'E187A2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17ContratoOcorrenciaNotificacao_Data1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA1',pic:'',nv:''},{av:'AV18ContratoOcorrenciaNotificacao_Data_To1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO1',pic:'',nv:''},{av:'AV19ContratoOcorrenciaNotificacao_Prazo1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_PRAZO1',pic:'ZZZ9',nv:0},{av:'AV20ContratoOcorrenciaNotificacao_Prazo_To1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_PRAZO_TO1',pic:'ZZZ9',nv:0},{av:'AV22DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23ContratoOcorrenciaNotificacao_Data2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA2',pic:'',nv:''},{av:'AV24ContratoOcorrenciaNotificacao_Data_To2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO2',pic:'',nv:''},{av:'AV25ContratoOcorrenciaNotificacao_Prazo2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_PRAZO2',pic:'ZZZ9',nv:0},{av:'AV26ContratoOcorrenciaNotificacao_Prazo_To2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_PRAZO_TO2',pic:'ZZZ9',nv:0},{av:'AV21DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV39TFContratoOcorrenciaNotificacao_Data',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DATA',pic:'',nv:''},{av:'AV40TFContratoOcorrenciaNotificacao_Data_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DATA_TO',pic:'',nv:''},{av:'AV45TFContratoOcorrenciaNotificacao_Prazo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO',pic:'ZZZ9',nv:0},{av:'AV46TFContratoOcorrenciaNotificacao_Prazo_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO_TO',pic:'ZZZ9',nv:0},{av:'AV49TFContratoOcorrenciaNotificacao_Descricao',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO',pic:'@!',nv:''},{av:'AV50TFContratoOcorrenciaNotificacao_Descricao_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV53TFContratoOcorrenciaNotificacao_Cumprido',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO',pic:'',nv:''},{av:'AV54TFContratoOcorrenciaNotificacao_Cumprido_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_TO',pic:'',nv:''},{av:'AV59TFContratoOcorrenciaNotificacao_Protocolo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO',pic:'',nv:''},{av:'AV60TFContratoOcorrenciaNotificacao_Protocolo_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_SEL',pic:'',nv:''},{av:'AV63TFContratoOcorrenciaNotificacao_Responsavel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL',pic:'ZZZZZ9',nv:0},{av:'AV64TFContratoOcorrenciaNotificacao_Responsavel_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_TO',pic:'ZZZZZ9',nv:0},{av:'AV67TFContratoOcorrenciaNotificacao_Nome',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_NOME',pic:'@!',nv:''},{av:'AV68TFContratoOcorrenciaNotificacao_Nome_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_NOME_SEL',pic:'@!',nv:''},{av:'AV71TFContratoOcorrenciaNotificacao_Docto',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DOCTO',pic:'',nv:''},{av:'AV72TFContratoOcorrenciaNotificacao_Docto_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DOCTO_SEL',pic:'',nv:''},{av:'AV7ContratoOcorrencia_Codigo',fld:'vCONTRATOOCORRENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV43ddo_ContratoOcorrenciaNotificacao_DataTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_ContratoOcorrenciaNotificacao_PrazoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_ContratoOcorrenciaNotificacao_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_ContratoOcorrenciaNotificacao_CumpridoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_ContratoOcorrenciaNotificacao_ProtocoloTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_ContratoOcorrenciaNotificacao_ResponsavelTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVELTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_ContratoOcorrenciaNotificacao_NomeTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_ContratoOcorrenciaNotificacao_DoctoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV82Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV34DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV33DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A297ContratoOcorrenciaNotificacao_Codigo',fld:'CONTRATOOCORRENCIANOTIFICACAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A294ContratoOcorrencia_Codigo',fld:'CONTRATOOCORRENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A303ContratoOcorrenciaNotificacao_Responsavel',fld:'CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL',pic:'ZZZZZ9',hsh:true,nv:0},{av:'sPrefix',nv:''},{av:'Ddo_contratoocorrencianotificacao_nome_Activeeventkey',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME',prop:'ActiveEventKey'},{av:'Ddo_contratoocorrencianotificacao_nome_Filteredtext_get',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME',prop:'FilteredText_get'},{av:'Ddo_contratoocorrencianotificacao_nome_Selectedvalue_get',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME',prop:'SelectedValue_get'}],oparms:[{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratoocorrencianotificacao_nome_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME',prop:'SortedStatus'},{av:'AV67TFContratoOcorrenciaNotificacao_Nome',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_NOME',pic:'@!',nv:''},{av:'AV68TFContratoOcorrenciaNotificacao_Nome_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_contratoocorrencianotificacao_data_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_prazo_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_descricao_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_cumprido_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_protocolo_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_responsavel_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_docto_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO.ONOPTIONCLICKED","{handler:'E197A2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17ContratoOcorrenciaNotificacao_Data1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA1',pic:'',nv:''},{av:'AV18ContratoOcorrenciaNotificacao_Data_To1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO1',pic:'',nv:''},{av:'AV19ContratoOcorrenciaNotificacao_Prazo1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_PRAZO1',pic:'ZZZ9',nv:0},{av:'AV20ContratoOcorrenciaNotificacao_Prazo_To1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_PRAZO_TO1',pic:'ZZZ9',nv:0},{av:'AV22DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23ContratoOcorrenciaNotificacao_Data2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA2',pic:'',nv:''},{av:'AV24ContratoOcorrenciaNotificacao_Data_To2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO2',pic:'',nv:''},{av:'AV25ContratoOcorrenciaNotificacao_Prazo2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_PRAZO2',pic:'ZZZ9',nv:0},{av:'AV26ContratoOcorrenciaNotificacao_Prazo_To2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_PRAZO_TO2',pic:'ZZZ9',nv:0},{av:'AV21DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV39TFContratoOcorrenciaNotificacao_Data',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DATA',pic:'',nv:''},{av:'AV40TFContratoOcorrenciaNotificacao_Data_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DATA_TO',pic:'',nv:''},{av:'AV45TFContratoOcorrenciaNotificacao_Prazo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO',pic:'ZZZ9',nv:0},{av:'AV46TFContratoOcorrenciaNotificacao_Prazo_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO_TO',pic:'ZZZ9',nv:0},{av:'AV49TFContratoOcorrenciaNotificacao_Descricao',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO',pic:'@!',nv:''},{av:'AV50TFContratoOcorrenciaNotificacao_Descricao_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV53TFContratoOcorrenciaNotificacao_Cumprido',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO',pic:'',nv:''},{av:'AV54TFContratoOcorrenciaNotificacao_Cumprido_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_TO',pic:'',nv:''},{av:'AV59TFContratoOcorrenciaNotificacao_Protocolo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO',pic:'',nv:''},{av:'AV60TFContratoOcorrenciaNotificacao_Protocolo_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_SEL',pic:'',nv:''},{av:'AV63TFContratoOcorrenciaNotificacao_Responsavel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL',pic:'ZZZZZ9',nv:0},{av:'AV64TFContratoOcorrenciaNotificacao_Responsavel_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_TO',pic:'ZZZZZ9',nv:0},{av:'AV67TFContratoOcorrenciaNotificacao_Nome',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_NOME',pic:'@!',nv:''},{av:'AV68TFContratoOcorrenciaNotificacao_Nome_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_NOME_SEL',pic:'@!',nv:''},{av:'AV71TFContratoOcorrenciaNotificacao_Docto',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DOCTO',pic:'',nv:''},{av:'AV72TFContratoOcorrenciaNotificacao_Docto_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DOCTO_SEL',pic:'',nv:''},{av:'AV7ContratoOcorrencia_Codigo',fld:'vCONTRATOOCORRENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV43ddo_ContratoOcorrenciaNotificacao_DataTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_ContratoOcorrenciaNotificacao_PrazoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_ContratoOcorrenciaNotificacao_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_ContratoOcorrenciaNotificacao_CumpridoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_ContratoOcorrenciaNotificacao_ProtocoloTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_ContratoOcorrenciaNotificacao_ResponsavelTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVELTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_ContratoOcorrenciaNotificacao_NomeTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_ContratoOcorrenciaNotificacao_DoctoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV82Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV34DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV33DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A297ContratoOcorrenciaNotificacao_Codigo',fld:'CONTRATOOCORRENCIANOTIFICACAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A294ContratoOcorrencia_Codigo',fld:'CONTRATOOCORRENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A303ContratoOcorrenciaNotificacao_Responsavel',fld:'CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL',pic:'ZZZZZ9',hsh:true,nv:0},{av:'sPrefix',nv:''},{av:'Ddo_contratoocorrencianotificacao_docto_Activeeventkey',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO',prop:'ActiveEventKey'},{av:'Ddo_contratoocorrencianotificacao_docto_Filteredtext_get',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO',prop:'FilteredText_get'},{av:'Ddo_contratoocorrencianotificacao_docto_Selectedvalue_get',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO',prop:'SelectedValue_get'}],oparms:[{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratoocorrencianotificacao_docto_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO',prop:'SortedStatus'},{av:'AV71TFContratoOcorrenciaNotificacao_Docto',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DOCTO',pic:'',nv:''},{av:'AV72TFContratoOcorrenciaNotificacao_Docto_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DOCTO_SEL',pic:'',nv:''},{av:'Ddo_contratoocorrencianotificacao_data_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_DATA',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_prazo_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_descricao_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_cumprido_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_protocolo_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_responsavel_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL',prop:'SortedStatus'},{av:'Ddo_contratoocorrencianotificacao_nome_Sortedstatus',ctrl:'DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E297A2',iparms:[{av:'A297ContratoOcorrenciaNotificacao_Codigo',fld:'CONTRATOOCORRENCIANOTIFICACAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A294ContratoOcorrencia_Codigo',fld:'CONTRATOOCORRENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A303ContratoOcorrenciaNotificacao_Responsavel',fld:'CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV35Delete',fld:'vDELETE',pic:'',nv:''},{av:'edtavDelete_Tooltiptext',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'edtavDelete_Link',ctrl:'vDELETE',prop:'Link'},{av:'AV36Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'edtavUpdate_Link',ctrl:'vUPDATE',prop:'Link'},{av:'edtContratoOcorrenciaNotificacao_Data_Link',ctrl:'CONTRATOOCORRENCIANOTIFICACAO_DATA',prop:'Link'},{av:'edtContratoOcorrenciaNotificacao_Nome_Link',ctrl:'CONTRATOOCORRENCIANOTIFICACAO_NOME',prop:'Link'}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E207A2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17ContratoOcorrenciaNotificacao_Data1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA1',pic:'',nv:''},{av:'AV18ContratoOcorrenciaNotificacao_Data_To1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO1',pic:'',nv:''},{av:'AV19ContratoOcorrenciaNotificacao_Prazo1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_PRAZO1',pic:'ZZZ9',nv:0},{av:'AV20ContratoOcorrenciaNotificacao_Prazo_To1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_PRAZO_TO1',pic:'ZZZ9',nv:0},{av:'AV22DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23ContratoOcorrenciaNotificacao_Data2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA2',pic:'',nv:''},{av:'AV24ContratoOcorrenciaNotificacao_Data_To2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO2',pic:'',nv:''},{av:'AV25ContratoOcorrenciaNotificacao_Prazo2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_PRAZO2',pic:'ZZZ9',nv:0},{av:'AV26ContratoOcorrenciaNotificacao_Prazo_To2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_PRAZO_TO2',pic:'ZZZ9',nv:0},{av:'AV21DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV39TFContratoOcorrenciaNotificacao_Data',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DATA',pic:'',nv:''},{av:'AV40TFContratoOcorrenciaNotificacao_Data_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DATA_TO',pic:'',nv:''},{av:'AV45TFContratoOcorrenciaNotificacao_Prazo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO',pic:'ZZZ9',nv:0},{av:'AV46TFContratoOcorrenciaNotificacao_Prazo_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO_TO',pic:'ZZZ9',nv:0},{av:'AV49TFContratoOcorrenciaNotificacao_Descricao',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO',pic:'@!',nv:''},{av:'AV50TFContratoOcorrenciaNotificacao_Descricao_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV53TFContratoOcorrenciaNotificacao_Cumprido',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO',pic:'',nv:''},{av:'AV54TFContratoOcorrenciaNotificacao_Cumprido_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_TO',pic:'',nv:''},{av:'AV59TFContratoOcorrenciaNotificacao_Protocolo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO',pic:'',nv:''},{av:'AV60TFContratoOcorrenciaNotificacao_Protocolo_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_SEL',pic:'',nv:''},{av:'AV63TFContratoOcorrenciaNotificacao_Responsavel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL',pic:'ZZZZZ9',nv:0},{av:'AV64TFContratoOcorrenciaNotificacao_Responsavel_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_TO',pic:'ZZZZZ9',nv:0},{av:'AV67TFContratoOcorrenciaNotificacao_Nome',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_NOME',pic:'@!',nv:''},{av:'AV68TFContratoOcorrenciaNotificacao_Nome_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_NOME_SEL',pic:'@!',nv:''},{av:'AV71TFContratoOcorrenciaNotificacao_Docto',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DOCTO',pic:'',nv:''},{av:'AV72TFContratoOcorrenciaNotificacao_Docto_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DOCTO_SEL',pic:'',nv:''},{av:'AV7ContratoOcorrencia_Codigo',fld:'vCONTRATOOCORRENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV43ddo_ContratoOcorrenciaNotificacao_DataTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_ContratoOcorrenciaNotificacao_PrazoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_ContratoOcorrenciaNotificacao_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_ContratoOcorrenciaNotificacao_CumpridoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_ContratoOcorrenciaNotificacao_ProtocoloTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_ContratoOcorrenciaNotificacao_ResponsavelTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVELTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_ContratoOcorrenciaNotificacao_NomeTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_ContratoOcorrenciaNotificacao_DoctoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV82Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV34DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV33DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A297ContratoOcorrenciaNotificacao_Codigo',fld:'CONTRATOOCORRENCIANOTIFICACAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A294ContratoOcorrencia_Codigo',fld:'CONTRATOOCORRENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A303ContratoOcorrenciaNotificacao_Responsavel',fld:'CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL',pic:'ZZZZZ9',hsh:true,nv:0},{av:'sPrefix',nv:''}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E247A2',iparms:[],oparms:[{av:'AV21DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E217A2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17ContratoOcorrenciaNotificacao_Data1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA1',pic:'',nv:''},{av:'AV18ContratoOcorrenciaNotificacao_Data_To1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO1',pic:'',nv:''},{av:'AV19ContratoOcorrenciaNotificacao_Prazo1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_PRAZO1',pic:'ZZZ9',nv:0},{av:'AV20ContratoOcorrenciaNotificacao_Prazo_To1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_PRAZO_TO1',pic:'ZZZ9',nv:0},{av:'AV22DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23ContratoOcorrenciaNotificacao_Data2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA2',pic:'',nv:''},{av:'AV24ContratoOcorrenciaNotificacao_Data_To2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO2',pic:'',nv:''},{av:'AV25ContratoOcorrenciaNotificacao_Prazo2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_PRAZO2',pic:'ZZZ9',nv:0},{av:'AV26ContratoOcorrenciaNotificacao_Prazo_To2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_PRAZO_TO2',pic:'ZZZ9',nv:0},{av:'AV21DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV39TFContratoOcorrenciaNotificacao_Data',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DATA',pic:'',nv:''},{av:'AV40TFContratoOcorrenciaNotificacao_Data_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DATA_TO',pic:'',nv:''},{av:'AV45TFContratoOcorrenciaNotificacao_Prazo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO',pic:'ZZZ9',nv:0},{av:'AV46TFContratoOcorrenciaNotificacao_Prazo_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO_TO',pic:'ZZZ9',nv:0},{av:'AV49TFContratoOcorrenciaNotificacao_Descricao',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO',pic:'@!',nv:''},{av:'AV50TFContratoOcorrenciaNotificacao_Descricao_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV53TFContratoOcorrenciaNotificacao_Cumprido',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO',pic:'',nv:''},{av:'AV54TFContratoOcorrenciaNotificacao_Cumprido_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_TO',pic:'',nv:''},{av:'AV59TFContratoOcorrenciaNotificacao_Protocolo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO',pic:'',nv:''},{av:'AV60TFContratoOcorrenciaNotificacao_Protocolo_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_SEL',pic:'',nv:''},{av:'AV63TFContratoOcorrenciaNotificacao_Responsavel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL',pic:'ZZZZZ9',nv:0},{av:'AV64TFContratoOcorrenciaNotificacao_Responsavel_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_TO',pic:'ZZZZZ9',nv:0},{av:'AV67TFContratoOcorrenciaNotificacao_Nome',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_NOME',pic:'@!',nv:''},{av:'AV68TFContratoOcorrenciaNotificacao_Nome_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_NOME_SEL',pic:'@!',nv:''},{av:'AV71TFContratoOcorrenciaNotificacao_Docto',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DOCTO',pic:'',nv:''},{av:'AV72TFContratoOcorrenciaNotificacao_Docto_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DOCTO_SEL',pic:'',nv:''},{av:'AV7ContratoOcorrencia_Codigo',fld:'vCONTRATOOCORRENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV43ddo_ContratoOcorrenciaNotificacao_DataTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_ContratoOcorrenciaNotificacao_PrazoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_ContratoOcorrenciaNotificacao_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_ContratoOcorrenciaNotificacao_CumpridoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_ContratoOcorrenciaNotificacao_ProtocoloTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_ContratoOcorrenciaNotificacao_ResponsavelTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVELTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_ContratoOcorrenciaNotificacao_NomeTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_ContratoOcorrenciaNotificacao_DoctoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV82Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV34DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV33DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A297ContratoOcorrenciaNotificacao_Codigo',fld:'CONTRATOOCORRENCIANOTIFICACAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A294ContratoOcorrencia_Codigo',fld:'CONTRATOOCORRENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A303ContratoOcorrenciaNotificacao_Responsavel',fld:'CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL',pic:'ZZZZZ9',hsh:true,nv:0},{av:'sPrefix',nv:''}],oparms:[{av:'AV33DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV34DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV21DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23ContratoOcorrenciaNotificacao_Data2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA2',pic:'',nv:''},{av:'AV24ContratoOcorrenciaNotificacao_Data_To2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO2',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17ContratoOcorrenciaNotificacao_Data1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA1',pic:'',nv:''},{av:'AV18ContratoOcorrenciaNotificacao_Data_To1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO1',pic:'',nv:''},{av:'AV19ContratoOcorrenciaNotificacao_Prazo1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_PRAZO1',pic:'ZZZ9',nv:0},{av:'AV20ContratoOcorrenciaNotificacao_Prazo_To1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_PRAZO_TO1',pic:'ZZZ9',nv:0},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV25ContratoOcorrenciaNotificacao_Prazo2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_PRAZO2',pic:'ZZZ9',nv:0},{av:'AV26ContratoOcorrenciaNotificacao_Prazo_To2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_PRAZO_TO2',pic:'ZZZ9',nv:0},{av:'tblTablemergeddynamicfilterscontratoocorrencianotificacao_data2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOOCORRENCIANOTIFICACAO_DATA2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratoocorrencianotificacao_prazo2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOOCORRENCIANOTIFICACAO_PRAZO2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratoocorrencianotificacao_data1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOOCORRENCIANOTIFICACAO_DATA1',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratoocorrencianotificacao_prazo1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOOCORRENCIANOTIFICACAO_PRAZO1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E257A2',iparms:[{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'tblTablemergeddynamicfilterscontratoocorrencianotificacao_data1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOOCORRENCIANOTIFICACAO_DATA1',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratoocorrencianotificacao_prazo1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOOCORRENCIANOTIFICACAO_PRAZO1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E227A2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17ContratoOcorrenciaNotificacao_Data1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA1',pic:'',nv:''},{av:'AV18ContratoOcorrenciaNotificacao_Data_To1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO1',pic:'',nv:''},{av:'AV19ContratoOcorrenciaNotificacao_Prazo1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_PRAZO1',pic:'ZZZ9',nv:0},{av:'AV20ContratoOcorrenciaNotificacao_Prazo_To1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_PRAZO_TO1',pic:'ZZZ9',nv:0},{av:'AV22DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23ContratoOcorrenciaNotificacao_Data2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA2',pic:'',nv:''},{av:'AV24ContratoOcorrenciaNotificacao_Data_To2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO2',pic:'',nv:''},{av:'AV25ContratoOcorrenciaNotificacao_Prazo2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_PRAZO2',pic:'ZZZ9',nv:0},{av:'AV26ContratoOcorrenciaNotificacao_Prazo_To2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_PRAZO_TO2',pic:'ZZZ9',nv:0},{av:'AV21DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV39TFContratoOcorrenciaNotificacao_Data',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DATA',pic:'',nv:''},{av:'AV40TFContratoOcorrenciaNotificacao_Data_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DATA_TO',pic:'',nv:''},{av:'AV45TFContratoOcorrenciaNotificacao_Prazo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO',pic:'ZZZ9',nv:0},{av:'AV46TFContratoOcorrenciaNotificacao_Prazo_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PRAZO_TO',pic:'ZZZ9',nv:0},{av:'AV49TFContratoOcorrenciaNotificacao_Descricao',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO',pic:'@!',nv:''},{av:'AV50TFContratoOcorrenciaNotificacao_Descricao_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV53TFContratoOcorrenciaNotificacao_Cumprido',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO',pic:'',nv:''},{av:'AV54TFContratoOcorrenciaNotificacao_Cumprido_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO_TO',pic:'',nv:''},{av:'AV59TFContratoOcorrenciaNotificacao_Protocolo',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO',pic:'',nv:''},{av:'AV60TFContratoOcorrenciaNotificacao_Protocolo_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_SEL',pic:'',nv:''},{av:'AV63TFContratoOcorrenciaNotificacao_Responsavel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL',pic:'ZZZZZ9',nv:0},{av:'AV64TFContratoOcorrenciaNotificacao_Responsavel_To',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL_TO',pic:'ZZZZZ9',nv:0},{av:'AV67TFContratoOcorrenciaNotificacao_Nome',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_NOME',pic:'@!',nv:''},{av:'AV68TFContratoOcorrenciaNotificacao_Nome_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_NOME_SEL',pic:'@!',nv:''},{av:'AV71TFContratoOcorrenciaNotificacao_Docto',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DOCTO',pic:'',nv:''},{av:'AV72TFContratoOcorrenciaNotificacao_Docto_Sel',fld:'vTFCONTRATOOCORRENCIANOTIFICACAO_DOCTO_SEL',pic:'',nv:''},{av:'AV7ContratoOcorrencia_Codigo',fld:'vCONTRATOOCORRENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV43ddo_ContratoOcorrenciaNotificacao_DataTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_ContratoOcorrenciaNotificacao_PrazoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_ContratoOcorrenciaNotificacao_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_ContratoOcorrenciaNotificacao_CumpridoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_CUMPRIDOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_ContratoOcorrenciaNotificacao_ProtocoloTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_ContratoOcorrenciaNotificacao_ResponsavelTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVELTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_ContratoOcorrenciaNotificacao_NomeTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV73ddo_ContratoOcorrenciaNotificacao_DoctoTitleControlIdToReplace',fld:'vDDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV82Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV34DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV33DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A297ContratoOcorrenciaNotificacao_Codigo',fld:'CONTRATOOCORRENCIANOTIFICACAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A294ContratoOcorrencia_Codigo',fld:'CONTRATOOCORRENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A303ContratoOcorrenciaNotificacao_Responsavel',fld:'CONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL',pic:'ZZZZZ9',hsh:true,nv:0},{av:'sPrefix',nv:''}],oparms:[{av:'AV33DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV21DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV22DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23ContratoOcorrenciaNotificacao_Data2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA2',pic:'',nv:''},{av:'AV24ContratoOcorrenciaNotificacao_Data_To2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO2',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17ContratoOcorrenciaNotificacao_Data1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA1',pic:'',nv:''},{av:'AV18ContratoOcorrenciaNotificacao_Data_To1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_DATA_TO1',pic:'',nv:''},{av:'AV19ContratoOcorrenciaNotificacao_Prazo1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_PRAZO1',pic:'ZZZ9',nv:0},{av:'AV20ContratoOcorrenciaNotificacao_Prazo_To1',fld:'vCONTRATOOCORRENCIANOTIFICACAO_PRAZO_TO1',pic:'ZZZ9',nv:0},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV25ContratoOcorrenciaNotificacao_Prazo2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_PRAZO2',pic:'ZZZ9',nv:0},{av:'AV26ContratoOcorrenciaNotificacao_Prazo_To2',fld:'vCONTRATOOCORRENCIANOTIFICACAO_PRAZO_TO2',pic:'ZZZ9',nv:0},{av:'tblTablemergeddynamicfilterscontratoocorrencianotificacao_data2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOOCORRENCIANOTIFICACAO_DATA2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratoocorrencianotificacao_prazo2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOOCORRENCIANOTIFICACAO_PRAZO2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratoocorrencianotificacao_data1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOOCORRENCIANOTIFICACAO_DATA1',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratoocorrencianotificacao_prazo1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOOCORRENCIANOTIFICACAO_PRAZO1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E267A2',iparms:[{av:'AV22DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'tblTablemergeddynamicfilterscontratoocorrencianotificacao_data2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOOCORRENCIANOTIFICACAO_DATA2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratoocorrencianotificacao_prazo2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOOCORRENCIANOTIFICACAO_PRAZO2',prop:'Visible'}]}");
         setEventMetadata("'DOINSERT'","{handler:'E237A2',iparms:[{av:'AV7ContratoOcorrencia_Codigo',fld:'vCONTRATOOCORRENCIA_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV7ContratoOcorrencia_Codigo',fld:'vCONTRATOOCORRENCIA_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_contratoocorrencianotificacao_data_Activeeventkey = "";
         Ddo_contratoocorrencianotificacao_data_Filteredtext_get = "";
         Ddo_contratoocorrencianotificacao_data_Filteredtextto_get = "";
         Ddo_contratoocorrencianotificacao_prazo_Activeeventkey = "";
         Ddo_contratoocorrencianotificacao_prazo_Filteredtext_get = "";
         Ddo_contratoocorrencianotificacao_prazo_Filteredtextto_get = "";
         Ddo_contratoocorrencianotificacao_descricao_Activeeventkey = "";
         Ddo_contratoocorrencianotificacao_descricao_Filteredtext_get = "";
         Ddo_contratoocorrencianotificacao_descricao_Selectedvalue_get = "";
         Ddo_contratoocorrencianotificacao_cumprido_Activeeventkey = "";
         Ddo_contratoocorrencianotificacao_cumprido_Filteredtext_get = "";
         Ddo_contratoocorrencianotificacao_cumprido_Filteredtextto_get = "";
         Ddo_contratoocorrencianotificacao_protocolo_Activeeventkey = "";
         Ddo_contratoocorrencianotificacao_protocolo_Filteredtext_get = "";
         Ddo_contratoocorrencianotificacao_protocolo_Selectedvalue_get = "";
         Ddo_contratoocorrencianotificacao_responsavel_Activeeventkey = "";
         Ddo_contratoocorrencianotificacao_responsavel_Filteredtext_get = "";
         Ddo_contratoocorrencianotificacao_responsavel_Filteredtextto_get = "";
         Ddo_contratoocorrencianotificacao_nome_Activeeventkey = "";
         Ddo_contratoocorrencianotificacao_nome_Filteredtext_get = "";
         Ddo_contratoocorrencianotificacao_nome_Selectedvalue_get = "";
         Ddo_contratoocorrencianotificacao_docto_Activeeventkey = "";
         Ddo_contratoocorrencianotificacao_docto_Filteredtext_get = "";
         Ddo_contratoocorrencianotificacao_docto_Selectedvalue_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV16DynamicFiltersSelector1 = "";
         AV17ContratoOcorrenciaNotificacao_Data1 = DateTime.MinValue;
         AV18ContratoOcorrenciaNotificacao_Data_To1 = DateTime.MinValue;
         AV22DynamicFiltersSelector2 = "";
         AV23ContratoOcorrenciaNotificacao_Data2 = DateTime.MinValue;
         AV24ContratoOcorrenciaNotificacao_Data_To2 = DateTime.MinValue;
         AV39TFContratoOcorrenciaNotificacao_Data = DateTime.MinValue;
         AV40TFContratoOcorrenciaNotificacao_Data_To = DateTime.MinValue;
         AV49TFContratoOcorrenciaNotificacao_Descricao = "";
         AV50TFContratoOcorrenciaNotificacao_Descricao_Sel = "";
         AV53TFContratoOcorrenciaNotificacao_Cumprido = DateTime.MinValue;
         AV54TFContratoOcorrenciaNotificacao_Cumprido_To = DateTime.MinValue;
         AV59TFContratoOcorrenciaNotificacao_Protocolo = "";
         AV60TFContratoOcorrenciaNotificacao_Protocolo_Sel = "";
         AV67TFContratoOcorrenciaNotificacao_Nome = "";
         AV68TFContratoOcorrenciaNotificacao_Nome_Sel = "";
         AV71TFContratoOcorrenciaNotificacao_Docto = "";
         AV72TFContratoOcorrenciaNotificacao_Docto_Sel = "";
         AV43ddo_ContratoOcorrenciaNotificacao_DataTitleControlIdToReplace = "";
         AV47ddo_ContratoOcorrenciaNotificacao_PrazoTitleControlIdToReplace = "";
         AV51ddo_ContratoOcorrenciaNotificacao_DescricaoTitleControlIdToReplace = "";
         AV57ddo_ContratoOcorrenciaNotificacao_CumpridoTitleControlIdToReplace = "";
         AV61ddo_ContratoOcorrenciaNotificacao_ProtocoloTitleControlIdToReplace = "";
         AV65ddo_ContratoOcorrenciaNotificacao_ResponsavelTitleControlIdToReplace = "";
         AV69ddo_ContratoOcorrenciaNotificacao_NomeTitleControlIdToReplace = "";
         AV73ddo_ContratoOcorrenciaNotificacao_DoctoTitleControlIdToReplace = "";
         AV82Pgmname = "";
         AV11GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GXKey = "";
         forbiddenHiddens = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV74DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV38ContratoOcorrenciaNotificacao_DataTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV44ContratoOcorrenciaNotificacao_PrazoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV48ContratoOcorrenciaNotificacao_DescricaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV52ContratoOcorrenciaNotificacao_CumpridoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV58ContratoOcorrenciaNotificacao_ProtocoloTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV62ContratoOcorrenciaNotificacao_ResponsavelTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV66ContratoOcorrenciaNotificacao_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV70ContratoOcorrenciaNotificacao_DoctoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_contratoocorrencianotificacao_data_Filteredtext_set = "";
         Ddo_contratoocorrencianotificacao_data_Filteredtextto_set = "";
         Ddo_contratoocorrencianotificacao_data_Sortedstatus = "";
         Ddo_contratoocorrencianotificacao_prazo_Filteredtext_set = "";
         Ddo_contratoocorrencianotificacao_prazo_Filteredtextto_set = "";
         Ddo_contratoocorrencianotificacao_prazo_Sortedstatus = "";
         Ddo_contratoocorrencianotificacao_descricao_Filteredtext_set = "";
         Ddo_contratoocorrencianotificacao_descricao_Selectedvalue_set = "";
         Ddo_contratoocorrencianotificacao_descricao_Sortedstatus = "";
         Ddo_contratoocorrencianotificacao_cumprido_Filteredtext_set = "";
         Ddo_contratoocorrencianotificacao_cumprido_Filteredtextto_set = "";
         Ddo_contratoocorrencianotificacao_cumprido_Sortedstatus = "";
         Ddo_contratoocorrencianotificacao_protocolo_Filteredtext_set = "";
         Ddo_contratoocorrencianotificacao_protocolo_Selectedvalue_set = "";
         Ddo_contratoocorrencianotificacao_protocolo_Sortedstatus = "";
         Ddo_contratoocorrencianotificacao_responsavel_Filteredtext_set = "";
         Ddo_contratoocorrencianotificacao_responsavel_Filteredtextto_set = "";
         Ddo_contratoocorrencianotificacao_responsavel_Sortedstatus = "";
         Ddo_contratoocorrencianotificacao_nome_Filteredtext_set = "";
         Ddo_contratoocorrencianotificacao_nome_Selectedvalue_set = "";
         Ddo_contratoocorrencianotificacao_nome_Sortedstatus = "";
         Ddo_contratoocorrencianotificacao_docto_Filteredtext_set = "";
         Ddo_contratoocorrencianotificacao_docto_Selectedvalue_set = "";
         Ddo_contratoocorrencianotificacao_docto_Sortedstatus = "";
         GX_FocusControl = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         AV41DDO_ContratoOcorrenciaNotificacao_DataAuxDate = DateTime.MinValue;
         AV42DDO_ContratoOcorrenciaNotificacao_DataAuxDateTo = DateTime.MinValue;
         AV55DDO_ContratoOcorrenciaNotificacao_CumpridoAuxDate = DateTime.MinValue;
         AV56DDO_ContratoOcorrenciaNotificacao_CumpridoAuxDateTo = DateTime.MinValue;
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV35Delete = "";
         AV80Delete_GXI = "";
         AV36Update = "";
         AV81Update_GXI = "";
         A298ContratoOcorrenciaNotificacao_Data = DateTime.MinValue;
         A300ContratoOcorrenciaNotificacao_Descricao = "";
         A301ContratoOcorrenciaNotificacao_Cumprido = DateTime.MinValue;
         A302ContratoOcorrenciaNotificacao_Protocolo = "";
         A304ContratoOcorrenciaNotificacao_Nome = "";
         A306ContratoOcorrenciaNotificacao_Docto = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV49TFContratoOcorrenciaNotificacao_Descricao = "";
         lV59TFContratoOcorrenciaNotificacao_Protocolo = "";
         lV67TFContratoOcorrenciaNotificacao_Nome = "";
         lV71TFContratoOcorrenciaNotificacao_Docto = "";
         H007A2_A295ContratoOcorrencia_Data = new DateTime[] {DateTime.MinValue} ;
         H007A2_A297ContratoOcorrenciaNotificacao_Codigo = new int[1] ;
         H007A2_A294ContratoOcorrencia_Codigo = new int[1] ;
         H007A2_A306ContratoOcorrenciaNotificacao_Docto = new String[] {""} ;
         H007A2_n306ContratoOcorrenciaNotificacao_Docto = new bool[] {false} ;
         H007A2_A304ContratoOcorrenciaNotificacao_Nome = new String[] {""} ;
         H007A2_n304ContratoOcorrenciaNotificacao_Nome = new bool[] {false} ;
         H007A2_A303ContratoOcorrenciaNotificacao_Responsavel = new int[1] ;
         H007A2_A302ContratoOcorrenciaNotificacao_Protocolo = new String[] {""} ;
         H007A2_n302ContratoOcorrenciaNotificacao_Protocolo = new bool[] {false} ;
         H007A2_A301ContratoOcorrenciaNotificacao_Cumprido = new DateTime[] {DateTime.MinValue} ;
         H007A2_n301ContratoOcorrenciaNotificacao_Cumprido = new bool[] {false} ;
         H007A2_A300ContratoOcorrenciaNotificacao_Descricao = new String[] {""} ;
         H007A2_A299ContratoOcorrenciaNotificacao_Prazo = new short[1] ;
         H007A2_A298ContratoOcorrenciaNotificacao_Data = new DateTime[] {DateTime.MinValue} ;
         A295ContratoOcorrencia_Data = DateTime.MinValue;
         H007A3_AGRID_nRecordCount = new long[1] ;
         hsh = "";
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GridRow = new GXWebRow();
         AV37Session = context.GetSession();
         AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8HTTPRequest = new GxHttpRequest( context);
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblOrderedtext_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfilterscontratoocorrencianotificacao_prazo_rangemiddletext2_Jsonclick = "";
         lblDynamicfilterscontratoocorrencianotificacao_data_rangemiddletext2_Jsonclick = "";
         lblDynamicfilterscontratoocorrencianotificacao_prazo_rangemiddletext1_Jsonclick = "";
         lblDynamicfilterscontratoocorrencianotificacao_data_rangemiddletext1_Jsonclick = "";
         imgInsert_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlAV7ContratoOcorrencia_Codigo = "";
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contratoocorrenciacontratoocorrencianotificacaowc__default(),
            new Object[][] {
                new Object[] {
               H007A2_A295ContratoOcorrencia_Data, H007A2_A297ContratoOcorrenciaNotificacao_Codigo, H007A2_A294ContratoOcorrencia_Codigo, H007A2_A306ContratoOcorrenciaNotificacao_Docto, H007A2_n306ContratoOcorrenciaNotificacao_Docto, H007A2_A304ContratoOcorrenciaNotificacao_Nome, H007A2_n304ContratoOcorrenciaNotificacao_Nome, H007A2_A303ContratoOcorrenciaNotificacao_Responsavel, H007A2_A302ContratoOcorrenciaNotificacao_Protocolo, H007A2_n302ContratoOcorrenciaNotificacao_Protocolo,
               H007A2_A301ContratoOcorrenciaNotificacao_Cumprido, H007A2_n301ContratoOcorrenciaNotificacao_Cumprido, H007A2_A300ContratoOcorrenciaNotificacao_Descricao, H007A2_A299ContratoOcorrenciaNotificacao_Prazo, H007A2_A298ContratoOcorrenciaNotificacao_Data
               }
               , new Object[] {
               H007A3_AGRID_nRecordCount
               }
            }
         );
         AV82Pgmname = "ContratoOcorrenciaContratoOcorrenciaNotificacaoWC";
         /* GeneXus formulas. */
         AV82Pgmname = "ContratoOcorrenciaContratoOcorrenciaNotificacaoWC";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short nRC_GXsfl_84 ;
      private short nGXsfl_84_idx=1 ;
      private short AV14OrderedBy ;
      private short AV19ContratoOcorrenciaNotificacao_Prazo1 ;
      private short AV20ContratoOcorrenciaNotificacao_Prazo_To1 ;
      private short AV25ContratoOcorrenciaNotificacao_Prazo2 ;
      private short AV26ContratoOcorrenciaNotificacao_Prazo_To2 ;
      private short AV45TFContratoOcorrenciaNotificacao_Prazo ;
      private short AV46TFContratoOcorrenciaNotificacao_Prazo_To ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short A299ContratoOcorrenciaNotificacao_Prazo ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_84_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtContratoOcorrenciaNotificacao_Data_Titleformat ;
      private short edtContratoOcorrenciaNotificacao_Prazo_Titleformat ;
      private short edtContratoOcorrenciaNotificacao_Descricao_Titleformat ;
      private short edtContratoOcorrenciaNotificacao_Cumprido_Titleformat ;
      private short edtContratoOcorrenciaNotificacao_Protocolo_Titleformat ;
      private short edtContratoOcorrenciaNotificacao_Responsavel_Titleformat ;
      private short edtContratoOcorrenciaNotificacao_Nome_Titleformat ;
      private short edtContratoOcorrenciaNotificacao_Docto_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV7ContratoOcorrencia_Codigo ;
      private int wcpOAV7ContratoOcorrencia_Codigo ;
      private int subGrid_Rows ;
      private int AV63TFContratoOcorrenciaNotificacao_Responsavel ;
      private int AV64TFContratoOcorrenciaNotificacao_Responsavel_To ;
      private int A297ContratoOcorrenciaNotificacao_Codigo ;
      private int A294ContratoOcorrencia_Codigo ;
      private int A303ContratoOcorrenciaNotificacao_Responsavel ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_contratoocorrencianotificacao_descricao_Datalistupdateminimumcharacters ;
      private int Ddo_contratoocorrencianotificacao_protocolo_Datalistupdateminimumcharacters ;
      private int Ddo_contratoocorrencianotificacao_nome_Datalistupdateminimumcharacters ;
      private int Ddo_contratoocorrencianotificacao_docto_Datalistupdateminimumcharacters ;
      private int edtContratoOcorrencia_Codigo_Visible ;
      private int edtavTfcontratoocorrencianotificacao_data_Visible ;
      private int edtavTfcontratoocorrencianotificacao_data_to_Visible ;
      private int edtavTfcontratoocorrencianotificacao_prazo_Visible ;
      private int edtavTfcontratoocorrencianotificacao_prazo_to_Visible ;
      private int edtavTfcontratoocorrencianotificacao_descricao_Visible ;
      private int edtavTfcontratoocorrencianotificacao_descricao_sel_Visible ;
      private int edtavTfcontratoocorrencianotificacao_cumprido_Visible ;
      private int edtavTfcontratoocorrencianotificacao_cumprido_to_Visible ;
      private int edtavTfcontratoocorrencianotificacao_protocolo_Visible ;
      private int edtavTfcontratoocorrencianotificacao_protocolo_sel_Visible ;
      private int edtavTfcontratoocorrencianotificacao_responsavel_Visible ;
      private int edtavTfcontratoocorrencianotificacao_responsavel_to_Visible ;
      private int edtavTfcontratoocorrencianotificacao_nome_Visible ;
      private int edtavTfcontratoocorrencianotificacao_nome_sel_Visible ;
      private int edtavTfcontratoocorrencianotificacao_docto_Visible ;
      private int edtavTfcontratoocorrencianotificacao_docto_sel_Visible ;
      private int edtavDdo_contratoocorrencianotificacao_datatitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratoocorrencianotificacao_prazotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratoocorrencianotificacao_descricaotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratoocorrencianotificacao_cumpridotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratoocorrencianotificacao_protocolotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratoocorrencianotificacao_responsaveltitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratoocorrencianotificacao_nometitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratoocorrencianotificacao_doctotitlecontrolidtoreplace_Visible ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int edtavOrdereddsc_Visible ;
      private int AV75PageToGo ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int tblTablemergeddynamicfilterscontratoocorrencianotificacao_data1_Visible ;
      private int tblTablemergeddynamicfilterscontratoocorrencianotificacao_prazo1_Visible ;
      private int tblTablemergeddynamicfilterscontratoocorrencianotificacao_data2_Visible ;
      private int tblTablemergeddynamicfilterscontratoocorrencianotificacao_prazo2_Visible ;
      private int AV83GXV1 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long AV76GridCurrentPage ;
      private long AV77GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_contratoocorrencianotificacao_data_Activeeventkey ;
      private String Ddo_contratoocorrencianotificacao_data_Filteredtext_get ;
      private String Ddo_contratoocorrencianotificacao_data_Filteredtextto_get ;
      private String Ddo_contratoocorrencianotificacao_prazo_Activeeventkey ;
      private String Ddo_contratoocorrencianotificacao_prazo_Filteredtext_get ;
      private String Ddo_contratoocorrencianotificacao_prazo_Filteredtextto_get ;
      private String Ddo_contratoocorrencianotificacao_descricao_Activeeventkey ;
      private String Ddo_contratoocorrencianotificacao_descricao_Filteredtext_get ;
      private String Ddo_contratoocorrencianotificacao_descricao_Selectedvalue_get ;
      private String Ddo_contratoocorrencianotificacao_cumprido_Activeeventkey ;
      private String Ddo_contratoocorrencianotificacao_cumprido_Filteredtext_get ;
      private String Ddo_contratoocorrencianotificacao_cumprido_Filteredtextto_get ;
      private String Ddo_contratoocorrencianotificacao_protocolo_Activeeventkey ;
      private String Ddo_contratoocorrencianotificacao_protocolo_Filteredtext_get ;
      private String Ddo_contratoocorrencianotificacao_protocolo_Selectedvalue_get ;
      private String Ddo_contratoocorrencianotificacao_responsavel_Activeeventkey ;
      private String Ddo_contratoocorrencianotificacao_responsavel_Filteredtext_get ;
      private String Ddo_contratoocorrencianotificacao_responsavel_Filteredtextto_get ;
      private String Ddo_contratoocorrencianotificacao_nome_Activeeventkey ;
      private String Ddo_contratoocorrencianotificacao_nome_Filteredtext_get ;
      private String Ddo_contratoocorrencianotificacao_nome_Selectedvalue_get ;
      private String Ddo_contratoocorrencianotificacao_docto_Activeeventkey ;
      private String Ddo_contratoocorrencianotificacao_docto_Filteredtext_get ;
      private String Ddo_contratoocorrencianotificacao_docto_Selectedvalue_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String sGXsfl_84_idx="0001" ;
      private String AV67TFContratoOcorrenciaNotificacao_Nome ;
      private String AV68TFContratoOcorrenciaNotificacao_Nome_Sel ;
      private String AV82Pgmname ;
      private String GXKey ;
      private String forbiddenHiddens ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_contratoocorrencianotificacao_data_Caption ;
      private String Ddo_contratoocorrencianotificacao_data_Tooltip ;
      private String Ddo_contratoocorrencianotificacao_data_Cls ;
      private String Ddo_contratoocorrencianotificacao_data_Filteredtext_set ;
      private String Ddo_contratoocorrencianotificacao_data_Filteredtextto_set ;
      private String Ddo_contratoocorrencianotificacao_data_Dropdownoptionstype ;
      private String Ddo_contratoocorrencianotificacao_data_Titlecontrolidtoreplace ;
      private String Ddo_contratoocorrencianotificacao_data_Sortedstatus ;
      private String Ddo_contratoocorrencianotificacao_data_Filtertype ;
      private String Ddo_contratoocorrencianotificacao_data_Sortasc ;
      private String Ddo_contratoocorrencianotificacao_data_Sortdsc ;
      private String Ddo_contratoocorrencianotificacao_data_Cleanfilter ;
      private String Ddo_contratoocorrencianotificacao_data_Rangefilterfrom ;
      private String Ddo_contratoocorrencianotificacao_data_Rangefilterto ;
      private String Ddo_contratoocorrencianotificacao_data_Searchbuttontext ;
      private String Ddo_contratoocorrencianotificacao_prazo_Caption ;
      private String Ddo_contratoocorrencianotificacao_prazo_Tooltip ;
      private String Ddo_contratoocorrencianotificacao_prazo_Cls ;
      private String Ddo_contratoocorrencianotificacao_prazo_Filteredtext_set ;
      private String Ddo_contratoocorrencianotificacao_prazo_Filteredtextto_set ;
      private String Ddo_contratoocorrencianotificacao_prazo_Dropdownoptionstype ;
      private String Ddo_contratoocorrencianotificacao_prazo_Titlecontrolidtoreplace ;
      private String Ddo_contratoocorrencianotificacao_prazo_Sortedstatus ;
      private String Ddo_contratoocorrencianotificacao_prazo_Filtertype ;
      private String Ddo_contratoocorrencianotificacao_prazo_Sortasc ;
      private String Ddo_contratoocorrencianotificacao_prazo_Sortdsc ;
      private String Ddo_contratoocorrencianotificacao_prazo_Cleanfilter ;
      private String Ddo_contratoocorrencianotificacao_prazo_Rangefilterfrom ;
      private String Ddo_contratoocorrencianotificacao_prazo_Rangefilterto ;
      private String Ddo_contratoocorrencianotificacao_prazo_Searchbuttontext ;
      private String Ddo_contratoocorrencianotificacao_descricao_Caption ;
      private String Ddo_contratoocorrencianotificacao_descricao_Tooltip ;
      private String Ddo_contratoocorrencianotificacao_descricao_Cls ;
      private String Ddo_contratoocorrencianotificacao_descricao_Filteredtext_set ;
      private String Ddo_contratoocorrencianotificacao_descricao_Selectedvalue_set ;
      private String Ddo_contratoocorrencianotificacao_descricao_Dropdownoptionstype ;
      private String Ddo_contratoocorrencianotificacao_descricao_Titlecontrolidtoreplace ;
      private String Ddo_contratoocorrencianotificacao_descricao_Sortedstatus ;
      private String Ddo_contratoocorrencianotificacao_descricao_Filtertype ;
      private String Ddo_contratoocorrencianotificacao_descricao_Datalisttype ;
      private String Ddo_contratoocorrencianotificacao_descricao_Datalistproc ;
      private String Ddo_contratoocorrencianotificacao_descricao_Sortasc ;
      private String Ddo_contratoocorrencianotificacao_descricao_Sortdsc ;
      private String Ddo_contratoocorrencianotificacao_descricao_Loadingdata ;
      private String Ddo_contratoocorrencianotificacao_descricao_Cleanfilter ;
      private String Ddo_contratoocorrencianotificacao_descricao_Noresultsfound ;
      private String Ddo_contratoocorrencianotificacao_descricao_Searchbuttontext ;
      private String Ddo_contratoocorrencianotificacao_cumprido_Caption ;
      private String Ddo_contratoocorrencianotificacao_cumprido_Tooltip ;
      private String Ddo_contratoocorrencianotificacao_cumprido_Cls ;
      private String Ddo_contratoocorrencianotificacao_cumprido_Filteredtext_set ;
      private String Ddo_contratoocorrencianotificacao_cumprido_Filteredtextto_set ;
      private String Ddo_contratoocorrencianotificacao_cumprido_Dropdownoptionstype ;
      private String Ddo_contratoocorrencianotificacao_cumprido_Titlecontrolidtoreplace ;
      private String Ddo_contratoocorrencianotificacao_cumprido_Sortedstatus ;
      private String Ddo_contratoocorrencianotificacao_cumprido_Filtertype ;
      private String Ddo_contratoocorrencianotificacao_cumprido_Sortasc ;
      private String Ddo_contratoocorrencianotificacao_cumprido_Sortdsc ;
      private String Ddo_contratoocorrencianotificacao_cumprido_Cleanfilter ;
      private String Ddo_contratoocorrencianotificacao_cumprido_Rangefilterfrom ;
      private String Ddo_contratoocorrencianotificacao_cumprido_Rangefilterto ;
      private String Ddo_contratoocorrencianotificacao_cumprido_Searchbuttontext ;
      private String Ddo_contratoocorrencianotificacao_protocolo_Caption ;
      private String Ddo_contratoocorrencianotificacao_protocolo_Tooltip ;
      private String Ddo_contratoocorrencianotificacao_protocolo_Cls ;
      private String Ddo_contratoocorrencianotificacao_protocolo_Filteredtext_set ;
      private String Ddo_contratoocorrencianotificacao_protocolo_Selectedvalue_set ;
      private String Ddo_contratoocorrencianotificacao_protocolo_Dropdownoptionstype ;
      private String Ddo_contratoocorrencianotificacao_protocolo_Titlecontrolidtoreplace ;
      private String Ddo_contratoocorrencianotificacao_protocolo_Sortedstatus ;
      private String Ddo_contratoocorrencianotificacao_protocolo_Filtertype ;
      private String Ddo_contratoocorrencianotificacao_protocolo_Datalisttype ;
      private String Ddo_contratoocorrencianotificacao_protocolo_Datalistproc ;
      private String Ddo_contratoocorrencianotificacao_protocolo_Sortasc ;
      private String Ddo_contratoocorrencianotificacao_protocolo_Sortdsc ;
      private String Ddo_contratoocorrencianotificacao_protocolo_Loadingdata ;
      private String Ddo_contratoocorrencianotificacao_protocolo_Cleanfilter ;
      private String Ddo_contratoocorrencianotificacao_protocolo_Noresultsfound ;
      private String Ddo_contratoocorrencianotificacao_protocolo_Searchbuttontext ;
      private String Ddo_contratoocorrencianotificacao_responsavel_Caption ;
      private String Ddo_contratoocorrencianotificacao_responsavel_Tooltip ;
      private String Ddo_contratoocorrencianotificacao_responsavel_Cls ;
      private String Ddo_contratoocorrencianotificacao_responsavel_Filteredtext_set ;
      private String Ddo_contratoocorrencianotificacao_responsavel_Filteredtextto_set ;
      private String Ddo_contratoocorrencianotificacao_responsavel_Dropdownoptionstype ;
      private String Ddo_contratoocorrencianotificacao_responsavel_Titlecontrolidtoreplace ;
      private String Ddo_contratoocorrencianotificacao_responsavel_Sortedstatus ;
      private String Ddo_contratoocorrencianotificacao_responsavel_Filtertype ;
      private String Ddo_contratoocorrencianotificacao_responsavel_Sortasc ;
      private String Ddo_contratoocorrencianotificacao_responsavel_Sortdsc ;
      private String Ddo_contratoocorrencianotificacao_responsavel_Cleanfilter ;
      private String Ddo_contratoocorrencianotificacao_responsavel_Rangefilterfrom ;
      private String Ddo_contratoocorrencianotificacao_responsavel_Rangefilterto ;
      private String Ddo_contratoocorrencianotificacao_responsavel_Searchbuttontext ;
      private String Ddo_contratoocorrencianotificacao_nome_Caption ;
      private String Ddo_contratoocorrencianotificacao_nome_Tooltip ;
      private String Ddo_contratoocorrencianotificacao_nome_Cls ;
      private String Ddo_contratoocorrencianotificacao_nome_Filteredtext_set ;
      private String Ddo_contratoocorrencianotificacao_nome_Selectedvalue_set ;
      private String Ddo_contratoocorrencianotificacao_nome_Dropdownoptionstype ;
      private String Ddo_contratoocorrencianotificacao_nome_Titlecontrolidtoreplace ;
      private String Ddo_contratoocorrencianotificacao_nome_Sortedstatus ;
      private String Ddo_contratoocorrencianotificacao_nome_Filtertype ;
      private String Ddo_contratoocorrencianotificacao_nome_Datalisttype ;
      private String Ddo_contratoocorrencianotificacao_nome_Datalistproc ;
      private String Ddo_contratoocorrencianotificacao_nome_Sortasc ;
      private String Ddo_contratoocorrencianotificacao_nome_Sortdsc ;
      private String Ddo_contratoocorrencianotificacao_nome_Loadingdata ;
      private String Ddo_contratoocorrencianotificacao_nome_Cleanfilter ;
      private String Ddo_contratoocorrencianotificacao_nome_Noresultsfound ;
      private String Ddo_contratoocorrencianotificacao_nome_Searchbuttontext ;
      private String Ddo_contratoocorrencianotificacao_docto_Caption ;
      private String Ddo_contratoocorrencianotificacao_docto_Tooltip ;
      private String Ddo_contratoocorrencianotificacao_docto_Cls ;
      private String Ddo_contratoocorrencianotificacao_docto_Filteredtext_set ;
      private String Ddo_contratoocorrencianotificacao_docto_Selectedvalue_set ;
      private String Ddo_contratoocorrencianotificacao_docto_Dropdownoptionstype ;
      private String Ddo_contratoocorrencianotificacao_docto_Titlecontrolidtoreplace ;
      private String Ddo_contratoocorrencianotificacao_docto_Sortedstatus ;
      private String Ddo_contratoocorrencianotificacao_docto_Filtertype ;
      private String Ddo_contratoocorrencianotificacao_docto_Datalisttype ;
      private String Ddo_contratoocorrencianotificacao_docto_Datalistproc ;
      private String Ddo_contratoocorrencianotificacao_docto_Sortasc ;
      private String Ddo_contratoocorrencianotificacao_docto_Sortdsc ;
      private String Ddo_contratoocorrencianotificacao_docto_Loadingdata ;
      private String Ddo_contratoocorrencianotificacao_docto_Cleanfilter ;
      private String Ddo_contratoocorrencianotificacao_docto_Noresultsfound ;
      private String Ddo_contratoocorrencianotificacao_docto_Searchbuttontext ;
      private String GX_FocusControl ;
      private String edtContratoOcorrencia_Codigo_Internalname ;
      private String edtContratoOcorrencia_Codigo_Jsonclick ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String edtavTfcontratoocorrencianotificacao_data_Internalname ;
      private String edtavTfcontratoocorrencianotificacao_data_Jsonclick ;
      private String edtavTfcontratoocorrencianotificacao_data_to_Internalname ;
      private String edtavTfcontratoocorrencianotificacao_data_to_Jsonclick ;
      private String divDdo_contratoocorrencianotificacao_dataauxdates_Internalname ;
      private String edtavDdo_contratoocorrencianotificacao_dataauxdate_Internalname ;
      private String edtavDdo_contratoocorrencianotificacao_dataauxdate_Jsonclick ;
      private String edtavDdo_contratoocorrencianotificacao_dataauxdateto_Internalname ;
      private String edtavDdo_contratoocorrencianotificacao_dataauxdateto_Jsonclick ;
      private String edtavTfcontratoocorrencianotificacao_prazo_Internalname ;
      private String edtavTfcontratoocorrencianotificacao_prazo_Jsonclick ;
      private String edtavTfcontratoocorrencianotificacao_prazo_to_Internalname ;
      private String edtavTfcontratoocorrencianotificacao_prazo_to_Jsonclick ;
      private String edtavTfcontratoocorrencianotificacao_descricao_Internalname ;
      private String edtavTfcontratoocorrencianotificacao_descricao_Jsonclick ;
      private String edtavTfcontratoocorrencianotificacao_descricao_sel_Internalname ;
      private String edtavTfcontratoocorrencianotificacao_descricao_sel_Jsonclick ;
      private String edtavTfcontratoocorrencianotificacao_cumprido_Internalname ;
      private String edtavTfcontratoocorrencianotificacao_cumprido_Jsonclick ;
      private String edtavTfcontratoocorrencianotificacao_cumprido_to_Internalname ;
      private String edtavTfcontratoocorrencianotificacao_cumprido_to_Jsonclick ;
      private String divDdo_contratoocorrencianotificacao_cumpridoauxdates_Internalname ;
      private String edtavDdo_contratoocorrencianotificacao_cumpridoauxdate_Internalname ;
      private String edtavDdo_contratoocorrencianotificacao_cumpridoauxdate_Jsonclick ;
      private String edtavDdo_contratoocorrencianotificacao_cumpridoauxdateto_Internalname ;
      private String edtavDdo_contratoocorrencianotificacao_cumpridoauxdateto_Jsonclick ;
      private String edtavTfcontratoocorrencianotificacao_protocolo_Internalname ;
      private String edtavTfcontratoocorrencianotificacao_protocolo_Jsonclick ;
      private String edtavTfcontratoocorrencianotificacao_protocolo_sel_Internalname ;
      private String edtavTfcontratoocorrencianotificacao_protocolo_sel_Jsonclick ;
      private String edtavTfcontratoocorrencianotificacao_responsavel_Internalname ;
      private String edtavTfcontratoocorrencianotificacao_responsavel_Jsonclick ;
      private String edtavTfcontratoocorrencianotificacao_responsavel_to_Internalname ;
      private String edtavTfcontratoocorrencianotificacao_responsavel_to_Jsonclick ;
      private String edtavTfcontratoocorrencianotificacao_nome_Internalname ;
      private String edtavTfcontratoocorrencianotificacao_nome_Jsonclick ;
      private String edtavTfcontratoocorrencianotificacao_nome_sel_Internalname ;
      private String edtavTfcontratoocorrencianotificacao_nome_sel_Jsonclick ;
      private String edtavTfcontratoocorrencianotificacao_docto_Internalname ;
      private String edtavTfcontratoocorrencianotificacao_docto_Jsonclick ;
      private String edtavTfcontratoocorrencianotificacao_docto_sel_Internalname ;
      private String edtavTfcontratoocorrencianotificacao_docto_sel_Jsonclick ;
      private String edtavDdo_contratoocorrencianotificacao_datatitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratoocorrencianotificacao_prazotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratoocorrencianotificacao_descricaotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratoocorrencianotificacao_cumpridotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratoocorrencianotificacao_protocolotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratoocorrencianotificacao_responsaveltitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratoocorrencianotificacao_nometitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratoocorrencianotificacao_doctotitlecontrolidtoreplace_Internalname ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String cmbavOrderedby_Internalname ;
      private String edtavDelete_Internalname ;
      private String edtavUpdate_Internalname ;
      private String edtContratoOcorrenciaNotificacao_Data_Internalname ;
      private String edtContratoOcorrenciaNotificacao_Prazo_Internalname ;
      private String edtContratoOcorrenciaNotificacao_Descricao_Internalname ;
      private String edtContratoOcorrenciaNotificacao_Cumprido_Internalname ;
      private String edtContratoOcorrenciaNotificacao_Protocolo_Internalname ;
      private String edtContratoOcorrenciaNotificacao_Responsavel_Internalname ;
      private String A304ContratoOcorrenciaNotificacao_Nome ;
      private String edtContratoOcorrenciaNotificacao_Nome_Internalname ;
      private String edtContratoOcorrenciaNotificacao_Docto_Internalname ;
      private String scmdbuf ;
      private String lV67TFContratoOcorrenciaNotificacao_Nome ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String edtavContratoocorrencianotificacao_data1_Internalname ;
      private String edtavContratoocorrencianotificacao_data_to1_Internalname ;
      private String edtavContratoocorrencianotificacao_prazo1_Internalname ;
      private String edtavContratoocorrencianotificacao_prazo_to1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String edtavContratoocorrencianotificacao_data2_Internalname ;
      private String edtavContratoocorrencianotificacao_data_to2_Internalname ;
      private String edtavContratoocorrencianotificacao_prazo2_Internalname ;
      private String edtavContratoocorrencianotificacao_prazo_to2_Internalname ;
      private String hsh ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_contratoocorrencianotificacao_data_Internalname ;
      private String Ddo_contratoocorrencianotificacao_prazo_Internalname ;
      private String Ddo_contratoocorrencianotificacao_descricao_Internalname ;
      private String Ddo_contratoocorrencianotificacao_cumprido_Internalname ;
      private String Ddo_contratoocorrencianotificacao_protocolo_Internalname ;
      private String Ddo_contratoocorrencianotificacao_responsavel_Internalname ;
      private String Ddo_contratoocorrencianotificacao_nome_Internalname ;
      private String Ddo_contratoocorrencianotificacao_docto_Internalname ;
      private String edtContratoOcorrenciaNotificacao_Data_Title ;
      private String edtContratoOcorrenciaNotificacao_Prazo_Title ;
      private String edtContratoOcorrenciaNotificacao_Descricao_Title ;
      private String edtContratoOcorrenciaNotificacao_Cumprido_Title ;
      private String edtContratoOcorrenciaNotificacao_Protocolo_Title ;
      private String edtContratoOcorrenciaNotificacao_Responsavel_Title ;
      private String edtContratoOcorrenciaNotificacao_Nome_Title ;
      private String edtContratoOcorrenciaNotificacao_Docto_Title ;
      private String edtavDelete_Tooltiptext ;
      private String edtavDelete_Link ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavUpdate_Link ;
      private String edtContratoOcorrenciaNotificacao_Data_Link ;
      private String edtContratoOcorrenciaNotificacao_Nome_Link ;
      private String tblTablemergeddynamicfilterscontratoocorrencianotificacao_data1_Internalname ;
      private String tblTablemergeddynamicfilterscontratoocorrencianotificacao_prazo1_Internalname ;
      private String tblTablemergeddynamicfilterscontratoocorrencianotificacao_data2_Internalname ;
      private String tblTablemergeddynamicfilterscontratoocorrencianotificacao_prazo2_Internalname ;
      private String sStyleString ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTablesearch_Internalname ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String edtavContratoocorrencianotificacao_prazo2_Jsonclick ;
      private String lblDynamicfilterscontratoocorrencianotificacao_prazo_rangemiddletext2_Internalname ;
      private String lblDynamicfilterscontratoocorrencianotificacao_prazo_rangemiddletext2_Jsonclick ;
      private String edtavContratoocorrencianotificacao_prazo_to2_Jsonclick ;
      private String edtavContratoocorrencianotificacao_data2_Jsonclick ;
      private String lblDynamicfilterscontratoocorrencianotificacao_data_rangemiddletext2_Internalname ;
      private String lblDynamicfilterscontratoocorrencianotificacao_data_rangemiddletext2_Jsonclick ;
      private String edtavContratoocorrencianotificacao_data_to2_Jsonclick ;
      private String edtavContratoocorrencianotificacao_prazo1_Jsonclick ;
      private String lblDynamicfilterscontratoocorrencianotificacao_prazo_rangemiddletext1_Internalname ;
      private String lblDynamicfilterscontratoocorrencianotificacao_prazo_rangemiddletext1_Jsonclick ;
      private String edtavContratoocorrencianotificacao_prazo_to1_Jsonclick ;
      private String edtavContratoocorrencianotificacao_data1_Jsonclick ;
      private String lblDynamicfilterscontratoocorrencianotificacao_data_rangemiddletext1_Internalname ;
      private String lblDynamicfilterscontratoocorrencianotificacao_data_rangemiddletext1_Jsonclick ;
      private String edtavContratoocorrencianotificacao_data_to1_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String imgInsert_Internalname ;
      private String imgInsert_Jsonclick ;
      private String sCtrlAV7ContratoOcorrencia_Codigo ;
      private String sGXsfl_84_fel_idx="0001" ;
      private String ROClassString ;
      private String edtContratoOcorrenciaNotificacao_Data_Jsonclick ;
      private String edtContratoOcorrenciaNotificacao_Prazo_Jsonclick ;
      private String edtContratoOcorrenciaNotificacao_Descricao_Jsonclick ;
      private String edtContratoOcorrenciaNotificacao_Cumprido_Jsonclick ;
      private String edtContratoOcorrenciaNotificacao_Protocolo_Jsonclick ;
      private String edtContratoOcorrenciaNotificacao_Responsavel_Jsonclick ;
      private String edtContratoOcorrenciaNotificacao_Nome_Jsonclick ;
      private String edtContratoOcorrenciaNotificacao_Docto_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private DateTime AV17ContratoOcorrenciaNotificacao_Data1 ;
      private DateTime AV18ContratoOcorrenciaNotificacao_Data_To1 ;
      private DateTime AV23ContratoOcorrenciaNotificacao_Data2 ;
      private DateTime AV24ContratoOcorrenciaNotificacao_Data_To2 ;
      private DateTime AV39TFContratoOcorrenciaNotificacao_Data ;
      private DateTime AV40TFContratoOcorrenciaNotificacao_Data_To ;
      private DateTime AV53TFContratoOcorrenciaNotificacao_Cumprido ;
      private DateTime AV54TFContratoOcorrenciaNotificacao_Cumprido_To ;
      private DateTime AV41DDO_ContratoOcorrenciaNotificacao_DataAuxDate ;
      private DateTime AV42DDO_ContratoOcorrenciaNotificacao_DataAuxDateTo ;
      private DateTime AV55DDO_ContratoOcorrenciaNotificacao_CumpridoAuxDate ;
      private DateTime AV56DDO_ContratoOcorrenciaNotificacao_CumpridoAuxDateTo ;
      private DateTime A298ContratoOcorrenciaNotificacao_Data ;
      private DateTime A301ContratoOcorrenciaNotificacao_Cumprido ;
      private DateTime A295ContratoOcorrencia_Data ;
      private bool entryPointCalled ;
      private bool AV15OrderedDsc ;
      private bool AV21DynamicFiltersEnabled2 ;
      private bool AV34DynamicFiltersIgnoreFirst ;
      private bool AV33DynamicFiltersRemoving ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_contratoocorrencianotificacao_data_Includesortasc ;
      private bool Ddo_contratoocorrencianotificacao_data_Includesortdsc ;
      private bool Ddo_contratoocorrencianotificacao_data_Includefilter ;
      private bool Ddo_contratoocorrencianotificacao_data_Filterisrange ;
      private bool Ddo_contratoocorrencianotificacao_data_Includedatalist ;
      private bool Ddo_contratoocorrencianotificacao_prazo_Includesortasc ;
      private bool Ddo_contratoocorrencianotificacao_prazo_Includesortdsc ;
      private bool Ddo_contratoocorrencianotificacao_prazo_Includefilter ;
      private bool Ddo_contratoocorrencianotificacao_prazo_Filterisrange ;
      private bool Ddo_contratoocorrencianotificacao_prazo_Includedatalist ;
      private bool Ddo_contratoocorrencianotificacao_descricao_Includesortasc ;
      private bool Ddo_contratoocorrencianotificacao_descricao_Includesortdsc ;
      private bool Ddo_contratoocorrencianotificacao_descricao_Includefilter ;
      private bool Ddo_contratoocorrencianotificacao_descricao_Filterisrange ;
      private bool Ddo_contratoocorrencianotificacao_descricao_Includedatalist ;
      private bool Ddo_contratoocorrencianotificacao_cumprido_Includesortasc ;
      private bool Ddo_contratoocorrencianotificacao_cumprido_Includesortdsc ;
      private bool Ddo_contratoocorrencianotificacao_cumprido_Includefilter ;
      private bool Ddo_contratoocorrencianotificacao_cumprido_Filterisrange ;
      private bool Ddo_contratoocorrencianotificacao_cumprido_Includedatalist ;
      private bool Ddo_contratoocorrencianotificacao_protocolo_Includesortasc ;
      private bool Ddo_contratoocorrencianotificacao_protocolo_Includesortdsc ;
      private bool Ddo_contratoocorrencianotificacao_protocolo_Includefilter ;
      private bool Ddo_contratoocorrencianotificacao_protocolo_Filterisrange ;
      private bool Ddo_contratoocorrencianotificacao_protocolo_Includedatalist ;
      private bool Ddo_contratoocorrencianotificacao_responsavel_Includesortasc ;
      private bool Ddo_contratoocorrencianotificacao_responsavel_Includesortdsc ;
      private bool Ddo_contratoocorrencianotificacao_responsavel_Includefilter ;
      private bool Ddo_contratoocorrencianotificacao_responsavel_Filterisrange ;
      private bool Ddo_contratoocorrencianotificacao_responsavel_Includedatalist ;
      private bool Ddo_contratoocorrencianotificacao_nome_Includesortasc ;
      private bool Ddo_contratoocorrencianotificacao_nome_Includesortdsc ;
      private bool Ddo_contratoocorrencianotificacao_nome_Includefilter ;
      private bool Ddo_contratoocorrencianotificacao_nome_Filterisrange ;
      private bool Ddo_contratoocorrencianotificacao_nome_Includedatalist ;
      private bool Ddo_contratoocorrencianotificacao_docto_Includesortasc ;
      private bool Ddo_contratoocorrencianotificacao_docto_Includesortdsc ;
      private bool Ddo_contratoocorrencianotificacao_docto_Includefilter ;
      private bool Ddo_contratoocorrencianotificacao_docto_Filterisrange ;
      private bool Ddo_contratoocorrencianotificacao_docto_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n301ContratoOcorrenciaNotificacao_Cumprido ;
      private bool n302ContratoOcorrenciaNotificacao_Protocolo ;
      private bool n304ContratoOcorrenciaNotificacao_Nome ;
      private bool n306ContratoOcorrenciaNotificacao_Docto ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV35Delete_IsBlob ;
      private bool AV36Update_IsBlob ;
      private String AV16DynamicFiltersSelector1 ;
      private String AV22DynamicFiltersSelector2 ;
      private String AV49TFContratoOcorrenciaNotificacao_Descricao ;
      private String AV50TFContratoOcorrenciaNotificacao_Descricao_Sel ;
      private String AV59TFContratoOcorrenciaNotificacao_Protocolo ;
      private String AV60TFContratoOcorrenciaNotificacao_Protocolo_Sel ;
      private String AV71TFContratoOcorrenciaNotificacao_Docto ;
      private String AV72TFContratoOcorrenciaNotificacao_Docto_Sel ;
      private String AV43ddo_ContratoOcorrenciaNotificacao_DataTitleControlIdToReplace ;
      private String AV47ddo_ContratoOcorrenciaNotificacao_PrazoTitleControlIdToReplace ;
      private String AV51ddo_ContratoOcorrenciaNotificacao_DescricaoTitleControlIdToReplace ;
      private String AV57ddo_ContratoOcorrenciaNotificacao_CumpridoTitleControlIdToReplace ;
      private String AV61ddo_ContratoOcorrenciaNotificacao_ProtocoloTitleControlIdToReplace ;
      private String AV65ddo_ContratoOcorrenciaNotificacao_ResponsavelTitleControlIdToReplace ;
      private String AV69ddo_ContratoOcorrenciaNotificacao_NomeTitleControlIdToReplace ;
      private String AV73ddo_ContratoOcorrenciaNotificacao_DoctoTitleControlIdToReplace ;
      private String AV80Delete_GXI ;
      private String AV81Update_GXI ;
      private String A300ContratoOcorrenciaNotificacao_Descricao ;
      private String A302ContratoOcorrenciaNotificacao_Protocolo ;
      private String A306ContratoOcorrenciaNotificacao_Docto ;
      private String lV49TFContratoOcorrenciaNotificacao_Descricao ;
      private String lV59TFContratoOcorrenciaNotificacao_Protocolo ;
      private String lV71TFContratoOcorrenciaNotificacao_Docto ;
      private String AV35Delete ;
      private String AV36Update ;
      private IGxSession AV37Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private IDataStoreProvider pr_default ;
      private DateTime[] H007A2_A295ContratoOcorrencia_Data ;
      private int[] H007A2_A297ContratoOcorrenciaNotificacao_Codigo ;
      private int[] H007A2_A294ContratoOcorrencia_Codigo ;
      private String[] H007A2_A306ContratoOcorrenciaNotificacao_Docto ;
      private bool[] H007A2_n306ContratoOcorrenciaNotificacao_Docto ;
      private String[] H007A2_A304ContratoOcorrenciaNotificacao_Nome ;
      private bool[] H007A2_n304ContratoOcorrenciaNotificacao_Nome ;
      private int[] H007A2_A303ContratoOcorrenciaNotificacao_Responsavel ;
      private String[] H007A2_A302ContratoOcorrenciaNotificacao_Protocolo ;
      private bool[] H007A2_n302ContratoOcorrenciaNotificacao_Protocolo ;
      private DateTime[] H007A2_A301ContratoOcorrenciaNotificacao_Cumprido ;
      private bool[] H007A2_n301ContratoOcorrenciaNotificacao_Cumprido ;
      private String[] H007A2_A300ContratoOcorrenciaNotificacao_Descricao ;
      private short[] H007A2_A299ContratoOcorrenciaNotificacao_Prazo ;
      private DateTime[] H007A2_A298ContratoOcorrenciaNotificacao_Data ;
      private long[] H007A3_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV8HTTPRequest ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV38ContratoOcorrenciaNotificacao_DataTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV44ContratoOcorrenciaNotificacao_PrazoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV48ContratoOcorrenciaNotificacao_DescricaoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV52ContratoOcorrenciaNotificacao_CumpridoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV58ContratoOcorrenciaNotificacao_ProtocoloTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV62ContratoOcorrenciaNotificacao_ResponsavelTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV66ContratoOcorrenciaNotificacao_NomeTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV70ContratoOcorrenciaNotificacao_DoctoTitleFilterData ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV10TrnContextAtt ;
      private wwpbaseobjects.SdtWWPGridState AV11GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV12GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV13GridStateDynamicFilter ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV74DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class contratoocorrenciacontratoocorrencianotificacaowc__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H007A2( IGxContext context ,
                                             String AV16DynamicFiltersSelector1 ,
                                             DateTime AV17ContratoOcorrenciaNotificacao_Data1 ,
                                             DateTime AV18ContratoOcorrenciaNotificacao_Data_To1 ,
                                             short AV19ContratoOcorrenciaNotificacao_Prazo1 ,
                                             short AV20ContratoOcorrenciaNotificacao_Prazo_To1 ,
                                             bool AV21DynamicFiltersEnabled2 ,
                                             String AV22DynamicFiltersSelector2 ,
                                             DateTime AV23ContratoOcorrenciaNotificacao_Data2 ,
                                             DateTime AV24ContratoOcorrenciaNotificacao_Data_To2 ,
                                             short AV25ContratoOcorrenciaNotificacao_Prazo2 ,
                                             short AV26ContratoOcorrenciaNotificacao_Prazo_To2 ,
                                             DateTime AV39TFContratoOcorrenciaNotificacao_Data ,
                                             DateTime AV40TFContratoOcorrenciaNotificacao_Data_To ,
                                             short AV45TFContratoOcorrenciaNotificacao_Prazo ,
                                             short AV46TFContratoOcorrenciaNotificacao_Prazo_To ,
                                             String AV50TFContratoOcorrenciaNotificacao_Descricao_Sel ,
                                             String AV49TFContratoOcorrenciaNotificacao_Descricao ,
                                             DateTime AV53TFContratoOcorrenciaNotificacao_Cumprido ,
                                             DateTime AV54TFContratoOcorrenciaNotificacao_Cumprido_To ,
                                             String AV60TFContratoOcorrenciaNotificacao_Protocolo_Sel ,
                                             String AV59TFContratoOcorrenciaNotificacao_Protocolo ,
                                             int AV63TFContratoOcorrenciaNotificacao_Responsavel ,
                                             int AV64TFContratoOcorrenciaNotificacao_Responsavel_To ,
                                             String AV68TFContratoOcorrenciaNotificacao_Nome_Sel ,
                                             String AV67TFContratoOcorrenciaNotificacao_Nome ,
                                             String AV72TFContratoOcorrenciaNotificacao_Docto_Sel ,
                                             String AV71TFContratoOcorrenciaNotificacao_Docto ,
                                             DateTime A298ContratoOcorrenciaNotificacao_Data ,
                                             short A299ContratoOcorrenciaNotificacao_Prazo ,
                                             String A300ContratoOcorrenciaNotificacao_Descricao ,
                                             DateTime A301ContratoOcorrenciaNotificacao_Cumprido ,
                                             String A302ContratoOcorrenciaNotificacao_Protocolo ,
                                             int A303ContratoOcorrenciaNotificacao_Responsavel ,
                                             String A304ContratoOcorrenciaNotificacao_Nome ,
                                             String A306ContratoOcorrenciaNotificacao_Docto ,
                                             short AV14OrderedBy ,
                                             bool AV15OrderedDsc ,
                                             int A294ContratoOcorrencia_Codigo ,
                                             int AV7ContratoOcorrencia_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [30] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T2.[ContratoOcorrencia_Data], T1.[ContratoOcorrenciaNotificacao_Codigo], T1.[ContratoOcorrencia_Codigo], T3.[Pessoa_Docto] AS ContratoOcorrenciaNotificacao_Docto, T3.[Pessoa_Nome] AS ContratoOcorrenciaNotificacao_Nome, T1.[ContratoOcorrenciaNotificacao_Responsavel] AS ContratoOcorrenciaNotificacao_Responsavel, T1.[ContratoOcorrenciaNotificacao_Protocolo], T1.[ContratoOcorrenciaNotificacao_Cumprido], T1.[ContratoOcorrenciaNotificacao_Descricao], T1.[ContratoOcorrenciaNotificacao_Prazo], T1.[ContratoOcorrenciaNotificacao_Data]";
         sFromString = " FROM (([ContratoOcorrenciaNotificacao] T1 WITH (NOLOCK) INNER JOIN [ContratoOcorrencia] T2 WITH (NOLOCK) ON T2.[ContratoOcorrencia_Codigo] = T1.[ContratoOcorrencia_Codigo]) INNER JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T1.[ContratoOcorrenciaNotificacao_Responsavel])";
         sOrderString = "";
         sWhereString = sWhereString + " WHERE (T1.[ContratoOcorrencia_Codigo] = @AV7ContratoOcorrencia_Codigo)";
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV17ContratoOcorrenciaNotificacao_Data1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] >= @AV17ContratoOcorrenciaNotificacao_Data1)";
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV18ContratoOcorrenciaNotificacao_Data_To1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] <= @AV18ContratoOcorrenciaNotificacao_Data_To1)";
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTRATOOCORRENCIANOTIFICACAO_PRAZO") == 0 ) && ( ! (0==AV19ContratoOcorrenciaNotificacao_Prazo1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Prazo] >= @AV19ContratoOcorrenciaNotificacao_Prazo1)";
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTRATOOCORRENCIANOTIFICACAO_PRAZO") == 0 ) && ( ! (0==AV20ContratoOcorrenciaNotificacao_Prazo_To1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Prazo] <= @AV20ContratoOcorrenciaNotificacao_Prazo_To1)";
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( AV21DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV22DynamicFiltersSelector2, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV23ContratoOcorrenciaNotificacao_Data2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] >= @AV23ContratoOcorrenciaNotificacao_Data2)";
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( AV21DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV22DynamicFiltersSelector2, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV24ContratoOcorrenciaNotificacao_Data_To2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] <= @AV24ContratoOcorrenciaNotificacao_Data_To2)";
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( AV21DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV22DynamicFiltersSelector2, "CONTRATOOCORRENCIANOTIFICACAO_PRAZO") == 0 ) && ( ! (0==AV25ContratoOcorrenciaNotificacao_Prazo2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Prazo] >= @AV25ContratoOcorrenciaNotificacao_Prazo2)";
         }
         else
         {
            GXv_int2[7] = 1;
         }
         if ( AV21DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV22DynamicFiltersSelector2, "CONTRATOOCORRENCIANOTIFICACAO_PRAZO") == 0 ) && ( ! (0==AV26ContratoOcorrenciaNotificacao_Prazo_To2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Prazo] <= @AV26ContratoOcorrenciaNotificacao_Prazo_To2)";
         }
         else
         {
            GXv_int2[8] = 1;
         }
         if ( ! (DateTime.MinValue==AV39TFContratoOcorrenciaNotificacao_Data) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] >= @AV39TFContratoOcorrenciaNotificacao_Data)";
         }
         else
         {
            GXv_int2[9] = 1;
         }
         if ( ! (DateTime.MinValue==AV40TFContratoOcorrenciaNotificacao_Data_To) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] <= @AV40TFContratoOcorrenciaNotificacao_Data_To)";
         }
         else
         {
            GXv_int2[10] = 1;
         }
         if ( ! (0==AV45TFContratoOcorrenciaNotificacao_Prazo) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Prazo] >= @AV45TFContratoOcorrenciaNotificacao_Prazo)";
         }
         else
         {
            GXv_int2[11] = 1;
         }
         if ( ! (0==AV46TFContratoOcorrenciaNotificacao_Prazo_To) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Prazo] <= @AV46TFContratoOcorrenciaNotificacao_Prazo_To)";
         }
         else
         {
            GXv_int2[12] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV50TFContratoOcorrenciaNotificacao_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49TFContratoOcorrenciaNotificacao_Descricao)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Descricao] like @lV49TFContratoOcorrenciaNotificacao_Descricao)";
         }
         else
         {
            GXv_int2[13] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV50TFContratoOcorrenciaNotificacao_Descricao_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Descricao] = @AV50TFContratoOcorrenciaNotificacao_Descricao_Sel)";
         }
         else
         {
            GXv_int2[14] = 1;
         }
         if ( ! (DateTime.MinValue==AV53TFContratoOcorrenciaNotificacao_Cumprido) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Cumprido] >= @AV53TFContratoOcorrenciaNotificacao_Cumprido)";
         }
         else
         {
            GXv_int2[15] = 1;
         }
         if ( ! (DateTime.MinValue==AV54TFContratoOcorrenciaNotificacao_Cumprido_To) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Cumprido] <= @AV54TFContratoOcorrenciaNotificacao_Cumprido_To)";
         }
         else
         {
            GXv_int2[16] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV60TFContratoOcorrenciaNotificacao_Protocolo_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59TFContratoOcorrenciaNotificacao_Protocolo)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Protocolo] like @lV59TFContratoOcorrenciaNotificacao_Protocolo)";
         }
         else
         {
            GXv_int2[17] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60TFContratoOcorrenciaNotificacao_Protocolo_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Protocolo] = @AV60TFContratoOcorrenciaNotificacao_Protocolo_Sel)";
         }
         else
         {
            GXv_int2[18] = 1;
         }
         if ( ! (0==AV63TFContratoOcorrenciaNotificacao_Responsavel) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Responsavel] >= @AV63TFContratoOcorrenciaNotificacao_Responsavel)";
         }
         else
         {
            GXv_int2[19] = 1;
         }
         if ( ! (0==AV64TFContratoOcorrenciaNotificacao_Responsavel_To) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Responsavel] <= @AV64TFContratoOcorrenciaNotificacao_Responsavel_To)";
         }
         else
         {
            GXv_int2[20] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV68TFContratoOcorrenciaNotificacao_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67TFContratoOcorrenciaNotificacao_Nome)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV67TFContratoOcorrenciaNotificacao_Nome)";
         }
         else
         {
            GXv_int2[21] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV68TFContratoOcorrenciaNotificacao_Nome_Sel)) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] = @AV68TFContratoOcorrenciaNotificacao_Nome_Sel)";
         }
         else
         {
            GXv_int2[22] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV72TFContratoOcorrenciaNotificacao_Docto_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV71TFContratoOcorrenciaNotificacao_Docto)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Docto] like @lV71TFContratoOcorrenciaNotificacao_Docto)";
         }
         else
         {
            GXv_int2[23] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV72TFContratoOcorrenciaNotificacao_Docto_Sel)) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Docto] = @AV72TFContratoOcorrenciaNotificacao_Docto_Sel)";
         }
         else
         {
            GXv_int2[24] = 1;
         }
         if ( AV14OrderedBy == 1 )
         {
            sOrderString = sOrderString + " ORDER BY T2.[ContratoOcorrencia_Data]";
         }
         else if ( ( AV14OrderedBy == 2 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoOcorrencia_Codigo], T1.[ContratoOcorrenciaNotificacao_Data]";
         }
         else if ( ( AV14OrderedBy == 2 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoOcorrencia_Codigo] DESC, T1.[ContratoOcorrenciaNotificacao_Data] DESC";
         }
         else if ( ( AV14OrderedBy == 3 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoOcorrencia_Codigo], T1.[ContratoOcorrenciaNotificacao_Prazo]";
         }
         else if ( ( AV14OrderedBy == 3 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoOcorrencia_Codigo] DESC, T1.[ContratoOcorrenciaNotificacao_Prazo] DESC";
         }
         else if ( ( AV14OrderedBy == 4 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoOcorrencia_Codigo], T1.[ContratoOcorrenciaNotificacao_Descricao]";
         }
         else if ( ( AV14OrderedBy == 4 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoOcorrencia_Codigo] DESC, T1.[ContratoOcorrenciaNotificacao_Descricao] DESC";
         }
         else if ( ( AV14OrderedBy == 5 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoOcorrencia_Codigo], T1.[ContratoOcorrenciaNotificacao_Cumprido]";
         }
         else if ( ( AV14OrderedBy == 5 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoOcorrencia_Codigo] DESC, T1.[ContratoOcorrenciaNotificacao_Cumprido] DESC";
         }
         else if ( ( AV14OrderedBy == 6 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoOcorrencia_Codigo], T1.[ContratoOcorrenciaNotificacao_Protocolo]";
         }
         else if ( ( AV14OrderedBy == 6 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoOcorrencia_Codigo] DESC, T1.[ContratoOcorrenciaNotificacao_Protocolo] DESC";
         }
         else if ( ( AV14OrderedBy == 7 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoOcorrencia_Codigo], T1.[ContratoOcorrenciaNotificacao_Responsavel]";
         }
         else if ( ( AV14OrderedBy == 7 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoOcorrencia_Codigo] DESC, T1.[ContratoOcorrenciaNotificacao_Responsavel] DESC";
         }
         else if ( ( AV14OrderedBy == 8 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoOcorrencia_Codigo], T3.[Pessoa_Nome]";
         }
         else if ( ( AV14OrderedBy == 8 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoOcorrencia_Codigo] DESC, T3.[Pessoa_Nome] DESC";
         }
         else if ( ( AV14OrderedBy == 9 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoOcorrencia_Codigo], T3.[Pessoa_Docto]";
         }
         else if ( ( AV14OrderedBy == 9 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoOcorrencia_Codigo] DESC, T3.[Pessoa_Docto] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoOcorrenciaNotificacao_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H007A3( IGxContext context ,
                                             String AV16DynamicFiltersSelector1 ,
                                             DateTime AV17ContratoOcorrenciaNotificacao_Data1 ,
                                             DateTime AV18ContratoOcorrenciaNotificacao_Data_To1 ,
                                             short AV19ContratoOcorrenciaNotificacao_Prazo1 ,
                                             short AV20ContratoOcorrenciaNotificacao_Prazo_To1 ,
                                             bool AV21DynamicFiltersEnabled2 ,
                                             String AV22DynamicFiltersSelector2 ,
                                             DateTime AV23ContratoOcorrenciaNotificacao_Data2 ,
                                             DateTime AV24ContratoOcorrenciaNotificacao_Data_To2 ,
                                             short AV25ContratoOcorrenciaNotificacao_Prazo2 ,
                                             short AV26ContratoOcorrenciaNotificacao_Prazo_To2 ,
                                             DateTime AV39TFContratoOcorrenciaNotificacao_Data ,
                                             DateTime AV40TFContratoOcorrenciaNotificacao_Data_To ,
                                             short AV45TFContratoOcorrenciaNotificacao_Prazo ,
                                             short AV46TFContratoOcorrenciaNotificacao_Prazo_To ,
                                             String AV50TFContratoOcorrenciaNotificacao_Descricao_Sel ,
                                             String AV49TFContratoOcorrenciaNotificacao_Descricao ,
                                             DateTime AV53TFContratoOcorrenciaNotificacao_Cumprido ,
                                             DateTime AV54TFContratoOcorrenciaNotificacao_Cumprido_To ,
                                             String AV60TFContratoOcorrenciaNotificacao_Protocolo_Sel ,
                                             String AV59TFContratoOcorrenciaNotificacao_Protocolo ,
                                             int AV63TFContratoOcorrenciaNotificacao_Responsavel ,
                                             int AV64TFContratoOcorrenciaNotificacao_Responsavel_To ,
                                             String AV68TFContratoOcorrenciaNotificacao_Nome_Sel ,
                                             String AV67TFContratoOcorrenciaNotificacao_Nome ,
                                             String AV72TFContratoOcorrenciaNotificacao_Docto_Sel ,
                                             String AV71TFContratoOcorrenciaNotificacao_Docto ,
                                             DateTime A298ContratoOcorrenciaNotificacao_Data ,
                                             short A299ContratoOcorrenciaNotificacao_Prazo ,
                                             String A300ContratoOcorrenciaNotificacao_Descricao ,
                                             DateTime A301ContratoOcorrenciaNotificacao_Cumprido ,
                                             String A302ContratoOcorrenciaNotificacao_Protocolo ,
                                             int A303ContratoOcorrenciaNotificacao_Responsavel ,
                                             String A304ContratoOcorrenciaNotificacao_Nome ,
                                             String A306ContratoOcorrenciaNotificacao_Docto ,
                                             short AV14OrderedBy ,
                                             bool AV15OrderedDsc ,
                                             int A294ContratoOcorrencia_Codigo ,
                                             int AV7ContratoOcorrencia_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [25] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM (([ContratoOcorrenciaNotificacao] T1 WITH (NOLOCK) INNER JOIN [ContratoOcorrencia] T2 WITH (NOLOCK) ON T2.[ContratoOcorrencia_Codigo] = T1.[ContratoOcorrencia_Codigo]) INNER JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T1.[ContratoOcorrenciaNotificacao_Responsavel])";
         scmdbuf = scmdbuf + " WHERE (T1.[ContratoOcorrencia_Codigo] = @AV7ContratoOcorrencia_Codigo)";
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV17ContratoOcorrenciaNotificacao_Data1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] >= @AV17ContratoOcorrenciaNotificacao_Data1)";
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV18ContratoOcorrenciaNotificacao_Data_To1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] <= @AV18ContratoOcorrenciaNotificacao_Data_To1)";
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTRATOOCORRENCIANOTIFICACAO_PRAZO") == 0 ) && ( ! (0==AV19ContratoOcorrenciaNotificacao_Prazo1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Prazo] >= @AV19ContratoOcorrenciaNotificacao_Prazo1)";
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTRATOOCORRENCIANOTIFICACAO_PRAZO") == 0 ) && ( ! (0==AV20ContratoOcorrenciaNotificacao_Prazo_To1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Prazo] <= @AV20ContratoOcorrenciaNotificacao_Prazo_To1)";
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( AV21DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV22DynamicFiltersSelector2, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV23ContratoOcorrenciaNotificacao_Data2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] >= @AV23ContratoOcorrenciaNotificacao_Data2)";
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( AV21DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV22DynamicFiltersSelector2, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV24ContratoOcorrenciaNotificacao_Data_To2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] <= @AV24ContratoOcorrenciaNotificacao_Data_To2)";
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( AV21DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV22DynamicFiltersSelector2, "CONTRATOOCORRENCIANOTIFICACAO_PRAZO") == 0 ) && ( ! (0==AV25ContratoOcorrenciaNotificacao_Prazo2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Prazo] >= @AV25ContratoOcorrenciaNotificacao_Prazo2)";
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( AV21DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV22DynamicFiltersSelector2, "CONTRATOOCORRENCIANOTIFICACAO_PRAZO") == 0 ) && ( ! (0==AV26ContratoOcorrenciaNotificacao_Prazo_To2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Prazo] <= @AV26ContratoOcorrenciaNotificacao_Prazo_To2)";
         }
         else
         {
            GXv_int4[8] = 1;
         }
         if ( ! (DateTime.MinValue==AV39TFContratoOcorrenciaNotificacao_Data) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] >= @AV39TFContratoOcorrenciaNotificacao_Data)";
         }
         else
         {
            GXv_int4[9] = 1;
         }
         if ( ! (DateTime.MinValue==AV40TFContratoOcorrenciaNotificacao_Data_To) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] <= @AV40TFContratoOcorrenciaNotificacao_Data_To)";
         }
         else
         {
            GXv_int4[10] = 1;
         }
         if ( ! (0==AV45TFContratoOcorrenciaNotificacao_Prazo) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Prazo] >= @AV45TFContratoOcorrenciaNotificacao_Prazo)";
         }
         else
         {
            GXv_int4[11] = 1;
         }
         if ( ! (0==AV46TFContratoOcorrenciaNotificacao_Prazo_To) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Prazo] <= @AV46TFContratoOcorrenciaNotificacao_Prazo_To)";
         }
         else
         {
            GXv_int4[12] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV50TFContratoOcorrenciaNotificacao_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49TFContratoOcorrenciaNotificacao_Descricao)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Descricao] like @lV49TFContratoOcorrenciaNotificacao_Descricao)";
         }
         else
         {
            GXv_int4[13] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV50TFContratoOcorrenciaNotificacao_Descricao_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Descricao] = @AV50TFContratoOcorrenciaNotificacao_Descricao_Sel)";
         }
         else
         {
            GXv_int4[14] = 1;
         }
         if ( ! (DateTime.MinValue==AV53TFContratoOcorrenciaNotificacao_Cumprido) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Cumprido] >= @AV53TFContratoOcorrenciaNotificacao_Cumprido)";
         }
         else
         {
            GXv_int4[15] = 1;
         }
         if ( ! (DateTime.MinValue==AV54TFContratoOcorrenciaNotificacao_Cumprido_To) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Cumprido] <= @AV54TFContratoOcorrenciaNotificacao_Cumprido_To)";
         }
         else
         {
            GXv_int4[16] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV60TFContratoOcorrenciaNotificacao_Protocolo_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59TFContratoOcorrenciaNotificacao_Protocolo)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Protocolo] like @lV59TFContratoOcorrenciaNotificacao_Protocolo)";
         }
         else
         {
            GXv_int4[17] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60TFContratoOcorrenciaNotificacao_Protocolo_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Protocolo] = @AV60TFContratoOcorrenciaNotificacao_Protocolo_Sel)";
         }
         else
         {
            GXv_int4[18] = 1;
         }
         if ( ! (0==AV63TFContratoOcorrenciaNotificacao_Responsavel) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Responsavel] >= @AV63TFContratoOcorrenciaNotificacao_Responsavel)";
         }
         else
         {
            GXv_int4[19] = 1;
         }
         if ( ! (0==AV64TFContratoOcorrenciaNotificacao_Responsavel_To) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Responsavel] <= @AV64TFContratoOcorrenciaNotificacao_Responsavel_To)";
         }
         else
         {
            GXv_int4[20] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV68TFContratoOcorrenciaNotificacao_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67TFContratoOcorrenciaNotificacao_Nome)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV67TFContratoOcorrenciaNotificacao_Nome)";
         }
         else
         {
            GXv_int4[21] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV68TFContratoOcorrenciaNotificacao_Nome_Sel)) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] = @AV68TFContratoOcorrenciaNotificacao_Nome_Sel)";
         }
         else
         {
            GXv_int4[22] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV72TFContratoOcorrenciaNotificacao_Docto_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV71TFContratoOcorrenciaNotificacao_Docto)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Docto] like @lV71TFContratoOcorrenciaNotificacao_Docto)";
         }
         else
         {
            GXv_int4[23] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV72TFContratoOcorrenciaNotificacao_Docto_Sel)) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Docto] = @AV72TFContratoOcorrenciaNotificacao_Docto_Sel)";
         }
         else
         {
            GXv_int4[24] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( AV14OrderedBy == 1 )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 2 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 2 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 3 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 3 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 4 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 4 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 5 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 5 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 6 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 6 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 7 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 7 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 8 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 8 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 9 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 9 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H007A2(context, (String)dynConstraints[0] , (DateTime)dynConstraints[1] , (DateTime)dynConstraints[2] , (short)dynConstraints[3] , (short)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (DateTime)dynConstraints[7] , (DateTime)dynConstraints[8] , (short)dynConstraints[9] , (short)dynConstraints[10] , (DateTime)dynConstraints[11] , (DateTime)dynConstraints[12] , (short)dynConstraints[13] , (short)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (DateTime)dynConstraints[17] , (DateTime)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (int)dynConstraints[21] , (int)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (DateTime)dynConstraints[27] , (short)dynConstraints[28] , (String)dynConstraints[29] , (DateTime)dynConstraints[30] , (String)dynConstraints[31] , (int)dynConstraints[32] , (String)dynConstraints[33] , (String)dynConstraints[34] , (short)dynConstraints[35] , (bool)dynConstraints[36] , (int)dynConstraints[37] , (int)dynConstraints[38] );
               case 1 :
                     return conditional_H007A3(context, (String)dynConstraints[0] , (DateTime)dynConstraints[1] , (DateTime)dynConstraints[2] , (short)dynConstraints[3] , (short)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (DateTime)dynConstraints[7] , (DateTime)dynConstraints[8] , (short)dynConstraints[9] , (short)dynConstraints[10] , (DateTime)dynConstraints[11] , (DateTime)dynConstraints[12] , (short)dynConstraints[13] , (short)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (DateTime)dynConstraints[17] , (DateTime)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (int)dynConstraints[21] , (int)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (DateTime)dynConstraints[27] , (short)dynConstraints[28] , (String)dynConstraints[29] , (DateTime)dynConstraints[30] , (String)dynConstraints[31] , (int)dynConstraints[32] , (String)dynConstraints[33] , (String)dynConstraints[34] , (short)dynConstraints[35] , (bool)dynConstraints[36] , (int)dynConstraints[37] , (int)dynConstraints[38] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH007A2 ;
          prmH007A2 = new Object[] {
          new Object[] {"@AV7ContratoOcorrencia_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV17ContratoOcorrenciaNotificacao_Data1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV18ContratoOcorrenciaNotificacao_Data_To1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV19ContratoOcorrenciaNotificacao_Prazo1",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV20ContratoOcorrenciaNotificacao_Prazo_To1",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV23ContratoOcorrenciaNotificacao_Data2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV24ContratoOcorrenciaNotificacao_Data_To2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV25ContratoOcorrenciaNotificacao_Prazo2",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV26ContratoOcorrenciaNotificacao_Prazo_To2",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV39TFContratoOcorrenciaNotificacao_Data",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV40TFContratoOcorrenciaNotificacao_Data_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV45TFContratoOcorrenciaNotificacao_Prazo",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV46TFContratoOcorrenciaNotificacao_Prazo_To",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@lV49TFContratoOcorrenciaNotificacao_Descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV50TFContratoOcorrenciaNotificacao_Descricao_Sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV53TFContratoOcorrenciaNotificacao_Cumprido",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV54TFContratoOcorrenciaNotificacao_Cumprido_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV59TFContratoOcorrenciaNotificacao_Protocolo",SqlDbType.VarChar,20,0} ,
          new Object[] {"@AV60TFContratoOcorrenciaNotificacao_Protocolo_Sel",SqlDbType.VarChar,20,0} ,
          new Object[] {"@AV63TFContratoOcorrenciaNotificacao_Responsavel",SqlDbType.Int,6,0} ,
          new Object[] {"@AV64TFContratoOcorrenciaNotificacao_Responsavel_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV67TFContratoOcorrenciaNotificacao_Nome",SqlDbType.Char,100,0} ,
          new Object[] {"@AV68TFContratoOcorrenciaNotificacao_Nome_Sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV71TFContratoOcorrenciaNotificacao_Docto",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV72TFContratoOcorrenciaNotificacao_Docto_Sel",SqlDbType.VarChar,15,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH007A3 ;
          prmH007A3 = new Object[] {
          new Object[] {"@AV7ContratoOcorrencia_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV17ContratoOcorrenciaNotificacao_Data1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV18ContratoOcorrenciaNotificacao_Data_To1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV19ContratoOcorrenciaNotificacao_Prazo1",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV20ContratoOcorrenciaNotificacao_Prazo_To1",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV23ContratoOcorrenciaNotificacao_Data2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV24ContratoOcorrenciaNotificacao_Data_To2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV25ContratoOcorrenciaNotificacao_Prazo2",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV26ContratoOcorrenciaNotificacao_Prazo_To2",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV39TFContratoOcorrenciaNotificacao_Data",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV40TFContratoOcorrenciaNotificacao_Data_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV45TFContratoOcorrenciaNotificacao_Prazo",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV46TFContratoOcorrenciaNotificacao_Prazo_To",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@lV49TFContratoOcorrenciaNotificacao_Descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV50TFContratoOcorrenciaNotificacao_Descricao_Sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV53TFContratoOcorrenciaNotificacao_Cumprido",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV54TFContratoOcorrenciaNotificacao_Cumprido_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV59TFContratoOcorrenciaNotificacao_Protocolo",SqlDbType.VarChar,20,0} ,
          new Object[] {"@AV60TFContratoOcorrenciaNotificacao_Protocolo_Sel",SqlDbType.VarChar,20,0} ,
          new Object[] {"@AV63TFContratoOcorrenciaNotificacao_Responsavel",SqlDbType.Int,6,0} ,
          new Object[] {"@AV64TFContratoOcorrenciaNotificacao_Responsavel_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV67TFContratoOcorrenciaNotificacao_Nome",SqlDbType.Char,100,0} ,
          new Object[] {"@AV68TFContratoOcorrenciaNotificacao_Nome_Sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV71TFContratoOcorrenciaNotificacao_Docto",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV72TFContratoOcorrenciaNotificacao_Docto_Sel",SqlDbType.VarChar,15,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H007A2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH007A2,11,0,true,false )
             ,new CursorDef("H007A3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH007A3,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((DateTime[]) buf[0])[0] = rslt.getGXDate(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((String[]) buf[3])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((String[]) buf[5])[0] = rslt.getString(5, 100) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((int[]) buf[7])[0] = rslt.getInt(6) ;
                ((String[]) buf[8])[0] = rslt.getVarchar(7) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(7);
                ((DateTime[]) buf[10])[0] = rslt.getGXDate(8) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(8);
                ((String[]) buf[12])[0] = rslt.getVarchar(9) ;
                ((short[]) buf[13])[0] = rslt.getShort(10) ;
                ((DateTime[]) buf[14])[0] = rslt.getGXDate(11) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[30]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[31]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[32]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[33]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[34]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[35]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[36]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[37]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[38]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[39]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[40]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[41]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[42]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[45]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[46]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[48]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[49]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[50]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[51]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[52]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[53]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[54]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[55]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[56]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[57]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[58]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[59]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[25]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[26]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[27]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[28]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[29]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[30]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[31]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[32]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[33]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[34]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[35]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[36]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[37]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[40]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[41]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[44]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[45]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[48]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[49]);
                }
                return;
       }
    }

 }

}
