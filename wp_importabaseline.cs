/*
               File: WP_ImportaBaseline
        Description: Importa Baseline
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 23:44:27.46
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Office;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_importabaseline : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wp_importabaseline( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_importabaseline( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         dynavSistema_codigo = new GXCombobox();
         chkavCriar = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vSISTEMA_CODIGO") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvSISTEMA_CODIGOJ82( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_58 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_58_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_58_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               ajax_req_read_hidden_sdt(GetNextPar( ), AV57SDT_Baseline);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( AV57SDT_Baseline) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PAJ82( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTJ82( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202042823442752");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Window/InNewWindowRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wp_importabaseline.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "Sdt_baseline", AV57SDT_Baseline);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("Sdt_baseline", AV57SDT_Baseline);
         }
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_58", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_58), 4, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSDT_BASELINE", AV57SDT_Baseline);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSDT_BASELINE", AV57SDT_Baseline);
         }
         GxWebStd.gx_hidden_field( context, "vARQUIVO", StringUtil.RTrim( AV16Arquivo));
         GxWebStd.gx_hidden_field( context, "vCOLNOMEN", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV21ColNomen), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCOLTIPON", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV24ColTipon), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCOLDERN", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV18ColDERn), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCOLRLRN", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV23ColRLRn), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCOLOBSERVN", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV22ColObservn), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vSISTEMA_SIGLA", StringUtil.RTrim( AV61Sistema_Sigla));
         GXCCtlgxBlob = "vBLOB" + "_gxBlob";
         GxWebStd.gx_hidden_field( context, GXCCtlgxBlob, AV12Blob);
         GxWebStd.gx_hidden_field( context, "INNEWWINDOW_Width", StringUtil.RTrim( Innewwindow_Width));
         GxWebStd.gx_hidden_field( context, "INNEWWINDOW_Height", StringUtil.RTrim( Innewwindow_Height));
         GxWebStd.gx_hidden_field( context, "INNEWWINDOW_Target", StringUtil.RTrim( Innewwindow_Target));
         GxWebStd.gx_hidden_field( context, "vSISTEMA_CODIGO_Text", StringUtil.RTrim( dynavSistema_codigo.Description));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WEJ82( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTJ82( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wp_importabaseline.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WP_ImportaBaseline" ;
      }

      public override String GetPgmdesc( )
      {
         return "Importa Baseline" ;
      }

      protected void WBJ80( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_J82( true) ;
         }
         else
         {
            wb_table1_2_J82( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_J82e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"INNEWWINDOWContainer"+"\"></div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 71,'',false,'" + sGXsfl_58_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavAreatrabalho_codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV56AreaTrabalho_Codigo), 6, 0, ",", "")), ((edtavAreatrabalho_codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV56AreaTrabalho_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(AV56AreaTrabalho_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,71);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavAreatrabalho_codigo_Jsonclick, 0, "Attribute", "", "", "", edtavAreatrabalho_codigo_Visible, edtavAreatrabalho_codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_ImportaBaseline.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 72,'',false,'" + sGXsfl_58_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavFilename_Internalname, StringUtil.RTrim( AV37FileName), StringUtil.RTrim( context.localUtil.Format( AV37FileName, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,72);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavFilename_Jsonclick, 0, "AttributeCustom", "", "", "", edtavFilename_Visible, edtavFilename_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_ImportaBaseline.htm");
         }
         wbLoad = true;
      }

      protected void STARTJ82( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Importa Baseline", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPJ80( ) ;
      }

      protected void WSJ82( )
      {
         STARTJ82( ) ;
         EVTJ82( ) ;
      }

      protected void EVTJ82( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ENTER'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11J82 */
                              E11J82 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VSISTEMA_CODIGO.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12J82 */
                              E12J82 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'IMPORTAR'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E13J82 */
                              E13J82 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              nGXsfl_58_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_58_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_58_idx), 4, 0)), 4, "0");
                              SubsflControlProps_582( ) ;
                              AV65GXV1 = nGXsfl_58_idx;
                              if ( ( AV57SDT_Baseline.Count >= AV65GXV1 ) && ( AV65GXV1 > 0 ) )
                              {
                                 AV57SDT_Baseline.CurrentItem = ((SdtSDT_Baselines_Funcao)AV57SDT_Baseline.Item(AV65GXV1));
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( AV12Blob)) )
                              {
                                 GXCCtl = "vBLOB_" + sGXsfl_58_idx;
                                 GXCCtlgxBlob = GXCCtl + "_gxBlob";
                                 AV12Blob = cgiGet( GXCCtlgxBlob);
                              }
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E14J82 */
                                    E14J82 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E15J82 */
                                    E15J82 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E16J82 */
                                    E16J82 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       if ( ! Rfr0gs )
                                       {
                                          /* Execute user event: E11J82 */
                                          E11J82 ();
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEJ82( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PAJ82( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            dynavSistema_codigo.Name = "vSISTEMA_CODIGO";
            dynavSistema_codigo.WebTags = "";
            chkavCriar.Name = "vCRIAR";
            chkavCriar.WebTags = "";
            chkavCriar.Caption = "Criar RLR e DER";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavCriar_Internalname, "TitleCaption", chkavCriar.Caption);
            chkavCriar.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = dynavSistema_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLVvSISTEMA_CODIGOJ82( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvSISTEMA_CODIGO_dataJ82( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvSISTEMA_CODIGO_htmlJ82( )
      {
         int gxdynajaxvalue ;
         GXDLVvSISTEMA_CODIGO_dataJ82( ) ;
         gxdynajaxindex = 1;
         dynavSistema_codigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavSistema_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavSistema_codigo.ItemCount > 0 )
         {
            AV5Sistema_Codigo = (int)(NumberUtil.Val( dynavSistema_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV5Sistema_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV5Sistema_Codigo), 6, 0)));
         }
      }

      protected void GXDLVvSISTEMA_CODIGO_dataJ82( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhum)");
         /* Using cursor H00J82 */
         pr_default.execute(0);
         while ( (pr_default.getStatus(0) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00J82_A127Sistema_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00J82_A129Sistema_Sigla[0]));
            pr_default.readNext(0);
         }
         pr_default.close(0);
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_582( ) ;
         while ( nGXsfl_58_idx <= nRC_GXsfl_58 )
         {
            sendrow_582( ) ;
            nGXsfl_58_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_58_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_58_idx+1));
            sGXsfl_58_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_58_idx), 4, 0)), 4, "0");
            SubsflControlProps_582( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( IGxCollection AV57SDT_Baseline )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GRID_nCurrentRecord = 0;
         RFJ82( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( dynavSistema_codigo.ItemCount > 0 )
         {
            AV5Sistema_Codigo = (int)(NumberUtil.Val( dynavSistema_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV5Sistema_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV5Sistema_Codigo), 6, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFJ82( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavCtlnome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtlnome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlnome_Enabled), 5, 0)));
         edtavCtltipo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtltipo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtltipo_Enabled), 5, 0)));
         edtavCtlder_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtlder_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlder_Enabled), 5, 0)));
         edtavCtlrlr_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtlrlr_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlrlr_Enabled), 5, 0)));
         edtavCtlobservacao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtlobservacao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlobservacao_Enabled), 5, 0)));
      }

      protected void RFJ82( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 58;
         /* Execute user event: E15J82 */
         E15J82 ();
         nGXsfl_58_idx = 1;
         sGXsfl_58_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_58_idx), 4, 0)), 4, "0");
         SubsflControlProps_582( ) ;
         nGXsfl_58_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Visible), 5, 0, ".", "")));
         GridContainer.AddObjectProperty("Height", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Height), 9, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_582( ) ;
            /* Execute user event: E16J82 */
            E16J82 ();
            wbEnd = 58;
            WBJ80( ) ;
         }
         nGXsfl_58_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         return (int)(-1) ;
      }

      protected int subGrid_Recordcount( )
      {
         return (int)(-1) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         return (int)(-1) ;
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(-1) ;
      }

      protected void STRUPJ80( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         edtavCtlnome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtlnome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlnome_Enabled), 5, 0)));
         edtavCtltipo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtltipo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtltipo_Enabled), 5, 0)));
         edtavCtlder_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtlder_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlder_Enabled), 5, 0)));
         edtavCtlrlr_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtlrlr_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlrlr_Enabled), 5, 0)));
         edtavCtlobservacao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtlobservacao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlobservacao_Enabled), 5, 0)));
         GXVvSISTEMA_CODIGO_htmlJ82( ) ;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E14J82 */
         E14J82 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "Sdt_baseline"), AV57SDT_Baseline);
            /* Read variables values. */
            dynavSistema_codigo.Name = dynavSistema_codigo_Internalname;
            dynavSistema_codigo.CurrentValue = cgiGet( dynavSistema_codigo_Internalname);
            AV5Sistema_Codigo = (int)(NumberUtil.Val( cgiGet( dynavSistema_codigo_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV5Sistema_Codigo), 6, 0)));
            if ( ( ( context.localUtil.CToN( cgiGet( edtavPralinha_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavPralinha_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vPRALINHA");
               GX_FocusControl = edtavPralinha_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV6PraLinha = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6PraLinha", StringUtil.LTrim( StringUtil.Str( (decimal)(AV6PraLinha), 4, 0)));
            }
            else
            {
               AV6PraLinha = (short)(context.localUtil.CToN( cgiGet( edtavPralinha_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6PraLinha", StringUtil.LTrim( StringUtil.Str( (decimal)(AV6PraLinha), 4, 0)));
            }
            AV7ColNome = StringUtil.Upper( cgiGet( edtavColnome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7ColNome", AV7ColNome);
            AV8ColTipo = StringUtil.Upper( cgiGet( edtavColtipo_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8ColTipo", AV8ColTipo);
            AV9ColDER = StringUtil.Upper( cgiGet( edtavColder_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9ColDER", AV9ColDER);
            AV10ColRLR = StringUtil.Upper( cgiGet( edtavColrlr_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10ColRLR", AV10ColRLR);
            AV11ColObserv = StringUtil.Upper( cgiGet( edtavColobserv_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11ColObserv", AV11ColObserv);
            AV62Criar = StringUtil.StrToBool( cgiGet( chkavCriar_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62Criar", AV62Criar);
            AV12Blob = cgiGet( edtavBlob_Internalname);
            AV13Aba = cgiGet( edtavAba_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Aba", AV13Aba);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavAreatrabalho_codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavAreatrabalho_codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vAREATRABALHO_CODIGO");
               GX_FocusControl = edtavAreatrabalho_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV56AreaTrabalho_Codigo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV56AreaTrabalho_Codigo), 6, 0)));
            }
            else
            {
               AV56AreaTrabalho_Codigo = (int)(context.localUtil.CToN( cgiGet( edtavAreatrabalho_codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV56AreaTrabalho_Codigo), 6, 0)));
            }
            AV37FileName = cgiGet( edtavFilename_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37FileName", AV37FileName);
            /* Read saved values. */
            nRC_GXsfl_58 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_58"), ",", "."));
            Innewwindow_Width = cgiGet( "INNEWWINDOW_Width");
            Innewwindow_Height = cgiGet( "INNEWWINDOW_Height");
            Innewwindow_Target = cgiGet( "INNEWWINDOW_Target");
            nRC_GXsfl_58 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_58"), ",", "."));
            nGXsfl_58_fel_idx = 0;
            while ( nGXsfl_58_fel_idx < nRC_GXsfl_58 )
            {
               nGXsfl_58_fel_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_58_fel_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_58_fel_idx+1));
               sGXsfl_58_fel_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_58_fel_idx), 4, 0)), 4, "0");
               SubsflControlProps_fel_582( ) ;
               AV65GXV1 = nGXsfl_58_fel_idx;
               if ( ( AV57SDT_Baseline.Count >= AV65GXV1 ) && ( AV65GXV1 > 0 ) )
               {
                  AV57SDT_Baseline.CurrentItem = ((SdtSDT_Baselines_Funcao)AV57SDT_Baseline.Item(AV65GXV1));
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( AV12Blob)) )
               {
                  GXCCtl = "vBLOB_" + sGXsfl_58_fel_idx;
                  GXCCtlgxBlob = GXCCtl + "_gxBlob";
                  AV12Blob = cgiGet( GXCCtlgxBlob);
               }
            }
            if ( nGXsfl_58_fel_idx == 0 )
            {
               nGXsfl_58_idx = 1;
               sGXsfl_58_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_58_idx), 4, 0)), 4, "0");
               SubsflControlProps_582( ) ;
            }
            nGXsfl_58_fel_idx = 1;
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            if ( String.IsNullOrEmpty(StringUtil.RTrim( AV12Blob)) )
            {
               GXCCtlgxBlob = "vBLOB" + "_gxBlob";
               AV12Blob = cgiGet( GXCCtlgxBlob);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E14J82 */
         E14J82 ();
         if (returnInSub) return;
      }

      protected void E14J82( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV58WWPContext) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV37FileName)) )
         {
            bttCarregar_Caption = "Carregar";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttCarregar_Internalname, "Caption", bttCarregar_Caption);
         }
         bttCarregar_Caption = "Analisar";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttCarregar_Internalname, "Caption", bttCarregar_Caption);
         edtavFilename_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFilename_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFilename_Visible), 5, 0)));
         AV6PraLinha = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6PraLinha", StringUtil.LTrim( StringUtil.Str( (decimal)(AV6PraLinha), 4, 0)));
         AV57SDT_Baseline.Clear();
         gx_BV58 = true;
         edtavAreatrabalho_codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAreatrabalho_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAreatrabalho_codigo_Visible), 5, 0)));
         Form.Jscriptsrc.Add("http://code.jquery.com/jquery-latest.js\">") ;
         Form.Headerrawhtml = "<script type=\"text/javascript\">";
         Form.Headerrawhtml = Form.Headerrawhtml+"$(document).ready(function(){";
         Form.Headerrawhtml = Form.Headerrawhtml+"$(\"#vBLOB\").blur(function(){";
         Form.Headerrawhtml = Form.Headerrawhtml+"var nome = $(\"#vBLOB\").val();";
         Form.Headerrawhtml = Form.Headerrawhtml+"if(nome.split('\\\\').length>1) nome = nome.split('\\\\')[nome.split('\\\\').length-1]; else nome = nome.split('/')[nome.split('/').length-1];";
         Form.Headerrawhtml = Form.Headerrawhtml+"$(\"#vFILENAME\").val(nome);";
         Form.Headerrawhtml = Form.Headerrawhtml+"});";
         Form.Headerrawhtml = Form.Headerrawhtml+"});";
         Form.Headerrawhtml = Form.Headerrawhtml+"</script>";
      }

      protected void E15J82( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV58WWPContext) ;
         AV56AreaTrabalho_Codigo = AV58WWPContext.gxTpr_Areatrabalho_codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV56AreaTrabalho_Codigo), 6, 0)));
         subGrid_Visible = ((AV57SDT_Baseline.Count>0) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "GridContainerDiv", "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(subGrid_Visible), 5, 0)));
         bttCarregar_Caption = "Analisar";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttCarregar_Internalname, "Caption", bttCarregar_Caption);
         AV57SDT_Baseline.FromXml(AV59WebSession.Get("SDTBaseline"), "");
         gx_BV58 = true;
         AV54Qtde = (short)(AV57SDT_Baseline.Count);
         bttImportar_Visible = (AV58WWPContext.gxTpr_Insert&&(AV57SDT_Baseline.Count>0) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttImportar_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttImportar_Visible), 5, 0)));
         AV47Ln = 0;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV57SDT_Baseline", AV57SDT_Baseline);
      }

      protected void E11J82( )
      {
         /* 'Enter' Routine */
         AV16Arquivo = AV12Blob;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16Arquivo", AV16Arquivo);
         AV37FileName = StringUtil.Substring( AV37FileName, StringUtil.StringSearchRev( AV37FileName, "\\", -1)+1, 255);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37FileName", AV37FileName);
         AV37FileName = StringUtil.Substring( AV37FileName, 1, StringUtil.StringSearchRev( AV37FileName, ".", -1)-1);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37FileName", AV37FileName);
         AV51Ok = true;
         if ( (0==AV5Sistema_Codigo) )
         {
            GX_msglist.addItem("Falta selecionar o Sistema!");
            AV51Ok = false;
         }
         else if ( String.IsNullOrEmpty(StringUtil.RTrim( AV7ColNome)) )
         {
            GX_msglist.addItem("Falta informar a coluna dos Nome das Fun��es!");
            AV51Ok = false;
         }
         else if ( String.IsNullOrEmpty(StringUtil.RTrim( AV8ColTipo)) )
         {
            GX_msglist.addItem("Falta informar a coluna do Tipo das Fun��es!");
            AV51Ok = false;
         }
         else if ( String.IsNullOrEmpty(StringUtil.RTrim( AV9ColDER)) )
         {
            GX_msglist.addItem("Falta informar a coluna dos DER!");
            AV51Ok = false;
         }
         else if ( String.IsNullOrEmpty(StringUtil.RTrim( AV10ColRLR)) )
         {
            GX_msglist.addItem("Falta informar a coluna dos RLR!");
            AV51Ok = false;
         }
         else if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11ColObserv)) )
         {
            GX_msglist.addItem("Falta informar a coluna das Observa��es!");
            AV51Ok = false;
         }
         else if ( StringUtil.StringSearch( AV16Arquivo, "xlsx", 1) == 0 )
         {
            GX_msglist.addItem("Arquivo esperado para processar: Excel com extens�o .xlsx!");
            AV51Ok = false;
         }
         if ( AV51Ok )
         {
            AV33ErrCod = AV35ExcelDocument.Open(AV16Arquivo);
            if ( AV33ErrCod != 0 )
            {
               AV35ExcelDocument.Close();
               GX_msglist.addItem(AV35ExcelDocument.ErrDescription);
            }
            else
            {
               AV33ErrCod = AV35ExcelDocument.Close();
               if ( String.IsNullOrEmpty(StringUtil.RTrim( StringUtil.Substring( AV7ColNome, 2, 1))) )
               {
                  AV21ColNomen = (short)(StringUtil.Asc( AV7ColNome)-64);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ColNomen", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21ColNomen), 4, 0)));
               }
               else
               {
                  AV21ColNomen = (short)(StringUtil.Asc( StringUtil.Substring( AV7ColNome, 2, 1))-64+26);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ColNomen", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21ColNomen), 4, 0)));
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( StringUtil.Substring( AV8ColTipo, 2, 1))) )
               {
                  AV24ColTipon = (short)(StringUtil.Asc( AV8ColTipo)-64);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24ColTipon", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24ColTipon), 4, 0)));
               }
               else
               {
                  AV24ColTipon = (short)(StringUtil.Asc( StringUtil.Substring( AV8ColTipo, 2, 1))-64+26);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24ColTipon", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24ColTipon), 4, 0)));
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( StringUtil.Substring( AV9ColDER, 2, 1))) )
               {
                  AV18ColDERn = (short)(StringUtil.Asc( AV9ColDER)-64);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18ColDERn", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18ColDERn), 4, 0)));
               }
               else
               {
                  AV18ColDERn = (short)(StringUtil.Asc( StringUtil.Substring( AV9ColDER, 2, 1))-64+26);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18ColDERn", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18ColDERn), 4, 0)));
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( StringUtil.Substring( AV10ColRLR, 2, 1))) )
               {
                  AV23ColRLRn = (short)(StringUtil.Asc( AV10ColRLR)-64);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23ColRLRn", StringUtil.LTrim( StringUtil.Str( (decimal)(AV23ColRLRn), 4, 0)));
               }
               else
               {
                  AV23ColRLRn = (short)(StringUtil.Asc( StringUtil.Substring( AV10ColRLR, 2, 1))-64+26);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23ColRLRn", StringUtil.LTrim( StringUtil.Str( (decimal)(AV23ColRLRn), 4, 0)));
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( StringUtil.Substring( AV11ColObserv, 2, 1))) )
               {
                  AV22ColObservn = (short)(StringUtil.Asc( AV11ColObserv)-64);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22ColObservn", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22ColObservn), 4, 0)));
               }
               else
               {
                  AV22ColObservn = (short)(StringUtil.Asc( StringUtil.Substring( AV11ColObserv, 2, 1))-64+26);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22ColObservn", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22ColObservn), 4, 0)));
               }
               /* Execute user subroutine: 'CARREGASDTBASELINE' */
               S112 ();
               if (returnInSub) return;
            }
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV57SDT_Baseline", AV57SDT_Baseline);
         nGXsfl_58_bak_idx = nGXsfl_58_idx;
         gxgrGrid_refresh( AV57SDT_Baseline) ;
         nGXsfl_58_idx = nGXsfl_58_bak_idx;
         sGXsfl_58_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_58_idx), 4, 0)), 4, "0");
         SubsflControlProps_582( ) ;
      }

      protected void E12J82( )
      {
         /* Sistema_codigo_Click Routine */
         AV61Sistema_Sigla = dynavSistema_codigo.Description;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61Sistema_Sigla", AV61Sistema_Sigla);
      }

      protected void S112( )
      {
         /* 'CARREGASDTBASELINE' Routine */
         AV57SDT_Baseline.Clear();
         gx_BV58 = true;
         Innewwindow_Target = formatLink("aprc_carregasdtbaseline.aspx") + "?" + UrlEncode("" +AV6PraLinha) + "," + UrlEncode(StringUtil.RTrim(AV16Arquivo)) + "," + UrlEncode(StringUtil.RTrim(AV13Aba)) + "," + UrlEncode("" +AV21ColNomen) + "," + UrlEncode("" +AV24ColTipon) + "," + UrlEncode("" +AV18ColDERn) + "," + UrlEncode("" +AV23ColRLRn) + "," + UrlEncode("" +AV22ColObservn) + "," + UrlEncode(StringUtil.RTrim(AV37FileName)) + "," + UrlEncode("" +AV5Sistema_Codigo) + "," + UrlEncode(StringUtil.RTrim(AV61Sistema_Sigla));
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Innewwindow_Internalname, "Target", Innewwindow_Target);
         this.executeUsercontrolMethod("", false, "INNEWWINDOWContainer", "OpenWindow", "", new Object[] {});
         context.DoAjaxRefresh();
      }

      protected void E13J82( )
      {
         /* 'Importar' Routine */
         Innewwindow_Target = formatLink("aprc_inserirsdtbaseline.aspx") + "?" + UrlEncode("" +AV5Sistema_Codigo) + "," + UrlEncode(StringUtil.RTrim(AV61Sistema_Sigla)) + "," + UrlEncode(StringUtil.RTrim(AV37FileName)) + "," + UrlEncode(StringUtil.RTrim(AV13Aba)) + "," + UrlEncode(StringUtil.BoolToStr(AV62Criar));
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Innewwindow_Internalname, "Target", Innewwindow_Target);
         this.executeUsercontrolMethod("", false, "INNEWWINDOWContainer", "OpenWindow", "", new Object[] {});
         AV57SDT_Baseline.Clear();
         gx_BV58 = true;
         AV54Qtde = 0;
         context.DoAjaxRefresh();
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV57SDT_Baseline", AV57SDT_Baseline);
         nGXsfl_58_bak_idx = nGXsfl_58_idx;
         gxgrGrid_refresh( AV57SDT_Baseline) ;
         nGXsfl_58_idx = nGXsfl_58_bak_idx;
         sGXsfl_58_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_58_idx), 4, 0)), 4, "0");
         SubsflControlProps_582( ) ;
      }

      private void E16J82( )
      {
         /* Grid_Load Routine */
         AV65GXV1 = 1;
         while ( AV65GXV1 <= AV57SDT_Baseline.Count )
         {
            AV57SDT_Baseline.CurrentItem = ((SdtSDT_Baselines_Funcao)AV57SDT_Baseline.Item(AV65GXV1));
            /* Load Method */
            if ( wbStart != -1 )
            {
               wbStart = 58;
            }
            sendrow_582( ) ;
            if ( isFullAjaxMode( ) && ( nGXsfl_58_Refreshing == 0 ) )
            {
               context.DoAjaxLoad(58, GridRow);
            }
            AV65GXV1 = (short)(AV65GXV1+1);
         }
      }

      protected void wb_table1_2_J82( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 2, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td colspan=\"2\"  class='Table'>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td rowspan=\"8\"  style=\""+CSSHelper.Prettify( "vertical-align:top;width:30%")+"\">") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            wb_table2_9_J82( true) ;
         }
         else
         {
            wb_table2_9_J82( false) ;
         }
         return  ;
      }

      protected void wb_table2_9_J82e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td colspan=\"2\"  class='Table'>") ;
            wb_table3_12_J82( true) ;
         }
         else
         {
            wb_table3_12_J82( false) ;
         }
         return  ;
      }

      protected void wb_table3_12_J82e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:1px")+"\" class='Table'>") ;
            ClassString = "Image";
            StyleString = "";
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'" + sGXsfl_58_idx + "',0)\"";
            edtavBlob_Filetype = "tmp";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavBlob_Internalname, "Filetype", edtavBlob_Filetype);
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12Blob)) )
            {
               gxblobfileaux.Source = AV12Blob;
               if ( ! gxblobfileaux.HasExtension() || ( StringUtil.StrCmp(edtavBlob_Filetype, "tmp") != 0 ) )
               {
                  gxblobfileaux.SetExtension(StringUtil.Trim( edtavBlob_Filetype));
               }
               if ( gxblobfileaux.ErrCode == 0 )
               {
                  AV12Blob = gxblobfileaux.GetAbsoluteName();
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavBlob_Internalname, "URL", context.PathToRelativeUrl( AV12Blob));
                  edtavBlob_Filetype = gxblobfileaux.GetExtension();
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavBlob_Internalname, "Filetype", edtavBlob_Filetype);
               }
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavBlob_Internalname, "URL", context.PathToRelativeUrl( AV12Blob));
            }
            GxWebStd.gx_blob_field( context, edtavBlob_Internalname, StringUtil.RTrim( AV12Blob), context.PathToRelativeUrl( AV12Blob), (String.IsNullOrEmpty(StringUtil.RTrim( edtavBlob_Contenttype)) ? context.GetContentType( (String.IsNullOrEmpty(StringUtil.RTrim( edtavBlob_Filetype)) ? AV12Blob : edtavBlob_Filetype)) : edtavBlob_Contenttype), false, "", edtavBlob_Parameters, 0, 1, 1, "", "", 0, 0, 250, "px", 18, "px", 0, 0, 0, edtavBlob_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", StyleString, ClassString, "", ""+TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,49);\"", "", "", "HLP_WP_ImportaBaseline.htm");
            context.WriteHtmlText( "&nbsp;&nbsp;&nbsp;&nbsp; ") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "width:712px")+"\" class=''>") ;
            context.WriteHtmlText( "&nbsp;&nbsp;&nbsp;&nbsp; ") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock27_Internalname, "Nome da Aba:", "", "", lblTextblock27_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ImportaBaseline.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 52,'',false,'" + sGXsfl_58_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavAba_Internalname, StringUtil.RTrim( AV13Aba), StringUtil.RTrim( context.localUtil.Format( AV13Aba, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,52);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavAba_Jsonclick, 0, "AttributeCustom", "", "", "", 1, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_ImportaBaseline.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td colspan=\"2\"  class='Table'>") ;
            wb_table4_55_J82( true) ;
         }
         else
         {
            wb_table4_55_J82( false) ;
         }
         return  ;
      }

      protected void wb_table4_55_J82e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td colspan=\"2\"  class='Table'>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td colspan=\"2\"  class='Table'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 68,'',false,'',0)\"";
            ClassString = "Button";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttCarregar_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(58), 2, 0)+","+"null"+");", bttCarregar_Caption, bttCarregar_Jsonclick, 5, "Carregar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'ENTER\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_ImportaBaseline.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'',false,'',0)\"";
            ClassString = "Button";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttImportar_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(58), 2, 0)+","+"null"+");", "Importar", bttImportar_Jsonclick, 5, "Importar", "", StyleString, ClassString, bttImportar_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'IMPORTAR\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_ImportaBaseline.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_J82e( true) ;
         }
         else
         {
            wb_table1_2_J82e( false) ;
         }
      }

      protected void wb_table4_55_J82( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegrid_Internalname, tblTablegrid_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"58\">") ;
               sStyleString = "";
               if ( subGrid_Visible == 0 )
               {
                  sStyleString = sStyleString + "display:none;";
               }
               sStyleString = sStyleString + " height: " + StringUtil.LTrim( StringUtil.Str( (decimal)(2), 10, 0)) + "" + ";";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWith", 0, "", "", 1, 2, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Nome") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(49), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Tipo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(42), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "DER") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(42), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "RLR") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Observacao") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Visible), 5, 0, ".", "")));
               GridContainer.AddObjectProperty("Height", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Height), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavCtlnome_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavCtltipo_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavCtlder_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavCtlrlr_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavCtlobservacao_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 58 )
         {
            wbEnd = 0;
            nRC_GXsfl_58 = (short)(nGXsfl_58_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               AV65GXV1 = nGXsfl_58_idx;
               if ( subGrid_Visible != 0 )
               {
                  sStyleString = "";
               }
               else
               {
                  sStyleString = " style=\"display:none;\"";
               }
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_55_J82e( true) ;
         }
         else
         {
            wb_table4_55_J82e( false) ;
         }
      }

      protected void wb_table3_12_J82( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTbldadosdmn_Internalname, tblTbldadosdmn_Internalname, "", "", 0, "", "", 1, 5, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock28_Internalname, "Sistema:", "", "", lblTextblock28_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ImportaBaseline.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"12\"  class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'" + sGXsfl_58_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavSistema_codigo, dynavSistema_codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV5Sistema_Codigo), 6, 0)), 1, dynavSistema_codigo_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVSISTEMA_CODIGO.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,17);\"", "", true, "HLP_WP_ImportaBaseline.htm");
            dynavSistema_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV5Sistema_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavSistema_codigo_Internalname, "Values", (String)(dynavSistema_codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock23_Internalname, "1� linha:", "", "", lblTextblock23_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ImportaBaseline.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 22,'',false,'" + sGXsfl_58_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavPralinha_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV6PraLinha), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV6PraLinha), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,22);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavPralinha_Jsonclick, 0, "AttributeCustom", "", "", "", 1, 1, 0, "text", "", 35, "px", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_WP_ImportaBaseline.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            context.WriteHtmlText( "&nbsp;") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock18_Internalname, "COLUNAS ::  Nome �", "", "", lblTextblock18_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ImportaBaseline.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 26,'',false,'" + sGXsfl_58_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavColnome_Internalname, StringUtil.RTrim( AV7ColNome), StringUtil.RTrim( context.localUtil.Format( AV7ColNome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,26);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavColnome_Jsonclick, 0, "AttributeCustom", "", "", "", 1, 1, 0, "text", "", 30, "px", 1, "row", 2, 0, 0, 0, 1, 0, 0, true, "", "left", true, "HLP_WP_ImportaBaseline.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock19_Internalname, "Tipo �", "", "", lblTextblock19_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ImportaBaseline.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 29,'',false,'" + sGXsfl_58_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavColtipo_Internalname, StringUtil.RTrim( AV8ColTipo), StringUtil.RTrim( context.localUtil.Format( AV8ColTipo, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,29);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavColtipo_Jsonclick, 0, "AttributeCustom", "", "", "", 1, 1, 0, "text", "", 30, "px", 1, "row", 2, 0, 0, 0, 1, 0, 0, true, "", "left", true, "HLP_WP_ImportaBaseline.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock25_Internalname, "DER �", "", "", lblTextblock25_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ImportaBaseline.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'" + sGXsfl_58_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavColder_Internalname, StringUtil.RTrim( AV9ColDER), StringUtil.RTrim( context.localUtil.Format( AV9ColDER, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,33);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavColder_Jsonclick, 0, "AttributeCustom", "", "", "", 1, 1, 0, "text", "", 30, "px", 1, "row", 2, 0, 0, 0, 1, 0, 0, true, "", "left", true, "HLP_WP_ImportaBaseline.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbpflfm_Internalname, "RLR �", "", "", lblTbpflfm_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ImportaBaseline.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 37,'',false,'" + sGXsfl_58_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavColrlr_Internalname, StringUtil.RTrim( AV10ColRLR), StringUtil.RTrim( context.localUtil.Format( AV10ColRLR, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,37);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavColrlr_Jsonclick, 0, "AttributeCustom", "", "", "", 1, 1, 0, "text", "", 30, "px", 1, "row", 2, 0, 0, 0, 1, 0, 0, true, "", "left", true, "HLP_WP_ImportaBaseline.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbpflfm2_Internalname, "Observa��es �", "", "", lblTbpflfm2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ImportaBaseline.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"3\" >") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 41,'',false,'" + sGXsfl_58_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavColobserv_Internalname, StringUtil.RTrim( AV11ColObserv), StringUtil.RTrim( context.localUtil.Format( AV11ColObserv, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,41);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavColobserv_Jsonclick, 0, "AttributeCustom", "", "", "", 1, 1, 0, "text", "", 30, "px", 1, "row", 2, 0, 0, 0, 1, 0, 0, true, "", "left", true, "HLP_WP_ImportaBaseline.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"13\" >") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'" + sGXsfl_58_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavCriar_Internalname, StringUtil.BoolToStr( AV62Criar), "", "", 1, 1, "true", "Criar RLR e DER", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(44, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,44);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_12_J82e( true) ;
         }
         else
         {
            wb_table3_12_J82e( false) ;
         }
      }

      protected void wb_table2_9_J82( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTblsrvvnc_Internalname, tblTblsrvvnc_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody></tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_9_J82e( true) ;
         }
         else
         {
            wb_table2_9_J82e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAJ82( ) ;
         WSJ82( ) ;
         WEJ82( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?2249787");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202042823442819");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wp_importabaseline.js", "?202042823442819");
         context.AddJavascriptSource("Window/InNewWindowRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_582( )
      {
         edtavCtlnome_Internalname = "CTLNOME_"+sGXsfl_58_idx;
         edtavCtltipo_Internalname = "CTLTIPO_"+sGXsfl_58_idx;
         edtavCtlder_Internalname = "CTLDER_"+sGXsfl_58_idx;
         edtavCtlrlr_Internalname = "CTLRLR_"+sGXsfl_58_idx;
         edtavCtlobservacao_Internalname = "CTLOBSERVACAO_"+sGXsfl_58_idx;
      }

      protected void SubsflControlProps_fel_582( )
      {
         edtavCtlnome_Internalname = "CTLNOME_"+sGXsfl_58_fel_idx;
         edtavCtltipo_Internalname = "CTLTIPO_"+sGXsfl_58_fel_idx;
         edtavCtlder_Internalname = "CTLDER_"+sGXsfl_58_fel_idx;
         edtavCtlrlr_Internalname = "CTLRLR_"+sGXsfl_58_fel_idx;
         edtavCtlobservacao_Internalname = "CTLOBSERVACAO_"+sGXsfl_58_fel_idx;
      }

      protected void sendrow_582( )
      {
         SubsflControlProps_582( ) ;
         WBJ80( ) ;
         GridRow = GXWebRow.GetNew(context,GridContainer);
         if ( subGrid_Backcolorstyle == 0 )
         {
            /* None style subfile background logic. */
            subGrid_Backstyle = 0;
            if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
            {
               subGrid_Linesclass = subGrid_Class+"Odd";
            }
         }
         else if ( subGrid_Backcolorstyle == 1 )
         {
            /* Uniform style subfile background logic. */
            subGrid_Backstyle = 0;
            subGrid_Backcolor = subGrid_Allbackcolor;
            if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
            {
               subGrid_Linesclass = subGrid_Class+"Uniform";
            }
         }
         else if ( subGrid_Backcolorstyle == 2 )
         {
            /* Header style subfile background logic. */
            subGrid_Backstyle = 1;
            if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
            {
               subGrid_Linesclass = subGrid_Class+"Odd";
            }
            subGrid_Backcolor = (int)(0xFFFFFF);
         }
         else if ( subGrid_Backcolorstyle == 3 )
         {
            /* Report style subfile background logic. */
            subGrid_Backstyle = 1;
            if ( ((int)((nGXsfl_58_idx) % (2))) == 0 )
            {
               subGrid_Backcolor = (int)(0xFFFFFF);
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Even";
               }
            }
            else
            {
               subGrid_Backcolor = (int)(0xFFFFFF);
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
         }
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<tr ") ;
            context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
            context.WriteHtmlText( " gxrow=\""+sGXsfl_58_idx+"\">") ;
         }
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavCtlnome_Internalname,StringUtil.RTrim( ((SdtSDT_Baselines_Funcao)AV57SDT_Baseline.Item(AV65GXV1)).gxTpr_Nome),(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavCtlnome_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavCtlnome_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)200,(short)0,(short)0,(short)58,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavCtltipo_Internalname,StringUtil.RTrim( ((SdtSDT_Baselines_Funcao)AV57SDT_Baseline.Item(AV65GXV1)).gxTpr_Tipo),(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavCtltipo_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavCtltipo_Enabled,(short)0,(String)"text",(String)"",(short)49,(String)"px",(short)17,(String)"px",(short)3,(short)0,(short)0,(short)58,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavCtlder_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(((SdtSDT_Baselines_Funcao)AV57SDT_Baseline.Item(AV65GXV1)).gxTpr_Der), 4, 0, ",", "")),((edtavCtlder_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(((SdtSDT_Baselines_Funcao)AV57SDT_Baseline.Item(AV65GXV1)).gxTpr_Der), "ZZZ9")) : context.localUtil.Format( (decimal)(((SdtSDT_Baselines_Funcao)AV57SDT_Baseline.Item(AV65GXV1)).gxTpr_Der), "ZZZ9")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavCtlder_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavCtlder_Enabled,(short)0,(String)"text",(String)"",(short)42,(String)"px",(short)17,(String)"px",(short)4,(short)0,(short)0,(short)58,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavCtlrlr_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(((SdtSDT_Baselines_Funcao)AV57SDT_Baseline.Item(AV65GXV1)).gxTpr_Rlr), 4, 0, ",", "")),((edtavCtlrlr_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(((SdtSDT_Baselines_Funcao)AV57SDT_Baseline.Item(AV65GXV1)).gxTpr_Rlr), "ZZZ9")) : context.localUtil.Format( (decimal)(((SdtSDT_Baselines_Funcao)AV57SDT_Baseline.Item(AV65GXV1)).gxTpr_Rlr), "ZZZ9")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavCtlrlr_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavCtlrlr_Enabled,(short)0,(String)"text",(String)"",(short)42,(String)"px",(short)17,(String)"px",(short)4,(short)0,(short)0,(short)58,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavCtlobservacao_Internalname,((SdtSDT_Baselines_Funcao)AV57SDT_Baseline.Item(AV65GXV1)).gxTpr_Observacao,((SdtSDT_Baselines_Funcao)AV57SDT_Baseline.Item(AV65GXV1)).gxTpr_Observacao,(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavCtlobservacao_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavCtlobservacao_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)11,(String)"px",(short)1000,(short)0,(short)0,(short)58,(short)1,(short)0,(short)-1,(bool)true,(String)"",(String)"left",(bool)false});
         GXCCtl = "vBLOB_" + sGXsfl_58_idx;
         GXCCtlgxBlob = GXCCtl + "_gxBlob";
         GxWebStd.gx_hidden_field( context, GXCCtlgxBlob, AV12Blob);
         GridContainer.AddRow(GridRow);
         nGXsfl_58_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_58_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_58_idx+1));
         sGXsfl_58_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_58_idx), 4, 0)), 4, "0");
         SubsflControlProps_582( ) ;
         /* End function sendrow_582 */
      }

      protected void init_default_properties( )
      {
         tblTblsrvvnc_Internalname = "TBLSRVVNC";
         lblTextblock28_Internalname = "TEXTBLOCK28";
         dynavSistema_codigo_Internalname = "vSISTEMA_CODIGO";
         lblTextblock23_Internalname = "TEXTBLOCK23";
         edtavPralinha_Internalname = "vPRALINHA";
         lblTextblock18_Internalname = "TEXTBLOCK18";
         edtavColnome_Internalname = "vCOLNOME";
         lblTextblock19_Internalname = "TEXTBLOCK19";
         edtavColtipo_Internalname = "vCOLTIPO";
         lblTextblock25_Internalname = "TEXTBLOCK25";
         edtavColder_Internalname = "vCOLDER";
         lblTbpflfm_Internalname = "TBPFLFM";
         edtavColrlr_Internalname = "vCOLRLR";
         lblTbpflfm2_Internalname = "TBPFLFM2";
         edtavColobserv_Internalname = "vCOLOBSERV";
         chkavCriar_Internalname = "vCRIAR";
         tblTbldadosdmn_Internalname = "TBLDADOSDMN";
         edtavBlob_Internalname = "vBLOB";
         lblTextblock27_Internalname = "TEXTBLOCK27";
         edtavAba_Internalname = "vABA";
         edtavCtlnome_Internalname = "CTLNOME";
         edtavCtltipo_Internalname = "CTLTIPO";
         edtavCtlder_Internalname = "CTLDER";
         edtavCtlrlr_Internalname = "CTLRLR";
         edtavCtlobservacao_Internalname = "CTLOBSERVACAO";
         tblTablegrid_Internalname = "TABLEGRID";
         bttCarregar_Internalname = "CARREGAR";
         bttImportar_Internalname = "IMPORTAR";
         tblTable1_Internalname = "TABLE1";
         Innewwindow_Internalname = "INNEWWINDOW";
         edtavAreatrabalho_codigo_Internalname = "vAREATRABALHO_CODIGO";
         edtavFilename_Internalname = "vFILENAME";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtavCtlobservacao_Jsonclick = "";
         edtavCtlrlr_Jsonclick = "";
         edtavCtlder_Jsonclick = "";
         edtavCtltipo_Jsonclick = "";
         edtavCtlnome_Jsonclick = "";
         edtavColobserv_Jsonclick = "";
         edtavColrlr_Jsonclick = "";
         edtavColder_Jsonclick = "";
         edtavColtipo_Jsonclick = "";
         edtavColnome_Jsonclick = "";
         edtavPralinha_Jsonclick = "";
         dynavSistema_codigo_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavCtlobservacao_Enabled = 0;
         edtavCtlrlr_Enabled = 0;
         edtavCtlder_Enabled = 0;
         edtavCtltipo_Enabled = 0;
         edtavCtlnome_Enabled = 0;
         subGrid_Class = "WorkWith";
         subGrid_Visible = 1;
         bttImportar_Visible = 1;
         edtavAba_Jsonclick = "";
         edtavBlob_Jsonclick = "";
         edtavBlob_Parameters = "";
         edtavBlob_Contenttype = "";
         edtavBlob_Filetype = "";
         bttCarregar_Caption = "Carregar";
         subGrid_Height = 2;
         subGrid_Backcolorstyle = 3;
         edtavCtlobservacao_Enabled = -1;
         edtavCtlrlr_Enabled = -1;
         edtavCtlder_Enabled = -1;
         edtavCtltipo_Enabled = -1;
         edtavCtlnome_Enabled = -1;
         chkavCriar.Caption = "";
         edtavFilename_Jsonclick = "";
         edtavFilename_Enabled = 1;
         edtavFilename_Visible = 1;
         edtavAreatrabalho_codigo_Jsonclick = "";
         edtavAreatrabalho_codigo_Enabled = 1;
         edtavAreatrabalho_codigo_Visible = 1;
         dynavSistema_codigo.Description = "";
         Innewwindow_Target = "";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Importa Baseline";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'AV57SDT_Baseline',fld:'vSDT_BASELINE',grid:58,pic:'',nv:null}],oparms:[{av:'AV56AreaTrabalho_Codigo',fld:'vAREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'subGrid_Visible',ctrl:'GRID',prop:'Visible'},{ctrl:'CARREGAR',prop:'Caption'},{av:'AV57SDT_Baseline',fld:'vSDT_BASELINE',grid:58,pic:'',nv:null},{ctrl:'IMPORTAR',prop:'Visible'}]}");
         setEventMetadata("'ENTER'","{handler:'E11J82',iparms:[{av:'AV12Blob',fld:'vBLOB',pic:'',nv:''},{av:'AV37FileName',fld:'vFILENAME',pic:'',nv:''},{av:'AV5Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV7ColNome',fld:'vCOLNOME',pic:'@!',nv:''},{av:'AV8ColTipo',fld:'vCOLTIPO',pic:'@!',nv:''},{av:'AV9ColDER',fld:'vCOLDER',pic:'@!',nv:''},{av:'AV10ColRLR',fld:'vCOLRLR',pic:'@!',nv:''},{av:'AV11ColObserv',fld:'vCOLOBSERV',pic:'@!',nv:''},{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'AV57SDT_Baseline',fld:'vSDT_BASELINE',grid:58,pic:'',nv:null},{av:'AV6PraLinha',fld:'vPRALINHA',pic:'ZZZ9',nv:0},{av:'AV16Arquivo',fld:'vARQUIVO',pic:'',nv:''},{av:'AV13Aba',fld:'vABA',pic:'',nv:''},{av:'AV21ColNomen',fld:'vCOLNOMEN',pic:'ZZZ9',nv:0},{av:'AV24ColTipon',fld:'vCOLTIPON',pic:'ZZZ9',nv:0},{av:'AV18ColDERn',fld:'vCOLDERN',pic:'ZZZ9',nv:0},{av:'AV23ColRLRn',fld:'vCOLRLRN',pic:'ZZZ9',nv:0},{av:'AV22ColObservn',fld:'vCOLOBSERVN',pic:'ZZZ9',nv:0},{av:'AV61Sistema_Sigla',fld:'vSISTEMA_SIGLA',pic:'@!',nv:''}],oparms:[{av:'AV16Arquivo',fld:'vARQUIVO',pic:'',nv:''},{av:'AV37FileName',fld:'vFILENAME',pic:'',nv:''},{av:'AV21ColNomen',fld:'vCOLNOMEN',pic:'ZZZ9',nv:0},{av:'AV24ColTipon',fld:'vCOLTIPON',pic:'ZZZ9',nv:0},{av:'AV18ColDERn',fld:'vCOLDERN',pic:'ZZZ9',nv:0},{av:'AV23ColRLRn',fld:'vCOLRLRN',pic:'ZZZ9',nv:0},{av:'AV22ColObservn',fld:'vCOLOBSERVN',pic:'ZZZ9',nv:0},{av:'AV57SDT_Baseline',fld:'vSDT_BASELINE',grid:58,pic:'',nv:null},{av:'Innewwindow_Target',ctrl:'INNEWWINDOW',prop:'Target'}]}");
         setEventMetadata("VSISTEMA_CODIGO.CLICK","{handler:'E12J82',iparms:[{av:'dynavSistema_codigo'}],oparms:[{av:'AV61Sistema_Sigla',fld:'vSISTEMA_SIGLA',pic:'@!',nv:''}]}");
         setEventMetadata("'IMPORTAR'","{handler:'E13J82',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'AV57SDT_Baseline',fld:'vSDT_BASELINE',grid:58,pic:'',nv:null},{av:'AV5Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV61Sistema_Sigla',fld:'vSISTEMA_SIGLA',pic:'@!',nv:''},{av:'AV37FileName',fld:'vFILENAME',pic:'',nv:''},{av:'AV13Aba',fld:'vABA',pic:'',nv:''},{av:'AV62Criar',fld:'vCRIAR',pic:'',nv:false}],oparms:[{av:'Innewwindow_Target',ctrl:'INNEWWINDOW',prop:'Target'},{av:'AV57SDT_Baseline',fld:'vSDT_BASELINE',grid:58,pic:'',nv:null}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV57SDT_Baseline = new GxObjectCollection( context, "SDT_Baselines.Funcao", "GxEv3Up14_MeetrikaVs3", "SdtSDT_Baselines_Funcao", "GeneXus.Programs");
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV16Arquivo = "";
         AV61Sistema_Sigla = "";
         GXCCtlgxBlob = "";
         AV12Blob = "";
         Innewwindow_Width = "";
         Innewwindow_Height = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         TempTags = "";
         AV37FileName = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         GXCCtl = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         scmdbuf = "";
         H00J82_A127Sistema_Codigo = new int[1] ;
         H00J82_A129Sistema_Sigla = new String[] {""} ;
         GridContainer = new GXWebGrid( context);
         AV7ColNome = "";
         AV8ColTipo = "";
         AV9ColDER = "";
         AV10ColRLR = "";
         AV11ColObserv = "";
         AV13Aba = "";
         AV58WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV59WebSession = context.GetSession();
         AV35ExcelDocument = new ExcelDocumentI();
         GridRow = new GXWebRow();
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         gxblobfileaux = new GxFile(context.GetPhysicalPath());
         lblTextblock27_Jsonclick = "";
         bttCarregar_Jsonclick = "";
         bttImportar_Jsonclick = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblTextblock28_Jsonclick = "";
         lblTextblock23_Jsonclick = "";
         lblTextblock18_Jsonclick = "";
         lblTextblock19_Jsonclick = "";
         lblTextblock25_Jsonclick = "";
         lblTbpflfm_Jsonclick = "";
         lblTbpflfm2_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wp_importabaseline__default(),
            new Object[][] {
                new Object[] {
               H00J82_A127Sistema_Codigo, H00J82_A129Sistema_Sigla
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavCtlnome_Enabled = 0;
         edtavCtltipo_Enabled = 0;
         edtavCtlder_Enabled = 0;
         edtavCtlrlr_Enabled = 0;
         edtavCtlobservacao_Enabled = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_58 ;
      private short nGXsfl_58_idx=1 ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short AV21ColNomen ;
      private short AV24ColTipon ;
      private short AV18ColDERn ;
      private short AV23ColRLRn ;
      private short AV22ColObservn ;
      private short wbEnd ;
      private short wbStart ;
      private short AV65GXV1 ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_58_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short AV6PraLinha ;
      private short nGXsfl_58_fel_idx=1 ;
      private short AV54Qtde ;
      private short AV47Ln ;
      private short GRID_nEOF ;
      private short AV33ErrCod ;
      private short nGXsfl_58_bak_idx=1 ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV56AreaTrabalho_Codigo ;
      private int edtavAreatrabalho_codigo_Enabled ;
      private int edtavAreatrabalho_codigo_Visible ;
      private int edtavFilename_Visible ;
      private int edtavFilename_Enabled ;
      private int gxdynajaxindex ;
      private int AV5Sistema_Codigo ;
      private int subGrid_Islastpage ;
      private int edtavCtlnome_Enabled ;
      private int edtavCtltipo_Enabled ;
      private int edtavCtlder_Enabled ;
      private int edtavCtlrlr_Enabled ;
      private int edtavCtlobservacao_Enabled ;
      private int subGrid_Visible ;
      private int subGrid_Height ;
      private int bttImportar_Visible ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nCurrentRecord ;
      private long GRID_nFirstRecordOnPage ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_58_idx="0001" ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String AV16Arquivo ;
      private String AV61Sistema_Sigla ;
      private String GXCCtlgxBlob ;
      private String Innewwindow_Width ;
      private String Innewwindow_Height ;
      private String Innewwindow_Target ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String edtavAreatrabalho_codigo_Internalname ;
      private String edtavAreatrabalho_codigo_Jsonclick ;
      private String edtavFilename_Internalname ;
      private String AV37FileName ;
      private String edtavFilename_Jsonclick ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String GXCCtl ;
      private String chkavCriar_Internalname ;
      private String dynavSistema_codigo_Internalname ;
      private String gxwrpcisep ;
      private String scmdbuf ;
      private String edtavCtlnome_Internalname ;
      private String edtavCtltipo_Internalname ;
      private String edtavCtlder_Internalname ;
      private String edtavCtlrlr_Internalname ;
      private String edtavCtlobservacao_Internalname ;
      private String edtavPralinha_Internalname ;
      private String AV7ColNome ;
      private String edtavColnome_Internalname ;
      private String AV8ColTipo ;
      private String edtavColtipo_Internalname ;
      private String AV9ColDER ;
      private String edtavColder_Internalname ;
      private String AV10ColRLR ;
      private String edtavColrlr_Internalname ;
      private String AV11ColObserv ;
      private String edtavColobserv_Internalname ;
      private String edtavBlob_Internalname ;
      private String AV13Aba ;
      private String edtavAba_Internalname ;
      private String sGXsfl_58_fel_idx="0001" ;
      private String bttCarregar_Caption ;
      private String bttCarregar_Internalname ;
      private String bttImportar_Internalname ;
      private String Innewwindow_Internalname ;
      private String sStyleString ;
      private String tblTable1_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String edtavBlob_Filetype ;
      private String edtavBlob_Contenttype ;
      private String edtavBlob_Parameters ;
      private String edtavBlob_Jsonclick ;
      private String lblTextblock27_Internalname ;
      private String lblTextblock27_Jsonclick ;
      private String edtavAba_Jsonclick ;
      private String bttCarregar_Jsonclick ;
      private String bttImportar_Jsonclick ;
      private String tblTablegrid_Internalname ;
      private String subGrid_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTbldadosdmn_Internalname ;
      private String lblTextblock28_Internalname ;
      private String lblTextblock28_Jsonclick ;
      private String dynavSistema_codigo_Jsonclick ;
      private String lblTextblock23_Internalname ;
      private String lblTextblock23_Jsonclick ;
      private String edtavPralinha_Jsonclick ;
      private String lblTextblock18_Internalname ;
      private String lblTextblock18_Jsonclick ;
      private String edtavColnome_Jsonclick ;
      private String lblTextblock19_Internalname ;
      private String lblTextblock19_Jsonclick ;
      private String edtavColtipo_Jsonclick ;
      private String lblTextblock25_Internalname ;
      private String lblTextblock25_Jsonclick ;
      private String edtavColder_Jsonclick ;
      private String lblTbpflfm_Internalname ;
      private String lblTbpflfm_Jsonclick ;
      private String edtavColrlr_Jsonclick ;
      private String lblTbpflfm2_Internalname ;
      private String lblTbpflfm2_Jsonclick ;
      private String edtavColobserv_Jsonclick ;
      private String tblTblsrvvnc_Internalname ;
      private String ROClassString ;
      private String edtavCtlnome_Jsonclick ;
      private String edtavCtltipo_Jsonclick ;
      private String edtavCtlder_Jsonclick ;
      private String edtavCtlrlr_Jsonclick ;
      private String edtavCtlobservacao_Jsonclick ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool AV62Criar ;
      private bool returnInSub ;
      private bool gx_BV58 ;
      private bool gx_refresh_fired ;
      private bool AV51Ok ;
      private String AV12Blob ;
      private IGxSession AV59WebSession ;
      private ExcelDocumentI AV35ExcelDocument ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GxFile gxblobfileaux ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox dynavSistema_codigo ;
      private GXCheckbox chkavCriar ;
      private IDataStoreProvider pr_default ;
      private int[] H00J82_A127Sistema_Codigo ;
      private String[] H00J82_A129Sistema_Sigla ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      [ObjectCollection(ItemType=typeof( SdtSDT_Baselines_Funcao ))]
      private IGxCollection AV57SDT_Baseline ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV58WWPContext ;
   }

   public class wp_importabaseline__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00J82 ;
          prmH00J82 = new Object[] {
          } ;
          def= new CursorDef[] {
              new CursorDef("H00J82", "SELECT [Sistema_Codigo], [Sistema_Sigla] FROM [Sistema] WITH (NOLOCK) ORDER BY [Sistema_Sigla] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00J82,0,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 25) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
       }
    }

 }

}
