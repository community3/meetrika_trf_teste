/*
               File: REL_RedmineSistemas
        Description: Verifica��o de Sistemas
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:55:7.40
       Program type: Main program
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.Printer;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class arel_redminesistemas : GXWebProcedure, System.Web.SessionState.IRequiresSessionState
   {
      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize();
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
         }
         if ( GxWebError == 0 )
         {
            executePrivate();
         }
         cleanup();
      }

      public arel_redminesistemas( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public arel_redminesistemas( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         initialize();
         executePrivate();
      }

      public void executeSubmit( )
      {
         arel_redminesistemas objarel_redminesistemas;
         objarel_redminesistemas = new arel_redminesistemas();
         objarel_redminesistemas.context.SetSubmitInitialConfig(context);
         objarel_redminesistemas.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objarel_redminesistemas);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((arel_redminesistemas)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         M_top = 0;
         M_bot = 6;
         P_lines = (int)(66-M_bot);
         getPrinter().GxClearAttris() ;
         add_metrics( ) ;
         lineHeight = 15;
         PrtOffset = 0;
         gxXPage = 100;
         gxYPage = 100;
         getPrinter().GxSetDocName("") ;
         try
         {
            Gx_out = "FIL" ;
            if (!initPrinter (Gx_out, gxXPage, gxYPage, "GXPRN.INI", "", "", 2, 1, 256, 16834, 11909, 0, 1, 1, 0, 1, 1) )
            {
               cleanup();
               return;
            }
            getPrinter().setModal(false) ;
            P_lines = (int)(gxYPage-(lineHeight*6));
            Gx_line = (int)(P_lines+1);
            getPrinter().setPageLines(P_lines);
            getPrinter().setLineHeight(lineHeight);
            getPrinter().setM_top(M_top);
            getPrinter().setM_bot(M_bot);
            AV15Titulo = "Sistemas no Redmine vs Meetrika";
            AV11SDT_Issues.FromXml(AV14websession.Get("Issues"), "");
            AV12SDT_Parametros.FromXml(AV14websession.Get("Parametros"), "");
            AV14websession.Remove("Issues");
            AV14websession.Remove("Parametros");
            AV8Area = (int)(AV12SDT_Parametros.gxTpr_Numeric.GetNumeric(1));
            AV9Campo = StringUtil.Lower( ((String)AV12SDT_Parametros.gxTpr_Character.Item(1)));
            AV30GXV1 = 1;
            while ( AV30GXV1 <= AV11SDT_Issues.gxTpr_Issues.Count )
            {
               AV10Issue = ((SdtSDT_RedmineIssues_issue)AV11SDT_Issues.gxTpr_Issues.Item(AV30GXV1));
               /* Execute user subroutine: 'SIGLA' */
               S111 ();
               if ( returnInSub )
               {
                  this.cleanup();
                  if (true) return;
               }
               AV21Sistema.gxTpr_Codigo = AV10Issue.gxTpr_Id;
               AV21Sistema.gxTpr_Descricao = AV13Sigla;
               AV20Sistemas.Add(AV21Sistema, 0);
               if ( AV16Siglas.IndexOf(AV13Sigla) == 0 )
               {
                  AV16Siglas.Add(AV13Sigla, 0);
               }
               AV30GXV1 = (int)(AV30GXV1+1);
            }
            AV31GXV2 = 1;
            while ( AV31GXV2 <= AV16Siglas.Count )
            {
               AV13Sigla = AV16Siglas.GetString(AV31GXV2);
               AV32GXLvl37 = 0;
               /* Using cursor P00U02 */
               pr_default.execute(0, new Object[] {AV8Area, AV13Sigla});
               while ( (pr_default.getStatus(0) != 101) )
               {
                  A129Sistema_Sigla = P00U02_A129Sistema_Sigla[0];
                  A135Sistema_AreaTrabalhoCod = P00U02_A135Sistema_AreaTrabalhoCod[0];
                  A127Sistema_Codigo = P00U02_A127Sistema_Codigo[0];
                  AV32GXLvl37 = 1;
                  AV24i = 0;
                  AV33GXV3 = 1;
                  while ( AV33GXV3 <= AV20Sistemas.Count )
                  {
                     AV23Item = ((SdtSDT_Codigos)AV20Sistemas.Item(AV33GXV3));
                     AV24i = (short)(AV24i+1);
                     if ( StringUtil.StrCmp(AV23Item.gxTpr_Descricao, AV13Sigla) == 0 )
                     {
                        ((SdtSDT_Codigos)AV20Sistemas.Item(AV24i)).gxTpr_Descricao = StringUtil.Trim( StringUtil.Str( (decimal)(A127Sistema_Codigo), 6, 0));
                     }
                     AV33GXV3 = (int)(AV33GXV3+1);
                  }
                  pr_default.readNext(0);
               }
               pr_default.close(0);
               if ( AV32GXLvl37 == 0 )
               {
                  AV22Linha = AV13Sigla + " n�o identificado";
                  HU00( false, 15) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV22Linha, "")), 42, Gx_line+0, 303, Gx_line+15, 0+256, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+15);
                  AV19Erros = (short)(AV19Erros+1);
               }
               AV31GXV2 = (int)(AV31GXV2+1);
            }
            AV14websession.Set("Sistemas", AV20Sistemas.ToXml(false, true, "SDT_CodigosCollection", "GxEv3Up14_MeetrikaVs3"));
            if ( (0==AV19Erros) )
            {
               AV22Linha = "Tudo OK";
               HU00( false, 15) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV22Linha, "")), 42, Gx_line+0, 303, Gx_line+15, 0+256, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+15);
            }
            /* Print footer for last page */
            ToSkip = (int)(P_lines+1);
            HU00( true, 0) ;
         }
         catch ( GeneXus.Printer.ProcessInterruptedException e )
         {
         }
         finally
         {
            /* Close printer file */
            try
            {
               getPrinter().GxEndPage() ;
               getPrinter().GxEndDocument() ;
            }
            catch ( GeneXus.Printer.ProcessInterruptedException e )
            {
            }
            endPrinter();
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
         {
            context.Redirect( context.wjLoc );
            context.wjLoc = "";
         }
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'SIGLA' Routine */
         if ( StringUtil.StrCmp(AV9Campo, "project") == 0 )
         {
            AV13Sigla = AV10Issue.gxTpr_Project.gxTpr_Name;
         }
         else if ( StringUtil.StrCmp(AV9Campo, "status") == 0 )
         {
            AV13Sigla = AV10Issue.gxTpr_Status.gxTpr_Name;
         }
         else if ( StringUtil.StrCmp(AV9Campo, "subject") == 0 )
         {
            AV13Sigla = AV10Issue.gxTpr_Subject;
         }
         else if ( StringUtil.StrCmp(AV9Campo, "trucker") == 0 )
         {
            AV13Sigla = AV10Issue.gxTpr_Tracker.gxTpr_Name;
         }
         else if ( StringUtil.StrCmp(AV9Campo, "description") == 0 )
         {
            AV13Sigla = AV10Issue.gxTpr_Description;
         }
         else
         {
            AV34GXV4 = 1;
            while ( AV34GXV4 <= AV10Issue.gxTpr_Custom_fields.gxTpr_Custom_fields.Count )
            {
               AV18Field = ((SdtSDT_RedmineIssues_issue_custom_fields_custom_field)AV10Issue.gxTpr_Custom_fields.gxTpr_Custom_fields.Item(AV34GXV4));
               AV17Nome = StringUtil.Trim( AV18Field.gxTpr_Name);
               if ( StringUtil.StrCmp(AV17Nome, AV9Campo) == 0 )
               {
                  AV13Sigla = StringUtil.Lower( AV18Field.gxTpr_Value);
               }
               AV34GXV4 = (int)(AV34GXV4+1);
            }
         }
      }

      protected void HU00( bool bFoot ,
                           int Inc )
      {
         /* Skip the required number of lines */
         while ( ( ToSkip > 0 ) || ( Gx_line + Inc > P_lines ) )
         {
            if ( Gx_line + Inc >= P_lines )
            {
               if ( Gx_page > 0 )
               {
                  /* Print footers */
                  Gx_line = P_lines;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText("P�gina", 675, Gx_line+0, 710, Gx_line+14, 0+256, 0, 0, 0) ;
                  getPrinter().GxDrawText("de", 750, Gx_line+0, 764, Gx_line+14, 0+256, 0, 0, 0) ;
                  getPrinter().GxDrawText("{{Pages}}", 767, Gx_line+0, 816, Gx_line+14, 1+256, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(Gx_page), "ZZZZZ9")), 708, Gx_line+0, 747, Gx_line+15, 1+256, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+15);
                  getPrinter().GxEndPage() ;
                  if ( bFoot )
                  {
                     return  ;
                  }
               }
               ToSkip = 0;
               Gx_line = 0;
               Gx_page = (int)(Gx_page+1);
               /* Skip Margin Top Lines */
               Gx_line = (int)(Gx_line+(M_top*lineHeight));
               /* Print headers */
               getPrinter().GxStartPage() ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( Gx_time, "")), 725, Gx_line+5, 818, Gx_line+20, 0+256, 0, 0, 0) ;
               getPrinter().GxDrawText(context.localUtil.Format( Gx_date, "99/99/99"), 667, Gx_line+5, 716, Gx_line+20, 2+256, 0, 0, 0) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 14, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV15Titulo, "")), 204, Gx_line+0, 622, Gx_line+26, 1+256, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+33);
               if (true) break;
            }
            else
            {
               PrtOffset = 0;
               Gx_line = (int)(Gx_line+1);
            }
            ToSkip = (int)(ToSkip-1);
         }
         getPrinter().setPage(Gx_page);
      }

      protected void add_metrics( )
      {
         add_metrics0( ) ;
      }

      protected void add_metrics0( )
      {
         getPrinter().setMetrics("Microsoft Sans Serif", false, false, 58, 14, 72, 171,  new int[] {48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 23, 36, 36, 57, 43, 12, 21, 21, 25, 37, 18, 21, 18, 18, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 18, 18, 37, 37, 37, 36, 65, 43, 43, 46, 46, 43, 39, 50, 46, 18, 32, 43, 36, 53, 46, 50, 43, 50, 46, 43, 40, 46, 43, 64, 41, 42, 39, 18, 18, 18, 27, 36, 21, 36, 36, 32, 36, 36, 18, 36, 36, 14, 15, 33, 14, 55, 36, 36, 36, 36, 21, 32, 18, 36, 33, 47, 31, 31, 31, 21, 17, 21, 37, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 36, 36, 36, 36, 17, 36, 21, 47, 24, 36, 37, 21, 47, 35, 26, 35, 21, 21, 21, 37, 34, 21, 21, 21, 23, 36, 53, 53, 53, 39, 43, 43, 43, 43, 43, 43, 64, 46, 43, 43, 43, 43, 18, 18, 18, 18, 46, 46, 50, 50, 50, 50, 50, 37, 50, 46, 46, 46, 46, 43, 43, 39, 36, 36, 36, 36, 36, 36, 57, 32, 36, 36, 36, 36, 18, 18, 18, 18, 36, 36, 36, 36, 36, 36, 36, 35, 39, 36, 36, 36, 36, 32, 36, 32}) ;
      }

      public override int getOutputType( )
      {
         return GxReportUtils.OUTPUT_PDF ;
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if (IsMain)	waitPrinterEnd();
         base.cleanup();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         GXKey = "";
         gxfirstwebparm = "";
         AV15Titulo = "";
         AV11SDT_Issues = new SdtSDT_RedmineIssues(context);
         AV14websession = context.GetSession();
         AV12SDT_Parametros = new SdtSDT_Parametros(context);
         AV9Campo = "";
         AV10Issue = new SdtSDT_RedmineIssues_issue(context);
         AV21Sistema = new SdtSDT_Codigos(context);
         AV13Sigla = "";
         AV20Sistemas = new GxObjectCollection( context, "SDT_Codigos", "GxEv3Up14_MeetrikaVs3", "SdtSDT_Codigos", "GeneXus.Programs");
         AV16Siglas = new GxSimpleCollection();
         scmdbuf = "";
         P00U02_A129Sistema_Sigla = new String[] {""} ;
         P00U02_A135Sistema_AreaTrabalhoCod = new int[1] ;
         P00U02_A127Sistema_Codigo = new int[1] ;
         A129Sistema_Sigla = "";
         AV23Item = new SdtSDT_Codigos(context);
         AV22Linha = "";
         AV18Field = new SdtSDT_RedmineIssues_issue_custom_fields_custom_field(context);
         AV17Nome = "";
         Gx_time = "";
         Gx_date = DateTime.MinValue;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.arel_redminesistemas__default(),
            new Object[][] {
                new Object[] {
               P00U02_A129Sistema_Sigla, P00U02_A135Sistema_AreaTrabalhoCod, P00U02_A127Sistema_Codigo
               }
            }
         );
         Gx_date = DateTimeUtil.Today( context);
         Gx_time = context.localUtil.Time( );
         /* GeneXus formulas. */
         Gx_line = 0;
         Gx_date = DateTimeUtil.Today( context);
         Gx_time = context.localUtil.Time( );
         context.Gx_err = 0;
      }

      private short gxcookieaux ;
      private short nGotPars ;
      private short GxWebError ;
      private short AV32GXLvl37 ;
      private short AV24i ;
      private short AV19Erros ;
      private int M_top ;
      private int M_bot ;
      private int Line ;
      private int ToSkip ;
      private int PrtOffset ;
      private int AV8Area ;
      private int AV30GXV1 ;
      private int AV31GXV2 ;
      private int A135Sistema_AreaTrabalhoCod ;
      private int A127Sistema_Codigo ;
      private int AV33GXV3 ;
      private int Gx_OldLine ;
      private int AV34GXV4 ;
      private String GXKey ;
      private String gxfirstwebparm ;
      private String AV15Titulo ;
      private String AV9Campo ;
      private String AV13Sigla ;
      private String scmdbuf ;
      private String A129Sistema_Sigla ;
      private String AV22Linha ;
      private String AV17Nome ;
      private String Gx_time ;
      private DateTime Gx_date ;
      private bool entryPointCalled ;
      private bool returnInSub ;
      private IGxSession AV14websession ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV16Siglas ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] P00U02_A129Sistema_Sigla ;
      private int[] P00U02_A135Sistema_AreaTrabalhoCod ;
      private int[] P00U02_A127Sistema_Codigo ;
      [ObjectCollection(ItemType=typeof( SdtSDT_Codigos ))]
      private IGxCollection AV20Sistemas ;
      private SdtSDT_RedmineIssues AV11SDT_Issues ;
      private SdtSDT_RedmineIssues_issue AV10Issue ;
      private SdtSDT_RedmineIssues_issue_custom_fields_custom_field AV18Field ;
      private SdtSDT_Parametros AV12SDT_Parametros ;
      private SdtSDT_Codigos AV21Sistema ;
      private SdtSDT_Codigos AV23Item ;
   }

   public class arel_redminesistemas__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00U02 ;
          prmP00U02 = new Object[] {
          new Object[] {"@AV8Area",SqlDbType.Int,6,0} ,
          new Object[] {"@AV13Sigla",SqlDbType.Char,15,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00U02", "SELECT [Sistema_Sigla], [Sistema_AreaTrabalhoCod], [Sistema_Codigo] FROM [Sistema] WITH (NOLOCK) WHERE ([Sistema_AreaTrabalhoCod] = @AV8Area) AND ([Sistema_Sigla] = @AV13Sigla) ORDER BY [Sistema_AreaTrabalhoCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00U02,100,0,false,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getString(1, 25) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                return;
       }
    }

 }

}
