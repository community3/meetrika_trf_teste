/*
               File: ContratoServicosDePara_BC
        Description: Servicos (De Para)
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 23:7:53.27
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contratoservicosdepara_bc : GXHttpHandler, IGxSilentTrn
   {
      public contratoservicosdepara_bc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public contratoservicosdepara_bc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      protected void INITTRN( )
      {
      }

      public void GetInsDefault( )
      {
         ReadRow3S173( ) ;
         standaloneNotModal( ) ;
         InitializeNonKey3S173( ) ;
         standaloneModal( ) ;
         AddRow3S173( ) ;
         Gx_mode = "INS";
         return  ;
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               Z160ContratoServicos_Codigo = A160ContratoServicos_Codigo;
               Z1465ContratoServicosDePara_Codigo = A1465ContratoServicosDePara_Codigo;
               SetMode( "UPD") ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      public bool Reindex( )
      {
         return true ;
      }

      protected void CONFIRM_3S0( )
      {
         BeforeValidate3S173( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls3S173( ) ;
            }
            else
            {
               CheckExtendedTable3S173( ) ;
               if ( AnyError == 0 )
               {
                  ZM3S173( 3) ;
               }
               CloseExtendedTableCursors3S173( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
         }
      }

      protected void ZM3S173( short GX_JID )
      {
         if ( ( GX_JID == 2 ) || ( GX_JID == 0 ) )
         {
            Z1466ContratoServicosDePara_Origem = A1466ContratoServicosDePara_Origem;
            Z1467ContratoServicosDePara_OrigenId = A1467ContratoServicosDePara_OrigenId;
            Z1469ContratoServicosDePara_OrigenId2 = A1469ContratoServicosDePara_OrigenId2;
         }
         if ( ( GX_JID == 3 ) || ( GX_JID == 0 ) )
         {
         }
         if ( GX_JID == -2 )
         {
            Z1465ContratoServicosDePara_Codigo = A1465ContratoServicosDePara_Codigo;
            Z1466ContratoServicosDePara_Origem = A1466ContratoServicosDePara_Origem;
            Z1467ContratoServicosDePara_OrigenId = A1467ContratoServicosDePara_OrigenId;
            Z1468ContratoServicosDePara_OrigemDsc = A1468ContratoServicosDePara_OrigemDsc;
            Z1469ContratoServicosDePara_OrigenId2 = A1469ContratoServicosDePara_OrigenId2;
            Z1470ContratoServicosDePara_OrigemDsc2 = A1470ContratoServicosDePara_OrigemDsc2;
            Z160ContratoServicos_Codigo = A160ContratoServicos_Codigo;
         }
      }

      protected void standaloneNotModal( )
      {
      }

      protected void standaloneModal( )
      {
      }

      protected void Load3S173( )
      {
         /* Using cursor BC003S5 */
         pr_default.execute(3, new Object[] {A160ContratoServicos_Codigo, A1465ContratoServicosDePara_Codigo});
         if ( (pr_default.getStatus(3) != 101) )
         {
            RcdFound173 = 1;
            A1466ContratoServicosDePara_Origem = BC003S5_A1466ContratoServicosDePara_Origem[0];
            A1467ContratoServicosDePara_OrigenId = BC003S5_A1467ContratoServicosDePara_OrigenId[0];
            n1467ContratoServicosDePara_OrigenId = BC003S5_n1467ContratoServicosDePara_OrigenId[0];
            A1468ContratoServicosDePara_OrigemDsc = BC003S5_A1468ContratoServicosDePara_OrigemDsc[0];
            n1468ContratoServicosDePara_OrigemDsc = BC003S5_n1468ContratoServicosDePara_OrigemDsc[0];
            A1469ContratoServicosDePara_OrigenId2 = BC003S5_A1469ContratoServicosDePara_OrigenId2[0];
            n1469ContratoServicosDePara_OrigenId2 = BC003S5_n1469ContratoServicosDePara_OrigenId2[0];
            A1470ContratoServicosDePara_OrigemDsc2 = BC003S5_A1470ContratoServicosDePara_OrigemDsc2[0];
            n1470ContratoServicosDePara_OrigemDsc2 = BC003S5_n1470ContratoServicosDePara_OrigemDsc2[0];
            ZM3S173( -2) ;
         }
         pr_default.close(3);
         OnLoadActions3S173( ) ;
      }

      protected void OnLoadActions3S173( )
      {
      }

      protected void CheckExtendedTable3S173( )
      {
         standaloneModal( ) ;
         /* Using cursor BC003S4 */
         pr_default.execute(2, new Object[] {A160ContratoServicos_Codigo});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato Servicos'.", "ForeignKeyNotFound", 1, "CONTRATOSERVICOS_CODIGO");
            AnyError = 1;
         }
         pr_default.close(2);
         if ( ! ( ( StringUtil.StrCmp(A1466ContratoServicosDePara_Origem, "R") == 0 ) ) )
         {
            GX_msglist.addItem("Campo Origem fora do intervalo", "OutOfRange", 1, "");
            AnyError = 1;
         }
      }

      protected void CloseExtendedTableCursors3S173( )
      {
         pr_default.close(2);
      }

      protected void enableDisable( )
      {
      }

      protected void GetKey3S173( )
      {
         /* Using cursor BC003S6 */
         pr_default.execute(4, new Object[] {A160ContratoServicos_Codigo, A1465ContratoServicosDePara_Codigo});
         if ( (pr_default.getStatus(4) != 101) )
         {
            RcdFound173 = 1;
         }
         else
         {
            RcdFound173 = 0;
         }
         pr_default.close(4);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor BC003S3 */
         pr_default.execute(1, new Object[] {A160ContratoServicos_Codigo, A1465ContratoServicosDePara_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM3S173( 2) ;
            RcdFound173 = 1;
            A1465ContratoServicosDePara_Codigo = BC003S3_A1465ContratoServicosDePara_Codigo[0];
            A1466ContratoServicosDePara_Origem = BC003S3_A1466ContratoServicosDePara_Origem[0];
            A1467ContratoServicosDePara_OrigenId = BC003S3_A1467ContratoServicosDePara_OrigenId[0];
            n1467ContratoServicosDePara_OrigenId = BC003S3_n1467ContratoServicosDePara_OrigenId[0];
            A1468ContratoServicosDePara_OrigemDsc = BC003S3_A1468ContratoServicosDePara_OrigemDsc[0];
            n1468ContratoServicosDePara_OrigemDsc = BC003S3_n1468ContratoServicosDePara_OrigemDsc[0];
            A1469ContratoServicosDePara_OrigenId2 = BC003S3_A1469ContratoServicosDePara_OrigenId2[0];
            n1469ContratoServicosDePara_OrigenId2 = BC003S3_n1469ContratoServicosDePara_OrigenId2[0];
            A1470ContratoServicosDePara_OrigemDsc2 = BC003S3_A1470ContratoServicosDePara_OrigemDsc2[0];
            n1470ContratoServicosDePara_OrigemDsc2 = BC003S3_n1470ContratoServicosDePara_OrigemDsc2[0];
            A160ContratoServicos_Codigo = BC003S3_A160ContratoServicos_Codigo[0];
            Z160ContratoServicos_Codigo = A160ContratoServicos_Codigo;
            Z1465ContratoServicosDePara_Codigo = A1465ContratoServicosDePara_Codigo;
            sMode173 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Load3S173( ) ;
            if ( AnyError == 1 )
            {
               RcdFound173 = 0;
               InitializeNonKey3S173( ) ;
            }
            Gx_mode = sMode173;
         }
         else
         {
            RcdFound173 = 0;
            InitializeNonKey3S173( ) ;
            sMode173 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Gx_mode = sMode173;
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey3S173( ) ;
         if ( RcdFound173 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
         }
         getByPrimaryKey( ) ;
      }

      protected void insert_Check( )
      {
         CONFIRM_3S0( ) ;
         IsConfirmed = 0;
      }

      protected void update_Check( )
      {
         insert_Check( ) ;
      }

      protected void delete_Check( )
      {
         insert_Check( ) ;
      }

      protected void CheckOptimisticConcurrency3S173( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor BC003S2 */
            pr_default.execute(0, new Object[] {A160ContratoServicos_Codigo, A1465ContratoServicosDePara_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContratoServicosDePara"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z1466ContratoServicosDePara_Origem, BC003S2_A1466ContratoServicosDePara_Origem[0]) != 0 ) || ( Z1467ContratoServicosDePara_OrigenId != BC003S2_A1467ContratoServicosDePara_OrigenId[0] ) || ( Z1469ContratoServicosDePara_OrigenId2 != BC003S2_A1469ContratoServicosDePara_OrigenId2[0] ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"ContratoServicosDePara"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert3S173( )
      {
         BeforeValidate3S173( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable3S173( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM3S173( 0) ;
            CheckOptimisticConcurrency3S173( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm3S173( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert3S173( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC003S7 */
                     pr_default.execute(5, new Object[] {A1465ContratoServicosDePara_Codigo, A1466ContratoServicosDePara_Origem, n1467ContratoServicosDePara_OrigenId, A1467ContratoServicosDePara_OrigenId, n1468ContratoServicosDePara_OrigemDsc, A1468ContratoServicosDePara_OrigemDsc, n1469ContratoServicosDePara_OrigenId2, A1469ContratoServicosDePara_OrigenId2, n1470ContratoServicosDePara_OrigemDsc2, A1470ContratoServicosDePara_OrigemDsc2, A160ContratoServicos_Codigo});
                     pr_default.close(5);
                     dsDefault.SmartCacheProvider.SetUpdated("ContratoServicosDePara") ;
                     if ( (pr_default.getStatus(5) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load3S173( ) ;
            }
            EndLevel3S173( ) ;
         }
         CloseExtendedTableCursors3S173( ) ;
      }

      protected void Update3S173( )
      {
         BeforeValidate3S173( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable3S173( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency3S173( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm3S173( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate3S173( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC003S8 */
                     pr_default.execute(6, new Object[] {A1466ContratoServicosDePara_Origem, n1467ContratoServicosDePara_OrigenId, A1467ContratoServicosDePara_OrigenId, n1468ContratoServicosDePara_OrigemDsc, A1468ContratoServicosDePara_OrigemDsc, n1469ContratoServicosDePara_OrigenId2, A1469ContratoServicosDePara_OrigenId2, n1470ContratoServicosDePara_OrigemDsc2, A1470ContratoServicosDePara_OrigemDsc2, A160ContratoServicos_Codigo, A1465ContratoServicosDePara_Codigo});
                     pr_default.close(6);
                     dsDefault.SmartCacheProvider.SetUpdated("ContratoServicosDePara") ;
                     if ( (pr_default.getStatus(6) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContratoServicosDePara"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate3S173( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel3S173( ) ;
         }
         CloseExtendedTableCursors3S173( ) ;
      }

      protected void DeferredUpdate3S173( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         BeforeValidate3S173( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency3S173( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls3S173( ) ;
            AfterConfirm3S173( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete3S173( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor BC003S9 */
                  pr_default.execute(7, new Object[] {A160ContratoServicos_Codigo, A1465ContratoServicosDePara_Codigo});
                  pr_default.close(7);
                  dsDefault.SmartCacheProvider.SetUpdated("ContratoServicosDePara") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode173 = Gx_mode;
         Gx_mode = "DLT";
         EndLevel3S173( ) ;
         Gx_mode = sMode173;
      }

      protected void OnDeleteControls3S173( )
      {
         standaloneModal( ) ;
         /* No delete mode formulas found. */
      }

      protected void EndLevel3S173( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete3S173( ) ;
         }
         if ( AnyError == 0 )
         {
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanKeyStart3S173( )
      {
         /* Using cursor BC003S10 */
         pr_default.execute(8, new Object[] {A160ContratoServicos_Codigo, A1465ContratoServicosDePara_Codigo});
         RcdFound173 = 0;
         if ( (pr_default.getStatus(8) != 101) )
         {
            RcdFound173 = 1;
            A1465ContratoServicosDePara_Codigo = BC003S10_A1465ContratoServicosDePara_Codigo[0];
            A1466ContratoServicosDePara_Origem = BC003S10_A1466ContratoServicosDePara_Origem[0];
            A1467ContratoServicosDePara_OrigenId = BC003S10_A1467ContratoServicosDePara_OrigenId[0];
            n1467ContratoServicosDePara_OrigenId = BC003S10_n1467ContratoServicosDePara_OrigenId[0];
            A1468ContratoServicosDePara_OrigemDsc = BC003S10_A1468ContratoServicosDePara_OrigemDsc[0];
            n1468ContratoServicosDePara_OrigemDsc = BC003S10_n1468ContratoServicosDePara_OrigemDsc[0];
            A1469ContratoServicosDePara_OrigenId2 = BC003S10_A1469ContratoServicosDePara_OrigenId2[0];
            n1469ContratoServicosDePara_OrigenId2 = BC003S10_n1469ContratoServicosDePara_OrigenId2[0];
            A1470ContratoServicosDePara_OrigemDsc2 = BC003S10_A1470ContratoServicosDePara_OrigemDsc2[0];
            n1470ContratoServicosDePara_OrigemDsc2 = BC003S10_n1470ContratoServicosDePara_OrigemDsc2[0];
            A160ContratoServicos_Codigo = BC003S10_A160ContratoServicos_Codigo[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanKeyNext3S173( )
      {
         /* Scan next routine */
         pr_default.readNext(8);
         RcdFound173 = 0;
         ScanKeyLoad3S173( ) ;
      }

      protected void ScanKeyLoad3S173( )
      {
         sMode173 = Gx_mode;
         Gx_mode = "DSP";
         if ( (pr_default.getStatus(8) != 101) )
         {
            RcdFound173 = 1;
            A1465ContratoServicosDePara_Codigo = BC003S10_A1465ContratoServicosDePara_Codigo[0];
            A1466ContratoServicosDePara_Origem = BC003S10_A1466ContratoServicosDePara_Origem[0];
            A1467ContratoServicosDePara_OrigenId = BC003S10_A1467ContratoServicosDePara_OrigenId[0];
            n1467ContratoServicosDePara_OrigenId = BC003S10_n1467ContratoServicosDePara_OrigenId[0];
            A1468ContratoServicosDePara_OrigemDsc = BC003S10_A1468ContratoServicosDePara_OrigemDsc[0];
            n1468ContratoServicosDePara_OrigemDsc = BC003S10_n1468ContratoServicosDePara_OrigemDsc[0];
            A1469ContratoServicosDePara_OrigenId2 = BC003S10_A1469ContratoServicosDePara_OrigenId2[0];
            n1469ContratoServicosDePara_OrigenId2 = BC003S10_n1469ContratoServicosDePara_OrigenId2[0];
            A1470ContratoServicosDePara_OrigemDsc2 = BC003S10_A1470ContratoServicosDePara_OrigemDsc2[0];
            n1470ContratoServicosDePara_OrigemDsc2 = BC003S10_n1470ContratoServicosDePara_OrigemDsc2[0];
            A160ContratoServicos_Codigo = BC003S10_A160ContratoServicos_Codigo[0];
         }
         Gx_mode = sMode173;
      }

      protected void ScanKeyEnd3S173( )
      {
         pr_default.close(8);
      }

      protected void AfterConfirm3S173( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert3S173( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate3S173( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete3S173( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete3S173( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate3S173( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes3S173( )
      {
      }

      protected void AddRow3S173( )
      {
         VarsToRow173( bcContratoServicosDePara) ;
      }

      protected void ReadRow3S173( )
      {
         RowToVars173( bcContratoServicosDePara, 1) ;
      }

      protected void InitializeNonKey3S173( )
      {
         A1466ContratoServicosDePara_Origem = "";
         A1467ContratoServicosDePara_OrigenId = 0;
         n1467ContratoServicosDePara_OrigenId = false;
         A1468ContratoServicosDePara_OrigemDsc = "";
         n1468ContratoServicosDePara_OrigemDsc = false;
         A1469ContratoServicosDePara_OrigenId2 = 0;
         n1469ContratoServicosDePara_OrigenId2 = false;
         A1470ContratoServicosDePara_OrigemDsc2 = "";
         n1470ContratoServicosDePara_OrigemDsc2 = false;
         Z1466ContratoServicosDePara_Origem = "";
         Z1467ContratoServicosDePara_OrigenId = 0;
         Z1469ContratoServicosDePara_OrigenId2 = 0;
      }

      protected void InitAll3S173( )
      {
         A160ContratoServicos_Codigo = 0;
         A1465ContratoServicosDePara_Codigo = 0;
         InitializeNonKey3S173( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      public void VarsToRow173( SdtContratoServicosDePara obj173 )
      {
         obj173.gxTpr_Mode = Gx_mode;
         obj173.gxTpr_Contratoservicosdepara_origem = A1466ContratoServicosDePara_Origem;
         obj173.gxTpr_Contratoservicosdepara_origenid = A1467ContratoServicosDePara_OrigenId;
         obj173.gxTpr_Contratoservicosdepara_origemdsc = A1468ContratoServicosDePara_OrigemDsc;
         obj173.gxTpr_Contratoservicosdepara_origenid2 = A1469ContratoServicosDePara_OrigenId2;
         obj173.gxTpr_Contratoservicosdepara_origemdsc2 = A1470ContratoServicosDePara_OrigemDsc2;
         obj173.gxTpr_Contratoservicos_codigo = A160ContratoServicos_Codigo;
         obj173.gxTpr_Contratoservicosdepara_codigo = A1465ContratoServicosDePara_Codigo;
         obj173.gxTpr_Contratoservicos_codigo_Z = Z160ContratoServicos_Codigo;
         obj173.gxTpr_Contratoservicosdepara_codigo_Z = Z1465ContratoServicosDePara_Codigo;
         obj173.gxTpr_Contratoservicosdepara_origem_Z = Z1466ContratoServicosDePara_Origem;
         obj173.gxTpr_Contratoservicosdepara_origenid_Z = Z1467ContratoServicosDePara_OrigenId;
         obj173.gxTpr_Contratoservicosdepara_origenid2_Z = Z1469ContratoServicosDePara_OrigenId2;
         obj173.gxTpr_Contratoservicosdepara_origenid_N = (short)(Convert.ToInt16(n1467ContratoServicosDePara_OrigenId));
         obj173.gxTpr_Contratoservicosdepara_origemdsc_N = (short)(Convert.ToInt16(n1468ContratoServicosDePara_OrigemDsc));
         obj173.gxTpr_Contratoservicosdepara_origenid2_N = (short)(Convert.ToInt16(n1469ContratoServicosDePara_OrigenId2));
         obj173.gxTpr_Contratoservicosdepara_origemdsc2_N = (short)(Convert.ToInt16(n1470ContratoServicosDePara_OrigemDsc2));
         obj173.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void KeyVarsToRow173( SdtContratoServicosDePara obj173 )
      {
         obj173.gxTpr_Contratoservicos_codigo = A160ContratoServicos_Codigo;
         obj173.gxTpr_Contratoservicosdepara_codigo = A1465ContratoServicosDePara_Codigo;
         return  ;
      }

      public void RowToVars173( SdtContratoServicosDePara obj173 ,
                                int forceLoad )
      {
         Gx_mode = obj173.gxTpr_Mode;
         A1466ContratoServicosDePara_Origem = obj173.gxTpr_Contratoservicosdepara_origem;
         A1467ContratoServicosDePara_OrigenId = obj173.gxTpr_Contratoservicosdepara_origenid;
         n1467ContratoServicosDePara_OrigenId = false;
         A1468ContratoServicosDePara_OrigemDsc = obj173.gxTpr_Contratoservicosdepara_origemdsc;
         n1468ContratoServicosDePara_OrigemDsc = false;
         A1469ContratoServicosDePara_OrigenId2 = obj173.gxTpr_Contratoservicosdepara_origenid2;
         n1469ContratoServicosDePara_OrigenId2 = false;
         A1470ContratoServicosDePara_OrigemDsc2 = obj173.gxTpr_Contratoservicosdepara_origemdsc2;
         n1470ContratoServicosDePara_OrigemDsc2 = false;
         A160ContratoServicos_Codigo = obj173.gxTpr_Contratoservicos_codigo;
         A1465ContratoServicosDePara_Codigo = obj173.gxTpr_Contratoservicosdepara_codigo;
         Z160ContratoServicos_Codigo = obj173.gxTpr_Contratoservicos_codigo_Z;
         Z1465ContratoServicosDePara_Codigo = obj173.gxTpr_Contratoservicosdepara_codigo_Z;
         Z1466ContratoServicosDePara_Origem = obj173.gxTpr_Contratoservicosdepara_origem_Z;
         Z1467ContratoServicosDePara_OrigenId = obj173.gxTpr_Contratoservicosdepara_origenid_Z;
         Z1469ContratoServicosDePara_OrigenId2 = obj173.gxTpr_Contratoservicosdepara_origenid2_Z;
         n1467ContratoServicosDePara_OrigenId = (bool)(Convert.ToBoolean(obj173.gxTpr_Contratoservicosdepara_origenid_N));
         n1468ContratoServicosDePara_OrigemDsc = (bool)(Convert.ToBoolean(obj173.gxTpr_Contratoservicosdepara_origemdsc_N));
         n1469ContratoServicosDePara_OrigenId2 = (bool)(Convert.ToBoolean(obj173.gxTpr_Contratoservicosdepara_origenid2_N));
         n1470ContratoServicosDePara_OrigemDsc2 = (bool)(Convert.ToBoolean(obj173.gxTpr_Contratoservicosdepara_origemdsc2_N));
         Gx_mode = obj173.gxTpr_Mode;
         return  ;
      }

      public void LoadKey( Object[] obj )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         A160ContratoServicos_Codigo = (int)getParm(obj,0);
         A1465ContratoServicosDePara_Codigo = (int)getParm(obj,1);
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         InitializeNonKey3S173( ) ;
         ScanKeyStart3S173( ) ;
         if ( RcdFound173 == 0 )
         {
            Gx_mode = "INS";
            /* Using cursor BC003S11 */
            pr_default.execute(9, new Object[] {A160ContratoServicos_Codigo});
            if ( (pr_default.getStatus(9) == 101) )
            {
               GX_msglist.addItem("N�o existe 'Contrato Servicos'.", "ForeignKeyNotFound", 1, "CONTRATOSERVICOS_CODIGO");
               AnyError = 1;
            }
            pr_default.close(9);
         }
         else
         {
            Gx_mode = "UPD";
            Z160ContratoServicos_Codigo = A160ContratoServicos_Codigo;
            Z1465ContratoServicosDePara_Codigo = A1465ContratoServicosDePara_Codigo;
         }
         ZM3S173( -2) ;
         OnLoadActions3S173( ) ;
         AddRow3S173( ) ;
         ScanKeyEnd3S173( ) ;
         if ( RcdFound173 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Load( )
      {
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         RowToVars173( bcContratoServicosDePara, 0) ;
         ScanKeyStart3S173( ) ;
         if ( RcdFound173 == 0 )
         {
            Gx_mode = "INS";
            /* Using cursor BC003S11 */
            pr_default.execute(9, new Object[] {A160ContratoServicos_Codigo});
            if ( (pr_default.getStatus(9) == 101) )
            {
               GX_msglist.addItem("N�o existe 'Contrato Servicos'.", "ForeignKeyNotFound", 1, "CONTRATOSERVICOS_CODIGO");
               AnyError = 1;
            }
            pr_default.close(9);
         }
         else
         {
            Gx_mode = "UPD";
            Z160ContratoServicos_Codigo = A160ContratoServicos_Codigo;
            Z1465ContratoServicosDePara_Codigo = A1465ContratoServicosDePara_Codigo;
         }
         ZM3S173( -2) ;
         OnLoadActions3S173( ) ;
         AddRow3S173( ) ;
         ScanKeyEnd3S173( ) ;
         if ( RcdFound173 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Save( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         IsConfirmed = 1;
         RowToVars173( bcContratoServicosDePara, 0) ;
         nKeyPressed = 1;
         GetKey3S173( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            Insert3S173( ) ;
         }
         else
         {
            if ( RcdFound173 == 1 )
            {
               if ( ( A160ContratoServicos_Codigo != Z160ContratoServicos_Codigo ) || ( A1465ContratoServicosDePara_Codigo != Z1465ContratoServicosDePara_Codigo ) )
               {
                  A160ContratoServicos_Codigo = Z160ContratoServicos_Codigo;
                  A1465ContratoServicosDePara_Codigo = Z1465ContratoServicosDePara_Codigo;
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
               }
               else
               {
                  Gx_mode = "UPD";
                  /* Update record */
                  Update3S173( ) ;
               }
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else
               {
                  if ( ( A160ContratoServicos_Codigo != Z160ContratoServicos_Codigo ) || ( A1465ContratoServicosDePara_Codigo != Z1465ContratoServicosDePara_Codigo ) )
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert3S173( ) ;
                     }
                  }
                  else
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert3S173( ) ;
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         VarsToRow173( bcContratoServicosDePara) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public void Check( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         RowToVars173( bcContratoServicosDePara, 0) ;
         nKeyPressed = 3;
         IsConfirmed = 0;
         GetKey3S173( ) ;
         if ( RcdFound173 == 1 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( ( A160ContratoServicos_Codigo != Z160ContratoServicos_Codigo ) || ( A1465ContratoServicosDePara_Codigo != Z1465ContratoServicosDePara_Codigo ) )
            {
               A160ContratoServicos_Codigo = Z160ContratoServicos_Codigo;
               A1465ContratoServicosDePara_Codigo = Z1465ContratoServicosDePara_Codigo;
               GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               delete_Check( ) ;
            }
            else
            {
               Gx_mode = "UPD";
               update_Check( ) ;
            }
         }
         else
         {
            if ( ( A160ContratoServicos_Codigo != Z160ContratoServicos_Codigo ) || ( A1465ContratoServicosDePara_Codigo != Z1465ContratoServicosDePara_Codigo ) )
            {
               Gx_mode = "INS";
               insert_Check( ) ;
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                  AnyError = 1;
               }
               else
               {
                  Gx_mode = "INS";
                  insert_Check( ) ;
               }
            }
         }
         pr_default.close(1);
         pr_default.close(9);
         context.RollbackDataStores( "ContratoServicosDePara_BC");
         VarsToRow173( bcContratoServicosDePara) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public int Errors( )
      {
         if ( AnyError == 0 )
         {
            return (int)(0) ;
         }
         return (int)(1) ;
      }

      public msglist GetMessages( )
      {
         return LclMsgLst ;
      }

      public String GetMode( )
      {
         Gx_mode = bcContratoServicosDePara.gxTpr_Mode;
         return Gx_mode ;
      }

      public void SetMode( String lMode )
      {
         Gx_mode = lMode;
         bcContratoServicosDePara.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void SetSDT( GxSilentTrnSdt sdt ,
                          short sdtToBc )
      {
         if ( sdt != bcContratoServicosDePara )
         {
            bcContratoServicosDePara = (SdtContratoServicosDePara)(sdt);
            if ( StringUtil.StrCmp(bcContratoServicosDePara.gxTpr_Mode, "") == 0 )
            {
               bcContratoServicosDePara.gxTpr_Mode = "INS";
            }
            if ( sdtToBc == 1 )
            {
               VarsToRow173( bcContratoServicosDePara) ;
            }
            else
            {
               RowToVars173( bcContratoServicosDePara, 1) ;
            }
         }
         else
         {
            if ( StringUtil.StrCmp(bcContratoServicosDePara.gxTpr_Mode, "") == 0 )
            {
               bcContratoServicosDePara.gxTpr_Mode = "INS";
            }
         }
         return  ;
      }

      public void ReloadFromSDT( )
      {
         RowToVars173( bcContratoServicosDePara, 1) ;
         return  ;
      }

      public SdtContratoServicosDePara ContratoServicosDePara_BC
      {
         get {
            return bcContratoServicosDePara ;
         }

      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         createObjects();
         initialize();
      }

      protected override void createObjects( )
      {
      }

      protected void Process( )
      {
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(9);
      }

      public override void initialize( )
      {
         scmdbuf = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Gx_mode = "";
         Z1466ContratoServicosDePara_Origem = "";
         A1466ContratoServicosDePara_Origem = "";
         Z1468ContratoServicosDePara_OrigemDsc = "";
         A1468ContratoServicosDePara_OrigemDsc = "";
         Z1470ContratoServicosDePara_OrigemDsc2 = "";
         A1470ContratoServicosDePara_OrigemDsc2 = "";
         BC003S5_A1465ContratoServicosDePara_Codigo = new int[1] ;
         BC003S5_A1466ContratoServicosDePara_Origem = new String[] {""} ;
         BC003S5_A1467ContratoServicosDePara_OrigenId = new long[1] ;
         BC003S5_n1467ContratoServicosDePara_OrigenId = new bool[] {false} ;
         BC003S5_A1468ContratoServicosDePara_OrigemDsc = new String[] {""} ;
         BC003S5_n1468ContratoServicosDePara_OrigemDsc = new bool[] {false} ;
         BC003S5_A1469ContratoServicosDePara_OrigenId2 = new long[1] ;
         BC003S5_n1469ContratoServicosDePara_OrigenId2 = new bool[] {false} ;
         BC003S5_A1470ContratoServicosDePara_OrigemDsc2 = new String[] {""} ;
         BC003S5_n1470ContratoServicosDePara_OrigemDsc2 = new bool[] {false} ;
         BC003S5_A160ContratoServicos_Codigo = new int[1] ;
         BC003S4_A160ContratoServicos_Codigo = new int[1] ;
         BC003S6_A160ContratoServicos_Codigo = new int[1] ;
         BC003S6_A1465ContratoServicosDePara_Codigo = new int[1] ;
         BC003S3_A1465ContratoServicosDePara_Codigo = new int[1] ;
         BC003S3_A1466ContratoServicosDePara_Origem = new String[] {""} ;
         BC003S3_A1467ContratoServicosDePara_OrigenId = new long[1] ;
         BC003S3_n1467ContratoServicosDePara_OrigenId = new bool[] {false} ;
         BC003S3_A1468ContratoServicosDePara_OrigemDsc = new String[] {""} ;
         BC003S3_n1468ContratoServicosDePara_OrigemDsc = new bool[] {false} ;
         BC003S3_A1469ContratoServicosDePara_OrigenId2 = new long[1] ;
         BC003S3_n1469ContratoServicosDePara_OrigenId2 = new bool[] {false} ;
         BC003S3_A1470ContratoServicosDePara_OrigemDsc2 = new String[] {""} ;
         BC003S3_n1470ContratoServicosDePara_OrigemDsc2 = new bool[] {false} ;
         BC003S3_A160ContratoServicos_Codigo = new int[1] ;
         sMode173 = "";
         BC003S2_A1465ContratoServicosDePara_Codigo = new int[1] ;
         BC003S2_A1466ContratoServicosDePara_Origem = new String[] {""} ;
         BC003S2_A1467ContratoServicosDePara_OrigenId = new long[1] ;
         BC003S2_n1467ContratoServicosDePara_OrigenId = new bool[] {false} ;
         BC003S2_A1468ContratoServicosDePara_OrigemDsc = new String[] {""} ;
         BC003S2_n1468ContratoServicosDePara_OrigemDsc = new bool[] {false} ;
         BC003S2_A1469ContratoServicosDePara_OrigenId2 = new long[1] ;
         BC003S2_n1469ContratoServicosDePara_OrigenId2 = new bool[] {false} ;
         BC003S2_A1470ContratoServicosDePara_OrigemDsc2 = new String[] {""} ;
         BC003S2_n1470ContratoServicosDePara_OrigemDsc2 = new bool[] {false} ;
         BC003S2_A160ContratoServicos_Codigo = new int[1] ;
         BC003S10_A1465ContratoServicosDePara_Codigo = new int[1] ;
         BC003S10_A1466ContratoServicosDePara_Origem = new String[] {""} ;
         BC003S10_A1467ContratoServicosDePara_OrigenId = new long[1] ;
         BC003S10_n1467ContratoServicosDePara_OrigenId = new bool[] {false} ;
         BC003S10_A1468ContratoServicosDePara_OrigemDsc = new String[] {""} ;
         BC003S10_n1468ContratoServicosDePara_OrigemDsc = new bool[] {false} ;
         BC003S10_A1469ContratoServicosDePara_OrigenId2 = new long[1] ;
         BC003S10_n1469ContratoServicosDePara_OrigenId2 = new bool[] {false} ;
         BC003S10_A1470ContratoServicosDePara_OrigemDsc2 = new String[] {""} ;
         BC003S10_n1470ContratoServicosDePara_OrigemDsc2 = new bool[] {false} ;
         BC003S10_A160ContratoServicos_Codigo = new int[1] ;
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         BC003S11_A160ContratoServicos_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contratoservicosdepara_bc__default(),
            new Object[][] {
                new Object[] {
               BC003S2_A1465ContratoServicosDePara_Codigo, BC003S2_A1466ContratoServicosDePara_Origem, BC003S2_A1467ContratoServicosDePara_OrigenId, BC003S2_n1467ContratoServicosDePara_OrigenId, BC003S2_A1468ContratoServicosDePara_OrigemDsc, BC003S2_n1468ContratoServicosDePara_OrigemDsc, BC003S2_A1469ContratoServicosDePara_OrigenId2, BC003S2_n1469ContratoServicosDePara_OrigenId2, BC003S2_A1470ContratoServicosDePara_OrigemDsc2, BC003S2_n1470ContratoServicosDePara_OrigemDsc2,
               BC003S2_A160ContratoServicos_Codigo
               }
               , new Object[] {
               BC003S3_A1465ContratoServicosDePara_Codigo, BC003S3_A1466ContratoServicosDePara_Origem, BC003S3_A1467ContratoServicosDePara_OrigenId, BC003S3_n1467ContratoServicosDePara_OrigenId, BC003S3_A1468ContratoServicosDePara_OrigemDsc, BC003S3_n1468ContratoServicosDePara_OrigemDsc, BC003S3_A1469ContratoServicosDePara_OrigenId2, BC003S3_n1469ContratoServicosDePara_OrigenId2, BC003S3_A1470ContratoServicosDePara_OrigemDsc2, BC003S3_n1470ContratoServicosDePara_OrigemDsc2,
               BC003S3_A160ContratoServicos_Codigo
               }
               , new Object[] {
               BC003S4_A160ContratoServicos_Codigo
               }
               , new Object[] {
               BC003S5_A1465ContratoServicosDePara_Codigo, BC003S5_A1466ContratoServicosDePara_Origem, BC003S5_A1467ContratoServicosDePara_OrigenId, BC003S5_n1467ContratoServicosDePara_OrigenId, BC003S5_A1468ContratoServicosDePara_OrigemDsc, BC003S5_n1468ContratoServicosDePara_OrigemDsc, BC003S5_A1469ContratoServicosDePara_OrigenId2, BC003S5_n1469ContratoServicosDePara_OrigenId2, BC003S5_A1470ContratoServicosDePara_OrigemDsc2, BC003S5_n1470ContratoServicosDePara_OrigemDsc2,
               BC003S5_A160ContratoServicos_Codigo
               }
               , new Object[] {
               BC003S6_A160ContratoServicos_Codigo, BC003S6_A1465ContratoServicosDePara_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               BC003S10_A1465ContratoServicosDePara_Codigo, BC003S10_A1466ContratoServicosDePara_Origem, BC003S10_A1467ContratoServicosDePara_OrigenId, BC003S10_n1467ContratoServicosDePara_OrigenId, BC003S10_A1468ContratoServicosDePara_OrigemDsc, BC003S10_n1468ContratoServicosDePara_OrigemDsc, BC003S10_A1469ContratoServicosDePara_OrigenId2, BC003S10_n1469ContratoServicosDePara_OrigenId2, BC003S10_A1470ContratoServicosDePara_OrigemDsc2, BC003S10_n1470ContratoServicosDePara_OrigemDsc2,
               BC003S10_A160ContratoServicos_Codigo
               }
               , new Object[] {
               BC003S11_A160ContratoServicos_Codigo
               }
            }
         );
         INITTRN();
         /* Execute Start event if defined. */
         standaloneNotModal( ) ;
      }

      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short GX_JID ;
      private short RcdFound173 ;
      private int trnEnded ;
      private int Z160ContratoServicos_Codigo ;
      private int A160ContratoServicos_Codigo ;
      private int Z1465ContratoServicosDePara_Codigo ;
      private int A1465ContratoServicosDePara_Codigo ;
      private long Z1467ContratoServicosDePara_OrigenId ;
      private long A1467ContratoServicosDePara_OrigenId ;
      private long Z1469ContratoServicosDePara_OrigenId2 ;
      private long A1469ContratoServicosDePara_OrigenId2 ;
      private String scmdbuf ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String Gx_mode ;
      private String Z1466ContratoServicosDePara_Origem ;
      private String A1466ContratoServicosDePara_Origem ;
      private String sMode173 ;
      private bool n1467ContratoServicosDePara_OrigenId ;
      private bool n1468ContratoServicosDePara_OrigemDsc ;
      private bool n1469ContratoServicosDePara_OrigenId2 ;
      private bool n1470ContratoServicosDePara_OrigemDsc2 ;
      private String Z1468ContratoServicosDePara_OrigemDsc ;
      private String A1468ContratoServicosDePara_OrigemDsc ;
      private String Z1470ContratoServicosDePara_OrigemDsc2 ;
      private String A1470ContratoServicosDePara_OrigemDsc2 ;
      private SdtContratoServicosDePara bcContratoServicosDePara ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] BC003S5_A1465ContratoServicosDePara_Codigo ;
      private String[] BC003S5_A1466ContratoServicosDePara_Origem ;
      private long[] BC003S5_A1467ContratoServicosDePara_OrigenId ;
      private bool[] BC003S5_n1467ContratoServicosDePara_OrigenId ;
      private String[] BC003S5_A1468ContratoServicosDePara_OrigemDsc ;
      private bool[] BC003S5_n1468ContratoServicosDePara_OrigemDsc ;
      private long[] BC003S5_A1469ContratoServicosDePara_OrigenId2 ;
      private bool[] BC003S5_n1469ContratoServicosDePara_OrigenId2 ;
      private String[] BC003S5_A1470ContratoServicosDePara_OrigemDsc2 ;
      private bool[] BC003S5_n1470ContratoServicosDePara_OrigemDsc2 ;
      private int[] BC003S5_A160ContratoServicos_Codigo ;
      private int[] BC003S4_A160ContratoServicos_Codigo ;
      private int[] BC003S6_A160ContratoServicos_Codigo ;
      private int[] BC003S6_A1465ContratoServicosDePara_Codigo ;
      private int[] BC003S3_A1465ContratoServicosDePara_Codigo ;
      private String[] BC003S3_A1466ContratoServicosDePara_Origem ;
      private long[] BC003S3_A1467ContratoServicosDePara_OrigenId ;
      private bool[] BC003S3_n1467ContratoServicosDePara_OrigenId ;
      private String[] BC003S3_A1468ContratoServicosDePara_OrigemDsc ;
      private bool[] BC003S3_n1468ContratoServicosDePara_OrigemDsc ;
      private long[] BC003S3_A1469ContratoServicosDePara_OrigenId2 ;
      private bool[] BC003S3_n1469ContratoServicosDePara_OrigenId2 ;
      private String[] BC003S3_A1470ContratoServicosDePara_OrigemDsc2 ;
      private bool[] BC003S3_n1470ContratoServicosDePara_OrigemDsc2 ;
      private int[] BC003S3_A160ContratoServicos_Codigo ;
      private int[] BC003S2_A1465ContratoServicosDePara_Codigo ;
      private String[] BC003S2_A1466ContratoServicosDePara_Origem ;
      private long[] BC003S2_A1467ContratoServicosDePara_OrigenId ;
      private bool[] BC003S2_n1467ContratoServicosDePara_OrigenId ;
      private String[] BC003S2_A1468ContratoServicosDePara_OrigemDsc ;
      private bool[] BC003S2_n1468ContratoServicosDePara_OrigemDsc ;
      private long[] BC003S2_A1469ContratoServicosDePara_OrigenId2 ;
      private bool[] BC003S2_n1469ContratoServicosDePara_OrigenId2 ;
      private String[] BC003S2_A1470ContratoServicosDePara_OrigemDsc2 ;
      private bool[] BC003S2_n1470ContratoServicosDePara_OrigemDsc2 ;
      private int[] BC003S2_A160ContratoServicos_Codigo ;
      private int[] BC003S10_A1465ContratoServicosDePara_Codigo ;
      private String[] BC003S10_A1466ContratoServicosDePara_Origem ;
      private long[] BC003S10_A1467ContratoServicosDePara_OrigenId ;
      private bool[] BC003S10_n1467ContratoServicosDePara_OrigenId ;
      private String[] BC003S10_A1468ContratoServicosDePara_OrigemDsc ;
      private bool[] BC003S10_n1468ContratoServicosDePara_OrigemDsc ;
      private long[] BC003S10_A1469ContratoServicosDePara_OrigenId2 ;
      private bool[] BC003S10_n1469ContratoServicosDePara_OrigenId2 ;
      private String[] BC003S10_A1470ContratoServicosDePara_OrigemDsc2 ;
      private bool[] BC003S10_n1470ContratoServicosDePara_OrigemDsc2 ;
      private int[] BC003S10_A160ContratoServicos_Codigo ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private int[] BC003S11_A160ContratoServicos_Codigo ;
   }

   public class contratoservicosdepara_bc__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new UpdateCursor(def[5])
         ,new UpdateCursor(def[6])
         ,new UpdateCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmBC003S5 ;
          prmBC003S5 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosDePara_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003S4 ;
          prmBC003S4 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003S6 ;
          prmBC003S6 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosDePara_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003S3 ;
          prmBC003S3 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosDePara_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003S2 ;
          prmBC003S2 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosDePara_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003S7 ;
          prmBC003S7 = new Object[] {
          new Object[] {"@ContratoServicosDePara_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosDePara_Origem",SqlDbType.Char,2,0} ,
          new Object[] {"@ContratoServicosDePara_OrigenId",SqlDbType.Decimal,18,0} ,
          new Object[] {"@ContratoServicosDePara_OrigemDsc",SqlDbType.VarChar,500,0} ,
          new Object[] {"@ContratoServicosDePara_OrigenId2",SqlDbType.Decimal,18,0} ,
          new Object[] {"@ContratoServicosDePara_OrigemDsc2",SqlDbType.VarChar,500,0} ,
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003S8 ;
          prmBC003S8 = new Object[] {
          new Object[] {"@ContratoServicosDePara_Origem",SqlDbType.Char,2,0} ,
          new Object[] {"@ContratoServicosDePara_OrigenId",SqlDbType.Decimal,18,0} ,
          new Object[] {"@ContratoServicosDePara_OrigemDsc",SqlDbType.VarChar,500,0} ,
          new Object[] {"@ContratoServicosDePara_OrigenId2",SqlDbType.Decimal,18,0} ,
          new Object[] {"@ContratoServicosDePara_OrigemDsc2",SqlDbType.VarChar,500,0} ,
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosDePara_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003S9 ;
          prmBC003S9 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosDePara_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003S10 ;
          prmBC003S10 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoServicosDePara_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003S11 ;
          prmBC003S11 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("BC003S2", "SELECT [ContratoServicosDePara_Codigo], [ContratoServicosDePara_Origem], [ContratoServicosDePara_OrigenId], [ContratoServicosDePara_OrigemDsc], [ContratoServicosDePara_OrigenId2], [ContratoServicosDePara_OrigemDsc2], [ContratoServicos_Codigo] FROM [ContratoServicosDePara] WITH (UPDLOCK) WHERE [ContratoServicos_Codigo] = @ContratoServicos_Codigo AND [ContratoServicosDePara_Codigo] = @ContratoServicosDePara_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC003S2,1,0,true,false )
             ,new CursorDef("BC003S3", "SELECT [ContratoServicosDePara_Codigo], [ContratoServicosDePara_Origem], [ContratoServicosDePara_OrigenId], [ContratoServicosDePara_OrigemDsc], [ContratoServicosDePara_OrigenId2], [ContratoServicosDePara_OrigemDsc2], [ContratoServicos_Codigo] FROM [ContratoServicosDePara] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @ContratoServicos_Codigo AND [ContratoServicosDePara_Codigo] = @ContratoServicosDePara_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC003S3,1,0,true,false )
             ,new CursorDef("BC003S4", "SELECT [ContratoServicos_Codigo] FROM [ContratoServicos] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @ContratoServicos_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC003S4,1,0,true,false )
             ,new CursorDef("BC003S5", "SELECT TM1.[ContratoServicosDePara_Codigo], TM1.[ContratoServicosDePara_Origem], TM1.[ContratoServicosDePara_OrigenId], TM1.[ContratoServicosDePara_OrigemDsc], TM1.[ContratoServicosDePara_OrigenId2], TM1.[ContratoServicosDePara_OrigemDsc2], TM1.[ContratoServicos_Codigo] FROM [ContratoServicosDePara] TM1 WITH (NOLOCK) WHERE TM1.[ContratoServicos_Codigo] = @ContratoServicos_Codigo and TM1.[ContratoServicosDePara_Codigo] = @ContratoServicosDePara_Codigo ORDER BY TM1.[ContratoServicos_Codigo], TM1.[ContratoServicosDePara_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC003S5,100,0,true,false )
             ,new CursorDef("BC003S6", "SELECT [ContratoServicos_Codigo], [ContratoServicosDePara_Codigo] FROM [ContratoServicosDePara] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @ContratoServicos_Codigo AND [ContratoServicosDePara_Codigo] = @ContratoServicosDePara_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmBC003S6,1,0,true,false )
             ,new CursorDef("BC003S7", "INSERT INTO [ContratoServicosDePara]([ContratoServicosDePara_Codigo], [ContratoServicosDePara_Origem], [ContratoServicosDePara_OrigenId], [ContratoServicosDePara_OrigemDsc], [ContratoServicosDePara_OrigenId2], [ContratoServicosDePara_OrigemDsc2], [ContratoServicos_Codigo]) VALUES(@ContratoServicosDePara_Codigo, @ContratoServicosDePara_Origem, @ContratoServicosDePara_OrigenId, @ContratoServicosDePara_OrigemDsc, @ContratoServicosDePara_OrigenId2, @ContratoServicosDePara_OrigemDsc2, @ContratoServicos_Codigo)", GxErrorMask.GX_NOMASK,prmBC003S7)
             ,new CursorDef("BC003S8", "UPDATE [ContratoServicosDePara] SET [ContratoServicosDePara_Origem]=@ContratoServicosDePara_Origem, [ContratoServicosDePara_OrigenId]=@ContratoServicosDePara_OrigenId, [ContratoServicosDePara_OrigemDsc]=@ContratoServicosDePara_OrigemDsc, [ContratoServicosDePara_OrigenId2]=@ContratoServicosDePara_OrigenId2, [ContratoServicosDePara_OrigemDsc2]=@ContratoServicosDePara_OrigemDsc2  WHERE [ContratoServicos_Codigo] = @ContratoServicos_Codigo AND [ContratoServicosDePara_Codigo] = @ContratoServicosDePara_Codigo", GxErrorMask.GX_NOMASK,prmBC003S8)
             ,new CursorDef("BC003S9", "DELETE FROM [ContratoServicosDePara]  WHERE [ContratoServicos_Codigo] = @ContratoServicos_Codigo AND [ContratoServicosDePara_Codigo] = @ContratoServicosDePara_Codigo", GxErrorMask.GX_NOMASK,prmBC003S9)
             ,new CursorDef("BC003S10", "SELECT TM1.[ContratoServicosDePara_Codigo], TM1.[ContratoServicosDePara_Origem], TM1.[ContratoServicosDePara_OrigenId], TM1.[ContratoServicosDePara_OrigemDsc], TM1.[ContratoServicosDePara_OrigenId2], TM1.[ContratoServicosDePara_OrigemDsc2], TM1.[ContratoServicos_Codigo] FROM [ContratoServicosDePara] TM1 WITH (NOLOCK) WHERE TM1.[ContratoServicos_Codigo] = @ContratoServicos_Codigo and TM1.[ContratoServicosDePara_Codigo] = @ContratoServicosDePara_Codigo ORDER BY TM1.[ContratoServicos_Codigo], TM1.[ContratoServicosDePara_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC003S10,100,0,true,false )
             ,new CursorDef("BC003S11", "SELECT [ContratoServicos_Codigo] FROM [ContratoServicos] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @ContratoServicos_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC003S11,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 2) ;
                ((long[]) buf[2])[0] = rslt.getLong(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getLongVarchar(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((long[]) buf[6])[0] = rslt.getLong(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getLongVarchar(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((int[]) buf[10])[0] = rslt.getInt(7) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 2) ;
                ((long[]) buf[2])[0] = rslt.getLong(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getLongVarchar(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((long[]) buf[6])[0] = rslt.getLong(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getLongVarchar(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((int[]) buf[10])[0] = rslt.getInt(7) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 2) ;
                ((long[]) buf[2])[0] = rslt.getLong(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getLongVarchar(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((long[]) buf[6])[0] = rslt.getLong(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getLongVarchar(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((int[]) buf[10])[0] = rslt.getInt(7) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 2) ;
                ((long[]) buf[2])[0] = rslt.getLong(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getLongVarchar(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((long[]) buf[6])[0] = rslt.getLong(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getLongVarchar(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((int[]) buf[10])[0] = rslt.getInt(7) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 3 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(3, (long)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 4 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 5 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(5, (long)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 6 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(6, (String)parms[9]);
                }
                stmt.SetParameter(7, (int)parms[10]);
                return;
             case 6 :
                stmt.SetParameter(1, (String)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(2, (long)parms[2]);
                }
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 3 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 4 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(4, (long)parms[6]);
                }
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 5 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(5, (String)parms[8]);
                }
                stmt.SetParameter(6, (int)parms[9]);
                stmt.SetParameter(7, (int)parms[10]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
