/*
               File: type_SdtServico
        Description: Servico
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:15:35.33
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "Servico" )]
   [XmlType(TypeName =  "Servico" , Namespace = "GxEv3Up14_MeetrikaVs3" )]
   [Serializable]
   public class SdtServico : GxSilentTrnSdt, System.Web.SessionState.IRequiresSessionState
   {
      public SdtServico( )
      {
         /* Constructor for serialization */
         gxTv_SdtServico_Servico_nome = "";
         gxTv_SdtServico_Servico_descricao = "";
         gxTv_SdtServico_Servico_sigla = "";
         gxTv_SdtServico_Servicogrupo_descricao = "";
         gxTv_SdtServico_Servico_vincdesc = "";
         gxTv_SdtServico_Servico_vincsigla = "";
         gxTv_SdtServico_Servico_tela = "";
         gxTv_SdtServico_Servico_atende = "";
         gxTv_SdtServico_Servico_obrigavalores = "";
         gxTv_SdtServico_Servico_objetocontrole = "";
         gxTv_SdtServico_Servico_linnegdsc = "";
         gxTv_SdtServico_Servico_identificacao = "";
         gxTv_SdtServico_Mode = "";
         gxTv_SdtServico_Servico_nome_Z = "";
         gxTv_SdtServico_Servico_sigla_Z = "";
         gxTv_SdtServico_Servicogrupo_descricao_Z = "";
         gxTv_SdtServico_Servico_vincsigla_Z = "";
         gxTv_SdtServico_Servico_tela_Z = "";
         gxTv_SdtServico_Servico_atende_Z = "";
         gxTv_SdtServico_Servico_obrigavalores_Z = "";
         gxTv_SdtServico_Servico_objetocontrole_Z = "";
         gxTv_SdtServico_Servico_linnegdsc_Z = "";
         gxTv_SdtServico_Servico_identificacao_Z = "";
      }

      public SdtServico( IGxContext context )
      {
         this.context = context;
         constructorCallingAssembly = Assembly.GetCallingAssembly();
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public void Load( int AV155Servico_Codigo )
      {
         IGxSilentTrn obj ;
         obj = getTransaction();
         obj.LoadKey(new Object[] {(int)AV155Servico_Codigo});
         return  ;
      }

      public override Object[][] GetBCKey( )
      {
         return (Object[][])(new Object[][]{new Object[]{"Servico_Codigo", typeof(int)}}) ;
      }

      public override IGxCollection GetMessages( )
      {
         short item = 1 ;
         IGxCollection msgs = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs") ;
         SdtMessages_Message m1 ;
         IGxSilentTrn trn = getTransaction() ;
         msglist msgList = trn.GetMessages() ;
         while ( item <= msgList.ItemCount )
         {
            m1 = new SdtMessages_Message(context);
            m1.gxTpr_Id = msgList.getItemValue(item);
            m1.gxTpr_Description = msgList.getItemText(item);
            m1.gxTpr_Type = msgList.getItemType(item);
            msgs.Add(m1, 0);
            item = (short)(item+1);
         }
         return msgs ;
      }

      public override GXProperties GetMetadata( )
      {
         GXProperties metadata = new GXProperties() ;
         metadata.Set("Name", "Servico");
         metadata.Set("BT", "Servico");
         metadata.Set("PK", "[ \"Servico_Codigo\" ]");
         metadata.Set("PKAssigned", "[ \"Servico_Codigo\" ]");
         metadata.Set("FKList", "[ { \"FK\":[ \"LinhadeNegocio_Codigo\" ],\"FKMap\":[ \"Servico_LinNegCod-LinhadeNegocio_Codigo\" ] },{ \"FK\":[ \"ServicoGrupo_Codigo\" ],\"FKMap\":[  ] },{ \"FK\":[ \"Servico_Codigo\" ],\"FKMap\":[ \"Servico_Vinculado-Servico_Codigo\" ] },{ \"FK\":[ \"UnidadeOrganizacional_Codigo\" ],\"FKMap\":[ \"Servico_UO-UnidadeOrganizacional_Codigo\" ] } ]");
         metadata.Set("AllowInsert", "True");
         metadata.Set("AllowUpdate", "True");
         metadata.Set("AllowDelete", "True");
         return metadata ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         if ( ! includeState )
         {
            XmlAttributeOverrides ov = new XmlAttributeOverrides();
            XmlAttributes attrs = new XmlAttributes();
            attrs.XmlIgnore = true;
            ov.Add(this.GetType(),  "gxTpr_Mode" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Initialized" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Servico_codigo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Servico_nome_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Servico_sigla_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Servicogrupo_codigo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Servicogrupo_descricao_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Servico_uo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Servico_uorespexclusiva_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Servico_vinculado_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Servico_vinculados_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Servico_vincsigla_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Servico_terceriza_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Servico_tela_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Servico_atende_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Servico_obrigavalores_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Servico_objetocontrole_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Servico_tipohierarquia_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Servico_perctmp_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Servico_percpgm_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Servico_perccnc_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Servico_anterior_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Servico_posterior_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Servico_responsavel_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Servico_ativo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Servico_ispublico_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Servico_linnegcod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Servico_linnegdsc_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Servico_isorigemreferencia_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Servico_identificacao_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Servico_pausasla_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Servico_descricao_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Servico_uo_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Servico_uorespexclusiva_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Servico_vinculado_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Servico_vincdesc_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Servico_vincsigla_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Servico_terceriza_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Servico_tela_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Servico_atende_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Servico_obrigavalores_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Servico_tipohierarquia_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Servico_perctmp_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Servico_percpgm_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Servico_perccnc_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Servico_anterior_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Servico_posterior_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Servico_linnegcod_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Servico_linnegdsc_N" , attrs);
            xmls = new XmlSerializer(this.GetType(), ov, new Type[0], null, sNameSpace);
         }
         else
         {
            xmls = new XmlSerializer(this.GetType(), sNameSpace);
         }
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtServico deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_MeetrikaVs3" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtServico)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtServico obj ;
         obj = this;
         obj.gxTpr_Servico_codigo = deserialized.gxTpr_Servico_codigo;
         obj.gxTpr_Servico_nome = deserialized.gxTpr_Servico_nome;
         obj.gxTpr_Servico_descricao = deserialized.gxTpr_Servico_descricao;
         obj.gxTpr_Servico_sigla = deserialized.gxTpr_Servico_sigla;
         obj.gxTpr_Servicogrupo_codigo = deserialized.gxTpr_Servicogrupo_codigo;
         obj.gxTpr_Servicogrupo_descricao = deserialized.gxTpr_Servicogrupo_descricao;
         obj.gxTpr_Servico_uo = deserialized.gxTpr_Servico_uo;
         obj.gxTpr_Servico_uorespexclusiva = deserialized.gxTpr_Servico_uorespexclusiva;
         obj.gxTpr_Servico_vinculado = deserialized.gxTpr_Servico_vinculado;
         obj.gxTpr_Servico_vinculados = deserialized.gxTpr_Servico_vinculados;
         obj.gxTpr_Servico_vincdesc = deserialized.gxTpr_Servico_vincdesc;
         obj.gxTpr_Servico_vincsigla = deserialized.gxTpr_Servico_vincsigla;
         obj.gxTpr_Servico_terceriza = deserialized.gxTpr_Servico_terceriza;
         obj.gxTpr_Servico_tela = deserialized.gxTpr_Servico_tela;
         obj.gxTpr_Servico_atende = deserialized.gxTpr_Servico_atende;
         obj.gxTpr_Servico_obrigavalores = deserialized.gxTpr_Servico_obrigavalores;
         obj.gxTpr_Servico_objetocontrole = deserialized.gxTpr_Servico_objetocontrole;
         obj.gxTpr_Servico_tipohierarquia = deserialized.gxTpr_Servico_tipohierarquia;
         obj.gxTpr_Servico_perctmp = deserialized.gxTpr_Servico_perctmp;
         obj.gxTpr_Servico_percpgm = deserialized.gxTpr_Servico_percpgm;
         obj.gxTpr_Servico_perccnc = deserialized.gxTpr_Servico_perccnc;
         obj.gxTpr_Servico_anterior = deserialized.gxTpr_Servico_anterior;
         obj.gxTpr_Servico_posterior = deserialized.gxTpr_Servico_posterior;
         obj.gxTpr_Servico_responsavel = deserialized.gxTpr_Servico_responsavel;
         obj.gxTpr_Servico_ativo = deserialized.gxTpr_Servico_ativo;
         obj.gxTpr_Servico_ispublico = deserialized.gxTpr_Servico_ispublico;
         obj.gxTpr_Servico_linnegcod = deserialized.gxTpr_Servico_linnegcod;
         obj.gxTpr_Servico_linnegdsc = deserialized.gxTpr_Servico_linnegdsc;
         obj.gxTpr_Servico_isorigemreferencia = deserialized.gxTpr_Servico_isorigemreferencia;
         obj.gxTpr_Servico_identificacao = deserialized.gxTpr_Servico_identificacao;
         obj.gxTpr_Servico_pausasla = deserialized.gxTpr_Servico_pausasla;
         obj.gxTpr_Mode = deserialized.gxTpr_Mode;
         obj.gxTpr_Initialized = deserialized.gxTpr_Initialized;
         obj.gxTpr_Servico_codigo_Z = deserialized.gxTpr_Servico_codigo_Z;
         obj.gxTpr_Servico_nome_Z = deserialized.gxTpr_Servico_nome_Z;
         obj.gxTpr_Servico_sigla_Z = deserialized.gxTpr_Servico_sigla_Z;
         obj.gxTpr_Servicogrupo_codigo_Z = deserialized.gxTpr_Servicogrupo_codigo_Z;
         obj.gxTpr_Servicogrupo_descricao_Z = deserialized.gxTpr_Servicogrupo_descricao_Z;
         obj.gxTpr_Servico_uo_Z = deserialized.gxTpr_Servico_uo_Z;
         obj.gxTpr_Servico_uorespexclusiva_Z = deserialized.gxTpr_Servico_uorespexclusiva_Z;
         obj.gxTpr_Servico_vinculado_Z = deserialized.gxTpr_Servico_vinculado_Z;
         obj.gxTpr_Servico_vinculados_Z = deserialized.gxTpr_Servico_vinculados_Z;
         obj.gxTpr_Servico_vincsigla_Z = deserialized.gxTpr_Servico_vincsigla_Z;
         obj.gxTpr_Servico_terceriza_Z = deserialized.gxTpr_Servico_terceriza_Z;
         obj.gxTpr_Servico_tela_Z = deserialized.gxTpr_Servico_tela_Z;
         obj.gxTpr_Servico_atende_Z = deserialized.gxTpr_Servico_atende_Z;
         obj.gxTpr_Servico_obrigavalores_Z = deserialized.gxTpr_Servico_obrigavalores_Z;
         obj.gxTpr_Servico_objetocontrole_Z = deserialized.gxTpr_Servico_objetocontrole_Z;
         obj.gxTpr_Servico_tipohierarquia_Z = deserialized.gxTpr_Servico_tipohierarquia_Z;
         obj.gxTpr_Servico_perctmp_Z = deserialized.gxTpr_Servico_perctmp_Z;
         obj.gxTpr_Servico_percpgm_Z = deserialized.gxTpr_Servico_percpgm_Z;
         obj.gxTpr_Servico_perccnc_Z = deserialized.gxTpr_Servico_perccnc_Z;
         obj.gxTpr_Servico_anterior_Z = deserialized.gxTpr_Servico_anterior_Z;
         obj.gxTpr_Servico_posterior_Z = deserialized.gxTpr_Servico_posterior_Z;
         obj.gxTpr_Servico_responsavel_Z = deserialized.gxTpr_Servico_responsavel_Z;
         obj.gxTpr_Servico_ativo_Z = deserialized.gxTpr_Servico_ativo_Z;
         obj.gxTpr_Servico_ispublico_Z = deserialized.gxTpr_Servico_ispublico_Z;
         obj.gxTpr_Servico_linnegcod_Z = deserialized.gxTpr_Servico_linnegcod_Z;
         obj.gxTpr_Servico_linnegdsc_Z = deserialized.gxTpr_Servico_linnegdsc_Z;
         obj.gxTpr_Servico_isorigemreferencia_Z = deserialized.gxTpr_Servico_isorigemreferencia_Z;
         obj.gxTpr_Servico_identificacao_Z = deserialized.gxTpr_Servico_identificacao_Z;
         obj.gxTpr_Servico_pausasla_Z = deserialized.gxTpr_Servico_pausasla_Z;
         obj.gxTpr_Servico_descricao_N = deserialized.gxTpr_Servico_descricao_N;
         obj.gxTpr_Servico_uo_N = deserialized.gxTpr_Servico_uo_N;
         obj.gxTpr_Servico_uorespexclusiva_N = deserialized.gxTpr_Servico_uorespexclusiva_N;
         obj.gxTpr_Servico_vinculado_N = deserialized.gxTpr_Servico_vinculado_N;
         obj.gxTpr_Servico_vincdesc_N = deserialized.gxTpr_Servico_vincdesc_N;
         obj.gxTpr_Servico_vincsigla_N = deserialized.gxTpr_Servico_vincsigla_N;
         obj.gxTpr_Servico_terceriza_N = deserialized.gxTpr_Servico_terceriza_N;
         obj.gxTpr_Servico_tela_N = deserialized.gxTpr_Servico_tela_N;
         obj.gxTpr_Servico_atende_N = deserialized.gxTpr_Servico_atende_N;
         obj.gxTpr_Servico_obrigavalores_N = deserialized.gxTpr_Servico_obrigavalores_N;
         obj.gxTpr_Servico_tipohierarquia_N = deserialized.gxTpr_Servico_tipohierarquia_N;
         obj.gxTpr_Servico_perctmp_N = deserialized.gxTpr_Servico_perctmp_N;
         obj.gxTpr_Servico_percpgm_N = deserialized.gxTpr_Servico_percpgm_N;
         obj.gxTpr_Servico_perccnc_N = deserialized.gxTpr_Servico_perccnc_N;
         obj.gxTpr_Servico_anterior_N = deserialized.gxTpr_Servico_anterior_N;
         obj.gxTpr_Servico_posterior_N = deserialized.gxTpr_Servico_posterior_N;
         obj.gxTpr_Servico_linnegcod_N = deserialized.gxTpr_Servico_linnegcod_N;
         obj.gxTpr_Servico_linnegdsc_N = deserialized.gxTpr_Servico_linnegdsc_N;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_Codigo") )
               {
                  gxTv_SdtServico_Servico_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_Nome") )
               {
                  gxTv_SdtServico_Servico_nome = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_Descricao") )
               {
                  gxTv_SdtServico_Servico_descricao = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_Sigla") )
               {
                  gxTv_SdtServico_Servico_sigla = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ServicoGrupo_Codigo") )
               {
                  gxTv_SdtServico_Servicogrupo_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ServicoGrupo_Descricao") )
               {
                  gxTv_SdtServico_Servicogrupo_descricao = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_UO") )
               {
                  gxTv_SdtServico_Servico_uo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_UORespExclusiva") )
               {
                  gxTv_SdtServico_Servico_uorespexclusiva = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_Vinculado") )
               {
                  gxTv_SdtServico_Servico_vinculado = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_Vinculados") )
               {
                  gxTv_SdtServico_Servico_vinculados = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_VincDesc") )
               {
                  gxTv_SdtServico_Servico_vincdesc = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_VincSigla") )
               {
                  gxTv_SdtServico_Servico_vincsigla = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_Terceriza") )
               {
                  gxTv_SdtServico_Servico_terceriza = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_Tela") )
               {
                  gxTv_SdtServico_Servico_tela = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_Atende") )
               {
                  gxTv_SdtServico_Servico_atende = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_ObrigaValores") )
               {
                  gxTv_SdtServico_Servico_obrigavalores = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_ObjetoControle") )
               {
                  gxTv_SdtServico_Servico_objetocontrole = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_TipoHierarquia") )
               {
                  gxTv_SdtServico_Servico_tipohierarquia = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_PercTmp") )
               {
                  gxTv_SdtServico_Servico_perctmp = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_PercPgm") )
               {
                  gxTv_SdtServico_Servico_percpgm = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_PercCnc") )
               {
                  gxTv_SdtServico_Servico_perccnc = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_Anterior") )
               {
                  gxTv_SdtServico_Servico_anterior = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_Posterior") )
               {
                  gxTv_SdtServico_Servico_posterior = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_Responsavel") )
               {
                  gxTv_SdtServico_Servico_responsavel = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_Ativo") )
               {
                  gxTv_SdtServico_Servico_ativo = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_IsPublico") )
               {
                  gxTv_SdtServico_Servico_ispublico = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_LinNegCod") )
               {
                  gxTv_SdtServico_Servico_linnegcod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_LinNegDsc") )
               {
                  gxTv_SdtServico_Servico_linnegdsc = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_IsOrigemReferencia") )
               {
                  gxTv_SdtServico_Servico_isorigemreferencia = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_Identificacao") )
               {
                  gxTv_SdtServico_Servico_identificacao = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_PausaSLA") )
               {
                  gxTv_SdtServico_Servico_pausasla = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Mode") )
               {
                  gxTv_SdtServico_Mode = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Initialized") )
               {
                  gxTv_SdtServico_Initialized = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_Codigo_Z") )
               {
                  gxTv_SdtServico_Servico_codigo_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_Nome_Z") )
               {
                  gxTv_SdtServico_Servico_nome_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_Sigla_Z") )
               {
                  gxTv_SdtServico_Servico_sigla_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ServicoGrupo_Codigo_Z") )
               {
                  gxTv_SdtServico_Servicogrupo_codigo_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ServicoGrupo_Descricao_Z") )
               {
                  gxTv_SdtServico_Servicogrupo_descricao_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_UO_Z") )
               {
                  gxTv_SdtServico_Servico_uo_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_UORespExclusiva_Z") )
               {
                  gxTv_SdtServico_Servico_uorespexclusiva_Z = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_Vinculado_Z") )
               {
                  gxTv_SdtServico_Servico_vinculado_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_Vinculados_Z") )
               {
                  gxTv_SdtServico_Servico_vinculados_Z = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_VincSigla_Z") )
               {
                  gxTv_SdtServico_Servico_vincsigla_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_Terceriza_Z") )
               {
                  gxTv_SdtServico_Servico_terceriza_Z = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_Tela_Z") )
               {
                  gxTv_SdtServico_Servico_tela_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_Atende_Z") )
               {
                  gxTv_SdtServico_Servico_atende_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_ObrigaValores_Z") )
               {
                  gxTv_SdtServico_Servico_obrigavalores_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_ObjetoControle_Z") )
               {
                  gxTv_SdtServico_Servico_objetocontrole_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_TipoHierarquia_Z") )
               {
                  gxTv_SdtServico_Servico_tipohierarquia_Z = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_PercTmp_Z") )
               {
                  gxTv_SdtServico_Servico_perctmp_Z = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_PercPgm_Z") )
               {
                  gxTv_SdtServico_Servico_percpgm_Z = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_PercCnc_Z") )
               {
                  gxTv_SdtServico_Servico_perccnc_Z = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_Anterior_Z") )
               {
                  gxTv_SdtServico_Servico_anterior_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_Posterior_Z") )
               {
                  gxTv_SdtServico_Servico_posterior_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_Responsavel_Z") )
               {
                  gxTv_SdtServico_Servico_responsavel_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_Ativo_Z") )
               {
                  gxTv_SdtServico_Servico_ativo_Z = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_IsPublico_Z") )
               {
                  gxTv_SdtServico_Servico_ispublico_Z = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_LinNegCod_Z") )
               {
                  gxTv_SdtServico_Servico_linnegcod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_LinNegDsc_Z") )
               {
                  gxTv_SdtServico_Servico_linnegdsc_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_IsOrigemReferencia_Z") )
               {
                  gxTv_SdtServico_Servico_isorigemreferencia_Z = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_Identificacao_Z") )
               {
                  gxTv_SdtServico_Servico_identificacao_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_PausaSLA_Z") )
               {
                  gxTv_SdtServico_Servico_pausasla_Z = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_Descricao_N") )
               {
                  gxTv_SdtServico_Servico_descricao_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_UO_N") )
               {
                  gxTv_SdtServico_Servico_uo_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_UORespExclusiva_N") )
               {
                  gxTv_SdtServico_Servico_uorespexclusiva_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_Vinculado_N") )
               {
                  gxTv_SdtServico_Servico_vinculado_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_VincDesc_N") )
               {
                  gxTv_SdtServico_Servico_vincdesc_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_VincSigla_N") )
               {
                  gxTv_SdtServico_Servico_vincsigla_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_Terceriza_N") )
               {
                  gxTv_SdtServico_Servico_terceriza_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_Tela_N") )
               {
                  gxTv_SdtServico_Servico_tela_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_Atende_N") )
               {
                  gxTv_SdtServico_Servico_atende_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_ObrigaValores_N") )
               {
                  gxTv_SdtServico_Servico_obrigavalores_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_TipoHierarquia_N") )
               {
                  gxTv_SdtServico_Servico_tipohierarquia_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_PercTmp_N") )
               {
                  gxTv_SdtServico_Servico_perctmp_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_PercPgm_N") )
               {
                  gxTv_SdtServico_Servico_percpgm_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_PercCnc_N") )
               {
                  gxTv_SdtServico_Servico_perccnc_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_Anterior_N") )
               {
                  gxTv_SdtServico_Servico_anterior_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_Posterior_N") )
               {
                  gxTv_SdtServico_Servico_posterior_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_LinNegCod_N") )
               {
                  gxTv_SdtServico_Servico_linnegcod_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Servico_LinNegDsc_N") )
               {
                  gxTv_SdtServico_Servico_linnegdsc_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "Servico";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "GxEv3Up14_MeetrikaVs3";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("Servico_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtServico_Servico_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Servico_Nome", StringUtil.RTrim( gxTv_SdtServico_Servico_nome));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Servico_Descricao", StringUtil.RTrim( gxTv_SdtServico_Servico_descricao));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Servico_Sigla", StringUtil.RTrim( gxTv_SdtServico_Servico_sigla));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ServicoGrupo_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtServico_Servicogrupo_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ServicoGrupo_Descricao", StringUtil.RTrim( gxTv_SdtServico_Servicogrupo_descricao));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Servico_UO", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtServico_Servico_uo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Servico_UORespExclusiva", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtServico_Servico_uorespexclusiva)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Servico_Vinculado", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtServico_Servico_vinculado), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Servico_Vinculados", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtServico_Servico_vinculados), 4, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Servico_VincDesc", StringUtil.RTrim( gxTv_SdtServico_Servico_vincdesc));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Servico_VincSigla", StringUtil.RTrim( gxTv_SdtServico_Servico_vincsigla));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Servico_Terceriza", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtServico_Servico_terceriza)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Servico_Tela", StringUtil.RTrim( gxTv_SdtServico_Servico_tela));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Servico_Atende", StringUtil.RTrim( gxTv_SdtServico_Servico_atende));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Servico_ObrigaValores", StringUtil.RTrim( gxTv_SdtServico_Servico_obrigavalores));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Servico_ObjetoControle", StringUtil.RTrim( gxTv_SdtServico_Servico_objetocontrole));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Servico_TipoHierarquia", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtServico_Servico_tipohierarquia), 4, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Servico_PercTmp", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtServico_Servico_perctmp), 4, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Servico_PercPgm", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtServico_Servico_percpgm), 4, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Servico_PercCnc", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtServico_Servico_perccnc), 4, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Servico_Anterior", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtServico_Servico_anterior), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Servico_Posterior", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtServico_Servico_posterior), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Servico_Responsavel", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtServico_Servico_responsavel), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Servico_Ativo", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtServico_Servico_ativo)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Servico_IsPublico", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtServico_Servico_ispublico)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Servico_LinNegCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtServico_Servico_linnegcod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Servico_LinNegDsc", StringUtil.RTrim( gxTv_SdtServico_Servico_linnegdsc));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Servico_IsOrigemReferencia", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtServico_Servico_isorigemreferencia)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Servico_Identificacao", StringUtil.RTrim( gxTv_SdtServico_Servico_identificacao));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Servico_PausaSLA", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtServico_Servico_pausasla)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         if ( sIncludeState )
         {
            oWriter.WriteElement("Mode", StringUtil.RTrim( gxTv_SdtServico_Mode));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Initialized", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtServico_Initialized), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Servico_Codigo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtServico_Servico_codigo_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Servico_Nome_Z", StringUtil.RTrim( gxTv_SdtServico_Servico_nome_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Servico_Sigla_Z", StringUtil.RTrim( gxTv_SdtServico_Servico_sigla_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ServicoGrupo_Codigo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtServico_Servicogrupo_codigo_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ServicoGrupo_Descricao_Z", StringUtil.RTrim( gxTv_SdtServico_Servicogrupo_descricao_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Servico_UO_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtServico_Servico_uo_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Servico_UORespExclusiva_Z", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtServico_Servico_uorespexclusiva_Z)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Servico_Vinculado_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtServico_Servico_vinculado_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Servico_Vinculados_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtServico_Servico_vinculados_Z), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Servico_VincSigla_Z", StringUtil.RTrim( gxTv_SdtServico_Servico_vincsigla_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Servico_Terceriza_Z", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtServico_Servico_terceriza_Z)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Servico_Tela_Z", StringUtil.RTrim( gxTv_SdtServico_Servico_tela_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Servico_Atende_Z", StringUtil.RTrim( gxTv_SdtServico_Servico_atende_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Servico_ObrigaValores_Z", StringUtil.RTrim( gxTv_SdtServico_Servico_obrigavalores_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Servico_ObjetoControle_Z", StringUtil.RTrim( gxTv_SdtServico_Servico_objetocontrole_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Servico_TipoHierarquia_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtServico_Servico_tipohierarquia_Z), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Servico_PercTmp_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtServico_Servico_perctmp_Z), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Servico_PercPgm_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtServico_Servico_percpgm_Z), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Servico_PercCnc_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtServico_Servico_perccnc_Z), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Servico_Anterior_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtServico_Servico_anterior_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Servico_Posterior_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtServico_Servico_posterior_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Servico_Responsavel_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtServico_Servico_responsavel_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Servico_Ativo_Z", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtServico_Servico_ativo_Z)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Servico_IsPublico_Z", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtServico_Servico_ispublico_Z)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Servico_LinNegCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtServico_Servico_linnegcod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Servico_LinNegDsc_Z", StringUtil.RTrim( gxTv_SdtServico_Servico_linnegdsc_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Servico_IsOrigemReferencia_Z", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtServico_Servico_isorigemreferencia_Z)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Servico_Identificacao_Z", StringUtil.RTrim( gxTv_SdtServico_Servico_identificacao_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Servico_PausaSLA_Z", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtServico_Servico_pausasla_Z)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Servico_Descricao_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtServico_Servico_descricao_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Servico_UO_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtServico_Servico_uo_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Servico_UORespExclusiva_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtServico_Servico_uorespexclusiva_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Servico_Vinculado_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtServico_Servico_vinculado_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Servico_VincDesc_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtServico_Servico_vincdesc_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Servico_VincSigla_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtServico_Servico_vincsigla_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Servico_Terceriza_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtServico_Servico_terceriza_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Servico_Tela_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtServico_Servico_tela_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Servico_Atende_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtServico_Servico_atende_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Servico_ObrigaValores_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtServico_Servico_obrigavalores_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Servico_TipoHierarquia_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtServico_Servico_tipohierarquia_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Servico_PercTmp_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtServico_Servico_perctmp_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Servico_PercPgm_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtServico_Servico_percpgm_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Servico_PercCnc_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtServico_Servico_perccnc_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Servico_Anterior_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtServico_Servico_anterior_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Servico_Posterior_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtServico_Servico_posterior_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Servico_LinNegCod_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtServico_Servico_linnegcod_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Servico_LinNegDsc_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtServico_Servico_linnegdsc_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("Servico_Codigo", gxTv_SdtServico_Servico_codigo, false);
         AddObjectProperty("Servico_Nome", gxTv_SdtServico_Servico_nome, false);
         AddObjectProperty("Servico_Descricao", gxTv_SdtServico_Servico_descricao, false);
         AddObjectProperty("Servico_Sigla", gxTv_SdtServico_Servico_sigla, false);
         AddObjectProperty("ServicoGrupo_Codigo", gxTv_SdtServico_Servicogrupo_codigo, false);
         AddObjectProperty("ServicoGrupo_Descricao", gxTv_SdtServico_Servicogrupo_descricao, false);
         AddObjectProperty("Servico_UO", gxTv_SdtServico_Servico_uo, false);
         AddObjectProperty("Servico_UORespExclusiva", gxTv_SdtServico_Servico_uorespexclusiva, false);
         AddObjectProperty("Servico_Vinculado", gxTv_SdtServico_Servico_vinculado, false);
         AddObjectProperty("Servico_Vinculados", gxTv_SdtServico_Servico_vinculados, false);
         AddObjectProperty("Servico_VincDesc", gxTv_SdtServico_Servico_vincdesc, false);
         AddObjectProperty("Servico_VincSigla", gxTv_SdtServico_Servico_vincsigla, false);
         AddObjectProperty("Servico_Terceriza", gxTv_SdtServico_Servico_terceriza, false);
         AddObjectProperty("Servico_Tela", gxTv_SdtServico_Servico_tela, false);
         AddObjectProperty("Servico_Atende", gxTv_SdtServico_Servico_atende, false);
         AddObjectProperty("Servico_ObrigaValores", gxTv_SdtServico_Servico_obrigavalores, false);
         AddObjectProperty("Servico_ObjetoControle", gxTv_SdtServico_Servico_objetocontrole, false);
         AddObjectProperty("Servico_TipoHierarquia", gxTv_SdtServico_Servico_tipohierarquia, false);
         AddObjectProperty("Servico_PercTmp", gxTv_SdtServico_Servico_perctmp, false);
         AddObjectProperty("Servico_PercPgm", gxTv_SdtServico_Servico_percpgm, false);
         AddObjectProperty("Servico_PercCnc", gxTv_SdtServico_Servico_perccnc, false);
         AddObjectProperty("Servico_Anterior", gxTv_SdtServico_Servico_anterior, false);
         AddObjectProperty("Servico_Posterior", gxTv_SdtServico_Servico_posterior, false);
         AddObjectProperty("Servico_Responsavel", gxTv_SdtServico_Servico_responsavel, false);
         AddObjectProperty("Servico_Ativo", gxTv_SdtServico_Servico_ativo, false);
         AddObjectProperty("Servico_IsPublico", gxTv_SdtServico_Servico_ispublico, false);
         AddObjectProperty("Servico_LinNegCod", gxTv_SdtServico_Servico_linnegcod, false);
         AddObjectProperty("Servico_LinNegDsc", gxTv_SdtServico_Servico_linnegdsc, false);
         AddObjectProperty("Servico_IsOrigemReferencia", gxTv_SdtServico_Servico_isorigemreferencia, false);
         AddObjectProperty("Servico_Identificacao", gxTv_SdtServico_Servico_identificacao, false);
         AddObjectProperty("Servico_PausaSLA", gxTv_SdtServico_Servico_pausasla, false);
         if ( includeState )
         {
            AddObjectProperty("Mode", gxTv_SdtServico_Mode, false);
            AddObjectProperty("Initialized", gxTv_SdtServico_Initialized, false);
            AddObjectProperty("Servico_Codigo_Z", gxTv_SdtServico_Servico_codigo_Z, false);
            AddObjectProperty("Servico_Nome_Z", gxTv_SdtServico_Servico_nome_Z, false);
            AddObjectProperty("Servico_Sigla_Z", gxTv_SdtServico_Servico_sigla_Z, false);
            AddObjectProperty("ServicoGrupo_Codigo_Z", gxTv_SdtServico_Servicogrupo_codigo_Z, false);
            AddObjectProperty("ServicoGrupo_Descricao_Z", gxTv_SdtServico_Servicogrupo_descricao_Z, false);
            AddObjectProperty("Servico_UO_Z", gxTv_SdtServico_Servico_uo_Z, false);
            AddObjectProperty("Servico_UORespExclusiva_Z", gxTv_SdtServico_Servico_uorespexclusiva_Z, false);
            AddObjectProperty("Servico_Vinculado_Z", gxTv_SdtServico_Servico_vinculado_Z, false);
            AddObjectProperty("Servico_Vinculados_Z", gxTv_SdtServico_Servico_vinculados_Z, false);
            AddObjectProperty("Servico_VincSigla_Z", gxTv_SdtServico_Servico_vincsigla_Z, false);
            AddObjectProperty("Servico_Terceriza_Z", gxTv_SdtServico_Servico_terceriza_Z, false);
            AddObjectProperty("Servico_Tela_Z", gxTv_SdtServico_Servico_tela_Z, false);
            AddObjectProperty("Servico_Atende_Z", gxTv_SdtServico_Servico_atende_Z, false);
            AddObjectProperty("Servico_ObrigaValores_Z", gxTv_SdtServico_Servico_obrigavalores_Z, false);
            AddObjectProperty("Servico_ObjetoControle_Z", gxTv_SdtServico_Servico_objetocontrole_Z, false);
            AddObjectProperty("Servico_TipoHierarquia_Z", gxTv_SdtServico_Servico_tipohierarquia_Z, false);
            AddObjectProperty("Servico_PercTmp_Z", gxTv_SdtServico_Servico_perctmp_Z, false);
            AddObjectProperty("Servico_PercPgm_Z", gxTv_SdtServico_Servico_percpgm_Z, false);
            AddObjectProperty("Servico_PercCnc_Z", gxTv_SdtServico_Servico_perccnc_Z, false);
            AddObjectProperty("Servico_Anterior_Z", gxTv_SdtServico_Servico_anterior_Z, false);
            AddObjectProperty("Servico_Posterior_Z", gxTv_SdtServico_Servico_posterior_Z, false);
            AddObjectProperty("Servico_Responsavel_Z", gxTv_SdtServico_Servico_responsavel_Z, false);
            AddObjectProperty("Servico_Ativo_Z", gxTv_SdtServico_Servico_ativo_Z, false);
            AddObjectProperty("Servico_IsPublico_Z", gxTv_SdtServico_Servico_ispublico_Z, false);
            AddObjectProperty("Servico_LinNegCod_Z", gxTv_SdtServico_Servico_linnegcod_Z, false);
            AddObjectProperty("Servico_LinNegDsc_Z", gxTv_SdtServico_Servico_linnegdsc_Z, false);
            AddObjectProperty("Servico_IsOrigemReferencia_Z", gxTv_SdtServico_Servico_isorigemreferencia_Z, false);
            AddObjectProperty("Servico_Identificacao_Z", gxTv_SdtServico_Servico_identificacao_Z, false);
            AddObjectProperty("Servico_PausaSLA_Z", gxTv_SdtServico_Servico_pausasla_Z, false);
            AddObjectProperty("Servico_Descricao_N", gxTv_SdtServico_Servico_descricao_N, false);
            AddObjectProperty("Servico_UO_N", gxTv_SdtServico_Servico_uo_N, false);
            AddObjectProperty("Servico_UORespExclusiva_N", gxTv_SdtServico_Servico_uorespexclusiva_N, false);
            AddObjectProperty("Servico_Vinculado_N", gxTv_SdtServico_Servico_vinculado_N, false);
            AddObjectProperty("Servico_VincDesc_N", gxTv_SdtServico_Servico_vincdesc_N, false);
            AddObjectProperty("Servico_VincSigla_N", gxTv_SdtServico_Servico_vincsigla_N, false);
            AddObjectProperty("Servico_Terceriza_N", gxTv_SdtServico_Servico_terceriza_N, false);
            AddObjectProperty("Servico_Tela_N", gxTv_SdtServico_Servico_tela_N, false);
            AddObjectProperty("Servico_Atende_N", gxTv_SdtServico_Servico_atende_N, false);
            AddObjectProperty("Servico_ObrigaValores_N", gxTv_SdtServico_Servico_obrigavalores_N, false);
            AddObjectProperty("Servico_TipoHierarquia_N", gxTv_SdtServico_Servico_tipohierarquia_N, false);
            AddObjectProperty("Servico_PercTmp_N", gxTv_SdtServico_Servico_perctmp_N, false);
            AddObjectProperty("Servico_PercPgm_N", gxTv_SdtServico_Servico_percpgm_N, false);
            AddObjectProperty("Servico_PercCnc_N", gxTv_SdtServico_Servico_perccnc_N, false);
            AddObjectProperty("Servico_Anterior_N", gxTv_SdtServico_Servico_anterior_N, false);
            AddObjectProperty("Servico_Posterior_N", gxTv_SdtServico_Servico_posterior_N, false);
            AddObjectProperty("Servico_LinNegCod_N", gxTv_SdtServico_Servico_linnegcod_N, false);
            AddObjectProperty("Servico_LinNegDsc_N", gxTv_SdtServico_Servico_linnegdsc_N, false);
         }
         return  ;
      }

      [  SoapElement( ElementName = "Servico_Codigo" )]
      [  XmlElement( ElementName = "Servico_Codigo"   )]
      public int gxTpr_Servico_codigo
      {
         get {
            return gxTv_SdtServico_Servico_codigo ;
         }

         set {
            if ( gxTv_SdtServico_Servico_codigo != value )
            {
               gxTv_SdtServico_Mode = "INS";
               this.gxTv_SdtServico_Servico_codigo_Z_SetNull( );
               this.gxTv_SdtServico_Servico_nome_Z_SetNull( );
               this.gxTv_SdtServico_Servico_sigla_Z_SetNull( );
               this.gxTv_SdtServico_Servicogrupo_codigo_Z_SetNull( );
               this.gxTv_SdtServico_Servicogrupo_descricao_Z_SetNull( );
               this.gxTv_SdtServico_Servico_uo_Z_SetNull( );
               this.gxTv_SdtServico_Servico_uorespexclusiva_Z_SetNull( );
               this.gxTv_SdtServico_Servico_vinculado_Z_SetNull( );
               this.gxTv_SdtServico_Servico_vinculados_Z_SetNull( );
               this.gxTv_SdtServico_Servico_vincsigla_Z_SetNull( );
               this.gxTv_SdtServico_Servico_terceriza_Z_SetNull( );
               this.gxTv_SdtServico_Servico_tela_Z_SetNull( );
               this.gxTv_SdtServico_Servico_atende_Z_SetNull( );
               this.gxTv_SdtServico_Servico_obrigavalores_Z_SetNull( );
               this.gxTv_SdtServico_Servico_objetocontrole_Z_SetNull( );
               this.gxTv_SdtServico_Servico_tipohierarquia_Z_SetNull( );
               this.gxTv_SdtServico_Servico_perctmp_Z_SetNull( );
               this.gxTv_SdtServico_Servico_percpgm_Z_SetNull( );
               this.gxTv_SdtServico_Servico_perccnc_Z_SetNull( );
               this.gxTv_SdtServico_Servico_anterior_Z_SetNull( );
               this.gxTv_SdtServico_Servico_posterior_Z_SetNull( );
               this.gxTv_SdtServico_Servico_responsavel_Z_SetNull( );
               this.gxTv_SdtServico_Servico_ativo_Z_SetNull( );
               this.gxTv_SdtServico_Servico_ispublico_Z_SetNull( );
               this.gxTv_SdtServico_Servico_linnegcod_Z_SetNull( );
               this.gxTv_SdtServico_Servico_linnegdsc_Z_SetNull( );
               this.gxTv_SdtServico_Servico_isorigemreferencia_Z_SetNull( );
               this.gxTv_SdtServico_Servico_identificacao_Z_SetNull( );
               this.gxTv_SdtServico_Servico_pausasla_Z_SetNull( );
            }
            gxTv_SdtServico_Servico_codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Servico_Nome" )]
      [  XmlElement( ElementName = "Servico_Nome"   )]
      public String gxTpr_Servico_nome
      {
         get {
            return gxTv_SdtServico_Servico_nome ;
         }

         set {
            gxTv_SdtServico_Servico_nome = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Servico_Descricao" )]
      [  XmlElement( ElementName = "Servico_Descricao"   )]
      public String gxTpr_Servico_descricao
      {
         get {
            return gxTv_SdtServico_Servico_descricao ;
         }

         set {
            gxTv_SdtServico_Servico_descricao_N = 0;
            gxTv_SdtServico_Servico_descricao = (String)(value);
         }

      }

      public void gxTv_SdtServico_Servico_descricao_SetNull( )
      {
         gxTv_SdtServico_Servico_descricao_N = 1;
         gxTv_SdtServico_Servico_descricao = "";
         return  ;
      }

      public bool gxTv_SdtServico_Servico_descricao_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Servico_Sigla" )]
      [  XmlElement( ElementName = "Servico_Sigla"   )]
      public String gxTpr_Servico_sigla
      {
         get {
            return gxTv_SdtServico_Servico_sigla ;
         }

         set {
            gxTv_SdtServico_Servico_sigla = (String)(value);
         }

      }

      [  SoapElement( ElementName = "ServicoGrupo_Codigo" )]
      [  XmlElement( ElementName = "ServicoGrupo_Codigo"   )]
      public int gxTpr_Servicogrupo_codigo
      {
         get {
            return gxTv_SdtServico_Servicogrupo_codigo ;
         }

         set {
            gxTv_SdtServico_Servicogrupo_codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ServicoGrupo_Descricao" )]
      [  XmlElement( ElementName = "ServicoGrupo_Descricao"   )]
      public String gxTpr_Servicogrupo_descricao
      {
         get {
            return gxTv_SdtServico_Servicogrupo_descricao ;
         }

         set {
            gxTv_SdtServico_Servicogrupo_descricao = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Servico_UO" )]
      [  XmlElement( ElementName = "Servico_UO"   )]
      public int gxTpr_Servico_uo
      {
         get {
            return gxTv_SdtServico_Servico_uo ;
         }

         set {
            gxTv_SdtServico_Servico_uo_N = 0;
            gxTv_SdtServico_Servico_uo = (int)(value);
         }

      }

      public void gxTv_SdtServico_Servico_uo_SetNull( )
      {
         gxTv_SdtServico_Servico_uo_N = 1;
         gxTv_SdtServico_Servico_uo = 0;
         return  ;
      }

      public bool gxTv_SdtServico_Servico_uo_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Servico_UORespExclusiva" )]
      [  XmlElement( ElementName = "Servico_UORespExclusiva"   )]
      public bool gxTpr_Servico_uorespexclusiva
      {
         get {
            return gxTv_SdtServico_Servico_uorespexclusiva ;
         }

         set {
            gxTv_SdtServico_Servico_uorespexclusiva_N = 0;
            gxTv_SdtServico_Servico_uorespexclusiva = value;
         }

      }

      public void gxTv_SdtServico_Servico_uorespexclusiva_SetNull( )
      {
         gxTv_SdtServico_Servico_uorespexclusiva_N = 1;
         gxTv_SdtServico_Servico_uorespexclusiva = false;
         return  ;
      }

      public bool gxTv_SdtServico_Servico_uorespexclusiva_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Servico_Vinculado" )]
      [  XmlElement( ElementName = "Servico_Vinculado"   )]
      public int gxTpr_Servico_vinculado
      {
         get {
            return gxTv_SdtServico_Servico_vinculado ;
         }

         set {
            gxTv_SdtServico_Servico_vinculado_N = 0;
            gxTv_SdtServico_Servico_vinculado = (int)(value);
         }

      }

      public void gxTv_SdtServico_Servico_vinculado_SetNull( )
      {
         gxTv_SdtServico_Servico_vinculado_N = 1;
         gxTv_SdtServico_Servico_vinculado = 0;
         return  ;
      }

      public bool gxTv_SdtServico_Servico_vinculado_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Servico_Vinculados" )]
      [  XmlElement( ElementName = "Servico_Vinculados"   )]
      public short gxTpr_Servico_vinculados
      {
         get {
            return gxTv_SdtServico_Servico_vinculados ;
         }

         set {
            gxTv_SdtServico_Servico_vinculados = (short)(value);
         }

      }

      public void gxTv_SdtServico_Servico_vinculados_SetNull( )
      {
         gxTv_SdtServico_Servico_vinculados = 0;
         return  ;
      }

      public bool gxTv_SdtServico_Servico_vinculados_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Servico_VincDesc" )]
      [  XmlElement( ElementName = "Servico_VincDesc"   )]
      public String gxTpr_Servico_vincdesc
      {
         get {
            return gxTv_SdtServico_Servico_vincdesc ;
         }

         set {
            gxTv_SdtServico_Servico_vincdesc_N = 0;
            gxTv_SdtServico_Servico_vincdesc = (String)(value);
         }

      }

      public void gxTv_SdtServico_Servico_vincdesc_SetNull( )
      {
         gxTv_SdtServico_Servico_vincdesc_N = 1;
         gxTv_SdtServico_Servico_vincdesc = "";
         return  ;
      }

      public bool gxTv_SdtServico_Servico_vincdesc_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Servico_VincSigla" )]
      [  XmlElement( ElementName = "Servico_VincSigla"   )]
      public String gxTpr_Servico_vincsigla
      {
         get {
            return gxTv_SdtServico_Servico_vincsigla ;
         }

         set {
            gxTv_SdtServico_Servico_vincsigla_N = 0;
            gxTv_SdtServico_Servico_vincsigla = (String)(value);
         }

      }

      public void gxTv_SdtServico_Servico_vincsigla_SetNull( )
      {
         gxTv_SdtServico_Servico_vincsigla_N = 1;
         gxTv_SdtServico_Servico_vincsigla = "";
         return  ;
      }

      public bool gxTv_SdtServico_Servico_vincsigla_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Servico_Terceriza" )]
      [  XmlElement( ElementName = "Servico_Terceriza"   )]
      public bool gxTpr_Servico_terceriza
      {
         get {
            return gxTv_SdtServico_Servico_terceriza ;
         }

         set {
            gxTv_SdtServico_Servico_terceriza_N = 0;
            gxTv_SdtServico_Servico_terceriza = value;
         }

      }

      public void gxTv_SdtServico_Servico_terceriza_SetNull( )
      {
         gxTv_SdtServico_Servico_terceriza_N = 1;
         gxTv_SdtServico_Servico_terceriza = false;
         return  ;
      }

      public bool gxTv_SdtServico_Servico_terceriza_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Servico_Tela" )]
      [  XmlElement( ElementName = "Servico_Tela"   )]
      public String gxTpr_Servico_tela
      {
         get {
            return gxTv_SdtServico_Servico_tela ;
         }

         set {
            gxTv_SdtServico_Servico_tela_N = 0;
            gxTv_SdtServico_Servico_tela = (String)(value);
         }

      }

      public void gxTv_SdtServico_Servico_tela_SetNull( )
      {
         gxTv_SdtServico_Servico_tela_N = 1;
         gxTv_SdtServico_Servico_tela = "";
         return  ;
      }

      public bool gxTv_SdtServico_Servico_tela_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Servico_Atende" )]
      [  XmlElement( ElementName = "Servico_Atende"   )]
      public String gxTpr_Servico_atende
      {
         get {
            return gxTv_SdtServico_Servico_atende ;
         }

         set {
            gxTv_SdtServico_Servico_atende_N = 0;
            gxTv_SdtServico_Servico_atende = (String)(value);
         }

      }

      public void gxTv_SdtServico_Servico_atende_SetNull( )
      {
         gxTv_SdtServico_Servico_atende_N = 1;
         gxTv_SdtServico_Servico_atende = "";
         return  ;
      }

      public bool gxTv_SdtServico_Servico_atende_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Servico_ObrigaValores" )]
      [  XmlElement( ElementName = "Servico_ObrigaValores"   )]
      public String gxTpr_Servico_obrigavalores
      {
         get {
            return gxTv_SdtServico_Servico_obrigavalores ;
         }

         set {
            gxTv_SdtServico_Servico_obrigavalores_N = 0;
            gxTv_SdtServico_Servico_obrigavalores = (String)(value);
         }

      }

      public void gxTv_SdtServico_Servico_obrigavalores_SetNull( )
      {
         gxTv_SdtServico_Servico_obrigavalores_N = 1;
         gxTv_SdtServico_Servico_obrigavalores = "";
         return  ;
      }

      public bool gxTv_SdtServico_Servico_obrigavalores_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Servico_ObjetoControle" )]
      [  XmlElement( ElementName = "Servico_ObjetoControle"   )]
      public String gxTpr_Servico_objetocontrole
      {
         get {
            return gxTv_SdtServico_Servico_objetocontrole ;
         }

         set {
            gxTv_SdtServico_Servico_objetocontrole = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Servico_TipoHierarquia" )]
      [  XmlElement( ElementName = "Servico_TipoHierarquia"   )]
      public short gxTpr_Servico_tipohierarquia
      {
         get {
            return gxTv_SdtServico_Servico_tipohierarquia ;
         }

         set {
            gxTv_SdtServico_Servico_tipohierarquia_N = 0;
            gxTv_SdtServico_Servico_tipohierarquia = (short)(value);
         }

      }

      public void gxTv_SdtServico_Servico_tipohierarquia_SetNull( )
      {
         gxTv_SdtServico_Servico_tipohierarquia_N = 1;
         gxTv_SdtServico_Servico_tipohierarquia = 0;
         return  ;
      }

      public bool gxTv_SdtServico_Servico_tipohierarquia_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Servico_PercTmp" )]
      [  XmlElement( ElementName = "Servico_PercTmp"   )]
      public short gxTpr_Servico_perctmp
      {
         get {
            return gxTv_SdtServico_Servico_perctmp ;
         }

         set {
            gxTv_SdtServico_Servico_perctmp_N = 0;
            gxTv_SdtServico_Servico_perctmp = (short)(value);
         }

      }

      public void gxTv_SdtServico_Servico_perctmp_SetNull( )
      {
         gxTv_SdtServico_Servico_perctmp_N = 1;
         gxTv_SdtServico_Servico_perctmp = 0;
         return  ;
      }

      public bool gxTv_SdtServico_Servico_perctmp_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Servico_PercPgm" )]
      [  XmlElement( ElementName = "Servico_PercPgm"   )]
      public short gxTpr_Servico_percpgm
      {
         get {
            return gxTv_SdtServico_Servico_percpgm ;
         }

         set {
            gxTv_SdtServico_Servico_percpgm_N = 0;
            gxTv_SdtServico_Servico_percpgm = (short)(value);
         }

      }

      public void gxTv_SdtServico_Servico_percpgm_SetNull( )
      {
         gxTv_SdtServico_Servico_percpgm_N = 1;
         gxTv_SdtServico_Servico_percpgm = 0;
         return  ;
      }

      public bool gxTv_SdtServico_Servico_percpgm_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Servico_PercCnc" )]
      [  XmlElement( ElementName = "Servico_PercCnc"   )]
      public short gxTpr_Servico_perccnc
      {
         get {
            return gxTv_SdtServico_Servico_perccnc ;
         }

         set {
            gxTv_SdtServico_Servico_perccnc_N = 0;
            gxTv_SdtServico_Servico_perccnc = (short)(value);
         }

      }

      public void gxTv_SdtServico_Servico_perccnc_SetNull( )
      {
         gxTv_SdtServico_Servico_perccnc_N = 1;
         gxTv_SdtServico_Servico_perccnc = 0;
         return  ;
      }

      public bool gxTv_SdtServico_Servico_perccnc_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Servico_Anterior" )]
      [  XmlElement( ElementName = "Servico_Anterior"   )]
      public int gxTpr_Servico_anterior
      {
         get {
            return gxTv_SdtServico_Servico_anterior ;
         }

         set {
            gxTv_SdtServico_Servico_anterior_N = 0;
            gxTv_SdtServico_Servico_anterior = (int)(value);
         }

      }

      public void gxTv_SdtServico_Servico_anterior_SetNull( )
      {
         gxTv_SdtServico_Servico_anterior_N = 1;
         gxTv_SdtServico_Servico_anterior = 0;
         return  ;
      }

      public bool gxTv_SdtServico_Servico_anterior_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Servico_Posterior" )]
      [  XmlElement( ElementName = "Servico_Posterior"   )]
      public int gxTpr_Servico_posterior
      {
         get {
            return gxTv_SdtServico_Servico_posterior ;
         }

         set {
            gxTv_SdtServico_Servico_posterior_N = 0;
            gxTv_SdtServico_Servico_posterior = (int)(value);
         }

      }

      public void gxTv_SdtServico_Servico_posterior_SetNull( )
      {
         gxTv_SdtServico_Servico_posterior_N = 1;
         gxTv_SdtServico_Servico_posterior = 0;
         return  ;
      }

      public bool gxTv_SdtServico_Servico_posterior_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Servico_Responsavel" )]
      [  XmlElement( ElementName = "Servico_Responsavel"   )]
      public int gxTpr_Servico_responsavel
      {
         get {
            return gxTv_SdtServico_Servico_responsavel ;
         }

         set {
            gxTv_SdtServico_Servico_responsavel = (int)(value);
         }

      }

      public void gxTv_SdtServico_Servico_responsavel_SetNull( )
      {
         gxTv_SdtServico_Servico_responsavel = 0;
         return  ;
      }

      public bool gxTv_SdtServico_Servico_responsavel_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Servico_Ativo" )]
      [  XmlElement( ElementName = "Servico_Ativo"   )]
      public bool gxTpr_Servico_ativo
      {
         get {
            return gxTv_SdtServico_Servico_ativo ;
         }

         set {
            gxTv_SdtServico_Servico_ativo = value;
         }

      }

      [  SoapElement( ElementName = "Servico_IsPublico" )]
      [  XmlElement( ElementName = "Servico_IsPublico"   )]
      public bool gxTpr_Servico_ispublico
      {
         get {
            return gxTv_SdtServico_Servico_ispublico ;
         }

         set {
            gxTv_SdtServico_Servico_ispublico = value;
         }

      }

      [  SoapElement( ElementName = "Servico_LinNegCod" )]
      [  XmlElement( ElementName = "Servico_LinNegCod"   )]
      public int gxTpr_Servico_linnegcod
      {
         get {
            return gxTv_SdtServico_Servico_linnegcod ;
         }

         set {
            gxTv_SdtServico_Servico_linnegcod_N = 0;
            gxTv_SdtServico_Servico_linnegcod = (int)(value);
         }

      }

      public void gxTv_SdtServico_Servico_linnegcod_SetNull( )
      {
         gxTv_SdtServico_Servico_linnegcod_N = 1;
         gxTv_SdtServico_Servico_linnegcod = 0;
         return  ;
      }

      public bool gxTv_SdtServico_Servico_linnegcod_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Servico_LinNegDsc" )]
      [  XmlElement( ElementName = "Servico_LinNegDsc"   )]
      public String gxTpr_Servico_linnegdsc
      {
         get {
            return gxTv_SdtServico_Servico_linnegdsc ;
         }

         set {
            gxTv_SdtServico_Servico_linnegdsc_N = 0;
            gxTv_SdtServico_Servico_linnegdsc = (String)(value);
         }

      }

      public void gxTv_SdtServico_Servico_linnegdsc_SetNull( )
      {
         gxTv_SdtServico_Servico_linnegdsc_N = 1;
         gxTv_SdtServico_Servico_linnegdsc = "";
         return  ;
      }

      public bool gxTv_SdtServico_Servico_linnegdsc_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Servico_IsOrigemReferencia" )]
      [  XmlElement( ElementName = "Servico_IsOrigemReferencia"   )]
      public bool gxTpr_Servico_isorigemreferencia
      {
         get {
            return gxTv_SdtServico_Servico_isorigemreferencia ;
         }

         set {
            gxTv_SdtServico_Servico_isorigemreferencia = value;
         }

      }

      [  SoapElement( ElementName = "Servico_Identificacao" )]
      [  XmlElement( ElementName = "Servico_Identificacao"   )]
      public String gxTpr_Servico_identificacao
      {
         get {
            return gxTv_SdtServico_Servico_identificacao ;
         }

         set {
            gxTv_SdtServico_Servico_identificacao = (String)(value);
         }

      }

      public void gxTv_SdtServico_Servico_identificacao_SetNull( )
      {
         gxTv_SdtServico_Servico_identificacao = "";
         return  ;
      }

      public bool gxTv_SdtServico_Servico_identificacao_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Servico_PausaSLA" )]
      [  XmlElement( ElementName = "Servico_PausaSLA"   )]
      public bool gxTpr_Servico_pausasla
      {
         get {
            return gxTv_SdtServico_Servico_pausasla ;
         }

         set {
            gxTv_SdtServico_Servico_pausasla = value;
         }

      }

      [  SoapElement( ElementName = "Mode" )]
      [  XmlElement( ElementName = "Mode"   )]
      public String gxTpr_Mode
      {
         get {
            return gxTv_SdtServico_Mode ;
         }

         set {
            gxTv_SdtServico_Mode = (String)(value);
         }

      }

      public void gxTv_SdtServico_Mode_SetNull( )
      {
         gxTv_SdtServico_Mode = "";
         return  ;
      }

      public bool gxTv_SdtServico_Mode_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Initialized" )]
      [  XmlElement( ElementName = "Initialized"   )]
      public short gxTpr_Initialized
      {
         get {
            return gxTv_SdtServico_Initialized ;
         }

         set {
            gxTv_SdtServico_Initialized = (short)(value);
         }

      }

      public void gxTv_SdtServico_Initialized_SetNull( )
      {
         gxTv_SdtServico_Initialized = 0;
         return  ;
      }

      public bool gxTv_SdtServico_Initialized_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Servico_Codigo_Z" )]
      [  XmlElement( ElementName = "Servico_Codigo_Z"   )]
      public int gxTpr_Servico_codigo_Z
      {
         get {
            return gxTv_SdtServico_Servico_codigo_Z ;
         }

         set {
            gxTv_SdtServico_Servico_codigo_Z = (int)(value);
         }

      }

      public void gxTv_SdtServico_Servico_codigo_Z_SetNull( )
      {
         gxTv_SdtServico_Servico_codigo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtServico_Servico_codigo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Servico_Nome_Z" )]
      [  XmlElement( ElementName = "Servico_Nome_Z"   )]
      public String gxTpr_Servico_nome_Z
      {
         get {
            return gxTv_SdtServico_Servico_nome_Z ;
         }

         set {
            gxTv_SdtServico_Servico_nome_Z = (String)(value);
         }

      }

      public void gxTv_SdtServico_Servico_nome_Z_SetNull( )
      {
         gxTv_SdtServico_Servico_nome_Z = "";
         return  ;
      }

      public bool gxTv_SdtServico_Servico_nome_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Servico_Sigla_Z" )]
      [  XmlElement( ElementName = "Servico_Sigla_Z"   )]
      public String gxTpr_Servico_sigla_Z
      {
         get {
            return gxTv_SdtServico_Servico_sigla_Z ;
         }

         set {
            gxTv_SdtServico_Servico_sigla_Z = (String)(value);
         }

      }

      public void gxTv_SdtServico_Servico_sigla_Z_SetNull( )
      {
         gxTv_SdtServico_Servico_sigla_Z = "";
         return  ;
      }

      public bool gxTv_SdtServico_Servico_sigla_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ServicoGrupo_Codigo_Z" )]
      [  XmlElement( ElementName = "ServicoGrupo_Codigo_Z"   )]
      public int gxTpr_Servicogrupo_codigo_Z
      {
         get {
            return gxTv_SdtServico_Servicogrupo_codigo_Z ;
         }

         set {
            gxTv_SdtServico_Servicogrupo_codigo_Z = (int)(value);
         }

      }

      public void gxTv_SdtServico_Servicogrupo_codigo_Z_SetNull( )
      {
         gxTv_SdtServico_Servicogrupo_codigo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtServico_Servicogrupo_codigo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ServicoGrupo_Descricao_Z" )]
      [  XmlElement( ElementName = "ServicoGrupo_Descricao_Z"   )]
      public String gxTpr_Servicogrupo_descricao_Z
      {
         get {
            return gxTv_SdtServico_Servicogrupo_descricao_Z ;
         }

         set {
            gxTv_SdtServico_Servicogrupo_descricao_Z = (String)(value);
         }

      }

      public void gxTv_SdtServico_Servicogrupo_descricao_Z_SetNull( )
      {
         gxTv_SdtServico_Servicogrupo_descricao_Z = "";
         return  ;
      }

      public bool gxTv_SdtServico_Servicogrupo_descricao_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Servico_UO_Z" )]
      [  XmlElement( ElementName = "Servico_UO_Z"   )]
      public int gxTpr_Servico_uo_Z
      {
         get {
            return gxTv_SdtServico_Servico_uo_Z ;
         }

         set {
            gxTv_SdtServico_Servico_uo_Z = (int)(value);
         }

      }

      public void gxTv_SdtServico_Servico_uo_Z_SetNull( )
      {
         gxTv_SdtServico_Servico_uo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtServico_Servico_uo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Servico_UORespExclusiva_Z" )]
      [  XmlElement( ElementName = "Servico_UORespExclusiva_Z"   )]
      public bool gxTpr_Servico_uorespexclusiva_Z
      {
         get {
            return gxTv_SdtServico_Servico_uorespexclusiva_Z ;
         }

         set {
            gxTv_SdtServico_Servico_uorespexclusiva_Z = value;
         }

      }

      public void gxTv_SdtServico_Servico_uorespexclusiva_Z_SetNull( )
      {
         gxTv_SdtServico_Servico_uorespexclusiva_Z = false;
         return  ;
      }

      public bool gxTv_SdtServico_Servico_uorespexclusiva_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Servico_Vinculado_Z" )]
      [  XmlElement( ElementName = "Servico_Vinculado_Z"   )]
      public int gxTpr_Servico_vinculado_Z
      {
         get {
            return gxTv_SdtServico_Servico_vinculado_Z ;
         }

         set {
            gxTv_SdtServico_Servico_vinculado_Z = (int)(value);
         }

      }

      public void gxTv_SdtServico_Servico_vinculado_Z_SetNull( )
      {
         gxTv_SdtServico_Servico_vinculado_Z = 0;
         return  ;
      }

      public bool gxTv_SdtServico_Servico_vinculado_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Servico_Vinculados_Z" )]
      [  XmlElement( ElementName = "Servico_Vinculados_Z"   )]
      public short gxTpr_Servico_vinculados_Z
      {
         get {
            return gxTv_SdtServico_Servico_vinculados_Z ;
         }

         set {
            gxTv_SdtServico_Servico_vinculados_Z = (short)(value);
         }

      }

      public void gxTv_SdtServico_Servico_vinculados_Z_SetNull( )
      {
         gxTv_SdtServico_Servico_vinculados_Z = 0;
         return  ;
      }

      public bool gxTv_SdtServico_Servico_vinculados_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Servico_VincSigla_Z" )]
      [  XmlElement( ElementName = "Servico_VincSigla_Z"   )]
      public String gxTpr_Servico_vincsigla_Z
      {
         get {
            return gxTv_SdtServico_Servico_vincsigla_Z ;
         }

         set {
            gxTv_SdtServico_Servico_vincsigla_Z = (String)(value);
         }

      }

      public void gxTv_SdtServico_Servico_vincsigla_Z_SetNull( )
      {
         gxTv_SdtServico_Servico_vincsigla_Z = "";
         return  ;
      }

      public bool gxTv_SdtServico_Servico_vincsigla_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Servico_Terceriza_Z" )]
      [  XmlElement( ElementName = "Servico_Terceriza_Z"   )]
      public bool gxTpr_Servico_terceriza_Z
      {
         get {
            return gxTv_SdtServico_Servico_terceriza_Z ;
         }

         set {
            gxTv_SdtServico_Servico_terceriza_Z = value;
         }

      }

      public void gxTv_SdtServico_Servico_terceriza_Z_SetNull( )
      {
         gxTv_SdtServico_Servico_terceriza_Z = false;
         return  ;
      }

      public bool gxTv_SdtServico_Servico_terceriza_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Servico_Tela_Z" )]
      [  XmlElement( ElementName = "Servico_Tela_Z"   )]
      public String gxTpr_Servico_tela_Z
      {
         get {
            return gxTv_SdtServico_Servico_tela_Z ;
         }

         set {
            gxTv_SdtServico_Servico_tela_Z = (String)(value);
         }

      }

      public void gxTv_SdtServico_Servico_tela_Z_SetNull( )
      {
         gxTv_SdtServico_Servico_tela_Z = "";
         return  ;
      }

      public bool gxTv_SdtServico_Servico_tela_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Servico_Atende_Z" )]
      [  XmlElement( ElementName = "Servico_Atende_Z"   )]
      public String gxTpr_Servico_atende_Z
      {
         get {
            return gxTv_SdtServico_Servico_atende_Z ;
         }

         set {
            gxTv_SdtServico_Servico_atende_Z = (String)(value);
         }

      }

      public void gxTv_SdtServico_Servico_atende_Z_SetNull( )
      {
         gxTv_SdtServico_Servico_atende_Z = "";
         return  ;
      }

      public bool gxTv_SdtServico_Servico_atende_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Servico_ObrigaValores_Z" )]
      [  XmlElement( ElementName = "Servico_ObrigaValores_Z"   )]
      public String gxTpr_Servico_obrigavalores_Z
      {
         get {
            return gxTv_SdtServico_Servico_obrigavalores_Z ;
         }

         set {
            gxTv_SdtServico_Servico_obrigavalores_Z = (String)(value);
         }

      }

      public void gxTv_SdtServico_Servico_obrigavalores_Z_SetNull( )
      {
         gxTv_SdtServico_Servico_obrigavalores_Z = "";
         return  ;
      }

      public bool gxTv_SdtServico_Servico_obrigavalores_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Servico_ObjetoControle_Z" )]
      [  XmlElement( ElementName = "Servico_ObjetoControle_Z"   )]
      public String gxTpr_Servico_objetocontrole_Z
      {
         get {
            return gxTv_SdtServico_Servico_objetocontrole_Z ;
         }

         set {
            gxTv_SdtServico_Servico_objetocontrole_Z = (String)(value);
         }

      }

      public void gxTv_SdtServico_Servico_objetocontrole_Z_SetNull( )
      {
         gxTv_SdtServico_Servico_objetocontrole_Z = "";
         return  ;
      }

      public bool gxTv_SdtServico_Servico_objetocontrole_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Servico_TipoHierarquia_Z" )]
      [  XmlElement( ElementName = "Servico_TipoHierarquia_Z"   )]
      public short gxTpr_Servico_tipohierarquia_Z
      {
         get {
            return gxTv_SdtServico_Servico_tipohierarquia_Z ;
         }

         set {
            gxTv_SdtServico_Servico_tipohierarquia_Z = (short)(value);
         }

      }

      public void gxTv_SdtServico_Servico_tipohierarquia_Z_SetNull( )
      {
         gxTv_SdtServico_Servico_tipohierarquia_Z = 0;
         return  ;
      }

      public bool gxTv_SdtServico_Servico_tipohierarquia_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Servico_PercTmp_Z" )]
      [  XmlElement( ElementName = "Servico_PercTmp_Z"   )]
      public short gxTpr_Servico_perctmp_Z
      {
         get {
            return gxTv_SdtServico_Servico_perctmp_Z ;
         }

         set {
            gxTv_SdtServico_Servico_perctmp_Z = (short)(value);
         }

      }

      public void gxTv_SdtServico_Servico_perctmp_Z_SetNull( )
      {
         gxTv_SdtServico_Servico_perctmp_Z = 0;
         return  ;
      }

      public bool gxTv_SdtServico_Servico_perctmp_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Servico_PercPgm_Z" )]
      [  XmlElement( ElementName = "Servico_PercPgm_Z"   )]
      public short gxTpr_Servico_percpgm_Z
      {
         get {
            return gxTv_SdtServico_Servico_percpgm_Z ;
         }

         set {
            gxTv_SdtServico_Servico_percpgm_Z = (short)(value);
         }

      }

      public void gxTv_SdtServico_Servico_percpgm_Z_SetNull( )
      {
         gxTv_SdtServico_Servico_percpgm_Z = 0;
         return  ;
      }

      public bool gxTv_SdtServico_Servico_percpgm_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Servico_PercCnc_Z" )]
      [  XmlElement( ElementName = "Servico_PercCnc_Z"   )]
      public short gxTpr_Servico_perccnc_Z
      {
         get {
            return gxTv_SdtServico_Servico_perccnc_Z ;
         }

         set {
            gxTv_SdtServico_Servico_perccnc_Z = (short)(value);
         }

      }

      public void gxTv_SdtServico_Servico_perccnc_Z_SetNull( )
      {
         gxTv_SdtServico_Servico_perccnc_Z = 0;
         return  ;
      }

      public bool gxTv_SdtServico_Servico_perccnc_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Servico_Anterior_Z" )]
      [  XmlElement( ElementName = "Servico_Anterior_Z"   )]
      public int gxTpr_Servico_anterior_Z
      {
         get {
            return gxTv_SdtServico_Servico_anterior_Z ;
         }

         set {
            gxTv_SdtServico_Servico_anterior_Z = (int)(value);
         }

      }

      public void gxTv_SdtServico_Servico_anterior_Z_SetNull( )
      {
         gxTv_SdtServico_Servico_anterior_Z = 0;
         return  ;
      }

      public bool gxTv_SdtServico_Servico_anterior_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Servico_Posterior_Z" )]
      [  XmlElement( ElementName = "Servico_Posterior_Z"   )]
      public int gxTpr_Servico_posterior_Z
      {
         get {
            return gxTv_SdtServico_Servico_posterior_Z ;
         }

         set {
            gxTv_SdtServico_Servico_posterior_Z = (int)(value);
         }

      }

      public void gxTv_SdtServico_Servico_posterior_Z_SetNull( )
      {
         gxTv_SdtServico_Servico_posterior_Z = 0;
         return  ;
      }

      public bool gxTv_SdtServico_Servico_posterior_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Servico_Responsavel_Z" )]
      [  XmlElement( ElementName = "Servico_Responsavel_Z"   )]
      public int gxTpr_Servico_responsavel_Z
      {
         get {
            return gxTv_SdtServico_Servico_responsavel_Z ;
         }

         set {
            gxTv_SdtServico_Servico_responsavel_Z = (int)(value);
         }

      }

      public void gxTv_SdtServico_Servico_responsavel_Z_SetNull( )
      {
         gxTv_SdtServico_Servico_responsavel_Z = 0;
         return  ;
      }

      public bool gxTv_SdtServico_Servico_responsavel_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Servico_Ativo_Z" )]
      [  XmlElement( ElementName = "Servico_Ativo_Z"   )]
      public bool gxTpr_Servico_ativo_Z
      {
         get {
            return gxTv_SdtServico_Servico_ativo_Z ;
         }

         set {
            gxTv_SdtServico_Servico_ativo_Z = value;
         }

      }

      public void gxTv_SdtServico_Servico_ativo_Z_SetNull( )
      {
         gxTv_SdtServico_Servico_ativo_Z = false;
         return  ;
      }

      public bool gxTv_SdtServico_Servico_ativo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Servico_IsPublico_Z" )]
      [  XmlElement( ElementName = "Servico_IsPublico_Z"   )]
      public bool gxTpr_Servico_ispublico_Z
      {
         get {
            return gxTv_SdtServico_Servico_ispublico_Z ;
         }

         set {
            gxTv_SdtServico_Servico_ispublico_Z = value;
         }

      }

      public void gxTv_SdtServico_Servico_ispublico_Z_SetNull( )
      {
         gxTv_SdtServico_Servico_ispublico_Z = false;
         return  ;
      }

      public bool gxTv_SdtServico_Servico_ispublico_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Servico_LinNegCod_Z" )]
      [  XmlElement( ElementName = "Servico_LinNegCod_Z"   )]
      public int gxTpr_Servico_linnegcod_Z
      {
         get {
            return gxTv_SdtServico_Servico_linnegcod_Z ;
         }

         set {
            gxTv_SdtServico_Servico_linnegcod_Z = (int)(value);
         }

      }

      public void gxTv_SdtServico_Servico_linnegcod_Z_SetNull( )
      {
         gxTv_SdtServico_Servico_linnegcod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtServico_Servico_linnegcod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Servico_LinNegDsc_Z" )]
      [  XmlElement( ElementName = "Servico_LinNegDsc_Z"   )]
      public String gxTpr_Servico_linnegdsc_Z
      {
         get {
            return gxTv_SdtServico_Servico_linnegdsc_Z ;
         }

         set {
            gxTv_SdtServico_Servico_linnegdsc_Z = (String)(value);
         }

      }

      public void gxTv_SdtServico_Servico_linnegdsc_Z_SetNull( )
      {
         gxTv_SdtServico_Servico_linnegdsc_Z = "";
         return  ;
      }

      public bool gxTv_SdtServico_Servico_linnegdsc_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Servico_IsOrigemReferencia_Z" )]
      [  XmlElement( ElementName = "Servico_IsOrigemReferencia_Z"   )]
      public bool gxTpr_Servico_isorigemreferencia_Z
      {
         get {
            return gxTv_SdtServico_Servico_isorigemreferencia_Z ;
         }

         set {
            gxTv_SdtServico_Servico_isorigemreferencia_Z = value;
         }

      }

      public void gxTv_SdtServico_Servico_isorigemreferencia_Z_SetNull( )
      {
         gxTv_SdtServico_Servico_isorigemreferencia_Z = false;
         return  ;
      }

      public bool gxTv_SdtServico_Servico_isorigemreferencia_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Servico_Identificacao_Z" )]
      [  XmlElement( ElementName = "Servico_Identificacao_Z"   )]
      public String gxTpr_Servico_identificacao_Z
      {
         get {
            return gxTv_SdtServico_Servico_identificacao_Z ;
         }

         set {
            gxTv_SdtServico_Servico_identificacao_Z = (String)(value);
         }

      }

      public void gxTv_SdtServico_Servico_identificacao_Z_SetNull( )
      {
         gxTv_SdtServico_Servico_identificacao_Z = "";
         return  ;
      }

      public bool gxTv_SdtServico_Servico_identificacao_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Servico_PausaSLA_Z" )]
      [  XmlElement( ElementName = "Servico_PausaSLA_Z"   )]
      public bool gxTpr_Servico_pausasla_Z
      {
         get {
            return gxTv_SdtServico_Servico_pausasla_Z ;
         }

         set {
            gxTv_SdtServico_Servico_pausasla_Z = value;
         }

      }

      public void gxTv_SdtServico_Servico_pausasla_Z_SetNull( )
      {
         gxTv_SdtServico_Servico_pausasla_Z = false;
         return  ;
      }

      public bool gxTv_SdtServico_Servico_pausasla_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Servico_Descricao_N" )]
      [  XmlElement( ElementName = "Servico_Descricao_N"   )]
      public short gxTpr_Servico_descricao_N
      {
         get {
            return gxTv_SdtServico_Servico_descricao_N ;
         }

         set {
            gxTv_SdtServico_Servico_descricao_N = (short)(value);
         }

      }

      public void gxTv_SdtServico_Servico_descricao_N_SetNull( )
      {
         gxTv_SdtServico_Servico_descricao_N = 0;
         return  ;
      }

      public bool gxTv_SdtServico_Servico_descricao_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Servico_UO_N" )]
      [  XmlElement( ElementName = "Servico_UO_N"   )]
      public short gxTpr_Servico_uo_N
      {
         get {
            return gxTv_SdtServico_Servico_uo_N ;
         }

         set {
            gxTv_SdtServico_Servico_uo_N = (short)(value);
         }

      }

      public void gxTv_SdtServico_Servico_uo_N_SetNull( )
      {
         gxTv_SdtServico_Servico_uo_N = 0;
         return  ;
      }

      public bool gxTv_SdtServico_Servico_uo_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Servico_UORespExclusiva_N" )]
      [  XmlElement( ElementName = "Servico_UORespExclusiva_N"   )]
      public short gxTpr_Servico_uorespexclusiva_N
      {
         get {
            return gxTv_SdtServico_Servico_uorespexclusiva_N ;
         }

         set {
            gxTv_SdtServico_Servico_uorespexclusiva_N = (short)(value);
         }

      }

      public void gxTv_SdtServico_Servico_uorespexclusiva_N_SetNull( )
      {
         gxTv_SdtServico_Servico_uorespexclusiva_N = 0;
         return  ;
      }

      public bool gxTv_SdtServico_Servico_uorespexclusiva_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Servico_Vinculado_N" )]
      [  XmlElement( ElementName = "Servico_Vinculado_N"   )]
      public short gxTpr_Servico_vinculado_N
      {
         get {
            return gxTv_SdtServico_Servico_vinculado_N ;
         }

         set {
            gxTv_SdtServico_Servico_vinculado_N = (short)(value);
         }

      }

      public void gxTv_SdtServico_Servico_vinculado_N_SetNull( )
      {
         gxTv_SdtServico_Servico_vinculado_N = 0;
         return  ;
      }

      public bool gxTv_SdtServico_Servico_vinculado_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Servico_VincDesc_N" )]
      [  XmlElement( ElementName = "Servico_VincDesc_N"   )]
      public short gxTpr_Servico_vincdesc_N
      {
         get {
            return gxTv_SdtServico_Servico_vincdesc_N ;
         }

         set {
            gxTv_SdtServico_Servico_vincdesc_N = (short)(value);
         }

      }

      public void gxTv_SdtServico_Servico_vincdesc_N_SetNull( )
      {
         gxTv_SdtServico_Servico_vincdesc_N = 0;
         return  ;
      }

      public bool gxTv_SdtServico_Servico_vincdesc_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Servico_VincSigla_N" )]
      [  XmlElement( ElementName = "Servico_VincSigla_N"   )]
      public short gxTpr_Servico_vincsigla_N
      {
         get {
            return gxTv_SdtServico_Servico_vincsigla_N ;
         }

         set {
            gxTv_SdtServico_Servico_vincsigla_N = (short)(value);
         }

      }

      public void gxTv_SdtServico_Servico_vincsigla_N_SetNull( )
      {
         gxTv_SdtServico_Servico_vincsigla_N = 0;
         return  ;
      }

      public bool gxTv_SdtServico_Servico_vincsigla_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Servico_Terceriza_N" )]
      [  XmlElement( ElementName = "Servico_Terceriza_N"   )]
      public short gxTpr_Servico_terceriza_N
      {
         get {
            return gxTv_SdtServico_Servico_terceriza_N ;
         }

         set {
            gxTv_SdtServico_Servico_terceriza_N = (short)(value);
         }

      }

      public void gxTv_SdtServico_Servico_terceriza_N_SetNull( )
      {
         gxTv_SdtServico_Servico_terceriza_N = 0;
         return  ;
      }

      public bool gxTv_SdtServico_Servico_terceriza_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Servico_Tela_N" )]
      [  XmlElement( ElementName = "Servico_Tela_N"   )]
      public short gxTpr_Servico_tela_N
      {
         get {
            return gxTv_SdtServico_Servico_tela_N ;
         }

         set {
            gxTv_SdtServico_Servico_tela_N = (short)(value);
         }

      }

      public void gxTv_SdtServico_Servico_tela_N_SetNull( )
      {
         gxTv_SdtServico_Servico_tela_N = 0;
         return  ;
      }

      public bool gxTv_SdtServico_Servico_tela_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Servico_Atende_N" )]
      [  XmlElement( ElementName = "Servico_Atende_N"   )]
      public short gxTpr_Servico_atende_N
      {
         get {
            return gxTv_SdtServico_Servico_atende_N ;
         }

         set {
            gxTv_SdtServico_Servico_atende_N = (short)(value);
         }

      }

      public void gxTv_SdtServico_Servico_atende_N_SetNull( )
      {
         gxTv_SdtServico_Servico_atende_N = 0;
         return  ;
      }

      public bool gxTv_SdtServico_Servico_atende_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Servico_ObrigaValores_N" )]
      [  XmlElement( ElementName = "Servico_ObrigaValores_N"   )]
      public short gxTpr_Servico_obrigavalores_N
      {
         get {
            return gxTv_SdtServico_Servico_obrigavalores_N ;
         }

         set {
            gxTv_SdtServico_Servico_obrigavalores_N = (short)(value);
         }

      }

      public void gxTv_SdtServico_Servico_obrigavalores_N_SetNull( )
      {
         gxTv_SdtServico_Servico_obrigavalores_N = 0;
         return  ;
      }

      public bool gxTv_SdtServico_Servico_obrigavalores_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Servico_TipoHierarquia_N" )]
      [  XmlElement( ElementName = "Servico_TipoHierarquia_N"   )]
      public short gxTpr_Servico_tipohierarquia_N
      {
         get {
            return gxTv_SdtServico_Servico_tipohierarquia_N ;
         }

         set {
            gxTv_SdtServico_Servico_tipohierarquia_N = (short)(value);
         }

      }

      public void gxTv_SdtServico_Servico_tipohierarquia_N_SetNull( )
      {
         gxTv_SdtServico_Servico_tipohierarquia_N = 0;
         return  ;
      }

      public bool gxTv_SdtServico_Servico_tipohierarquia_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Servico_PercTmp_N" )]
      [  XmlElement( ElementName = "Servico_PercTmp_N"   )]
      public short gxTpr_Servico_perctmp_N
      {
         get {
            return gxTv_SdtServico_Servico_perctmp_N ;
         }

         set {
            gxTv_SdtServico_Servico_perctmp_N = (short)(value);
         }

      }

      public void gxTv_SdtServico_Servico_perctmp_N_SetNull( )
      {
         gxTv_SdtServico_Servico_perctmp_N = 0;
         return  ;
      }

      public bool gxTv_SdtServico_Servico_perctmp_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Servico_PercPgm_N" )]
      [  XmlElement( ElementName = "Servico_PercPgm_N"   )]
      public short gxTpr_Servico_percpgm_N
      {
         get {
            return gxTv_SdtServico_Servico_percpgm_N ;
         }

         set {
            gxTv_SdtServico_Servico_percpgm_N = (short)(value);
         }

      }

      public void gxTv_SdtServico_Servico_percpgm_N_SetNull( )
      {
         gxTv_SdtServico_Servico_percpgm_N = 0;
         return  ;
      }

      public bool gxTv_SdtServico_Servico_percpgm_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Servico_PercCnc_N" )]
      [  XmlElement( ElementName = "Servico_PercCnc_N"   )]
      public short gxTpr_Servico_perccnc_N
      {
         get {
            return gxTv_SdtServico_Servico_perccnc_N ;
         }

         set {
            gxTv_SdtServico_Servico_perccnc_N = (short)(value);
         }

      }

      public void gxTv_SdtServico_Servico_perccnc_N_SetNull( )
      {
         gxTv_SdtServico_Servico_perccnc_N = 0;
         return  ;
      }

      public bool gxTv_SdtServico_Servico_perccnc_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Servico_Anterior_N" )]
      [  XmlElement( ElementName = "Servico_Anterior_N"   )]
      public short gxTpr_Servico_anterior_N
      {
         get {
            return gxTv_SdtServico_Servico_anterior_N ;
         }

         set {
            gxTv_SdtServico_Servico_anterior_N = (short)(value);
         }

      }

      public void gxTv_SdtServico_Servico_anterior_N_SetNull( )
      {
         gxTv_SdtServico_Servico_anterior_N = 0;
         return  ;
      }

      public bool gxTv_SdtServico_Servico_anterior_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Servico_Posterior_N" )]
      [  XmlElement( ElementName = "Servico_Posterior_N"   )]
      public short gxTpr_Servico_posterior_N
      {
         get {
            return gxTv_SdtServico_Servico_posterior_N ;
         }

         set {
            gxTv_SdtServico_Servico_posterior_N = (short)(value);
         }

      }

      public void gxTv_SdtServico_Servico_posterior_N_SetNull( )
      {
         gxTv_SdtServico_Servico_posterior_N = 0;
         return  ;
      }

      public bool gxTv_SdtServico_Servico_posterior_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Servico_LinNegCod_N" )]
      [  XmlElement( ElementName = "Servico_LinNegCod_N"   )]
      public short gxTpr_Servico_linnegcod_N
      {
         get {
            return gxTv_SdtServico_Servico_linnegcod_N ;
         }

         set {
            gxTv_SdtServico_Servico_linnegcod_N = (short)(value);
         }

      }

      public void gxTv_SdtServico_Servico_linnegcod_N_SetNull( )
      {
         gxTv_SdtServico_Servico_linnegcod_N = 0;
         return  ;
      }

      public bool gxTv_SdtServico_Servico_linnegcod_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Servico_LinNegDsc_N" )]
      [  XmlElement( ElementName = "Servico_LinNegDsc_N"   )]
      public short gxTpr_Servico_linnegdsc_N
      {
         get {
            return gxTv_SdtServico_Servico_linnegdsc_N ;
         }

         set {
            gxTv_SdtServico_Servico_linnegdsc_N = (short)(value);
         }

      }

      public void gxTv_SdtServico_Servico_linnegdsc_N_SetNull( )
      {
         gxTv_SdtServico_Servico_linnegdsc_N = 0;
         return  ;
      }

      public bool gxTv_SdtServico_Servico_linnegdsc_N_IsNull( )
      {
         return false ;
      }

      public void initialize( )
      {
         gxTv_SdtServico_Servico_nome = "";
         gxTv_SdtServico_Servico_descricao = "";
         gxTv_SdtServico_Servico_sigla = "";
         gxTv_SdtServico_Servicogrupo_descricao = "";
         gxTv_SdtServico_Servico_vincdesc = "";
         gxTv_SdtServico_Servico_vincsigla = "";
         gxTv_SdtServico_Servico_tela = "";
         gxTv_SdtServico_Servico_atende = "";
         gxTv_SdtServico_Servico_obrigavalores = "";
         gxTv_SdtServico_Servico_objetocontrole = "";
         gxTv_SdtServico_Servico_linnegdsc = "";
         gxTv_SdtServico_Servico_identificacao = "";
         gxTv_SdtServico_Mode = "";
         gxTv_SdtServico_Servico_nome_Z = "";
         gxTv_SdtServico_Servico_sigla_Z = "";
         gxTv_SdtServico_Servicogrupo_descricao_Z = "";
         gxTv_SdtServico_Servico_vincsigla_Z = "";
         gxTv_SdtServico_Servico_tela_Z = "";
         gxTv_SdtServico_Servico_atende_Z = "";
         gxTv_SdtServico_Servico_obrigavalores_Z = "";
         gxTv_SdtServico_Servico_objetocontrole_Z = "";
         gxTv_SdtServico_Servico_linnegdsc_Z = "";
         gxTv_SdtServico_Servico_identificacao_Z = "";
         gxTv_SdtServico_Servico_tipohierarquia = 1;
         gxTv_SdtServico_Servico_ativo = true;
         gxTv_SdtServico_Servico_ispublico = false;
         gxTv_SdtServico_Servico_pausasla = false;
         sTagName = "";
         IGxSilentTrn obj ;
         obj = (IGxSilentTrn)ClassLoader.FindInstance( "servico", "GeneXus.Programs.servico_bc", new Object[] {context}, constructorCallingAssembly);
         obj.initialize();
         obj.SetSDT(this, 1);
         setTransaction( obj) ;
         obj.SetMode("INS");
         return  ;
      }

      private short gxTv_SdtServico_Servico_vinculados ;
      private short gxTv_SdtServico_Servico_tipohierarquia ;
      private short gxTv_SdtServico_Servico_perctmp ;
      private short gxTv_SdtServico_Servico_percpgm ;
      private short gxTv_SdtServico_Servico_perccnc ;
      private short gxTv_SdtServico_Initialized ;
      private short gxTv_SdtServico_Servico_vinculados_Z ;
      private short gxTv_SdtServico_Servico_tipohierarquia_Z ;
      private short gxTv_SdtServico_Servico_perctmp_Z ;
      private short gxTv_SdtServico_Servico_percpgm_Z ;
      private short gxTv_SdtServico_Servico_perccnc_Z ;
      private short gxTv_SdtServico_Servico_descricao_N ;
      private short gxTv_SdtServico_Servico_uo_N ;
      private short gxTv_SdtServico_Servico_uorespexclusiva_N ;
      private short gxTv_SdtServico_Servico_vinculado_N ;
      private short gxTv_SdtServico_Servico_vincdesc_N ;
      private short gxTv_SdtServico_Servico_vincsigla_N ;
      private short gxTv_SdtServico_Servico_terceriza_N ;
      private short gxTv_SdtServico_Servico_tela_N ;
      private short gxTv_SdtServico_Servico_atende_N ;
      private short gxTv_SdtServico_Servico_obrigavalores_N ;
      private short gxTv_SdtServico_Servico_tipohierarquia_N ;
      private short gxTv_SdtServico_Servico_perctmp_N ;
      private short gxTv_SdtServico_Servico_percpgm_N ;
      private short gxTv_SdtServico_Servico_perccnc_N ;
      private short gxTv_SdtServico_Servico_anterior_N ;
      private short gxTv_SdtServico_Servico_posterior_N ;
      private short gxTv_SdtServico_Servico_linnegcod_N ;
      private short gxTv_SdtServico_Servico_linnegdsc_N ;
      private short readOk ;
      private short nOutParmCount ;
      private int gxTv_SdtServico_Servico_codigo ;
      private int gxTv_SdtServico_Servicogrupo_codigo ;
      private int gxTv_SdtServico_Servico_uo ;
      private int gxTv_SdtServico_Servico_vinculado ;
      private int gxTv_SdtServico_Servico_anterior ;
      private int gxTv_SdtServico_Servico_posterior ;
      private int gxTv_SdtServico_Servico_responsavel ;
      private int gxTv_SdtServico_Servico_linnegcod ;
      private int gxTv_SdtServico_Servico_codigo_Z ;
      private int gxTv_SdtServico_Servicogrupo_codigo_Z ;
      private int gxTv_SdtServico_Servico_uo_Z ;
      private int gxTv_SdtServico_Servico_vinculado_Z ;
      private int gxTv_SdtServico_Servico_anterior_Z ;
      private int gxTv_SdtServico_Servico_posterior_Z ;
      private int gxTv_SdtServico_Servico_responsavel_Z ;
      private int gxTv_SdtServico_Servico_linnegcod_Z ;
      private String gxTv_SdtServico_Servico_nome ;
      private String gxTv_SdtServico_Servico_sigla ;
      private String gxTv_SdtServico_Servico_vincsigla ;
      private String gxTv_SdtServico_Servico_tela ;
      private String gxTv_SdtServico_Servico_atende ;
      private String gxTv_SdtServico_Servico_obrigavalores ;
      private String gxTv_SdtServico_Servico_objetocontrole ;
      private String gxTv_SdtServico_Mode ;
      private String gxTv_SdtServico_Servico_nome_Z ;
      private String gxTv_SdtServico_Servico_sigla_Z ;
      private String gxTv_SdtServico_Servico_vincsigla_Z ;
      private String gxTv_SdtServico_Servico_tela_Z ;
      private String gxTv_SdtServico_Servico_atende_Z ;
      private String gxTv_SdtServico_Servico_obrigavalores_Z ;
      private String gxTv_SdtServico_Servico_objetocontrole_Z ;
      private String sTagName ;
      private bool gxTv_SdtServico_Servico_uorespexclusiva ;
      private bool gxTv_SdtServico_Servico_terceriza ;
      private bool gxTv_SdtServico_Servico_ativo ;
      private bool gxTv_SdtServico_Servico_ispublico ;
      private bool gxTv_SdtServico_Servico_isorigemreferencia ;
      private bool gxTv_SdtServico_Servico_pausasla ;
      private bool gxTv_SdtServico_Servico_uorespexclusiva_Z ;
      private bool gxTv_SdtServico_Servico_terceriza_Z ;
      private bool gxTv_SdtServico_Servico_ativo_Z ;
      private bool gxTv_SdtServico_Servico_ispublico_Z ;
      private bool gxTv_SdtServico_Servico_isorigemreferencia_Z ;
      private bool gxTv_SdtServico_Servico_pausasla_Z ;
      private String gxTv_SdtServico_Servico_descricao ;
      private String gxTv_SdtServico_Servico_vincdesc ;
      private String gxTv_SdtServico_Servicogrupo_descricao ;
      private String gxTv_SdtServico_Servico_linnegdsc ;
      private String gxTv_SdtServico_Servico_identificacao ;
      private String gxTv_SdtServico_Servicogrupo_descricao_Z ;
      private String gxTv_SdtServico_Servico_linnegdsc_Z ;
      private String gxTv_SdtServico_Servico_identificacao_Z ;
      private Assembly constructorCallingAssembly ;
   }

   [DataContract(Name = @"Servico", Namespace = "GxEv3Up14_MeetrikaVs3")]
   public class SdtServico_RESTInterface : GxGenericCollectionItem<SdtServico>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtServico_RESTInterface( ) : base()
      {
      }

      public SdtServico_RESTInterface( SdtServico psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "Servico_Codigo" , Order = 0 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Servico_codigo
      {
         get {
            return sdt.gxTpr_Servico_codigo ;
         }

         set {
            sdt.gxTpr_Servico_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Servico_Nome" , Order = 1 )]
      [GxSeudo()]
      public String gxTpr_Servico_nome
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Servico_nome) ;
         }

         set {
            sdt.gxTpr_Servico_nome = (String)(value);
         }

      }

      [DataMember( Name = "Servico_Descricao" , Order = 2 )]
      public String gxTpr_Servico_descricao
      {
         get {
            return sdt.gxTpr_Servico_descricao ;
         }

         set {
            sdt.gxTpr_Servico_descricao = (String)(value);
         }

      }

      [DataMember( Name = "Servico_Sigla" , Order = 3 )]
      [GxSeudo()]
      public String gxTpr_Servico_sigla
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Servico_sigla) ;
         }

         set {
            sdt.gxTpr_Servico_sigla = (String)(value);
         }

      }

      [DataMember( Name = "ServicoGrupo_Codigo" , Order = 4 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Servicogrupo_codigo
      {
         get {
            return sdt.gxTpr_Servicogrupo_codigo ;
         }

         set {
            sdt.gxTpr_Servicogrupo_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ServicoGrupo_Descricao" , Order = 5 )]
      [GxSeudo()]
      public String gxTpr_Servicogrupo_descricao
      {
         get {
            return sdt.gxTpr_Servicogrupo_descricao ;
         }

         set {
            sdt.gxTpr_Servicogrupo_descricao = (String)(value);
         }

      }

      [DataMember( Name = "Servico_UO" , Order = 6 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Servico_uo
      {
         get {
            return sdt.gxTpr_Servico_uo ;
         }

         set {
            sdt.gxTpr_Servico_uo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Servico_UORespExclusiva" , Order = 7 )]
      [GxSeudo()]
      public bool gxTpr_Servico_uorespexclusiva
      {
         get {
            return sdt.gxTpr_Servico_uorespexclusiva ;
         }

         set {
            sdt.gxTpr_Servico_uorespexclusiva = value;
         }

      }

      [DataMember( Name = "Servico_Vinculado" , Order = 8 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Servico_vinculado
      {
         get {
            return sdt.gxTpr_Servico_vinculado ;
         }

         set {
            sdt.gxTpr_Servico_vinculado = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Servico_Vinculados" , Order = 9 )]
      [GxSeudo()]
      public Nullable<short> gxTpr_Servico_vinculados
      {
         get {
            return sdt.gxTpr_Servico_vinculados ;
         }

         set {
            sdt.gxTpr_Servico_vinculados = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Servico_VincDesc" , Order = 10 )]
      public String gxTpr_Servico_vincdesc
      {
         get {
            return sdt.gxTpr_Servico_vincdesc ;
         }

         set {
            sdt.gxTpr_Servico_vincdesc = (String)(value);
         }

      }

      [DataMember( Name = "Servico_VincSigla" , Order = 11 )]
      [GxSeudo()]
      public String gxTpr_Servico_vincsigla
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Servico_vincsigla) ;
         }

         set {
            sdt.gxTpr_Servico_vincsigla = (String)(value);
         }

      }

      [DataMember( Name = "Servico_Terceriza" , Order = 12 )]
      [GxSeudo()]
      public bool gxTpr_Servico_terceriza
      {
         get {
            return sdt.gxTpr_Servico_terceriza ;
         }

         set {
            sdt.gxTpr_Servico_terceriza = value;
         }

      }

      [DataMember( Name = "Servico_Tela" , Order = 13 )]
      [GxSeudo()]
      public String gxTpr_Servico_tela
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Servico_tela) ;
         }

         set {
            sdt.gxTpr_Servico_tela = (String)(value);
         }

      }

      [DataMember( Name = "Servico_Atende" , Order = 14 )]
      [GxSeudo()]
      public String gxTpr_Servico_atende
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Servico_atende) ;
         }

         set {
            sdt.gxTpr_Servico_atende = (String)(value);
         }

      }

      [DataMember( Name = "Servico_ObrigaValores" , Order = 15 )]
      [GxSeudo()]
      public String gxTpr_Servico_obrigavalores
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Servico_obrigavalores) ;
         }

         set {
            sdt.gxTpr_Servico_obrigavalores = (String)(value);
         }

      }

      [DataMember( Name = "Servico_ObjetoControle" , Order = 16 )]
      [GxSeudo()]
      public String gxTpr_Servico_objetocontrole
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Servico_objetocontrole) ;
         }

         set {
            sdt.gxTpr_Servico_objetocontrole = (String)(value);
         }

      }

      [DataMember( Name = "Servico_TipoHierarquia" , Order = 17 )]
      [GxSeudo()]
      public Nullable<short> gxTpr_Servico_tipohierarquia
      {
         get {
            return sdt.gxTpr_Servico_tipohierarquia ;
         }

         set {
            sdt.gxTpr_Servico_tipohierarquia = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Servico_PercTmp" , Order = 18 )]
      [GxSeudo()]
      public Nullable<short> gxTpr_Servico_perctmp
      {
         get {
            return sdt.gxTpr_Servico_perctmp ;
         }

         set {
            sdt.gxTpr_Servico_perctmp = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Servico_PercPgm" , Order = 19 )]
      [GxSeudo()]
      public Nullable<short> gxTpr_Servico_percpgm
      {
         get {
            return sdt.gxTpr_Servico_percpgm ;
         }

         set {
            sdt.gxTpr_Servico_percpgm = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Servico_PercCnc" , Order = 20 )]
      [GxSeudo()]
      public Nullable<short> gxTpr_Servico_perccnc
      {
         get {
            return sdt.gxTpr_Servico_perccnc ;
         }

         set {
            sdt.gxTpr_Servico_perccnc = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Servico_Anterior" , Order = 21 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Servico_anterior
      {
         get {
            return sdt.gxTpr_Servico_anterior ;
         }

         set {
            sdt.gxTpr_Servico_anterior = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Servico_Posterior" , Order = 22 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Servico_posterior
      {
         get {
            return sdt.gxTpr_Servico_posterior ;
         }

         set {
            sdt.gxTpr_Servico_posterior = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Servico_Responsavel" , Order = 23 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Servico_responsavel
      {
         get {
            return sdt.gxTpr_Servico_responsavel ;
         }

         set {
            sdt.gxTpr_Servico_responsavel = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Servico_Ativo" , Order = 24 )]
      [GxSeudo()]
      public bool gxTpr_Servico_ativo
      {
         get {
            return sdt.gxTpr_Servico_ativo ;
         }

         set {
            sdt.gxTpr_Servico_ativo = value;
         }

      }

      [DataMember( Name = "Servico_IsPublico" , Order = 25 )]
      [GxSeudo()]
      public bool gxTpr_Servico_ispublico
      {
         get {
            return sdt.gxTpr_Servico_ispublico ;
         }

         set {
            sdt.gxTpr_Servico_ispublico = value;
         }

      }

      [DataMember( Name = "Servico_LinNegCod" , Order = 26 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Servico_linnegcod
      {
         get {
            return sdt.gxTpr_Servico_linnegcod ;
         }

         set {
            sdt.gxTpr_Servico_linnegcod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Servico_LinNegDsc" , Order = 27 )]
      [GxSeudo()]
      public String gxTpr_Servico_linnegdsc
      {
         get {
            return sdt.gxTpr_Servico_linnegdsc ;
         }

         set {
            sdt.gxTpr_Servico_linnegdsc = (String)(value);
         }

      }

      [DataMember( Name = "Servico_IsOrigemReferencia" , Order = 28 )]
      [GxSeudo()]
      public bool gxTpr_Servico_isorigemreferencia
      {
         get {
            return sdt.gxTpr_Servico_isorigemreferencia ;
         }

         set {
            sdt.gxTpr_Servico_isorigemreferencia = value;
         }

      }

      [DataMember( Name = "Servico_Identificacao" , Order = 29 )]
      [GxSeudo()]
      public String gxTpr_Servico_identificacao
      {
         get {
            return sdt.gxTpr_Servico_identificacao ;
         }

         set {
            sdt.gxTpr_Servico_identificacao = (String)(value);
         }

      }

      [DataMember( Name = "Servico_PausaSLA" , Order = 30 )]
      [GxSeudo()]
      public bool gxTpr_Servico_pausasla
      {
         get {
            return sdt.gxTpr_Servico_pausasla ;
         }

         set {
            sdt.gxTpr_Servico_pausasla = value;
         }

      }

      public SdtServico sdt
      {
         get {
            return (SdtServico)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtServico() ;
         }
      }

      [DataMember( Name = "gx_md5_hash", Order = 80 )]
      public string Hash
      {
         get {
            if ( StringUtil.StrCmp(md5Hash, null) == 0 )
            {
               md5Hash = (String)(getHash());
            }
            return md5Hash ;
         }

         set {
            md5Hash = value ;
         }

      }

      private String md5Hash ;
   }

}
