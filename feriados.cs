/*
               File: Feriados
        Description: Feriados
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 23:6:42.62
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class feriados : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV7Feriado_Data = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7Feriado_Data", context.localUtil.Format(AV7Feriado_Data, "99/99/99"));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vFERIADO_DATA", GetSecureSignedToken( "", AV7Feriado_Data));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         chkFeriado_Fixo.Name = "FERIADO_FIXO";
         chkFeriado_Fixo.WebTags = "";
         chkFeriado_Fixo.Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkFeriado_Fixo_Internalname, "TitleCaption", chkFeriado_Fixo.Caption);
         chkFeriado_Fixo.CheckedValue = "false";
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Feriados", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtFeriado_Data_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public feriados( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public feriados( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           DateTime aP1_Feriado_Data )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV7Feriado_Data = aP1_Feriado_Data;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         chkFeriado_Fixo = new GXCheckbox();
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_39137( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_39137e( bool wbgen )
      {
         if ( wbgen )
         {
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_39137( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_39137( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_39137e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_31_39137( true) ;
         }
         return  ;
      }

      protected void wb_table3_31_39137e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_39137e( true) ;
         }
         else
         {
            wb_table1_2_39137e( false) ;
         }
      }

      protected void wb_table3_31_39137( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 34,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", "Confirmar", bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_trn_enter_Visible, bttBtn_trn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_Feriados.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "", "Fechar", bttBtn_trn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_trn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_Feriados.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'',false,'',0)\"";
            ClassString = "btn btn-danger";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_delete_Internalname, "", "Eliminar", bttBtn_trn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_trn_delete_Visible, bttBtn_trn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_Feriados.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_31_39137e( true) ;
         }
         else
         {
            wb_table3_31_39137e( false) ;
         }
      }

      protected void wb_table2_5_39137( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table4_13_39137( true) ;
         }
         return  ;
      }

      protected void wb_table4_13_39137e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_39137e( true) ;
         }
         else
         {
            wb_table2_5_39137e( false) ;
         }
      }

      protected void wb_table4_13_39137( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableData", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockferiado_data_Internalname, "Data", "", "", lblTextblockferiado_data_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Feriados.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtFeriado_Data_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtFeriado_Data_Internalname, context.localUtil.Format(A1175Feriado_Data, "99/99/99"), context.localUtil.Format( A1175Feriado_Data, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,18);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtFeriado_Data_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, edtFeriado_Data_Enabled, 1, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Feriados.htm");
            GxWebStd.gx_bitmap( context, edtFeriado_Data_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtFeriado_Data_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_Feriados.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockferiado_nome_Internalname, "Descri��o", "", "", lblTextblockferiado_nome_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Feriados.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtFeriado_Nome_Internalname, StringUtil.RTrim( A1176Feriado_Nome), StringUtil.RTrim( context.localUtil.Format( A1176Feriado_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,23);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtFeriado_Nome_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtFeriado_Nome_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_Feriados.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockferiado_fixo_Internalname, "Fixo", "", "", lblTextblockferiado_fixo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Feriados.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 28,'',false,'',0)\"";
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkFeriado_Fixo_Internalname, StringUtil.BoolToStr( A1195Feriado_Fixo), "", "", 1, chkFeriado_Fixo.Enabled, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(28, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,28);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_13_39137e( true) ;
         }
         else
         {
            wb_table4_13_39137e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11392 */
         E11392 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               if ( context.localUtil.VCDate( cgiGet( edtFeriado_Data_Internalname), 2) == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Data"}), 1, "FERIADO_DATA");
                  AnyError = 1;
                  GX_FocusControl = edtFeriado_Data_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1175Feriado_Data = DateTime.MinValue;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1175Feriado_Data", context.localUtil.Format(A1175Feriado_Data, "99/99/99"));
               }
               else
               {
                  A1175Feriado_Data = context.localUtil.CToD( cgiGet( edtFeriado_Data_Internalname), 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1175Feriado_Data", context.localUtil.Format(A1175Feriado_Data, "99/99/99"));
               }
               A1176Feriado_Nome = StringUtil.Upper( cgiGet( edtFeriado_Nome_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1176Feriado_Nome", A1176Feriado_Nome);
               A1195Feriado_Fixo = StringUtil.StrToBool( cgiGet( chkFeriado_Fixo_Internalname));
               n1195Feriado_Fixo = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1195Feriado_Fixo", A1195Feriado_Fixo);
               n1195Feriado_Fixo = ((false==A1195Feriado_Fixo) ? true : false);
               /* Read saved values. */
               Z1175Feriado_Data = context.localUtil.CToD( cgiGet( "Z1175Feriado_Data"), 0);
               Z1176Feriado_Nome = cgiGet( "Z1176Feriado_Nome");
               Z1195Feriado_Fixo = StringUtil.StrToBool( cgiGet( "Z1195Feriado_Fixo"));
               n1195Feriado_Fixo = ((false==A1195Feriado_Fixo) ? true : false);
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               AV7Feriado_Data = context.localUtil.CToD( cgiGet( "vFERIADO_DATA"), 0);
               Gx_mode = cgiGet( "vMODE");
               Dvpanel_tableattributes_Width = cgiGet( "DVPANEL_TABLEATTRIBUTES_Width");
               Dvpanel_tableattributes_Height = cgiGet( "DVPANEL_TABLEATTRIBUTES_Height");
               Dvpanel_tableattributes_Cls = cgiGet( "DVPANEL_TABLEATTRIBUTES_Cls");
               Dvpanel_tableattributes_Title = cgiGet( "DVPANEL_TABLEATTRIBUTES_Title");
               Dvpanel_tableattributes_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsible"));
               Dvpanel_tableattributes_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsed"));
               Dvpanel_tableattributes_Enabled = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Enabled"));
               Dvpanel_tableattributes_Class = cgiGet( "DVPANEL_TABLEATTRIBUTES_Class");
               Dvpanel_tableattributes_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autowidth"));
               Dvpanel_tableattributes_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoheight"));
               Dvpanel_tableattributes_Showheader = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showheader"));
               Dvpanel_tableattributes_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showcollapseicon"));
               Dvpanel_tableattributes_Iconposition = cgiGet( "DVPANEL_TABLEATTRIBUTES_Iconposition");
               Dvpanel_tableattributes_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoscroll"));
               Dvpanel_tableattributes_Visible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Visible"));
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "Feriados";
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               hsh = cgiGet( "hsh");
               if ( ( ! ( ( A1175Feriado_Data != Z1175Feriado_Data ) ) || ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ) && ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("feriados:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  A1175Feriado_Data = context.localUtil.ParseDateParm( GetNextPar( ));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1175Feriado_Data", context.localUtil.Format(A1175Feriado_Data, "99/99/99"));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode137 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     Gx_mode = sMode137;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound137 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_390( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_trn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "FERIADO_DATA");
                        AnyError = 1;
                        GX_FocusControl = edtFeriado_Data_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E11392 */
                           E11392 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E12392 */
                           E12392 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E12392 */
            E12392 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll39137( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         bttBtn_trn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtn_trn_delete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_trn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
            }
            DisableAttributes39137( ) ;
         }
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_390( )
      {
         BeforeValidate39137( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls39137( ) ;
            }
            else
            {
               CheckExtendedTable39137( ) ;
               CloseExtendedTableCursors39137( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         }
      }

      protected void ResetCaption390( )
      {
      }

      protected void E11392( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
      }

      protected void E12392( )
      {
         /* After Trn Routine */
         if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV9TrnContext.gxTpr_Callerondelete )
         {
            context.wjLoc = formatLink("wwferiados.aspx") ;
            context.wjLocDisableFrm = 1;
         }
         context.setWebReturnParms(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void ZM39137( short GX_JID )
      {
         if ( ( GX_JID == 5 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z1176Feriado_Nome = T00393_A1176Feriado_Nome[0];
               Z1195Feriado_Fixo = T00393_A1195Feriado_Fixo[0];
            }
            else
            {
               Z1176Feriado_Nome = A1176Feriado_Nome;
               Z1195Feriado_Fixo = A1195Feriado_Fixo;
            }
         }
         if ( GX_JID == -5 )
         {
            Z1175Feriado_Data = A1175Feriado_Data;
            Z1176Feriado_Nome = A1176Feriado_Nome;
            Z1195Feriado_Fixo = A1195Feriado_Fixo;
         }
      }

      protected void standaloneNotModal( )
      {
         bttBtn_trn_delete_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         if ( ! (DateTime.MinValue==AV7Feriado_Data) )
         {
            A1175Feriado_Data = AV7Feriado_Data;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1175Feriado_Data", context.localUtil.Format(A1175Feriado_Data, "99/99/99"));
         }
         if ( ! (DateTime.MinValue==AV7Feriado_Data) )
         {
            edtFeriado_Data_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFeriado_Data_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFeriado_Data_Enabled), 5, 0)));
         }
         else
         {
            edtFeriado_Data_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFeriado_Data_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFeriado_Data_Enabled), 5, 0)));
         }
         if ( ! (DateTime.MinValue==AV7Feriado_Data) )
         {
            edtFeriado_Data_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFeriado_Data_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFeriado_Data_Enabled), 5, 0)));
         }
      }

      protected void standaloneModal( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
         }
      }

      protected void Load39137( )
      {
         /* Using cursor T00394 */
         pr_default.execute(2, new Object[] {A1175Feriado_Data});
         if ( (pr_default.getStatus(2) != 101) )
         {
            RcdFound137 = 1;
            A1176Feriado_Nome = T00394_A1176Feriado_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1176Feriado_Nome", A1176Feriado_Nome);
            A1195Feriado_Fixo = T00394_A1195Feriado_Fixo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1195Feriado_Fixo", A1195Feriado_Fixo);
            n1195Feriado_Fixo = T00394_n1195Feriado_Fixo[0];
            ZM39137( -5) ;
         }
         pr_default.close(2);
         OnLoadActions39137( ) ;
      }

      protected void OnLoadActions39137( )
      {
      }

      protected void CheckExtendedTable39137( )
      {
         Gx_BScreen = 1;
         standaloneModal( ) ;
         if ( ! ( (DateTime.MinValue==A1175Feriado_Data) || ( A1175Feriado_Data >= context.localUtil.YMDToD( 1753, 1, 1) ) ) )
         {
            GX_msglist.addItem("Campo Data fora do intervalo", "OutOfRange", 1, "FERIADO_DATA");
            AnyError = 1;
            GX_FocusControl = edtFeriado_Data_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
      }

      protected void CloseExtendedTableCursors39137( )
      {
      }

      protected void enableDisable( )
      {
      }

      protected void GetKey39137( )
      {
         /* Using cursor T00395 */
         pr_default.execute(3, new Object[] {A1175Feriado_Data});
         if ( (pr_default.getStatus(3) != 101) )
         {
            RcdFound137 = 1;
         }
         else
         {
            RcdFound137 = 0;
         }
         pr_default.close(3);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T00393 */
         pr_default.execute(1, new Object[] {A1175Feriado_Data});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM39137( 5) ;
            RcdFound137 = 1;
            A1175Feriado_Data = T00393_A1175Feriado_Data[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1175Feriado_Data", context.localUtil.Format(A1175Feriado_Data, "99/99/99"));
            A1176Feriado_Nome = T00393_A1176Feriado_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1176Feriado_Nome", A1176Feriado_Nome);
            A1195Feriado_Fixo = T00393_A1195Feriado_Fixo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1195Feriado_Fixo", A1195Feriado_Fixo);
            n1195Feriado_Fixo = T00393_n1195Feriado_Fixo[0];
            Z1175Feriado_Data = A1175Feriado_Data;
            sMode137 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load39137( ) ;
            if ( AnyError == 1 )
            {
               RcdFound137 = 0;
               InitializeNonKey39137( ) ;
            }
            Gx_mode = sMode137;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound137 = 0;
            InitializeNonKey39137( ) ;
            sMode137 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal( ) ;
            Gx_mode = sMode137;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey39137( ) ;
         if ( RcdFound137 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound137 = 0;
         /* Using cursor T00396 */
         pr_default.execute(4, new Object[] {A1175Feriado_Data});
         if ( (pr_default.getStatus(4) != 101) )
         {
            while ( (pr_default.getStatus(4) != 101) && ( ( T00396_A1175Feriado_Data[0] < A1175Feriado_Data ) ) )
            {
               pr_default.readNext(4);
            }
            if ( (pr_default.getStatus(4) != 101) && ( ( T00396_A1175Feriado_Data[0] > A1175Feriado_Data ) ) )
            {
               A1175Feriado_Data = T00396_A1175Feriado_Data[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1175Feriado_Data", context.localUtil.Format(A1175Feriado_Data, "99/99/99"));
               RcdFound137 = 1;
            }
         }
         pr_default.close(4);
      }

      protected void move_previous( )
      {
         RcdFound137 = 0;
         /* Using cursor T00397 */
         pr_default.execute(5, new Object[] {A1175Feriado_Data});
         if ( (pr_default.getStatus(5) != 101) )
         {
            while ( (pr_default.getStatus(5) != 101) && ( ( T00397_A1175Feriado_Data[0] > A1175Feriado_Data ) ) )
            {
               pr_default.readNext(5);
            }
            if ( (pr_default.getStatus(5) != 101) && ( ( T00397_A1175Feriado_Data[0] < A1175Feriado_Data ) ) )
            {
               A1175Feriado_Data = T00397_A1175Feriado_Data[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1175Feriado_Data", context.localUtil.Format(A1175Feriado_Data, "99/99/99"));
               RcdFound137 = 1;
            }
         }
         pr_default.close(5);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey39137( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtFeriado_Data_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert39137( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound137 == 1 )
            {
               if ( A1175Feriado_Data != Z1175Feriado_Data )
               {
                  A1175Feriado_Data = Z1175Feriado_Data;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1175Feriado_Data", context.localUtil.Format(A1175Feriado_Data, "99/99/99"));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "FERIADO_DATA");
                  AnyError = 1;
                  GX_FocusControl = edtFeriado_Data_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtFeriado_Data_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  Update39137( ) ;
                  GX_FocusControl = edtFeriado_Data_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A1175Feriado_Data != Z1175Feriado_Data )
               {
                  /* Insert record */
                  GX_FocusControl = edtFeriado_Data_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert39137( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "FERIADO_DATA");
                     AnyError = 1;
                     GX_FocusControl = edtFeriado_Data_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     /* Insert record */
                     GX_FocusControl = edtFeriado_Data_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert39137( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( A1175Feriado_Data != Z1175Feriado_Data )
         {
            A1175Feriado_Data = Z1175Feriado_Data;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1175Feriado_Data", context.localUtil.Format(A1175Feriado_Data, "99/99/99"));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "FERIADO_DATA");
            AnyError = 1;
            GX_FocusControl = edtFeriado_Data_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtFeriado_Data_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency39137( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T00392 */
            pr_default.execute(0, new Object[] {A1175Feriado_Data});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Feriados"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z1176Feriado_Nome, T00392_A1176Feriado_Nome[0]) != 0 ) || ( Z1195Feriado_Fixo != T00392_A1195Feriado_Fixo[0] ) )
            {
               if ( StringUtil.StrCmp(Z1176Feriado_Nome, T00392_A1176Feriado_Nome[0]) != 0 )
               {
                  GXUtil.WriteLog("feriados:[seudo value changed for attri]"+"Feriado_Nome");
                  GXUtil.WriteLogRaw("Old: ",Z1176Feriado_Nome);
                  GXUtil.WriteLogRaw("Current: ",T00392_A1176Feriado_Nome[0]);
               }
               if ( Z1195Feriado_Fixo != T00392_A1195Feriado_Fixo[0] )
               {
                  GXUtil.WriteLog("feriados:[seudo value changed for attri]"+"Feriado_Fixo");
                  GXUtil.WriteLogRaw("Old: ",Z1195Feriado_Fixo);
                  GXUtil.WriteLogRaw("Current: ",T00392_A1195Feriado_Fixo[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"Feriados"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert39137( )
      {
         BeforeValidate39137( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable39137( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM39137( 0) ;
            CheckOptimisticConcurrency39137( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm39137( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert39137( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T00398 */
                     pr_default.execute(6, new Object[] {A1175Feriado_Data, A1176Feriado_Nome, n1195Feriado_Fixo, A1195Feriado_Fixo});
                     pr_default.close(6);
                     dsDefault.SmartCacheProvider.SetUpdated("Feriados") ;
                     if ( (pr_default.getStatus(6) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption390( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load39137( ) ;
            }
            EndLevel39137( ) ;
         }
         CloseExtendedTableCursors39137( ) ;
      }

      protected void Update39137( )
      {
         BeforeValidate39137( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable39137( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency39137( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm39137( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate39137( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T00399 */
                     pr_default.execute(7, new Object[] {A1176Feriado_Nome, n1195Feriado_Fixo, A1195Feriado_Fixo, A1175Feriado_Data});
                     pr_default.close(7);
                     dsDefault.SmartCacheProvider.SetUpdated("Feriados") ;
                     if ( (pr_default.getStatus(7) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Feriados"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate39137( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel39137( ) ;
         }
         CloseExtendedTableCursors39137( ) ;
      }

      protected void DeferredUpdate39137( )
      {
      }

      protected void delete( )
      {
         BeforeValidate39137( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency39137( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls39137( ) ;
            AfterConfirm39137( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete39137( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T003910 */
                  pr_default.execute(8, new Object[] {A1175Feriado_Data});
                  pr_default.close(8);
                  dsDefault.SmartCacheProvider.SetUpdated("Feriados") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                        {
                           if ( AnyError == 0 )
                           {
                              context.nUserReturn = 1;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode137 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel39137( ) ;
         Gx_mode = sMode137;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls39137( )
      {
         standaloneModal( ) ;
         /* No delete mode formulas found. */
      }

      protected void EndLevel39137( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete39137( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            context.CommitDataStores( "Feriados");
            if ( AnyError == 0 )
            {
               ConfirmValues390( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            context.RollbackDataStores( "Feriados");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart39137( )
      {
         /* Scan By routine */
         /* Using cursor T003911 */
         pr_default.execute(9);
         RcdFound137 = 0;
         if ( (pr_default.getStatus(9) != 101) )
         {
            RcdFound137 = 1;
            A1175Feriado_Data = T003911_A1175Feriado_Data[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1175Feriado_Data", context.localUtil.Format(A1175Feriado_Data, "99/99/99"));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext39137( )
      {
         /* Scan next routine */
         pr_default.readNext(9);
         RcdFound137 = 0;
         if ( (pr_default.getStatus(9) != 101) )
         {
            RcdFound137 = 1;
            A1175Feriado_Data = T003911_A1175Feriado_Data[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1175Feriado_Data", context.localUtil.Format(A1175Feriado_Data, "99/99/99"));
         }
      }

      protected void ScanEnd39137( )
      {
         pr_default.close(9);
      }

      protected void AfterConfirm39137( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert39137( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate39137( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete39137( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete39137( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate39137( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes39137( )
      {
         edtFeriado_Data_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFeriado_Data_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFeriado_Data_Enabled), 5, 0)));
         edtFeriado_Nome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFeriado_Nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFeriado_Nome_Enabled), 5, 0)));
         chkFeriado_Fixo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkFeriado_Fixo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkFeriado_Fixo.Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues390( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20204282364318");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("feriados.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode(DateTimeUtil.FormatDateParm(AV7Feriado_Data))+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z1175Feriado_Data", context.localUtil.DToC( Z1175Feriado_Data, 0, "/"));
         GxWebStd.gx_hidden_field( context, "Z1176Feriado_Nome", StringUtil.RTrim( Z1176Feriado_Nome));
         GxWebStd.gx_boolean_hidden_field( context, "Z1195Feriado_Fixo", Z1195Feriado_Fixo);
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV9TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV9TrnContext);
         }
         GxWebStd.gx_hidden_field( context, "vFERIADO_DATA", context.localUtil.DToC( AV7Feriado_Data, 0, "/"));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vFERIADO_DATA", GetSecureSignedToken( "", AV7Feriado_Data));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Width", StringUtil.RTrim( Dvpanel_tableattributes_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Cls", StringUtil.RTrim( Dvpanel_tableattributes_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Title", StringUtil.RTrim( Dvpanel_tableattributes_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsible", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsed", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Enabled", StringUtil.BoolToStr( Dvpanel_tableattributes_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autowidth", StringUtil.BoolToStr( Dvpanel_tableattributes_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoheight", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tableattributes_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Iconposition", StringUtil.RTrim( Dvpanel_tableattributes_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoscroll", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoscroll));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "Feriados";
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("feriados:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("feriados.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode(DateTimeUtil.FormatDateParm(AV7Feriado_Data)) ;
      }

      public override String GetPgmname( )
      {
         return "Feriados" ;
      }

      public override String GetPgmdesc( )
      {
         return "Feriados" ;
      }

      protected void InitializeNonKey39137( )
      {
         A1176Feriado_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1176Feriado_Nome", A1176Feriado_Nome);
         A1195Feriado_Fixo = false;
         n1195Feriado_Fixo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1195Feriado_Fixo", A1195Feriado_Fixo);
         n1195Feriado_Fixo = ((false==A1195Feriado_Fixo) ? true : false);
         Z1176Feriado_Nome = "";
         Z1195Feriado_Fixo = false;
      }

      protected void InitAll39137( )
      {
         A1175Feriado_Data = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1175Feriado_Data", context.localUtil.Format(A1175Feriado_Data, "99/99/99"));
         InitializeNonKey39137( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?2249787");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20204282364330");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("feriados.js", "?20204282364330");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockferiado_data_Internalname = "TEXTBLOCKFERIADO_DATA";
         edtFeriado_Data_Internalname = "FERIADO_DATA";
         lblTextblockferiado_nome_Internalname = "TEXTBLOCKFERIADO_NOME";
         edtFeriado_Nome_Internalname = "FERIADO_NOME";
         lblTextblockferiado_fixo_Internalname = "TEXTBLOCKFERIADO_FIXO";
         chkFeriado_Fixo_Internalname = "FERIADO_FIXO";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         Dvpanel_tableattributes_Internalname = "DVPANEL_TABLEATTRIBUTES";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         bttBtn_trn_delete_Internalname = "BTN_TRN_DELETE";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Dvpanel_tableattributes_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Iconposition = "left";
         Dvpanel_tableattributes_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tableattributes_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Title = "Feriado";
         Dvpanel_tableattributes_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tableattributes_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Feriados";
         chkFeriado_Fixo.Enabled = 1;
         edtFeriado_Nome_Jsonclick = "";
         edtFeriado_Nome_Enabled = 1;
         edtFeriado_Data_Jsonclick = "";
         edtFeriado_Data_Enabled = 1;
         bttBtn_trn_delete_Enabled = 0;
         bttBtn_trn_delete_Visible = 1;
         bttBtn_trn_cancel_Visible = 1;
         bttBtn_trn_enter_Enabled = 1;
         bttBtn_trn_enter_Visible = 1;
         chkFeriado_Fixo.Caption = "";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV7Feriado_Data',fld:'vFERIADO_DATA',pic:'',hsh:true,nv:''}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E12392',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV9TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         wcpOAV7Feriado_Data = DateTime.MinValue;
         Z1175Feriado_Data = DateTime.MinValue;
         Z1176Feriado_Nome = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         bttBtn_trn_delete_Jsonclick = "";
         lblTextblockferiado_data_Jsonclick = "";
         A1175Feriado_Data = DateTime.MinValue;
         lblTextblockferiado_nome_Jsonclick = "";
         A1176Feriado_Nome = "";
         lblTextblockferiado_fixo_Jsonclick = "";
         Dvpanel_tableattributes_Height = "";
         Dvpanel_tableattributes_Class = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode137 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         T00394_A1175Feriado_Data = new DateTime[] {DateTime.MinValue} ;
         T00394_A1176Feriado_Nome = new String[] {""} ;
         T00394_A1195Feriado_Fixo = new bool[] {false} ;
         T00394_n1195Feriado_Fixo = new bool[] {false} ;
         T00395_A1175Feriado_Data = new DateTime[] {DateTime.MinValue} ;
         T00393_A1175Feriado_Data = new DateTime[] {DateTime.MinValue} ;
         T00393_A1176Feriado_Nome = new String[] {""} ;
         T00393_A1195Feriado_Fixo = new bool[] {false} ;
         T00393_n1195Feriado_Fixo = new bool[] {false} ;
         T00396_A1175Feriado_Data = new DateTime[] {DateTime.MinValue} ;
         T00397_A1175Feriado_Data = new DateTime[] {DateTime.MinValue} ;
         T00392_A1175Feriado_Data = new DateTime[] {DateTime.MinValue} ;
         T00392_A1176Feriado_Nome = new String[] {""} ;
         T00392_A1195Feriado_Fixo = new bool[] {false} ;
         T00392_n1195Feriado_Fixo = new bool[] {false} ;
         T003911_A1175Feriado_Data = new DateTime[] {DateTime.MinValue} ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.feriados__default(),
            new Object[][] {
                new Object[] {
               T00392_A1175Feriado_Data, T00392_A1176Feriado_Nome, T00392_A1195Feriado_Fixo, T00392_n1195Feriado_Fixo
               }
               , new Object[] {
               T00393_A1175Feriado_Data, T00393_A1176Feriado_Nome, T00393_A1195Feriado_Fixo, T00393_n1195Feriado_Fixo
               }
               , new Object[] {
               T00394_A1175Feriado_Data, T00394_A1176Feriado_Nome, T00394_A1195Feriado_Fixo, T00394_n1195Feriado_Fixo
               }
               , new Object[] {
               T00395_A1175Feriado_Data
               }
               , new Object[] {
               T00396_A1175Feriado_Data
               }
               , new Object[] {
               T00397_A1175Feriado_Data
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T003911_A1175Feriado_Data
               }
            }
         );
      }

      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short RcdFound137 ;
      private short GX_JID ;
      private short Gx_BScreen ;
      private short gxajaxcallmode ;
      private int trnEnded ;
      private int bttBtn_trn_enter_Visible ;
      private int bttBtn_trn_enter_Enabled ;
      private int bttBtn_trn_cancel_Visible ;
      private int bttBtn_trn_delete_Visible ;
      private int bttBtn_trn_delete_Enabled ;
      private int edtFeriado_Data_Enabled ;
      private int edtFeriado_Nome_Enabled ;
      private int idxLst ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String Z1176Feriado_Nome ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String Gx_mode ;
      private String GXKey ;
      private String chkFeriado_Fixo_Internalname ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtFeriado_Data_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String bttBtn_trn_delete_Internalname ;
      private String bttBtn_trn_delete_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockferiado_data_Internalname ;
      private String lblTextblockferiado_data_Jsonclick ;
      private String edtFeriado_Data_Jsonclick ;
      private String lblTextblockferiado_nome_Internalname ;
      private String lblTextblockferiado_nome_Jsonclick ;
      private String edtFeriado_Nome_Internalname ;
      private String A1176Feriado_Nome ;
      private String edtFeriado_Nome_Jsonclick ;
      private String lblTextblockferiado_fixo_Internalname ;
      private String lblTextblockferiado_fixo_Jsonclick ;
      private String Dvpanel_tableattributes_Width ;
      private String Dvpanel_tableattributes_Height ;
      private String Dvpanel_tableattributes_Cls ;
      private String Dvpanel_tableattributes_Title ;
      private String Dvpanel_tableattributes_Class ;
      private String Dvpanel_tableattributes_Iconposition ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode137 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Dvpanel_tableattributes_Internalname ;
      private DateTime wcpOAV7Feriado_Data ;
      private DateTime Z1175Feriado_Data ;
      private DateTime AV7Feriado_Data ;
      private DateTime A1175Feriado_Data ;
      private bool Z1195Feriado_Fixo ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool A1195Feriado_Fixo ;
      private bool n1195Feriado_Fixo ;
      private bool Dvpanel_tableattributes_Collapsible ;
      private bool Dvpanel_tableattributes_Collapsed ;
      private bool Dvpanel_tableattributes_Enabled ;
      private bool Dvpanel_tableattributes_Autowidth ;
      private bool Dvpanel_tableattributes_Autoheight ;
      private bool Dvpanel_tableattributes_Showheader ;
      private bool Dvpanel_tableattributes_Showcollapseicon ;
      private bool Dvpanel_tableattributes_Autoscroll ;
      private bool Dvpanel_tableattributes_Visible ;
      private bool returnInSub ;
      private IGxSession AV10WebSession ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCheckbox chkFeriado_Fixo ;
      private IDataStoreProvider pr_default ;
      private DateTime[] T00394_A1175Feriado_Data ;
      private String[] T00394_A1176Feriado_Nome ;
      private bool[] T00394_A1195Feriado_Fixo ;
      private bool[] T00394_n1195Feriado_Fixo ;
      private DateTime[] T00395_A1175Feriado_Data ;
      private DateTime[] T00393_A1175Feriado_Data ;
      private String[] T00393_A1176Feriado_Nome ;
      private bool[] T00393_A1195Feriado_Fixo ;
      private bool[] T00393_n1195Feriado_Fixo ;
      private DateTime[] T00396_A1175Feriado_Data ;
      private DateTime[] T00397_A1175Feriado_Data ;
      private DateTime[] T00392_A1175Feriado_Data ;
      private String[] T00392_A1176Feriado_Nome ;
      private bool[] T00392_A1195Feriado_Fixo ;
      private bool[] T00392_n1195Feriado_Fixo ;
      private DateTime[] T003911_A1175Feriado_Data ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
   }

   public class feriados__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new UpdateCursor(def[6])
         ,new UpdateCursor(def[7])
         ,new UpdateCursor(def[8])
         ,new ForEachCursor(def[9])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT00394 ;
          prmT00394 = new Object[] {
          new Object[] {"@Feriado_Data",SqlDbType.DateTime,8,0}
          } ;
          Object[] prmT00395 ;
          prmT00395 = new Object[] {
          new Object[] {"@Feriado_Data",SqlDbType.DateTime,8,0}
          } ;
          Object[] prmT00393 ;
          prmT00393 = new Object[] {
          new Object[] {"@Feriado_Data",SqlDbType.DateTime,8,0}
          } ;
          Object[] prmT00396 ;
          prmT00396 = new Object[] {
          new Object[] {"@Feriado_Data",SqlDbType.DateTime,8,0}
          } ;
          Object[] prmT00397 ;
          prmT00397 = new Object[] {
          new Object[] {"@Feriado_Data",SqlDbType.DateTime,8,0}
          } ;
          Object[] prmT00392 ;
          prmT00392 = new Object[] {
          new Object[] {"@Feriado_Data",SqlDbType.DateTime,8,0}
          } ;
          Object[] prmT00398 ;
          prmT00398 = new Object[] {
          new Object[] {"@Feriado_Data",SqlDbType.DateTime,8,0} ,
          new Object[] {"@Feriado_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@Feriado_Fixo",SqlDbType.Bit,4,0}
          } ;
          Object[] prmT00399 ;
          prmT00399 = new Object[] {
          new Object[] {"@Feriado_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@Feriado_Fixo",SqlDbType.Bit,4,0} ,
          new Object[] {"@Feriado_Data",SqlDbType.DateTime,8,0}
          } ;
          Object[] prmT003910 ;
          prmT003910 = new Object[] {
          new Object[] {"@Feriado_Data",SqlDbType.DateTime,8,0}
          } ;
          Object[] prmT003911 ;
          prmT003911 = new Object[] {
          } ;
          def= new CursorDef[] {
              new CursorDef("T00392", "SELECT [Feriado_Data], [Feriado_Nome], [Feriado_Fixo] FROM [Feriados] WITH (UPDLOCK) WHERE [Feriado_Data] = @Feriado_Data ",true, GxErrorMask.GX_NOMASK, false, this,prmT00392,1,0,true,false )
             ,new CursorDef("T00393", "SELECT [Feriado_Data], [Feriado_Nome], [Feriado_Fixo] FROM [Feriados] WITH (NOLOCK) WHERE [Feriado_Data] = @Feriado_Data ",true, GxErrorMask.GX_NOMASK, false, this,prmT00393,1,0,true,false )
             ,new CursorDef("T00394", "SELECT TM1.[Feriado_Data], TM1.[Feriado_Nome], TM1.[Feriado_Fixo] FROM [Feriados] TM1 WITH (NOLOCK) WHERE TM1.[Feriado_Data] = @Feriado_Data ORDER BY TM1.[Feriado_Data]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT00394,100,0,true,false )
             ,new CursorDef("T00395", "SELECT [Feriado_Data] FROM [Feriados] WITH (NOLOCK) WHERE [Feriado_Data] = @Feriado_Data  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT00395,1,0,true,false )
             ,new CursorDef("T00396", "SELECT TOP 1 [Feriado_Data] FROM [Feriados] WITH (NOLOCK) WHERE ( [Feriado_Data] > @Feriado_Data) ORDER BY [Feriado_Data]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT00396,1,0,true,true )
             ,new CursorDef("T00397", "SELECT TOP 1 [Feriado_Data] FROM [Feriados] WITH (NOLOCK) WHERE ( [Feriado_Data] < @Feriado_Data) ORDER BY [Feriado_Data] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT00397,1,0,true,true )
             ,new CursorDef("T00398", "INSERT INTO [Feriados]([Feriado_Data], [Feriado_Nome], [Feriado_Fixo]) VALUES(@Feriado_Data, @Feriado_Nome, @Feriado_Fixo)", GxErrorMask.GX_NOMASK,prmT00398)
             ,new CursorDef("T00399", "UPDATE [Feriados] SET [Feriado_Nome]=@Feriado_Nome, [Feriado_Fixo]=@Feriado_Fixo  WHERE [Feriado_Data] = @Feriado_Data", GxErrorMask.GX_NOMASK,prmT00399)
             ,new CursorDef("T003910", "DELETE FROM [Feriados]  WHERE [Feriado_Data] = @Feriado_Data", GxErrorMask.GX_NOMASK,prmT003910)
             ,new CursorDef("T003911", "SELECT [Feriado_Data] FROM [Feriados] WITH (NOLOCK) ORDER BY [Feriado_Data]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT003911,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((DateTime[]) buf[0])[0] = rslt.getGXDate(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 1 :
                ((DateTime[]) buf[0])[0] = rslt.getGXDate(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 2 :
                ((DateTime[]) buf[0])[0] = rslt.getGXDate(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 3 :
                ((DateTime[]) buf[0])[0] = rslt.getGXDate(1) ;
                return;
             case 4 :
                ((DateTime[]) buf[0])[0] = rslt.getGXDate(1) ;
                return;
             case 5 :
                ((DateTime[]) buf[0])[0] = rslt.getGXDate(1) ;
                return;
             case 9 :
                ((DateTime[]) buf[0])[0] = rslt.getGXDate(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (DateTime)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (DateTime)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (DateTime)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (DateTime)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (DateTime)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (DateTime)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (DateTime)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 3 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(3, (bool)parms[3]);
                }
                return;
             case 7 :
                stmt.SetParameter(1, (String)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(2, (bool)parms[2]);
                }
                stmt.SetParameter(3, (DateTime)parms[3]);
                return;
             case 8 :
                stmt.SetParameter(1, (DateTime)parms[0]);
                return;
       }
    }

 }

}
