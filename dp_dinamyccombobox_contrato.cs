/*
               File: DP_DinamycCombobox_Contrato
        Description: DP_Dinamyc Combobox_Contrato
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/29/2020 0:4:56.72
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class dp_dinamyccombobox_contrato : GXProcedure
   {
      public dp_dinamyccombobox_contrato( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public dp_dinamyccombobox_contrato( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Contrato_AreaTrabalhoCod ,
                           out IGxCollection aP1_Gxm2rootcol )
      {
         this.AV7Contrato_AreaTrabalhoCod = aP0_Contrato_AreaTrabalhoCod;
         this.Gxm2rootcol = new GxObjectCollection( context, "SDT_Codigos", "GxEv3Up14_MeetrikaVs3", "SdtSDT_Codigos", "GeneXus.Programs") ;
         initialize();
         executePrivate();
         aP1_Gxm2rootcol=this.Gxm2rootcol;
      }

      public IGxCollection executeUdp( int aP0_Contrato_AreaTrabalhoCod )
      {
         this.AV7Contrato_AreaTrabalhoCod = aP0_Contrato_AreaTrabalhoCod;
         this.Gxm2rootcol = new GxObjectCollection( context, "SDT_Codigos", "GxEv3Up14_MeetrikaVs3", "SdtSDT_Codigos", "GeneXus.Programs") ;
         initialize();
         executePrivate();
         aP1_Gxm2rootcol=this.Gxm2rootcol;
         return Gxm2rootcol ;
      }

      public void executeSubmit( int aP0_Contrato_AreaTrabalhoCod ,
                                 out IGxCollection aP1_Gxm2rootcol )
      {
         dp_dinamyccombobox_contrato objdp_dinamyccombobox_contrato;
         objdp_dinamyccombobox_contrato = new dp_dinamyccombobox_contrato();
         objdp_dinamyccombobox_contrato.AV7Contrato_AreaTrabalhoCod = aP0_Contrato_AreaTrabalhoCod;
         objdp_dinamyccombobox_contrato.Gxm2rootcol = new GxObjectCollection( context, "SDT_Codigos", "GxEv3Up14_MeetrikaVs3", "SdtSDT_Codigos", "GeneXus.Programs") ;
         objdp_dinamyccombobox_contrato.context.SetSubmitInitialConfig(context);
         objdp_dinamyccombobox_contrato.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objdp_dinamyccombobox_contrato);
         aP1_Gxm2rootcol=this.Gxm2rootcol;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((dp_dinamyccombobox_contrato)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P000Q2 */
         pr_default.execute(0, new Object[] {AV7Contrato_AreaTrabalhoCod});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A39Contratada_Codigo = P000Q2_A39Contratada_Codigo[0];
            A40Contratada_PessoaCod = P000Q2_A40Contratada_PessoaCod[0];
            A75Contrato_AreaTrabalhoCod = P000Q2_A75Contrato_AreaTrabalhoCod[0];
            A74Contrato_Codigo = P000Q2_A74Contrato_Codigo[0];
            A92Contrato_Ativo = P000Q2_A92Contrato_Ativo[0];
            A41Contratada_PessoaNom = P000Q2_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P000Q2_n41Contratada_PessoaNom[0];
            A77Contrato_Numero = P000Q2_A77Contrato_Numero[0];
            A40Contratada_PessoaCod = P000Q2_A40Contratada_PessoaCod[0];
            A41Contratada_PessoaNom = P000Q2_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P000Q2_n41Contratada_PessoaNom[0];
            Gxm1sdt_codigos = new SdtSDT_Codigos(context);
            Gxm2rootcol.Add(Gxm1sdt_codigos, 0);
            Gxm1sdt_codigos.gxTpr_Codigo = A74Contrato_Codigo;
            Gxm1sdt_codigos.gxTpr_Descricao = StringUtil.Format( "%1 - %2 %3", A77Contrato_Numero, A41Contratada_PessoaNom, (A92Contrato_Ativo ? "(Vigente)" : "(Encerrado)"), "", "", "", "", "", "");
            pr_default.readNext(0);
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P000Q2_A39Contratada_Codigo = new int[1] ;
         P000Q2_A40Contratada_PessoaCod = new int[1] ;
         P000Q2_A75Contrato_AreaTrabalhoCod = new int[1] ;
         P000Q2_A74Contrato_Codigo = new int[1] ;
         P000Q2_A92Contrato_Ativo = new bool[] {false} ;
         P000Q2_A41Contratada_PessoaNom = new String[] {""} ;
         P000Q2_n41Contratada_PessoaNom = new bool[] {false} ;
         P000Q2_A77Contrato_Numero = new String[] {""} ;
         A41Contratada_PessoaNom = "";
         A77Contrato_Numero = "";
         Gxm1sdt_codigos = new SdtSDT_Codigos(context);
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.dp_dinamyccombobox_contrato__default(),
            new Object[][] {
                new Object[] {
               P000Q2_A39Contratada_Codigo, P000Q2_A40Contratada_PessoaCod, P000Q2_A75Contrato_AreaTrabalhoCod, P000Q2_A74Contrato_Codigo, P000Q2_A92Contrato_Ativo, P000Q2_A41Contratada_PessoaNom, P000Q2_n41Contratada_PessoaNom, P000Q2_A77Contrato_Numero
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV7Contrato_AreaTrabalhoCod ;
      private int A39Contratada_Codigo ;
      private int A40Contratada_PessoaCod ;
      private int A75Contrato_AreaTrabalhoCod ;
      private int A74Contrato_Codigo ;
      private String scmdbuf ;
      private String A41Contratada_PessoaNom ;
      private String A77Contrato_Numero ;
      private bool A92Contrato_Ativo ;
      private bool n41Contratada_PessoaNom ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P000Q2_A39Contratada_Codigo ;
      private int[] P000Q2_A40Contratada_PessoaCod ;
      private int[] P000Q2_A75Contrato_AreaTrabalhoCod ;
      private int[] P000Q2_A74Contrato_Codigo ;
      private bool[] P000Q2_A92Contrato_Ativo ;
      private String[] P000Q2_A41Contratada_PessoaNom ;
      private bool[] P000Q2_n41Contratada_PessoaNom ;
      private String[] P000Q2_A77Contrato_Numero ;
      private IGxCollection aP1_Gxm2rootcol ;
      [ObjectCollection(ItemType=typeof( SdtSDT_Codigos ))]
      private IGxCollection Gxm2rootcol ;
      private SdtSDT_Codigos Gxm1sdt_codigos ;
   }

   public class dp_dinamyccombobox_contrato__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP000Q2 ;
          prmP000Q2 = new Object[] {
          new Object[] {"@AV7Contrato_AreaTrabalhoCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P000Q2", "SELECT T1.[Contratada_Codigo], T2.[Contratada_PessoaCod] AS Contratada_PessoaCod, T1.[Contrato_AreaTrabalhoCod], T1.[Contrato_Codigo], T1.[Contrato_Ativo], T3.[Pessoa_Nome] AS Contratada_PessoaNom, T1.[Contrato_Numero] FROM (([Contrato] T1 WITH (NOLOCK) INNER JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[Contratada_Codigo]) INNER JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Contratada_PessoaCod]) WHERE T1.[Contrato_AreaTrabalhoCod] = @AV7Contrato_AreaTrabalhoCod ORDER BY T1.[Contrato_AreaTrabalhoCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP000Q2,100,0,false,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((bool[]) buf[4])[0] = rslt.getBool(5) ;
                ((String[]) buf[5])[0] = rslt.getString(6, 100) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(6);
                ((String[]) buf[7])[0] = rslt.getString(7, 20) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
