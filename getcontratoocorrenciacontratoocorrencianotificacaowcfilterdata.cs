/*
               File: GetContratoOcorrenciaContratoOcorrenciaNotificacaoWCFilterData
        Description: Get Contrato Ocorrencia Contrato Ocorrencia Notificacao WCFilter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/29/2020 2:22:46.52
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getcontratoocorrenciacontratoocorrencianotificacaowcfilterdata : GXProcedure
   {
      public getcontratoocorrenciacontratoocorrencianotificacaowcfilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getcontratoocorrenciacontratoocorrencianotificacaowcfilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV28DDOName = aP0_DDOName;
         this.AV26SearchTxt = aP1_SearchTxt;
         this.AV27SearchTxtTo = aP2_SearchTxtTo;
         this.AV32OptionsJson = "" ;
         this.AV35OptionsDescJson = "" ;
         this.AV37OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV32OptionsJson;
         aP4_OptionsDescJson=this.AV35OptionsDescJson;
         aP5_OptionIndexesJson=this.AV37OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV28DDOName = aP0_DDOName;
         this.AV26SearchTxt = aP1_SearchTxt;
         this.AV27SearchTxtTo = aP2_SearchTxtTo;
         this.AV32OptionsJson = "" ;
         this.AV35OptionsDescJson = "" ;
         this.AV37OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV32OptionsJson;
         aP4_OptionsDescJson=this.AV35OptionsDescJson;
         aP5_OptionIndexesJson=this.AV37OptionIndexesJson;
         return AV37OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getcontratoocorrenciacontratoocorrencianotificacaowcfilterdata objgetcontratoocorrenciacontratoocorrencianotificacaowcfilterdata;
         objgetcontratoocorrenciacontratoocorrencianotificacaowcfilterdata = new getcontratoocorrenciacontratoocorrencianotificacaowcfilterdata();
         objgetcontratoocorrenciacontratoocorrencianotificacaowcfilterdata.AV28DDOName = aP0_DDOName;
         objgetcontratoocorrenciacontratoocorrencianotificacaowcfilterdata.AV26SearchTxt = aP1_SearchTxt;
         objgetcontratoocorrenciacontratoocorrencianotificacaowcfilterdata.AV27SearchTxtTo = aP2_SearchTxtTo;
         objgetcontratoocorrenciacontratoocorrencianotificacaowcfilterdata.AV32OptionsJson = "" ;
         objgetcontratoocorrenciacontratoocorrencianotificacaowcfilterdata.AV35OptionsDescJson = "" ;
         objgetcontratoocorrenciacontratoocorrencianotificacaowcfilterdata.AV37OptionIndexesJson = "" ;
         objgetcontratoocorrenciacontratoocorrencianotificacaowcfilterdata.context.SetSubmitInitialConfig(context);
         objgetcontratoocorrenciacontratoocorrencianotificacaowcfilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetcontratoocorrenciacontratoocorrencianotificacaowcfilterdata);
         aP3_OptionsJson=this.AV32OptionsJson;
         aP4_OptionsDescJson=this.AV35OptionsDescJson;
         aP5_OptionIndexesJson=this.AV37OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getcontratoocorrenciacontratoocorrencianotificacaowcfilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV31Options = (IGxCollection)(new GxSimpleCollection());
         AV34OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV36OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV28DDOName), "DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATOOCORRENCIANOTIFICACAO_DESCRICAOOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV28DDOName), "DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLOOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV28DDOName), "DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATOOCORRENCIANOTIFICACAO_NOMEOPTIONS' */
            S141 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV28DDOName), "DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATOOCORRENCIANOTIFICACAO_DOCTOOPTIONS' */
            S151 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV32OptionsJson = AV31Options.ToJSonString(false);
         AV35OptionsDescJson = AV34OptionsDesc.ToJSonString(false);
         AV37OptionIndexesJson = AV36OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV39Session.Get("ContratoOcorrenciaContratoOcorrenciaNotificacaoWCGridState"), "") == 0 )
         {
            AV41GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "ContratoOcorrenciaContratoOcorrenciaNotificacaoWCGridState"), "");
         }
         else
         {
            AV41GridState.FromXml(AV39Session.Get("ContratoOcorrenciaContratoOcorrenciaNotificacaoWCGridState"), "");
         }
         AV58GXV1 = 1;
         while ( AV58GXV1 <= AV41GridState.gxTpr_Filtervalues.Count )
         {
            AV42GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV41GridState.gxTpr_Filtervalues.Item(AV58GXV1));
            if ( StringUtil.StrCmp(AV42GridStateFilterValue.gxTpr_Name, "TFCONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 )
            {
               AV10TFContratoOcorrenciaNotificacao_Data = context.localUtil.CToD( AV42GridStateFilterValue.gxTpr_Value, 2);
               AV11TFContratoOcorrenciaNotificacao_Data_To = context.localUtil.CToD( AV42GridStateFilterValue.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV42GridStateFilterValue.gxTpr_Name, "TFCONTRATOOCORRENCIANOTIFICACAO_PRAZO") == 0 )
            {
               AV12TFContratoOcorrenciaNotificacao_Prazo = (short)(NumberUtil.Val( AV42GridStateFilterValue.gxTpr_Value, "."));
               AV13TFContratoOcorrenciaNotificacao_Prazo_To = (short)(NumberUtil.Val( AV42GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV42GridStateFilterValue.gxTpr_Name, "TFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO") == 0 )
            {
               AV14TFContratoOcorrenciaNotificacao_Descricao = AV42GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV42GridStateFilterValue.gxTpr_Name, "TFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_SEL") == 0 )
            {
               AV15TFContratoOcorrenciaNotificacao_Descricao_Sel = AV42GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV42GridStateFilterValue.gxTpr_Name, "TFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO") == 0 )
            {
               AV16TFContratoOcorrenciaNotificacao_Cumprido = context.localUtil.CToD( AV42GridStateFilterValue.gxTpr_Value, 2);
               AV17TFContratoOcorrenciaNotificacao_Cumprido_To = context.localUtil.CToD( AV42GridStateFilterValue.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV42GridStateFilterValue.gxTpr_Name, "TFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO") == 0 )
            {
               AV18TFContratoOcorrenciaNotificacao_Protocolo = AV42GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV42GridStateFilterValue.gxTpr_Name, "TFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_SEL") == 0 )
            {
               AV19TFContratoOcorrenciaNotificacao_Protocolo_Sel = AV42GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV42GridStateFilterValue.gxTpr_Name, "TFCONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL") == 0 )
            {
               AV20TFContratoOcorrenciaNotificacao_Responsavel = (int)(NumberUtil.Val( AV42GridStateFilterValue.gxTpr_Value, "."));
               AV21TFContratoOcorrenciaNotificacao_Responsavel_To = (int)(NumberUtil.Val( AV42GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV42GridStateFilterValue.gxTpr_Name, "TFCONTRATOOCORRENCIANOTIFICACAO_NOME") == 0 )
            {
               AV22TFContratoOcorrenciaNotificacao_Nome = AV42GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV42GridStateFilterValue.gxTpr_Name, "TFCONTRATOOCORRENCIANOTIFICACAO_NOME_SEL") == 0 )
            {
               AV23TFContratoOcorrenciaNotificacao_Nome_Sel = AV42GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV42GridStateFilterValue.gxTpr_Name, "TFCONTRATOOCORRENCIANOTIFICACAO_DOCTO") == 0 )
            {
               AV24TFContratoOcorrenciaNotificacao_Docto = AV42GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV42GridStateFilterValue.gxTpr_Name, "TFCONTRATOOCORRENCIANOTIFICACAO_DOCTO_SEL") == 0 )
            {
               AV25TFContratoOcorrenciaNotificacao_Docto_Sel = AV42GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV42GridStateFilterValue.gxTpr_Name, "PARM_&CONTRATOOCORRENCIA_CODIGO") == 0 )
            {
               AV55ContratoOcorrencia_Codigo = (int)(NumberUtil.Val( AV42GridStateFilterValue.gxTpr_Value, "."));
            }
            AV58GXV1 = (int)(AV58GXV1+1);
         }
         if ( AV41GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV43GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV41GridState.gxTpr_Dynamicfilters.Item(1));
            AV44DynamicFiltersSelector1 = AV43GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV44DynamicFiltersSelector1, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 )
            {
               AV45ContratoOcorrenciaNotificacao_Data1 = context.localUtil.CToD( AV43GridStateDynamicFilter.gxTpr_Value, 2);
               AV46ContratoOcorrenciaNotificacao_Data_To1 = context.localUtil.CToD( AV43GridStateDynamicFilter.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV44DynamicFiltersSelector1, "CONTRATOOCORRENCIANOTIFICACAO_PRAZO") == 0 )
            {
               AV47ContratoOcorrenciaNotificacao_Prazo1 = (short)(NumberUtil.Val( AV43GridStateDynamicFilter.gxTpr_Value, "."));
               AV48ContratoOcorrenciaNotificacao_Prazo_To1 = (short)(NumberUtil.Val( AV43GridStateDynamicFilter.gxTpr_Valueto, "."));
            }
            if ( AV41GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV49DynamicFiltersEnabled2 = true;
               AV43GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV41GridState.gxTpr_Dynamicfilters.Item(2));
               AV50DynamicFiltersSelector2 = AV43GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV50DynamicFiltersSelector2, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 )
               {
                  AV51ContratoOcorrenciaNotificacao_Data2 = context.localUtil.CToD( AV43GridStateDynamicFilter.gxTpr_Value, 2);
                  AV52ContratoOcorrenciaNotificacao_Data_To2 = context.localUtil.CToD( AV43GridStateDynamicFilter.gxTpr_Valueto, 2);
               }
               else if ( StringUtil.StrCmp(AV50DynamicFiltersSelector2, "CONTRATOOCORRENCIANOTIFICACAO_PRAZO") == 0 )
               {
                  AV53ContratoOcorrenciaNotificacao_Prazo2 = (short)(NumberUtil.Val( AV43GridStateDynamicFilter.gxTpr_Value, "."));
                  AV54ContratoOcorrenciaNotificacao_Prazo_To2 = (short)(NumberUtil.Val( AV43GridStateDynamicFilter.gxTpr_Valueto, "."));
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADCONTRATOOCORRENCIANOTIFICACAO_DESCRICAOOPTIONS' Routine */
         AV14TFContratoOcorrenciaNotificacao_Descricao = AV26SearchTxt;
         AV15TFContratoOcorrenciaNotificacao_Descricao_Sel = "";
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV44DynamicFiltersSelector1 ,
                                              AV45ContratoOcorrenciaNotificacao_Data1 ,
                                              AV46ContratoOcorrenciaNotificacao_Data_To1 ,
                                              AV47ContratoOcorrenciaNotificacao_Prazo1 ,
                                              AV48ContratoOcorrenciaNotificacao_Prazo_To1 ,
                                              AV49DynamicFiltersEnabled2 ,
                                              AV50DynamicFiltersSelector2 ,
                                              AV51ContratoOcorrenciaNotificacao_Data2 ,
                                              AV52ContratoOcorrenciaNotificacao_Data_To2 ,
                                              AV53ContratoOcorrenciaNotificacao_Prazo2 ,
                                              AV54ContratoOcorrenciaNotificacao_Prazo_To2 ,
                                              AV10TFContratoOcorrenciaNotificacao_Data ,
                                              AV11TFContratoOcorrenciaNotificacao_Data_To ,
                                              AV12TFContratoOcorrenciaNotificacao_Prazo ,
                                              AV13TFContratoOcorrenciaNotificacao_Prazo_To ,
                                              AV15TFContratoOcorrenciaNotificacao_Descricao_Sel ,
                                              AV14TFContratoOcorrenciaNotificacao_Descricao ,
                                              AV16TFContratoOcorrenciaNotificacao_Cumprido ,
                                              AV17TFContratoOcorrenciaNotificacao_Cumprido_To ,
                                              AV19TFContratoOcorrenciaNotificacao_Protocolo_Sel ,
                                              AV18TFContratoOcorrenciaNotificacao_Protocolo ,
                                              AV20TFContratoOcorrenciaNotificacao_Responsavel ,
                                              AV21TFContratoOcorrenciaNotificacao_Responsavel_To ,
                                              AV23TFContratoOcorrenciaNotificacao_Nome_Sel ,
                                              AV22TFContratoOcorrenciaNotificacao_Nome ,
                                              AV25TFContratoOcorrenciaNotificacao_Docto_Sel ,
                                              AV24TFContratoOcorrenciaNotificacao_Docto ,
                                              A298ContratoOcorrenciaNotificacao_Data ,
                                              A299ContratoOcorrenciaNotificacao_Prazo ,
                                              A300ContratoOcorrenciaNotificacao_Descricao ,
                                              A301ContratoOcorrenciaNotificacao_Cumprido ,
                                              A302ContratoOcorrenciaNotificacao_Protocolo ,
                                              A303ContratoOcorrenciaNotificacao_Responsavel ,
                                              A304ContratoOcorrenciaNotificacao_Nome ,
                                              A306ContratoOcorrenciaNotificacao_Docto ,
                                              AV55ContratoOcorrencia_Codigo ,
                                              A294ContratoOcorrencia_Codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT,
                                              TypeConstants.SHORT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.STRING,
                                              TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT,
                                              TypeConstants.INT
                                              }
         });
         lV14TFContratoOcorrenciaNotificacao_Descricao = StringUtil.Concat( StringUtil.RTrim( AV14TFContratoOcorrenciaNotificacao_Descricao), "%", "");
         lV18TFContratoOcorrenciaNotificacao_Protocolo = StringUtil.Concat( StringUtil.RTrim( AV18TFContratoOcorrenciaNotificacao_Protocolo), "%", "");
         lV22TFContratoOcorrenciaNotificacao_Nome = StringUtil.PadR( StringUtil.RTrim( AV22TFContratoOcorrenciaNotificacao_Nome), 100, "%");
         lV24TFContratoOcorrenciaNotificacao_Docto = StringUtil.Concat( StringUtil.RTrim( AV24TFContratoOcorrenciaNotificacao_Docto), "%", "");
         /* Using cursor P00K62 */
         pr_default.execute(0, new Object[] {AV55ContratoOcorrencia_Codigo, AV45ContratoOcorrenciaNotificacao_Data1, AV46ContratoOcorrenciaNotificacao_Data_To1, AV47ContratoOcorrenciaNotificacao_Prazo1, AV48ContratoOcorrenciaNotificacao_Prazo_To1, AV51ContratoOcorrenciaNotificacao_Data2, AV52ContratoOcorrenciaNotificacao_Data_To2, AV53ContratoOcorrenciaNotificacao_Prazo2, AV54ContratoOcorrenciaNotificacao_Prazo_To2, AV10TFContratoOcorrenciaNotificacao_Data, AV11TFContratoOcorrenciaNotificacao_Data_To, AV12TFContratoOcorrenciaNotificacao_Prazo, AV13TFContratoOcorrenciaNotificacao_Prazo_To, lV14TFContratoOcorrenciaNotificacao_Descricao, AV15TFContratoOcorrenciaNotificacao_Descricao_Sel, AV16TFContratoOcorrenciaNotificacao_Cumprido, AV17TFContratoOcorrenciaNotificacao_Cumprido_To, lV18TFContratoOcorrenciaNotificacao_Protocolo, AV19TFContratoOcorrenciaNotificacao_Protocolo_Sel, AV20TFContratoOcorrenciaNotificacao_Responsavel, AV21TFContratoOcorrenciaNotificacao_Responsavel_To, lV22TFContratoOcorrenciaNotificacao_Nome, AV23TFContratoOcorrenciaNotificacao_Nome_Sel, lV24TFContratoOcorrenciaNotificacao_Docto, AV25TFContratoOcorrenciaNotificacao_Docto_Sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKK62 = false;
            A294ContratoOcorrencia_Codigo = P00K62_A294ContratoOcorrencia_Codigo[0];
            A300ContratoOcorrenciaNotificacao_Descricao = P00K62_A300ContratoOcorrenciaNotificacao_Descricao[0];
            A306ContratoOcorrenciaNotificacao_Docto = P00K62_A306ContratoOcorrenciaNotificacao_Docto[0];
            n306ContratoOcorrenciaNotificacao_Docto = P00K62_n306ContratoOcorrenciaNotificacao_Docto[0];
            A304ContratoOcorrenciaNotificacao_Nome = P00K62_A304ContratoOcorrenciaNotificacao_Nome[0];
            n304ContratoOcorrenciaNotificacao_Nome = P00K62_n304ContratoOcorrenciaNotificacao_Nome[0];
            A303ContratoOcorrenciaNotificacao_Responsavel = P00K62_A303ContratoOcorrenciaNotificacao_Responsavel[0];
            A302ContratoOcorrenciaNotificacao_Protocolo = P00K62_A302ContratoOcorrenciaNotificacao_Protocolo[0];
            n302ContratoOcorrenciaNotificacao_Protocolo = P00K62_n302ContratoOcorrenciaNotificacao_Protocolo[0];
            A301ContratoOcorrenciaNotificacao_Cumprido = P00K62_A301ContratoOcorrenciaNotificacao_Cumprido[0];
            n301ContratoOcorrenciaNotificacao_Cumprido = P00K62_n301ContratoOcorrenciaNotificacao_Cumprido[0];
            A299ContratoOcorrenciaNotificacao_Prazo = P00K62_A299ContratoOcorrenciaNotificacao_Prazo[0];
            A298ContratoOcorrenciaNotificacao_Data = P00K62_A298ContratoOcorrenciaNotificacao_Data[0];
            A297ContratoOcorrenciaNotificacao_Codigo = P00K62_A297ContratoOcorrenciaNotificacao_Codigo[0];
            A306ContratoOcorrenciaNotificacao_Docto = P00K62_A306ContratoOcorrenciaNotificacao_Docto[0];
            n306ContratoOcorrenciaNotificacao_Docto = P00K62_n306ContratoOcorrenciaNotificacao_Docto[0];
            A304ContratoOcorrenciaNotificacao_Nome = P00K62_A304ContratoOcorrenciaNotificacao_Nome[0];
            n304ContratoOcorrenciaNotificacao_Nome = P00K62_n304ContratoOcorrenciaNotificacao_Nome[0];
            AV38count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( P00K62_A294ContratoOcorrencia_Codigo[0] == A294ContratoOcorrencia_Codigo ) && ( StringUtil.StrCmp(P00K62_A300ContratoOcorrenciaNotificacao_Descricao[0], A300ContratoOcorrenciaNotificacao_Descricao) == 0 ) )
            {
               BRKK62 = false;
               A297ContratoOcorrenciaNotificacao_Codigo = P00K62_A297ContratoOcorrenciaNotificacao_Codigo[0];
               AV38count = (long)(AV38count+1);
               BRKK62 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A300ContratoOcorrenciaNotificacao_Descricao)) )
            {
               AV30Option = A300ContratoOcorrenciaNotificacao_Descricao;
               AV31Options.Add(AV30Option, 0);
               AV36OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV38count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV31Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKK62 )
            {
               BRKK62 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLOOPTIONS' Routine */
         AV18TFContratoOcorrenciaNotificacao_Protocolo = AV26SearchTxt;
         AV19TFContratoOcorrenciaNotificacao_Protocolo_Sel = "";
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV44DynamicFiltersSelector1 ,
                                              AV45ContratoOcorrenciaNotificacao_Data1 ,
                                              AV46ContratoOcorrenciaNotificacao_Data_To1 ,
                                              AV47ContratoOcorrenciaNotificacao_Prazo1 ,
                                              AV48ContratoOcorrenciaNotificacao_Prazo_To1 ,
                                              AV49DynamicFiltersEnabled2 ,
                                              AV50DynamicFiltersSelector2 ,
                                              AV51ContratoOcorrenciaNotificacao_Data2 ,
                                              AV52ContratoOcorrenciaNotificacao_Data_To2 ,
                                              AV53ContratoOcorrenciaNotificacao_Prazo2 ,
                                              AV54ContratoOcorrenciaNotificacao_Prazo_To2 ,
                                              AV10TFContratoOcorrenciaNotificacao_Data ,
                                              AV11TFContratoOcorrenciaNotificacao_Data_To ,
                                              AV12TFContratoOcorrenciaNotificacao_Prazo ,
                                              AV13TFContratoOcorrenciaNotificacao_Prazo_To ,
                                              AV15TFContratoOcorrenciaNotificacao_Descricao_Sel ,
                                              AV14TFContratoOcorrenciaNotificacao_Descricao ,
                                              AV16TFContratoOcorrenciaNotificacao_Cumprido ,
                                              AV17TFContratoOcorrenciaNotificacao_Cumprido_To ,
                                              AV19TFContratoOcorrenciaNotificacao_Protocolo_Sel ,
                                              AV18TFContratoOcorrenciaNotificacao_Protocolo ,
                                              AV20TFContratoOcorrenciaNotificacao_Responsavel ,
                                              AV21TFContratoOcorrenciaNotificacao_Responsavel_To ,
                                              AV23TFContratoOcorrenciaNotificacao_Nome_Sel ,
                                              AV22TFContratoOcorrenciaNotificacao_Nome ,
                                              AV25TFContratoOcorrenciaNotificacao_Docto_Sel ,
                                              AV24TFContratoOcorrenciaNotificacao_Docto ,
                                              A298ContratoOcorrenciaNotificacao_Data ,
                                              A299ContratoOcorrenciaNotificacao_Prazo ,
                                              A300ContratoOcorrenciaNotificacao_Descricao ,
                                              A301ContratoOcorrenciaNotificacao_Cumprido ,
                                              A302ContratoOcorrenciaNotificacao_Protocolo ,
                                              A303ContratoOcorrenciaNotificacao_Responsavel ,
                                              A304ContratoOcorrenciaNotificacao_Nome ,
                                              A306ContratoOcorrenciaNotificacao_Docto ,
                                              AV55ContratoOcorrencia_Codigo ,
                                              A294ContratoOcorrencia_Codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT,
                                              TypeConstants.SHORT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.STRING,
                                              TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT,
                                              TypeConstants.INT
                                              }
         });
         lV14TFContratoOcorrenciaNotificacao_Descricao = StringUtil.Concat( StringUtil.RTrim( AV14TFContratoOcorrenciaNotificacao_Descricao), "%", "");
         lV18TFContratoOcorrenciaNotificacao_Protocolo = StringUtil.Concat( StringUtil.RTrim( AV18TFContratoOcorrenciaNotificacao_Protocolo), "%", "");
         lV22TFContratoOcorrenciaNotificacao_Nome = StringUtil.PadR( StringUtil.RTrim( AV22TFContratoOcorrenciaNotificacao_Nome), 100, "%");
         lV24TFContratoOcorrenciaNotificacao_Docto = StringUtil.Concat( StringUtil.RTrim( AV24TFContratoOcorrenciaNotificacao_Docto), "%", "");
         /* Using cursor P00K63 */
         pr_default.execute(1, new Object[] {AV55ContratoOcorrencia_Codigo, AV45ContratoOcorrenciaNotificacao_Data1, AV46ContratoOcorrenciaNotificacao_Data_To1, AV47ContratoOcorrenciaNotificacao_Prazo1, AV48ContratoOcorrenciaNotificacao_Prazo_To1, AV51ContratoOcorrenciaNotificacao_Data2, AV52ContratoOcorrenciaNotificacao_Data_To2, AV53ContratoOcorrenciaNotificacao_Prazo2, AV54ContratoOcorrenciaNotificacao_Prazo_To2, AV10TFContratoOcorrenciaNotificacao_Data, AV11TFContratoOcorrenciaNotificacao_Data_To, AV12TFContratoOcorrenciaNotificacao_Prazo, AV13TFContratoOcorrenciaNotificacao_Prazo_To, lV14TFContratoOcorrenciaNotificacao_Descricao, AV15TFContratoOcorrenciaNotificacao_Descricao_Sel, AV16TFContratoOcorrenciaNotificacao_Cumprido, AV17TFContratoOcorrenciaNotificacao_Cumprido_To, lV18TFContratoOcorrenciaNotificacao_Protocolo, AV19TFContratoOcorrenciaNotificacao_Protocolo_Sel, AV20TFContratoOcorrenciaNotificacao_Responsavel, AV21TFContratoOcorrenciaNotificacao_Responsavel_To, lV22TFContratoOcorrenciaNotificacao_Nome, AV23TFContratoOcorrenciaNotificacao_Nome_Sel, lV24TFContratoOcorrenciaNotificacao_Docto, AV25TFContratoOcorrenciaNotificacao_Docto_Sel});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKK64 = false;
            A294ContratoOcorrencia_Codigo = P00K63_A294ContratoOcorrencia_Codigo[0];
            A302ContratoOcorrenciaNotificacao_Protocolo = P00K63_A302ContratoOcorrenciaNotificacao_Protocolo[0];
            n302ContratoOcorrenciaNotificacao_Protocolo = P00K63_n302ContratoOcorrenciaNotificacao_Protocolo[0];
            A306ContratoOcorrenciaNotificacao_Docto = P00K63_A306ContratoOcorrenciaNotificacao_Docto[0];
            n306ContratoOcorrenciaNotificacao_Docto = P00K63_n306ContratoOcorrenciaNotificacao_Docto[0];
            A304ContratoOcorrenciaNotificacao_Nome = P00K63_A304ContratoOcorrenciaNotificacao_Nome[0];
            n304ContratoOcorrenciaNotificacao_Nome = P00K63_n304ContratoOcorrenciaNotificacao_Nome[0];
            A303ContratoOcorrenciaNotificacao_Responsavel = P00K63_A303ContratoOcorrenciaNotificacao_Responsavel[0];
            A301ContratoOcorrenciaNotificacao_Cumprido = P00K63_A301ContratoOcorrenciaNotificacao_Cumprido[0];
            n301ContratoOcorrenciaNotificacao_Cumprido = P00K63_n301ContratoOcorrenciaNotificacao_Cumprido[0];
            A300ContratoOcorrenciaNotificacao_Descricao = P00K63_A300ContratoOcorrenciaNotificacao_Descricao[0];
            A299ContratoOcorrenciaNotificacao_Prazo = P00K63_A299ContratoOcorrenciaNotificacao_Prazo[0];
            A298ContratoOcorrenciaNotificacao_Data = P00K63_A298ContratoOcorrenciaNotificacao_Data[0];
            A297ContratoOcorrenciaNotificacao_Codigo = P00K63_A297ContratoOcorrenciaNotificacao_Codigo[0];
            A306ContratoOcorrenciaNotificacao_Docto = P00K63_A306ContratoOcorrenciaNotificacao_Docto[0];
            n306ContratoOcorrenciaNotificacao_Docto = P00K63_n306ContratoOcorrenciaNotificacao_Docto[0];
            A304ContratoOcorrenciaNotificacao_Nome = P00K63_A304ContratoOcorrenciaNotificacao_Nome[0];
            n304ContratoOcorrenciaNotificacao_Nome = P00K63_n304ContratoOcorrenciaNotificacao_Nome[0];
            AV38count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( P00K63_A294ContratoOcorrencia_Codigo[0] == A294ContratoOcorrencia_Codigo ) && ( StringUtil.StrCmp(P00K63_A302ContratoOcorrenciaNotificacao_Protocolo[0], A302ContratoOcorrenciaNotificacao_Protocolo) == 0 ) )
            {
               BRKK64 = false;
               A297ContratoOcorrenciaNotificacao_Codigo = P00K63_A297ContratoOcorrenciaNotificacao_Codigo[0];
               AV38count = (long)(AV38count+1);
               BRKK64 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A302ContratoOcorrenciaNotificacao_Protocolo)) )
            {
               AV30Option = A302ContratoOcorrenciaNotificacao_Protocolo;
               AV31Options.Add(AV30Option, 0);
               AV36OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV38count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV31Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKK64 )
            {
               BRKK64 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      protected void S141( )
      {
         /* 'LOADCONTRATOOCORRENCIANOTIFICACAO_NOMEOPTIONS' Routine */
         AV22TFContratoOcorrenciaNotificacao_Nome = AV26SearchTxt;
         AV23TFContratoOcorrenciaNotificacao_Nome_Sel = "";
         pr_default.dynParam(2, new Object[]{ new Object[]{
                                              AV44DynamicFiltersSelector1 ,
                                              AV45ContratoOcorrenciaNotificacao_Data1 ,
                                              AV46ContratoOcorrenciaNotificacao_Data_To1 ,
                                              AV47ContratoOcorrenciaNotificacao_Prazo1 ,
                                              AV48ContratoOcorrenciaNotificacao_Prazo_To1 ,
                                              AV49DynamicFiltersEnabled2 ,
                                              AV50DynamicFiltersSelector2 ,
                                              AV51ContratoOcorrenciaNotificacao_Data2 ,
                                              AV52ContratoOcorrenciaNotificacao_Data_To2 ,
                                              AV53ContratoOcorrenciaNotificacao_Prazo2 ,
                                              AV54ContratoOcorrenciaNotificacao_Prazo_To2 ,
                                              AV10TFContratoOcorrenciaNotificacao_Data ,
                                              AV11TFContratoOcorrenciaNotificacao_Data_To ,
                                              AV12TFContratoOcorrenciaNotificacao_Prazo ,
                                              AV13TFContratoOcorrenciaNotificacao_Prazo_To ,
                                              AV15TFContratoOcorrenciaNotificacao_Descricao_Sel ,
                                              AV14TFContratoOcorrenciaNotificacao_Descricao ,
                                              AV16TFContratoOcorrenciaNotificacao_Cumprido ,
                                              AV17TFContratoOcorrenciaNotificacao_Cumprido_To ,
                                              AV19TFContratoOcorrenciaNotificacao_Protocolo_Sel ,
                                              AV18TFContratoOcorrenciaNotificacao_Protocolo ,
                                              AV20TFContratoOcorrenciaNotificacao_Responsavel ,
                                              AV21TFContratoOcorrenciaNotificacao_Responsavel_To ,
                                              AV23TFContratoOcorrenciaNotificacao_Nome_Sel ,
                                              AV22TFContratoOcorrenciaNotificacao_Nome ,
                                              AV25TFContratoOcorrenciaNotificacao_Docto_Sel ,
                                              AV24TFContratoOcorrenciaNotificacao_Docto ,
                                              A298ContratoOcorrenciaNotificacao_Data ,
                                              A299ContratoOcorrenciaNotificacao_Prazo ,
                                              A300ContratoOcorrenciaNotificacao_Descricao ,
                                              A301ContratoOcorrenciaNotificacao_Cumprido ,
                                              A302ContratoOcorrenciaNotificacao_Protocolo ,
                                              A303ContratoOcorrenciaNotificacao_Responsavel ,
                                              A304ContratoOcorrenciaNotificacao_Nome ,
                                              A306ContratoOcorrenciaNotificacao_Docto ,
                                              AV55ContratoOcorrencia_Codigo ,
                                              A294ContratoOcorrencia_Codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT,
                                              TypeConstants.SHORT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.STRING,
                                              TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT,
                                              TypeConstants.INT
                                              }
         });
         lV14TFContratoOcorrenciaNotificacao_Descricao = StringUtil.Concat( StringUtil.RTrim( AV14TFContratoOcorrenciaNotificacao_Descricao), "%", "");
         lV18TFContratoOcorrenciaNotificacao_Protocolo = StringUtil.Concat( StringUtil.RTrim( AV18TFContratoOcorrenciaNotificacao_Protocolo), "%", "");
         lV22TFContratoOcorrenciaNotificacao_Nome = StringUtil.PadR( StringUtil.RTrim( AV22TFContratoOcorrenciaNotificacao_Nome), 100, "%");
         lV24TFContratoOcorrenciaNotificacao_Docto = StringUtil.Concat( StringUtil.RTrim( AV24TFContratoOcorrenciaNotificacao_Docto), "%", "");
         /* Using cursor P00K64 */
         pr_default.execute(2, new Object[] {AV55ContratoOcorrencia_Codigo, AV45ContratoOcorrenciaNotificacao_Data1, AV46ContratoOcorrenciaNotificacao_Data_To1, AV47ContratoOcorrenciaNotificacao_Prazo1, AV48ContratoOcorrenciaNotificacao_Prazo_To1, AV51ContratoOcorrenciaNotificacao_Data2, AV52ContratoOcorrenciaNotificacao_Data_To2, AV53ContratoOcorrenciaNotificacao_Prazo2, AV54ContratoOcorrenciaNotificacao_Prazo_To2, AV10TFContratoOcorrenciaNotificacao_Data, AV11TFContratoOcorrenciaNotificacao_Data_To, AV12TFContratoOcorrenciaNotificacao_Prazo, AV13TFContratoOcorrenciaNotificacao_Prazo_To, lV14TFContratoOcorrenciaNotificacao_Descricao, AV15TFContratoOcorrenciaNotificacao_Descricao_Sel, AV16TFContratoOcorrenciaNotificacao_Cumprido, AV17TFContratoOcorrenciaNotificacao_Cumprido_To, lV18TFContratoOcorrenciaNotificacao_Protocolo, AV19TFContratoOcorrenciaNotificacao_Protocolo_Sel, AV20TFContratoOcorrenciaNotificacao_Responsavel, AV21TFContratoOcorrenciaNotificacao_Responsavel_To, lV22TFContratoOcorrenciaNotificacao_Nome, AV23TFContratoOcorrenciaNotificacao_Nome_Sel, lV24TFContratoOcorrenciaNotificacao_Docto, AV25TFContratoOcorrenciaNotificacao_Docto_Sel});
         while ( (pr_default.getStatus(2) != 101) )
         {
            BRKK66 = false;
            A294ContratoOcorrencia_Codigo = P00K64_A294ContratoOcorrencia_Codigo[0];
            A303ContratoOcorrenciaNotificacao_Responsavel = P00K64_A303ContratoOcorrenciaNotificacao_Responsavel[0];
            A306ContratoOcorrenciaNotificacao_Docto = P00K64_A306ContratoOcorrenciaNotificacao_Docto[0];
            n306ContratoOcorrenciaNotificacao_Docto = P00K64_n306ContratoOcorrenciaNotificacao_Docto[0];
            A304ContratoOcorrenciaNotificacao_Nome = P00K64_A304ContratoOcorrenciaNotificacao_Nome[0];
            n304ContratoOcorrenciaNotificacao_Nome = P00K64_n304ContratoOcorrenciaNotificacao_Nome[0];
            A302ContratoOcorrenciaNotificacao_Protocolo = P00K64_A302ContratoOcorrenciaNotificacao_Protocolo[0];
            n302ContratoOcorrenciaNotificacao_Protocolo = P00K64_n302ContratoOcorrenciaNotificacao_Protocolo[0];
            A301ContratoOcorrenciaNotificacao_Cumprido = P00K64_A301ContratoOcorrenciaNotificacao_Cumprido[0];
            n301ContratoOcorrenciaNotificacao_Cumprido = P00K64_n301ContratoOcorrenciaNotificacao_Cumprido[0];
            A300ContratoOcorrenciaNotificacao_Descricao = P00K64_A300ContratoOcorrenciaNotificacao_Descricao[0];
            A299ContratoOcorrenciaNotificacao_Prazo = P00K64_A299ContratoOcorrenciaNotificacao_Prazo[0];
            A298ContratoOcorrenciaNotificacao_Data = P00K64_A298ContratoOcorrenciaNotificacao_Data[0];
            A297ContratoOcorrenciaNotificacao_Codigo = P00K64_A297ContratoOcorrenciaNotificacao_Codigo[0];
            A306ContratoOcorrenciaNotificacao_Docto = P00K64_A306ContratoOcorrenciaNotificacao_Docto[0];
            n306ContratoOcorrenciaNotificacao_Docto = P00K64_n306ContratoOcorrenciaNotificacao_Docto[0];
            A304ContratoOcorrenciaNotificacao_Nome = P00K64_A304ContratoOcorrenciaNotificacao_Nome[0];
            n304ContratoOcorrenciaNotificacao_Nome = P00K64_n304ContratoOcorrenciaNotificacao_Nome[0];
            AV38count = 0;
            while ( (pr_default.getStatus(2) != 101) && ( P00K64_A294ContratoOcorrencia_Codigo[0] == A294ContratoOcorrencia_Codigo ) && ( P00K64_A303ContratoOcorrenciaNotificacao_Responsavel[0] == A303ContratoOcorrenciaNotificacao_Responsavel ) )
            {
               BRKK66 = false;
               A297ContratoOcorrenciaNotificacao_Codigo = P00K64_A297ContratoOcorrenciaNotificacao_Codigo[0];
               AV38count = (long)(AV38count+1);
               BRKK66 = true;
               pr_default.readNext(2);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A304ContratoOcorrenciaNotificacao_Nome)) )
            {
               AV30Option = A304ContratoOcorrenciaNotificacao_Nome;
               AV29InsertIndex = 1;
               while ( ( AV29InsertIndex <= AV31Options.Count ) && ( StringUtil.StrCmp(((String)AV31Options.Item(AV29InsertIndex)), AV30Option) < 0 ) )
               {
                  AV29InsertIndex = (int)(AV29InsertIndex+1);
               }
               AV31Options.Add(AV30Option, AV29InsertIndex);
               AV36OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV38count), "Z,ZZZ,ZZZ,ZZ9")), AV29InsertIndex);
            }
            if ( AV31Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKK66 )
            {
               BRKK66 = true;
               pr_default.readNext(2);
            }
         }
         pr_default.close(2);
      }

      protected void S151( )
      {
         /* 'LOADCONTRATOOCORRENCIANOTIFICACAO_DOCTOOPTIONS' Routine */
         AV24TFContratoOcorrenciaNotificacao_Docto = AV26SearchTxt;
         AV25TFContratoOcorrenciaNotificacao_Docto_Sel = "";
         pr_default.dynParam(3, new Object[]{ new Object[]{
                                              AV44DynamicFiltersSelector1 ,
                                              AV45ContratoOcorrenciaNotificacao_Data1 ,
                                              AV46ContratoOcorrenciaNotificacao_Data_To1 ,
                                              AV47ContratoOcorrenciaNotificacao_Prazo1 ,
                                              AV48ContratoOcorrenciaNotificacao_Prazo_To1 ,
                                              AV49DynamicFiltersEnabled2 ,
                                              AV50DynamicFiltersSelector2 ,
                                              AV51ContratoOcorrenciaNotificacao_Data2 ,
                                              AV52ContratoOcorrenciaNotificacao_Data_To2 ,
                                              AV53ContratoOcorrenciaNotificacao_Prazo2 ,
                                              AV54ContratoOcorrenciaNotificacao_Prazo_To2 ,
                                              AV10TFContratoOcorrenciaNotificacao_Data ,
                                              AV11TFContratoOcorrenciaNotificacao_Data_To ,
                                              AV12TFContratoOcorrenciaNotificacao_Prazo ,
                                              AV13TFContratoOcorrenciaNotificacao_Prazo_To ,
                                              AV15TFContratoOcorrenciaNotificacao_Descricao_Sel ,
                                              AV14TFContratoOcorrenciaNotificacao_Descricao ,
                                              AV16TFContratoOcorrenciaNotificacao_Cumprido ,
                                              AV17TFContratoOcorrenciaNotificacao_Cumprido_To ,
                                              AV19TFContratoOcorrenciaNotificacao_Protocolo_Sel ,
                                              AV18TFContratoOcorrenciaNotificacao_Protocolo ,
                                              AV20TFContratoOcorrenciaNotificacao_Responsavel ,
                                              AV21TFContratoOcorrenciaNotificacao_Responsavel_To ,
                                              AV23TFContratoOcorrenciaNotificacao_Nome_Sel ,
                                              AV22TFContratoOcorrenciaNotificacao_Nome ,
                                              AV25TFContratoOcorrenciaNotificacao_Docto_Sel ,
                                              AV24TFContratoOcorrenciaNotificacao_Docto ,
                                              A298ContratoOcorrenciaNotificacao_Data ,
                                              A299ContratoOcorrenciaNotificacao_Prazo ,
                                              A300ContratoOcorrenciaNotificacao_Descricao ,
                                              A301ContratoOcorrenciaNotificacao_Cumprido ,
                                              A302ContratoOcorrenciaNotificacao_Protocolo ,
                                              A303ContratoOcorrenciaNotificacao_Responsavel ,
                                              A304ContratoOcorrenciaNotificacao_Nome ,
                                              A306ContratoOcorrenciaNotificacao_Docto ,
                                              AV55ContratoOcorrencia_Codigo ,
                                              A294ContratoOcorrencia_Codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT,
                                              TypeConstants.SHORT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.STRING,
                                              TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT,
                                              TypeConstants.INT
                                              }
         });
         lV14TFContratoOcorrenciaNotificacao_Descricao = StringUtil.Concat( StringUtil.RTrim( AV14TFContratoOcorrenciaNotificacao_Descricao), "%", "");
         lV18TFContratoOcorrenciaNotificacao_Protocolo = StringUtil.Concat( StringUtil.RTrim( AV18TFContratoOcorrenciaNotificacao_Protocolo), "%", "");
         lV22TFContratoOcorrenciaNotificacao_Nome = StringUtil.PadR( StringUtil.RTrim( AV22TFContratoOcorrenciaNotificacao_Nome), 100, "%");
         lV24TFContratoOcorrenciaNotificacao_Docto = StringUtil.Concat( StringUtil.RTrim( AV24TFContratoOcorrenciaNotificacao_Docto), "%", "");
         /* Using cursor P00K65 */
         pr_default.execute(3, new Object[] {AV55ContratoOcorrencia_Codigo, AV45ContratoOcorrenciaNotificacao_Data1, AV46ContratoOcorrenciaNotificacao_Data_To1, AV47ContratoOcorrenciaNotificacao_Prazo1, AV48ContratoOcorrenciaNotificacao_Prazo_To1, AV51ContratoOcorrenciaNotificacao_Data2, AV52ContratoOcorrenciaNotificacao_Data_To2, AV53ContratoOcorrenciaNotificacao_Prazo2, AV54ContratoOcorrenciaNotificacao_Prazo_To2, AV10TFContratoOcorrenciaNotificacao_Data, AV11TFContratoOcorrenciaNotificacao_Data_To, AV12TFContratoOcorrenciaNotificacao_Prazo, AV13TFContratoOcorrenciaNotificacao_Prazo_To, lV14TFContratoOcorrenciaNotificacao_Descricao, AV15TFContratoOcorrenciaNotificacao_Descricao_Sel, AV16TFContratoOcorrenciaNotificacao_Cumprido, AV17TFContratoOcorrenciaNotificacao_Cumprido_To, lV18TFContratoOcorrenciaNotificacao_Protocolo, AV19TFContratoOcorrenciaNotificacao_Protocolo_Sel, AV20TFContratoOcorrenciaNotificacao_Responsavel, AV21TFContratoOcorrenciaNotificacao_Responsavel_To, lV22TFContratoOcorrenciaNotificacao_Nome, AV23TFContratoOcorrenciaNotificacao_Nome_Sel, lV24TFContratoOcorrenciaNotificacao_Docto, AV25TFContratoOcorrenciaNotificacao_Docto_Sel});
         while ( (pr_default.getStatus(3) != 101) )
         {
            BRKK68 = false;
            A294ContratoOcorrencia_Codigo = P00K65_A294ContratoOcorrencia_Codigo[0];
            A306ContratoOcorrenciaNotificacao_Docto = P00K65_A306ContratoOcorrenciaNotificacao_Docto[0];
            n306ContratoOcorrenciaNotificacao_Docto = P00K65_n306ContratoOcorrenciaNotificacao_Docto[0];
            A304ContratoOcorrenciaNotificacao_Nome = P00K65_A304ContratoOcorrenciaNotificacao_Nome[0];
            n304ContratoOcorrenciaNotificacao_Nome = P00K65_n304ContratoOcorrenciaNotificacao_Nome[0];
            A303ContratoOcorrenciaNotificacao_Responsavel = P00K65_A303ContratoOcorrenciaNotificacao_Responsavel[0];
            A302ContratoOcorrenciaNotificacao_Protocolo = P00K65_A302ContratoOcorrenciaNotificacao_Protocolo[0];
            n302ContratoOcorrenciaNotificacao_Protocolo = P00K65_n302ContratoOcorrenciaNotificacao_Protocolo[0];
            A301ContratoOcorrenciaNotificacao_Cumprido = P00K65_A301ContratoOcorrenciaNotificacao_Cumprido[0];
            n301ContratoOcorrenciaNotificacao_Cumprido = P00K65_n301ContratoOcorrenciaNotificacao_Cumprido[0];
            A300ContratoOcorrenciaNotificacao_Descricao = P00K65_A300ContratoOcorrenciaNotificacao_Descricao[0];
            A299ContratoOcorrenciaNotificacao_Prazo = P00K65_A299ContratoOcorrenciaNotificacao_Prazo[0];
            A298ContratoOcorrenciaNotificacao_Data = P00K65_A298ContratoOcorrenciaNotificacao_Data[0];
            A297ContratoOcorrenciaNotificacao_Codigo = P00K65_A297ContratoOcorrenciaNotificacao_Codigo[0];
            A306ContratoOcorrenciaNotificacao_Docto = P00K65_A306ContratoOcorrenciaNotificacao_Docto[0];
            n306ContratoOcorrenciaNotificacao_Docto = P00K65_n306ContratoOcorrenciaNotificacao_Docto[0];
            A304ContratoOcorrenciaNotificacao_Nome = P00K65_A304ContratoOcorrenciaNotificacao_Nome[0];
            n304ContratoOcorrenciaNotificacao_Nome = P00K65_n304ContratoOcorrenciaNotificacao_Nome[0];
            AV38count = 0;
            while ( (pr_default.getStatus(3) != 101) && ( P00K65_A294ContratoOcorrencia_Codigo[0] == A294ContratoOcorrencia_Codigo ) && ( StringUtil.StrCmp(P00K65_A306ContratoOcorrenciaNotificacao_Docto[0], A306ContratoOcorrenciaNotificacao_Docto) == 0 ) )
            {
               BRKK68 = false;
               A303ContratoOcorrenciaNotificacao_Responsavel = P00K65_A303ContratoOcorrenciaNotificacao_Responsavel[0];
               A297ContratoOcorrenciaNotificacao_Codigo = P00K65_A297ContratoOcorrenciaNotificacao_Codigo[0];
               AV38count = (long)(AV38count+1);
               BRKK68 = true;
               pr_default.readNext(3);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A306ContratoOcorrenciaNotificacao_Docto)) )
            {
               AV30Option = A306ContratoOcorrenciaNotificacao_Docto;
               AV31Options.Add(AV30Option, 0);
               AV36OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV38count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV31Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKK68 )
            {
               BRKK68 = true;
               pr_default.readNext(3);
            }
         }
         pr_default.close(3);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV31Options = new GxSimpleCollection();
         AV34OptionsDesc = new GxSimpleCollection();
         AV36OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV39Session = context.GetSession();
         AV41GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV42GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFContratoOcorrenciaNotificacao_Data = DateTime.MinValue;
         AV11TFContratoOcorrenciaNotificacao_Data_To = DateTime.MinValue;
         AV14TFContratoOcorrenciaNotificacao_Descricao = "";
         AV15TFContratoOcorrenciaNotificacao_Descricao_Sel = "";
         AV16TFContratoOcorrenciaNotificacao_Cumprido = DateTime.MinValue;
         AV17TFContratoOcorrenciaNotificacao_Cumprido_To = DateTime.MinValue;
         AV18TFContratoOcorrenciaNotificacao_Protocolo = "";
         AV19TFContratoOcorrenciaNotificacao_Protocolo_Sel = "";
         AV22TFContratoOcorrenciaNotificacao_Nome = "";
         AV23TFContratoOcorrenciaNotificacao_Nome_Sel = "";
         AV24TFContratoOcorrenciaNotificacao_Docto = "";
         AV25TFContratoOcorrenciaNotificacao_Docto_Sel = "";
         AV43GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV44DynamicFiltersSelector1 = "";
         AV45ContratoOcorrenciaNotificacao_Data1 = DateTime.MinValue;
         AV46ContratoOcorrenciaNotificacao_Data_To1 = DateTime.MinValue;
         AV50DynamicFiltersSelector2 = "";
         AV51ContratoOcorrenciaNotificacao_Data2 = DateTime.MinValue;
         AV52ContratoOcorrenciaNotificacao_Data_To2 = DateTime.MinValue;
         scmdbuf = "";
         lV14TFContratoOcorrenciaNotificacao_Descricao = "";
         lV18TFContratoOcorrenciaNotificacao_Protocolo = "";
         lV22TFContratoOcorrenciaNotificacao_Nome = "";
         lV24TFContratoOcorrenciaNotificacao_Docto = "";
         A298ContratoOcorrenciaNotificacao_Data = DateTime.MinValue;
         A300ContratoOcorrenciaNotificacao_Descricao = "";
         A301ContratoOcorrenciaNotificacao_Cumprido = DateTime.MinValue;
         A302ContratoOcorrenciaNotificacao_Protocolo = "";
         A304ContratoOcorrenciaNotificacao_Nome = "";
         A306ContratoOcorrenciaNotificacao_Docto = "";
         P00K62_A294ContratoOcorrencia_Codigo = new int[1] ;
         P00K62_A300ContratoOcorrenciaNotificacao_Descricao = new String[] {""} ;
         P00K62_A306ContratoOcorrenciaNotificacao_Docto = new String[] {""} ;
         P00K62_n306ContratoOcorrenciaNotificacao_Docto = new bool[] {false} ;
         P00K62_A304ContratoOcorrenciaNotificacao_Nome = new String[] {""} ;
         P00K62_n304ContratoOcorrenciaNotificacao_Nome = new bool[] {false} ;
         P00K62_A303ContratoOcorrenciaNotificacao_Responsavel = new int[1] ;
         P00K62_A302ContratoOcorrenciaNotificacao_Protocolo = new String[] {""} ;
         P00K62_n302ContratoOcorrenciaNotificacao_Protocolo = new bool[] {false} ;
         P00K62_A301ContratoOcorrenciaNotificacao_Cumprido = new DateTime[] {DateTime.MinValue} ;
         P00K62_n301ContratoOcorrenciaNotificacao_Cumprido = new bool[] {false} ;
         P00K62_A299ContratoOcorrenciaNotificacao_Prazo = new short[1] ;
         P00K62_A298ContratoOcorrenciaNotificacao_Data = new DateTime[] {DateTime.MinValue} ;
         P00K62_A297ContratoOcorrenciaNotificacao_Codigo = new int[1] ;
         AV30Option = "";
         P00K63_A294ContratoOcorrencia_Codigo = new int[1] ;
         P00K63_A302ContratoOcorrenciaNotificacao_Protocolo = new String[] {""} ;
         P00K63_n302ContratoOcorrenciaNotificacao_Protocolo = new bool[] {false} ;
         P00K63_A306ContratoOcorrenciaNotificacao_Docto = new String[] {""} ;
         P00K63_n306ContratoOcorrenciaNotificacao_Docto = new bool[] {false} ;
         P00K63_A304ContratoOcorrenciaNotificacao_Nome = new String[] {""} ;
         P00K63_n304ContratoOcorrenciaNotificacao_Nome = new bool[] {false} ;
         P00K63_A303ContratoOcorrenciaNotificacao_Responsavel = new int[1] ;
         P00K63_A301ContratoOcorrenciaNotificacao_Cumprido = new DateTime[] {DateTime.MinValue} ;
         P00K63_n301ContratoOcorrenciaNotificacao_Cumprido = new bool[] {false} ;
         P00K63_A300ContratoOcorrenciaNotificacao_Descricao = new String[] {""} ;
         P00K63_A299ContratoOcorrenciaNotificacao_Prazo = new short[1] ;
         P00K63_A298ContratoOcorrenciaNotificacao_Data = new DateTime[] {DateTime.MinValue} ;
         P00K63_A297ContratoOcorrenciaNotificacao_Codigo = new int[1] ;
         P00K64_A294ContratoOcorrencia_Codigo = new int[1] ;
         P00K64_A303ContratoOcorrenciaNotificacao_Responsavel = new int[1] ;
         P00K64_A306ContratoOcorrenciaNotificacao_Docto = new String[] {""} ;
         P00K64_n306ContratoOcorrenciaNotificacao_Docto = new bool[] {false} ;
         P00K64_A304ContratoOcorrenciaNotificacao_Nome = new String[] {""} ;
         P00K64_n304ContratoOcorrenciaNotificacao_Nome = new bool[] {false} ;
         P00K64_A302ContratoOcorrenciaNotificacao_Protocolo = new String[] {""} ;
         P00K64_n302ContratoOcorrenciaNotificacao_Protocolo = new bool[] {false} ;
         P00K64_A301ContratoOcorrenciaNotificacao_Cumprido = new DateTime[] {DateTime.MinValue} ;
         P00K64_n301ContratoOcorrenciaNotificacao_Cumprido = new bool[] {false} ;
         P00K64_A300ContratoOcorrenciaNotificacao_Descricao = new String[] {""} ;
         P00K64_A299ContratoOcorrenciaNotificacao_Prazo = new short[1] ;
         P00K64_A298ContratoOcorrenciaNotificacao_Data = new DateTime[] {DateTime.MinValue} ;
         P00K64_A297ContratoOcorrenciaNotificacao_Codigo = new int[1] ;
         P00K65_A294ContratoOcorrencia_Codigo = new int[1] ;
         P00K65_A306ContratoOcorrenciaNotificacao_Docto = new String[] {""} ;
         P00K65_n306ContratoOcorrenciaNotificacao_Docto = new bool[] {false} ;
         P00K65_A304ContratoOcorrenciaNotificacao_Nome = new String[] {""} ;
         P00K65_n304ContratoOcorrenciaNotificacao_Nome = new bool[] {false} ;
         P00K65_A303ContratoOcorrenciaNotificacao_Responsavel = new int[1] ;
         P00K65_A302ContratoOcorrenciaNotificacao_Protocolo = new String[] {""} ;
         P00K65_n302ContratoOcorrenciaNotificacao_Protocolo = new bool[] {false} ;
         P00K65_A301ContratoOcorrenciaNotificacao_Cumprido = new DateTime[] {DateTime.MinValue} ;
         P00K65_n301ContratoOcorrenciaNotificacao_Cumprido = new bool[] {false} ;
         P00K65_A300ContratoOcorrenciaNotificacao_Descricao = new String[] {""} ;
         P00K65_A299ContratoOcorrenciaNotificacao_Prazo = new short[1] ;
         P00K65_A298ContratoOcorrenciaNotificacao_Data = new DateTime[] {DateTime.MinValue} ;
         P00K65_A297ContratoOcorrenciaNotificacao_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getcontratoocorrenciacontratoocorrencianotificacaowcfilterdata__default(),
            new Object[][] {
                new Object[] {
               P00K62_A294ContratoOcorrencia_Codigo, P00K62_A300ContratoOcorrenciaNotificacao_Descricao, P00K62_A306ContratoOcorrenciaNotificacao_Docto, P00K62_n306ContratoOcorrenciaNotificacao_Docto, P00K62_A304ContratoOcorrenciaNotificacao_Nome, P00K62_n304ContratoOcorrenciaNotificacao_Nome, P00K62_A303ContratoOcorrenciaNotificacao_Responsavel, P00K62_A302ContratoOcorrenciaNotificacao_Protocolo, P00K62_n302ContratoOcorrenciaNotificacao_Protocolo, P00K62_A301ContratoOcorrenciaNotificacao_Cumprido,
               P00K62_n301ContratoOcorrenciaNotificacao_Cumprido, P00K62_A299ContratoOcorrenciaNotificacao_Prazo, P00K62_A298ContratoOcorrenciaNotificacao_Data, P00K62_A297ContratoOcorrenciaNotificacao_Codigo
               }
               , new Object[] {
               P00K63_A294ContratoOcorrencia_Codigo, P00K63_A302ContratoOcorrenciaNotificacao_Protocolo, P00K63_n302ContratoOcorrenciaNotificacao_Protocolo, P00K63_A306ContratoOcorrenciaNotificacao_Docto, P00K63_n306ContratoOcorrenciaNotificacao_Docto, P00K63_A304ContratoOcorrenciaNotificacao_Nome, P00K63_n304ContratoOcorrenciaNotificacao_Nome, P00K63_A303ContratoOcorrenciaNotificacao_Responsavel, P00K63_A301ContratoOcorrenciaNotificacao_Cumprido, P00K63_n301ContratoOcorrenciaNotificacao_Cumprido,
               P00K63_A300ContratoOcorrenciaNotificacao_Descricao, P00K63_A299ContratoOcorrenciaNotificacao_Prazo, P00K63_A298ContratoOcorrenciaNotificacao_Data, P00K63_A297ContratoOcorrenciaNotificacao_Codigo
               }
               , new Object[] {
               P00K64_A294ContratoOcorrencia_Codigo, P00K64_A303ContratoOcorrenciaNotificacao_Responsavel, P00K64_A306ContratoOcorrenciaNotificacao_Docto, P00K64_n306ContratoOcorrenciaNotificacao_Docto, P00K64_A304ContratoOcorrenciaNotificacao_Nome, P00K64_n304ContratoOcorrenciaNotificacao_Nome, P00K64_A302ContratoOcorrenciaNotificacao_Protocolo, P00K64_n302ContratoOcorrenciaNotificacao_Protocolo, P00K64_A301ContratoOcorrenciaNotificacao_Cumprido, P00K64_n301ContratoOcorrenciaNotificacao_Cumprido,
               P00K64_A300ContratoOcorrenciaNotificacao_Descricao, P00K64_A299ContratoOcorrenciaNotificacao_Prazo, P00K64_A298ContratoOcorrenciaNotificacao_Data, P00K64_A297ContratoOcorrenciaNotificacao_Codigo
               }
               , new Object[] {
               P00K65_A294ContratoOcorrencia_Codigo, P00K65_A306ContratoOcorrenciaNotificacao_Docto, P00K65_n306ContratoOcorrenciaNotificacao_Docto, P00K65_A304ContratoOcorrenciaNotificacao_Nome, P00K65_n304ContratoOcorrenciaNotificacao_Nome, P00K65_A303ContratoOcorrenciaNotificacao_Responsavel, P00K65_A302ContratoOcorrenciaNotificacao_Protocolo, P00K65_n302ContratoOcorrenciaNotificacao_Protocolo, P00K65_A301ContratoOcorrenciaNotificacao_Cumprido, P00K65_n301ContratoOcorrenciaNotificacao_Cumprido,
               P00K65_A300ContratoOcorrenciaNotificacao_Descricao, P00K65_A299ContratoOcorrenciaNotificacao_Prazo, P00K65_A298ContratoOcorrenciaNotificacao_Data, P00K65_A297ContratoOcorrenciaNotificacao_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV12TFContratoOcorrenciaNotificacao_Prazo ;
      private short AV13TFContratoOcorrenciaNotificacao_Prazo_To ;
      private short AV47ContratoOcorrenciaNotificacao_Prazo1 ;
      private short AV48ContratoOcorrenciaNotificacao_Prazo_To1 ;
      private short AV53ContratoOcorrenciaNotificacao_Prazo2 ;
      private short AV54ContratoOcorrenciaNotificacao_Prazo_To2 ;
      private short A299ContratoOcorrenciaNotificacao_Prazo ;
      private int AV58GXV1 ;
      private int AV20TFContratoOcorrenciaNotificacao_Responsavel ;
      private int AV21TFContratoOcorrenciaNotificacao_Responsavel_To ;
      private int AV55ContratoOcorrencia_Codigo ;
      private int A303ContratoOcorrenciaNotificacao_Responsavel ;
      private int A294ContratoOcorrencia_Codigo ;
      private int A297ContratoOcorrenciaNotificacao_Codigo ;
      private int AV29InsertIndex ;
      private long AV38count ;
      private String AV22TFContratoOcorrenciaNotificacao_Nome ;
      private String AV23TFContratoOcorrenciaNotificacao_Nome_Sel ;
      private String scmdbuf ;
      private String lV22TFContratoOcorrenciaNotificacao_Nome ;
      private String A304ContratoOcorrenciaNotificacao_Nome ;
      private DateTime AV10TFContratoOcorrenciaNotificacao_Data ;
      private DateTime AV11TFContratoOcorrenciaNotificacao_Data_To ;
      private DateTime AV16TFContratoOcorrenciaNotificacao_Cumprido ;
      private DateTime AV17TFContratoOcorrenciaNotificacao_Cumprido_To ;
      private DateTime AV45ContratoOcorrenciaNotificacao_Data1 ;
      private DateTime AV46ContratoOcorrenciaNotificacao_Data_To1 ;
      private DateTime AV51ContratoOcorrenciaNotificacao_Data2 ;
      private DateTime AV52ContratoOcorrenciaNotificacao_Data_To2 ;
      private DateTime A298ContratoOcorrenciaNotificacao_Data ;
      private DateTime A301ContratoOcorrenciaNotificacao_Cumprido ;
      private bool returnInSub ;
      private bool AV49DynamicFiltersEnabled2 ;
      private bool BRKK62 ;
      private bool n306ContratoOcorrenciaNotificacao_Docto ;
      private bool n304ContratoOcorrenciaNotificacao_Nome ;
      private bool n302ContratoOcorrenciaNotificacao_Protocolo ;
      private bool n301ContratoOcorrenciaNotificacao_Cumprido ;
      private bool BRKK64 ;
      private bool BRKK66 ;
      private bool BRKK68 ;
      private String AV37OptionIndexesJson ;
      private String AV32OptionsJson ;
      private String AV35OptionsDescJson ;
      private String AV28DDOName ;
      private String AV26SearchTxt ;
      private String AV27SearchTxtTo ;
      private String AV14TFContratoOcorrenciaNotificacao_Descricao ;
      private String AV15TFContratoOcorrenciaNotificacao_Descricao_Sel ;
      private String AV18TFContratoOcorrenciaNotificacao_Protocolo ;
      private String AV19TFContratoOcorrenciaNotificacao_Protocolo_Sel ;
      private String AV24TFContratoOcorrenciaNotificacao_Docto ;
      private String AV25TFContratoOcorrenciaNotificacao_Docto_Sel ;
      private String AV44DynamicFiltersSelector1 ;
      private String AV50DynamicFiltersSelector2 ;
      private String lV14TFContratoOcorrenciaNotificacao_Descricao ;
      private String lV18TFContratoOcorrenciaNotificacao_Protocolo ;
      private String lV24TFContratoOcorrenciaNotificacao_Docto ;
      private String A300ContratoOcorrenciaNotificacao_Descricao ;
      private String A302ContratoOcorrenciaNotificacao_Protocolo ;
      private String A306ContratoOcorrenciaNotificacao_Docto ;
      private String AV30Option ;
      private IGxSession AV39Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00K62_A294ContratoOcorrencia_Codigo ;
      private String[] P00K62_A300ContratoOcorrenciaNotificacao_Descricao ;
      private String[] P00K62_A306ContratoOcorrenciaNotificacao_Docto ;
      private bool[] P00K62_n306ContratoOcorrenciaNotificacao_Docto ;
      private String[] P00K62_A304ContratoOcorrenciaNotificacao_Nome ;
      private bool[] P00K62_n304ContratoOcorrenciaNotificacao_Nome ;
      private int[] P00K62_A303ContratoOcorrenciaNotificacao_Responsavel ;
      private String[] P00K62_A302ContratoOcorrenciaNotificacao_Protocolo ;
      private bool[] P00K62_n302ContratoOcorrenciaNotificacao_Protocolo ;
      private DateTime[] P00K62_A301ContratoOcorrenciaNotificacao_Cumprido ;
      private bool[] P00K62_n301ContratoOcorrenciaNotificacao_Cumprido ;
      private short[] P00K62_A299ContratoOcorrenciaNotificacao_Prazo ;
      private DateTime[] P00K62_A298ContratoOcorrenciaNotificacao_Data ;
      private int[] P00K62_A297ContratoOcorrenciaNotificacao_Codigo ;
      private int[] P00K63_A294ContratoOcorrencia_Codigo ;
      private String[] P00K63_A302ContratoOcorrenciaNotificacao_Protocolo ;
      private bool[] P00K63_n302ContratoOcorrenciaNotificacao_Protocolo ;
      private String[] P00K63_A306ContratoOcorrenciaNotificacao_Docto ;
      private bool[] P00K63_n306ContratoOcorrenciaNotificacao_Docto ;
      private String[] P00K63_A304ContratoOcorrenciaNotificacao_Nome ;
      private bool[] P00K63_n304ContratoOcorrenciaNotificacao_Nome ;
      private int[] P00K63_A303ContratoOcorrenciaNotificacao_Responsavel ;
      private DateTime[] P00K63_A301ContratoOcorrenciaNotificacao_Cumprido ;
      private bool[] P00K63_n301ContratoOcorrenciaNotificacao_Cumprido ;
      private String[] P00K63_A300ContratoOcorrenciaNotificacao_Descricao ;
      private short[] P00K63_A299ContratoOcorrenciaNotificacao_Prazo ;
      private DateTime[] P00K63_A298ContratoOcorrenciaNotificacao_Data ;
      private int[] P00K63_A297ContratoOcorrenciaNotificacao_Codigo ;
      private int[] P00K64_A294ContratoOcorrencia_Codigo ;
      private int[] P00K64_A303ContratoOcorrenciaNotificacao_Responsavel ;
      private String[] P00K64_A306ContratoOcorrenciaNotificacao_Docto ;
      private bool[] P00K64_n306ContratoOcorrenciaNotificacao_Docto ;
      private String[] P00K64_A304ContratoOcorrenciaNotificacao_Nome ;
      private bool[] P00K64_n304ContratoOcorrenciaNotificacao_Nome ;
      private String[] P00K64_A302ContratoOcorrenciaNotificacao_Protocolo ;
      private bool[] P00K64_n302ContratoOcorrenciaNotificacao_Protocolo ;
      private DateTime[] P00K64_A301ContratoOcorrenciaNotificacao_Cumprido ;
      private bool[] P00K64_n301ContratoOcorrenciaNotificacao_Cumprido ;
      private String[] P00K64_A300ContratoOcorrenciaNotificacao_Descricao ;
      private short[] P00K64_A299ContratoOcorrenciaNotificacao_Prazo ;
      private DateTime[] P00K64_A298ContratoOcorrenciaNotificacao_Data ;
      private int[] P00K64_A297ContratoOcorrenciaNotificacao_Codigo ;
      private int[] P00K65_A294ContratoOcorrencia_Codigo ;
      private String[] P00K65_A306ContratoOcorrenciaNotificacao_Docto ;
      private bool[] P00K65_n306ContratoOcorrenciaNotificacao_Docto ;
      private String[] P00K65_A304ContratoOcorrenciaNotificacao_Nome ;
      private bool[] P00K65_n304ContratoOcorrenciaNotificacao_Nome ;
      private int[] P00K65_A303ContratoOcorrenciaNotificacao_Responsavel ;
      private String[] P00K65_A302ContratoOcorrenciaNotificacao_Protocolo ;
      private bool[] P00K65_n302ContratoOcorrenciaNotificacao_Protocolo ;
      private DateTime[] P00K65_A301ContratoOcorrenciaNotificacao_Cumprido ;
      private bool[] P00K65_n301ContratoOcorrenciaNotificacao_Cumprido ;
      private String[] P00K65_A300ContratoOcorrenciaNotificacao_Descricao ;
      private short[] P00K65_A299ContratoOcorrenciaNotificacao_Prazo ;
      private DateTime[] P00K65_A298ContratoOcorrenciaNotificacao_Data ;
      private int[] P00K65_A297ContratoOcorrenciaNotificacao_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV31Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV34OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV36OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV41GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV42GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV43GridStateDynamicFilter ;
   }

   public class getcontratoocorrenciacontratoocorrencianotificacaowcfilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00K62( IGxContext context ,
                                             String AV44DynamicFiltersSelector1 ,
                                             DateTime AV45ContratoOcorrenciaNotificacao_Data1 ,
                                             DateTime AV46ContratoOcorrenciaNotificacao_Data_To1 ,
                                             short AV47ContratoOcorrenciaNotificacao_Prazo1 ,
                                             short AV48ContratoOcorrenciaNotificacao_Prazo_To1 ,
                                             bool AV49DynamicFiltersEnabled2 ,
                                             String AV50DynamicFiltersSelector2 ,
                                             DateTime AV51ContratoOcorrenciaNotificacao_Data2 ,
                                             DateTime AV52ContratoOcorrenciaNotificacao_Data_To2 ,
                                             short AV53ContratoOcorrenciaNotificacao_Prazo2 ,
                                             short AV54ContratoOcorrenciaNotificacao_Prazo_To2 ,
                                             DateTime AV10TFContratoOcorrenciaNotificacao_Data ,
                                             DateTime AV11TFContratoOcorrenciaNotificacao_Data_To ,
                                             short AV12TFContratoOcorrenciaNotificacao_Prazo ,
                                             short AV13TFContratoOcorrenciaNotificacao_Prazo_To ,
                                             String AV15TFContratoOcorrenciaNotificacao_Descricao_Sel ,
                                             String AV14TFContratoOcorrenciaNotificacao_Descricao ,
                                             DateTime AV16TFContratoOcorrenciaNotificacao_Cumprido ,
                                             DateTime AV17TFContratoOcorrenciaNotificacao_Cumprido_To ,
                                             String AV19TFContratoOcorrenciaNotificacao_Protocolo_Sel ,
                                             String AV18TFContratoOcorrenciaNotificacao_Protocolo ,
                                             int AV20TFContratoOcorrenciaNotificacao_Responsavel ,
                                             int AV21TFContratoOcorrenciaNotificacao_Responsavel_To ,
                                             String AV23TFContratoOcorrenciaNotificacao_Nome_Sel ,
                                             String AV22TFContratoOcorrenciaNotificacao_Nome ,
                                             String AV25TFContratoOcorrenciaNotificacao_Docto_Sel ,
                                             String AV24TFContratoOcorrenciaNotificacao_Docto ,
                                             DateTime A298ContratoOcorrenciaNotificacao_Data ,
                                             short A299ContratoOcorrenciaNotificacao_Prazo ,
                                             String A300ContratoOcorrenciaNotificacao_Descricao ,
                                             DateTime A301ContratoOcorrenciaNotificacao_Cumprido ,
                                             String A302ContratoOcorrenciaNotificacao_Protocolo ,
                                             int A303ContratoOcorrenciaNotificacao_Responsavel ,
                                             String A304ContratoOcorrenciaNotificacao_Nome ,
                                             String A306ContratoOcorrenciaNotificacao_Docto ,
                                             int AV55ContratoOcorrencia_Codigo ,
                                             int A294ContratoOcorrencia_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [25] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContratoOcorrencia_Codigo], T1.[ContratoOcorrenciaNotificacao_Descricao], T2.[Pessoa_Docto] AS ContratoOcorrenciaNotificacao_Docto, T2.[Pessoa_Nome] AS ContratoOcorrenciaNotificacao_Nome, T1.[ContratoOcorrenciaNotificacao_Responsavel] AS ContratoOcorrenciaNotificacao_Responsavel, T1.[ContratoOcorrenciaNotificacao_Protocolo], T1.[ContratoOcorrenciaNotificacao_Cumprido], T1.[ContratoOcorrenciaNotificacao_Prazo], T1.[ContratoOcorrenciaNotificacao_Data], T1.[ContratoOcorrenciaNotificacao_Codigo] FROM ([ContratoOcorrenciaNotificacao] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[ContratoOcorrenciaNotificacao_Responsavel])";
         scmdbuf = scmdbuf + " WHERE (T1.[ContratoOcorrencia_Codigo] = @AV55ContratoOcorrencia_Codigo)";
         if ( ( StringUtil.StrCmp(AV44DynamicFiltersSelector1, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV45ContratoOcorrenciaNotificacao_Data1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] >= @AV45ContratoOcorrenciaNotificacao_Data1)";
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV44DynamicFiltersSelector1, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV46ContratoOcorrenciaNotificacao_Data_To1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] <= @AV46ContratoOcorrenciaNotificacao_Data_To1)";
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV44DynamicFiltersSelector1, "CONTRATOOCORRENCIANOTIFICACAO_PRAZO") == 0 ) && ( ! (0==AV47ContratoOcorrenciaNotificacao_Prazo1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Prazo] >= @AV47ContratoOcorrenciaNotificacao_Prazo1)";
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV44DynamicFiltersSelector1, "CONTRATOOCORRENCIANOTIFICACAO_PRAZO") == 0 ) && ( ! (0==AV48ContratoOcorrenciaNotificacao_Prazo_To1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Prazo] <= @AV48ContratoOcorrenciaNotificacao_Prazo_To1)";
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( AV49DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV50DynamicFiltersSelector2, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV51ContratoOcorrenciaNotificacao_Data2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] >= @AV51ContratoOcorrenciaNotificacao_Data2)";
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( AV49DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV50DynamicFiltersSelector2, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV52ContratoOcorrenciaNotificacao_Data_To2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] <= @AV52ContratoOcorrenciaNotificacao_Data_To2)";
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( AV49DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV50DynamicFiltersSelector2, "CONTRATOOCORRENCIANOTIFICACAO_PRAZO") == 0 ) && ( ! (0==AV53ContratoOcorrenciaNotificacao_Prazo2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Prazo] >= @AV53ContratoOcorrenciaNotificacao_Prazo2)";
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( AV49DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV50DynamicFiltersSelector2, "CONTRATOOCORRENCIANOTIFICACAO_PRAZO") == 0 ) && ( ! (0==AV54ContratoOcorrenciaNotificacao_Prazo_To2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Prazo] <= @AV54ContratoOcorrenciaNotificacao_Prazo_To2)";
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( ! (DateTime.MinValue==AV10TFContratoOcorrenciaNotificacao_Data) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] >= @AV10TFContratoOcorrenciaNotificacao_Data)";
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( ! (DateTime.MinValue==AV11TFContratoOcorrenciaNotificacao_Data_To) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] <= @AV11TFContratoOcorrenciaNotificacao_Data_To)";
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( ! (0==AV12TFContratoOcorrenciaNotificacao_Prazo) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Prazo] >= @AV12TFContratoOcorrenciaNotificacao_Prazo)";
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( ! (0==AV13TFContratoOcorrenciaNotificacao_Prazo_To) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Prazo] <= @AV13TFContratoOcorrenciaNotificacao_Prazo_To)";
         }
         else
         {
            GXv_int1[12] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFContratoOcorrenciaNotificacao_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFContratoOcorrenciaNotificacao_Descricao)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Descricao] like @lV14TFContratoOcorrenciaNotificacao_Descricao)";
         }
         else
         {
            GXv_int1[13] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFContratoOcorrenciaNotificacao_Descricao_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Descricao] = @AV15TFContratoOcorrenciaNotificacao_Descricao_Sel)";
         }
         else
         {
            GXv_int1[14] = 1;
         }
         if ( ! (DateTime.MinValue==AV16TFContratoOcorrenciaNotificacao_Cumprido) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Cumprido] >= @AV16TFContratoOcorrenciaNotificacao_Cumprido)";
         }
         else
         {
            GXv_int1[15] = 1;
         }
         if ( ! (DateTime.MinValue==AV17TFContratoOcorrenciaNotificacao_Cumprido_To) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Cumprido] <= @AV17TFContratoOcorrenciaNotificacao_Cumprido_To)";
         }
         else
         {
            GXv_int1[16] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV19TFContratoOcorrenciaNotificacao_Protocolo_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18TFContratoOcorrenciaNotificacao_Protocolo)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Protocolo] like @lV18TFContratoOcorrenciaNotificacao_Protocolo)";
         }
         else
         {
            GXv_int1[17] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19TFContratoOcorrenciaNotificacao_Protocolo_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Protocolo] = @AV19TFContratoOcorrenciaNotificacao_Protocolo_Sel)";
         }
         else
         {
            GXv_int1[18] = 1;
         }
         if ( ! (0==AV20TFContratoOcorrenciaNotificacao_Responsavel) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Responsavel] >= @AV20TFContratoOcorrenciaNotificacao_Responsavel)";
         }
         else
         {
            GXv_int1[19] = 1;
         }
         if ( ! (0==AV21TFContratoOcorrenciaNotificacao_Responsavel_To) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Responsavel] <= @AV21TFContratoOcorrenciaNotificacao_Responsavel_To)";
         }
         else
         {
            GXv_int1[20] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV23TFContratoOcorrenciaNotificacao_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV22TFContratoOcorrenciaNotificacao_Nome)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like @lV22TFContratoOcorrenciaNotificacao_Nome)";
         }
         else
         {
            GXv_int1[21] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23TFContratoOcorrenciaNotificacao_Nome_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Nome] = @AV23TFContratoOcorrenciaNotificacao_Nome_Sel)";
         }
         else
         {
            GXv_int1[22] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV25TFContratoOcorrenciaNotificacao_Docto_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV24TFContratoOcorrenciaNotificacao_Docto)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Docto] like @lV24TFContratoOcorrenciaNotificacao_Docto)";
         }
         else
         {
            GXv_int1[23] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV25TFContratoOcorrenciaNotificacao_Docto_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Docto] = @AV25TFContratoOcorrenciaNotificacao_Docto_Sel)";
         }
         else
         {
            GXv_int1[24] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[ContratoOcorrencia_Codigo], T1.[ContratoOcorrenciaNotificacao_Descricao]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00K63( IGxContext context ,
                                             String AV44DynamicFiltersSelector1 ,
                                             DateTime AV45ContratoOcorrenciaNotificacao_Data1 ,
                                             DateTime AV46ContratoOcorrenciaNotificacao_Data_To1 ,
                                             short AV47ContratoOcorrenciaNotificacao_Prazo1 ,
                                             short AV48ContratoOcorrenciaNotificacao_Prazo_To1 ,
                                             bool AV49DynamicFiltersEnabled2 ,
                                             String AV50DynamicFiltersSelector2 ,
                                             DateTime AV51ContratoOcorrenciaNotificacao_Data2 ,
                                             DateTime AV52ContratoOcorrenciaNotificacao_Data_To2 ,
                                             short AV53ContratoOcorrenciaNotificacao_Prazo2 ,
                                             short AV54ContratoOcorrenciaNotificacao_Prazo_To2 ,
                                             DateTime AV10TFContratoOcorrenciaNotificacao_Data ,
                                             DateTime AV11TFContratoOcorrenciaNotificacao_Data_To ,
                                             short AV12TFContratoOcorrenciaNotificacao_Prazo ,
                                             short AV13TFContratoOcorrenciaNotificacao_Prazo_To ,
                                             String AV15TFContratoOcorrenciaNotificacao_Descricao_Sel ,
                                             String AV14TFContratoOcorrenciaNotificacao_Descricao ,
                                             DateTime AV16TFContratoOcorrenciaNotificacao_Cumprido ,
                                             DateTime AV17TFContratoOcorrenciaNotificacao_Cumprido_To ,
                                             String AV19TFContratoOcorrenciaNotificacao_Protocolo_Sel ,
                                             String AV18TFContratoOcorrenciaNotificacao_Protocolo ,
                                             int AV20TFContratoOcorrenciaNotificacao_Responsavel ,
                                             int AV21TFContratoOcorrenciaNotificacao_Responsavel_To ,
                                             String AV23TFContratoOcorrenciaNotificacao_Nome_Sel ,
                                             String AV22TFContratoOcorrenciaNotificacao_Nome ,
                                             String AV25TFContratoOcorrenciaNotificacao_Docto_Sel ,
                                             String AV24TFContratoOcorrenciaNotificacao_Docto ,
                                             DateTime A298ContratoOcorrenciaNotificacao_Data ,
                                             short A299ContratoOcorrenciaNotificacao_Prazo ,
                                             String A300ContratoOcorrenciaNotificacao_Descricao ,
                                             DateTime A301ContratoOcorrenciaNotificacao_Cumprido ,
                                             String A302ContratoOcorrenciaNotificacao_Protocolo ,
                                             int A303ContratoOcorrenciaNotificacao_Responsavel ,
                                             String A304ContratoOcorrenciaNotificacao_Nome ,
                                             String A306ContratoOcorrenciaNotificacao_Docto ,
                                             int AV55ContratoOcorrencia_Codigo ,
                                             int A294ContratoOcorrencia_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [25] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContratoOcorrencia_Codigo], T1.[ContratoOcorrenciaNotificacao_Protocolo], T2.[Pessoa_Docto] AS ContratoOcorrenciaNotificacao_Docto, T2.[Pessoa_Nome] AS ContratoOcorrenciaNotificacao_Nome, T1.[ContratoOcorrenciaNotificacao_Responsavel] AS ContratoOcorrenciaNotificacao_Responsavel, T1.[ContratoOcorrenciaNotificacao_Cumprido], T1.[ContratoOcorrenciaNotificacao_Descricao], T1.[ContratoOcorrenciaNotificacao_Prazo], T1.[ContratoOcorrenciaNotificacao_Data], T1.[ContratoOcorrenciaNotificacao_Codigo] FROM ([ContratoOcorrenciaNotificacao] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[ContratoOcorrenciaNotificacao_Responsavel])";
         scmdbuf = scmdbuf + " WHERE (T1.[ContratoOcorrencia_Codigo] = @AV55ContratoOcorrencia_Codigo)";
         if ( ( StringUtil.StrCmp(AV44DynamicFiltersSelector1, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV45ContratoOcorrenciaNotificacao_Data1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] >= @AV45ContratoOcorrenciaNotificacao_Data1)";
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV44DynamicFiltersSelector1, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV46ContratoOcorrenciaNotificacao_Data_To1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] <= @AV46ContratoOcorrenciaNotificacao_Data_To1)";
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV44DynamicFiltersSelector1, "CONTRATOOCORRENCIANOTIFICACAO_PRAZO") == 0 ) && ( ! (0==AV47ContratoOcorrenciaNotificacao_Prazo1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Prazo] >= @AV47ContratoOcorrenciaNotificacao_Prazo1)";
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV44DynamicFiltersSelector1, "CONTRATOOCORRENCIANOTIFICACAO_PRAZO") == 0 ) && ( ! (0==AV48ContratoOcorrenciaNotificacao_Prazo_To1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Prazo] <= @AV48ContratoOcorrenciaNotificacao_Prazo_To1)";
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( AV49DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV50DynamicFiltersSelector2, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV51ContratoOcorrenciaNotificacao_Data2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] >= @AV51ContratoOcorrenciaNotificacao_Data2)";
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( AV49DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV50DynamicFiltersSelector2, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV52ContratoOcorrenciaNotificacao_Data_To2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] <= @AV52ContratoOcorrenciaNotificacao_Data_To2)";
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( AV49DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV50DynamicFiltersSelector2, "CONTRATOOCORRENCIANOTIFICACAO_PRAZO") == 0 ) && ( ! (0==AV53ContratoOcorrenciaNotificacao_Prazo2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Prazo] >= @AV53ContratoOcorrenciaNotificacao_Prazo2)";
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( AV49DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV50DynamicFiltersSelector2, "CONTRATOOCORRENCIANOTIFICACAO_PRAZO") == 0 ) && ( ! (0==AV54ContratoOcorrenciaNotificacao_Prazo_To2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Prazo] <= @AV54ContratoOcorrenciaNotificacao_Prazo_To2)";
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( ! (DateTime.MinValue==AV10TFContratoOcorrenciaNotificacao_Data) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] >= @AV10TFContratoOcorrenciaNotificacao_Data)";
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( ! (DateTime.MinValue==AV11TFContratoOcorrenciaNotificacao_Data_To) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] <= @AV11TFContratoOcorrenciaNotificacao_Data_To)";
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( ! (0==AV12TFContratoOcorrenciaNotificacao_Prazo) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Prazo] >= @AV12TFContratoOcorrenciaNotificacao_Prazo)";
         }
         else
         {
            GXv_int3[11] = 1;
         }
         if ( ! (0==AV13TFContratoOcorrenciaNotificacao_Prazo_To) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Prazo] <= @AV13TFContratoOcorrenciaNotificacao_Prazo_To)";
         }
         else
         {
            GXv_int3[12] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFContratoOcorrenciaNotificacao_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFContratoOcorrenciaNotificacao_Descricao)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Descricao] like @lV14TFContratoOcorrenciaNotificacao_Descricao)";
         }
         else
         {
            GXv_int3[13] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFContratoOcorrenciaNotificacao_Descricao_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Descricao] = @AV15TFContratoOcorrenciaNotificacao_Descricao_Sel)";
         }
         else
         {
            GXv_int3[14] = 1;
         }
         if ( ! (DateTime.MinValue==AV16TFContratoOcorrenciaNotificacao_Cumprido) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Cumprido] >= @AV16TFContratoOcorrenciaNotificacao_Cumprido)";
         }
         else
         {
            GXv_int3[15] = 1;
         }
         if ( ! (DateTime.MinValue==AV17TFContratoOcorrenciaNotificacao_Cumprido_To) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Cumprido] <= @AV17TFContratoOcorrenciaNotificacao_Cumprido_To)";
         }
         else
         {
            GXv_int3[16] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV19TFContratoOcorrenciaNotificacao_Protocolo_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18TFContratoOcorrenciaNotificacao_Protocolo)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Protocolo] like @lV18TFContratoOcorrenciaNotificacao_Protocolo)";
         }
         else
         {
            GXv_int3[17] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19TFContratoOcorrenciaNotificacao_Protocolo_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Protocolo] = @AV19TFContratoOcorrenciaNotificacao_Protocolo_Sel)";
         }
         else
         {
            GXv_int3[18] = 1;
         }
         if ( ! (0==AV20TFContratoOcorrenciaNotificacao_Responsavel) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Responsavel] >= @AV20TFContratoOcorrenciaNotificacao_Responsavel)";
         }
         else
         {
            GXv_int3[19] = 1;
         }
         if ( ! (0==AV21TFContratoOcorrenciaNotificacao_Responsavel_To) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Responsavel] <= @AV21TFContratoOcorrenciaNotificacao_Responsavel_To)";
         }
         else
         {
            GXv_int3[20] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV23TFContratoOcorrenciaNotificacao_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV22TFContratoOcorrenciaNotificacao_Nome)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like @lV22TFContratoOcorrenciaNotificacao_Nome)";
         }
         else
         {
            GXv_int3[21] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23TFContratoOcorrenciaNotificacao_Nome_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Nome] = @AV23TFContratoOcorrenciaNotificacao_Nome_Sel)";
         }
         else
         {
            GXv_int3[22] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV25TFContratoOcorrenciaNotificacao_Docto_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV24TFContratoOcorrenciaNotificacao_Docto)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Docto] like @lV24TFContratoOcorrenciaNotificacao_Docto)";
         }
         else
         {
            GXv_int3[23] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV25TFContratoOcorrenciaNotificacao_Docto_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Docto] = @AV25TFContratoOcorrenciaNotificacao_Docto_Sel)";
         }
         else
         {
            GXv_int3[24] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[ContratoOcorrencia_Codigo], T1.[ContratoOcorrenciaNotificacao_Protocolo]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      protected Object[] conditional_P00K64( IGxContext context ,
                                             String AV44DynamicFiltersSelector1 ,
                                             DateTime AV45ContratoOcorrenciaNotificacao_Data1 ,
                                             DateTime AV46ContratoOcorrenciaNotificacao_Data_To1 ,
                                             short AV47ContratoOcorrenciaNotificacao_Prazo1 ,
                                             short AV48ContratoOcorrenciaNotificacao_Prazo_To1 ,
                                             bool AV49DynamicFiltersEnabled2 ,
                                             String AV50DynamicFiltersSelector2 ,
                                             DateTime AV51ContratoOcorrenciaNotificacao_Data2 ,
                                             DateTime AV52ContratoOcorrenciaNotificacao_Data_To2 ,
                                             short AV53ContratoOcorrenciaNotificacao_Prazo2 ,
                                             short AV54ContratoOcorrenciaNotificacao_Prazo_To2 ,
                                             DateTime AV10TFContratoOcorrenciaNotificacao_Data ,
                                             DateTime AV11TFContratoOcorrenciaNotificacao_Data_To ,
                                             short AV12TFContratoOcorrenciaNotificacao_Prazo ,
                                             short AV13TFContratoOcorrenciaNotificacao_Prazo_To ,
                                             String AV15TFContratoOcorrenciaNotificacao_Descricao_Sel ,
                                             String AV14TFContratoOcorrenciaNotificacao_Descricao ,
                                             DateTime AV16TFContratoOcorrenciaNotificacao_Cumprido ,
                                             DateTime AV17TFContratoOcorrenciaNotificacao_Cumprido_To ,
                                             String AV19TFContratoOcorrenciaNotificacao_Protocolo_Sel ,
                                             String AV18TFContratoOcorrenciaNotificacao_Protocolo ,
                                             int AV20TFContratoOcorrenciaNotificacao_Responsavel ,
                                             int AV21TFContratoOcorrenciaNotificacao_Responsavel_To ,
                                             String AV23TFContratoOcorrenciaNotificacao_Nome_Sel ,
                                             String AV22TFContratoOcorrenciaNotificacao_Nome ,
                                             String AV25TFContratoOcorrenciaNotificacao_Docto_Sel ,
                                             String AV24TFContratoOcorrenciaNotificacao_Docto ,
                                             DateTime A298ContratoOcorrenciaNotificacao_Data ,
                                             short A299ContratoOcorrenciaNotificacao_Prazo ,
                                             String A300ContratoOcorrenciaNotificacao_Descricao ,
                                             DateTime A301ContratoOcorrenciaNotificacao_Cumprido ,
                                             String A302ContratoOcorrenciaNotificacao_Protocolo ,
                                             int A303ContratoOcorrenciaNotificacao_Responsavel ,
                                             String A304ContratoOcorrenciaNotificacao_Nome ,
                                             String A306ContratoOcorrenciaNotificacao_Docto ,
                                             int AV55ContratoOcorrencia_Codigo ,
                                             int A294ContratoOcorrencia_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int5 ;
         GXv_int5 = new short [25] ;
         Object[] GXv_Object6 ;
         GXv_Object6 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContratoOcorrencia_Codigo], T1.[ContratoOcorrenciaNotificacao_Responsavel] AS ContratoOcorrenciaNotificacao_Responsavel, T2.[Pessoa_Docto] AS ContratoOcorrenciaNotificacao_Docto, T2.[Pessoa_Nome] AS ContratoOcorrenciaNotificacao_Nome, T1.[ContratoOcorrenciaNotificacao_Protocolo], T1.[ContratoOcorrenciaNotificacao_Cumprido], T1.[ContratoOcorrenciaNotificacao_Descricao], T1.[ContratoOcorrenciaNotificacao_Prazo], T1.[ContratoOcorrenciaNotificacao_Data], T1.[ContratoOcorrenciaNotificacao_Codigo] FROM ([ContratoOcorrenciaNotificacao] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[ContratoOcorrenciaNotificacao_Responsavel])";
         scmdbuf = scmdbuf + " WHERE (T1.[ContratoOcorrencia_Codigo] = @AV55ContratoOcorrencia_Codigo)";
         if ( ( StringUtil.StrCmp(AV44DynamicFiltersSelector1, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV45ContratoOcorrenciaNotificacao_Data1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] >= @AV45ContratoOcorrenciaNotificacao_Data1)";
         }
         else
         {
            GXv_int5[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV44DynamicFiltersSelector1, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV46ContratoOcorrenciaNotificacao_Data_To1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] <= @AV46ContratoOcorrenciaNotificacao_Data_To1)";
         }
         else
         {
            GXv_int5[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV44DynamicFiltersSelector1, "CONTRATOOCORRENCIANOTIFICACAO_PRAZO") == 0 ) && ( ! (0==AV47ContratoOcorrenciaNotificacao_Prazo1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Prazo] >= @AV47ContratoOcorrenciaNotificacao_Prazo1)";
         }
         else
         {
            GXv_int5[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV44DynamicFiltersSelector1, "CONTRATOOCORRENCIANOTIFICACAO_PRAZO") == 0 ) && ( ! (0==AV48ContratoOcorrenciaNotificacao_Prazo_To1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Prazo] <= @AV48ContratoOcorrenciaNotificacao_Prazo_To1)";
         }
         else
         {
            GXv_int5[4] = 1;
         }
         if ( AV49DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV50DynamicFiltersSelector2, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV51ContratoOcorrenciaNotificacao_Data2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] >= @AV51ContratoOcorrenciaNotificacao_Data2)";
         }
         else
         {
            GXv_int5[5] = 1;
         }
         if ( AV49DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV50DynamicFiltersSelector2, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV52ContratoOcorrenciaNotificacao_Data_To2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] <= @AV52ContratoOcorrenciaNotificacao_Data_To2)";
         }
         else
         {
            GXv_int5[6] = 1;
         }
         if ( AV49DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV50DynamicFiltersSelector2, "CONTRATOOCORRENCIANOTIFICACAO_PRAZO") == 0 ) && ( ! (0==AV53ContratoOcorrenciaNotificacao_Prazo2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Prazo] >= @AV53ContratoOcorrenciaNotificacao_Prazo2)";
         }
         else
         {
            GXv_int5[7] = 1;
         }
         if ( AV49DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV50DynamicFiltersSelector2, "CONTRATOOCORRENCIANOTIFICACAO_PRAZO") == 0 ) && ( ! (0==AV54ContratoOcorrenciaNotificacao_Prazo_To2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Prazo] <= @AV54ContratoOcorrenciaNotificacao_Prazo_To2)";
         }
         else
         {
            GXv_int5[8] = 1;
         }
         if ( ! (DateTime.MinValue==AV10TFContratoOcorrenciaNotificacao_Data) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] >= @AV10TFContratoOcorrenciaNotificacao_Data)";
         }
         else
         {
            GXv_int5[9] = 1;
         }
         if ( ! (DateTime.MinValue==AV11TFContratoOcorrenciaNotificacao_Data_To) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] <= @AV11TFContratoOcorrenciaNotificacao_Data_To)";
         }
         else
         {
            GXv_int5[10] = 1;
         }
         if ( ! (0==AV12TFContratoOcorrenciaNotificacao_Prazo) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Prazo] >= @AV12TFContratoOcorrenciaNotificacao_Prazo)";
         }
         else
         {
            GXv_int5[11] = 1;
         }
         if ( ! (0==AV13TFContratoOcorrenciaNotificacao_Prazo_To) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Prazo] <= @AV13TFContratoOcorrenciaNotificacao_Prazo_To)";
         }
         else
         {
            GXv_int5[12] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFContratoOcorrenciaNotificacao_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFContratoOcorrenciaNotificacao_Descricao)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Descricao] like @lV14TFContratoOcorrenciaNotificacao_Descricao)";
         }
         else
         {
            GXv_int5[13] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFContratoOcorrenciaNotificacao_Descricao_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Descricao] = @AV15TFContratoOcorrenciaNotificacao_Descricao_Sel)";
         }
         else
         {
            GXv_int5[14] = 1;
         }
         if ( ! (DateTime.MinValue==AV16TFContratoOcorrenciaNotificacao_Cumprido) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Cumprido] >= @AV16TFContratoOcorrenciaNotificacao_Cumprido)";
         }
         else
         {
            GXv_int5[15] = 1;
         }
         if ( ! (DateTime.MinValue==AV17TFContratoOcorrenciaNotificacao_Cumprido_To) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Cumprido] <= @AV17TFContratoOcorrenciaNotificacao_Cumprido_To)";
         }
         else
         {
            GXv_int5[16] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV19TFContratoOcorrenciaNotificacao_Protocolo_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18TFContratoOcorrenciaNotificacao_Protocolo)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Protocolo] like @lV18TFContratoOcorrenciaNotificacao_Protocolo)";
         }
         else
         {
            GXv_int5[17] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19TFContratoOcorrenciaNotificacao_Protocolo_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Protocolo] = @AV19TFContratoOcorrenciaNotificacao_Protocolo_Sel)";
         }
         else
         {
            GXv_int5[18] = 1;
         }
         if ( ! (0==AV20TFContratoOcorrenciaNotificacao_Responsavel) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Responsavel] >= @AV20TFContratoOcorrenciaNotificacao_Responsavel)";
         }
         else
         {
            GXv_int5[19] = 1;
         }
         if ( ! (0==AV21TFContratoOcorrenciaNotificacao_Responsavel_To) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Responsavel] <= @AV21TFContratoOcorrenciaNotificacao_Responsavel_To)";
         }
         else
         {
            GXv_int5[20] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV23TFContratoOcorrenciaNotificacao_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV22TFContratoOcorrenciaNotificacao_Nome)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like @lV22TFContratoOcorrenciaNotificacao_Nome)";
         }
         else
         {
            GXv_int5[21] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23TFContratoOcorrenciaNotificacao_Nome_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Nome] = @AV23TFContratoOcorrenciaNotificacao_Nome_Sel)";
         }
         else
         {
            GXv_int5[22] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV25TFContratoOcorrenciaNotificacao_Docto_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV24TFContratoOcorrenciaNotificacao_Docto)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Docto] like @lV24TFContratoOcorrenciaNotificacao_Docto)";
         }
         else
         {
            GXv_int5[23] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV25TFContratoOcorrenciaNotificacao_Docto_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Docto] = @AV25TFContratoOcorrenciaNotificacao_Docto_Sel)";
         }
         else
         {
            GXv_int5[24] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[ContratoOcorrencia_Codigo], T1.[ContratoOcorrenciaNotificacao_Responsavel]";
         GXv_Object6[0] = scmdbuf;
         GXv_Object6[1] = GXv_int5;
         return GXv_Object6 ;
      }

      protected Object[] conditional_P00K65( IGxContext context ,
                                             String AV44DynamicFiltersSelector1 ,
                                             DateTime AV45ContratoOcorrenciaNotificacao_Data1 ,
                                             DateTime AV46ContratoOcorrenciaNotificacao_Data_To1 ,
                                             short AV47ContratoOcorrenciaNotificacao_Prazo1 ,
                                             short AV48ContratoOcorrenciaNotificacao_Prazo_To1 ,
                                             bool AV49DynamicFiltersEnabled2 ,
                                             String AV50DynamicFiltersSelector2 ,
                                             DateTime AV51ContratoOcorrenciaNotificacao_Data2 ,
                                             DateTime AV52ContratoOcorrenciaNotificacao_Data_To2 ,
                                             short AV53ContratoOcorrenciaNotificacao_Prazo2 ,
                                             short AV54ContratoOcorrenciaNotificacao_Prazo_To2 ,
                                             DateTime AV10TFContratoOcorrenciaNotificacao_Data ,
                                             DateTime AV11TFContratoOcorrenciaNotificacao_Data_To ,
                                             short AV12TFContratoOcorrenciaNotificacao_Prazo ,
                                             short AV13TFContratoOcorrenciaNotificacao_Prazo_To ,
                                             String AV15TFContratoOcorrenciaNotificacao_Descricao_Sel ,
                                             String AV14TFContratoOcorrenciaNotificacao_Descricao ,
                                             DateTime AV16TFContratoOcorrenciaNotificacao_Cumprido ,
                                             DateTime AV17TFContratoOcorrenciaNotificacao_Cumprido_To ,
                                             String AV19TFContratoOcorrenciaNotificacao_Protocolo_Sel ,
                                             String AV18TFContratoOcorrenciaNotificacao_Protocolo ,
                                             int AV20TFContratoOcorrenciaNotificacao_Responsavel ,
                                             int AV21TFContratoOcorrenciaNotificacao_Responsavel_To ,
                                             String AV23TFContratoOcorrenciaNotificacao_Nome_Sel ,
                                             String AV22TFContratoOcorrenciaNotificacao_Nome ,
                                             String AV25TFContratoOcorrenciaNotificacao_Docto_Sel ,
                                             String AV24TFContratoOcorrenciaNotificacao_Docto ,
                                             DateTime A298ContratoOcorrenciaNotificacao_Data ,
                                             short A299ContratoOcorrenciaNotificacao_Prazo ,
                                             String A300ContratoOcorrenciaNotificacao_Descricao ,
                                             DateTime A301ContratoOcorrenciaNotificacao_Cumprido ,
                                             String A302ContratoOcorrenciaNotificacao_Protocolo ,
                                             int A303ContratoOcorrenciaNotificacao_Responsavel ,
                                             String A304ContratoOcorrenciaNotificacao_Nome ,
                                             String A306ContratoOcorrenciaNotificacao_Docto ,
                                             int AV55ContratoOcorrencia_Codigo ,
                                             int A294ContratoOcorrencia_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int7 ;
         GXv_int7 = new short [25] ;
         Object[] GXv_Object8 ;
         GXv_Object8 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContratoOcorrencia_Codigo], T2.[Pessoa_Docto] AS ContratoOcorrenciaNotificacao_Docto, T2.[Pessoa_Nome] AS ContratoOcorrenciaNotificacao_Nome, T1.[ContratoOcorrenciaNotificacao_Responsavel] AS ContratoOcorrenciaNotificacao_Responsavel, T1.[ContratoOcorrenciaNotificacao_Protocolo], T1.[ContratoOcorrenciaNotificacao_Cumprido], T1.[ContratoOcorrenciaNotificacao_Descricao], T1.[ContratoOcorrenciaNotificacao_Prazo], T1.[ContratoOcorrenciaNotificacao_Data], T1.[ContratoOcorrenciaNotificacao_Codigo] FROM ([ContratoOcorrenciaNotificacao] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[ContratoOcorrenciaNotificacao_Responsavel])";
         scmdbuf = scmdbuf + " WHERE (T1.[ContratoOcorrencia_Codigo] = @AV55ContratoOcorrencia_Codigo)";
         if ( ( StringUtil.StrCmp(AV44DynamicFiltersSelector1, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV45ContratoOcorrenciaNotificacao_Data1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] >= @AV45ContratoOcorrenciaNotificacao_Data1)";
         }
         else
         {
            GXv_int7[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV44DynamicFiltersSelector1, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV46ContratoOcorrenciaNotificacao_Data_To1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] <= @AV46ContratoOcorrenciaNotificacao_Data_To1)";
         }
         else
         {
            GXv_int7[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV44DynamicFiltersSelector1, "CONTRATOOCORRENCIANOTIFICACAO_PRAZO") == 0 ) && ( ! (0==AV47ContratoOcorrenciaNotificacao_Prazo1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Prazo] >= @AV47ContratoOcorrenciaNotificacao_Prazo1)";
         }
         else
         {
            GXv_int7[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV44DynamicFiltersSelector1, "CONTRATOOCORRENCIANOTIFICACAO_PRAZO") == 0 ) && ( ! (0==AV48ContratoOcorrenciaNotificacao_Prazo_To1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Prazo] <= @AV48ContratoOcorrenciaNotificacao_Prazo_To1)";
         }
         else
         {
            GXv_int7[4] = 1;
         }
         if ( AV49DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV50DynamicFiltersSelector2, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV51ContratoOcorrenciaNotificacao_Data2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] >= @AV51ContratoOcorrenciaNotificacao_Data2)";
         }
         else
         {
            GXv_int7[5] = 1;
         }
         if ( AV49DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV50DynamicFiltersSelector2, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV52ContratoOcorrenciaNotificacao_Data_To2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] <= @AV52ContratoOcorrenciaNotificacao_Data_To2)";
         }
         else
         {
            GXv_int7[6] = 1;
         }
         if ( AV49DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV50DynamicFiltersSelector2, "CONTRATOOCORRENCIANOTIFICACAO_PRAZO") == 0 ) && ( ! (0==AV53ContratoOcorrenciaNotificacao_Prazo2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Prazo] >= @AV53ContratoOcorrenciaNotificacao_Prazo2)";
         }
         else
         {
            GXv_int7[7] = 1;
         }
         if ( AV49DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV50DynamicFiltersSelector2, "CONTRATOOCORRENCIANOTIFICACAO_PRAZO") == 0 ) && ( ! (0==AV54ContratoOcorrenciaNotificacao_Prazo_To2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Prazo] <= @AV54ContratoOcorrenciaNotificacao_Prazo_To2)";
         }
         else
         {
            GXv_int7[8] = 1;
         }
         if ( ! (DateTime.MinValue==AV10TFContratoOcorrenciaNotificacao_Data) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] >= @AV10TFContratoOcorrenciaNotificacao_Data)";
         }
         else
         {
            GXv_int7[9] = 1;
         }
         if ( ! (DateTime.MinValue==AV11TFContratoOcorrenciaNotificacao_Data_To) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] <= @AV11TFContratoOcorrenciaNotificacao_Data_To)";
         }
         else
         {
            GXv_int7[10] = 1;
         }
         if ( ! (0==AV12TFContratoOcorrenciaNotificacao_Prazo) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Prazo] >= @AV12TFContratoOcorrenciaNotificacao_Prazo)";
         }
         else
         {
            GXv_int7[11] = 1;
         }
         if ( ! (0==AV13TFContratoOcorrenciaNotificacao_Prazo_To) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Prazo] <= @AV13TFContratoOcorrenciaNotificacao_Prazo_To)";
         }
         else
         {
            GXv_int7[12] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFContratoOcorrenciaNotificacao_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFContratoOcorrenciaNotificacao_Descricao)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Descricao] like @lV14TFContratoOcorrenciaNotificacao_Descricao)";
         }
         else
         {
            GXv_int7[13] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFContratoOcorrenciaNotificacao_Descricao_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Descricao] = @AV15TFContratoOcorrenciaNotificacao_Descricao_Sel)";
         }
         else
         {
            GXv_int7[14] = 1;
         }
         if ( ! (DateTime.MinValue==AV16TFContratoOcorrenciaNotificacao_Cumprido) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Cumprido] >= @AV16TFContratoOcorrenciaNotificacao_Cumprido)";
         }
         else
         {
            GXv_int7[15] = 1;
         }
         if ( ! (DateTime.MinValue==AV17TFContratoOcorrenciaNotificacao_Cumprido_To) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Cumprido] <= @AV17TFContratoOcorrenciaNotificacao_Cumprido_To)";
         }
         else
         {
            GXv_int7[16] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV19TFContratoOcorrenciaNotificacao_Protocolo_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18TFContratoOcorrenciaNotificacao_Protocolo)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Protocolo] like @lV18TFContratoOcorrenciaNotificacao_Protocolo)";
         }
         else
         {
            GXv_int7[17] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19TFContratoOcorrenciaNotificacao_Protocolo_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Protocolo] = @AV19TFContratoOcorrenciaNotificacao_Protocolo_Sel)";
         }
         else
         {
            GXv_int7[18] = 1;
         }
         if ( ! (0==AV20TFContratoOcorrenciaNotificacao_Responsavel) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Responsavel] >= @AV20TFContratoOcorrenciaNotificacao_Responsavel)";
         }
         else
         {
            GXv_int7[19] = 1;
         }
         if ( ! (0==AV21TFContratoOcorrenciaNotificacao_Responsavel_To) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Responsavel] <= @AV21TFContratoOcorrenciaNotificacao_Responsavel_To)";
         }
         else
         {
            GXv_int7[20] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV23TFContratoOcorrenciaNotificacao_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV22TFContratoOcorrenciaNotificacao_Nome)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like @lV22TFContratoOcorrenciaNotificacao_Nome)";
         }
         else
         {
            GXv_int7[21] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23TFContratoOcorrenciaNotificacao_Nome_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Nome] = @AV23TFContratoOcorrenciaNotificacao_Nome_Sel)";
         }
         else
         {
            GXv_int7[22] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV25TFContratoOcorrenciaNotificacao_Docto_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV24TFContratoOcorrenciaNotificacao_Docto)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Docto] like @lV24TFContratoOcorrenciaNotificacao_Docto)";
         }
         else
         {
            GXv_int7[23] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV25TFContratoOcorrenciaNotificacao_Docto_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Docto] = @AV25TFContratoOcorrenciaNotificacao_Docto_Sel)";
         }
         else
         {
            GXv_int7[24] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[ContratoOcorrencia_Codigo], T2.[Pessoa_Docto]";
         GXv_Object8[0] = scmdbuf;
         GXv_Object8[1] = GXv_int7;
         return GXv_Object8 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00K62(context, (String)dynConstraints[0] , (DateTime)dynConstraints[1] , (DateTime)dynConstraints[2] , (short)dynConstraints[3] , (short)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (DateTime)dynConstraints[7] , (DateTime)dynConstraints[8] , (short)dynConstraints[9] , (short)dynConstraints[10] , (DateTime)dynConstraints[11] , (DateTime)dynConstraints[12] , (short)dynConstraints[13] , (short)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (DateTime)dynConstraints[17] , (DateTime)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (int)dynConstraints[21] , (int)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (DateTime)dynConstraints[27] , (short)dynConstraints[28] , (String)dynConstraints[29] , (DateTime)dynConstraints[30] , (String)dynConstraints[31] , (int)dynConstraints[32] , (String)dynConstraints[33] , (String)dynConstraints[34] , (int)dynConstraints[35] , (int)dynConstraints[36] );
               case 1 :
                     return conditional_P00K63(context, (String)dynConstraints[0] , (DateTime)dynConstraints[1] , (DateTime)dynConstraints[2] , (short)dynConstraints[3] , (short)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (DateTime)dynConstraints[7] , (DateTime)dynConstraints[8] , (short)dynConstraints[9] , (short)dynConstraints[10] , (DateTime)dynConstraints[11] , (DateTime)dynConstraints[12] , (short)dynConstraints[13] , (short)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (DateTime)dynConstraints[17] , (DateTime)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (int)dynConstraints[21] , (int)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (DateTime)dynConstraints[27] , (short)dynConstraints[28] , (String)dynConstraints[29] , (DateTime)dynConstraints[30] , (String)dynConstraints[31] , (int)dynConstraints[32] , (String)dynConstraints[33] , (String)dynConstraints[34] , (int)dynConstraints[35] , (int)dynConstraints[36] );
               case 2 :
                     return conditional_P00K64(context, (String)dynConstraints[0] , (DateTime)dynConstraints[1] , (DateTime)dynConstraints[2] , (short)dynConstraints[3] , (short)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (DateTime)dynConstraints[7] , (DateTime)dynConstraints[8] , (short)dynConstraints[9] , (short)dynConstraints[10] , (DateTime)dynConstraints[11] , (DateTime)dynConstraints[12] , (short)dynConstraints[13] , (short)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (DateTime)dynConstraints[17] , (DateTime)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (int)dynConstraints[21] , (int)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (DateTime)dynConstraints[27] , (short)dynConstraints[28] , (String)dynConstraints[29] , (DateTime)dynConstraints[30] , (String)dynConstraints[31] , (int)dynConstraints[32] , (String)dynConstraints[33] , (String)dynConstraints[34] , (int)dynConstraints[35] , (int)dynConstraints[36] );
               case 3 :
                     return conditional_P00K65(context, (String)dynConstraints[0] , (DateTime)dynConstraints[1] , (DateTime)dynConstraints[2] , (short)dynConstraints[3] , (short)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (DateTime)dynConstraints[7] , (DateTime)dynConstraints[8] , (short)dynConstraints[9] , (short)dynConstraints[10] , (DateTime)dynConstraints[11] , (DateTime)dynConstraints[12] , (short)dynConstraints[13] , (short)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (DateTime)dynConstraints[17] , (DateTime)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (int)dynConstraints[21] , (int)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (DateTime)dynConstraints[27] , (short)dynConstraints[28] , (String)dynConstraints[29] , (DateTime)dynConstraints[30] , (String)dynConstraints[31] , (int)dynConstraints[32] , (String)dynConstraints[33] , (String)dynConstraints[34] , (int)dynConstraints[35] , (int)dynConstraints[36] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00K62 ;
          prmP00K62 = new Object[] {
          new Object[] {"@AV55ContratoOcorrencia_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV45ContratoOcorrenciaNotificacao_Data1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV46ContratoOcorrenciaNotificacao_Data_To1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV47ContratoOcorrenciaNotificacao_Prazo1",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV48ContratoOcorrenciaNotificacao_Prazo_To1",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV51ContratoOcorrenciaNotificacao_Data2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV52ContratoOcorrenciaNotificacao_Data_To2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV53ContratoOcorrenciaNotificacao_Prazo2",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV54ContratoOcorrenciaNotificacao_Prazo_To2",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV10TFContratoOcorrenciaNotificacao_Data",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV11TFContratoOcorrenciaNotificacao_Data_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV12TFContratoOcorrenciaNotificacao_Prazo",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV13TFContratoOcorrenciaNotificacao_Prazo_To",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@lV14TFContratoOcorrenciaNotificacao_Descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV15TFContratoOcorrenciaNotificacao_Descricao_Sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV16TFContratoOcorrenciaNotificacao_Cumprido",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV17TFContratoOcorrenciaNotificacao_Cumprido_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV18TFContratoOcorrenciaNotificacao_Protocolo",SqlDbType.VarChar,20,0} ,
          new Object[] {"@AV19TFContratoOcorrenciaNotificacao_Protocolo_Sel",SqlDbType.VarChar,20,0} ,
          new Object[] {"@AV20TFContratoOcorrenciaNotificacao_Responsavel",SqlDbType.Int,6,0} ,
          new Object[] {"@AV21TFContratoOcorrenciaNotificacao_Responsavel_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV22TFContratoOcorrenciaNotificacao_Nome",SqlDbType.Char,100,0} ,
          new Object[] {"@AV23TFContratoOcorrenciaNotificacao_Nome_Sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV24TFContratoOcorrenciaNotificacao_Docto",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV25TFContratoOcorrenciaNotificacao_Docto_Sel",SqlDbType.VarChar,15,0}
          } ;
          Object[] prmP00K63 ;
          prmP00K63 = new Object[] {
          new Object[] {"@AV55ContratoOcorrencia_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV45ContratoOcorrenciaNotificacao_Data1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV46ContratoOcorrenciaNotificacao_Data_To1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV47ContratoOcorrenciaNotificacao_Prazo1",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV48ContratoOcorrenciaNotificacao_Prazo_To1",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV51ContratoOcorrenciaNotificacao_Data2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV52ContratoOcorrenciaNotificacao_Data_To2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV53ContratoOcorrenciaNotificacao_Prazo2",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV54ContratoOcorrenciaNotificacao_Prazo_To2",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV10TFContratoOcorrenciaNotificacao_Data",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV11TFContratoOcorrenciaNotificacao_Data_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV12TFContratoOcorrenciaNotificacao_Prazo",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV13TFContratoOcorrenciaNotificacao_Prazo_To",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@lV14TFContratoOcorrenciaNotificacao_Descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV15TFContratoOcorrenciaNotificacao_Descricao_Sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV16TFContratoOcorrenciaNotificacao_Cumprido",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV17TFContratoOcorrenciaNotificacao_Cumprido_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV18TFContratoOcorrenciaNotificacao_Protocolo",SqlDbType.VarChar,20,0} ,
          new Object[] {"@AV19TFContratoOcorrenciaNotificacao_Protocolo_Sel",SqlDbType.VarChar,20,0} ,
          new Object[] {"@AV20TFContratoOcorrenciaNotificacao_Responsavel",SqlDbType.Int,6,0} ,
          new Object[] {"@AV21TFContratoOcorrenciaNotificacao_Responsavel_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV22TFContratoOcorrenciaNotificacao_Nome",SqlDbType.Char,100,0} ,
          new Object[] {"@AV23TFContratoOcorrenciaNotificacao_Nome_Sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV24TFContratoOcorrenciaNotificacao_Docto",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV25TFContratoOcorrenciaNotificacao_Docto_Sel",SqlDbType.VarChar,15,0}
          } ;
          Object[] prmP00K64 ;
          prmP00K64 = new Object[] {
          new Object[] {"@AV55ContratoOcorrencia_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV45ContratoOcorrenciaNotificacao_Data1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV46ContratoOcorrenciaNotificacao_Data_To1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV47ContratoOcorrenciaNotificacao_Prazo1",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV48ContratoOcorrenciaNotificacao_Prazo_To1",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV51ContratoOcorrenciaNotificacao_Data2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV52ContratoOcorrenciaNotificacao_Data_To2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV53ContratoOcorrenciaNotificacao_Prazo2",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV54ContratoOcorrenciaNotificacao_Prazo_To2",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV10TFContratoOcorrenciaNotificacao_Data",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV11TFContratoOcorrenciaNotificacao_Data_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV12TFContratoOcorrenciaNotificacao_Prazo",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV13TFContratoOcorrenciaNotificacao_Prazo_To",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@lV14TFContratoOcorrenciaNotificacao_Descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV15TFContratoOcorrenciaNotificacao_Descricao_Sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV16TFContratoOcorrenciaNotificacao_Cumprido",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV17TFContratoOcorrenciaNotificacao_Cumprido_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV18TFContratoOcorrenciaNotificacao_Protocolo",SqlDbType.VarChar,20,0} ,
          new Object[] {"@AV19TFContratoOcorrenciaNotificacao_Protocolo_Sel",SqlDbType.VarChar,20,0} ,
          new Object[] {"@AV20TFContratoOcorrenciaNotificacao_Responsavel",SqlDbType.Int,6,0} ,
          new Object[] {"@AV21TFContratoOcorrenciaNotificacao_Responsavel_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV22TFContratoOcorrenciaNotificacao_Nome",SqlDbType.Char,100,0} ,
          new Object[] {"@AV23TFContratoOcorrenciaNotificacao_Nome_Sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV24TFContratoOcorrenciaNotificacao_Docto",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV25TFContratoOcorrenciaNotificacao_Docto_Sel",SqlDbType.VarChar,15,0}
          } ;
          Object[] prmP00K65 ;
          prmP00K65 = new Object[] {
          new Object[] {"@AV55ContratoOcorrencia_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV45ContratoOcorrenciaNotificacao_Data1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV46ContratoOcorrenciaNotificacao_Data_To1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV47ContratoOcorrenciaNotificacao_Prazo1",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV48ContratoOcorrenciaNotificacao_Prazo_To1",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV51ContratoOcorrenciaNotificacao_Data2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV52ContratoOcorrenciaNotificacao_Data_To2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV53ContratoOcorrenciaNotificacao_Prazo2",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV54ContratoOcorrenciaNotificacao_Prazo_To2",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV10TFContratoOcorrenciaNotificacao_Data",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV11TFContratoOcorrenciaNotificacao_Data_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV12TFContratoOcorrenciaNotificacao_Prazo",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV13TFContratoOcorrenciaNotificacao_Prazo_To",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@lV14TFContratoOcorrenciaNotificacao_Descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV15TFContratoOcorrenciaNotificacao_Descricao_Sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV16TFContratoOcorrenciaNotificacao_Cumprido",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV17TFContratoOcorrenciaNotificacao_Cumprido_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV18TFContratoOcorrenciaNotificacao_Protocolo",SqlDbType.VarChar,20,0} ,
          new Object[] {"@AV19TFContratoOcorrenciaNotificacao_Protocolo_Sel",SqlDbType.VarChar,20,0} ,
          new Object[] {"@AV20TFContratoOcorrenciaNotificacao_Responsavel",SqlDbType.Int,6,0} ,
          new Object[] {"@AV21TFContratoOcorrenciaNotificacao_Responsavel_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV22TFContratoOcorrenciaNotificacao_Nome",SqlDbType.Char,100,0} ,
          new Object[] {"@AV23TFContratoOcorrenciaNotificacao_Nome_Sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV24TFContratoOcorrenciaNotificacao_Docto",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV25TFContratoOcorrenciaNotificacao_Docto_Sel",SqlDbType.VarChar,15,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00K62", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00K62,100,0,true,false )
             ,new CursorDef("P00K63", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00K63,100,0,true,false )
             ,new CursorDef("P00K64", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00K64,100,0,true,false )
             ,new CursorDef("P00K65", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00K65,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getString(4, 100) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((String[]) buf[7])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((DateTime[]) buf[9])[0] = rslt.getGXDate(7) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((short[]) buf[11])[0] = rslt.getShort(8) ;
                ((DateTime[]) buf[12])[0] = rslt.getGXDate(9) ;
                ((int[]) buf[13])[0] = rslt.getInt(10) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getString(4, 100) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                ((DateTime[]) buf[8])[0] = rslt.getGXDate(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((String[]) buf[10])[0] = rslt.getVarchar(7) ;
                ((short[]) buf[11])[0] = rslt.getShort(8) ;
                ((DateTime[]) buf[12])[0] = rslt.getGXDate(9) ;
                ((int[]) buf[13])[0] = rslt.getInt(10) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getString(4, 100) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((DateTime[]) buf[8])[0] = rslt.getGXDate(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((String[]) buf[10])[0] = rslt.getVarchar(7) ;
                ((short[]) buf[11])[0] = rslt.getShort(8) ;
                ((DateTime[]) buf[12])[0] = rslt.getGXDate(9) ;
                ((int[]) buf[13])[0] = rslt.getInt(10) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((String[]) buf[6])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((DateTime[]) buf[8])[0] = rslt.getGXDate(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((String[]) buf[10])[0] = rslt.getVarchar(7) ;
                ((short[]) buf[11])[0] = rslt.getShort(8) ;
                ((DateTime[]) buf[12])[0] = rslt.getGXDate(9) ;
                ((int[]) buf[13])[0] = rslt.getInt(10) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[25]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[26]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[27]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[28]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[29]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[30]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[31]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[32]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[33]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[34]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[35]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[36]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[37]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[40]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[41]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[44]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[45]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[48]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[49]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[25]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[26]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[27]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[28]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[29]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[30]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[31]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[32]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[33]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[34]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[35]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[36]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[37]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[40]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[41]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[44]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[45]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[48]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[49]);
                }
                return;
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[25]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[26]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[27]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[28]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[29]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[30]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[31]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[32]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[33]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[34]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[35]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[36]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[37]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[40]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[41]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[44]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[45]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[48]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[49]);
                }
                return;
             case 3 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[25]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[26]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[27]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[28]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[29]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[30]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[31]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[32]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[33]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[34]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[35]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[36]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[37]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[40]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[41]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[44]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[45]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[48]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[49]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getcontratoocorrenciacontratoocorrencianotificacaowcfilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getcontratoocorrenciacontratoocorrencianotificacaowcfilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getcontratoocorrenciacontratoocorrencianotificacaowcfilterdata") )
          {
             return  ;
          }
          getcontratoocorrenciacontratoocorrencianotificacaowcfilterdata worker = new getcontratoocorrenciacontratoocorrencianotificacaowcfilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
