/*
               File: Check
        Description: Controle
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 23:9:22.38
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class check : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV7Check_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7Check_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Check_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCHECK_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7Check_Codigo), "ZZZZZ9")));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         cmbCheck_Momento.Name = "CHECK_MOMENTO";
         cmbCheck_Momento.WebTags = "";
         cmbCheck_Momento.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 4, 0)), "(Nenhum)", 0);
         cmbCheck_Momento.addItem("1", "Ao Iniciar uma Captura", 0);
         cmbCheck_Momento.addItem("2", "Ao Terminar uma Captura", 0);
         cmbCheck_Momento.addItem("3", "Ao Iniciar um Cancelamento", 0);
         cmbCheck_Momento.addItem("4", "Ao Terminar um Cancelamento", 0);
         cmbCheck_Momento.addItem("5", "Ao Iniciar uma Rejei��o", 0);
         cmbCheck_Momento.addItem("6", "Ao Terminar uma Rejei��o", 0);
         cmbCheck_Momento.addItem("7", "Ao Enviar para o Backlog", 0);
         cmbCheck_Momento.addItem("8", "Ao Sair do Backlog", 0);
         cmbCheck_Momento.addItem("9", "Ao Iniciar uma An�lise", 0);
         cmbCheck_Momento.addItem("10", "Ao Terminar uma An�lise", 0);
         cmbCheck_Momento.addItem("11", "Ao Iniciar uma Execu��o", 0);
         cmbCheck_Momento.addItem("12", "Ao Terminar uma Execu��o", 0);
         cmbCheck_Momento.addItem("13", "Ao Acatar uma Diverg�ncia", 0);
         cmbCheck_Momento.addItem("14", "Ao Homologar uma demanda", 0);
         cmbCheck_Momento.addItem("15", "Ao Liquidar uma demanda", 0);
         cmbCheck_Momento.addItem("16", "Ao dar o Aceite de uma Demanda", 0);
         if ( cmbCheck_Momento.ItemCount > 0 )
         {
            A1843Check_Momento = (short)(NumberUtil.Val( cmbCheck_Momento.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1843Check_Momento), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1843Check_Momento", StringUtil.LTrim( StringUtil.Str( (decimal)(A1843Check_Momento), 4, 0)));
         }
         cmbCheck_QdoNaoCmp.Name = "CHECK_QDONAOCMP";
         cmbCheck_QdoNaoCmp.WebTags = "";
         cmbCheck_QdoNaoCmp.addItem("0", "N�o proseguir", 0);
         cmbCheck_QdoNaoCmp.addItem("1", "Proseguir", 0);
         if ( cmbCheck_QdoNaoCmp.ItemCount > 0 )
         {
            A1856Check_QdoNaoCmp = (short)(NumberUtil.Val( cmbCheck_QdoNaoCmp.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1856Check_QdoNaoCmp), 4, 0))), "."));
            n1856Check_QdoNaoCmp = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1856Check_QdoNaoCmp", StringUtil.LTrim( StringUtil.Str( (decimal)(A1856Check_QdoNaoCmp), 4, 0)));
         }
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Controle", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtCheck_Nome_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public check( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public check( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           int aP1_Check_Codigo )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV7Check_Codigo = aP1_Check_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbCheck_Momento = new GXCombobox();
         cmbCheck_QdoNaoCmp = new GXCombobox();
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbCheck_Momento.ItemCount > 0 )
         {
            A1843Check_Momento = (short)(NumberUtil.Val( cmbCheck_Momento.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1843Check_Momento), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1843Check_Momento", StringUtil.LTrim( StringUtil.Str( (decimal)(A1843Check_Momento), 4, 0)));
         }
         if ( cmbCheck_QdoNaoCmp.ItemCount > 0 )
         {
            A1856Check_QdoNaoCmp = (short)(NumberUtil.Val( cmbCheck_QdoNaoCmp.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1856Check_QdoNaoCmp), 4, 0))), "."));
            n1856Check_QdoNaoCmp = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1856Check_QdoNaoCmp", StringUtil.LTrim( StringUtil.Str( (decimal)(A1856Check_QdoNaoCmp), 4, 0)));
         }
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_4M203( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_4M203e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtCheck_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1839Check_Codigo), 6, 0, ",", "")), ((edtCheck_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1839Check_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A1839Check_Codigo), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtCheck_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtCheck_Codigo_Visible, edtCheck_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_Check.htm");
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_4M203( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_4M203( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_4M203e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_31_4M203( true) ;
         }
         return  ;
      }

      protected void wb_table3_31_4M203e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_4M203e( true) ;
         }
         else
         {
            wb_table1_2_4M203e( false) ;
         }
      }

      protected void wb_table3_31_4M203( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 34,'',false,'',0)\"";
            ClassString = "btn btn-success";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", "Confirmar", bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_trn_enter_Visible, bttBtn_trn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_Check.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'',false,'',0)\"";
            ClassString = "btn btn-default";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "", "Fechar", bttBtn_trn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_trn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_Check.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'',false,'',0)\"";
            ClassString = "btn btn-default";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_delete_Internalname, "", "Eliminar", bttBtn_trn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_trn_delete_Visible, bttBtn_trn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_Check.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_31_4M203e( true) ;
         }
         else
         {
            wb_table3_31_4M203e( false) ;
         }
      }

      protected void wb_table2_5_4M203( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table4_13_4M203( true) ;
         }
         return  ;
      }

      protected void wb_table4_13_4M203e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_4M203e( true) ;
         }
         else
         {
            wb_table2_5_4M203e( false) ;
         }
      }

      protected void wb_table4_13_4M203( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableData", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcheck_nome_Internalname, "Nome", "", "", lblTextblockcheck_nome_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Check.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtCheck_Nome_Internalname, StringUtil.RTrim( A1841Check_Nome), StringUtil.RTrim( context.localUtil.Format( A1841Check_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,18);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtCheck_Nome_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtCheck_Nome_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_Check.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcheck_momento_Internalname, "Momento", "", "", lblTextblockcheck_momento_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Check.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbCheck_Momento, cmbCheck_Momento_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A1843Check_Momento), 4, 0)), 1, cmbCheck_Momento_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, cmbCheck_Momento.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,23);\"", "", true, "HLP_Check.htm");
            cmbCheck_Momento.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1843Check_Momento), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbCheck_Momento_Internalname, "Values", (String)(cmbCheck_Momento.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcheck_qdonaocmp_Internalname, "Se n�o cumpre", "", "", lblTextblockcheck_qdonaocmp_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Check.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 28,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbCheck_QdoNaoCmp, cmbCheck_QdoNaoCmp_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A1856Check_QdoNaoCmp), 4, 0)), 1, cmbCheck_QdoNaoCmp_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, cmbCheck_QdoNaoCmp.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,28);\"", "", true, "HLP_Check.htm");
            cmbCheck_QdoNaoCmp.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1856Check_QdoNaoCmp), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbCheck_QdoNaoCmp_Internalname, "Values", (String)(cmbCheck_QdoNaoCmp.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_13_4M203e( true) ;
         }
         else
         {
            wb_table4_13_4M203e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E114M2 */
         E114M2 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               A1841Check_Nome = StringUtil.Upper( cgiGet( edtCheck_Nome_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1841Check_Nome", A1841Check_Nome);
               cmbCheck_Momento.CurrentValue = cgiGet( cmbCheck_Momento_Internalname);
               A1843Check_Momento = (short)(NumberUtil.Val( cgiGet( cmbCheck_Momento_Internalname), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1843Check_Momento", StringUtil.LTrim( StringUtil.Str( (decimal)(A1843Check_Momento), 4, 0)));
               cmbCheck_QdoNaoCmp.CurrentValue = cgiGet( cmbCheck_QdoNaoCmp_Internalname);
               A1856Check_QdoNaoCmp = (short)(NumberUtil.Val( cgiGet( cmbCheck_QdoNaoCmp_Internalname), "."));
               n1856Check_QdoNaoCmp = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1856Check_QdoNaoCmp", StringUtil.LTrim( StringUtil.Str( (decimal)(A1856Check_QdoNaoCmp), 4, 0)));
               n1856Check_QdoNaoCmp = ((0==A1856Check_QdoNaoCmp) ? true : false);
               A1839Check_Codigo = (int)(context.localUtil.CToN( cgiGet( edtCheck_Codigo_Internalname), ",", "."));
               n1839Check_Codigo = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1839Check_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1839Check_Codigo), 6, 0)));
               /* Read saved values. */
               Z1839Check_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z1839Check_Codigo"), ",", "."));
               Z1840Check_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( "Z1840Check_AreaTrabalhoCod"), ",", "."));
               Z1841Check_Nome = cgiGet( "Z1841Check_Nome");
               Z1843Check_Momento = (short)(context.localUtil.CToN( cgiGet( "Z1843Check_Momento"), ",", "."));
               Z1856Check_QdoNaoCmp = (short)(context.localUtil.CToN( cgiGet( "Z1856Check_QdoNaoCmp"), ",", "."));
               n1856Check_QdoNaoCmp = ((0==A1856Check_QdoNaoCmp) ? true : false);
               A1840Check_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( "Z1840Check_AreaTrabalhoCod"), ",", "."));
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               AV7Check_Codigo = (int)(context.localUtil.CToN( cgiGet( "vCHECK_CODIGO"), ",", "."));
               Gx_BScreen = (short)(context.localUtil.CToN( cgiGet( "vGXBSCREEN"), ",", "."));
               A1840Check_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( "CHECK_AREATRABALHOCOD"), ",", "."));
               Gx_mode = cgiGet( "vMODE");
               Dvpanel_tableattributes_Width = cgiGet( "DVPANEL_TABLEATTRIBUTES_Width");
               Dvpanel_tableattributes_Height = cgiGet( "DVPANEL_TABLEATTRIBUTES_Height");
               Dvpanel_tableattributes_Cls = cgiGet( "DVPANEL_TABLEATTRIBUTES_Cls");
               Dvpanel_tableattributes_Title = cgiGet( "DVPANEL_TABLEATTRIBUTES_Title");
               Dvpanel_tableattributes_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsible"));
               Dvpanel_tableattributes_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsed"));
               Dvpanel_tableattributes_Enabled = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Enabled"));
               Dvpanel_tableattributes_Class = cgiGet( "DVPANEL_TABLEATTRIBUTES_Class");
               Dvpanel_tableattributes_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autowidth"));
               Dvpanel_tableattributes_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoheight"));
               Dvpanel_tableattributes_Showheader = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showheader"));
               Dvpanel_tableattributes_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showcollapseicon"));
               Dvpanel_tableattributes_Iconposition = cgiGet( "DVPANEL_TABLEATTRIBUTES_Iconposition");
               Dvpanel_tableattributes_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoscroll"));
               Dvpanel_tableattributes_Visible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Visible"));
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "Check";
               A1839Check_Codigo = (int)(context.localUtil.CToN( cgiGet( edtCheck_Codigo_Internalname), ",", "."));
               n1839Check_Codigo = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1839Check_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1839Check_Codigo), 6, 0)));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1839Check_Codigo), "ZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1840Check_AreaTrabalhoCod), "ZZZZZ9");
               hsh = cgiGet( "hsh");
               if ( ( ! ( ( A1839Check_Codigo != Z1839Check_Codigo ) ) || ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ) && ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("check:[SecurityCheckFailed value for]"+"Check_Codigo:"+context.localUtil.Format( (decimal)(A1839Check_Codigo), "ZZZZZ9"));
                  GXUtil.WriteLog("check:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GXUtil.WriteLog("check:[SecurityCheckFailed value for]"+"Check_AreaTrabalhoCod:"+context.localUtil.Format( (decimal)(A1840Check_AreaTrabalhoCod), "ZZZZZ9"));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  A1839Check_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  n1839Check_Codigo = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1839Check_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1839Check_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode203 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     Gx_mode = sMode203;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound203 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_4M0( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_trn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "CHECK_CODIGO");
                        AnyError = 1;
                        GX_FocusControl = edtCheck_Codigo_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E114M2 */
                           E114M2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E124M2 */
                           E124M2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E124M2 */
            E124M2 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll4M203( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         bttBtn_trn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtn_trn_delete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_trn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
            }
            DisableAttributes4M203( ) ;
         }
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_4M0( )
      {
         BeforeValidate4M203( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls4M203( ) ;
            }
            else
            {
               CheckExtendedTable4M203( ) ;
               CloseExtendedTableCursors4M203( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         }
      }

      protected void ResetCaption4M0( )
      {
      }

      protected void E114M2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
         edtCheck_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtCheck_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtCheck_Codigo_Visible), 5, 0)));
      }

      protected void E124M2( )
      {
         /* After Trn Routine */
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            context.wjLoc = formatLink("viewcheck.aspx") + "?" + UrlEncode("" +A1839Check_Codigo) + "," + UrlEncode(StringUtil.RTrim("Itens"));
            context.wjLocDisableFrm = 1;
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV9TrnContext.gxTpr_Callerondelete )
         {
            context.wjLoc = formatLink("wwcheck.aspx") ;
            context.wjLocDisableFrm = 1;
         }
         context.setWebReturnParms(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void ZM4M203( short GX_JID )
      {
         if ( ( GX_JID == 5 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z1840Check_AreaTrabalhoCod = T004M3_A1840Check_AreaTrabalhoCod[0];
               Z1841Check_Nome = T004M3_A1841Check_Nome[0];
               Z1843Check_Momento = T004M3_A1843Check_Momento[0];
               Z1856Check_QdoNaoCmp = T004M3_A1856Check_QdoNaoCmp[0];
            }
            else
            {
               Z1840Check_AreaTrabalhoCod = A1840Check_AreaTrabalhoCod;
               Z1841Check_Nome = A1841Check_Nome;
               Z1843Check_Momento = A1843Check_Momento;
               Z1856Check_QdoNaoCmp = A1856Check_QdoNaoCmp;
            }
         }
         if ( GX_JID == -5 )
         {
            Z1839Check_Codigo = A1839Check_Codigo;
            Z1840Check_AreaTrabalhoCod = A1840Check_AreaTrabalhoCod;
            Z1841Check_Nome = A1841Check_Nome;
            Z1843Check_Momento = A1843Check_Momento;
            Z1856Check_QdoNaoCmp = A1856Check_QdoNaoCmp;
         }
      }

      protected void standaloneNotModal( )
      {
         edtCheck_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtCheck_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtCheck_Codigo_Enabled), 5, 0)));
         Gx_BScreen = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         edtCheck_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtCheck_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtCheck_Codigo_Enabled), 5, 0)));
         bttBtn_trn_delete_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         if ( ! (0==AV7Check_Codigo) )
         {
            A1839Check_Codigo = AV7Check_Codigo;
            n1839Check_Codigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1839Check_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1839Check_Codigo), 6, 0)));
         }
      }

      protected void standaloneModal( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (0==A1840Check_AreaTrabalhoCod) && ( Gx_BScreen == 0 ) )
         {
            A1840Check_AreaTrabalhoCod = AV8WWPContext.gxTpr_Areatrabalho_codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1840Check_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1840Check_AreaTrabalhoCod), 6, 0)));
         }
      }

      protected void Load4M203( )
      {
         /* Using cursor T004M4 */
         pr_default.execute(2, new Object[] {n1839Check_Codigo, A1839Check_Codigo});
         if ( (pr_default.getStatus(2) != 101) )
         {
            RcdFound203 = 1;
            A1840Check_AreaTrabalhoCod = T004M4_A1840Check_AreaTrabalhoCod[0];
            A1841Check_Nome = T004M4_A1841Check_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1841Check_Nome", A1841Check_Nome);
            A1843Check_Momento = T004M4_A1843Check_Momento[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1843Check_Momento", StringUtil.LTrim( StringUtil.Str( (decimal)(A1843Check_Momento), 4, 0)));
            A1856Check_QdoNaoCmp = T004M4_A1856Check_QdoNaoCmp[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1856Check_QdoNaoCmp", StringUtil.LTrim( StringUtil.Str( (decimal)(A1856Check_QdoNaoCmp), 4, 0)));
            n1856Check_QdoNaoCmp = T004M4_n1856Check_QdoNaoCmp[0];
            ZM4M203( -5) ;
         }
         pr_default.close(2);
         OnLoadActions4M203( ) ;
      }

      protected void OnLoadActions4M203( )
      {
      }

      protected void CheckExtendedTable4M203( )
      {
         Gx_BScreen = 1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         standaloneModal( ) ;
         if ( ! ( ( A1843Check_Momento == 1 ) || ( A1843Check_Momento == 2 ) || ( A1843Check_Momento == 3 ) || ( A1843Check_Momento == 4 ) || ( A1843Check_Momento == 5 ) || ( A1843Check_Momento == 6 ) || ( A1843Check_Momento == 7 ) || ( A1843Check_Momento == 8 ) || ( A1843Check_Momento == 9 ) || ( A1843Check_Momento == 10 ) || ( A1843Check_Momento == 11 ) || ( A1843Check_Momento == 12 ) || ( A1843Check_Momento == 13 ) || ( A1843Check_Momento == 14 ) || ( A1843Check_Momento == 15 ) || ( A1843Check_Momento == 16 ) ) )
         {
            GX_msglist.addItem("Campo Momento fora do intervalo", "OutOfRange", 1, "CHECK_MOMENTO");
            AnyError = 1;
            GX_FocusControl = cmbCheck_Momento_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
      }

      protected void CloseExtendedTableCursors4M203( )
      {
      }

      protected void enableDisable( )
      {
      }

      protected void GetKey4M203( )
      {
         /* Using cursor T004M5 */
         pr_default.execute(3, new Object[] {n1839Check_Codigo, A1839Check_Codigo});
         if ( (pr_default.getStatus(3) != 101) )
         {
            RcdFound203 = 1;
         }
         else
         {
            RcdFound203 = 0;
         }
         pr_default.close(3);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T004M3 */
         pr_default.execute(1, new Object[] {n1839Check_Codigo, A1839Check_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM4M203( 5) ;
            RcdFound203 = 1;
            A1839Check_Codigo = T004M3_A1839Check_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1839Check_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1839Check_Codigo), 6, 0)));
            n1839Check_Codigo = T004M3_n1839Check_Codigo[0];
            A1840Check_AreaTrabalhoCod = T004M3_A1840Check_AreaTrabalhoCod[0];
            A1841Check_Nome = T004M3_A1841Check_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1841Check_Nome", A1841Check_Nome);
            A1843Check_Momento = T004M3_A1843Check_Momento[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1843Check_Momento", StringUtil.LTrim( StringUtil.Str( (decimal)(A1843Check_Momento), 4, 0)));
            A1856Check_QdoNaoCmp = T004M3_A1856Check_QdoNaoCmp[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1856Check_QdoNaoCmp", StringUtil.LTrim( StringUtil.Str( (decimal)(A1856Check_QdoNaoCmp), 4, 0)));
            n1856Check_QdoNaoCmp = T004M3_n1856Check_QdoNaoCmp[0];
            Z1839Check_Codigo = A1839Check_Codigo;
            sMode203 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load4M203( ) ;
            if ( AnyError == 1 )
            {
               RcdFound203 = 0;
               InitializeNonKey4M203( ) ;
            }
            Gx_mode = sMode203;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound203 = 0;
            InitializeNonKey4M203( ) ;
            sMode203 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal( ) ;
            Gx_mode = sMode203;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey4M203( ) ;
         if ( RcdFound203 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound203 = 0;
         /* Using cursor T004M6 */
         pr_default.execute(4, new Object[] {n1839Check_Codigo, A1839Check_Codigo});
         if ( (pr_default.getStatus(4) != 101) )
         {
            while ( (pr_default.getStatus(4) != 101) && ( ( T004M6_A1839Check_Codigo[0] < A1839Check_Codigo ) ) )
            {
               pr_default.readNext(4);
            }
            if ( (pr_default.getStatus(4) != 101) && ( ( T004M6_A1839Check_Codigo[0] > A1839Check_Codigo ) ) )
            {
               A1839Check_Codigo = T004M6_A1839Check_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1839Check_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1839Check_Codigo), 6, 0)));
               n1839Check_Codigo = T004M6_n1839Check_Codigo[0];
               RcdFound203 = 1;
            }
         }
         pr_default.close(4);
      }

      protected void move_previous( )
      {
         RcdFound203 = 0;
         /* Using cursor T004M7 */
         pr_default.execute(5, new Object[] {n1839Check_Codigo, A1839Check_Codigo});
         if ( (pr_default.getStatus(5) != 101) )
         {
            while ( (pr_default.getStatus(5) != 101) && ( ( T004M7_A1839Check_Codigo[0] > A1839Check_Codigo ) ) )
            {
               pr_default.readNext(5);
            }
            if ( (pr_default.getStatus(5) != 101) && ( ( T004M7_A1839Check_Codigo[0] < A1839Check_Codigo ) ) )
            {
               A1839Check_Codigo = T004M7_A1839Check_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1839Check_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1839Check_Codigo), 6, 0)));
               n1839Check_Codigo = T004M7_n1839Check_Codigo[0];
               RcdFound203 = 1;
            }
         }
         pr_default.close(5);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey4M203( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtCheck_Nome_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert4M203( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound203 == 1 )
            {
               if ( A1839Check_Codigo != Z1839Check_Codigo )
               {
                  A1839Check_Codigo = Z1839Check_Codigo;
                  n1839Check_Codigo = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1839Check_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1839Check_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "CHECK_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtCheck_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtCheck_Nome_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  Update4M203( ) ;
                  GX_FocusControl = edtCheck_Nome_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A1839Check_Codigo != Z1839Check_Codigo )
               {
                  /* Insert record */
                  GX_FocusControl = edtCheck_Nome_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert4M203( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "CHECK_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtCheck_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     /* Insert record */
                     GX_FocusControl = edtCheck_Nome_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert4M203( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( A1839Check_Codigo != Z1839Check_Codigo )
         {
            A1839Check_Codigo = Z1839Check_Codigo;
            n1839Check_Codigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1839Check_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1839Check_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "CHECK_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtCheck_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtCheck_Nome_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency4M203( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T004M2 */
            pr_default.execute(0, new Object[] {n1839Check_Codigo, A1839Check_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Check"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( Z1840Check_AreaTrabalhoCod != T004M2_A1840Check_AreaTrabalhoCod[0] ) || ( StringUtil.StrCmp(Z1841Check_Nome, T004M2_A1841Check_Nome[0]) != 0 ) || ( Z1843Check_Momento != T004M2_A1843Check_Momento[0] ) || ( Z1856Check_QdoNaoCmp != T004M2_A1856Check_QdoNaoCmp[0] ) )
            {
               if ( Z1840Check_AreaTrabalhoCod != T004M2_A1840Check_AreaTrabalhoCod[0] )
               {
                  GXUtil.WriteLog("check:[seudo value changed for attri]"+"Check_AreaTrabalhoCod");
                  GXUtil.WriteLogRaw("Old: ",Z1840Check_AreaTrabalhoCod);
                  GXUtil.WriteLogRaw("Current: ",T004M2_A1840Check_AreaTrabalhoCod[0]);
               }
               if ( StringUtil.StrCmp(Z1841Check_Nome, T004M2_A1841Check_Nome[0]) != 0 )
               {
                  GXUtil.WriteLog("check:[seudo value changed for attri]"+"Check_Nome");
                  GXUtil.WriteLogRaw("Old: ",Z1841Check_Nome);
                  GXUtil.WriteLogRaw("Current: ",T004M2_A1841Check_Nome[0]);
               }
               if ( Z1843Check_Momento != T004M2_A1843Check_Momento[0] )
               {
                  GXUtil.WriteLog("check:[seudo value changed for attri]"+"Check_Momento");
                  GXUtil.WriteLogRaw("Old: ",Z1843Check_Momento);
                  GXUtil.WriteLogRaw("Current: ",T004M2_A1843Check_Momento[0]);
               }
               if ( Z1856Check_QdoNaoCmp != T004M2_A1856Check_QdoNaoCmp[0] )
               {
                  GXUtil.WriteLog("check:[seudo value changed for attri]"+"Check_QdoNaoCmp");
                  GXUtil.WriteLogRaw("Old: ",Z1856Check_QdoNaoCmp);
                  GXUtil.WriteLogRaw("Current: ",T004M2_A1856Check_QdoNaoCmp[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"Check"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert4M203( )
      {
         BeforeValidate4M203( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable4M203( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM4M203( 0) ;
            CheckOptimisticConcurrency4M203( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm4M203( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert4M203( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T004M8 */
                     pr_default.execute(6, new Object[] {A1840Check_AreaTrabalhoCod, A1841Check_Nome, A1843Check_Momento, n1856Check_QdoNaoCmp, A1856Check_QdoNaoCmp});
                     A1839Check_Codigo = T004M8_A1839Check_Codigo[0];
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1839Check_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1839Check_Codigo), 6, 0)));
                     n1839Check_Codigo = T004M8_n1839Check_Codigo[0];
                     pr_default.close(6);
                     dsDefault.SmartCacheProvider.SetUpdated("Check") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption4M0( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load4M203( ) ;
            }
            EndLevel4M203( ) ;
         }
         CloseExtendedTableCursors4M203( ) ;
      }

      protected void Update4M203( )
      {
         BeforeValidate4M203( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable4M203( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency4M203( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm4M203( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate4M203( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T004M9 */
                     pr_default.execute(7, new Object[] {A1840Check_AreaTrabalhoCod, A1841Check_Nome, A1843Check_Momento, n1856Check_QdoNaoCmp, A1856Check_QdoNaoCmp, n1839Check_Codigo, A1839Check_Codigo});
                     pr_default.close(7);
                     dsDefault.SmartCacheProvider.SetUpdated("Check") ;
                     if ( (pr_default.getStatus(7) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Check"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate4M203( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel4M203( ) ;
         }
         CloseExtendedTableCursors4M203( ) ;
      }

      protected void DeferredUpdate4M203( )
      {
      }

      protected void delete( )
      {
         BeforeValidate4M203( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency4M203( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls4M203( ) ;
            AfterConfirm4M203( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete4M203( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T004M10 */
                  pr_default.execute(8, new Object[] {n1839Check_Codigo, A1839Check_Codigo});
                  pr_default.close(8);
                  dsDefault.SmartCacheProvider.SetUpdated("Check") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                        {
                           if ( AnyError == 0 )
                           {
                              context.nUserReturn = 1;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode203 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel4M203( ) ;
         Gx_mode = sMode203;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls4M203( )
      {
         standaloneModal( ) ;
         /* No delete mode formulas found. */
         if ( AnyError == 0 )
         {
            /* Using cursor T004M11 */
            pr_default.execute(9, new Object[] {n1839Check_Codigo, A1839Check_Codigo});
            if ( (pr_default.getStatus(9) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Check List"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(9);
            /* Using cursor T004M12 */
            pr_default.execute(10, new Object[] {n1839Check_Codigo, A1839Check_Codigo});
            if ( (pr_default.getStatus(10) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Check Lists"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(10);
         }
      }

      protected void EndLevel4M203( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete4M203( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            context.CommitDataStores( "Check");
            if ( AnyError == 0 )
            {
               ConfirmValues4M0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            context.RollbackDataStores( "Check");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart4M203( )
      {
         /* Scan By routine */
         /* Using cursor T004M13 */
         pr_default.execute(11);
         RcdFound203 = 0;
         if ( (pr_default.getStatus(11) != 101) )
         {
            RcdFound203 = 1;
            A1839Check_Codigo = T004M13_A1839Check_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1839Check_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1839Check_Codigo), 6, 0)));
            n1839Check_Codigo = T004M13_n1839Check_Codigo[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext4M203( )
      {
         /* Scan next routine */
         pr_default.readNext(11);
         RcdFound203 = 0;
         if ( (pr_default.getStatus(11) != 101) )
         {
            RcdFound203 = 1;
            A1839Check_Codigo = T004M13_A1839Check_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1839Check_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1839Check_Codigo), 6, 0)));
            n1839Check_Codigo = T004M13_n1839Check_Codigo[0];
         }
      }

      protected void ScanEnd4M203( )
      {
         pr_default.close(11);
      }

      protected void AfterConfirm4M203( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert4M203( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate4M203( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete4M203( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete4M203( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate4M203( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes4M203( )
      {
         edtCheck_Nome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtCheck_Nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtCheck_Nome_Enabled), 5, 0)));
         cmbCheck_Momento.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbCheck_Momento_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbCheck_Momento.Enabled), 5, 0)));
         cmbCheck_QdoNaoCmp.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbCheck_QdoNaoCmp_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbCheck_QdoNaoCmp.Enabled), 5, 0)));
         edtCheck_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtCheck_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtCheck_Codigo_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues4M0( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020428239233");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("check.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7Check_Codigo)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z1839Check_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1839Check_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1840Check_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1840Check_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1841Check_Nome", StringUtil.RTrim( Z1841Check_Nome));
         GxWebStd.gx_hidden_field( context, "Z1843Check_Momento", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1843Check_Momento), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1856Check_QdoNaoCmp", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1856Check_QdoNaoCmp), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV9TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV9TrnContext);
         }
         GxWebStd.gx_hidden_field( context, "vCHECK_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7Check_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGXBSCREEN", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gx_BScreen), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CHECK_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1840Check_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vCHECK_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7Check_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Width", StringUtil.RTrim( Dvpanel_tableattributes_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Cls", StringUtil.RTrim( Dvpanel_tableattributes_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Title", StringUtil.RTrim( Dvpanel_tableattributes_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsible", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsed", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Enabled", StringUtil.BoolToStr( Dvpanel_tableattributes_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autowidth", StringUtil.BoolToStr( Dvpanel_tableattributes_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoheight", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tableattributes_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Iconposition", StringUtil.RTrim( Dvpanel_tableattributes_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoscroll", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoscroll));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "Check";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1839Check_Codigo), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1840Check_AreaTrabalhoCod), "ZZZZZ9");
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("check:[SendSecurityCheck value for]"+"Check_Codigo:"+context.localUtil.Format( (decimal)(A1839Check_Codigo), "ZZZZZ9"));
         GXUtil.WriteLog("check:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
         GXUtil.WriteLog("check:[SendSecurityCheck value for]"+"Check_AreaTrabalhoCod:"+context.localUtil.Format( (decimal)(A1840Check_AreaTrabalhoCod), "ZZZZZ9"));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("check.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7Check_Codigo) ;
      }

      public override String GetPgmname( )
      {
         return "Check" ;
      }

      public override String GetPgmdesc( )
      {
         return "Controle" ;
      }

      protected void InitializeNonKey4M203( )
      {
         A1841Check_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1841Check_Nome", A1841Check_Nome);
         A1843Check_Momento = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1843Check_Momento", StringUtil.LTrim( StringUtil.Str( (decimal)(A1843Check_Momento), 4, 0)));
         A1856Check_QdoNaoCmp = 0;
         n1856Check_QdoNaoCmp = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1856Check_QdoNaoCmp", StringUtil.LTrim( StringUtil.Str( (decimal)(A1856Check_QdoNaoCmp), 4, 0)));
         n1856Check_QdoNaoCmp = ((0==A1856Check_QdoNaoCmp) ? true : false);
         A1840Check_AreaTrabalhoCod = AV8WWPContext.gxTpr_Areatrabalho_codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1840Check_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1840Check_AreaTrabalhoCod), 6, 0)));
         Z1840Check_AreaTrabalhoCod = 0;
         Z1841Check_Nome = "";
         Z1843Check_Momento = 0;
         Z1856Check_QdoNaoCmp = 0;
      }

      protected void InitAll4M203( )
      {
         A1839Check_Codigo = 0;
         n1839Check_Codigo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1839Check_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1839Check_Codigo), 6, 0)));
         InitializeNonKey4M203( ) ;
      }

      protected void StandaloneModalInsert( )
      {
         A1840Check_AreaTrabalhoCod = i1840Check_AreaTrabalhoCod;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1840Check_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1840Check_AreaTrabalhoCod), 6, 0)));
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?2249787");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20204282392318");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("check.js", "?20204282392318");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockcheck_nome_Internalname = "TEXTBLOCKCHECK_NOME";
         edtCheck_Nome_Internalname = "CHECK_NOME";
         lblTextblockcheck_momento_Internalname = "TEXTBLOCKCHECK_MOMENTO";
         cmbCheck_Momento_Internalname = "CHECK_MOMENTO";
         lblTextblockcheck_qdonaocmp_Internalname = "TEXTBLOCKCHECK_QDONAOCMP";
         cmbCheck_QdoNaoCmp_Internalname = "CHECK_QDONAOCMP";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         Dvpanel_tableattributes_Internalname = "DVPANEL_TABLEATTRIBUTES";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         bttBtn_trn_delete_Internalname = "BTN_TRN_DELETE";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         edtCheck_Codigo_Internalname = "CHECK_CODIGO";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Dvpanel_tableattributes_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Iconposition = "left";
         Dvpanel_tableattributes_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tableattributes_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Title = "Check List";
         Dvpanel_tableattributes_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tableattributes_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Controle";
         cmbCheck_QdoNaoCmp_Jsonclick = "";
         cmbCheck_QdoNaoCmp.Enabled = 1;
         cmbCheck_Momento_Jsonclick = "";
         cmbCheck_Momento.Enabled = 1;
         edtCheck_Nome_Jsonclick = "";
         edtCheck_Nome_Enabled = 1;
         bttBtn_trn_delete_Enabled = 0;
         bttBtn_trn_delete_Visible = 1;
         bttBtn_trn_cancel_Visible = 1;
         bttBtn_trn_enter_Enabled = 1;
         bttBtn_trn_enter_Visible = 1;
         edtCheck_Codigo_Jsonclick = "";
         edtCheck_Codigo_Enabled = 0;
         edtCheck_Codigo_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV7Check_Codigo',fld:'vCHECK_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E124M2',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'A1839Check_Codigo',fld:'CHECK_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV9TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         Z1841Check_Nome = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         bttBtn_trn_delete_Jsonclick = "";
         lblTextblockcheck_nome_Jsonclick = "";
         A1841Check_Nome = "";
         lblTextblockcheck_momento_Jsonclick = "";
         lblTextblockcheck_qdonaocmp_Jsonclick = "";
         Dvpanel_tableattributes_Height = "";
         Dvpanel_tableattributes_Class = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode203 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         T004M4_A1839Check_Codigo = new int[1] ;
         T004M4_n1839Check_Codigo = new bool[] {false} ;
         T004M4_A1840Check_AreaTrabalhoCod = new int[1] ;
         T004M4_A1841Check_Nome = new String[] {""} ;
         T004M4_A1843Check_Momento = new short[1] ;
         T004M4_A1856Check_QdoNaoCmp = new short[1] ;
         T004M4_n1856Check_QdoNaoCmp = new bool[] {false} ;
         T004M5_A1839Check_Codigo = new int[1] ;
         T004M5_n1839Check_Codigo = new bool[] {false} ;
         T004M3_A1839Check_Codigo = new int[1] ;
         T004M3_n1839Check_Codigo = new bool[] {false} ;
         T004M3_A1840Check_AreaTrabalhoCod = new int[1] ;
         T004M3_A1841Check_Nome = new String[] {""} ;
         T004M3_A1843Check_Momento = new short[1] ;
         T004M3_A1856Check_QdoNaoCmp = new short[1] ;
         T004M3_n1856Check_QdoNaoCmp = new bool[] {false} ;
         T004M6_A1839Check_Codigo = new int[1] ;
         T004M6_n1839Check_Codigo = new bool[] {false} ;
         T004M7_A1839Check_Codigo = new int[1] ;
         T004M7_n1839Check_Codigo = new bool[] {false} ;
         T004M2_A1839Check_Codigo = new int[1] ;
         T004M2_n1839Check_Codigo = new bool[] {false} ;
         T004M2_A1840Check_AreaTrabalhoCod = new int[1] ;
         T004M2_A1841Check_Nome = new String[] {""} ;
         T004M2_A1843Check_Momento = new short[1] ;
         T004M2_A1856Check_QdoNaoCmp = new short[1] ;
         T004M2_n1856Check_QdoNaoCmp = new bool[] {false} ;
         T004M8_A1839Check_Codigo = new int[1] ;
         T004M8_n1839Check_Codigo = new bool[] {false} ;
         T004M11_A758CheckList_Codigo = new int[1] ;
         T004M12_A155Servico_Codigo = new int[1] ;
         T004M12_A1839Check_Codigo = new int[1] ;
         T004M12_n1839Check_Codigo = new bool[] {false} ;
         T004M13_A1839Check_Codigo = new int[1] ;
         T004M13_n1839Check_Codigo = new bool[] {false} ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.check__default(),
            new Object[][] {
                new Object[] {
               T004M2_A1839Check_Codigo, T004M2_A1840Check_AreaTrabalhoCod, T004M2_A1841Check_Nome, T004M2_A1843Check_Momento, T004M2_A1856Check_QdoNaoCmp, T004M2_n1856Check_QdoNaoCmp
               }
               , new Object[] {
               T004M3_A1839Check_Codigo, T004M3_A1840Check_AreaTrabalhoCod, T004M3_A1841Check_Nome, T004M3_A1843Check_Momento, T004M3_A1856Check_QdoNaoCmp, T004M3_n1856Check_QdoNaoCmp
               }
               , new Object[] {
               T004M4_A1839Check_Codigo, T004M4_A1840Check_AreaTrabalhoCod, T004M4_A1841Check_Nome, T004M4_A1843Check_Momento, T004M4_A1856Check_QdoNaoCmp, T004M4_n1856Check_QdoNaoCmp
               }
               , new Object[] {
               T004M5_A1839Check_Codigo
               }
               , new Object[] {
               T004M6_A1839Check_Codigo
               }
               , new Object[] {
               T004M7_A1839Check_Codigo
               }
               , new Object[] {
               T004M8_A1839Check_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T004M11_A758CheckList_Codigo
               }
               , new Object[] {
               T004M12_A155Servico_Codigo, T004M12_A1839Check_Codigo
               }
               , new Object[] {
               T004M13_A1839Check_Codigo
               }
            }
         );
         Z1840Check_AreaTrabalhoCod = AV8WWPContext.gxTpr_Areatrabalho_codigo;
         A1840Check_AreaTrabalhoCod = AV8WWPContext.gxTpr_Areatrabalho_codigo;
         i1840Check_AreaTrabalhoCod = AV8WWPContext.gxTpr_Areatrabalho_codigo;
      }

      private short Z1843Check_Momento ;
      private short Z1856Check_QdoNaoCmp ;
      private short GxWebError ;
      private short gxcookieaux ;
      private short A1843Check_Momento ;
      private short A1856Check_QdoNaoCmp ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short Gx_BScreen ;
      private short RcdFound203 ;
      private short GX_JID ;
      private short gxajaxcallmode ;
      private int wcpOAV7Check_Codigo ;
      private int Z1839Check_Codigo ;
      private int Z1840Check_AreaTrabalhoCod ;
      private int AV7Check_Codigo ;
      private int trnEnded ;
      private int A1839Check_Codigo ;
      private int edtCheck_Codigo_Enabled ;
      private int edtCheck_Codigo_Visible ;
      private int bttBtn_trn_enter_Visible ;
      private int bttBtn_trn_enter_Enabled ;
      private int bttBtn_trn_cancel_Visible ;
      private int bttBtn_trn_delete_Visible ;
      private int bttBtn_trn_delete_Enabled ;
      private int edtCheck_Nome_Enabled ;
      private int A1840Check_AreaTrabalhoCod ;
      private int i1840Check_AreaTrabalhoCod ;
      private int idxLst ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String Z1841Check_Nome ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String Gx_mode ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtCheck_Nome_Internalname ;
      private String edtCheck_Codigo_Internalname ;
      private String edtCheck_Codigo_Jsonclick ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String bttBtn_trn_delete_Internalname ;
      private String bttBtn_trn_delete_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockcheck_nome_Internalname ;
      private String lblTextblockcheck_nome_Jsonclick ;
      private String A1841Check_Nome ;
      private String edtCheck_Nome_Jsonclick ;
      private String lblTextblockcheck_momento_Internalname ;
      private String lblTextblockcheck_momento_Jsonclick ;
      private String cmbCheck_Momento_Internalname ;
      private String cmbCheck_Momento_Jsonclick ;
      private String lblTextblockcheck_qdonaocmp_Internalname ;
      private String lblTextblockcheck_qdonaocmp_Jsonclick ;
      private String cmbCheck_QdoNaoCmp_Internalname ;
      private String cmbCheck_QdoNaoCmp_Jsonclick ;
      private String Dvpanel_tableattributes_Width ;
      private String Dvpanel_tableattributes_Height ;
      private String Dvpanel_tableattributes_Cls ;
      private String Dvpanel_tableattributes_Title ;
      private String Dvpanel_tableattributes_Class ;
      private String Dvpanel_tableattributes_Iconposition ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode203 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Dvpanel_tableattributes_Internalname ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool n1856Check_QdoNaoCmp ;
      private bool wbErr ;
      private bool n1839Check_Codigo ;
      private bool Dvpanel_tableattributes_Collapsible ;
      private bool Dvpanel_tableattributes_Collapsed ;
      private bool Dvpanel_tableattributes_Enabled ;
      private bool Dvpanel_tableattributes_Autowidth ;
      private bool Dvpanel_tableattributes_Autoheight ;
      private bool Dvpanel_tableattributes_Showheader ;
      private bool Dvpanel_tableattributes_Showcollapseicon ;
      private bool Dvpanel_tableattributes_Autoscroll ;
      private bool Dvpanel_tableattributes_Visible ;
      private bool returnInSub ;
      private IGxSession AV10WebSession ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbCheck_Momento ;
      private GXCombobox cmbCheck_QdoNaoCmp ;
      private IDataStoreProvider pr_default ;
      private int[] T004M4_A1839Check_Codigo ;
      private bool[] T004M4_n1839Check_Codigo ;
      private int[] T004M4_A1840Check_AreaTrabalhoCod ;
      private String[] T004M4_A1841Check_Nome ;
      private short[] T004M4_A1843Check_Momento ;
      private short[] T004M4_A1856Check_QdoNaoCmp ;
      private bool[] T004M4_n1856Check_QdoNaoCmp ;
      private int[] T004M5_A1839Check_Codigo ;
      private bool[] T004M5_n1839Check_Codigo ;
      private int[] T004M3_A1839Check_Codigo ;
      private bool[] T004M3_n1839Check_Codigo ;
      private int[] T004M3_A1840Check_AreaTrabalhoCod ;
      private String[] T004M3_A1841Check_Nome ;
      private short[] T004M3_A1843Check_Momento ;
      private short[] T004M3_A1856Check_QdoNaoCmp ;
      private bool[] T004M3_n1856Check_QdoNaoCmp ;
      private int[] T004M6_A1839Check_Codigo ;
      private bool[] T004M6_n1839Check_Codigo ;
      private int[] T004M7_A1839Check_Codigo ;
      private bool[] T004M7_n1839Check_Codigo ;
      private int[] T004M2_A1839Check_Codigo ;
      private bool[] T004M2_n1839Check_Codigo ;
      private int[] T004M2_A1840Check_AreaTrabalhoCod ;
      private String[] T004M2_A1841Check_Nome ;
      private short[] T004M2_A1843Check_Momento ;
      private short[] T004M2_A1856Check_QdoNaoCmp ;
      private bool[] T004M2_n1856Check_QdoNaoCmp ;
      private int[] T004M8_A1839Check_Codigo ;
      private bool[] T004M8_n1839Check_Codigo ;
      private int[] T004M11_A758CheckList_Codigo ;
      private int[] T004M12_A155Servico_Codigo ;
      private int[] T004M12_A1839Check_Codigo ;
      private bool[] T004M12_n1839Check_Codigo ;
      private int[] T004M13_A1839Check_Codigo ;
      private bool[] T004M13_n1839Check_Codigo ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
   }

   public class check__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new UpdateCursor(def[7])
         ,new UpdateCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT004M4 ;
          prmT004M4 = new Object[] {
          new Object[] {"@Check_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004M5 ;
          prmT004M5 = new Object[] {
          new Object[] {"@Check_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004M3 ;
          prmT004M3 = new Object[] {
          new Object[] {"@Check_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004M6 ;
          prmT004M6 = new Object[] {
          new Object[] {"@Check_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004M7 ;
          prmT004M7 = new Object[] {
          new Object[] {"@Check_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004M2 ;
          prmT004M2 = new Object[] {
          new Object[] {"@Check_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004M8 ;
          prmT004M8 = new Object[] {
          new Object[] {"@Check_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Check_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@Check_Momento",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Check_QdoNaoCmp",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT004M9 ;
          prmT004M9 = new Object[] {
          new Object[] {"@Check_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Check_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@Check_Momento",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Check_QdoNaoCmp",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Check_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004M10 ;
          prmT004M10 = new Object[] {
          new Object[] {"@Check_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004M11 ;
          prmT004M11 = new Object[] {
          new Object[] {"@Check_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004M12 ;
          prmT004M12 = new Object[] {
          new Object[] {"@Check_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004M13 ;
          prmT004M13 = new Object[] {
          } ;
          def= new CursorDef[] {
              new CursorDef("T004M2", "SELECT [Check_Codigo], [Check_AreaTrabalhoCod], [Check_Nome], [Check_Momento], [Check_QdoNaoCmp] FROM [Check] WITH (UPDLOCK) WHERE [Check_Codigo] = @Check_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004M2,1,0,true,false )
             ,new CursorDef("T004M3", "SELECT [Check_Codigo], [Check_AreaTrabalhoCod], [Check_Nome], [Check_Momento], [Check_QdoNaoCmp] FROM [Check] WITH (NOLOCK) WHERE [Check_Codigo] = @Check_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004M3,1,0,true,false )
             ,new CursorDef("T004M4", "SELECT TM1.[Check_Codigo], TM1.[Check_AreaTrabalhoCod], TM1.[Check_Nome], TM1.[Check_Momento], TM1.[Check_QdoNaoCmp] FROM [Check] TM1 WITH (NOLOCK) WHERE TM1.[Check_Codigo] = @Check_Codigo ORDER BY TM1.[Check_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT004M4,100,0,true,false )
             ,new CursorDef("T004M5", "SELECT [Check_Codigo] FROM [Check] WITH (NOLOCK) WHERE [Check_Codigo] = @Check_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT004M5,1,0,true,false )
             ,new CursorDef("T004M6", "SELECT TOP 1 [Check_Codigo] FROM [Check] WITH (NOLOCK) WHERE ( [Check_Codigo] > @Check_Codigo) ORDER BY [Check_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT004M6,1,0,true,true )
             ,new CursorDef("T004M7", "SELECT TOP 1 [Check_Codigo] FROM [Check] WITH (NOLOCK) WHERE ( [Check_Codigo] < @Check_Codigo) ORDER BY [Check_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT004M7,1,0,true,true )
             ,new CursorDef("T004M8", "INSERT INTO [Check]([Check_AreaTrabalhoCod], [Check_Nome], [Check_Momento], [Check_QdoNaoCmp]) VALUES(@Check_AreaTrabalhoCod, @Check_Nome, @Check_Momento, @Check_QdoNaoCmp); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT004M8)
             ,new CursorDef("T004M9", "UPDATE [Check] SET [Check_AreaTrabalhoCod]=@Check_AreaTrabalhoCod, [Check_Nome]=@Check_Nome, [Check_Momento]=@Check_Momento, [Check_QdoNaoCmp]=@Check_QdoNaoCmp  WHERE [Check_Codigo] = @Check_Codigo", GxErrorMask.GX_NOMASK,prmT004M9)
             ,new CursorDef("T004M10", "DELETE FROM [Check]  WHERE [Check_Codigo] = @Check_Codigo", GxErrorMask.GX_NOMASK,prmT004M10)
             ,new CursorDef("T004M11", "SELECT TOP 1 [CheckList_Codigo] FROM [CheckList] WITH (NOLOCK) WHERE [Check_Codigo] = @Check_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004M11,1,0,true,true )
             ,new CursorDef("T004M12", "SELECT TOP 1 [Servico_Codigo], [Check_Codigo] FROM [ServicoCheck] WITH (NOLOCK) WHERE [Check_Codigo] = @Check_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004M12,1,0,true,true )
             ,new CursorDef("T004M13", "SELECT [Check_Codigo] FROM [Check] WITH (NOLOCK) ORDER BY [Check_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT004M13,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 50) ;
                ((short[]) buf[3])[0] = rslt.getShort(4) ;
                ((short[]) buf[4])[0] = rslt.getShort(5) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(5);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 50) ;
                ((short[]) buf[3])[0] = rslt.getShort(4) ;
                ((short[]) buf[4])[0] = rslt.getShort(5) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(5);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 50) ;
                ((short[]) buf[3])[0] = rslt.getShort(4) ;
                ((short[]) buf[4])[0] = rslt.getShort(5) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(5);
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 1 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 3 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 4 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 5 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (short)parms[2]);
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 4 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(4, (short)parms[4]);
                }
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (short)parms[2]);
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 4 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(4, (short)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 5 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(5, (int)parms[6]);
                }
                return;
             case 8 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 9 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 10 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
       }
    }

 }

}
