/*
               File: WC_MultiplosStatus
        Description: Status
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/18/2020 12:40:42.33
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wc_multiplosstatus : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public wc_multiplosstatus( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public wc_multiplosstatus( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
         chkavSelected = new GXCheckbox();
         cmbavStatus = new GXCombobox();
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
               {
                  nRC_GXsfl_5 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  nGXsfl_5_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  sGXsfl_5_idx = GetNextPar( );
                  sPrefix = GetNextPar( );
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxnrGrid_newrow( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
               {
                  AV6Status = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavStatus_Internalname, AV6Status);
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vSTATUS"+"_"+sGXsfl_5_idx, GetSecureSignedToken( sPrefix+sGXsfl_5_idx, StringUtil.RTrim( context.localUtil.Format( AV6Status, ""))));
                  AV7StatusSelecionados = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7StatusSelecionados", AV7StatusSelecionados);
                  sPrefix = GetNextPar( );
                  init_default_properties( ) ;
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxgrGrid_refresh( AV6Status, AV7StatusSelecionados, sPrefix) ;
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
                  return  ;
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PAHU2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               context.Gx_err = 0;
               cmbavStatus.Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavStatus_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavStatus.Enabled), 5, 0)));
               WSHU2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Status") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202051812404237");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "white-space: nowrap;";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wc_multiplosstatus.aspx") +"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"nRC_GXsfl_5", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_5), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vSTATUSSELECIONADOS", StringUtil.RTrim( AV7StatusSelecionados));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormHU2( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("wc_multiplosstatus.js", "?202051812404238");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "WC_MultiplosStatus" ;
      }

      public override String GetPgmdesc( )
      {
         return "Status" ;
      }

      protected void WBHU0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "wc_multiplosstatus.aspx");
            }
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", sPrefix, "false");
            wb_table1_2_HU2( true) ;
         }
         else
         {
            wb_table1_2_HU2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_HU2e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void STARTHU2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Status", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPHU0( ) ;
            }
         }
      }

      protected void WSHU2( )
      {
         STARTHU2( ) ;
         EVTHU2( ) ;
      }

      protected void EVTHU2( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPHU0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPHU0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    GX_FocusControl = chkavSelected_Internalname;
                                    context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 4), "LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 15), "VSELECTED.CLICK") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 15), "VSELECTED.CLICK") == 0 ) )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPHU0( ) ;
                              }
                              nGXsfl_5_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_5_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_5_idx), 4, 0)), 4, "0");
                              SubsflControlProps_52( ) ;
                              AV5Selected = StringUtil.StrToBool( cgiGet( chkavSelected_Internalname));
                              context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, chkavSelected_Internalname, AV5Selected);
                              cmbavStatus.Name = cmbavStatus_Internalname;
                              cmbavStatus.CurrentValue = cgiGet( cmbavStatus_Internalname);
                              AV6Status = cgiGet( cmbavStatus_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavStatus_Internalname, AV6Status);
                              context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vSTATUS"+"_"+sGXsfl_5_idx, GetSecureSignedToken( sPrefix+sGXsfl_5_idx, StringUtil.RTrim( context.localUtil.Format( AV6Status, ""))));
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = chkavSelected_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E11HU2 */
                                          E11HU2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = chkavSelected_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E12HU2 */
                                          E12HU2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "VSELECTED.CLICK") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = chkavSelected_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E13HU2 */
                                          E13HU2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          if ( ! wbErr )
                                          {
                                             Rfr0gs = false;
                                             if ( ! Rfr0gs )
                                             {
                                             }
                                             dynload_actions( ) ;
                                          }
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = chkavSelected_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                                    {
                                       STRUPHU0( ) ;
                                    }
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = chkavSelected_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEHU2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormHU2( ) ;
            }
         }
      }

      protected void PAHU2( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            GXCCtl = "vSELECTED_" + sGXsfl_5_idx;
            chkavSelected.Name = GXCCtl;
            chkavSelected.WebTags = "";
            chkavSelected.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavSelected_Internalname, "TitleCaption", chkavSelected.Caption);
            chkavSelected.CheckedValue = "false";
            GXCCtl = "vSTATUS_" + sGXsfl_5_idx;
            cmbavStatus.Name = GXCCtl;
            cmbavStatus.WebTags = "";
            cmbavStatus.addItem("", "Todos", 0);
            cmbavStatus.addItem("B", "Stand by", 0);
            cmbavStatus.addItem("S", "Solicitada", 0);
            cmbavStatus.addItem("E", "Em An�lise", 0);
            cmbavStatus.addItem("A", "Em execu��o", 0);
            cmbavStatus.addItem("R", "Resolvida", 0);
            cmbavStatus.addItem("C", "Conferida", 0);
            cmbavStatus.addItem("D", "Retornada", 0);
            cmbavStatus.addItem("H", "Homologada", 0);
            cmbavStatus.addItem("O", "Aceite", 0);
            cmbavStatus.addItem("P", "A Pagar", 0);
            cmbavStatus.addItem("L", "Liquidada", 0);
            cmbavStatus.addItem("X", "Cancelada", 0);
            cmbavStatus.addItem("N", "N�o Faturada", 0);
            cmbavStatus.addItem("J", "Planejamento", 0);
            cmbavStatus.addItem("I", "An�lise Planejamento", 0);
            cmbavStatus.addItem("T", "Validacao T�cnica", 0);
            cmbavStatus.addItem("Q", "Validacao Qualidade", 0);
            cmbavStatus.addItem("G", "Em Homologa��o", 0);
            cmbavStatus.addItem("M", "Valida��o Mensura��o", 0);
            cmbavStatus.addItem("U", "Rascunho", 0);
            if ( cmbavStatus.ItemCount > 0 )
            {
               AV6Status = cmbavStatus.getValidValue(AV6Status);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavStatus_Internalname, AV6Status);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vSTATUS"+"_"+sGXsfl_5_idx, GetSecureSignedToken( sPrefix+sGXsfl_5_idx, StringUtil.RTrim( context.localUtil.Format( AV6Status, ""))));
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_52( ) ;
         while ( nGXsfl_5_idx <= nRC_GXsfl_5 )
         {
            sendrow_52( ) ;
            nGXsfl_5_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_5_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_5_idx+1));
            sGXsfl_5_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_5_idx), 4, 0)), 4, "0");
            SubsflControlProps_52( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( String AV6Status ,
                                       String AV7StatusSelecionados ,
                                       String sPrefix )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GRID_nCurrentRecord = 0;
         RFHU2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_vSTATUS", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( AV6Status, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"vSTATUS", StringUtil.RTrim( AV6Status));
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFHU2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
         cmbavStatus.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavStatus_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavStatus.Enabled), 5, 0)));
      }

      protected void RFHU2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 5;
         nGXsfl_5_idx = 1;
         sGXsfl_5_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_5_idx), 4, 0)), 4, "0");
         SubsflControlProps_52( ) ;
         nGXsfl_5_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", sPrefix);
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "Grid");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Width", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Width), 9, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_52( ) ;
            /* Execute user event: E12HU2 */
            E12HU2 ();
            wbEnd = 5;
            WBHU0( ) ;
         }
         nGXsfl_5_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         return (int)(-1) ;
      }

      protected int subGrid_Recordcount( )
      {
         return (int)(-1) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         return (int)(-1) ;
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(-1) ;
      }

      protected void STRUPHU0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         cmbavStatus.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavStatus_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavStatus.Enabled), 5, 0)));
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11HU2 */
         E11HU2 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            /* Read saved values. */
            nRC_GXsfl_5 = (short)(context.localUtil.CToN( cgiGet( sPrefix+"nRC_GXsfl_5"), ",", "."));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11HU2 */
         E11HU2 ();
         if (returnInSub) return;
      }

      protected void E11HU2( )
      {
         /* Start Routine */
         AV7StatusSelecionados = AV9WebSession.Get("Status");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7StatusSelecionados", AV7StatusSelecionados);
      }

      private void E12HU2( )
      {
         /* Load Routine */
         AV6Status = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavStatus_Internalname, AV6Status);
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vSTATUS"+"_"+sGXsfl_5_idx, GetSecureSignedToken( sPrefix+sGXsfl_5_idx, StringUtil.RTrim( context.localUtil.Format( AV6Status, ""))));
         /* Execute user subroutine: 'SELECIONADO' */
         S112 ();
         if (returnInSub) return;
         sendrow_52( ) ;
         if ( isFullAjaxMode( ) && ( nGXsfl_5_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(5, GridRow);
         }
         AV6Status = "U";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavStatus_Internalname, AV6Status);
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vSTATUS"+"_"+sGXsfl_5_idx, GetSecureSignedToken( sPrefix+sGXsfl_5_idx, StringUtil.RTrim( context.localUtil.Format( AV6Status, ""))));
         /* Execute user subroutine: 'SELECIONADO' */
         S112 ();
         if (returnInSub) return;
         sendrow_52( ) ;
         if ( isFullAjaxMode( ) && ( nGXsfl_5_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(5, GridRow);
         }
         AV6Status = "J";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavStatus_Internalname, AV6Status);
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vSTATUS"+"_"+sGXsfl_5_idx, GetSecureSignedToken( sPrefix+sGXsfl_5_idx, StringUtil.RTrim( context.localUtil.Format( AV6Status, ""))));
         /* Execute user subroutine: 'SELECIONADO' */
         S112 ();
         if (returnInSub) return;
         sendrow_52( ) ;
         if ( isFullAjaxMode( ) && ( nGXsfl_5_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(5, GridRow);
         }
         AV6Status = "S";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavStatus_Internalname, AV6Status);
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vSTATUS"+"_"+sGXsfl_5_idx, GetSecureSignedToken( sPrefix+sGXsfl_5_idx, StringUtil.RTrim( context.localUtil.Format( AV6Status, ""))));
         /* Execute user subroutine: 'SELECIONADO' */
         S112 ();
         if (returnInSub) return;
         sendrow_52( ) ;
         if ( isFullAjaxMode( ) && ( nGXsfl_5_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(5, GridRow);
         }
         AV6Status = "E";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavStatus_Internalname, AV6Status);
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vSTATUS"+"_"+sGXsfl_5_idx, GetSecureSignedToken( sPrefix+sGXsfl_5_idx, StringUtil.RTrim( context.localUtil.Format( AV6Status, ""))));
         /* Execute user subroutine: 'SELECIONADO' */
         S112 ();
         if (returnInSub) return;
         sendrow_52( ) ;
         if ( isFullAjaxMode( ) && ( nGXsfl_5_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(5, GridRow);
         }
         AV6Status = "A";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavStatus_Internalname, AV6Status);
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vSTATUS"+"_"+sGXsfl_5_idx, GetSecureSignedToken( sPrefix+sGXsfl_5_idx, StringUtil.RTrim( context.localUtil.Format( AV6Status, ""))));
         /* Execute user subroutine: 'SELECIONADO' */
         S112 ();
         if (returnInSub) return;
         sendrow_52( ) ;
         if ( isFullAjaxMode( ) && ( nGXsfl_5_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(5, GridRow);
         }
         AV6Status = "R";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavStatus_Internalname, AV6Status);
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vSTATUS"+"_"+sGXsfl_5_idx, GetSecureSignedToken( sPrefix+sGXsfl_5_idx, StringUtil.RTrim( context.localUtil.Format( AV6Status, ""))));
         /* Execute user subroutine: 'SELECIONADO' */
         S112 ();
         if (returnInSub) return;
         sendrow_52( ) ;
         if ( isFullAjaxMode( ) && ( nGXsfl_5_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(5, GridRow);
         }
         AV6Status = "C";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavStatus_Internalname, AV6Status);
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vSTATUS"+"_"+sGXsfl_5_idx, GetSecureSignedToken( sPrefix+sGXsfl_5_idx, StringUtil.RTrim( context.localUtil.Format( AV6Status, ""))));
         /* Execute user subroutine: 'SELECIONADO' */
         S112 ();
         if (returnInSub) return;
         sendrow_52( ) ;
         if ( isFullAjaxMode( ) && ( nGXsfl_5_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(5, GridRow);
         }
         AV6Status = "M";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavStatus_Internalname, AV6Status);
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vSTATUS"+"_"+sGXsfl_5_idx, GetSecureSignedToken( sPrefix+sGXsfl_5_idx, StringUtil.RTrim( context.localUtil.Format( AV6Status, ""))));
         /* Execute user subroutine: 'SELECIONADO' */
         S112 ();
         if (returnInSub) return;
         sendrow_52( ) ;
         if ( isFullAjaxMode( ) && ( nGXsfl_5_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(5, GridRow);
         }
         AV6Status = "Q";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavStatus_Internalname, AV6Status);
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vSTATUS"+"_"+sGXsfl_5_idx, GetSecureSignedToken( sPrefix+sGXsfl_5_idx, StringUtil.RTrim( context.localUtil.Format( AV6Status, ""))));
         /* Execute user subroutine: 'SELECIONADO' */
         S112 ();
         if (returnInSub) return;
         sendrow_52( ) ;
         if ( isFullAjaxMode( ) && ( nGXsfl_5_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(5, GridRow);
         }
         AV6Status = "T";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavStatus_Internalname, AV6Status);
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vSTATUS"+"_"+sGXsfl_5_idx, GetSecureSignedToken( sPrefix+sGXsfl_5_idx, StringUtil.RTrim( context.localUtil.Format( AV6Status, ""))));
         /* Execute user subroutine: 'SELECIONADO' */
         S112 ();
         if (returnInSub) return;
         sendrow_52( ) ;
         if ( isFullAjaxMode( ) && ( nGXsfl_5_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(5, GridRow);
         }
         AV6Status = "H";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavStatus_Internalname, AV6Status);
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vSTATUS"+"_"+sGXsfl_5_idx, GetSecureSignedToken( sPrefix+sGXsfl_5_idx, StringUtil.RTrim( context.localUtil.Format( AV6Status, ""))));
         /* Execute user subroutine: 'SELECIONADO' */
         S112 ();
         if (returnInSub) return;
         sendrow_52( ) ;
         if ( isFullAjaxMode( ) && ( nGXsfl_5_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(5, GridRow);
         }
         AV6Status = "O";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavStatus_Internalname, AV6Status);
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vSTATUS"+"_"+sGXsfl_5_idx, GetSecureSignedToken( sPrefix+sGXsfl_5_idx, StringUtil.RTrim( context.localUtil.Format( AV6Status, ""))));
         /* Execute user subroutine: 'SELECIONADO' */
         S112 ();
         if (returnInSub) return;
         sendrow_52( ) ;
         if ( isFullAjaxMode( ) && ( nGXsfl_5_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(5, GridRow);
         }
         AV6Status = "P";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavStatus_Internalname, AV6Status);
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vSTATUS"+"_"+sGXsfl_5_idx, GetSecureSignedToken( sPrefix+sGXsfl_5_idx, StringUtil.RTrim( context.localUtil.Format( AV6Status, ""))));
         /* Execute user subroutine: 'SELECIONADO' */
         S112 ();
         if (returnInSub) return;
         sendrow_52( ) ;
         if ( isFullAjaxMode( ) && ( nGXsfl_5_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(5, GridRow);
         }
         AV6Status = "L";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavStatus_Internalname, AV6Status);
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vSTATUS"+"_"+sGXsfl_5_idx, GetSecureSignedToken( sPrefix+sGXsfl_5_idx, StringUtil.RTrim( context.localUtil.Format( AV6Status, ""))));
         /* Execute user subroutine: 'SELECIONADO' */
         S112 ();
         if (returnInSub) return;
         sendrow_52( ) ;
         if ( isFullAjaxMode( ) && ( nGXsfl_5_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(5, GridRow);
         }
         AV6Status = "D";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavStatus_Internalname, AV6Status);
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vSTATUS"+"_"+sGXsfl_5_idx, GetSecureSignedToken( sPrefix+sGXsfl_5_idx, StringUtil.RTrim( context.localUtil.Format( AV6Status, ""))));
         /* Execute user subroutine: 'SELECIONADO' */
         S112 ();
         if (returnInSub) return;
         sendrow_52( ) ;
         if ( isFullAjaxMode( ) && ( nGXsfl_5_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(5, GridRow);
         }
         AV6Status = "X";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavStatus_Internalname, AV6Status);
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vSTATUS"+"_"+sGXsfl_5_idx, GetSecureSignedToken( sPrefix+sGXsfl_5_idx, StringUtil.RTrim( context.localUtil.Format( AV6Status, ""))));
         /* Execute user subroutine: 'SELECIONADO' */
         S112 ();
         if (returnInSub) return;
         sendrow_52( ) ;
         if ( isFullAjaxMode( ) && ( nGXsfl_5_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(5, GridRow);
         }
         AV6Status = "B";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavStatus_Internalname, AV6Status);
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vSTATUS"+"_"+sGXsfl_5_idx, GetSecureSignedToken( sPrefix+sGXsfl_5_idx, StringUtil.RTrim( context.localUtil.Format( AV6Status, ""))));
         /* Execute user subroutine: 'SELECIONADO' */
         S112 ();
         if (returnInSub) return;
         sendrow_52( ) ;
         if ( isFullAjaxMode( ) && ( nGXsfl_5_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(5, GridRow);
         }
      }

      protected void E13HU2( )
      {
         /* Selected_Click Routine */
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV6Status)) )
         {
            if ( AV5Selected )
            {
               AV7StatusSelecionados = "*";
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7StatusSelecionados", AV7StatusSelecionados);
               /* Start For Each Line */
               nRC_GXsfl_5 = (short)(context.localUtil.CToN( cgiGet( sPrefix+"nRC_GXsfl_5"), ",", "."));
               nGXsfl_5_fel_idx = 0;
               while ( nGXsfl_5_fel_idx < nRC_GXsfl_5 )
               {
                  nGXsfl_5_fel_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_5_fel_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_5_fel_idx+1));
                  sGXsfl_5_fel_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_5_fel_idx), 4, 0)), 4, "0");
                  SubsflControlProps_fel_52( ) ;
                  AV5Selected = StringUtil.StrToBool( cgiGet( chkavSelected_Internalname));
                  cmbavStatus.Name = cmbavStatus_Internalname;
                  cmbavStatus.CurrentValue = cgiGet( cmbavStatus_Internalname);
                  AV6Status = cgiGet( cmbavStatus_Internalname);
                  if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV6Status)) )
                  {
                     AV5Selected = false;
                     context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, chkavSelected_Internalname, AV5Selected);
                     chkavSelected.Visible = 0;
                     context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavSelected_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavSelected.Visible), 5, 0)));
                  }
                  /* End For Each Line */
               }
               if ( nGXsfl_5_fel_idx == 0 )
               {
                  nGXsfl_5_idx = 1;
                  sGXsfl_5_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_5_idx), 4, 0)), 4, "0");
                  SubsflControlProps_52( ) ;
               }
               nGXsfl_5_fel_idx = 1;
            }
            else
            {
               AV7StatusSelecionados = "";
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7StatusSelecionados", AV7StatusSelecionados);
               gxgrGrid_refresh( AV6Status, AV7StatusSelecionados, sPrefix) ;
            }
         }
         else
         {
            AV10Valor = AV6Status;
            if ( AV5Selected )
            {
               AV7StatusSelecionados = AV7StatusSelecionados + AV10Valor;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7StatusSelecionados", AV7StatusSelecionados);
            }
            else
            {
               AV7StatusSelecionados = StringUtil.StringReplace( AV7StatusSelecionados, AV10Valor, "");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7StatusSelecionados", AV7StatusSelecionados);
            }
         }
         AV9WebSession.Set("Status", AV7StatusSelecionados);
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7StatusSelecionados", AV7StatusSelecionados);
      }

      protected void S112( )
      {
         /* 'SELECIONADO' Routine */
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV6Status)) )
         {
            AV10Valor = "*";
            AV5Selected = (bool)(((StringUtil.StringSearch( AV7StatusSelecionados, AV10Valor, 1)>0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, chkavSelected_Internalname, AV5Selected);
         }
         else
         {
            if ( StringUtil.StrCmp(AV7StatusSelecionados, "*") == 0 )
            {
               AV5Selected = false;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, chkavSelected_Internalname, AV5Selected);
               chkavSelected.Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavSelected_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavSelected.Visible), 5, 0)));
            }
            else
            {
               AV10Valor = AV6Status;
               AV5Selected = (bool)(((StringUtil.StringSearch( AV7StatusSelecionados, AV10Valor, 1)>0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, chkavSelected_Internalname, AV5Selected);
            }
         }
      }

      protected void wb_table1_2_HU2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblTable_Internalname, tblTable_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\">") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"DivS\" data-gxgridid=\"5\">") ;
               sStyleString = "";
               sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "Grid", 0, "", "", 1, 2, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(30), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((chkavSelected.Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(137), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "Grid");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Width", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Width), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", sPrefix);
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.BoolToStr( AV5Selected));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(chkavSelected.Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( AV6Status));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbavStatus.Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 5 )
         {
            wbEnd = 0;
            nRC_GXsfl_5 = (short)(nGXsfl_5_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid(sPrefix+"_"+"Grid", GridContainer);
               if ( ! isAjaxCallMode( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+sPrefix+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_HU2e( true) ;
         }
         else
         {
            wb_table1_2_HU2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAHU2( ) ;
         WSHU2( ) ;
         WEHU2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PAHU2( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "wc_multiplosstatus");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PAHU2( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
         }
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PAHU2( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSHU2( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSHU2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WEHU2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202051812404266");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("wc_multiplosstatus.js", "?202051812404266");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_52( )
      {
         chkavSelected_Internalname = sPrefix+"vSELECTED_"+sGXsfl_5_idx;
         cmbavStatus_Internalname = sPrefix+"vSTATUS_"+sGXsfl_5_idx;
      }

      protected void SubsflControlProps_fel_52( )
      {
         chkavSelected_Internalname = sPrefix+"vSELECTED_"+sGXsfl_5_fel_idx;
         cmbavStatus_Internalname = sPrefix+"vSTATUS_"+sGXsfl_5_fel_idx;
      }

      protected void sendrow_52( )
      {
         SubsflControlProps_52( ) ;
         WBHU0( ) ;
         GridRow = GXWebRow.GetNew(context,GridContainer);
         if ( subGrid_Backcolorstyle == 0 )
         {
            /* None style subfile background logic. */
            subGrid_Backstyle = 0;
            if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
            {
               subGrid_Linesclass = subGrid_Class+"Odd";
            }
         }
         else if ( subGrid_Backcolorstyle == 1 )
         {
            /* Uniform style subfile background logic. */
            subGrid_Backstyle = 0;
            subGrid_Backcolor = subGrid_Allbackcolor;
            if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
            {
               subGrid_Linesclass = subGrid_Class+"Uniform";
            }
         }
         else if ( subGrid_Backcolorstyle == 2 )
         {
            /* Header style subfile background logic. */
            subGrid_Backstyle = 1;
            if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
            {
               subGrid_Linesclass = subGrid_Class+"Odd";
            }
            subGrid_Backcolor = (int)(0x0);
         }
         else if ( subGrid_Backcolorstyle == 3 )
         {
            /* Report style subfile background logic. */
            subGrid_Backstyle = 1;
            if ( ((int)((nGXsfl_5_idx) % (2))) == 0 )
            {
               subGrid_Backcolor = (int)(0x0);
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Even";
               }
            }
            else
            {
               subGrid_Backcolor = (int)(0x0);
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
         }
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<tr ") ;
            context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
            context.WriteHtmlText( " gxrow=\""+sGXsfl_5_idx+"\">") ;
         }
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((chkavSelected.Visible==0) ? "display:none;" : "")+"\">") ;
         }
         /* Check box */
         TempTags = " " + ((chkavSelected.Enabled!=0)&&(chkavSelected.Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 6,'"+sPrefix+"',false,'"+sGXsfl_5_idx+"',5)\"" : " ");
         ClassString = "Attribute";
         StyleString = "";
         GridRow.AddColumnProperties("checkbox", 1, isAjaxCallMode( ), new Object[] {(String)chkavSelected_Internalname,StringUtil.BoolToStr( AV5Selected),(String)"",(String)"",chkavSelected.Visible,(short)1,(String)"true",(String)"",(String)StyleString,(String)ClassString,(String)"",TempTags+((chkavSelected.Enabled!=0)&&(chkavSelected.Visible!=0) ? " onclick=\"gx.fn.checkboxClick(6, this, 'true', 'false');gx.ajax.executeCliEvent('e13hu2_client',this, event);gx.evt.onchange(this);\" " : " ")+((chkavSelected.Enabled!=0)&&(chkavSelected.Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,6);\"" : " ")});
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
         }
         TempTags = " " + ((cmbavStatus.Enabled!=0)&&(cmbavStatus.Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 7,'"+sPrefix+"',false,'"+sGXsfl_5_idx+"',5)\"" : " ");
         if ( ( nGXsfl_5_idx == 1 ) && isAjaxCallMode( ) )
         {
            GXCCtl = "vSTATUS_" + sGXsfl_5_idx;
            cmbavStatus.Name = GXCCtl;
            cmbavStatus.WebTags = "";
            cmbavStatus.addItem("", "Todos", 0);
            cmbavStatus.addItem("B", "Stand by", 0);
            cmbavStatus.addItem("S", "Solicitada", 0);
            cmbavStatus.addItem("E", "Em An�lise", 0);
            cmbavStatus.addItem("A", "Em execu��o", 0);
            cmbavStatus.addItem("R", "Resolvida", 0);
            cmbavStatus.addItem("C", "Conferida", 0);
            cmbavStatus.addItem("D", "Retornada", 0);
            cmbavStatus.addItem("H", "Homologada", 0);
            cmbavStatus.addItem("O", "Aceite", 0);
            cmbavStatus.addItem("P", "A Pagar", 0);
            cmbavStatus.addItem("L", "Liquidada", 0);
            cmbavStatus.addItem("X", "Cancelada", 0);
            cmbavStatus.addItem("N", "N�o Faturada", 0);
            cmbavStatus.addItem("J", "Planejamento", 0);
            cmbavStatus.addItem("I", "An�lise Planejamento", 0);
            cmbavStatus.addItem("T", "Validacao T�cnica", 0);
            cmbavStatus.addItem("Q", "Validacao Qualidade", 0);
            cmbavStatus.addItem("G", "Em Homologa��o", 0);
            cmbavStatus.addItem("M", "Valida��o Mensura��o", 0);
            cmbavStatus.addItem("U", "Rascunho", 0);
            if ( cmbavStatus.ItemCount > 0 )
            {
               AV6Status = cmbavStatus.getValidValue(AV6Status);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavStatus_Internalname, AV6Status);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_vSTATUS"+"_"+sGXsfl_5_idx, GetSecureSignedToken( sPrefix+sGXsfl_5_idx, StringUtil.RTrim( context.localUtil.Format( AV6Status, ""))));
            }
         }
         /* ComboBox */
         GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbavStatus,(String)cmbavStatus_Internalname,StringUtil.RTrim( AV6Status),(short)1,(String)cmbavStatus_Jsonclick,(short)0,(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"char",(String)"",(short)-1,cmbavStatus.Enabled,(short)0,(short)0,(short)137,(String)"px",(short)0,(String)"px",(String)"",(String)"Attribute",(String)"",TempTags+((cmbavStatus.Enabled!=0)&&(cmbavStatus.Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((cmbavStatus.Enabled!=0)&&(cmbavStatus.Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,7);\"" : " "),(String)"",(bool)true});
         cmbavStatus.CurrentValue = StringUtil.RTrim( AV6Status);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavStatus_Internalname, "Values", (String)(cmbavStatus.ToJavascriptSource()));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_vSTATUS"+"_"+sGXsfl_5_idx, GetSecureSignedToken( sPrefix+sGXsfl_5_idx, StringUtil.RTrim( context.localUtil.Format( AV6Status, ""))));
         GridContainer.AddRow(GridRow);
         nGXsfl_5_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_5_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_5_idx+1));
         sGXsfl_5_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_5_idx), 4, 0)), 4, "0");
         SubsflControlProps_52( ) ;
         /* End function sendrow_52 */
      }

      protected void init_default_properties( )
      {
         chkavSelected_Internalname = sPrefix+"vSELECTED";
         cmbavStatus_Internalname = sPrefix+"vSTATUS";
         tblTable_Internalname = sPrefix+"TABLE";
         Form.Internalname = sPrefix+"FORM";
         subGrid_Internalname = sPrefix+"GRID";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         cmbavStatus_Jsonclick = "";
         cmbavStatus.Visible = -1;
         chkavSelected.Enabled = 1;
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         cmbavStatus.Enabled = 1;
         subGrid_Class = "Grid";
         chkavSelected.Visible = -1;
         subGrid_Width = 100;
         subGrid_Backcolorstyle = 0;
         chkavSelected.Caption = "";
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'AV6Status',fld:'vSTATUS',pic:'',hsh:true,nv:''},{av:'AV7StatusSelecionados',fld:'vSTATUSSELECIONADOS',pic:'',nv:''},{av:'sPrefix',nv:''}],oparms:[]}");
         setEventMetadata("VSELECTED.CLICK","{handler:'E13HU2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'AV6Status',fld:'vSTATUS',grid:5,pic:'',hsh:true,nv:''},{av:'AV7StatusSelecionados',fld:'vSTATUSSELECIONADOS',pic:'',nv:''},{av:'sPrefix',nv:''},{av:'AV5Selected',fld:'vSELECTED',grid:5,pic:'',nv:false}],oparms:[{av:'AV5Selected',fld:'vSELECTED',pic:'',nv:false},{av:'chkavSelected.Visible',ctrl:'vSELECTED',prop:'Visible'},{av:'AV7StatusSelecionados',fld:'vSTATUSSELECIONADOS',pic:'',nv:''}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV6Status = "";
         AV7StatusSelecionados = "";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         GXCCtl = "";
         GridContainer = new GXWebGrid( context);
         AV9WebSession = context.GetSession();
         GridRow = new GXWebRow();
         AV10Valor = "";
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         TempTags = "";
         ClassString = "";
         StyleString = "";
         /* GeneXus formulas. */
         context.Gx_err = 0;
         cmbavStatus.Enabled = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short nRC_GXsfl_5 ;
      private short nGXsfl_5_idx=1 ;
      private short initialized ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_5_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short nGXsfl_5_fel_idx=1 ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private short GRID_nEOF ;
      private int subGrid_Islastpage ;
      private int subGrid_Width ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nCurrentRecord ;
      private long GRID_nFirstRecordOnPage ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String sGXsfl_5_idx="0001" ;
      private String AV6Status ;
      private String cmbavStatus_Internalname ;
      private String AV7StatusSelecionados ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GX_FocusControl ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String chkavSelected_Internalname ;
      private String GXCCtl ;
      private String sGXsfl_5_fel_idx="0001" ;
      private String AV10Valor ;
      private String sStyleString ;
      private String tblTable_Internalname ;
      private String subGrid_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String cmbavStatus_Jsonclick ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool AV5Selected ;
      private bool returnInSub ;
      private IGxSession AV9WebSession ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCheckbox chkavSelected ;
      private GXCombobox cmbavStatus ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
   }

}
