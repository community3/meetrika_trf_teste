/*
               File: Projeto_BC
        Description: Projeto
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/29/2020 9:30:19.32
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class projeto_bc : GXHttpHandler, IGxSilentTrn
   {
      public projeto_bc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public projeto_bc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      protected void INITTRN( )
      {
      }

      public void GetInsDefault( )
      {
         ReadRow2986( ) ;
         standaloneNotModal( ) ;
         InitializeNonKey2986( ) ;
         standaloneModal( ) ;
         AddRow2986( ) ;
         Gx_mode = "INS";
         return  ;
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E11292 */
            E11292 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               Z648Projeto_Codigo = A648Projeto_Codigo;
               SetMode( "UPD") ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      public bool Reindex( )
      {
         return true ;
      }

      protected void CONFIRM_290( )
      {
         BeforeValidate2986( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls2986( ) ;
            }
            else
            {
               CheckExtendedTable2986( ) ;
               if ( AnyError == 0 )
               {
                  ZM2986( 12) ;
                  ZM2986( 13) ;
                  ZM2986( 14) ;
                  ZM2986( 15) ;
                  ZM2986( 16) ;
                  ZM2986( 17) ;
               }
               CloseExtendedTableCursors2986( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            /* Save parent mode. */
            sMode86 = Gx_mode;
            CONFIRM_29235( ) ;
            if ( AnyError == 0 )
            {
               /* Restore parent mode. */
               Gx_mode = sMode86;
               IsConfirmed = 1;
            }
            /* Restore parent mode. */
            Gx_mode = sMode86;
         }
      }

      protected void CONFIRM_29235( )
      {
         nGXsfl_235_idx = 0;
         while ( nGXsfl_235_idx < bcProjeto.gxTpr_Anexos.Count )
         {
            ReadRow29235( ) ;
            if ( String.IsNullOrEmpty(StringUtil.RTrim( Gx_mode)) )
            {
               if ( RcdFound235 == 0 )
               {
                  Gx_mode = "INS";
               }
               else
               {
                  Gx_mode = "UPD";
               }
            }
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") != 0 ) || ( nIsMod_235 != 0 ) )
            {
               GetKey29235( ) ;
               if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( StringUtil.StrCmp(Gx_mode, "DLT") != 0 ) )
               {
                  if ( RcdFound235 == 0 )
                  {
                     Gx_mode = "INS";
                     BeforeValidate29235( ) ;
                     if ( AnyError == 0 )
                     {
                        CheckExtendedTable29235( ) ;
                        if ( AnyError == 0 )
                        {
                           ZM29235( 19) ;
                        }
                        CloseExtendedTableCursors29235( ) ;
                        if ( AnyError == 0 )
                        {
                           IsConfirmed = 1;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                     AnyError = 1;
                  }
               }
               else
               {
                  if ( RcdFound235 != 0 )
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                     {
                        Gx_mode = "DLT";
                        getByPrimaryKey29235( ) ;
                        Load29235( ) ;
                        BeforeValidate29235( ) ;
                        if ( AnyError == 0 )
                        {
                           OnDeleteControls29235( ) ;
                        }
                     }
                     else
                     {
                        if ( nIsMod_235 != 0 )
                        {
                           Gx_mode = "UPD";
                           BeforeValidate29235( ) ;
                           if ( AnyError == 0 )
                           {
                              CheckExtendedTable29235( ) ;
                              if ( AnyError == 0 )
                              {
                                 ZM29235( 19) ;
                              }
                              CloseExtendedTableCursors29235( ) ;
                              if ( AnyError == 0 )
                              {
                                 IsConfirmed = 1;
                              }
                           }
                        }
                     }
                  }
                  else
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "DLT") != 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
               VarsToRow235( ((SdtProjeto_Anexos)bcProjeto.gxTpr_Anexos.Item(nGXsfl_235_idx))) ;
            }
         }
         /* Start of After( level) rules */
         /* End of After( level) rules */
      }

      protected void E12292( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
         if ( ( StringUtil.StrCmp(AV9TrnContext.gxTpr_Transactionname, AV23Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV24GXV1 = 1;
            while ( AV24GXV1 <= AV9TrnContext.gxTpr_Attributes.Count )
            {
               AV13TrnContextAtt = ((wwpbaseobjects.SdtWWPTransactionContext_Attribute)AV9TrnContext.gxTpr_Attributes.Item(AV24GXV1));
               if ( StringUtil.StrCmp(AV13TrnContextAtt.gxTpr_Attributename, "Projeto_TipoProjetoCod") == 0 )
               {
                  AV12Insert_Projeto_TipoProjetoCod = (int)(NumberUtil.Val( AV13TrnContextAtt.gxTpr_Attributevalue, "."));
               }
               else if ( StringUtil.StrCmp(AV13TrnContextAtt.gxTpr_Attributename, "Projeto_AreaTrabalhoCodigo") == 0 )
               {
                  AV19Insert_Projeto_AreaTrabalhoCodigo = (int)(NumberUtil.Val( AV13TrnContextAtt.gxTpr_Attributevalue, "."));
               }
               else if ( StringUtil.StrCmp(AV13TrnContextAtt.gxTpr_Attributename, "Projeto_GpoObjCtrlCodigo") == 0 )
               {
                  AV20Insert_Projeto_GpoObjCtrlCodigo = (int)(NumberUtil.Val( AV13TrnContextAtt.gxTpr_Attributevalue, "."));
               }
               else if ( StringUtil.StrCmp(AV13TrnContextAtt.gxTpr_Attributename, "Projeto_SistemaCodigo") == 0 )
               {
                  AV21Insert_Projeto_SistemaCodigo = (int)(NumberUtil.Val( AV13TrnContextAtt.gxTpr_Attributevalue, "."));
               }
               else if ( StringUtil.StrCmp(AV13TrnContextAtt.gxTpr_Attributename, "Projeto_ModuloCodigo") == 0 )
               {
                  AV22Insert_Projeto_ModuloCodigo = (int)(NumberUtil.Val( AV13TrnContextAtt.gxTpr_Attributevalue, "."));
               }
               AV24GXV1 = (int)(AV24GXV1+1);
            }
         }
         AV16FatorEscala = 0;
         AV15FatorMultiplicador = 0;
      }

      protected void E11292( )
      {
         /* After Trn Routine */
      }

      protected void E13292( )
      {
         /* 'DoBntAnexos' Routine */
      }

      protected void E14292( )
      {
         /* 'DoFatorEscala' Routine */
         context.PopUp(formatLink("wp_projetofatores.aspx") + "?" + UrlEncode("" +AV8WWPContext.gxTpr_Areatrabalho_codigo) + "," + UrlEncode("" +AV7Projeto_Codigo) + "," + UrlEncode(StringUtil.RTrim("E")) + "," + UrlEncode(StringUtil.RTrim("")), new Object[] {"AV7Projeto_Codigo","","A985Projeto_FatorEscala"});
      }

      protected void E15292( )
      {
         /* 'DoFatorMultiplicador' Routine */
         context.PopUp(formatLink("wp_projetofatores.aspx") + "?" + UrlEncode("" +AV8WWPContext.gxTpr_Areatrabalho_codigo) + "," + UrlEncode("" +AV7Projeto_Codigo) + "," + UrlEncode(StringUtil.RTrim("M")) + "," + UrlEncode(StringUtil.RTrim("")), new Object[] {"AV7Projeto_Codigo","","A989Projeto_FatorMultiplicador"});
      }

      protected void E16292( )
      {
         /* 'DoBntEquipe' Routine */
      }

      protected void ZM2986( short GX_JID )
      {
         if ( ( GX_JID == 11 ) || ( GX_JID == 0 ) )
         {
            Z649Projeto_Nome = A649Projeto_Nome;
            Z650Projeto_Sigla = A650Projeto_Sigla;
            Z652Projeto_TecnicaContagem = A652Projeto_TecnicaContagem;
            Z1541Projeto_Previsao = A1541Projeto_Previsao;
            Z1542Projeto_GerenteCod = A1542Projeto_GerenteCod;
            Z1543Projeto_ServicoCod = A1543Projeto_ServicoCod;
            Z658Projeto_Status = A658Projeto_Status;
            Z985Projeto_FatorEscala = A985Projeto_FatorEscala;
            Z989Projeto_FatorMultiplicador = A989Projeto_FatorMultiplicador;
            Z990Projeto_ConstACocomo = A990Projeto_ConstACocomo;
            Z1232Projeto_Incremental = A1232Projeto_Incremental;
            Z2144Projeto_Identificador = A2144Projeto_Identificador;
            Z2146Projeto_DTInicio = A2146Projeto_DTInicio;
            Z2147Projeto_DTFim = A2147Projeto_DTFim;
            Z2148Projeto_PrazoPrevisto = A2148Projeto_PrazoPrevisto;
            Z2149Projeto_EsforcoPrevisto = A2149Projeto_EsforcoPrevisto;
            Z2150Projeto_CustoPrevisto = A2150Projeto_CustoPrevisto;
            Z2170Projeto_AnexoSequecial = A2170Projeto_AnexoSequecial;
            Z983Projeto_TipoProjetoCod = A983Projeto_TipoProjetoCod;
            Z2151Projeto_AreaTrabalhoCodigo = A2151Projeto_AreaTrabalhoCodigo;
            Z2153Projeto_GpoObjCtrlCodigo = A2153Projeto_GpoObjCtrlCodigo;
            Z2155Projeto_SistemaCodigo = A2155Projeto_SistemaCodigo;
            Z2158Projeto_ModuloCodigo = A2158Projeto_ModuloCodigo;
            Z655Projeto_Custo = A655Projeto_Custo;
            Z656Projeto_Prazo = A656Projeto_Prazo;
            Z657Projeto_Esforco = A657Projeto_Esforco;
            Z740Projeto_SistemaCod = A740Projeto_SistemaCod;
            Z2163ProjetoAnexos_Codigo = A2163ProjetoAnexos_Codigo;
            Z2166ProjetoAnexos_ArquivoNome = A2166ProjetoAnexos_ArquivoNome;
            Z2167ProjetoAnexos_ArquivoTipo = A2167ProjetoAnexos_ArquivoTipo;
            Z2169ProjetoAnexos_Data = A2169ProjetoAnexos_Data;
            Z645TipoDocumento_Codigo = A645TipoDocumento_Codigo;
            Z646TipoDocumento_Nome = A646TipoDocumento_Nome;
         }
         if ( ( GX_JID == 12 ) || ( GX_JID == 0 ) )
         {
            Z655Projeto_Custo = A655Projeto_Custo;
            Z656Projeto_Prazo = A656Projeto_Prazo;
            Z657Projeto_Esforco = A657Projeto_Esforco;
            Z740Projeto_SistemaCod = A740Projeto_SistemaCod;
            Z2163ProjetoAnexos_Codigo = A2163ProjetoAnexos_Codigo;
            Z2166ProjetoAnexos_ArquivoNome = A2166ProjetoAnexos_ArquivoNome;
            Z2167ProjetoAnexos_ArquivoTipo = A2167ProjetoAnexos_ArquivoTipo;
            Z2169ProjetoAnexos_Data = A2169ProjetoAnexos_Data;
            Z645TipoDocumento_Codigo = A645TipoDocumento_Codigo;
            Z646TipoDocumento_Nome = A646TipoDocumento_Nome;
         }
         if ( ( GX_JID == 13 ) || ( GX_JID == 0 ) )
         {
            Z2152Projeto_AreaTrabalhoDescricao = A2152Projeto_AreaTrabalhoDescricao;
            Z655Projeto_Custo = A655Projeto_Custo;
            Z656Projeto_Prazo = A656Projeto_Prazo;
            Z657Projeto_Esforco = A657Projeto_Esforco;
            Z740Projeto_SistemaCod = A740Projeto_SistemaCod;
            Z2163ProjetoAnexos_Codigo = A2163ProjetoAnexos_Codigo;
            Z2166ProjetoAnexos_ArquivoNome = A2166ProjetoAnexos_ArquivoNome;
            Z2167ProjetoAnexos_ArquivoTipo = A2167ProjetoAnexos_ArquivoTipo;
            Z2169ProjetoAnexos_Data = A2169ProjetoAnexos_Data;
            Z645TipoDocumento_Codigo = A645TipoDocumento_Codigo;
            Z646TipoDocumento_Nome = A646TipoDocumento_Nome;
         }
         if ( ( GX_JID == 14 ) || ( GX_JID == 0 ) )
         {
            Z2154Projeto_GpoObjCtrlNome = A2154Projeto_GpoObjCtrlNome;
            Z655Projeto_Custo = A655Projeto_Custo;
            Z656Projeto_Prazo = A656Projeto_Prazo;
            Z657Projeto_Esforco = A657Projeto_Esforco;
            Z740Projeto_SistemaCod = A740Projeto_SistemaCod;
            Z2163ProjetoAnexos_Codigo = A2163ProjetoAnexos_Codigo;
            Z2166ProjetoAnexos_ArquivoNome = A2166ProjetoAnexos_ArquivoNome;
            Z2167ProjetoAnexos_ArquivoTipo = A2167ProjetoAnexos_ArquivoTipo;
            Z2169ProjetoAnexos_Data = A2169ProjetoAnexos_Data;
            Z645TipoDocumento_Codigo = A645TipoDocumento_Codigo;
            Z646TipoDocumento_Nome = A646TipoDocumento_Nome;
         }
         if ( ( GX_JID == 15 ) || ( GX_JID == 0 ) )
         {
            Z2156Projeto_SistemaNome = A2156Projeto_SistemaNome;
            Z2157Projeto_SistemaSigla = A2157Projeto_SistemaSigla;
            Z655Projeto_Custo = A655Projeto_Custo;
            Z656Projeto_Prazo = A656Projeto_Prazo;
            Z657Projeto_Esforco = A657Projeto_Esforco;
            Z740Projeto_SistemaCod = A740Projeto_SistemaCod;
            Z2163ProjetoAnexos_Codigo = A2163ProjetoAnexos_Codigo;
            Z2166ProjetoAnexos_ArquivoNome = A2166ProjetoAnexos_ArquivoNome;
            Z2167ProjetoAnexos_ArquivoTipo = A2167ProjetoAnexos_ArquivoTipo;
            Z2169ProjetoAnexos_Data = A2169ProjetoAnexos_Data;
            Z645TipoDocumento_Codigo = A645TipoDocumento_Codigo;
            Z646TipoDocumento_Nome = A646TipoDocumento_Nome;
         }
         if ( ( GX_JID == 16 ) || ( GX_JID == 0 ) )
         {
            Z2159Projeto_ModuloNome = A2159Projeto_ModuloNome;
            Z2160Projeto_ModuloSigla = A2160Projeto_ModuloSigla;
            Z655Projeto_Custo = A655Projeto_Custo;
            Z656Projeto_Prazo = A656Projeto_Prazo;
            Z657Projeto_Esforco = A657Projeto_Esforco;
            Z740Projeto_SistemaCod = A740Projeto_SistemaCod;
            Z2163ProjetoAnexos_Codigo = A2163ProjetoAnexos_Codigo;
            Z2166ProjetoAnexos_ArquivoNome = A2166ProjetoAnexos_ArquivoNome;
            Z2167ProjetoAnexos_ArquivoTipo = A2167ProjetoAnexos_ArquivoTipo;
            Z2169ProjetoAnexos_Data = A2169ProjetoAnexos_Data;
            Z645TipoDocumento_Codigo = A645TipoDocumento_Codigo;
            Z646TipoDocumento_Nome = A646TipoDocumento_Nome;
         }
         if ( ( GX_JID == 17 ) || ( GX_JID == 0 ) )
         {
            Z655Projeto_Custo = A655Projeto_Custo;
            Z656Projeto_Prazo = A656Projeto_Prazo;
            Z657Projeto_Esforco = A657Projeto_Esforco;
            Z740Projeto_SistemaCod = A740Projeto_SistemaCod;
            Z2163ProjetoAnexos_Codigo = A2163ProjetoAnexos_Codigo;
            Z2166ProjetoAnexos_ArquivoNome = A2166ProjetoAnexos_ArquivoNome;
            Z2167ProjetoAnexos_ArquivoTipo = A2167ProjetoAnexos_ArquivoTipo;
            Z2169ProjetoAnexos_Data = A2169ProjetoAnexos_Data;
            Z645TipoDocumento_Codigo = A645TipoDocumento_Codigo;
            Z646TipoDocumento_Nome = A646TipoDocumento_Nome;
         }
         if ( GX_JID == -11 )
         {
            Z648Projeto_Codigo = A648Projeto_Codigo;
            Z649Projeto_Nome = A649Projeto_Nome;
            Z650Projeto_Sigla = A650Projeto_Sigla;
            Z652Projeto_TecnicaContagem = A652Projeto_TecnicaContagem;
            Z1540Projeto_Introducao = A1540Projeto_Introducao;
            Z653Projeto_Escopo = A653Projeto_Escopo;
            Z1541Projeto_Previsao = A1541Projeto_Previsao;
            Z1542Projeto_GerenteCod = A1542Projeto_GerenteCod;
            Z1543Projeto_ServicoCod = A1543Projeto_ServicoCod;
            Z658Projeto_Status = A658Projeto_Status;
            Z985Projeto_FatorEscala = A985Projeto_FatorEscala;
            Z989Projeto_FatorMultiplicador = A989Projeto_FatorMultiplicador;
            Z990Projeto_ConstACocomo = A990Projeto_ConstACocomo;
            Z1232Projeto_Incremental = A1232Projeto_Incremental;
            Z2144Projeto_Identificador = A2144Projeto_Identificador;
            Z2145Projeto_Objetivo = A2145Projeto_Objetivo;
            Z2146Projeto_DTInicio = A2146Projeto_DTInicio;
            Z2147Projeto_DTFim = A2147Projeto_DTFim;
            Z2148Projeto_PrazoPrevisto = A2148Projeto_PrazoPrevisto;
            Z2149Projeto_EsforcoPrevisto = A2149Projeto_EsforcoPrevisto;
            Z2150Projeto_CustoPrevisto = A2150Projeto_CustoPrevisto;
            Z2170Projeto_AnexoSequecial = A2170Projeto_AnexoSequecial;
            Z983Projeto_TipoProjetoCod = A983Projeto_TipoProjetoCod;
            Z2151Projeto_AreaTrabalhoCodigo = A2151Projeto_AreaTrabalhoCodigo;
            Z2153Projeto_GpoObjCtrlCodigo = A2153Projeto_GpoObjCtrlCodigo;
            Z2155Projeto_SistemaCodigo = A2155Projeto_SistemaCodigo;
            Z2158Projeto_ModuloCodigo = A2158Projeto_ModuloCodigo;
            Z2152Projeto_AreaTrabalhoDescricao = A2152Projeto_AreaTrabalhoDescricao;
            Z2154Projeto_GpoObjCtrlNome = A2154Projeto_GpoObjCtrlNome;
            Z2156Projeto_SistemaNome = A2156Projeto_SistemaNome;
            Z2157Projeto_SistemaSigla = A2157Projeto_SistemaSigla;
            Z655Projeto_Custo = A655Projeto_Custo;
            Z656Projeto_Prazo = A656Projeto_Prazo;
            Z657Projeto_Esforco = A657Projeto_Esforco;
            Z2159Projeto_ModuloNome = A2159Projeto_ModuloNome;
            Z2160Projeto_ModuloSigla = A2160Projeto_ModuloSigla;
         }
      }

      protected void standaloneNotModal( )
      {
         Gx_BScreen = 0;
         AV23Pgmname = "Projeto_BC";
      }

      protected void standaloneModal( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && String.IsNullOrEmpty(StringUtil.RTrim( A658Projeto_Status)) && ( Gx_BScreen == 0 ) )
         {
            A658Projeto_Status = "A";
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
         }
      }

      protected void Load2986( )
      {
         /* Using cursor BC002915 */
         pr_default.execute(11, new Object[] {A648Projeto_Codigo});
         if ( (pr_default.getStatus(11) != 101) )
         {
            RcdFound86 = 1;
            A649Projeto_Nome = BC002915_A649Projeto_Nome[0];
            A650Projeto_Sigla = BC002915_A650Projeto_Sigla[0];
            A652Projeto_TecnicaContagem = BC002915_A652Projeto_TecnicaContagem[0];
            A1540Projeto_Introducao = BC002915_A1540Projeto_Introducao[0];
            n1540Projeto_Introducao = BC002915_n1540Projeto_Introducao[0];
            A653Projeto_Escopo = BC002915_A653Projeto_Escopo[0];
            A1541Projeto_Previsao = BC002915_A1541Projeto_Previsao[0];
            n1541Projeto_Previsao = BC002915_n1541Projeto_Previsao[0];
            A1542Projeto_GerenteCod = BC002915_A1542Projeto_GerenteCod[0];
            n1542Projeto_GerenteCod = BC002915_n1542Projeto_GerenteCod[0];
            A1543Projeto_ServicoCod = BC002915_A1543Projeto_ServicoCod[0];
            n1543Projeto_ServicoCod = BC002915_n1543Projeto_ServicoCod[0];
            A658Projeto_Status = BC002915_A658Projeto_Status[0];
            A985Projeto_FatorEscala = BC002915_A985Projeto_FatorEscala[0];
            n985Projeto_FatorEscala = BC002915_n985Projeto_FatorEscala[0];
            A989Projeto_FatorMultiplicador = BC002915_A989Projeto_FatorMultiplicador[0];
            n989Projeto_FatorMultiplicador = BC002915_n989Projeto_FatorMultiplicador[0];
            A990Projeto_ConstACocomo = BC002915_A990Projeto_ConstACocomo[0];
            n990Projeto_ConstACocomo = BC002915_n990Projeto_ConstACocomo[0];
            A1232Projeto_Incremental = BC002915_A1232Projeto_Incremental[0];
            n1232Projeto_Incremental = BC002915_n1232Projeto_Incremental[0];
            A2144Projeto_Identificador = BC002915_A2144Projeto_Identificador[0];
            n2144Projeto_Identificador = BC002915_n2144Projeto_Identificador[0];
            A2145Projeto_Objetivo = BC002915_A2145Projeto_Objetivo[0];
            n2145Projeto_Objetivo = BC002915_n2145Projeto_Objetivo[0];
            A2146Projeto_DTInicio = BC002915_A2146Projeto_DTInicio[0];
            n2146Projeto_DTInicio = BC002915_n2146Projeto_DTInicio[0];
            A2147Projeto_DTFim = BC002915_A2147Projeto_DTFim[0];
            n2147Projeto_DTFim = BC002915_n2147Projeto_DTFim[0];
            A2148Projeto_PrazoPrevisto = BC002915_A2148Projeto_PrazoPrevisto[0];
            n2148Projeto_PrazoPrevisto = BC002915_n2148Projeto_PrazoPrevisto[0];
            A2149Projeto_EsforcoPrevisto = BC002915_A2149Projeto_EsforcoPrevisto[0];
            n2149Projeto_EsforcoPrevisto = BC002915_n2149Projeto_EsforcoPrevisto[0];
            A2150Projeto_CustoPrevisto = BC002915_A2150Projeto_CustoPrevisto[0];
            n2150Projeto_CustoPrevisto = BC002915_n2150Projeto_CustoPrevisto[0];
            A2152Projeto_AreaTrabalhoDescricao = BC002915_A2152Projeto_AreaTrabalhoDescricao[0];
            n2152Projeto_AreaTrabalhoDescricao = BC002915_n2152Projeto_AreaTrabalhoDescricao[0];
            A2154Projeto_GpoObjCtrlNome = BC002915_A2154Projeto_GpoObjCtrlNome[0];
            n2154Projeto_GpoObjCtrlNome = BC002915_n2154Projeto_GpoObjCtrlNome[0];
            A2156Projeto_SistemaNome = BC002915_A2156Projeto_SistemaNome[0];
            n2156Projeto_SistemaNome = BC002915_n2156Projeto_SistemaNome[0];
            A2157Projeto_SistemaSigla = BC002915_A2157Projeto_SistemaSigla[0];
            n2157Projeto_SistemaSigla = BC002915_n2157Projeto_SistemaSigla[0];
            A2159Projeto_ModuloNome = BC002915_A2159Projeto_ModuloNome[0];
            n2159Projeto_ModuloNome = BC002915_n2159Projeto_ModuloNome[0];
            A2160Projeto_ModuloSigla = BC002915_A2160Projeto_ModuloSigla[0];
            n2160Projeto_ModuloSigla = BC002915_n2160Projeto_ModuloSigla[0];
            A2170Projeto_AnexoSequecial = BC002915_A2170Projeto_AnexoSequecial[0];
            n2170Projeto_AnexoSequecial = BC002915_n2170Projeto_AnexoSequecial[0];
            A983Projeto_TipoProjetoCod = BC002915_A983Projeto_TipoProjetoCod[0];
            n983Projeto_TipoProjetoCod = BC002915_n983Projeto_TipoProjetoCod[0];
            A2151Projeto_AreaTrabalhoCodigo = BC002915_A2151Projeto_AreaTrabalhoCodigo[0];
            n2151Projeto_AreaTrabalhoCodigo = BC002915_n2151Projeto_AreaTrabalhoCodigo[0];
            A2153Projeto_GpoObjCtrlCodigo = BC002915_A2153Projeto_GpoObjCtrlCodigo[0];
            n2153Projeto_GpoObjCtrlCodigo = BC002915_n2153Projeto_GpoObjCtrlCodigo[0];
            A2155Projeto_SistemaCodigo = BC002915_A2155Projeto_SistemaCodigo[0];
            n2155Projeto_SistemaCodigo = BC002915_n2155Projeto_SistemaCodigo[0];
            A2158Projeto_ModuloCodigo = BC002915_A2158Projeto_ModuloCodigo[0];
            n2158Projeto_ModuloCodigo = BC002915_n2158Projeto_ModuloCodigo[0];
            A655Projeto_Custo = BC002915_A655Projeto_Custo[0];
            A656Projeto_Prazo = BC002915_A656Projeto_Prazo[0];
            A657Projeto_Esforco = BC002915_A657Projeto_Esforco[0];
            n657Projeto_Esforco = BC002915_n657Projeto_Esforco[0];
            ZM2986( -11) ;
         }
         pr_default.close(11);
         OnLoadActions2986( ) ;
      }

      protected void OnLoadActions2986( )
      {
         GXt_int1 = A740Projeto_SistemaCod;
         new prc_projeto_sistemacod(context ).execute( ref  A648Projeto_Codigo, ref  GXt_int1) ;
         A740Projeto_SistemaCod = GXt_int1;
      }

      protected void CheckExtendedTable2986( )
      {
         standaloneModal( ) ;
         GXt_int1 = A740Projeto_SistemaCod;
         new prc_projeto_sistemacod(context ).execute( ref  A648Projeto_Codigo, ref  GXt_int1) ;
         A740Projeto_SistemaCod = GXt_int1;
         /* Using cursor BC00297 */
         pr_default.execute(5, new Object[] {n983Projeto_TipoProjetoCod, A983Projeto_TipoProjetoCod});
         if ( (pr_default.getStatus(5) == 101) )
         {
            if ( ! ( (0==A983Projeto_TipoProjetoCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Projeto_Tipo Projeto'.", "ForeignKeyNotFound", 1, "PROJETO_TIPOPROJETOCOD");
               AnyError = 1;
            }
         }
         pr_default.close(5);
         if ( ! ( ( StringUtil.StrCmp(A652Projeto_TecnicaContagem, "1") == 0 ) || ( StringUtil.StrCmp(A652Projeto_TecnicaContagem, "2") == 0 ) || ( StringUtil.StrCmp(A652Projeto_TecnicaContagem, "3") == 0 ) ) )
         {
            GX_msglist.addItem("Campo T�cnica fora do intervalo", "OutOfRange", 1, "");
            AnyError = 1;
         }
         if ( ! ( (DateTime.MinValue==A1541Projeto_Previsao) || ( A1541Projeto_Previsao >= context.localUtil.YMDToD( 1753, 1, 1) ) ) )
         {
            GX_msglist.addItem("Campo Previs�o de Entrega fora do intervalo", "OutOfRange", 1, "");
            AnyError = 1;
         }
         if ( ! ( ( StringUtil.StrCmp(A658Projeto_Status, "A") == 0 ) || ( StringUtil.StrCmp(A658Projeto_Status, "E") == 0 ) || ( StringUtil.StrCmp(A658Projeto_Status, "C") == 0 ) ) )
         {
            GX_msglist.addItem("Campo Status fora do intervalo", "OutOfRange", 1, "");
            AnyError = 1;
         }
         if ( ! ( (DateTime.MinValue==A2146Projeto_DTInicio) || ( A2146Projeto_DTInicio >= context.localUtil.YMDToD( 1753, 1, 1) ) ) )
         {
            GX_msglist.addItem("Campo Data Prevista de In�cio fora do intervalo", "OutOfRange", 1, "");
            AnyError = 1;
         }
         if ( ! ( (DateTime.MinValue==A2147Projeto_DTFim) || ( A2147Projeto_DTFim >= context.localUtil.YMDToD( 1753, 1, 1) ) ) )
         {
            GX_msglist.addItem("Campo Data Prevista de T�rmino fora do intervalo", "OutOfRange", 1, "");
            AnyError = 1;
         }
         /* Using cursor BC00298 */
         pr_default.execute(6, new Object[] {n2151Projeto_AreaTrabalhoCodigo, A2151Projeto_AreaTrabalhoCodigo});
         if ( (pr_default.getStatus(6) == 101) )
         {
            if ( ! ( (0==A2151Projeto_AreaTrabalhoCodigo) ) )
            {
               GX_msglist.addItem("N�o existe '�real de Trabalho'.", "ForeignKeyNotFound", 1, "PROJETO_AREATRABALHOCODIGO");
               AnyError = 1;
            }
         }
         A2152Projeto_AreaTrabalhoDescricao = BC00298_A2152Projeto_AreaTrabalhoDescricao[0];
         n2152Projeto_AreaTrabalhoDescricao = BC00298_n2152Projeto_AreaTrabalhoDescricao[0];
         pr_default.close(6);
         /* Using cursor BC00299 */
         pr_default.execute(7, new Object[] {n2153Projeto_GpoObjCtrlCodigo, A2153Projeto_GpoObjCtrlCodigo});
         if ( (pr_default.getStatus(7) == 101) )
         {
            if ( ! ( (0==A2153Projeto_GpoObjCtrlCodigo) ) )
            {
               GX_msglist.addItem("N�o existe 'Grupo Objeto Controle'.", "ForeignKeyNotFound", 1, "PROJETO_GPOOBJCTRLCODIGO");
               AnyError = 1;
            }
         }
         A2154Projeto_GpoObjCtrlNome = BC00299_A2154Projeto_GpoObjCtrlNome[0];
         n2154Projeto_GpoObjCtrlNome = BC00299_n2154Projeto_GpoObjCtrlNome[0];
         pr_default.close(7);
         /* Using cursor BC002910 */
         pr_default.execute(8, new Object[] {n2155Projeto_SistemaCodigo, A2155Projeto_SistemaCodigo});
         if ( (pr_default.getStatus(8) == 101) )
         {
            if ( ! ( (0==A2155Projeto_SistemaCodigo) ) )
            {
               GX_msglist.addItem("N�o existe 'Sistema'.", "ForeignKeyNotFound", 1, "PROJETO_SISTEMACODIGO");
               AnyError = 1;
            }
         }
         A2156Projeto_SistemaNome = BC002910_A2156Projeto_SistemaNome[0];
         n2156Projeto_SistemaNome = BC002910_n2156Projeto_SistemaNome[0];
         A2157Projeto_SistemaSigla = BC002910_A2157Projeto_SistemaSigla[0];
         n2157Projeto_SistemaSigla = BC002910_n2157Projeto_SistemaSigla[0];
         pr_default.close(8);
         /* Using cursor BC002913 */
         pr_default.execute(10, new Object[] {n2155Projeto_SistemaCodigo, A2155Projeto_SistemaCodigo});
         if ( (pr_default.getStatus(10) != 101) )
         {
            A655Projeto_Custo = BC002913_A655Projeto_Custo[0];
            A656Projeto_Prazo = BC002913_A656Projeto_Prazo[0];
            A657Projeto_Esforco = BC002913_A657Projeto_Esforco[0];
            n657Projeto_Esforco = BC002913_n657Projeto_Esforco[0];
         }
         else
         {
            A657Projeto_Esforco = 0;
            n657Projeto_Esforco = false;
            A656Projeto_Prazo = 0;
            A655Projeto_Custo = 0;
         }
         pr_default.close(10);
         /* Using cursor BC002911 */
         pr_default.execute(9, new Object[] {n2158Projeto_ModuloCodigo, A2158Projeto_ModuloCodigo});
         if ( (pr_default.getStatus(9) == 101) )
         {
            if ( ! ( (0==A2158Projeto_ModuloCodigo) ) )
            {
               GX_msglist.addItem("N�o existe 'M�dulo '.", "ForeignKeyNotFound", 1, "PROJETO_MODULOCODIGO");
               AnyError = 1;
            }
         }
         A2159Projeto_ModuloNome = BC002911_A2159Projeto_ModuloNome[0];
         n2159Projeto_ModuloNome = BC002911_n2159Projeto_ModuloNome[0];
         A2160Projeto_ModuloSigla = BC002911_A2160Projeto_ModuloSigla[0];
         n2160Projeto_ModuloSigla = BC002911_n2160Projeto_ModuloSigla[0];
         pr_default.close(9);
      }

      protected void CloseExtendedTableCursors2986( )
      {
         pr_default.close(5);
         pr_default.close(6);
         pr_default.close(7);
         pr_default.close(8);
         pr_default.close(10);
         pr_default.close(9);
      }

      protected void enableDisable( )
      {
      }

      protected void GetKey2986( )
      {
         /* Using cursor BC002916 */
         pr_default.execute(12, new Object[] {A648Projeto_Codigo});
         if ( (pr_default.getStatus(12) != 101) )
         {
            RcdFound86 = 1;
         }
         else
         {
            RcdFound86 = 0;
         }
         pr_default.close(12);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor BC00296 */
         pr_default.execute(4, new Object[] {A648Projeto_Codigo});
         if ( (pr_default.getStatus(4) != 101) )
         {
            ZM2986( 11) ;
            RcdFound86 = 1;
            A648Projeto_Codigo = BC00296_A648Projeto_Codigo[0];
            A649Projeto_Nome = BC00296_A649Projeto_Nome[0];
            A650Projeto_Sigla = BC00296_A650Projeto_Sigla[0];
            A652Projeto_TecnicaContagem = BC00296_A652Projeto_TecnicaContagem[0];
            A1540Projeto_Introducao = BC00296_A1540Projeto_Introducao[0];
            n1540Projeto_Introducao = BC00296_n1540Projeto_Introducao[0];
            A653Projeto_Escopo = BC00296_A653Projeto_Escopo[0];
            A1541Projeto_Previsao = BC00296_A1541Projeto_Previsao[0];
            n1541Projeto_Previsao = BC00296_n1541Projeto_Previsao[0];
            A1542Projeto_GerenteCod = BC00296_A1542Projeto_GerenteCod[0];
            n1542Projeto_GerenteCod = BC00296_n1542Projeto_GerenteCod[0];
            A1543Projeto_ServicoCod = BC00296_A1543Projeto_ServicoCod[0];
            n1543Projeto_ServicoCod = BC00296_n1543Projeto_ServicoCod[0];
            A658Projeto_Status = BC00296_A658Projeto_Status[0];
            A985Projeto_FatorEscala = BC00296_A985Projeto_FatorEscala[0];
            n985Projeto_FatorEscala = BC00296_n985Projeto_FatorEscala[0];
            A989Projeto_FatorMultiplicador = BC00296_A989Projeto_FatorMultiplicador[0];
            n989Projeto_FatorMultiplicador = BC00296_n989Projeto_FatorMultiplicador[0];
            A990Projeto_ConstACocomo = BC00296_A990Projeto_ConstACocomo[0];
            n990Projeto_ConstACocomo = BC00296_n990Projeto_ConstACocomo[0];
            A1232Projeto_Incremental = BC00296_A1232Projeto_Incremental[0];
            n1232Projeto_Incremental = BC00296_n1232Projeto_Incremental[0];
            A2144Projeto_Identificador = BC00296_A2144Projeto_Identificador[0];
            n2144Projeto_Identificador = BC00296_n2144Projeto_Identificador[0];
            A2145Projeto_Objetivo = BC00296_A2145Projeto_Objetivo[0];
            n2145Projeto_Objetivo = BC00296_n2145Projeto_Objetivo[0];
            A2146Projeto_DTInicio = BC00296_A2146Projeto_DTInicio[0];
            n2146Projeto_DTInicio = BC00296_n2146Projeto_DTInicio[0];
            A2147Projeto_DTFim = BC00296_A2147Projeto_DTFim[0];
            n2147Projeto_DTFim = BC00296_n2147Projeto_DTFim[0];
            A2148Projeto_PrazoPrevisto = BC00296_A2148Projeto_PrazoPrevisto[0];
            n2148Projeto_PrazoPrevisto = BC00296_n2148Projeto_PrazoPrevisto[0];
            A2149Projeto_EsforcoPrevisto = BC00296_A2149Projeto_EsforcoPrevisto[0];
            n2149Projeto_EsforcoPrevisto = BC00296_n2149Projeto_EsforcoPrevisto[0];
            A2150Projeto_CustoPrevisto = BC00296_A2150Projeto_CustoPrevisto[0];
            n2150Projeto_CustoPrevisto = BC00296_n2150Projeto_CustoPrevisto[0];
            A2170Projeto_AnexoSequecial = BC00296_A2170Projeto_AnexoSequecial[0];
            n2170Projeto_AnexoSequecial = BC00296_n2170Projeto_AnexoSequecial[0];
            A983Projeto_TipoProjetoCod = BC00296_A983Projeto_TipoProjetoCod[0];
            n983Projeto_TipoProjetoCod = BC00296_n983Projeto_TipoProjetoCod[0];
            A2151Projeto_AreaTrabalhoCodigo = BC00296_A2151Projeto_AreaTrabalhoCodigo[0];
            n2151Projeto_AreaTrabalhoCodigo = BC00296_n2151Projeto_AreaTrabalhoCodigo[0];
            A2153Projeto_GpoObjCtrlCodigo = BC00296_A2153Projeto_GpoObjCtrlCodigo[0];
            n2153Projeto_GpoObjCtrlCodigo = BC00296_n2153Projeto_GpoObjCtrlCodigo[0];
            A2155Projeto_SistemaCodigo = BC00296_A2155Projeto_SistemaCodigo[0];
            n2155Projeto_SistemaCodigo = BC00296_n2155Projeto_SistemaCodigo[0];
            A2158Projeto_ModuloCodigo = BC00296_A2158Projeto_ModuloCodigo[0];
            n2158Projeto_ModuloCodigo = BC00296_n2158Projeto_ModuloCodigo[0];
            Z648Projeto_Codigo = A648Projeto_Codigo;
            sMode86 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Load2986( ) ;
            if ( AnyError == 1 )
            {
               RcdFound86 = 0;
               InitializeNonKey2986( ) ;
            }
            Gx_mode = sMode86;
         }
         else
         {
            RcdFound86 = 0;
            InitializeNonKey2986( ) ;
            sMode86 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Gx_mode = sMode86;
         }
         pr_default.close(4);
      }

      protected void getEqualNoModal( )
      {
         GetKey2986( ) ;
         if ( RcdFound86 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
         }
         getByPrimaryKey( ) ;
      }

      protected void insert_Check( )
      {
         CONFIRM_290( ) ;
         IsConfirmed = 0;
      }

      protected void update_Check( )
      {
         insert_Check( ) ;
      }

      protected void delete_Check( )
      {
         insert_Check( ) ;
      }

      protected void CheckOptimisticConcurrency2986( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor BC00295 */
            pr_default.execute(3, new Object[] {A648Projeto_Codigo});
            if ( (pr_default.getStatus(3) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Projeto"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            Gx_longc = false;
            if ( (pr_default.getStatus(3) == 101) || ( StringUtil.StrCmp(Z649Projeto_Nome, BC00295_A649Projeto_Nome[0]) != 0 ) || ( StringUtil.StrCmp(Z650Projeto_Sigla, BC00295_A650Projeto_Sigla[0]) != 0 ) || ( StringUtil.StrCmp(Z652Projeto_TecnicaContagem, BC00295_A652Projeto_TecnicaContagem[0]) != 0 ) || ( Z1541Projeto_Previsao != BC00295_A1541Projeto_Previsao[0] ) || ( Z1542Projeto_GerenteCod != BC00295_A1542Projeto_GerenteCod[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z1543Projeto_ServicoCod != BC00295_A1543Projeto_ServicoCod[0] ) || ( StringUtil.StrCmp(Z658Projeto_Status, BC00295_A658Projeto_Status[0]) != 0 ) || ( Z985Projeto_FatorEscala != BC00295_A985Projeto_FatorEscala[0] ) || ( Z989Projeto_FatorMultiplicador != BC00295_A989Projeto_FatorMultiplicador[0] ) || ( Z990Projeto_ConstACocomo != BC00295_A990Projeto_ConstACocomo[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z1232Projeto_Incremental != BC00295_A1232Projeto_Incremental[0] ) || ( StringUtil.StrCmp(Z2144Projeto_Identificador, BC00295_A2144Projeto_Identificador[0]) != 0 ) || ( Z2146Projeto_DTInicio != BC00295_A2146Projeto_DTInicio[0] ) || ( Z2147Projeto_DTFim != BC00295_A2147Projeto_DTFim[0] ) || ( Z2148Projeto_PrazoPrevisto != BC00295_A2148Projeto_PrazoPrevisto[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z2149Projeto_EsforcoPrevisto != BC00295_A2149Projeto_EsforcoPrevisto[0] ) || ( Z2150Projeto_CustoPrevisto != BC00295_A2150Projeto_CustoPrevisto[0] ) || ( Z2170Projeto_AnexoSequecial != BC00295_A2170Projeto_AnexoSequecial[0] ) || ( Z983Projeto_TipoProjetoCod != BC00295_A983Projeto_TipoProjetoCod[0] ) || ( Z2151Projeto_AreaTrabalhoCodigo != BC00295_A2151Projeto_AreaTrabalhoCodigo[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z2153Projeto_GpoObjCtrlCodigo != BC00295_A2153Projeto_GpoObjCtrlCodigo[0] ) || ( Z2155Projeto_SistemaCodigo != BC00295_A2155Projeto_SistemaCodigo[0] ) || ( Z2158Projeto_ModuloCodigo != BC00295_A2158Projeto_ModuloCodigo[0] ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"Projeto"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert2986( )
      {
         BeforeValidate2986( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable2986( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM2986( 0) ;
            CheckOptimisticConcurrency2986( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm2986( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert2986( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC002917 */
                     pr_default.execute(13, new Object[] {A649Projeto_Nome, A650Projeto_Sigla, A652Projeto_TecnicaContagem, n1540Projeto_Introducao, A1540Projeto_Introducao, A653Projeto_Escopo, n1541Projeto_Previsao, A1541Projeto_Previsao, n1542Projeto_GerenteCod, A1542Projeto_GerenteCod, n1543Projeto_ServicoCod, A1543Projeto_ServicoCod, A658Projeto_Status, n985Projeto_FatorEscala, A985Projeto_FatorEscala, n989Projeto_FatorMultiplicador, A989Projeto_FatorMultiplicador, n990Projeto_ConstACocomo, A990Projeto_ConstACocomo, n1232Projeto_Incremental, A1232Projeto_Incremental, n2144Projeto_Identificador, A2144Projeto_Identificador, n2145Projeto_Objetivo, A2145Projeto_Objetivo, n2146Projeto_DTInicio, A2146Projeto_DTInicio, n2147Projeto_DTFim, A2147Projeto_DTFim, n2148Projeto_PrazoPrevisto, A2148Projeto_PrazoPrevisto, n2149Projeto_EsforcoPrevisto, A2149Projeto_EsforcoPrevisto, n2150Projeto_CustoPrevisto, A2150Projeto_CustoPrevisto, n2170Projeto_AnexoSequecial, A2170Projeto_AnexoSequecial, n983Projeto_TipoProjetoCod, A983Projeto_TipoProjetoCod, n2151Projeto_AreaTrabalhoCodigo, A2151Projeto_AreaTrabalhoCodigo, n2153Projeto_GpoObjCtrlCodigo, A2153Projeto_GpoObjCtrlCodigo, n2155Projeto_SistemaCodigo, A2155Projeto_SistemaCodigo, n2158Projeto_ModuloCodigo, A2158Projeto_ModuloCodigo});
                     A648Projeto_Codigo = BC002917_A648Projeto_Codigo[0];
                     pr_default.close(13);
                     dsDefault.SmartCacheProvider.SetUpdated("Projeto") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           ProcessLevel2986( ) ;
                           if ( AnyError == 0 )
                           {
                              /* Save values for previous() function. */
                              GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load2986( ) ;
            }
            EndLevel2986( ) ;
         }
         CloseExtendedTableCursors2986( ) ;
      }

      protected void Update2986( )
      {
         BeforeValidate2986( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable2986( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency2986( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm2986( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate2986( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC002918 */
                     pr_default.execute(14, new Object[] {A649Projeto_Nome, A650Projeto_Sigla, A652Projeto_TecnicaContagem, n1540Projeto_Introducao, A1540Projeto_Introducao, A653Projeto_Escopo, n1541Projeto_Previsao, A1541Projeto_Previsao, n1542Projeto_GerenteCod, A1542Projeto_GerenteCod, n1543Projeto_ServicoCod, A1543Projeto_ServicoCod, A658Projeto_Status, n985Projeto_FatorEscala, A985Projeto_FatorEscala, n989Projeto_FatorMultiplicador, A989Projeto_FatorMultiplicador, n990Projeto_ConstACocomo, A990Projeto_ConstACocomo, n1232Projeto_Incremental, A1232Projeto_Incremental, n2144Projeto_Identificador, A2144Projeto_Identificador, n2145Projeto_Objetivo, A2145Projeto_Objetivo, n2146Projeto_DTInicio, A2146Projeto_DTInicio, n2147Projeto_DTFim, A2147Projeto_DTFim, n2148Projeto_PrazoPrevisto, A2148Projeto_PrazoPrevisto, n2149Projeto_EsforcoPrevisto, A2149Projeto_EsforcoPrevisto, n2150Projeto_CustoPrevisto, A2150Projeto_CustoPrevisto, n2170Projeto_AnexoSequecial, A2170Projeto_AnexoSequecial, n983Projeto_TipoProjetoCod, A983Projeto_TipoProjetoCod, n2151Projeto_AreaTrabalhoCodigo, A2151Projeto_AreaTrabalhoCodigo, n2153Projeto_GpoObjCtrlCodigo, A2153Projeto_GpoObjCtrlCodigo, n2155Projeto_SistemaCodigo, A2155Projeto_SistemaCodigo, n2158Projeto_ModuloCodigo, A2158Projeto_ModuloCodigo, A648Projeto_Codigo});
                     pr_default.close(14);
                     dsDefault.SmartCacheProvider.SetUpdated("Projeto") ;
                     if ( (pr_default.getStatus(14) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Projeto"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate2986( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           ProcessLevel2986( ) ;
                           if ( AnyError == 0 )
                           {
                              getByPrimaryKey( ) ;
                              GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel2986( ) ;
         }
         CloseExtendedTableCursors2986( ) ;
      }

      protected void DeferredUpdate2986( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         BeforeValidate2986( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency2986( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls2986( ) ;
            AfterConfirm2986( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete2986( ) ;
               if ( AnyError == 0 )
               {
                  ScanKeyStart29235( ) ;
                  while ( RcdFound235 != 0 )
                  {
                     getByPrimaryKey29235( ) ;
                     Delete29235( ) ;
                     ScanKeyNext29235( ) ;
                  }
                  ScanKeyEnd29235( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC002919 */
                     pr_default.execute(15, new Object[] {A648Projeto_Codigo});
                     pr_default.close(15);
                     dsDefault.SmartCacheProvider.SetUpdated("Projeto") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( delete) rules */
                        /* End of After( delete) rules */
                        if ( AnyError == 0 )
                        {
                           GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
         }
         sMode86 = Gx_mode;
         Gx_mode = "DLT";
         EndLevel2986( ) ;
         Gx_mode = sMode86;
      }

      protected void OnDeleteControls2986( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            GXt_int1 = A740Projeto_SistemaCod;
            new prc_projeto_sistemacod(context ).execute( ref  A648Projeto_Codigo, ref  GXt_int1) ;
            A740Projeto_SistemaCod = GXt_int1;
            /* Using cursor BC002920 */
            pr_default.execute(16, new Object[] {n2151Projeto_AreaTrabalhoCodigo, A2151Projeto_AreaTrabalhoCodigo});
            A2152Projeto_AreaTrabalhoDescricao = BC002920_A2152Projeto_AreaTrabalhoDescricao[0];
            n2152Projeto_AreaTrabalhoDescricao = BC002920_n2152Projeto_AreaTrabalhoDescricao[0];
            pr_default.close(16);
            /* Using cursor BC002921 */
            pr_default.execute(17, new Object[] {n2153Projeto_GpoObjCtrlCodigo, A2153Projeto_GpoObjCtrlCodigo});
            A2154Projeto_GpoObjCtrlNome = BC002921_A2154Projeto_GpoObjCtrlNome[0];
            n2154Projeto_GpoObjCtrlNome = BC002921_n2154Projeto_GpoObjCtrlNome[0];
            pr_default.close(17);
            /* Using cursor BC002922 */
            pr_default.execute(18, new Object[] {n2155Projeto_SistemaCodigo, A2155Projeto_SistemaCodigo});
            A2156Projeto_SistemaNome = BC002922_A2156Projeto_SistemaNome[0];
            n2156Projeto_SistemaNome = BC002922_n2156Projeto_SistemaNome[0];
            A2157Projeto_SistemaSigla = BC002922_A2157Projeto_SistemaSigla[0];
            n2157Projeto_SistemaSigla = BC002922_n2157Projeto_SistemaSigla[0];
            pr_default.close(18);
            /* Using cursor BC002924 */
            pr_default.execute(19, new Object[] {n2155Projeto_SistemaCodigo, A2155Projeto_SistemaCodigo});
            if ( (pr_default.getStatus(19) != 101) )
            {
               A655Projeto_Custo = BC002924_A655Projeto_Custo[0];
               A656Projeto_Prazo = BC002924_A656Projeto_Prazo[0];
               A657Projeto_Esforco = BC002924_A657Projeto_Esforco[0];
               n657Projeto_Esforco = BC002924_n657Projeto_Esforco[0];
            }
            else
            {
               A657Projeto_Esforco = 0;
               n657Projeto_Esforco = false;
               A656Projeto_Prazo = 0;
               A655Projeto_Custo = 0;
            }
            pr_default.close(19);
            /* Using cursor BC002925 */
            pr_default.execute(20, new Object[] {n2158Projeto_ModuloCodigo, A2158Projeto_ModuloCodigo});
            A2159Projeto_ModuloNome = BC002925_A2159Projeto_ModuloNome[0];
            n2159Projeto_ModuloNome = BC002925_n2159Projeto_ModuloNome[0];
            A2160Projeto_ModuloSigla = BC002925_A2160Projeto_ModuloSigla[0];
            n2160Projeto_ModuloSigla = BC002925_n2160Projeto_ModuloSigla[0];
            pr_default.close(20);
         }
         if ( AnyError == 0 )
         {
            /* Using cursor BC002926 */
            pr_default.execute(21, new Object[] {A648Projeto_Codigo});
            if ( (pr_default.getStatus(21) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Vari�vel Cocomo"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(21);
            /* Using cursor BC002927 */
            pr_default.execute(22, new Object[] {A648Projeto_Codigo});
            if ( (pr_default.getStatus(22) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contagem"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(22);
            /* Using cursor BC002928 */
            pr_default.execute(23, new Object[] {A648Projeto_Codigo});
            if ( (pr_default.getStatus(23) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Projeto Melhoria"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(23);
            /* Using cursor BC002929 */
            pr_default.execute(24, new Object[] {A648Projeto_Codigo});
            if ( (pr_default.getStatus(24) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Proposta"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(24);
         }
      }

      protected void ProcessNestedLevel29235( )
      {
         nGXsfl_235_idx = 0;
         while ( nGXsfl_235_idx < bcProjeto.gxTpr_Anexos.Count )
         {
            ReadRow29235( ) ;
            if ( String.IsNullOrEmpty(StringUtil.RTrim( Gx_mode)) )
            {
               if ( RcdFound235 == 0 )
               {
                  Gx_mode = "INS";
               }
               else
               {
                  Gx_mode = "UPD";
               }
            }
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") != 0 ) || ( nIsMod_235 != 0 ) )
            {
               standaloneNotModal29235( ) ;
               if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
               {
                  Gx_mode = "INS";
                  Insert29235( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                  {
                     Gx_mode = "DLT";
                     Delete29235( ) ;
                  }
                  else
                  {
                     Gx_mode = "UPD";
                     Update29235( ) ;
                  }
               }
            }
            KeyVarsToRow235( ((SdtProjeto_Anexos)bcProjeto.gxTpr_Anexos.Item(nGXsfl_235_idx))) ;
         }
         if ( AnyError == 0 )
         {
            /* Batch update SDT rows */
            nGXsfl_235_idx = 0;
            while ( nGXsfl_235_idx < bcProjeto.gxTpr_Anexos.Count )
            {
               ReadRow29235( ) ;
               if ( String.IsNullOrEmpty(StringUtil.RTrim( Gx_mode)) )
               {
                  if ( RcdFound235 == 0 )
                  {
                     Gx_mode = "INS";
                  }
                  else
                  {
                     Gx_mode = "UPD";
                  }
               }
               /* Update SDT row */
               if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  bcProjeto.gxTpr_Anexos.RemoveElement(nGXsfl_235_idx);
                  nGXsfl_235_idx = (short)(nGXsfl_235_idx-1);
               }
               else
               {
                  Gx_mode = "UPD";
                  getByPrimaryKey29235( ) ;
                  VarsToRow235( ((SdtProjeto_Anexos)bcProjeto.gxTpr_Anexos.Item(nGXsfl_235_idx))) ;
               }
            }
         }
         /* Start of After( level) rules */
         /* End of After( level) rules */
         InitAll29235( ) ;
         if ( AnyError != 0 )
         {
         }
         nRcdExists_235 = 0;
         nIsMod_235 = 0;
         Gxremove235 = 0;
      }

      protected void ProcessLevel2986( )
      {
         /* Save parent mode. */
         sMode86 = Gx_mode;
         ProcessNestedLevel29235( ) ;
         if ( AnyError != 0 )
         {
         }
         /* Restore parent mode. */
         Gx_mode = sMode86;
         /* ' Update level parameters */
      }

      protected void EndLevel2986( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(3);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete2986( ) ;
         }
         if ( AnyError == 0 )
         {
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanKeyStart2986( )
      {
         /* Scan By routine */
         /* Using cursor BC002931 */
         pr_default.execute(25, new Object[] {A648Projeto_Codigo});
         RcdFound86 = 0;
         if ( (pr_default.getStatus(25) != 101) )
         {
            RcdFound86 = 1;
            A648Projeto_Codigo = BC002931_A648Projeto_Codigo[0];
            A649Projeto_Nome = BC002931_A649Projeto_Nome[0];
            A650Projeto_Sigla = BC002931_A650Projeto_Sigla[0];
            A652Projeto_TecnicaContagem = BC002931_A652Projeto_TecnicaContagem[0];
            A1540Projeto_Introducao = BC002931_A1540Projeto_Introducao[0];
            n1540Projeto_Introducao = BC002931_n1540Projeto_Introducao[0];
            A653Projeto_Escopo = BC002931_A653Projeto_Escopo[0];
            A1541Projeto_Previsao = BC002931_A1541Projeto_Previsao[0];
            n1541Projeto_Previsao = BC002931_n1541Projeto_Previsao[0];
            A1542Projeto_GerenteCod = BC002931_A1542Projeto_GerenteCod[0];
            n1542Projeto_GerenteCod = BC002931_n1542Projeto_GerenteCod[0];
            A1543Projeto_ServicoCod = BC002931_A1543Projeto_ServicoCod[0];
            n1543Projeto_ServicoCod = BC002931_n1543Projeto_ServicoCod[0];
            A658Projeto_Status = BC002931_A658Projeto_Status[0];
            A985Projeto_FatorEscala = BC002931_A985Projeto_FatorEscala[0];
            n985Projeto_FatorEscala = BC002931_n985Projeto_FatorEscala[0];
            A989Projeto_FatorMultiplicador = BC002931_A989Projeto_FatorMultiplicador[0];
            n989Projeto_FatorMultiplicador = BC002931_n989Projeto_FatorMultiplicador[0];
            A990Projeto_ConstACocomo = BC002931_A990Projeto_ConstACocomo[0];
            n990Projeto_ConstACocomo = BC002931_n990Projeto_ConstACocomo[0];
            A1232Projeto_Incremental = BC002931_A1232Projeto_Incremental[0];
            n1232Projeto_Incremental = BC002931_n1232Projeto_Incremental[0];
            A2144Projeto_Identificador = BC002931_A2144Projeto_Identificador[0];
            n2144Projeto_Identificador = BC002931_n2144Projeto_Identificador[0];
            A2145Projeto_Objetivo = BC002931_A2145Projeto_Objetivo[0];
            n2145Projeto_Objetivo = BC002931_n2145Projeto_Objetivo[0];
            A2146Projeto_DTInicio = BC002931_A2146Projeto_DTInicio[0];
            n2146Projeto_DTInicio = BC002931_n2146Projeto_DTInicio[0];
            A2147Projeto_DTFim = BC002931_A2147Projeto_DTFim[0];
            n2147Projeto_DTFim = BC002931_n2147Projeto_DTFim[0];
            A2148Projeto_PrazoPrevisto = BC002931_A2148Projeto_PrazoPrevisto[0];
            n2148Projeto_PrazoPrevisto = BC002931_n2148Projeto_PrazoPrevisto[0];
            A2149Projeto_EsforcoPrevisto = BC002931_A2149Projeto_EsforcoPrevisto[0];
            n2149Projeto_EsforcoPrevisto = BC002931_n2149Projeto_EsforcoPrevisto[0];
            A2150Projeto_CustoPrevisto = BC002931_A2150Projeto_CustoPrevisto[0];
            n2150Projeto_CustoPrevisto = BC002931_n2150Projeto_CustoPrevisto[0];
            A2152Projeto_AreaTrabalhoDescricao = BC002931_A2152Projeto_AreaTrabalhoDescricao[0];
            n2152Projeto_AreaTrabalhoDescricao = BC002931_n2152Projeto_AreaTrabalhoDescricao[0];
            A2154Projeto_GpoObjCtrlNome = BC002931_A2154Projeto_GpoObjCtrlNome[0];
            n2154Projeto_GpoObjCtrlNome = BC002931_n2154Projeto_GpoObjCtrlNome[0];
            A2156Projeto_SistemaNome = BC002931_A2156Projeto_SistemaNome[0];
            n2156Projeto_SistemaNome = BC002931_n2156Projeto_SistemaNome[0];
            A2157Projeto_SistemaSigla = BC002931_A2157Projeto_SistemaSigla[0];
            n2157Projeto_SistemaSigla = BC002931_n2157Projeto_SistemaSigla[0];
            A2159Projeto_ModuloNome = BC002931_A2159Projeto_ModuloNome[0];
            n2159Projeto_ModuloNome = BC002931_n2159Projeto_ModuloNome[0];
            A2160Projeto_ModuloSigla = BC002931_A2160Projeto_ModuloSigla[0];
            n2160Projeto_ModuloSigla = BC002931_n2160Projeto_ModuloSigla[0];
            A2170Projeto_AnexoSequecial = BC002931_A2170Projeto_AnexoSequecial[0];
            n2170Projeto_AnexoSequecial = BC002931_n2170Projeto_AnexoSequecial[0];
            A983Projeto_TipoProjetoCod = BC002931_A983Projeto_TipoProjetoCod[0];
            n983Projeto_TipoProjetoCod = BC002931_n983Projeto_TipoProjetoCod[0];
            A2151Projeto_AreaTrabalhoCodigo = BC002931_A2151Projeto_AreaTrabalhoCodigo[0];
            n2151Projeto_AreaTrabalhoCodigo = BC002931_n2151Projeto_AreaTrabalhoCodigo[0];
            A2153Projeto_GpoObjCtrlCodigo = BC002931_A2153Projeto_GpoObjCtrlCodigo[0];
            n2153Projeto_GpoObjCtrlCodigo = BC002931_n2153Projeto_GpoObjCtrlCodigo[0];
            A2155Projeto_SistemaCodigo = BC002931_A2155Projeto_SistemaCodigo[0];
            n2155Projeto_SistemaCodigo = BC002931_n2155Projeto_SistemaCodigo[0];
            A2158Projeto_ModuloCodigo = BC002931_A2158Projeto_ModuloCodigo[0];
            n2158Projeto_ModuloCodigo = BC002931_n2158Projeto_ModuloCodigo[0];
            A655Projeto_Custo = BC002931_A655Projeto_Custo[0];
            A656Projeto_Prazo = BC002931_A656Projeto_Prazo[0];
            A657Projeto_Esforco = BC002931_A657Projeto_Esforco[0];
            n657Projeto_Esforco = BC002931_n657Projeto_Esforco[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanKeyNext2986( )
      {
         /* Scan next routine */
         pr_default.readNext(25);
         RcdFound86 = 0;
         ScanKeyLoad2986( ) ;
      }

      protected void ScanKeyLoad2986( )
      {
         sMode86 = Gx_mode;
         Gx_mode = "DSP";
         if ( (pr_default.getStatus(25) != 101) )
         {
            RcdFound86 = 1;
            A648Projeto_Codigo = BC002931_A648Projeto_Codigo[0];
            A649Projeto_Nome = BC002931_A649Projeto_Nome[0];
            A650Projeto_Sigla = BC002931_A650Projeto_Sigla[0];
            A652Projeto_TecnicaContagem = BC002931_A652Projeto_TecnicaContagem[0];
            A1540Projeto_Introducao = BC002931_A1540Projeto_Introducao[0];
            n1540Projeto_Introducao = BC002931_n1540Projeto_Introducao[0];
            A653Projeto_Escopo = BC002931_A653Projeto_Escopo[0];
            A1541Projeto_Previsao = BC002931_A1541Projeto_Previsao[0];
            n1541Projeto_Previsao = BC002931_n1541Projeto_Previsao[0];
            A1542Projeto_GerenteCod = BC002931_A1542Projeto_GerenteCod[0];
            n1542Projeto_GerenteCod = BC002931_n1542Projeto_GerenteCod[0];
            A1543Projeto_ServicoCod = BC002931_A1543Projeto_ServicoCod[0];
            n1543Projeto_ServicoCod = BC002931_n1543Projeto_ServicoCod[0];
            A658Projeto_Status = BC002931_A658Projeto_Status[0];
            A985Projeto_FatorEscala = BC002931_A985Projeto_FatorEscala[0];
            n985Projeto_FatorEscala = BC002931_n985Projeto_FatorEscala[0];
            A989Projeto_FatorMultiplicador = BC002931_A989Projeto_FatorMultiplicador[0];
            n989Projeto_FatorMultiplicador = BC002931_n989Projeto_FatorMultiplicador[0];
            A990Projeto_ConstACocomo = BC002931_A990Projeto_ConstACocomo[0];
            n990Projeto_ConstACocomo = BC002931_n990Projeto_ConstACocomo[0];
            A1232Projeto_Incremental = BC002931_A1232Projeto_Incremental[0];
            n1232Projeto_Incremental = BC002931_n1232Projeto_Incremental[0];
            A2144Projeto_Identificador = BC002931_A2144Projeto_Identificador[0];
            n2144Projeto_Identificador = BC002931_n2144Projeto_Identificador[0];
            A2145Projeto_Objetivo = BC002931_A2145Projeto_Objetivo[0];
            n2145Projeto_Objetivo = BC002931_n2145Projeto_Objetivo[0];
            A2146Projeto_DTInicio = BC002931_A2146Projeto_DTInicio[0];
            n2146Projeto_DTInicio = BC002931_n2146Projeto_DTInicio[0];
            A2147Projeto_DTFim = BC002931_A2147Projeto_DTFim[0];
            n2147Projeto_DTFim = BC002931_n2147Projeto_DTFim[0];
            A2148Projeto_PrazoPrevisto = BC002931_A2148Projeto_PrazoPrevisto[0];
            n2148Projeto_PrazoPrevisto = BC002931_n2148Projeto_PrazoPrevisto[0];
            A2149Projeto_EsforcoPrevisto = BC002931_A2149Projeto_EsforcoPrevisto[0];
            n2149Projeto_EsforcoPrevisto = BC002931_n2149Projeto_EsforcoPrevisto[0];
            A2150Projeto_CustoPrevisto = BC002931_A2150Projeto_CustoPrevisto[0];
            n2150Projeto_CustoPrevisto = BC002931_n2150Projeto_CustoPrevisto[0];
            A2152Projeto_AreaTrabalhoDescricao = BC002931_A2152Projeto_AreaTrabalhoDescricao[0];
            n2152Projeto_AreaTrabalhoDescricao = BC002931_n2152Projeto_AreaTrabalhoDescricao[0];
            A2154Projeto_GpoObjCtrlNome = BC002931_A2154Projeto_GpoObjCtrlNome[0];
            n2154Projeto_GpoObjCtrlNome = BC002931_n2154Projeto_GpoObjCtrlNome[0];
            A2156Projeto_SistemaNome = BC002931_A2156Projeto_SistemaNome[0];
            n2156Projeto_SistemaNome = BC002931_n2156Projeto_SistemaNome[0];
            A2157Projeto_SistemaSigla = BC002931_A2157Projeto_SistemaSigla[0];
            n2157Projeto_SistemaSigla = BC002931_n2157Projeto_SistemaSigla[0];
            A2159Projeto_ModuloNome = BC002931_A2159Projeto_ModuloNome[0];
            n2159Projeto_ModuloNome = BC002931_n2159Projeto_ModuloNome[0];
            A2160Projeto_ModuloSigla = BC002931_A2160Projeto_ModuloSigla[0];
            n2160Projeto_ModuloSigla = BC002931_n2160Projeto_ModuloSigla[0];
            A2170Projeto_AnexoSequecial = BC002931_A2170Projeto_AnexoSequecial[0];
            n2170Projeto_AnexoSequecial = BC002931_n2170Projeto_AnexoSequecial[0];
            A983Projeto_TipoProjetoCod = BC002931_A983Projeto_TipoProjetoCod[0];
            n983Projeto_TipoProjetoCod = BC002931_n983Projeto_TipoProjetoCod[0];
            A2151Projeto_AreaTrabalhoCodigo = BC002931_A2151Projeto_AreaTrabalhoCodigo[0];
            n2151Projeto_AreaTrabalhoCodigo = BC002931_n2151Projeto_AreaTrabalhoCodigo[0];
            A2153Projeto_GpoObjCtrlCodigo = BC002931_A2153Projeto_GpoObjCtrlCodigo[0];
            n2153Projeto_GpoObjCtrlCodigo = BC002931_n2153Projeto_GpoObjCtrlCodigo[0];
            A2155Projeto_SistemaCodigo = BC002931_A2155Projeto_SistemaCodigo[0];
            n2155Projeto_SistemaCodigo = BC002931_n2155Projeto_SistemaCodigo[0];
            A2158Projeto_ModuloCodigo = BC002931_A2158Projeto_ModuloCodigo[0];
            n2158Projeto_ModuloCodigo = BC002931_n2158Projeto_ModuloCodigo[0];
            A655Projeto_Custo = BC002931_A655Projeto_Custo[0];
            A656Projeto_Prazo = BC002931_A656Projeto_Prazo[0];
            A657Projeto_Esforco = BC002931_A657Projeto_Esforco[0];
            n657Projeto_Esforco = BC002931_n657Projeto_Esforco[0];
         }
         Gx_mode = sMode86;
      }

      protected void ScanKeyEnd2986( )
      {
         pr_default.close(25);
      }

      protected void AfterConfirm2986( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert2986( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate2986( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete2986( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete2986( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate2986( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes2986( )
      {
      }

      protected void ZM29235( short GX_JID )
      {
         if ( ( GX_JID == 18 ) || ( GX_JID == 0 ) )
         {
            Z2169ProjetoAnexos_Data = A2169ProjetoAnexos_Data;
            Z645TipoDocumento_Codigo = A645TipoDocumento_Codigo;
            Z649Projeto_Nome = A649Projeto_Nome;
            Z650Projeto_Sigla = A650Projeto_Sigla;
            Z983Projeto_TipoProjetoCod = A983Projeto_TipoProjetoCod;
            Z652Projeto_TecnicaContagem = A652Projeto_TecnicaContagem;
            Z1541Projeto_Previsao = A1541Projeto_Previsao;
            Z1542Projeto_GerenteCod = A1542Projeto_GerenteCod;
            Z1543Projeto_ServicoCod = A1543Projeto_ServicoCod;
            Z655Projeto_Custo = A655Projeto_Custo;
            Z656Projeto_Prazo = A656Projeto_Prazo;
            Z657Projeto_Esforco = A657Projeto_Esforco;
            Z740Projeto_SistemaCod = A740Projeto_SistemaCod;
            Z658Projeto_Status = A658Projeto_Status;
            Z985Projeto_FatorEscala = A985Projeto_FatorEscala;
            Z989Projeto_FatorMultiplicador = A989Projeto_FatorMultiplicador;
            Z990Projeto_ConstACocomo = A990Projeto_ConstACocomo;
            Z1232Projeto_Incremental = A1232Projeto_Incremental;
            Z2144Projeto_Identificador = A2144Projeto_Identificador;
            Z2146Projeto_DTInicio = A2146Projeto_DTInicio;
            Z2147Projeto_DTFim = A2147Projeto_DTFim;
            Z2148Projeto_PrazoPrevisto = A2148Projeto_PrazoPrevisto;
            Z2149Projeto_EsforcoPrevisto = A2149Projeto_EsforcoPrevisto;
            Z2150Projeto_CustoPrevisto = A2150Projeto_CustoPrevisto;
            Z2151Projeto_AreaTrabalhoCodigo = A2151Projeto_AreaTrabalhoCodigo;
            Z2152Projeto_AreaTrabalhoDescricao = A2152Projeto_AreaTrabalhoDescricao;
            Z2153Projeto_GpoObjCtrlCodigo = A2153Projeto_GpoObjCtrlCodigo;
            Z2154Projeto_GpoObjCtrlNome = A2154Projeto_GpoObjCtrlNome;
            Z2155Projeto_SistemaCodigo = A2155Projeto_SistemaCodigo;
            Z2156Projeto_SistemaNome = A2156Projeto_SistemaNome;
            Z2157Projeto_SistemaSigla = A2157Projeto_SistemaSigla;
            Z2158Projeto_ModuloCodigo = A2158Projeto_ModuloCodigo;
            Z2159Projeto_ModuloNome = A2159Projeto_ModuloNome;
            Z2160Projeto_ModuloSigla = A2160Projeto_ModuloSigla;
            Z2170Projeto_AnexoSequecial = A2170Projeto_AnexoSequecial;
         }
         if ( ( GX_JID == 19 ) || ( GX_JID == 0 ) )
         {
            Z646TipoDocumento_Nome = A646TipoDocumento_Nome;
            Z649Projeto_Nome = A649Projeto_Nome;
            Z650Projeto_Sigla = A650Projeto_Sigla;
            Z983Projeto_TipoProjetoCod = A983Projeto_TipoProjetoCod;
            Z652Projeto_TecnicaContagem = A652Projeto_TecnicaContagem;
            Z1541Projeto_Previsao = A1541Projeto_Previsao;
            Z1542Projeto_GerenteCod = A1542Projeto_GerenteCod;
            Z1543Projeto_ServicoCod = A1543Projeto_ServicoCod;
            Z655Projeto_Custo = A655Projeto_Custo;
            Z656Projeto_Prazo = A656Projeto_Prazo;
            Z657Projeto_Esforco = A657Projeto_Esforco;
            Z740Projeto_SistemaCod = A740Projeto_SistemaCod;
            Z658Projeto_Status = A658Projeto_Status;
            Z985Projeto_FatorEscala = A985Projeto_FatorEscala;
            Z989Projeto_FatorMultiplicador = A989Projeto_FatorMultiplicador;
            Z990Projeto_ConstACocomo = A990Projeto_ConstACocomo;
            Z1232Projeto_Incremental = A1232Projeto_Incremental;
            Z2144Projeto_Identificador = A2144Projeto_Identificador;
            Z2146Projeto_DTInicio = A2146Projeto_DTInicio;
            Z2147Projeto_DTFim = A2147Projeto_DTFim;
            Z2148Projeto_PrazoPrevisto = A2148Projeto_PrazoPrevisto;
            Z2149Projeto_EsforcoPrevisto = A2149Projeto_EsforcoPrevisto;
            Z2150Projeto_CustoPrevisto = A2150Projeto_CustoPrevisto;
            Z2151Projeto_AreaTrabalhoCodigo = A2151Projeto_AreaTrabalhoCodigo;
            Z2152Projeto_AreaTrabalhoDescricao = A2152Projeto_AreaTrabalhoDescricao;
            Z2153Projeto_GpoObjCtrlCodigo = A2153Projeto_GpoObjCtrlCodigo;
            Z2154Projeto_GpoObjCtrlNome = A2154Projeto_GpoObjCtrlNome;
            Z2155Projeto_SistemaCodigo = A2155Projeto_SistemaCodigo;
            Z2156Projeto_SistemaNome = A2156Projeto_SistemaNome;
            Z2157Projeto_SistemaSigla = A2157Projeto_SistemaSigla;
            Z2158Projeto_ModuloCodigo = A2158Projeto_ModuloCodigo;
            Z2159Projeto_ModuloNome = A2159Projeto_ModuloNome;
            Z2160Projeto_ModuloSigla = A2160Projeto_ModuloSigla;
            Z2170Projeto_AnexoSequecial = A2170Projeto_AnexoSequecial;
         }
         if ( GX_JID == -18 )
         {
            Z648Projeto_Codigo = A648Projeto_Codigo;
            Z2163ProjetoAnexos_Codigo = A2163ProjetoAnexos_Codigo;
            Z2164ProjetoAnexos_Descricao = A2164ProjetoAnexos_Descricao;
            Z2165ProjetoAnexos_Arquivo = A2165ProjetoAnexos_Arquivo;
            Z2168ProjetoAnexos_Link = A2168ProjetoAnexos_Link;
            Z2169ProjetoAnexos_Data = A2169ProjetoAnexos_Data;
            Z2167ProjetoAnexos_ArquivoTipo = A2167ProjetoAnexos_ArquivoTipo;
            Z2166ProjetoAnexos_ArquivoNome = A2166ProjetoAnexos_ArquivoNome;
            Z645TipoDocumento_Codigo = A645TipoDocumento_Codigo;
            Z646TipoDocumento_Nome = A646TipoDocumento_Nome;
         }
      }

      protected void standaloneNotModal29235( )
      {
      }

      protected void standaloneModal29235( )
      {
      }

      protected void Load29235( )
      {
         /* Using cursor BC002932 */
         pr_default.execute(26, new Object[] {A648Projeto_Codigo, A2163ProjetoAnexos_Codigo});
         if ( (pr_default.getStatus(26) != 101) )
         {
            RcdFound235 = 1;
            A2164ProjetoAnexos_Descricao = BC002932_A2164ProjetoAnexos_Descricao[0];
            A2168ProjetoAnexos_Link = BC002932_A2168ProjetoAnexos_Link[0];
            n2168ProjetoAnexos_Link = BC002932_n2168ProjetoAnexos_Link[0];
            A2169ProjetoAnexos_Data = BC002932_A2169ProjetoAnexos_Data[0];
            n2169ProjetoAnexos_Data = BC002932_n2169ProjetoAnexos_Data[0];
            A646TipoDocumento_Nome = BC002932_A646TipoDocumento_Nome[0];
            A2167ProjetoAnexos_ArquivoTipo = BC002932_A2167ProjetoAnexos_ArquivoTipo[0];
            n2167ProjetoAnexos_ArquivoTipo = BC002932_n2167ProjetoAnexos_ArquivoTipo[0];
            A2165ProjetoAnexos_Arquivo_Filetype = A2167ProjetoAnexos_ArquivoTipo;
            A2166ProjetoAnexos_ArquivoNome = BC002932_A2166ProjetoAnexos_ArquivoNome[0];
            n2166ProjetoAnexos_ArquivoNome = BC002932_n2166ProjetoAnexos_ArquivoNome[0];
            A2165ProjetoAnexos_Arquivo_Filename = A2166ProjetoAnexos_ArquivoNome;
            A645TipoDocumento_Codigo = BC002932_A645TipoDocumento_Codigo[0];
            n645TipoDocumento_Codigo = BC002932_n645TipoDocumento_Codigo[0];
            A2165ProjetoAnexos_Arquivo = BC002932_A2165ProjetoAnexos_Arquivo[0];
            n2165ProjetoAnexos_Arquivo = BC002932_n2165ProjetoAnexos_Arquivo[0];
            ZM29235( -18) ;
         }
         pr_default.close(26);
         OnLoadActions29235( ) ;
      }

      protected void OnLoadActions29235( )
      {
      }

      protected void CheckExtendedTable29235( )
      {
         Gx_BScreen = 1;
         standaloneModal29235( ) ;
         Gx_BScreen = 0;
         if ( ! ( (DateTime.MinValue==A2169ProjetoAnexos_Data) || ( A2169ProjetoAnexos_Data >= context.localUtil.YMDHMSToT( 1753, 1, 1, 0, 0, 0) ) ) )
         {
            GX_msglist.addItem("Campo Upload fora do intervalo", "OutOfRange", 1, "");
            AnyError = 1;
         }
         /* Using cursor BC00294 */
         pr_default.execute(2, new Object[] {n645TipoDocumento_Codigo, A645TipoDocumento_Codigo});
         if ( (pr_default.getStatus(2) == 101) )
         {
            if ( ! ( (0==A645TipoDocumento_Codigo) ) )
            {
               GX_msglist.addItem("N�o existe 'Tipo de Documentos'.", "ForeignKeyNotFound", 1, "TIPODOCUMENTO_CODIGO");
               AnyError = 1;
            }
         }
         A646TipoDocumento_Nome = BC00294_A646TipoDocumento_Nome[0];
         pr_default.close(2);
      }

      protected void CloseExtendedTableCursors29235( )
      {
         pr_default.close(2);
      }

      protected void enableDisable29235( )
      {
      }

      protected void GetKey29235( )
      {
         /* Using cursor BC002933 */
         pr_default.execute(27, new Object[] {A648Projeto_Codigo, A2163ProjetoAnexos_Codigo});
         if ( (pr_default.getStatus(27) != 101) )
         {
            RcdFound235 = 1;
         }
         else
         {
            RcdFound235 = 0;
         }
         pr_default.close(27);
      }

      protected void getByPrimaryKey29235( )
      {
         /* Using cursor BC00293 */
         pr_default.execute(1, new Object[] {A648Projeto_Codigo, A2163ProjetoAnexos_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM29235( 18) ;
            RcdFound235 = 1;
            InitializeNonKey29235( ) ;
            A2163ProjetoAnexos_Codigo = BC00293_A2163ProjetoAnexos_Codigo[0];
            A2164ProjetoAnexos_Descricao = BC00293_A2164ProjetoAnexos_Descricao[0];
            A2168ProjetoAnexos_Link = BC00293_A2168ProjetoAnexos_Link[0];
            n2168ProjetoAnexos_Link = BC00293_n2168ProjetoAnexos_Link[0];
            A2169ProjetoAnexos_Data = BC00293_A2169ProjetoAnexos_Data[0];
            n2169ProjetoAnexos_Data = BC00293_n2169ProjetoAnexos_Data[0];
            A2167ProjetoAnexos_ArquivoTipo = BC00293_A2167ProjetoAnexos_ArquivoTipo[0];
            n2167ProjetoAnexos_ArquivoTipo = BC00293_n2167ProjetoAnexos_ArquivoTipo[0];
            A2165ProjetoAnexos_Arquivo_Filetype = A2167ProjetoAnexos_ArquivoTipo;
            A2166ProjetoAnexos_ArquivoNome = BC00293_A2166ProjetoAnexos_ArquivoNome[0];
            n2166ProjetoAnexos_ArquivoNome = BC00293_n2166ProjetoAnexos_ArquivoNome[0];
            A2165ProjetoAnexos_Arquivo_Filename = A2166ProjetoAnexos_ArquivoNome;
            A645TipoDocumento_Codigo = BC00293_A645TipoDocumento_Codigo[0];
            n645TipoDocumento_Codigo = BC00293_n645TipoDocumento_Codigo[0];
            A2165ProjetoAnexos_Arquivo = BC00293_A2165ProjetoAnexos_Arquivo[0];
            n2165ProjetoAnexos_Arquivo = BC00293_n2165ProjetoAnexos_Arquivo[0];
            Z648Projeto_Codigo = A648Projeto_Codigo;
            Z2163ProjetoAnexos_Codigo = A2163ProjetoAnexos_Codigo;
            sMode235 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal29235( ) ;
            Load29235( ) ;
            Gx_mode = sMode235;
         }
         else
         {
            RcdFound235 = 0;
            InitializeNonKey29235( ) ;
            sMode235 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal29235( ) ;
            Gx_mode = sMode235;
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            DisableAttributes29235( ) ;
         }
         pr_default.close(1);
      }

      protected void CheckOptimisticConcurrency29235( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor BC00292 */
            pr_default.execute(0, new Object[] {A648Projeto_Codigo, A2163ProjetoAnexos_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ProjetoAnexos"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( Z2169ProjetoAnexos_Data != BC00292_A2169ProjetoAnexos_Data[0] ) || ( Z645TipoDocumento_Codigo != BC00292_A645TipoDocumento_Codigo[0] ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"ProjetoAnexos"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert29235( )
      {
         BeforeValidate29235( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable29235( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM29235( 0) ;
            CheckOptimisticConcurrency29235( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm29235( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert29235( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC002934 */
                     pr_default.execute(28, new Object[] {A648Projeto_Codigo, A2163ProjetoAnexos_Codigo, A2164ProjetoAnexos_Descricao, n2165ProjetoAnexos_Arquivo, A2165ProjetoAnexos_Arquivo, n2168ProjetoAnexos_Link, A2168ProjetoAnexos_Link, n2169ProjetoAnexos_Data, A2169ProjetoAnexos_Data, n2167ProjetoAnexos_ArquivoTipo, A2167ProjetoAnexos_ArquivoTipo, n2166ProjetoAnexos_ArquivoNome, A2166ProjetoAnexos_ArquivoNome, n645TipoDocumento_Codigo, A645TipoDocumento_Codigo});
                     pr_default.close(28);
                     dsDefault.SmartCacheProvider.SetUpdated("ProjetoAnexos") ;
                     if ( (pr_default.getStatus(28) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load29235( ) ;
            }
            EndLevel29235( ) ;
         }
         CloseExtendedTableCursors29235( ) ;
      }

      protected void Update29235( )
      {
         BeforeValidate29235( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable29235( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency29235( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm29235( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate29235( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC002935 */
                     pr_default.execute(29, new Object[] {A2164ProjetoAnexos_Descricao, n2168ProjetoAnexos_Link, A2168ProjetoAnexos_Link, n2169ProjetoAnexos_Data, A2169ProjetoAnexos_Data, n2167ProjetoAnexos_ArquivoTipo, A2167ProjetoAnexos_ArquivoTipo, n2166ProjetoAnexos_ArquivoNome, A2166ProjetoAnexos_ArquivoNome, n645TipoDocumento_Codigo, A645TipoDocumento_Codigo, A648Projeto_Codigo, A2163ProjetoAnexos_Codigo});
                     pr_default.close(29);
                     dsDefault.SmartCacheProvider.SetUpdated("ProjetoAnexos") ;
                     if ( (pr_default.getStatus(29) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ProjetoAnexos"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate29235( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey29235( ) ;
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel29235( ) ;
         }
         CloseExtendedTableCursors29235( ) ;
      }

      protected void DeferredUpdate29235( )
      {
         if ( AnyError == 0 )
         {
            /* Using cursor BC002936 */
            pr_default.execute(30, new Object[] {n2165ProjetoAnexos_Arquivo, A2165ProjetoAnexos_Arquivo, A648Projeto_Codigo, A2163ProjetoAnexos_Codigo});
            pr_default.close(30);
            dsDefault.SmartCacheProvider.SetUpdated("ProjetoAnexos") ;
         }
      }

      protected void Delete29235( )
      {
         Gx_mode = "DLT";
         BeforeValidate29235( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency29235( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls29235( ) ;
            AfterConfirm29235( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete29235( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor BC002937 */
                  pr_default.execute(31, new Object[] {A648Projeto_Codigo, A2163ProjetoAnexos_Codigo});
                  pr_default.close(31);
                  dsDefault.SmartCacheProvider.SetUpdated("ProjetoAnexos") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode235 = Gx_mode;
         Gx_mode = "DLT";
         EndLevel29235( ) ;
         Gx_mode = sMode235;
      }

      protected void OnDeleteControls29235( )
      {
         standaloneModal29235( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor BC002938 */
            pr_default.execute(32, new Object[] {n645TipoDocumento_Codigo, A645TipoDocumento_Codigo});
            A646TipoDocumento_Nome = BC002938_A646TipoDocumento_Nome[0];
            pr_default.close(32);
         }
      }

      protected void EndLevel29235( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanKeyStart29235( )
      {
         /* Scan By routine */
         /* Using cursor BC002939 */
         pr_default.execute(33, new Object[] {A648Projeto_Codigo});
         RcdFound235 = 0;
         if ( (pr_default.getStatus(33) != 101) )
         {
            RcdFound235 = 1;
            A2163ProjetoAnexos_Codigo = BC002939_A2163ProjetoAnexos_Codigo[0];
            A2164ProjetoAnexos_Descricao = BC002939_A2164ProjetoAnexos_Descricao[0];
            A2168ProjetoAnexos_Link = BC002939_A2168ProjetoAnexos_Link[0];
            n2168ProjetoAnexos_Link = BC002939_n2168ProjetoAnexos_Link[0];
            A2169ProjetoAnexos_Data = BC002939_A2169ProjetoAnexos_Data[0];
            n2169ProjetoAnexos_Data = BC002939_n2169ProjetoAnexos_Data[0];
            A646TipoDocumento_Nome = BC002939_A646TipoDocumento_Nome[0];
            A2167ProjetoAnexos_ArquivoTipo = BC002939_A2167ProjetoAnexos_ArquivoTipo[0];
            n2167ProjetoAnexos_ArquivoTipo = BC002939_n2167ProjetoAnexos_ArquivoTipo[0];
            A2165ProjetoAnexos_Arquivo_Filetype = A2167ProjetoAnexos_ArquivoTipo;
            A2166ProjetoAnexos_ArquivoNome = BC002939_A2166ProjetoAnexos_ArquivoNome[0];
            n2166ProjetoAnexos_ArquivoNome = BC002939_n2166ProjetoAnexos_ArquivoNome[0];
            A2165ProjetoAnexos_Arquivo_Filename = A2166ProjetoAnexos_ArquivoNome;
            A645TipoDocumento_Codigo = BC002939_A645TipoDocumento_Codigo[0];
            n645TipoDocumento_Codigo = BC002939_n645TipoDocumento_Codigo[0];
            A2165ProjetoAnexos_Arquivo = BC002939_A2165ProjetoAnexos_Arquivo[0];
            n2165ProjetoAnexos_Arquivo = BC002939_n2165ProjetoAnexos_Arquivo[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanKeyNext29235( )
      {
         /* Scan next routine */
         pr_default.readNext(33);
         RcdFound235 = 0;
         ScanKeyLoad29235( ) ;
      }

      protected void ScanKeyLoad29235( )
      {
         sMode235 = Gx_mode;
         Gx_mode = "DSP";
         if ( (pr_default.getStatus(33) != 101) )
         {
            RcdFound235 = 1;
            A2163ProjetoAnexos_Codigo = BC002939_A2163ProjetoAnexos_Codigo[0];
            A2164ProjetoAnexos_Descricao = BC002939_A2164ProjetoAnexos_Descricao[0];
            A2168ProjetoAnexos_Link = BC002939_A2168ProjetoAnexos_Link[0];
            n2168ProjetoAnexos_Link = BC002939_n2168ProjetoAnexos_Link[0];
            A2169ProjetoAnexos_Data = BC002939_A2169ProjetoAnexos_Data[0];
            n2169ProjetoAnexos_Data = BC002939_n2169ProjetoAnexos_Data[0];
            A646TipoDocumento_Nome = BC002939_A646TipoDocumento_Nome[0];
            A2167ProjetoAnexos_ArquivoTipo = BC002939_A2167ProjetoAnexos_ArquivoTipo[0];
            n2167ProjetoAnexos_ArquivoTipo = BC002939_n2167ProjetoAnexos_ArquivoTipo[0];
            A2165ProjetoAnexos_Arquivo_Filetype = A2167ProjetoAnexos_ArquivoTipo;
            A2166ProjetoAnexos_ArquivoNome = BC002939_A2166ProjetoAnexos_ArquivoNome[0];
            n2166ProjetoAnexos_ArquivoNome = BC002939_n2166ProjetoAnexos_ArquivoNome[0];
            A2165ProjetoAnexos_Arquivo_Filename = A2166ProjetoAnexos_ArquivoNome;
            A645TipoDocumento_Codigo = BC002939_A645TipoDocumento_Codigo[0];
            n645TipoDocumento_Codigo = BC002939_n645TipoDocumento_Codigo[0];
            A2165ProjetoAnexos_Arquivo = BC002939_A2165ProjetoAnexos_Arquivo[0];
            n2165ProjetoAnexos_Arquivo = BC002939_n2165ProjetoAnexos_Arquivo[0];
         }
         Gx_mode = sMode235;
      }

      protected void ScanKeyEnd29235( )
      {
         pr_default.close(33);
      }

      protected void AfterConfirm29235( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert29235( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate29235( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete29235( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete29235( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate29235( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes29235( )
      {
      }

      protected void AddRow2986( )
      {
         VarsToRow86( bcProjeto) ;
      }

      protected void ReadRow2986( )
      {
         RowToVars86( bcProjeto, 1) ;
      }

      protected void AddRow29235( )
      {
         SdtProjeto_Anexos obj235 ;
         obj235 = new SdtProjeto_Anexos(context);
         VarsToRow235( obj235) ;
         bcProjeto.gxTpr_Anexos.Add(obj235, 0);
         obj235.gxTpr_Mode = "UPD";
         obj235.gxTpr_Modified = 0;
      }

      protected void ReadRow29235( )
      {
         nGXsfl_235_idx = (short)(nGXsfl_235_idx+1);
         RowToVars235( ((SdtProjeto_Anexos)bcProjeto.gxTpr_Anexos.Item(nGXsfl_235_idx)), 1) ;
      }

      protected void InitializeNonKey2986( )
      {
         A740Projeto_SistemaCod = 0;
         A649Projeto_Nome = "";
         A650Projeto_Sigla = "";
         A983Projeto_TipoProjetoCod = 0;
         n983Projeto_TipoProjetoCod = false;
         A652Projeto_TecnicaContagem = "";
         A1540Projeto_Introducao = "";
         n1540Projeto_Introducao = false;
         A653Projeto_Escopo = "";
         A1541Projeto_Previsao = DateTime.MinValue;
         n1541Projeto_Previsao = false;
         A1542Projeto_GerenteCod = 0;
         n1542Projeto_GerenteCod = false;
         A1543Projeto_ServicoCod = 0;
         n1543Projeto_ServicoCod = false;
         A655Projeto_Custo = 0;
         A656Projeto_Prazo = 0;
         A657Projeto_Esforco = 0;
         n657Projeto_Esforco = false;
         A985Projeto_FatorEscala = 0;
         n985Projeto_FatorEscala = false;
         A989Projeto_FatorMultiplicador = 0;
         n989Projeto_FatorMultiplicador = false;
         A990Projeto_ConstACocomo = 0;
         n990Projeto_ConstACocomo = false;
         A1232Projeto_Incremental = false;
         n1232Projeto_Incremental = false;
         A2144Projeto_Identificador = "";
         n2144Projeto_Identificador = false;
         A2145Projeto_Objetivo = "";
         n2145Projeto_Objetivo = false;
         A2146Projeto_DTInicio = DateTime.MinValue;
         n2146Projeto_DTInicio = false;
         A2147Projeto_DTFim = DateTime.MinValue;
         n2147Projeto_DTFim = false;
         A2148Projeto_PrazoPrevisto = 0;
         n2148Projeto_PrazoPrevisto = false;
         A2149Projeto_EsforcoPrevisto = 0;
         n2149Projeto_EsforcoPrevisto = false;
         A2150Projeto_CustoPrevisto = 0;
         n2150Projeto_CustoPrevisto = false;
         A2151Projeto_AreaTrabalhoCodigo = 0;
         n2151Projeto_AreaTrabalhoCodigo = false;
         A2152Projeto_AreaTrabalhoDescricao = "";
         n2152Projeto_AreaTrabalhoDescricao = false;
         A2153Projeto_GpoObjCtrlCodigo = 0;
         n2153Projeto_GpoObjCtrlCodigo = false;
         A2154Projeto_GpoObjCtrlNome = "";
         n2154Projeto_GpoObjCtrlNome = false;
         A2155Projeto_SistemaCodigo = 0;
         n2155Projeto_SistemaCodigo = false;
         A2156Projeto_SistemaNome = "";
         n2156Projeto_SistemaNome = false;
         A2157Projeto_SistemaSigla = "";
         n2157Projeto_SistemaSigla = false;
         A2158Projeto_ModuloCodigo = 0;
         n2158Projeto_ModuloCodigo = false;
         A2159Projeto_ModuloNome = "";
         n2159Projeto_ModuloNome = false;
         A2160Projeto_ModuloSigla = "";
         n2160Projeto_ModuloSigla = false;
         A2170Projeto_AnexoSequecial = 0;
         n2170Projeto_AnexoSequecial = false;
         A658Projeto_Status = "A";
         Z649Projeto_Nome = "";
         Z650Projeto_Sigla = "";
         Z652Projeto_TecnicaContagem = "";
         Z1541Projeto_Previsao = DateTime.MinValue;
         Z1542Projeto_GerenteCod = 0;
         Z1543Projeto_ServicoCod = 0;
         Z658Projeto_Status = "";
         Z985Projeto_FatorEscala = 0;
         Z989Projeto_FatorMultiplicador = 0;
         Z990Projeto_ConstACocomo = 0;
         Z1232Projeto_Incremental = false;
         Z2144Projeto_Identificador = "";
         Z2146Projeto_DTInicio = DateTime.MinValue;
         Z2147Projeto_DTFim = DateTime.MinValue;
         Z2148Projeto_PrazoPrevisto = 0;
         Z2149Projeto_EsforcoPrevisto = 0;
         Z2150Projeto_CustoPrevisto = 0;
         Z2170Projeto_AnexoSequecial = 0;
         Z983Projeto_TipoProjetoCod = 0;
         Z2151Projeto_AreaTrabalhoCodigo = 0;
         Z2153Projeto_GpoObjCtrlCodigo = 0;
         Z2155Projeto_SistemaCodigo = 0;
         Z2158Projeto_ModuloCodigo = 0;
      }

      protected void InitAll2986( )
      {
         A648Projeto_Codigo = 0;
         InitializeNonKey2986( ) ;
      }

      protected void StandaloneModalInsert( )
      {
         A658Projeto_Status = i658Projeto_Status;
      }

      protected void InitializeNonKey29235( )
      {
         A2164ProjetoAnexos_Descricao = "";
         A2165ProjetoAnexos_Arquivo = "";
         n2165ProjetoAnexos_Arquivo = false;
         A2168ProjetoAnexos_Link = "";
         n2168ProjetoAnexos_Link = false;
         A2169ProjetoAnexos_Data = (DateTime)(DateTime.MinValue);
         n2169ProjetoAnexos_Data = false;
         A645TipoDocumento_Codigo = 0;
         n645TipoDocumento_Codigo = false;
         A646TipoDocumento_Nome = "";
         A2167ProjetoAnexos_ArquivoTipo = "";
         n2167ProjetoAnexos_ArquivoTipo = false;
         A2166ProjetoAnexos_ArquivoNome = "";
         n2166ProjetoAnexos_ArquivoNome = false;
         Z2169ProjetoAnexos_Data = (DateTime)(DateTime.MinValue);
         Z645TipoDocumento_Codigo = 0;
      }

      protected void InitAll29235( )
      {
         A2163ProjetoAnexos_Codigo = 0;
         InitializeNonKey29235( ) ;
      }

      protected void StandaloneModalInsert29235( )
      {
      }

      public void VarsToRow86( SdtProjeto obj86 )
      {
         obj86.gxTpr_Mode = Gx_mode;
         obj86.gxTpr_Projeto_sistemacod = A740Projeto_SistemaCod;
         obj86.gxTpr_Projeto_nome = A649Projeto_Nome;
         obj86.gxTpr_Projeto_sigla = A650Projeto_Sigla;
         obj86.gxTpr_Projeto_tipoprojetocod = A983Projeto_TipoProjetoCod;
         obj86.gxTpr_Projeto_tecnicacontagem = A652Projeto_TecnicaContagem;
         obj86.gxTpr_Projeto_introducao = A1540Projeto_Introducao;
         obj86.gxTpr_Projeto_escopo = A653Projeto_Escopo;
         obj86.gxTpr_Projeto_previsao = A1541Projeto_Previsao;
         obj86.gxTpr_Projeto_gerentecod = A1542Projeto_GerenteCod;
         obj86.gxTpr_Projeto_servicocod = A1543Projeto_ServicoCod;
         obj86.gxTpr_Projeto_custo = A655Projeto_Custo;
         obj86.gxTpr_Projeto_prazo = A656Projeto_Prazo;
         obj86.gxTpr_Projeto_esforco = A657Projeto_Esforco;
         obj86.gxTpr_Projeto_fatorescala = A985Projeto_FatorEscala;
         obj86.gxTpr_Projeto_fatormultiplicador = A989Projeto_FatorMultiplicador;
         obj86.gxTpr_Projeto_constacocomo = A990Projeto_ConstACocomo;
         obj86.gxTpr_Projeto_incremental = A1232Projeto_Incremental;
         obj86.gxTpr_Projeto_identificador = A2144Projeto_Identificador;
         obj86.gxTpr_Projeto_objetivo = A2145Projeto_Objetivo;
         obj86.gxTpr_Projeto_dtinicio = A2146Projeto_DTInicio;
         obj86.gxTpr_Projeto_dtfim = A2147Projeto_DTFim;
         obj86.gxTpr_Projeto_prazoprevisto = A2148Projeto_PrazoPrevisto;
         obj86.gxTpr_Projeto_esforcoprevisto = A2149Projeto_EsforcoPrevisto;
         obj86.gxTpr_Projeto_custoprevisto = A2150Projeto_CustoPrevisto;
         obj86.gxTpr_Projeto_areatrabalhocodigo = A2151Projeto_AreaTrabalhoCodigo;
         obj86.gxTpr_Projeto_areatrabalhodescricao = A2152Projeto_AreaTrabalhoDescricao;
         obj86.gxTpr_Projeto_gpoobjctrlcodigo = A2153Projeto_GpoObjCtrlCodigo;
         obj86.gxTpr_Projeto_gpoobjctrlnome = A2154Projeto_GpoObjCtrlNome;
         obj86.gxTpr_Projeto_sistemacodigo = A2155Projeto_SistemaCodigo;
         obj86.gxTpr_Projeto_sistemanome = A2156Projeto_SistemaNome;
         obj86.gxTpr_Projeto_sistemasigla = A2157Projeto_SistemaSigla;
         obj86.gxTpr_Projeto_modulocodigo = A2158Projeto_ModuloCodigo;
         obj86.gxTpr_Projeto_modulonome = A2159Projeto_ModuloNome;
         obj86.gxTpr_Projeto_modulosigla = A2160Projeto_ModuloSigla;
         obj86.gxTpr_Projeto_anexosequecial = A2170Projeto_AnexoSequecial;
         obj86.gxTpr_Projeto_status = A658Projeto_Status;
         obj86.gxTpr_Projeto_codigo = A648Projeto_Codigo;
         obj86.gxTpr_Projeto_codigo_Z = Z648Projeto_Codigo;
         obj86.gxTpr_Projeto_nome_Z = Z649Projeto_Nome;
         obj86.gxTpr_Projeto_sigla_Z = Z650Projeto_Sigla;
         obj86.gxTpr_Projeto_tipoprojetocod_Z = Z983Projeto_TipoProjetoCod;
         obj86.gxTpr_Projeto_tecnicacontagem_Z = Z652Projeto_TecnicaContagem;
         obj86.gxTpr_Projeto_previsao_Z = Z1541Projeto_Previsao;
         obj86.gxTpr_Projeto_gerentecod_Z = Z1542Projeto_GerenteCod;
         obj86.gxTpr_Projeto_servicocod_Z = Z1543Projeto_ServicoCod;
         obj86.gxTpr_Projeto_custo_Z = Z655Projeto_Custo;
         obj86.gxTpr_Projeto_prazo_Z = Z656Projeto_Prazo;
         obj86.gxTpr_Projeto_esforco_Z = Z657Projeto_Esforco;
         obj86.gxTpr_Projeto_sistemacod_Z = Z740Projeto_SistemaCod;
         obj86.gxTpr_Projeto_status_Z = Z658Projeto_Status;
         obj86.gxTpr_Projeto_fatorescala_Z = Z985Projeto_FatorEscala;
         obj86.gxTpr_Projeto_fatormultiplicador_Z = Z989Projeto_FatorMultiplicador;
         obj86.gxTpr_Projeto_constacocomo_Z = Z990Projeto_ConstACocomo;
         obj86.gxTpr_Projeto_incremental_Z = Z1232Projeto_Incremental;
         obj86.gxTpr_Projeto_identificador_Z = Z2144Projeto_Identificador;
         obj86.gxTpr_Projeto_dtinicio_Z = Z2146Projeto_DTInicio;
         obj86.gxTpr_Projeto_dtfim_Z = Z2147Projeto_DTFim;
         obj86.gxTpr_Projeto_prazoprevisto_Z = Z2148Projeto_PrazoPrevisto;
         obj86.gxTpr_Projeto_esforcoprevisto_Z = Z2149Projeto_EsforcoPrevisto;
         obj86.gxTpr_Projeto_custoprevisto_Z = Z2150Projeto_CustoPrevisto;
         obj86.gxTpr_Projeto_areatrabalhocodigo_Z = Z2151Projeto_AreaTrabalhoCodigo;
         obj86.gxTpr_Projeto_areatrabalhodescricao_Z = Z2152Projeto_AreaTrabalhoDescricao;
         obj86.gxTpr_Projeto_gpoobjctrlcodigo_Z = Z2153Projeto_GpoObjCtrlCodigo;
         obj86.gxTpr_Projeto_gpoobjctrlnome_Z = Z2154Projeto_GpoObjCtrlNome;
         obj86.gxTpr_Projeto_sistemacodigo_Z = Z2155Projeto_SistemaCodigo;
         obj86.gxTpr_Projeto_sistemanome_Z = Z2156Projeto_SistemaNome;
         obj86.gxTpr_Projeto_sistemasigla_Z = Z2157Projeto_SistemaSigla;
         obj86.gxTpr_Projeto_modulocodigo_Z = Z2158Projeto_ModuloCodigo;
         obj86.gxTpr_Projeto_modulonome_Z = Z2159Projeto_ModuloNome;
         obj86.gxTpr_Projeto_modulosigla_Z = Z2160Projeto_ModuloSigla;
         obj86.gxTpr_Projeto_anexosequecial_Z = Z2170Projeto_AnexoSequecial;
         obj86.gxTpr_Projeto_tipoprojetocod_N = (short)(Convert.ToInt16(n983Projeto_TipoProjetoCod));
         obj86.gxTpr_Projeto_introducao_N = (short)(Convert.ToInt16(n1540Projeto_Introducao));
         obj86.gxTpr_Projeto_previsao_N = (short)(Convert.ToInt16(n1541Projeto_Previsao));
         obj86.gxTpr_Projeto_gerentecod_N = (short)(Convert.ToInt16(n1542Projeto_GerenteCod));
         obj86.gxTpr_Projeto_servicocod_N = (short)(Convert.ToInt16(n1543Projeto_ServicoCod));
         obj86.gxTpr_Projeto_esforco_N = (short)(Convert.ToInt16(n657Projeto_Esforco));
         obj86.gxTpr_Projeto_fatorescala_N = (short)(Convert.ToInt16(n985Projeto_FatorEscala));
         obj86.gxTpr_Projeto_fatormultiplicador_N = (short)(Convert.ToInt16(n989Projeto_FatorMultiplicador));
         obj86.gxTpr_Projeto_constacocomo_N = (short)(Convert.ToInt16(n990Projeto_ConstACocomo));
         obj86.gxTpr_Projeto_incremental_N = (short)(Convert.ToInt16(n1232Projeto_Incremental));
         obj86.gxTpr_Projeto_identificador_N = (short)(Convert.ToInt16(n2144Projeto_Identificador));
         obj86.gxTpr_Projeto_objetivo_N = (short)(Convert.ToInt16(n2145Projeto_Objetivo));
         obj86.gxTpr_Projeto_dtinicio_N = (short)(Convert.ToInt16(n2146Projeto_DTInicio));
         obj86.gxTpr_Projeto_dtfim_N = (short)(Convert.ToInt16(n2147Projeto_DTFim));
         obj86.gxTpr_Projeto_prazoprevisto_N = (short)(Convert.ToInt16(n2148Projeto_PrazoPrevisto));
         obj86.gxTpr_Projeto_esforcoprevisto_N = (short)(Convert.ToInt16(n2149Projeto_EsforcoPrevisto));
         obj86.gxTpr_Projeto_custoprevisto_N = (short)(Convert.ToInt16(n2150Projeto_CustoPrevisto));
         obj86.gxTpr_Projeto_areatrabalhocodigo_N = (short)(Convert.ToInt16(n2151Projeto_AreaTrabalhoCodigo));
         obj86.gxTpr_Projeto_areatrabalhodescricao_N = (short)(Convert.ToInt16(n2152Projeto_AreaTrabalhoDescricao));
         obj86.gxTpr_Projeto_gpoobjctrlcodigo_N = (short)(Convert.ToInt16(n2153Projeto_GpoObjCtrlCodigo));
         obj86.gxTpr_Projeto_gpoobjctrlnome_N = (short)(Convert.ToInt16(n2154Projeto_GpoObjCtrlNome));
         obj86.gxTpr_Projeto_sistemacodigo_N = (short)(Convert.ToInt16(n2155Projeto_SistemaCodigo));
         obj86.gxTpr_Projeto_sistemanome_N = (short)(Convert.ToInt16(n2156Projeto_SistemaNome));
         obj86.gxTpr_Projeto_sistemasigla_N = (short)(Convert.ToInt16(n2157Projeto_SistemaSigla));
         obj86.gxTpr_Projeto_modulocodigo_N = (short)(Convert.ToInt16(n2158Projeto_ModuloCodigo));
         obj86.gxTpr_Projeto_modulonome_N = (short)(Convert.ToInt16(n2159Projeto_ModuloNome));
         obj86.gxTpr_Projeto_modulosigla_N = (short)(Convert.ToInt16(n2160Projeto_ModuloSigla));
         obj86.gxTpr_Projeto_anexosequecial_N = (short)(Convert.ToInt16(n2170Projeto_AnexoSequecial));
         obj86.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void KeyVarsToRow86( SdtProjeto obj86 )
      {
         obj86.gxTpr_Projeto_codigo = A648Projeto_Codigo;
         return  ;
      }

      public void RowToVars86( SdtProjeto obj86 ,
                               int forceLoad )
      {
         Gx_mode = obj86.gxTpr_Mode;
         A740Projeto_SistemaCod = obj86.gxTpr_Projeto_sistemacod;
         A649Projeto_Nome = obj86.gxTpr_Projeto_nome;
         A650Projeto_Sigla = obj86.gxTpr_Projeto_sigla;
         A983Projeto_TipoProjetoCod = obj86.gxTpr_Projeto_tipoprojetocod;
         n983Projeto_TipoProjetoCod = false;
         A652Projeto_TecnicaContagem = obj86.gxTpr_Projeto_tecnicacontagem;
         A1540Projeto_Introducao = obj86.gxTpr_Projeto_introducao;
         n1540Projeto_Introducao = false;
         A653Projeto_Escopo = obj86.gxTpr_Projeto_escopo;
         A1541Projeto_Previsao = obj86.gxTpr_Projeto_previsao;
         n1541Projeto_Previsao = false;
         A1542Projeto_GerenteCod = obj86.gxTpr_Projeto_gerentecod;
         n1542Projeto_GerenteCod = false;
         A1543Projeto_ServicoCod = obj86.gxTpr_Projeto_servicocod;
         n1543Projeto_ServicoCod = false;
         A655Projeto_Custo = obj86.gxTpr_Projeto_custo;
         A656Projeto_Prazo = obj86.gxTpr_Projeto_prazo;
         A657Projeto_Esforco = obj86.gxTpr_Projeto_esforco;
         n657Projeto_Esforco = false;
         A985Projeto_FatorEscala = obj86.gxTpr_Projeto_fatorescala;
         n985Projeto_FatorEscala = false;
         A989Projeto_FatorMultiplicador = obj86.gxTpr_Projeto_fatormultiplicador;
         n989Projeto_FatorMultiplicador = false;
         A990Projeto_ConstACocomo = obj86.gxTpr_Projeto_constacocomo;
         n990Projeto_ConstACocomo = false;
         A1232Projeto_Incremental = obj86.gxTpr_Projeto_incremental;
         n1232Projeto_Incremental = false;
         A2144Projeto_Identificador = obj86.gxTpr_Projeto_identificador;
         n2144Projeto_Identificador = false;
         A2145Projeto_Objetivo = obj86.gxTpr_Projeto_objetivo;
         n2145Projeto_Objetivo = false;
         A2146Projeto_DTInicio = obj86.gxTpr_Projeto_dtinicio;
         n2146Projeto_DTInicio = false;
         A2147Projeto_DTFim = obj86.gxTpr_Projeto_dtfim;
         n2147Projeto_DTFim = false;
         A2148Projeto_PrazoPrevisto = obj86.gxTpr_Projeto_prazoprevisto;
         n2148Projeto_PrazoPrevisto = false;
         A2149Projeto_EsforcoPrevisto = obj86.gxTpr_Projeto_esforcoprevisto;
         n2149Projeto_EsforcoPrevisto = false;
         A2150Projeto_CustoPrevisto = obj86.gxTpr_Projeto_custoprevisto;
         n2150Projeto_CustoPrevisto = false;
         A2151Projeto_AreaTrabalhoCodigo = obj86.gxTpr_Projeto_areatrabalhocodigo;
         n2151Projeto_AreaTrabalhoCodigo = false;
         A2152Projeto_AreaTrabalhoDescricao = obj86.gxTpr_Projeto_areatrabalhodescricao;
         n2152Projeto_AreaTrabalhoDescricao = false;
         A2153Projeto_GpoObjCtrlCodigo = obj86.gxTpr_Projeto_gpoobjctrlcodigo;
         n2153Projeto_GpoObjCtrlCodigo = false;
         A2154Projeto_GpoObjCtrlNome = obj86.gxTpr_Projeto_gpoobjctrlnome;
         n2154Projeto_GpoObjCtrlNome = false;
         A2155Projeto_SistemaCodigo = obj86.gxTpr_Projeto_sistemacodigo;
         n2155Projeto_SistemaCodigo = false;
         A2156Projeto_SistemaNome = obj86.gxTpr_Projeto_sistemanome;
         n2156Projeto_SistemaNome = false;
         A2157Projeto_SistemaSigla = obj86.gxTpr_Projeto_sistemasigla;
         n2157Projeto_SistemaSigla = false;
         A2158Projeto_ModuloCodigo = obj86.gxTpr_Projeto_modulocodigo;
         n2158Projeto_ModuloCodigo = false;
         A2159Projeto_ModuloNome = obj86.gxTpr_Projeto_modulonome;
         n2159Projeto_ModuloNome = false;
         A2160Projeto_ModuloSigla = obj86.gxTpr_Projeto_modulosigla;
         n2160Projeto_ModuloSigla = false;
         A2170Projeto_AnexoSequecial = obj86.gxTpr_Projeto_anexosequecial;
         n2170Projeto_AnexoSequecial = false;
         A658Projeto_Status = obj86.gxTpr_Projeto_status;
         A648Projeto_Codigo = obj86.gxTpr_Projeto_codigo;
         Z648Projeto_Codigo = obj86.gxTpr_Projeto_codigo_Z;
         Z649Projeto_Nome = obj86.gxTpr_Projeto_nome_Z;
         Z650Projeto_Sigla = obj86.gxTpr_Projeto_sigla_Z;
         Z983Projeto_TipoProjetoCod = obj86.gxTpr_Projeto_tipoprojetocod_Z;
         Z652Projeto_TecnicaContagem = obj86.gxTpr_Projeto_tecnicacontagem_Z;
         Z1541Projeto_Previsao = obj86.gxTpr_Projeto_previsao_Z;
         Z1542Projeto_GerenteCod = obj86.gxTpr_Projeto_gerentecod_Z;
         Z1543Projeto_ServicoCod = obj86.gxTpr_Projeto_servicocod_Z;
         Z655Projeto_Custo = obj86.gxTpr_Projeto_custo_Z;
         Z656Projeto_Prazo = obj86.gxTpr_Projeto_prazo_Z;
         Z657Projeto_Esforco = obj86.gxTpr_Projeto_esforco_Z;
         Z740Projeto_SistemaCod = obj86.gxTpr_Projeto_sistemacod_Z;
         Z658Projeto_Status = obj86.gxTpr_Projeto_status_Z;
         Z985Projeto_FatorEscala = obj86.gxTpr_Projeto_fatorescala_Z;
         Z989Projeto_FatorMultiplicador = obj86.gxTpr_Projeto_fatormultiplicador_Z;
         Z990Projeto_ConstACocomo = obj86.gxTpr_Projeto_constacocomo_Z;
         Z1232Projeto_Incremental = obj86.gxTpr_Projeto_incremental_Z;
         Z2144Projeto_Identificador = obj86.gxTpr_Projeto_identificador_Z;
         Z2146Projeto_DTInicio = obj86.gxTpr_Projeto_dtinicio_Z;
         Z2147Projeto_DTFim = obj86.gxTpr_Projeto_dtfim_Z;
         Z2148Projeto_PrazoPrevisto = obj86.gxTpr_Projeto_prazoprevisto_Z;
         Z2149Projeto_EsforcoPrevisto = obj86.gxTpr_Projeto_esforcoprevisto_Z;
         Z2150Projeto_CustoPrevisto = obj86.gxTpr_Projeto_custoprevisto_Z;
         Z2151Projeto_AreaTrabalhoCodigo = obj86.gxTpr_Projeto_areatrabalhocodigo_Z;
         Z2152Projeto_AreaTrabalhoDescricao = obj86.gxTpr_Projeto_areatrabalhodescricao_Z;
         Z2153Projeto_GpoObjCtrlCodigo = obj86.gxTpr_Projeto_gpoobjctrlcodigo_Z;
         Z2154Projeto_GpoObjCtrlNome = obj86.gxTpr_Projeto_gpoobjctrlnome_Z;
         Z2155Projeto_SistemaCodigo = obj86.gxTpr_Projeto_sistemacodigo_Z;
         Z2156Projeto_SistemaNome = obj86.gxTpr_Projeto_sistemanome_Z;
         Z2157Projeto_SistemaSigla = obj86.gxTpr_Projeto_sistemasigla_Z;
         Z2158Projeto_ModuloCodigo = obj86.gxTpr_Projeto_modulocodigo_Z;
         Z2159Projeto_ModuloNome = obj86.gxTpr_Projeto_modulonome_Z;
         Z2160Projeto_ModuloSigla = obj86.gxTpr_Projeto_modulosigla_Z;
         Z2170Projeto_AnexoSequecial = obj86.gxTpr_Projeto_anexosequecial_Z;
         n983Projeto_TipoProjetoCod = (bool)(Convert.ToBoolean(obj86.gxTpr_Projeto_tipoprojetocod_N));
         n1540Projeto_Introducao = (bool)(Convert.ToBoolean(obj86.gxTpr_Projeto_introducao_N));
         n1541Projeto_Previsao = (bool)(Convert.ToBoolean(obj86.gxTpr_Projeto_previsao_N));
         n1542Projeto_GerenteCod = (bool)(Convert.ToBoolean(obj86.gxTpr_Projeto_gerentecod_N));
         n1543Projeto_ServicoCod = (bool)(Convert.ToBoolean(obj86.gxTpr_Projeto_servicocod_N));
         n657Projeto_Esforco = (bool)(Convert.ToBoolean(obj86.gxTpr_Projeto_esforco_N));
         n985Projeto_FatorEscala = (bool)(Convert.ToBoolean(obj86.gxTpr_Projeto_fatorescala_N));
         n989Projeto_FatorMultiplicador = (bool)(Convert.ToBoolean(obj86.gxTpr_Projeto_fatormultiplicador_N));
         n990Projeto_ConstACocomo = (bool)(Convert.ToBoolean(obj86.gxTpr_Projeto_constacocomo_N));
         n1232Projeto_Incremental = (bool)(Convert.ToBoolean(obj86.gxTpr_Projeto_incremental_N));
         n2144Projeto_Identificador = (bool)(Convert.ToBoolean(obj86.gxTpr_Projeto_identificador_N));
         n2145Projeto_Objetivo = (bool)(Convert.ToBoolean(obj86.gxTpr_Projeto_objetivo_N));
         n2146Projeto_DTInicio = (bool)(Convert.ToBoolean(obj86.gxTpr_Projeto_dtinicio_N));
         n2147Projeto_DTFim = (bool)(Convert.ToBoolean(obj86.gxTpr_Projeto_dtfim_N));
         n2148Projeto_PrazoPrevisto = (bool)(Convert.ToBoolean(obj86.gxTpr_Projeto_prazoprevisto_N));
         n2149Projeto_EsforcoPrevisto = (bool)(Convert.ToBoolean(obj86.gxTpr_Projeto_esforcoprevisto_N));
         n2150Projeto_CustoPrevisto = (bool)(Convert.ToBoolean(obj86.gxTpr_Projeto_custoprevisto_N));
         n2151Projeto_AreaTrabalhoCodigo = (bool)(Convert.ToBoolean(obj86.gxTpr_Projeto_areatrabalhocodigo_N));
         n2152Projeto_AreaTrabalhoDescricao = (bool)(Convert.ToBoolean(obj86.gxTpr_Projeto_areatrabalhodescricao_N));
         n2153Projeto_GpoObjCtrlCodigo = (bool)(Convert.ToBoolean(obj86.gxTpr_Projeto_gpoobjctrlcodigo_N));
         n2154Projeto_GpoObjCtrlNome = (bool)(Convert.ToBoolean(obj86.gxTpr_Projeto_gpoobjctrlnome_N));
         n2155Projeto_SistemaCodigo = (bool)(Convert.ToBoolean(obj86.gxTpr_Projeto_sistemacodigo_N));
         n2156Projeto_SistemaNome = (bool)(Convert.ToBoolean(obj86.gxTpr_Projeto_sistemanome_N));
         n2157Projeto_SistemaSigla = (bool)(Convert.ToBoolean(obj86.gxTpr_Projeto_sistemasigla_N));
         n2158Projeto_ModuloCodigo = (bool)(Convert.ToBoolean(obj86.gxTpr_Projeto_modulocodigo_N));
         n2159Projeto_ModuloNome = (bool)(Convert.ToBoolean(obj86.gxTpr_Projeto_modulonome_N));
         n2160Projeto_ModuloSigla = (bool)(Convert.ToBoolean(obj86.gxTpr_Projeto_modulosigla_N));
         n2170Projeto_AnexoSequecial = (bool)(Convert.ToBoolean(obj86.gxTpr_Projeto_anexosequecial_N));
         Gx_mode = obj86.gxTpr_Mode;
         return  ;
      }

      public void VarsToRow235( SdtProjeto_Anexos obj235 )
      {
         obj235.gxTpr_Mode = Gx_mode;
         obj235.gxTpr_Projetoanexos_descricao = A2164ProjetoAnexos_Descricao;
         obj235.gxTpr_Projetoanexos_arquivo = A2165ProjetoAnexos_Arquivo;
         obj235.gxTpr_Projetoanexos_link = A2168ProjetoAnexos_Link;
         obj235.gxTpr_Projetoanexos_data = A2169ProjetoAnexos_Data;
         obj235.gxTpr_Tipodocumento_codigo = A645TipoDocumento_Codigo;
         obj235.gxTpr_Tipodocumento_nome = A646TipoDocumento_Nome;
         obj235.gxTpr_Projetoanexos_arquivotipo = A2167ProjetoAnexos_ArquivoTipo;
         obj235.gxTpr_Projetoanexos_arquivonome = A2166ProjetoAnexos_ArquivoNome;
         obj235.gxTpr_Projetoanexos_codigo = A2163ProjetoAnexos_Codigo;
         obj235.gxTpr_Projetoanexos_codigo_Z = Z2163ProjetoAnexos_Codigo;
         obj235.gxTpr_Projetoanexos_arquivonome_Z = Z2166ProjetoAnexos_ArquivoNome;
         obj235.gxTpr_Projetoanexos_arquivotipo_Z = Z2167ProjetoAnexos_ArquivoTipo;
         obj235.gxTpr_Projetoanexos_data_Z = Z2169ProjetoAnexos_Data;
         obj235.gxTpr_Tipodocumento_codigo_Z = Z645TipoDocumento_Codigo;
         obj235.gxTpr_Tipodocumento_nome_Z = Z646TipoDocumento_Nome;
         obj235.gxTpr_Projetoanexos_arquivo_N = (short)(Convert.ToInt16(n2165ProjetoAnexos_Arquivo));
         obj235.gxTpr_Projetoanexos_arquivonome_N = (short)(Convert.ToInt16(n2166ProjetoAnexos_ArquivoNome));
         obj235.gxTpr_Projetoanexos_arquivotipo_N = (short)(Convert.ToInt16(n2167ProjetoAnexos_ArquivoTipo));
         obj235.gxTpr_Projetoanexos_link_N = (short)(Convert.ToInt16(n2168ProjetoAnexos_Link));
         obj235.gxTpr_Projetoanexos_data_N = (short)(Convert.ToInt16(n2169ProjetoAnexos_Data));
         obj235.gxTpr_Tipodocumento_codigo_N = (short)(Convert.ToInt16(n645TipoDocumento_Codigo));
         obj235.gxTpr_Modified = nIsMod_235;
         return  ;
      }

      public void KeyVarsToRow235( SdtProjeto_Anexos obj235 )
      {
         obj235.gxTpr_Projetoanexos_codigo = A2163ProjetoAnexos_Codigo;
         return  ;
      }

      public void RowToVars235( SdtProjeto_Anexos obj235 ,
                                int forceLoad )
      {
         Gx_mode = obj235.gxTpr_Mode;
         A2164ProjetoAnexos_Descricao = obj235.gxTpr_Projetoanexos_descricao;
         A2165ProjetoAnexos_Arquivo = obj235.gxTpr_Projetoanexos_arquivo;
         n2165ProjetoAnexos_Arquivo = false;
         A2168ProjetoAnexos_Link = obj235.gxTpr_Projetoanexos_link;
         n2168ProjetoAnexos_Link = false;
         A2169ProjetoAnexos_Data = obj235.gxTpr_Projetoanexos_data;
         n2169ProjetoAnexos_Data = false;
         A645TipoDocumento_Codigo = obj235.gxTpr_Tipodocumento_codigo;
         n645TipoDocumento_Codigo = false;
         A646TipoDocumento_Nome = obj235.gxTpr_Tipodocumento_nome;
         A2167ProjetoAnexos_ArquivoTipo = (String.IsNullOrEmpty(StringUtil.RTrim( obj235.gxTpr_Projetoanexos_arquivotipo)) ? FileUtil.GetFileType( A2165ProjetoAnexos_Arquivo) : obj235.gxTpr_Projetoanexos_arquivotipo);
         n2167ProjetoAnexos_ArquivoTipo = false;
         A2166ProjetoAnexos_ArquivoNome = (String.IsNullOrEmpty(StringUtil.RTrim( obj235.gxTpr_Projetoanexos_arquivonome)) ? FileUtil.GetFileName( A2165ProjetoAnexos_Arquivo) : obj235.gxTpr_Projetoanexos_arquivonome);
         n2166ProjetoAnexos_ArquivoNome = false;
         A2163ProjetoAnexos_Codigo = obj235.gxTpr_Projetoanexos_codigo;
         Z2163ProjetoAnexos_Codigo = obj235.gxTpr_Projetoanexos_codigo_Z;
         Z2166ProjetoAnexos_ArquivoNome = obj235.gxTpr_Projetoanexos_arquivonome_Z;
         Z2167ProjetoAnexos_ArquivoTipo = obj235.gxTpr_Projetoanexos_arquivotipo_Z;
         Z2169ProjetoAnexos_Data = obj235.gxTpr_Projetoanexos_data_Z;
         Z645TipoDocumento_Codigo = obj235.gxTpr_Tipodocumento_codigo_Z;
         Z646TipoDocumento_Nome = obj235.gxTpr_Tipodocumento_nome_Z;
         n2165ProjetoAnexos_Arquivo = (bool)(Convert.ToBoolean(obj235.gxTpr_Projetoanexos_arquivo_N));
         n2166ProjetoAnexos_ArquivoNome = (bool)(Convert.ToBoolean(obj235.gxTpr_Projetoanexos_arquivonome_N));
         n2167ProjetoAnexos_ArquivoTipo = (bool)(Convert.ToBoolean(obj235.gxTpr_Projetoanexos_arquivotipo_N));
         n2168ProjetoAnexos_Link = (bool)(Convert.ToBoolean(obj235.gxTpr_Projetoanexos_link_N));
         n2169ProjetoAnexos_Data = (bool)(Convert.ToBoolean(obj235.gxTpr_Projetoanexos_data_N));
         n645TipoDocumento_Codigo = (bool)(Convert.ToBoolean(obj235.gxTpr_Tipodocumento_codigo_N));
         nIsMod_235 = obj235.gxTpr_Modified;
         return  ;
      }

      public void LoadKey( Object[] obj )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         A648Projeto_Codigo = (int)getParm(obj,0);
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         InitializeNonKey2986( ) ;
         ScanKeyStart2986( ) ;
         if ( RcdFound86 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
            Z648Projeto_Codigo = A648Projeto_Codigo;
         }
         ZM2986( -11) ;
         OnLoadActions2986( ) ;
         AddRow2986( ) ;
         bcProjeto.gxTpr_Anexos.ClearCollection();
         if ( RcdFound86 == 1 )
         {
            ScanKeyStart29235( ) ;
            nGXsfl_235_idx = 1;
            while ( RcdFound235 != 0 )
            {
               Z648Projeto_Codigo = A648Projeto_Codigo;
               Z2163ProjetoAnexos_Codigo = A2163ProjetoAnexos_Codigo;
               ZM29235( -18) ;
               OnLoadActions29235( ) ;
               nRcdExists_235 = 1;
               nIsMod_235 = 0;
               AddRow29235( ) ;
               nGXsfl_235_idx = (short)(nGXsfl_235_idx+1);
               ScanKeyNext29235( ) ;
            }
            ScanKeyEnd29235( ) ;
         }
         ScanKeyEnd2986( ) ;
         if ( RcdFound86 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Load( )
      {
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         RowToVars86( bcProjeto, 0) ;
         ScanKeyStart2986( ) ;
         if ( RcdFound86 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
            Z648Projeto_Codigo = A648Projeto_Codigo;
         }
         ZM2986( -11) ;
         OnLoadActions2986( ) ;
         AddRow2986( ) ;
         bcProjeto.gxTpr_Anexos.ClearCollection();
         if ( RcdFound86 == 1 )
         {
            ScanKeyStart29235( ) ;
            nGXsfl_235_idx = 1;
            while ( RcdFound235 != 0 )
            {
               Z648Projeto_Codigo = A648Projeto_Codigo;
               Z2163ProjetoAnexos_Codigo = A2163ProjetoAnexos_Codigo;
               ZM29235( -18) ;
               OnLoadActions29235( ) ;
               nRcdExists_235 = 1;
               nIsMod_235 = 0;
               AddRow29235( ) ;
               nGXsfl_235_idx = (short)(nGXsfl_235_idx+1);
               ScanKeyNext29235( ) ;
            }
            ScanKeyEnd29235( ) ;
         }
         ScanKeyEnd2986( ) ;
         if ( RcdFound86 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Save( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         IsConfirmed = 1;
         RowToVars86( bcProjeto, 0) ;
         nKeyPressed = 1;
         GetKey2986( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            Insert2986( ) ;
         }
         else
         {
            if ( RcdFound86 == 1 )
            {
               if ( A648Projeto_Codigo != Z648Projeto_Codigo )
               {
                  A648Projeto_Codigo = Z648Projeto_Codigo;
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
               }
               else
               {
                  Gx_mode = "UPD";
                  /* Update record */
                  Update2986( ) ;
               }
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else
               {
                  if ( A648Projeto_Codigo != Z648Projeto_Codigo )
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert2986( ) ;
                     }
                  }
                  else
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert2986( ) ;
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         VarsToRow86( bcProjeto) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public void Check( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         RowToVars86( bcProjeto, 0) ;
         nKeyPressed = 3;
         IsConfirmed = 0;
         GetKey2986( ) ;
         if ( RcdFound86 == 1 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( A648Projeto_Codigo != Z648Projeto_Codigo )
            {
               A648Projeto_Codigo = Z648Projeto_Codigo;
               GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               delete_Check( ) ;
            }
            else
            {
               Gx_mode = "UPD";
               update_Check( ) ;
            }
         }
         else
         {
            if ( A648Projeto_Codigo != Z648Projeto_Codigo )
            {
               Gx_mode = "INS";
               insert_Check( ) ;
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                  AnyError = 1;
               }
               else
               {
                  Gx_mode = "INS";
                  insert_Check( ) ;
               }
            }
         }
         pr_default.close(4);
         pr_default.close(1);
         pr_default.close(16);
         pr_default.close(17);
         pr_default.close(18);
         pr_default.close(20);
         pr_default.close(19);
         pr_default.close(32);
         context.RollbackDataStores( "Projeto_BC");
         VarsToRow86( bcProjeto) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public int Errors( )
      {
         if ( AnyError == 0 )
         {
            return (int)(0) ;
         }
         return (int)(1) ;
      }

      public msglist GetMessages( )
      {
         return LclMsgLst ;
      }

      public String GetMode( )
      {
         Gx_mode = bcProjeto.gxTpr_Mode;
         return Gx_mode ;
      }

      public void SetMode( String lMode )
      {
         Gx_mode = lMode;
         bcProjeto.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void SetSDT( GxSilentTrnSdt sdt ,
                          short sdtToBc )
      {
         if ( sdt != bcProjeto )
         {
            bcProjeto = (SdtProjeto)(sdt);
            if ( StringUtil.StrCmp(bcProjeto.gxTpr_Mode, "") == 0 )
            {
               bcProjeto.gxTpr_Mode = "INS";
            }
            if ( sdtToBc == 1 )
            {
               VarsToRow86( bcProjeto) ;
            }
            else
            {
               RowToVars86( bcProjeto, 1) ;
            }
         }
         else
         {
            if ( StringUtil.StrCmp(bcProjeto.gxTpr_Mode, "") == 0 )
            {
               bcProjeto.gxTpr_Mode = "INS";
            }
         }
         return  ;
      }

      public void ReloadFromSDT( )
      {
         RowToVars86( bcProjeto, 1) ;
         return  ;
      }

      public SdtProjeto Projeto_BC
      {
         get {
            return bcProjeto ;
         }

      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         createObjects();
         initialize();
      }

      protected override void createObjects( )
      {
      }

      protected void Process( )
      {
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(32);
         pr_default.close(4);
         pr_default.close(16);
         pr_default.close(17);
         pr_default.close(18);
         pr_default.close(20);
         pr_default.close(19);
      }

      public override void initialize( )
      {
         scmdbuf = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Gx_mode = "";
         sMode86 = "";
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         AV23Pgmname = "";
         AV13TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         Z649Projeto_Nome = "";
         A649Projeto_Nome = "";
         Z650Projeto_Sigla = "";
         A650Projeto_Sigla = "";
         Z652Projeto_TecnicaContagem = "";
         A652Projeto_TecnicaContagem = "";
         Z1541Projeto_Previsao = DateTime.MinValue;
         A1541Projeto_Previsao = DateTime.MinValue;
         Z658Projeto_Status = "";
         A658Projeto_Status = "";
         Z2144Projeto_Identificador = "";
         A2144Projeto_Identificador = "";
         Z2146Projeto_DTInicio = DateTime.MinValue;
         A2146Projeto_DTInicio = DateTime.MinValue;
         Z2147Projeto_DTFim = DateTime.MinValue;
         A2147Projeto_DTFim = DateTime.MinValue;
         Z2166ProjetoAnexos_ArquivoNome = "";
         A2166ProjetoAnexos_ArquivoNome = "";
         Z2167ProjetoAnexos_ArquivoTipo = "";
         A2167ProjetoAnexos_ArquivoTipo = "";
         Z2169ProjetoAnexos_Data = (DateTime)(DateTime.MinValue);
         A2169ProjetoAnexos_Data = (DateTime)(DateTime.MinValue);
         Z646TipoDocumento_Nome = "";
         A646TipoDocumento_Nome = "";
         Z2152Projeto_AreaTrabalhoDescricao = "";
         A2152Projeto_AreaTrabalhoDescricao = "";
         Z2154Projeto_GpoObjCtrlNome = "";
         A2154Projeto_GpoObjCtrlNome = "";
         Z2156Projeto_SistemaNome = "";
         A2156Projeto_SistemaNome = "";
         Z2157Projeto_SistemaSigla = "";
         A2157Projeto_SistemaSigla = "";
         Z2159Projeto_ModuloNome = "";
         A2159Projeto_ModuloNome = "";
         Z2160Projeto_ModuloSigla = "";
         A2160Projeto_ModuloSigla = "";
         Z1540Projeto_Introducao = "";
         A1540Projeto_Introducao = "";
         Z653Projeto_Escopo = "";
         A653Projeto_Escopo = "";
         Z2145Projeto_Objetivo = "";
         A2145Projeto_Objetivo = "";
         BC002915_A648Projeto_Codigo = new int[1] ;
         BC002915_A649Projeto_Nome = new String[] {""} ;
         BC002915_A650Projeto_Sigla = new String[] {""} ;
         BC002915_A652Projeto_TecnicaContagem = new String[] {""} ;
         BC002915_A1540Projeto_Introducao = new String[] {""} ;
         BC002915_n1540Projeto_Introducao = new bool[] {false} ;
         BC002915_A653Projeto_Escopo = new String[] {""} ;
         BC002915_A1541Projeto_Previsao = new DateTime[] {DateTime.MinValue} ;
         BC002915_n1541Projeto_Previsao = new bool[] {false} ;
         BC002915_A1542Projeto_GerenteCod = new int[1] ;
         BC002915_n1542Projeto_GerenteCod = new bool[] {false} ;
         BC002915_A1543Projeto_ServicoCod = new int[1] ;
         BC002915_n1543Projeto_ServicoCod = new bool[] {false} ;
         BC002915_A658Projeto_Status = new String[] {""} ;
         BC002915_A985Projeto_FatorEscala = new decimal[1] ;
         BC002915_n985Projeto_FatorEscala = new bool[] {false} ;
         BC002915_A989Projeto_FatorMultiplicador = new decimal[1] ;
         BC002915_n989Projeto_FatorMultiplicador = new bool[] {false} ;
         BC002915_A990Projeto_ConstACocomo = new decimal[1] ;
         BC002915_n990Projeto_ConstACocomo = new bool[] {false} ;
         BC002915_A1232Projeto_Incremental = new bool[] {false} ;
         BC002915_n1232Projeto_Incremental = new bool[] {false} ;
         BC002915_A2144Projeto_Identificador = new String[] {""} ;
         BC002915_n2144Projeto_Identificador = new bool[] {false} ;
         BC002915_A2145Projeto_Objetivo = new String[] {""} ;
         BC002915_n2145Projeto_Objetivo = new bool[] {false} ;
         BC002915_A2146Projeto_DTInicio = new DateTime[] {DateTime.MinValue} ;
         BC002915_n2146Projeto_DTInicio = new bool[] {false} ;
         BC002915_A2147Projeto_DTFim = new DateTime[] {DateTime.MinValue} ;
         BC002915_n2147Projeto_DTFim = new bool[] {false} ;
         BC002915_A2148Projeto_PrazoPrevisto = new short[1] ;
         BC002915_n2148Projeto_PrazoPrevisto = new bool[] {false} ;
         BC002915_A2149Projeto_EsforcoPrevisto = new short[1] ;
         BC002915_n2149Projeto_EsforcoPrevisto = new bool[] {false} ;
         BC002915_A2150Projeto_CustoPrevisto = new decimal[1] ;
         BC002915_n2150Projeto_CustoPrevisto = new bool[] {false} ;
         BC002915_A2152Projeto_AreaTrabalhoDescricao = new String[] {""} ;
         BC002915_n2152Projeto_AreaTrabalhoDescricao = new bool[] {false} ;
         BC002915_A2154Projeto_GpoObjCtrlNome = new String[] {""} ;
         BC002915_n2154Projeto_GpoObjCtrlNome = new bool[] {false} ;
         BC002915_A2156Projeto_SistemaNome = new String[] {""} ;
         BC002915_n2156Projeto_SistemaNome = new bool[] {false} ;
         BC002915_A2157Projeto_SistemaSigla = new String[] {""} ;
         BC002915_n2157Projeto_SistemaSigla = new bool[] {false} ;
         BC002915_A2159Projeto_ModuloNome = new String[] {""} ;
         BC002915_n2159Projeto_ModuloNome = new bool[] {false} ;
         BC002915_A2160Projeto_ModuloSigla = new String[] {""} ;
         BC002915_n2160Projeto_ModuloSigla = new bool[] {false} ;
         BC002915_A2170Projeto_AnexoSequecial = new short[1] ;
         BC002915_n2170Projeto_AnexoSequecial = new bool[] {false} ;
         BC002915_A983Projeto_TipoProjetoCod = new int[1] ;
         BC002915_n983Projeto_TipoProjetoCod = new bool[] {false} ;
         BC002915_A2151Projeto_AreaTrabalhoCodigo = new int[1] ;
         BC002915_n2151Projeto_AreaTrabalhoCodigo = new bool[] {false} ;
         BC002915_A2153Projeto_GpoObjCtrlCodigo = new int[1] ;
         BC002915_n2153Projeto_GpoObjCtrlCodigo = new bool[] {false} ;
         BC002915_A2155Projeto_SistemaCodigo = new int[1] ;
         BC002915_n2155Projeto_SistemaCodigo = new bool[] {false} ;
         BC002915_A2158Projeto_ModuloCodigo = new int[1] ;
         BC002915_n2158Projeto_ModuloCodigo = new bool[] {false} ;
         BC002915_A655Projeto_Custo = new decimal[1] ;
         BC002915_A656Projeto_Prazo = new short[1] ;
         BC002915_A657Projeto_Esforco = new short[1] ;
         BC002915_n657Projeto_Esforco = new bool[] {false} ;
         BC00297_A983Projeto_TipoProjetoCod = new int[1] ;
         BC00297_n983Projeto_TipoProjetoCod = new bool[] {false} ;
         BC00298_A2152Projeto_AreaTrabalhoDescricao = new String[] {""} ;
         BC00298_n2152Projeto_AreaTrabalhoDescricao = new bool[] {false} ;
         BC00299_A2154Projeto_GpoObjCtrlNome = new String[] {""} ;
         BC00299_n2154Projeto_GpoObjCtrlNome = new bool[] {false} ;
         BC002910_A2156Projeto_SistemaNome = new String[] {""} ;
         BC002910_n2156Projeto_SistemaNome = new bool[] {false} ;
         BC002910_A2157Projeto_SistemaSigla = new String[] {""} ;
         BC002910_n2157Projeto_SistemaSigla = new bool[] {false} ;
         BC002913_A655Projeto_Custo = new decimal[1] ;
         BC002913_A656Projeto_Prazo = new short[1] ;
         BC002913_A657Projeto_Esforco = new short[1] ;
         BC002913_n657Projeto_Esforco = new bool[] {false} ;
         BC002911_A2159Projeto_ModuloNome = new String[] {""} ;
         BC002911_n2159Projeto_ModuloNome = new bool[] {false} ;
         BC002911_A2160Projeto_ModuloSigla = new String[] {""} ;
         BC002911_n2160Projeto_ModuloSigla = new bool[] {false} ;
         BC002916_A648Projeto_Codigo = new int[1] ;
         BC00296_A648Projeto_Codigo = new int[1] ;
         BC00296_A649Projeto_Nome = new String[] {""} ;
         BC00296_A650Projeto_Sigla = new String[] {""} ;
         BC00296_A652Projeto_TecnicaContagem = new String[] {""} ;
         BC00296_A1540Projeto_Introducao = new String[] {""} ;
         BC00296_n1540Projeto_Introducao = new bool[] {false} ;
         BC00296_A653Projeto_Escopo = new String[] {""} ;
         BC00296_A1541Projeto_Previsao = new DateTime[] {DateTime.MinValue} ;
         BC00296_n1541Projeto_Previsao = new bool[] {false} ;
         BC00296_A1542Projeto_GerenteCod = new int[1] ;
         BC00296_n1542Projeto_GerenteCod = new bool[] {false} ;
         BC00296_A1543Projeto_ServicoCod = new int[1] ;
         BC00296_n1543Projeto_ServicoCod = new bool[] {false} ;
         BC00296_A658Projeto_Status = new String[] {""} ;
         BC00296_A985Projeto_FatorEscala = new decimal[1] ;
         BC00296_n985Projeto_FatorEscala = new bool[] {false} ;
         BC00296_A989Projeto_FatorMultiplicador = new decimal[1] ;
         BC00296_n989Projeto_FatorMultiplicador = new bool[] {false} ;
         BC00296_A990Projeto_ConstACocomo = new decimal[1] ;
         BC00296_n990Projeto_ConstACocomo = new bool[] {false} ;
         BC00296_A1232Projeto_Incremental = new bool[] {false} ;
         BC00296_n1232Projeto_Incremental = new bool[] {false} ;
         BC00296_A2144Projeto_Identificador = new String[] {""} ;
         BC00296_n2144Projeto_Identificador = new bool[] {false} ;
         BC00296_A2145Projeto_Objetivo = new String[] {""} ;
         BC00296_n2145Projeto_Objetivo = new bool[] {false} ;
         BC00296_A2146Projeto_DTInicio = new DateTime[] {DateTime.MinValue} ;
         BC00296_n2146Projeto_DTInicio = new bool[] {false} ;
         BC00296_A2147Projeto_DTFim = new DateTime[] {DateTime.MinValue} ;
         BC00296_n2147Projeto_DTFim = new bool[] {false} ;
         BC00296_A2148Projeto_PrazoPrevisto = new short[1] ;
         BC00296_n2148Projeto_PrazoPrevisto = new bool[] {false} ;
         BC00296_A2149Projeto_EsforcoPrevisto = new short[1] ;
         BC00296_n2149Projeto_EsforcoPrevisto = new bool[] {false} ;
         BC00296_A2150Projeto_CustoPrevisto = new decimal[1] ;
         BC00296_n2150Projeto_CustoPrevisto = new bool[] {false} ;
         BC00296_A2170Projeto_AnexoSequecial = new short[1] ;
         BC00296_n2170Projeto_AnexoSequecial = new bool[] {false} ;
         BC00296_A983Projeto_TipoProjetoCod = new int[1] ;
         BC00296_n983Projeto_TipoProjetoCod = new bool[] {false} ;
         BC00296_A2151Projeto_AreaTrabalhoCodigo = new int[1] ;
         BC00296_n2151Projeto_AreaTrabalhoCodigo = new bool[] {false} ;
         BC00296_A2153Projeto_GpoObjCtrlCodigo = new int[1] ;
         BC00296_n2153Projeto_GpoObjCtrlCodigo = new bool[] {false} ;
         BC00296_A2155Projeto_SistemaCodigo = new int[1] ;
         BC00296_n2155Projeto_SistemaCodigo = new bool[] {false} ;
         BC00296_A2158Projeto_ModuloCodigo = new int[1] ;
         BC00296_n2158Projeto_ModuloCodigo = new bool[] {false} ;
         BC00295_A648Projeto_Codigo = new int[1] ;
         BC00295_A649Projeto_Nome = new String[] {""} ;
         BC00295_A650Projeto_Sigla = new String[] {""} ;
         BC00295_A652Projeto_TecnicaContagem = new String[] {""} ;
         BC00295_A1540Projeto_Introducao = new String[] {""} ;
         BC00295_n1540Projeto_Introducao = new bool[] {false} ;
         BC00295_A653Projeto_Escopo = new String[] {""} ;
         BC00295_A1541Projeto_Previsao = new DateTime[] {DateTime.MinValue} ;
         BC00295_n1541Projeto_Previsao = new bool[] {false} ;
         BC00295_A1542Projeto_GerenteCod = new int[1] ;
         BC00295_n1542Projeto_GerenteCod = new bool[] {false} ;
         BC00295_A1543Projeto_ServicoCod = new int[1] ;
         BC00295_n1543Projeto_ServicoCod = new bool[] {false} ;
         BC00295_A658Projeto_Status = new String[] {""} ;
         BC00295_A985Projeto_FatorEscala = new decimal[1] ;
         BC00295_n985Projeto_FatorEscala = new bool[] {false} ;
         BC00295_A989Projeto_FatorMultiplicador = new decimal[1] ;
         BC00295_n989Projeto_FatorMultiplicador = new bool[] {false} ;
         BC00295_A990Projeto_ConstACocomo = new decimal[1] ;
         BC00295_n990Projeto_ConstACocomo = new bool[] {false} ;
         BC00295_A1232Projeto_Incremental = new bool[] {false} ;
         BC00295_n1232Projeto_Incremental = new bool[] {false} ;
         BC00295_A2144Projeto_Identificador = new String[] {""} ;
         BC00295_n2144Projeto_Identificador = new bool[] {false} ;
         BC00295_A2145Projeto_Objetivo = new String[] {""} ;
         BC00295_n2145Projeto_Objetivo = new bool[] {false} ;
         BC00295_A2146Projeto_DTInicio = new DateTime[] {DateTime.MinValue} ;
         BC00295_n2146Projeto_DTInicio = new bool[] {false} ;
         BC00295_A2147Projeto_DTFim = new DateTime[] {DateTime.MinValue} ;
         BC00295_n2147Projeto_DTFim = new bool[] {false} ;
         BC00295_A2148Projeto_PrazoPrevisto = new short[1] ;
         BC00295_n2148Projeto_PrazoPrevisto = new bool[] {false} ;
         BC00295_A2149Projeto_EsforcoPrevisto = new short[1] ;
         BC00295_n2149Projeto_EsforcoPrevisto = new bool[] {false} ;
         BC00295_A2150Projeto_CustoPrevisto = new decimal[1] ;
         BC00295_n2150Projeto_CustoPrevisto = new bool[] {false} ;
         BC00295_A2170Projeto_AnexoSequecial = new short[1] ;
         BC00295_n2170Projeto_AnexoSequecial = new bool[] {false} ;
         BC00295_A983Projeto_TipoProjetoCod = new int[1] ;
         BC00295_n983Projeto_TipoProjetoCod = new bool[] {false} ;
         BC00295_A2151Projeto_AreaTrabalhoCodigo = new int[1] ;
         BC00295_n2151Projeto_AreaTrabalhoCodigo = new bool[] {false} ;
         BC00295_A2153Projeto_GpoObjCtrlCodigo = new int[1] ;
         BC00295_n2153Projeto_GpoObjCtrlCodigo = new bool[] {false} ;
         BC00295_A2155Projeto_SistemaCodigo = new int[1] ;
         BC00295_n2155Projeto_SistemaCodigo = new bool[] {false} ;
         BC00295_A2158Projeto_ModuloCodigo = new int[1] ;
         BC00295_n2158Projeto_ModuloCodigo = new bool[] {false} ;
         BC002917_A648Projeto_Codigo = new int[1] ;
         BC002920_A2152Projeto_AreaTrabalhoDescricao = new String[] {""} ;
         BC002920_n2152Projeto_AreaTrabalhoDescricao = new bool[] {false} ;
         BC002921_A2154Projeto_GpoObjCtrlNome = new String[] {""} ;
         BC002921_n2154Projeto_GpoObjCtrlNome = new bool[] {false} ;
         BC002922_A2156Projeto_SistemaNome = new String[] {""} ;
         BC002922_n2156Projeto_SistemaNome = new bool[] {false} ;
         BC002922_A2157Projeto_SistemaSigla = new String[] {""} ;
         BC002922_n2157Projeto_SistemaSigla = new bool[] {false} ;
         BC002924_A655Projeto_Custo = new decimal[1] ;
         BC002924_A656Projeto_Prazo = new short[1] ;
         BC002924_A657Projeto_Esforco = new short[1] ;
         BC002924_n657Projeto_Esforco = new bool[] {false} ;
         BC002925_A2159Projeto_ModuloNome = new String[] {""} ;
         BC002925_n2159Projeto_ModuloNome = new bool[] {false} ;
         BC002925_A2160Projeto_ModuloSigla = new String[] {""} ;
         BC002925_n2160Projeto_ModuloSigla = new bool[] {false} ;
         BC002926_A961VariavelCocomo_AreaTrabalhoCod = new int[1] ;
         BC002926_A962VariavelCocomo_Sigla = new String[] {""} ;
         BC002926_A964VariavelCocomo_Tipo = new String[] {""} ;
         BC002926_A992VariavelCocomo_Sequencial = new short[1] ;
         BC002927_A192Contagem_Codigo = new int[1] ;
         BC002928_A736ProjetoMelhoria_Codigo = new int[1] ;
         BC002929_A1685Proposta_Codigo = new int[1] ;
         BC002931_A648Projeto_Codigo = new int[1] ;
         BC002931_A649Projeto_Nome = new String[] {""} ;
         BC002931_A650Projeto_Sigla = new String[] {""} ;
         BC002931_A652Projeto_TecnicaContagem = new String[] {""} ;
         BC002931_A1540Projeto_Introducao = new String[] {""} ;
         BC002931_n1540Projeto_Introducao = new bool[] {false} ;
         BC002931_A653Projeto_Escopo = new String[] {""} ;
         BC002931_A1541Projeto_Previsao = new DateTime[] {DateTime.MinValue} ;
         BC002931_n1541Projeto_Previsao = new bool[] {false} ;
         BC002931_A1542Projeto_GerenteCod = new int[1] ;
         BC002931_n1542Projeto_GerenteCod = new bool[] {false} ;
         BC002931_A1543Projeto_ServicoCod = new int[1] ;
         BC002931_n1543Projeto_ServicoCod = new bool[] {false} ;
         BC002931_A658Projeto_Status = new String[] {""} ;
         BC002931_A985Projeto_FatorEscala = new decimal[1] ;
         BC002931_n985Projeto_FatorEscala = new bool[] {false} ;
         BC002931_A989Projeto_FatorMultiplicador = new decimal[1] ;
         BC002931_n989Projeto_FatorMultiplicador = new bool[] {false} ;
         BC002931_A990Projeto_ConstACocomo = new decimal[1] ;
         BC002931_n990Projeto_ConstACocomo = new bool[] {false} ;
         BC002931_A1232Projeto_Incremental = new bool[] {false} ;
         BC002931_n1232Projeto_Incremental = new bool[] {false} ;
         BC002931_A2144Projeto_Identificador = new String[] {""} ;
         BC002931_n2144Projeto_Identificador = new bool[] {false} ;
         BC002931_A2145Projeto_Objetivo = new String[] {""} ;
         BC002931_n2145Projeto_Objetivo = new bool[] {false} ;
         BC002931_A2146Projeto_DTInicio = new DateTime[] {DateTime.MinValue} ;
         BC002931_n2146Projeto_DTInicio = new bool[] {false} ;
         BC002931_A2147Projeto_DTFim = new DateTime[] {DateTime.MinValue} ;
         BC002931_n2147Projeto_DTFim = new bool[] {false} ;
         BC002931_A2148Projeto_PrazoPrevisto = new short[1] ;
         BC002931_n2148Projeto_PrazoPrevisto = new bool[] {false} ;
         BC002931_A2149Projeto_EsforcoPrevisto = new short[1] ;
         BC002931_n2149Projeto_EsforcoPrevisto = new bool[] {false} ;
         BC002931_A2150Projeto_CustoPrevisto = new decimal[1] ;
         BC002931_n2150Projeto_CustoPrevisto = new bool[] {false} ;
         BC002931_A2152Projeto_AreaTrabalhoDescricao = new String[] {""} ;
         BC002931_n2152Projeto_AreaTrabalhoDescricao = new bool[] {false} ;
         BC002931_A2154Projeto_GpoObjCtrlNome = new String[] {""} ;
         BC002931_n2154Projeto_GpoObjCtrlNome = new bool[] {false} ;
         BC002931_A2156Projeto_SistemaNome = new String[] {""} ;
         BC002931_n2156Projeto_SistemaNome = new bool[] {false} ;
         BC002931_A2157Projeto_SistemaSigla = new String[] {""} ;
         BC002931_n2157Projeto_SistemaSigla = new bool[] {false} ;
         BC002931_A2159Projeto_ModuloNome = new String[] {""} ;
         BC002931_n2159Projeto_ModuloNome = new bool[] {false} ;
         BC002931_A2160Projeto_ModuloSigla = new String[] {""} ;
         BC002931_n2160Projeto_ModuloSigla = new bool[] {false} ;
         BC002931_A2170Projeto_AnexoSequecial = new short[1] ;
         BC002931_n2170Projeto_AnexoSequecial = new bool[] {false} ;
         BC002931_A983Projeto_TipoProjetoCod = new int[1] ;
         BC002931_n983Projeto_TipoProjetoCod = new bool[] {false} ;
         BC002931_A2151Projeto_AreaTrabalhoCodigo = new int[1] ;
         BC002931_n2151Projeto_AreaTrabalhoCodigo = new bool[] {false} ;
         BC002931_A2153Projeto_GpoObjCtrlCodigo = new int[1] ;
         BC002931_n2153Projeto_GpoObjCtrlCodigo = new bool[] {false} ;
         BC002931_A2155Projeto_SistemaCodigo = new int[1] ;
         BC002931_n2155Projeto_SistemaCodigo = new bool[] {false} ;
         BC002931_A2158Projeto_ModuloCodigo = new int[1] ;
         BC002931_n2158Projeto_ModuloCodigo = new bool[] {false} ;
         BC002931_A655Projeto_Custo = new decimal[1] ;
         BC002931_A656Projeto_Prazo = new short[1] ;
         BC002931_A657Projeto_Esforco = new short[1] ;
         BC002931_n657Projeto_Esforco = new bool[] {false} ;
         Z2164ProjetoAnexos_Descricao = "";
         A2164ProjetoAnexos_Descricao = "";
         Z2165ProjetoAnexos_Arquivo = "";
         A2165ProjetoAnexos_Arquivo = "";
         Z2168ProjetoAnexos_Link = "";
         A2168ProjetoAnexos_Link = "";
         BC002932_A648Projeto_Codigo = new int[1] ;
         BC002932_A2163ProjetoAnexos_Codigo = new int[1] ;
         BC002932_A2164ProjetoAnexos_Descricao = new String[] {""} ;
         BC002932_A2168ProjetoAnexos_Link = new String[] {""} ;
         BC002932_n2168ProjetoAnexos_Link = new bool[] {false} ;
         BC002932_A2169ProjetoAnexos_Data = new DateTime[] {DateTime.MinValue} ;
         BC002932_n2169ProjetoAnexos_Data = new bool[] {false} ;
         BC002932_A646TipoDocumento_Nome = new String[] {""} ;
         BC002932_A2167ProjetoAnexos_ArquivoTipo = new String[] {""} ;
         BC002932_n2167ProjetoAnexos_ArquivoTipo = new bool[] {false} ;
         BC002932_A2166ProjetoAnexos_ArquivoNome = new String[] {""} ;
         BC002932_n2166ProjetoAnexos_ArquivoNome = new bool[] {false} ;
         BC002932_A645TipoDocumento_Codigo = new int[1] ;
         BC002932_n645TipoDocumento_Codigo = new bool[] {false} ;
         BC002932_A2165ProjetoAnexos_Arquivo = new String[] {""} ;
         BC002932_n2165ProjetoAnexos_Arquivo = new bool[] {false} ;
         A2165ProjetoAnexos_Arquivo_Filetype = "";
         A2165ProjetoAnexos_Arquivo_Filename = "";
         BC00294_A646TipoDocumento_Nome = new String[] {""} ;
         BC002933_A648Projeto_Codigo = new int[1] ;
         BC002933_A2163ProjetoAnexos_Codigo = new int[1] ;
         BC00293_A648Projeto_Codigo = new int[1] ;
         BC00293_A2163ProjetoAnexos_Codigo = new int[1] ;
         BC00293_A2164ProjetoAnexos_Descricao = new String[] {""} ;
         BC00293_A2168ProjetoAnexos_Link = new String[] {""} ;
         BC00293_n2168ProjetoAnexos_Link = new bool[] {false} ;
         BC00293_A2169ProjetoAnexos_Data = new DateTime[] {DateTime.MinValue} ;
         BC00293_n2169ProjetoAnexos_Data = new bool[] {false} ;
         BC00293_A2167ProjetoAnexos_ArquivoTipo = new String[] {""} ;
         BC00293_n2167ProjetoAnexos_ArquivoTipo = new bool[] {false} ;
         BC00293_A2166ProjetoAnexos_ArquivoNome = new String[] {""} ;
         BC00293_n2166ProjetoAnexos_ArquivoNome = new bool[] {false} ;
         BC00293_A645TipoDocumento_Codigo = new int[1] ;
         BC00293_n645TipoDocumento_Codigo = new bool[] {false} ;
         BC00293_A2165ProjetoAnexos_Arquivo = new String[] {""} ;
         BC00293_n2165ProjetoAnexos_Arquivo = new bool[] {false} ;
         sMode235 = "";
         BC00292_A648Projeto_Codigo = new int[1] ;
         BC00292_A2163ProjetoAnexos_Codigo = new int[1] ;
         BC00292_A2164ProjetoAnexos_Descricao = new String[] {""} ;
         BC00292_A2168ProjetoAnexos_Link = new String[] {""} ;
         BC00292_n2168ProjetoAnexos_Link = new bool[] {false} ;
         BC00292_A2169ProjetoAnexos_Data = new DateTime[] {DateTime.MinValue} ;
         BC00292_n2169ProjetoAnexos_Data = new bool[] {false} ;
         BC00292_A2167ProjetoAnexos_ArquivoTipo = new String[] {""} ;
         BC00292_n2167ProjetoAnexos_ArquivoTipo = new bool[] {false} ;
         BC00292_A2166ProjetoAnexos_ArquivoNome = new String[] {""} ;
         BC00292_n2166ProjetoAnexos_ArquivoNome = new bool[] {false} ;
         BC00292_A645TipoDocumento_Codigo = new int[1] ;
         BC00292_n645TipoDocumento_Codigo = new bool[] {false} ;
         BC00292_A2165ProjetoAnexos_Arquivo = new String[] {""} ;
         BC00292_n2165ProjetoAnexos_Arquivo = new bool[] {false} ;
         BC002938_A646TipoDocumento_Nome = new String[] {""} ;
         BC002939_A648Projeto_Codigo = new int[1] ;
         BC002939_A2163ProjetoAnexos_Codigo = new int[1] ;
         BC002939_A2164ProjetoAnexos_Descricao = new String[] {""} ;
         BC002939_A2168ProjetoAnexos_Link = new String[] {""} ;
         BC002939_n2168ProjetoAnexos_Link = new bool[] {false} ;
         BC002939_A2169ProjetoAnexos_Data = new DateTime[] {DateTime.MinValue} ;
         BC002939_n2169ProjetoAnexos_Data = new bool[] {false} ;
         BC002939_A646TipoDocumento_Nome = new String[] {""} ;
         BC002939_A2167ProjetoAnexos_ArquivoTipo = new String[] {""} ;
         BC002939_n2167ProjetoAnexos_ArquivoTipo = new bool[] {false} ;
         BC002939_A2166ProjetoAnexos_ArquivoNome = new String[] {""} ;
         BC002939_n2166ProjetoAnexos_ArquivoNome = new bool[] {false} ;
         BC002939_A645TipoDocumento_Codigo = new int[1] ;
         BC002939_n645TipoDocumento_Codigo = new bool[] {false} ;
         BC002939_A2165ProjetoAnexos_Arquivo = new String[] {""} ;
         BC002939_n2165ProjetoAnexos_Arquivo = new bool[] {false} ;
         i658Projeto_Status = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.projeto_bc__default(),
            new Object[][] {
                new Object[] {
               BC00292_A648Projeto_Codigo, BC00292_A2163ProjetoAnexos_Codigo, BC00292_A2164ProjetoAnexos_Descricao, BC00292_A2168ProjetoAnexos_Link, BC00292_n2168ProjetoAnexos_Link, BC00292_A2169ProjetoAnexos_Data, BC00292_n2169ProjetoAnexos_Data, BC00292_A2167ProjetoAnexos_ArquivoTipo, BC00292_n2167ProjetoAnexos_ArquivoTipo, BC00292_A2166ProjetoAnexos_ArquivoNome,
               BC00292_n2166ProjetoAnexos_ArquivoNome, BC00292_A645TipoDocumento_Codigo, BC00292_n645TipoDocumento_Codigo, BC00292_A2165ProjetoAnexos_Arquivo, BC00292_n2165ProjetoAnexos_Arquivo
               }
               , new Object[] {
               BC00293_A648Projeto_Codigo, BC00293_A2163ProjetoAnexos_Codigo, BC00293_A2164ProjetoAnexos_Descricao, BC00293_A2168ProjetoAnexos_Link, BC00293_n2168ProjetoAnexos_Link, BC00293_A2169ProjetoAnexos_Data, BC00293_n2169ProjetoAnexos_Data, BC00293_A2167ProjetoAnexos_ArquivoTipo, BC00293_n2167ProjetoAnexos_ArquivoTipo, BC00293_A2166ProjetoAnexos_ArquivoNome,
               BC00293_n2166ProjetoAnexos_ArquivoNome, BC00293_A645TipoDocumento_Codigo, BC00293_n645TipoDocumento_Codigo, BC00293_A2165ProjetoAnexos_Arquivo, BC00293_n2165ProjetoAnexos_Arquivo
               }
               , new Object[] {
               BC00294_A646TipoDocumento_Nome
               }
               , new Object[] {
               BC00295_A648Projeto_Codigo, BC00295_A649Projeto_Nome, BC00295_A650Projeto_Sigla, BC00295_A652Projeto_TecnicaContagem, BC00295_A1540Projeto_Introducao, BC00295_n1540Projeto_Introducao, BC00295_A653Projeto_Escopo, BC00295_A1541Projeto_Previsao, BC00295_n1541Projeto_Previsao, BC00295_A1542Projeto_GerenteCod,
               BC00295_n1542Projeto_GerenteCod, BC00295_A1543Projeto_ServicoCod, BC00295_n1543Projeto_ServicoCod, BC00295_A658Projeto_Status, BC00295_A985Projeto_FatorEscala, BC00295_n985Projeto_FatorEscala, BC00295_A989Projeto_FatorMultiplicador, BC00295_n989Projeto_FatorMultiplicador, BC00295_A990Projeto_ConstACocomo, BC00295_n990Projeto_ConstACocomo,
               BC00295_A1232Projeto_Incremental, BC00295_n1232Projeto_Incremental, BC00295_A2144Projeto_Identificador, BC00295_n2144Projeto_Identificador, BC00295_A2145Projeto_Objetivo, BC00295_n2145Projeto_Objetivo, BC00295_A2146Projeto_DTInicio, BC00295_n2146Projeto_DTInicio, BC00295_A2147Projeto_DTFim, BC00295_n2147Projeto_DTFim,
               BC00295_A2148Projeto_PrazoPrevisto, BC00295_n2148Projeto_PrazoPrevisto, BC00295_A2149Projeto_EsforcoPrevisto, BC00295_n2149Projeto_EsforcoPrevisto, BC00295_A2150Projeto_CustoPrevisto, BC00295_n2150Projeto_CustoPrevisto, BC00295_A2170Projeto_AnexoSequecial, BC00295_n2170Projeto_AnexoSequecial, BC00295_A983Projeto_TipoProjetoCod, BC00295_n983Projeto_TipoProjetoCod,
               BC00295_A2151Projeto_AreaTrabalhoCodigo, BC00295_n2151Projeto_AreaTrabalhoCodigo, BC00295_A2153Projeto_GpoObjCtrlCodigo, BC00295_n2153Projeto_GpoObjCtrlCodigo, BC00295_A2155Projeto_SistemaCodigo, BC00295_n2155Projeto_SistemaCodigo, BC00295_A2158Projeto_ModuloCodigo, BC00295_n2158Projeto_ModuloCodigo
               }
               , new Object[] {
               BC00296_A648Projeto_Codigo, BC00296_A649Projeto_Nome, BC00296_A650Projeto_Sigla, BC00296_A652Projeto_TecnicaContagem, BC00296_A1540Projeto_Introducao, BC00296_n1540Projeto_Introducao, BC00296_A653Projeto_Escopo, BC00296_A1541Projeto_Previsao, BC00296_n1541Projeto_Previsao, BC00296_A1542Projeto_GerenteCod,
               BC00296_n1542Projeto_GerenteCod, BC00296_A1543Projeto_ServicoCod, BC00296_n1543Projeto_ServicoCod, BC00296_A658Projeto_Status, BC00296_A985Projeto_FatorEscala, BC00296_n985Projeto_FatorEscala, BC00296_A989Projeto_FatorMultiplicador, BC00296_n989Projeto_FatorMultiplicador, BC00296_A990Projeto_ConstACocomo, BC00296_n990Projeto_ConstACocomo,
               BC00296_A1232Projeto_Incremental, BC00296_n1232Projeto_Incremental, BC00296_A2144Projeto_Identificador, BC00296_n2144Projeto_Identificador, BC00296_A2145Projeto_Objetivo, BC00296_n2145Projeto_Objetivo, BC00296_A2146Projeto_DTInicio, BC00296_n2146Projeto_DTInicio, BC00296_A2147Projeto_DTFim, BC00296_n2147Projeto_DTFim,
               BC00296_A2148Projeto_PrazoPrevisto, BC00296_n2148Projeto_PrazoPrevisto, BC00296_A2149Projeto_EsforcoPrevisto, BC00296_n2149Projeto_EsforcoPrevisto, BC00296_A2150Projeto_CustoPrevisto, BC00296_n2150Projeto_CustoPrevisto, BC00296_A2170Projeto_AnexoSequecial, BC00296_n2170Projeto_AnexoSequecial, BC00296_A983Projeto_TipoProjetoCod, BC00296_n983Projeto_TipoProjetoCod,
               BC00296_A2151Projeto_AreaTrabalhoCodigo, BC00296_n2151Projeto_AreaTrabalhoCodigo, BC00296_A2153Projeto_GpoObjCtrlCodigo, BC00296_n2153Projeto_GpoObjCtrlCodigo, BC00296_A2155Projeto_SistemaCodigo, BC00296_n2155Projeto_SistemaCodigo, BC00296_A2158Projeto_ModuloCodigo, BC00296_n2158Projeto_ModuloCodigo
               }
               , new Object[] {
               BC00297_A983Projeto_TipoProjetoCod
               }
               , new Object[] {
               BC00298_A2152Projeto_AreaTrabalhoDescricao, BC00298_n2152Projeto_AreaTrabalhoDescricao
               }
               , new Object[] {
               BC00299_A2154Projeto_GpoObjCtrlNome, BC00299_n2154Projeto_GpoObjCtrlNome
               }
               , new Object[] {
               BC002910_A2156Projeto_SistemaNome, BC002910_n2156Projeto_SistemaNome, BC002910_A2157Projeto_SistemaSigla, BC002910_n2157Projeto_SistemaSigla
               }
               , new Object[] {
               BC002911_A2159Projeto_ModuloNome, BC002911_n2159Projeto_ModuloNome, BC002911_A2160Projeto_ModuloSigla, BC002911_n2160Projeto_ModuloSigla
               }
               , new Object[] {
               BC002913_A655Projeto_Custo, BC002913_A656Projeto_Prazo, BC002913_A657Projeto_Esforco, BC002913_n657Projeto_Esforco
               }
               , new Object[] {
               BC002915_A648Projeto_Codigo, BC002915_A649Projeto_Nome, BC002915_A650Projeto_Sigla, BC002915_A652Projeto_TecnicaContagem, BC002915_A1540Projeto_Introducao, BC002915_n1540Projeto_Introducao, BC002915_A653Projeto_Escopo, BC002915_A1541Projeto_Previsao, BC002915_n1541Projeto_Previsao, BC002915_A1542Projeto_GerenteCod,
               BC002915_n1542Projeto_GerenteCod, BC002915_A1543Projeto_ServicoCod, BC002915_n1543Projeto_ServicoCod, BC002915_A658Projeto_Status, BC002915_A985Projeto_FatorEscala, BC002915_n985Projeto_FatorEscala, BC002915_A989Projeto_FatorMultiplicador, BC002915_n989Projeto_FatorMultiplicador, BC002915_A990Projeto_ConstACocomo, BC002915_n990Projeto_ConstACocomo,
               BC002915_A1232Projeto_Incremental, BC002915_n1232Projeto_Incremental, BC002915_A2144Projeto_Identificador, BC002915_n2144Projeto_Identificador, BC002915_A2145Projeto_Objetivo, BC002915_n2145Projeto_Objetivo, BC002915_A2146Projeto_DTInicio, BC002915_n2146Projeto_DTInicio, BC002915_A2147Projeto_DTFim, BC002915_n2147Projeto_DTFim,
               BC002915_A2148Projeto_PrazoPrevisto, BC002915_n2148Projeto_PrazoPrevisto, BC002915_A2149Projeto_EsforcoPrevisto, BC002915_n2149Projeto_EsforcoPrevisto, BC002915_A2150Projeto_CustoPrevisto, BC002915_n2150Projeto_CustoPrevisto, BC002915_A2152Projeto_AreaTrabalhoDescricao, BC002915_n2152Projeto_AreaTrabalhoDescricao, BC002915_A2154Projeto_GpoObjCtrlNome, BC002915_n2154Projeto_GpoObjCtrlNome,
               BC002915_A2156Projeto_SistemaNome, BC002915_n2156Projeto_SistemaNome, BC002915_A2157Projeto_SistemaSigla, BC002915_n2157Projeto_SistemaSigla, BC002915_A2159Projeto_ModuloNome, BC002915_n2159Projeto_ModuloNome, BC002915_A2160Projeto_ModuloSigla, BC002915_n2160Projeto_ModuloSigla, BC002915_A2170Projeto_AnexoSequecial, BC002915_n2170Projeto_AnexoSequecial,
               BC002915_A983Projeto_TipoProjetoCod, BC002915_n983Projeto_TipoProjetoCod, BC002915_A2151Projeto_AreaTrabalhoCodigo, BC002915_n2151Projeto_AreaTrabalhoCodigo, BC002915_A2153Projeto_GpoObjCtrlCodigo, BC002915_n2153Projeto_GpoObjCtrlCodigo, BC002915_A2155Projeto_SistemaCodigo, BC002915_n2155Projeto_SistemaCodigo, BC002915_A2158Projeto_ModuloCodigo, BC002915_n2158Projeto_ModuloCodigo,
               BC002915_A655Projeto_Custo, BC002915_A656Projeto_Prazo, BC002915_A657Projeto_Esforco, BC002915_n657Projeto_Esforco
               }
               , new Object[] {
               BC002916_A648Projeto_Codigo
               }
               , new Object[] {
               BC002917_A648Projeto_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               BC002920_A2152Projeto_AreaTrabalhoDescricao, BC002920_n2152Projeto_AreaTrabalhoDescricao
               }
               , new Object[] {
               BC002921_A2154Projeto_GpoObjCtrlNome, BC002921_n2154Projeto_GpoObjCtrlNome
               }
               , new Object[] {
               BC002922_A2156Projeto_SistemaNome, BC002922_n2156Projeto_SistemaNome, BC002922_A2157Projeto_SistemaSigla, BC002922_n2157Projeto_SistemaSigla
               }
               , new Object[] {
               BC002924_A655Projeto_Custo, BC002924_A656Projeto_Prazo, BC002924_A657Projeto_Esforco, BC002924_n657Projeto_Esforco
               }
               , new Object[] {
               BC002925_A2159Projeto_ModuloNome, BC002925_n2159Projeto_ModuloNome, BC002925_A2160Projeto_ModuloSigla, BC002925_n2160Projeto_ModuloSigla
               }
               , new Object[] {
               BC002926_A961VariavelCocomo_AreaTrabalhoCod, BC002926_A962VariavelCocomo_Sigla, BC002926_A964VariavelCocomo_Tipo, BC002926_A992VariavelCocomo_Sequencial
               }
               , new Object[] {
               BC002927_A192Contagem_Codigo
               }
               , new Object[] {
               BC002928_A736ProjetoMelhoria_Codigo
               }
               , new Object[] {
               BC002929_A1685Proposta_Codigo
               }
               , new Object[] {
               BC002931_A648Projeto_Codigo, BC002931_A649Projeto_Nome, BC002931_A650Projeto_Sigla, BC002931_A652Projeto_TecnicaContagem, BC002931_A1540Projeto_Introducao, BC002931_n1540Projeto_Introducao, BC002931_A653Projeto_Escopo, BC002931_A1541Projeto_Previsao, BC002931_n1541Projeto_Previsao, BC002931_A1542Projeto_GerenteCod,
               BC002931_n1542Projeto_GerenteCod, BC002931_A1543Projeto_ServicoCod, BC002931_n1543Projeto_ServicoCod, BC002931_A658Projeto_Status, BC002931_A985Projeto_FatorEscala, BC002931_n985Projeto_FatorEscala, BC002931_A989Projeto_FatorMultiplicador, BC002931_n989Projeto_FatorMultiplicador, BC002931_A990Projeto_ConstACocomo, BC002931_n990Projeto_ConstACocomo,
               BC002931_A1232Projeto_Incremental, BC002931_n1232Projeto_Incremental, BC002931_A2144Projeto_Identificador, BC002931_n2144Projeto_Identificador, BC002931_A2145Projeto_Objetivo, BC002931_n2145Projeto_Objetivo, BC002931_A2146Projeto_DTInicio, BC002931_n2146Projeto_DTInicio, BC002931_A2147Projeto_DTFim, BC002931_n2147Projeto_DTFim,
               BC002931_A2148Projeto_PrazoPrevisto, BC002931_n2148Projeto_PrazoPrevisto, BC002931_A2149Projeto_EsforcoPrevisto, BC002931_n2149Projeto_EsforcoPrevisto, BC002931_A2150Projeto_CustoPrevisto, BC002931_n2150Projeto_CustoPrevisto, BC002931_A2152Projeto_AreaTrabalhoDescricao, BC002931_n2152Projeto_AreaTrabalhoDescricao, BC002931_A2154Projeto_GpoObjCtrlNome, BC002931_n2154Projeto_GpoObjCtrlNome,
               BC002931_A2156Projeto_SistemaNome, BC002931_n2156Projeto_SistemaNome, BC002931_A2157Projeto_SistemaSigla, BC002931_n2157Projeto_SistemaSigla, BC002931_A2159Projeto_ModuloNome, BC002931_n2159Projeto_ModuloNome, BC002931_A2160Projeto_ModuloSigla, BC002931_n2160Projeto_ModuloSigla, BC002931_A2170Projeto_AnexoSequecial, BC002931_n2170Projeto_AnexoSequecial,
               BC002931_A983Projeto_TipoProjetoCod, BC002931_n983Projeto_TipoProjetoCod, BC002931_A2151Projeto_AreaTrabalhoCodigo, BC002931_n2151Projeto_AreaTrabalhoCodigo, BC002931_A2153Projeto_GpoObjCtrlCodigo, BC002931_n2153Projeto_GpoObjCtrlCodigo, BC002931_A2155Projeto_SistemaCodigo, BC002931_n2155Projeto_SistemaCodigo, BC002931_A2158Projeto_ModuloCodigo, BC002931_n2158Projeto_ModuloCodigo,
               BC002931_A655Projeto_Custo, BC002931_A656Projeto_Prazo, BC002931_A657Projeto_Esforco, BC002931_n657Projeto_Esforco
               }
               , new Object[] {
               BC002932_A648Projeto_Codigo, BC002932_A2163ProjetoAnexos_Codigo, BC002932_A2164ProjetoAnexos_Descricao, BC002932_A2168ProjetoAnexos_Link, BC002932_n2168ProjetoAnexos_Link, BC002932_A2169ProjetoAnexos_Data, BC002932_n2169ProjetoAnexos_Data, BC002932_A646TipoDocumento_Nome, BC002932_A2167ProjetoAnexos_ArquivoTipo, BC002932_n2167ProjetoAnexos_ArquivoTipo,
               BC002932_A2166ProjetoAnexos_ArquivoNome, BC002932_n2166ProjetoAnexos_ArquivoNome, BC002932_A645TipoDocumento_Codigo, BC002932_n645TipoDocumento_Codigo, BC002932_A2165ProjetoAnexos_Arquivo, BC002932_n2165ProjetoAnexos_Arquivo
               }
               , new Object[] {
               BC002933_A648Projeto_Codigo, BC002933_A2163ProjetoAnexos_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               BC002938_A646TipoDocumento_Nome
               }
               , new Object[] {
               BC002939_A648Projeto_Codigo, BC002939_A2163ProjetoAnexos_Codigo, BC002939_A2164ProjetoAnexos_Descricao, BC002939_A2168ProjetoAnexos_Link, BC002939_n2168ProjetoAnexos_Link, BC002939_A2169ProjetoAnexos_Data, BC002939_n2169ProjetoAnexos_Data, BC002939_A646TipoDocumento_Nome, BC002939_A2167ProjetoAnexos_ArquivoTipo, BC002939_n2167ProjetoAnexos_ArquivoTipo,
               BC002939_A2166ProjetoAnexos_ArquivoNome, BC002939_n2166ProjetoAnexos_ArquivoNome, BC002939_A645TipoDocumento_Codigo, BC002939_n645TipoDocumento_Codigo, BC002939_A2165ProjetoAnexos_Arquivo, BC002939_n2165ProjetoAnexos_Arquivo
               }
            }
         );
         Z658Projeto_Status = "A";
         A658Projeto_Status = "A";
         i658Projeto_Status = "A";
         AV23Pgmname = "Projeto_BC";
         INITTRN();
         /* Execute Start event if defined. */
         /* Execute user event: E12292 */
         E12292 ();
         standaloneNotModal( ) ;
      }

      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short nGXsfl_235_idx=1 ;
      private short nIsMod_235 ;
      private short RcdFound235 ;
      private short GX_JID ;
      private short Z2148Projeto_PrazoPrevisto ;
      private short A2148Projeto_PrazoPrevisto ;
      private short Z2149Projeto_EsforcoPrevisto ;
      private short A2149Projeto_EsforcoPrevisto ;
      private short Z2170Projeto_AnexoSequecial ;
      private short A2170Projeto_AnexoSequecial ;
      private short Z656Projeto_Prazo ;
      private short A656Projeto_Prazo ;
      private short Z657Projeto_Esforco ;
      private short A657Projeto_Esforco ;
      private short Gx_BScreen ;
      private short RcdFound86 ;
      private short nRcdExists_235 ;
      private short Gxremove235 ;
      private int trnEnded ;
      private int Z648Projeto_Codigo ;
      private int A648Projeto_Codigo ;
      private int AV24GXV1 ;
      private int AV12Insert_Projeto_TipoProjetoCod ;
      private int AV19Insert_Projeto_AreaTrabalhoCodigo ;
      private int AV20Insert_Projeto_GpoObjCtrlCodigo ;
      private int AV21Insert_Projeto_SistemaCodigo ;
      private int AV22Insert_Projeto_ModuloCodigo ;
      private int AV7Projeto_Codigo ;
      private int Z1542Projeto_GerenteCod ;
      private int A1542Projeto_GerenteCod ;
      private int Z1543Projeto_ServicoCod ;
      private int A1543Projeto_ServicoCod ;
      private int Z983Projeto_TipoProjetoCod ;
      private int A983Projeto_TipoProjetoCod ;
      private int Z2151Projeto_AreaTrabalhoCodigo ;
      private int A2151Projeto_AreaTrabalhoCodigo ;
      private int Z2153Projeto_GpoObjCtrlCodigo ;
      private int A2153Projeto_GpoObjCtrlCodigo ;
      private int Z2155Projeto_SistemaCodigo ;
      private int A2155Projeto_SistemaCodigo ;
      private int Z2158Projeto_ModuloCodigo ;
      private int A2158Projeto_ModuloCodigo ;
      private int Z740Projeto_SistemaCod ;
      private int A740Projeto_SistemaCod ;
      private int Z2163ProjetoAnexos_Codigo ;
      private int A2163ProjetoAnexos_Codigo ;
      private int Z645TipoDocumento_Codigo ;
      private int A645TipoDocumento_Codigo ;
      private int GXt_int1 ;
      private decimal AV16FatorEscala ;
      private decimal AV15FatorMultiplicador ;
      private decimal Z985Projeto_FatorEscala ;
      private decimal A985Projeto_FatorEscala ;
      private decimal Z989Projeto_FatorMultiplicador ;
      private decimal A989Projeto_FatorMultiplicador ;
      private decimal Z990Projeto_ConstACocomo ;
      private decimal A990Projeto_ConstACocomo ;
      private decimal Z2150Projeto_CustoPrevisto ;
      private decimal A2150Projeto_CustoPrevisto ;
      private decimal Z655Projeto_Custo ;
      private decimal A655Projeto_Custo ;
      private String scmdbuf ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String Gx_mode ;
      private String sMode86 ;
      private String AV23Pgmname ;
      private String Z649Projeto_Nome ;
      private String A649Projeto_Nome ;
      private String Z650Projeto_Sigla ;
      private String A650Projeto_Sigla ;
      private String Z652Projeto_TecnicaContagem ;
      private String A652Projeto_TecnicaContagem ;
      private String Z658Projeto_Status ;
      private String A658Projeto_Status ;
      private String Z2166ProjetoAnexos_ArquivoNome ;
      private String A2166ProjetoAnexos_ArquivoNome ;
      private String Z2167ProjetoAnexos_ArquivoTipo ;
      private String A2167ProjetoAnexos_ArquivoTipo ;
      private String Z646TipoDocumento_Nome ;
      private String A646TipoDocumento_Nome ;
      private String Z2154Projeto_GpoObjCtrlNome ;
      private String A2154Projeto_GpoObjCtrlNome ;
      private String Z2157Projeto_SistemaSigla ;
      private String A2157Projeto_SistemaSigla ;
      private String Z2159Projeto_ModuloNome ;
      private String A2159Projeto_ModuloNome ;
      private String Z2160Projeto_ModuloSigla ;
      private String A2160Projeto_ModuloSigla ;
      private String A2165ProjetoAnexos_Arquivo_Filetype ;
      private String A2165ProjetoAnexos_Arquivo_Filename ;
      private String sMode235 ;
      private String i658Projeto_Status ;
      private DateTime Z2169ProjetoAnexos_Data ;
      private DateTime A2169ProjetoAnexos_Data ;
      private DateTime Z1541Projeto_Previsao ;
      private DateTime A1541Projeto_Previsao ;
      private DateTime Z2146Projeto_DTInicio ;
      private DateTime A2146Projeto_DTInicio ;
      private DateTime Z2147Projeto_DTFim ;
      private DateTime A2147Projeto_DTFim ;
      private bool Z1232Projeto_Incremental ;
      private bool A1232Projeto_Incremental ;
      private bool n1540Projeto_Introducao ;
      private bool n1541Projeto_Previsao ;
      private bool n1542Projeto_GerenteCod ;
      private bool n1543Projeto_ServicoCod ;
      private bool n985Projeto_FatorEscala ;
      private bool n989Projeto_FatorMultiplicador ;
      private bool n990Projeto_ConstACocomo ;
      private bool n1232Projeto_Incremental ;
      private bool n2144Projeto_Identificador ;
      private bool n2145Projeto_Objetivo ;
      private bool n2146Projeto_DTInicio ;
      private bool n2147Projeto_DTFim ;
      private bool n2148Projeto_PrazoPrevisto ;
      private bool n2149Projeto_EsforcoPrevisto ;
      private bool n2150Projeto_CustoPrevisto ;
      private bool n2152Projeto_AreaTrabalhoDescricao ;
      private bool n2154Projeto_GpoObjCtrlNome ;
      private bool n2156Projeto_SistemaNome ;
      private bool n2157Projeto_SistemaSigla ;
      private bool n2159Projeto_ModuloNome ;
      private bool n2160Projeto_ModuloSigla ;
      private bool n2170Projeto_AnexoSequecial ;
      private bool n983Projeto_TipoProjetoCod ;
      private bool n2151Projeto_AreaTrabalhoCodigo ;
      private bool n2153Projeto_GpoObjCtrlCodigo ;
      private bool n2155Projeto_SistemaCodigo ;
      private bool n2158Projeto_ModuloCodigo ;
      private bool n657Projeto_Esforco ;
      private bool Gx_longc ;
      private bool n2168ProjetoAnexos_Link ;
      private bool n2169ProjetoAnexos_Data ;
      private bool n2167ProjetoAnexos_ArquivoTipo ;
      private bool n2166ProjetoAnexos_ArquivoNome ;
      private bool n645TipoDocumento_Codigo ;
      private bool n2165ProjetoAnexos_Arquivo ;
      private String Z1540Projeto_Introducao ;
      private String A1540Projeto_Introducao ;
      private String Z653Projeto_Escopo ;
      private String A653Projeto_Escopo ;
      private String Z2145Projeto_Objetivo ;
      private String A2145Projeto_Objetivo ;
      private String Z2164ProjetoAnexos_Descricao ;
      private String A2164ProjetoAnexos_Descricao ;
      private String Z2168ProjetoAnexos_Link ;
      private String A2168ProjetoAnexos_Link ;
      private String Z2144Projeto_Identificador ;
      private String A2144Projeto_Identificador ;
      private String Z2152Projeto_AreaTrabalhoDescricao ;
      private String A2152Projeto_AreaTrabalhoDescricao ;
      private String Z2156Projeto_SistemaNome ;
      private String A2156Projeto_SistemaNome ;
      private String Z2165ProjetoAnexos_Arquivo ;
      private String A2165ProjetoAnexos_Arquivo ;
      private IGxSession AV10WebSession ;
      private SdtProjeto bcProjeto ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] BC002915_A648Projeto_Codigo ;
      private String[] BC002915_A649Projeto_Nome ;
      private String[] BC002915_A650Projeto_Sigla ;
      private String[] BC002915_A652Projeto_TecnicaContagem ;
      private String[] BC002915_A1540Projeto_Introducao ;
      private bool[] BC002915_n1540Projeto_Introducao ;
      private String[] BC002915_A653Projeto_Escopo ;
      private DateTime[] BC002915_A1541Projeto_Previsao ;
      private bool[] BC002915_n1541Projeto_Previsao ;
      private int[] BC002915_A1542Projeto_GerenteCod ;
      private bool[] BC002915_n1542Projeto_GerenteCod ;
      private int[] BC002915_A1543Projeto_ServicoCod ;
      private bool[] BC002915_n1543Projeto_ServicoCod ;
      private String[] BC002915_A658Projeto_Status ;
      private decimal[] BC002915_A985Projeto_FatorEscala ;
      private bool[] BC002915_n985Projeto_FatorEscala ;
      private decimal[] BC002915_A989Projeto_FatorMultiplicador ;
      private bool[] BC002915_n989Projeto_FatorMultiplicador ;
      private decimal[] BC002915_A990Projeto_ConstACocomo ;
      private bool[] BC002915_n990Projeto_ConstACocomo ;
      private bool[] BC002915_A1232Projeto_Incremental ;
      private bool[] BC002915_n1232Projeto_Incremental ;
      private String[] BC002915_A2144Projeto_Identificador ;
      private bool[] BC002915_n2144Projeto_Identificador ;
      private String[] BC002915_A2145Projeto_Objetivo ;
      private bool[] BC002915_n2145Projeto_Objetivo ;
      private DateTime[] BC002915_A2146Projeto_DTInicio ;
      private bool[] BC002915_n2146Projeto_DTInicio ;
      private DateTime[] BC002915_A2147Projeto_DTFim ;
      private bool[] BC002915_n2147Projeto_DTFim ;
      private short[] BC002915_A2148Projeto_PrazoPrevisto ;
      private bool[] BC002915_n2148Projeto_PrazoPrevisto ;
      private short[] BC002915_A2149Projeto_EsforcoPrevisto ;
      private bool[] BC002915_n2149Projeto_EsforcoPrevisto ;
      private decimal[] BC002915_A2150Projeto_CustoPrevisto ;
      private bool[] BC002915_n2150Projeto_CustoPrevisto ;
      private String[] BC002915_A2152Projeto_AreaTrabalhoDescricao ;
      private bool[] BC002915_n2152Projeto_AreaTrabalhoDescricao ;
      private String[] BC002915_A2154Projeto_GpoObjCtrlNome ;
      private bool[] BC002915_n2154Projeto_GpoObjCtrlNome ;
      private String[] BC002915_A2156Projeto_SistemaNome ;
      private bool[] BC002915_n2156Projeto_SistemaNome ;
      private String[] BC002915_A2157Projeto_SistemaSigla ;
      private bool[] BC002915_n2157Projeto_SistemaSigla ;
      private String[] BC002915_A2159Projeto_ModuloNome ;
      private bool[] BC002915_n2159Projeto_ModuloNome ;
      private String[] BC002915_A2160Projeto_ModuloSigla ;
      private bool[] BC002915_n2160Projeto_ModuloSigla ;
      private short[] BC002915_A2170Projeto_AnexoSequecial ;
      private bool[] BC002915_n2170Projeto_AnexoSequecial ;
      private int[] BC002915_A983Projeto_TipoProjetoCod ;
      private bool[] BC002915_n983Projeto_TipoProjetoCod ;
      private int[] BC002915_A2151Projeto_AreaTrabalhoCodigo ;
      private bool[] BC002915_n2151Projeto_AreaTrabalhoCodigo ;
      private int[] BC002915_A2153Projeto_GpoObjCtrlCodigo ;
      private bool[] BC002915_n2153Projeto_GpoObjCtrlCodigo ;
      private int[] BC002915_A2155Projeto_SistemaCodigo ;
      private bool[] BC002915_n2155Projeto_SistemaCodigo ;
      private int[] BC002915_A2158Projeto_ModuloCodigo ;
      private bool[] BC002915_n2158Projeto_ModuloCodigo ;
      private decimal[] BC002915_A655Projeto_Custo ;
      private short[] BC002915_A656Projeto_Prazo ;
      private short[] BC002915_A657Projeto_Esforco ;
      private bool[] BC002915_n657Projeto_Esforco ;
      private int[] BC00297_A983Projeto_TipoProjetoCod ;
      private bool[] BC00297_n983Projeto_TipoProjetoCod ;
      private String[] BC00298_A2152Projeto_AreaTrabalhoDescricao ;
      private bool[] BC00298_n2152Projeto_AreaTrabalhoDescricao ;
      private String[] BC00299_A2154Projeto_GpoObjCtrlNome ;
      private bool[] BC00299_n2154Projeto_GpoObjCtrlNome ;
      private String[] BC002910_A2156Projeto_SistemaNome ;
      private bool[] BC002910_n2156Projeto_SistemaNome ;
      private String[] BC002910_A2157Projeto_SistemaSigla ;
      private bool[] BC002910_n2157Projeto_SistemaSigla ;
      private decimal[] BC002913_A655Projeto_Custo ;
      private short[] BC002913_A656Projeto_Prazo ;
      private short[] BC002913_A657Projeto_Esforco ;
      private bool[] BC002913_n657Projeto_Esforco ;
      private String[] BC002911_A2159Projeto_ModuloNome ;
      private bool[] BC002911_n2159Projeto_ModuloNome ;
      private String[] BC002911_A2160Projeto_ModuloSigla ;
      private bool[] BC002911_n2160Projeto_ModuloSigla ;
      private int[] BC002916_A648Projeto_Codigo ;
      private int[] BC00296_A648Projeto_Codigo ;
      private String[] BC00296_A649Projeto_Nome ;
      private String[] BC00296_A650Projeto_Sigla ;
      private String[] BC00296_A652Projeto_TecnicaContagem ;
      private String[] BC00296_A1540Projeto_Introducao ;
      private bool[] BC00296_n1540Projeto_Introducao ;
      private String[] BC00296_A653Projeto_Escopo ;
      private DateTime[] BC00296_A1541Projeto_Previsao ;
      private bool[] BC00296_n1541Projeto_Previsao ;
      private int[] BC00296_A1542Projeto_GerenteCod ;
      private bool[] BC00296_n1542Projeto_GerenteCod ;
      private int[] BC00296_A1543Projeto_ServicoCod ;
      private bool[] BC00296_n1543Projeto_ServicoCod ;
      private String[] BC00296_A658Projeto_Status ;
      private decimal[] BC00296_A985Projeto_FatorEscala ;
      private bool[] BC00296_n985Projeto_FatorEscala ;
      private decimal[] BC00296_A989Projeto_FatorMultiplicador ;
      private bool[] BC00296_n989Projeto_FatorMultiplicador ;
      private decimal[] BC00296_A990Projeto_ConstACocomo ;
      private bool[] BC00296_n990Projeto_ConstACocomo ;
      private bool[] BC00296_A1232Projeto_Incremental ;
      private bool[] BC00296_n1232Projeto_Incremental ;
      private String[] BC00296_A2144Projeto_Identificador ;
      private bool[] BC00296_n2144Projeto_Identificador ;
      private String[] BC00296_A2145Projeto_Objetivo ;
      private bool[] BC00296_n2145Projeto_Objetivo ;
      private DateTime[] BC00296_A2146Projeto_DTInicio ;
      private bool[] BC00296_n2146Projeto_DTInicio ;
      private DateTime[] BC00296_A2147Projeto_DTFim ;
      private bool[] BC00296_n2147Projeto_DTFim ;
      private short[] BC00296_A2148Projeto_PrazoPrevisto ;
      private bool[] BC00296_n2148Projeto_PrazoPrevisto ;
      private short[] BC00296_A2149Projeto_EsforcoPrevisto ;
      private bool[] BC00296_n2149Projeto_EsforcoPrevisto ;
      private decimal[] BC00296_A2150Projeto_CustoPrevisto ;
      private bool[] BC00296_n2150Projeto_CustoPrevisto ;
      private short[] BC00296_A2170Projeto_AnexoSequecial ;
      private bool[] BC00296_n2170Projeto_AnexoSequecial ;
      private int[] BC00296_A983Projeto_TipoProjetoCod ;
      private bool[] BC00296_n983Projeto_TipoProjetoCod ;
      private int[] BC00296_A2151Projeto_AreaTrabalhoCodigo ;
      private bool[] BC00296_n2151Projeto_AreaTrabalhoCodigo ;
      private int[] BC00296_A2153Projeto_GpoObjCtrlCodigo ;
      private bool[] BC00296_n2153Projeto_GpoObjCtrlCodigo ;
      private int[] BC00296_A2155Projeto_SistemaCodigo ;
      private bool[] BC00296_n2155Projeto_SistemaCodigo ;
      private int[] BC00296_A2158Projeto_ModuloCodigo ;
      private bool[] BC00296_n2158Projeto_ModuloCodigo ;
      private int[] BC00295_A648Projeto_Codigo ;
      private String[] BC00295_A649Projeto_Nome ;
      private String[] BC00295_A650Projeto_Sigla ;
      private String[] BC00295_A652Projeto_TecnicaContagem ;
      private String[] BC00295_A1540Projeto_Introducao ;
      private bool[] BC00295_n1540Projeto_Introducao ;
      private String[] BC00295_A653Projeto_Escopo ;
      private DateTime[] BC00295_A1541Projeto_Previsao ;
      private bool[] BC00295_n1541Projeto_Previsao ;
      private int[] BC00295_A1542Projeto_GerenteCod ;
      private bool[] BC00295_n1542Projeto_GerenteCod ;
      private int[] BC00295_A1543Projeto_ServicoCod ;
      private bool[] BC00295_n1543Projeto_ServicoCod ;
      private String[] BC00295_A658Projeto_Status ;
      private decimal[] BC00295_A985Projeto_FatorEscala ;
      private bool[] BC00295_n985Projeto_FatorEscala ;
      private decimal[] BC00295_A989Projeto_FatorMultiplicador ;
      private bool[] BC00295_n989Projeto_FatorMultiplicador ;
      private decimal[] BC00295_A990Projeto_ConstACocomo ;
      private bool[] BC00295_n990Projeto_ConstACocomo ;
      private bool[] BC00295_A1232Projeto_Incremental ;
      private bool[] BC00295_n1232Projeto_Incremental ;
      private String[] BC00295_A2144Projeto_Identificador ;
      private bool[] BC00295_n2144Projeto_Identificador ;
      private String[] BC00295_A2145Projeto_Objetivo ;
      private bool[] BC00295_n2145Projeto_Objetivo ;
      private DateTime[] BC00295_A2146Projeto_DTInicio ;
      private bool[] BC00295_n2146Projeto_DTInicio ;
      private DateTime[] BC00295_A2147Projeto_DTFim ;
      private bool[] BC00295_n2147Projeto_DTFim ;
      private short[] BC00295_A2148Projeto_PrazoPrevisto ;
      private bool[] BC00295_n2148Projeto_PrazoPrevisto ;
      private short[] BC00295_A2149Projeto_EsforcoPrevisto ;
      private bool[] BC00295_n2149Projeto_EsforcoPrevisto ;
      private decimal[] BC00295_A2150Projeto_CustoPrevisto ;
      private bool[] BC00295_n2150Projeto_CustoPrevisto ;
      private short[] BC00295_A2170Projeto_AnexoSequecial ;
      private bool[] BC00295_n2170Projeto_AnexoSequecial ;
      private int[] BC00295_A983Projeto_TipoProjetoCod ;
      private bool[] BC00295_n983Projeto_TipoProjetoCod ;
      private int[] BC00295_A2151Projeto_AreaTrabalhoCodigo ;
      private bool[] BC00295_n2151Projeto_AreaTrabalhoCodigo ;
      private int[] BC00295_A2153Projeto_GpoObjCtrlCodigo ;
      private bool[] BC00295_n2153Projeto_GpoObjCtrlCodigo ;
      private int[] BC00295_A2155Projeto_SistemaCodigo ;
      private bool[] BC00295_n2155Projeto_SistemaCodigo ;
      private int[] BC00295_A2158Projeto_ModuloCodigo ;
      private bool[] BC00295_n2158Projeto_ModuloCodigo ;
      private int[] BC002917_A648Projeto_Codigo ;
      private String[] BC002920_A2152Projeto_AreaTrabalhoDescricao ;
      private bool[] BC002920_n2152Projeto_AreaTrabalhoDescricao ;
      private String[] BC002921_A2154Projeto_GpoObjCtrlNome ;
      private bool[] BC002921_n2154Projeto_GpoObjCtrlNome ;
      private String[] BC002922_A2156Projeto_SistemaNome ;
      private bool[] BC002922_n2156Projeto_SistemaNome ;
      private String[] BC002922_A2157Projeto_SistemaSigla ;
      private bool[] BC002922_n2157Projeto_SistemaSigla ;
      private decimal[] BC002924_A655Projeto_Custo ;
      private short[] BC002924_A656Projeto_Prazo ;
      private short[] BC002924_A657Projeto_Esforco ;
      private bool[] BC002924_n657Projeto_Esforco ;
      private String[] BC002925_A2159Projeto_ModuloNome ;
      private bool[] BC002925_n2159Projeto_ModuloNome ;
      private String[] BC002925_A2160Projeto_ModuloSigla ;
      private bool[] BC002925_n2160Projeto_ModuloSigla ;
      private int[] BC002926_A961VariavelCocomo_AreaTrabalhoCod ;
      private String[] BC002926_A962VariavelCocomo_Sigla ;
      private String[] BC002926_A964VariavelCocomo_Tipo ;
      private short[] BC002926_A992VariavelCocomo_Sequencial ;
      private int[] BC002927_A192Contagem_Codigo ;
      private int[] BC002928_A736ProjetoMelhoria_Codigo ;
      private int[] BC002929_A1685Proposta_Codigo ;
      private int[] BC002931_A648Projeto_Codigo ;
      private String[] BC002931_A649Projeto_Nome ;
      private String[] BC002931_A650Projeto_Sigla ;
      private String[] BC002931_A652Projeto_TecnicaContagem ;
      private String[] BC002931_A1540Projeto_Introducao ;
      private bool[] BC002931_n1540Projeto_Introducao ;
      private String[] BC002931_A653Projeto_Escopo ;
      private DateTime[] BC002931_A1541Projeto_Previsao ;
      private bool[] BC002931_n1541Projeto_Previsao ;
      private int[] BC002931_A1542Projeto_GerenteCod ;
      private bool[] BC002931_n1542Projeto_GerenteCod ;
      private int[] BC002931_A1543Projeto_ServicoCod ;
      private bool[] BC002931_n1543Projeto_ServicoCod ;
      private String[] BC002931_A658Projeto_Status ;
      private decimal[] BC002931_A985Projeto_FatorEscala ;
      private bool[] BC002931_n985Projeto_FatorEscala ;
      private decimal[] BC002931_A989Projeto_FatorMultiplicador ;
      private bool[] BC002931_n989Projeto_FatorMultiplicador ;
      private decimal[] BC002931_A990Projeto_ConstACocomo ;
      private bool[] BC002931_n990Projeto_ConstACocomo ;
      private bool[] BC002931_A1232Projeto_Incremental ;
      private bool[] BC002931_n1232Projeto_Incremental ;
      private String[] BC002931_A2144Projeto_Identificador ;
      private bool[] BC002931_n2144Projeto_Identificador ;
      private String[] BC002931_A2145Projeto_Objetivo ;
      private bool[] BC002931_n2145Projeto_Objetivo ;
      private DateTime[] BC002931_A2146Projeto_DTInicio ;
      private bool[] BC002931_n2146Projeto_DTInicio ;
      private DateTime[] BC002931_A2147Projeto_DTFim ;
      private bool[] BC002931_n2147Projeto_DTFim ;
      private short[] BC002931_A2148Projeto_PrazoPrevisto ;
      private bool[] BC002931_n2148Projeto_PrazoPrevisto ;
      private short[] BC002931_A2149Projeto_EsforcoPrevisto ;
      private bool[] BC002931_n2149Projeto_EsforcoPrevisto ;
      private decimal[] BC002931_A2150Projeto_CustoPrevisto ;
      private bool[] BC002931_n2150Projeto_CustoPrevisto ;
      private String[] BC002931_A2152Projeto_AreaTrabalhoDescricao ;
      private bool[] BC002931_n2152Projeto_AreaTrabalhoDescricao ;
      private String[] BC002931_A2154Projeto_GpoObjCtrlNome ;
      private bool[] BC002931_n2154Projeto_GpoObjCtrlNome ;
      private String[] BC002931_A2156Projeto_SistemaNome ;
      private bool[] BC002931_n2156Projeto_SistemaNome ;
      private String[] BC002931_A2157Projeto_SistemaSigla ;
      private bool[] BC002931_n2157Projeto_SistemaSigla ;
      private String[] BC002931_A2159Projeto_ModuloNome ;
      private bool[] BC002931_n2159Projeto_ModuloNome ;
      private String[] BC002931_A2160Projeto_ModuloSigla ;
      private bool[] BC002931_n2160Projeto_ModuloSigla ;
      private short[] BC002931_A2170Projeto_AnexoSequecial ;
      private bool[] BC002931_n2170Projeto_AnexoSequecial ;
      private int[] BC002931_A983Projeto_TipoProjetoCod ;
      private bool[] BC002931_n983Projeto_TipoProjetoCod ;
      private int[] BC002931_A2151Projeto_AreaTrabalhoCodigo ;
      private bool[] BC002931_n2151Projeto_AreaTrabalhoCodigo ;
      private int[] BC002931_A2153Projeto_GpoObjCtrlCodigo ;
      private bool[] BC002931_n2153Projeto_GpoObjCtrlCodigo ;
      private int[] BC002931_A2155Projeto_SistemaCodigo ;
      private bool[] BC002931_n2155Projeto_SistemaCodigo ;
      private int[] BC002931_A2158Projeto_ModuloCodigo ;
      private bool[] BC002931_n2158Projeto_ModuloCodigo ;
      private decimal[] BC002931_A655Projeto_Custo ;
      private short[] BC002931_A656Projeto_Prazo ;
      private short[] BC002931_A657Projeto_Esforco ;
      private bool[] BC002931_n657Projeto_Esforco ;
      private int[] BC002932_A648Projeto_Codigo ;
      private int[] BC002932_A2163ProjetoAnexos_Codigo ;
      private String[] BC002932_A2164ProjetoAnexos_Descricao ;
      private String[] BC002932_A2168ProjetoAnexos_Link ;
      private bool[] BC002932_n2168ProjetoAnexos_Link ;
      private DateTime[] BC002932_A2169ProjetoAnexos_Data ;
      private bool[] BC002932_n2169ProjetoAnexos_Data ;
      private String[] BC002932_A646TipoDocumento_Nome ;
      private String[] BC002932_A2167ProjetoAnexos_ArquivoTipo ;
      private bool[] BC002932_n2167ProjetoAnexos_ArquivoTipo ;
      private String[] BC002932_A2166ProjetoAnexos_ArquivoNome ;
      private bool[] BC002932_n2166ProjetoAnexos_ArquivoNome ;
      private int[] BC002932_A645TipoDocumento_Codigo ;
      private bool[] BC002932_n645TipoDocumento_Codigo ;
      private String[] BC002932_A2165ProjetoAnexos_Arquivo ;
      private bool[] BC002932_n2165ProjetoAnexos_Arquivo ;
      private String[] BC00294_A646TipoDocumento_Nome ;
      private int[] BC002933_A648Projeto_Codigo ;
      private int[] BC002933_A2163ProjetoAnexos_Codigo ;
      private int[] BC00293_A648Projeto_Codigo ;
      private int[] BC00293_A2163ProjetoAnexos_Codigo ;
      private String[] BC00293_A2164ProjetoAnexos_Descricao ;
      private String[] BC00293_A2168ProjetoAnexos_Link ;
      private bool[] BC00293_n2168ProjetoAnexos_Link ;
      private DateTime[] BC00293_A2169ProjetoAnexos_Data ;
      private bool[] BC00293_n2169ProjetoAnexos_Data ;
      private String[] BC00293_A2167ProjetoAnexos_ArquivoTipo ;
      private bool[] BC00293_n2167ProjetoAnexos_ArquivoTipo ;
      private String[] BC00293_A2166ProjetoAnexos_ArquivoNome ;
      private bool[] BC00293_n2166ProjetoAnexos_ArquivoNome ;
      private int[] BC00293_A645TipoDocumento_Codigo ;
      private bool[] BC00293_n645TipoDocumento_Codigo ;
      private String[] BC00293_A2165ProjetoAnexos_Arquivo ;
      private bool[] BC00293_n2165ProjetoAnexos_Arquivo ;
      private int[] BC00292_A648Projeto_Codigo ;
      private int[] BC00292_A2163ProjetoAnexos_Codigo ;
      private String[] BC00292_A2164ProjetoAnexos_Descricao ;
      private String[] BC00292_A2168ProjetoAnexos_Link ;
      private bool[] BC00292_n2168ProjetoAnexos_Link ;
      private DateTime[] BC00292_A2169ProjetoAnexos_Data ;
      private bool[] BC00292_n2169ProjetoAnexos_Data ;
      private String[] BC00292_A2167ProjetoAnexos_ArquivoTipo ;
      private bool[] BC00292_n2167ProjetoAnexos_ArquivoTipo ;
      private String[] BC00292_A2166ProjetoAnexos_ArquivoNome ;
      private bool[] BC00292_n2166ProjetoAnexos_ArquivoNome ;
      private int[] BC00292_A645TipoDocumento_Codigo ;
      private bool[] BC00292_n645TipoDocumento_Codigo ;
      private String[] BC00292_A2165ProjetoAnexos_Arquivo ;
      private bool[] BC00292_n2165ProjetoAnexos_Arquivo ;
      private String[] BC002938_A646TipoDocumento_Nome ;
      private int[] BC002939_A648Projeto_Codigo ;
      private int[] BC002939_A2163ProjetoAnexos_Codigo ;
      private String[] BC002939_A2164ProjetoAnexos_Descricao ;
      private String[] BC002939_A2168ProjetoAnexos_Link ;
      private bool[] BC002939_n2168ProjetoAnexos_Link ;
      private DateTime[] BC002939_A2169ProjetoAnexos_Data ;
      private bool[] BC002939_n2169ProjetoAnexos_Data ;
      private String[] BC002939_A646TipoDocumento_Nome ;
      private String[] BC002939_A2167ProjetoAnexos_ArquivoTipo ;
      private bool[] BC002939_n2167ProjetoAnexos_ArquivoTipo ;
      private String[] BC002939_A2166ProjetoAnexos_ArquivoNome ;
      private bool[] BC002939_n2166ProjetoAnexos_ArquivoNome ;
      private int[] BC002939_A645TipoDocumento_Codigo ;
      private bool[] BC002939_n645TipoDocumento_Codigo ;
      private String[] BC002939_A2165ProjetoAnexos_Arquivo ;
      private bool[] BC002939_n2165ProjetoAnexos_Arquivo ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV13TrnContextAtt ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
   }

   public class projeto_bc__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new UpdateCursor(def[14])
         ,new UpdateCursor(def[15])
         ,new ForEachCursor(def[16])
         ,new ForEachCursor(def[17])
         ,new ForEachCursor(def[18])
         ,new ForEachCursor(def[19])
         ,new ForEachCursor(def[20])
         ,new ForEachCursor(def[21])
         ,new ForEachCursor(def[22])
         ,new ForEachCursor(def[23])
         ,new ForEachCursor(def[24])
         ,new ForEachCursor(def[25])
         ,new ForEachCursor(def[26])
         ,new ForEachCursor(def[27])
         ,new UpdateCursor(def[28])
         ,new UpdateCursor(def[29])
         ,new UpdateCursor(def[30])
         ,new UpdateCursor(def[31])
         ,new ForEachCursor(def[32])
         ,new ForEachCursor(def[33])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmBC002915 ;
          prmBC002915 = new Object[] {
          new Object[] {"@Projeto_Codigo",SqlDbType.Int,6,0}
          } ;
          String cmdBufferBC002915 ;
          cmdBufferBC002915=" SELECT TM1.[Projeto_Codigo], TM1.[Projeto_Nome], TM1.[Projeto_Sigla], TM1.[Projeto_TecnicaContagem], TM1.[Projeto_Introducao], TM1.[Projeto_Escopo], TM1.[Projeto_Previsao], TM1.[Projeto_GerenteCod], TM1.[Projeto_ServicoCod], TM1.[Projeto_Status], TM1.[Projeto_FatorEscala], TM1.[Projeto_FatorMultiplicador], TM1.[Projeto_ConstACocomo], TM1.[Projeto_Incremental], TM1.[Projeto_Identificador], TM1.[Projeto_Objetivo], TM1.[Projeto_DTInicio], TM1.[Projeto_DTFim], TM1.[Projeto_PrazoPrevisto], TM1.[Projeto_EsforcoPrevisto], TM1.[Projeto_CustoPrevisto], T2.[AreaTrabalho_Descricao] AS Projeto_AreaTrabalhoDescricao, T3.[GpoObjCtrl_Nome] AS Projeto_GpoObjCtrlNome, T4.[Sistema_Nome] AS Projeto_SistemaNome, T4.[Sistema_Sigla] AS Projeto_SistemaSigla, T6.[Modulo_Nome] AS Projeto_ModuloNome, T6.[Modulo_Sigla] AS Projeto_ModuloSigla, TM1.[Projeto_AnexoSequecial], TM1.[Projeto_TipoProjetoCod] AS Projeto_TipoProjetoCod, TM1.[Projeto_AreaTrabalhoCodigo] AS Projeto_AreaTrabalhoCodigo, TM1.[Projeto_GpoObjCtrlCodigo] AS Projeto_GpoObjCtrlCodigo, TM1.[Projeto_SistemaCodigo] AS Projeto_SistemaCodigo, TM1.[Projeto_ModuloCodigo] AS Projeto_ModuloCodigo, COALESCE( T5.[Projeto_Custo], 0) AS Projeto_Custo, COALESCE( T5.[Projeto_Prazo], 0) AS Projeto_Prazo, COALESCE( T5.[Projeto_Esforco], 0) AS Projeto_Esforco FROM ((((([Projeto] TM1 WITH (NOLOCK) LEFT JOIN [AreaTrabalho] T2 WITH (NOLOCK) ON T2.[AreaTrabalho_Codigo] = TM1.[Projeto_AreaTrabalhoCodigo]) LEFT JOIN [GrupoObjetoControle] T3 WITH (NOLOCK) ON T3.[GpoObjCtrl_Codigo] = TM1.[Projeto_GpoObjCtrlCodigo]) LEFT JOIN [Sistema] T4 WITH (NOLOCK) ON T4.[Sistema_Codigo] = TM1.[Projeto_SistemaCodigo]) LEFT JOIN (SELECT [Sistema_Esforco] AS Projeto_Esforco, [Sistema_Codigo], [Sistema_Prazo] AS Projeto_Prazo, [Sistema_Custo] AS Projeto_Custo FROM [Sistema] WITH "
          + " (NOLOCK) ) T5 ON T5.[Sistema_Codigo] = TM1.[Projeto_SistemaCodigo]) LEFT JOIN [Modulo] T6 WITH (NOLOCK) ON T6.[Modulo_Codigo] = TM1.[Projeto_ModuloCodigo]) WHERE TM1.[Projeto_Codigo] = @Projeto_Codigo ORDER BY TM1.[Projeto_Codigo]  OPTION (FAST 100)" ;
          Object[] prmBC00297 ;
          prmBC00297 = new Object[] {
          new Object[] {"@Projeto_TipoProjetoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00298 ;
          prmBC00298 = new Object[] {
          new Object[] {"@Projeto_AreaTrabalhoCodigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00299 ;
          prmBC00299 = new Object[] {
          new Object[] {"@Projeto_GpoObjCtrlCodigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC002910 ;
          prmBC002910 = new Object[] {
          new Object[] {"@Projeto_SistemaCodigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC002913 ;
          prmBC002913 = new Object[] {
          new Object[] {"@Projeto_SistemaCodigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC002911 ;
          prmBC002911 = new Object[] {
          new Object[] {"@Projeto_ModuloCodigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC002916 ;
          prmBC002916 = new Object[] {
          new Object[] {"@Projeto_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00296 ;
          prmBC00296 = new Object[] {
          new Object[] {"@Projeto_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00295 ;
          prmBC00295 = new Object[] {
          new Object[] {"@Projeto_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC002917 ;
          prmBC002917 = new Object[] {
          new Object[] {"@Projeto_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@Projeto_Sigla",SqlDbType.Char,15,0} ,
          new Object[] {"@Projeto_TecnicaContagem",SqlDbType.Char,1,0} ,
          new Object[] {"@Projeto_Introducao",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@Projeto_Escopo",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@Projeto_Previsao",SqlDbType.DateTime,8,0} ,
          new Object[] {"@Projeto_GerenteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Projeto_ServicoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Projeto_Status",SqlDbType.Char,1,0} ,
          new Object[] {"@Projeto_FatorEscala",SqlDbType.Decimal,6,2} ,
          new Object[] {"@Projeto_FatorMultiplicador",SqlDbType.Decimal,6,2} ,
          new Object[] {"@Projeto_ConstACocomo",SqlDbType.Decimal,6,2} ,
          new Object[] {"@Projeto_Incremental",SqlDbType.Bit,4,0} ,
          new Object[] {"@Projeto_Identificador",SqlDbType.VarChar,50,0} ,
          new Object[] {"@Projeto_Objetivo",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@Projeto_DTInicio",SqlDbType.DateTime,8,0} ,
          new Object[] {"@Projeto_DTFim",SqlDbType.DateTime,8,0} ,
          new Object[] {"@Projeto_PrazoPrevisto",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Projeto_EsforcoPrevisto",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Projeto_CustoPrevisto",SqlDbType.Decimal,12,2} ,
          new Object[] {"@Projeto_AnexoSequecial",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Projeto_TipoProjetoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Projeto_AreaTrabalhoCodigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Projeto_GpoObjCtrlCodigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Projeto_SistemaCodigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Projeto_ModuloCodigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC002918 ;
          prmBC002918 = new Object[] {
          new Object[] {"@Projeto_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@Projeto_Sigla",SqlDbType.Char,15,0} ,
          new Object[] {"@Projeto_TecnicaContagem",SqlDbType.Char,1,0} ,
          new Object[] {"@Projeto_Introducao",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@Projeto_Escopo",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@Projeto_Previsao",SqlDbType.DateTime,8,0} ,
          new Object[] {"@Projeto_GerenteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Projeto_ServicoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Projeto_Status",SqlDbType.Char,1,0} ,
          new Object[] {"@Projeto_FatorEscala",SqlDbType.Decimal,6,2} ,
          new Object[] {"@Projeto_FatorMultiplicador",SqlDbType.Decimal,6,2} ,
          new Object[] {"@Projeto_ConstACocomo",SqlDbType.Decimal,6,2} ,
          new Object[] {"@Projeto_Incremental",SqlDbType.Bit,4,0} ,
          new Object[] {"@Projeto_Identificador",SqlDbType.VarChar,50,0} ,
          new Object[] {"@Projeto_Objetivo",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@Projeto_DTInicio",SqlDbType.DateTime,8,0} ,
          new Object[] {"@Projeto_DTFim",SqlDbType.DateTime,8,0} ,
          new Object[] {"@Projeto_PrazoPrevisto",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Projeto_EsforcoPrevisto",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Projeto_CustoPrevisto",SqlDbType.Decimal,12,2} ,
          new Object[] {"@Projeto_AnexoSequecial",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Projeto_TipoProjetoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Projeto_AreaTrabalhoCodigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Projeto_GpoObjCtrlCodigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Projeto_SistemaCodigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Projeto_ModuloCodigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Projeto_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC002919 ;
          prmBC002919 = new Object[] {
          new Object[] {"@Projeto_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC002920 ;
          prmBC002920 = new Object[] {
          new Object[] {"@Projeto_AreaTrabalhoCodigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC002921 ;
          prmBC002921 = new Object[] {
          new Object[] {"@Projeto_GpoObjCtrlCodigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC002922 ;
          prmBC002922 = new Object[] {
          new Object[] {"@Projeto_SistemaCodigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC002924 ;
          prmBC002924 = new Object[] {
          new Object[] {"@Projeto_SistemaCodigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC002925 ;
          prmBC002925 = new Object[] {
          new Object[] {"@Projeto_ModuloCodigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC002926 ;
          prmBC002926 = new Object[] {
          new Object[] {"@Projeto_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC002927 ;
          prmBC002927 = new Object[] {
          new Object[] {"@Projeto_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC002928 ;
          prmBC002928 = new Object[] {
          new Object[] {"@Projeto_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC002929 ;
          prmBC002929 = new Object[] {
          new Object[] {"@Projeto_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC002931 ;
          prmBC002931 = new Object[] {
          new Object[] {"@Projeto_Codigo",SqlDbType.Int,6,0}
          } ;
          String cmdBufferBC002931 ;
          cmdBufferBC002931=" SELECT TM1.[Projeto_Codigo], TM1.[Projeto_Nome], TM1.[Projeto_Sigla], TM1.[Projeto_TecnicaContagem], TM1.[Projeto_Introducao], TM1.[Projeto_Escopo], TM1.[Projeto_Previsao], TM1.[Projeto_GerenteCod], TM1.[Projeto_ServicoCod], TM1.[Projeto_Status], TM1.[Projeto_FatorEscala], TM1.[Projeto_FatorMultiplicador], TM1.[Projeto_ConstACocomo], TM1.[Projeto_Incremental], TM1.[Projeto_Identificador], TM1.[Projeto_Objetivo], TM1.[Projeto_DTInicio], TM1.[Projeto_DTFim], TM1.[Projeto_PrazoPrevisto], TM1.[Projeto_EsforcoPrevisto], TM1.[Projeto_CustoPrevisto], T2.[AreaTrabalho_Descricao] AS Projeto_AreaTrabalhoDescricao, T3.[GpoObjCtrl_Nome] AS Projeto_GpoObjCtrlNome, T4.[Sistema_Nome] AS Projeto_SistemaNome, T4.[Sistema_Sigla] AS Projeto_SistemaSigla, T6.[Modulo_Nome] AS Projeto_ModuloNome, T6.[Modulo_Sigla] AS Projeto_ModuloSigla, TM1.[Projeto_AnexoSequecial], TM1.[Projeto_TipoProjetoCod] AS Projeto_TipoProjetoCod, TM1.[Projeto_AreaTrabalhoCodigo] AS Projeto_AreaTrabalhoCodigo, TM1.[Projeto_GpoObjCtrlCodigo] AS Projeto_GpoObjCtrlCodigo, TM1.[Projeto_SistemaCodigo] AS Projeto_SistemaCodigo, TM1.[Projeto_ModuloCodigo] AS Projeto_ModuloCodigo, COALESCE( T5.[Projeto_Custo], 0) AS Projeto_Custo, COALESCE( T5.[Projeto_Prazo], 0) AS Projeto_Prazo, COALESCE( T5.[Projeto_Esforco], 0) AS Projeto_Esforco FROM ((((([Projeto] TM1 WITH (NOLOCK) LEFT JOIN [AreaTrabalho] T2 WITH (NOLOCK) ON T2.[AreaTrabalho_Codigo] = TM1.[Projeto_AreaTrabalhoCodigo]) LEFT JOIN [GrupoObjetoControle] T3 WITH (NOLOCK) ON T3.[GpoObjCtrl_Codigo] = TM1.[Projeto_GpoObjCtrlCodigo]) LEFT JOIN [Sistema] T4 WITH (NOLOCK) ON T4.[Sistema_Codigo] = TM1.[Projeto_SistemaCodigo]) LEFT JOIN (SELECT [Sistema_Esforco] AS Projeto_Esforco, [Sistema_Codigo], [Sistema_Prazo] AS Projeto_Prazo, [Sistema_Custo] AS Projeto_Custo FROM [Sistema] WITH "
          + " (NOLOCK) ) T5 ON T5.[Sistema_Codigo] = TM1.[Projeto_SistemaCodigo]) LEFT JOIN [Modulo] T6 WITH (NOLOCK) ON T6.[Modulo_Codigo] = TM1.[Projeto_ModuloCodigo]) WHERE TM1.[Projeto_Codigo] = @Projeto_Codigo ORDER BY TM1.[Projeto_Codigo]  OPTION (FAST 100)" ;
          Object[] prmBC002932 ;
          prmBC002932 = new Object[] {
          new Object[] {"@Projeto_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ProjetoAnexos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00294 ;
          prmBC00294 = new Object[] {
          new Object[] {"@TipoDocumento_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC002933 ;
          prmBC002933 = new Object[] {
          new Object[] {"@Projeto_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ProjetoAnexos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00293 ;
          prmBC00293 = new Object[] {
          new Object[] {"@Projeto_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ProjetoAnexos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00292 ;
          prmBC00292 = new Object[] {
          new Object[] {"@Projeto_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ProjetoAnexos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC002934 ;
          prmBC002934 = new Object[] {
          new Object[] {"@Projeto_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ProjetoAnexos_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ProjetoAnexos_Descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@ProjetoAnexos_Arquivo",SqlDbType.VarBinary,1024,0} ,
          new Object[] {"@ProjetoAnexos_Link",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@ProjetoAnexos_Data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ProjetoAnexos_ArquivoTipo",SqlDbType.Char,10,0} ,
          new Object[] {"@ProjetoAnexos_ArquivoNome",SqlDbType.Char,50,0} ,
          new Object[] {"@TipoDocumento_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC002935 ;
          prmBC002935 = new Object[] {
          new Object[] {"@ProjetoAnexos_Descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@ProjetoAnexos_Link",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@ProjetoAnexos_Data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ProjetoAnexos_ArquivoTipo",SqlDbType.Char,10,0} ,
          new Object[] {"@ProjetoAnexos_ArquivoNome",SqlDbType.Char,50,0} ,
          new Object[] {"@TipoDocumento_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Projeto_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ProjetoAnexos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC002936 ;
          prmBC002936 = new Object[] {
          new Object[] {"@ProjetoAnexos_Arquivo",SqlDbType.VarBinary,1024,0} ,
          new Object[] {"@Projeto_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ProjetoAnexos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC002937 ;
          prmBC002937 = new Object[] {
          new Object[] {"@Projeto_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ProjetoAnexos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC002938 ;
          prmBC002938 = new Object[] {
          new Object[] {"@TipoDocumento_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC002939 ;
          prmBC002939 = new Object[] {
          new Object[] {"@Projeto_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("BC00292", "SELECT [Projeto_Codigo], [ProjetoAnexos_Codigo], [ProjetoAnexos_Descricao], [ProjetoAnexos_Link], [ProjetoAnexos_Data], [ProjetoAnexos_ArquivoTipo], [ProjetoAnexos_ArquivoNome], [TipoDocumento_Codigo], [ProjetoAnexos_Arquivo] FROM [ProjetoAnexos] WITH (UPDLOCK) WHERE [Projeto_Codigo] = @Projeto_Codigo AND [ProjetoAnexos_Codigo] = @ProjetoAnexos_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00292,1,0,true,false )
             ,new CursorDef("BC00293", "SELECT [Projeto_Codigo], [ProjetoAnexos_Codigo], [ProjetoAnexos_Descricao], [ProjetoAnexos_Link], [ProjetoAnexos_Data], [ProjetoAnexos_ArquivoTipo], [ProjetoAnexos_ArquivoNome], [TipoDocumento_Codigo], [ProjetoAnexos_Arquivo] FROM [ProjetoAnexos] WITH (NOLOCK) WHERE [Projeto_Codigo] = @Projeto_Codigo AND [ProjetoAnexos_Codigo] = @ProjetoAnexos_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00293,1,0,true,false )
             ,new CursorDef("BC00294", "SELECT [TipoDocumento_Nome] FROM [TipoDocumento] WITH (NOLOCK) WHERE [TipoDocumento_Codigo] = @TipoDocumento_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00294,1,0,true,false )
             ,new CursorDef("BC00295", "SELECT [Projeto_Codigo], [Projeto_Nome], [Projeto_Sigla], [Projeto_TecnicaContagem], [Projeto_Introducao], [Projeto_Escopo], [Projeto_Previsao], [Projeto_GerenteCod], [Projeto_ServicoCod], [Projeto_Status], [Projeto_FatorEscala], [Projeto_FatorMultiplicador], [Projeto_ConstACocomo], [Projeto_Incremental], [Projeto_Identificador], [Projeto_Objetivo], [Projeto_DTInicio], [Projeto_DTFim], [Projeto_PrazoPrevisto], [Projeto_EsforcoPrevisto], [Projeto_CustoPrevisto], [Projeto_AnexoSequecial], [Projeto_TipoProjetoCod] AS Projeto_TipoProjetoCod, [Projeto_AreaTrabalhoCodigo] AS Projeto_AreaTrabalhoCodigo, [Projeto_GpoObjCtrlCodigo] AS Projeto_GpoObjCtrlCodigo, [Projeto_SistemaCodigo] AS Projeto_SistemaCodigo, [Projeto_ModuloCodigo] AS Projeto_ModuloCodigo FROM [Projeto] WITH (UPDLOCK) WHERE [Projeto_Codigo] = @Projeto_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00295,1,0,true,false )
             ,new CursorDef("BC00296", "SELECT [Projeto_Codigo], [Projeto_Nome], [Projeto_Sigla], [Projeto_TecnicaContagem], [Projeto_Introducao], [Projeto_Escopo], [Projeto_Previsao], [Projeto_GerenteCod], [Projeto_ServicoCod], [Projeto_Status], [Projeto_FatorEscala], [Projeto_FatorMultiplicador], [Projeto_ConstACocomo], [Projeto_Incremental], [Projeto_Identificador], [Projeto_Objetivo], [Projeto_DTInicio], [Projeto_DTFim], [Projeto_PrazoPrevisto], [Projeto_EsforcoPrevisto], [Projeto_CustoPrevisto], [Projeto_AnexoSequecial], [Projeto_TipoProjetoCod] AS Projeto_TipoProjetoCod, [Projeto_AreaTrabalhoCodigo] AS Projeto_AreaTrabalhoCodigo, [Projeto_GpoObjCtrlCodigo] AS Projeto_GpoObjCtrlCodigo, [Projeto_SistemaCodigo] AS Projeto_SistemaCodigo, [Projeto_ModuloCodigo] AS Projeto_ModuloCodigo FROM [Projeto] WITH (NOLOCK) WHERE [Projeto_Codigo] = @Projeto_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00296,1,0,true,false )
             ,new CursorDef("BC00297", "SELECT [TipoProjeto_Codigo] AS Projeto_TipoProjetoCod FROM [TipoProjeto] WITH (NOLOCK) WHERE [TipoProjeto_Codigo] = @Projeto_TipoProjetoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00297,1,0,true,false )
             ,new CursorDef("BC00298", "SELECT [AreaTrabalho_Descricao] AS Projeto_AreaTrabalhoDescricao FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @Projeto_AreaTrabalhoCodigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00298,1,0,true,false )
             ,new CursorDef("BC00299", "SELECT [GpoObjCtrl_Nome] AS Projeto_GpoObjCtrlNome FROM [GrupoObjetoControle] WITH (NOLOCK) WHERE [GpoObjCtrl_Codigo] = @Projeto_GpoObjCtrlCodigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00299,1,0,true,false )
             ,new CursorDef("BC002910", "SELECT [Sistema_Nome] AS Projeto_SistemaNome, [Sistema_Sigla] AS Projeto_SistemaSigla FROM [Sistema] WITH (NOLOCK) WHERE [Sistema_Codigo] = @Projeto_SistemaCodigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC002910,1,0,true,false )
             ,new CursorDef("BC002911", "SELECT [Modulo_Nome] AS Projeto_ModuloNome, [Modulo_Sigla] AS Projeto_ModuloSigla FROM [Modulo] WITH (NOLOCK) WHERE [Modulo_Codigo] = @Projeto_ModuloCodigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC002911,1,0,true,false )
             ,new CursorDef("BC002913", "SELECT COALESCE( T1.[Projeto_Custo], 0) AS Projeto_Custo, COALESCE( T1.[Projeto_Prazo], 0) AS Projeto_Prazo, COALESCE( T1.[Projeto_Esforco], 0) AS Projeto_Esforco FROM (SELECT [Sistema_Esforco] AS Projeto_Esforco, [Sistema_Codigo], [Sistema_Prazo] AS Projeto_Prazo, [Sistema_Custo] AS Projeto_Custo FROM [Sistema] WITH (NOLOCK) ) T1 WHERE T1.[Sistema_Codigo] = @Projeto_SistemaCodigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC002913,1,0,true,false )
             ,new CursorDef("BC002915", cmdBufferBC002915,true, GxErrorMask.GX_NOMASK, false, this,prmBC002915,100,0,true,false )
             ,new CursorDef("BC002916", "SELECT [Projeto_Codigo] FROM [Projeto] WITH (NOLOCK) WHERE [Projeto_Codigo] = @Projeto_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmBC002916,1,0,true,false )
             ,new CursorDef("BC002917", "INSERT INTO [Projeto]([Projeto_Nome], [Projeto_Sigla], [Projeto_TecnicaContagem], [Projeto_Introducao], [Projeto_Escopo], [Projeto_Previsao], [Projeto_GerenteCod], [Projeto_ServicoCod], [Projeto_Status], [Projeto_FatorEscala], [Projeto_FatorMultiplicador], [Projeto_ConstACocomo], [Projeto_Incremental], [Projeto_Identificador], [Projeto_Objetivo], [Projeto_DTInicio], [Projeto_DTFim], [Projeto_PrazoPrevisto], [Projeto_EsforcoPrevisto], [Projeto_CustoPrevisto], [Projeto_AnexoSequecial], [Projeto_TipoProjetoCod], [Projeto_AreaTrabalhoCodigo], [Projeto_GpoObjCtrlCodigo], [Projeto_SistemaCodigo], [Projeto_ModuloCodigo]) VALUES(@Projeto_Nome, @Projeto_Sigla, @Projeto_TecnicaContagem, @Projeto_Introducao, @Projeto_Escopo, @Projeto_Previsao, @Projeto_GerenteCod, @Projeto_ServicoCod, @Projeto_Status, @Projeto_FatorEscala, @Projeto_FatorMultiplicador, @Projeto_ConstACocomo, @Projeto_Incremental, @Projeto_Identificador, @Projeto_Objetivo, @Projeto_DTInicio, @Projeto_DTFim, @Projeto_PrazoPrevisto, @Projeto_EsforcoPrevisto, @Projeto_CustoPrevisto, @Projeto_AnexoSequecial, @Projeto_TipoProjetoCod, @Projeto_AreaTrabalhoCodigo, @Projeto_GpoObjCtrlCodigo, @Projeto_SistemaCodigo, @Projeto_ModuloCodigo); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmBC002917)
             ,new CursorDef("BC002918", "UPDATE [Projeto] SET [Projeto_Nome]=@Projeto_Nome, [Projeto_Sigla]=@Projeto_Sigla, [Projeto_TecnicaContagem]=@Projeto_TecnicaContagem, [Projeto_Introducao]=@Projeto_Introducao, [Projeto_Escopo]=@Projeto_Escopo, [Projeto_Previsao]=@Projeto_Previsao, [Projeto_GerenteCod]=@Projeto_GerenteCod, [Projeto_ServicoCod]=@Projeto_ServicoCod, [Projeto_Status]=@Projeto_Status, [Projeto_FatorEscala]=@Projeto_FatorEscala, [Projeto_FatorMultiplicador]=@Projeto_FatorMultiplicador, [Projeto_ConstACocomo]=@Projeto_ConstACocomo, [Projeto_Incremental]=@Projeto_Incremental, [Projeto_Identificador]=@Projeto_Identificador, [Projeto_Objetivo]=@Projeto_Objetivo, [Projeto_DTInicio]=@Projeto_DTInicio, [Projeto_DTFim]=@Projeto_DTFim, [Projeto_PrazoPrevisto]=@Projeto_PrazoPrevisto, [Projeto_EsforcoPrevisto]=@Projeto_EsforcoPrevisto, [Projeto_CustoPrevisto]=@Projeto_CustoPrevisto, [Projeto_AnexoSequecial]=@Projeto_AnexoSequecial, [Projeto_TipoProjetoCod]=@Projeto_TipoProjetoCod, [Projeto_AreaTrabalhoCodigo]=@Projeto_AreaTrabalhoCodigo, [Projeto_GpoObjCtrlCodigo]=@Projeto_GpoObjCtrlCodigo, [Projeto_SistemaCodigo]=@Projeto_SistemaCodigo, [Projeto_ModuloCodigo]=@Projeto_ModuloCodigo  WHERE [Projeto_Codigo] = @Projeto_Codigo", GxErrorMask.GX_NOMASK,prmBC002918)
             ,new CursorDef("BC002919", "DELETE FROM [Projeto]  WHERE [Projeto_Codigo] = @Projeto_Codigo", GxErrorMask.GX_NOMASK,prmBC002919)
             ,new CursorDef("BC002920", "SELECT [AreaTrabalho_Descricao] AS Projeto_AreaTrabalhoDescricao FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @Projeto_AreaTrabalhoCodigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC002920,1,0,true,false )
             ,new CursorDef("BC002921", "SELECT [GpoObjCtrl_Nome] AS Projeto_GpoObjCtrlNome FROM [GrupoObjetoControle] WITH (NOLOCK) WHERE [GpoObjCtrl_Codigo] = @Projeto_GpoObjCtrlCodigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC002921,1,0,true,false )
             ,new CursorDef("BC002922", "SELECT [Sistema_Nome] AS Projeto_SistemaNome, [Sistema_Sigla] AS Projeto_SistemaSigla FROM [Sistema] WITH (NOLOCK) WHERE [Sistema_Codigo] = @Projeto_SistemaCodigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC002922,1,0,true,false )
             ,new CursorDef("BC002924", "SELECT COALESCE( T1.[Projeto_Custo], 0) AS Projeto_Custo, COALESCE( T1.[Projeto_Prazo], 0) AS Projeto_Prazo, COALESCE( T1.[Projeto_Esforco], 0) AS Projeto_Esforco FROM (SELECT [Sistema_Esforco] AS Projeto_Esforco, [Sistema_Codigo], [Sistema_Prazo] AS Projeto_Prazo, [Sistema_Custo] AS Projeto_Custo FROM [Sistema] WITH (NOLOCK) ) T1 WHERE T1.[Sistema_Codigo] = @Projeto_SistemaCodigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC002924,1,0,true,false )
             ,new CursorDef("BC002925", "SELECT [Modulo_Nome] AS Projeto_ModuloNome, [Modulo_Sigla] AS Projeto_ModuloSigla FROM [Modulo] WITH (NOLOCK) WHERE [Modulo_Codigo] = @Projeto_ModuloCodigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC002925,1,0,true,false )
             ,new CursorDef("BC002926", "SELECT TOP 1 [VariavelCocomo_AreaTrabalhoCod], [VariavelCocomo_Sigla], [VariavelCocomo_Tipo], [VariavelCocomo_Sequencial] FROM [VariaveisCocomo] WITH (NOLOCK) WHERE [VariavelCocomo_ProjetoCod] = @Projeto_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC002926,1,0,true,true )
             ,new CursorDef("BC002927", "SELECT TOP 1 [Contagem_Codigo] FROM [Contagem] WITH (NOLOCK) WHERE [Contagem_ProjetoCod] = @Projeto_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC002927,1,0,true,true )
             ,new CursorDef("BC002928", "SELECT TOP 1 [ProjetoMelhoria_Codigo] FROM [ProjetoMelhoria] WITH (NOLOCK) WHERE [ProjetoMelhoria_ProjetoCod] = @Projeto_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC002928,1,0,true,true )
             ,new CursorDef("BC002929", "SELECT TOP 1 [Proposta_Codigo] FROM dbo.[Proposta] WITH (NOLOCK) WHERE [Projeto_Codigo] = @Projeto_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC002929,1,0,true,true )
             ,new CursorDef("BC002931", cmdBufferBC002931,true, GxErrorMask.GX_NOMASK, false, this,prmBC002931,100,0,true,false )
             ,new CursorDef("BC002932", "SELECT T1.[Projeto_Codigo], T1.[ProjetoAnexos_Codigo], T1.[ProjetoAnexos_Descricao], T1.[ProjetoAnexos_Link], T1.[ProjetoAnexos_Data], T2.[TipoDocumento_Nome], T1.[ProjetoAnexos_ArquivoTipo], T1.[ProjetoAnexos_ArquivoNome], T1.[TipoDocumento_Codigo], T1.[ProjetoAnexos_Arquivo] FROM ([ProjetoAnexos] T1 WITH (NOLOCK) LEFT JOIN [TipoDocumento] T2 WITH (NOLOCK) ON T2.[TipoDocumento_Codigo] = T1.[TipoDocumento_Codigo]) WHERE T1.[Projeto_Codigo] = @Projeto_Codigo and T1.[ProjetoAnexos_Codigo] = @ProjetoAnexos_Codigo ORDER BY T1.[Projeto_Codigo], T1.[ProjetoAnexos_Codigo]  OPTION (FAST 11)",true, GxErrorMask.GX_NOMASK, false, this,prmBC002932,11,0,true,false )
             ,new CursorDef("BC002933", "SELECT [Projeto_Codigo], [ProjetoAnexos_Codigo] FROM [ProjetoAnexos] WITH (NOLOCK) WHERE [Projeto_Codigo] = @Projeto_Codigo AND [ProjetoAnexos_Codigo] = @ProjetoAnexos_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmBC002933,1,0,true,false )
             ,new CursorDef("BC002934", "INSERT INTO [ProjetoAnexos]([Projeto_Codigo], [ProjetoAnexos_Codigo], [ProjetoAnexos_Descricao], [ProjetoAnexos_Arquivo], [ProjetoAnexos_Link], [ProjetoAnexos_Data], [ProjetoAnexos_ArquivoTipo], [ProjetoAnexos_ArquivoNome], [TipoDocumento_Codigo]) VALUES(@Projeto_Codigo, @ProjetoAnexos_Codigo, @ProjetoAnexos_Descricao, @ProjetoAnexos_Arquivo, @ProjetoAnexos_Link, @ProjetoAnexos_Data, @ProjetoAnexos_ArquivoTipo, @ProjetoAnexos_ArquivoNome, @TipoDocumento_Codigo)", GxErrorMask.GX_NOMASK,prmBC002934)
             ,new CursorDef("BC002935", "UPDATE [ProjetoAnexos] SET [ProjetoAnexos_Descricao]=@ProjetoAnexos_Descricao, [ProjetoAnexos_Link]=@ProjetoAnexos_Link, [ProjetoAnexos_Data]=@ProjetoAnexos_Data, [ProjetoAnexos_ArquivoTipo]=@ProjetoAnexos_ArquivoTipo, [ProjetoAnexos_ArquivoNome]=@ProjetoAnexos_ArquivoNome, [TipoDocumento_Codigo]=@TipoDocumento_Codigo  WHERE [Projeto_Codigo] = @Projeto_Codigo AND [ProjetoAnexos_Codigo] = @ProjetoAnexos_Codigo", GxErrorMask.GX_NOMASK,prmBC002935)
             ,new CursorDef("BC002936", "UPDATE [ProjetoAnexos] SET [ProjetoAnexos_Arquivo]=@ProjetoAnexos_Arquivo  WHERE [Projeto_Codigo] = @Projeto_Codigo AND [ProjetoAnexos_Codigo] = @ProjetoAnexos_Codigo", GxErrorMask.GX_NOMASK,prmBC002936)
             ,new CursorDef("BC002937", "DELETE FROM [ProjetoAnexos]  WHERE [Projeto_Codigo] = @Projeto_Codigo AND [ProjetoAnexos_Codigo] = @ProjetoAnexos_Codigo", GxErrorMask.GX_NOMASK,prmBC002937)
             ,new CursorDef("BC002938", "SELECT [TipoDocumento_Nome] FROM [TipoDocumento] WITH (NOLOCK) WHERE [TipoDocumento_Codigo] = @TipoDocumento_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC002938,1,0,true,false )
             ,new CursorDef("BC002939", "SELECT T1.[Projeto_Codigo], T1.[ProjetoAnexos_Codigo], T1.[ProjetoAnexos_Descricao], T1.[ProjetoAnexos_Link], T1.[ProjetoAnexos_Data], T2.[TipoDocumento_Nome], T1.[ProjetoAnexos_ArquivoTipo], T1.[ProjetoAnexos_ArquivoNome], T1.[TipoDocumento_Codigo], T1.[ProjetoAnexos_Arquivo] FROM ([ProjetoAnexos] T1 WITH (NOLOCK) LEFT JOIN [TipoDocumento] T2 WITH (NOLOCK) ON T2.[TipoDocumento_Codigo] = T1.[TipoDocumento_Codigo]) WHERE T1.[Projeto_Codigo] = @Projeto_Codigo ORDER BY T1.[Projeto_Codigo], T1.[ProjetoAnexos_Codigo]  OPTION (FAST 11)",true, GxErrorMask.GX_NOMASK, false, this,prmBC002939,11,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((String[]) buf[3])[0] = rslt.getLongVarchar(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((DateTime[]) buf[5])[0] = rslt.getGXDateTime(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((String[]) buf[7])[0] = rslt.getString(6, 10) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((String[]) buf[9])[0] = rslt.getString(7, 50) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((int[]) buf[11])[0] = rslt.getInt(8) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((String[]) buf[13])[0] = rslt.getBLOBFile(9, rslt.getString(6, 10), rslt.getString(7, 50)) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((String[]) buf[3])[0] = rslt.getLongVarchar(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((DateTime[]) buf[5])[0] = rslt.getGXDateTime(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((String[]) buf[7])[0] = rslt.getString(6, 10) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((String[]) buf[9])[0] = rslt.getString(7, 50) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((int[]) buf[11])[0] = rslt.getInt(8) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((String[]) buf[13])[0] = rslt.getBLOBFile(9, rslt.getString(6, 10), rslt.getString(7, 50)) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 15) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 1) ;
                ((String[]) buf[4])[0] = rslt.getLongVarchar(5) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(5);
                ((String[]) buf[6])[0] = rslt.getLongVarchar(6) ;
                ((DateTime[]) buf[7])[0] = rslt.getGXDate(7) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(7);
                ((int[]) buf[9])[0] = rslt.getInt(8) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(8);
                ((int[]) buf[11])[0] = rslt.getInt(9) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(9);
                ((String[]) buf[13])[0] = rslt.getString(10, 1) ;
                ((decimal[]) buf[14])[0] = rslt.getDecimal(11) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(11);
                ((decimal[]) buf[16])[0] = rslt.getDecimal(12) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(12);
                ((decimal[]) buf[18])[0] = rslt.getDecimal(13) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(13);
                ((bool[]) buf[20])[0] = rslt.getBool(14) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(14);
                ((String[]) buf[22])[0] = rslt.getVarchar(15) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(15);
                ((String[]) buf[24])[0] = rslt.getLongVarchar(16) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(16);
                ((DateTime[]) buf[26])[0] = rslt.getGXDate(17) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(17);
                ((DateTime[]) buf[28])[0] = rslt.getGXDate(18) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(18);
                ((short[]) buf[30])[0] = rslt.getShort(19) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(19);
                ((short[]) buf[32])[0] = rslt.getShort(20) ;
                ((bool[]) buf[33])[0] = rslt.wasNull(20);
                ((decimal[]) buf[34])[0] = rslt.getDecimal(21) ;
                ((bool[]) buf[35])[0] = rslt.wasNull(21);
                ((short[]) buf[36])[0] = rslt.getShort(22) ;
                ((bool[]) buf[37])[0] = rslt.wasNull(22);
                ((int[]) buf[38])[0] = rslt.getInt(23) ;
                ((bool[]) buf[39])[0] = rslt.wasNull(23);
                ((int[]) buf[40])[0] = rslt.getInt(24) ;
                ((bool[]) buf[41])[0] = rslt.wasNull(24);
                ((int[]) buf[42])[0] = rslt.getInt(25) ;
                ((bool[]) buf[43])[0] = rslt.wasNull(25);
                ((int[]) buf[44])[0] = rslt.getInt(26) ;
                ((bool[]) buf[45])[0] = rslt.wasNull(26);
                ((int[]) buf[46])[0] = rslt.getInt(27) ;
                ((bool[]) buf[47])[0] = rslt.wasNull(27);
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 15) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 1) ;
                ((String[]) buf[4])[0] = rslt.getLongVarchar(5) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(5);
                ((String[]) buf[6])[0] = rslt.getLongVarchar(6) ;
                ((DateTime[]) buf[7])[0] = rslt.getGXDate(7) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(7);
                ((int[]) buf[9])[0] = rslt.getInt(8) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(8);
                ((int[]) buf[11])[0] = rslt.getInt(9) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(9);
                ((String[]) buf[13])[0] = rslt.getString(10, 1) ;
                ((decimal[]) buf[14])[0] = rslt.getDecimal(11) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(11);
                ((decimal[]) buf[16])[0] = rslt.getDecimal(12) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(12);
                ((decimal[]) buf[18])[0] = rslt.getDecimal(13) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(13);
                ((bool[]) buf[20])[0] = rslt.getBool(14) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(14);
                ((String[]) buf[22])[0] = rslt.getVarchar(15) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(15);
                ((String[]) buf[24])[0] = rslt.getLongVarchar(16) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(16);
                ((DateTime[]) buf[26])[0] = rslt.getGXDate(17) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(17);
                ((DateTime[]) buf[28])[0] = rslt.getGXDate(18) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(18);
                ((short[]) buf[30])[0] = rslt.getShort(19) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(19);
                ((short[]) buf[32])[0] = rslt.getShort(20) ;
                ((bool[]) buf[33])[0] = rslt.wasNull(20);
                ((decimal[]) buf[34])[0] = rslt.getDecimal(21) ;
                ((bool[]) buf[35])[0] = rslt.wasNull(21);
                ((short[]) buf[36])[0] = rslt.getShort(22) ;
                ((bool[]) buf[37])[0] = rslt.wasNull(22);
                ((int[]) buf[38])[0] = rslt.getInt(23) ;
                ((bool[]) buf[39])[0] = rslt.wasNull(23);
                ((int[]) buf[40])[0] = rslt.getInt(24) ;
                ((bool[]) buf[41])[0] = rslt.wasNull(24);
                ((int[]) buf[42])[0] = rslt.getInt(25) ;
                ((bool[]) buf[43])[0] = rslt.wasNull(25);
                ((int[]) buf[44])[0] = rslt.getInt(26) ;
                ((bool[]) buf[45])[0] = rslt.wasNull(26);
                ((int[]) buf[46])[0] = rslt.getInt(27) ;
                ((bool[]) buf[47])[0] = rslt.wasNull(27);
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 6 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 7 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 8 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 25) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 9 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 15) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 10 :
                ((decimal[]) buf[0])[0] = rslt.getDecimal(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 15) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 1) ;
                ((String[]) buf[4])[0] = rslt.getLongVarchar(5) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(5);
                ((String[]) buf[6])[0] = rslt.getLongVarchar(6) ;
                ((DateTime[]) buf[7])[0] = rslt.getGXDate(7) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(7);
                ((int[]) buf[9])[0] = rslt.getInt(8) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(8);
                ((int[]) buf[11])[0] = rslt.getInt(9) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(9);
                ((String[]) buf[13])[0] = rslt.getString(10, 1) ;
                ((decimal[]) buf[14])[0] = rslt.getDecimal(11) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(11);
                ((decimal[]) buf[16])[0] = rslt.getDecimal(12) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(12);
                ((decimal[]) buf[18])[0] = rslt.getDecimal(13) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(13);
                ((bool[]) buf[20])[0] = rslt.getBool(14) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(14);
                ((String[]) buf[22])[0] = rslt.getVarchar(15) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(15);
                ((String[]) buf[24])[0] = rslt.getLongVarchar(16) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(16);
                ((DateTime[]) buf[26])[0] = rslt.getGXDate(17) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(17);
                ((DateTime[]) buf[28])[0] = rslt.getGXDate(18) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(18);
                ((short[]) buf[30])[0] = rslt.getShort(19) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(19);
                ((short[]) buf[32])[0] = rslt.getShort(20) ;
                ((bool[]) buf[33])[0] = rslt.wasNull(20);
                ((decimal[]) buf[34])[0] = rslt.getDecimal(21) ;
                ((bool[]) buf[35])[0] = rslt.wasNull(21);
                ((String[]) buf[36])[0] = rslt.getVarchar(22) ;
                ((bool[]) buf[37])[0] = rslt.wasNull(22);
                ((String[]) buf[38])[0] = rslt.getString(23, 50) ;
                ((bool[]) buf[39])[0] = rslt.wasNull(23);
                ((String[]) buf[40])[0] = rslt.getVarchar(24) ;
                ((bool[]) buf[41])[0] = rslt.wasNull(24);
                ((String[]) buf[42])[0] = rslt.getString(25, 25) ;
                ((bool[]) buf[43])[0] = rslt.wasNull(25);
                ((String[]) buf[44])[0] = rslt.getString(26, 50) ;
                ((bool[]) buf[45])[0] = rslt.wasNull(26);
                ((String[]) buf[46])[0] = rslt.getString(27, 15) ;
                ((bool[]) buf[47])[0] = rslt.wasNull(27);
                ((short[]) buf[48])[0] = rslt.getShort(28) ;
                ((bool[]) buf[49])[0] = rslt.wasNull(28);
                ((int[]) buf[50])[0] = rslt.getInt(29) ;
                ((bool[]) buf[51])[0] = rslt.wasNull(29);
                ((int[]) buf[52])[0] = rslt.getInt(30) ;
                ((bool[]) buf[53])[0] = rslt.wasNull(30);
                ((int[]) buf[54])[0] = rslt.getInt(31) ;
                ((bool[]) buf[55])[0] = rslt.wasNull(31);
                ((int[]) buf[56])[0] = rslt.getInt(32) ;
                ((bool[]) buf[57])[0] = rslt.wasNull(32);
                ((int[]) buf[58])[0] = rslt.getInt(33) ;
                ((bool[]) buf[59])[0] = rslt.wasNull(33);
                ((decimal[]) buf[60])[0] = rslt.getDecimal(34) ;
                ((short[]) buf[61])[0] = rslt.getShort(35) ;
                ((short[]) buf[62])[0] = rslt.getShort(36) ;
                ((bool[]) buf[63])[0] = rslt.wasNull(36);
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 13 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 16 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 17 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 18 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 25) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 19 :
                ((decimal[]) buf[0])[0] = rslt.getDecimal(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 20 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 15) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 21 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 15) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 1) ;
                ((short[]) buf[3])[0] = rslt.getShort(4) ;
                return;
             case 22 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 23 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 24 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 25 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 15) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 1) ;
                ((String[]) buf[4])[0] = rslt.getLongVarchar(5) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(5);
                ((String[]) buf[6])[0] = rslt.getLongVarchar(6) ;
                ((DateTime[]) buf[7])[0] = rslt.getGXDate(7) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(7);
                ((int[]) buf[9])[0] = rslt.getInt(8) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(8);
                ((int[]) buf[11])[0] = rslt.getInt(9) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(9);
                ((String[]) buf[13])[0] = rslt.getString(10, 1) ;
                ((decimal[]) buf[14])[0] = rslt.getDecimal(11) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(11);
                ((decimal[]) buf[16])[0] = rslt.getDecimal(12) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(12);
                ((decimal[]) buf[18])[0] = rslt.getDecimal(13) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(13);
                ((bool[]) buf[20])[0] = rslt.getBool(14) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(14);
                ((String[]) buf[22])[0] = rslt.getVarchar(15) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(15);
                ((String[]) buf[24])[0] = rslt.getLongVarchar(16) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(16);
                ((DateTime[]) buf[26])[0] = rslt.getGXDate(17) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(17);
                ((DateTime[]) buf[28])[0] = rslt.getGXDate(18) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(18);
                ((short[]) buf[30])[0] = rslt.getShort(19) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(19);
                ((short[]) buf[32])[0] = rslt.getShort(20) ;
                ((bool[]) buf[33])[0] = rslt.wasNull(20);
                ((decimal[]) buf[34])[0] = rslt.getDecimal(21) ;
                ((bool[]) buf[35])[0] = rslt.wasNull(21);
                ((String[]) buf[36])[0] = rslt.getVarchar(22) ;
                ((bool[]) buf[37])[0] = rslt.wasNull(22);
                ((String[]) buf[38])[0] = rslt.getString(23, 50) ;
                ((bool[]) buf[39])[0] = rslt.wasNull(23);
                ((String[]) buf[40])[0] = rslt.getVarchar(24) ;
                ((bool[]) buf[41])[0] = rslt.wasNull(24);
                ((String[]) buf[42])[0] = rslt.getString(25, 25) ;
                ((bool[]) buf[43])[0] = rslt.wasNull(25);
                ((String[]) buf[44])[0] = rslt.getString(26, 50) ;
                ((bool[]) buf[45])[0] = rslt.wasNull(26);
                ((String[]) buf[46])[0] = rslt.getString(27, 15) ;
                ((bool[]) buf[47])[0] = rslt.wasNull(27);
                ((short[]) buf[48])[0] = rslt.getShort(28) ;
                ((bool[]) buf[49])[0] = rslt.wasNull(28);
                ((int[]) buf[50])[0] = rslt.getInt(29) ;
                ((bool[]) buf[51])[0] = rslt.wasNull(29);
                ((int[]) buf[52])[0] = rslt.getInt(30) ;
                ((bool[]) buf[53])[0] = rslt.wasNull(30);
                ((int[]) buf[54])[0] = rslt.getInt(31) ;
                ((bool[]) buf[55])[0] = rslt.wasNull(31);
                ((int[]) buf[56])[0] = rslt.getInt(32) ;
                ((bool[]) buf[57])[0] = rslt.wasNull(32);
                ((int[]) buf[58])[0] = rslt.getInt(33) ;
                ((bool[]) buf[59])[0] = rslt.wasNull(33);
                ((decimal[]) buf[60])[0] = rslt.getDecimal(34) ;
                ((short[]) buf[61])[0] = rslt.getShort(35) ;
                ((short[]) buf[62])[0] = rslt.getShort(36) ;
                ((bool[]) buf[63])[0] = rslt.wasNull(36);
                return;
             case 26 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((String[]) buf[3])[0] = rslt.getLongVarchar(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((DateTime[]) buf[5])[0] = rslt.getGXDateTime(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((String[]) buf[7])[0] = rslt.getString(6, 50) ;
                ((String[]) buf[8])[0] = rslt.getString(7, 10) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(7);
                ((String[]) buf[10])[0] = rslt.getString(8, 50) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(8);
                ((int[]) buf[12])[0] = rslt.getInt(9) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(9);
                ((String[]) buf[14])[0] = rslt.getBLOBFile(10, rslt.getString(7, 10), rslt.getString(8, 50)) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(10);
                return;
             case 27 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 32 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                return;
             case 33 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((String[]) buf[3])[0] = rslt.getLongVarchar(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((DateTime[]) buf[5])[0] = rslt.getGXDateTime(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((String[]) buf[7])[0] = rslt.getString(6, 50) ;
                ((String[]) buf[8])[0] = rslt.getString(7, 10) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(7);
                ((String[]) buf[10])[0] = rslt.getString(8, 50) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(8);
                ((int[]) buf[12])[0] = rslt.getInt(9) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(9);
                ((String[]) buf[14])[0] = rslt.getBLOBFile(10, rslt.getString(7, 10), rslt.getString(8, 50)) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(10);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 6 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 7 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 8 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 9 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 10 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 13 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 4 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[4]);
                }
                stmt.SetParameter(5, (String)parms[5]);
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 6 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(6, (DateTime)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 7 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(7, (int)parms[9]);
                }
                if ( (bool)parms[10] )
                {
                   stmt.setNull( 8 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(8, (int)parms[11]);
                }
                stmt.SetParameter(9, (String)parms[12]);
                if ( (bool)parms[13] )
                {
                   stmt.setNull( 10 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(10, (decimal)parms[14]);
                }
                if ( (bool)parms[15] )
                {
                   stmt.setNull( 11 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(11, (decimal)parms[16]);
                }
                if ( (bool)parms[17] )
                {
                   stmt.setNull( 12 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(12, (decimal)parms[18]);
                }
                if ( (bool)parms[19] )
                {
                   stmt.setNull( 13 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(13, (bool)parms[20]);
                }
                if ( (bool)parms[21] )
                {
                   stmt.setNull( 14 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(14, (String)parms[22]);
                }
                if ( (bool)parms[23] )
                {
                   stmt.setNull( 15 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(15, (String)parms[24]);
                }
                if ( (bool)parms[25] )
                {
                   stmt.setNull( 16 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(16, (DateTime)parms[26]);
                }
                if ( (bool)parms[27] )
                {
                   stmt.setNull( 17 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(17, (DateTime)parms[28]);
                }
                if ( (bool)parms[29] )
                {
                   stmt.setNull( 18 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(18, (short)parms[30]);
                }
                if ( (bool)parms[31] )
                {
                   stmt.setNull( 19 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(19, (short)parms[32]);
                }
                if ( (bool)parms[33] )
                {
                   stmt.setNull( 20 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(20, (decimal)parms[34]);
                }
                if ( (bool)parms[35] )
                {
                   stmt.setNull( 21 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(21, (short)parms[36]);
                }
                if ( (bool)parms[37] )
                {
                   stmt.setNull( 22 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(22, (int)parms[38]);
                }
                if ( (bool)parms[39] )
                {
                   stmt.setNull( 23 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(23, (int)parms[40]);
                }
                if ( (bool)parms[41] )
                {
                   stmt.setNull( 24 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(24, (int)parms[42]);
                }
                if ( (bool)parms[43] )
                {
                   stmt.setNull( 25 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(25, (int)parms[44]);
                }
                if ( (bool)parms[45] )
                {
                   stmt.setNull( 26 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(26, (int)parms[46]);
                }
                return;
             case 14 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 4 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[4]);
                }
                stmt.SetParameter(5, (String)parms[5]);
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 6 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(6, (DateTime)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 7 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(7, (int)parms[9]);
                }
                if ( (bool)parms[10] )
                {
                   stmt.setNull( 8 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(8, (int)parms[11]);
                }
                stmt.SetParameter(9, (String)parms[12]);
                if ( (bool)parms[13] )
                {
                   stmt.setNull( 10 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(10, (decimal)parms[14]);
                }
                if ( (bool)parms[15] )
                {
                   stmt.setNull( 11 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(11, (decimal)parms[16]);
                }
                if ( (bool)parms[17] )
                {
                   stmt.setNull( 12 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(12, (decimal)parms[18]);
                }
                if ( (bool)parms[19] )
                {
                   stmt.setNull( 13 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(13, (bool)parms[20]);
                }
                if ( (bool)parms[21] )
                {
                   stmt.setNull( 14 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(14, (String)parms[22]);
                }
                if ( (bool)parms[23] )
                {
                   stmt.setNull( 15 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(15, (String)parms[24]);
                }
                if ( (bool)parms[25] )
                {
                   stmt.setNull( 16 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(16, (DateTime)parms[26]);
                }
                if ( (bool)parms[27] )
                {
                   stmt.setNull( 17 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(17, (DateTime)parms[28]);
                }
                if ( (bool)parms[29] )
                {
                   stmt.setNull( 18 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(18, (short)parms[30]);
                }
                if ( (bool)parms[31] )
                {
                   stmt.setNull( 19 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(19, (short)parms[32]);
                }
                if ( (bool)parms[33] )
                {
                   stmt.setNull( 20 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(20, (decimal)parms[34]);
                }
                if ( (bool)parms[35] )
                {
                   stmt.setNull( 21 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(21, (short)parms[36]);
                }
                if ( (bool)parms[37] )
                {
                   stmt.setNull( 22 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(22, (int)parms[38]);
                }
                if ( (bool)parms[39] )
                {
                   stmt.setNull( 23 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(23, (int)parms[40]);
                }
                if ( (bool)parms[41] )
                {
                   stmt.setNull( 24 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(24, (int)parms[42]);
                }
                if ( (bool)parms[43] )
                {
                   stmt.setNull( 25 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(25, (int)parms[44]);
                }
                if ( (bool)parms[45] )
                {
                   stmt.setNull( 26 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(26, (int)parms[46]);
                }
                stmt.SetParameter(27, (int)parms[47]);
                return;
             case 15 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 16 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 17 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 18 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 19 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 20 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 21 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 22 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 23 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 24 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 25 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 26 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 27 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 28 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 4 , SqlDbType.VarBinary );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 5 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(5, (String)parms[6]);
                }
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 6 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(6, (DateTime)parms[8]);
                }
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 7 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(7, (String)parms[10]);
                }
                if ( (bool)parms[11] )
                {
                   stmt.setNull( 8 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(8, (String)parms[12]);
                }
                if ( (bool)parms[13] )
                {
                   stmt.setNull( 9 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(9, (int)parms[14]);
                }
                return;
             case 29 :
                stmt.SetParameter(1, (String)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[2]);
                }
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 3 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(3, (DateTime)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 4 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[6]);
                }
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 5 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(5, (String)parms[8]);
                }
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 6 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(6, (int)parms[10]);
                }
                stmt.SetParameter(7, (int)parms[11]);
                stmt.SetParameter(8, (int)parms[12]);
                return;
       }
       setparameters30( cursor, stmt, parms) ;
    }

    public void setparameters30( int cursor ,
                                 IFieldSetter stmt ,
                                 Object[] parms )
    {
       switch ( cursor )
       {
             case 30 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.VarBinary );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                stmt.SetParameter(3, (int)parms[3]);
                return;
             case 31 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 32 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 33 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
