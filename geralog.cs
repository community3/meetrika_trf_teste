/*
               File: geralog
        Description: geralog
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/14/2020 23:56:51.47
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class geralog : GXProcedure
   {
      public geralog( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public geralog( IGxContext context )
      {
         this.context = context;
         IsMain = false;
      }

      public void release( )
      {
      }

      public void execute( ref String aP0_Txt )
      {
         this.AV10Txt = aP0_Txt;
         initialize();
         executePrivate();
         aP0_Txt=this.AV10Txt;
      }

      public String executeUdp( )
      {
         this.AV10Txt = aP0_Txt;
         initialize();
         executePrivate();
         aP0_Txt=this.AV10Txt;
         return AV10Txt ;
      }

      public void executeSubmit( ref String aP0_Txt )
      {
         geralog objgeralog;
         objgeralog = new geralog();
         objgeralog.AV10Txt = aP0_Txt;
         objgeralog.context.SetSubmitInitialConfig(context);
         objgeralog.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgeralog);
         aP0_Txt=this.AV10Txt;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((geralog)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         if ( StringUtil.Like( AV13HttpRequest.BaseURL , StringUtil.PadR( "http://localhost%" , 2048 , "%"),  ' ' ) )
         {
            AV14Arquivo = "";
            AV12Directory.Source = "D:\\Log";
            if ( AV12Directory.Exists() )
            {
               AV14Arquivo = "D:\\Log\\Log.txt";
            }
            else
            {
               AV12Directory.Source = "C:\\Log";
               AV14Arquivo = "C:\\Log\\Log.txt";
               if ( ! AV12Directory.Exists() )
               {
                  AV12Directory.Create();
               }
            }
            AV8append = 1;
            AV11TxtAuxiliar = context.localUtil.TToC( DateTimeUtil.ServerNow( context, "DEFAULT"), 10, 8, 0, 3, "/", ":", " ") + " " + StringUtil.Trim( AV10Txt);
            AV9FlagFile = context.FileIOInstance.dfwopen( AV14Arquivo, "", "", AV8append, "UTF8");
            if ( AV9FlagFile == 0 )
            {
               AV9FlagFile = context.FileIOInstance.dfwptxt( AV11TxtAuxiliar, 0);
               AV9FlagFile = context.FileIOInstance.dfwnext( );
               AV9FlagFile = context.FileIOInstance.dfwclose( );
            }
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV13HttpRequest = new GxHttpRequest( context);
         AV14Arquivo = "";
         AV12Directory = new GxDirectory(context.GetPhysicalPath());
         AV11TxtAuxiliar = "";
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV8append ;
      private short AV9FlagFile ;
      private String AV10Txt ;
      private String AV14Arquivo ;
      private String AV11TxtAuxiliar ;
      private String aP0_Txt ;
      private GxHttpRequest AV13HttpRequest ;
      private GxDirectory AV12Directory ;
   }

}
