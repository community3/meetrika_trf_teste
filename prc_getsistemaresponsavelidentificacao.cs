/*
               File: PRC_GetSistemaResponsavelIdentificacao
        Description: Busca a identifica��o do Respons�vel pelo Sistema
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:56:18.71
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_getsistemaresponsavelidentificacao : GXProcedure
   {
      public prc_getsistemaresponsavelidentificacao( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_getsistemaresponsavelidentificacao( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Sistema_Codigo ,
                           int aP1_AreaTrabalho_Codigo ,
                           out String aP2_Usuario_PessoaNom )
      {
         this.A127Sistema_Codigo = aP0_Sistema_Codigo;
         this.A5AreaTrabalho_Codigo = aP1_AreaTrabalho_Codigo;
         this.AV9Usuario_PessoaNom = "" ;
         initialize();
         executePrivate();
         aP2_Usuario_PessoaNom=this.AV9Usuario_PessoaNom;
      }

      public String executeUdp( int aP0_Sistema_Codigo ,
                                int aP1_AreaTrabalho_Codigo )
      {
         this.A127Sistema_Codigo = aP0_Sistema_Codigo;
         this.A5AreaTrabalho_Codigo = aP1_AreaTrabalho_Codigo;
         this.AV9Usuario_PessoaNom = "" ;
         initialize();
         executePrivate();
         aP2_Usuario_PessoaNom=this.AV9Usuario_PessoaNom;
         return AV9Usuario_PessoaNom ;
      }

      public void executeSubmit( int aP0_Sistema_Codigo ,
                                 int aP1_AreaTrabalho_Codigo ,
                                 out String aP2_Usuario_PessoaNom )
      {
         prc_getsistemaresponsavelidentificacao objprc_getsistemaresponsavelidentificacao;
         objprc_getsistemaresponsavelidentificacao = new prc_getsistemaresponsavelidentificacao();
         objprc_getsistemaresponsavelidentificacao.A127Sistema_Codigo = aP0_Sistema_Codigo;
         objprc_getsistemaresponsavelidentificacao.A5AreaTrabalho_Codigo = aP1_AreaTrabalho_Codigo;
         objprc_getsistemaresponsavelidentificacao.AV9Usuario_PessoaNom = "" ;
         objprc_getsistemaresponsavelidentificacao.context.SetSubmitInitialConfig(context);
         objprc_getsistemaresponsavelidentificacao.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_getsistemaresponsavelidentificacao);
         aP2_Usuario_PessoaNom=this.AV9Usuario_PessoaNom;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_getsistemaresponsavelidentificacao)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV9Usuario_PessoaNom = "N�o Identificado";
         AV12Sistema_Responsavel = 0;
         AV11Pessoa_Codigo = 0;
         /* Using cursor P00XZ2 */
         pr_default.execute(0, new Object[] {A127Sistema_Codigo, A5AreaTrabalho_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A135Sistema_AreaTrabalhoCod = P00XZ2_A135Sistema_AreaTrabalhoCod[0];
            A1831Sistema_Responsavel = P00XZ2_A1831Sistema_Responsavel[0];
            n1831Sistema_Responsavel = P00XZ2_n1831Sistema_Responsavel[0];
            AV12Sistema_Responsavel = A1831Sistema_Responsavel;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         /* Using cursor P00XZ3 */
         pr_default.execute(1, new Object[] {n1831Sistema_Responsavel, A1831Sistema_Responsavel});
         while ( (pr_default.getStatus(1) != 101) )
         {
            A1Usuario_Codigo = P00XZ3_A1Usuario_Codigo[0];
            A57Usuario_PessoaCod = P00XZ3_A57Usuario_PessoaCod[0];
            AV11Pessoa_Codigo = A57Usuario_PessoaCod;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(1);
         /* Using cursor P00XZ4 */
         pr_default.execute(2, new Object[] {AV11Pessoa_Codigo});
         while ( (pr_default.getStatus(2) != 101) )
         {
            A34Pessoa_Codigo = P00XZ4_A34Pessoa_Codigo[0];
            A35Pessoa_Nome = P00XZ4_A35Pessoa_Nome[0];
            AV9Usuario_PessoaNom = A35Pessoa_Nome;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(2);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00XZ2_A127Sistema_Codigo = new int[1] ;
         P00XZ2_A135Sistema_AreaTrabalhoCod = new int[1] ;
         P00XZ2_A1831Sistema_Responsavel = new int[1] ;
         P00XZ2_n1831Sistema_Responsavel = new bool[] {false} ;
         P00XZ3_A1Usuario_Codigo = new int[1] ;
         P00XZ3_A57Usuario_PessoaCod = new int[1] ;
         P00XZ4_A34Pessoa_Codigo = new int[1] ;
         P00XZ4_A35Pessoa_Nome = new String[] {""} ;
         A35Pessoa_Nome = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_getsistemaresponsavelidentificacao__default(),
            new Object[][] {
                new Object[] {
               P00XZ2_A127Sistema_Codigo, P00XZ2_A135Sistema_AreaTrabalhoCod, P00XZ2_A1831Sistema_Responsavel, P00XZ2_n1831Sistema_Responsavel
               }
               , new Object[] {
               P00XZ3_A1Usuario_Codigo, P00XZ3_A57Usuario_PessoaCod
               }
               , new Object[] {
               P00XZ4_A34Pessoa_Codigo, P00XZ4_A35Pessoa_Nome
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A127Sistema_Codigo ;
      private int A5AreaTrabalho_Codigo ;
      private int AV12Sistema_Responsavel ;
      private int AV11Pessoa_Codigo ;
      private int A135Sistema_AreaTrabalhoCod ;
      private int A1831Sistema_Responsavel ;
      private int A1Usuario_Codigo ;
      private int A57Usuario_PessoaCod ;
      private int A34Pessoa_Codigo ;
      private String AV9Usuario_PessoaNom ;
      private String scmdbuf ;
      private String A35Pessoa_Nome ;
      private bool n1831Sistema_Responsavel ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00XZ2_A127Sistema_Codigo ;
      private int[] P00XZ2_A135Sistema_AreaTrabalhoCod ;
      private int[] P00XZ2_A1831Sistema_Responsavel ;
      private bool[] P00XZ2_n1831Sistema_Responsavel ;
      private int[] P00XZ3_A1Usuario_Codigo ;
      private int[] P00XZ3_A57Usuario_PessoaCod ;
      private int[] P00XZ4_A34Pessoa_Codigo ;
      private String[] P00XZ4_A35Pessoa_Nome ;
      private String aP2_Usuario_PessoaNom ;
   }

   public class prc_getsistemaresponsavelidentificacao__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00XZ2 ;
          prmP00XZ2 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00XZ3 ;
          prmP00XZ3 = new Object[] {
          new Object[] {"@Sistema_Responsavel",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00XZ4 ;
          prmP00XZ4 = new Object[] {
          new Object[] {"@AV11Pessoa_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00XZ2", "SELECT TOP 1 [Sistema_Codigo], [Sistema_AreaTrabalhoCod], [Sistema_Responsavel] FROM [Sistema] WITH (NOLOCK) WHERE ([Sistema_Codigo] = @Sistema_Codigo) AND ([Sistema_AreaTrabalhoCod] = @AreaTrabalho_Codigo) ORDER BY [Sistema_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00XZ2,1,0,false,true )
             ,new CursorDef("P00XZ3", "SELECT TOP 1 [Usuario_Codigo], [Usuario_PessoaCod] FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @Sistema_Responsavel ORDER BY [Usuario_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00XZ3,1,0,false,true )
             ,new CursorDef("P00XZ4", "SELECT TOP 1 [Pessoa_Codigo], [Pessoa_Nome] FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @AV11Pessoa_Codigo ORDER BY [Pessoa_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00XZ4,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 100) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 1 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
