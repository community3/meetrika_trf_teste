/*
               File: GAMExampleEntryConnection
        Description: Connection
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 23:11:54.11
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class gamexampleentryconnection : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public gamexampleentryconnection( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("GeneXusXEv2");
      }

      public gamexampleentryconnection( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref String aP0_Gx_mode ,
                           ref String aP1_pConnectionName )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV14pConnectionName = aP1_pConnectionName;
         executePrivate();
         aP0_Gx_mode=this.Gx_mode;
         aP1_pConnectionName=this.AV14pConnectionName;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("GeneXusXEv2");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               Gx_mode = gxfirstwebparm;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV14pConnectionName = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14pConnectionName", AV14pConnectionName);
               }
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("gammasterpagepopup", "GeneXus.Programs.gammasterpagepopup", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PA2F2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            START2F2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202042823115481");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("gamexampleentryconnection.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode(StringUtil.RTrim(AV14pConnectionName))+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "vPCONNECTIONNAME", StringUtil.RTrim( AV14pConnectionName));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         context.WriteHtmlTextNl( "</form>") ;
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WE2F2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVT2F2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("gamexampleentryconnection.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode(StringUtil.RTrim(AV14pConnectionName)) ;
      }

      public override String GetPgmname( )
      {
         return "GAMExampleEntryConnection" ;
      }

      public override String GetPgmdesc( )
      {
         return "Connection" ;
      }

      protected void WB2F0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            wb_table1_3_2F2( true) ;
         }
         else
         {
            wb_table1_3_2F2( false) ;
         }
         return  ;
      }

      protected void wb_table1_3_2F2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavConnectionfilexml_Internalname, StringUtil.RTrim( AV7ConnectionFileXML), "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,38);\"", 0, edtavConnectionfilexml_Visible, 1, 0, 110, "chr", 10, "row", StyleString, ClassString, "", "2048", 1, "", "", -1, true, "", "HLP_GAMExampleEntryConnection.htm");
            wb_table2_39_2F2( true) ;
         }
         else
         {
            wb_table2_39_2F2( false) ;
         }
         return  ;
      }

      protected void wb_table2_39_2F2e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void START2F2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Connection", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUP2F0( ) ;
      }

      protected void WS2F2( )
      {
         START2F2( ) ;
         EVT2F2( ) ;
      }

      protected void EVT2F2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E112F2 */
                              E112F2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ! wbErr )
                              {
                                 Rfr0gs = false;
                                 if ( ! Rfr0gs )
                                 {
                                    /* Execute user event: E122F2 */
                                    E122F2 ();
                                 }
                                 dynload_actions( ) ;
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'CLOSE'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E132F2 */
                              E132F2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'GENERATEKEY'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E142F2 */
                              E142F2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E152F2 */
                              E152F2 ();
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE2F2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PA2F2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( toggleJsOutput )
            {
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavConnectionname_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF2F2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      protected void RF2F2( )
      {
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Execute user event: E152F2 */
            E152F2 ();
            WB2F0( ) ;
         }
      }

      protected void STRUP2F0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E112F2 */
         E112F2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            AV8ConnectionName = cgiGet( edtavConnectionname_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8ConnectionName", AV8ConnectionName);
            AV17UserName = cgiGet( edtavUsername_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17UserName", AV17UserName);
            AV18UserPassword = cgiGet( edtavUserpassword_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18UserPassword", AV18UserPassword);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavChallengeexpire_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavChallengeexpire_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCHALLENGEEXPIRE");
               GX_FocusControl = edtavChallengeexpire_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV5ChallengeExpire = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5ChallengeExpire", StringUtil.LTrim( StringUtil.Str( (decimal)(AV5ChallengeExpire), 6, 0)));
            }
            else
            {
               AV5ChallengeExpire = (int)(context.localUtil.CToN( cgiGet( edtavChallengeexpire_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5ChallengeExpire", StringUtil.LTrim( StringUtil.Str( (decimal)(AV5ChallengeExpire), 6, 0)));
            }
            AV12Key = cgiGet( edtavKey_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12Key", AV12Key);
            AV7ConnectionFileXML = cgiGet( edtavConnectionfilexml_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7ConnectionFileXML", AV7ConnectionFileXML);
            /* Read saved values. */
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E112F2 */
         E112F2 ();
         if (returnInSub) return;
      }

      protected void E112F2( )
      {
         /* Start Routine */
         AV8ConnectionName = AV14pConnectionName;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8ConnectionName", AV8ConnectionName);
         GX_FocusControl = edtavConnectionname_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         context.DoAjaxSetFocus(GX_FocusControl);
         lblTbtxtxml_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbtxtxml_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTbtxtxml_Visible), 5, 0)));
         edtavConnectionfilexml_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavConnectionfilexml_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavConnectionfilexml_Visible), 5, 0)));
         edtavConnectionname_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavConnectionname_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavConnectionname_Enabled), 5, 0)));
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            edtavConnectionname_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavConnectionname_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavConnectionname_Enabled), 5, 0)));
            edtavUsername_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUsername_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUsername_Enabled), 5, 0)));
            lblTbuserpwd_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbuserpwd_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTbuserpwd_Visible), 5, 0)));
            edtavUserpassword_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUserpassword_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUserpassword_Visible), 5, 0)));
         }
         else
         {
            AV6Connection.load( AV8ConnectionName);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8ConnectionName", AV8ConnectionName);
            AV17UserName = AV6Connection.gxTpr_Username;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17UserName", AV17UserName);
            AV18UserPassword = AV6Connection.gxTpr_Userpassword;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18UserPassword", AV18UserPassword);
            AV5ChallengeExpire = AV6Connection.gxTpr_Challengeexpire;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5ChallengeExpire", StringUtil.LTrim( StringUtil.Str( (decimal)(AV5ChallengeExpire), 6, 0)));
            AV12Key = AV6Connection.gxTpr_Key;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12Key", AV12Key);
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "XML") == 0 ) )
         {
            bttBtnconfirm_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnconfirm_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnconfirm_Visible), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "XML") == 0 ) )
         {
            bttBtngenkey_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtngenkey_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtngenkey_Visible), 5, 0)));
            lblTbuserpwd_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbuserpwd_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTbuserpwd_Visible), 5, 0)));
            edtavUserpassword_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUserpassword_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUserpassword_Visible), 5, 0)));
            edtavConnectionname_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavConnectionname_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavConnectionname_Enabled), 5, 0)));
            edtavChallengeexpire_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavChallengeexpire_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavChallengeexpire_Enabled), 5, 0)));
            edtavUsername_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUsername_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUsername_Enabled), 5, 0)));
            edtavKey_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavKey_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavKey_Enabled), 5, 0)));
            bttBtnconfirm_Caption = "Delete";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnconfirm_Internalname, "Caption", bttBtnconfirm_Caption);
         }
         if ( StringUtil.StrCmp(Gx_mode, "XML") == 0 )
         {
            lblTbtxtxml_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbtxtxml_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTbtxtxml_Visible), 5, 0)));
            edtavConnectionfilexml_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavConnectionfilexml_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavConnectionfilexml_Visible), 5, 0)));
            AV11isOk = new SdtGAMRepository(context).generateconnectionfile(AV8ConnectionName, out  AV7ConnectionFileXML, out  AV10Errors);
            if ( ! AV11isOk )
            {
               /* Execute user subroutine: 'DISPLAYMESSAGES' */
               S112 ();
               if (returnInSub) return;
            }
         }
      }

      public void GXEnter( )
      {
         /* Execute user event: E122F2 */
         E122F2 ();
         if (returnInSub) return;
      }

      protected void E122F2( )
      {
         /* Enter Routine */
         AV6Connection.gxTpr_Name = AV8ConnectionName;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV6Connection", AV6Connection);
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) )
         {
            AV6Connection.gxTpr_Username = AV17UserName;
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV6Connection", AV6Connection);
            AV6Connection.gxTpr_Userpassword = AV18UserPassword;
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV6Connection", AV6Connection);
            AV6Connection.gxTpr_Challengeexpire = AV5ChallengeExpire;
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV6Connection", AV6Connection);
            AV6Connection.gxTpr_Key = AV12Key;
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV6Connection", AV6Connection);
            AV6Connection.save();
         }
         else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
         {
            AV6Connection.delete();
         }
         if ( AV6Connection.success() )
         {
            context.CommitDataStores( "GAMExampleEntryConnection");
            context.setWebReturnParms(new Object[] {(String)Gx_mode,(String)AV14pConnectionName});
            context.wjLocDisableFrm = 1;
            context.nUserReturn = 1;
            returnInSub = true;
            if (true) return;
         }
         else
         {
            AV10Errors = AV6Connection.geterrors();
            /* Execute user subroutine: 'DISPLAYMESSAGES' */
            S112 ();
            if (returnInSub) return;
         }
      }

      protected void E132F2( )
      {
         /* 'Close' Routine */
         context.setWebReturnParms(new Object[] {(String)Gx_mode,(String)AV14pConnectionName});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void E142F2( )
      {
         /* 'GenerateKey' Routine */
         AV12Key = Crypto.GetEncryptionKey( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12Key", AV12Key);
      }

      protected void S112( )
      {
         /* 'DISPLAYMESSAGES' Routine */
         AV23GXV1 = 1;
         while ( AV23GXV1 <= AV10Errors.Count )
         {
            AV9Error = ((SdtGAMError)AV10Errors.Item(AV23GXV1));
            if ( AV9Error.gxTpr_Code != 13 )
            {
               GX_msglist.addItem(StringUtil.Format( "%1 (GAM%2)", AV9Error.gxTpr_Message, StringUtil.LTrim( StringUtil.Str( (decimal)(AV9Error.gxTpr_Code), 12, 0)), "", "", "", "", "", "", ""));
            }
            AV23GXV1 = (int)(AV23GXV1+1);
         }
      }

      protected void nextLoad( )
      {
      }

      protected void E152F2( )
      {
         /* Load Routine */
      }

      protected void wb_table2_39_2F2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(400), 10, 0)) + "px" + ";";
            GxWebStd.gx_table_start( context, tblTable2_Internalname, tblTable2_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "width:200px")+"\">") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnconfirm_Internalname, "", bttBtnconfirm_Caption, bttBtnconfirm_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtnconfirm_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_GAMExampleEntryConnection.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 47,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnclose_Internalname, "", "Close", bttBtnclose_Jsonclick, 5, "Close", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'CLOSE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_GAMExampleEntryConnection.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_39_2F2e( true) ;
         }
         else
         {
            wb_table2_39_2F2e( false) ;
         }
      }

      protected void wb_table1_3_2F2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "width:200px")+"\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbconnname_Internalname, "Connection name", "", "", lblTbconnname_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleEntryConnection.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "width:300px")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 8,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavConnectionname_Internalname, StringUtil.RTrim( AV8ConnectionName), StringUtil.RTrim( context.localUtil.Format( AV8ConnectionName, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,8);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavConnectionname_Jsonclick, 0, "Attribute", "", "", "", 1, edtavConnectionname_Enabled, 1, "text", "", 421, "px", 1, "row", 254, 0, 0, 0, 1, -1, -1, true, "GAMDescriptionLong", "left", true, "HLP_GAMExampleEntryConnection.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbusername_Internalname, "User name", "", "", lblTbusername_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleEntryConnection.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 13,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavUsername_Internalname, StringUtil.RTrim( AV17UserName), StringUtil.RTrim( context.localUtil.Format( AV17UserName, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,13);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavUsername_Jsonclick, 0, "Attribute", "", "", "", 1, edtavUsername_Enabled, 1, "text", "", 421, "px", 1, "row", 254, 0, 0, 0, 1, -1, -1, true, "GAMDescriptionLong", "left", true, "HLP_GAMExampleEntryConnection.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "height:21px")+"\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbuserpwd_Internalname, "User password", "", "", lblTbuserpwd_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", lblTbuserpwd_Visible, 1, 0, "HLP_GAMExampleEntryConnection.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavUserpassword_Internalname, StringUtil.RTrim( AV18UserPassword), StringUtil.RTrim( context.localUtil.Format( AV18UserPassword, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,18);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavUserpassword_Jsonclick, 0, "Attribute", "", "", "", edtavUserpassword_Visible, 1, 0, "text", "", 421, "px", 1, "row", 254, -1, 0, 0, 1, 0, -1, true, "GAMDescriptionLong", "left", true, "HLP_GAMExampleEntryConnection.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "height:21px")+"\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbchallangeexpire_Internalname, "Connection challenge expire (minutes)", "", "", lblTbchallangeexpire_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleEntryConnection.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavChallengeexpire_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV5ChallengeExpire), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV5ChallengeExpire), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,23);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavChallengeexpire_Jsonclick, 0, "Attribute", "", "", "", 1, edtavChallengeexpire_Enabled, 1, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_GAMExampleEntryConnection.htm");
            context.WriteHtmlText( "&nbsp;") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbchallangeexpiredsc_Internalname, " (0 = never expires)", "", "", lblTbchallangeexpiredsc_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleEntryConnection.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbenckey_Internalname, "Encription key", "", "", lblTbenckey_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleEntryConnection.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 29,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavKey_Internalname, StringUtil.RTrim( AV12Key), StringUtil.RTrim( context.localUtil.Format( AV12Key, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,29);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavKey_Jsonclick, 0, "Attribute", "", "", "", 1, edtavKey_Enabled, 1, "text", "", 45, "chr", 1, "row", 32, 0, 0, 0, 1, -1, 0, true, "GAMEncryptionKey", "left", true, "HLP_GAMExampleEntryConnection.htm");
            context.WriteHtmlText( "&nbsp;") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 30,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtngenkey_Internalname, "", "Generate Key", bttBtngenkey_Jsonclick, 5, "Generate Key", "", StyleString, ClassString, bttBtngenkey_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'GENERATEKEY\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_GAMExampleEntryConnection.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;height:20px")+"\">") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbtxtxml_Internalname, "Content of conection.gam:", "", "", lblTbtxtxml_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", lblTbtxtxml_Visible, 1, 0, "HLP_GAMExampleEntryConnection.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "&nbsp;&nbsp;&nbsp;&nbsp; ") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_3_2F2e( true) ;
         }
         else
         {
            wb_table1_3_2F2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         Gx_mode = (String)getParm(obj,0);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         AV14pConnectionName = (String)getParm(obj,1);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14pConnectionName", AV14pConnectionName);
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("GeneXusXEv2");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA2F2( ) ;
         WS2F2( ) ;
         WE2F2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?2249785");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202042823115614");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gamexampleentryconnection.js", "?202042823115615");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTbconnname_Internalname = "TBCONNNAME";
         edtavConnectionname_Internalname = "vCONNECTIONNAME";
         lblTbusername_Internalname = "TBUSERNAME";
         edtavUsername_Internalname = "vUSERNAME";
         lblTbuserpwd_Internalname = "TBUSERPWD";
         edtavUserpassword_Internalname = "vUSERPASSWORD";
         lblTbchallangeexpire_Internalname = "TBCHALLANGEEXPIRE";
         edtavChallengeexpire_Internalname = "vCHALLENGEEXPIRE";
         lblTbchallangeexpiredsc_Internalname = "TBCHALLANGEEXPIREDSC";
         lblTbenckey_Internalname = "TBENCKEY";
         edtavKey_Internalname = "vKEY";
         bttBtngenkey_Internalname = "BTNGENKEY";
         lblTbtxtxml_Internalname = "TBTXTXML";
         tblTable1_Internalname = "TABLE1";
         edtavConnectionfilexml_Internalname = "vCONNECTIONFILEXML";
         bttBtnconfirm_Internalname = "BTNCONFIRM";
         bttBtnclose_Internalname = "BTNCLOSE";
         tblTable2_Internalname = "TABLE2";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         init_default_properties( ) ;
         lblTbtxtxml_Visible = 1;
         bttBtngenkey_Visible = 1;
         edtavKey_Jsonclick = "";
         edtavChallengeexpire_Jsonclick = "";
         edtavUserpassword_Jsonclick = "";
         lblTbuserpwd_Visible = 1;
         edtavUsername_Jsonclick = "";
         edtavConnectionname_Jsonclick = "";
         bttBtnconfirm_Visible = 1;
         bttBtnconfirm_Caption = "Confirmar";
         edtavKey_Enabled = 1;
         edtavChallengeexpire_Enabled = 1;
         edtavUserpassword_Visible = 1;
         edtavUsername_Enabled = 1;
         edtavConnectionname_Enabled = 1;
         edtavConnectionfilexml_Visible = 1;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Connection";
         context.GX_msglist.DisplayMode = 1;
      }

      protected override bool IsSpaSupported( )
      {
         return false ;
      }

      public override void InitializeDynEvents( )
      {
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         wcpOGx_mode = "";
         wcpOAV14pConnectionName = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         AV7ConnectionFileXML = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV8ConnectionName = "";
         AV17UserName = "";
         AV18UserPassword = "";
         AV12Key = "";
         AV6Connection = new SdtGAMRepositoryConnection(context);
         AV10Errors = new GxExternalCollection( context, "SdtGAMError", "GeneXus.Programs");
         AV9Error = new SdtGAMError(context);
         sStyleString = "";
         bttBtnconfirm_Jsonclick = "";
         bttBtnclose_Jsonclick = "";
         lblTbconnname_Jsonclick = "";
         lblTbusername_Jsonclick = "";
         lblTbuserpwd_Jsonclick = "";
         lblTbchallangeexpire_Jsonclick = "";
         lblTbchallangeexpiredsc_Jsonclick = "";
         lblTbenckey_Jsonclick = "";
         bttBtngenkey_Jsonclick = "";
         lblTbtxtxml_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.gamexampleentryconnection__default(),
            new Object[][] {
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXWrapped ;
      private int edtavConnectionfilexml_Visible ;
      private int AV5ChallengeExpire ;
      private int lblTbtxtxml_Visible ;
      private int edtavConnectionname_Enabled ;
      private int edtavUsername_Enabled ;
      private int lblTbuserpwd_Visible ;
      private int edtavUserpassword_Visible ;
      private int bttBtnconfirm_Visible ;
      private int bttBtngenkey_Visible ;
      private int edtavChallengeexpire_Enabled ;
      private int edtavKey_Enabled ;
      private int AV23GXV1 ;
      private int idxLst ;
      private String Gx_mode ;
      private String AV14pConnectionName ;
      private String wcpOGx_mode ;
      private String wcpOAV14pConnectionName ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String ClassString ;
      private String StyleString ;
      private String TempTags ;
      private String edtavConnectionfilexml_Internalname ;
      private String AV7ConnectionFileXML ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavConnectionname_Internalname ;
      private String AV8ConnectionName ;
      private String AV17UserName ;
      private String edtavUsername_Internalname ;
      private String AV18UserPassword ;
      private String edtavUserpassword_Internalname ;
      private String edtavChallengeexpire_Internalname ;
      private String AV12Key ;
      private String edtavKey_Internalname ;
      private String lblTbtxtxml_Internalname ;
      private String lblTbuserpwd_Internalname ;
      private String bttBtnconfirm_Internalname ;
      private String bttBtngenkey_Internalname ;
      private String bttBtnconfirm_Caption ;
      private String sStyleString ;
      private String tblTable2_Internalname ;
      private String bttBtnconfirm_Jsonclick ;
      private String bttBtnclose_Internalname ;
      private String bttBtnclose_Jsonclick ;
      private String tblTable1_Internalname ;
      private String lblTbconnname_Internalname ;
      private String lblTbconnname_Jsonclick ;
      private String edtavConnectionname_Jsonclick ;
      private String lblTbusername_Internalname ;
      private String lblTbusername_Jsonclick ;
      private String edtavUsername_Jsonclick ;
      private String lblTbuserpwd_Jsonclick ;
      private String edtavUserpassword_Jsonclick ;
      private String lblTbchallangeexpire_Internalname ;
      private String lblTbchallangeexpire_Jsonclick ;
      private String edtavChallengeexpire_Jsonclick ;
      private String lblTbchallangeexpiredsc_Internalname ;
      private String lblTbchallangeexpiredsc_Jsonclick ;
      private String lblTbenckey_Internalname ;
      private String lblTbenckey_Jsonclick ;
      private String edtavKey_Jsonclick ;
      private String bttBtngenkey_Jsonclick ;
      private String lblTbtxtxml_Jsonclick ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private bool AV11isOk ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private String aP0_Gx_mode ;
      private String aP1_pConnectionName ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private IDataStoreProvider pr_default ;
      [ObjectCollection(ItemType=typeof( SdtGAMError ))]
      private IGxCollection AV10Errors ;
      private GXWebForm Form ;
      private SdtGAMRepositoryConnection AV6Connection ;
      private SdtGAMError AV9Error ;
   }

   public class gamexampleentryconnection__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          def= new CursorDef[] {
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
       }
    }

 }

}
