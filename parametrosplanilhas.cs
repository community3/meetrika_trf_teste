/*
               File: ParametrosPlanilhas
        Description: Par�metros das Planilhas
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/6/2020 23:55:39.71
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class parametrosplanilhas : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"PARAMETROSPLN_AREATRABALHOCOD") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GXDLAPARAMETROSPLN_AREATRABALHOCOD2O104( ) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_10") == 0 )
         {
            A847ParametrosPln_AreaTrabalhoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n847ParametrosPln_AreaTrabalhoCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A847ParametrosPln_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A847ParametrosPln_AreaTrabalhoCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_10( A847ParametrosPln_AreaTrabalhoCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV7ParametrosPln_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7ParametrosPln_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ParametrosPln_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPARAMETROSPLN_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7ParametrosPln_Codigo), "ZZZZZ9")));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         dynParametrosPln_AreaTrabalhoCod.Name = "PARAMETROSPLN_AREATRABALHOCOD";
         dynParametrosPln_AreaTrabalhoCod.WebTags = "";
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Par�metros das Planilhas", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = dynParametrosPln_AreaTrabalhoCod_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public parametrosplanilhas( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public parametrosplanilhas( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           int aP1_ParametrosPln_Codigo )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV7ParametrosPln_Codigo = aP1_ParametrosPln_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         dynParametrosPln_AreaTrabalhoCod = new GXCombobox();
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("workwithplusbootstrapmasterpage", "GeneXus.Programs.workwithplusbootstrapmasterpage", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
         if ( dynParametrosPln_AreaTrabalhoCod.ItemCount > 0 )
         {
            A847ParametrosPln_AreaTrabalhoCod = (int)(NumberUtil.Val( dynParametrosPln_AreaTrabalhoCod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A847ParametrosPln_AreaTrabalhoCod), 6, 0))), "."));
            n847ParametrosPln_AreaTrabalhoCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A847ParametrosPln_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A847ParametrosPln_AreaTrabalhoCod), 6, 0)));
         }
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_2O104( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_2O104e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtParametrosPln_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A848ParametrosPln_Codigo), 6, 0, ",", "")), ((edtParametrosPln_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A848ParametrosPln_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A848ParametrosPln_Codigo), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtParametrosPln_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtParametrosPln_Codigo_Visible, edtParametrosPln_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ParametrosPlanilhas.htm");
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_2O104( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_2O104( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_2O104e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_41_2O104( true) ;
         }
         return  ;
      }

      protected void wb_table3_41_2O104e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_2O104e( true) ;
         }
         else
         {
            wb_table1_2_2O104e( false) ;
         }
      }

      protected void wb_table3_41_2O104( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", "Confirmar", bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_trn_enter_Visible, bttBtn_trn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_ParametrosPlanilhas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "", "Fechar", bttBtn_trn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_trn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_ParametrosPlanilhas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 48,'',false,'',0)\"";
            ClassString = "btn btn-danger";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_delete_Internalname, "", "Eliminar", bttBtn_trn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_trn_delete_Visible, bttBtn_trn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_ParametrosPlanilhas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_41_2O104e( true) ;
         }
         else
         {
            wb_table3_41_2O104e( false) ;
         }
      }

      protected void wb_table2_5_2O104( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table4_13_2O104( true) ;
         }
         return  ;
      }

      protected void wb_table4_13_2O104e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_2O104e( true) ;
         }
         else
         {
            wb_table2_5_2O104e( false) ;
         }
      }

      protected void wb_table4_13_2O104( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableData", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockparametrospln_areatrabalhocod_Internalname, "�rea de Trabalho", "", "", lblTextblockparametrospln_areatrabalhocod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ParametrosPlanilhas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynParametrosPln_AreaTrabalhoCod, dynParametrosPln_AreaTrabalhoCod_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A847ParametrosPln_AreaTrabalhoCod), 6, 0)), 1, dynParametrosPln_AreaTrabalhoCod_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, dynParametrosPln_AreaTrabalhoCod.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,18);\"", "", true, "HLP_ParametrosPlanilhas.htm");
            dynParametrosPln_AreaTrabalhoCod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A847ParametrosPln_AreaTrabalhoCod), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynParametrosPln_AreaTrabalhoCod_Internalname, "Values", (String)(dynParametrosPln_AreaTrabalhoCod.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockparametrospln_aba_Internalname, "Aba", "", "", lblTextblockparametrospln_aba_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ParametrosPlanilhas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtParametrosPln_Aba_Internalname, StringUtil.RTrim( A2015ParametrosPln_Aba), StringUtil.RTrim( context.localUtil.Format( A2015ParametrosPln_Aba, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,23);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtParametrosPln_Aba_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtParametrosPln_Aba_Enabled, 0, "text", "", 30, "chr", 1, "row", 30, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ParametrosPlanilhas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockparametrospln_campo_Internalname, "Campo", "", "", lblTextblockparametrospln_campo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ParametrosPlanilhas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 28,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtParametrosPln_Campo_Internalname, A849ParametrosPln_Campo, StringUtil.RTrim( context.localUtil.Format( A849ParametrosPln_Campo, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,28);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtParametrosPln_Campo_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtParametrosPln_Campo_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Descricao", "left", true, "HLP_ParametrosPlanilhas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockparametrospln_coluna_Internalname, "Coluna", "", "", lblTextblockparametrospln_coluna_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ParametrosPlanilhas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtParametrosPln_Coluna_Internalname, StringUtil.RTrim( A850ParametrosPln_Coluna), StringUtil.RTrim( context.localUtil.Format( A850ParametrosPln_Coluna, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,33);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtParametrosPln_Coluna_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtParametrosPln_Coluna_Enabled, 0, "text", "", 30, "px", 1, "row", 2, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ParametrosPlanilhas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockparametrospln_linha_Internalname, "Linha", "", "", lblTextblockparametrospln_linha_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ParametrosPlanilhas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtParametrosPln_Linha_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A851ParametrosPln_Linha), 6, 0, ",", "")), ((edtParametrosPln_Linha_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A851ParametrosPln_Linha), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A851ParametrosPln_Linha), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,38);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtParametrosPln_Linha_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtParametrosPln_Linha_Enabled, 0, "text", "", 70, "px", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ParametrosPlanilhas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_13_2O104e( true) ;
         }
         else
         {
            wb_table4_13_2O104e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E112O2 */
         E112O2 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               dynParametrosPln_AreaTrabalhoCod.CurrentValue = cgiGet( dynParametrosPln_AreaTrabalhoCod_Internalname);
               A847ParametrosPln_AreaTrabalhoCod = (int)(NumberUtil.Val( cgiGet( dynParametrosPln_AreaTrabalhoCod_Internalname), "."));
               n847ParametrosPln_AreaTrabalhoCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A847ParametrosPln_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A847ParametrosPln_AreaTrabalhoCod), 6, 0)));
               n847ParametrosPln_AreaTrabalhoCod = ((0==A847ParametrosPln_AreaTrabalhoCod) ? true : false);
               A2015ParametrosPln_Aba = cgiGet( edtParametrosPln_Aba_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2015ParametrosPln_Aba", A2015ParametrosPln_Aba);
               A849ParametrosPln_Campo = StringUtil.Upper( cgiGet( edtParametrosPln_Campo_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A849ParametrosPln_Campo", A849ParametrosPln_Campo);
               A850ParametrosPln_Coluna = StringUtil.Upper( cgiGet( edtParametrosPln_Coluna_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A850ParametrosPln_Coluna", A850ParametrosPln_Coluna);
               if ( ( ( context.localUtil.CToN( cgiGet( edtParametrosPln_Linha_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtParametrosPln_Linha_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "PARAMETROSPLN_LINHA");
                  AnyError = 1;
                  GX_FocusControl = edtParametrosPln_Linha_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A851ParametrosPln_Linha = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A851ParametrosPln_Linha", StringUtil.LTrim( StringUtil.Str( (decimal)(A851ParametrosPln_Linha), 6, 0)));
               }
               else
               {
                  A851ParametrosPln_Linha = (int)(context.localUtil.CToN( cgiGet( edtParametrosPln_Linha_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A851ParametrosPln_Linha", StringUtil.LTrim( StringUtil.Str( (decimal)(A851ParametrosPln_Linha), 6, 0)));
               }
               A848ParametrosPln_Codigo = (int)(context.localUtil.CToN( cgiGet( edtParametrosPln_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A848ParametrosPln_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A848ParametrosPln_Codigo), 6, 0)));
               /* Read saved values. */
               Z848ParametrosPln_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z848ParametrosPln_Codigo"), ",", "."));
               Z2015ParametrosPln_Aba = cgiGet( "Z2015ParametrosPln_Aba");
               Z849ParametrosPln_Campo = cgiGet( "Z849ParametrosPln_Campo");
               Z850ParametrosPln_Coluna = cgiGet( "Z850ParametrosPln_Coluna");
               Z851ParametrosPln_Linha = (int)(context.localUtil.CToN( cgiGet( "Z851ParametrosPln_Linha"), ",", "."));
               Z847ParametrosPln_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( "Z847ParametrosPln_AreaTrabalhoCod"), ",", "."));
               n847ParametrosPln_AreaTrabalhoCod = ((0==A847ParametrosPln_AreaTrabalhoCod) ? true : false);
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               N847ParametrosPln_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( "N847ParametrosPln_AreaTrabalhoCod"), ",", "."));
               n847ParametrosPln_AreaTrabalhoCod = ((0==A847ParametrosPln_AreaTrabalhoCod) ? true : false);
               AV7ParametrosPln_Codigo = (int)(context.localUtil.CToN( cgiGet( "vPARAMETROSPLN_CODIGO"), ",", "."));
               AV11Insert_ParametrosPln_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( "vINSERT_PARAMETROSPLN_AREATRABALHOCOD"), ",", "."));
               AV13Pgmname = cgiGet( "vPGMNAME");
               Gx_mode = cgiGet( "vMODE");
               Dvpanel_tableattributes_Width = cgiGet( "DVPANEL_TABLEATTRIBUTES_Width");
               Dvpanel_tableattributes_Height = cgiGet( "DVPANEL_TABLEATTRIBUTES_Height");
               Dvpanel_tableattributes_Cls = cgiGet( "DVPANEL_TABLEATTRIBUTES_Cls");
               Dvpanel_tableattributes_Title = cgiGet( "DVPANEL_TABLEATTRIBUTES_Title");
               Dvpanel_tableattributes_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsible"));
               Dvpanel_tableattributes_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsed"));
               Dvpanel_tableattributes_Enabled = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Enabled"));
               Dvpanel_tableattributes_Class = cgiGet( "DVPANEL_TABLEATTRIBUTES_Class");
               Dvpanel_tableattributes_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autowidth"));
               Dvpanel_tableattributes_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoheight"));
               Dvpanel_tableattributes_Showheader = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showheader"));
               Dvpanel_tableattributes_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showcollapseicon"));
               Dvpanel_tableattributes_Iconposition = cgiGet( "DVPANEL_TABLEATTRIBUTES_Iconposition");
               Dvpanel_tableattributes_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoscroll"));
               Dvpanel_tableattributes_Visible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Visible"));
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "ParametrosPlanilhas";
               A848ParametrosPln_Codigo = (int)(context.localUtil.CToN( cgiGet( edtParametrosPln_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A848ParametrosPln_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A848ParametrosPln_Codigo), 6, 0)));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A848ParametrosPln_Codigo), "ZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               hsh = cgiGet( "hsh");
               if ( ( ! ( ( A848ParametrosPln_Codigo != Z848ParametrosPln_Codigo ) ) || ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ) && ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("parametrosplanilhas:[SecurityCheckFailed value for]"+"ParametrosPln_Codigo:"+context.localUtil.Format( (decimal)(A848ParametrosPln_Codigo), "ZZZZZ9"));
                  GXUtil.WriteLog("parametrosplanilhas:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  A848ParametrosPln_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A848ParametrosPln_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A848ParametrosPln_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode104 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     Gx_mode = sMode104;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound104 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_2O0( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_trn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "PARAMETROSPLN_CODIGO");
                        AnyError = 1;
                        GX_FocusControl = edtParametrosPln_Codigo_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E112O2 */
                           E112O2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E122O2 */
                           E122O2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E122O2 */
            E122O2 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll2O104( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         bttBtn_trn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtn_trn_delete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_trn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
            }
            DisableAttributes2O104( ) ;
         }
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_2O0( )
      {
         BeforeValidate2O104( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls2O104( ) ;
            }
            else
            {
               CheckExtendedTable2O104( ) ;
               CloseExtendedTableCursors2O104( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         }
      }

      protected void ResetCaption2O0( )
      {
      }

      protected void E112O2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
         if ( ( StringUtil.StrCmp(AV9TrnContext.gxTpr_Transactionname, AV13Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV14GXV1 = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14GXV1), 8, 0)));
            while ( AV14GXV1 <= AV9TrnContext.gxTpr_Attributes.Count )
            {
               AV12TrnContextAtt = ((wwpbaseobjects.SdtWWPTransactionContext_Attribute)AV9TrnContext.gxTpr_Attributes.Item(AV14GXV1));
               if ( StringUtil.StrCmp(AV12TrnContextAtt.gxTpr_Attributename, "ParametrosPln_AreaTrabalhoCod") == 0 )
               {
                  AV11Insert_ParametrosPln_AreaTrabalhoCod = (int)(NumberUtil.Val( AV12TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Insert_ParametrosPln_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Insert_ParametrosPln_AreaTrabalhoCod), 6, 0)));
               }
               AV14GXV1 = (int)(AV14GXV1+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14GXV1), 8, 0)));
            }
         }
         edtParametrosPln_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtParametrosPln_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtParametrosPln_Codigo_Visible), 5, 0)));
      }

      protected void E122O2( )
      {
         /* After Trn Routine */
         if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV9TrnContext.gxTpr_Callerondelete )
         {
            context.wjLoc = formatLink("wwparametrosplanilhas.aspx") ;
            context.wjLocDisableFrm = 1;
         }
         context.setWebReturnParms(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void ZM2O104( short GX_JID )
      {
         if ( ( GX_JID == 9 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z2015ParametrosPln_Aba = T002O3_A2015ParametrosPln_Aba[0];
               Z849ParametrosPln_Campo = T002O3_A849ParametrosPln_Campo[0];
               Z850ParametrosPln_Coluna = T002O3_A850ParametrosPln_Coluna[0];
               Z851ParametrosPln_Linha = T002O3_A851ParametrosPln_Linha[0];
               Z847ParametrosPln_AreaTrabalhoCod = T002O3_A847ParametrosPln_AreaTrabalhoCod[0];
            }
            else
            {
               Z2015ParametrosPln_Aba = A2015ParametrosPln_Aba;
               Z849ParametrosPln_Campo = A849ParametrosPln_Campo;
               Z850ParametrosPln_Coluna = A850ParametrosPln_Coluna;
               Z851ParametrosPln_Linha = A851ParametrosPln_Linha;
               Z847ParametrosPln_AreaTrabalhoCod = A847ParametrosPln_AreaTrabalhoCod;
            }
         }
         if ( GX_JID == -9 )
         {
            Z848ParametrosPln_Codigo = A848ParametrosPln_Codigo;
            Z2015ParametrosPln_Aba = A2015ParametrosPln_Aba;
            Z849ParametrosPln_Campo = A849ParametrosPln_Campo;
            Z850ParametrosPln_Coluna = A850ParametrosPln_Coluna;
            Z851ParametrosPln_Linha = A851ParametrosPln_Linha;
            Z847ParametrosPln_AreaTrabalhoCod = A847ParametrosPln_AreaTrabalhoCod;
         }
      }

      protected void standaloneNotModal( )
      {
         GXAPARAMETROSPLN_AREATRABALHOCOD_html2O104( ) ;
         edtParametrosPln_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtParametrosPln_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtParametrosPln_Codigo_Enabled), 5, 0)));
         AV13Pgmname = "ParametrosPlanilhas";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Pgmname", AV13Pgmname);
         edtParametrosPln_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtParametrosPln_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtParametrosPln_Codigo_Enabled), 5, 0)));
         bttBtn_trn_delete_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         if ( ! (0==AV7ParametrosPln_Codigo) )
         {
            A848ParametrosPln_Codigo = AV7ParametrosPln_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A848ParametrosPln_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A848ParametrosPln_Codigo), 6, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_ParametrosPln_AreaTrabalhoCod) )
         {
            dynParametrosPln_AreaTrabalhoCod.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynParametrosPln_AreaTrabalhoCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynParametrosPln_AreaTrabalhoCod.Enabled), 5, 0)));
         }
         else
         {
            dynParametrosPln_AreaTrabalhoCod.Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynParametrosPln_AreaTrabalhoCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynParametrosPln_AreaTrabalhoCod.Enabled), 5, 0)));
         }
      }

      protected void standaloneModal( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_ParametrosPln_AreaTrabalhoCod) )
         {
            A847ParametrosPln_AreaTrabalhoCod = AV11Insert_ParametrosPln_AreaTrabalhoCod;
            n847ParametrosPln_AreaTrabalhoCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A847ParametrosPln_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A847ParametrosPln_AreaTrabalhoCod), 6, 0)));
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
      }

      protected void Load2O104( )
      {
         /* Using cursor T002O5 */
         pr_default.execute(3, new Object[] {A848ParametrosPln_Codigo});
         if ( (pr_default.getStatus(3) != 101) )
         {
            RcdFound104 = 1;
            A2015ParametrosPln_Aba = T002O5_A2015ParametrosPln_Aba[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2015ParametrosPln_Aba", A2015ParametrosPln_Aba);
            A849ParametrosPln_Campo = T002O5_A849ParametrosPln_Campo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A849ParametrosPln_Campo", A849ParametrosPln_Campo);
            A850ParametrosPln_Coluna = T002O5_A850ParametrosPln_Coluna[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A850ParametrosPln_Coluna", A850ParametrosPln_Coluna);
            A851ParametrosPln_Linha = T002O5_A851ParametrosPln_Linha[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A851ParametrosPln_Linha", StringUtil.LTrim( StringUtil.Str( (decimal)(A851ParametrosPln_Linha), 6, 0)));
            A847ParametrosPln_AreaTrabalhoCod = T002O5_A847ParametrosPln_AreaTrabalhoCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A847ParametrosPln_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A847ParametrosPln_AreaTrabalhoCod), 6, 0)));
            n847ParametrosPln_AreaTrabalhoCod = T002O5_n847ParametrosPln_AreaTrabalhoCod[0];
            ZM2O104( -9) ;
         }
         pr_default.close(3);
         OnLoadActions2O104( ) ;
      }

      protected void OnLoadActions2O104( )
      {
      }

      protected void CheckExtendedTable2O104( )
      {
         Gx_BScreen = 1;
         standaloneModal( ) ;
         /* Using cursor T002O4 */
         pr_default.execute(2, new Object[] {n847ParametrosPln_AreaTrabalhoCod, A847ParametrosPln_AreaTrabalhoCod});
         if ( (pr_default.getStatus(2) == 101) )
         {
            if ( ! ( (0==A847ParametrosPln_AreaTrabalhoCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Parametros Planiljas_Area Trabalho'.", "ForeignKeyNotFound", 1, "PARAMETROSPLN_AREATRABALHOCOD");
               AnyError = 1;
               GX_FocusControl = dynParametrosPln_AreaTrabalhoCod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         pr_default.close(2);
      }

      protected void CloseExtendedTableCursors2O104( )
      {
         pr_default.close(2);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_10( int A847ParametrosPln_AreaTrabalhoCod )
      {
         /* Using cursor T002O6 */
         pr_default.execute(4, new Object[] {n847ParametrosPln_AreaTrabalhoCod, A847ParametrosPln_AreaTrabalhoCod});
         if ( (pr_default.getStatus(4) == 101) )
         {
            if ( ! ( (0==A847ParametrosPln_AreaTrabalhoCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Parametros Planiljas_Area Trabalho'.", "ForeignKeyNotFound", 1, "PARAMETROSPLN_AREATRABALHOCOD");
               AnyError = 1;
               GX_FocusControl = dynParametrosPln_AreaTrabalhoCod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(4) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(4);
      }

      protected void GetKey2O104( )
      {
         /* Using cursor T002O7 */
         pr_default.execute(5, new Object[] {A848ParametrosPln_Codigo});
         if ( (pr_default.getStatus(5) != 101) )
         {
            RcdFound104 = 1;
         }
         else
         {
            RcdFound104 = 0;
         }
         pr_default.close(5);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T002O3 */
         pr_default.execute(1, new Object[] {A848ParametrosPln_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM2O104( 9) ;
            RcdFound104 = 1;
            A848ParametrosPln_Codigo = T002O3_A848ParametrosPln_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A848ParametrosPln_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A848ParametrosPln_Codigo), 6, 0)));
            A2015ParametrosPln_Aba = T002O3_A2015ParametrosPln_Aba[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2015ParametrosPln_Aba", A2015ParametrosPln_Aba);
            A849ParametrosPln_Campo = T002O3_A849ParametrosPln_Campo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A849ParametrosPln_Campo", A849ParametrosPln_Campo);
            A850ParametrosPln_Coluna = T002O3_A850ParametrosPln_Coluna[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A850ParametrosPln_Coluna", A850ParametrosPln_Coluna);
            A851ParametrosPln_Linha = T002O3_A851ParametrosPln_Linha[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A851ParametrosPln_Linha", StringUtil.LTrim( StringUtil.Str( (decimal)(A851ParametrosPln_Linha), 6, 0)));
            A847ParametrosPln_AreaTrabalhoCod = T002O3_A847ParametrosPln_AreaTrabalhoCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A847ParametrosPln_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A847ParametrosPln_AreaTrabalhoCod), 6, 0)));
            n847ParametrosPln_AreaTrabalhoCod = T002O3_n847ParametrosPln_AreaTrabalhoCod[0];
            Z848ParametrosPln_Codigo = A848ParametrosPln_Codigo;
            sMode104 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load2O104( ) ;
            if ( AnyError == 1 )
            {
               RcdFound104 = 0;
               InitializeNonKey2O104( ) ;
            }
            Gx_mode = sMode104;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound104 = 0;
            InitializeNonKey2O104( ) ;
            sMode104 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal( ) ;
            Gx_mode = sMode104;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey2O104( ) ;
         if ( RcdFound104 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound104 = 0;
         /* Using cursor T002O8 */
         pr_default.execute(6, new Object[] {A848ParametrosPln_Codigo});
         if ( (pr_default.getStatus(6) != 101) )
         {
            while ( (pr_default.getStatus(6) != 101) && ( ( T002O8_A848ParametrosPln_Codigo[0] < A848ParametrosPln_Codigo ) ) )
            {
               pr_default.readNext(6);
            }
            if ( (pr_default.getStatus(6) != 101) && ( ( T002O8_A848ParametrosPln_Codigo[0] > A848ParametrosPln_Codigo ) ) )
            {
               A848ParametrosPln_Codigo = T002O8_A848ParametrosPln_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A848ParametrosPln_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A848ParametrosPln_Codigo), 6, 0)));
               RcdFound104 = 1;
            }
         }
         pr_default.close(6);
      }

      protected void move_previous( )
      {
         RcdFound104 = 0;
         /* Using cursor T002O9 */
         pr_default.execute(7, new Object[] {A848ParametrosPln_Codigo});
         if ( (pr_default.getStatus(7) != 101) )
         {
            while ( (pr_default.getStatus(7) != 101) && ( ( T002O9_A848ParametrosPln_Codigo[0] > A848ParametrosPln_Codigo ) ) )
            {
               pr_default.readNext(7);
            }
            if ( (pr_default.getStatus(7) != 101) && ( ( T002O9_A848ParametrosPln_Codigo[0] < A848ParametrosPln_Codigo ) ) )
            {
               A848ParametrosPln_Codigo = T002O9_A848ParametrosPln_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A848ParametrosPln_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A848ParametrosPln_Codigo), 6, 0)));
               RcdFound104 = 1;
            }
         }
         pr_default.close(7);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey2O104( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = dynParametrosPln_AreaTrabalhoCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert2O104( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound104 == 1 )
            {
               if ( A848ParametrosPln_Codigo != Z848ParametrosPln_Codigo )
               {
                  A848ParametrosPln_Codigo = Z848ParametrosPln_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A848ParametrosPln_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A848ParametrosPln_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "PARAMETROSPLN_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtParametrosPln_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = dynParametrosPln_AreaTrabalhoCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  Update2O104( ) ;
                  GX_FocusControl = dynParametrosPln_AreaTrabalhoCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A848ParametrosPln_Codigo != Z848ParametrosPln_Codigo )
               {
                  /* Insert record */
                  GX_FocusControl = dynParametrosPln_AreaTrabalhoCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert2O104( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "PARAMETROSPLN_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtParametrosPln_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     /* Insert record */
                     GX_FocusControl = dynParametrosPln_AreaTrabalhoCod_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert2O104( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( A848ParametrosPln_Codigo != Z848ParametrosPln_Codigo )
         {
            A848ParametrosPln_Codigo = Z848ParametrosPln_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A848ParametrosPln_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A848ParametrosPln_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "PARAMETROSPLN_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtParametrosPln_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = dynParametrosPln_AreaTrabalhoCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency2O104( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T002O2 */
            pr_default.execute(0, new Object[] {A848ParametrosPln_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ParametrosPlanilhas"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z2015ParametrosPln_Aba, T002O2_A2015ParametrosPln_Aba[0]) != 0 ) || ( StringUtil.StrCmp(Z849ParametrosPln_Campo, T002O2_A849ParametrosPln_Campo[0]) != 0 ) || ( StringUtil.StrCmp(Z850ParametrosPln_Coluna, T002O2_A850ParametrosPln_Coluna[0]) != 0 ) || ( Z851ParametrosPln_Linha != T002O2_A851ParametrosPln_Linha[0] ) || ( Z847ParametrosPln_AreaTrabalhoCod != T002O2_A847ParametrosPln_AreaTrabalhoCod[0] ) )
            {
               if ( StringUtil.StrCmp(Z2015ParametrosPln_Aba, T002O2_A2015ParametrosPln_Aba[0]) != 0 )
               {
                  GXUtil.WriteLog("parametrosplanilhas:[seudo value changed for attri]"+"ParametrosPln_Aba");
                  GXUtil.WriteLogRaw("Old: ",Z2015ParametrosPln_Aba);
                  GXUtil.WriteLogRaw("Current: ",T002O2_A2015ParametrosPln_Aba[0]);
               }
               if ( StringUtil.StrCmp(Z849ParametrosPln_Campo, T002O2_A849ParametrosPln_Campo[0]) != 0 )
               {
                  GXUtil.WriteLog("parametrosplanilhas:[seudo value changed for attri]"+"ParametrosPln_Campo");
                  GXUtil.WriteLogRaw("Old: ",Z849ParametrosPln_Campo);
                  GXUtil.WriteLogRaw("Current: ",T002O2_A849ParametrosPln_Campo[0]);
               }
               if ( StringUtil.StrCmp(Z850ParametrosPln_Coluna, T002O2_A850ParametrosPln_Coluna[0]) != 0 )
               {
                  GXUtil.WriteLog("parametrosplanilhas:[seudo value changed for attri]"+"ParametrosPln_Coluna");
                  GXUtil.WriteLogRaw("Old: ",Z850ParametrosPln_Coluna);
                  GXUtil.WriteLogRaw("Current: ",T002O2_A850ParametrosPln_Coluna[0]);
               }
               if ( Z851ParametrosPln_Linha != T002O2_A851ParametrosPln_Linha[0] )
               {
                  GXUtil.WriteLog("parametrosplanilhas:[seudo value changed for attri]"+"ParametrosPln_Linha");
                  GXUtil.WriteLogRaw("Old: ",Z851ParametrosPln_Linha);
                  GXUtil.WriteLogRaw("Current: ",T002O2_A851ParametrosPln_Linha[0]);
               }
               if ( Z847ParametrosPln_AreaTrabalhoCod != T002O2_A847ParametrosPln_AreaTrabalhoCod[0] )
               {
                  GXUtil.WriteLog("parametrosplanilhas:[seudo value changed for attri]"+"ParametrosPln_AreaTrabalhoCod");
                  GXUtil.WriteLogRaw("Old: ",Z847ParametrosPln_AreaTrabalhoCod);
                  GXUtil.WriteLogRaw("Current: ",T002O2_A847ParametrosPln_AreaTrabalhoCod[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"ParametrosPlanilhas"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert2O104( )
      {
         BeforeValidate2O104( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable2O104( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM2O104( 0) ;
            CheckOptimisticConcurrency2O104( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm2O104( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert2O104( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T002O10 */
                     pr_default.execute(8, new Object[] {A2015ParametrosPln_Aba, A849ParametrosPln_Campo, A850ParametrosPln_Coluna, A851ParametrosPln_Linha, n847ParametrosPln_AreaTrabalhoCod, A847ParametrosPln_AreaTrabalhoCod});
                     A848ParametrosPln_Codigo = T002O10_A848ParametrosPln_Codigo[0];
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A848ParametrosPln_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A848ParametrosPln_Codigo), 6, 0)));
                     pr_default.close(8);
                     dsDefault.SmartCacheProvider.SetUpdated("ParametrosPlanilhas") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption2O0( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load2O104( ) ;
            }
            EndLevel2O104( ) ;
         }
         CloseExtendedTableCursors2O104( ) ;
      }

      protected void Update2O104( )
      {
         BeforeValidate2O104( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable2O104( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency2O104( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm2O104( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate2O104( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T002O11 */
                     pr_default.execute(9, new Object[] {A2015ParametrosPln_Aba, A849ParametrosPln_Campo, A850ParametrosPln_Coluna, A851ParametrosPln_Linha, n847ParametrosPln_AreaTrabalhoCod, A847ParametrosPln_AreaTrabalhoCod, A848ParametrosPln_Codigo});
                     pr_default.close(9);
                     dsDefault.SmartCacheProvider.SetUpdated("ParametrosPlanilhas") ;
                     if ( (pr_default.getStatus(9) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ParametrosPlanilhas"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate2O104( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel2O104( ) ;
         }
         CloseExtendedTableCursors2O104( ) ;
      }

      protected void DeferredUpdate2O104( )
      {
      }

      protected void delete( )
      {
         BeforeValidate2O104( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency2O104( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls2O104( ) ;
            AfterConfirm2O104( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete2O104( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T002O12 */
                  pr_default.execute(10, new Object[] {A848ParametrosPln_Codigo});
                  pr_default.close(10);
                  dsDefault.SmartCacheProvider.SetUpdated("ParametrosPlanilhas") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                        {
                           if ( AnyError == 0 )
                           {
                              context.nUserReturn = 1;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode104 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel2O104( ) ;
         Gx_mode = sMode104;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls2O104( )
      {
         standaloneModal( ) ;
         /* No delete mode formulas found. */
      }

      protected void EndLevel2O104( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete2O104( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            context.CommitDataStores( "ParametrosPlanilhas");
            if ( AnyError == 0 )
            {
               ConfirmValues2O0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            context.RollbackDataStores( "ParametrosPlanilhas");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart2O104( )
      {
         /* Scan By routine */
         /* Using cursor T002O13 */
         pr_default.execute(11);
         RcdFound104 = 0;
         if ( (pr_default.getStatus(11) != 101) )
         {
            RcdFound104 = 1;
            A848ParametrosPln_Codigo = T002O13_A848ParametrosPln_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A848ParametrosPln_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A848ParametrosPln_Codigo), 6, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext2O104( )
      {
         /* Scan next routine */
         pr_default.readNext(11);
         RcdFound104 = 0;
         if ( (pr_default.getStatus(11) != 101) )
         {
            RcdFound104 = 1;
            A848ParametrosPln_Codigo = T002O13_A848ParametrosPln_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A848ParametrosPln_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A848ParametrosPln_Codigo), 6, 0)));
         }
      }

      protected void ScanEnd2O104( )
      {
         pr_default.close(11);
      }

      protected void AfterConfirm2O104( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert2O104( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate2O104( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete2O104( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete2O104( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate2O104( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes2O104( )
      {
         dynParametrosPln_AreaTrabalhoCod.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynParametrosPln_AreaTrabalhoCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynParametrosPln_AreaTrabalhoCod.Enabled), 5, 0)));
         edtParametrosPln_Aba_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtParametrosPln_Aba_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtParametrosPln_Aba_Enabled), 5, 0)));
         edtParametrosPln_Campo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtParametrosPln_Campo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtParametrosPln_Campo_Enabled), 5, 0)));
         edtParametrosPln_Coluna_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtParametrosPln_Coluna_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtParametrosPln_Coluna_Enabled), 5, 0)));
         edtParametrosPln_Linha_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtParametrosPln_Linha_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtParametrosPln_Linha_Enabled), 5, 0)));
         edtParametrosPln_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtParametrosPln_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtParametrosPln_Codigo_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues2O0( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20205623554065");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("parametrosplanilhas.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7ParametrosPln_Codigo)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z848ParametrosPln_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z848ParametrosPln_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z2015ParametrosPln_Aba", StringUtil.RTrim( Z2015ParametrosPln_Aba));
         GxWebStd.gx_hidden_field( context, "Z849ParametrosPln_Campo", Z849ParametrosPln_Campo);
         GxWebStd.gx_hidden_field( context, "Z850ParametrosPln_Coluna", StringUtil.RTrim( Z850ParametrosPln_Coluna));
         GxWebStd.gx_hidden_field( context, "Z851ParametrosPln_Linha", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z851ParametrosPln_Linha), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z847ParametrosPln_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z847ParametrosPln_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "N847ParametrosPln_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(A847ParametrosPln_AreaTrabalhoCod), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV9TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV9TrnContext);
         }
         GxWebStd.gx_hidden_field( context, "vPARAMETROSPLN_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7ParametrosPln_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_PARAMETROSPLN_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV11Insert_ParametrosPln_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV13Pgmname));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vPARAMETROSPLN_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7ParametrosPln_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Width", StringUtil.RTrim( Dvpanel_tableattributes_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Cls", StringUtil.RTrim( Dvpanel_tableattributes_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Title", StringUtil.RTrim( Dvpanel_tableattributes_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsible", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsed", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Enabled", StringUtil.BoolToStr( Dvpanel_tableattributes_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autowidth", StringUtil.BoolToStr( Dvpanel_tableattributes_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoheight", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tableattributes_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Iconposition", StringUtil.RTrim( Dvpanel_tableattributes_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoscroll", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoscroll));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "ParametrosPlanilhas";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A848ParametrosPln_Codigo), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("parametrosplanilhas:[SendSecurityCheck value for]"+"ParametrosPln_Codigo:"+context.localUtil.Format( (decimal)(A848ParametrosPln_Codigo), "ZZZZZ9"));
         GXUtil.WriteLog("parametrosplanilhas:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("parametrosplanilhas.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7ParametrosPln_Codigo) ;
      }

      public override String GetPgmname( )
      {
         return "ParametrosPlanilhas" ;
      }

      public override String GetPgmdesc( )
      {
         return "Par�metros das Planilhas" ;
      }

      protected void InitializeNonKey2O104( )
      {
         A847ParametrosPln_AreaTrabalhoCod = 0;
         n847ParametrosPln_AreaTrabalhoCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A847ParametrosPln_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A847ParametrosPln_AreaTrabalhoCod), 6, 0)));
         n847ParametrosPln_AreaTrabalhoCod = ((0==A847ParametrosPln_AreaTrabalhoCod) ? true : false);
         A2015ParametrosPln_Aba = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2015ParametrosPln_Aba", A2015ParametrosPln_Aba);
         A849ParametrosPln_Campo = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A849ParametrosPln_Campo", A849ParametrosPln_Campo);
         A850ParametrosPln_Coluna = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A850ParametrosPln_Coluna", A850ParametrosPln_Coluna);
         A851ParametrosPln_Linha = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A851ParametrosPln_Linha", StringUtil.LTrim( StringUtil.Str( (decimal)(A851ParametrosPln_Linha), 6, 0)));
         Z2015ParametrosPln_Aba = "";
         Z849ParametrosPln_Campo = "";
         Z850ParametrosPln_Coluna = "";
         Z851ParametrosPln_Linha = 0;
         Z847ParametrosPln_AreaTrabalhoCod = 0;
      }

      protected void InitAll2O104( )
      {
         A848ParametrosPln_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A848ParametrosPln_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A848ParametrosPln_Codigo), 6, 0)));
         InitializeNonKey2O104( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20205623554083");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("parametrosplanilhas.js", "?20205623554083");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockparametrospln_areatrabalhocod_Internalname = "TEXTBLOCKPARAMETROSPLN_AREATRABALHOCOD";
         dynParametrosPln_AreaTrabalhoCod_Internalname = "PARAMETROSPLN_AREATRABALHOCOD";
         lblTextblockparametrospln_aba_Internalname = "TEXTBLOCKPARAMETROSPLN_ABA";
         edtParametrosPln_Aba_Internalname = "PARAMETROSPLN_ABA";
         lblTextblockparametrospln_campo_Internalname = "TEXTBLOCKPARAMETROSPLN_CAMPO";
         edtParametrosPln_Campo_Internalname = "PARAMETROSPLN_CAMPO";
         lblTextblockparametrospln_coluna_Internalname = "TEXTBLOCKPARAMETROSPLN_COLUNA";
         edtParametrosPln_Coluna_Internalname = "PARAMETROSPLN_COLUNA";
         lblTextblockparametrospln_linha_Internalname = "TEXTBLOCKPARAMETROSPLN_LINHA";
         edtParametrosPln_Linha_Internalname = "PARAMETROSPLN_LINHA";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         Dvpanel_tableattributes_Internalname = "DVPANEL_TABLEATTRIBUTES";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         bttBtn_trn_delete_Internalname = "BTN_TRN_DELETE";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         edtParametrosPln_Codigo_Internalname = "PARAMETROSPLN_CODIGO";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Dvpanel_tableattributes_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Iconposition = "left";
         Dvpanel_tableattributes_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tableattributes_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Title = "Par�metros das Planilhas";
         Dvpanel_tableattributes_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tableattributes_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Par�metros das Planilhas";
         edtParametrosPln_Linha_Jsonclick = "";
         edtParametrosPln_Linha_Enabled = 1;
         edtParametrosPln_Coluna_Jsonclick = "";
         edtParametrosPln_Coluna_Enabled = 1;
         edtParametrosPln_Campo_Jsonclick = "";
         edtParametrosPln_Campo_Enabled = 1;
         edtParametrosPln_Aba_Jsonclick = "";
         edtParametrosPln_Aba_Enabled = 1;
         dynParametrosPln_AreaTrabalhoCod_Jsonclick = "";
         dynParametrosPln_AreaTrabalhoCod.Enabled = 1;
         bttBtn_trn_delete_Enabled = 0;
         bttBtn_trn_delete_Visible = 1;
         bttBtn_trn_cancel_Visible = 1;
         bttBtn_trn_enter_Enabled = 1;
         bttBtn_trn_enter_Visible = 1;
         edtParametrosPln_Codigo_Jsonclick = "";
         edtParametrosPln_Codigo_Enabled = 0;
         edtParametrosPln_Codigo_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLAPARAMETROSPLN_AREATRABALHOCOD2O104( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLAPARAMETROSPLN_AREATRABALHOCOD_data2O104( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXAPARAMETROSPLN_AREATRABALHOCOD_html2O104( )
      {
         int gxdynajaxvalue ;
         GXDLAPARAMETROSPLN_AREATRABALHOCOD_data2O104( ) ;
         gxdynajaxindex = 1;
         dynParametrosPln_AreaTrabalhoCod.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynParametrosPln_AreaTrabalhoCod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLAPARAMETROSPLN_AREATRABALHOCOD_data2O104( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("Todas");
         /* Using cursor T002O14 */
         pr_default.execute(12);
         while ( (pr_default.getStatus(12) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(T002O14_A847ParametrosPln_AreaTrabalhoCod[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(T002O14_A6AreaTrabalho_Descricao[0]);
            pr_default.readNext(12);
         }
         pr_default.close(12);
      }

      public void Valid_Parametrospln_areatrabalhocod( GXCombobox dynGX_Parm1 )
      {
         dynParametrosPln_AreaTrabalhoCod = dynGX_Parm1;
         A847ParametrosPln_AreaTrabalhoCod = (int)(NumberUtil.Val( dynParametrosPln_AreaTrabalhoCod.CurrentValue, "."));
         n847ParametrosPln_AreaTrabalhoCod = false;
         /* Using cursor T002O15 */
         pr_default.execute(13, new Object[] {n847ParametrosPln_AreaTrabalhoCod, A847ParametrosPln_AreaTrabalhoCod});
         if ( (pr_default.getStatus(13) == 101) )
         {
            if ( ! ( (0==A847ParametrosPln_AreaTrabalhoCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Parametros Planiljas_Area Trabalho'.", "ForeignKeyNotFound", 1, "PARAMETROSPLN_AREATRABALHOCOD");
               AnyError = 1;
               GX_FocusControl = dynParametrosPln_AreaTrabalhoCod_Internalname;
            }
         }
         pr_default.close(13);
         dynload_actions( ) ;
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV7ParametrosPln_Codigo',fld:'vPARAMETROSPLN_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E122O2',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV9TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(13);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         Z2015ParametrosPln_Aba = "";
         Z849ParametrosPln_Campo = "";
         Z850ParametrosPln_Coluna = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         bttBtn_trn_delete_Jsonclick = "";
         lblTextblockparametrospln_areatrabalhocod_Jsonclick = "";
         lblTextblockparametrospln_aba_Jsonclick = "";
         A2015ParametrosPln_Aba = "";
         lblTextblockparametrospln_campo_Jsonclick = "";
         A849ParametrosPln_Campo = "";
         lblTextblockparametrospln_coluna_Jsonclick = "";
         A850ParametrosPln_Coluna = "";
         lblTextblockparametrospln_linha_Jsonclick = "";
         AV13Pgmname = "";
         Dvpanel_tableattributes_Height = "";
         Dvpanel_tableattributes_Class = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode104 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         AV12TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         T002O5_A848ParametrosPln_Codigo = new int[1] ;
         T002O5_A2015ParametrosPln_Aba = new String[] {""} ;
         T002O5_A849ParametrosPln_Campo = new String[] {""} ;
         T002O5_A850ParametrosPln_Coluna = new String[] {""} ;
         T002O5_A851ParametrosPln_Linha = new int[1] ;
         T002O5_A847ParametrosPln_AreaTrabalhoCod = new int[1] ;
         T002O5_n847ParametrosPln_AreaTrabalhoCod = new bool[] {false} ;
         T002O4_A847ParametrosPln_AreaTrabalhoCod = new int[1] ;
         T002O4_n847ParametrosPln_AreaTrabalhoCod = new bool[] {false} ;
         T002O6_A847ParametrosPln_AreaTrabalhoCod = new int[1] ;
         T002O6_n847ParametrosPln_AreaTrabalhoCod = new bool[] {false} ;
         T002O7_A848ParametrosPln_Codigo = new int[1] ;
         T002O3_A848ParametrosPln_Codigo = new int[1] ;
         T002O3_A2015ParametrosPln_Aba = new String[] {""} ;
         T002O3_A849ParametrosPln_Campo = new String[] {""} ;
         T002O3_A850ParametrosPln_Coluna = new String[] {""} ;
         T002O3_A851ParametrosPln_Linha = new int[1] ;
         T002O3_A847ParametrosPln_AreaTrabalhoCod = new int[1] ;
         T002O3_n847ParametrosPln_AreaTrabalhoCod = new bool[] {false} ;
         T002O8_A848ParametrosPln_Codigo = new int[1] ;
         T002O9_A848ParametrosPln_Codigo = new int[1] ;
         T002O2_A848ParametrosPln_Codigo = new int[1] ;
         T002O2_A2015ParametrosPln_Aba = new String[] {""} ;
         T002O2_A849ParametrosPln_Campo = new String[] {""} ;
         T002O2_A850ParametrosPln_Coluna = new String[] {""} ;
         T002O2_A851ParametrosPln_Linha = new int[1] ;
         T002O2_A847ParametrosPln_AreaTrabalhoCod = new int[1] ;
         T002O2_n847ParametrosPln_AreaTrabalhoCod = new bool[] {false} ;
         T002O10_A848ParametrosPln_Codigo = new int[1] ;
         T002O13_A848ParametrosPln_Codigo = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         T002O14_A847ParametrosPln_AreaTrabalhoCod = new int[1] ;
         T002O14_n847ParametrosPln_AreaTrabalhoCod = new bool[] {false} ;
         T002O14_A6AreaTrabalho_Descricao = new String[] {""} ;
         T002O14_n6AreaTrabalho_Descricao = new bool[] {false} ;
         T002O15_A847ParametrosPln_AreaTrabalhoCod = new int[1] ;
         T002O15_n847ParametrosPln_AreaTrabalhoCod = new bool[] {false} ;
         isValidOutput = new GxUnknownObjectCollection();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.parametrosplanilhas__default(),
            new Object[][] {
                new Object[] {
               T002O2_A848ParametrosPln_Codigo, T002O2_A2015ParametrosPln_Aba, T002O2_A849ParametrosPln_Campo, T002O2_A850ParametrosPln_Coluna, T002O2_A851ParametrosPln_Linha, T002O2_A847ParametrosPln_AreaTrabalhoCod, T002O2_n847ParametrosPln_AreaTrabalhoCod
               }
               , new Object[] {
               T002O3_A848ParametrosPln_Codigo, T002O3_A2015ParametrosPln_Aba, T002O3_A849ParametrosPln_Campo, T002O3_A850ParametrosPln_Coluna, T002O3_A851ParametrosPln_Linha, T002O3_A847ParametrosPln_AreaTrabalhoCod, T002O3_n847ParametrosPln_AreaTrabalhoCod
               }
               , new Object[] {
               T002O4_A847ParametrosPln_AreaTrabalhoCod
               }
               , new Object[] {
               T002O5_A848ParametrosPln_Codigo, T002O5_A2015ParametrosPln_Aba, T002O5_A849ParametrosPln_Campo, T002O5_A850ParametrosPln_Coluna, T002O5_A851ParametrosPln_Linha, T002O5_A847ParametrosPln_AreaTrabalhoCod, T002O5_n847ParametrosPln_AreaTrabalhoCod
               }
               , new Object[] {
               T002O6_A847ParametrosPln_AreaTrabalhoCod
               }
               , new Object[] {
               T002O7_A848ParametrosPln_Codigo
               }
               , new Object[] {
               T002O8_A848ParametrosPln_Codigo
               }
               , new Object[] {
               T002O9_A848ParametrosPln_Codigo
               }
               , new Object[] {
               T002O10_A848ParametrosPln_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T002O13_A848ParametrosPln_Codigo
               }
               , new Object[] {
               T002O14_A847ParametrosPln_AreaTrabalhoCod, T002O14_A6AreaTrabalho_Descricao, T002O14_n6AreaTrabalho_Descricao
               }
               , new Object[] {
               T002O15_A847ParametrosPln_AreaTrabalhoCod
               }
            }
         );
         AV13Pgmname = "ParametrosPlanilhas";
      }

      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short RcdFound104 ;
      private short GX_JID ;
      private short Gx_BScreen ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int wcpOAV7ParametrosPln_Codigo ;
      private int Z848ParametrosPln_Codigo ;
      private int Z851ParametrosPln_Linha ;
      private int Z847ParametrosPln_AreaTrabalhoCod ;
      private int N847ParametrosPln_AreaTrabalhoCod ;
      private int A847ParametrosPln_AreaTrabalhoCod ;
      private int AV7ParametrosPln_Codigo ;
      private int trnEnded ;
      private int A848ParametrosPln_Codigo ;
      private int edtParametrosPln_Codigo_Enabled ;
      private int edtParametrosPln_Codigo_Visible ;
      private int bttBtn_trn_enter_Visible ;
      private int bttBtn_trn_enter_Enabled ;
      private int bttBtn_trn_cancel_Visible ;
      private int bttBtn_trn_delete_Visible ;
      private int bttBtn_trn_delete_Enabled ;
      private int edtParametrosPln_Aba_Enabled ;
      private int edtParametrosPln_Campo_Enabled ;
      private int edtParametrosPln_Coluna_Enabled ;
      private int A851ParametrosPln_Linha ;
      private int edtParametrosPln_Linha_Enabled ;
      private int AV11Insert_ParametrosPln_AreaTrabalhoCod ;
      private int AV14GXV1 ;
      private int idxLst ;
      private int gxdynajaxindex ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String Z2015ParametrosPln_Aba ;
      private String Z850ParametrosPln_Coluna ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String Gx_mode ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String dynParametrosPln_AreaTrabalhoCod_Internalname ;
      private String edtParametrosPln_Codigo_Internalname ;
      private String edtParametrosPln_Codigo_Jsonclick ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String bttBtn_trn_delete_Internalname ;
      private String bttBtn_trn_delete_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockparametrospln_areatrabalhocod_Internalname ;
      private String lblTextblockparametrospln_areatrabalhocod_Jsonclick ;
      private String dynParametrosPln_AreaTrabalhoCod_Jsonclick ;
      private String lblTextblockparametrospln_aba_Internalname ;
      private String lblTextblockparametrospln_aba_Jsonclick ;
      private String edtParametrosPln_Aba_Internalname ;
      private String A2015ParametrosPln_Aba ;
      private String edtParametrosPln_Aba_Jsonclick ;
      private String lblTextblockparametrospln_campo_Internalname ;
      private String lblTextblockparametrospln_campo_Jsonclick ;
      private String edtParametrosPln_Campo_Internalname ;
      private String edtParametrosPln_Campo_Jsonclick ;
      private String lblTextblockparametrospln_coluna_Internalname ;
      private String lblTextblockparametrospln_coluna_Jsonclick ;
      private String edtParametrosPln_Coluna_Internalname ;
      private String A850ParametrosPln_Coluna ;
      private String edtParametrosPln_Coluna_Jsonclick ;
      private String lblTextblockparametrospln_linha_Internalname ;
      private String lblTextblockparametrospln_linha_Jsonclick ;
      private String edtParametrosPln_Linha_Internalname ;
      private String edtParametrosPln_Linha_Jsonclick ;
      private String AV13Pgmname ;
      private String Dvpanel_tableattributes_Width ;
      private String Dvpanel_tableattributes_Height ;
      private String Dvpanel_tableattributes_Cls ;
      private String Dvpanel_tableattributes_Title ;
      private String Dvpanel_tableattributes_Class ;
      private String Dvpanel_tableattributes_Iconposition ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode104 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Dvpanel_tableattributes_Internalname ;
      private String gxwrpcisep ;
      private bool entryPointCalled ;
      private bool n847ParametrosPln_AreaTrabalhoCod ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool Dvpanel_tableattributes_Collapsible ;
      private bool Dvpanel_tableattributes_Collapsed ;
      private bool Dvpanel_tableattributes_Enabled ;
      private bool Dvpanel_tableattributes_Autowidth ;
      private bool Dvpanel_tableattributes_Autoheight ;
      private bool Dvpanel_tableattributes_Showheader ;
      private bool Dvpanel_tableattributes_Showcollapseicon ;
      private bool Dvpanel_tableattributes_Autoscroll ;
      private bool Dvpanel_tableattributes_Visible ;
      private bool returnInSub ;
      private String Z849ParametrosPln_Campo ;
      private String A849ParametrosPln_Campo ;
      private IGxSession AV10WebSession ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox dynParametrosPln_AreaTrabalhoCod ;
      private IDataStoreProvider pr_default ;
      private int[] T002O5_A848ParametrosPln_Codigo ;
      private String[] T002O5_A2015ParametrosPln_Aba ;
      private String[] T002O5_A849ParametrosPln_Campo ;
      private String[] T002O5_A850ParametrosPln_Coluna ;
      private int[] T002O5_A851ParametrosPln_Linha ;
      private int[] T002O5_A847ParametrosPln_AreaTrabalhoCod ;
      private bool[] T002O5_n847ParametrosPln_AreaTrabalhoCod ;
      private int[] T002O4_A847ParametrosPln_AreaTrabalhoCod ;
      private bool[] T002O4_n847ParametrosPln_AreaTrabalhoCod ;
      private int[] T002O6_A847ParametrosPln_AreaTrabalhoCod ;
      private bool[] T002O6_n847ParametrosPln_AreaTrabalhoCod ;
      private int[] T002O7_A848ParametrosPln_Codigo ;
      private int[] T002O3_A848ParametrosPln_Codigo ;
      private String[] T002O3_A2015ParametrosPln_Aba ;
      private String[] T002O3_A849ParametrosPln_Campo ;
      private String[] T002O3_A850ParametrosPln_Coluna ;
      private int[] T002O3_A851ParametrosPln_Linha ;
      private int[] T002O3_A847ParametrosPln_AreaTrabalhoCod ;
      private bool[] T002O3_n847ParametrosPln_AreaTrabalhoCod ;
      private int[] T002O8_A848ParametrosPln_Codigo ;
      private int[] T002O9_A848ParametrosPln_Codigo ;
      private int[] T002O2_A848ParametrosPln_Codigo ;
      private String[] T002O2_A2015ParametrosPln_Aba ;
      private String[] T002O2_A849ParametrosPln_Campo ;
      private String[] T002O2_A850ParametrosPln_Coluna ;
      private int[] T002O2_A851ParametrosPln_Linha ;
      private int[] T002O2_A847ParametrosPln_AreaTrabalhoCod ;
      private bool[] T002O2_n847ParametrosPln_AreaTrabalhoCod ;
      private int[] T002O10_A848ParametrosPln_Codigo ;
      private int[] T002O13_A848ParametrosPln_Codigo ;
      private int[] T002O14_A847ParametrosPln_AreaTrabalhoCod ;
      private bool[] T002O14_n847ParametrosPln_AreaTrabalhoCod ;
      private String[] T002O14_A6AreaTrabalho_Descricao ;
      private bool[] T002O14_n6AreaTrabalho_Descricao ;
      private int[] T002O15_A847ParametrosPln_AreaTrabalhoCod ;
      private bool[] T002O15_n847ParametrosPln_AreaTrabalhoCod ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV12TrnContextAtt ;
   }

   public class parametrosplanilhas__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new UpdateCursor(def[9])
         ,new UpdateCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT002O5 ;
          prmT002O5 = new Object[] {
          new Object[] {"@ParametrosPln_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002O4 ;
          prmT002O4 = new Object[] {
          new Object[] {"@ParametrosPln_AreaTrabalhoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002O6 ;
          prmT002O6 = new Object[] {
          new Object[] {"@ParametrosPln_AreaTrabalhoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002O7 ;
          prmT002O7 = new Object[] {
          new Object[] {"@ParametrosPln_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002O3 ;
          prmT002O3 = new Object[] {
          new Object[] {"@ParametrosPln_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002O8 ;
          prmT002O8 = new Object[] {
          new Object[] {"@ParametrosPln_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002O9 ;
          prmT002O9 = new Object[] {
          new Object[] {"@ParametrosPln_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002O2 ;
          prmT002O2 = new Object[] {
          new Object[] {"@ParametrosPln_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002O10 ;
          prmT002O10 = new Object[] {
          new Object[] {"@ParametrosPln_Aba",SqlDbType.Char,30,0} ,
          new Object[] {"@ParametrosPln_Campo",SqlDbType.VarChar,50,0} ,
          new Object[] {"@ParametrosPln_Coluna",SqlDbType.Char,2,0} ,
          new Object[] {"@ParametrosPln_Linha",SqlDbType.Int,6,0} ,
          new Object[] {"@ParametrosPln_AreaTrabalhoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002O11 ;
          prmT002O11 = new Object[] {
          new Object[] {"@ParametrosPln_Aba",SqlDbType.Char,30,0} ,
          new Object[] {"@ParametrosPln_Campo",SqlDbType.VarChar,50,0} ,
          new Object[] {"@ParametrosPln_Coluna",SqlDbType.Char,2,0} ,
          new Object[] {"@ParametrosPln_Linha",SqlDbType.Int,6,0} ,
          new Object[] {"@ParametrosPln_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ParametrosPln_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002O12 ;
          prmT002O12 = new Object[] {
          new Object[] {"@ParametrosPln_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002O13 ;
          prmT002O13 = new Object[] {
          } ;
          Object[] prmT002O14 ;
          prmT002O14 = new Object[] {
          } ;
          Object[] prmT002O15 ;
          prmT002O15 = new Object[] {
          new Object[] {"@ParametrosPln_AreaTrabalhoCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T002O2", "SELECT [ParametrosPln_Codigo], [ParametrosPln_Aba], [ParametrosPln_Campo], [ParametrosPln_Coluna], [ParametrosPln_Linha], [ParametrosPln_AreaTrabalhoCod] AS ParametrosPln_AreaTrabalhoCod FROM [ParametrosPlanilhas] WITH (UPDLOCK) WHERE [ParametrosPln_Codigo] = @ParametrosPln_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002O2,1,0,true,false )
             ,new CursorDef("T002O3", "SELECT [ParametrosPln_Codigo], [ParametrosPln_Aba], [ParametrosPln_Campo], [ParametrosPln_Coluna], [ParametrosPln_Linha], [ParametrosPln_AreaTrabalhoCod] AS ParametrosPln_AreaTrabalhoCod FROM [ParametrosPlanilhas] WITH (NOLOCK) WHERE [ParametrosPln_Codigo] = @ParametrosPln_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002O3,1,0,true,false )
             ,new CursorDef("T002O4", "SELECT [AreaTrabalho_Codigo] AS ParametrosPln_AreaTrabalhoCod FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @ParametrosPln_AreaTrabalhoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002O4,1,0,true,false )
             ,new CursorDef("T002O5", "SELECT TM1.[ParametrosPln_Codigo], TM1.[ParametrosPln_Aba], TM1.[ParametrosPln_Campo], TM1.[ParametrosPln_Coluna], TM1.[ParametrosPln_Linha], TM1.[ParametrosPln_AreaTrabalhoCod] AS ParametrosPln_AreaTrabalhoCod FROM [ParametrosPlanilhas] TM1 WITH (NOLOCK) WHERE TM1.[ParametrosPln_Codigo] = @ParametrosPln_Codigo ORDER BY TM1.[ParametrosPln_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT002O5,100,0,true,false )
             ,new CursorDef("T002O6", "SELECT [AreaTrabalho_Codigo] AS ParametrosPln_AreaTrabalhoCod FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @ParametrosPln_AreaTrabalhoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002O6,1,0,true,false )
             ,new CursorDef("T002O7", "SELECT [ParametrosPln_Codigo] FROM [ParametrosPlanilhas] WITH (NOLOCK) WHERE [ParametrosPln_Codigo] = @ParametrosPln_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT002O7,1,0,true,false )
             ,new CursorDef("T002O8", "SELECT TOP 1 [ParametrosPln_Codigo] FROM [ParametrosPlanilhas] WITH (NOLOCK) WHERE ( [ParametrosPln_Codigo] > @ParametrosPln_Codigo) ORDER BY [ParametrosPln_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT002O8,1,0,true,true )
             ,new CursorDef("T002O9", "SELECT TOP 1 [ParametrosPln_Codigo] FROM [ParametrosPlanilhas] WITH (NOLOCK) WHERE ( [ParametrosPln_Codigo] < @ParametrosPln_Codigo) ORDER BY [ParametrosPln_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT002O9,1,0,true,true )
             ,new CursorDef("T002O10", "INSERT INTO [ParametrosPlanilhas]([ParametrosPln_Aba], [ParametrosPln_Campo], [ParametrosPln_Coluna], [ParametrosPln_Linha], [ParametrosPln_AreaTrabalhoCod]) VALUES(@ParametrosPln_Aba, @ParametrosPln_Campo, @ParametrosPln_Coluna, @ParametrosPln_Linha, @ParametrosPln_AreaTrabalhoCod); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT002O10)
             ,new CursorDef("T002O11", "UPDATE [ParametrosPlanilhas] SET [ParametrosPln_Aba]=@ParametrosPln_Aba, [ParametrosPln_Campo]=@ParametrosPln_Campo, [ParametrosPln_Coluna]=@ParametrosPln_Coluna, [ParametrosPln_Linha]=@ParametrosPln_Linha, [ParametrosPln_AreaTrabalhoCod]=@ParametrosPln_AreaTrabalhoCod  WHERE [ParametrosPln_Codigo] = @ParametrosPln_Codigo", GxErrorMask.GX_NOMASK,prmT002O11)
             ,new CursorDef("T002O12", "DELETE FROM [ParametrosPlanilhas]  WHERE [ParametrosPln_Codigo] = @ParametrosPln_Codigo", GxErrorMask.GX_NOMASK,prmT002O12)
             ,new CursorDef("T002O13", "SELECT [ParametrosPln_Codigo] FROM [ParametrosPlanilhas] WITH (NOLOCK) ORDER BY [ParametrosPln_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT002O13,100,0,true,false )
             ,new CursorDef("T002O14", "SELECT [AreaTrabalho_Codigo] AS ParametrosPln_AreaTrabalhoCod, [AreaTrabalho_Descricao] FROM [AreaTrabalho] WITH (NOLOCK) ORDER BY [AreaTrabalho_Descricao] ",true, GxErrorMask.GX_NOMASK, false, this,prmT002O14,0,0,true,false )
             ,new CursorDef("T002O15", "SELECT [AreaTrabalho_Codigo] AS ParametrosPln_AreaTrabalhoCod FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @ParametrosPln_AreaTrabalhoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002O15,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 30) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 2) ;
                ((int[]) buf[4])[0] = rslt.getInt(5) ;
                ((int[]) buf[5])[0] = rslt.getInt(6) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(6);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 30) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 2) ;
                ((int[]) buf[4])[0] = rslt.getInt(5) ;
                ((int[]) buf[5])[0] = rslt.getInt(6) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(6);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 30) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 2) ;
                ((int[]) buf[4])[0] = rslt.getInt(5) ;
                ((int[]) buf[5])[0] = rslt.getInt(6) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(6);
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
             case 13 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                stmt.SetParameter(4, (int)parms[3]);
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 5 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(5, (int)parms[5]);
                }
                return;
             case 9 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                stmt.SetParameter(4, (int)parms[3]);
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 5 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(5, (int)parms[5]);
                }
                stmt.SetParameter(6, (int)parms[6]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 13 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
       }
    }

 }

}
