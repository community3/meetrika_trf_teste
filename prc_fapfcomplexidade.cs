/*
               File: PRC_FAPFComplexidade
        Description: Fun��o APF Complexidade
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:50:49.55
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_fapfcomplexidade : GXProcedure
   {
      public prc_fapfcomplexidade( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_fapfcomplexidade( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_FuncaoAPF_Codigo ,
                           ref String aP1_Complexidade )
      {
         this.AV9FuncaoAPF_Codigo = aP0_FuncaoAPF_Codigo;
         this.AV8Complexidade = aP1_Complexidade;
         initialize();
         executePrivate();
         aP0_FuncaoAPF_Codigo=this.AV9FuncaoAPF_Codigo;
         aP1_Complexidade=this.AV8Complexidade;
      }

      public String executeUdp( ref int aP0_FuncaoAPF_Codigo )
      {
         this.AV9FuncaoAPF_Codigo = aP0_FuncaoAPF_Codigo;
         this.AV8Complexidade = aP1_Complexidade;
         initialize();
         executePrivate();
         aP0_FuncaoAPF_Codigo=this.AV9FuncaoAPF_Codigo;
         aP1_Complexidade=this.AV8Complexidade;
         return AV8Complexidade ;
      }

      public void executeSubmit( ref int aP0_FuncaoAPF_Codigo ,
                                 ref String aP1_Complexidade )
      {
         prc_fapfcomplexidade objprc_fapfcomplexidade;
         objprc_fapfcomplexidade = new prc_fapfcomplexidade();
         objprc_fapfcomplexidade.AV9FuncaoAPF_Codigo = aP0_FuncaoAPF_Codigo;
         objprc_fapfcomplexidade.AV8Complexidade = aP1_Complexidade;
         objprc_fapfcomplexidade.context.SetSubmitInitialConfig(context);
         objprc_fapfcomplexidade.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_fapfcomplexidade);
         aP0_FuncaoAPF_Codigo=this.AV9FuncaoAPF_Codigo;
         aP1_Complexidade=this.AV8Complexidade;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_fapfcomplexidade)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV8Complexidade = "E";
         /* Using cursor P001X2 */
         pr_default.execute(0, new Object[] {AV9FuncaoAPF_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A184FuncaoAPF_Tipo = P001X2_A184FuncaoAPF_Tipo[0];
            A165FuncaoAPF_Codigo = P001X2_A165FuncaoAPF_Codigo[0];
            GXt_int1 = A388FuncaoAPF_TD;
            new prc_funcaoapf_td(context ).execute(  A165FuncaoAPF_Codigo, out  GXt_int1) ;
            A388FuncaoAPF_TD = GXt_int1;
            GXt_int1 = A387FuncaoAPF_AR;
            new prc_funcaoapf_ar(context ).execute( ref  A165FuncaoAPF_Codigo, ref  GXt_int1) ;
            A387FuncaoAPF_AR = GXt_int1;
            if ( StringUtil.StrCmp(A184FuncaoAPF_Tipo, "EE") == 0 )
            {
               if ( A387FuncaoAPF_AR < 2 )
               {
                  if ( A388FuncaoAPF_TD < 16 )
                  {
                     AV8Complexidade = "B";
                  }
                  else
                  {
                     AV8Complexidade = "M";
                  }
               }
               else if ( A387FuncaoAPF_AR == 2 )
               {
                  if ( A388FuncaoAPF_TD < 5 )
                  {
                     AV8Complexidade = "B";
                  }
                  else if ( A388FuncaoAPF_TD < 16 )
                  {
                     AV8Complexidade = "M";
                  }
                  else if ( A388FuncaoAPF_TD > 15 )
                  {
                     AV8Complexidade = "A";
                  }
               }
               else if ( A387FuncaoAPF_AR > 2 )
               {
                  if ( A388FuncaoAPF_TD < 5 )
                  {
                     AV8Complexidade = "M";
                  }
                  else if ( A388FuncaoAPF_TD > 4 )
                  {
                     AV8Complexidade = "A";
                  }
               }
            }
            else
            {
               if ( A387FuncaoAPF_AR < 2 )
               {
                  if ( A388FuncaoAPF_TD > 19 )
                  {
                     AV8Complexidade = "M";
                  }
                  else
                  {
                     AV8Complexidade = "B";
                  }
               }
               else if ( ( A387FuncaoAPF_AR > 1 ) && ( A387FuncaoAPF_AR < 4 ) )
               {
                  if ( A388FuncaoAPF_TD < 6 )
                  {
                     AV8Complexidade = "B";
                  }
                  else if ( A388FuncaoAPF_TD > 19 )
                  {
                     AV8Complexidade = "A";
                  }
                  else
                  {
                     AV8Complexidade = "M";
                  }
               }
               else if ( A387FuncaoAPF_AR > 3 )
               {
                  if ( A388FuncaoAPF_TD < 6 )
                  {
                     AV8Complexidade = "M";
                  }
                  else
                  {
                     AV8Complexidade = "A";
                  }
               }
            }
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P001X2_A184FuncaoAPF_Tipo = new String[] {""} ;
         P001X2_A165FuncaoAPF_Codigo = new int[1] ;
         A184FuncaoAPF_Tipo = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_fapfcomplexidade__default(),
            new Object[][] {
                new Object[] {
               P001X2_A184FuncaoAPF_Tipo, P001X2_A165FuncaoAPF_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short A388FuncaoAPF_TD ;
      private short A387FuncaoAPF_AR ;
      private short GXt_int1 ;
      private int AV9FuncaoAPF_Codigo ;
      private int A165FuncaoAPF_Codigo ;
      private String AV8Complexidade ;
      private String scmdbuf ;
      private String A184FuncaoAPF_Tipo ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_FuncaoAPF_Codigo ;
      private String aP1_Complexidade ;
      private IDataStoreProvider pr_default ;
      private String[] P001X2_A184FuncaoAPF_Tipo ;
      private int[] P001X2_A165FuncaoAPF_Codigo ;
   }

   public class prc_fapfcomplexidade__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP001X2 ;
          prmP001X2 = new Object[] {
          new Object[] {"@AV9FuncaoAPF_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P001X2", "SELECT [FuncaoAPF_Tipo], [FuncaoAPF_Codigo] FROM [FuncoesAPF] WITH (NOLOCK) WHERE [FuncaoAPF_Codigo] = @AV9FuncaoAPF_Codigo ORDER BY [FuncaoAPF_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP001X2,1,0,true,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getString(1, 3) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
