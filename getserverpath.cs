/*
               File: GetServerPath
        Description: Get Server Path
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:51:36.26
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getserverpath : GXProcedure
   {
      public getserverpath( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getserverpath( IGxContext context )
      {
         this.context = context;
         IsMain = false;
      }

      public void release( )
      {
      }

      public void execute( ref String aP0_ProgramName ,
                           ref String aP1_ServerPath )
      {
         this.AV11ProgramName = aP0_ProgramName;
         this.AV12ServerPath = aP1_ServerPath;
         initialize();
         executePrivate();
         aP0_ProgramName=this.AV11ProgramName;
         aP1_ServerPath=this.AV12ServerPath;
      }

      public String executeUdp( ref String aP0_ProgramName )
      {
         this.AV11ProgramName = aP0_ProgramName;
         this.AV12ServerPath = aP1_ServerPath;
         initialize();
         executePrivate();
         aP0_ProgramName=this.AV11ProgramName;
         aP1_ServerPath=this.AV12ServerPath;
         return AV12ServerPath ;
      }

      public void executeSubmit( ref String aP0_ProgramName ,
                                 ref String aP1_ServerPath )
      {
         getserverpath objgetserverpath;
         objgetserverpath = new getserverpath();
         objgetserverpath.AV11ProgramName = aP0_ProgramName;
         objgetserverpath.AV12ServerPath = aP1_ServerPath;
         objgetserverpath.context.SetSubmitInitialConfig(context);
         objgetserverpath.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetserverpath);
         aP0_ProgramName=this.AV11ProgramName;
         aP1_ServerPath=this.AV12ServerPath;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getserverpath)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV8Dir.Source = AV11ProgramName;
         AV9Directory = AV8Dir.GetAbsoluteName();
         if ( AV8Dir.ErrCode != 0 )
         {
            GX_msglist.addItem("No se puede leer el directorio");
            GX_msglist.addItem(AV8Dir.ErrDescription);
            this.cleanup();
            if (true) return;
         }
         AV10LastIndex = (short)(StringUtil.StringSearchRev( AV9Directory, "\\", -1));
         AV12ServerPath = StringUtil.Substring( AV9Directory, 1, AV10LastIndex);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV8Dir = new GxDirectory(context.GetPhysicalPath());
         AV9Directory = "";
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV10LastIndex ;
      private String AV11ProgramName ;
      private String AV12ServerPath ;
      private String AV9Directory ;
      private String aP0_ProgramName ;
      private String aP1_ServerPath ;
      private GxDirectory AV8Dir ;
   }

}
