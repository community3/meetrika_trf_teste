/*
               File: Prc_SaldoContratoReservar
        Description: Prc_Saldo Contrato Reservar
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:52:40.37
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_saldocontratoreservar : GXProcedure
   {
      public prc_saldocontratoreservar( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_saldocontratoreservar( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_SaldoContrato_Codigo ,
                           int aP1_Contrato_Codigo ,
                           int aP2_NotaEmpenho_Codigo ,
                           int aP3_ContagemResultado_Codigo ,
                           int aP4_AutorizacaoConsumo_Codigo ,
                           decimal aP5_AuxSaldoContrato_Reservado )
      {
         this.AV8SaldoContrato_Codigo = aP0_SaldoContrato_Codigo;
         this.AV10Contrato_Codigo = aP1_Contrato_Codigo;
         this.AV11NotaEmpenho_Codigo = aP2_NotaEmpenho_Codigo;
         this.AV14ContagemResultado_Codigo = aP3_ContagemResultado_Codigo;
         this.AV15AutorizacaoConsumo_Codigo = aP4_AutorizacaoConsumo_Codigo;
         this.AV9AuxSaldoContrato_Reservado = aP5_AuxSaldoContrato_Reservado;
         initialize();
         executePrivate();
      }

      public void executeSubmit( int aP0_SaldoContrato_Codigo ,
                                 int aP1_Contrato_Codigo ,
                                 int aP2_NotaEmpenho_Codigo ,
                                 int aP3_ContagemResultado_Codigo ,
                                 int aP4_AutorizacaoConsumo_Codigo ,
                                 decimal aP5_AuxSaldoContrato_Reservado )
      {
         prc_saldocontratoreservar objprc_saldocontratoreservar;
         objprc_saldocontratoreservar = new prc_saldocontratoreservar();
         objprc_saldocontratoreservar.AV8SaldoContrato_Codigo = aP0_SaldoContrato_Codigo;
         objprc_saldocontratoreservar.AV10Contrato_Codigo = aP1_Contrato_Codigo;
         objprc_saldocontratoreservar.AV11NotaEmpenho_Codigo = aP2_NotaEmpenho_Codigo;
         objprc_saldocontratoreservar.AV14ContagemResultado_Codigo = aP3_ContagemResultado_Codigo;
         objprc_saldocontratoreservar.AV15AutorizacaoConsumo_Codigo = aP4_AutorizacaoConsumo_Codigo;
         objprc_saldocontratoreservar.AV9AuxSaldoContrato_Reservado = aP5_AuxSaldoContrato_Reservado;
         objprc_saldocontratoreservar.context.SetSubmitInitialConfig(context);
         objprc_saldocontratoreservar.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_saldocontratoreservar);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_saldocontratoreservar)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00BZ2 */
         pr_default.execute(0, new Object[] {AV8SaldoContrato_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1561SaldoContrato_Codigo = P00BZ2_A1561SaldoContrato_Codigo[0];
            A1576SaldoContrato_Saldo = P00BZ2_A1576SaldoContrato_Saldo[0];
            A1574SaldoContrato_Reservado = P00BZ2_A1574SaldoContrato_Reservado[0];
            if ( AV9AuxSaldoContrato_Reservado <= A1576SaldoContrato_Saldo )
            {
               A1576SaldoContrato_Saldo = (decimal)(A1576SaldoContrato_Saldo-AV9AuxSaldoContrato_Reservado);
               A1574SaldoContrato_Reservado = (decimal)(A1574SaldoContrato_Reservado+AV9AuxSaldoContrato_Reservado);
               new prc_historicoconsumo(context ).execute(  A1561SaldoContrato_Codigo,  AV10Contrato_Codigo,  AV11NotaEmpenho_Codigo,  AV14ContagemResultado_Codigo,  0,  AV9AuxSaldoContrato_Reservado,  "RES") ;
            }
            /* Using cursor P00BZ3 */
            pr_default.execute(1, new Object[] {A1576SaldoContrato_Saldo, A1574SaldoContrato_Reservado, A1561SaldoContrato_Codigo});
            pr_default.close(1);
            dsDefault.SmartCacheProvider.SetUpdated("SaldoContrato") ;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00BZ2_A1561SaldoContrato_Codigo = new int[1] ;
         P00BZ2_A1576SaldoContrato_Saldo = new decimal[1] ;
         P00BZ2_A1574SaldoContrato_Reservado = new decimal[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_saldocontratoreservar__default(),
            new Object[][] {
                new Object[] {
               P00BZ2_A1561SaldoContrato_Codigo, P00BZ2_A1576SaldoContrato_Saldo, P00BZ2_A1574SaldoContrato_Reservado
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV8SaldoContrato_Codigo ;
      private int AV10Contrato_Codigo ;
      private int AV11NotaEmpenho_Codigo ;
      private int AV14ContagemResultado_Codigo ;
      private int AV15AutorizacaoConsumo_Codigo ;
      private int A1561SaldoContrato_Codigo ;
      private decimal AV9AuxSaldoContrato_Reservado ;
      private decimal A1576SaldoContrato_Saldo ;
      private decimal A1574SaldoContrato_Reservado ;
      private String scmdbuf ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00BZ2_A1561SaldoContrato_Codigo ;
      private decimal[] P00BZ2_A1576SaldoContrato_Saldo ;
      private decimal[] P00BZ2_A1574SaldoContrato_Reservado ;
   }

   public class prc_saldocontratoreservar__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new UpdateCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00BZ2 ;
          prmP00BZ2 = new Object[] {
          new Object[] {"@AV8SaldoContrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00BZ3 ;
          prmP00BZ3 = new Object[] {
          new Object[] {"@SaldoContrato_Saldo",SqlDbType.Decimal,18,5} ,
          new Object[] {"@SaldoContrato_Reservado",SqlDbType.Decimal,18,5} ,
          new Object[] {"@SaldoContrato_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00BZ2", "SELECT [SaldoContrato_Codigo], [SaldoContrato_Saldo], [SaldoContrato_Reservado] FROM [SaldoContrato] WITH (UPDLOCK) WHERE [SaldoContrato_Codigo] = @AV8SaldoContrato_Codigo ORDER BY [SaldoContrato_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00BZ2,1,0,true,true )
             ,new CursorDef("P00BZ3", "UPDATE [SaldoContrato] SET [SaldoContrato_Saldo]=@SaldoContrato_Saldo, [SaldoContrato_Reservado]=@SaldoContrato_Reservado  WHERE [SaldoContrato_Codigo] = @SaldoContrato_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00BZ3)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((decimal[]) buf[1])[0] = rslt.getDecimal(2) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (decimal)parms[0]);
                stmt.SetParameter(2, (decimal)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
       }
    }

 }

}
