/*
               File: WWContratoClausulas
        Description:  Contrato Clausulas
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/18/2020 12:59:43.83
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wwcontratoclausulas : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wwcontratoclausulas( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wwcontratoclausulas( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_36 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_36_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_36_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               AV40TFContrato_Numero = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFContrato_Numero", AV40TFContrato_Numero);
               AV41TFContrato_Numero_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41TFContrato_Numero_Sel", AV41TFContrato_Numero_Sel);
               AV44TFContratoClausulas_Item = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFContratoClausulas_Item", AV44TFContratoClausulas_Item);
               AV45TFContratoClausulas_Item_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFContratoClausulas_Item_Sel", AV45TFContratoClausulas_Item_Sel);
               AV48TFContratoClausulas_Descricao = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48TFContratoClausulas_Descricao", AV48TFContratoClausulas_Descricao);
               AV49TFContratoClausulas_Descricao_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFContratoClausulas_Descricao_Sel", AV49TFContratoClausulas_Descricao_Sel);
               AV42ddo_Contrato_NumeroTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42ddo_Contrato_NumeroTitleControlIdToReplace", AV42ddo_Contrato_NumeroTitleControlIdToReplace);
               AV46ddo_ContratoClausulas_ItemTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46ddo_ContratoClausulas_ItemTitleControlIdToReplace", AV46ddo_ContratoClausulas_ItemTitleControlIdToReplace);
               AV50ddo_ContratoClausulas_DescricaoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50ddo_ContratoClausulas_DescricaoTitleControlIdToReplace", AV50ddo_ContratoClausulas_DescricaoTitleControlIdToReplace);
               ajax_req_read_hidden_sdt(GetNextPar( ), AV6WWPContext);
               AV86Pgmname = GetNextPar( );
               A152ContratoClausulas_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A152ContratoClausulas_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A152ContratoClausulas_Codigo), 6, 0)));
               A74Contrato_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV40TFContrato_Numero, AV41TFContrato_Numero_Sel, AV44TFContratoClausulas_Item, AV45TFContratoClausulas_Item_Sel, AV48TFContratoClausulas_Descricao, AV49TFContratoClausulas_Descricao_Sel, AV42ddo_Contrato_NumeroTitleControlIdToReplace, AV46ddo_ContratoClausulas_ItemTitleControlIdToReplace, AV50ddo_ContratoClausulas_DescricaoTitleControlIdToReplace, AV6WWPContext, AV86Pgmname, A152ContratoClausulas_Codigo, A74Contrato_Codigo) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PA6P2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            START6P2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20205181259443");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wwcontratoclausulas.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATO_NUMERO", StringUtil.RTrim( AV40TFContrato_Numero));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATO_NUMERO_SEL", StringUtil.RTrim( AV41TFContrato_Numero_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOCLAUSULAS_ITEM", StringUtil.RTrim( AV44TFContratoClausulas_Item));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOCLAUSULAS_ITEM_SEL", StringUtil.RTrim( AV45TFContratoClausulas_Item_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOCLAUSULAS_DESCRICAO", AV48TFContratoClausulas_Descricao);
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOCLAUSULAS_DESCRICAO_SEL", AV49TFContratoClausulas_Descricao_Sel);
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_36", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_36), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV53GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV54GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV51DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV51DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATO_NUMEROTITLEFILTERDATA", AV39Contrato_NumeroTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATO_NUMEROTITLEFILTERDATA", AV39Contrato_NumeroTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATOCLAUSULAS_ITEMTITLEFILTERDATA", AV43ContratoClausulas_ItemTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATOCLAUSULAS_ITEMTITLEFILTERDATA", AV43ContratoClausulas_ItemTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATOCLAUSULAS_DESCRICAOTITLEFILTERDATA", AV47ContratoClausulas_DescricaoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATOCLAUSULAS_DESCRICAOTITLEFILTERDATA", AV47ContratoClausulas_DescricaoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vWWPCONTEXT", AV6WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vWWPCONTEXT", AV6WWPContext);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV86Pgmname));
         GxWebStd.gx_hidden_field( context, "CONTRATOCLAUSULAS_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A152ContratoClausulas_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A74Contrato_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Caption", StringUtil.RTrim( Ddo_contrato_numero_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Tooltip", StringUtil.RTrim( Ddo_contrato_numero_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Cls", StringUtil.RTrim( Ddo_contrato_numero_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Filteredtext_set", StringUtil.RTrim( Ddo_contrato_numero_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Selectedvalue_set", StringUtil.RTrim( Ddo_contrato_numero_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Dropdownoptionstype", StringUtil.RTrim( Ddo_contrato_numero_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contrato_numero_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Includesortasc", StringUtil.BoolToStr( Ddo_contrato_numero_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Includesortdsc", StringUtil.BoolToStr( Ddo_contrato_numero_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Sortedstatus", StringUtil.RTrim( Ddo_contrato_numero_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Includefilter", StringUtil.BoolToStr( Ddo_contrato_numero_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Filtertype", StringUtil.RTrim( Ddo_contrato_numero_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Filterisrange", StringUtil.BoolToStr( Ddo_contrato_numero_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Includedatalist", StringUtil.BoolToStr( Ddo_contrato_numero_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Datalisttype", StringUtil.RTrim( Ddo_contrato_numero_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Datalistproc", StringUtil.RTrim( Ddo_contrato_numero_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contrato_numero_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Sortasc", StringUtil.RTrim( Ddo_contrato_numero_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Sortdsc", StringUtil.RTrim( Ddo_contrato_numero_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Loadingdata", StringUtil.RTrim( Ddo_contrato_numero_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Cleanfilter", StringUtil.RTrim( Ddo_contrato_numero_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Noresultsfound", StringUtil.RTrim( Ddo_contrato_numero_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Searchbuttontext", StringUtil.RTrim( Ddo_contrato_numero_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOCLAUSULAS_ITEM_Caption", StringUtil.RTrim( Ddo_contratoclausulas_item_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOCLAUSULAS_ITEM_Tooltip", StringUtil.RTrim( Ddo_contratoclausulas_item_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOCLAUSULAS_ITEM_Cls", StringUtil.RTrim( Ddo_contratoclausulas_item_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOCLAUSULAS_ITEM_Filteredtext_set", StringUtil.RTrim( Ddo_contratoclausulas_item_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOCLAUSULAS_ITEM_Selectedvalue_set", StringUtil.RTrim( Ddo_contratoclausulas_item_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOCLAUSULAS_ITEM_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratoclausulas_item_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOCLAUSULAS_ITEM_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratoclausulas_item_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOCLAUSULAS_ITEM_Includesortasc", StringUtil.BoolToStr( Ddo_contratoclausulas_item_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOCLAUSULAS_ITEM_Includesortdsc", StringUtil.BoolToStr( Ddo_contratoclausulas_item_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOCLAUSULAS_ITEM_Sortedstatus", StringUtil.RTrim( Ddo_contratoclausulas_item_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOCLAUSULAS_ITEM_Includefilter", StringUtil.BoolToStr( Ddo_contratoclausulas_item_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOCLAUSULAS_ITEM_Filtertype", StringUtil.RTrim( Ddo_contratoclausulas_item_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOCLAUSULAS_ITEM_Filterisrange", StringUtil.BoolToStr( Ddo_contratoclausulas_item_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOCLAUSULAS_ITEM_Includedatalist", StringUtil.BoolToStr( Ddo_contratoclausulas_item_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOCLAUSULAS_ITEM_Datalisttype", StringUtil.RTrim( Ddo_contratoclausulas_item_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOCLAUSULAS_ITEM_Datalistproc", StringUtil.RTrim( Ddo_contratoclausulas_item_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOCLAUSULAS_ITEM_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contratoclausulas_item_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOCLAUSULAS_ITEM_Sortasc", StringUtil.RTrim( Ddo_contratoclausulas_item_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOCLAUSULAS_ITEM_Sortdsc", StringUtil.RTrim( Ddo_contratoclausulas_item_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOCLAUSULAS_ITEM_Loadingdata", StringUtil.RTrim( Ddo_contratoclausulas_item_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOCLAUSULAS_ITEM_Cleanfilter", StringUtil.RTrim( Ddo_contratoclausulas_item_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOCLAUSULAS_ITEM_Noresultsfound", StringUtil.RTrim( Ddo_contratoclausulas_item_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOCLAUSULAS_ITEM_Searchbuttontext", StringUtil.RTrim( Ddo_contratoclausulas_item_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOCLAUSULAS_DESCRICAO_Caption", StringUtil.RTrim( Ddo_contratoclausulas_descricao_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOCLAUSULAS_DESCRICAO_Tooltip", StringUtil.RTrim( Ddo_contratoclausulas_descricao_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOCLAUSULAS_DESCRICAO_Cls", StringUtil.RTrim( Ddo_contratoclausulas_descricao_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOCLAUSULAS_DESCRICAO_Filteredtext_set", StringUtil.RTrim( Ddo_contratoclausulas_descricao_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOCLAUSULAS_DESCRICAO_Selectedvalue_set", StringUtil.RTrim( Ddo_contratoclausulas_descricao_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOCLAUSULAS_DESCRICAO_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratoclausulas_descricao_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOCLAUSULAS_DESCRICAO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratoclausulas_descricao_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOCLAUSULAS_DESCRICAO_Includesortasc", StringUtil.BoolToStr( Ddo_contratoclausulas_descricao_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOCLAUSULAS_DESCRICAO_Includesortdsc", StringUtil.BoolToStr( Ddo_contratoclausulas_descricao_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOCLAUSULAS_DESCRICAO_Sortedstatus", StringUtil.RTrim( Ddo_contratoclausulas_descricao_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOCLAUSULAS_DESCRICAO_Includefilter", StringUtil.BoolToStr( Ddo_contratoclausulas_descricao_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOCLAUSULAS_DESCRICAO_Filtertype", StringUtil.RTrim( Ddo_contratoclausulas_descricao_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOCLAUSULAS_DESCRICAO_Filterisrange", StringUtil.BoolToStr( Ddo_contratoclausulas_descricao_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOCLAUSULAS_DESCRICAO_Includedatalist", StringUtil.BoolToStr( Ddo_contratoclausulas_descricao_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOCLAUSULAS_DESCRICAO_Datalisttype", StringUtil.RTrim( Ddo_contratoclausulas_descricao_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOCLAUSULAS_DESCRICAO_Datalistproc", StringUtil.RTrim( Ddo_contratoclausulas_descricao_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOCLAUSULAS_DESCRICAO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contratoclausulas_descricao_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOCLAUSULAS_DESCRICAO_Sortasc", StringUtil.RTrim( Ddo_contratoclausulas_descricao_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOCLAUSULAS_DESCRICAO_Sortdsc", StringUtil.RTrim( Ddo_contratoclausulas_descricao_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOCLAUSULAS_DESCRICAO_Loadingdata", StringUtil.RTrim( Ddo_contratoclausulas_descricao_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOCLAUSULAS_DESCRICAO_Cleanfilter", StringUtil.RTrim( Ddo_contratoclausulas_descricao_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOCLAUSULAS_DESCRICAO_Noresultsfound", StringUtil.RTrim( Ddo_contratoclausulas_descricao_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOCLAUSULAS_DESCRICAO_Searchbuttontext", StringUtil.RTrim( Ddo_contratoclausulas_descricao_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Activeeventkey", StringUtil.RTrim( Ddo_contrato_numero_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Filteredtext_get", StringUtil.RTrim( Ddo_contrato_numero_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Selectedvalue_get", StringUtil.RTrim( Ddo_contrato_numero_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOCLAUSULAS_ITEM_Activeeventkey", StringUtil.RTrim( Ddo_contratoclausulas_item_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOCLAUSULAS_ITEM_Filteredtext_get", StringUtil.RTrim( Ddo_contratoclausulas_item_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOCLAUSULAS_ITEM_Selectedvalue_get", StringUtil.RTrim( Ddo_contratoclausulas_item_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOCLAUSULAS_DESCRICAO_Activeeventkey", StringUtil.RTrim( Ddo_contratoclausulas_descricao_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOCLAUSULAS_DESCRICAO_Filteredtext_get", StringUtil.RTrim( Ddo_contratoclausulas_descricao_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOCLAUSULAS_DESCRICAO_Selectedvalue_get", StringUtil.RTrim( Ddo_contratoclausulas_descricao_Selectedvalue_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WE6P2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVT6P2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wwcontratoclausulas.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WWContratoClausulas" ;
      }

      public override String GetPgmdesc( )
      {
         return " Contrato Clausulas" ;
      }

      protected void WB6P0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_6P2( true) ;
         }
         else
         {
            wb_table1_2_6P2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_6P2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 48,'',false,'" + sGXsfl_36_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontrato_numero_Internalname, StringUtil.RTrim( AV40TFContrato_Numero), StringUtil.RTrim( context.localUtil.Format( AV40TFContrato_Numero, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,48);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontrato_numero_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontrato_numero_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContratoClausulas.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'" + sGXsfl_36_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontrato_numero_sel_Internalname, StringUtil.RTrim( AV41TFContrato_Numero_Sel), StringUtil.RTrim( context.localUtil.Format( AV41TFContrato_Numero_Sel, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,49);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontrato_numero_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontrato_numero_sel_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContratoClausulas.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 50,'',false,'" + sGXsfl_36_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoclausulas_item_Internalname, StringUtil.RTrim( AV44TFContratoClausulas_Item), StringUtil.RTrim( context.localUtil.Format( AV44TFContratoClausulas_Item, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,50);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoclausulas_item_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoclausulas_item_Visible, 1, 0, "text", "", 10, "chr", 1, "row", 10, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContratoClausulas.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 51,'',false,'" + sGXsfl_36_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoclausulas_item_sel_Internalname, StringUtil.RTrim( AV45TFContratoClausulas_Item_Sel), StringUtil.RTrim( context.localUtil.Format( AV45TFContratoClausulas_Item_Sel, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,51);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoclausulas_item_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoclausulas_item_sel_Visible, 1, 0, "text", "", 10, "chr", 1, "row", 10, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContratoClausulas.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 52,'',false,'" + sGXsfl_36_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavTfcontratoclausulas_descricao_Internalname, AV48TFContratoClausulas_Descricao, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,52);\"", 0, edtavTfcontratoclausulas_descricao_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_WWContratoClausulas.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 53,'',false,'" + sGXsfl_36_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavTfcontratoclausulas_descricao_sel_Internalname, AV49TFContratoClausulas_Descricao_Sel, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,53);\"", 0, edtavTfcontratoclausulas_descricao_sel_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_WWContratoClausulas.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATO_NUMEROContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 55,'',false,'" + sGXsfl_36_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contrato_numerotitlecontrolidtoreplace_Internalname, AV42ddo_Contrato_NumeroTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,55);\"", 0, edtavDdo_contrato_numerotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWContratoClausulas.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATOCLAUSULAS_ITEMContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 57,'',false,'" + sGXsfl_36_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratoclausulas_itemtitlecontrolidtoreplace_Internalname, AV46ddo_ContratoClausulas_ItemTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,57);\"", 0, edtavDdo_contratoclausulas_itemtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWContratoClausulas.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATOCLAUSULAS_DESCRICAOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'" + sGXsfl_36_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratoclausulas_descricaotitlecontrolidtoreplace_Internalname, AV50ddo_ContratoClausulas_DescricaoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,59);\"", 0, edtavDdo_contratoclausulas_descricaotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWContratoClausulas.htm");
         }
         wbLoad = true;
      }

      protected void START6P2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", " Contrato Clausulas", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUP6P0( ) ;
      }

      protected void WS6P2( )
      {
         START6P2( ) ;
         EVT6P2( ) ;
      }

      protected void EVT6P2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E116P2 */
                              E116P2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATO_NUMERO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E126P2 */
                              E126P2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOCLAUSULAS_ITEM.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E136P2 */
                              E136P2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOCLAUSULAS_DESCRICAO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E146P2 */
                              E146P2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E156P2 */
                              E156P2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOINSERT'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E166P2 */
                              E166P2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              nGXsfl_36_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_36_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_36_idx), 4, 0)), 4, "0");
                              SubsflControlProps_362( ) ;
                              AV28Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV28Update)) ? AV84Update_GXI : context.convertURL( context.PathToRelativeUrl( AV28Update))));
                              AV29Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete)) ? AV85Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV29Delete))));
                              A77Contrato_Numero = cgiGet( edtContrato_Numero_Internalname);
                              A153ContratoClausulas_Item = cgiGet( edtContratoClausulas_Item_Internalname);
                              A154ContratoClausulas_Descricao = cgiGet( edtContratoClausulas_Descricao_Internalname);
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E176P2 */
                                    E176P2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E186P2 */
                                    E186P2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E196P2 */
                                    E196P2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       /* Set Refresh If Orderedby Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Ordereddsc Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontrato_numero Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATO_NUMERO"), AV40TFContrato_Numero) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontrato_numero_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATO_NUMERO_SEL"), AV41TFContrato_Numero_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratoclausulas_item Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOCLAUSULAS_ITEM"), AV44TFContratoClausulas_Item) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratoclausulas_item_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOCLAUSULAS_ITEM_SEL"), AV45TFContratoClausulas_Item_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratoclausulas_descricao Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOCLAUSULAS_DESCRICAO"), AV48TFContratoClausulas_Descricao) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratoclausulas_descricao_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOCLAUSULAS_DESCRICAO_SEL"), AV49TFContratoClausulas_Descricao_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE6P2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PA6P2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_362( ) ;
         while ( nGXsfl_36_idx <= nRC_GXsfl_36 )
         {
            sendrow_362( ) ;
            nGXsfl_36_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_36_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_36_idx+1));
            sGXsfl_36_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_36_idx), 4, 0)), 4, "0");
            SubsflControlProps_362( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       String AV40TFContrato_Numero ,
                                       String AV41TFContrato_Numero_Sel ,
                                       String AV44TFContratoClausulas_Item ,
                                       String AV45TFContratoClausulas_Item_Sel ,
                                       String AV48TFContratoClausulas_Descricao ,
                                       String AV49TFContratoClausulas_Descricao_Sel ,
                                       String AV42ddo_Contrato_NumeroTitleControlIdToReplace ,
                                       String AV46ddo_ContratoClausulas_ItemTitleControlIdToReplace ,
                                       String AV50ddo_ContratoClausulas_DescricaoTitleControlIdToReplace ,
                                       wwpbaseobjects.SdtWWPContext AV6WWPContext ,
                                       String AV86Pgmname ,
                                       int A152ContratoClausulas_Codigo ,
                                       int A74Contrato_Codigo )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RF6P2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOCLAUSULAS_ITEM", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A153ContratoClausulas_Item, ""))));
         GxWebStd.gx_hidden_field( context, "CONTRATOCLAUSULAS_ITEM", StringUtil.RTrim( A153ContratoClausulas_Item));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOCLAUSULAS_DESCRICAO", GetSecureSignedToken( "", A154ContratoClausulas_Descricao));
         GxWebStd.gx_hidden_field( context, "CONTRATOCLAUSULAS_DESCRICAO", A154ContratoClausulas_Descricao);
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF6P2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV86Pgmname = "WWContratoClausulas";
         context.Gx_err = 0;
      }

      protected void RF6P2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 36;
         /* Execute user event: E186P2 */
         E186P2 ();
         nGXsfl_36_idx = 1;
         sGXsfl_36_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_36_idx), 4, 0)), 4, "0");
         SubsflControlProps_362( ) ;
         nGXsfl_36_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_362( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV79WWContratoClausulasDS_2_Tfcontrato_numero_sel ,
                                                 AV78WWContratoClausulasDS_1_Tfcontrato_numero ,
                                                 AV81WWContratoClausulasDS_4_Tfcontratoclausulas_item_sel ,
                                                 AV80WWContratoClausulasDS_3_Tfcontratoclausulas_item ,
                                                 AV83WWContratoClausulasDS_6_Tfcontratoclausulas_descricao_sel ,
                                                 AV82WWContratoClausulasDS_5_Tfcontratoclausulas_descricao ,
                                                 A77Contrato_Numero ,
                                                 A153ContratoClausulas_Item ,
                                                 A154ContratoClausulas_Descricao ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT,
                                                 TypeConstants.BOOLEAN
                                                 }
            });
            lV78WWContratoClausulasDS_1_Tfcontrato_numero = StringUtil.PadR( StringUtil.RTrim( AV78WWContratoClausulasDS_1_Tfcontrato_numero), 20, "%");
            lV80WWContratoClausulasDS_3_Tfcontratoclausulas_item = StringUtil.PadR( StringUtil.RTrim( AV80WWContratoClausulasDS_3_Tfcontratoclausulas_item), 10, "%");
            lV82WWContratoClausulasDS_5_Tfcontratoclausulas_descricao = StringUtil.Concat( StringUtil.RTrim( AV82WWContratoClausulasDS_5_Tfcontratoclausulas_descricao), "%", "");
            /* Using cursor H006P2 */
            pr_default.execute(0, new Object[] {lV78WWContratoClausulasDS_1_Tfcontrato_numero, AV79WWContratoClausulasDS_2_Tfcontrato_numero_sel, lV80WWContratoClausulasDS_3_Tfcontratoclausulas_item, AV81WWContratoClausulasDS_4_Tfcontratoclausulas_item_sel, lV82WWContratoClausulasDS_5_Tfcontratoclausulas_descricao, AV83WWContratoClausulasDS_6_Tfcontratoclausulas_descricao_sel, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_36_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A74Contrato_Codigo = H006P2_A74Contrato_Codigo[0];
               A152ContratoClausulas_Codigo = H006P2_A152ContratoClausulas_Codigo[0];
               A154ContratoClausulas_Descricao = H006P2_A154ContratoClausulas_Descricao[0];
               A153ContratoClausulas_Item = H006P2_A153ContratoClausulas_Item[0];
               A77Contrato_Numero = H006P2_A77Contrato_Numero[0];
               A77Contrato_Numero = H006P2_A77Contrato_Numero[0];
               /* Execute user event: E196P2 */
               E196P2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 36;
            WB6P0( ) ;
         }
         nGXsfl_36_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         AV78WWContratoClausulasDS_1_Tfcontrato_numero = AV40TFContrato_Numero;
         AV79WWContratoClausulasDS_2_Tfcontrato_numero_sel = AV41TFContrato_Numero_Sel;
         AV80WWContratoClausulasDS_3_Tfcontratoclausulas_item = AV44TFContratoClausulas_Item;
         AV81WWContratoClausulasDS_4_Tfcontratoclausulas_item_sel = AV45TFContratoClausulas_Item_Sel;
         AV82WWContratoClausulasDS_5_Tfcontratoclausulas_descricao = AV48TFContratoClausulas_Descricao;
         AV83WWContratoClausulasDS_6_Tfcontratoclausulas_descricao_sel = AV49TFContratoClausulas_Descricao_Sel;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV79WWContratoClausulasDS_2_Tfcontrato_numero_sel ,
                                              AV78WWContratoClausulasDS_1_Tfcontrato_numero ,
                                              AV81WWContratoClausulasDS_4_Tfcontratoclausulas_item_sel ,
                                              AV80WWContratoClausulasDS_3_Tfcontratoclausulas_item ,
                                              AV83WWContratoClausulasDS_6_Tfcontratoclausulas_descricao_sel ,
                                              AV82WWContratoClausulasDS_5_Tfcontratoclausulas_descricao ,
                                              A77Contrato_Numero ,
                                              A153ContratoClausulas_Item ,
                                              A154ContratoClausulas_Descricao ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT,
                                              TypeConstants.BOOLEAN
                                              }
         });
         lV78WWContratoClausulasDS_1_Tfcontrato_numero = StringUtil.PadR( StringUtil.RTrim( AV78WWContratoClausulasDS_1_Tfcontrato_numero), 20, "%");
         lV80WWContratoClausulasDS_3_Tfcontratoclausulas_item = StringUtil.PadR( StringUtil.RTrim( AV80WWContratoClausulasDS_3_Tfcontratoclausulas_item), 10, "%");
         lV82WWContratoClausulasDS_5_Tfcontratoclausulas_descricao = StringUtil.Concat( StringUtil.RTrim( AV82WWContratoClausulasDS_5_Tfcontratoclausulas_descricao), "%", "");
         /* Using cursor H006P3 */
         pr_default.execute(1, new Object[] {lV78WWContratoClausulasDS_1_Tfcontrato_numero, AV79WWContratoClausulasDS_2_Tfcontrato_numero_sel, lV80WWContratoClausulasDS_3_Tfcontratoclausulas_item, AV81WWContratoClausulasDS_4_Tfcontratoclausulas_item_sel, lV82WWContratoClausulasDS_5_Tfcontratoclausulas_descricao, AV83WWContratoClausulasDS_6_Tfcontratoclausulas_descricao_sel});
         GRID_nRecordCount = H006P3_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         AV78WWContratoClausulasDS_1_Tfcontrato_numero = AV40TFContrato_Numero;
         AV79WWContratoClausulasDS_2_Tfcontrato_numero_sel = AV41TFContrato_Numero_Sel;
         AV80WWContratoClausulasDS_3_Tfcontratoclausulas_item = AV44TFContratoClausulas_Item;
         AV81WWContratoClausulasDS_4_Tfcontratoclausulas_item_sel = AV45TFContratoClausulas_Item_Sel;
         AV82WWContratoClausulasDS_5_Tfcontratoclausulas_descricao = AV48TFContratoClausulas_Descricao;
         AV83WWContratoClausulasDS_6_Tfcontratoclausulas_descricao_sel = AV49TFContratoClausulas_Descricao_Sel;
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV40TFContrato_Numero, AV41TFContrato_Numero_Sel, AV44TFContratoClausulas_Item, AV45TFContratoClausulas_Item_Sel, AV48TFContratoClausulas_Descricao, AV49TFContratoClausulas_Descricao_Sel, AV42ddo_Contrato_NumeroTitleControlIdToReplace, AV46ddo_ContratoClausulas_ItemTitleControlIdToReplace, AV50ddo_ContratoClausulas_DescricaoTitleControlIdToReplace, AV6WWPContext, AV86Pgmname, A152ContratoClausulas_Codigo, A74Contrato_Codigo) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         AV78WWContratoClausulasDS_1_Tfcontrato_numero = AV40TFContrato_Numero;
         AV79WWContratoClausulasDS_2_Tfcontrato_numero_sel = AV41TFContrato_Numero_Sel;
         AV80WWContratoClausulasDS_3_Tfcontratoclausulas_item = AV44TFContratoClausulas_Item;
         AV81WWContratoClausulasDS_4_Tfcontratoclausulas_item_sel = AV45TFContratoClausulas_Item_Sel;
         AV82WWContratoClausulasDS_5_Tfcontratoclausulas_descricao = AV48TFContratoClausulas_Descricao;
         AV83WWContratoClausulasDS_6_Tfcontratoclausulas_descricao_sel = AV49TFContratoClausulas_Descricao_Sel;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV40TFContrato_Numero, AV41TFContrato_Numero_Sel, AV44TFContratoClausulas_Item, AV45TFContratoClausulas_Item_Sel, AV48TFContratoClausulas_Descricao, AV49TFContratoClausulas_Descricao_Sel, AV42ddo_Contrato_NumeroTitleControlIdToReplace, AV46ddo_ContratoClausulas_ItemTitleControlIdToReplace, AV50ddo_ContratoClausulas_DescricaoTitleControlIdToReplace, AV6WWPContext, AV86Pgmname, A152ContratoClausulas_Codigo, A74Contrato_Codigo) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         AV78WWContratoClausulasDS_1_Tfcontrato_numero = AV40TFContrato_Numero;
         AV79WWContratoClausulasDS_2_Tfcontrato_numero_sel = AV41TFContrato_Numero_Sel;
         AV80WWContratoClausulasDS_3_Tfcontratoclausulas_item = AV44TFContratoClausulas_Item;
         AV81WWContratoClausulasDS_4_Tfcontratoclausulas_item_sel = AV45TFContratoClausulas_Item_Sel;
         AV82WWContratoClausulasDS_5_Tfcontratoclausulas_descricao = AV48TFContratoClausulas_Descricao;
         AV83WWContratoClausulasDS_6_Tfcontratoclausulas_descricao_sel = AV49TFContratoClausulas_Descricao_Sel;
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV40TFContrato_Numero, AV41TFContrato_Numero_Sel, AV44TFContratoClausulas_Item, AV45TFContratoClausulas_Item_Sel, AV48TFContratoClausulas_Descricao, AV49TFContratoClausulas_Descricao_Sel, AV42ddo_Contrato_NumeroTitleControlIdToReplace, AV46ddo_ContratoClausulas_ItemTitleControlIdToReplace, AV50ddo_ContratoClausulas_DescricaoTitleControlIdToReplace, AV6WWPContext, AV86Pgmname, A152ContratoClausulas_Codigo, A74Contrato_Codigo) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         AV78WWContratoClausulasDS_1_Tfcontrato_numero = AV40TFContrato_Numero;
         AV79WWContratoClausulasDS_2_Tfcontrato_numero_sel = AV41TFContrato_Numero_Sel;
         AV80WWContratoClausulasDS_3_Tfcontratoclausulas_item = AV44TFContratoClausulas_Item;
         AV81WWContratoClausulasDS_4_Tfcontratoclausulas_item_sel = AV45TFContratoClausulas_Item_Sel;
         AV82WWContratoClausulasDS_5_Tfcontratoclausulas_descricao = AV48TFContratoClausulas_Descricao;
         AV83WWContratoClausulasDS_6_Tfcontratoclausulas_descricao_sel = AV49TFContratoClausulas_Descricao_Sel;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV40TFContrato_Numero, AV41TFContrato_Numero_Sel, AV44TFContratoClausulas_Item, AV45TFContratoClausulas_Item_Sel, AV48TFContratoClausulas_Descricao, AV49TFContratoClausulas_Descricao_Sel, AV42ddo_Contrato_NumeroTitleControlIdToReplace, AV46ddo_ContratoClausulas_ItemTitleControlIdToReplace, AV50ddo_ContratoClausulas_DescricaoTitleControlIdToReplace, AV6WWPContext, AV86Pgmname, A152ContratoClausulas_Codigo, A74Contrato_Codigo) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         AV78WWContratoClausulasDS_1_Tfcontrato_numero = AV40TFContrato_Numero;
         AV79WWContratoClausulasDS_2_Tfcontrato_numero_sel = AV41TFContrato_Numero_Sel;
         AV80WWContratoClausulasDS_3_Tfcontratoclausulas_item = AV44TFContratoClausulas_Item;
         AV81WWContratoClausulasDS_4_Tfcontratoclausulas_item_sel = AV45TFContratoClausulas_Item_Sel;
         AV82WWContratoClausulasDS_5_Tfcontratoclausulas_descricao = AV48TFContratoClausulas_Descricao;
         AV83WWContratoClausulasDS_6_Tfcontratoclausulas_descricao_sel = AV49TFContratoClausulas_Descricao_Sel;
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV40TFContrato_Numero, AV41TFContrato_Numero_Sel, AV44TFContratoClausulas_Item, AV45TFContratoClausulas_Item_Sel, AV48TFContratoClausulas_Descricao, AV49TFContratoClausulas_Descricao_Sel, AV42ddo_Contrato_NumeroTitleControlIdToReplace, AV46ddo_ContratoClausulas_ItemTitleControlIdToReplace, AV50ddo_ContratoClausulas_DescricaoTitleControlIdToReplace, AV6WWPContext, AV86Pgmname, A152ContratoClausulas_Codigo, A74Contrato_Codigo) ;
         }
         return (int)(0) ;
      }

      protected void STRUP6P0( )
      {
         /* Before Start, stand alone formulas. */
         AV86Pgmname = "WWContratoClausulas";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E176P2 */
         E176P2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV51DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATO_NUMEROTITLEFILTERDATA"), AV39Contrato_NumeroTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATOCLAUSULAS_ITEMTITLEFILTERDATA"), AV43ContratoClausulas_ItemTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATOCLAUSULAS_DESCRICAOTITLEFILTERDATA"), AV47ContratoClausulas_DescricaoTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV13OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            AV40TFContrato_Numero = cgiGet( edtavTfcontrato_numero_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFContrato_Numero", AV40TFContrato_Numero);
            AV41TFContrato_Numero_Sel = cgiGet( edtavTfcontrato_numero_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41TFContrato_Numero_Sel", AV41TFContrato_Numero_Sel);
            AV44TFContratoClausulas_Item = cgiGet( edtavTfcontratoclausulas_item_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFContratoClausulas_Item", AV44TFContratoClausulas_Item);
            AV45TFContratoClausulas_Item_Sel = cgiGet( edtavTfcontratoclausulas_item_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFContratoClausulas_Item_Sel", AV45TFContratoClausulas_Item_Sel);
            AV48TFContratoClausulas_Descricao = cgiGet( edtavTfcontratoclausulas_descricao_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48TFContratoClausulas_Descricao", AV48TFContratoClausulas_Descricao);
            AV49TFContratoClausulas_Descricao_Sel = cgiGet( edtavTfcontratoclausulas_descricao_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFContratoClausulas_Descricao_Sel", AV49TFContratoClausulas_Descricao_Sel);
            AV42ddo_Contrato_NumeroTitleControlIdToReplace = cgiGet( edtavDdo_contrato_numerotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42ddo_Contrato_NumeroTitleControlIdToReplace", AV42ddo_Contrato_NumeroTitleControlIdToReplace);
            AV46ddo_ContratoClausulas_ItemTitleControlIdToReplace = cgiGet( edtavDdo_contratoclausulas_itemtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46ddo_ContratoClausulas_ItemTitleControlIdToReplace", AV46ddo_ContratoClausulas_ItemTitleControlIdToReplace);
            AV50ddo_ContratoClausulas_DescricaoTitleControlIdToReplace = cgiGet( edtavDdo_contratoclausulas_descricaotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50ddo_ContratoClausulas_DescricaoTitleControlIdToReplace", AV50ddo_ContratoClausulas_DescricaoTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_36 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_36"), ",", "."));
            AV53GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV54GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_contrato_numero_Caption = cgiGet( "DDO_CONTRATO_NUMERO_Caption");
            Ddo_contrato_numero_Tooltip = cgiGet( "DDO_CONTRATO_NUMERO_Tooltip");
            Ddo_contrato_numero_Cls = cgiGet( "DDO_CONTRATO_NUMERO_Cls");
            Ddo_contrato_numero_Filteredtext_set = cgiGet( "DDO_CONTRATO_NUMERO_Filteredtext_set");
            Ddo_contrato_numero_Selectedvalue_set = cgiGet( "DDO_CONTRATO_NUMERO_Selectedvalue_set");
            Ddo_contrato_numero_Dropdownoptionstype = cgiGet( "DDO_CONTRATO_NUMERO_Dropdownoptionstype");
            Ddo_contrato_numero_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATO_NUMERO_Titlecontrolidtoreplace");
            Ddo_contrato_numero_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATO_NUMERO_Includesortasc"));
            Ddo_contrato_numero_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATO_NUMERO_Includesortdsc"));
            Ddo_contrato_numero_Sortedstatus = cgiGet( "DDO_CONTRATO_NUMERO_Sortedstatus");
            Ddo_contrato_numero_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATO_NUMERO_Includefilter"));
            Ddo_contrato_numero_Filtertype = cgiGet( "DDO_CONTRATO_NUMERO_Filtertype");
            Ddo_contrato_numero_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATO_NUMERO_Filterisrange"));
            Ddo_contrato_numero_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATO_NUMERO_Includedatalist"));
            Ddo_contrato_numero_Datalisttype = cgiGet( "DDO_CONTRATO_NUMERO_Datalisttype");
            Ddo_contrato_numero_Datalistproc = cgiGet( "DDO_CONTRATO_NUMERO_Datalistproc");
            Ddo_contrato_numero_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTRATO_NUMERO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contrato_numero_Sortasc = cgiGet( "DDO_CONTRATO_NUMERO_Sortasc");
            Ddo_contrato_numero_Sortdsc = cgiGet( "DDO_CONTRATO_NUMERO_Sortdsc");
            Ddo_contrato_numero_Loadingdata = cgiGet( "DDO_CONTRATO_NUMERO_Loadingdata");
            Ddo_contrato_numero_Cleanfilter = cgiGet( "DDO_CONTRATO_NUMERO_Cleanfilter");
            Ddo_contrato_numero_Noresultsfound = cgiGet( "DDO_CONTRATO_NUMERO_Noresultsfound");
            Ddo_contrato_numero_Searchbuttontext = cgiGet( "DDO_CONTRATO_NUMERO_Searchbuttontext");
            Ddo_contratoclausulas_item_Caption = cgiGet( "DDO_CONTRATOCLAUSULAS_ITEM_Caption");
            Ddo_contratoclausulas_item_Tooltip = cgiGet( "DDO_CONTRATOCLAUSULAS_ITEM_Tooltip");
            Ddo_contratoclausulas_item_Cls = cgiGet( "DDO_CONTRATOCLAUSULAS_ITEM_Cls");
            Ddo_contratoclausulas_item_Filteredtext_set = cgiGet( "DDO_CONTRATOCLAUSULAS_ITEM_Filteredtext_set");
            Ddo_contratoclausulas_item_Selectedvalue_set = cgiGet( "DDO_CONTRATOCLAUSULAS_ITEM_Selectedvalue_set");
            Ddo_contratoclausulas_item_Dropdownoptionstype = cgiGet( "DDO_CONTRATOCLAUSULAS_ITEM_Dropdownoptionstype");
            Ddo_contratoclausulas_item_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATOCLAUSULAS_ITEM_Titlecontrolidtoreplace");
            Ddo_contratoclausulas_item_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOCLAUSULAS_ITEM_Includesortasc"));
            Ddo_contratoclausulas_item_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOCLAUSULAS_ITEM_Includesortdsc"));
            Ddo_contratoclausulas_item_Sortedstatus = cgiGet( "DDO_CONTRATOCLAUSULAS_ITEM_Sortedstatus");
            Ddo_contratoclausulas_item_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOCLAUSULAS_ITEM_Includefilter"));
            Ddo_contratoclausulas_item_Filtertype = cgiGet( "DDO_CONTRATOCLAUSULAS_ITEM_Filtertype");
            Ddo_contratoclausulas_item_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOCLAUSULAS_ITEM_Filterisrange"));
            Ddo_contratoclausulas_item_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOCLAUSULAS_ITEM_Includedatalist"));
            Ddo_contratoclausulas_item_Datalisttype = cgiGet( "DDO_CONTRATOCLAUSULAS_ITEM_Datalisttype");
            Ddo_contratoclausulas_item_Datalistproc = cgiGet( "DDO_CONTRATOCLAUSULAS_ITEM_Datalistproc");
            Ddo_contratoclausulas_item_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTRATOCLAUSULAS_ITEM_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contratoclausulas_item_Sortasc = cgiGet( "DDO_CONTRATOCLAUSULAS_ITEM_Sortasc");
            Ddo_contratoclausulas_item_Sortdsc = cgiGet( "DDO_CONTRATOCLAUSULAS_ITEM_Sortdsc");
            Ddo_contratoclausulas_item_Loadingdata = cgiGet( "DDO_CONTRATOCLAUSULAS_ITEM_Loadingdata");
            Ddo_contratoclausulas_item_Cleanfilter = cgiGet( "DDO_CONTRATOCLAUSULAS_ITEM_Cleanfilter");
            Ddo_contratoclausulas_item_Noresultsfound = cgiGet( "DDO_CONTRATOCLAUSULAS_ITEM_Noresultsfound");
            Ddo_contratoclausulas_item_Searchbuttontext = cgiGet( "DDO_CONTRATOCLAUSULAS_ITEM_Searchbuttontext");
            Ddo_contratoclausulas_descricao_Caption = cgiGet( "DDO_CONTRATOCLAUSULAS_DESCRICAO_Caption");
            Ddo_contratoclausulas_descricao_Tooltip = cgiGet( "DDO_CONTRATOCLAUSULAS_DESCRICAO_Tooltip");
            Ddo_contratoclausulas_descricao_Cls = cgiGet( "DDO_CONTRATOCLAUSULAS_DESCRICAO_Cls");
            Ddo_contratoclausulas_descricao_Filteredtext_set = cgiGet( "DDO_CONTRATOCLAUSULAS_DESCRICAO_Filteredtext_set");
            Ddo_contratoclausulas_descricao_Selectedvalue_set = cgiGet( "DDO_CONTRATOCLAUSULAS_DESCRICAO_Selectedvalue_set");
            Ddo_contratoclausulas_descricao_Dropdownoptionstype = cgiGet( "DDO_CONTRATOCLAUSULAS_DESCRICAO_Dropdownoptionstype");
            Ddo_contratoclausulas_descricao_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATOCLAUSULAS_DESCRICAO_Titlecontrolidtoreplace");
            Ddo_contratoclausulas_descricao_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOCLAUSULAS_DESCRICAO_Includesortasc"));
            Ddo_contratoclausulas_descricao_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOCLAUSULAS_DESCRICAO_Includesortdsc"));
            Ddo_contratoclausulas_descricao_Sortedstatus = cgiGet( "DDO_CONTRATOCLAUSULAS_DESCRICAO_Sortedstatus");
            Ddo_contratoclausulas_descricao_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOCLAUSULAS_DESCRICAO_Includefilter"));
            Ddo_contratoclausulas_descricao_Filtertype = cgiGet( "DDO_CONTRATOCLAUSULAS_DESCRICAO_Filtertype");
            Ddo_contratoclausulas_descricao_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOCLAUSULAS_DESCRICAO_Filterisrange"));
            Ddo_contratoclausulas_descricao_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOCLAUSULAS_DESCRICAO_Includedatalist"));
            Ddo_contratoclausulas_descricao_Datalisttype = cgiGet( "DDO_CONTRATOCLAUSULAS_DESCRICAO_Datalisttype");
            Ddo_contratoclausulas_descricao_Datalistproc = cgiGet( "DDO_CONTRATOCLAUSULAS_DESCRICAO_Datalistproc");
            Ddo_contratoclausulas_descricao_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTRATOCLAUSULAS_DESCRICAO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contratoclausulas_descricao_Sortasc = cgiGet( "DDO_CONTRATOCLAUSULAS_DESCRICAO_Sortasc");
            Ddo_contratoclausulas_descricao_Sortdsc = cgiGet( "DDO_CONTRATOCLAUSULAS_DESCRICAO_Sortdsc");
            Ddo_contratoclausulas_descricao_Loadingdata = cgiGet( "DDO_CONTRATOCLAUSULAS_DESCRICAO_Loadingdata");
            Ddo_contratoclausulas_descricao_Cleanfilter = cgiGet( "DDO_CONTRATOCLAUSULAS_DESCRICAO_Cleanfilter");
            Ddo_contratoclausulas_descricao_Noresultsfound = cgiGet( "DDO_CONTRATOCLAUSULAS_DESCRICAO_Noresultsfound");
            Ddo_contratoclausulas_descricao_Searchbuttontext = cgiGet( "DDO_CONTRATOCLAUSULAS_DESCRICAO_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_contrato_numero_Activeeventkey = cgiGet( "DDO_CONTRATO_NUMERO_Activeeventkey");
            Ddo_contrato_numero_Filteredtext_get = cgiGet( "DDO_CONTRATO_NUMERO_Filteredtext_get");
            Ddo_contrato_numero_Selectedvalue_get = cgiGet( "DDO_CONTRATO_NUMERO_Selectedvalue_get");
            Ddo_contratoclausulas_item_Activeeventkey = cgiGet( "DDO_CONTRATOCLAUSULAS_ITEM_Activeeventkey");
            Ddo_contratoclausulas_item_Filteredtext_get = cgiGet( "DDO_CONTRATOCLAUSULAS_ITEM_Filteredtext_get");
            Ddo_contratoclausulas_item_Selectedvalue_get = cgiGet( "DDO_CONTRATOCLAUSULAS_ITEM_Selectedvalue_get");
            Ddo_contratoclausulas_descricao_Activeeventkey = cgiGet( "DDO_CONTRATOCLAUSULAS_DESCRICAO_Activeeventkey");
            Ddo_contratoclausulas_descricao_Filteredtext_get = cgiGet( "DDO_CONTRATOCLAUSULAS_DESCRICAO_Filteredtext_get");
            Ddo_contratoclausulas_descricao_Selectedvalue_get = cgiGet( "DDO_CONTRATOCLAUSULAS_DESCRICAO_Selectedvalue_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATO_NUMERO"), AV40TFContrato_Numero) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATO_NUMERO_SEL"), AV41TFContrato_Numero_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOCLAUSULAS_ITEM"), AV44TFContratoClausulas_Item) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOCLAUSULAS_ITEM_SEL"), AV45TFContratoClausulas_Item_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOCLAUSULAS_DESCRICAO"), AV48TFContratoClausulas_Descricao) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATOCLAUSULAS_DESCRICAO_SEL"), AV49TFContratoClausulas_Descricao_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E176P2 */
         E176P2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E176P2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         edtavTfcontrato_numero_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontrato_numero_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontrato_numero_Visible), 5, 0)));
         edtavTfcontrato_numero_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontrato_numero_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontrato_numero_sel_Visible), 5, 0)));
         edtavTfcontratoclausulas_item_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoclausulas_item_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoclausulas_item_Visible), 5, 0)));
         edtavTfcontratoclausulas_item_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoclausulas_item_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoclausulas_item_sel_Visible), 5, 0)));
         edtavTfcontratoclausulas_descricao_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoclausulas_descricao_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoclausulas_descricao_Visible), 5, 0)));
         edtavTfcontratoclausulas_descricao_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratoclausulas_descricao_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoclausulas_descricao_sel_Visible), 5, 0)));
         Ddo_contrato_numero_Titlecontrolidtoreplace = subGrid_Internalname+"_Contrato_Numero";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_numero_Internalname, "TitleControlIdToReplace", Ddo_contrato_numero_Titlecontrolidtoreplace);
         AV42ddo_Contrato_NumeroTitleControlIdToReplace = Ddo_contrato_numero_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42ddo_Contrato_NumeroTitleControlIdToReplace", AV42ddo_Contrato_NumeroTitleControlIdToReplace);
         edtavDdo_contrato_numerotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contrato_numerotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contrato_numerotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratoclausulas_item_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoClausulas_Item";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoclausulas_item_Internalname, "TitleControlIdToReplace", Ddo_contratoclausulas_item_Titlecontrolidtoreplace);
         AV46ddo_ContratoClausulas_ItemTitleControlIdToReplace = Ddo_contratoclausulas_item_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46ddo_ContratoClausulas_ItemTitleControlIdToReplace", AV46ddo_ContratoClausulas_ItemTitleControlIdToReplace);
         edtavDdo_contratoclausulas_itemtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratoclausulas_itemtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratoclausulas_itemtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratoclausulas_descricao_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoClausulas_Descricao";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoclausulas_descricao_Internalname, "TitleControlIdToReplace", Ddo_contratoclausulas_descricao_Titlecontrolidtoreplace);
         AV50ddo_ContratoClausulas_DescricaoTitleControlIdToReplace = Ddo_contratoclausulas_descricao_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50ddo_ContratoClausulas_DescricaoTitleControlIdToReplace", AV50ddo_ContratoClausulas_DescricaoTitleControlIdToReplace);
         edtavDdo_contratoclausulas_descricaotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratoclausulas_descricaotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratoclausulas_descricaotitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = " Contrato Clausulas";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Item", 0);
         cmbavOrderedby.addItem("2", "N� Contrato", 0);
         cmbavOrderedby.addItem("3", "Descri��o", 0);
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S132 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV51DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV51DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E186P2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV39Contrato_NumeroTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV43ContratoClausulas_ItemTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV47ContratoClausulas_DescricaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'CHECKSECURITYFORACTIONS' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtContrato_Numero_Titleformat = 2;
         edtContrato_Numero_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "N� Contrato", AV42ddo_Contrato_NumeroTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContrato_Numero_Internalname, "Title", edtContrato_Numero_Title);
         edtContratoClausulas_Item_Titleformat = 2;
         edtContratoClausulas_Item_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Item", AV46ddo_ContratoClausulas_ItemTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoClausulas_Item_Internalname, "Title", edtContratoClausulas_Item_Title);
         edtContratoClausulas_Descricao_Titleformat = 2;
         edtContratoClausulas_Descricao_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Descri��o", AV50ddo_ContratoClausulas_DescricaoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoClausulas_Descricao_Internalname, "Title", edtContratoClausulas_Descricao_Title);
         AV53GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV53GridCurrentPage), 10, 0)));
         AV54GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV54GridPageCount), 10, 0)));
         AV78WWContratoClausulasDS_1_Tfcontrato_numero = AV40TFContrato_Numero;
         AV79WWContratoClausulasDS_2_Tfcontrato_numero_sel = AV41TFContrato_Numero_Sel;
         AV80WWContratoClausulasDS_3_Tfcontratoclausulas_item = AV44TFContratoClausulas_Item;
         AV81WWContratoClausulasDS_4_Tfcontratoclausulas_item_sel = AV45TFContratoClausulas_Item_Sel;
         AV82WWContratoClausulasDS_5_Tfcontratoclausulas_descricao = AV48TFContratoClausulas_Descricao;
         AV83WWContratoClausulasDS_6_Tfcontratoclausulas_descricao_sel = AV49TFContratoClausulas_Descricao_Sel;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV39Contrato_NumeroTitleFilterData", AV39Contrato_NumeroTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV43ContratoClausulas_ItemTitleFilterData", AV43ContratoClausulas_ItemTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV47ContratoClausulas_DescricaoTitleFilterData", AV47ContratoClausulas_DescricaoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV6WWPContext", AV6WWPContext);
      }

      protected void E116P2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV52PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV52PageToGo) ;
         }
      }

      protected void E126P2( )
      {
         /* Ddo_contrato_numero_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contrato_numero_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contrato_numero_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_numero_Internalname, "SortedStatus", Ddo_contrato_numero_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contrato_numero_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contrato_numero_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_numero_Internalname, "SortedStatus", Ddo_contrato_numero_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contrato_numero_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV40TFContrato_Numero = Ddo_contrato_numero_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFContrato_Numero", AV40TFContrato_Numero);
            AV41TFContrato_Numero_Sel = Ddo_contrato_numero_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41TFContrato_Numero_Sel", AV41TFContrato_Numero_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E136P2( )
      {
         /* Ddo_contratoclausulas_item_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratoclausulas_item_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoclausulas_item_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoclausulas_item_Internalname, "SortedStatus", Ddo_contratoclausulas_item_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoclausulas_item_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoclausulas_item_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoclausulas_item_Internalname, "SortedStatus", Ddo_contratoclausulas_item_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoclausulas_item_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV44TFContratoClausulas_Item = Ddo_contratoclausulas_item_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFContratoClausulas_Item", AV44TFContratoClausulas_Item);
            AV45TFContratoClausulas_Item_Sel = Ddo_contratoclausulas_item_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFContratoClausulas_Item_Sel", AV45TFContratoClausulas_Item_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E146P2( )
      {
         /* Ddo_contratoclausulas_descricao_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratoclausulas_descricao_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoclausulas_descricao_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoclausulas_descricao_Internalname, "SortedStatus", Ddo_contratoclausulas_descricao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoclausulas_descricao_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoclausulas_descricao_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoclausulas_descricao_Internalname, "SortedStatus", Ddo_contratoclausulas_descricao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratoclausulas_descricao_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV48TFContratoClausulas_Descricao = Ddo_contratoclausulas_descricao_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48TFContratoClausulas_Descricao", AV48TFContratoClausulas_Descricao);
            AV49TFContratoClausulas_Descricao_Sel = Ddo_contratoclausulas_descricao_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFContratoClausulas_Descricao_Sel", AV49TFContratoClausulas_Descricao_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      private void E196P2( )
      {
         /* Grid_Load Routine */
         AV28Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUpdate_Internalname, AV28Update);
         AV84Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = formatLink("contratoclausulas.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A152ContratoClausulas_Codigo) + "," + UrlEncode("" +A74Contrato_Codigo);
         AV29Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDelete_Internalname, AV29Delete);
         AV85Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = formatLink("contratoclausulas.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A152ContratoClausulas_Codigo) + "," + UrlEncode("" +A74Contrato_Codigo);
         edtContratoClausulas_Item_Link = formatLink("viewcontratoclausulas.aspx") + "?" + UrlEncode("" +A152ContratoClausulas_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 36;
         }
         sendrow_362( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_36_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(36, GridRow);
         }
      }

      protected void E156P2( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E166P2( )
      {
         /* 'DoInsert' Routine */
         context.wjLoc = formatLink("contratoclausulas.aspx") + "?" + UrlEncode(StringUtil.RTrim("INS")) + "," + UrlEncode("" +0) + "," + UrlEncode("" +0);
         context.wjLocDisableFrm = 1;
      }

      protected void S162( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_contrato_numero_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_numero_Internalname, "SortedStatus", Ddo_contrato_numero_Sortedstatus);
         Ddo_contratoclausulas_item_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoclausulas_item_Internalname, "SortedStatus", Ddo_contratoclausulas_item_Sortedstatus);
         Ddo_contratoclausulas_descricao_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoclausulas_descricao_Internalname, "SortedStatus", Ddo_contratoclausulas_descricao_Sortedstatus);
      }

      protected void S132( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 2 )
         {
            Ddo_contrato_numero_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_numero_Internalname, "SortedStatus", Ddo_contrato_numero_Sortedstatus);
         }
         else if ( AV13OrderedBy == 1 )
         {
            Ddo_contratoclausulas_item_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoclausulas_item_Internalname, "SortedStatus", Ddo_contratoclausulas_item_Sortedstatus);
         }
         else if ( AV13OrderedBy == 3 )
         {
            Ddo_contratoclausulas_descricao_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoclausulas_descricao_Internalname, "SortedStatus", Ddo_contratoclausulas_descricao_Sortedstatus);
         }
      }

      protected void S142( )
      {
         /* 'CHECKSECURITYFORACTIONS' Routine */
         if ( ! ( AV6WWPContext.gxTpr_Insert ) )
         {
            imgInsert_Link = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgInsert_Internalname, "Link", imgInsert_Link);
            imgInsert_Bitmap = context.GetImagePath( "13d28f37-c579-4dd9-a404-ba51ab71d1e3", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgInsert_Internalname, "Bitmap", context.convertURL( context.PathToRelativeUrl( imgInsert_Bitmap)));
            imgInsert_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgInsert_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgInsert_Enabled), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV30Session.Get(AV86Pgmname+"GridState"), "") == 0 )
         {
            AV10GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV86Pgmname+"GridState"), "");
         }
         else
         {
            AV10GridState.FromXml(AV30Session.Get(AV86Pgmname+"GridState"), "");
         }
         AV13OrderedBy = AV10GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         AV14OrderedDsc = AV10GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV87GXV1 = 1;
         while ( AV87GXV1 <= AV10GridState.gxTpr_Filtervalues.Count )
         {
            AV11GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV10GridState.gxTpr_Filtervalues.Item(AV87GXV1));
            if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATO_NUMERO") == 0 )
            {
               AV40TFContrato_Numero = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFContrato_Numero", AV40TFContrato_Numero);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40TFContrato_Numero)) )
               {
                  Ddo_contrato_numero_Filteredtext_set = AV40TFContrato_Numero;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_numero_Internalname, "FilteredText_set", Ddo_contrato_numero_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATO_NUMERO_SEL") == 0 )
            {
               AV41TFContrato_Numero_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41TFContrato_Numero_Sel", AV41TFContrato_Numero_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV41TFContrato_Numero_Sel)) )
               {
                  Ddo_contrato_numero_Selectedvalue_set = AV41TFContrato_Numero_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_numero_Internalname, "SelectedValue_set", Ddo_contrato_numero_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATOCLAUSULAS_ITEM") == 0 )
            {
               AV44TFContratoClausulas_Item = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFContratoClausulas_Item", AV44TFContratoClausulas_Item);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44TFContratoClausulas_Item)) )
               {
                  Ddo_contratoclausulas_item_Filteredtext_set = AV44TFContratoClausulas_Item;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoclausulas_item_Internalname, "FilteredText_set", Ddo_contratoclausulas_item_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATOCLAUSULAS_ITEM_SEL") == 0 )
            {
               AV45TFContratoClausulas_Item_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFContratoClausulas_Item_Sel", AV45TFContratoClausulas_Item_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV45TFContratoClausulas_Item_Sel)) )
               {
                  Ddo_contratoclausulas_item_Selectedvalue_set = AV45TFContratoClausulas_Item_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoclausulas_item_Internalname, "SelectedValue_set", Ddo_contratoclausulas_item_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATOCLAUSULAS_DESCRICAO") == 0 )
            {
               AV48TFContratoClausulas_Descricao = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48TFContratoClausulas_Descricao", AV48TFContratoClausulas_Descricao);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48TFContratoClausulas_Descricao)) )
               {
                  Ddo_contratoclausulas_descricao_Filteredtext_set = AV48TFContratoClausulas_Descricao;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoclausulas_descricao_Internalname, "FilteredText_set", Ddo_contratoclausulas_descricao_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATOCLAUSULAS_DESCRICAO_SEL") == 0 )
            {
               AV49TFContratoClausulas_Descricao_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFContratoClausulas_Descricao_Sel", AV49TFContratoClausulas_Descricao_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49TFContratoClausulas_Descricao_Sel)) )
               {
                  Ddo_contratoclausulas_descricao_Selectedvalue_set = AV49TFContratoClausulas_Descricao_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratoclausulas_descricao_Internalname, "SelectedValue_set", Ddo_contratoclausulas_descricao_Selectedvalue_set);
               }
            }
            AV87GXV1 = (int)(AV87GXV1+1);
         }
         subgrid_gotopage( AV10GridState.gxTpr_Currentpage) ;
      }

      protected void S152( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV10GridState.FromXml(AV30Session.Get(AV86Pgmname+"GridState"), "");
         AV10GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV10GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV10GridState.gxTpr_Filtervalues.Clear();
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40TFContrato_Numero)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATO_NUMERO";
            AV11GridStateFilterValue.gxTpr_Value = AV40TFContrato_Numero;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV41TFContrato_Numero_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATO_NUMERO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV41TFContrato_Numero_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44TFContratoClausulas_Item)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOCLAUSULAS_ITEM";
            AV11GridStateFilterValue.gxTpr_Value = AV44TFContratoClausulas_Item;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV45TFContratoClausulas_Item_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOCLAUSULAS_ITEM_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV45TFContratoClausulas_Item_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48TFContratoClausulas_Descricao)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOCLAUSULAS_DESCRICAO";
            AV11GridStateFilterValue.gxTpr_Value = AV48TFContratoClausulas_Descricao;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49TFContratoClausulas_Descricao_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOCLAUSULAS_DESCRICAO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV49TFContratoClausulas_Descricao_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         AV10GridState.gxTpr_Currentpage = (short)(subGrid_Currentpage( ));
         new wwpbaseobjects.savegridstate(context ).execute(  AV86Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_MeetrikaVs3")) ;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV86Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = true;
         AV8TrnContext.gxTpr_Callerurl = AV7HTTPRequest.ScriptName+"?"+AV7HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "ContratoClausulas";
         AV30Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_MeetrikaVs3"));
      }

      protected void wb_table1_2_6P2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_6P2( true) ;
         }
         else
         {
            wb_table2_8_6P2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_6P2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_27_6P2( true) ;
         }
         else
         {
            wb_table3_27_6P2( false) ;
         }
         return  ;
      }

      protected void wb_table3_27_6P2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_6P2e( true) ;
         }
         else
         {
            wb_table1_2_6P2e( false) ;
         }
      }

      protected void wb_table3_27_6P2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "TableGridHeader", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_30_6P2( true) ;
         }
         else
         {
            wb_table4_30_6P2( false) ;
         }
         return  ;
      }

      protected void wb_table4_30_6P2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_27_6P2e( true) ;
         }
         else
         {
            wb_table3_27_6P2e( false) ;
         }
      }

      protected void wb_table4_30_6P2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedgrid_Internalname, tblTablemergedgrid_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table5_33_6P2( true) ;
         }
         else
         {
            wb_table5_33_6P2( false) ;
         }
         return  ;
      }

      protected void wb_table5_33_6P2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgInsert_Internalname, imgInsert_Bitmap, "", "", "", context.GetTheme( ), 1, imgInsert_Enabled, "", "Inserir", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgInsert_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOINSERT\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWContratoClausulas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_30_6P2e( true) ;
         }
         else
         {
            wb_table4_30_6P2e( false) ;
         }
      }

      protected void wb_table5_33_6P2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"36\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(110), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContrato_Numero_Titleformat == 0 )
               {
                  context.SendWebValue( edtContrato_Numero_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContrato_Numero_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoClausulas_Item_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoClausulas_Item_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoClausulas_Item_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoClausulas_Descricao_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoClausulas_Descricao_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoClausulas_Descricao_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV28Update));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavUpdate_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV29Delete));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDelete_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A77Contrato_Numero));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContrato_Numero_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContrato_Numero_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A153ContratoClausulas_Item));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoClausulas_Item_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoClausulas_Item_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtContratoClausulas_Item_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A154ContratoClausulas_Descricao);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoClausulas_Descricao_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoClausulas_Descricao_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 36 )
         {
            wbEnd = 0;
            nRC_GXsfl_36 = (short)(nGXsfl_36_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_33_6P2e( true) ;
         }
         else
         {
            wb_table5_33_6P2e( false) ;
         }
      }

      protected void wb_table2_8_6P2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableheader_Internalname, tblTableheader_Internalname, "", "TableSearch", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='GridHeaderCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblContratoclausulastitle_Internalname, "Cl�usulas do Contrato", "", "", lblContratoclausulastitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WWContratoClausulas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='Width100'>") ;
            wb_table6_13_6P2( true) ;
         }
         else
         {
            wb_table6_13_6P2( false) ;
         }
         return  ;
      }

      protected void wb_table6_13_6P2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_WWContratoClausulas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'',false,'" + sGXsfl_36_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,19);\"", "", true, "HLP_WWContratoClausulas.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'" + sGXsfl_36_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,20);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_WWContratoClausulas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table7_22_6P2( true) ;
         }
         else
         {
            wb_table7_22_6P2( false) ;
         }
         return  ;
      }

      protected void wb_table7_22_6P2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_6P2e( true) ;
         }
         else
         {
            wb_table2_8_6P2e( false) ;
         }
      }

      protected void wb_table7_22_6P2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_22_6P2e( true) ;
         }
         else
         {
            wb_table7_22_6P2e( false) ;
         }
      }

      protected void wb_table6_13_6P2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_13_6P2e( true) ;
         }
         else
         {
            wb_table6_13_6P2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA6P2( ) ;
         WS6P2( ) ;
         WE6P2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202051812594784");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wwcontratoclausulas.js", "?202051812594784");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_362( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_36_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_36_idx;
         edtContrato_Numero_Internalname = "CONTRATO_NUMERO_"+sGXsfl_36_idx;
         edtContratoClausulas_Item_Internalname = "CONTRATOCLAUSULAS_ITEM_"+sGXsfl_36_idx;
         edtContratoClausulas_Descricao_Internalname = "CONTRATOCLAUSULAS_DESCRICAO_"+sGXsfl_36_idx;
      }

      protected void SubsflControlProps_fel_362( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_36_fel_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_36_fel_idx;
         edtContrato_Numero_Internalname = "CONTRATO_NUMERO_"+sGXsfl_36_fel_idx;
         edtContratoClausulas_Item_Internalname = "CONTRATOCLAUSULAS_ITEM_"+sGXsfl_36_fel_idx;
         edtContratoClausulas_Descricao_Internalname = "CONTRATOCLAUSULAS_DESCRICAO_"+sGXsfl_36_fel_idx;
      }

      protected void sendrow_362( )
      {
         SubsflControlProps_362( ) ;
         WB6P0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_36_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_36_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_36_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV28Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV28Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV84Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV28Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV28Update)) ? AV84Update_GXI : context.PathToRelativeUrl( AV28Update)),(String)edtavUpdate_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV28Update_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV29Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV85Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete)) ? AV85Delete_GXI : context.PathToRelativeUrl( AV29Delete)),(String)edtavDelete_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV29Delete_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContrato_Numero_Internalname,StringUtil.RTrim( A77Contrato_Numero),(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContrato_Numero_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)110,(String)"px",(short)17,(String)"px",(short)20,(short)0,(short)0,(short)36,(short)1,(short)-1,(short)-1,(bool)true,(String)"NumeroContrato",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoClausulas_Item_Internalname,StringUtil.RTrim( A153ContratoClausulas_Item),(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)edtContratoClausulas_Item_Link,(String)"",(String)"",(String)"",(String)edtContratoClausulas_Item_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)10,(short)0,(short)0,(short)36,(short)1,(short)-1,(short)-1,(bool)true,(String)"ItemClausula",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoClausulas_Descricao_Internalname,(String)A154ContratoClausulas_Descricao,(String)A154ContratoClausulas_Descricao,(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoClausulas_Descricao_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)500,(short)0,(short)0,(short)36,(short)1,(short)0,(short)-1,(bool)true,(String)"DescricaoLonga",(String)"left",(bool)false});
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOCLAUSULAS_ITEM"+"_"+sGXsfl_36_idx, GetSecureSignedToken( sGXsfl_36_idx, StringUtil.RTrim( context.localUtil.Format( A153ContratoClausulas_Item, ""))));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOCLAUSULAS_DESCRICAO"+"_"+sGXsfl_36_idx, GetSecureSignedToken( sGXsfl_36_idx, A154ContratoClausulas_Descricao));
            GridContainer.AddRow(GridRow);
            nGXsfl_36_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_36_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_36_idx+1));
            sGXsfl_36_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_36_idx), 4, 0)), 4, "0");
            SubsflControlProps_362( ) ;
         }
         /* End function sendrow_362 */
      }

      protected void init_default_properties( )
      {
         lblContratoclausulastitle_Internalname = "CONTRATOCLAUSULASTITLE";
         tblTableactions_Internalname = "TABLEACTIONS";
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTableheader_Internalname = "TABLEHEADER";
         edtavUpdate_Internalname = "vUPDATE";
         edtavDelete_Internalname = "vDELETE";
         edtContrato_Numero_Internalname = "CONTRATO_NUMERO";
         edtContratoClausulas_Item_Internalname = "CONTRATOCLAUSULAS_ITEM";
         edtContratoClausulas_Descricao_Internalname = "CONTRATOCLAUSULAS_DESCRICAO";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         imgInsert_Internalname = "INSERT";
         tblTablemergedgrid_Internalname = "TABLEMERGEDGRID";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         edtavTfcontrato_numero_Internalname = "vTFCONTRATO_NUMERO";
         edtavTfcontrato_numero_sel_Internalname = "vTFCONTRATO_NUMERO_SEL";
         edtavTfcontratoclausulas_item_Internalname = "vTFCONTRATOCLAUSULAS_ITEM";
         edtavTfcontratoclausulas_item_sel_Internalname = "vTFCONTRATOCLAUSULAS_ITEM_SEL";
         edtavTfcontratoclausulas_descricao_Internalname = "vTFCONTRATOCLAUSULAS_DESCRICAO";
         edtavTfcontratoclausulas_descricao_sel_Internalname = "vTFCONTRATOCLAUSULAS_DESCRICAO_SEL";
         Ddo_contrato_numero_Internalname = "DDO_CONTRATO_NUMERO";
         edtavDdo_contrato_numerotitlecontrolidtoreplace_Internalname = "vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE";
         Ddo_contratoclausulas_item_Internalname = "DDO_CONTRATOCLAUSULAS_ITEM";
         edtavDdo_contratoclausulas_itemtitlecontrolidtoreplace_Internalname = "vDDO_CONTRATOCLAUSULAS_ITEMTITLECONTROLIDTOREPLACE";
         Ddo_contratoclausulas_descricao_Internalname = "DDO_CONTRATOCLAUSULAS_DESCRICAO";
         edtavDdo_contratoclausulas_descricaotitlecontrolidtoreplace_Internalname = "vDDO_CONTRATOCLAUSULAS_DESCRICAOTITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtContratoClausulas_Descricao_Jsonclick = "";
         edtContratoClausulas_Item_Jsonclick = "";
         edtContrato_Numero_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtContratoClausulas_Item_Link = "";
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = "";
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = "";
         edtContratoClausulas_Descricao_Titleformat = 0;
         edtContratoClausulas_Item_Titleformat = 0;
         edtContrato_Numero_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         imgInsert_Enabled = 1;
         imgInsert_Bitmap = (String)(context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( )));
         edtContratoClausulas_Descricao_Title = "Descri��o";
         edtContratoClausulas_Item_Title = "Item";
         edtContrato_Numero_Title = "N� Contrato";
         edtavOrdereddsc_Visible = 1;
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         edtavDdo_contratoclausulas_descricaotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratoclausulas_itemtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contrato_numerotitlecontrolidtoreplace_Visible = 1;
         edtavTfcontratoclausulas_descricao_sel_Visible = 1;
         edtavTfcontratoclausulas_descricao_Visible = 1;
         edtavTfcontratoclausulas_item_sel_Jsonclick = "";
         edtavTfcontratoclausulas_item_sel_Visible = 1;
         edtavTfcontratoclausulas_item_Jsonclick = "";
         edtavTfcontratoclausulas_item_Visible = 1;
         edtavTfcontrato_numero_sel_Jsonclick = "";
         edtavTfcontrato_numero_sel_Visible = 1;
         edtavTfcontrato_numero_Jsonclick = "";
         edtavTfcontrato_numero_Visible = 1;
         Ddo_contratoclausulas_descricao_Searchbuttontext = "Pesquisar";
         Ddo_contratoclausulas_descricao_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contratoclausulas_descricao_Cleanfilter = "Limpar pesquisa";
         Ddo_contratoclausulas_descricao_Loadingdata = "Carregando dados...";
         Ddo_contratoclausulas_descricao_Sortdsc = "Ordenar de Z � A";
         Ddo_contratoclausulas_descricao_Sortasc = "Ordenar de A � Z";
         Ddo_contratoclausulas_descricao_Datalistupdateminimumcharacters = 0;
         Ddo_contratoclausulas_descricao_Datalistproc = "GetWWContratoClausulasFilterData";
         Ddo_contratoclausulas_descricao_Datalisttype = "Dynamic";
         Ddo_contratoclausulas_descricao_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contratoclausulas_descricao_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contratoclausulas_descricao_Filtertype = "Character";
         Ddo_contratoclausulas_descricao_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratoclausulas_descricao_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratoclausulas_descricao_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratoclausulas_descricao_Titlecontrolidtoreplace = "";
         Ddo_contratoclausulas_descricao_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratoclausulas_descricao_Cls = "ColumnSettings";
         Ddo_contratoclausulas_descricao_Tooltip = "Op��es";
         Ddo_contratoclausulas_descricao_Caption = "";
         Ddo_contratoclausulas_item_Searchbuttontext = "Pesquisar";
         Ddo_contratoclausulas_item_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contratoclausulas_item_Cleanfilter = "Limpar pesquisa";
         Ddo_contratoclausulas_item_Loadingdata = "Carregando dados...";
         Ddo_contratoclausulas_item_Sortdsc = "Ordenar de Z � A";
         Ddo_contratoclausulas_item_Sortasc = "Ordenar de A � Z";
         Ddo_contratoclausulas_item_Datalistupdateminimumcharacters = 0;
         Ddo_contratoclausulas_item_Datalistproc = "GetWWContratoClausulasFilterData";
         Ddo_contratoclausulas_item_Datalisttype = "Dynamic";
         Ddo_contratoclausulas_item_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contratoclausulas_item_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contratoclausulas_item_Filtertype = "Character";
         Ddo_contratoclausulas_item_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratoclausulas_item_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratoclausulas_item_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratoclausulas_item_Titlecontrolidtoreplace = "";
         Ddo_contratoclausulas_item_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratoclausulas_item_Cls = "ColumnSettings";
         Ddo_contratoclausulas_item_Tooltip = "Op��es";
         Ddo_contratoclausulas_item_Caption = "";
         Ddo_contrato_numero_Searchbuttontext = "Pesquisar";
         Ddo_contrato_numero_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contrato_numero_Cleanfilter = "Limpar pesquisa";
         Ddo_contrato_numero_Loadingdata = "Carregando dados...";
         Ddo_contrato_numero_Sortdsc = "Ordenar de Z � A";
         Ddo_contrato_numero_Sortasc = "Ordenar de A � Z";
         Ddo_contrato_numero_Datalistupdateminimumcharacters = 0;
         Ddo_contrato_numero_Datalistproc = "GetWWContratoClausulasFilterData";
         Ddo_contrato_numero_Datalisttype = "Dynamic";
         Ddo_contrato_numero_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contrato_numero_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contrato_numero_Filtertype = "Character";
         Ddo_contrato_numero_Includefilter = Convert.ToBoolean( -1);
         Ddo_contrato_numero_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contrato_numero_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contrato_numero_Titlecontrolidtoreplace = "";
         Ddo_contrato_numero_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contrato_numero_Cls = "ColumnSettings";
         Ddo_contrato_numero_Tooltip = "Op��es";
         Ddo_contrato_numero_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = " Contrato Clausulas";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A152ContratoClausulas_Codigo',fld:'CONTRATOCLAUSULAS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV42ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_ContratoClausulas_ItemTitleControlIdToReplace',fld:'vDDO_CONTRATOCLAUSULAS_ITEMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_ContratoClausulas_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOCLAUSULAS_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV41TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV44TFContratoClausulas_Item',fld:'vTFCONTRATOCLAUSULAS_ITEM',pic:'',nv:''},{av:'AV45TFContratoClausulas_Item_Sel',fld:'vTFCONTRATOCLAUSULAS_ITEM_SEL',pic:'',nv:''},{av:'AV48TFContratoClausulas_Descricao',fld:'vTFCONTRATOCLAUSULAS_DESCRICAO',pic:'',nv:''},{av:'AV49TFContratoClausulas_Descricao_Sel',fld:'vTFCONTRATOCLAUSULAS_DESCRICAO_SEL',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV86Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false}],oparms:[{av:'AV39Contrato_NumeroTitleFilterData',fld:'vCONTRATO_NUMEROTITLEFILTERDATA',pic:'',nv:null},{av:'AV43ContratoClausulas_ItemTitleFilterData',fld:'vCONTRATOCLAUSULAS_ITEMTITLEFILTERDATA',pic:'',nv:null},{av:'AV47ContratoClausulas_DescricaoTitleFilterData',fld:'vCONTRATOCLAUSULAS_DESCRICAOTITLEFILTERDATA',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'edtContrato_Numero_Titleformat',ctrl:'CONTRATO_NUMERO',prop:'Titleformat'},{av:'edtContrato_Numero_Title',ctrl:'CONTRATO_NUMERO',prop:'Title'},{av:'edtContratoClausulas_Item_Titleformat',ctrl:'CONTRATOCLAUSULAS_ITEM',prop:'Titleformat'},{av:'edtContratoClausulas_Item_Title',ctrl:'CONTRATOCLAUSULAS_ITEM',prop:'Title'},{av:'edtContratoClausulas_Descricao_Titleformat',ctrl:'CONTRATOCLAUSULAS_DESCRICAO',prop:'Titleformat'},{av:'edtContratoClausulas_Descricao_Title',ctrl:'CONTRATOCLAUSULAS_DESCRICAO',prop:'Title'},{av:'AV53GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV54GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'imgInsert_Link',ctrl:'INSERT',prop:'Link'},{av:'imgInsert_Enabled',ctrl:'INSERT',prop:'Enabled'}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E116P2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV40TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV41TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV44TFContratoClausulas_Item',fld:'vTFCONTRATOCLAUSULAS_ITEM',pic:'',nv:''},{av:'AV45TFContratoClausulas_Item_Sel',fld:'vTFCONTRATOCLAUSULAS_ITEM_SEL',pic:'',nv:''},{av:'AV48TFContratoClausulas_Descricao',fld:'vTFCONTRATOCLAUSULAS_DESCRICAO',pic:'',nv:''},{av:'AV49TFContratoClausulas_Descricao_Sel',fld:'vTFCONTRATOCLAUSULAS_DESCRICAO_SEL',pic:'',nv:''},{av:'AV42ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_ContratoClausulas_ItemTitleControlIdToReplace',fld:'vDDO_CONTRATOCLAUSULAS_ITEMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_ContratoClausulas_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOCLAUSULAS_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV86Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A152ContratoClausulas_Codigo',fld:'CONTRATOCLAUSULAS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_CONTRATO_NUMERO.ONOPTIONCLICKED","{handler:'E126P2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV40TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV41TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV44TFContratoClausulas_Item',fld:'vTFCONTRATOCLAUSULAS_ITEM',pic:'',nv:''},{av:'AV45TFContratoClausulas_Item_Sel',fld:'vTFCONTRATOCLAUSULAS_ITEM_SEL',pic:'',nv:''},{av:'AV48TFContratoClausulas_Descricao',fld:'vTFCONTRATOCLAUSULAS_DESCRICAO',pic:'',nv:''},{av:'AV49TFContratoClausulas_Descricao_Sel',fld:'vTFCONTRATOCLAUSULAS_DESCRICAO_SEL',pic:'',nv:''},{av:'AV42ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_ContratoClausulas_ItemTitleControlIdToReplace',fld:'vDDO_CONTRATOCLAUSULAS_ITEMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_ContratoClausulas_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOCLAUSULAS_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV86Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A152ContratoClausulas_Codigo',fld:'CONTRATOCLAUSULAS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contrato_numero_Activeeventkey',ctrl:'DDO_CONTRATO_NUMERO',prop:'ActiveEventKey'},{av:'Ddo_contrato_numero_Filteredtext_get',ctrl:'DDO_CONTRATO_NUMERO',prop:'FilteredText_get'},{av:'Ddo_contrato_numero_Selectedvalue_get',ctrl:'DDO_CONTRATO_NUMERO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contrato_numero_Sortedstatus',ctrl:'DDO_CONTRATO_NUMERO',prop:'SortedStatus'},{av:'AV40TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV41TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'Ddo_contratoclausulas_item_Sortedstatus',ctrl:'DDO_CONTRATOCLAUSULAS_ITEM',prop:'SortedStatus'},{av:'Ddo_contratoclausulas_descricao_Sortedstatus',ctrl:'DDO_CONTRATOCLAUSULAS_DESCRICAO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOCLAUSULAS_ITEM.ONOPTIONCLICKED","{handler:'E136P2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV40TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV41TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV44TFContratoClausulas_Item',fld:'vTFCONTRATOCLAUSULAS_ITEM',pic:'',nv:''},{av:'AV45TFContratoClausulas_Item_Sel',fld:'vTFCONTRATOCLAUSULAS_ITEM_SEL',pic:'',nv:''},{av:'AV48TFContratoClausulas_Descricao',fld:'vTFCONTRATOCLAUSULAS_DESCRICAO',pic:'',nv:''},{av:'AV49TFContratoClausulas_Descricao_Sel',fld:'vTFCONTRATOCLAUSULAS_DESCRICAO_SEL',pic:'',nv:''},{av:'AV42ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_ContratoClausulas_ItemTitleControlIdToReplace',fld:'vDDO_CONTRATOCLAUSULAS_ITEMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_ContratoClausulas_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOCLAUSULAS_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV86Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A152ContratoClausulas_Codigo',fld:'CONTRATOCLAUSULAS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contratoclausulas_item_Activeeventkey',ctrl:'DDO_CONTRATOCLAUSULAS_ITEM',prop:'ActiveEventKey'},{av:'Ddo_contratoclausulas_item_Filteredtext_get',ctrl:'DDO_CONTRATOCLAUSULAS_ITEM',prop:'FilteredText_get'},{av:'Ddo_contratoclausulas_item_Selectedvalue_get',ctrl:'DDO_CONTRATOCLAUSULAS_ITEM',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratoclausulas_item_Sortedstatus',ctrl:'DDO_CONTRATOCLAUSULAS_ITEM',prop:'SortedStatus'},{av:'AV44TFContratoClausulas_Item',fld:'vTFCONTRATOCLAUSULAS_ITEM',pic:'',nv:''},{av:'AV45TFContratoClausulas_Item_Sel',fld:'vTFCONTRATOCLAUSULAS_ITEM_SEL',pic:'',nv:''},{av:'Ddo_contrato_numero_Sortedstatus',ctrl:'DDO_CONTRATO_NUMERO',prop:'SortedStatus'},{av:'Ddo_contratoclausulas_descricao_Sortedstatus',ctrl:'DDO_CONTRATOCLAUSULAS_DESCRICAO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOCLAUSULAS_DESCRICAO.ONOPTIONCLICKED","{handler:'E146P2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV40TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV41TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV44TFContratoClausulas_Item',fld:'vTFCONTRATOCLAUSULAS_ITEM',pic:'',nv:''},{av:'AV45TFContratoClausulas_Item_Sel',fld:'vTFCONTRATOCLAUSULAS_ITEM_SEL',pic:'',nv:''},{av:'AV48TFContratoClausulas_Descricao',fld:'vTFCONTRATOCLAUSULAS_DESCRICAO',pic:'',nv:''},{av:'AV49TFContratoClausulas_Descricao_Sel',fld:'vTFCONTRATOCLAUSULAS_DESCRICAO_SEL',pic:'',nv:''},{av:'AV42ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_ContratoClausulas_ItemTitleControlIdToReplace',fld:'vDDO_CONTRATOCLAUSULAS_ITEMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_ContratoClausulas_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOCLAUSULAS_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV86Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A152ContratoClausulas_Codigo',fld:'CONTRATOCLAUSULAS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contratoclausulas_descricao_Activeeventkey',ctrl:'DDO_CONTRATOCLAUSULAS_DESCRICAO',prop:'ActiveEventKey'},{av:'Ddo_contratoclausulas_descricao_Filteredtext_get',ctrl:'DDO_CONTRATOCLAUSULAS_DESCRICAO',prop:'FilteredText_get'},{av:'Ddo_contratoclausulas_descricao_Selectedvalue_get',ctrl:'DDO_CONTRATOCLAUSULAS_DESCRICAO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratoclausulas_descricao_Sortedstatus',ctrl:'DDO_CONTRATOCLAUSULAS_DESCRICAO',prop:'SortedStatus'},{av:'AV48TFContratoClausulas_Descricao',fld:'vTFCONTRATOCLAUSULAS_DESCRICAO',pic:'',nv:''},{av:'AV49TFContratoClausulas_Descricao_Sel',fld:'vTFCONTRATOCLAUSULAS_DESCRICAO_SEL',pic:'',nv:''},{av:'Ddo_contrato_numero_Sortedstatus',ctrl:'DDO_CONTRATO_NUMERO',prop:'SortedStatus'},{av:'Ddo_contratoclausulas_item_Sortedstatus',ctrl:'DDO_CONTRATOCLAUSULAS_ITEM',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E196P2',iparms:[{av:'A152ContratoClausulas_Codigo',fld:'CONTRATOCLAUSULAS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV28Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'edtavUpdate_Link',ctrl:'vUPDATE',prop:'Link'},{av:'AV29Delete',fld:'vDELETE',pic:'',nv:''},{av:'edtavDelete_Tooltiptext',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'edtavDelete_Link',ctrl:'vDELETE',prop:'Link'},{av:'edtContratoClausulas_Item_Link',ctrl:'CONTRATOCLAUSULAS_ITEM',prop:'Link'}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E156P2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV40TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV41TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV44TFContratoClausulas_Item',fld:'vTFCONTRATOCLAUSULAS_ITEM',pic:'',nv:''},{av:'AV45TFContratoClausulas_Item_Sel',fld:'vTFCONTRATOCLAUSULAS_ITEM_SEL',pic:'',nv:''},{av:'AV48TFContratoClausulas_Descricao',fld:'vTFCONTRATOCLAUSULAS_DESCRICAO',pic:'',nv:''},{av:'AV49TFContratoClausulas_Descricao_Sel',fld:'vTFCONTRATOCLAUSULAS_DESCRICAO_SEL',pic:'',nv:''},{av:'AV42ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_ContratoClausulas_ItemTitleControlIdToReplace',fld:'vDDO_CONTRATOCLAUSULAS_ITEMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_ContratoClausulas_DescricaoTitleControlIdToReplace',fld:'vDDO_CONTRATOCLAUSULAS_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV86Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A152ContratoClausulas_Codigo',fld:'CONTRATOCLAUSULAS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("'DOINSERT'","{handler:'E166P2',iparms:[{av:'A152ContratoClausulas_Codigo',fld:'CONTRATOCLAUSULAS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_contrato_numero_Activeeventkey = "";
         Ddo_contrato_numero_Filteredtext_get = "";
         Ddo_contrato_numero_Selectedvalue_get = "";
         Ddo_contratoclausulas_item_Activeeventkey = "";
         Ddo_contratoclausulas_item_Filteredtext_get = "";
         Ddo_contratoclausulas_item_Selectedvalue_get = "";
         Ddo_contratoclausulas_descricao_Activeeventkey = "";
         Ddo_contratoclausulas_descricao_Filteredtext_get = "";
         Ddo_contratoclausulas_descricao_Selectedvalue_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV40TFContrato_Numero = "";
         AV41TFContrato_Numero_Sel = "";
         AV44TFContratoClausulas_Item = "";
         AV45TFContratoClausulas_Item_Sel = "";
         AV48TFContratoClausulas_Descricao = "";
         AV49TFContratoClausulas_Descricao_Sel = "";
         AV42ddo_Contrato_NumeroTitleControlIdToReplace = "";
         AV46ddo_ContratoClausulas_ItemTitleControlIdToReplace = "";
         AV50ddo_ContratoClausulas_DescricaoTitleControlIdToReplace = "";
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV86Pgmname = "";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV51DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV39Contrato_NumeroTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV43ContratoClausulas_ItemTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV47ContratoClausulas_DescricaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_contrato_numero_Filteredtext_set = "";
         Ddo_contrato_numero_Selectedvalue_set = "";
         Ddo_contrato_numero_Sortedstatus = "";
         Ddo_contratoclausulas_item_Filteredtext_set = "";
         Ddo_contratoclausulas_item_Selectedvalue_set = "";
         Ddo_contratoclausulas_item_Sortedstatus = "";
         Ddo_contratoclausulas_descricao_Filteredtext_set = "";
         Ddo_contratoclausulas_descricao_Selectedvalue_set = "";
         Ddo_contratoclausulas_descricao_Sortedstatus = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV28Update = "";
         AV84Update_GXI = "";
         AV29Delete = "";
         AV85Delete_GXI = "";
         A77Contrato_Numero = "";
         A153ContratoClausulas_Item = "";
         A154ContratoClausulas_Descricao = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV78WWContratoClausulasDS_1_Tfcontrato_numero = "";
         lV80WWContratoClausulasDS_3_Tfcontratoclausulas_item = "";
         lV82WWContratoClausulasDS_5_Tfcontratoclausulas_descricao = "";
         AV79WWContratoClausulasDS_2_Tfcontrato_numero_sel = "";
         AV78WWContratoClausulasDS_1_Tfcontrato_numero = "";
         AV81WWContratoClausulasDS_4_Tfcontratoclausulas_item_sel = "";
         AV80WWContratoClausulasDS_3_Tfcontratoclausulas_item = "";
         AV83WWContratoClausulasDS_6_Tfcontratoclausulas_descricao_sel = "";
         AV82WWContratoClausulasDS_5_Tfcontratoclausulas_descricao = "";
         H006P2_A74Contrato_Codigo = new int[1] ;
         H006P2_A152ContratoClausulas_Codigo = new int[1] ;
         H006P2_A154ContratoClausulas_Descricao = new String[] {""} ;
         H006P2_A153ContratoClausulas_Item = new String[] {""} ;
         H006P2_A77Contrato_Numero = new String[] {""} ;
         H006P3_AGRID_nRecordCount = new long[1] ;
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         GridRow = new GXWebRow();
         imgInsert_Link = "";
         AV30Session = context.GetSession();
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV7HTTPRequest = new GxHttpRequest( context);
         sStyleString = "";
         imgInsert_Jsonclick = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblContratoclausulastitle_Jsonclick = "";
         lblOrderedtext_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wwcontratoclausulas__default(),
            new Object[][] {
                new Object[] {
               H006P2_A74Contrato_Codigo, H006P2_A152ContratoClausulas_Codigo, H006P2_A154ContratoClausulas_Descricao, H006P2_A153ContratoClausulas_Item, H006P2_A77Contrato_Numero
               }
               , new Object[] {
               H006P3_AGRID_nRecordCount
               }
            }
         );
         AV86Pgmname = "WWContratoClausulas";
         /* GeneXus formulas. */
         AV86Pgmname = "WWContratoClausulas";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_36 ;
      private short nGXsfl_36_idx=1 ;
      private short AV13OrderedBy ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_36_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtContrato_Numero_Titleformat ;
      private short edtContratoClausulas_Item_Titleformat ;
      private short edtContratoClausulas_Descricao_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int subGrid_Rows ;
      private int A152ContratoClausulas_Codigo ;
      private int A74Contrato_Codigo ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_contrato_numero_Datalistupdateminimumcharacters ;
      private int Ddo_contratoclausulas_item_Datalistupdateminimumcharacters ;
      private int Ddo_contratoclausulas_descricao_Datalistupdateminimumcharacters ;
      private int edtavTfcontrato_numero_Visible ;
      private int edtavTfcontrato_numero_sel_Visible ;
      private int edtavTfcontratoclausulas_item_Visible ;
      private int edtavTfcontratoclausulas_item_sel_Visible ;
      private int edtavTfcontratoclausulas_descricao_Visible ;
      private int edtavTfcontratoclausulas_descricao_sel_Visible ;
      private int edtavDdo_contrato_numerotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratoclausulas_itemtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratoclausulas_descricaotitlecontrolidtoreplace_Visible ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int edtavOrdereddsc_Visible ;
      private int AV52PageToGo ;
      private int imgInsert_Enabled ;
      private int AV87GXV1 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long AV53GridCurrentPage ;
      private long AV54GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_contrato_numero_Activeeventkey ;
      private String Ddo_contrato_numero_Filteredtext_get ;
      private String Ddo_contrato_numero_Selectedvalue_get ;
      private String Ddo_contratoclausulas_item_Activeeventkey ;
      private String Ddo_contratoclausulas_item_Filteredtext_get ;
      private String Ddo_contratoclausulas_item_Selectedvalue_get ;
      private String Ddo_contratoclausulas_descricao_Activeeventkey ;
      private String Ddo_contratoclausulas_descricao_Filteredtext_get ;
      private String Ddo_contratoclausulas_descricao_Selectedvalue_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_36_idx="0001" ;
      private String AV40TFContrato_Numero ;
      private String AV41TFContrato_Numero_Sel ;
      private String AV44TFContratoClausulas_Item ;
      private String AV45TFContratoClausulas_Item_Sel ;
      private String AV86Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_contrato_numero_Caption ;
      private String Ddo_contrato_numero_Tooltip ;
      private String Ddo_contrato_numero_Cls ;
      private String Ddo_contrato_numero_Filteredtext_set ;
      private String Ddo_contrato_numero_Selectedvalue_set ;
      private String Ddo_contrato_numero_Dropdownoptionstype ;
      private String Ddo_contrato_numero_Titlecontrolidtoreplace ;
      private String Ddo_contrato_numero_Sortedstatus ;
      private String Ddo_contrato_numero_Filtertype ;
      private String Ddo_contrato_numero_Datalisttype ;
      private String Ddo_contrato_numero_Datalistproc ;
      private String Ddo_contrato_numero_Sortasc ;
      private String Ddo_contrato_numero_Sortdsc ;
      private String Ddo_contrato_numero_Loadingdata ;
      private String Ddo_contrato_numero_Cleanfilter ;
      private String Ddo_contrato_numero_Noresultsfound ;
      private String Ddo_contrato_numero_Searchbuttontext ;
      private String Ddo_contratoclausulas_item_Caption ;
      private String Ddo_contratoclausulas_item_Tooltip ;
      private String Ddo_contratoclausulas_item_Cls ;
      private String Ddo_contratoclausulas_item_Filteredtext_set ;
      private String Ddo_contratoclausulas_item_Selectedvalue_set ;
      private String Ddo_contratoclausulas_item_Dropdownoptionstype ;
      private String Ddo_contratoclausulas_item_Titlecontrolidtoreplace ;
      private String Ddo_contratoclausulas_item_Sortedstatus ;
      private String Ddo_contratoclausulas_item_Filtertype ;
      private String Ddo_contratoclausulas_item_Datalisttype ;
      private String Ddo_contratoclausulas_item_Datalistproc ;
      private String Ddo_contratoclausulas_item_Sortasc ;
      private String Ddo_contratoclausulas_item_Sortdsc ;
      private String Ddo_contratoclausulas_item_Loadingdata ;
      private String Ddo_contratoclausulas_item_Cleanfilter ;
      private String Ddo_contratoclausulas_item_Noresultsfound ;
      private String Ddo_contratoclausulas_item_Searchbuttontext ;
      private String Ddo_contratoclausulas_descricao_Caption ;
      private String Ddo_contratoclausulas_descricao_Tooltip ;
      private String Ddo_contratoclausulas_descricao_Cls ;
      private String Ddo_contratoclausulas_descricao_Filteredtext_set ;
      private String Ddo_contratoclausulas_descricao_Selectedvalue_set ;
      private String Ddo_contratoclausulas_descricao_Dropdownoptionstype ;
      private String Ddo_contratoclausulas_descricao_Titlecontrolidtoreplace ;
      private String Ddo_contratoclausulas_descricao_Sortedstatus ;
      private String Ddo_contratoclausulas_descricao_Filtertype ;
      private String Ddo_contratoclausulas_descricao_Datalisttype ;
      private String Ddo_contratoclausulas_descricao_Datalistproc ;
      private String Ddo_contratoclausulas_descricao_Sortasc ;
      private String Ddo_contratoclausulas_descricao_Sortdsc ;
      private String Ddo_contratoclausulas_descricao_Loadingdata ;
      private String Ddo_contratoclausulas_descricao_Cleanfilter ;
      private String Ddo_contratoclausulas_descricao_Noresultsfound ;
      private String Ddo_contratoclausulas_descricao_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String edtavTfcontrato_numero_Internalname ;
      private String edtavTfcontrato_numero_Jsonclick ;
      private String edtavTfcontrato_numero_sel_Internalname ;
      private String edtavTfcontrato_numero_sel_Jsonclick ;
      private String edtavTfcontratoclausulas_item_Internalname ;
      private String edtavTfcontratoclausulas_item_Jsonclick ;
      private String edtavTfcontratoclausulas_item_sel_Internalname ;
      private String edtavTfcontratoclausulas_item_sel_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String edtavTfcontratoclausulas_descricao_Internalname ;
      private String edtavTfcontratoclausulas_descricao_sel_Internalname ;
      private String edtavDdo_contrato_numerotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratoclausulas_itemtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratoclausulas_descricaotitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavUpdate_Internalname ;
      private String edtavDelete_Internalname ;
      private String A77Contrato_Numero ;
      private String edtContrato_Numero_Internalname ;
      private String A153ContratoClausulas_Item ;
      private String edtContratoClausulas_Item_Internalname ;
      private String edtContratoClausulas_Descricao_Internalname ;
      private String cmbavOrderedby_Internalname ;
      private String scmdbuf ;
      private String lV78WWContratoClausulasDS_1_Tfcontrato_numero ;
      private String lV80WWContratoClausulasDS_3_Tfcontratoclausulas_item ;
      private String AV79WWContratoClausulasDS_2_Tfcontrato_numero_sel ;
      private String AV78WWContratoClausulasDS_1_Tfcontrato_numero ;
      private String AV81WWContratoClausulasDS_4_Tfcontratoclausulas_item_sel ;
      private String AV80WWContratoClausulasDS_3_Tfcontratoclausulas_item ;
      private String edtavOrdereddsc_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_contrato_numero_Internalname ;
      private String Ddo_contratoclausulas_item_Internalname ;
      private String Ddo_contratoclausulas_descricao_Internalname ;
      private String edtContrato_Numero_Title ;
      private String edtContratoClausulas_Item_Title ;
      private String edtContratoClausulas_Descricao_Title ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavUpdate_Link ;
      private String edtavDelete_Tooltiptext ;
      private String edtavDelete_Link ;
      private String edtContratoClausulas_Item_Link ;
      private String imgInsert_Link ;
      private String imgInsert_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblTablemergedgrid_Internalname ;
      private String imgInsert_Jsonclick ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTableheader_Internalname ;
      private String lblContratoclausulastitle_Internalname ;
      private String lblContratoclausulastitle_Jsonclick ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String tblTableactions_Internalname ;
      private String sGXsfl_36_fel_idx="0001" ;
      private String ROClassString ;
      private String edtContrato_Numero_Jsonclick ;
      private String edtContratoClausulas_Item_Jsonclick ;
      private String edtContratoClausulas_Descricao_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_contrato_numero_Includesortasc ;
      private bool Ddo_contrato_numero_Includesortdsc ;
      private bool Ddo_contrato_numero_Includefilter ;
      private bool Ddo_contrato_numero_Filterisrange ;
      private bool Ddo_contrato_numero_Includedatalist ;
      private bool Ddo_contratoclausulas_item_Includesortasc ;
      private bool Ddo_contratoclausulas_item_Includesortdsc ;
      private bool Ddo_contratoclausulas_item_Includefilter ;
      private bool Ddo_contratoclausulas_item_Filterisrange ;
      private bool Ddo_contratoclausulas_item_Includedatalist ;
      private bool Ddo_contratoclausulas_descricao_Includesortasc ;
      private bool Ddo_contratoclausulas_descricao_Includesortdsc ;
      private bool Ddo_contratoclausulas_descricao_Includefilter ;
      private bool Ddo_contratoclausulas_descricao_Filterisrange ;
      private bool Ddo_contratoclausulas_descricao_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV28Update_IsBlob ;
      private bool AV29Delete_IsBlob ;
      private String A154ContratoClausulas_Descricao ;
      private String AV48TFContratoClausulas_Descricao ;
      private String AV49TFContratoClausulas_Descricao_Sel ;
      private String AV42ddo_Contrato_NumeroTitleControlIdToReplace ;
      private String AV46ddo_ContratoClausulas_ItemTitleControlIdToReplace ;
      private String AV50ddo_ContratoClausulas_DescricaoTitleControlIdToReplace ;
      private String AV84Update_GXI ;
      private String AV85Delete_GXI ;
      private String lV82WWContratoClausulasDS_5_Tfcontratoclausulas_descricao ;
      private String AV83WWContratoClausulasDS_6_Tfcontratoclausulas_descricao_sel ;
      private String AV82WWContratoClausulasDS_5_Tfcontratoclausulas_descricao ;
      private String AV28Update ;
      private String AV29Delete ;
      private String imgInsert_Bitmap ;
      private IGxSession AV30Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavOrderedby ;
      private IDataStoreProvider pr_default ;
      private int[] H006P2_A74Contrato_Codigo ;
      private int[] H006P2_A152ContratoClausulas_Codigo ;
      private String[] H006P2_A154ContratoClausulas_Descricao ;
      private String[] H006P2_A153ContratoClausulas_Item ;
      private String[] H006P2_A77Contrato_Numero ;
      private long[] H006P3_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV7HTTPRequest ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV39Contrato_NumeroTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV43ContratoClausulas_ItemTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV47ContratoClausulas_DescricaoTitleFilterData ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV11GridStateFilterValue ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV51DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class wwcontratoclausulas__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H006P2( IGxContext context ,
                                             String AV79WWContratoClausulasDS_2_Tfcontrato_numero_sel ,
                                             String AV78WWContratoClausulasDS_1_Tfcontrato_numero ,
                                             String AV81WWContratoClausulasDS_4_Tfcontratoclausulas_item_sel ,
                                             String AV80WWContratoClausulasDS_3_Tfcontratoclausulas_item ,
                                             String AV83WWContratoClausulasDS_6_Tfcontratoclausulas_descricao_sel ,
                                             String AV82WWContratoClausulasDS_5_Tfcontratoclausulas_descricao ,
                                             String A77Contrato_Numero ,
                                             String A153ContratoClausulas_Item ,
                                             String A154ContratoClausulas_Descricao ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [11] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T1.[Contrato_Codigo], T1.[ContratoClausulas_Codigo], T1.[ContratoClausulas_Descricao], T1.[ContratoClausulas_Item], T2.[Contrato_Numero]";
         sFromString = " FROM ([ContratoClausulas] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo])";
         sOrderString = "";
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV79WWContratoClausulasDS_2_Tfcontrato_numero_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV78WWContratoClausulasDS_1_Tfcontrato_numero)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] like @lV78WWContratoClausulasDS_1_Tfcontrato_numero)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] like @lV78WWContratoClausulasDS_1_Tfcontrato_numero)";
            }
         }
         else
         {
            GXv_int2[0] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV79WWContratoClausulasDS_2_Tfcontrato_numero_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] = @AV79WWContratoClausulasDS_2_Tfcontrato_numero_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] = @AV79WWContratoClausulasDS_2_Tfcontrato_numero_sel)";
            }
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV81WWContratoClausulasDS_4_Tfcontratoclausulas_item_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV80WWContratoClausulasDS_3_Tfcontratoclausulas_item)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoClausulas_Item] like @lV80WWContratoClausulasDS_3_Tfcontratoclausulas_item)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoClausulas_Item] like @lV80WWContratoClausulasDS_3_Tfcontratoclausulas_item)";
            }
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV81WWContratoClausulasDS_4_Tfcontratoclausulas_item_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoClausulas_Item] = @AV81WWContratoClausulasDS_4_Tfcontratoclausulas_item_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoClausulas_Item] = @AV81WWContratoClausulasDS_4_Tfcontratoclausulas_item_sel)";
            }
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV83WWContratoClausulasDS_6_Tfcontratoclausulas_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV82WWContratoClausulasDS_5_Tfcontratoclausulas_descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoClausulas_Descricao] like @lV82WWContratoClausulasDS_5_Tfcontratoclausulas_descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoClausulas_Descricao] like @lV82WWContratoClausulasDS_5_Tfcontratoclausulas_descricao)";
            }
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV83WWContratoClausulasDS_6_Tfcontratoclausulas_descricao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoClausulas_Descricao] = @AV83WWContratoClausulasDS_6_Tfcontratoclausulas_descricao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoClausulas_Descricao] = @AV83WWContratoClausulasDS_6_Tfcontratoclausulas_descricao_sel)";
            }
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            sWhereString = " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoClausulas_Item]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoClausulas_Item] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Contrato_Numero]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Contrato_Numero] DESC";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoClausulas_Descricao]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoClausulas_Descricao] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoClausulas_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H006P3( IGxContext context ,
                                             String AV79WWContratoClausulasDS_2_Tfcontrato_numero_sel ,
                                             String AV78WWContratoClausulasDS_1_Tfcontrato_numero ,
                                             String AV81WWContratoClausulasDS_4_Tfcontratoclausulas_item_sel ,
                                             String AV80WWContratoClausulasDS_3_Tfcontratoclausulas_item ,
                                             String AV83WWContratoClausulasDS_6_Tfcontratoclausulas_descricao_sel ,
                                             String AV82WWContratoClausulasDS_5_Tfcontratoclausulas_descricao ,
                                             String A77Contrato_Numero ,
                                             String A153ContratoClausulas_Item ,
                                             String A154ContratoClausulas_Descricao ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [6] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM ([ContratoClausulas] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo])";
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV79WWContratoClausulasDS_2_Tfcontrato_numero_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV78WWContratoClausulasDS_1_Tfcontrato_numero)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] like @lV78WWContratoClausulasDS_1_Tfcontrato_numero)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] like @lV78WWContratoClausulasDS_1_Tfcontrato_numero)";
            }
         }
         else
         {
            GXv_int4[0] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV79WWContratoClausulasDS_2_Tfcontrato_numero_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] = @AV79WWContratoClausulasDS_2_Tfcontrato_numero_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] = @AV79WWContratoClausulasDS_2_Tfcontrato_numero_sel)";
            }
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV81WWContratoClausulasDS_4_Tfcontratoclausulas_item_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV80WWContratoClausulasDS_3_Tfcontratoclausulas_item)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoClausulas_Item] like @lV80WWContratoClausulasDS_3_Tfcontratoclausulas_item)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoClausulas_Item] like @lV80WWContratoClausulasDS_3_Tfcontratoclausulas_item)";
            }
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV81WWContratoClausulasDS_4_Tfcontratoclausulas_item_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoClausulas_Item] = @AV81WWContratoClausulasDS_4_Tfcontratoclausulas_item_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoClausulas_Item] = @AV81WWContratoClausulasDS_4_Tfcontratoclausulas_item_sel)";
            }
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV83WWContratoClausulasDS_6_Tfcontratoclausulas_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV82WWContratoClausulasDS_5_Tfcontratoclausulas_descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoClausulas_Descricao] like @lV82WWContratoClausulasDS_5_Tfcontratoclausulas_descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoClausulas_Descricao] like @lV82WWContratoClausulasDS_5_Tfcontratoclausulas_descricao)";
            }
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV83WWContratoClausulasDS_6_Tfcontratoclausulas_descricao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoClausulas_Descricao] = @AV83WWContratoClausulasDS_6_Tfcontratoclausulas_descricao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoClausulas_Descricao] = @AV83WWContratoClausulasDS_6_Tfcontratoclausulas_descricao_sel)";
            }
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H006P2(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (bool)dynConstraints[10] );
               case 1 :
                     return conditional_H006P3(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (bool)dynConstraints[10] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH006P2 ;
          prmH006P2 = new Object[] {
          new Object[] {"@lV78WWContratoClausulasDS_1_Tfcontrato_numero",SqlDbType.Char,20,0} ,
          new Object[] {"@AV79WWContratoClausulasDS_2_Tfcontrato_numero_sel",SqlDbType.Char,20,0} ,
          new Object[] {"@lV80WWContratoClausulasDS_3_Tfcontratoclausulas_item",SqlDbType.Char,10,0} ,
          new Object[] {"@AV81WWContratoClausulasDS_4_Tfcontratoclausulas_item_sel",SqlDbType.Char,10,0} ,
          new Object[] {"@lV82WWContratoClausulasDS_5_Tfcontratoclausulas_descricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV83WWContratoClausulasDS_6_Tfcontratoclausulas_descricao_sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH006P3 ;
          prmH006P3 = new Object[] {
          new Object[] {"@lV78WWContratoClausulasDS_1_Tfcontrato_numero",SqlDbType.Char,20,0} ,
          new Object[] {"@AV79WWContratoClausulasDS_2_Tfcontrato_numero_sel",SqlDbType.Char,20,0} ,
          new Object[] {"@lV80WWContratoClausulasDS_3_Tfcontratoclausulas_item",SqlDbType.Char,10,0} ,
          new Object[] {"@AV81WWContratoClausulasDS_4_Tfcontratoclausulas_item_sel",SqlDbType.Char,10,0} ,
          new Object[] {"@lV82WWContratoClausulasDS_5_Tfcontratoclausulas_descricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV83WWContratoClausulasDS_6_Tfcontratoclausulas_descricao_sel",SqlDbType.VarChar,200,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H006P2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH006P2,11,0,true,false )
             ,new CursorDef("H006P3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH006P3,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 10) ;
                ((String[]) buf[4])[0] = rslt.getString(5, 20) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[17]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[18]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[19]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[20]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[21]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[6]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[7]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[8]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[9]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[10]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                return;
       }
    }

 }

}
