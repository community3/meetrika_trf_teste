/*
               File: WP_SummaryAdd
        Description: Summary Add
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:30:1.0
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Office;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_summaryadd : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wp_summaryadd( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_summaryadd( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavTabela = new GXCombobox();
         chkavSelected = new GXCheckbox();
         dynavAreatrabalho_codigo = new GXCombobox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_11 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_11_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_11_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               A456ContagemResultado_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               n456ContagemResultado_Codigo = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
               ajax_req_read_hidden_sdt(GetNextPar( ), AV7Codigos);
               A590ContagemResultadoEvidencia_TipoArq = GetNextPar( );
               n590ContagemResultadoEvidencia_TipoArq = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A590ContagemResultadoEvidencia_TipoArq", A590ContagemResultadoEvidencia_TipoArq);
               A586ContagemResultadoEvidencia_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A586ContagemResultadoEvidencia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A586ContagemResultadoEvidencia_Codigo), 6, 0)));
               A589ContagemResultadoEvidencia_NomeArq = GetNextPar( );
               n589ContagemResultadoEvidencia_NomeArq = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A589ContagemResultadoEvidencia_NomeArq", A589ContagemResultadoEvidencia_NomeArq);
               A501ContagemResultado_OsFsOsFm = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A501ContagemResultado_OsFsOsFm", A501ContagemResultado_OsFsOsFm);
               A495ContagemResultado_SistemaNom = GetNextPar( );
               n495ContagemResultado_SistemaNom = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A495ContagemResultado_SistemaNom", A495ContagemResultado_SistemaNom);
               A52Contratada_AreaTrabalhoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               n52Contratada_AreaTrabalhoCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A52Contratada_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A52Contratada_AreaTrabalhoCod), 6, 0)));
               A566ContagemResultado_DataUltCnt = context.localUtil.ParseDateParm( GetNextPar( ));
               n566ContagemResultado_DataUltCnt = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A566ContagemResultado_DataUltCnt", context.localUtil.Format(A566ContagemResultado_DataUltCnt, "99/99/99"));
               A9Contratante_RazaoSocial = GetNextPar( );
               n9Contratante_RazaoSocial = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A9Contratante_RazaoSocial", A9Contratante_RazaoSocial);
               A31Contratante_Telefone = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A31Contratante_Telefone", A31Contratante_Telefone);
               A14Contratante_Email = GetNextPar( );
               n14Contratante_Email = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A14Contratante_Email", A14Contratante_Email);
               A519Pessoa_Endereco = GetNextPar( );
               n519Pessoa_Endereco = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A519Pessoa_Endereco", A519Pessoa_Endereco);
               A26Municipio_Nome = GetNextPar( );
               n26Municipio_Nome = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A26Municipio_Nome", A26Municipio_Nome);
               A23Estado_UF = GetNextPar( );
               n23Estado_UF = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A23Estado_UF", A23Estado_UF);
               A521Pessoa_CEP = GetNextPar( );
               n521Pessoa_CEP = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A521Pessoa_CEP", A521Pessoa_CEP);
               A1109AnexoDe_Id = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1109AnexoDe_Id", StringUtil.LTrim( StringUtil.Str( (decimal)(A1109AnexoDe_Id), 6, 0)));
               A1110AnexoDe_Tabela = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1110AnexoDe_Tabela", StringUtil.LTrim( StringUtil.Str( (decimal)(A1110AnexoDe_Tabela), 6, 0)));
               A1108Anexo_TipoArq = GetNextPar( );
               n1108Anexo_TipoArq = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1108Anexo_TipoArq", A1108Anexo_TipoArq);
               A1106Anexo_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1106Anexo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1106Anexo_Codigo), 6, 0)));
               A1107Anexo_NomeArq = GetNextPar( );
               n1107Anexo_NomeArq = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1107Anexo_NomeArq", A1107Anexo_NomeArq);
               A517ContagemResultado_Ultima = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A517ContagemResultado_Ultima", A517ContagemResultado_Ultima);
               A473ContagemResultado_DataCnt = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A473ContagemResultado_DataCnt", context.localUtil.Format(A473ContagemResultado_DataCnt, "99/99/99"));
               A474ContagemResultado_ContadorFMNom = GetNextPar( );
               n474ContagemResultado_ContadorFMNom = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A474ContagemResultado_ContadorFMNom", A474ContagemResultado_ContadorFMNom);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( A456ContagemResultado_Codigo, AV7Codigos, A590ContagemResultadoEvidencia_TipoArq, A586ContagemResultadoEvidencia_Codigo, A589ContagemResultadoEvidencia_NomeArq, A501ContagemResultado_OsFsOsFm, A495ContagemResultado_SistemaNom, A52Contratada_AreaTrabalhoCod, A566ContagemResultado_DataUltCnt, A9Contratante_RazaoSocial, A31Contratante_Telefone, A14Contratante_Email, A519Pessoa_Endereco, A26Municipio_Nome, A23Estado_UF, A521Pessoa_CEP, A1109AnexoDe_Id, A1110AnexoDe_Tabela, A1108Anexo_TipoArq, A1106Anexo_Codigo, A1107Anexo_NomeArq, A517ContagemResultado_Ultima, A473ContagemResultado_DataCnt, A474ContagemResultado_ContadorFMNom) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PARD2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTRD2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020621630111");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wp_summaryadd.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_11", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_11), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A456ContagemResultado_Codigo), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCODIGOS", AV7Codigos);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCODIGOS", AV7Codigos);
         }
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADOEVIDENCIA_TIPOARQ", StringUtil.RTrim( A590ContagemResultadoEvidencia_TipoArq));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADOEVIDENCIA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A586ContagemResultadoEvidencia_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADOEVIDENCIA_NOMEARQ", StringUtil.RTrim( A589ContagemResultadoEvidencia_NomeArq));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_SISTEMANOM", A495ContagemResultado_SistemaNom);
         GxWebStd.gx_hidden_field( context, "CONTRATADA_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A52Contratada_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_DATAULTCNT", context.localUtil.DToC( A566ContagemResultado_DataUltCnt, 0, "/"));
         GxWebStd.gx_hidden_field( context, "CONTRATANTE_RAZAOSOCIAL", StringUtil.RTrim( A9Contratante_RazaoSocial));
         GxWebStd.gx_hidden_field( context, "CONTRATANTE_TELEFONE", StringUtil.RTrim( A31Contratante_Telefone));
         GxWebStd.gx_hidden_field( context, "CONTRATANTE_EMAIL", A14Contratante_Email);
         GxWebStd.gx_hidden_field( context, "PESSOA_ENDERECO", A519Pessoa_Endereco);
         GxWebStd.gx_hidden_field( context, "MUNICIPIO_NOME", StringUtil.RTrim( A26Municipio_Nome));
         GxWebStd.gx_hidden_field( context, "ESTADO_UF", StringUtil.RTrim( A23Estado_UF));
         GxWebStd.gx_hidden_field( context, "PESSOA_CEP", StringUtil.RTrim( A521Pessoa_CEP));
         GxWebStd.gx_hidden_field( context, "ANEXODE_ID", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1109AnexoDe_Id), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "ANEXODE_TABELA", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1110AnexoDe_Tabela), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "ANEXO_TIPOARQ", StringUtil.RTrim( A1108Anexo_TipoArq));
         GxWebStd.gx_hidden_field( context, "ANEXO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1106Anexo_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "ANEXO_NOMEARQ", StringUtil.RTrim( A1107Anexo_NomeArq));
         GxWebStd.gx_boolean_hidden_field( context, "CONTAGEMRESULTADO_ULTIMA", A517ContagemResultado_Ultima);
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_DATACNT", context.localUtil.DToC( A473ContagemResultado_DataCnt, 0, "/"));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_CONTADORFMNOM", StringUtil.RTrim( A474ContagemResultado_ContadorFMNom));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADOEVIDENCIA_ARQUIVO", A588ContagemResultadoEvidencia_Arquivo);
         GxWebStd.gx_hidden_field( context, "ANEXO_ARQUIVO", A1101Anexo_Arquivo);
         GxWebStd.gx_hidden_field( context, "vPLANILHA", AV17Planilha);
         GxWebStd.gx_hidden_field( context, "vCOLABORADOR_NOME", StringUtil.RTrim( AV33Colaborador_Nome));
         GxWebStd.gx_hidden_field( context, "vC", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV22c), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vL", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV23l), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCAMPO", AV24Campo);
         GxWebStd.gx_hidden_field( context, "PARAMETROSPLN_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A847ParametrosPln_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "PARAMETROSPLN_CAMPO", A849ParametrosPln_Campo);
         GxWebStd.gx_hidden_field( context, "PARAMETROSPLN_ABA", StringUtil.RTrim( A2015ParametrosPln_Aba));
         GxWebStd.gx_hidden_field( context, "PARAMETROSPLN_LINHA", StringUtil.LTrim( StringUtil.NToC( (decimal)(A851ParametrosPln_Linha), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "PARAMETROSPLN_COLUNA", StringUtil.RTrim( A850ParametrosPln_Coluna));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_DEMANDA", A457ContagemResultado_Demanda);
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_DEMANDAFM", A493ContagemResultado_DemandaFM);
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_OSFSOSFM", A501ContagemResultado_OsFsOsFm);
         GXCCtlgxBlob = "vBLOB" + "_gxBlob";
         GxWebStd.gx_hidden_field( context, GXCCtlgxBlob, AV12Blob);
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WERD2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTRD2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wp_summaryadd.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WP_SummaryAdd" ;
      }

      public override String GetPgmdesc( )
      {
         return "Summary Add" ;
      }

      protected void WBRD0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_RD2( true) ;
         }
         else
         {
            wb_table1_2_RD2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_RD2e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void STARTRD2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Summary Add", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPRD0( ) ;
      }

      protected void WSRD2( )
      {
         STARTRD2( ) ;
         EVTRD2( ) ;
      }

      protected void EVTRD2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ! wbErr )
                              {
                                 Rfr0gs = false;
                                 if ( ! Rfr0gs )
                                 {
                                    /* Execute user event: E11RD2 */
                                    E11RD2 ();
                                 }
                                 dynload_actions( ) ;
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) )
                           {
                              nGXsfl_11_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_11_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_11_idx), 4, 0)), 4, "0");
                              SubsflControlProps_112( ) ;
                              if ( context.localUtil.VCDateTime( cgiGet( edtavDatacnt_Internalname), 0, 0) == 0 )
                              {
                                 GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Data Cnt"}), 1, "vDATACNT");
                                 GX_FocusControl = edtavDatacnt_Internalname;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                                 wbErr = true;
                                 AV32DataCnt = (DateTime)(DateTime.MinValue);
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDatacnt_Internalname, context.localUtil.Format(AV32DataCnt, "99/99/99"));
                              }
                              else
                              {
                                 AV32DataCnt = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDatacnt_Internalname), 0));
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDatacnt_Internalname, context.localUtil.Format(AV32DataCnt, "99/99/99"));
                              }
                              AV34Contratante_RazaoSocial = StringUtil.Upper( cgiGet( edtavContratante_razaosocial_Internalname));
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavContratante_razaosocial_Internalname, AV34Contratante_RazaoSocial);
                              AV35Contratante_Telefone = cgiGet( edtavContratante_telefone_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavContratante_telefone_Internalname, AV35Contratante_Telefone);
                              AV36Contratante_Email = cgiGet( edtavContratante_email_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavContratante_email_Internalname, AV36Contratante_Email);
                              AV37Contratante_Endereco = cgiGet( edtavContratante_endereco_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavContratante_endereco_Internalname, AV37Contratante_Endereco);
                              cmbavTabela.Name = cmbavTabela_Internalname;
                              cmbavTabela.CurrentValue = cgiGet( cmbavTabela_Internalname);
                              AV10Tabela = (int)(NumberUtil.Val( cgiGet( cmbavTabela_Internalname), "."));
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, cmbavTabela_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV10Tabela), 6, 0)));
                              if ( ( ( context.localUtil.CToN( cgiGet( edtavLocal_codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavLocal_codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
                              {
                                 GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vLOCAL_CODIGO");
                                 GX_FocusControl = edtavLocal_codigo_Internalname;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                                 wbErr = true;
                                 AV9Local_Codigo = 0;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavLocal_codigo_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV9Local_Codigo), 6, 0)));
                              }
                              else
                              {
                                 AV9Local_Codigo = (int)(context.localUtil.CToN( cgiGet( edtavLocal_codigo_Internalname), ",", "."));
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavLocal_codigo_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV9Local_Codigo), 6, 0)));
                              }
                              AV8Selected = StringUtil.StrToBool( cgiGet( chkavSelected_Internalname));
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, chkavSelected_Internalname, AV8Selected);
                              dynavAreatrabalho_codigo.Name = dynavAreatrabalho_codigo_Internalname;
                              dynavAreatrabalho_codigo.CurrentValue = cgiGet( dynavAreatrabalho_codigo_Internalname);
                              AV25AreaTrabalho_Codigo = (int)(NumberUtil.Val( cgiGet( dynavAreatrabalho_codigo_Internalname), "."));
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, dynavAreatrabalho_codigo_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV25AreaTrabalho_Codigo), 6, 0)));
                              AV20OsFsOsFm = cgiGet( edtavOsfsosfm_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavOsfsosfm_Internalname, AV20OsFsOsFm);
                              AV30Sistema_Nome = StringUtil.Upper( cgiGet( edtavSistema_nome_Internalname));
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavSistema_nome_Internalname, AV30Sistema_Nome);
                              AV11File_Nome = cgiGet( edtavFile_nome_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavFile_nome_Internalname, AV11File_Nome);
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( AV12Blob)) )
                              {
                                 GXCCtl = "vBLOB_" + sGXsfl_11_idx;
                                 GXCCtlgxBlob = GXCCtl + "_gxBlob";
                                 AV12Blob = cgiGet( GXCCtlgxBlob);
                              }
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E12RD2 */
                                    E12RD2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E13RD2 */
                                    E13RD2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E14RD2 */
                                    E14RD2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WERD2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PARD2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            GXCCtl = "vTABELA_" + sGXsfl_11_idx;
            cmbavTabela.Name = GXCCtl;
            cmbavTabela.WebTags = "";
            cmbavTabela.addItem("1", "Contagem Resultado", 0);
            cmbavTabela.addItem("2", "Evid�ncias", 0);
            cmbavTabela.addItem("3", "Anexos", 0);
            if ( cmbavTabela.ItemCount > 0 )
            {
               AV10Tabela = (int)(NumberUtil.Val( cmbavTabela.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV10Tabela), 6, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, cmbavTabela_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV10Tabela), 6, 0)));
            }
            GXCCtl = "vSELECTED_" + sGXsfl_11_idx;
            chkavSelected.Name = GXCCtl;
            chkavSelected.WebTags = "";
            chkavSelected.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavSelected_Internalname, "TitleCaption", chkavSelected.Caption);
            chkavSelected.CheckedValue = "false";
            GXCCtl = "vAREATRABALHO_CODIGO_" + sGXsfl_11_idx;
            dynavAreatrabalho_codigo.Name = GXCCtl;
            dynavAreatrabalho_codigo.WebTags = "";
            dynavAreatrabalho_codigo.removeAllItems();
            /* Using cursor H00RD2 */
            pr_default.execute(0);
            while ( (pr_default.getStatus(0) != 101) )
            {
               dynavAreatrabalho_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(H00RD2_A5AreaTrabalho_Codigo[0]), 6, 0)), H00RD2_A6AreaTrabalho_Descricao[0], 0);
               pr_default.readNext(0);
            }
            pr_default.close(0);
            if ( dynavAreatrabalho_codigo.ItemCount > 0 )
            {
               AV25AreaTrabalho_Codigo = (int)(NumberUtil.Val( dynavAreatrabalho_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV25AreaTrabalho_Codigo), 6, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, dynavAreatrabalho_codigo_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV25AreaTrabalho_Codigo), 6, 0)));
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavBlob_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_112( ) ;
         while ( nGXsfl_11_idx <= nRC_GXsfl_11 )
         {
            sendrow_112( ) ;
            nGXsfl_11_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_11_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_11_idx+1));
            sGXsfl_11_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_11_idx), 4, 0)), 4, "0");
            SubsflControlProps_112( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int A456ContagemResultado_Codigo ,
                                       IGxCollection AV7Codigos ,
                                       String A590ContagemResultadoEvidencia_TipoArq ,
                                       int A586ContagemResultadoEvidencia_Codigo ,
                                       String A589ContagemResultadoEvidencia_NomeArq ,
                                       String A501ContagemResultado_OsFsOsFm ,
                                       String A495ContagemResultado_SistemaNom ,
                                       int A52Contratada_AreaTrabalhoCod ,
                                       DateTime A566ContagemResultado_DataUltCnt ,
                                       String A9Contratante_RazaoSocial ,
                                       String A31Contratante_Telefone ,
                                       String A14Contratante_Email ,
                                       String A519Pessoa_Endereco ,
                                       String A26Municipio_Nome ,
                                       String A23Estado_UF ,
                                       String A521Pessoa_CEP ,
                                       int A1109AnexoDe_Id ,
                                       int A1110AnexoDe_Tabela ,
                                       String A1108Anexo_TipoArq ,
                                       int A1106Anexo_Codigo ,
                                       String A1107Anexo_NomeArq ,
                                       bool A517ContagemResultado_Ultima ,
                                       DateTime A473ContagemResultado_DataCnt ,
                                       String A474ContagemResultado_ContadorFMNom )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GRID_nCurrentRecord = 0;
         RFRD2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFRD2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavDatacnt_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDatacnt_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDatacnt_Enabled), 5, 0)));
         edtavContratante_razaosocial_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratante_razaosocial_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratante_razaosocial_Enabled), 5, 0)));
         edtavContratante_telefone_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratante_telefone_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratante_telefone_Enabled), 5, 0)));
         edtavContratante_email_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratante_email_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratante_email_Enabled), 5, 0)));
         edtavContratante_endereco_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratante_endereco_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratante_endereco_Enabled), 5, 0)));
         cmbavTabela.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavTabela_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavTabela.Enabled), 5, 0)));
         edtavLocal_codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavLocal_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavLocal_codigo_Enabled), 5, 0)));
         dynavAreatrabalho_codigo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavAreatrabalho_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavAreatrabalho_codigo.Enabled), 5, 0)));
         edtavOsfsosfm_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOsfsosfm_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOsfsosfm_Enabled), 5, 0)));
         edtavSistema_nome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSistema_nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSistema_nome_Enabled), 5, 0)));
         edtavFile_nome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFile_nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFile_nome_Enabled), 5, 0)));
      }

      protected void RFRD2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 11;
         /* Execute user event: E13RD2 */
         E13RD2 ();
         nGXsfl_11_idx = 1;
         sGXsfl_11_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_11_idx), 4, 0)), 4, "0");
         SubsflControlProps_112( ) ;
         nGXsfl_11_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "Grid");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorodd", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorodd), 9, 0, ".", "")));
         GridContainer.AddObjectProperty("Titleforecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Titleforecolor), 9, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_112( ) ;
            /* Execute user event: E14RD2 */
            E14RD2 ();
            wbEnd = 11;
            WBRD0( ) ;
         }
         nGXsfl_11_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         return (int)(-1) ;
      }

      protected int subGrid_Recordcount( )
      {
         return (int)(-1) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         return (int)(-1) ;
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(-1) ;
      }

      protected void STRUPRD0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         edtavDatacnt_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDatacnt_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDatacnt_Enabled), 5, 0)));
         edtavContratante_razaosocial_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratante_razaosocial_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratante_razaosocial_Enabled), 5, 0)));
         edtavContratante_telefone_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratante_telefone_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratante_telefone_Enabled), 5, 0)));
         edtavContratante_email_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratante_email_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratante_email_Enabled), 5, 0)));
         edtavContratante_endereco_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratante_endereco_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratante_endereco_Enabled), 5, 0)));
         cmbavTabela.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavTabela_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavTabela.Enabled), 5, 0)));
         edtavLocal_codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavLocal_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavLocal_codigo_Enabled), 5, 0)));
         dynavAreatrabalho_codigo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavAreatrabalho_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavAreatrabalho_codigo.Enabled), 5, 0)));
         edtavOsfsosfm_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOsfsosfm_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOsfsosfm_Enabled), 5, 0)));
         edtavSistema_nome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSistema_nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSistema_nome_Enabled), 5, 0)));
         edtavFile_nome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFile_nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFile_nome_Enabled), 5, 0)));
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E12RD2 */
         E12RD2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            AV12Blob = cgiGet( edtavBlob_Internalname);
            /* Read saved values. */
            nRC_GXsfl_11 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_11"), ",", "."));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            if ( String.IsNullOrEmpty(StringUtil.RTrim( AV12Blob)) )
            {
               GXCCtlgxBlob = "vBLOB" + "_gxBlob";
               AV12Blob = cgiGet( GXCCtlgxBlob);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E12RD2 */
         E12RD2 ();
         if (returnInSub) return;
      }

      protected void E12RD2( )
      {
         /* Start Routine */
         AV5SDT_Codigos.FromXml(AV19WebSession.Get("Codigos"), "SDT_CodigosCollection");
         AV40GXV1 = 1;
         while ( AV40GXV1 <= AV5SDT_Codigos.Count )
         {
            AV6Codigo = ((SdtSDT_Codigos)AV5SDT_Codigos.Item(AV40GXV1));
            AV7Codigos.Add(AV6Codigo.gxTpr_Codigo, 0);
            AV40GXV1 = (int)(AV40GXV1+1);
         }
         edtavBlob_Display = 1;
         bttBtnfechar_Jsonclick = "self.close()";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnfechar_Internalname, "Jsonclick", bttBtnfechar_Jsonclick);
      }

      protected void E13RD2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV8Selected = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, chkavSelected_Internalname, AV8Selected);
         edtavOsfsosfm_Linktarget = "_blank";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOsfsosfm_Internalname, "Linktarget", edtavOsfsosfm_Linktarget);
      }

      private void E14RD2( )
      {
         /* Grid_Load Routine */
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              A456ContagemResultado_Codigo ,
                                              AV7Codigos ,
                                              A590ContagemResultadoEvidencia_TipoArq },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN
                                              }
         });
         /* Using cursor H00RD5 */
         pr_default.execute(1);
         while ( (pr_default.getStatus(1) != 101) )
         {
            A146Modulo_Codigo = H00RD5_A146Modulo_Codigo[0];
            n146Modulo_Codigo = H00RD5_n146Modulo_Codigo[0];
            A127Sistema_Codigo = H00RD5_A127Sistema_Codigo[0];
            A1399Sistema_ImpUserCod = H00RD5_A1399Sistema_ImpUserCod[0];
            n1399Sistema_ImpUserCod = H00RD5_n1399Sistema_ImpUserCod[0];
            A1402Sistema_ImpUserPesCod = H00RD5_A1402Sistema_ImpUserPesCod[0];
            n1402Sistema_ImpUserPesCod = H00RD5_n1402Sistema_ImpUserPesCod[0];
            A503Pessoa_MunicipioCod = H00RD5_A503Pessoa_MunicipioCod[0];
            n503Pessoa_MunicipioCod = H00RD5_n503Pessoa_MunicipioCod[0];
            A135Sistema_AreaTrabalhoCod = H00RD5_A135Sistema_AreaTrabalhoCod[0];
            A1073Usuario_CargoCod = H00RD5_A1073Usuario_CargoCod[0];
            n1073Usuario_CargoCod = H00RD5_n1073Usuario_CargoCod[0];
            A1075Usuario_CargoUOCod = H00RD5_A1075Usuario_CargoUOCod[0];
            n1075Usuario_CargoUOCod = H00RD5_n1075Usuario_CargoUOCod[0];
            A490ContagemResultado_ContratadaCod = H00RD5_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = H00RD5_n490ContagemResultado_ContratadaCod[0];
            A489ContagemResultado_SistemaCod = H00RD5_A489ContagemResultado_SistemaCod[0];
            n489ContagemResultado_SistemaCod = H00RD5_n489ContagemResultado_SistemaCod[0];
            A29Contratante_Codigo = H00RD5_A29Contratante_Codigo[0];
            n29Contratante_Codigo = H00RD5_n29Contratante_Codigo[0];
            A335Contratante_PessoaCod = H00RD5_A335Contratante_PessoaCod[0];
            A590ContagemResultadoEvidencia_TipoArq = H00RD5_A590ContagemResultadoEvidencia_TipoArq[0];
            n590ContagemResultadoEvidencia_TipoArq = H00RD5_n590ContagemResultadoEvidencia_TipoArq[0];
            A588ContagemResultadoEvidencia_Arquivo_Filetype = A590ContagemResultadoEvidencia_TipoArq;
            A456ContagemResultado_Codigo = H00RD5_A456ContagemResultado_Codigo[0];
            n456ContagemResultado_Codigo = H00RD5_n456ContagemResultado_Codigo[0];
            A586ContagemResultadoEvidencia_Codigo = H00RD5_A586ContagemResultadoEvidencia_Codigo[0];
            A589ContagemResultadoEvidencia_NomeArq = H00RD5_A589ContagemResultadoEvidencia_NomeArq[0];
            n589ContagemResultadoEvidencia_NomeArq = H00RD5_n589ContagemResultadoEvidencia_NomeArq[0];
            A588ContagemResultadoEvidencia_Arquivo_Filename = A589ContagemResultadoEvidencia_NomeArq;
            A495ContagemResultado_SistemaNom = H00RD5_A495ContagemResultado_SistemaNom[0];
            n495ContagemResultado_SistemaNom = H00RD5_n495ContagemResultado_SistemaNom[0];
            A52Contratada_AreaTrabalhoCod = H00RD5_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = H00RD5_n52Contratada_AreaTrabalhoCod[0];
            A9Contratante_RazaoSocial = H00RD5_A9Contratante_RazaoSocial[0];
            n9Contratante_RazaoSocial = H00RD5_n9Contratante_RazaoSocial[0];
            A31Contratante_Telefone = H00RD5_A31Contratante_Telefone[0];
            A14Contratante_Email = H00RD5_A14Contratante_Email[0];
            n14Contratante_Email = H00RD5_n14Contratante_Email[0];
            A521Pessoa_CEP = H00RD5_A521Pessoa_CEP[0];
            n521Pessoa_CEP = H00RD5_n521Pessoa_CEP[0];
            A23Estado_UF = H00RD5_A23Estado_UF[0];
            n23Estado_UF = H00RD5_n23Estado_UF[0];
            A26Municipio_Nome = H00RD5_A26Municipio_Nome[0];
            n26Municipio_Nome = H00RD5_n26Municipio_Nome[0];
            A519Pessoa_Endereco = H00RD5_A519Pessoa_Endereco[0];
            n519Pessoa_Endereco = H00RD5_n519Pessoa_Endereco[0];
            A566ContagemResultado_DataUltCnt = H00RD5_A566ContagemResultado_DataUltCnt[0];
            n566ContagemResultado_DataUltCnt = H00RD5_n566ContagemResultado_DataUltCnt[0];
            A40000ContagemResultado_ContadorFMNom = H00RD5_A40000ContagemResultado_ContadorFMNom[0];
            n40000ContagemResultado_ContadorFMNom = H00RD5_n40000ContagemResultado_ContadorFMNom[0];
            A493ContagemResultado_DemandaFM = H00RD5_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = H00RD5_n493ContagemResultado_DemandaFM[0];
            A457ContagemResultado_Demanda = H00RD5_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = H00RD5_n457ContagemResultado_Demanda[0];
            A146Modulo_Codigo = H00RD5_A146Modulo_Codigo[0];
            n146Modulo_Codigo = H00RD5_n146Modulo_Codigo[0];
            A490ContagemResultado_ContratadaCod = H00RD5_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = H00RD5_n490ContagemResultado_ContratadaCod[0];
            A489ContagemResultado_SistemaCod = H00RD5_A489ContagemResultado_SistemaCod[0];
            n489ContagemResultado_SistemaCod = H00RD5_n489ContagemResultado_SistemaCod[0];
            A493ContagemResultado_DemandaFM = H00RD5_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = H00RD5_n493ContagemResultado_DemandaFM[0];
            A457ContagemResultado_Demanda = H00RD5_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = H00RD5_n457ContagemResultado_Demanda[0];
            A127Sistema_Codigo = H00RD5_A127Sistema_Codigo[0];
            A1399Sistema_ImpUserCod = H00RD5_A1399Sistema_ImpUserCod[0];
            n1399Sistema_ImpUserCod = H00RD5_n1399Sistema_ImpUserCod[0];
            A135Sistema_AreaTrabalhoCod = H00RD5_A135Sistema_AreaTrabalhoCod[0];
            A1402Sistema_ImpUserPesCod = H00RD5_A1402Sistema_ImpUserPesCod[0];
            n1402Sistema_ImpUserPesCod = H00RD5_n1402Sistema_ImpUserPesCod[0];
            A1073Usuario_CargoCod = H00RD5_A1073Usuario_CargoCod[0];
            n1073Usuario_CargoCod = H00RD5_n1073Usuario_CargoCod[0];
            A503Pessoa_MunicipioCod = H00RD5_A503Pessoa_MunicipioCod[0];
            n503Pessoa_MunicipioCod = H00RD5_n503Pessoa_MunicipioCod[0];
            A521Pessoa_CEP = H00RD5_A521Pessoa_CEP[0];
            n521Pessoa_CEP = H00RD5_n521Pessoa_CEP[0];
            A519Pessoa_Endereco = H00RD5_A519Pessoa_Endereco[0];
            n519Pessoa_Endereco = H00RD5_n519Pessoa_Endereco[0];
            A26Municipio_Nome = H00RD5_A26Municipio_Nome[0];
            n26Municipio_Nome = H00RD5_n26Municipio_Nome[0];
            A1075Usuario_CargoUOCod = H00RD5_A1075Usuario_CargoUOCod[0];
            n1075Usuario_CargoUOCod = H00RD5_n1075Usuario_CargoUOCod[0];
            A23Estado_UF = H00RD5_A23Estado_UF[0];
            n23Estado_UF = H00RD5_n23Estado_UF[0];
            A29Contratante_Codigo = H00RD5_A29Contratante_Codigo[0];
            n29Contratante_Codigo = H00RD5_n29Contratante_Codigo[0];
            A335Contratante_PessoaCod = H00RD5_A335Contratante_PessoaCod[0];
            A31Contratante_Telefone = H00RD5_A31Contratante_Telefone[0];
            A14Contratante_Email = H00RD5_A14Contratante_Email[0];
            n14Contratante_Email = H00RD5_n14Contratante_Email[0];
            A9Contratante_RazaoSocial = H00RD5_A9Contratante_RazaoSocial[0];
            n9Contratante_RazaoSocial = H00RD5_n9Contratante_RazaoSocial[0];
            A52Contratada_AreaTrabalhoCod = H00RD5_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = H00RD5_n52Contratada_AreaTrabalhoCod[0];
            A495ContagemResultado_SistemaNom = H00RD5_A495ContagemResultado_SistemaNom[0];
            n495ContagemResultado_SistemaNom = H00RD5_n495ContagemResultado_SistemaNom[0];
            A566ContagemResultado_DataUltCnt = H00RD5_A566ContagemResultado_DataUltCnt[0];
            n566ContagemResultado_DataUltCnt = H00RD5_n566ContagemResultado_DataUltCnt[0];
            A40000ContagemResultado_ContadorFMNom = H00RD5_A40000ContagemResultado_ContadorFMNom[0];
            n40000ContagemResultado_ContadorFMNom = H00RD5_n40000ContagemResultado_ContadorFMNom[0];
            A501ContagemResultado_OsFsOsFm = StringUtil.Trim( A457ContagemResultado_Demanda) + (String.IsNullOrEmpty(StringUtil.RTrim( A493ContagemResultado_DemandaFM)) ? "" : "|"+StringUtil.Trim( A493ContagemResultado_DemandaFM));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A501ContagemResultado_OsFsOsFm", A501ContagemResultado_OsFsOsFm);
            AV10Tabela = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, cmbavTabela_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV10Tabela), 6, 0)));
            AV9Local_Codigo = A586ContagemResultadoEvidencia_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavLocal_codigo_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV9Local_Codigo), 6, 0)));
            AV11File_Nome = StringUtil.Trim( A589ContagemResultadoEvidencia_NomeArq) + "." + A590ContagemResultadoEvidencia_TipoArq;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavFile_nome_Internalname, AV11File_Nome);
            AV20OsFsOsFm = A501ContagemResultado_OsFsOsFm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavOsfsosfm_Internalname, AV20OsFsOsFm);
            edtavOsfsosfm_Link = formatLink("viewcontagemresultado.aspx") + "?" + UrlEncode("" +A456ContagemResultado_Codigo) + "," + UrlEncode(StringUtil.RTrim("ContagemResultadoEvidencia"));
            AV30Sistema_Nome = A495ContagemResultado_SistemaNom;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavSistema_nome_Internalname, AV30Sistema_Nome);
            AV25AreaTrabalho_Codigo = A52Contratada_AreaTrabalhoCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, dynavAreatrabalho_codigo_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV25AreaTrabalho_Codigo), 6, 0)));
            AV32DataCnt = A566ContagemResultado_DataUltCnt;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDatacnt_Internalname, context.localUtil.Format(AV32DataCnt, "99/99/99"));
            AV33Colaborador_Nome = A40000ContagemResultado_ContadorFMNom;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33Colaborador_Nome", AV33Colaborador_Nome);
            AV34Contratante_RazaoSocial = A9Contratante_RazaoSocial;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavContratante_razaosocial_Internalname, AV34Contratante_RazaoSocial);
            AV35Contratante_Telefone = A31Contratante_Telefone;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavContratante_telefone_Internalname, AV35Contratante_Telefone);
            AV36Contratante_Email = A14Contratante_Email;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavContratante_email_Internalname, AV36Contratante_Email);
            AV37Contratante_Endereco = StringUtil.Trim( A519Pessoa_Endereco) + ", " + StringUtil.Trim( A26Municipio_Nome) + " - " + A23Estado_UF + " CEP " + A521Pessoa_CEP;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavContratante_endereco_Internalname, AV37Contratante_Endereco);
            /* Load Method */
            if ( wbStart != -1 )
            {
               wbStart = 11;
            }
            sendrow_112( ) ;
            if ( isFullAjaxMode( ) && ( nGXsfl_11_Refreshing == 0 ) )
            {
               context.DoAjaxLoad(11, GridRow);
            }
            pr_default.readNext(1);
         }
         pr_default.close(1);
         pr_default.dynParam(2, new Object[]{ new Object[]{
                                              A1109AnexoDe_Id ,
                                              AV7Codigos ,
                                              A1108Anexo_TipoArq ,
                                              A1110AnexoDe_Tabela },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT
                                              }
         });
         /* Using cursor H00RD6 */
         pr_default.execute(2);
         while ( (pr_default.getStatus(2) != 101) )
         {
            A1109AnexoDe_Id = H00RD6_A1109AnexoDe_Id[0];
            A1108Anexo_TipoArq = H00RD6_A1108Anexo_TipoArq[0];
            n1108Anexo_TipoArq = H00RD6_n1108Anexo_TipoArq[0];
            A1101Anexo_Arquivo_Filetype = A1108Anexo_TipoArq;
            A1110AnexoDe_Tabela = H00RD6_A1110AnexoDe_Tabela[0];
            A1106Anexo_Codigo = H00RD6_A1106Anexo_Codigo[0];
            A1107Anexo_NomeArq = H00RD6_A1107Anexo_NomeArq[0];
            n1107Anexo_NomeArq = H00RD6_n1107Anexo_NomeArq[0];
            A1101Anexo_Arquivo_Filename = A1107Anexo_NomeArq;
            A1108Anexo_TipoArq = H00RD6_A1108Anexo_TipoArq[0];
            n1108Anexo_TipoArq = H00RD6_n1108Anexo_TipoArq[0];
            A1101Anexo_Arquivo_Filetype = A1108Anexo_TipoArq;
            A1107Anexo_NomeArq = H00RD6_A1107Anexo_NomeArq[0];
            n1107Anexo_NomeArq = H00RD6_n1107Anexo_NomeArq[0];
            A1101Anexo_Arquivo_Filename = A1107Anexo_NomeArq;
            AV10Tabela = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, cmbavTabela_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV10Tabela), 6, 0)));
            AV9Local_Codigo = A1106Anexo_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavLocal_codigo_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV9Local_Codigo), 6, 0)));
            AV11File_Nome = StringUtil.Trim( A1107Anexo_NomeArq) + "." + A1108Anexo_TipoArq;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavFile_nome_Internalname, AV11File_Nome);
            /* Using cursor H00RD7 */
            pr_default.execute(3, new Object[] {A1109AnexoDe_Id});
            while ( (pr_default.getStatus(3) != 101) )
            {
               A454ContagemResultado_ContadorFSCod = H00RD7_A454ContagemResultado_ContadorFSCod[0];
               n454ContagemResultado_ContadorFSCod = H00RD7_n454ContagemResultado_ContadorFSCod[0];
               A480ContagemResultado_CrFSPessoaCod = H00RD7_A480ContagemResultado_CrFSPessoaCod[0];
               n480ContagemResultado_CrFSPessoaCod = H00RD7_n480ContagemResultado_CrFSPessoaCod[0];
               A503Pessoa_MunicipioCod = H00RD7_A503Pessoa_MunicipioCod[0];
               n503Pessoa_MunicipioCod = H00RD7_n503Pessoa_MunicipioCod[0];
               A1073Usuario_CargoCod = H00RD7_A1073Usuario_CargoCod[0];
               n1073Usuario_CargoCod = H00RD7_n1073Usuario_CargoCod[0];
               A1075Usuario_CargoUOCod = H00RD7_A1075Usuario_CargoUOCod[0];
               n1075Usuario_CargoUOCod = H00RD7_n1075Usuario_CargoUOCod[0];
               A468ContagemResultado_NaoCnfDmnCod = H00RD7_A468ContagemResultado_NaoCnfDmnCod[0];
               n468ContagemResultado_NaoCnfDmnCod = H00RD7_n468ContagemResultado_NaoCnfDmnCod[0];
               A428NaoConformidade_AreaTrabalhoCod = H00RD7_A428NaoConformidade_AreaTrabalhoCod[0];
               n428NaoConformidade_AreaTrabalhoCod = H00RD7_n428NaoConformidade_AreaTrabalhoCod[0];
               A490ContagemResultado_ContratadaCod = H00RD7_A490ContagemResultado_ContratadaCod[0];
               n490ContagemResultado_ContratadaCod = H00RD7_n490ContagemResultado_ContratadaCod[0];
               A489ContagemResultado_SistemaCod = H00RD7_A489ContagemResultado_SistemaCod[0];
               n489ContagemResultado_SistemaCod = H00RD7_n489ContagemResultado_SistemaCod[0];
               A470ContagemResultado_ContadorFMCod = H00RD7_A470ContagemResultado_ContadorFMCod[0];
               A479ContagemResultado_CrFMPessoaCod = H00RD7_A479ContagemResultado_CrFMPessoaCod[0];
               n479ContagemResultado_CrFMPessoaCod = H00RD7_n479ContagemResultado_CrFMPessoaCod[0];
               A29Contratante_Codigo = H00RD7_A29Contratante_Codigo[0];
               n29Contratante_Codigo = H00RD7_n29Contratante_Codigo[0];
               A335Contratante_PessoaCod = H00RD7_A335Contratante_PessoaCod[0];
               A456ContagemResultado_Codigo = H00RD7_A456ContagemResultado_Codigo[0];
               n456ContagemResultado_Codigo = H00RD7_n456ContagemResultado_Codigo[0];
               A517ContagemResultado_Ultima = H00RD7_A517ContagemResultado_Ultima[0];
               A495ContagemResultado_SistemaNom = H00RD7_A495ContagemResultado_SistemaNom[0];
               n495ContagemResultado_SistemaNom = H00RD7_n495ContagemResultado_SistemaNom[0];
               A52Contratada_AreaTrabalhoCod = H00RD7_A52Contratada_AreaTrabalhoCod[0];
               n52Contratada_AreaTrabalhoCod = H00RD7_n52Contratada_AreaTrabalhoCod[0];
               A473ContagemResultado_DataCnt = H00RD7_A473ContagemResultado_DataCnt[0];
               A474ContagemResultado_ContadorFMNom = H00RD7_A474ContagemResultado_ContadorFMNom[0];
               n474ContagemResultado_ContadorFMNom = H00RD7_n474ContagemResultado_ContadorFMNom[0];
               A9Contratante_RazaoSocial = H00RD7_A9Contratante_RazaoSocial[0];
               n9Contratante_RazaoSocial = H00RD7_n9Contratante_RazaoSocial[0];
               A31Contratante_Telefone = H00RD7_A31Contratante_Telefone[0];
               A14Contratante_Email = H00RD7_A14Contratante_Email[0];
               n14Contratante_Email = H00RD7_n14Contratante_Email[0];
               A521Pessoa_CEP = H00RD7_A521Pessoa_CEP[0];
               n521Pessoa_CEP = H00RD7_n521Pessoa_CEP[0];
               A23Estado_UF = H00RD7_A23Estado_UF[0];
               n23Estado_UF = H00RD7_n23Estado_UF[0];
               A26Municipio_Nome = H00RD7_A26Municipio_Nome[0];
               n26Municipio_Nome = H00RD7_n26Municipio_Nome[0];
               A519Pessoa_Endereco = H00RD7_A519Pessoa_Endereco[0];
               n519Pessoa_Endereco = H00RD7_n519Pessoa_Endereco[0];
               A493ContagemResultado_DemandaFM = H00RD7_A493ContagemResultado_DemandaFM[0];
               n493ContagemResultado_DemandaFM = H00RD7_n493ContagemResultado_DemandaFM[0];
               A457ContagemResultado_Demanda = H00RD7_A457ContagemResultado_Demanda[0];
               n457ContagemResultado_Demanda = H00RD7_n457ContagemResultado_Demanda[0];
               A479ContagemResultado_CrFMPessoaCod = H00RD7_A479ContagemResultado_CrFMPessoaCod[0];
               n479ContagemResultado_CrFMPessoaCod = H00RD7_n479ContagemResultado_CrFMPessoaCod[0];
               A474ContagemResultado_ContadorFMNom = H00RD7_A474ContagemResultado_ContadorFMNom[0];
               n474ContagemResultado_ContadorFMNom = H00RD7_n474ContagemResultado_ContadorFMNom[0];
               A454ContagemResultado_ContadorFSCod = H00RD7_A454ContagemResultado_ContadorFSCod[0];
               n454ContagemResultado_ContadorFSCod = H00RD7_n454ContagemResultado_ContadorFSCod[0];
               A468ContagemResultado_NaoCnfDmnCod = H00RD7_A468ContagemResultado_NaoCnfDmnCod[0];
               n468ContagemResultado_NaoCnfDmnCod = H00RD7_n468ContagemResultado_NaoCnfDmnCod[0];
               A490ContagemResultado_ContratadaCod = H00RD7_A490ContagemResultado_ContratadaCod[0];
               n490ContagemResultado_ContratadaCod = H00RD7_n490ContagemResultado_ContratadaCod[0];
               A489ContagemResultado_SistemaCod = H00RD7_A489ContagemResultado_SistemaCod[0];
               n489ContagemResultado_SistemaCod = H00RD7_n489ContagemResultado_SistemaCod[0];
               A493ContagemResultado_DemandaFM = H00RD7_A493ContagemResultado_DemandaFM[0];
               n493ContagemResultado_DemandaFM = H00RD7_n493ContagemResultado_DemandaFM[0];
               A457ContagemResultado_Demanda = H00RD7_A457ContagemResultado_Demanda[0];
               n457ContagemResultado_Demanda = H00RD7_n457ContagemResultado_Demanda[0];
               A480ContagemResultado_CrFSPessoaCod = H00RD7_A480ContagemResultado_CrFSPessoaCod[0];
               n480ContagemResultado_CrFSPessoaCod = H00RD7_n480ContagemResultado_CrFSPessoaCod[0];
               A1073Usuario_CargoCod = H00RD7_A1073Usuario_CargoCod[0];
               n1073Usuario_CargoCod = H00RD7_n1073Usuario_CargoCod[0];
               A503Pessoa_MunicipioCod = H00RD7_A503Pessoa_MunicipioCod[0];
               n503Pessoa_MunicipioCod = H00RD7_n503Pessoa_MunicipioCod[0];
               A521Pessoa_CEP = H00RD7_A521Pessoa_CEP[0];
               n521Pessoa_CEP = H00RD7_n521Pessoa_CEP[0];
               A519Pessoa_Endereco = H00RD7_A519Pessoa_Endereco[0];
               n519Pessoa_Endereco = H00RD7_n519Pessoa_Endereco[0];
               A26Municipio_Nome = H00RD7_A26Municipio_Nome[0];
               n26Municipio_Nome = H00RD7_n26Municipio_Nome[0];
               A1075Usuario_CargoUOCod = H00RD7_A1075Usuario_CargoUOCod[0];
               n1075Usuario_CargoUOCod = H00RD7_n1075Usuario_CargoUOCod[0];
               A23Estado_UF = H00RD7_A23Estado_UF[0];
               n23Estado_UF = H00RD7_n23Estado_UF[0];
               A428NaoConformidade_AreaTrabalhoCod = H00RD7_A428NaoConformidade_AreaTrabalhoCod[0];
               n428NaoConformidade_AreaTrabalhoCod = H00RD7_n428NaoConformidade_AreaTrabalhoCod[0];
               A29Contratante_Codigo = H00RD7_A29Contratante_Codigo[0];
               n29Contratante_Codigo = H00RD7_n29Contratante_Codigo[0];
               A335Contratante_PessoaCod = H00RD7_A335Contratante_PessoaCod[0];
               A31Contratante_Telefone = H00RD7_A31Contratante_Telefone[0];
               A14Contratante_Email = H00RD7_A14Contratante_Email[0];
               n14Contratante_Email = H00RD7_n14Contratante_Email[0];
               A9Contratante_RazaoSocial = H00RD7_A9Contratante_RazaoSocial[0];
               n9Contratante_RazaoSocial = H00RD7_n9Contratante_RazaoSocial[0];
               A52Contratada_AreaTrabalhoCod = H00RD7_A52Contratada_AreaTrabalhoCod[0];
               n52Contratada_AreaTrabalhoCod = H00RD7_n52Contratada_AreaTrabalhoCod[0];
               A495ContagemResultado_SistemaNom = H00RD7_A495ContagemResultado_SistemaNom[0];
               n495ContagemResultado_SistemaNom = H00RD7_n495ContagemResultado_SistemaNom[0];
               A501ContagemResultado_OsFsOsFm = StringUtil.Trim( A457ContagemResultado_Demanda) + (String.IsNullOrEmpty(StringUtil.RTrim( A493ContagemResultado_DemandaFM)) ? "" : "|"+StringUtil.Trim( A493ContagemResultado_DemandaFM));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A501ContagemResultado_OsFsOsFm", A501ContagemResultado_OsFsOsFm);
               AV20OsFsOsFm = A501ContagemResultado_OsFsOsFm;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavOsfsosfm_Internalname, AV20OsFsOsFm);
               edtavOsfsosfm_Link = formatLink("viewcontagemresultado.aspx") + "?" + UrlEncode("" +A456ContagemResultado_Codigo) + "," + UrlEncode(StringUtil.RTrim("ContagemResultadoEvidencia"));
               AV30Sistema_Nome = A495ContagemResultado_SistemaNom;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavSistema_nome_Internalname, AV30Sistema_Nome);
               AV25AreaTrabalho_Codigo = A52Contratada_AreaTrabalhoCod;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, dynavAreatrabalho_codigo_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV25AreaTrabalho_Codigo), 6, 0)));
               AV32DataCnt = A473ContagemResultado_DataCnt;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDatacnt_Internalname, context.localUtil.Format(AV32DataCnt, "99/99/99"));
               AV33Colaborador_Nome = A474ContagemResultado_ContadorFMNom;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33Colaborador_Nome", AV33Colaborador_Nome);
               AV34Contratante_RazaoSocial = A9Contratante_RazaoSocial;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavContratante_razaosocial_Internalname, AV34Contratante_RazaoSocial);
               AV35Contratante_Telefone = A31Contratante_Telefone;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavContratante_telefone_Internalname, AV35Contratante_Telefone);
               AV36Contratante_Email = A14Contratante_Email;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavContratante_email_Internalname, AV36Contratante_Email);
               AV37Contratante_Endereco = StringUtil.Trim( A519Pessoa_Endereco) + ", " + StringUtil.Trim( A26Municipio_Nome) + " - " + A23Estado_UF + " CEP " + A521Pessoa_CEP;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavContratante_endereco_Internalname, AV37Contratante_Endereco);
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               pr_default.readNext(3);
            }
            pr_default.close(3);
            /* Load Method */
            if ( wbStart != -1 )
            {
               wbStart = 11;
            }
            sendrow_112( ) ;
            if ( isFullAjaxMode( ) && ( nGXsfl_11_Refreshing == 0 ) )
            {
               context.DoAjaxLoad(11, GridRow);
            }
            pr_default.readNext(2);
         }
         pr_default.close(2);
         cmbavTabela.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV10Tabela), 6, 0));
         dynavAreatrabalho_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV25AreaTrabalho_Codigo), 6, 0));
      }

      public void GXEnter( )
      {
         /* Execute user event: E11RD2 */
         E11RD2 ();
         if (returnInSub) return;
      }

      protected void E11RD2( )
      {
         /* Enter Routine */
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV12Blob)) )
         {
            GX_msglist.addItem("Selecione a planilha de origem com a aba Summary!");
         }
         else
         {
            AV13ExcelOrigem.Open(AV12Blob);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12Blob", AV12Blob);
            AV13ExcelOrigem.SelectSheet("Summary");
            /* Start For Each Line */
            nRC_GXsfl_11 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_11"), ",", "."));
            nGXsfl_11_fel_idx = 0;
            while ( nGXsfl_11_fel_idx < nRC_GXsfl_11 )
            {
               nGXsfl_11_fel_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_11_fel_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_11_fel_idx+1));
               sGXsfl_11_fel_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_11_fel_idx), 4, 0)), 4, "0");
               SubsflControlProps_fel_112( ) ;
               if ( context.localUtil.VCDateTime( cgiGet( edtavDatacnt_Internalname), 0, 0) == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Data Cnt"}), 1, "vDATACNT");
                  GX_FocusControl = edtavDatacnt_Internalname;
                  wbErr = true;
                  AV32DataCnt = (DateTime)(DateTime.MinValue);
               }
               else
               {
                  AV32DataCnt = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDatacnt_Internalname), 0));
               }
               AV34Contratante_RazaoSocial = StringUtil.Upper( cgiGet( edtavContratante_razaosocial_Internalname));
               AV35Contratante_Telefone = cgiGet( edtavContratante_telefone_Internalname);
               AV36Contratante_Email = cgiGet( edtavContratante_email_Internalname);
               AV37Contratante_Endereco = cgiGet( edtavContratante_endereco_Internalname);
               cmbavTabela.Name = cmbavTabela_Internalname;
               cmbavTabela.CurrentValue = cgiGet( cmbavTabela_Internalname);
               AV10Tabela = (int)(NumberUtil.Val( cgiGet( cmbavTabela_Internalname), "."));
               if ( ( ( context.localUtil.CToN( cgiGet( edtavLocal_codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavLocal_codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vLOCAL_CODIGO");
                  GX_FocusControl = edtavLocal_codigo_Internalname;
                  wbErr = true;
                  AV9Local_Codigo = 0;
               }
               else
               {
                  AV9Local_Codigo = (int)(context.localUtil.CToN( cgiGet( edtavLocal_codigo_Internalname), ",", "."));
               }
               AV8Selected = StringUtil.StrToBool( cgiGet( chkavSelected_Internalname));
               dynavAreatrabalho_codigo.Name = dynavAreatrabalho_codigo_Internalname;
               dynavAreatrabalho_codigo.CurrentValue = cgiGet( dynavAreatrabalho_codigo_Internalname);
               AV25AreaTrabalho_Codigo = (int)(NumberUtil.Val( cgiGet( dynavAreatrabalho_codigo_Internalname), "."));
               AV20OsFsOsFm = cgiGet( edtavOsfsosfm_Internalname);
               AV30Sistema_Nome = StringUtil.Upper( cgiGet( edtavSistema_nome_Internalname));
               AV11File_Nome = cgiGet( edtavFile_nome_Internalname);
               if ( String.IsNullOrEmpty(StringUtil.RTrim( AV12Blob)) )
               {
                  GXCCtl = "vBLOB_" + sGXsfl_11_fel_idx;
                  GXCCtlgxBlob = GXCCtl + "_gxBlob";
                  AV12Blob = cgiGet( GXCCtlgxBlob);
               }
               if ( AV8Selected )
               {
                  if ( AV10Tabela == 2 )
                  {
                     /* Using cursor H00RD8 */
                     pr_default.execute(4, new Object[] {AV9Local_Codigo});
                     while ( (pr_default.getStatus(4) != 101) )
                     {
                        A586ContagemResultadoEvidencia_Codigo = H00RD8_A586ContagemResultadoEvidencia_Codigo[0];
                        A589ContagemResultadoEvidencia_NomeArq = H00RD8_A589ContagemResultadoEvidencia_NomeArq[0];
                        n589ContagemResultadoEvidencia_NomeArq = H00RD8_n589ContagemResultadoEvidencia_NomeArq[0];
                        A588ContagemResultadoEvidencia_Arquivo_Filename = A589ContagemResultadoEvidencia_NomeArq;
                        A590ContagemResultadoEvidencia_TipoArq = H00RD8_A590ContagemResultadoEvidencia_TipoArq[0];
                        n590ContagemResultadoEvidencia_TipoArq = H00RD8_n590ContagemResultadoEvidencia_TipoArq[0];
                        A588ContagemResultadoEvidencia_Arquivo_Filetype = A590ContagemResultadoEvidencia_TipoArq;
                        A588ContagemResultadoEvidencia_Arquivo = H00RD8_A588ContagemResultadoEvidencia_Arquivo[0];
                        n588ContagemResultadoEvidencia_Arquivo = H00RD8_n588ContagemResultadoEvidencia_Arquivo[0];
                        AV17Planilha = A588ContagemResultadoEvidencia_Arquivo;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Planilha", AV17Planilha);
                        /* Exit For each command. Update data (if necessary), close cursors & exit. */
                        if (true) break;
                        /* Exiting from a For First loop. */
                        if (true) break;
                     }
                     pr_default.close(4);
                  }
                  else if ( AV10Tabela == 3 )
                  {
                     /* Using cursor H00RD9 */
                     pr_default.execute(5, new Object[] {AV9Local_Codigo});
                     while ( (pr_default.getStatus(5) != 101) )
                     {
                        A1106Anexo_Codigo = H00RD9_A1106Anexo_Codigo[0];
                        A1107Anexo_NomeArq = H00RD9_A1107Anexo_NomeArq[0];
                        n1107Anexo_NomeArq = H00RD9_n1107Anexo_NomeArq[0];
                        A1101Anexo_Arquivo_Filename = A1107Anexo_NomeArq;
                        A1108Anexo_TipoArq = H00RD9_A1108Anexo_TipoArq[0];
                        n1108Anexo_TipoArq = H00RD9_n1108Anexo_TipoArq[0];
                        A1101Anexo_Arquivo_Filetype = A1108Anexo_TipoArq;
                        A1101Anexo_Arquivo = H00RD9_A1101Anexo_Arquivo[0];
                        n1101Anexo_Arquivo = H00RD9_n1101Anexo_Arquivo[0];
                        AV17Planilha = A1101Anexo_Arquivo;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Planilha", AV17Planilha);
                        /* Exit For each command. Update data (if necessary), close cursors & exit. */
                        if (true) break;
                        /* Exiting from a For First loop. */
                        if (true) break;
                     }
                     pr_default.close(5);
                  }
                  AV15ExcelDestino.Open(AV17Planilha);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Planilha", AV17Planilha);
                  AV15ExcelDestino.SelectSheet("Summary");
                  AV22c = 1;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22c", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22c), 4, 0)));
                  while ( AV22c <= 6 )
                  {
                     AV23l = 1;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23l", StringUtil.LTrim( StringUtil.Str( (decimal)(AV23l), 4, 0)));
                     while ( AV23l <= 55 )
                     {
                        AV21Text = AV13ExcelOrigem.get_Cells(AV23l, AV22c, 1, 1).Text;
                        AV24Campo = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Campo", AV24Campo);
                        if ( AV22c == 1 )
                        {
                           if ( AV23l == 50 )
                           {
                              GXt_dtime1 = DateTimeUtil.ResetTime( AV32DataCnt ) ;
                              AV15ExcelDestino.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                              AV15ExcelDestino.get_Cells(AV23l, AV22c, 1, 1).Date = GXt_dtime1;
                           }
                           else
                           {
                              AV15ExcelDestino.get_Cells(AV23l, AV22c, 1, 1).Text = AV13ExcelOrigem.get_Cells(AV23l, AV22c, 1, 1).Text;
                           }
                        }
                        else if ( AV22c == 2 )
                        {
                           if ( AV23l == 3 )
                           {
                              AV15ExcelDestino.get_Cells(AV23l, AV22c, 1, 1).Text = AV30Sistema_Nome;
                           }
                           else if ( AV23l == 4 )
                           {
                              AV15ExcelDestino.get_Cells(AV23l, AV22c, 1, 1).Text = AV30Sistema_Nome;
                           }
                           else if ( AV23l == 5 )
                           {
                              GXt_dtime1 = DateTimeUtil.ResetTime( AV32DataCnt ) ;
                              AV15ExcelDestino.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
                              AV15ExcelDestino.get_Cells(AV23l, AV22c, 1, 1).Date = GXt_dtime1;
                           }
                           else if ( AV23l == 6 )
                           {
                              AV15ExcelDestino.get_Cells(AV23l, AV22c, 1, 1).Text = "Enhancement project";
                           }
                           else if ( AV23l == 8 )
                           {
                              AV24Campo = "PDC";
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Campo", AV24Campo);
                           }
                           else if ( AV23l == 20 )
                           {
                              AV24Campo = "EDC";
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Campo", AV24Campo);
                           }
                           else if ( AV23l == 26 )
                           {
                              AV15ExcelDestino.get_Cells(AV23l, AV22c, 1, 1).Text = AV33Colaborador_Nome;
                           }
                           else if ( AV23l == 28 )
                           {
                              AV15ExcelDestino.get_Cells(AV23l, AV22c, 1, 1).Text = AV34Contratante_RazaoSocial;
                           }
                           else if ( AV23l == 29 )
                           {
                              AV15ExcelDestino.get_Cells(AV23l, AV22c, 1, 1).Text = AV35Contratante_Telefone;
                           }
                           else if ( AV23l == 30 )
                           {
                              AV15ExcelDestino.get_Cells(AV23l, AV22c, 1, 1).Text = AV36Contratante_Email;
                           }
                           else if ( AV23l == 31 )
                           {
                              AV15ExcelDestino.get_Cells(AV23l, AV22c, 1, 1).Text = AV37Contratante_Endereco;
                           }
                           else if ( AV23l == 39 )
                           {
                              AV24Campo = "ALIB";
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Campo", AV24Campo);
                           }
                           else if ( AV23l == 40 )
                           {
                              AV24Campo = "AIEB";
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Campo", AV24Campo);
                           }
                           else if ( AV23l == 41 )
                           {
                              AV24Campo = "EEB";
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Campo", AV24Campo);
                           }
                           else if ( AV23l == 42 )
                           {
                              AV24Campo = "SEB";
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Campo", AV24Campo);
                           }
                           else if ( AV23l == 43 )
                           {
                              AV24Campo = "CEB";
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Campo", AV24Campo);
                           }
                           else if ( AV23l == 45 )
                           {
                              AV24Campo = "PFT";
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Campo", AV24Campo);
                           }
                           else if ( AV23l == 46 )
                           {
                              AV15ExcelDestino.get_Cells(AV23l, AV22c, 1, 1).Number = AV13ExcelOrigem.get_Cells(AV23l, AV22c, 1, 1).Number;
                           }
                           else if ( AV23l == 47 )
                           {
                              AV15ExcelDestino.get_Cells(AV23l, AV22c, 1, 1).Number = (double)(AV13ExcelOrigem.get_Cells(45, AV22c, 1, 1).Number*AV13ExcelOrigem.get_Cells(46, AV22c, 1, 1).Number);
                           }
                           else
                           {
                              AV15ExcelDestino.get_Cells(AV23l, AV22c, 1, 1).Text = AV13ExcelOrigem.get_Cells(AV23l, AV22c, 1, 1).Text;
                           }
                        }
                        else if ( AV22c == 3 )
                        {
                           if ( AV23l == 39 )
                           {
                              AV24Campo = "ALIM";
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Campo", AV24Campo);
                           }
                           else if ( AV23l == 40 )
                           {
                              AV24Campo = "AIEM";
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Campo", AV24Campo);
                           }
                           else if ( AV23l == 41 )
                           {
                              AV24Campo = "EEM";
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Campo", AV24Campo);
                           }
                           else if ( AV23l == 42 )
                           {
                              AV24Campo = "SEM";
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Campo", AV24Campo);
                           }
                           else if ( AV23l == 43 )
                           {
                              AV24Campo = "CEM";
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Campo", AV24Campo);
                           }
                           else
                           {
                              AV15ExcelDestino.get_Cells(AV23l, AV22c, 1, 1).Text = AV13ExcelOrigem.get_Cells(AV23l, AV22c, 1, 1).Text;
                           }
                        }
                        else if ( AV22c == 4 )
                        {
                           if ( AV23l == 39 )
                           {
                              AV24Campo = "ALIA";
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Campo", AV24Campo);
                           }
                           else if ( AV23l == 40 )
                           {
                              AV24Campo = "AIEA";
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Campo", AV24Campo);
                           }
                           else if ( AV23l == 41 )
                           {
                              AV24Campo = "EEA";
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Campo", AV24Campo);
                           }
                           else if ( AV23l == 42 )
                           {
                              AV24Campo = "SEA";
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Campo", AV24Campo);
                           }
                           else if ( AV23l == 43 )
                           {
                              AV24Campo = "CEA";
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Campo", AV24Campo);
                           }
                           else
                           {
                              AV15ExcelDestino.get_Cells(AV23l, AV22c, 1, 1).Text = AV13ExcelOrigem.get_Cells(AV23l, AV22c, 1, 1).Text;
                           }
                        }
                        else
                        {
                           AV15ExcelDestino.get_Cells(AV23l, AV22c, 1, 1).Text = AV13ExcelOrigem.get_Cells(AV23l, AV22c, 1, 1).Text;
                        }
                        if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV24Campo)) )
                        {
                           /* Execute user subroutine: 'GETVALOR' */
                           S112 ();
                           if (returnInSub) return;
                        }
                        AV23l = (short)(AV23l+1);
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23l", StringUtil.LTrim( StringUtil.Str( (decimal)(AV23l), 4, 0)));
                     }
                     AV22c = (short)(AV22c+1);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22c", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22c), 4, 0)));
                  }
                  AV15ExcelDestino.Save();
                  AV15ExcelDestino.Close();
                  new prc_updanexo(context ).execute(  AV9Local_Codigo,  AV10Tabela,  AV17Planilha) ;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavLocal_codigo_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV9Local_Codigo), 6, 0)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, cmbavTabela_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV10Tabela), 6, 0)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Planilha", AV17Planilha);
               }
               /* End For Each Line */
            }
            if ( nGXsfl_11_fel_idx == 0 )
            {
               nGXsfl_11_idx = 1;
               sGXsfl_11_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_11_idx), 4, 0)), 4, "0");
               SubsflControlProps_112( ) ;
            }
            nGXsfl_11_fel_idx = 1;
            AV13ExcelOrigem.Close();
         }
      }

      protected void S112( )
      {
         /* 'GETVALOR' Routine */
         AV29Aba = "";
         AV26row = 0;
         AV27col = 0;
         AV47GXLvl201 = 0;
         /* Using cursor H00RD10 */
         pr_default.execute(6, new Object[] {AV25AreaTrabalho_Codigo, AV24Campo});
         while ( (pr_default.getStatus(6) != 101) )
         {
            A849ParametrosPln_Campo = H00RD10_A849ParametrosPln_Campo[0];
            A847ParametrosPln_AreaTrabalhoCod = H00RD10_A847ParametrosPln_AreaTrabalhoCod[0];
            n847ParametrosPln_AreaTrabalhoCod = H00RD10_n847ParametrosPln_AreaTrabalhoCod[0];
            A2015ParametrosPln_Aba = H00RD10_A2015ParametrosPln_Aba[0];
            A851ParametrosPln_Linha = H00RD10_A851ParametrosPln_Linha[0];
            A850ParametrosPln_Coluna = H00RD10_A850ParametrosPln_Coluna[0];
            AV47GXLvl201 = 1;
            AV29Aba = StringUtil.Trim( A2015ParametrosPln_Aba);
            AV26row = (short)(A851ParametrosPln_Linha);
            if ( String.IsNullOrEmpty(StringUtil.RTrim( StringUtil.Substring( A850ParametrosPln_Coluna, 2, 1))) )
            {
               AV27col = (short)(StringUtil.Asc( A850ParametrosPln_Coluna)-64);
            }
            else
            {
               AV27col = (short)(StringUtil.Asc( StringUtil.Substring( A850ParametrosPln_Coluna, 2, 1))-64+26);
            }
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            pr_default.readNext(6);
         }
         pr_default.close(6);
         if ( AV47GXLvl201 == 0 )
         {
            /* Using cursor H00RD11 */
            pr_default.execute(7, new Object[] {AV24Campo});
            while ( (pr_default.getStatus(7) != 101) )
            {
               A849ParametrosPln_Campo = H00RD11_A849ParametrosPln_Campo[0];
               A847ParametrosPln_AreaTrabalhoCod = H00RD11_A847ParametrosPln_AreaTrabalhoCod[0];
               n847ParametrosPln_AreaTrabalhoCod = H00RD11_n847ParametrosPln_AreaTrabalhoCod[0];
               A2015ParametrosPln_Aba = H00RD11_A2015ParametrosPln_Aba[0];
               A851ParametrosPln_Linha = H00RD11_A851ParametrosPln_Linha[0];
               A850ParametrosPln_Coluna = H00RD11_A850ParametrosPln_Coluna[0];
               AV29Aba = StringUtil.Trim( A2015ParametrosPln_Aba);
               AV26row = (short)(A851ParametrosPln_Linha);
               if ( String.IsNullOrEmpty(StringUtil.RTrim( StringUtil.Substring( A850ParametrosPln_Coluna, 2, 1))) )
               {
                  AV27col = (short)(StringUtil.Asc( A850ParametrosPln_Coluna)-64);
               }
               else
               {
                  AV27col = (short)(StringUtil.Asc( StringUtil.Substring( A850ParametrosPln_Coluna, 2, 1))-64+26);
               }
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               pr_default.readNext(7);
            }
            pr_default.close(7);
         }
         if ( ( AV23l == 8 ) || ( AV23l == 10 ) )
         {
            AV15ExcelDestino.get_Cells(AV23l, AV22c, 1, 1).Bold = AV13ExcelOrigem.get_Cells(AV23l, AV22c, 1, 1).Bold;
            AV15ExcelDestino.SelectSheet(AV29Aba);
            AV21Text = AV15ExcelDestino.get_Cells(AV26row, AV27col, 1, 1).Text;
            AV15ExcelDestino.SelectSheet("Summary");
            AV15ExcelDestino.get_Cells(AV23l, AV22c, 1, 1).Text = AV21Text;
         }
         else
         {
            AV15ExcelDestino.get_Cells(AV23l, AV22c, 1, 1).Bold = AV13ExcelOrigem.get_Cells(AV23l, AV22c, 1, 1).Bold;
            AV15ExcelDestino.SelectSheet(AV29Aba);
            AV28Valor = NumberUtil.Val( AV15ExcelDestino.get_Cells(AV26row, AV27col, 1, 1).Value, ".");
            if ( (Convert.ToDecimal(0)==AV28Valor) )
            {
               AV28Valor = (decimal)(AV15ExcelDestino.get_Cells(AV26row, AV27col, 1, 1).Number);
            }
            AV15ExcelDestino.SelectSheet("Summary");
            AV15ExcelDestino.get_Cells(AV23l, AV22c, 1, 1).Number = (double)(AV28Valor);
         }
      }

      protected void wb_table1_2_RD2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "CellWidth100", 0, "", "", 20, 20, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "Image";
            StyleString = "";
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 8,'',false,'" + sGXsfl_11_idx + "',0)\"";
            edtavBlob_Filetype = "tmp";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavBlob_Internalname, "Filetype", edtavBlob_Filetype);
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12Blob)) )
            {
               gxblobfileaux.Source = AV12Blob;
               if ( ! gxblobfileaux.HasExtension() || ( StringUtil.StrCmp(edtavBlob_Filetype, "tmp") != 0 ) )
               {
                  gxblobfileaux.SetExtension(StringUtil.Trim( edtavBlob_Filetype));
               }
               if ( gxblobfileaux.ErrCode == 0 )
               {
                  AV12Blob = gxblobfileaux.GetAbsoluteName();
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavBlob_Internalname, "URL", context.PathToRelativeUrl( AV12Blob));
                  edtavBlob_Filetype = gxblobfileaux.GetExtension();
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavBlob_Internalname, "Filetype", edtavBlob_Filetype);
               }
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavBlob_Internalname, "URL", context.PathToRelativeUrl( AV12Blob));
            }
            GxWebStd.gx_blob_field( context, edtavBlob_Internalname, StringUtil.RTrim( AV12Blob), context.PathToRelativeUrl( AV12Blob), (String.IsNullOrEmpty(StringUtil.RTrim( edtavBlob_Contenttype)) ? context.GetContentType( (String.IsNullOrEmpty(StringUtil.RTrim( edtavBlob_Filetype)) ? AV12Blob : edtavBlob_Filetype)) : edtavBlob_Contenttype), false, "", edtavBlob_Parameters, edtavBlob_Display, 1, 1, "", "", 0, -1, 250, "px", 60, "px", 0, 0, 0, edtavBlob_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", StyleString, ClassString, "", ""+TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,8);\"", "", "", "HLP_WP_SummaryAdd.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\">") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"11\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "Grid", 0, "", "", 1, 2, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "Data Cnt") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "Raz�o Social") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(110), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "Telefone") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "Email") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "Contratante_Endereco") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "Tabela") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "Local_Codigo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "�rea de Trabalho") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "OS Ref|OS") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "Nome") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "Planilha") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "Grid");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorodd", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorodd), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Titleforecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Titleforecolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.Format(AV32DataCnt, "99/99/99"));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDatacnt_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( AV34Contratante_RazaoSocial));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavContratante_razaosocial_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( AV35Contratante_Telefone));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavContratante_telefone_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", AV36Contratante_Email);
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavContratante_email_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( AV37Contratante_Endereco));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavContratante_endereco_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV10Tabela), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbavTabela.Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV9Local_Codigo), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavLocal_codigo_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.BoolToStr( AV8Selected));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV25AreaTrabalho_Codigo), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(dynavAreatrabalho_codigo.Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", AV20OsFsOsFm);
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavOsfsosfm_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavOsfsosfm_Link));
               GridColumn.AddObjectProperty("Linktarget", StringUtil.RTrim( edtavOsfsosfm_Linktarget));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", AV30Sistema_Nome);
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavSistema_nome_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( AV11File_Nome));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavFile_nome_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 11 )
         {
            wbEnd = 0;
            nRC_GXsfl_11 = (short)(nGXsfl_11_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 26,'',false,'',0)\"";
            ClassString = "Button";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnenter_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(11), 2, 0)+","+"null"+");", "Adicionar resumo", bttBtnenter_Jsonclick, 5, "Adicionar resumo", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_SummaryAdd.htm");
            context.WriteHtmlText( "&nbsp;&nbsp;&nbsp;&nbsp; ") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 27,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnfechar_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(11), 2, 0)+","+"null"+");", "Fechar", bttBtnfechar_Jsonclick, 1, "Fechar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_SummaryAdd.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_RD2e( true) ;
         }
         else
         {
            wb_table1_2_RD2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PARD2( ) ;
         WSRD2( ) ;
         WERD2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020621630327");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxdec.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wp_summaryadd.js", "?2020621630327");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_112( )
      {
         edtavDatacnt_Internalname = "vDATACNT_"+sGXsfl_11_idx;
         edtavContratante_razaosocial_Internalname = "vCONTRATANTE_RAZAOSOCIAL_"+sGXsfl_11_idx;
         edtavContratante_telefone_Internalname = "vCONTRATANTE_TELEFONE_"+sGXsfl_11_idx;
         edtavContratante_email_Internalname = "vCONTRATANTE_EMAIL_"+sGXsfl_11_idx;
         edtavContratante_endereco_Internalname = "vCONTRATANTE_ENDERECO_"+sGXsfl_11_idx;
         cmbavTabela_Internalname = "vTABELA_"+sGXsfl_11_idx;
         edtavLocal_codigo_Internalname = "vLOCAL_CODIGO_"+sGXsfl_11_idx;
         chkavSelected_Internalname = "vSELECTED_"+sGXsfl_11_idx;
         dynavAreatrabalho_codigo_Internalname = "vAREATRABALHO_CODIGO_"+sGXsfl_11_idx;
         edtavOsfsosfm_Internalname = "vOSFSOSFM_"+sGXsfl_11_idx;
         edtavSistema_nome_Internalname = "vSISTEMA_NOME_"+sGXsfl_11_idx;
         edtavFile_nome_Internalname = "vFILE_NOME_"+sGXsfl_11_idx;
      }

      protected void SubsflControlProps_fel_112( )
      {
         edtavDatacnt_Internalname = "vDATACNT_"+sGXsfl_11_fel_idx;
         edtavContratante_razaosocial_Internalname = "vCONTRATANTE_RAZAOSOCIAL_"+sGXsfl_11_fel_idx;
         edtavContratante_telefone_Internalname = "vCONTRATANTE_TELEFONE_"+sGXsfl_11_fel_idx;
         edtavContratante_email_Internalname = "vCONTRATANTE_EMAIL_"+sGXsfl_11_fel_idx;
         edtavContratante_endereco_Internalname = "vCONTRATANTE_ENDERECO_"+sGXsfl_11_fel_idx;
         cmbavTabela_Internalname = "vTABELA_"+sGXsfl_11_fel_idx;
         edtavLocal_codigo_Internalname = "vLOCAL_CODIGO_"+sGXsfl_11_fel_idx;
         chkavSelected_Internalname = "vSELECTED_"+sGXsfl_11_fel_idx;
         dynavAreatrabalho_codigo_Internalname = "vAREATRABALHO_CODIGO_"+sGXsfl_11_fel_idx;
         edtavOsfsosfm_Internalname = "vOSFSOSFM_"+sGXsfl_11_fel_idx;
         edtavSistema_nome_Internalname = "vSISTEMA_NOME_"+sGXsfl_11_fel_idx;
         edtavFile_nome_Internalname = "vFILE_NOME_"+sGXsfl_11_fel_idx;
      }

      protected void sendrow_112( )
      {
         SubsflControlProps_112( ) ;
         WBRD0( ) ;
         GridRow = GXWebRow.GetNew(context,GridContainer);
         if ( subGrid_Backcolorstyle == 0 )
         {
            /* None style subfile background logic. */
            subGrid_Backstyle = 0;
            if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
            {
               subGrid_Linesclass = subGrid_Class+"Odd";
            }
         }
         else if ( subGrid_Backcolorstyle == 1 )
         {
            /* Uniform style subfile background logic. */
            subGrid_Backstyle = 0;
            subGrid_Backcolor = subGrid_Allbackcolor;
            if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
            {
               subGrid_Linesclass = subGrid_Class+"Uniform";
            }
         }
         else if ( subGrid_Backcolorstyle == 2 )
         {
            /* Header style subfile background logic. */
            subGrid_Backstyle = 1;
            if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
            {
               subGrid_Linesclass = subGrid_Class+"Odd";
            }
            subGrid_Backcolor = (int)(0xFFFFFF);
         }
         else if ( subGrid_Backcolorstyle == 3 )
         {
            /* Report style subfile background logic. */
            subGrid_Backstyle = 1;
            if ( ((int)((nGXsfl_11_idx) % (2))) == 0 )
            {
               subGrid_Backcolor = (int)(0xE5E5E5);
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Even";
               }
            }
            else
            {
               subGrid_Backcolor = (int)(0xFFFFFF);
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
         }
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<tr ") ;
            context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+((subGrid_Backcolor==-1) ? "" : "background-color:"+context.BuildHTMLColor( subGrid_Backcolor)+";")+"\"") ;
            context.WriteHtmlText( " gxrow=\""+sGXsfl_11_idx+"\">") ;
         }
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
         }
         /* Single line edit */
         TempTags = " " + ((edtavDatacnt_Enabled!=0)&&(edtavDatacnt_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 12,'',false,'"+sGXsfl_11_idx+"',11)\"" : " ");
         ROClassString = "Attribute";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavDatacnt_Internalname,context.localUtil.Format(AV32DataCnt, "99/99/99"),context.localUtil.Format( AV32DataCnt, "99/99/99"),TempTags+((edtavDatacnt_Enabled!=0)&&(edtavDatacnt_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavDatacnt_Enabled!=0)&&(edtavDatacnt_Visible!=0) ? " onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,12);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavDatacnt_Jsonclick,(short)0,(String)"Attribute",((subGrid_Backcolor==-1) ? "" : "background-color:"+context.BuildHTMLColor( subGrid_Backcolor)+";"),(String)ROClassString,(String)"",(short)0,(int)edtavDatacnt_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)8,(short)0,(short)0,(short)11,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+"display:none;"+"\">") ;
         }
         /* Single line edit */
         TempTags = " " + ((edtavContratante_razaosocial_Enabled!=0)&&(edtavContratante_razaosocial_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 13,'',false,'"+sGXsfl_11_idx+"',11)\"" : " ");
         ROClassString = "BootstrapAttribute100";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavContratante_razaosocial_Internalname,StringUtil.RTrim( AV34Contratante_RazaoSocial),StringUtil.RTrim( context.localUtil.Format( AV34Contratante_RazaoSocial, "@!")),TempTags+((edtavContratante_razaosocial_Enabled!=0)&&(edtavContratante_razaosocial_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavContratante_razaosocial_Enabled!=0)&&(edtavContratante_razaosocial_Visible!=0) ? " onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,13);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavContratante_razaosocial_Jsonclick,(short)0,(String)"BootstrapAttribute100",((subGrid_Backcolor==-1) ? "" : "background-color:"+context.BuildHTMLColor( subGrid_Backcolor)+";"),(String)ROClassString,(String)"",(short)0,(int)edtavContratante_razaosocial_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)100,(short)0,(short)0,(short)11,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+"display:none;"+"\">") ;
         }
         /* Single line edit */
         TempTags = " " + ((edtavContratante_telefone_Enabled!=0)&&(edtavContratante_telefone_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 14,'',false,'"+sGXsfl_11_idx+"',11)\"" : " ");
         ROClassString = "Attribute";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavContratante_telefone_Internalname,StringUtil.RTrim( AV35Contratante_Telefone),(String)"",TempTags+((edtavContratante_telefone_Enabled!=0)&&(edtavContratante_telefone_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavContratante_telefone_Enabled!=0)&&(edtavContratante_telefone_Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,14);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavContratante_telefone_Jsonclick,(short)0,(String)"Attribute",((subGrid_Backcolor==-1) ? "" : "background-color:"+context.BuildHTMLColor( subGrid_Backcolor)+";"),(String)ROClassString,(String)"",(short)0,(int)edtavContratante_telefone_Enabled,(short)0,(String)"text",(String)"",(short)110,(String)"px",(short)17,(String)"px",(short)20,(short)0,(short)0,(short)11,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"left",(bool)true});
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+"display:none;"+"\">") ;
         }
         /* Single line edit */
         TempTags = " " + ((edtavContratante_email_Enabled!=0)&&(edtavContratante_email_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 15,'',false,'"+sGXsfl_11_idx+"',11)\"" : " ");
         ROClassString = "Attribute";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavContratante_email_Internalname,(String)AV36Contratante_Email,(String)"",TempTags+((edtavContratante_email_Enabled!=0)&&(edtavContratante_email_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavContratante_email_Enabled!=0)&&(edtavContratante_email_Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,15);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavContratante_email_Jsonclick,(short)0,(String)"Attribute",((subGrid_Backcolor==-1) ? "" : "background-color:"+context.BuildHTMLColor( subGrid_Backcolor)+";"),(String)ROClassString,(String)"",(short)0,(int)edtavContratante_email_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)100,(short)0,(short)0,(short)11,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"left",(bool)true});
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+"display:none;"+"\">") ;
         }
         /* Single line edit */
         TempTags = " " + ((edtavContratante_endereco_Enabled!=0)&&(edtavContratante_endereco_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 16,'',false,'"+sGXsfl_11_idx+"',11)\"" : " ");
         ROClassString = "Attribute";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavContratante_endereco_Internalname,StringUtil.RTrim( AV37Contratante_Endereco),(String)"",TempTags+((edtavContratante_endereco_Enabled!=0)&&(edtavContratante_endereco_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavContratante_endereco_Enabled!=0)&&(edtavContratante_endereco_Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,16);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavContratante_endereco_Jsonclick,(short)0,(String)"Attribute",((subGrid_Backcolor==-1) ? "" : "background-color:"+context.BuildHTMLColor( subGrid_Backcolor)+";"),(String)ROClassString,(String)"",(short)0,(int)edtavContratante_endereco_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)500,(short)0,(short)0,(short)11,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
         }
         TempTags = " " + ((cmbavTabela.Enabled!=0)&&(cmbavTabela.Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 17,'',false,'"+sGXsfl_11_idx+"',11)\"" : " ");
         if ( ( nGXsfl_11_idx == 1 ) && isAjaxCallMode( ) )
         {
            GXCCtl = "vTABELA_" + sGXsfl_11_idx;
            cmbavTabela.Name = GXCCtl;
            cmbavTabela.WebTags = "";
            cmbavTabela.addItem("1", "Contagem Resultado", 0);
            cmbavTabela.addItem("2", "Evid�ncias", 0);
            cmbavTabela.addItem("3", "Anexos", 0);
            if ( cmbavTabela.ItemCount > 0 )
            {
               AV10Tabela = (int)(NumberUtil.Val( cmbavTabela.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV10Tabela), 6, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, cmbavTabela_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV10Tabela), 6, 0)));
            }
         }
         /* ComboBox */
         GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbavTabela,(String)cmbavTabela_Internalname,StringUtil.Trim( StringUtil.Str( (decimal)(AV10Tabela), 6, 0)),(short)1,(String)cmbavTabela_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"int",(String)"",(short)0,cmbavTabela.Enabled,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",((subGrid_Backcolor==-1) ? "" : "background-color:"+context.BuildHTMLColor( subGrid_Backcolor)+";"),(String)"Attribute",(String)"",TempTags+((cmbavTabela.Enabled!=0)&&(cmbavTabela.Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((cmbavTabela.Enabled!=0)&&(cmbavTabela.Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,17);\"" : " "),(String)"",(bool)true});
         cmbavTabela.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV10Tabela), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavTabela_Internalname, "Values", (String)(cmbavTabela.ToJavascriptSource()));
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
         }
         /* Single line edit */
         TempTags = " " + ((edtavLocal_codigo_Enabled!=0)&&(edtavLocal_codigo_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 18,'',false,'"+sGXsfl_11_idx+"',11)\"" : " ");
         ROClassString = "Attribute";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavLocal_codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(AV9Local_Codigo), 6, 0, ",", "")),((edtavLocal_codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV9Local_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(AV9Local_Codigo), "ZZZZZ9")),TempTags+((edtavLocal_codigo_Enabled!=0)&&(edtavLocal_codigo_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavLocal_codigo_Enabled!=0)&&(edtavLocal_codigo_Visible!=0) ? " onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,18);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavLocal_codigo_Jsonclick,(short)0,(String)"Attribute",((subGrid_Backcolor==-1) ? "" : "background-color:"+context.BuildHTMLColor( subGrid_Backcolor)+";"),(String)ROClassString,(String)"",(short)0,(int)edtavLocal_codigo_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)11,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
         }
         /* Check box */
         TempTags = " " + ((chkavSelected.Enabled!=0)&&(chkavSelected.Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 19,'',false,'"+sGXsfl_11_idx+"',11)\"" : " ");
         ClassString = "Attribute";
         StyleString = ((subGrid_Backcolor==-1) ? "" : "background-color:"+context.BuildHTMLColor( subGrid_Backcolor)+";");
         GridRow.AddColumnProperties("checkbox", 1, isAjaxCallMode( ), new Object[] {(String)chkavSelected_Internalname,StringUtil.BoolToStr( AV8Selected),(String)"",(String)"",(short)-1,(short)1,(String)"true",(String)"",(String)StyleString,(String)ClassString,(String)"",TempTags+((chkavSelected.Enabled!=0)&&(chkavSelected.Visible!=0) ? " onclick=\"gx.fn.checkboxClick(19, this, 'true', 'false');gx.evt.onchange(this);\" " : " ")+((chkavSelected.Enabled!=0)&&(chkavSelected.Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,19);\"" : " ")});
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
         }
         TempTags = " " + ((dynavAreatrabalho_codigo.Enabled!=0)&&(dynavAreatrabalho_codigo.Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 20,'',false,'"+sGXsfl_11_idx+"',11)\"" : " ");
         if ( ( nGXsfl_11_idx == 1 ) && isAjaxCallMode( ) )
         {
            GXCCtl = "vAREATRABALHO_CODIGO_" + sGXsfl_11_idx;
            dynavAreatrabalho_codigo.Name = GXCCtl;
            dynavAreatrabalho_codigo.WebTags = "";
            dynavAreatrabalho_codigo.removeAllItems();
            /* Using cursor H00RD12 */
            pr_default.execute(8);
            while ( (pr_default.getStatus(8) != 101) )
            {
               dynavAreatrabalho_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(H00RD12_A5AreaTrabalho_Codigo[0]), 6, 0)), H00RD12_A6AreaTrabalho_Descricao[0], 0);
               pr_default.readNext(8);
            }
            pr_default.close(8);
            if ( dynavAreatrabalho_codigo.ItemCount > 0 )
            {
               AV25AreaTrabalho_Codigo = (int)(NumberUtil.Val( dynavAreatrabalho_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV25AreaTrabalho_Codigo), 6, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, dynavAreatrabalho_codigo_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV25AreaTrabalho_Codigo), 6, 0)));
            }
         }
         /* ComboBox */
         GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)dynavAreatrabalho_codigo,(String)dynavAreatrabalho_codigo_Internalname,StringUtil.Trim( StringUtil.Str( (decimal)(AV25AreaTrabalho_Codigo), 6, 0)),(short)1,(String)dynavAreatrabalho_codigo_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"int",(String)"",(short)-1,dynavAreatrabalho_codigo.Enabled,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",((subGrid_Backcolor==-1) ? "" : "background-color:"+context.BuildHTMLColor( subGrid_Backcolor)+";"),(String)"Attribute",(String)"",TempTags+((dynavAreatrabalho_codigo.Enabled!=0)&&(dynavAreatrabalho_codigo.Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((dynavAreatrabalho_codigo.Enabled!=0)&&(dynavAreatrabalho_codigo.Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,20);\"" : " "),(String)"",(bool)true});
         dynavAreatrabalho_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV25AreaTrabalho_Codigo), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavAreatrabalho_codigo_Internalname, "Values", (String)(dynavAreatrabalho_codigo.ToJavascriptSource()));
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         TempTags = " " + ((edtavOsfsosfm_Enabled!=0)&&(edtavOsfsosfm_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 21,'',false,'"+sGXsfl_11_idx+"',11)\"" : " ");
         ROClassString = "Attribute";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavOsfsosfm_Internalname,(String)AV20OsFsOsFm,(String)"",TempTags+((edtavOsfsosfm_Enabled!=0)&&(edtavOsfsosfm_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavOsfsosfm_Enabled!=0)&&(edtavOsfsosfm_Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,21);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)edtavOsfsosfm_Link,(String)edtavOsfsosfm_Linktarget,(String)"",(String)"",(String)edtavOsfsosfm_Jsonclick,(short)0,(String)"Attribute",((subGrid_Backcolor==-1) ? "" : "background-color:"+context.BuildHTMLColor( subGrid_Backcolor)+";"),(String)ROClassString,(String)"",(short)-1,(int)edtavOsfsosfm_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)30,(short)0,(short)0,(short)11,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         TempTags = " " + ((edtavSistema_nome_Enabled!=0)&&(edtavSistema_nome_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 22,'',false,'"+sGXsfl_11_idx+"',11)\"" : " ");
         ROClassString = "Attribute";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavSistema_nome_Internalname,(String)AV30Sistema_Nome,StringUtil.RTrim( context.localUtil.Format( AV30Sistema_Nome, "@!")),TempTags+((edtavSistema_nome_Enabled!=0)&&(edtavSistema_nome_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavSistema_nome_Enabled!=0)&&(edtavSistema_nome_Visible!=0) ? " onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,22);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavSistema_nome_Jsonclick,(short)0,(String)"Attribute",((subGrid_Backcolor==-1) ? "" : "background-color:"+context.BuildHTMLColor( subGrid_Backcolor)+";"),(String)ROClassString,(String)"",(short)-1,(int)edtavSistema_nome_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)100,(short)0,(short)0,(short)11,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         TempTags = " " + ((edtavFile_nome_Enabled!=0)&&(edtavFile_nome_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 23,'',false,'"+sGXsfl_11_idx+"',11)\"" : " ");
         ROClassString = "Attribute";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavFile_nome_Internalname,StringUtil.RTrim( AV11File_Nome),(String)"",TempTags+((edtavFile_nome_Enabled!=0)&&(edtavFile_nome_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavFile_nome_Enabled!=0)&&(edtavFile_nome_Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,23);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavFile_nome_Jsonclick,(short)0,(String)"Attribute",((subGrid_Backcolor==-1) ? "" : "background-color:"+context.BuildHTMLColor( subGrid_Backcolor)+";"),(String)ROClassString,(String)"",(short)-1,(int)edtavFile_nome_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)11,(short)1,(short)-1,(short)-1,(bool)true,(String)"NomeArq",(String)"left",(bool)true});
         GXCCtl = "vBLOB_" + sGXsfl_11_idx;
         GXCCtlgxBlob = GXCCtl + "_gxBlob";
         GxWebStd.gx_hidden_field( context, GXCCtlgxBlob, AV12Blob);
         GridContainer.AddRow(GridRow);
         nGXsfl_11_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_11_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_11_idx+1));
         sGXsfl_11_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_11_idx), 4, 0)), 4, "0");
         SubsflControlProps_112( ) ;
         /* End function sendrow_112 */
      }

      protected void init_default_properties( )
      {
         edtavBlob_Internalname = "vBLOB";
         edtavDatacnt_Internalname = "vDATACNT";
         edtavContratante_razaosocial_Internalname = "vCONTRATANTE_RAZAOSOCIAL";
         edtavContratante_telefone_Internalname = "vCONTRATANTE_TELEFONE";
         edtavContratante_email_Internalname = "vCONTRATANTE_EMAIL";
         edtavContratante_endereco_Internalname = "vCONTRATANTE_ENDERECO";
         cmbavTabela_Internalname = "vTABELA";
         edtavLocal_codigo_Internalname = "vLOCAL_CODIGO";
         chkavSelected_Internalname = "vSELECTED";
         dynavAreatrabalho_codigo_Internalname = "vAREATRABALHO_CODIGO";
         edtavOsfsosfm_Internalname = "vOSFSOSFM";
         edtavSistema_nome_Internalname = "vSISTEMA_NOME";
         edtavFile_nome_Internalname = "vFILE_NOME";
         bttBtnenter_Internalname = "BTNENTER";
         bttBtnfechar_Internalname = "BTNFECHAR";
         tblTable1_Internalname = "TABLE1";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtavFile_nome_Jsonclick = "";
         edtavFile_nome_Visible = -1;
         edtavSistema_nome_Jsonclick = "";
         edtavSistema_nome_Visible = -1;
         edtavOsfsosfm_Jsonclick = "";
         edtavOsfsosfm_Visible = -1;
         dynavAreatrabalho_codigo_Jsonclick = "";
         dynavAreatrabalho_codigo.Visible = -1;
         chkavSelected.Visible = -1;
         chkavSelected.Enabled = 1;
         edtavLocal_codigo_Jsonclick = "";
         edtavLocal_codigo_Visible = 0;
         cmbavTabela_Jsonclick = "";
         cmbavTabela.Visible = 0;
         edtavContratante_endereco_Jsonclick = "";
         edtavContratante_endereco_Visible = 0;
         edtavContratante_email_Jsonclick = "";
         edtavContratante_email_Visible = 0;
         edtavContratante_telefone_Jsonclick = "";
         edtavContratante_telefone_Visible = 0;
         edtavContratante_razaosocial_Jsonclick = "";
         edtavContratante_razaosocial_Visible = 0;
         edtavDatacnt_Jsonclick = "";
         edtavDatacnt_Visible = 0;
         subGrid_Backcolor = (int)(0x0);
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavFile_nome_Enabled = 1;
         edtavSistema_nome_Enabled = 1;
         edtavOsfsosfm_Link = "";
         edtavOsfsosfm_Enabled = 1;
         dynavAreatrabalho_codigo.Enabled = 1;
         edtavLocal_codigo_Enabled = 1;
         cmbavTabela.Enabled = 1;
         edtavContratante_endereco_Enabled = 1;
         edtavContratante_email_Enabled = 1;
         edtavContratante_telefone_Enabled = 1;
         edtavContratante_razaosocial_Enabled = 1;
         edtavDatacnt_Enabled = 1;
         subGrid_Class = "Grid";
         edtavBlob_Jsonclick = "";
         edtavBlob_Parameters = "";
         edtavBlob_Contenttype = "";
         edtavBlob_Filetype = "";
         edtavBlob_Display = 0;
         edtavOsfsosfm_Linktarget = "";
         subGrid_Titleforecolor = (int)(0x000000);
         subGrid_Backcolorodd = (int)(0xFFFFFF);
         subGrid_Backcolorstyle = 3;
         chkavSelected.Caption = "";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Summary Add";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV7Codigos',fld:'vCODIGOS',pic:'',nv:null},{av:'A590ContagemResultadoEvidencia_TipoArq',fld:'CONTAGEMRESULTADOEVIDENCIA_TIPOARQ',pic:'',nv:''},{av:'A586ContagemResultadoEvidencia_Codigo',fld:'CONTAGEMRESULTADOEVIDENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A589ContagemResultadoEvidencia_NomeArq',fld:'CONTAGEMRESULTADOEVIDENCIA_NOMEARQ',pic:'',nv:''},{av:'A501ContagemResultado_OsFsOsFm',fld:'CONTAGEMRESULTADO_OSFSOSFM',pic:'',nv:''},{av:'A495ContagemResultado_SistemaNom',fld:'CONTAGEMRESULTADO_SISTEMANOM',pic:'@!',nv:''},{av:'A52Contratada_AreaTrabalhoCod',fld:'CONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A566ContagemResultado_DataUltCnt',fld:'CONTAGEMRESULTADO_DATAULTCNT',pic:'',nv:''},{av:'A9Contratante_RazaoSocial',fld:'CONTRATANTE_RAZAOSOCIAL',pic:'@!',nv:''},{av:'A31Contratante_Telefone',fld:'CONTRATANTE_TELEFONE',pic:'',nv:''},{av:'A14Contratante_Email',fld:'CONTRATANTE_EMAIL',pic:'',nv:''},{av:'A519Pessoa_Endereco',fld:'PESSOA_ENDERECO',pic:'',nv:''},{av:'A26Municipio_Nome',fld:'MUNICIPIO_NOME',pic:'@!',nv:''},{av:'A23Estado_UF',fld:'ESTADO_UF',pic:'@!',nv:''},{av:'A521Pessoa_CEP',fld:'PESSOA_CEP',pic:'',nv:''},{av:'A1109AnexoDe_Id',fld:'ANEXODE_ID',pic:'ZZZZZ9',nv:0},{av:'A1110AnexoDe_Tabela',fld:'ANEXODE_TABELA',pic:'ZZZZZ9',nv:0},{av:'A1108Anexo_TipoArq',fld:'ANEXO_TIPOARQ',pic:'',nv:''},{av:'A1106Anexo_Codigo',fld:'ANEXO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1107Anexo_NomeArq',fld:'ANEXO_NOMEARQ',pic:'',nv:''},{av:'A517ContagemResultado_Ultima',fld:'CONTAGEMRESULTADO_ULTIMA',pic:'',nv:false},{av:'A473ContagemResultado_DataCnt',fld:'CONTAGEMRESULTADO_DATACNT',pic:'',nv:''},{av:'A474ContagemResultado_ContadorFMNom',fld:'CONTAGEMRESULTADO_CONTADORFMNOM',pic:'@!',nv:''}],oparms:[{av:'AV8Selected',fld:'vSELECTED',pic:'',nv:false},{av:'edtavOsfsosfm_Linktarget',ctrl:'vOSFSOSFM',prop:'Linktarget'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E14RD2',iparms:[{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV7Codigos',fld:'vCODIGOS',pic:'',nv:null},{av:'A590ContagemResultadoEvidencia_TipoArq',fld:'CONTAGEMRESULTADOEVIDENCIA_TIPOARQ',pic:'',nv:''},{av:'A586ContagemResultadoEvidencia_Codigo',fld:'CONTAGEMRESULTADOEVIDENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A589ContagemResultadoEvidencia_NomeArq',fld:'CONTAGEMRESULTADOEVIDENCIA_NOMEARQ',pic:'',nv:''},{av:'A501ContagemResultado_OsFsOsFm',fld:'CONTAGEMRESULTADO_OSFSOSFM',pic:'',nv:''},{av:'A495ContagemResultado_SistemaNom',fld:'CONTAGEMRESULTADO_SISTEMANOM',pic:'@!',nv:''},{av:'A52Contratada_AreaTrabalhoCod',fld:'CONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A566ContagemResultado_DataUltCnt',fld:'CONTAGEMRESULTADO_DATAULTCNT',pic:'',nv:''},{av:'A9Contratante_RazaoSocial',fld:'CONTRATANTE_RAZAOSOCIAL',pic:'@!',nv:''},{av:'A31Contratante_Telefone',fld:'CONTRATANTE_TELEFONE',pic:'',nv:''},{av:'A14Contratante_Email',fld:'CONTRATANTE_EMAIL',pic:'',nv:''},{av:'A519Pessoa_Endereco',fld:'PESSOA_ENDERECO',pic:'',nv:''},{av:'A26Municipio_Nome',fld:'MUNICIPIO_NOME',pic:'@!',nv:''},{av:'A23Estado_UF',fld:'ESTADO_UF',pic:'@!',nv:''},{av:'A521Pessoa_CEP',fld:'PESSOA_CEP',pic:'',nv:''},{av:'A1109AnexoDe_Id',fld:'ANEXODE_ID',pic:'ZZZZZ9',nv:0},{av:'A1110AnexoDe_Tabela',fld:'ANEXODE_TABELA',pic:'ZZZZZ9',nv:0},{av:'A1108Anexo_TipoArq',fld:'ANEXO_TIPOARQ',pic:'',nv:''},{av:'A1106Anexo_Codigo',fld:'ANEXO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1107Anexo_NomeArq',fld:'ANEXO_NOMEARQ',pic:'',nv:''},{av:'A517ContagemResultado_Ultima',fld:'CONTAGEMRESULTADO_ULTIMA',pic:'',nv:false},{av:'A473ContagemResultado_DataCnt',fld:'CONTAGEMRESULTADO_DATACNT',pic:'',nv:''},{av:'A474ContagemResultado_ContadorFMNom',fld:'CONTAGEMRESULTADO_CONTADORFMNOM',pic:'@!',nv:''}],oparms:[{av:'AV10Tabela',fld:'vTABELA',pic:'ZZZZZ9',nv:0},{av:'AV9Local_Codigo',fld:'vLOCAL_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV11File_Nome',fld:'vFILE_NOME',pic:'',nv:''},{av:'AV20OsFsOsFm',fld:'vOSFSOSFM',pic:'',nv:''},{av:'edtavOsfsosfm_Link',ctrl:'vOSFSOSFM',prop:'Link'},{av:'AV30Sistema_Nome',fld:'vSISTEMA_NOME',pic:'@!',nv:''},{av:'AV25AreaTrabalho_Codigo',fld:'vAREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32DataCnt',fld:'vDATACNT',pic:'',nv:''},{av:'AV33Colaborador_Nome',fld:'vCOLABORADOR_NOME',pic:'@!',nv:''},{av:'AV34Contratante_RazaoSocial',fld:'vCONTRATANTE_RAZAOSOCIAL',pic:'@!',nv:''},{av:'AV35Contratante_Telefone',fld:'vCONTRATANTE_TELEFONE',pic:'',nv:''},{av:'AV36Contratante_Email',fld:'vCONTRATANTE_EMAIL',pic:'',nv:''},{av:'AV37Contratante_Endereco',fld:'vCONTRATANTE_ENDERECO',pic:'',nv:''}]}");
         setEventMetadata("ENTER","{handler:'E11RD2',iparms:[{av:'AV12Blob',fld:'vBLOB',pic:'',nv:''},{av:'AV8Selected',fld:'vSELECTED',grid:11,pic:'',nv:false},{av:'AV10Tabela',fld:'vTABELA',grid:11,pic:'ZZZZZ9',nv:0},{av:'A586ContagemResultadoEvidencia_Codigo',fld:'CONTAGEMRESULTADOEVIDENCIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV9Local_Codigo',fld:'vLOCAL_CODIGO',grid:11,pic:'ZZZZZ9',nv:0},{av:'A588ContagemResultadoEvidencia_Arquivo',fld:'CONTAGEMRESULTADOEVIDENCIA_ARQUIVO',pic:'',nv:''},{av:'A1106Anexo_Codigo',fld:'ANEXO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1101Anexo_Arquivo',fld:'ANEXO_ARQUIVO',pic:'',nv:''},{av:'AV17Planilha',fld:'vPLANILHA',pic:'',nv:''},{av:'AV32DataCnt',fld:'vDATACNT',grid:11,pic:'',nv:''},{av:'AV30Sistema_Nome',fld:'vSISTEMA_NOME',grid:11,pic:'@!',nv:''},{av:'AV33Colaborador_Nome',fld:'vCOLABORADOR_NOME',pic:'@!',nv:''},{av:'AV34Contratante_RazaoSocial',fld:'vCONTRATANTE_RAZAOSOCIAL',grid:11,pic:'@!',nv:''},{av:'AV35Contratante_Telefone',fld:'vCONTRATANTE_TELEFONE',grid:11,pic:'',nv:''},{av:'AV36Contratante_Email',fld:'vCONTRATANTE_EMAIL',grid:11,pic:'',nv:''},{av:'AV37Contratante_Endereco',fld:'vCONTRATANTE_ENDERECO',grid:11,pic:'',nv:''},{av:'AV22c',fld:'vC',pic:'ZZZ9',nv:0},{av:'AV23l',fld:'vL',pic:'ZZZ9',nv:0},{av:'AV24Campo',fld:'vCAMPO',pic:'@!',nv:''},{av:'A847ParametrosPln_AreaTrabalhoCod',fld:'PARAMETROSPLN_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV25AreaTrabalho_Codigo',fld:'vAREATRABALHO_CODIGO',grid:11,pic:'ZZZZZ9',nv:0},{av:'A849ParametrosPln_Campo',fld:'PARAMETROSPLN_CAMPO',pic:'@!',nv:''},{av:'A2015ParametrosPln_Aba',fld:'PARAMETROSPLN_ABA',pic:'',nv:''},{av:'A851ParametrosPln_Linha',fld:'PARAMETROSPLN_LINHA',pic:'ZZZZZ9',nv:0},{av:'A850ParametrosPln_Coluna',fld:'PARAMETROSPLN_COLUNA',pic:'@!',nv:''}],oparms:[{av:'AV17Planilha',fld:'vPLANILHA',pic:'',nv:''},{av:'AV24Campo',fld:'vCAMPO',pic:'@!',nv:''},{av:'AV22c',fld:'vC',pic:'ZZZ9',nv:0},{av:'AV23l',fld:'vL',pic:'ZZZ9',nv:0}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV7Codigos = new GxSimpleCollection();
         A590ContagemResultadoEvidencia_TipoArq = "";
         A589ContagemResultadoEvidencia_NomeArq = "";
         A501ContagemResultado_OsFsOsFm = "";
         A495ContagemResultado_SistemaNom = "";
         A566ContagemResultado_DataUltCnt = DateTime.MinValue;
         A9Contratante_RazaoSocial = "";
         A31Contratante_Telefone = "";
         A14Contratante_Email = "";
         A519Pessoa_Endereco = "";
         A26Municipio_Nome = "";
         A23Estado_UF = "";
         A521Pessoa_CEP = "";
         A1108Anexo_TipoArq = "";
         A1107Anexo_NomeArq = "";
         A473ContagemResultado_DataCnt = DateTime.MinValue;
         A474ContagemResultado_ContadorFMNom = "";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         A588ContagemResultadoEvidencia_Arquivo = "";
         A1101Anexo_Arquivo = "";
         AV17Planilha = "";
         AV33Colaborador_Nome = "";
         AV24Campo = "";
         A849ParametrosPln_Campo = "";
         A2015ParametrosPln_Aba = "";
         A850ParametrosPln_Coluna = "";
         A457ContagemResultado_Demanda = "";
         A493ContagemResultado_DemandaFM = "";
         GXCCtlgxBlob = "";
         AV12Blob = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV32DataCnt = DateTime.MinValue;
         AV34Contratante_RazaoSocial = "";
         AV35Contratante_Telefone = "";
         AV36Contratante_Email = "";
         AV37Contratante_Endereco = "";
         AV20OsFsOsFm = "";
         AV30Sistema_Nome = "";
         AV11File_Nome = "";
         GXCCtl = "";
         scmdbuf = "";
         H00RD2_A5AreaTrabalho_Codigo = new int[1] ;
         H00RD2_A6AreaTrabalho_Descricao = new String[] {""} ;
         GridContainer = new GXWebGrid( context);
         AV5SDT_Codigos = new GxObjectCollection( context, "SDT_Codigos", "GxEv3Up14_MeetrikaVs3", "SdtSDT_Codigos", "GeneXus.Programs");
         AV19WebSession = context.GetSession();
         AV6Codigo = new SdtSDT_Codigos(context);
         bttBtnfechar_Jsonclick = "";
         H00RD5_A146Modulo_Codigo = new int[1] ;
         H00RD5_n146Modulo_Codigo = new bool[] {false} ;
         H00RD5_A127Sistema_Codigo = new int[1] ;
         H00RD5_A1399Sistema_ImpUserCod = new int[1] ;
         H00RD5_n1399Sistema_ImpUserCod = new bool[] {false} ;
         H00RD5_A1402Sistema_ImpUserPesCod = new int[1] ;
         H00RD5_n1402Sistema_ImpUserPesCod = new bool[] {false} ;
         H00RD5_A503Pessoa_MunicipioCod = new int[1] ;
         H00RD5_n503Pessoa_MunicipioCod = new bool[] {false} ;
         H00RD5_A135Sistema_AreaTrabalhoCod = new int[1] ;
         H00RD5_A1073Usuario_CargoCod = new int[1] ;
         H00RD5_n1073Usuario_CargoCod = new bool[] {false} ;
         H00RD5_A1075Usuario_CargoUOCod = new int[1] ;
         H00RD5_n1075Usuario_CargoUOCod = new bool[] {false} ;
         H00RD5_A490ContagemResultado_ContratadaCod = new int[1] ;
         H00RD5_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         H00RD5_A489ContagemResultado_SistemaCod = new int[1] ;
         H00RD5_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         H00RD5_A29Contratante_Codigo = new int[1] ;
         H00RD5_n29Contratante_Codigo = new bool[] {false} ;
         H00RD5_A335Contratante_PessoaCod = new int[1] ;
         H00RD5_A590ContagemResultadoEvidencia_TipoArq = new String[] {""} ;
         H00RD5_n590ContagemResultadoEvidencia_TipoArq = new bool[] {false} ;
         H00RD5_A456ContagemResultado_Codigo = new int[1] ;
         H00RD5_n456ContagemResultado_Codigo = new bool[] {false} ;
         H00RD5_A586ContagemResultadoEvidencia_Codigo = new int[1] ;
         H00RD5_A589ContagemResultadoEvidencia_NomeArq = new String[] {""} ;
         H00RD5_n589ContagemResultadoEvidencia_NomeArq = new bool[] {false} ;
         H00RD5_A495ContagemResultado_SistemaNom = new String[] {""} ;
         H00RD5_n495ContagemResultado_SistemaNom = new bool[] {false} ;
         H00RD5_A52Contratada_AreaTrabalhoCod = new int[1] ;
         H00RD5_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         H00RD5_A9Contratante_RazaoSocial = new String[] {""} ;
         H00RD5_n9Contratante_RazaoSocial = new bool[] {false} ;
         H00RD5_A31Contratante_Telefone = new String[] {""} ;
         H00RD5_A14Contratante_Email = new String[] {""} ;
         H00RD5_n14Contratante_Email = new bool[] {false} ;
         H00RD5_A521Pessoa_CEP = new String[] {""} ;
         H00RD5_n521Pessoa_CEP = new bool[] {false} ;
         H00RD5_A23Estado_UF = new String[] {""} ;
         H00RD5_n23Estado_UF = new bool[] {false} ;
         H00RD5_A26Municipio_Nome = new String[] {""} ;
         H00RD5_n26Municipio_Nome = new bool[] {false} ;
         H00RD5_A519Pessoa_Endereco = new String[] {""} ;
         H00RD5_n519Pessoa_Endereco = new bool[] {false} ;
         H00RD5_A566ContagemResultado_DataUltCnt = new DateTime[] {DateTime.MinValue} ;
         H00RD5_n566ContagemResultado_DataUltCnt = new bool[] {false} ;
         H00RD5_A40000ContagemResultado_ContadorFMNom = new String[] {""} ;
         H00RD5_n40000ContagemResultado_ContadorFMNom = new bool[] {false} ;
         H00RD5_A493ContagemResultado_DemandaFM = new String[] {""} ;
         H00RD5_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         H00RD5_A457ContagemResultado_Demanda = new String[] {""} ;
         H00RD5_n457ContagemResultado_Demanda = new bool[] {false} ;
         A588ContagemResultadoEvidencia_Arquivo_Filetype = "";
         A588ContagemResultadoEvidencia_Arquivo_Filename = "";
         A40000ContagemResultado_ContadorFMNom = "";
         GridRow = new GXWebRow();
         H00RD6_A1109AnexoDe_Id = new int[1] ;
         H00RD6_A1108Anexo_TipoArq = new String[] {""} ;
         H00RD6_n1108Anexo_TipoArq = new bool[] {false} ;
         H00RD6_A1110AnexoDe_Tabela = new int[1] ;
         H00RD6_A1106Anexo_Codigo = new int[1] ;
         H00RD6_A1107Anexo_NomeArq = new String[] {""} ;
         H00RD6_n1107Anexo_NomeArq = new bool[] {false} ;
         A1101Anexo_Arquivo_Filetype = "";
         A1101Anexo_Arquivo_Filename = "";
         H00RD7_A511ContagemResultado_HoraCnt = new String[] {""} ;
         H00RD7_A454ContagemResultado_ContadorFSCod = new int[1] ;
         H00RD7_n454ContagemResultado_ContadorFSCod = new bool[] {false} ;
         H00RD7_A480ContagemResultado_CrFSPessoaCod = new int[1] ;
         H00RD7_n480ContagemResultado_CrFSPessoaCod = new bool[] {false} ;
         H00RD7_A503Pessoa_MunicipioCod = new int[1] ;
         H00RD7_n503Pessoa_MunicipioCod = new bool[] {false} ;
         H00RD7_A1073Usuario_CargoCod = new int[1] ;
         H00RD7_n1073Usuario_CargoCod = new bool[] {false} ;
         H00RD7_A1075Usuario_CargoUOCod = new int[1] ;
         H00RD7_n1075Usuario_CargoUOCod = new bool[] {false} ;
         H00RD7_A468ContagemResultado_NaoCnfDmnCod = new int[1] ;
         H00RD7_n468ContagemResultado_NaoCnfDmnCod = new bool[] {false} ;
         H00RD7_A428NaoConformidade_AreaTrabalhoCod = new int[1] ;
         H00RD7_n428NaoConformidade_AreaTrabalhoCod = new bool[] {false} ;
         H00RD7_A490ContagemResultado_ContratadaCod = new int[1] ;
         H00RD7_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         H00RD7_A489ContagemResultado_SistemaCod = new int[1] ;
         H00RD7_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         H00RD7_A470ContagemResultado_ContadorFMCod = new int[1] ;
         H00RD7_A479ContagemResultado_CrFMPessoaCod = new int[1] ;
         H00RD7_n479ContagemResultado_CrFMPessoaCod = new bool[] {false} ;
         H00RD7_A29Contratante_Codigo = new int[1] ;
         H00RD7_n29Contratante_Codigo = new bool[] {false} ;
         H00RD7_A335Contratante_PessoaCod = new int[1] ;
         H00RD7_A456ContagemResultado_Codigo = new int[1] ;
         H00RD7_n456ContagemResultado_Codigo = new bool[] {false} ;
         H00RD7_A517ContagemResultado_Ultima = new bool[] {false} ;
         H00RD7_A495ContagemResultado_SistemaNom = new String[] {""} ;
         H00RD7_n495ContagemResultado_SistemaNom = new bool[] {false} ;
         H00RD7_A52Contratada_AreaTrabalhoCod = new int[1] ;
         H00RD7_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         H00RD7_A473ContagemResultado_DataCnt = new DateTime[] {DateTime.MinValue} ;
         H00RD7_A474ContagemResultado_ContadorFMNom = new String[] {""} ;
         H00RD7_n474ContagemResultado_ContadorFMNom = new bool[] {false} ;
         H00RD7_A9Contratante_RazaoSocial = new String[] {""} ;
         H00RD7_n9Contratante_RazaoSocial = new bool[] {false} ;
         H00RD7_A31Contratante_Telefone = new String[] {""} ;
         H00RD7_A14Contratante_Email = new String[] {""} ;
         H00RD7_n14Contratante_Email = new bool[] {false} ;
         H00RD7_A521Pessoa_CEP = new String[] {""} ;
         H00RD7_n521Pessoa_CEP = new bool[] {false} ;
         H00RD7_A23Estado_UF = new String[] {""} ;
         H00RD7_n23Estado_UF = new bool[] {false} ;
         H00RD7_A26Municipio_Nome = new String[] {""} ;
         H00RD7_n26Municipio_Nome = new bool[] {false} ;
         H00RD7_A519Pessoa_Endereco = new String[] {""} ;
         H00RD7_n519Pessoa_Endereco = new bool[] {false} ;
         H00RD7_A493ContagemResultado_DemandaFM = new String[] {""} ;
         H00RD7_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         H00RD7_A457ContagemResultado_Demanda = new String[] {""} ;
         H00RD7_n457ContagemResultado_Demanda = new bool[] {false} ;
         AV13ExcelOrigem = new ExcelDocumentI();
         H00RD8_A586ContagemResultadoEvidencia_Codigo = new int[1] ;
         H00RD8_A589ContagemResultadoEvidencia_NomeArq = new String[] {""} ;
         H00RD8_n589ContagemResultadoEvidencia_NomeArq = new bool[] {false} ;
         H00RD8_A590ContagemResultadoEvidencia_TipoArq = new String[] {""} ;
         H00RD8_n590ContagemResultadoEvidencia_TipoArq = new bool[] {false} ;
         H00RD8_A588ContagemResultadoEvidencia_Arquivo = new String[] {""} ;
         H00RD8_n588ContagemResultadoEvidencia_Arquivo = new bool[] {false} ;
         H00RD9_A1106Anexo_Codigo = new int[1] ;
         H00RD9_A1107Anexo_NomeArq = new String[] {""} ;
         H00RD9_n1107Anexo_NomeArq = new bool[] {false} ;
         H00RD9_A1108Anexo_TipoArq = new String[] {""} ;
         H00RD9_n1108Anexo_TipoArq = new bool[] {false} ;
         H00RD9_A1101Anexo_Arquivo = new String[] {""} ;
         H00RD9_n1101Anexo_Arquivo = new bool[] {false} ;
         AV15ExcelDestino = new ExcelDocumentI();
         AV21Text = "";
         GXt_dtime1 = (DateTime)(DateTime.MinValue);
         AV29Aba = "";
         H00RD10_A848ParametrosPln_Codigo = new int[1] ;
         H00RD10_A849ParametrosPln_Campo = new String[] {""} ;
         H00RD10_A847ParametrosPln_AreaTrabalhoCod = new int[1] ;
         H00RD10_n847ParametrosPln_AreaTrabalhoCod = new bool[] {false} ;
         H00RD10_A2015ParametrosPln_Aba = new String[] {""} ;
         H00RD10_A851ParametrosPln_Linha = new int[1] ;
         H00RD10_A850ParametrosPln_Coluna = new String[] {""} ;
         H00RD11_A848ParametrosPln_Codigo = new int[1] ;
         H00RD11_A849ParametrosPln_Campo = new String[] {""} ;
         H00RD11_A847ParametrosPln_AreaTrabalhoCod = new int[1] ;
         H00RD11_n847ParametrosPln_AreaTrabalhoCod = new bool[] {false} ;
         H00RD11_A2015ParametrosPln_Aba = new String[] {""} ;
         H00RD11_A851ParametrosPln_Linha = new int[1] ;
         H00RD11_A850ParametrosPln_Coluna = new String[] {""} ;
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         gxblobfileaux = new GxFile(context.GetPhysicalPath());
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         bttBtnenter_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         H00RD12_A5AreaTrabalho_Codigo = new int[1] ;
         H00RD12_A6AreaTrabalho_Descricao = new String[] {""} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wp_summaryadd__default(),
            new Object[][] {
                new Object[] {
               H00RD2_A5AreaTrabalho_Codigo, H00RD2_A6AreaTrabalho_Descricao
               }
               , new Object[] {
               H00RD5_A146Modulo_Codigo, H00RD5_n146Modulo_Codigo, H00RD5_A127Sistema_Codigo, H00RD5_A1399Sistema_ImpUserCod, H00RD5_n1399Sistema_ImpUserCod, H00RD5_A1402Sistema_ImpUserPesCod, H00RD5_n1402Sistema_ImpUserPesCod, H00RD5_A503Pessoa_MunicipioCod, H00RD5_n503Pessoa_MunicipioCod, H00RD5_A135Sistema_AreaTrabalhoCod,
               H00RD5_A1073Usuario_CargoCod, H00RD5_n1073Usuario_CargoCod, H00RD5_A1075Usuario_CargoUOCod, H00RD5_n1075Usuario_CargoUOCod, H00RD5_A490ContagemResultado_ContratadaCod, H00RD5_n490ContagemResultado_ContratadaCod, H00RD5_A489ContagemResultado_SistemaCod, H00RD5_n489ContagemResultado_SistemaCod, H00RD5_A29Contratante_Codigo, H00RD5_n29Contratante_Codigo,
               H00RD5_A335Contratante_PessoaCod, H00RD5_A590ContagemResultadoEvidencia_TipoArq, H00RD5_n590ContagemResultadoEvidencia_TipoArq, H00RD5_A456ContagemResultado_Codigo, H00RD5_n456ContagemResultado_Codigo, H00RD5_A586ContagemResultadoEvidencia_Codigo, H00RD5_A589ContagemResultadoEvidencia_NomeArq, H00RD5_n589ContagemResultadoEvidencia_NomeArq, H00RD5_A495ContagemResultado_SistemaNom, H00RD5_n495ContagemResultado_SistemaNom,
               H00RD5_A52Contratada_AreaTrabalhoCod, H00RD5_n52Contratada_AreaTrabalhoCod, H00RD5_A9Contratante_RazaoSocial, H00RD5_n9Contratante_RazaoSocial, H00RD5_A31Contratante_Telefone, H00RD5_A14Contratante_Email, H00RD5_n14Contratante_Email, H00RD5_A521Pessoa_CEP, H00RD5_n521Pessoa_CEP, H00RD5_A23Estado_UF,
               H00RD5_n23Estado_UF, H00RD5_A26Municipio_Nome, H00RD5_n26Municipio_Nome, H00RD5_A519Pessoa_Endereco, H00RD5_n519Pessoa_Endereco, H00RD5_A566ContagemResultado_DataUltCnt, H00RD5_n566ContagemResultado_DataUltCnt, H00RD5_A40000ContagemResultado_ContadorFMNom, H00RD5_n40000ContagemResultado_ContadorFMNom, H00RD5_A493ContagemResultado_DemandaFM,
               H00RD5_n493ContagemResultado_DemandaFM, H00RD5_A457ContagemResultado_Demanda, H00RD5_n457ContagemResultado_Demanda
               }
               , new Object[] {
               H00RD6_A1109AnexoDe_Id, H00RD6_A1108Anexo_TipoArq, H00RD6_n1108Anexo_TipoArq, H00RD6_A1110AnexoDe_Tabela, H00RD6_A1106Anexo_Codigo, H00RD6_A1107Anexo_NomeArq, H00RD6_n1107Anexo_NomeArq
               }
               , new Object[] {
               H00RD7_A511ContagemResultado_HoraCnt, H00RD7_A454ContagemResultado_ContadorFSCod, H00RD7_n454ContagemResultado_ContadorFSCod, H00RD7_A480ContagemResultado_CrFSPessoaCod, H00RD7_n480ContagemResultado_CrFSPessoaCod, H00RD7_A503Pessoa_MunicipioCod, H00RD7_n503Pessoa_MunicipioCod, H00RD7_A1073Usuario_CargoCod, H00RD7_n1073Usuario_CargoCod, H00RD7_A1075Usuario_CargoUOCod,
               H00RD7_n1075Usuario_CargoUOCod, H00RD7_A468ContagemResultado_NaoCnfDmnCod, H00RD7_n468ContagemResultado_NaoCnfDmnCod, H00RD7_A428NaoConformidade_AreaTrabalhoCod, H00RD7_n428NaoConformidade_AreaTrabalhoCod, H00RD7_A490ContagemResultado_ContratadaCod, H00RD7_n490ContagemResultado_ContratadaCod, H00RD7_A489ContagemResultado_SistemaCod, H00RD7_n489ContagemResultado_SistemaCod, H00RD7_A470ContagemResultado_ContadorFMCod,
               H00RD7_A479ContagemResultado_CrFMPessoaCod, H00RD7_n479ContagemResultado_CrFMPessoaCod, H00RD7_A29Contratante_Codigo, H00RD7_n29Contratante_Codigo, H00RD7_A335Contratante_PessoaCod, H00RD7_A456ContagemResultado_Codigo, H00RD7_A517ContagemResultado_Ultima, H00RD7_A495ContagemResultado_SistemaNom, H00RD7_n495ContagemResultado_SistemaNom, H00RD7_A52Contratada_AreaTrabalhoCod,
               H00RD7_n52Contratada_AreaTrabalhoCod, H00RD7_A473ContagemResultado_DataCnt, H00RD7_A474ContagemResultado_ContadorFMNom, H00RD7_n474ContagemResultado_ContadorFMNom, H00RD7_A9Contratante_RazaoSocial, H00RD7_n9Contratante_RazaoSocial, H00RD7_A31Contratante_Telefone, H00RD7_A14Contratante_Email, H00RD7_n14Contratante_Email, H00RD7_A521Pessoa_CEP,
               H00RD7_n521Pessoa_CEP, H00RD7_A23Estado_UF, H00RD7_n23Estado_UF, H00RD7_A26Municipio_Nome, H00RD7_n26Municipio_Nome, H00RD7_A519Pessoa_Endereco, H00RD7_n519Pessoa_Endereco, H00RD7_A493ContagemResultado_DemandaFM, H00RD7_n493ContagemResultado_DemandaFM, H00RD7_A457ContagemResultado_Demanda,
               H00RD7_n457ContagemResultado_Demanda
               }
               , new Object[] {
               H00RD8_A586ContagemResultadoEvidencia_Codigo, H00RD8_A589ContagemResultadoEvidencia_NomeArq, H00RD8_n589ContagemResultadoEvidencia_NomeArq, H00RD8_A590ContagemResultadoEvidencia_TipoArq, H00RD8_n590ContagemResultadoEvidencia_TipoArq, H00RD8_A588ContagemResultadoEvidencia_Arquivo, H00RD8_n588ContagemResultadoEvidencia_Arquivo
               }
               , new Object[] {
               H00RD9_A1106Anexo_Codigo, H00RD9_A1107Anexo_NomeArq, H00RD9_n1107Anexo_NomeArq, H00RD9_A1108Anexo_TipoArq, H00RD9_n1108Anexo_TipoArq, H00RD9_A1101Anexo_Arquivo, H00RD9_n1101Anexo_Arquivo
               }
               , new Object[] {
               H00RD10_A848ParametrosPln_Codigo, H00RD10_A849ParametrosPln_Campo, H00RD10_A847ParametrosPln_AreaTrabalhoCod, H00RD10_n847ParametrosPln_AreaTrabalhoCod, H00RD10_A2015ParametrosPln_Aba, H00RD10_A851ParametrosPln_Linha, H00RD10_A850ParametrosPln_Coluna
               }
               , new Object[] {
               H00RD11_A848ParametrosPln_Codigo, H00RD11_A849ParametrosPln_Campo, H00RD11_A847ParametrosPln_AreaTrabalhoCod, H00RD11_n847ParametrosPln_AreaTrabalhoCod, H00RD11_A2015ParametrosPln_Aba, H00RD11_A851ParametrosPln_Linha, H00RD11_A850ParametrosPln_Coluna
               }
               , new Object[] {
               H00RD12_A5AreaTrabalho_Codigo, H00RD12_A6AreaTrabalho_Descricao
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavDatacnt_Enabled = 0;
         edtavContratante_razaosocial_Enabled = 0;
         edtavContratante_telefone_Enabled = 0;
         edtavContratante_email_Enabled = 0;
         edtavContratante_endereco_Enabled = 0;
         cmbavTabela.Enabled = 0;
         edtavLocal_codigo_Enabled = 0;
         dynavAreatrabalho_codigo.Enabled = 0;
         edtavOsfsosfm_Enabled = 0;
         edtavSistema_nome_Enabled = 0;
         edtavFile_nome_Enabled = 0;
      }

      private short nRcdExists_7 ;
      private short nIsMod_7 ;
      private short nRcdExists_6 ;
      private short nIsMod_6 ;
      private short nRcdExists_4 ;
      private short nIsMod_4 ;
      private short nRcdExists_5 ;
      private short nIsMod_5 ;
      private short nRcdExists_3 ;
      private short nIsMod_3 ;
      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_11 ;
      private short nGXsfl_11_idx=1 ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short AV22c ;
      private short AV23l ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_11_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short edtavBlob_Display ;
      private short GRID_nEOF ;
      private short nGXsfl_11_fel_idx=1 ;
      private short AV26row ;
      private short AV27col ;
      private short AV47GXLvl201 ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int A456ContagemResultado_Codigo ;
      private int A586ContagemResultadoEvidencia_Codigo ;
      private int A52Contratada_AreaTrabalhoCod ;
      private int A1109AnexoDe_Id ;
      private int A1110AnexoDe_Tabela ;
      private int A1106Anexo_Codigo ;
      private int A847ParametrosPln_AreaTrabalhoCod ;
      private int A851ParametrosPln_Linha ;
      private int AV10Tabela ;
      private int AV9Local_Codigo ;
      private int AV25AreaTrabalho_Codigo ;
      private int subGrid_Islastpage ;
      private int edtavDatacnt_Enabled ;
      private int edtavContratante_razaosocial_Enabled ;
      private int edtavContratante_telefone_Enabled ;
      private int edtavContratante_email_Enabled ;
      private int edtavContratante_endereco_Enabled ;
      private int edtavLocal_codigo_Enabled ;
      private int edtavOsfsosfm_Enabled ;
      private int edtavSistema_nome_Enabled ;
      private int edtavFile_nome_Enabled ;
      private int subGrid_Backcolorodd ;
      private int subGrid_Titleforecolor ;
      private int AV40GXV1 ;
      private int A146Modulo_Codigo ;
      private int A127Sistema_Codigo ;
      private int A1399Sistema_ImpUserCod ;
      private int A1402Sistema_ImpUserPesCod ;
      private int A503Pessoa_MunicipioCod ;
      private int A135Sistema_AreaTrabalhoCod ;
      private int A1073Usuario_CargoCod ;
      private int A1075Usuario_CargoUOCod ;
      private int A490ContagemResultado_ContratadaCod ;
      private int A489ContagemResultado_SistemaCod ;
      private int A29Contratante_Codigo ;
      private int A335Contratante_PessoaCod ;
      private int A454ContagemResultado_ContadorFSCod ;
      private int A480ContagemResultado_CrFSPessoaCod ;
      private int A468ContagemResultado_NaoCnfDmnCod ;
      private int A428NaoConformidade_AreaTrabalhoCod ;
      private int A470ContagemResultado_ContadorFMCod ;
      private int A479ContagemResultado_CrFMPessoaCod ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private int edtavDatacnt_Visible ;
      private int edtavContratante_razaosocial_Visible ;
      private int edtavContratante_telefone_Visible ;
      private int edtavContratante_email_Visible ;
      private int edtavContratante_endereco_Visible ;
      private int edtavLocal_codigo_Visible ;
      private int edtavOsfsosfm_Visible ;
      private int edtavSistema_nome_Visible ;
      private int edtavFile_nome_Visible ;
      private long GRID_nCurrentRecord ;
      private long GRID_nFirstRecordOnPage ;
      private decimal AV28Valor ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_11_idx="0001" ;
      private String A590ContagemResultadoEvidencia_TipoArq ;
      private String A589ContagemResultadoEvidencia_NomeArq ;
      private String A9Contratante_RazaoSocial ;
      private String A31Contratante_Telefone ;
      private String A26Municipio_Nome ;
      private String A23Estado_UF ;
      private String A521Pessoa_CEP ;
      private String A1108Anexo_TipoArq ;
      private String A1107Anexo_NomeArq ;
      private String A474ContagemResultado_ContadorFMNom ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String AV33Colaborador_Nome ;
      private String A2015ParametrosPln_Aba ;
      private String A850ParametrosPln_Coluna ;
      private String GXCCtlgxBlob ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavDatacnt_Internalname ;
      private String AV34Contratante_RazaoSocial ;
      private String edtavContratante_razaosocial_Internalname ;
      private String AV35Contratante_Telefone ;
      private String edtavContratante_telefone_Internalname ;
      private String edtavContratante_email_Internalname ;
      private String AV37Contratante_Endereco ;
      private String edtavContratante_endereco_Internalname ;
      private String cmbavTabela_Internalname ;
      private String edtavLocal_codigo_Internalname ;
      private String chkavSelected_Internalname ;
      private String dynavAreatrabalho_codigo_Internalname ;
      private String edtavOsfsosfm_Internalname ;
      private String edtavSistema_nome_Internalname ;
      private String AV11File_Nome ;
      private String edtavFile_nome_Internalname ;
      private String GXCCtl ;
      private String scmdbuf ;
      private String edtavBlob_Internalname ;
      private String bttBtnfechar_Jsonclick ;
      private String bttBtnfechar_Internalname ;
      private String edtavOsfsosfm_Linktarget ;
      private String A588ContagemResultadoEvidencia_Arquivo_Filetype ;
      private String A588ContagemResultadoEvidencia_Arquivo_Filename ;
      private String A40000ContagemResultado_ContadorFMNom ;
      private String edtavOsfsosfm_Link ;
      private String A1101Anexo_Arquivo_Filetype ;
      private String A1101Anexo_Arquivo_Filename ;
      private String sGXsfl_11_fel_idx="0001" ;
      private String AV21Text ;
      private String AV29Aba ;
      private String sStyleString ;
      private String tblTable1_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String TempTags ;
      private String edtavBlob_Filetype ;
      private String edtavBlob_Contenttype ;
      private String edtavBlob_Parameters ;
      private String edtavBlob_Jsonclick ;
      private String subGrid_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String bttBtnenter_Internalname ;
      private String bttBtnenter_Jsonclick ;
      private String ROClassString ;
      private String edtavDatacnt_Jsonclick ;
      private String edtavContratante_razaosocial_Jsonclick ;
      private String edtavContratante_telefone_Jsonclick ;
      private String edtavContratante_email_Jsonclick ;
      private String edtavContratante_endereco_Jsonclick ;
      private String cmbavTabela_Jsonclick ;
      private String edtavLocal_codigo_Jsonclick ;
      private String dynavAreatrabalho_codigo_Jsonclick ;
      private String edtavOsfsosfm_Jsonclick ;
      private String edtavSistema_nome_Jsonclick ;
      private String edtavFile_nome_Jsonclick ;
      private DateTime GXt_dtime1 ;
      private DateTime A566ContagemResultado_DataUltCnt ;
      private DateTime A473ContagemResultado_DataCnt ;
      private DateTime AV32DataCnt ;
      private bool entryPointCalled ;
      private bool n456ContagemResultado_Codigo ;
      private bool n590ContagemResultadoEvidencia_TipoArq ;
      private bool n589ContagemResultadoEvidencia_NomeArq ;
      private bool n495ContagemResultado_SistemaNom ;
      private bool n52Contratada_AreaTrabalhoCod ;
      private bool n566ContagemResultado_DataUltCnt ;
      private bool n9Contratante_RazaoSocial ;
      private bool n14Contratante_Email ;
      private bool n519Pessoa_Endereco ;
      private bool n26Municipio_Nome ;
      private bool n23Estado_UF ;
      private bool n521Pessoa_CEP ;
      private bool n1108Anexo_TipoArq ;
      private bool n1107Anexo_NomeArq ;
      private bool A517ContagemResultado_Ultima ;
      private bool n474ContagemResultado_ContadorFMNom ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool AV8Selected ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool n146Modulo_Codigo ;
      private bool n1399Sistema_ImpUserCod ;
      private bool n1402Sistema_ImpUserPesCod ;
      private bool n503Pessoa_MunicipioCod ;
      private bool n1073Usuario_CargoCod ;
      private bool n1075Usuario_CargoUOCod ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n489ContagemResultado_SistemaCod ;
      private bool n29Contratante_Codigo ;
      private bool n40000ContagemResultado_ContadorFMNom ;
      private bool n493ContagemResultado_DemandaFM ;
      private bool n457ContagemResultado_Demanda ;
      private bool n454ContagemResultado_ContadorFSCod ;
      private bool n480ContagemResultado_CrFSPessoaCod ;
      private bool n468ContagemResultado_NaoCnfDmnCod ;
      private bool n428NaoConformidade_AreaTrabalhoCod ;
      private bool n479ContagemResultado_CrFMPessoaCod ;
      private bool n588ContagemResultadoEvidencia_Arquivo ;
      private bool n1101Anexo_Arquivo ;
      private bool n847ParametrosPln_AreaTrabalhoCod ;
      private String A501ContagemResultado_OsFsOsFm ;
      private String A495ContagemResultado_SistemaNom ;
      private String A14Contratante_Email ;
      private String A519Pessoa_Endereco ;
      private String AV24Campo ;
      private String A849ParametrosPln_Campo ;
      private String A457ContagemResultado_Demanda ;
      private String A493ContagemResultado_DemandaFM ;
      private String AV36Contratante_Email ;
      private String AV20OsFsOsFm ;
      private String AV30Sistema_Nome ;
      private String A588ContagemResultadoEvidencia_Arquivo ;
      private String A1101Anexo_Arquivo ;
      private String AV17Planilha ;
      private String AV12Blob ;
      private IGxSession AV19WebSession ;
      private GxFile gxblobfileaux ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavTabela ;
      private GXCheckbox chkavSelected ;
      private GXCombobox dynavAreatrabalho_codigo ;
      private IDataStoreProvider pr_default ;
      private int[] H00RD2_A5AreaTrabalho_Codigo ;
      private String[] H00RD2_A6AreaTrabalho_Descricao ;
      private int[] H00RD5_A146Modulo_Codigo ;
      private bool[] H00RD5_n146Modulo_Codigo ;
      private int[] H00RD5_A127Sistema_Codigo ;
      private int[] H00RD5_A1399Sistema_ImpUserCod ;
      private bool[] H00RD5_n1399Sistema_ImpUserCod ;
      private int[] H00RD5_A1402Sistema_ImpUserPesCod ;
      private bool[] H00RD5_n1402Sistema_ImpUserPesCod ;
      private int[] H00RD5_A503Pessoa_MunicipioCod ;
      private bool[] H00RD5_n503Pessoa_MunicipioCod ;
      private int[] H00RD5_A135Sistema_AreaTrabalhoCod ;
      private int[] H00RD5_A1073Usuario_CargoCod ;
      private bool[] H00RD5_n1073Usuario_CargoCod ;
      private int[] H00RD5_A1075Usuario_CargoUOCod ;
      private bool[] H00RD5_n1075Usuario_CargoUOCod ;
      private int[] H00RD5_A490ContagemResultado_ContratadaCod ;
      private bool[] H00RD5_n490ContagemResultado_ContratadaCod ;
      private int[] H00RD5_A489ContagemResultado_SistemaCod ;
      private bool[] H00RD5_n489ContagemResultado_SistemaCod ;
      private int[] H00RD5_A29Contratante_Codigo ;
      private bool[] H00RD5_n29Contratante_Codigo ;
      private int[] H00RD5_A335Contratante_PessoaCod ;
      private String[] H00RD5_A590ContagemResultadoEvidencia_TipoArq ;
      private bool[] H00RD5_n590ContagemResultadoEvidencia_TipoArq ;
      private int[] H00RD5_A456ContagemResultado_Codigo ;
      private bool[] H00RD5_n456ContagemResultado_Codigo ;
      private int[] H00RD5_A586ContagemResultadoEvidencia_Codigo ;
      private String[] H00RD5_A589ContagemResultadoEvidencia_NomeArq ;
      private bool[] H00RD5_n589ContagemResultadoEvidencia_NomeArq ;
      private String[] H00RD5_A495ContagemResultado_SistemaNom ;
      private bool[] H00RD5_n495ContagemResultado_SistemaNom ;
      private int[] H00RD5_A52Contratada_AreaTrabalhoCod ;
      private bool[] H00RD5_n52Contratada_AreaTrabalhoCod ;
      private String[] H00RD5_A9Contratante_RazaoSocial ;
      private bool[] H00RD5_n9Contratante_RazaoSocial ;
      private String[] H00RD5_A31Contratante_Telefone ;
      private String[] H00RD5_A14Contratante_Email ;
      private bool[] H00RD5_n14Contratante_Email ;
      private String[] H00RD5_A521Pessoa_CEP ;
      private bool[] H00RD5_n521Pessoa_CEP ;
      private String[] H00RD5_A23Estado_UF ;
      private bool[] H00RD5_n23Estado_UF ;
      private String[] H00RD5_A26Municipio_Nome ;
      private bool[] H00RD5_n26Municipio_Nome ;
      private String[] H00RD5_A519Pessoa_Endereco ;
      private bool[] H00RD5_n519Pessoa_Endereco ;
      private DateTime[] H00RD5_A566ContagemResultado_DataUltCnt ;
      private bool[] H00RD5_n566ContagemResultado_DataUltCnt ;
      private String[] H00RD5_A40000ContagemResultado_ContadorFMNom ;
      private bool[] H00RD5_n40000ContagemResultado_ContadorFMNom ;
      private String[] H00RD5_A493ContagemResultado_DemandaFM ;
      private bool[] H00RD5_n493ContagemResultado_DemandaFM ;
      private String[] H00RD5_A457ContagemResultado_Demanda ;
      private bool[] H00RD5_n457ContagemResultado_Demanda ;
      private int[] H00RD6_A1109AnexoDe_Id ;
      private String[] H00RD6_A1108Anexo_TipoArq ;
      private bool[] H00RD6_n1108Anexo_TipoArq ;
      private int[] H00RD6_A1110AnexoDe_Tabela ;
      private int[] H00RD6_A1106Anexo_Codigo ;
      private String[] H00RD6_A1107Anexo_NomeArq ;
      private bool[] H00RD6_n1107Anexo_NomeArq ;
      private String[] H00RD7_A511ContagemResultado_HoraCnt ;
      private int[] H00RD7_A454ContagemResultado_ContadorFSCod ;
      private bool[] H00RD7_n454ContagemResultado_ContadorFSCod ;
      private int[] H00RD7_A480ContagemResultado_CrFSPessoaCod ;
      private bool[] H00RD7_n480ContagemResultado_CrFSPessoaCod ;
      private int[] H00RD7_A503Pessoa_MunicipioCod ;
      private bool[] H00RD7_n503Pessoa_MunicipioCod ;
      private int[] H00RD7_A1073Usuario_CargoCod ;
      private bool[] H00RD7_n1073Usuario_CargoCod ;
      private int[] H00RD7_A1075Usuario_CargoUOCod ;
      private bool[] H00RD7_n1075Usuario_CargoUOCod ;
      private int[] H00RD7_A468ContagemResultado_NaoCnfDmnCod ;
      private bool[] H00RD7_n468ContagemResultado_NaoCnfDmnCod ;
      private int[] H00RD7_A428NaoConformidade_AreaTrabalhoCod ;
      private bool[] H00RD7_n428NaoConformidade_AreaTrabalhoCod ;
      private int[] H00RD7_A490ContagemResultado_ContratadaCod ;
      private bool[] H00RD7_n490ContagemResultado_ContratadaCod ;
      private int[] H00RD7_A489ContagemResultado_SistemaCod ;
      private bool[] H00RD7_n489ContagemResultado_SistemaCod ;
      private int[] H00RD7_A470ContagemResultado_ContadorFMCod ;
      private int[] H00RD7_A479ContagemResultado_CrFMPessoaCod ;
      private bool[] H00RD7_n479ContagemResultado_CrFMPessoaCod ;
      private int[] H00RD7_A29Contratante_Codigo ;
      private bool[] H00RD7_n29Contratante_Codigo ;
      private int[] H00RD7_A335Contratante_PessoaCod ;
      private int[] H00RD7_A456ContagemResultado_Codigo ;
      private bool[] H00RD7_n456ContagemResultado_Codigo ;
      private bool[] H00RD7_A517ContagemResultado_Ultima ;
      private String[] H00RD7_A495ContagemResultado_SistemaNom ;
      private bool[] H00RD7_n495ContagemResultado_SistemaNom ;
      private int[] H00RD7_A52Contratada_AreaTrabalhoCod ;
      private bool[] H00RD7_n52Contratada_AreaTrabalhoCod ;
      private DateTime[] H00RD7_A473ContagemResultado_DataCnt ;
      private String[] H00RD7_A474ContagemResultado_ContadorFMNom ;
      private bool[] H00RD7_n474ContagemResultado_ContadorFMNom ;
      private String[] H00RD7_A9Contratante_RazaoSocial ;
      private bool[] H00RD7_n9Contratante_RazaoSocial ;
      private String[] H00RD7_A31Contratante_Telefone ;
      private String[] H00RD7_A14Contratante_Email ;
      private bool[] H00RD7_n14Contratante_Email ;
      private String[] H00RD7_A521Pessoa_CEP ;
      private bool[] H00RD7_n521Pessoa_CEP ;
      private String[] H00RD7_A23Estado_UF ;
      private bool[] H00RD7_n23Estado_UF ;
      private String[] H00RD7_A26Municipio_Nome ;
      private bool[] H00RD7_n26Municipio_Nome ;
      private String[] H00RD7_A519Pessoa_Endereco ;
      private bool[] H00RD7_n519Pessoa_Endereco ;
      private String[] H00RD7_A493ContagemResultado_DemandaFM ;
      private bool[] H00RD7_n493ContagemResultado_DemandaFM ;
      private String[] H00RD7_A457ContagemResultado_Demanda ;
      private bool[] H00RD7_n457ContagemResultado_Demanda ;
      private int[] H00RD8_A586ContagemResultadoEvidencia_Codigo ;
      private String[] H00RD8_A589ContagemResultadoEvidencia_NomeArq ;
      private bool[] H00RD8_n589ContagemResultadoEvidencia_NomeArq ;
      private String[] H00RD8_A590ContagemResultadoEvidencia_TipoArq ;
      private bool[] H00RD8_n590ContagemResultadoEvidencia_TipoArq ;
      private String[] H00RD8_A588ContagemResultadoEvidencia_Arquivo ;
      private bool[] H00RD8_n588ContagemResultadoEvidencia_Arquivo ;
      private int[] H00RD9_A1106Anexo_Codigo ;
      private String[] H00RD9_A1107Anexo_NomeArq ;
      private bool[] H00RD9_n1107Anexo_NomeArq ;
      private String[] H00RD9_A1108Anexo_TipoArq ;
      private bool[] H00RD9_n1108Anexo_TipoArq ;
      private String[] H00RD9_A1101Anexo_Arquivo ;
      private bool[] H00RD9_n1101Anexo_Arquivo ;
      private int[] H00RD10_A848ParametrosPln_Codigo ;
      private String[] H00RD10_A849ParametrosPln_Campo ;
      private int[] H00RD10_A847ParametrosPln_AreaTrabalhoCod ;
      private bool[] H00RD10_n847ParametrosPln_AreaTrabalhoCod ;
      private String[] H00RD10_A2015ParametrosPln_Aba ;
      private int[] H00RD10_A851ParametrosPln_Linha ;
      private String[] H00RD10_A850ParametrosPln_Coluna ;
      private int[] H00RD11_A848ParametrosPln_Codigo ;
      private String[] H00RD11_A849ParametrosPln_Campo ;
      private int[] H00RD11_A847ParametrosPln_AreaTrabalhoCod ;
      private bool[] H00RD11_n847ParametrosPln_AreaTrabalhoCod ;
      private String[] H00RD11_A2015ParametrosPln_Aba ;
      private int[] H00RD11_A851ParametrosPln_Linha ;
      private String[] H00RD11_A850ParametrosPln_Coluna ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private int[] H00RD12_A5AreaTrabalho_Codigo ;
      private String[] H00RD12_A6AreaTrabalho_Descricao ;
      private ExcelDocumentI AV13ExcelOrigem ;
      private ExcelDocumentI AV15ExcelDestino ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV7Codigos ;
      [ObjectCollection(ItemType=typeof( SdtSDT_Codigos ))]
      private IGxCollection AV5SDT_Codigos ;
      private GXWebForm Form ;
      private SdtSDT_Codigos AV6Codigo ;
   }

   public class wp_summaryadd__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00RD5( IGxContext context ,
                                             int A456ContagemResultado_Codigo ,
                                             IGxCollection AV7Codigos ,
                                             String A590ContagemResultadoEvidencia_TipoArq )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T2.[Modulo_Codigo], T3.[Sistema_Codigo], T4.[Sistema_ImpUserCod] AS Sistema_ImpUserCod, T5.[Usuario_PessoaCod] AS Sistema_ImpUserPesCod, T6.[Pessoa_MunicipioCod] AS Pessoa_MunicipioCod, T4.[Sistema_AreaTrabalhoCod] AS Sistema_AreaTrabalhoCod, T5.[Usuario_CargoCod] AS Usuario_CargoCod, T8.[Cargo_UOCod] AS Usuario_CargoUOCod, T2.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T2.[ContagemResultado_SistemaCod] AS ContagemResultado_SistemaCod, T10.[Contratante_Codigo], T11.[Contratante_PessoaCod] AS Contratante_PessoaCod, T1.[ContagemResultadoEvidencia_TipoArq], T1.[ContagemResultado_Codigo], T1.[ContagemResultadoEvidencia_Codigo], T1.[ContagemResultadoEvidencia_NomeArq], T14.[Sistema_Nome] AS ContagemResultado_SistemaNom, T13.[Contratada_AreaTrabalhoCod], T12.[Pessoa_Nome] AS Contratante_RazaoSocial, T11.[Contratante_Telefone], T11.[Contratante_Email], T6.[Pessoa_CEP], T9.[Estado_UF], T7.[Municipio_Nome], T6.[Pessoa_Endereco], COALESCE( T15.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) AS ContagemResultado_DataUltCnt, COALESCE( T16.[ContagemResultado_ContadorFMNom], '') AS ContagemResultado_ContadorFMNom, T2.[ContagemResultado_DemandaFM], T2.[ContagemResultado_Demanda] FROM ((((((((((((((([ContagemResultadoEvidencia] T1 WITH (NOLOCK) INNER JOIN [ContagemResultado] T2 WITH (NOLOCK) ON T2.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) LEFT JOIN [Modulo] T3 WITH (NOLOCK) ON T3.[Modulo_Codigo] = T2.[Modulo_Codigo]) LEFT JOIN [Sistema] T4 WITH (NOLOCK) ON T4.[Sistema_Codigo] = T3.[Sistema_Codigo]) LEFT JOIN [Usuario] T5 WITH (NOLOCK) ON T5.[Usuario_Codigo] = T4.[Sistema_ImpUserCod]) LEFT JOIN [Pessoa] T6 WITH (NOLOCK) ON T6.[Pessoa_Codigo] = T5.[Usuario_PessoaCod]) LEFT JOIN [Municipio] T7 WITH (NOLOCK) ON T7.[Municipio_Codigo]";
         scmdbuf = scmdbuf + " = T6.[Pessoa_MunicipioCod]) LEFT JOIN [Geral_Cargo] T8 WITH (NOLOCK) ON T8.[Cargo_Codigo] = T5.[Usuario_CargoCod]) LEFT JOIN [Geral_UnidadeOrganizacional] T9 WITH (NOLOCK) ON T9.[UnidadeOrganizacional_Codigo] = T8.[Cargo_UOCod]) LEFT JOIN [AreaTrabalho] T10 WITH (NOLOCK) ON T10.[AreaTrabalho_Codigo] = T4.[Sistema_AreaTrabalhoCod]) LEFT JOIN [Contratante] T11 WITH (NOLOCK) ON T11.[Contratante_Codigo] = T10.[Contratante_Codigo]) LEFT JOIN [Pessoa] T12 WITH (NOLOCK) ON T12.[Pessoa_Codigo] = T11.[Contratante_PessoaCod]) LEFT JOIN [Contratada] T13 WITH (NOLOCK) ON T13.[Contratada_Codigo] = T2.[ContagemResultado_ContratadaCod]) LEFT JOIN [Sistema] T14 WITH (NOLOCK) ON T14.[Sistema_Codigo] = T2.[ContagemResultado_SistemaCod]) LEFT JOIN (SELECT MIN([ContagemResultado_DataCnt]) AS ContagemResultado_DataUltCnt, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T15 ON T15.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) LEFT JOIN (SELECT MIN(T19.[Pessoa_Nome]) AS ContagemResultado_ContadorFMNom, T17.[ContagemResultado_Codigo] FROM (([ContagemResultadoContagens] T17 WITH (NOLOCK) INNER JOIN [Usuario] T18 WITH (NOLOCK) ON T18.[Usuario_Codigo] = T17.[ContagemResultado_ContadorFMCod]) LEFT JOIN [Pessoa] T19 WITH (NOLOCK) ON T19.[Pessoa_Codigo] = T18.[Usuario_PessoaCod]) WHERE T17.[ContagemResultado_Ultima] = 1 GROUP BY T17.[ContagemResultado_Codigo] ) T16 ON T16.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo])";
         scmdbuf = scmdbuf + " WHERE (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV7Codigos, "T1.[ContagemResultado_Codigo] IN (", ")") + ")";
         scmdbuf = scmdbuf + " and (LOWER(RTRIM(LTRIM(T1.[ContagemResultadoEvidencia_TipoArq]))) = 'xlsx')";
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultadoEvidencia_Codigo]";
         GXv_Object2[0] = scmdbuf;
         return GXv_Object2 ;
      }

      protected Object[] conditional_H00RD6( IGxContext context ,
                                             int A1109AnexoDe_Id ,
                                             IGxCollection AV7Codigos ,
                                             String A1108Anexo_TipoArq ,
                                             int A1110AnexoDe_Tabela )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT T1.[AnexoDe_Id], T2.[Anexo_TipoArq], T1.[AnexoDe_Tabela], T1.[Anexo_Codigo], T2.[Anexo_NomeArq] FROM ([AnexosDe] T1 WITH (NOLOCK) INNER JOIN [Anexos] T2 WITH (NOLOCK) ON T2.[Anexo_Codigo] = T1.[Anexo_Codigo])";
         scmdbuf = scmdbuf + " WHERE (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV7Codigos, "T1.[AnexoDe_Id] IN (", ")") + ")";
         scmdbuf = scmdbuf + " and (LOWER(RTRIM(LTRIM(T2.[Anexo_TipoArq]))) = 'xlsx')";
         scmdbuf = scmdbuf + " and (T1.[AnexoDe_Tabela] = 1)";
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[Anexo_Codigo], T1.[AnexoDe_Id], T1.[AnexoDe_Tabela]";
         GXv_Object4[0] = scmdbuf;
         return GXv_Object4 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 1 :
                     return conditional_H00RD5(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] );
               case 2 :
                     return conditional_H00RD6(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (int)dynConstraints[3] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00RD2 ;
          prmH00RD2 = new Object[] {
          } ;
          Object[] prmH00RD7 ;
          prmH00RD7 = new Object[] {
          new Object[] {"@AnexoDe_Id",SqlDbType.Int,6,0}
          } ;
          String cmdBufferH00RD7 ;
          cmdBufferH00RD7=" SELECT TOP 1 T1.[ContagemResultado_HoraCnt], T4.[ContagemResultado_ContadorFSCod] AS ContagemResultado_ContadorFSCo, T5.[Usuario_PessoaCod] AS ContagemResultado_CrFSPessoaCo, T6.[Pessoa_MunicipioCod] AS Pessoa_MunicipioCod, T5.[Usuario_CargoCod] AS Usuario_CargoCod, T8.[Cargo_UOCod] AS Usuario_CargoUOCod, T4.[ContagemResultado_NaoCnfDmnCod] AS ContagemResultado_NaoCnfDmnCod, T10.[NaoConformidade_AreaTrabalhoCod] AS NaoConformidade_AreaTrabalhoCo, T4.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T4.[ContagemResultado_SistemaCod] AS ContagemResultado_SistemaCod, T1.[ContagemResultado_ContadorFMCod] AS ContagemResultado_ContadorFMCod, T2.[Usuario_PessoaCod] AS ContagemResultado_CrFMPessoaCod, T11.[Contratante_Codigo], T12.[Contratante_PessoaCod] AS Contratante_PessoaCod, T1.[ContagemResultado_Codigo], T1.[ContagemResultado_Ultima], T15.[Sistema_Nome] AS ContagemResultado_SistemaNom, T14.[Contratada_AreaTrabalhoCod], T1.[ContagemResultado_DataCnt], T3.[Pessoa_Nome] AS ContagemResultado_ContadorFMNom, T13.[Pessoa_Nome] AS Contratante_RazaoSocial, T12.[Contratante_Telefone], T12.[Contratante_Email], T6.[Pessoa_CEP], T9.[Estado_UF], T7.[Municipio_Nome], T6.[Pessoa_Endereco], T4.[ContagemResultado_DemandaFM], T4.[ContagemResultado_Demanda] FROM (((((((((((((([ContagemResultadoContagens] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[ContagemResultado_ContadorFMCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod]) INNER JOIN [ContagemResultado] T4 WITH (NOLOCK) ON T4.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) LEFT JOIN [Usuario] T5 WITH (NOLOCK) ON T5.[Usuario_Codigo] = T4.[ContagemResultado_ContadorFSCod]) LEFT JOIN [Pessoa] T6 WITH (NOLOCK) ON T6.[Pessoa_Codigo] = T5.[Usuario_PessoaCod]) "
          + " LEFT JOIN [Municipio] T7 WITH (NOLOCK) ON T7.[Municipio_Codigo] = T6.[Pessoa_MunicipioCod]) LEFT JOIN [Geral_Cargo] T8 WITH (NOLOCK) ON T8.[Cargo_Codigo] = T5.[Usuario_CargoCod]) LEFT JOIN [Geral_UnidadeOrganizacional] T9 WITH (NOLOCK) ON T9.[UnidadeOrganizacional_Codigo] = T8.[Cargo_UOCod]) LEFT JOIN [NaoConformidade] T10 WITH (NOLOCK) ON T10.[NaoConformidade_Codigo] = T4.[ContagemResultado_NaoCnfDmnCod]) LEFT JOIN [AreaTrabalho] T11 WITH (NOLOCK) ON T11.[AreaTrabalho_Codigo] = T10.[NaoConformidade_AreaTrabalhoCod]) LEFT JOIN [Contratante] T12 WITH (NOLOCK) ON T12.[Contratante_Codigo] = T11.[Contratante_Codigo]) LEFT JOIN [Pessoa] T13 WITH (NOLOCK) ON T13.[Pessoa_Codigo] = T12.[Contratante_PessoaCod]) LEFT JOIN [Contratada] T14 WITH (NOLOCK) ON T14.[Contratada_Codigo] = T4.[ContagemResultado_ContratadaCod]) LEFT JOIN [Sistema] T15 WITH (NOLOCK) ON T15.[Sistema_Codigo] = T4.[ContagemResultado_SistemaCod]) WHERE (T1.[ContagemResultado_Codigo] = @AnexoDe_Id) AND (T1.[ContagemResultado_Ultima] = 1) ORDER BY T1.[ContagemResultado_Codigo]" ;
          Object[] prmH00RD8 ;
          prmH00RD8 = new Object[] {
          new Object[] {"@AV9Local_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00RD9 ;
          prmH00RD9 = new Object[] {
          new Object[] {"@AV9Local_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00RD10 ;
          prmH00RD10 = new Object[] {
          new Object[] {"@AV25AreaTrabalho_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV24Campo",SqlDbType.VarChar,50,0}
          } ;
          Object[] prmH00RD11 ;
          prmH00RD11 = new Object[] {
          new Object[] {"@AV24Campo",SqlDbType.VarChar,50,0}
          } ;
          Object[] prmH00RD12 ;
          prmH00RD12 = new Object[] {
          } ;
          Object[] prmH00RD5 ;
          prmH00RD5 = new Object[] {
          } ;
          Object[] prmH00RD6 ;
          prmH00RD6 = new Object[] {
          } ;
          def= new CursorDef[] {
              new CursorDef("H00RD2", "SELECT [AreaTrabalho_Codigo], [AreaTrabalho_Descricao] FROM [AreaTrabalho] WITH (NOLOCK) ORDER BY [AreaTrabalho_Descricao] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00RD2,0,0,true,false )
             ,new CursorDef("H00RD5", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00RD5,100,0,true,false )
             ,new CursorDef("H00RD6", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00RD6,100,0,true,false )
             ,new CursorDef("H00RD7", cmdBufferH00RD7,false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00RD7,1,0,true,true )
             ,new CursorDef("H00RD8", "SELECT TOP 1 [ContagemResultadoEvidencia_Codigo], [ContagemResultadoEvidencia_NomeArq], [ContagemResultadoEvidencia_TipoArq], [ContagemResultadoEvidencia_Arquivo] FROM [ContagemResultadoEvidencia] WITH (NOLOCK) WHERE [ContagemResultadoEvidencia_Codigo] = @AV9Local_Codigo ORDER BY [ContagemResultadoEvidencia_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00RD8,1,0,true,true )
             ,new CursorDef("H00RD9", "SELECT TOP 1 [Anexo_Codigo], [Anexo_NomeArq], [Anexo_TipoArq], [Anexo_Arquivo] FROM [Anexos] WITH (NOLOCK) WHERE [Anexo_Codigo] = @AV9Local_Codigo ORDER BY [Anexo_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00RD9,1,0,true,true )
             ,new CursorDef("H00RD10", "SELECT TOP 1 [ParametrosPln_Codigo], [ParametrosPln_Campo], [ParametrosPln_AreaTrabalhoCod], [ParametrosPln_Aba], [ParametrosPln_Linha], [ParametrosPln_Coluna] FROM [ParametrosPlanilhas] WITH (NOLOCK) WHERE ([ParametrosPln_AreaTrabalhoCod] = @AV25AreaTrabalho_Codigo) AND ([ParametrosPln_Campo] = @AV24Campo) ORDER BY [ParametrosPln_AreaTrabalhoCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00RD10,1,0,false,true )
             ,new CursorDef("H00RD11", "SELECT TOP 1 [ParametrosPln_Codigo], [ParametrosPln_Campo], [ParametrosPln_AreaTrabalhoCod], [ParametrosPln_Aba], [ParametrosPln_Linha], [ParametrosPln_Coluna] FROM [ParametrosPlanilhas] WITH (NOLOCK) WHERE ([ParametrosPln_AreaTrabalhoCod] IS NULL or ([ParametrosPln_AreaTrabalhoCod] = convert(int, 0))) AND ([ParametrosPln_Campo] = @AV24Campo) ORDER BY [ParametrosPln_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00RD11,1,0,false,true )
             ,new CursorDef("H00RD12", "SELECT [AreaTrabalho_Codigo], [AreaTrabalho_Descricao] FROM [AreaTrabalho] WITH (NOLOCK) ORDER BY [AreaTrabalho_Descricao] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00RD12,0,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                ((int[]) buf[10])[0] = rslt.getInt(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((int[]) buf[12])[0] = rslt.getInt(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((int[]) buf[14])[0] = rslt.getInt(9) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((int[]) buf[16])[0] = rslt.getInt(10) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((int[]) buf[18])[0] = rslt.getInt(11) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(11);
                ((int[]) buf[20])[0] = rslt.getInt(12) ;
                ((String[]) buf[21])[0] = rslt.getString(13, 10) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(13);
                ((int[]) buf[23])[0] = rslt.getInt(14) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(14);
                ((int[]) buf[25])[0] = rslt.getInt(15) ;
                ((String[]) buf[26])[0] = rslt.getString(16, 50) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(16);
                ((String[]) buf[28])[0] = rslt.getVarchar(17) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(17);
                ((int[]) buf[30])[0] = rslt.getInt(18) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(18);
                ((String[]) buf[32])[0] = rslt.getString(19, 100) ;
                ((bool[]) buf[33])[0] = rslt.wasNull(19);
                ((String[]) buf[34])[0] = rslt.getString(20, 20) ;
                ((String[]) buf[35])[0] = rslt.getVarchar(21) ;
                ((bool[]) buf[36])[0] = rslt.wasNull(21);
                ((String[]) buf[37])[0] = rslt.getString(22, 10) ;
                ((bool[]) buf[38])[0] = rslt.wasNull(22);
                ((String[]) buf[39])[0] = rslt.getString(23, 2) ;
                ((bool[]) buf[40])[0] = rslt.wasNull(23);
                ((String[]) buf[41])[0] = rslt.getString(24, 50) ;
                ((bool[]) buf[42])[0] = rslt.wasNull(24);
                ((String[]) buf[43])[0] = rslt.getVarchar(25) ;
                ((bool[]) buf[44])[0] = rslt.wasNull(25);
                ((DateTime[]) buf[45])[0] = rslt.getGXDate(26) ;
                ((bool[]) buf[46])[0] = rslt.wasNull(26);
                ((String[]) buf[47])[0] = rslt.getString(27, 100) ;
                ((bool[]) buf[48])[0] = rslt.wasNull(27);
                ((String[]) buf[49])[0] = rslt.getVarchar(28) ;
                ((bool[]) buf[50])[0] = rslt.wasNull(28);
                ((String[]) buf[51])[0] = rslt.getVarchar(29) ;
                ((bool[]) buf[52])[0] = rslt.wasNull(29);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 10) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((String[]) buf[5])[0] = rslt.getString(5, 50) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                return;
             case 3 :
                ((String[]) buf[0])[0] = rslt.getString(1, 5) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((int[]) buf[11])[0] = rslt.getInt(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((int[]) buf[13])[0] = rslt.getInt(8) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((int[]) buf[15])[0] = rslt.getInt(9) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                ((int[]) buf[17])[0] = rslt.getInt(10) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(10);
                ((int[]) buf[19])[0] = rslt.getInt(11) ;
                ((int[]) buf[20])[0] = rslt.getInt(12) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(12);
                ((int[]) buf[22])[0] = rslt.getInt(13) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(13);
                ((int[]) buf[24])[0] = rslt.getInt(14) ;
                ((int[]) buf[25])[0] = rslt.getInt(15) ;
                ((bool[]) buf[26])[0] = rslt.getBool(16) ;
                ((String[]) buf[27])[0] = rslt.getVarchar(17) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(17);
                ((int[]) buf[29])[0] = rslt.getInt(18) ;
                ((bool[]) buf[30])[0] = rslt.wasNull(18);
                ((DateTime[]) buf[31])[0] = rslt.getGXDate(19) ;
                ((String[]) buf[32])[0] = rslt.getString(20, 100) ;
                ((bool[]) buf[33])[0] = rslt.wasNull(20);
                ((String[]) buf[34])[0] = rslt.getString(21, 100) ;
                ((bool[]) buf[35])[0] = rslt.wasNull(21);
                ((String[]) buf[36])[0] = rslt.getString(22, 20) ;
                ((String[]) buf[37])[0] = rslt.getVarchar(23) ;
                ((bool[]) buf[38])[0] = rslt.wasNull(23);
                ((String[]) buf[39])[0] = rslt.getString(24, 10) ;
                ((bool[]) buf[40])[0] = rslt.wasNull(24);
                ((String[]) buf[41])[0] = rslt.getString(25, 2) ;
                ((bool[]) buf[42])[0] = rslt.wasNull(25);
                ((String[]) buf[43])[0] = rslt.getString(26, 50) ;
                ((bool[]) buf[44])[0] = rslt.wasNull(26);
                ((String[]) buf[45])[0] = rslt.getVarchar(27) ;
                ((bool[]) buf[46])[0] = rslt.wasNull(27);
                ((String[]) buf[47])[0] = rslt.getVarchar(28) ;
                ((bool[]) buf[48])[0] = rslt.wasNull(28);
                ((String[]) buf[49])[0] = rslt.getVarchar(29) ;
                ((bool[]) buf[50])[0] = rslt.wasNull(29);
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 10) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getBLOBFile(4, rslt.getString(3, 10), rslt.getString(2, 50)) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 10) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getBLOBFile(4, rslt.getString(3, 10), rslt.getString(2, 50)) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getString(4, 30) ;
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                ((String[]) buf[6])[0] = rslt.getString(6, 2) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getString(4, 30) ;
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                ((String[]) buf[6])[0] = rslt.getString(6, 2) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                return;
             case 7 :
                stmt.SetParameter(1, (String)parms[0]);
                return;
       }
    }

 }

}
