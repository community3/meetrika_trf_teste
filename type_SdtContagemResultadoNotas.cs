/*
               File: type_SdtContagemResultadoNotas
        Description: Nota da OS
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:17:37.64
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "ContagemResultadoNotas" )]
   [XmlType(TypeName =  "ContagemResultadoNotas" , Namespace = "GxEv3Up14_MeetrikaVs3" )]
   [Serializable]
   public class SdtContagemResultadoNotas : GxSilentTrnSdt, System.Web.SessionState.IRequiresSessionState
   {
      public SdtContagemResultadoNotas( )
      {
         /* Constructor for serialization */
         gxTv_SdtContagemResultadoNotas_Contagemresultadonota_datahora = (DateTime)(DateTime.MinValue);
         gxTv_SdtContagemResultadoNotas_Contagemresultadonota_nota = "";
         gxTv_SdtContagemResultadoNotas_Contagemresultadonota_usuariopesnom = "";
         gxTv_SdtContagemResultadoNotas_Mode = "";
         gxTv_SdtContagemResultadoNotas_Contagemresultadonota_datahora_Z = (DateTime)(DateTime.MinValue);
         gxTv_SdtContagemResultadoNotas_Contagemresultadonota_usuariopesnom_Z = "";
      }

      public SdtContagemResultadoNotas( IGxContext context )
      {
         this.context = context;
         constructorCallingAssembly = Assembly.GetCallingAssembly();
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public void Load( int AV1331ContagemResultadoNota_Codigo )
      {
         IGxSilentTrn obj ;
         obj = getTransaction();
         obj.LoadKey(new Object[] {(int)AV1331ContagemResultadoNota_Codigo});
         return  ;
      }

      public override Object[][] GetBCKey( )
      {
         return (Object[][])(new Object[][]{new Object[]{"ContagemResultadoNota_Codigo", typeof(int)}}) ;
      }

      public override IGxCollection GetMessages( )
      {
         short item = 1 ;
         IGxCollection msgs = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs") ;
         SdtMessages_Message m1 ;
         IGxSilentTrn trn = getTransaction() ;
         msglist msgList = trn.GetMessages() ;
         while ( item <= msgList.ItemCount )
         {
            m1 = new SdtMessages_Message(context);
            m1.gxTpr_Id = msgList.getItemValue(item);
            m1.gxTpr_Description = msgList.getItemText(item);
            m1.gxTpr_Type = msgList.getItemType(item);
            msgs.Add(m1, 0);
            item = (short)(item+1);
         }
         return msgs ;
      }

      public override GXProperties GetMetadata( )
      {
         GXProperties metadata = new GXProperties() ;
         metadata.Set("Name", "ContagemResultadoNotas");
         metadata.Set("BT", "ContagemResultadoNotas");
         metadata.Set("PK", "[ \"ContagemResultadoNota_Codigo\" ]");
         metadata.Set("PKAssigned", "[ \"ContagemResultadoNota_Codigo\" ]");
         metadata.Set("FKList", "[ { \"FK\":[ \"ContagemResultado_Codigo\" ],\"FKMap\":[ \"ContagemResultadoNota_DemandaCod-ContagemResultado_Codigo\" ] },{ \"FK\":[ \"Usuario_Codigo\" ],\"FKMap\":[ \"ContagemResultadoNota_UsuarioCod-Usuario_Codigo\" ] } ]");
         metadata.Set("AllowInsert", "True");
         metadata.Set("AllowUpdate", "True");
         metadata.Set("AllowDelete", "True");
         return metadata ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         if ( ! includeState )
         {
            XmlAttributeOverrides ov = new XmlAttributeOverrides();
            XmlAttributes attrs = new XmlAttributes();
            attrs.XmlIgnore = true;
            ov.Add(this.GetType(),  "gxTpr_Mode" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Initialized" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultadonota_codigo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultadonota_datahora_Z_Nullable" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultadonota_demandacod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultadonota_usuariocod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultadonota_usuariopescod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultadonota_usuariopesnom_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultadonotas_ativo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultadonota_usuariopescod_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultadonota_usuariopesnom_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultadonotas_ativo_N" , attrs);
            xmls = new XmlSerializer(this.GetType(), ov, new Type[0], null, sNameSpace);
         }
         else
         {
            xmls = new XmlSerializer(this.GetType(), sNameSpace);
         }
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtContagemResultadoNotas deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_MeetrikaVs3" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtContagemResultadoNotas)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtContagemResultadoNotas obj ;
         obj = this;
         obj.gxTpr_Contagemresultadonota_codigo = deserialized.gxTpr_Contagemresultadonota_codigo;
         obj.gxTpr_Contagemresultadonota_datahora = deserialized.gxTpr_Contagemresultadonota_datahora;
         obj.gxTpr_Contagemresultadonota_nota = deserialized.gxTpr_Contagemresultadonota_nota;
         obj.gxTpr_Contagemresultadonota_demandacod = deserialized.gxTpr_Contagemresultadonota_demandacod;
         obj.gxTpr_Contagemresultadonota_usuariocod = deserialized.gxTpr_Contagemresultadonota_usuariocod;
         obj.gxTpr_Contagemresultadonota_usuariopescod = deserialized.gxTpr_Contagemresultadonota_usuariopescod;
         obj.gxTpr_Contagemresultadonota_usuariopesnom = deserialized.gxTpr_Contagemresultadonota_usuariopesnom;
         obj.gxTpr_Contagemresultadonotas_ativo = deserialized.gxTpr_Contagemresultadonotas_ativo;
         obj.gxTpr_Mode = deserialized.gxTpr_Mode;
         obj.gxTpr_Initialized = deserialized.gxTpr_Initialized;
         obj.gxTpr_Contagemresultadonota_codigo_Z = deserialized.gxTpr_Contagemresultadonota_codigo_Z;
         obj.gxTpr_Contagemresultadonota_datahora_Z = deserialized.gxTpr_Contagemresultadonota_datahora_Z;
         obj.gxTpr_Contagemresultadonota_demandacod_Z = deserialized.gxTpr_Contagemresultadonota_demandacod_Z;
         obj.gxTpr_Contagemresultadonota_usuariocod_Z = deserialized.gxTpr_Contagemresultadonota_usuariocod_Z;
         obj.gxTpr_Contagemresultadonota_usuariopescod_Z = deserialized.gxTpr_Contagemresultadonota_usuariopescod_Z;
         obj.gxTpr_Contagemresultadonota_usuariopesnom_Z = deserialized.gxTpr_Contagemresultadonota_usuariopesnom_Z;
         obj.gxTpr_Contagemresultadonotas_ativo_Z = deserialized.gxTpr_Contagemresultadonotas_ativo_Z;
         obj.gxTpr_Contagemresultadonota_usuariopescod_N = deserialized.gxTpr_Contagemresultadonota_usuariopescod_N;
         obj.gxTpr_Contagemresultadonota_usuariopesnom_N = deserialized.gxTpr_Contagemresultadonota_usuariopesnom_N;
         obj.gxTpr_Contagemresultadonotas_ativo_N = deserialized.gxTpr_Contagemresultadonotas_ativo_N;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoNota_Codigo") )
               {
                  gxTv_SdtContagemResultadoNotas_Contagemresultadonota_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoNota_DataHora") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00T00:00:00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtContagemResultadoNotas_Contagemresultadonota_datahora = (DateTime)(DateTime.MinValue);
                  }
                  else
                  {
                     gxTv_SdtContagemResultadoNotas_Contagemresultadonota_datahora = context.localUtil.YMDHMSToT( (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 12, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 15, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 18, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoNota_Nota") )
               {
                  gxTv_SdtContagemResultadoNotas_Contagemresultadonota_nota = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoNota_DemandaCod") )
               {
                  gxTv_SdtContagemResultadoNotas_Contagemresultadonota_demandacod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoNota_UsuarioCod") )
               {
                  gxTv_SdtContagemResultadoNotas_Contagemresultadonota_usuariocod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoNota_UsuarioPesCod") )
               {
                  gxTv_SdtContagemResultadoNotas_Contagemresultadonota_usuariopescod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoNota_UsuarioPesNom") )
               {
                  gxTv_SdtContagemResultadoNotas_Contagemresultadonota_usuariopesnom = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoNotas_Ativo") )
               {
                  gxTv_SdtContagemResultadoNotas_Contagemresultadonotas_ativo = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Mode") )
               {
                  gxTv_SdtContagemResultadoNotas_Mode = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Initialized") )
               {
                  gxTv_SdtContagemResultadoNotas_Initialized = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoNota_Codigo_Z") )
               {
                  gxTv_SdtContagemResultadoNotas_Contagemresultadonota_codigo_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoNota_DataHora_Z") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00T00:00:00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtContagemResultadoNotas_Contagemresultadonota_datahora_Z = (DateTime)(DateTime.MinValue);
                  }
                  else
                  {
                     gxTv_SdtContagemResultadoNotas_Contagemresultadonota_datahora_Z = context.localUtil.YMDHMSToT( (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 12, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 15, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 18, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoNota_DemandaCod_Z") )
               {
                  gxTv_SdtContagemResultadoNotas_Contagemresultadonota_demandacod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoNota_UsuarioCod_Z") )
               {
                  gxTv_SdtContagemResultadoNotas_Contagemresultadonota_usuariocod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoNota_UsuarioPesCod_Z") )
               {
                  gxTv_SdtContagemResultadoNotas_Contagemresultadonota_usuariopescod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoNota_UsuarioPesNom_Z") )
               {
                  gxTv_SdtContagemResultadoNotas_Contagemresultadonota_usuariopesnom_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoNotas_Ativo_Z") )
               {
                  gxTv_SdtContagemResultadoNotas_Contagemresultadonotas_ativo_Z = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoNota_UsuarioPesCod_N") )
               {
                  gxTv_SdtContagemResultadoNotas_Contagemresultadonota_usuariopescod_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoNota_UsuarioPesNom_N") )
               {
                  gxTv_SdtContagemResultadoNotas_Contagemresultadonota_usuariopesnom_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoNotas_Ativo_N") )
               {
                  gxTv_SdtContagemResultadoNotas_Contagemresultadonotas_ativo_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "ContagemResultadoNotas";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "GxEv3Up14_MeetrikaVs3";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("ContagemResultadoNota_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoNotas_Contagemresultadonota_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         if ( (DateTime.MinValue==gxTv_SdtContagemResultadoNotas_Contagemresultadonota_datahora) )
         {
            oWriter.WriteStartElement("ContagemResultadoNota_DataHora");
            oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            oWriter.WriteAttribute("xsi:nil", "true");
            oWriter.WriteEndElement();
         }
         else
         {
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtContagemResultadoNotas_Contagemresultadonota_datahora)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtContagemResultadoNotas_Contagemresultadonota_datahora)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtContagemResultadoNotas_Contagemresultadonota_datahora)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "T";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( gxTv_SdtContagemResultadoNotas_Contagemresultadonota_datahora)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( gxTv_SdtContagemResultadoNotas_Contagemresultadonota_datahora)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( gxTv_SdtContagemResultadoNotas_Contagemresultadonota_datahora)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            oWriter.WriteElement("ContagemResultadoNota_DataHora", sDateCnv);
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
         }
         oWriter.WriteElement("ContagemResultadoNota_Nota", StringUtil.RTrim( gxTv_SdtContagemResultadoNotas_Contagemresultadonota_nota));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContagemResultadoNota_DemandaCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoNotas_Contagemresultadonota_demandacod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContagemResultadoNota_UsuarioCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoNotas_Contagemresultadonota_usuariocod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContagemResultadoNota_UsuarioPesCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoNotas_Contagemresultadonota_usuariopescod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContagemResultadoNota_UsuarioPesNom", StringUtil.RTrim( gxTv_SdtContagemResultadoNotas_Contagemresultadonota_usuariopesnom));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContagemResultadoNotas_Ativo", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtContagemResultadoNotas_Contagemresultadonotas_ativo)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         if ( sIncludeState )
         {
            oWriter.WriteElement("Mode", StringUtil.RTrim( gxTv_SdtContagemResultadoNotas_Mode));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Initialized", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoNotas_Initialized), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultadoNota_Codigo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoNotas_Contagemresultadonota_codigo_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            if ( (DateTime.MinValue==gxTv_SdtContagemResultadoNotas_Contagemresultadonota_datahora_Z) )
            {
               oWriter.WriteStartElement("ContagemResultadoNota_DataHora_Z");
               oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
               oWriter.WriteAttribute("xsi:nil", "true");
               oWriter.WriteEndElement();
            }
            else
            {
               sDateCnv = "";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtContagemResultadoNotas_Contagemresultadonota_datahora_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "-";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtContagemResultadoNotas_Contagemresultadonota_datahora_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "-";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtContagemResultadoNotas_Contagemresultadonota_datahora_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "T";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( gxTv_SdtContagemResultadoNotas_Contagemresultadonota_datahora_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + ":";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( gxTv_SdtContagemResultadoNotas_Contagemresultadonota_datahora_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + ":";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( gxTv_SdtContagemResultadoNotas_Contagemresultadonota_datahora_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               oWriter.WriteElement("ContagemResultadoNota_DataHora_Z", sDateCnv);
               if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
               {
                  oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
               }
            }
            oWriter.WriteElement("ContagemResultadoNota_DemandaCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoNotas_Contagemresultadonota_demandacod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultadoNota_UsuarioCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoNotas_Contagemresultadonota_usuariocod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultadoNota_UsuarioPesCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoNotas_Contagemresultadonota_usuariopescod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultadoNota_UsuarioPesNom_Z", StringUtil.RTrim( gxTv_SdtContagemResultadoNotas_Contagemresultadonota_usuariopesnom_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultadoNotas_Ativo_Z", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtContagemResultadoNotas_Contagemresultadonotas_ativo_Z)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultadoNota_UsuarioPesCod_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoNotas_Contagemresultadonota_usuariopescod_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultadoNota_UsuarioPesNom_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoNotas_Contagemresultadonota_usuariopesnom_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultadoNotas_Ativo_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoNotas_Contagemresultadonotas_ativo_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("ContagemResultadoNota_Codigo", gxTv_SdtContagemResultadoNotas_Contagemresultadonota_codigo, false);
         datetime_STZ = gxTv_SdtContagemResultadoNotas_Contagemresultadonota_datahora;
         sDateCnv = "";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "T";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + ":";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + ":";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         AddObjectProperty("ContagemResultadoNota_DataHora", sDateCnv, false);
         AddObjectProperty("ContagemResultadoNota_Nota", gxTv_SdtContagemResultadoNotas_Contagemresultadonota_nota, false);
         AddObjectProperty("ContagemResultadoNota_DemandaCod", gxTv_SdtContagemResultadoNotas_Contagemresultadonota_demandacod, false);
         AddObjectProperty("ContagemResultadoNota_UsuarioCod", gxTv_SdtContagemResultadoNotas_Contagemresultadonota_usuariocod, false);
         AddObjectProperty("ContagemResultadoNota_UsuarioPesCod", gxTv_SdtContagemResultadoNotas_Contagemresultadonota_usuariopescod, false);
         AddObjectProperty("ContagemResultadoNota_UsuarioPesNom", gxTv_SdtContagemResultadoNotas_Contagemresultadonota_usuariopesnom, false);
         AddObjectProperty("ContagemResultadoNotas_Ativo", gxTv_SdtContagemResultadoNotas_Contagemresultadonotas_ativo, false);
         if ( includeState )
         {
            AddObjectProperty("Mode", gxTv_SdtContagemResultadoNotas_Mode, false);
            AddObjectProperty("Initialized", gxTv_SdtContagemResultadoNotas_Initialized, false);
            AddObjectProperty("ContagemResultadoNota_Codigo_Z", gxTv_SdtContagemResultadoNotas_Contagemresultadonota_codigo_Z, false);
            datetime_STZ = gxTv_SdtContagemResultadoNotas_Contagemresultadonota_datahora_Z;
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "T";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            AddObjectProperty("ContagemResultadoNota_DataHora_Z", sDateCnv, false);
            AddObjectProperty("ContagemResultadoNota_DemandaCod_Z", gxTv_SdtContagemResultadoNotas_Contagemresultadonota_demandacod_Z, false);
            AddObjectProperty("ContagemResultadoNota_UsuarioCod_Z", gxTv_SdtContagemResultadoNotas_Contagemresultadonota_usuariocod_Z, false);
            AddObjectProperty("ContagemResultadoNota_UsuarioPesCod_Z", gxTv_SdtContagemResultadoNotas_Contagemresultadonota_usuariopescod_Z, false);
            AddObjectProperty("ContagemResultadoNota_UsuarioPesNom_Z", gxTv_SdtContagemResultadoNotas_Contagemresultadonota_usuariopesnom_Z, false);
            AddObjectProperty("ContagemResultadoNotas_Ativo_Z", gxTv_SdtContagemResultadoNotas_Contagemresultadonotas_ativo_Z, false);
            AddObjectProperty("ContagemResultadoNota_UsuarioPesCod_N", gxTv_SdtContagemResultadoNotas_Contagemresultadonota_usuariopescod_N, false);
            AddObjectProperty("ContagemResultadoNota_UsuarioPesNom_N", gxTv_SdtContagemResultadoNotas_Contagemresultadonota_usuariopesnom_N, false);
            AddObjectProperty("ContagemResultadoNotas_Ativo_N", gxTv_SdtContagemResultadoNotas_Contagemresultadonotas_ativo_N, false);
         }
         return  ;
      }

      [  SoapElement( ElementName = "ContagemResultadoNota_Codigo" )]
      [  XmlElement( ElementName = "ContagemResultadoNota_Codigo"   )]
      public int gxTpr_Contagemresultadonota_codigo
      {
         get {
            return gxTv_SdtContagemResultadoNotas_Contagemresultadonota_codigo ;
         }

         set {
            if ( gxTv_SdtContagemResultadoNotas_Contagemresultadonota_codigo != value )
            {
               gxTv_SdtContagemResultadoNotas_Mode = "INS";
               this.gxTv_SdtContagemResultadoNotas_Contagemresultadonota_codigo_Z_SetNull( );
               this.gxTv_SdtContagemResultadoNotas_Contagemresultadonota_datahora_Z_SetNull( );
               this.gxTv_SdtContagemResultadoNotas_Contagemresultadonota_demandacod_Z_SetNull( );
               this.gxTv_SdtContagemResultadoNotas_Contagemresultadonota_usuariocod_Z_SetNull( );
               this.gxTv_SdtContagemResultadoNotas_Contagemresultadonota_usuariopescod_Z_SetNull( );
               this.gxTv_SdtContagemResultadoNotas_Contagemresultadonota_usuariopesnom_Z_SetNull( );
               this.gxTv_SdtContagemResultadoNotas_Contagemresultadonotas_ativo_Z_SetNull( );
            }
            gxTv_SdtContagemResultadoNotas_Contagemresultadonota_codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultadoNota_DataHora" )]
      [  XmlElement( ElementName = "ContagemResultadoNota_DataHora"  , IsNullable=true )]
      public string gxTpr_Contagemresultadonota_datahora_Nullable
      {
         get {
            if ( gxTv_SdtContagemResultadoNotas_Contagemresultadonota_datahora == DateTime.MinValue)
               return null;
            return new GxDatetimeString(gxTv_SdtContagemResultadoNotas_Contagemresultadonota_datahora).value ;
         }

         set {
            if (value == null || value == GxDatetimeString.NullValue )
               gxTv_SdtContagemResultadoNotas_Contagemresultadonota_datahora = DateTime.MinValue;
            else
               gxTv_SdtContagemResultadoNotas_Contagemresultadonota_datahora = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Contagemresultadonota_datahora
      {
         get {
            return gxTv_SdtContagemResultadoNotas_Contagemresultadonota_datahora ;
         }

         set {
            gxTv_SdtContagemResultadoNotas_Contagemresultadonota_datahora = (DateTime)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultadoNota_Nota" )]
      [  XmlElement( ElementName = "ContagemResultadoNota_Nota"   )]
      public String gxTpr_Contagemresultadonota_nota
      {
         get {
            return gxTv_SdtContagemResultadoNotas_Contagemresultadonota_nota ;
         }

         set {
            gxTv_SdtContagemResultadoNotas_Contagemresultadonota_nota = (String)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultadoNota_DemandaCod" )]
      [  XmlElement( ElementName = "ContagemResultadoNota_DemandaCod"   )]
      public int gxTpr_Contagemresultadonota_demandacod
      {
         get {
            return gxTv_SdtContagemResultadoNotas_Contagemresultadonota_demandacod ;
         }

         set {
            gxTv_SdtContagemResultadoNotas_Contagemresultadonota_demandacod = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultadoNota_UsuarioCod" )]
      [  XmlElement( ElementName = "ContagemResultadoNota_UsuarioCod"   )]
      public int gxTpr_Contagemresultadonota_usuariocod
      {
         get {
            return gxTv_SdtContagemResultadoNotas_Contagemresultadonota_usuariocod ;
         }

         set {
            gxTv_SdtContagemResultadoNotas_Contagemresultadonota_usuariocod = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultadoNota_UsuarioPesCod" )]
      [  XmlElement( ElementName = "ContagemResultadoNota_UsuarioPesCod"   )]
      public int gxTpr_Contagemresultadonota_usuariopescod
      {
         get {
            return gxTv_SdtContagemResultadoNotas_Contagemresultadonota_usuariopescod ;
         }

         set {
            gxTv_SdtContagemResultadoNotas_Contagemresultadonota_usuariopescod_N = 0;
            gxTv_SdtContagemResultadoNotas_Contagemresultadonota_usuariopescod = (int)(value);
         }

      }

      public void gxTv_SdtContagemResultadoNotas_Contagemresultadonota_usuariopescod_SetNull( )
      {
         gxTv_SdtContagemResultadoNotas_Contagemresultadonota_usuariopescod_N = 1;
         gxTv_SdtContagemResultadoNotas_Contagemresultadonota_usuariopescod = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoNotas_Contagemresultadonota_usuariopescod_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoNota_UsuarioPesNom" )]
      [  XmlElement( ElementName = "ContagemResultadoNota_UsuarioPesNom"   )]
      public String gxTpr_Contagemresultadonota_usuariopesnom
      {
         get {
            return gxTv_SdtContagemResultadoNotas_Contagemresultadonota_usuariopesnom ;
         }

         set {
            gxTv_SdtContagemResultadoNotas_Contagemresultadonota_usuariopesnom_N = 0;
            gxTv_SdtContagemResultadoNotas_Contagemresultadonota_usuariopesnom = (String)(value);
         }

      }

      public void gxTv_SdtContagemResultadoNotas_Contagemresultadonota_usuariopesnom_SetNull( )
      {
         gxTv_SdtContagemResultadoNotas_Contagemresultadonota_usuariopesnom_N = 1;
         gxTv_SdtContagemResultadoNotas_Contagemresultadonota_usuariopesnom = "";
         return  ;
      }

      public bool gxTv_SdtContagemResultadoNotas_Contagemresultadonota_usuariopesnom_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoNotas_Ativo" )]
      [  XmlElement( ElementName = "ContagemResultadoNotas_Ativo"   )]
      public bool gxTpr_Contagemresultadonotas_ativo
      {
         get {
            return gxTv_SdtContagemResultadoNotas_Contagemresultadonotas_ativo ;
         }

         set {
            gxTv_SdtContagemResultadoNotas_Contagemresultadonotas_ativo_N = 0;
            gxTv_SdtContagemResultadoNotas_Contagemresultadonotas_ativo = value;
         }

      }

      public void gxTv_SdtContagemResultadoNotas_Contagemresultadonotas_ativo_SetNull( )
      {
         gxTv_SdtContagemResultadoNotas_Contagemresultadonotas_ativo_N = 1;
         gxTv_SdtContagemResultadoNotas_Contagemresultadonotas_ativo = false;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoNotas_Contagemresultadonotas_ativo_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Mode" )]
      [  XmlElement( ElementName = "Mode"   )]
      public String gxTpr_Mode
      {
         get {
            return gxTv_SdtContagemResultadoNotas_Mode ;
         }

         set {
            gxTv_SdtContagemResultadoNotas_Mode = (String)(value);
         }

      }

      public void gxTv_SdtContagemResultadoNotas_Mode_SetNull( )
      {
         gxTv_SdtContagemResultadoNotas_Mode = "";
         return  ;
      }

      public bool gxTv_SdtContagemResultadoNotas_Mode_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Initialized" )]
      [  XmlElement( ElementName = "Initialized"   )]
      public short gxTpr_Initialized
      {
         get {
            return gxTv_SdtContagemResultadoNotas_Initialized ;
         }

         set {
            gxTv_SdtContagemResultadoNotas_Initialized = (short)(value);
         }

      }

      public void gxTv_SdtContagemResultadoNotas_Initialized_SetNull( )
      {
         gxTv_SdtContagemResultadoNotas_Initialized = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoNotas_Initialized_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoNota_Codigo_Z" )]
      [  XmlElement( ElementName = "ContagemResultadoNota_Codigo_Z"   )]
      public int gxTpr_Contagemresultadonota_codigo_Z
      {
         get {
            return gxTv_SdtContagemResultadoNotas_Contagemresultadonota_codigo_Z ;
         }

         set {
            gxTv_SdtContagemResultadoNotas_Contagemresultadonota_codigo_Z = (int)(value);
         }

      }

      public void gxTv_SdtContagemResultadoNotas_Contagemresultadonota_codigo_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoNotas_Contagemresultadonota_codigo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoNotas_Contagemresultadonota_codigo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoNota_DataHora_Z" )]
      [  XmlElement( ElementName = "ContagemResultadoNota_DataHora_Z"  , IsNullable=true )]
      public string gxTpr_Contagemresultadonota_datahora_Z_Nullable
      {
         get {
            if ( gxTv_SdtContagemResultadoNotas_Contagemresultadonota_datahora_Z == DateTime.MinValue)
               return null;
            return new GxDatetimeString(gxTv_SdtContagemResultadoNotas_Contagemresultadonota_datahora_Z).value ;
         }

         set {
            if (value == null || value == GxDatetimeString.NullValue )
               gxTv_SdtContagemResultadoNotas_Contagemresultadonota_datahora_Z = DateTime.MinValue;
            else
               gxTv_SdtContagemResultadoNotas_Contagemresultadonota_datahora_Z = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Contagemresultadonota_datahora_Z
      {
         get {
            return gxTv_SdtContagemResultadoNotas_Contagemresultadonota_datahora_Z ;
         }

         set {
            gxTv_SdtContagemResultadoNotas_Contagemresultadonota_datahora_Z = (DateTime)(value);
         }

      }

      public void gxTv_SdtContagemResultadoNotas_Contagemresultadonota_datahora_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoNotas_Contagemresultadonota_datahora_Z = (DateTime)(DateTime.MinValue);
         return  ;
      }

      public bool gxTv_SdtContagemResultadoNotas_Contagemresultadonota_datahora_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoNota_DemandaCod_Z" )]
      [  XmlElement( ElementName = "ContagemResultadoNota_DemandaCod_Z"   )]
      public int gxTpr_Contagemresultadonota_demandacod_Z
      {
         get {
            return gxTv_SdtContagemResultadoNotas_Contagemresultadonota_demandacod_Z ;
         }

         set {
            gxTv_SdtContagemResultadoNotas_Contagemresultadonota_demandacod_Z = (int)(value);
         }

      }

      public void gxTv_SdtContagemResultadoNotas_Contagemresultadonota_demandacod_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoNotas_Contagemresultadonota_demandacod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoNotas_Contagemresultadonota_demandacod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoNota_UsuarioCod_Z" )]
      [  XmlElement( ElementName = "ContagemResultadoNota_UsuarioCod_Z"   )]
      public int gxTpr_Contagemresultadonota_usuariocod_Z
      {
         get {
            return gxTv_SdtContagemResultadoNotas_Contagemresultadonota_usuariocod_Z ;
         }

         set {
            gxTv_SdtContagemResultadoNotas_Contagemresultadonota_usuariocod_Z = (int)(value);
         }

      }

      public void gxTv_SdtContagemResultadoNotas_Contagemresultadonota_usuariocod_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoNotas_Contagemresultadonota_usuariocod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoNotas_Contagemresultadonota_usuariocod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoNota_UsuarioPesCod_Z" )]
      [  XmlElement( ElementName = "ContagemResultadoNota_UsuarioPesCod_Z"   )]
      public int gxTpr_Contagemresultadonota_usuariopescod_Z
      {
         get {
            return gxTv_SdtContagemResultadoNotas_Contagemresultadonota_usuariopescod_Z ;
         }

         set {
            gxTv_SdtContagemResultadoNotas_Contagemresultadonota_usuariopescod_Z = (int)(value);
         }

      }

      public void gxTv_SdtContagemResultadoNotas_Contagemresultadonota_usuariopescod_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoNotas_Contagemresultadonota_usuariopescod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoNotas_Contagemresultadonota_usuariopescod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoNota_UsuarioPesNom_Z" )]
      [  XmlElement( ElementName = "ContagemResultadoNota_UsuarioPesNom_Z"   )]
      public String gxTpr_Contagemresultadonota_usuariopesnom_Z
      {
         get {
            return gxTv_SdtContagemResultadoNotas_Contagemresultadonota_usuariopesnom_Z ;
         }

         set {
            gxTv_SdtContagemResultadoNotas_Contagemresultadonota_usuariopesnom_Z = (String)(value);
         }

      }

      public void gxTv_SdtContagemResultadoNotas_Contagemresultadonota_usuariopesnom_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoNotas_Contagemresultadonota_usuariopesnom_Z = "";
         return  ;
      }

      public bool gxTv_SdtContagemResultadoNotas_Contagemresultadonota_usuariopesnom_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoNotas_Ativo_Z" )]
      [  XmlElement( ElementName = "ContagemResultadoNotas_Ativo_Z"   )]
      public bool gxTpr_Contagemresultadonotas_ativo_Z
      {
         get {
            return gxTv_SdtContagemResultadoNotas_Contagemresultadonotas_ativo_Z ;
         }

         set {
            gxTv_SdtContagemResultadoNotas_Contagemresultadonotas_ativo_Z = value;
         }

      }

      public void gxTv_SdtContagemResultadoNotas_Contagemresultadonotas_ativo_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoNotas_Contagemresultadonotas_ativo_Z = false;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoNotas_Contagemresultadonotas_ativo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoNota_UsuarioPesCod_N" )]
      [  XmlElement( ElementName = "ContagemResultadoNota_UsuarioPesCod_N"   )]
      public short gxTpr_Contagemresultadonota_usuariopescod_N
      {
         get {
            return gxTv_SdtContagemResultadoNotas_Contagemresultadonota_usuariopescod_N ;
         }

         set {
            gxTv_SdtContagemResultadoNotas_Contagemresultadonota_usuariopescod_N = (short)(value);
         }

      }

      public void gxTv_SdtContagemResultadoNotas_Contagemresultadonota_usuariopescod_N_SetNull( )
      {
         gxTv_SdtContagemResultadoNotas_Contagemresultadonota_usuariopescod_N = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoNotas_Contagemresultadonota_usuariopescod_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoNota_UsuarioPesNom_N" )]
      [  XmlElement( ElementName = "ContagemResultadoNota_UsuarioPesNom_N"   )]
      public short gxTpr_Contagemresultadonota_usuariopesnom_N
      {
         get {
            return gxTv_SdtContagemResultadoNotas_Contagemresultadonota_usuariopesnom_N ;
         }

         set {
            gxTv_SdtContagemResultadoNotas_Contagemresultadonota_usuariopesnom_N = (short)(value);
         }

      }

      public void gxTv_SdtContagemResultadoNotas_Contagemresultadonota_usuariopesnom_N_SetNull( )
      {
         gxTv_SdtContagemResultadoNotas_Contagemresultadonota_usuariopesnom_N = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoNotas_Contagemresultadonota_usuariopesnom_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoNotas_Ativo_N" )]
      [  XmlElement( ElementName = "ContagemResultadoNotas_Ativo_N"   )]
      public short gxTpr_Contagemresultadonotas_ativo_N
      {
         get {
            return gxTv_SdtContagemResultadoNotas_Contagemresultadonotas_ativo_N ;
         }

         set {
            gxTv_SdtContagemResultadoNotas_Contagemresultadonotas_ativo_N = (short)(value);
         }

      }

      public void gxTv_SdtContagemResultadoNotas_Contagemresultadonotas_ativo_N_SetNull( )
      {
         gxTv_SdtContagemResultadoNotas_Contagemresultadonotas_ativo_N = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoNotas_Contagemresultadonotas_ativo_N_IsNull( )
      {
         return false ;
      }

      public void initialize( )
      {
         gxTv_SdtContagemResultadoNotas_Contagemresultadonota_datahora = (DateTime)(DateTime.MinValue);
         gxTv_SdtContagemResultadoNotas_Contagemresultadonota_nota = "";
         gxTv_SdtContagemResultadoNotas_Contagemresultadonota_usuariopesnom = "";
         gxTv_SdtContagemResultadoNotas_Mode = "";
         gxTv_SdtContagemResultadoNotas_Contagemresultadonota_datahora_Z = (DateTime)(DateTime.MinValue);
         gxTv_SdtContagemResultadoNotas_Contagemresultadonota_usuariopesnom_Z = "";
         gxTv_SdtContagemResultadoNotas_Contagemresultadonotas_ativo = true;
         sTagName = "";
         sDateCnv = "";
         sNumToPad = "";
         datetime_STZ = (DateTime)(DateTime.MinValue);
         IGxSilentTrn obj ;
         obj = (IGxSilentTrn)ClassLoader.FindInstance( "contagemresultadonotas", "GeneXus.Programs.contagemresultadonotas_bc", new Object[] {context}, constructorCallingAssembly);
         obj.initialize();
         obj.SetSDT(this, 1);
         setTransaction( obj) ;
         obj.SetMode("INS");
         return  ;
      }

      private short gxTv_SdtContagemResultadoNotas_Initialized ;
      private short gxTv_SdtContagemResultadoNotas_Contagemresultadonota_usuariopescod_N ;
      private short gxTv_SdtContagemResultadoNotas_Contagemresultadonota_usuariopesnom_N ;
      private short gxTv_SdtContagemResultadoNotas_Contagemresultadonotas_ativo_N ;
      private short readOk ;
      private short nOutParmCount ;
      private int gxTv_SdtContagemResultadoNotas_Contagemresultadonota_codigo ;
      private int gxTv_SdtContagemResultadoNotas_Contagemresultadonota_demandacod ;
      private int gxTv_SdtContagemResultadoNotas_Contagemresultadonota_usuariocod ;
      private int gxTv_SdtContagemResultadoNotas_Contagemresultadonota_usuariopescod ;
      private int gxTv_SdtContagemResultadoNotas_Contagemresultadonota_codigo_Z ;
      private int gxTv_SdtContagemResultadoNotas_Contagemresultadonota_demandacod_Z ;
      private int gxTv_SdtContagemResultadoNotas_Contagemresultadonota_usuariocod_Z ;
      private int gxTv_SdtContagemResultadoNotas_Contagemresultadonota_usuariopescod_Z ;
      private String gxTv_SdtContagemResultadoNotas_Contagemresultadonota_usuariopesnom ;
      private String gxTv_SdtContagemResultadoNotas_Mode ;
      private String gxTv_SdtContagemResultadoNotas_Contagemresultadonota_usuariopesnom_Z ;
      private String sTagName ;
      private String sDateCnv ;
      private String sNumToPad ;
      private DateTime gxTv_SdtContagemResultadoNotas_Contagemresultadonota_datahora ;
      private DateTime gxTv_SdtContagemResultadoNotas_Contagemresultadonota_datahora_Z ;
      private DateTime datetime_STZ ;
      private bool gxTv_SdtContagemResultadoNotas_Contagemresultadonotas_ativo ;
      private bool gxTv_SdtContagemResultadoNotas_Contagemresultadonotas_ativo_Z ;
      private String gxTv_SdtContagemResultadoNotas_Contagemresultadonota_nota ;
      private Assembly constructorCallingAssembly ;
   }

   [DataContract(Name = @"ContagemResultadoNotas", Namespace = "GxEv3Up14_MeetrikaVs3")]
   public class SdtContagemResultadoNotas_RESTInterface : GxGenericCollectionItem<SdtContagemResultadoNotas>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtContagemResultadoNotas_RESTInterface( ) : base()
      {
      }

      public SdtContagemResultadoNotas_RESTInterface( SdtContagemResultadoNotas psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "ContagemResultadoNota_Codigo" , Order = 0 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contagemresultadonota_codigo
      {
         get {
            return sdt.gxTpr_Contagemresultadonota_codigo ;
         }

         set {
            sdt.gxTpr_Contagemresultadonota_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultadoNota_DataHora" , Order = 1 )]
      [GxSeudo()]
      public String gxTpr_Contagemresultadonota_datahora
      {
         get {
            return DateTimeUtil.TToC2( sdt.gxTpr_Contagemresultadonota_datahora) ;
         }

         set {
            sdt.gxTpr_Contagemresultadonota_datahora = DateTimeUtil.CToT2( (String)(value));
         }

      }

      [DataMember( Name = "ContagemResultadoNota_Nota" , Order = 2 )]
      public String gxTpr_Contagemresultadonota_nota
      {
         get {
            return sdt.gxTpr_Contagemresultadonota_nota ;
         }

         set {
            sdt.gxTpr_Contagemresultadonota_nota = (String)(value);
         }

      }

      [DataMember( Name = "ContagemResultadoNota_DemandaCod" , Order = 3 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contagemresultadonota_demandacod
      {
         get {
            return sdt.gxTpr_Contagemresultadonota_demandacod ;
         }

         set {
            sdt.gxTpr_Contagemresultadonota_demandacod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultadoNota_UsuarioCod" , Order = 4 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contagemresultadonota_usuariocod
      {
         get {
            return sdt.gxTpr_Contagemresultadonota_usuariocod ;
         }

         set {
            sdt.gxTpr_Contagemresultadonota_usuariocod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultadoNota_UsuarioPesCod" , Order = 5 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contagemresultadonota_usuariopescod
      {
         get {
            return sdt.gxTpr_Contagemresultadonota_usuariopescod ;
         }

         set {
            sdt.gxTpr_Contagemresultadonota_usuariopescod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultadoNota_UsuarioPesNom" , Order = 6 )]
      [GxSeudo()]
      public String gxTpr_Contagemresultadonota_usuariopesnom
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contagemresultadonota_usuariopesnom) ;
         }

         set {
            sdt.gxTpr_Contagemresultadonota_usuariopesnom = (String)(value);
         }

      }

      [DataMember( Name = "ContagemResultadoNotas_Ativo" , Order = 7 )]
      [GxSeudo()]
      public bool gxTpr_Contagemresultadonotas_ativo
      {
         get {
            return sdt.gxTpr_Contagemresultadonotas_ativo ;
         }

         set {
            sdt.gxTpr_Contagemresultadonotas_ativo = value;
         }

      }

      public SdtContagemResultadoNotas sdt
      {
         get {
            return (SdtContagemResultadoNotas)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtContagemResultadoNotas() ;
         }
      }

      [DataMember( Name = "gx_md5_hash", Order = 20 )]
      public string Hash
      {
         get {
            if ( StringUtil.StrCmp(md5Hash, null) == 0 )
            {
               md5Hash = (String)(getHash());
            }
            return md5Hash ;
         }

         set {
            md5Hash = value ;
         }

      }

      private String md5Hash ;
   }

}
