/*
               File: PdfCertificationLevel
        Description: PdfCertificationLevel
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 21:56:35.9
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class gxdomainpdfcertificationlevel
   {
      private static Hashtable domain = new Hashtable();
      static gxdomainpdfcertificationlevel ()
      {
         domain[(int)0] = "Creates an ordinary signature aka an approval or a recipient signature. A document can be signed for approval by one or more recipients.";
         domain[(int)1] = "Creates a certification signature aka an author signature. After the signature is applied, no changes to the document will be allowed.";
         domain[(int)2] = "Creates a certification signature for the author of the document. Other people can still fill out form fields or add approval signatures without invalidating the signature.";
         domain[(int)3] = "Creates a certification signature. Other people can still fill out form fields- or add approval signatures as well as annotations without invalidating the signature.";
      }

      public static string getDescription( IGxContext context ,
                                           int key )
      {
         return (string)domain[key] ;
      }

      public static GxSimpleCollection getValues( )
      {
         GxSimpleCollection value = new GxSimpleCollection();
         ArrayList aKeys = new ArrayList(domain.Keys);
         aKeys.Sort();
         foreach (int key in aKeys)
         {
            value.Add(key);
         }
         return value;
      }

   }

}
