/*
               File: GetPromptContagemResultadoFilterData
        Description: Get Prompt Contagem Resultado Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 5:20:5.56
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getpromptcontagemresultadofilterdata : GXProcedure
   {
      public getpromptcontagemresultadofilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getpromptcontagemresultadofilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV26DDOName = aP0_DDOName;
         this.AV24SearchTxt = aP1_SearchTxt;
         this.AV25SearchTxtTo = aP2_SearchTxtTo;
         this.AV30OptionsJson = "" ;
         this.AV33OptionsDescJson = "" ;
         this.AV35OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV30OptionsJson;
         aP4_OptionsDescJson=this.AV33OptionsDescJson;
         aP5_OptionIndexesJson=this.AV35OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV26DDOName = aP0_DDOName;
         this.AV24SearchTxt = aP1_SearchTxt;
         this.AV25SearchTxtTo = aP2_SearchTxtTo;
         this.AV30OptionsJson = "" ;
         this.AV33OptionsDescJson = "" ;
         this.AV35OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV30OptionsJson;
         aP4_OptionsDescJson=this.AV33OptionsDescJson;
         aP5_OptionIndexesJson=this.AV35OptionIndexesJson;
         return AV35OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getpromptcontagemresultadofilterdata objgetpromptcontagemresultadofilterdata;
         objgetpromptcontagemresultadofilterdata = new getpromptcontagemresultadofilterdata();
         objgetpromptcontagemresultadofilterdata.AV26DDOName = aP0_DDOName;
         objgetpromptcontagemresultadofilterdata.AV24SearchTxt = aP1_SearchTxt;
         objgetpromptcontagemresultadofilterdata.AV25SearchTxtTo = aP2_SearchTxtTo;
         objgetpromptcontagemresultadofilterdata.AV30OptionsJson = "" ;
         objgetpromptcontagemresultadofilterdata.AV33OptionsDescJson = "" ;
         objgetpromptcontagemresultadofilterdata.AV35OptionIndexesJson = "" ;
         objgetpromptcontagemresultadofilterdata.context.SetSubmitInitialConfig(context);
         objgetpromptcontagemresultadofilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetpromptcontagemresultadofilterdata);
         aP3_OptionsJson=this.AV30OptionsJson;
         aP4_OptionsDescJson=this.AV33OptionsDescJson;
         aP5_OptionIndexesJson=this.AV35OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getpromptcontagemresultadofilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV29Options = (IGxCollection)(new GxSimpleCollection());
         AV32OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV34OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV26DDOName), "DDO_CONTAGEMRESULTADO_DEMANDAFM") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTAGEMRESULTADO_DEMANDAFMOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV26DDOName), "DDO_CONTAGEMRESULTADO_DEMANDA") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTAGEMRESULTADO_DEMANDAOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV26DDOName), "DDO_CONTAGEMRESULTADO_CONTRATADAPESSOANOM") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTAGEMRESULTADO_CONTRATADAPESSOANOMOPTIONS' */
            S141 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV26DDOName), "DDO_CONTAGEMRESULTADO_SERVICO") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTAGEMRESULTADO_SERVICOOPTIONS' */
            S151 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV26DDOName), "DDO_CONTAGEMRRESULTADO_SISTEMASIGLA") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTAGEMRRESULTADO_SISTEMASIGLAOPTIONS' */
            S161 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV30OptionsJson = AV29Options.ToJSonString(false);
         AV33OptionsDescJson = AV32OptionsDesc.ToJSonString(false);
         AV35OptionIndexesJson = AV34OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV37Session.Get("PromptContagemResultadoGridState"), "") == 0 )
         {
            AV39GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "PromptContagemResultadoGridState"), "");
         }
         else
         {
            AV39GridState.FromXml(AV37Session.Get("PromptContagemResultadoGridState"), "");
         }
         AV66GXV1 = 1;
         while ( AV66GXV1 <= AV39GridState.gxTpr_Filtervalues.Count )
         {
            AV40GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV39GridState.gxTpr_Filtervalues.Item(AV66GXV1));
            if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "CONTRATADA_AREATRABALHOCOD") == 0 )
            {
               AV42Contratada_AreaTrabalhoCod = (int)(NumberUtil.Val( AV40GridStateFilterValue.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 )
            {
               AV43ContagemResultado_ContratadaCod = (int)(NumberUtil.Val( AV40GridStateFilterValue.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADO_DEMANDAFM") == 0 )
            {
               AV10TFContagemResultado_DemandaFM = AV40GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADO_DEMANDAFM_SEL") == 0 )
            {
               AV11TFContagemResultado_DemandaFM_Sel = AV40GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADO_DEMANDA") == 0 )
            {
               AV12TFContagemResultado_Demanda = AV40GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADO_DEMANDA_SEL") == 0 )
            {
               AV13TFContagemResultado_Demanda_Sel = AV40GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADO_DATADMN") == 0 )
            {
               AV14TFContagemResultado_DataDmn = context.localUtil.CToD( AV40GridStateFilterValue.gxTpr_Value, 2);
               AV15TFContagemResultado_DataDmn_To = context.localUtil.CToD( AV40GridStateFilterValue.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADO_CONTRATADAPESSOANOM") == 0 )
            {
               AV16TFContagemResultado_ContratadaPessoaNom = AV40GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADO_CONTRATADAPESSOANOM_SEL") == 0 )
            {
               AV17TFContagemResultado_ContratadaPessoaNom_Sel = AV40GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADO_SERVICO") == 0 )
            {
               AV18TFContagemResultado_Servico = AV40GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADO_SERVICO_SEL") == 0 )
            {
               AV19TFContagemResultado_Servico_Sel = (int)(NumberUtil.Val( AV40GridStateFilterValue.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRRESULTADO_SISTEMASIGLA") == 0 )
            {
               AV20TFContagemrResultado_SistemaSigla = AV40GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRRESULTADO_SISTEMASIGLA_SEL") == 0 )
            {
               AV21TFContagemrResultado_SistemaSigla_Sel = AV40GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADO_STATUSDMN_SEL") == 0 )
            {
               AV22TFContagemResultado_StatusDmn_SelsJson = AV40GridStateFilterValue.gxTpr_Value;
               AV23TFContagemResultado_StatusDmn_Sels.FromJSonString(AV22TFContagemResultado_StatusDmn_SelsJson);
            }
            AV66GXV1 = (int)(AV66GXV1+1);
         }
         if ( AV39GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV41GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV39GridState.gxTpr_Dynamicfilters.Item(1));
            AV44DynamicFiltersSelector1 = AV41GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV44DynamicFiltersSelector1, "CONTAGEMRESULTADO_DEMANDA") == 0 )
            {
               AV45DynamicFiltersOperator1 = AV41GridStateDynamicFilter.gxTpr_Operator;
               AV46ContagemResultado_Demanda1 = AV41GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV44DynamicFiltersSelector1, "CONTAGEMRESULTADO_DATADMN") == 0 )
            {
               AV47ContagemResultado_DataDmn1 = context.localUtil.CToD( AV41GridStateDynamicFilter.gxTpr_Value, 2);
               AV48ContagemResultado_DataDmn_To1 = context.localUtil.CToD( AV41GridStateDynamicFilter.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV44DynamicFiltersSelector1, "CONTAGEMRESULTADO_STATUSDMN") == 0 )
            {
               AV49ContagemResultado_StatusDmn1 = AV41GridStateDynamicFilter.gxTpr_Value;
            }
            if ( AV39GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV50DynamicFiltersEnabled2 = true;
               AV41GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV39GridState.gxTpr_Dynamicfilters.Item(2));
               AV51DynamicFiltersSelector2 = AV41GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV51DynamicFiltersSelector2, "CONTAGEMRESULTADO_DEMANDA") == 0 )
               {
                  AV52DynamicFiltersOperator2 = AV41GridStateDynamicFilter.gxTpr_Operator;
                  AV53ContagemResultado_Demanda2 = AV41GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV51DynamicFiltersSelector2, "CONTAGEMRESULTADO_DATADMN") == 0 )
               {
                  AV54ContagemResultado_DataDmn2 = context.localUtil.CToD( AV41GridStateDynamicFilter.gxTpr_Value, 2);
                  AV55ContagemResultado_DataDmn_To2 = context.localUtil.CToD( AV41GridStateDynamicFilter.gxTpr_Valueto, 2);
               }
               else if ( StringUtil.StrCmp(AV51DynamicFiltersSelector2, "CONTAGEMRESULTADO_STATUSDMN") == 0 )
               {
                  AV56ContagemResultado_StatusDmn2 = AV41GridStateDynamicFilter.gxTpr_Value;
               }
               if ( AV39GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV57DynamicFiltersEnabled3 = true;
                  AV41GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV39GridState.gxTpr_Dynamicfilters.Item(3));
                  AV58DynamicFiltersSelector3 = AV41GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV58DynamicFiltersSelector3, "CONTAGEMRESULTADO_DEMANDA") == 0 )
                  {
                     AV59DynamicFiltersOperator3 = AV41GridStateDynamicFilter.gxTpr_Operator;
                     AV60ContagemResultado_Demanda3 = AV41GridStateDynamicFilter.gxTpr_Value;
                  }
                  else if ( StringUtil.StrCmp(AV58DynamicFiltersSelector3, "CONTAGEMRESULTADO_DATADMN") == 0 )
                  {
                     AV61ContagemResultado_DataDmn3 = context.localUtil.CToD( AV41GridStateDynamicFilter.gxTpr_Value, 2);
                     AV62ContagemResultado_DataDmn_To3 = context.localUtil.CToD( AV41GridStateDynamicFilter.gxTpr_Valueto, 2);
                  }
                  else if ( StringUtil.StrCmp(AV58DynamicFiltersSelector3, "CONTAGEMRESULTADO_STATUSDMN") == 0 )
                  {
                     AV63ContagemResultado_StatusDmn3 = AV41GridStateDynamicFilter.gxTpr_Value;
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADCONTAGEMRESULTADO_DEMANDAFMOPTIONS' Routine */
         AV10TFContagemResultado_DemandaFM = AV24SearchTxt;
         AV11TFContagemResultado_DemandaFM_Sel = "";
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              A484ContagemResultado_StatusDmn ,
                                              AV23TFContagemResultado_StatusDmn_Sels ,
                                              AV44DynamicFiltersSelector1 ,
                                              AV45DynamicFiltersOperator1 ,
                                              AV46ContagemResultado_Demanda1 ,
                                              AV47ContagemResultado_DataDmn1 ,
                                              AV48ContagemResultado_DataDmn_To1 ,
                                              AV49ContagemResultado_StatusDmn1 ,
                                              AV50DynamicFiltersEnabled2 ,
                                              AV51DynamicFiltersSelector2 ,
                                              AV52DynamicFiltersOperator2 ,
                                              AV53ContagemResultado_Demanda2 ,
                                              AV54ContagemResultado_DataDmn2 ,
                                              AV55ContagemResultado_DataDmn_To2 ,
                                              AV56ContagemResultado_StatusDmn2 ,
                                              AV57DynamicFiltersEnabled3 ,
                                              AV58DynamicFiltersSelector3 ,
                                              AV59DynamicFiltersOperator3 ,
                                              AV60ContagemResultado_Demanda3 ,
                                              AV61ContagemResultado_DataDmn3 ,
                                              AV62ContagemResultado_DataDmn_To3 ,
                                              AV63ContagemResultado_StatusDmn3 ,
                                              AV11TFContagemResultado_DemandaFM_Sel ,
                                              AV10TFContagemResultado_DemandaFM ,
                                              AV13TFContagemResultado_Demanda_Sel ,
                                              AV12TFContagemResultado_Demanda ,
                                              AV14TFContagemResultado_DataDmn ,
                                              AV15TFContagemResultado_DataDmn_To ,
                                              AV17TFContagemResultado_ContratadaPessoaNom_Sel ,
                                              AV16TFContagemResultado_ContratadaPessoaNom ,
                                              AV19TFContagemResultado_Servico_Sel ,
                                              AV18TFContagemResultado_Servico ,
                                              AV21TFContagemrResultado_SistemaSigla_Sel ,
                                              AV20TFContagemrResultado_SistemaSigla ,
                                              AV23TFContagemResultado_StatusDmn_Sels.Count ,
                                              AV9WWPContext.gxTpr_Contratada_codigo ,
                                              A457ContagemResultado_Demanda ,
                                              A493ContagemResultado_DemandaFM ,
                                              A471ContagemResultado_DataDmn ,
                                              A500ContagemResultado_ContratadaPessoaNom ,
                                              A605Servico_Sigla ,
                                              A601ContagemResultado_Servico ,
                                              A509ContagemrResultado_SistemaSigla ,
                                              A490ContagemResultado_ContratadaCod ,
                                              A52Contratada_AreaTrabalhoCod ,
                                              AV9WWPContext.gxTpr_Areatrabalho_codigo ,
                                              A1583ContagemResultado_TipoRegistro },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.DATE, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT,
                                              TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.SHORT
                                              }
         });
         lV46ContagemResultado_Demanda1 = StringUtil.PadR( StringUtil.RTrim( AV46ContagemResultado_Demanda1), 50, "%");
         lV46ContagemResultado_Demanda1 = StringUtil.PadR( StringUtil.RTrim( AV46ContagemResultado_Demanda1), 50, "%");
         lV46ContagemResultado_Demanda1 = StringUtil.PadR( StringUtil.RTrim( AV46ContagemResultado_Demanda1), 50, "%");
         lV46ContagemResultado_Demanda1 = StringUtil.PadR( StringUtil.RTrim( AV46ContagemResultado_Demanda1), 50, "%");
         lV53ContagemResultado_Demanda2 = StringUtil.PadR( StringUtil.RTrim( AV53ContagemResultado_Demanda2), 50, "%");
         lV53ContagemResultado_Demanda2 = StringUtil.PadR( StringUtil.RTrim( AV53ContagemResultado_Demanda2), 50, "%");
         lV53ContagemResultado_Demanda2 = StringUtil.PadR( StringUtil.RTrim( AV53ContagemResultado_Demanda2), 50, "%");
         lV53ContagemResultado_Demanda2 = StringUtil.PadR( StringUtil.RTrim( AV53ContagemResultado_Demanda2), 50, "%");
         lV60ContagemResultado_Demanda3 = StringUtil.PadR( StringUtil.RTrim( AV60ContagemResultado_Demanda3), 50, "%");
         lV60ContagemResultado_Demanda3 = StringUtil.PadR( StringUtil.RTrim( AV60ContagemResultado_Demanda3), 50, "%");
         lV60ContagemResultado_Demanda3 = StringUtil.PadR( StringUtil.RTrim( AV60ContagemResultado_Demanda3), 50, "%");
         lV60ContagemResultado_Demanda3 = StringUtil.PadR( StringUtil.RTrim( AV60ContagemResultado_Demanda3), 50, "%");
         lV10TFContagemResultado_DemandaFM = StringUtil.Concat( StringUtil.RTrim( AV10TFContagemResultado_DemandaFM), "%", "");
         lV12TFContagemResultado_Demanda = StringUtil.Concat( StringUtil.RTrim( AV12TFContagemResultado_Demanda), "%", "");
         lV16TFContagemResultado_ContratadaPessoaNom = StringUtil.PadR( StringUtil.RTrim( AV16TFContagemResultado_ContratadaPessoaNom), 100, "%");
         lV18TFContagemResultado_Servico = StringUtil.PadR( StringUtil.RTrim( AV18TFContagemResultado_Servico), 15, "%");
         lV20TFContagemrResultado_SistemaSigla = StringUtil.PadR( StringUtil.RTrim( AV20TFContagemrResultado_SistemaSigla), 25, "%");
         /* Using cursor P00S42 */
         pr_default.execute(0, new Object[] {AV9WWPContext.gxTpr_Areatrabalho_codigo, AV46ContagemResultado_Demanda1, AV46ContagemResultado_Demanda1, lV46ContagemResultado_Demanda1, lV46ContagemResultado_Demanda1, lV46ContagemResultado_Demanda1, lV46ContagemResultado_Demanda1, AV47ContagemResultado_DataDmn1, AV48ContagemResultado_DataDmn_To1, AV49ContagemResultado_StatusDmn1, AV53ContagemResultado_Demanda2, AV53ContagemResultado_Demanda2, lV53ContagemResultado_Demanda2, lV53ContagemResultado_Demanda2, lV53ContagemResultado_Demanda2, lV53ContagemResultado_Demanda2, AV54ContagemResultado_DataDmn2, AV55ContagemResultado_DataDmn_To2, AV56ContagemResultado_StatusDmn2, AV60ContagemResultado_Demanda3, AV60ContagemResultado_Demanda3, lV60ContagemResultado_Demanda3, lV60ContagemResultado_Demanda3, lV60ContagemResultado_Demanda3, lV60ContagemResultado_Demanda3, AV61ContagemResultado_DataDmn3, AV62ContagemResultado_DataDmn_To3, AV63ContagemResultado_StatusDmn3, lV10TFContagemResultado_DemandaFM, AV11TFContagemResultado_DemandaFM_Sel, lV12TFContagemResultado_Demanda, AV13TFContagemResultado_Demanda_Sel, AV14TFContagemResultado_DataDmn, AV15TFContagemResultado_DataDmn_To, lV16TFContagemResultado_ContratadaPessoaNom, AV17TFContagemResultado_ContratadaPessoaNom_Sel, lV18TFContagemResultado_Servico, AV19TFContagemResultado_Servico_Sel, lV20TFContagemrResultado_SistemaSigla, AV21TFContagemrResultado_SistemaSigla_Sel, AV9WWPContext.gxTpr_Contratada_codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKS42 = false;
            A146Modulo_Codigo = P00S42_A146Modulo_Codigo[0];
            n146Modulo_Codigo = P00S42_n146Modulo_Codigo[0];
            A127Sistema_Codigo = P00S42_A127Sistema_Codigo[0];
            A135Sistema_AreaTrabalhoCod = P00S42_A135Sistema_AreaTrabalhoCod[0];
            A830AreaTrabalho_ServicoPadrao = P00S42_A830AreaTrabalho_ServicoPadrao[0];
            n830AreaTrabalho_ServicoPadrao = P00S42_n830AreaTrabalho_ServicoPadrao[0];
            A499ContagemResultado_ContratadaPessoaCod = P00S42_A499ContagemResultado_ContratadaPessoaCod[0];
            n499ContagemResultado_ContratadaPessoaCod = P00S42_n499ContagemResultado_ContratadaPessoaCod[0];
            A489ContagemResultado_SistemaCod = P00S42_A489ContagemResultado_SistemaCod[0];
            n489ContagemResultado_SistemaCod = P00S42_n489ContagemResultado_SistemaCod[0];
            A1553ContagemResultado_CntSrvCod = P00S42_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P00S42_n1553ContagemResultado_CntSrvCod[0];
            A52Contratada_AreaTrabalhoCod = P00S42_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = P00S42_n52Contratada_AreaTrabalhoCod[0];
            A1583ContagemResultado_TipoRegistro = P00S42_A1583ContagemResultado_TipoRegistro[0];
            A493ContagemResultado_DemandaFM = P00S42_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = P00S42_n493ContagemResultado_DemandaFM[0];
            A490ContagemResultado_ContratadaCod = P00S42_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = P00S42_n490ContagemResultado_ContratadaCod[0];
            A509ContagemrResultado_SistemaSigla = P00S42_A509ContagemrResultado_SistemaSigla[0];
            n509ContagemrResultado_SistemaSigla = P00S42_n509ContagemrResultado_SistemaSigla[0];
            A601ContagemResultado_Servico = P00S42_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P00S42_n601ContagemResultado_Servico[0];
            A605Servico_Sigla = P00S42_A605Servico_Sigla[0];
            n605Servico_Sigla = P00S42_n605Servico_Sigla[0];
            A500ContagemResultado_ContratadaPessoaNom = P00S42_A500ContagemResultado_ContratadaPessoaNom[0];
            n500ContagemResultado_ContratadaPessoaNom = P00S42_n500ContagemResultado_ContratadaPessoaNom[0];
            A484ContagemResultado_StatusDmn = P00S42_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P00S42_n484ContagemResultado_StatusDmn[0];
            A471ContagemResultado_DataDmn = P00S42_A471ContagemResultado_DataDmn[0];
            A457ContagemResultado_Demanda = P00S42_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = P00S42_n457ContagemResultado_Demanda[0];
            A456ContagemResultado_Codigo = P00S42_A456ContagemResultado_Codigo[0];
            A127Sistema_Codigo = P00S42_A127Sistema_Codigo[0];
            A135Sistema_AreaTrabalhoCod = P00S42_A135Sistema_AreaTrabalhoCod[0];
            A830AreaTrabalho_ServicoPadrao = P00S42_A830AreaTrabalho_ServicoPadrao[0];
            n830AreaTrabalho_ServicoPadrao = P00S42_n830AreaTrabalho_ServicoPadrao[0];
            A605Servico_Sigla = P00S42_A605Servico_Sigla[0];
            n605Servico_Sigla = P00S42_n605Servico_Sigla[0];
            A509ContagemrResultado_SistemaSigla = P00S42_A509ContagemrResultado_SistemaSigla[0];
            n509ContagemrResultado_SistemaSigla = P00S42_n509ContagemrResultado_SistemaSigla[0];
            A601ContagemResultado_Servico = P00S42_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P00S42_n601ContagemResultado_Servico[0];
            A499ContagemResultado_ContratadaPessoaCod = P00S42_A499ContagemResultado_ContratadaPessoaCod[0];
            n499ContagemResultado_ContratadaPessoaCod = P00S42_n499ContagemResultado_ContratadaPessoaCod[0];
            A52Contratada_AreaTrabalhoCod = P00S42_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = P00S42_n52Contratada_AreaTrabalhoCod[0];
            A500ContagemResultado_ContratadaPessoaNom = P00S42_A500ContagemResultado_ContratadaPessoaNom[0];
            n500ContagemResultado_ContratadaPessoaNom = P00S42_n500ContagemResultado_ContratadaPessoaNom[0];
            AV36count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00S42_A493ContagemResultado_DemandaFM[0], A493ContagemResultado_DemandaFM) == 0 ) )
            {
               BRKS42 = false;
               A456ContagemResultado_Codigo = P00S42_A456ContagemResultado_Codigo[0];
               AV36count = (long)(AV36count+1);
               BRKS42 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A493ContagemResultado_DemandaFM)) )
            {
               AV28Option = A493ContagemResultado_DemandaFM;
               AV29Options.Add(AV28Option, 0);
               AV34OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV36count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV29Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKS42 )
            {
               BRKS42 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADCONTAGEMRESULTADO_DEMANDAOPTIONS' Routine */
         AV12TFContagemResultado_Demanda = AV24SearchTxt;
         AV13TFContagemResultado_Demanda_Sel = "";
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              A484ContagemResultado_StatusDmn ,
                                              AV23TFContagemResultado_StatusDmn_Sels ,
                                              AV44DynamicFiltersSelector1 ,
                                              AV45DynamicFiltersOperator1 ,
                                              AV46ContagemResultado_Demanda1 ,
                                              AV47ContagemResultado_DataDmn1 ,
                                              AV48ContagemResultado_DataDmn_To1 ,
                                              AV49ContagemResultado_StatusDmn1 ,
                                              AV50DynamicFiltersEnabled2 ,
                                              AV51DynamicFiltersSelector2 ,
                                              AV52DynamicFiltersOperator2 ,
                                              AV53ContagemResultado_Demanda2 ,
                                              AV54ContagemResultado_DataDmn2 ,
                                              AV55ContagemResultado_DataDmn_To2 ,
                                              AV56ContagemResultado_StatusDmn2 ,
                                              AV57DynamicFiltersEnabled3 ,
                                              AV58DynamicFiltersSelector3 ,
                                              AV59DynamicFiltersOperator3 ,
                                              AV60ContagemResultado_Demanda3 ,
                                              AV61ContagemResultado_DataDmn3 ,
                                              AV62ContagemResultado_DataDmn_To3 ,
                                              AV63ContagemResultado_StatusDmn3 ,
                                              AV11TFContagemResultado_DemandaFM_Sel ,
                                              AV10TFContagemResultado_DemandaFM ,
                                              AV13TFContagemResultado_Demanda_Sel ,
                                              AV12TFContagemResultado_Demanda ,
                                              AV14TFContagemResultado_DataDmn ,
                                              AV15TFContagemResultado_DataDmn_To ,
                                              AV17TFContagemResultado_ContratadaPessoaNom_Sel ,
                                              AV16TFContagemResultado_ContratadaPessoaNom ,
                                              AV19TFContagemResultado_Servico_Sel ,
                                              AV18TFContagemResultado_Servico ,
                                              AV21TFContagemrResultado_SistemaSigla_Sel ,
                                              AV20TFContagemrResultado_SistemaSigla ,
                                              AV23TFContagemResultado_StatusDmn_Sels.Count ,
                                              AV9WWPContext.gxTpr_Contratada_codigo ,
                                              A457ContagemResultado_Demanda ,
                                              A493ContagemResultado_DemandaFM ,
                                              A471ContagemResultado_DataDmn ,
                                              A500ContagemResultado_ContratadaPessoaNom ,
                                              A605Servico_Sigla ,
                                              A601ContagemResultado_Servico ,
                                              A509ContagemrResultado_SistemaSigla ,
                                              A490ContagemResultado_ContratadaCod ,
                                              A52Contratada_AreaTrabalhoCod ,
                                              AV9WWPContext.gxTpr_Areatrabalho_codigo ,
                                              A1583ContagemResultado_TipoRegistro },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.DATE, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT,
                                              TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.SHORT
                                              }
         });
         lV46ContagemResultado_Demanda1 = StringUtil.PadR( StringUtil.RTrim( AV46ContagemResultado_Demanda1), 50, "%");
         lV46ContagemResultado_Demanda1 = StringUtil.PadR( StringUtil.RTrim( AV46ContagemResultado_Demanda1), 50, "%");
         lV46ContagemResultado_Demanda1 = StringUtil.PadR( StringUtil.RTrim( AV46ContagemResultado_Demanda1), 50, "%");
         lV46ContagemResultado_Demanda1 = StringUtil.PadR( StringUtil.RTrim( AV46ContagemResultado_Demanda1), 50, "%");
         lV53ContagemResultado_Demanda2 = StringUtil.PadR( StringUtil.RTrim( AV53ContagemResultado_Demanda2), 50, "%");
         lV53ContagemResultado_Demanda2 = StringUtil.PadR( StringUtil.RTrim( AV53ContagemResultado_Demanda2), 50, "%");
         lV53ContagemResultado_Demanda2 = StringUtil.PadR( StringUtil.RTrim( AV53ContagemResultado_Demanda2), 50, "%");
         lV53ContagemResultado_Demanda2 = StringUtil.PadR( StringUtil.RTrim( AV53ContagemResultado_Demanda2), 50, "%");
         lV60ContagemResultado_Demanda3 = StringUtil.PadR( StringUtil.RTrim( AV60ContagemResultado_Demanda3), 50, "%");
         lV60ContagemResultado_Demanda3 = StringUtil.PadR( StringUtil.RTrim( AV60ContagemResultado_Demanda3), 50, "%");
         lV60ContagemResultado_Demanda3 = StringUtil.PadR( StringUtil.RTrim( AV60ContagemResultado_Demanda3), 50, "%");
         lV60ContagemResultado_Demanda3 = StringUtil.PadR( StringUtil.RTrim( AV60ContagemResultado_Demanda3), 50, "%");
         lV10TFContagemResultado_DemandaFM = StringUtil.Concat( StringUtil.RTrim( AV10TFContagemResultado_DemandaFM), "%", "");
         lV12TFContagemResultado_Demanda = StringUtil.Concat( StringUtil.RTrim( AV12TFContagemResultado_Demanda), "%", "");
         lV16TFContagemResultado_ContratadaPessoaNom = StringUtil.PadR( StringUtil.RTrim( AV16TFContagemResultado_ContratadaPessoaNom), 100, "%");
         lV18TFContagemResultado_Servico = StringUtil.PadR( StringUtil.RTrim( AV18TFContagemResultado_Servico), 15, "%");
         lV20TFContagemrResultado_SistemaSigla = StringUtil.PadR( StringUtil.RTrim( AV20TFContagemrResultado_SistemaSigla), 25, "%");
         /* Using cursor P00S43 */
         pr_default.execute(1, new Object[] {AV9WWPContext.gxTpr_Areatrabalho_codigo, AV46ContagemResultado_Demanda1, AV46ContagemResultado_Demanda1, lV46ContagemResultado_Demanda1, lV46ContagemResultado_Demanda1, lV46ContagemResultado_Demanda1, lV46ContagemResultado_Demanda1, AV47ContagemResultado_DataDmn1, AV48ContagemResultado_DataDmn_To1, AV49ContagemResultado_StatusDmn1, AV53ContagemResultado_Demanda2, AV53ContagemResultado_Demanda2, lV53ContagemResultado_Demanda2, lV53ContagemResultado_Demanda2, lV53ContagemResultado_Demanda2, lV53ContagemResultado_Demanda2, AV54ContagemResultado_DataDmn2, AV55ContagemResultado_DataDmn_To2, AV56ContagemResultado_StatusDmn2, AV60ContagemResultado_Demanda3, AV60ContagemResultado_Demanda3, lV60ContagemResultado_Demanda3, lV60ContagemResultado_Demanda3, lV60ContagemResultado_Demanda3, lV60ContagemResultado_Demanda3, AV61ContagemResultado_DataDmn3, AV62ContagemResultado_DataDmn_To3, AV63ContagemResultado_StatusDmn3, lV10TFContagemResultado_DemandaFM, AV11TFContagemResultado_DemandaFM_Sel, lV12TFContagemResultado_Demanda, AV13TFContagemResultado_Demanda_Sel, AV14TFContagemResultado_DataDmn, AV15TFContagemResultado_DataDmn_To, lV16TFContagemResultado_ContratadaPessoaNom, AV17TFContagemResultado_ContratadaPessoaNom_Sel, lV18TFContagemResultado_Servico, AV19TFContagemResultado_Servico_Sel, lV20TFContagemrResultado_SistemaSigla, AV21TFContagemrResultado_SistemaSigla_Sel, AV9WWPContext.gxTpr_Contratada_codigo});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKS44 = false;
            A146Modulo_Codigo = P00S43_A146Modulo_Codigo[0];
            n146Modulo_Codigo = P00S43_n146Modulo_Codigo[0];
            A127Sistema_Codigo = P00S43_A127Sistema_Codigo[0];
            A135Sistema_AreaTrabalhoCod = P00S43_A135Sistema_AreaTrabalhoCod[0];
            A830AreaTrabalho_ServicoPadrao = P00S43_A830AreaTrabalho_ServicoPadrao[0];
            n830AreaTrabalho_ServicoPadrao = P00S43_n830AreaTrabalho_ServicoPadrao[0];
            A499ContagemResultado_ContratadaPessoaCod = P00S43_A499ContagemResultado_ContratadaPessoaCod[0];
            n499ContagemResultado_ContratadaPessoaCod = P00S43_n499ContagemResultado_ContratadaPessoaCod[0];
            A489ContagemResultado_SistemaCod = P00S43_A489ContagemResultado_SistemaCod[0];
            n489ContagemResultado_SistemaCod = P00S43_n489ContagemResultado_SistemaCod[0];
            A1553ContagemResultado_CntSrvCod = P00S43_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P00S43_n1553ContagemResultado_CntSrvCod[0];
            A457ContagemResultado_Demanda = P00S43_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = P00S43_n457ContagemResultado_Demanda[0];
            A1583ContagemResultado_TipoRegistro = P00S43_A1583ContagemResultado_TipoRegistro[0];
            A490ContagemResultado_ContratadaCod = P00S43_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = P00S43_n490ContagemResultado_ContratadaCod[0];
            A509ContagemrResultado_SistemaSigla = P00S43_A509ContagemrResultado_SistemaSigla[0];
            n509ContagemrResultado_SistemaSigla = P00S43_n509ContagemrResultado_SistemaSigla[0];
            A601ContagemResultado_Servico = P00S43_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P00S43_n601ContagemResultado_Servico[0];
            A605Servico_Sigla = P00S43_A605Servico_Sigla[0];
            n605Servico_Sigla = P00S43_n605Servico_Sigla[0];
            A500ContagemResultado_ContratadaPessoaNom = P00S43_A500ContagemResultado_ContratadaPessoaNom[0];
            n500ContagemResultado_ContratadaPessoaNom = P00S43_n500ContagemResultado_ContratadaPessoaNom[0];
            A484ContagemResultado_StatusDmn = P00S43_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P00S43_n484ContagemResultado_StatusDmn[0];
            A471ContagemResultado_DataDmn = P00S43_A471ContagemResultado_DataDmn[0];
            A493ContagemResultado_DemandaFM = P00S43_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = P00S43_n493ContagemResultado_DemandaFM[0];
            A52Contratada_AreaTrabalhoCod = P00S43_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = P00S43_n52Contratada_AreaTrabalhoCod[0];
            A456ContagemResultado_Codigo = P00S43_A456ContagemResultado_Codigo[0];
            A127Sistema_Codigo = P00S43_A127Sistema_Codigo[0];
            A135Sistema_AreaTrabalhoCod = P00S43_A135Sistema_AreaTrabalhoCod[0];
            A830AreaTrabalho_ServicoPadrao = P00S43_A830AreaTrabalho_ServicoPadrao[0];
            n830AreaTrabalho_ServicoPadrao = P00S43_n830AreaTrabalho_ServicoPadrao[0];
            A605Servico_Sigla = P00S43_A605Servico_Sigla[0];
            n605Servico_Sigla = P00S43_n605Servico_Sigla[0];
            A509ContagemrResultado_SistemaSigla = P00S43_A509ContagemrResultado_SistemaSigla[0];
            n509ContagemrResultado_SistemaSigla = P00S43_n509ContagemrResultado_SistemaSigla[0];
            A601ContagemResultado_Servico = P00S43_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P00S43_n601ContagemResultado_Servico[0];
            A499ContagemResultado_ContratadaPessoaCod = P00S43_A499ContagemResultado_ContratadaPessoaCod[0];
            n499ContagemResultado_ContratadaPessoaCod = P00S43_n499ContagemResultado_ContratadaPessoaCod[0];
            A52Contratada_AreaTrabalhoCod = P00S43_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = P00S43_n52Contratada_AreaTrabalhoCod[0];
            A500ContagemResultado_ContratadaPessoaNom = P00S43_A500ContagemResultado_ContratadaPessoaNom[0];
            n500ContagemResultado_ContratadaPessoaNom = P00S43_n500ContagemResultado_ContratadaPessoaNom[0];
            AV36count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( StringUtil.StrCmp(P00S43_A457ContagemResultado_Demanda[0], A457ContagemResultado_Demanda) == 0 ) )
            {
               BRKS44 = false;
               A456ContagemResultado_Codigo = P00S43_A456ContagemResultado_Codigo[0];
               AV36count = (long)(AV36count+1);
               BRKS44 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A457ContagemResultado_Demanda)) )
            {
               AV28Option = A457ContagemResultado_Demanda;
               AV31OptionDesc = StringUtil.Trim( StringUtil.RTrim( context.localUtil.Format( A457ContagemResultado_Demanda, "@!")));
               AV29Options.Add(AV28Option, 0);
               AV32OptionsDesc.Add(AV31OptionDesc, 0);
               AV34OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV36count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV29Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKS44 )
            {
               BRKS44 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      protected void S141( )
      {
         /* 'LOADCONTAGEMRESULTADO_CONTRATADAPESSOANOMOPTIONS' Routine */
         AV16TFContagemResultado_ContratadaPessoaNom = AV24SearchTxt;
         AV17TFContagemResultado_ContratadaPessoaNom_Sel = "";
         pr_default.dynParam(2, new Object[]{ new Object[]{
                                              A484ContagemResultado_StatusDmn ,
                                              AV23TFContagemResultado_StatusDmn_Sels ,
                                              AV44DynamicFiltersSelector1 ,
                                              AV45DynamicFiltersOperator1 ,
                                              AV46ContagemResultado_Demanda1 ,
                                              AV47ContagemResultado_DataDmn1 ,
                                              AV48ContagemResultado_DataDmn_To1 ,
                                              AV49ContagemResultado_StatusDmn1 ,
                                              AV50DynamicFiltersEnabled2 ,
                                              AV51DynamicFiltersSelector2 ,
                                              AV52DynamicFiltersOperator2 ,
                                              AV53ContagemResultado_Demanda2 ,
                                              AV54ContagemResultado_DataDmn2 ,
                                              AV55ContagemResultado_DataDmn_To2 ,
                                              AV56ContagemResultado_StatusDmn2 ,
                                              AV57DynamicFiltersEnabled3 ,
                                              AV58DynamicFiltersSelector3 ,
                                              AV59DynamicFiltersOperator3 ,
                                              AV60ContagemResultado_Demanda3 ,
                                              AV61ContagemResultado_DataDmn3 ,
                                              AV62ContagemResultado_DataDmn_To3 ,
                                              AV63ContagemResultado_StatusDmn3 ,
                                              AV11TFContagemResultado_DemandaFM_Sel ,
                                              AV10TFContagemResultado_DemandaFM ,
                                              AV13TFContagemResultado_Demanda_Sel ,
                                              AV12TFContagemResultado_Demanda ,
                                              AV14TFContagemResultado_DataDmn ,
                                              AV15TFContagemResultado_DataDmn_To ,
                                              AV17TFContagemResultado_ContratadaPessoaNom_Sel ,
                                              AV16TFContagemResultado_ContratadaPessoaNom ,
                                              AV19TFContagemResultado_Servico_Sel ,
                                              AV18TFContagemResultado_Servico ,
                                              AV21TFContagemrResultado_SistemaSigla_Sel ,
                                              AV20TFContagemrResultado_SistemaSigla ,
                                              AV23TFContagemResultado_StatusDmn_Sels.Count ,
                                              AV9WWPContext.gxTpr_Contratada_codigo ,
                                              A457ContagemResultado_Demanda ,
                                              A493ContagemResultado_DemandaFM ,
                                              A471ContagemResultado_DataDmn ,
                                              A500ContagemResultado_ContratadaPessoaNom ,
                                              A605Servico_Sigla ,
                                              A601ContagemResultado_Servico ,
                                              A509ContagemrResultado_SistemaSigla ,
                                              A490ContagemResultado_ContratadaCod ,
                                              A52Contratada_AreaTrabalhoCod ,
                                              AV9WWPContext.gxTpr_Areatrabalho_codigo ,
                                              A1583ContagemResultado_TipoRegistro },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.DATE, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT,
                                              TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.SHORT
                                              }
         });
         lV46ContagemResultado_Demanda1 = StringUtil.PadR( StringUtil.RTrim( AV46ContagemResultado_Demanda1), 50, "%");
         lV46ContagemResultado_Demanda1 = StringUtil.PadR( StringUtil.RTrim( AV46ContagemResultado_Demanda1), 50, "%");
         lV46ContagemResultado_Demanda1 = StringUtil.PadR( StringUtil.RTrim( AV46ContagemResultado_Demanda1), 50, "%");
         lV46ContagemResultado_Demanda1 = StringUtil.PadR( StringUtil.RTrim( AV46ContagemResultado_Demanda1), 50, "%");
         lV53ContagemResultado_Demanda2 = StringUtil.PadR( StringUtil.RTrim( AV53ContagemResultado_Demanda2), 50, "%");
         lV53ContagemResultado_Demanda2 = StringUtil.PadR( StringUtil.RTrim( AV53ContagemResultado_Demanda2), 50, "%");
         lV53ContagemResultado_Demanda2 = StringUtil.PadR( StringUtil.RTrim( AV53ContagemResultado_Demanda2), 50, "%");
         lV53ContagemResultado_Demanda2 = StringUtil.PadR( StringUtil.RTrim( AV53ContagemResultado_Demanda2), 50, "%");
         lV60ContagemResultado_Demanda3 = StringUtil.PadR( StringUtil.RTrim( AV60ContagemResultado_Demanda3), 50, "%");
         lV60ContagemResultado_Demanda3 = StringUtil.PadR( StringUtil.RTrim( AV60ContagemResultado_Demanda3), 50, "%");
         lV60ContagemResultado_Demanda3 = StringUtil.PadR( StringUtil.RTrim( AV60ContagemResultado_Demanda3), 50, "%");
         lV60ContagemResultado_Demanda3 = StringUtil.PadR( StringUtil.RTrim( AV60ContagemResultado_Demanda3), 50, "%");
         lV10TFContagemResultado_DemandaFM = StringUtil.Concat( StringUtil.RTrim( AV10TFContagemResultado_DemandaFM), "%", "");
         lV12TFContagemResultado_Demanda = StringUtil.Concat( StringUtil.RTrim( AV12TFContagemResultado_Demanda), "%", "");
         lV16TFContagemResultado_ContratadaPessoaNom = StringUtil.PadR( StringUtil.RTrim( AV16TFContagemResultado_ContratadaPessoaNom), 100, "%");
         lV18TFContagemResultado_Servico = StringUtil.PadR( StringUtil.RTrim( AV18TFContagemResultado_Servico), 15, "%");
         lV20TFContagemrResultado_SistemaSigla = StringUtil.PadR( StringUtil.RTrim( AV20TFContagemrResultado_SistemaSigla), 25, "%");
         /* Using cursor P00S44 */
         pr_default.execute(2, new Object[] {AV9WWPContext.gxTpr_Areatrabalho_codigo, AV46ContagemResultado_Demanda1, AV46ContagemResultado_Demanda1, lV46ContagemResultado_Demanda1, lV46ContagemResultado_Demanda1, lV46ContagemResultado_Demanda1, lV46ContagemResultado_Demanda1, AV47ContagemResultado_DataDmn1, AV48ContagemResultado_DataDmn_To1, AV49ContagemResultado_StatusDmn1, AV53ContagemResultado_Demanda2, AV53ContagemResultado_Demanda2, lV53ContagemResultado_Demanda2, lV53ContagemResultado_Demanda2, lV53ContagemResultado_Demanda2, lV53ContagemResultado_Demanda2, AV54ContagemResultado_DataDmn2, AV55ContagemResultado_DataDmn_To2, AV56ContagemResultado_StatusDmn2, AV60ContagemResultado_Demanda3, AV60ContagemResultado_Demanda3, lV60ContagemResultado_Demanda3, lV60ContagemResultado_Demanda3, lV60ContagemResultado_Demanda3, lV60ContagemResultado_Demanda3, AV61ContagemResultado_DataDmn3, AV62ContagemResultado_DataDmn_To3, AV63ContagemResultado_StatusDmn3, lV10TFContagemResultado_DemandaFM, AV11TFContagemResultado_DemandaFM_Sel, lV12TFContagemResultado_Demanda, AV13TFContagemResultado_Demanda_Sel, AV14TFContagemResultado_DataDmn, AV15TFContagemResultado_DataDmn_To, lV16TFContagemResultado_ContratadaPessoaNom, AV17TFContagemResultado_ContratadaPessoaNom_Sel, lV18TFContagemResultado_Servico, AV19TFContagemResultado_Servico_Sel, lV20TFContagemrResultado_SistemaSigla, AV21TFContagemrResultado_SistemaSigla_Sel, AV9WWPContext.gxTpr_Contratada_codigo});
         while ( (pr_default.getStatus(2) != 101) )
         {
            BRKS46 = false;
            A146Modulo_Codigo = P00S44_A146Modulo_Codigo[0];
            n146Modulo_Codigo = P00S44_n146Modulo_Codigo[0];
            A127Sistema_Codigo = P00S44_A127Sistema_Codigo[0];
            A135Sistema_AreaTrabalhoCod = P00S44_A135Sistema_AreaTrabalhoCod[0];
            A830AreaTrabalho_ServicoPadrao = P00S44_A830AreaTrabalho_ServicoPadrao[0];
            n830AreaTrabalho_ServicoPadrao = P00S44_n830AreaTrabalho_ServicoPadrao[0];
            A499ContagemResultado_ContratadaPessoaCod = P00S44_A499ContagemResultado_ContratadaPessoaCod[0];
            n499ContagemResultado_ContratadaPessoaCod = P00S44_n499ContagemResultado_ContratadaPessoaCod[0];
            A489ContagemResultado_SistemaCod = P00S44_A489ContagemResultado_SistemaCod[0];
            n489ContagemResultado_SistemaCod = P00S44_n489ContagemResultado_SistemaCod[0];
            A1553ContagemResultado_CntSrvCod = P00S44_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P00S44_n1553ContagemResultado_CntSrvCod[0];
            A52Contratada_AreaTrabalhoCod = P00S44_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = P00S44_n52Contratada_AreaTrabalhoCod[0];
            A1583ContagemResultado_TipoRegistro = P00S44_A1583ContagemResultado_TipoRegistro[0];
            A500ContagemResultado_ContratadaPessoaNom = P00S44_A500ContagemResultado_ContratadaPessoaNom[0];
            n500ContagemResultado_ContratadaPessoaNom = P00S44_n500ContagemResultado_ContratadaPessoaNom[0];
            A490ContagemResultado_ContratadaCod = P00S44_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = P00S44_n490ContagemResultado_ContratadaCod[0];
            A509ContagemrResultado_SistemaSigla = P00S44_A509ContagemrResultado_SistemaSigla[0];
            n509ContagemrResultado_SistemaSigla = P00S44_n509ContagemrResultado_SistemaSigla[0];
            A601ContagemResultado_Servico = P00S44_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P00S44_n601ContagemResultado_Servico[0];
            A605Servico_Sigla = P00S44_A605Servico_Sigla[0];
            n605Servico_Sigla = P00S44_n605Servico_Sigla[0];
            A484ContagemResultado_StatusDmn = P00S44_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P00S44_n484ContagemResultado_StatusDmn[0];
            A471ContagemResultado_DataDmn = P00S44_A471ContagemResultado_DataDmn[0];
            A493ContagemResultado_DemandaFM = P00S44_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = P00S44_n493ContagemResultado_DemandaFM[0];
            A457ContagemResultado_Demanda = P00S44_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = P00S44_n457ContagemResultado_Demanda[0];
            A456ContagemResultado_Codigo = P00S44_A456ContagemResultado_Codigo[0];
            A127Sistema_Codigo = P00S44_A127Sistema_Codigo[0];
            A135Sistema_AreaTrabalhoCod = P00S44_A135Sistema_AreaTrabalhoCod[0];
            A830AreaTrabalho_ServicoPadrao = P00S44_A830AreaTrabalho_ServicoPadrao[0];
            n830AreaTrabalho_ServicoPadrao = P00S44_n830AreaTrabalho_ServicoPadrao[0];
            A605Servico_Sigla = P00S44_A605Servico_Sigla[0];
            n605Servico_Sigla = P00S44_n605Servico_Sigla[0];
            A509ContagemrResultado_SistemaSigla = P00S44_A509ContagemrResultado_SistemaSigla[0];
            n509ContagemrResultado_SistemaSigla = P00S44_n509ContagemrResultado_SistemaSigla[0];
            A601ContagemResultado_Servico = P00S44_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P00S44_n601ContagemResultado_Servico[0];
            A499ContagemResultado_ContratadaPessoaCod = P00S44_A499ContagemResultado_ContratadaPessoaCod[0];
            n499ContagemResultado_ContratadaPessoaCod = P00S44_n499ContagemResultado_ContratadaPessoaCod[0];
            A52Contratada_AreaTrabalhoCod = P00S44_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = P00S44_n52Contratada_AreaTrabalhoCod[0];
            A500ContagemResultado_ContratadaPessoaNom = P00S44_A500ContagemResultado_ContratadaPessoaNom[0];
            n500ContagemResultado_ContratadaPessoaNom = P00S44_n500ContagemResultado_ContratadaPessoaNom[0];
            AV36count = 0;
            while ( (pr_default.getStatus(2) != 101) && ( StringUtil.StrCmp(P00S44_A500ContagemResultado_ContratadaPessoaNom[0], A500ContagemResultado_ContratadaPessoaNom) == 0 ) )
            {
               BRKS46 = false;
               A499ContagemResultado_ContratadaPessoaCod = P00S44_A499ContagemResultado_ContratadaPessoaCod[0];
               n499ContagemResultado_ContratadaPessoaCod = P00S44_n499ContagemResultado_ContratadaPessoaCod[0];
               A490ContagemResultado_ContratadaCod = P00S44_A490ContagemResultado_ContratadaCod[0];
               n490ContagemResultado_ContratadaCod = P00S44_n490ContagemResultado_ContratadaCod[0];
               A456ContagemResultado_Codigo = P00S44_A456ContagemResultado_Codigo[0];
               A499ContagemResultado_ContratadaPessoaCod = P00S44_A499ContagemResultado_ContratadaPessoaCod[0];
               n499ContagemResultado_ContratadaPessoaCod = P00S44_n499ContagemResultado_ContratadaPessoaCod[0];
               AV36count = (long)(AV36count+1);
               BRKS46 = true;
               pr_default.readNext(2);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A500ContagemResultado_ContratadaPessoaNom)) )
            {
               AV28Option = A500ContagemResultado_ContratadaPessoaNom;
               AV29Options.Add(AV28Option, 0);
               AV34OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV36count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV29Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKS46 )
            {
               BRKS46 = true;
               pr_default.readNext(2);
            }
         }
         pr_default.close(2);
      }

      protected void S151( )
      {
         /* 'LOADCONTAGEMRESULTADO_SERVICOOPTIONS' Routine */
         AV18TFContagemResultado_Servico = AV24SearchTxt;
         AV19TFContagemResultado_Servico_Sel = 0;
         pr_default.dynParam(3, new Object[]{ new Object[]{
                                              A484ContagemResultado_StatusDmn ,
                                              AV23TFContagemResultado_StatusDmn_Sels ,
                                              AV44DynamicFiltersSelector1 ,
                                              AV45DynamicFiltersOperator1 ,
                                              AV46ContagemResultado_Demanda1 ,
                                              AV47ContagemResultado_DataDmn1 ,
                                              AV48ContagemResultado_DataDmn_To1 ,
                                              AV49ContagemResultado_StatusDmn1 ,
                                              AV50DynamicFiltersEnabled2 ,
                                              AV51DynamicFiltersSelector2 ,
                                              AV52DynamicFiltersOperator2 ,
                                              AV53ContagemResultado_Demanda2 ,
                                              AV54ContagemResultado_DataDmn2 ,
                                              AV55ContagemResultado_DataDmn_To2 ,
                                              AV56ContagemResultado_StatusDmn2 ,
                                              AV57DynamicFiltersEnabled3 ,
                                              AV58DynamicFiltersSelector3 ,
                                              AV59DynamicFiltersOperator3 ,
                                              AV60ContagemResultado_Demanda3 ,
                                              AV61ContagemResultado_DataDmn3 ,
                                              AV62ContagemResultado_DataDmn_To3 ,
                                              AV63ContagemResultado_StatusDmn3 ,
                                              AV11TFContagemResultado_DemandaFM_Sel ,
                                              AV10TFContagemResultado_DemandaFM ,
                                              AV13TFContagemResultado_Demanda_Sel ,
                                              AV12TFContagemResultado_Demanda ,
                                              AV14TFContagemResultado_DataDmn ,
                                              AV15TFContagemResultado_DataDmn_To ,
                                              AV17TFContagemResultado_ContratadaPessoaNom_Sel ,
                                              AV16TFContagemResultado_ContratadaPessoaNom ,
                                              AV19TFContagemResultado_Servico_Sel ,
                                              AV18TFContagemResultado_Servico ,
                                              AV21TFContagemrResultado_SistemaSigla_Sel ,
                                              AV20TFContagemrResultado_SistemaSigla ,
                                              AV23TFContagemResultado_StatusDmn_Sels.Count ,
                                              AV9WWPContext.gxTpr_Contratada_codigo ,
                                              A457ContagemResultado_Demanda ,
                                              A493ContagemResultado_DemandaFM ,
                                              A471ContagemResultado_DataDmn ,
                                              A500ContagemResultado_ContratadaPessoaNom ,
                                              A605Servico_Sigla ,
                                              A601ContagemResultado_Servico ,
                                              A509ContagemrResultado_SistemaSigla ,
                                              A490ContagemResultado_ContratadaCod ,
                                              A52Contratada_AreaTrabalhoCod ,
                                              AV9WWPContext.gxTpr_Areatrabalho_codigo ,
                                              A1583ContagemResultado_TipoRegistro },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.DATE, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT,
                                              TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.SHORT
                                              }
         });
         lV46ContagemResultado_Demanda1 = StringUtil.PadR( StringUtil.RTrim( AV46ContagemResultado_Demanda1), 50, "%");
         lV46ContagemResultado_Demanda1 = StringUtil.PadR( StringUtil.RTrim( AV46ContagemResultado_Demanda1), 50, "%");
         lV46ContagemResultado_Demanda1 = StringUtil.PadR( StringUtil.RTrim( AV46ContagemResultado_Demanda1), 50, "%");
         lV46ContagemResultado_Demanda1 = StringUtil.PadR( StringUtil.RTrim( AV46ContagemResultado_Demanda1), 50, "%");
         lV53ContagemResultado_Demanda2 = StringUtil.PadR( StringUtil.RTrim( AV53ContagemResultado_Demanda2), 50, "%");
         lV53ContagemResultado_Demanda2 = StringUtil.PadR( StringUtil.RTrim( AV53ContagemResultado_Demanda2), 50, "%");
         lV53ContagemResultado_Demanda2 = StringUtil.PadR( StringUtil.RTrim( AV53ContagemResultado_Demanda2), 50, "%");
         lV53ContagemResultado_Demanda2 = StringUtil.PadR( StringUtil.RTrim( AV53ContagemResultado_Demanda2), 50, "%");
         lV60ContagemResultado_Demanda3 = StringUtil.PadR( StringUtil.RTrim( AV60ContagemResultado_Demanda3), 50, "%");
         lV60ContagemResultado_Demanda3 = StringUtil.PadR( StringUtil.RTrim( AV60ContagemResultado_Demanda3), 50, "%");
         lV60ContagemResultado_Demanda3 = StringUtil.PadR( StringUtil.RTrim( AV60ContagemResultado_Demanda3), 50, "%");
         lV60ContagemResultado_Demanda3 = StringUtil.PadR( StringUtil.RTrim( AV60ContagemResultado_Demanda3), 50, "%");
         lV10TFContagemResultado_DemandaFM = StringUtil.Concat( StringUtil.RTrim( AV10TFContagemResultado_DemandaFM), "%", "");
         lV12TFContagemResultado_Demanda = StringUtil.Concat( StringUtil.RTrim( AV12TFContagemResultado_Demanda), "%", "");
         lV16TFContagemResultado_ContratadaPessoaNom = StringUtil.PadR( StringUtil.RTrim( AV16TFContagemResultado_ContratadaPessoaNom), 100, "%");
         lV18TFContagemResultado_Servico = StringUtil.PadR( StringUtil.RTrim( AV18TFContagemResultado_Servico), 15, "%");
         lV20TFContagemrResultado_SistemaSigla = StringUtil.PadR( StringUtil.RTrim( AV20TFContagemrResultado_SistemaSigla), 25, "%");
         /* Using cursor P00S45 */
         pr_default.execute(3, new Object[] {AV9WWPContext.gxTpr_Areatrabalho_codigo, AV46ContagemResultado_Demanda1, AV46ContagemResultado_Demanda1, lV46ContagemResultado_Demanda1, lV46ContagemResultado_Demanda1, lV46ContagemResultado_Demanda1, lV46ContagemResultado_Demanda1, AV47ContagemResultado_DataDmn1, AV48ContagemResultado_DataDmn_To1, AV49ContagemResultado_StatusDmn1, AV53ContagemResultado_Demanda2, AV53ContagemResultado_Demanda2, lV53ContagemResultado_Demanda2, lV53ContagemResultado_Demanda2, lV53ContagemResultado_Demanda2, lV53ContagemResultado_Demanda2, AV54ContagemResultado_DataDmn2, AV55ContagemResultado_DataDmn_To2, AV56ContagemResultado_StatusDmn2, AV60ContagemResultado_Demanda3, AV60ContagemResultado_Demanda3, lV60ContagemResultado_Demanda3, lV60ContagemResultado_Demanda3, lV60ContagemResultado_Demanda3, lV60ContagemResultado_Demanda3, AV61ContagemResultado_DataDmn3, AV62ContagemResultado_DataDmn_To3, AV63ContagemResultado_StatusDmn3, lV10TFContagemResultado_DemandaFM, AV11TFContagemResultado_DemandaFM_Sel, lV12TFContagemResultado_Demanda, AV13TFContagemResultado_Demanda_Sel, AV14TFContagemResultado_DataDmn, AV15TFContagemResultado_DataDmn_To, lV16TFContagemResultado_ContratadaPessoaNom, AV17TFContagemResultado_ContratadaPessoaNom_Sel, lV18TFContagemResultado_Servico, AV19TFContagemResultado_Servico_Sel, lV20TFContagemrResultado_SistemaSigla, AV21TFContagemrResultado_SistemaSigla_Sel, AV9WWPContext.gxTpr_Contratada_codigo});
         while ( (pr_default.getStatus(3) != 101) )
         {
            BRKS48 = false;
            A146Modulo_Codigo = P00S45_A146Modulo_Codigo[0];
            n146Modulo_Codigo = P00S45_n146Modulo_Codigo[0];
            A127Sistema_Codigo = P00S45_A127Sistema_Codigo[0];
            A135Sistema_AreaTrabalhoCod = P00S45_A135Sistema_AreaTrabalhoCod[0];
            A830AreaTrabalho_ServicoPadrao = P00S45_A830AreaTrabalho_ServicoPadrao[0];
            n830AreaTrabalho_ServicoPadrao = P00S45_n830AreaTrabalho_ServicoPadrao[0];
            A499ContagemResultado_ContratadaPessoaCod = P00S45_A499ContagemResultado_ContratadaPessoaCod[0];
            n499ContagemResultado_ContratadaPessoaCod = P00S45_n499ContagemResultado_ContratadaPessoaCod[0];
            A489ContagemResultado_SistemaCod = P00S45_A489ContagemResultado_SistemaCod[0];
            n489ContagemResultado_SistemaCod = P00S45_n489ContagemResultado_SistemaCod[0];
            A1553ContagemResultado_CntSrvCod = P00S45_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P00S45_n1553ContagemResultado_CntSrvCod[0];
            A52Contratada_AreaTrabalhoCod = P00S45_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = P00S45_n52Contratada_AreaTrabalhoCod[0];
            A1583ContagemResultado_TipoRegistro = P00S45_A1583ContagemResultado_TipoRegistro[0];
            A601ContagemResultado_Servico = P00S45_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P00S45_n601ContagemResultado_Servico[0];
            A490ContagemResultado_ContratadaCod = P00S45_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = P00S45_n490ContagemResultado_ContratadaCod[0];
            A509ContagemrResultado_SistemaSigla = P00S45_A509ContagemrResultado_SistemaSigla[0];
            n509ContagemrResultado_SistemaSigla = P00S45_n509ContagemrResultado_SistemaSigla[0];
            A605Servico_Sigla = P00S45_A605Servico_Sigla[0];
            n605Servico_Sigla = P00S45_n605Servico_Sigla[0];
            A500ContagemResultado_ContratadaPessoaNom = P00S45_A500ContagemResultado_ContratadaPessoaNom[0];
            n500ContagemResultado_ContratadaPessoaNom = P00S45_n500ContagemResultado_ContratadaPessoaNom[0];
            A484ContagemResultado_StatusDmn = P00S45_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P00S45_n484ContagemResultado_StatusDmn[0];
            A471ContagemResultado_DataDmn = P00S45_A471ContagemResultado_DataDmn[0];
            A493ContagemResultado_DemandaFM = P00S45_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = P00S45_n493ContagemResultado_DemandaFM[0];
            A457ContagemResultado_Demanda = P00S45_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = P00S45_n457ContagemResultado_Demanda[0];
            A456ContagemResultado_Codigo = P00S45_A456ContagemResultado_Codigo[0];
            A127Sistema_Codigo = P00S45_A127Sistema_Codigo[0];
            A135Sistema_AreaTrabalhoCod = P00S45_A135Sistema_AreaTrabalhoCod[0];
            A830AreaTrabalho_ServicoPadrao = P00S45_A830AreaTrabalho_ServicoPadrao[0];
            n830AreaTrabalho_ServicoPadrao = P00S45_n830AreaTrabalho_ServicoPadrao[0];
            A605Servico_Sigla = P00S45_A605Servico_Sigla[0];
            n605Servico_Sigla = P00S45_n605Servico_Sigla[0];
            A509ContagemrResultado_SistemaSigla = P00S45_A509ContagemrResultado_SistemaSigla[0];
            n509ContagemrResultado_SistemaSigla = P00S45_n509ContagemrResultado_SistemaSigla[0];
            A601ContagemResultado_Servico = P00S45_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P00S45_n601ContagemResultado_Servico[0];
            A499ContagemResultado_ContratadaPessoaCod = P00S45_A499ContagemResultado_ContratadaPessoaCod[0];
            n499ContagemResultado_ContratadaPessoaCod = P00S45_n499ContagemResultado_ContratadaPessoaCod[0];
            A52Contratada_AreaTrabalhoCod = P00S45_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = P00S45_n52Contratada_AreaTrabalhoCod[0];
            A500ContagemResultado_ContratadaPessoaNom = P00S45_A500ContagemResultado_ContratadaPessoaNom[0];
            n500ContagemResultado_ContratadaPessoaNom = P00S45_n500ContagemResultado_ContratadaPessoaNom[0];
            AV36count = 0;
            while ( (pr_default.getStatus(3) != 101) && ( P00S45_A601ContagemResultado_Servico[0] == A601ContagemResultado_Servico ) )
            {
               BRKS48 = false;
               A1553ContagemResultado_CntSrvCod = P00S45_A1553ContagemResultado_CntSrvCod[0];
               n1553ContagemResultado_CntSrvCod = P00S45_n1553ContagemResultado_CntSrvCod[0];
               A456ContagemResultado_Codigo = P00S45_A456ContagemResultado_Codigo[0];
               AV36count = (long)(AV36count+1);
               BRKS48 = true;
               pr_default.readNext(3);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A605Servico_Sigla)) )
            {
               AV28Option = StringUtil.Str( (decimal)(A601ContagemResultado_Servico), 6, 0);
               AV31OptionDesc = A605Servico_Sigla;
               AV27InsertIndex = 1;
               while ( ( AV27InsertIndex <= AV29Options.Count ) && ( StringUtil.StrCmp(((String)AV32OptionsDesc.Item(AV27InsertIndex)), AV31OptionDesc) < 0 ) )
               {
                  AV27InsertIndex = (int)(AV27InsertIndex+1);
               }
               AV29Options.Add(AV28Option, AV27InsertIndex);
               AV32OptionsDesc.Add(AV31OptionDesc, AV27InsertIndex);
               AV34OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV36count), "Z,ZZZ,ZZZ,ZZ9")), AV27InsertIndex);
            }
            if ( AV29Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKS48 )
            {
               BRKS48 = true;
               pr_default.readNext(3);
            }
         }
         pr_default.close(3);
      }

      protected void S161( )
      {
         /* 'LOADCONTAGEMRRESULTADO_SISTEMASIGLAOPTIONS' Routine */
         AV20TFContagemrResultado_SistemaSigla = AV24SearchTxt;
         AV21TFContagemrResultado_SistemaSigla_Sel = "";
         pr_default.dynParam(4, new Object[]{ new Object[]{
                                              A484ContagemResultado_StatusDmn ,
                                              AV23TFContagemResultado_StatusDmn_Sels ,
                                              AV44DynamicFiltersSelector1 ,
                                              AV45DynamicFiltersOperator1 ,
                                              AV46ContagemResultado_Demanda1 ,
                                              AV47ContagemResultado_DataDmn1 ,
                                              AV48ContagemResultado_DataDmn_To1 ,
                                              AV49ContagemResultado_StatusDmn1 ,
                                              AV50DynamicFiltersEnabled2 ,
                                              AV51DynamicFiltersSelector2 ,
                                              AV52DynamicFiltersOperator2 ,
                                              AV53ContagemResultado_Demanda2 ,
                                              AV54ContagemResultado_DataDmn2 ,
                                              AV55ContagemResultado_DataDmn_To2 ,
                                              AV56ContagemResultado_StatusDmn2 ,
                                              AV57DynamicFiltersEnabled3 ,
                                              AV58DynamicFiltersSelector3 ,
                                              AV59DynamicFiltersOperator3 ,
                                              AV60ContagemResultado_Demanda3 ,
                                              AV61ContagemResultado_DataDmn3 ,
                                              AV62ContagemResultado_DataDmn_To3 ,
                                              AV63ContagemResultado_StatusDmn3 ,
                                              AV11TFContagemResultado_DemandaFM_Sel ,
                                              AV10TFContagemResultado_DemandaFM ,
                                              AV13TFContagemResultado_Demanda_Sel ,
                                              AV12TFContagemResultado_Demanda ,
                                              AV14TFContagemResultado_DataDmn ,
                                              AV15TFContagemResultado_DataDmn_To ,
                                              AV17TFContagemResultado_ContratadaPessoaNom_Sel ,
                                              AV16TFContagemResultado_ContratadaPessoaNom ,
                                              AV19TFContagemResultado_Servico_Sel ,
                                              AV18TFContagemResultado_Servico ,
                                              AV21TFContagemrResultado_SistemaSigla_Sel ,
                                              AV20TFContagemrResultado_SistemaSigla ,
                                              AV23TFContagemResultado_StatusDmn_Sels.Count ,
                                              AV9WWPContext.gxTpr_Contratada_codigo ,
                                              A457ContagemResultado_Demanda ,
                                              A493ContagemResultado_DemandaFM ,
                                              A471ContagemResultado_DataDmn ,
                                              A500ContagemResultado_ContratadaPessoaNom ,
                                              A605Servico_Sigla ,
                                              A601ContagemResultado_Servico ,
                                              A509ContagemrResultado_SistemaSigla ,
                                              A490ContagemResultado_ContratadaCod ,
                                              A52Contratada_AreaTrabalhoCod ,
                                              AV9WWPContext.gxTpr_Areatrabalho_codigo ,
                                              A1583ContagemResultado_TipoRegistro },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.DATE, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT,
                                              TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.SHORT
                                              }
         });
         lV46ContagemResultado_Demanda1 = StringUtil.PadR( StringUtil.RTrim( AV46ContagemResultado_Demanda1), 50, "%");
         lV46ContagemResultado_Demanda1 = StringUtil.PadR( StringUtil.RTrim( AV46ContagemResultado_Demanda1), 50, "%");
         lV46ContagemResultado_Demanda1 = StringUtil.PadR( StringUtil.RTrim( AV46ContagemResultado_Demanda1), 50, "%");
         lV46ContagemResultado_Demanda1 = StringUtil.PadR( StringUtil.RTrim( AV46ContagemResultado_Demanda1), 50, "%");
         lV53ContagemResultado_Demanda2 = StringUtil.PadR( StringUtil.RTrim( AV53ContagemResultado_Demanda2), 50, "%");
         lV53ContagemResultado_Demanda2 = StringUtil.PadR( StringUtil.RTrim( AV53ContagemResultado_Demanda2), 50, "%");
         lV53ContagemResultado_Demanda2 = StringUtil.PadR( StringUtil.RTrim( AV53ContagemResultado_Demanda2), 50, "%");
         lV53ContagemResultado_Demanda2 = StringUtil.PadR( StringUtil.RTrim( AV53ContagemResultado_Demanda2), 50, "%");
         lV60ContagemResultado_Demanda3 = StringUtil.PadR( StringUtil.RTrim( AV60ContagemResultado_Demanda3), 50, "%");
         lV60ContagemResultado_Demanda3 = StringUtil.PadR( StringUtil.RTrim( AV60ContagemResultado_Demanda3), 50, "%");
         lV60ContagemResultado_Demanda3 = StringUtil.PadR( StringUtil.RTrim( AV60ContagemResultado_Demanda3), 50, "%");
         lV60ContagemResultado_Demanda3 = StringUtil.PadR( StringUtil.RTrim( AV60ContagemResultado_Demanda3), 50, "%");
         lV10TFContagemResultado_DemandaFM = StringUtil.Concat( StringUtil.RTrim( AV10TFContagemResultado_DemandaFM), "%", "");
         lV12TFContagemResultado_Demanda = StringUtil.Concat( StringUtil.RTrim( AV12TFContagemResultado_Demanda), "%", "");
         lV16TFContagemResultado_ContratadaPessoaNom = StringUtil.PadR( StringUtil.RTrim( AV16TFContagemResultado_ContratadaPessoaNom), 100, "%");
         lV18TFContagemResultado_Servico = StringUtil.PadR( StringUtil.RTrim( AV18TFContagemResultado_Servico), 15, "%");
         lV20TFContagemrResultado_SistemaSigla = StringUtil.PadR( StringUtil.RTrim( AV20TFContagemrResultado_SistemaSigla), 25, "%");
         /* Using cursor P00S46 */
         pr_default.execute(4, new Object[] {AV9WWPContext.gxTpr_Areatrabalho_codigo, AV46ContagemResultado_Demanda1, AV46ContagemResultado_Demanda1, lV46ContagemResultado_Demanda1, lV46ContagemResultado_Demanda1, lV46ContagemResultado_Demanda1, lV46ContagemResultado_Demanda1, AV47ContagemResultado_DataDmn1, AV48ContagemResultado_DataDmn_To1, AV49ContagemResultado_StatusDmn1, AV53ContagemResultado_Demanda2, AV53ContagemResultado_Demanda2, lV53ContagemResultado_Demanda2, lV53ContagemResultado_Demanda2, lV53ContagemResultado_Demanda2, lV53ContagemResultado_Demanda2, AV54ContagemResultado_DataDmn2, AV55ContagemResultado_DataDmn_To2, AV56ContagemResultado_StatusDmn2, AV60ContagemResultado_Demanda3, AV60ContagemResultado_Demanda3, lV60ContagemResultado_Demanda3, lV60ContagemResultado_Demanda3, lV60ContagemResultado_Demanda3, lV60ContagemResultado_Demanda3, AV61ContagemResultado_DataDmn3, AV62ContagemResultado_DataDmn_To3, AV63ContagemResultado_StatusDmn3, lV10TFContagemResultado_DemandaFM, AV11TFContagemResultado_DemandaFM_Sel, lV12TFContagemResultado_Demanda, AV13TFContagemResultado_Demanda_Sel, AV14TFContagemResultado_DataDmn, AV15TFContagemResultado_DataDmn_To, lV16TFContagemResultado_ContratadaPessoaNom, AV17TFContagemResultado_ContratadaPessoaNom_Sel, lV18TFContagemResultado_Servico, AV19TFContagemResultado_Servico_Sel, lV20TFContagemrResultado_SistemaSigla, AV21TFContagemrResultado_SistemaSigla_Sel, AV9WWPContext.gxTpr_Contratada_codigo});
         while ( (pr_default.getStatus(4) != 101) )
         {
            BRKS410 = false;
            A146Modulo_Codigo = P00S46_A146Modulo_Codigo[0];
            n146Modulo_Codigo = P00S46_n146Modulo_Codigo[0];
            A127Sistema_Codigo = P00S46_A127Sistema_Codigo[0];
            A135Sistema_AreaTrabalhoCod = P00S46_A135Sistema_AreaTrabalhoCod[0];
            A830AreaTrabalho_ServicoPadrao = P00S46_A830AreaTrabalho_ServicoPadrao[0];
            n830AreaTrabalho_ServicoPadrao = P00S46_n830AreaTrabalho_ServicoPadrao[0];
            A499ContagemResultado_ContratadaPessoaCod = P00S46_A499ContagemResultado_ContratadaPessoaCod[0];
            n499ContagemResultado_ContratadaPessoaCod = P00S46_n499ContagemResultado_ContratadaPessoaCod[0];
            A489ContagemResultado_SistemaCod = P00S46_A489ContagemResultado_SistemaCod[0];
            n489ContagemResultado_SistemaCod = P00S46_n489ContagemResultado_SistemaCod[0];
            A1553ContagemResultado_CntSrvCod = P00S46_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P00S46_n1553ContagemResultado_CntSrvCod[0];
            A52Contratada_AreaTrabalhoCod = P00S46_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = P00S46_n52Contratada_AreaTrabalhoCod[0];
            A1583ContagemResultado_TipoRegistro = P00S46_A1583ContagemResultado_TipoRegistro[0];
            A509ContagemrResultado_SistemaSigla = P00S46_A509ContagemrResultado_SistemaSigla[0];
            n509ContagemrResultado_SistemaSigla = P00S46_n509ContagemrResultado_SistemaSigla[0];
            A490ContagemResultado_ContratadaCod = P00S46_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = P00S46_n490ContagemResultado_ContratadaCod[0];
            A601ContagemResultado_Servico = P00S46_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P00S46_n601ContagemResultado_Servico[0];
            A605Servico_Sigla = P00S46_A605Servico_Sigla[0];
            n605Servico_Sigla = P00S46_n605Servico_Sigla[0];
            A500ContagemResultado_ContratadaPessoaNom = P00S46_A500ContagemResultado_ContratadaPessoaNom[0];
            n500ContagemResultado_ContratadaPessoaNom = P00S46_n500ContagemResultado_ContratadaPessoaNom[0];
            A484ContagemResultado_StatusDmn = P00S46_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P00S46_n484ContagemResultado_StatusDmn[0];
            A471ContagemResultado_DataDmn = P00S46_A471ContagemResultado_DataDmn[0];
            A493ContagemResultado_DemandaFM = P00S46_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = P00S46_n493ContagemResultado_DemandaFM[0];
            A457ContagemResultado_Demanda = P00S46_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = P00S46_n457ContagemResultado_Demanda[0];
            A456ContagemResultado_Codigo = P00S46_A456ContagemResultado_Codigo[0];
            A127Sistema_Codigo = P00S46_A127Sistema_Codigo[0];
            A135Sistema_AreaTrabalhoCod = P00S46_A135Sistema_AreaTrabalhoCod[0];
            A830AreaTrabalho_ServicoPadrao = P00S46_A830AreaTrabalho_ServicoPadrao[0];
            n830AreaTrabalho_ServicoPadrao = P00S46_n830AreaTrabalho_ServicoPadrao[0];
            A605Servico_Sigla = P00S46_A605Servico_Sigla[0];
            n605Servico_Sigla = P00S46_n605Servico_Sigla[0];
            A509ContagemrResultado_SistemaSigla = P00S46_A509ContagemrResultado_SistemaSigla[0];
            n509ContagemrResultado_SistemaSigla = P00S46_n509ContagemrResultado_SistemaSigla[0];
            A601ContagemResultado_Servico = P00S46_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P00S46_n601ContagemResultado_Servico[0];
            A499ContagemResultado_ContratadaPessoaCod = P00S46_A499ContagemResultado_ContratadaPessoaCod[0];
            n499ContagemResultado_ContratadaPessoaCod = P00S46_n499ContagemResultado_ContratadaPessoaCod[0];
            A52Contratada_AreaTrabalhoCod = P00S46_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = P00S46_n52Contratada_AreaTrabalhoCod[0];
            A500ContagemResultado_ContratadaPessoaNom = P00S46_A500ContagemResultado_ContratadaPessoaNom[0];
            n500ContagemResultado_ContratadaPessoaNom = P00S46_n500ContagemResultado_ContratadaPessoaNom[0];
            AV36count = 0;
            while ( (pr_default.getStatus(4) != 101) && ( StringUtil.StrCmp(P00S46_A509ContagemrResultado_SistemaSigla[0], A509ContagemrResultado_SistemaSigla) == 0 ) )
            {
               BRKS410 = false;
               A489ContagemResultado_SistemaCod = P00S46_A489ContagemResultado_SistemaCod[0];
               n489ContagemResultado_SistemaCod = P00S46_n489ContagemResultado_SistemaCod[0];
               A456ContagemResultado_Codigo = P00S46_A456ContagemResultado_Codigo[0];
               AV36count = (long)(AV36count+1);
               BRKS410 = true;
               pr_default.readNext(4);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A509ContagemrResultado_SistemaSigla)) )
            {
               AV28Option = A509ContagemrResultado_SistemaSigla;
               AV29Options.Add(AV28Option, 0);
               AV34OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV36count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV29Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKS410 )
            {
               BRKS410 = true;
               pr_default.readNext(4);
            }
         }
         pr_default.close(4);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV29Options = new GxSimpleCollection();
         AV32OptionsDesc = new GxSimpleCollection();
         AV34OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV37Session = context.GetSession();
         AV39GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV40GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFContagemResultado_DemandaFM = "";
         AV11TFContagemResultado_DemandaFM_Sel = "";
         AV12TFContagemResultado_Demanda = "";
         AV13TFContagemResultado_Demanda_Sel = "";
         AV14TFContagemResultado_DataDmn = DateTime.MinValue;
         AV15TFContagemResultado_DataDmn_To = DateTime.MinValue;
         AV16TFContagemResultado_ContratadaPessoaNom = "";
         AV17TFContagemResultado_ContratadaPessoaNom_Sel = "";
         AV18TFContagemResultado_Servico = "";
         AV20TFContagemrResultado_SistemaSigla = "";
         AV21TFContagemrResultado_SistemaSigla_Sel = "";
         AV22TFContagemResultado_StatusDmn_SelsJson = "";
         AV23TFContagemResultado_StatusDmn_Sels = new GxSimpleCollection();
         AV41GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV44DynamicFiltersSelector1 = "";
         AV46ContagemResultado_Demanda1 = "";
         AV47ContagemResultado_DataDmn1 = DateTime.MinValue;
         AV48ContagemResultado_DataDmn_To1 = DateTime.MinValue;
         AV49ContagemResultado_StatusDmn1 = "";
         AV51DynamicFiltersSelector2 = "";
         AV53ContagemResultado_Demanda2 = "";
         AV54ContagemResultado_DataDmn2 = DateTime.MinValue;
         AV55ContagemResultado_DataDmn_To2 = DateTime.MinValue;
         AV56ContagemResultado_StatusDmn2 = "";
         AV58DynamicFiltersSelector3 = "";
         AV60ContagemResultado_Demanda3 = "";
         AV61ContagemResultado_DataDmn3 = DateTime.MinValue;
         AV62ContagemResultado_DataDmn_To3 = DateTime.MinValue;
         AV63ContagemResultado_StatusDmn3 = "";
         scmdbuf = "";
         lV46ContagemResultado_Demanda1 = "";
         lV53ContagemResultado_Demanda2 = "";
         lV60ContagemResultado_Demanda3 = "";
         lV10TFContagemResultado_DemandaFM = "";
         lV12TFContagemResultado_Demanda = "";
         lV16TFContagemResultado_ContratadaPessoaNom = "";
         lV18TFContagemResultado_Servico = "";
         lV20TFContagemrResultado_SistemaSigla = "";
         A484ContagemResultado_StatusDmn = "";
         A457ContagemResultado_Demanda = "";
         A493ContagemResultado_DemandaFM = "";
         A471ContagemResultado_DataDmn = DateTime.MinValue;
         A500ContagemResultado_ContratadaPessoaNom = "";
         A605Servico_Sigla = "";
         A509ContagemrResultado_SistemaSigla = "";
         P00S42_A146Modulo_Codigo = new int[1] ;
         P00S42_n146Modulo_Codigo = new bool[] {false} ;
         P00S42_A127Sistema_Codigo = new int[1] ;
         P00S42_A135Sistema_AreaTrabalhoCod = new int[1] ;
         P00S42_A830AreaTrabalho_ServicoPadrao = new int[1] ;
         P00S42_n830AreaTrabalho_ServicoPadrao = new bool[] {false} ;
         P00S42_A499ContagemResultado_ContratadaPessoaCod = new int[1] ;
         P00S42_n499ContagemResultado_ContratadaPessoaCod = new bool[] {false} ;
         P00S42_A489ContagemResultado_SistemaCod = new int[1] ;
         P00S42_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         P00S42_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P00S42_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P00S42_A52Contratada_AreaTrabalhoCod = new int[1] ;
         P00S42_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         P00S42_A1583ContagemResultado_TipoRegistro = new short[1] ;
         P00S42_A493ContagemResultado_DemandaFM = new String[] {""} ;
         P00S42_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         P00S42_A490ContagemResultado_ContratadaCod = new int[1] ;
         P00S42_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P00S42_A509ContagemrResultado_SistemaSigla = new String[] {""} ;
         P00S42_n509ContagemrResultado_SistemaSigla = new bool[] {false} ;
         P00S42_A601ContagemResultado_Servico = new int[1] ;
         P00S42_n601ContagemResultado_Servico = new bool[] {false} ;
         P00S42_A605Servico_Sigla = new String[] {""} ;
         P00S42_n605Servico_Sigla = new bool[] {false} ;
         P00S42_A500ContagemResultado_ContratadaPessoaNom = new String[] {""} ;
         P00S42_n500ContagemResultado_ContratadaPessoaNom = new bool[] {false} ;
         P00S42_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P00S42_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P00S42_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         P00S42_A457ContagemResultado_Demanda = new String[] {""} ;
         P00S42_n457ContagemResultado_Demanda = new bool[] {false} ;
         P00S42_A456ContagemResultado_Codigo = new int[1] ;
         AV28Option = "";
         P00S43_A146Modulo_Codigo = new int[1] ;
         P00S43_n146Modulo_Codigo = new bool[] {false} ;
         P00S43_A127Sistema_Codigo = new int[1] ;
         P00S43_A135Sistema_AreaTrabalhoCod = new int[1] ;
         P00S43_A830AreaTrabalho_ServicoPadrao = new int[1] ;
         P00S43_n830AreaTrabalho_ServicoPadrao = new bool[] {false} ;
         P00S43_A499ContagemResultado_ContratadaPessoaCod = new int[1] ;
         P00S43_n499ContagemResultado_ContratadaPessoaCod = new bool[] {false} ;
         P00S43_A489ContagemResultado_SistemaCod = new int[1] ;
         P00S43_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         P00S43_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P00S43_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P00S43_A457ContagemResultado_Demanda = new String[] {""} ;
         P00S43_n457ContagemResultado_Demanda = new bool[] {false} ;
         P00S43_A1583ContagemResultado_TipoRegistro = new short[1] ;
         P00S43_A490ContagemResultado_ContratadaCod = new int[1] ;
         P00S43_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P00S43_A509ContagemrResultado_SistemaSigla = new String[] {""} ;
         P00S43_n509ContagemrResultado_SistemaSigla = new bool[] {false} ;
         P00S43_A601ContagemResultado_Servico = new int[1] ;
         P00S43_n601ContagemResultado_Servico = new bool[] {false} ;
         P00S43_A605Servico_Sigla = new String[] {""} ;
         P00S43_n605Servico_Sigla = new bool[] {false} ;
         P00S43_A500ContagemResultado_ContratadaPessoaNom = new String[] {""} ;
         P00S43_n500ContagemResultado_ContratadaPessoaNom = new bool[] {false} ;
         P00S43_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P00S43_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P00S43_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         P00S43_A493ContagemResultado_DemandaFM = new String[] {""} ;
         P00S43_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         P00S43_A52Contratada_AreaTrabalhoCod = new int[1] ;
         P00S43_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         P00S43_A456ContagemResultado_Codigo = new int[1] ;
         AV31OptionDesc = "";
         P00S44_A146Modulo_Codigo = new int[1] ;
         P00S44_n146Modulo_Codigo = new bool[] {false} ;
         P00S44_A127Sistema_Codigo = new int[1] ;
         P00S44_A135Sistema_AreaTrabalhoCod = new int[1] ;
         P00S44_A830AreaTrabalho_ServicoPadrao = new int[1] ;
         P00S44_n830AreaTrabalho_ServicoPadrao = new bool[] {false} ;
         P00S44_A499ContagemResultado_ContratadaPessoaCod = new int[1] ;
         P00S44_n499ContagemResultado_ContratadaPessoaCod = new bool[] {false} ;
         P00S44_A489ContagemResultado_SistemaCod = new int[1] ;
         P00S44_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         P00S44_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P00S44_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P00S44_A52Contratada_AreaTrabalhoCod = new int[1] ;
         P00S44_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         P00S44_A1583ContagemResultado_TipoRegistro = new short[1] ;
         P00S44_A500ContagemResultado_ContratadaPessoaNom = new String[] {""} ;
         P00S44_n500ContagemResultado_ContratadaPessoaNom = new bool[] {false} ;
         P00S44_A490ContagemResultado_ContratadaCod = new int[1] ;
         P00S44_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P00S44_A509ContagemrResultado_SistemaSigla = new String[] {""} ;
         P00S44_n509ContagemrResultado_SistemaSigla = new bool[] {false} ;
         P00S44_A601ContagemResultado_Servico = new int[1] ;
         P00S44_n601ContagemResultado_Servico = new bool[] {false} ;
         P00S44_A605Servico_Sigla = new String[] {""} ;
         P00S44_n605Servico_Sigla = new bool[] {false} ;
         P00S44_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P00S44_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P00S44_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         P00S44_A493ContagemResultado_DemandaFM = new String[] {""} ;
         P00S44_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         P00S44_A457ContagemResultado_Demanda = new String[] {""} ;
         P00S44_n457ContagemResultado_Demanda = new bool[] {false} ;
         P00S44_A456ContagemResultado_Codigo = new int[1] ;
         P00S45_A146Modulo_Codigo = new int[1] ;
         P00S45_n146Modulo_Codigo = new bool[] {false} ;
         P00S45_A127Sistema_Codigo = new int[1] ;
         P00S45_A135Sistema_AreaTrabalhoCod = new int[1] ;
         P00S45_A830AreaTrabalho_ServicoPadrao = new int[1] ;
         P00S45_n830AreaTrabalho_ServicoPadrao = new bool[] {false} ;
         P00S45_A499ContagemResultado_ContratadaPessoaCod = new int[1] ;
         P00S45_n499ContagemResultado_ContratadaPessoaCod = new bool[] {false} ;
         P00S45_A489ContagemResultado_SistemaCod = new int[1] ;
         P00S45_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         P00S45_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P00S45_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P00S45_A52Contratada_AreaTrabalhoCod = new int[1] ;
         P00S45_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         P00S45_A1583ContagemResultado_TipoRegistro = new short[1] ;
         P00S45_A601ContagemResultado_Servico = new int[1] ;
         P00S45_n601ContagemResultado_Servico = new bool[] {false} ;
         P00S45_A490ContagemResultado_ContratadaCod = new int[1] ;
         P00S45_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P00S45_A509ContagemrResultado_SistemaSigla = new String[] {""} ;
         P00S45_n509ContagemrResultado_SistemaSigla = new bool[] {false} ;
         P00S45_A605Servico_Sigla = new String[] {""} ;
         P00S45_n605Servico_Sigla = new bool[] {false} ;
         P00S45_A500ContagemResultado_ContratadaPessoaNom = new String[] {""} ;
         P00S45_n500ContagemResultado_ContratadaPessoaNom = new bool[] {false} ;
         P00S45_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P00S45_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P00S45_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         P00S45_A493ContagemResultado_DemandaFM = new String[] {""} ;
         P00S45_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         P00S45_A457ContagemResultado_Demanda = new String[] {""} ;
         P00S45_n457ContagemResultado_Demanda = new bool[] {false} ;
         P00S45_A456ContagemResultado_Codigo = new int[1] ;
         P00S46_A146Modulo_Codigo = new int[1] ;
         P00S46_n146Modulo_Codigo = new bool[] {false} ;
         P00S46_A127Sistema_Codigo = new int[1] ;
         P00S46_A135Sistema_AreaTrabalhoCod = new int[1] ;
         P00S46_A830AreaTrabalho_ServicoPadrao = new int[1] ;
         P00S46_n830AreaTrabalho_ServicoPadrao = new bool[] {false} ;
         P00S46_A499ContagemResultado_ContratadaPessoaCod = new int[1] ;
         P00S46_n499ContagemResultado_ContratadaPessoaCod = new bool[] {false} ;
         P00S46_A489ContagemResultado_SistemaCod = new int[1] ;
         P00S46_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         P00S46_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P00S46_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P00S46_A52Contratada_AreaTrabalhoCod = new int[1] ;
         P00S46_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         P00S46_A1583ContagemResultado_TipoRegistro = new short[1] ;
         P00S46_A509ContagemrResultado_SistemaSigla = new String[] {""} ;
         P00S46_n509ContagemrResultado_SistemaSigla = new bool[] {false} ;
         P00S46_A490ContagemResultado_ContratadaCod = new int[1] ;
         P00S46_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P00S46_A601ContagemResultado_Servico = new int[1] ;
         P00S46_n601ContagemResultado_Servico = new bool[] {false} ;
         P00S46_A605Servico_Sigla = new String[] {""} ;
         P00S46_n605Servico_Sigla = new bool[] {false} ;
         P00S46_A500ContagemResultado_ContratadaPessoaNom = new String[] {""} ;
         P00S46_n500ContagemResultado_ContratadaPessoaNom = new bool[] {false} ;
         P00S46_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P00S46_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P00S46_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         P00S46_A493ContagemResultado_DemandaFM = new String[] {""} ;
         P00S46_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         P00S46_A457ContagemResultado_Demanda = new String[] {""} ;
         P00S46_n457ContagemResultado_Demanda = new bool[] {false} ;
         P00S46_A456ContagemResultado_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getpromptcontagemresultadofilterdata__default(),
            new Object[][] {
                new Object[] {
               P00S42_A146Modulo_Codigo, P00S42_n146Modulo_Codigo, P00S42_A127Sistema_Codigo, P00S42_A135Sistema_AreaTrabalhoCod, P00S42_A830AreaTrabalho_ServicoPadrao, P00S42_n830AreaTrabalho_ServicoPadrao, P00S42_A499ContagemResultado_ContratadaPessoaCod, P00S42_n499ContagemResultado_ContratadaPessoaCod, P00S42_A489ContagemResultado_SistemaCod, P00S42_n489ContagemResultado_SistemaCod,
               P00S42_A1553ContagemResultado_CntSrvCod, P00S42_n1553ContagemResultado_CntSrvCod, P00S42_A52Contratada_AreaTrabalhoCod, P00S42_n52Contratada_AreaTrabalhoCod, P00S42_A1583ContagemResultado_TipoRegistro, P00S42_A493ContagemResultado_DemandaFM, P00S42_n493ContagemResultado_DemandaFM, P00S42_A490ContagemResultado_ContratadaCod, P00S42_n490ContagemResultado_ContratadaCod, P00S42_A509ContagemrResultado_SistemaSigla,
               P00S42_n509ContagemrResultado_SistemaSigla, P00S42_A601ContagemResultado_Servico, P00S42_n601ContagemResultado_Servico, P00S42_A605Servico_Sigla, P00S42_n605Servico_Sigla, P00S42_A500ContagemResultado_ContratadaPessoaNom, P00S42_n500ContagemResultado_ContratadaPessoaNom, P00S42_A484ContagemResultado_StatusDmn, P00S42_n484ContagemResultado_StatusDmn, P00S42_A471ContagemResultado_DataDmn,
               P00S42_A457ContagemResultado_Demanda, P00S42_n457ContagemResultado_Demanda, P00S42_A456ContagemResultado_Codigo
               }
               , new Object[] {
               P00S43_A146Modulo_Codigo, P00S43_n146Modulo_Codigo, P00S43_A127Sistema_Codigo, P00S43_A135Sistema_AreaTrabalhoCod, P00S43_A830AreaTrabalho_ServicoPadrao, P00S43_n830AreaTrabalho_ServicoPadrao, P00S43_A499ContagemResultado_ContratadaPessoaCod, P00S43_n499ContagemResultado_ContratadaPessoaCod, P00S43_A489ContagemResultado_SistemaCod, P00S43_n489ContagemResultado_SistemaCod,
               P00S43_A1553ContagemResultado_CntSrvCod, P00S43_n1553ContagemResultado_CntSrvCod, P00S43_A457ContagemResultado_Demanda, P00S43_n457ContagemResultado_Demanda, P00S43_A1583ContagemResultado_TipoRegistro, P00S43_A490ContagemResultado_ContratadaCod, P00S43_n490ContagemResultado_ContratadaCod, P00S43_A509ContagemrResultado_SistemaSigla, P00S43_n509ContagemrResultado_SistemaSigla, P00S43_A601ContagemResultado_Servico,
               P00S43_n601ContagemResultado_Servico, P00S43_A605Servico_Sigla, P00S43_n605Servico_Sigla, P00S43_A500ContagemResultado_ContratadaPessoaNom, P00S43_n500ContagemResultado_ContratadaPessoaNom, P00S43_A484ContagemResultado_StatusDmn, P00S43_n484ContagemResultado_StatusDmn, P00S43_A471ContagemResultado_DataDmn, P00S43_A493ContagemResultado_DemandaFM, P00S43_n493ContagemResultado_DemandaFM,
               P00S43_A52Contratada_AreaTrabalhoCod, P00S43_n52Contratada_AreaTrabalhoCod, P00S43_A456ContagemResultado_Codigo
               }
               , new Object[] {
               P00S44_A146Modulo_Codigo, P00S44_n146Modulo_Codigo, P00S44_A127Sistema_Codigo, P00S44_A135Sistema_AreaTrabalhoCod, P00S44_A830AreaTrabalho_ServicoPadrao, P00S44_n830AreaTrabalho_ServicoPadrao, P00S44_A499ContagemResultado_ContratadaPessoaCod, P00S44_n499ContagemResultado_ContratadaPessoaCod, P00S44_A489ContagemResultado_SistemaCod, P00S44_n489ContagemResultado_SistemaCod,
               P00S44_A1553ContagemResultado_CntSrvCod, P00S44_n1553ContagemResultado_CntSrvCod, P00S44_A52Contratada_AreaTrabalhoCod, P00S44_n52Contratada_AreaTrabalhoCod, P00S44_A1583ContagemResultado_TipoRegistro, P00S44_A500ContagemResultado_ContratadaPessoaNom, P00S44_n500ContagemResultado_ContratadaPessoaNom, P00S44_A490ContagemResultado_ContratadaCod, P00S44_n490ContagemResultado_ContratadaCod, P00S44_A509ContagemrResultado_SistemaSigla,
               P00S44_n509ContagemrResultado_SistemaSigla, P00S44_A601ContagemResultado_Servico, P00S44_n601ContagemResultado_Servico, P00S44_A605Servico_Sigla, P00S44_n605Servico_Sigla, P00S44_A484ContagemResultado_StatusDmn, P00S44_n484ContagemResultado_StatusDmn, P00S44_A471ContagemResultado_DataDmn, P00S44_A493ContagemResultado_DemandaFM, P00S44_n493ContagemResultado_DemandaFM,
               P00S44_A457ContagemResultado_Demanda, P00S44_n457ContagemResultado_Demanda, P00S44_A456ContagemResultado_Codigo
               }
               , new Object[] {
               P00S45_A146Modulo_Codigo, P00S45_n146Modulo_Codigo, P00S45_A127Sistema_Codigo, P00S45_A135Sistema_AreaTrabalhoCod, P00S45_A830AreaTrabalho_ServicoPadrao, P00S45_n830AreaTrabalho_ServicoPadrao, P00S45_A499ContagemResultado_ContratadaPessoaCod, P00S45_n499ContagemResultado_ContratadaPessoaCod, P00S45_A489ContagemResultado_SistemaCod, P00S45_n489ContagemResultado_SistemaCod,
               P00S45_A1553ContagemResultado_CntSrvCod, P00S45_n1553ContagemResultado_CntSrvCod, P00S45_A52Contratada_AreaTrabalhoCod, P00S45_n52Contratada_AreaTrabalhoCod, P00S45_A1583ContagemResultado_TipoRegistro, P00S45_A601ContagemResultado_Servico, P00S45_n601ContagemResultado_Servico, P00S45_A490ContagemResultado_ContratadaCod, P00S45_n490ContagemResultado_ContratadaCod, P00S45_A509ContagemrResultado_SistemaSigla,
               P00S45_n509ContagemrResultado_SistemaSigla, P00S45_A605Servico_Sigla, P00S45_n605Servico_Sigla, P00S45_A500ContagemResultado_ContratadaPessoaNom, P00S45_n500ContagemResultado_ContratadaPessoaNom, P00S45_A484ContagemResultado_StatusDmn, P00S45_n484ContagemResultado_StatusDmn, P00S45_A471ContagemResultado_DataDmn, P00S45_A493ContagemResultado_DemandaFM, P00S45_n493ContagemResultado_DemandaFM,
               P00S45_A457ContagemResultado_Demanda, P00S45_n457ContagemResultado_Demanda, P00S45_A456ContagemResultado_Codigo
               }
               , new Object[] {
               P00S46_A146Modulo_Codigo, P00S46_n146Modulo_Codigo, P00S46_A127Sistema_Codigo, P00S46_A135Sistema_AreaTrabalhoCod, P00S46_A830AreaTrabalho_ServicoPadrao, P00S46_n830AreaTrabalho_ServicoPadrao, P00S46_A499ContagemResultado_ContratadaPessoaCod, P00S46_n499ContagemResultado_ContratadaPessoaCod, P00S46_A489ContagemResultado_SistemaCod, P00S46_n489ContagemResultado_SistemaCod,
               P00S46_A1553ContagemResultado_CntSrvCod, P00S46_n1553ContagemResultado_CntSrvCod, P00S46_A52Contratada_AreaTrabalhoCod, P00S46_n52Contratada_AreaTrabalhoCod, P00S46_A1583ContagemResultado_TipoRegistro, P00S46_A509ContagemrResultado_SistemaSigla, P00S46_n509ContagemrResultado_SistemaSigla, P00S46_A490ContagemResultado_ContratadaCod, P00S46_n490ContagemResultado_ContratadaCod, P00S46_A601ContagemResultado_Servico,
               P00S46_n601ContagemResultado_Servico, P00S46_A605Servico_Sigla, P00S46_n605Servico_Sigla, P00S46_A500ContagemResultado_ContratadaPessoaNom, P00S46_n500ContagemResultado_ContratadaPessoaNom, P00S46_A484ContagemResultado_StatusDmn, P00S46_n484ContagemResultado_StatusDmn, P00S46_A471ContagemResultado_DataDmn, P00S46_A493ContagemResultado_DemandaFM, P00S46_n493ContagemResultado_DemandaFM,
               P00S46_A457ContagemResultado_Demanda, P00S46_n457ContagemResultado_Demanda, P00S46_A456ContagemResultado_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV45DynamicFiltersOperator1 ;
      private short AV52DynamicFiltersOperator2 ;
      private short AV59DynamicFiltersOperator3 ;
      private short A1583ContagemResultado_TipoRegistro ;
      private int AV66GXV1 ;
      private int AV42Contratada_AreaTrabalhoCod ;
      private int AV43ContagemResultado_ContratadaCod ;
      private int AV19TFContagemResultado_Servico_Sel ;
      private int AV23TFContagemResultado_StatusDmn_Sels_Count ;
      private int AV9WWPContext_gxTpr_Contratada_codigo ;
      private int AV9WWPContext_gxTpr_Areatrabalho_codigo ;
      private int A601ContagemResultado_Servico ;
      private int A490ContagemResultado_ContratadaCod ;
      private int A52Contratada_AreaTrabalhoCod ;
      private int A146Modulo_Codigo ;
      private int A127Sistema_Codigo ;
      private int A135Sistema_AreaTrabalhoCod ;
      private int A830AreaTrabalho_ServicoPadrao ;
      private int A499ContagemResultado_ContratadaPessoaCod ;
      private int A489ContagemResultado_SistemaCod ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A456ContagemResultado_Codigo ;
      private int AV27InsertIndex ;
      private long AV36count ;
      private String AV16TFContagemResultado_ContratadaPessoaNom ;
      private String AV17TFContagemResultado_ContratadaPessoaNom_Sel ;
      private String AV18TFContagemResultado_Servico ;
      private String AV20TFContagemrResultado_SistemaSigla ;
      private String AV21TFContagemrResultado_SistemaSigla_Sel ;
      private String AV46ContagemResultado_Demanda1 ;
      private String AV49ContagemResultado_StatusDmn1 ;
      private String AV53ContagemResultado_Demanda2 ;
      private String AV56ContagemResultado_StatusDmn2 ;
      private String AV60ContagemResultado_Demanda3 ;
      private String AV63ContagemResultado_StatusDmn3 ;
      private String scmdbuf ;
      private String lV46ContagemResultado_Demanda1 ;
      private String lV53ContagemResultado_Demanda2 ;
      private String lV60ContagemResultado_Demanda3 ;
      private String lV16TFContagemResultado_ContratadaPessoaNom ;
      private String lV18TFContagemResultado_Servico ;
      private String lV20TFContagemrResultado_SistemaSigla ;
      private String A484ContagemResultado_StatusDmn ;
      private String A500ContagemResultado_ContratadaPessoaNom ;
      private String A605Servico_Sigla ;
      private String A509ContagemrResultado_SistemaSigla ;
      private DateTime AV14TFContagemResultado_DataDmn ;
      private DateTime AV15TFContagemResultado_DataDmn_To ;
      private DateTime AV47ContagemResultado_DataDmn1 ;
      private DateTime AV48ContagemResultado_DataDmn_To1 ;
      private DateTime AV54ContagemResultado_DataDmn2 ;
      private DateTime AV55ContagemResultado_DataDmn_To2 ;
      private DateTime AV61ContagemResultado_DataDmn3 ;
      private DateTime AV62ContagemResultado_DataDmn_To3 ;
      private DateTime A471ContagemResultado_DataDmn ;
      private bool returnInSub ;
      private bool AV50DynamicFiltersEnabled2 ;
      private bool AV57DynamicFiltersEnabled3 ;
      private bool BRKS42 ;
      private bool n146Modulo_Codigo ;
      private bool n830AreaTrabalho_ServicoPadrao ;
      private bool n499ContagemResultado_ContratadaPessoaCod ;
      private bool n489ContagemResultado_SistemaCod ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n52Contratada_AreaTrabalhoCod ;
      private bool n493ContagemResultado_DemandaFM ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n509ContagemrResultado_SistemaSigla ;
      private bool n601ContagemResultado_Servico ;
      private bool n605Servico_Sigla ;
      private bool n500ContagemResultado_ContratadaPessoaNom ;
      private bool n484ContagemResultado_StatusDmn ;
      private bool n457ContagemResultado_Demanda ;
      private bool BRKS44 ;
      private bool BRKS46 ;
      private bool BRKS48 ;
      private bool BRKS410 ;
      private String AV35OptionIndexesJson ;
      private String AV30OptionsJson ;
      private String AV33OptionsDescJson ;
      private String AV22TFContagemResultado_StatusDmn_SelsJson ;
      private String AV26DDOName ;
      private String AV24SearchTxt ;
      private String AV25SearchTxtTo ;
      private String AV10TFContagemResultado_DemandaFM ;
      private String AV11TFContagemResultado_DemandaFM_Sel ;
      private String AV12TFContagemResultado_Demanda ;
      private String AV13TFContagemResultado_Demanda_Sel ;
      private String AV44DynamicFiltersSelector1 ;
      private String AV51DynamicFiltersSelector2 ;
      private String AV58DynamicFiltersSelector3 ;
      private String lV10TFContagemResultado_DemandaFM ;
      private String lV12TFContagemResultado_Demanda ;
      private String A457ContagemResultado_Demanda ;
      private String A493ContagemResultado_DemandaFM ;
      private String AV28Option ;
      private String AV31OptionDesc ;
      private IGxSession AV37Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00S42_A146Modulo_Codigo ;
      private bool[] P00S42_n146Modulo_Codigo ;
      private int[] P00S42_A127Sistema_Codigo ;
      private int[] P00S42_A135Sistema_AreaTrabalhoCod ;
      private int[] P00S42_A830AreaTrabalho_ServicoPadrao ;
      private bool[] P00S42_n830AreaTrabalho_ServicoPadrao ;
      private int[] P00S42_A499ContagemResultado_ContratadaPessoaCod ;
      private bool[] P00S42_n499ContagemResultado_ContratadaPessoaCod ;
      private int[] P00S42_A489ContagemResultado_SistemaCod ;
      private bool[] P00S42_n489ContagemResultado_SistemaCod ;
      private int[] P00S42_A1553ContagemResultado_CntSrvCod ;
      private bool[] P00S42_n1553ContagemResultado_CntSrvCod ;
      private int[] P00S42_A52Contratada_AreaTrabalhoCod ;
      private bool[] P00S42_n52Contratada_AreaTrabalhoCod ;
      private short[] P00S42_A1583ContagemResultado_TipoRegistro ;
      private String[] P00S42_A493ContagemResultado_DemandaFM ;
      private bool[] P00S42_n493ContagemResultado_DemandaFM ;
      private int[] P00S42_A490ContagemResultado_ContratadaCod ;
      private bool[] P00S42_n490ContagemResultado_ContratadaCod ;
      private String[] P00S42_A509ContagemrResultado_SistemaSigla ;
      private bool[] P00S42_n509ContagemrResultado_SistemaSigla ;
      private int[] P00S42_A601ContagemResultado_Servico ;
      private bool[] P00S42_n601ContagemResultado_Servico ;
      private String[] P00S42_A605Servico_Sigla ;
      private bool[] P00S42_n605Servico_Sigla ;
      private String[] P00S42_A500ContagemResultado_ContratadaPessoaNom ;
      private bool[] P00S42_n500ContagemResultado_ContratadaPessoaNom ;
      private String[] P00S42_A484ContagemResultado_StatusDmn ;
      private bool[] P00S42_n484ContagemResultado_StatusDmn ;
      private DateTime[] P00S42_A471ContagemResultado_DataDmn ;
      private String[] P00S42_A457ContagemResultado_Demanda ;
      private bool[] P00S42_n457ContagemResultado_Demanda ;
      private int[] P00S42_A456ContagemResultado_Codigo ;
      private int[] P00S43_A146Modulo_Codigo ;
      private bool[] P00S43_n146Modulo_Codigo ;
      private int[] P00S43_A127Sistema_Codigo ;
      private int[] P00S43_A135Sistema_AreaTrabalhoCod ;
      private int[] P00S43_A830AreaTrabalho_ServicoPadrao ;
      private bool[] P00S43_n830AreaTrabalho_ServicoPadrao ;
      private int[] P00S43_A499ContagemResultado_ContratadaPessoaCod ;
      private bool[] P00S43_n499ContagemResultado_ContratadaPessoaCod ;
      private int[] P00S43_A489ContagemResultado_SistemaCod ;
      private bool[] P00S43_n489ContagemResultado_SistemaCod ;
      private int[] P00S43_A1553ContagemResultado_CntSrvCod ;
      private bool[] P00S43_n1553ContagemResultado_CntSrvCod ;
      private String[] P00S43_A457ContagemResultado_Demanda ;
      private bool[] P00S43_n457ContagemResultado_Demanda ;
      private short[] P00S43_A1583ContagemResultado_TipoRegistro ;
      private int[] P00S43_A490ContagemResultado_ContratadaCod ;
      private bool[] P00S43_n490ContagemResultado_ContratadaCod ;
      private String[] P00S43_A509ContagemrResultado_SistemaSigla ;
      private bool[] P00S43_n509ContagemrResultado_SistemaSigla ;
      private int[] P00S43_A601ContagemResultado_Servico ;
      private bool[] P00S43_n601ContagemResultado_Servico ;
      private String[] P00S43_A605Servico_Sigla ;
      private bool[] P00S43_n605Servico_Sigla ;
      private String[] P00S43_A500ContagemResultado_ContratadaPessoaNom ;
      private bool[] P00S43_n500ContagemResultado_ContratadaPessoaNom ;
      private String[] P00S43_A484ContagemResultado_StatusDmn ;
      private bool[] P00S43_n484ContagemResultado_StatusDmn ;
      private DateTime[] P00S43_A471ContagemResultado_DataDmn ;
      private String[] P00S43_A493ContagemResultado_DemandaFM ;
      private bool[] P00S43_n493ContagemResultado_DemandaFM ;
      private int[] P00S43_A52Contratada_AreaTrabalhoCod ;
      private bool[] P00S43_n52Contratada_AreaTrabalhoCod ;
      private int[] P00S43_A456ContagemResultado_Codigo ;
      private int[] P00S44_A146Modulo_Codigo ;
      private bool[] P00S44_n146Modulo_Codigo ;
      private int[] P00S44_A127Sistema_Codigo ;
      private int[] P00S44_A135Sistema_AreaTrabalhoCod ;
      private int[] P00S44_A830AreaTrabalho_ServicoPadrao ;
      private bool[] P00S44_n830AreaTrabalho_ServicoPadrao ;
      private int[] P00S44_A499ContagemResultado_ContratadaPessoaCod ;
      private bool[] P00S44_n499ContagemResultado_ContratadaPessoaCod ;
      private int[] P00S44_A489ContagemResultado_SistemaCod ;
      private bool[] P00S44_n489ContagemResultado_SistemaCod ;
      private int[] P00S44_A1553ContagemResultado_CntSrvCod ;
      private bool[] P00S44_n1553ContagemResultado_CntSrvCod ;
      private int[] P00S44_A52Contratada_AreaTrabalhoCod ;
      private bool[] P00S44_n52Contratada_AreaTrabalhoCod ;
      private short[] P00S44_A1583ContagemResultado_TipoRegistro ;
      private String[] P00S44_A500ContagemResultado_ContratadaPessoaNom ;
      private bool[] P00S44_n500ContagemResultado_ContratadaPessoaNom ;
      private int[] P00S44_A490ContagemResultado_ContratadaCod ;
      private bool[] P00S44_n490ContagemResultado_ContratadaCod ;
      private String[] P00S44_A509ContagemrResultado_SistemaSigla ;
      private bool[] P00S44_n509ContagemrResultado_SistemaSigla ;
      private int[] P00S44_A601ContagemResultado_Servico ;
      private bool[] P00S44_n601ContagemResultado_Servico ;
      private String[] P00S44_A605Servico_Sigla ;
      private bool[] P00S44_n605Servico_Sigla ;
      private String[] P00S44_A484ContagemResultado_StatusDmn ;
      private bool[] P00S44_n484ContagemResultado_StatusDmn ;
      private DateTime[] P00S44_A471ContagemResultado_DataDmn ;
      private String[] P00S44_A493ContagemResultado_DemandaFM ;
      private bool[] P00S44_n493ContagemResultado_DemandaFM ;
      private String[] P00S44_A457ContagemResultado_Demanda ;
      private bool[] P00S44_n457ContagemResultado_Demanda ;
      private int[] P00S44_A456ContagemResultado_Codigo ;
      private int[] P00S45_A146Modulo_Codigo ;
      private bool[] P00S45_n146Modulo_Codigo ;
      private int[] P00S45_A127Sistema_Codigo ;
      private int[] P00S45_A135Sistema_AreaTrabalhoCod ;
      private int[] P00S45_A830AreaTrabalho_ServicoPadrao ;
      private bool[] P00S45_n830AreaTrabalho_ServicoPadrao ;
      private int[] P00S45_A499ContagemResultado_ContratadaPessoaCod ;
      private bool[] P00S45_n499ContagemResultado_ContratadaPessoaCod ;
      private int[] P00S45_A489ContagemResultado_SistemaCod ;
      private bool[] P00S45_n489ContagemResultado_SistemaCod ;
      private int[] P00S45_A1553ContagemResultado_CntSrvCod ;
      private bool[] P00S45_n1553ContagemResultado_CntSrvCod ;
      private int[] P00S45_A52Contratada_AreaTrabalhoCod ;
      private bool[] P00S45_n52Contratada_AreaTrabalhoCod ;
      private short[] P00S45_A1583ContagemResultado_TipoRegistro ;
      private int[] P00S45_A601ContagemResultado_Servico ;
      private bool[] P00S45_n601ContagemResultado_Servico ;
      private int[] P00S45_A490ContagemResultado_ContratadaCod ;
      private bool[] P00S45_n490ContagemResultado_ContratadaCod ;
      private String[] P00S45_A509ContagemrResultado_SistemaSigla ;
      private bool[] P00S45_n509ContagemrResultado_SistemaSigla ;
      private String[] P00S45_A605Servico_Sigla ;
      private bool[] P00S45_n605Servico_Sigla ;
      private String[] P00S45_A500ContagemResultado_ContratadaPessoaNom ;
      private bool[] P00S45_n500ContagemResultado_ContratadaPessoaNom ;
      private String[] P00S45_A484ContagemResultado_StatusDmn ;
      private bool[] P00S45_n484ContagemResultado_StatusDmn ;
      private DateTime[] P00S45_A471ContagemResultado_DataDmn ;
      private String[] P00S45_A493ContagemResultado_DemandaFM ;
      private bool[] P00S45_n493ContagemResultado_DemandaFM ;
      private String[] P00S45_A457ContagemResultado_Demanda ;
      private bool[] P00S45_n457ContagemResultado_Demanda ;
      private int[] P00S45_A456ContagemResultado_Codigo ;
      private int[] P00S46_A146Modulo_Codigo ;
      private bool[] P00S46_n146Modulo_Codigo ;
      private int[] P00S46_A127Sistema_Codigo ;
      private int[] P00S46_A135Sistema_AreaTrabalhoCod ;
      private int[] P00S46_A830AreaTrabalho_ServicoPadrao ;
      private bool[] P00S46_n830AreaTrabalho_ServicoPadrao ;
      private int[] P00S46_A499ContagemResultado_ContratadaPessoaCod ;
      private bool[] P00S46_n499ContagemResultado_ContratadaPessoaCod ;
      private int[] P00S46_A489ContagemResultado_SistemaCod ;
      private bool[] P00S46_n489ContagemResultado_SistemaCod ;
      private int[] P00S46_A1553ContagemResultado_CntSrvCod ;
      private bool[] P00S46_n1553ContagemResultado_CntSrvCod ;
      private int[] P00S46_A52Contratada_AreaTrabalhoCod ;
      private bool[] P00S46_n52Contratada_AreaTrabalhoCod ;
      private short[] P00S46_A1583ContagemResultado_TipoRegistro ;
      private String[] P00S46_A509ContagemrResultado_SistemaSigla ;
      private bool[] P00S46_n509ContagemrResultado_SistemaSigla ;
      private int[] P00S46_A490ContagemResultado_ContratadaCod ;
      private bool[] P00S46_n490ContagemResultado_ContratadaCod ;
      private int[] P00S46_A601ContagemResultado_Servico ;
      private bool[] P00S46_n601ContagemResultado_Servico ;
      private String[] P00S46_A605Servico_Sigla ;
      private bool[] P00S46_n605Servico_Sigla ;
      private String[] P00S46_A500ContagemResultado_ContratadaPessoaNom ;
      private bool[] P00S46_n500ContagemResultado_ContratadaPessoaNom ;
      private String[] P00S46_A484ContagemResultado_StatusDmn ;
      private bool[] P00S46_n484ContagemResultado_StatusDmn ;
      private DateTime[] P00S46_A471ContagemResultado_DataDmn ;
      private String[] P00S46_A493ContagemResultado_DemandaFM ;
      private bool[] P00S46_n493ContagemResultado_DemandaFM ;
      private String[] P00S46_A457ContagemResultado_Demanda ;
      private bool[] P00S46_n457ContagemResultado_Demanda ;
      private int[] P00S46_A456ContagemResultado_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV23TFContagemResultado_StatusDmn_Sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV29Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV32OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV34OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV39GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV40GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV41GridStateDynamicFilter ;
   }

   public class getpromptcontagemresultadofilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00S42( IGxContext context ,
                                             String A484ContagemResultado_StatusDmn ,
                                             IGxCollection AV23TFContagemResultado_StatusDmn_Sels ,
                                             String AV44DynamicFiltersSelector1 ,
                                             short AV45DynamicFiltersOperator1 ,
                                             String AV46ContagemResultado_Demanda1 ,
                                             DateTime AV47ContagemResultado_DataDmn1 ,
                                             DateTime AV48ContagemResultado_DataDmn_To1 ,
                                             String AV49ContagemResultado_StatusDmn1 ,
                                             bool AV50DynamicFiltersEnabled2 ,
                                             String AV51DynamicFiltersSelector2 ,
                                             short AV52DynamicFiltersOperator2 ,
                                             String AV53ContagemResultado_Demanda2 ,
                                             DateTime AV54ContagemResultado_DataDmn2 ,
                                             DateTime AV55ContagemResultado_DataDmn_To2 ,
                                             String AV56ContagemResultado_StatusDmn2 ,
                                             bool AV57DynamicFiltersEnabled3 ,
                                             String AV58DynamicFiltersSelector3 ,
                                             short AV59DynamicFiltersOperator3 ,
                                             String AV60ContagemResultado_Demanda3 ,
                                             DateTime AV61ContagemResultado_DataDmn3 ,
                                             DateTime AV62ContagemResultado_DataDmn_To3 ,
                                             String AV63ContagemResultado_StatusDmn3 ,
                                             String AV11TFContagemResultado_DemandaFM_Sel ,
                                             String AV10TFContagemResultado_DemandaFM ,
                                             String AV13TFContagemResultado_Demanda_Sel ,
                                             String AV12TFContagemResultado_Demanda ,
                                             DateTime AV14TFContagemResultado_DataDmn ,
                                             DateTime AV15TFContagemResultado_DataDmn_To ,
                                             String AV17TFContagemResultado_ContratadaPessoaNom_Sel ,
                                             String AV16TFContagemResultado_ContratadaPessoaNom ,
                                             int AV19TFContagemResultado_Servico_Sel ,
                                             String AV18TFContagemResultado_Servico ,
                                             String AV21TFContagemrResultado_SistemaSigla_Sel ,
                                             String AV20TFContagemrResultado_SistemaSigla ,
                                             int AV23TFContagemResultado_StatusDmn_Sels_Count ,
                                             int AV9WWPContext_gxTpr_Contratada_codigo ,
                                             String A457ContagemResultado_Demanda ,
                                             String A493ContagemResultado_DemandaFM ,
                                             DateTime A471ContagemResultado_DataDmn ,
                                             String A500ContagemResultado_ContratadaPessoaNom ,
                                             String A605Servico_Sigla ,
                                             int A601ContagemResultado_Servico ,
                                             String A509ContagemrResultado_SistemaSigla ,
                                             int A490ContagemResultado_ContratadaCod ,
                                             int A52Contratada_AreaTrabalhoCod ,
                                             int AV9WWPContext_gxTpr_Areatrabalho_codigo ,
                                             short A1583ContagemResultado_TipoRegistro )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [41] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[Modulo_Codigo], T2.[Sistema_Codigo], T3.[Sistema_AreaTrabalhoCod] AS Sistema_AreaTrabalhoCod, T4.[AreaTrabalho_ServicoPadrao] AS AreaTrabalho_ServicoPadrao, T8.[Contratada_PessoaCod] AS ContagemResultado_ContratadaPessoaCod, T1.[ContagemResultado_SistemaCod] AS ContagemResultado_SistemaCod, T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T8.[Contratada_AreaTrabalhoCod], T1.[ContagemResultado_TipoRegistro], T1.[ContagemResultado_DemandaFM], T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T6.[Sistema_Sigla] AS ContagemrResultado_SistemaSigla, T7.[Servico_Codigo] AS ContagemResultado_Servico, T5.[Servico_Sigla], T9.[Pessoa_Nome] AS ContagemResultado_ContratadaPessoaNom, T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_DataDmn], T1.[ContagemResultado_Demanda], T1.[ContagemResultado_Codigo] FROM (((((((([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [Modulo] T2 WITH (NOLOCK) ON T2.[Modulo_Codigo] = T1.[Modulo_Codigo]) LEFT JOIN [Sistema] T3 WITH (NOLOCK) ON T3.[Sistema_Codigo] = T2.[Sistema_Codigo]) LEFT JOIN [AreaTrabalho] T4 WITH (NOLOCK) ON T4.[AreaTrabalho_Codigo] = T3.[Sistema_AreaTrabalhoCod]) LEFT JOIN [Servico] T5 WITH (NOLOCK) ON T5.[Servico_Codigo] = T4.[AreaTrabalho_ServicoPadrao]) LEFT JOIN [Sistema] T6 WITH (NOLOCK) ON T6.[Sistema_Codigo] = T1.[ContagemResultado_SistemaCod]) LEFT JOIN [ContratoServicos] T7 WITH (NOLOCK) ON T7.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [Contratada] T8 WITH (NOLOCK) ON T8.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod]) LEFT JOIN [Pessoa] T9 WITH (NOLOCK) ON T9.[Pessoa_Codigo] = T8.[Contratada_PessoaCod])";
         scmdbuf = scmdbuf + " WHERE (T8.[Contratada_AreaTrabalhoCod] = @AV9WWPCo_1Areatrabalho_codigo)";
         scmdbuf = scmdbuf + " and (T1.[ContagemResultado_TipoRegistro] = 1)";
         if ( ( StringUtil.StrCmp(AV44DynamicFiltersSelector1, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV45DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV46ContagemResultado_Demanda1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV46ContagemResultado_Demanda1 or T1.[ContagemResultado_DemandaFM] = @AV46ContagemResultado_Demanda1)";
         }
         else
         {
            GXv_int1[1] = 1;
            GXv_int1[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV44DynamicFiltersSelector1, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV45DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV46ContagemResultado_Demanda1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV46ContagemResultado_Demanda1 or T1.[ContagemResultado_DemandaFM] like @lV46ContagemResultado_Demanda1)";
         }
         else
         {
            GXv_int1[3] = 1;
            GXv_int1[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV44DynamicFiltersSelector1, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV45DynamicFiltersOperator1 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV46ContagemResultado_Demanda1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like '%' + @lV46ContagemResultado_Demanda1 or T1.[ContagemResultado_DemandaFM] like '%' + @lV46ContagemResultado_Demanda1)";
         }
         else
         {
            GXv_int1[5] = 1;
            GXv_int1[6] = 1;
         }
         if ( ( StringUtil.StrCmp(AV44DynamicFiltersSelector1, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV47ContagemResultado_DataDmn1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV47ContagemResultado_DataDmn1)";
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( ( StringUtil.StrCmp(AV44DynamicFiltersSelector1, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV48ContagemResultado_DataDmn_To1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV48ContagemResultado_DataDmn_To1)";
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( ( StringUtil.StrCmp(AV44DynamicFiltersSelector1, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49ContagemResultado_StatusDmn1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = @AV49ContagemResultado_StatusDmn1)";
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( AV50DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV51DynamicFiltersSelector2, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV52DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53ContagemResultado_Demanda2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV53ContagemResultado_Demanda2 or T1.[ContagemResultado_DemandaFM] = @AV53ContagemResultado_Demanda2)";
         }
         else
         {
            GXv_int1[10] = 1;
            GXv_int1[11] = 1;
         }
         if ( AV50DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV51DynamicFiltersSelector2, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV52DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53ContagemResultado_Demanda2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV53ContagemResultado_Demanda2 or T1.[ContagemResultado_DemandaFM] like @lV53ContagemResultado_Demanda2)";
         }
         else
         {
            GXv_int1[12] = 1;
            GXv_int1[13] = 1;
         }
         if ( AV50DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV51DynamicFiltersSelector2, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV52DynamicFiltersOperator2 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53ContagemResultado_Demanda2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like '%' + @lV53ContagemResultado_Demanda2 or T1.[ContagemResultado_DemandaFM] like '%' + @lV53ContagemResultado_Demanda2)";
         }
         else
         {
            GXv_int1[14] = 1;
            GXv_int1[15] = 1;
         }
         if ( AV50DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV51DynamicFiltersSelector2, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV54ContagemResultado_DataDmn2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV54ContagemResultado_DataDmn2)";
         }
         else
         {
            GXv_int1[16] = 1;
         }
         if ( AV50DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV51DynamicFiltersSelector2, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV55ContagemResultado_DataDmn_To2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV55ContagemResultado_DataDmn_To2)";
         }
         else
         {
            GXv_int1[17] = 1;
         }
         if ( AV50DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV51DynamicFiltersSelector2, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56ContagemResultado_StatusDmn2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = @AV56ContagemResultado_StatusDmn2)";
         }
         else
         {
            GXv_int1[18] = 1;
         }
         if ( AV57DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV58DynamicFiltersSelector3, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV59DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60ContagemResultado_Demanda3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV60ContagemResultado_Demanda3 or T1.[ContagemResultado_DemandaFM] = @AV60ContagemResultado_Demanda3)";
         }
         else
         {
            GXv_int1[19] = 1;
            GXv_int1[20] = 1;
         }
         if ( AV57DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV58DynamicFiltersSelector3, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV59DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60ContagemResultado_Demanda3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV60ContagemResultado_Demanda3 or T1.[ContagemResultado_DemandaFM] like @lV60ContagemResultado_Demanda3)";
         }
         else
         {
            GXv_int1[21] = 1;
            GXv_int1[22] = 1;
         }
         if ( AV57DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV58DynamicFiltersSelector3, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV59DynamicFiltersOperator3 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60ContagemResultado_Demanda3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like '%' + @lV60ContagemResultado_Demanda3 or T1.[ContagemResultado_DemandaFM] like '%' + @lV60ContagemResultado_Demanda3)";
         }
         else
         {
            GXv_int1[23] = 1;
            GXv_int1[24] = 1;
         }
         if ( AV57DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV58DynamicFiltersSelector3, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV61ContagemResultado_DataDmn3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV61ContagemResultado_DataDmn3)";
         }
         else
         {
            GXv_int1[25] = 1;
         }
         if ( AV57DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV58DynamicFiltersSelector3, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV62ContagemResultado_DataDmn_To3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV62ContagemResultado_DataDmn_To3)";
         }
         else
         {
            GXv_int1[26] = 1;
         }
         if ( AV57DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV58DynamicFiltersSelector3, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63ContagemResultado_StatusDmn3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = @AV63ContagemResultado_StatusDmn3)";
         }
         else
         {
            GXv_int1[27] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFContagemResultado_DemandaFM_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFContagemResultado_DemandaFM)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DemandaFM] like @lV10TFContagemResultado_DemandaFM)";
         }
         else
         {
            GXv_int1[28] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFContagemResultado_DemandaFM_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DemandaFM] = @AV11TFContagemResultado_DemandaFM_Sel)";
         }
         else
         {
            GXv_int1[29] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFContagemResultado_Demanda_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFContagemResultado_Demanda)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV12TFContagemResultado_Demanda)";
         }
         else
         {
            GXv_int1[30] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFContagemResultado_Demanda_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV13TFContagemResultado_Demanda_Sel)";
         }
         else
         {
            GXv_int1[31] = 1;
         }
         if ( ! (DateTime.MinValue==AV14TFContagemResultado_DataDmn) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV14TFContagemResultado_DataDmn)";
         }
         else
         {
            GXv_int1[32] = 1;
         }
         if ( ! (DateTime.MinValue==AV15TFContagemResultado_DataDmn_To) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV15TFContagemResultado_DataDmn_To)";
         }
         else
         {
            GXv_int1[33] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV17TFContagemResultado_ContratadaPessoaNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV16TFContagemResultado_ContratadaPessoaNom)) ) )
         {
            sWhereString = sWhereString + " and (T9.[Pessoa_Nome] like @lV16TFContagemResultado_ContratadaPessoaNom)";
         }
         else
         {
            GXv_int1[34] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17TFContagemResultado_ContratadaPessoaNom_Sel)) )
         {
            sWhereString = sWhereString + " and (T9.[Pessoa_Nome] = @AV17TFContagemResultado_ContratadaPessoaNom_Sel)";
         }
         else
         {
            GXv_int1[35] = 1;
         }
         if ( (0==AV19TFContagemResultado_Servico_Sel) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18TFContagemResultado_Servico)) ) )
         {
            sWhereString = sWhereString + " and (T5.[Servico_Sigla] like @lV18TFContagemResultado_Servico)";
         }
         else
         {
            GXv_int1[36] = 1;
         }
         if ( ! (0==AV19TFContagemResultado_Servico_Sel) )
         {
            sWhereString = sWhereString + " and (T7.[Servico_Codigo] = @AV19TFContagemResultado_Servico_Sel)";
         }
         else
         {
            GXv_int1[37] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV21TFContagemrResultado_SistemaSigla_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV20TFContagemrResultado_SistemaSigla)) ) )
         {
            sWhereString = sWhereString + " and (T6.[Sistema_Sigla] like @lV20TFContagemrResultado_SistemaSigla)";
         }
         else
         {
            GXv_int1[38] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV21TFContagemrResultado_SistemaSigla_Sel)) )
         {
            sWhereString = sWhereString + " and (T6.[Sistema_Sigla] = @AV21TFContagemrResultado_SistemaSigla_Sel)";
         }
         else
         {
            GXv_int1[39] = 1;
         }
         if ( AV23TFContagemResultado_StatusDmn_Sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV23TFContagemResultado_StatusDmn_Sels, "T1.[ContagemResultado_StatusDmn] IN (", ")") + ")";
         }
         if ( AV9WWPContext_gxTpr_Contratada_codigo > 0 )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV9WWPCo_2Contratada_codigo)";
         }
         else
         {
            GXv_int1[40] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultado_DemandaFM]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00S43( IGxContext context ,
                                             String A484ContagemResultado_StatusDmn ,
                                             IGxCollection AV23TFContagemResultado_StatusDmn_Sels ,
                                             String AV44DynamicFiltersSelector1 ,
                                             short AV45DynamicFiltersOperator1 ,
                                             String AV46ContagemResultado_Demanda1 ,
                                             DateTime AV47ContagemResultado_DataDmn1 ,
                                             DateTime AV48ContagemResultado_DataDmn_To1 ,
                                             String AV49ContagemResultado_StatusDmn1 ,
                                             bool AV50DynamicFiltersEnabled2 ,
                                             String AV51DynamicFiltersSelector2 ,
                                             short AV52DynamicFiltersOperator2 ,
                                             String AV53ContagemResultado_Demanda2 ,
                                             DateTime AV54ContagemResultado_DataDmn2 ,
                                             DateTime AV55ContagemResultado_DataDmn_To2 ,
                                             String AV56ContagemResultado_StatusDmn2 ,
                                             bool AV57DynamicFiltersEnabled3 ,
                                             String AV58DynamicFiltersSelector3 ,
                                             short AV59DynamicFiltersOperator3 ,
                                             String AV60ContagemResultado_Demanda3 ,
                                             DateTime AV61ContagemResultado_DataDmn3 ,
                                             DateTime AV62ContagemResultado_DataDmn_To3 ,
                                             String AV63ContagemResultado_StatusDmn3 ,
                                             String AV11TFContagemResultado_DemandaFM_Sel ,
                                             String AV10TFContagemResultado_DemandaFM ,
                                             String AV13TFContagemResultado_Demanda_Sel ,
                                             String AV12TFContagemResultado_Demanda ,
                                             DateTime AV14TFContagemResultado_DataDmn ,
                                             DateTime AV15TFContagemResultado_DataDmn_To ,
                                             String AV17TFContagemResultado_ContratadaPessoaNom_Sel ,
                                             String AV16TFContagemResultado_ContratadaPessoaNom ,
                                             int AV19TFContagemResultado_Servico_Sel ,
                                             String AV18TFContagemResultado_Servico ,
                                             String AV21TFContagemrResultado_SistemaSigla_Sel ,
                                             String AV20TFContagemrResultado_SistemaSigla ,
                                             int AV23TFContagemResultado_StatusDmn_Sels_Count ,
                                             int AV9WWPContext_gxTpr_Contratada_codigo ,
                                             String A457ContagemResultado_Demanda ,
                                             String A493ContagemResultado_DemandaFM ,
                                             DateTime A471ContagemResultado_DataDmn ,
                                             String A500ContagemResultado_ContratadaPessoaNom ,
                                             String A605Servico_Sigla ,
                                             int A601ContagemResultado_Servico ,
                                             String A509ContagemrResultado_SistemaSigla ,
                                             int A490ContagemResultado_ContratadaCod ,
                                             int A52Contratada_AreaTrabalhoCod ,
                                             int AV9WWPContext_gxTpr_Areatrabalho_codigo ,
                                             short A1583ContagemResultado_TipoRegistro )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [41] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT T1.[Modulo_Codigo], T2.[Sistema_Codigo], T3.[Sistema_AreaTrabalhoCod] AS Sistema_AreaTrabalhoCod, T4.[AreaTrabalho_ServicoPadrao] AS AreaTrabalho_ServicoPadrao, T8.[Contratada_PessoaCod] AS ContagemResultado_ContratadaPessoaCod, T1.[ContagemResultado_SistemaCod] AS ContagemResultado_SistemaCod, T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T1.[ContagemResultado_Demanda], T1.[ContagemResultado_TipoRegistro], T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T6.[Sistema_Sigla] AS ContagemrResultado_SistemaSigla, T7.[Servico_Codigo] AS ContagemResultado_Servico, T5.[Servico_Sigla], T9.[Pessoa_Nome] AS ContagemResultado_ContratadaPessoaNom, T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_DataDmn], T1.[ContagemResultado_DemandaFM], T8.[Contratada_AreaTrabalhoCod], T1.[ContagemResultado_Codigo] FROM (((((((([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [Modulo] T2 WITH (NOLOCK) ON T2.[Modulo_Codigo] = T1.[Modulo_Codigo]) LEFT JOIN [Sistema] T3 WITH (NOLOCK) ON T3.[Sistema_Codigo] = T2.[Sistema_Codigo]) LEFT JOIN [AreaTrabalho] T4 WITH (NOLOCK) ON T4.[AreaTrabalho_Codigo] = T3.[Sistema_AreaTrabalhoCod]) LEFT JOIN [Servico] T5 WITH (NOLOCK) ON T5.[Servico_Codigo] = T4.[AreaTrabalho_ServicoPadrao]) LEFT JOIN [Sistema] T6 WITH (NOLOCK) ON T6.[Sistema_Codigo] = T1.[ContagemResultado_SistemaCod]) LEFT JOIN [ContratoServicos] T7 WITH (NOLOCK) ON T7.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [Contratada] T8 WITH (NOLOCK) ON T8.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod]) LEFT JOIN [Pessoa] T9 WITH (NOLOCK) ON T9.[Pessoa_Codigo] = T8.[Contratada_PessoaCod])";
         scmdbuf = scmdbuf + " WHERE (T8.[Contratada_AreaTrabalhoCod] = @AV9WWPCo_1Areatrabalho_codigo)";
         scmdbuf = scmdbuf + " and (T1.[ContagemResultado_TipoRegistro] = 1)";
         if ( ( StringUtil.StrCmp(AV44DynamicFiltersSelector1, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV45DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV46ContagemResultado_Demanda1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV46ContagemResultado_Demanda1 or T1.[ContagemResultado_DemandaFM] = @AV46ContagemResultado_Demanda1)";
         }
         else
         {
            GXv_int3[1] = 1;
            GXv_int3[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV44DynamicFiltersSelector1, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV45DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV46ContagemResultado_Demanda1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV46ContagemResultado_Demanda1 or T1.[ContagemResultado_DemandaFM] like @lV46ContagemResultado_Demanda1)";
         }
         else
         {
            GXv_int3[3] = 1;
            GXv_int3[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV44DynamicFiltersSelector1, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV45DynamicFiltersOperator1 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV46ContagemResultado_Demanda1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like '%' + @lV46ContagemResultado_Demanda1 or T1.[ContagemResultado_DemandaFM] like '%' + @lV46ContagemResultado_Demanda1)";
         }
         else
         {
            GXv_int3[5] = 1;
            GXv_int3[6] = 1;
         }
         if ( ( StringUtil.StrCmp(AV44DynamicFiltersSelector1, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV47ContagemResultado_DataDmn1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV47ContagemResultado_DataDmn1)";
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( ( StringUtil.StrCmp(AV44DynamicFiltersSelector1, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV48ContagemResultado_DataDmn_To1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV48ContagemResultado_DataDmn_To1)";
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( ( StringUtil.StrCmp(AV44DynamicFiltersSelector1, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49ContagemResultado_StatusDmn1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = @AV49ContagemResultado_StatusDmn1)";
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( AV50DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV51DynamicFiltersSelector2, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV52DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53ContagemResultado_Demanda2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV53ContagemResultado_Demanda2 or T1.[ContagemResultado_DemandaFM] = @AV53ContagemResultado_Demanda2)";
         }
         else
         {
            GXv_int3[10] = 1;
            GXv_int3[11] = 1;
         }
         if ( AV50DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV51DynamicFiltersSelector2, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV52DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53ContagemResultado_Demanda2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV53ContagemResultado_Demanda2 or T1.[ContagemResultado_DemandaFM] like @lV53ContagemResultado_Demanda2)";
         }
         else
         {
            GXv_int3[12] = 1;
            GXv_int3[13] = 1;
         }
         if ( AV50DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV51DynamicFiltersSelector2, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV52DynamicFiltersOperator2 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53ContagemResultado_Demanda2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like '%' + @lV53ContagemResultado_Demanda2 or T1.[ContagemResultado_DemandaFM] like '%' + @lV53ContagemResultado_Demanda2)";
         }
         else
         {
            GXv_int3[14] = 1;
            GXv_int3[15] = 1;
         }
         if ( AV50DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV51DynamicFiltersSelector2, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV54ContagemResultado_DataDmn2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV54ContagemResultado_DataDmn2)";
         }
         else
         {
            GXv_int3[16] = 1;
         }
         if ( AV50DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV51DynamicFiltersSelector2, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV55ContagemResultado_DataDmn_To2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV55ContagemResultado_DataDmn_To2)";
         }
         else
         {
            GXv_int3[17] = 1;
         }
         if ( AV50DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV51DynamicFiltersSelector2, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56ContagemResultado_StatusDmn2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = @AV56ContagemResultado_StatusDmn2)";
         }
         else
         {
            GXv_int3[18] = 1;
         }
         if ( AV57DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV58DynamicFiltersSelector3, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV59DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60ContagemResultado_Demanda3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV60ContagemResultado_Demanda3 or T1.[ContagemResultado_DemandaFM] = @AV60ContagemResultado_Demanda3)";
         }
         else
         {
            GXv_int3[19] = 1;
            GXv_int3[20] = 1;
         }
         if ( AV57DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV58DynamicFiltersSelector3, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV59DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60ContagemResultado_Demanda3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV60ContagemResultado_Demanda3 or T1.[ContagemResultado_DemandaFM] like @lV60ContagemResultado_Demanda3)";
         }
         else
         {
            GXv_int3[21] = 1;
            GXv_int3[22] = 1;
         }
         if ( AV57DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV58DynamicFiltersSelector3, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV59DynamicFiltersOperator3 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60ContagemResultado_Demanda3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like '%' + @lV60ContagemResultado_Demanda3 or T1.[ContagemResultado_DemandaFM] like '%' + @lV60ContagemResultado_Demanda3)";
         }
         else
         {
            GXv_int3[23] = 1;
            GXv_int3[24] = 1;
         }
         if ( AV57DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV58DynamicFiltersSelector3, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV61ContagemResultado_DataDmn3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV61ContagemResultado_DataDmn3)";
         }
         else
         {
            GXv_int3[25] = 1;
         }
         if ( AV57DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV58DynamicFiltersSelector3, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV62ContagemResultado_DataDmn_To3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV62ContagemResultado_DataDmn_To3)";
         }
         else
         {
            GXv_int3[26] = 1;
         }
         if ( AV57DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV58DynamicFiltersSelector3, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63ContagemResultado_StatusDmn3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = @AV63ContagemResultado_StatusDmn3)";
         }
         else
         {
            GXv_int3[27] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFContagemResultado_DemandaFM_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFContagemResultado_DemandaFM)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DemandaFM] like @lV10TFContagemResultado_DemandaFM)";
         }
         else
         {
            GXv_int3[28] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFContagemResultado_DemandaFM_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DemandaFM] = @AV11TFContagemResultado_DemandaFM_Sel)";
         }
         else
         {
            GXv_int3[29] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFContagemResultado_Demanda_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFContagemResultado_Demanda)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV12TFContagemResultado_Demanda)";
         }
         else
         {
            GXv_int3[30] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFContagemResultado_Demanda_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV13TFContagemResultado_Demanda_Sel)";
         }
         else
         {
            GXv_int3[31] = 1;
         }
         if ( ! (DateTime.MinValue==AV14TFContagemResultado_DataDmn) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV14TFContagemResultado_DataDmn)";
         }
         else
         {
            GXv_int3[32] = 1;
         }
         if ( ! (DateTime.MinValue==AV15TFContagemResultado_DataDmn_To) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV15TFContagemResultado_DataDmn_To)";
         }
         else
         {
            GXv_int3[33] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV17TFContagemResultado_ContratadaPessoaNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV16TFContagemResultado_ContratadaPessoaNom)) ) )
         {
            sWhereString = sWhereString + " and (T9.[Pessoa_Nome] like @lV16TFContagemResultado_ContratadaPessoaNom)";
         }
         else
         {
            GXv_int3[34] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17TFContagemResultado_ContratadaPessoaNom_Sel)) )
         {
            sWhereString = sWhereString + " and (T9.[Pessoa_Nome] = @AV17TFContagemResultado_ContratadaPessoaNom_Sel)";
         }
         else
         {
            GXv_int3[35] = 1;
         }
         if ( (0==AV19TFContagemResultado_Servico_Sel) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18TFContagemResultado_Servico)) ) )
         {
            sWhereString = sWhereString + " and (T5.[Servico_Sigla] like @lV18TFContagemResultado_Servico)";
         }
         else
         {
            GXv_int3[36] = 1;
         }
         if ( ! (0==AV19TFContagemResultado_Servico_Sel) )
         {
            sWhereString = sWhereString + " and (T7.[Servico_Codigo] = @AV19TFContagemResultado_Servico_Sel)";
         }
         else
         {
            GXv_int3[37] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV21TFContagemrResultado_SistemaSigla_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV20TFContagemrResultado_SistemaSigla)) ) )
         {
            sWhereString = sWhereString + " and (T6.[Sistema_Sigla] like @lV20TFContagemrResultado_SistemaSigla)";
         }
         else
         {
            GXv_int3[38] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV21TFContagemrResultado_SistemaSigla_Sel)) )
         {
            sWhereString = sWhereString + " and (T6.[Sistema_Sigla] = @AV21TFContagemrResultado_SistemaSigla_Sel)";
         }
         else
         {
            GXv_int3[39] = 1;
         }
         if ( AV23TFContagemResultado_StatusDmn_Sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV23TFContagemResultado_StatusDmn_Sels, "T1.[ContagemResultado_StatusDmn] IN (", ")") + ")";
         }
         if ( AV9WWPContext_gxTpr_Contratada_codigo > 0 )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV9WWPCo_2Contratada_codigo)";
         }
         else
         {
            GXv_int3[40] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultado_Demanda]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      protected Object[] conditional_P00S44( IGxContext context ,
                                             String A484ContagemResultado_StatusDmn ,
                                             IGxCollection AV23TFContagemResultado_StatusDmn_Sels ,
                                             String AV44DynamicFiltersSelector1 ,
                                             short AV45DynamicFiltersOperator1 ,
                                             String AV46ContagemResultado_Demanda1 ,
                                             DateTime AV47ContagemResultado_DataDmn1 ,
                                             DateTime AV48ContagemResultado_DataDmn_To1 ,
                                             String AV49ContagemResultado_StatusDmn1 ,
                                             bool AV50DynamicFiltersEnabled2 ,
                                             String AV51DynamicFiltersSelector2 ,
                                             short AV52DynamicFiltersOperator2 ,
                                             String AV53ContagemResultado_Demanda2 ,
                                             DateTime AV54ContagemResultado_DataDmn2 ,
                                             DateTime AV55ContagemResultado_DataDmn_To2 ,
                                             String AV56ContagemResultado_StatusDmn2 ,
                                             bool AV57DynamicFiltersEnabled3 ,
                                             String AV58DynamicFiltersSelector3 ,
                                             short AV59DynamicFiltersOperator3 ,
                                             String AV60ContagemResultado_Demanda3 ,
                                             DateTime AV61ContagemResultado_DataDmn3 ,
                                             DateTime AV62ContagemResultado_DataDmn_To3 ,
                                             String AV63ContagemResultado_StatusDmn3 ,
                                             String AV11TFContagemResultado_DemandaFM_Sel ,
                                             String AV10TFContagemResultado_DemandaFM ,
                                             String AV13TFContagemResultado_Demanda_Sel ,
                                             String AV12TFContagemResultado_Demanda ,
                                             DateTime AV14TFContagemResultado_DataDmn ,
                                             DateTime AV15TFContagemResultado_DataDmn_To ,
                                             String AV17TFContagemResultado_ContratadaPessoaNom_Sel ,
                                             String AV16TFContagemResultado_ContratadaPessoaNom ,
                                             int AV19TFContagemResultado_Servico_Sel ,
                                             String AV18TFContagemResultado_Servico ,
                                             String AV21TFContagemrResultado_SistemaSigla_Sel ,
                                             String AV20TFContagemrResultado_SistemaSigla ,
                                             int AV23TFContagemResultado_StatusDmn_Sels_Count ,
                                             int AV9WWPContext_gxTpr_Contratada_codigo ,
                                             String A457ContagemResultado_Demanda ,
                                             String A493ContagemResultado_DemandaFM ,
                                             DateTime A471ContagemResultado_DataDmn ,
                                             String A500ContagemResultado_ContratadaPessoaNom ,
                                             String A605Servico_Sigla ,
                                             int A601ContagemResultado_Servico ,
                                             String A509ContagemrResultado_SistemaSigla ,
                                             int A490ContagemResultado_ContratadaCod ,
                                             int A52Contratada_AreaTrabalhoCod ,
                                             int AV9WWPContext_gxTpr_Areatrabalho_codigo ,
                                             short A1583ContagemResultado_TipoRegistro )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int5 ;
         GXv_int5 = new short [41] ;
         Object[] GXv_Object6 ;
         GXv_Object6 = new Object [2] ;
         scmdbuf = "SELECT T1.[Modulo_Codigo], T2.[Sistema_Codigo], T3.[Sistema_AreaTrabalhoCod] AS Sistema_AreaTrabalhoCod, T4.[AreaTrabalho_ServicoPadrao] AS AreaTrabalho_ServicoPadrao, T8.[Contratada_PessoaCod] AS ContagemResultado_ContratadaPessoaCod, T1.[ContagemResultado_SistemaCod] AS ContagemResultado_SistemaCod, T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T8.[Contratada_AreaTrabalhoCod], T1.[ContagemResultado_TipoRegistro], T9.[Pessoa_Nome] AS ContagemResultado_ContratadaPessoaNom, T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T6.[Sistema_Sigla] AS ContagemrResultado_SistemaSigla, T7.[Servico_Codigo] AS ContagemResultado_Servico, T5.[Servico_Sigla], T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_DataDmn], T1.[ContagemResultado_DemandaFM], T1.[ContagemResultado_Demanda], T1.[ContagemResultado_Codigo] FROM (((((((([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [Modulo] T2 WITH (NOLOCK) ON T2.[Modulo_Codigo] = T1.[Modulo_Codigo]) LEFT JOIN [Sistema] T3 WITH (NOLOCK) ON T3.[Sistema_Codigo] = T2.[Sistema_Codigo]) LEFT JOIN [AreaTrabalho] T4 WITH (NOLOCK) ON T4.[AreaTrabalho_Codigo] = T3.[Sistema_AreaTrabalhoCod]) LEFT JOIN [Servico] T5 WITH (NOLOCK) ON T5.[Servico_Codigo] = T4.[AreaTrabalho_ServicoPadrao]) LEFT JOIN [Sistema] T6 WITH (NOLOCK) ON T6.[Sistema_Codigo] = T1.[ContagemResultado_SistemaCod]) LEFT JOIN [ContratoServicos] T7 WITH (NOLOCK) ON T7.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [Contratada] T8 WITH (NOLOCK) ON T8.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod]) LEFT JOIN [Pessoa] T9 WITH (NOLOCK) ON T9.[Pessoa_Codigo] = T8.[Contratada_PessoaCod])";
         scmdbuf = scmdbuf + " WHERE (T8.[Contratada_AreaTrabalhoCod] = @AV9WWPCo_1Areatrabalho_codigo)";
         scmdbuf = scmdbuf + " and (T1.[ContagemResultado_TipoRegistro] = 1)";
         if ( ( StringUtil.StrCmp(AV44DynamicFiltersSelector1, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV45DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV46ContagemResultado_Demanda1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV46ContagemResultado_Demanda1 or T1.[ContagemResultado_DemandaFM] = @AV46ContagemResultado_Demanda1)";
         }
         else
         {
            GXv_int5[1] = 1;
            GXv_int5[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV44DynamicFiltersSelector1, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV45DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV46ContagemResultado_Demanda1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV46ContagemResultado_Demanda1 or T1.[ContagemResultado_DemandaFM] like @lV46ContagemResultado_Demanda1)";
         }
         else
         {
            GXv_int5[3] = 1;
            GXv_int5[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV44DynamicFiltersSelector1, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV45DynamicFiltersOperator1 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV46ContagemResultado_Demanda1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like '%' + @lV46ContagemResultado_Demanda1 or T1.[ContagemResultado_DemandaFM] like '%' + @lV46ContagemResultado_Demanda1)";
         }
         else
         {
            GXv_int5[5] = 1;
            GXv_int5[6] = 1;
         }
         if ( ( StringUtil.StrCmp(AV44DynamicFiltersSelector1, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV47ContagemResultado_DataDmn1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV47ContagemResultado_DataDmn1)";
         }
         else
         {
            GXv_int5[7] = 1;
         }
         if ( ( StringUtil.StrCmp(AV44DynamicFiltersSelector1, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV48ContagemResultado_DataDmn_To1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV48ContagemResultado_DataDmn_To1)";
         }
         else
         {
            GXv_int5[8] = 1;
         }
         if ( ( StringUtil.StrCmp(AV44DynamicFiltersSelector1, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49ContagemResultado_StatusDmn1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = @AV49ContagemResultado_StatusDmn1)";
         }
         else
         {
            GXv_int5[9] = 1;
         }
         if ( AV50DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV51DynamicFiltersSelector2, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV52DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53ContagemResultado_Demanda2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV53ContagemResultado_Demanda2 or T1.[ContagemResultado_DemandaFM] = @AV53ContagemResultado_Demanda2)";
         }
         else
         {
            GXv_int5[10] = 1;
            GXv_int5[11] = 1;
         }
         if ( AV50DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV51DynamicFiltersSelector2, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV52DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53ContagemResultado_Demanda2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV53ContagemResultado_Demanda2 or T1.[ContagemResultado_DemandaFM] like @lV53ContagemResultado_Demanda2)";
         }
         else
         {
            GXv_int5[12] = 1;
            GXv_int5[13] = 1;
         }
         if ( AV50DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV51DynamicFiltersSelector2, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV52DynamicFiltersOperator2 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53ContagemResultado_Demanda2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like '%' + @lV53ContagemResultado_Demanda2 or T1.[ContagemResultado_DemandaFM] like '%' + @lV53ContagemResultado_Demanda2)";
         }
         else
         {
            GXv_int5[14] = 1;
            GXv_int5[15] = 1;
         }
         if ( AV50DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV51DynamicFiltersSelector2, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV54ContagemResultado_DataDmn2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV54ContagemResultado_DataDmn2)";
         }
         else
         {
            GXv_int5[16] = 1;
         }
         if ( AV50DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV51DynamicFiltersSelector2, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV55ContagemResultado_DataDmn_To2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV55ContagemResultado_DataDmn_To2)";
         }
         else
         {
            GXv_int5[17] = 1;
         }
         if ( AV50DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV51DynamicFiltersSelector2, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56ContagemResultado_StatusDmn2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = @AV56ContagemResultado_StatusDmn2)";
         }
         else
         {
            GXv_int5[18] = 1;
         }
         if ( AV57DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV58DynamicFiltersSelector3, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV59DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60ContagemResultado_Demanda3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV60ContagemResultado_Demanda3 or T1.[ContagemResultado_DemandaFM] = @AV60ContagemResultado_Demanda3)";
         }
         else
         {
            GXv_int5[19] = 1;
            GXv_int5[20] = 1;
         }
         if ( AV57DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV58DynamicFiltersSelector3, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV59DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60ContagemResultado_Demanda3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV60ContagemResultado_Demanda3 or T1.[ContagemResultado_DemandaFM] like @lV60ContagemResultado_Demanda3)";
         }
         else
         {
            GXv_int5[21] = 1;
            GXv_int5[22] = 1;
         }
         if ( AV57DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV58DynamicFiltersSelector3, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV59DynamicFiltersOperator3 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60ContagemResultado_Demanda3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like '%' + @lV60ContagemResultado_Demanda3 or T1.[ContagemResultado_DemandaFM] like '%' + @lV60ContagemResultado_Demanda3)";
         }
         else
         {
            GXv_int5[23] = 1;
            GXv_int5[24] = 1;
         }
         if ( AV57DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV58DynamicFiltersSelector3, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV61ContagemResultado_DataDmn3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV61ContagemResultado_DataDmn3)";
         }
         else
         {
            GXv_int5[25] = 1;
         }
         if ( AV57DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV58DynamicFiltersSelector3, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV62ContagemResultado_DataDmn_To3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV62ContagemResultado_DataDmn_To3)";
         }
         else
         {
            GXv_int5[26] = 1;
         }
         if ( AV57DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV58DynamicFiltersSelector3, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63ContagemResultado_StatusDmn3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = @AV63ContagemResultado_StatusDmn3)";
         }
         else
         {
            GXv_int5[27] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFContagemResultado_DemandaFM_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFContagemResultado_DemandaFM)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DemandaFM] like @lV10TFContagemResultado_DemandaFM)";
         }
         else
         {
            GXv_int5[28] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFContagemResultado_DemandaFM_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DemandaFM] = @AV11TFContagemResultado_DemandaFM_Sel)";
         }
         else
         {
            GXv_int5[29] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFContagemResultado_Demanda_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFContagemResultado_Demanda)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV12TFContagemResultado_Demanda)";
         }
         else
         {
            GXv_int5[30] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFContagemResultado_Demanda_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV13TFContagemResultado_Demanda_Sel)";
         }
         else
         {
            GXv_int5[31] = 1;
         }
         if ( ! (DateTime.MinValue==AV14TFContagemResultado_DataDmn) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV14TFContagemResultado_DataDmn)";
         }
         else
         {
            GXv_int5[32] = 1;
         }
         if ( ! (DateTime.MinValue==AV15TFContagemResultado_DataDmn_To) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV15TFContagemResultado_DataDmn_To)";
         }
         else
         {
            GXv_int5[33] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV17TFContagemResultado_ContratadaPessoaNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV16TFContagemResultado_ContratadaPessoaNom)) ) )
         {
            sWhereString = sWhereString + " and (T9.[Pessoa_Nome] like @lV16TFContagemResultado_ContratadaPessoaNom)";
         }
         else
         {
            GXv_int5[34] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17TFContagemResultado_ContratadaPessoaNom_Sel)) )
         {
            sWhereString = sWhereString + " and (T9.[Pessoa_Nome] = @AV17TFContagemResultado_ContratadaPessoaNom_Sel)";
         }
         else
         {
            GXv_int5[35] = 1;
         }
         if ( (0==AV19TFContagemResultado_Servico_Sel) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18TFContagemResultado_Servico)) ) )
         {
            sWhereString = sWhereString + " and (T5.[Servico_Sigla] like @lV18TFContagemResultado_Servico)";
         }
         else
         {
            GXv_int5[36] = 1;
         }
         if ( ! (0==AV19TFContagemResultado_Servico_Sel) )
         {
            sWhereString = sWhereString + " and (T7.[Servico_Codigo] = @AV19TFContagemResultado_Servico_Sel)";
         }
         else
         {
            GXv_int5[37] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV21TFContagemrResultado_SistemaSigla_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV20TFContagemrResultado_SistemaSigla)) ) )
         {
            sWhereString = sWhereString + " and (T6.[Sistema_Sigla] like @lV20TFContagemrResultado_SistemaSigla)";
         }
         else
         {
            GXv_int5[38] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV21TFContagemrResultado_SistemaSigla_Sel)) )
         {
            sWhereString = sWhereString + " and (T6.[Sistema_Sigla] = @AV21TFContagemrResultado_SistemaSigla_Sel)";
         }
         else
         {
            GXv_int5[39] = 1;
         }
         if ( AV23TFContagemResultado_StatusDmn_Sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV23TFContagemResultado_StatusDmn_Sels, "T1.[ContagemResultado_StatusDmn] IN (", ")") + ")";
         }
         if ( AV9WWPContext_gxTpr_Contratada_codigo > 0 )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV9WWPCo_2Contratada_codigo)";
         }
         else
         {
            GXv_int5[40] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T9.[Pessoa_Nome]";
         GXv_Object6[0] = scmdbuf;
         GXv_Object6[1] = GXv_int5;
         return GXv_Object6 ;
      }

      protected Object[] conditional_P00S45( IGxContext context ,
                                             String A484ContagemResultado_StatusDmn ,
                                             IGxCollection AV23TFContagemResultado_StatusDmn_Sels ,
                                             String AV44DynamicFiltersSelector1 ,
                                             short AV45DynamicFiltersOperator1 ,
                                             String AV46ContagemResultado_Demanda1 ,
                                             DateTime AV47ContagemResultado_DataDmn1 ,
                                             DateTime AV48ContagemResultado_DataDmn_To1 ,
                                             String AV49ContagemResultado_StatusDmn1 ,
                                             bool AV50DynamicFiltersEnabled2 ,
                                             String AV51DynamicFiltersSelector2 ,
                                             short AV52DynamicFiltersOperator2 ,
                                             String AV53ContagemResultado_Demanda2 ,
                                             DateTime AV54ContagemResultado_DataDmn2 ,
                                             DateTime AV55ContagemResultado_DataDmn_To2 ,
                                             String AV56ContagemResultado_StatusDmn2 ,
                                             bool AV57DynamicFiltersEnabled3 ,
                                             String AV58DynamicFiltersSelector3 ,
                                             short AV59DynamicFiltersOperator3 ,
                                             String AV60ContagemResultado_Demanda3 ,
                                             DateTime AV61ContagemResultado_DataDmn3 ,
                                             DateTime AV62ContagemResultado_DataDmn_To3 ,
                                             String AV63ContagemResultado_StatusDmn3 ,
                                             String AV11TFContagemResultado_DemandaFM_Sel ,
                                             String AV10TFContagemResultado_DemandaFM ,
                                             String AV13TFContagemResultado_Demanda_Sel ,
                                             String AV12TFContagemResultado_Demanda ,
                                             DateTime AV14TFContagemResultado_DataDmn ,
                                             DateTime AV15TFContagemResultado_DataDmn_To ,
                                             String AV17TFContagemResultado_ContratadaPessoaNom_Sel ,
                                             String AV16TFContagemResultado_ContratadaPessoaNom ,
                                             int AV19TFContagemResultado_Servico_Sel ,
                                             String AV18TFContagemResultado_Servico ,
                                             String AV21TFContagemrResultado_SistemaSigla_Sel ,
                                             String AV20TFContagemrResultado_SistemaSigla ,
                                             int AV23TFContagemResultado_StatusDmn_Sels_Count ,
                                             int AV9WWPContext_gxTpr_Contratada_codigo ,
                                             String A457ContagemResultado_Demanda ,
                                             String A493ContagemResultado_DemandaFM ,
                                             DateTime A471ContagemResultado_DataDmn ,
                                             String A500ContagemResultado_ContratadaPessoaNom ,
                                             String A605Servico_Sigla ,
                                             int A601ContagemResultado_Servico ,
                                             String A509ContagemrResultado_SistemaSigla ,
                                             int A490ContagemResultado_ContratadaCod ,
                                             int A52Contratada_AreaTrabalhoCod ,
                                             int AV9WWPContext_gxTpr_Areatrabalho_codigo ,
                                             short A1583ContagemResultado_TipoRegistro )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int7 ;
         GXv_int7 = new short [41] ;
         Object[] GXv_Object8 ;
         GXv_Object8 = new Object [2] ;
         scmdbuf = "SELECT T1.[Modulo_Codigo], T2.[Sistema_Codigo], T3.[Sistema_AreaTrabalhoCod] AS Sistema_AreaTrabalhoCod, T4.[AreaTrabalho_ServicoPadrao] AS AreaTrabalho_ServicoPadrao, T8.[Contratada_PessoaCod] AS ContagemResultado_ContratadaPessoaCod, T1.[ContagemResultado_SistemaCod] AS ContagemResultado_SistemaCod, T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T8.[Contratada_AreaTrabalhoCod], T1.[ContagemResultado_TipoRegistro], T7.[Servico_Codigo] AS ContagemResultado_Servico, T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T6.[Sistema_Sigla] AS ContagemrResultado_SistemaSigla, T5.[Servico_Sigla], T9.[Pessoa_Nome] AS ContagemResultado_ContratadaPessoaNom, T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_DataDmn], T1.[ContagemResultado_DemandaFM], T1.[ContagemResultado_Demanda], T1.[ContagemResultado_Codigo] FROM (((((((([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [Modulo] T2 WITH (NOLOCK) ON T2.[Modulo_Codigo] = T1.[Modulo_Codigo]) LEFT JOIN [Sistema] T3 WITH (NOLOCK) ON T3.[Sistema_Codigo] = T2.[Sistema_Codigo]) LEFT JOIN [AreaTrabalho] T4 WITH (NOLOCK) ON T4.[AreaTrabalho_Codigo] = T3.[Sistema_AreaTrabalhoCod]) LEFT JOIN [Servico] T5 WITH (NOLOCK) ON T5.[Servico_Codigo] = T4.[AreaTrabalho_ServicoPadrao]) LEFT JOIN [Sistema] T6 WITH (NOLOCK) ON T6.[Sistema_Codigo] = T1.[ContagemResultado_SistemaCod]) LEFT JOIN [ContratoServicos] T7 WITH (NOLOCK) ON T7.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [Contratada] T8 WITH (NOLOCK) ON T8.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod]) LEFT JOIN [Pessoa] T9 WITH (NOLOCK) ON T9.[Pessoa_Codigo] = T8.[Contratada_PessoaCod])";
         scmdbuf = scmdbuf + " WHERE (T8.[Contratada_AreaTrabalhoCod] = @AV9WWPCo_1Areatrabalho_codigo)";
         scmdbuf = scmdbuf + " and (T1.[ContagemResultado_TipoRegistro] = 1)";
         if ( ( StringUtil.StrCmp(AV44DynamicFiltersSelector1, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV45DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV46ContagemResultado_Demanda1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV46ContagemResultado_Demanda1 or T1.[ContagemResultado_DemandaFM] = @AV46ContagemResultado_Demanda1)";
         }
         else
         {
            GXv_int7[1] = 1;
            GXv_int7[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV44DynamicFiltersSelector1, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV45DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV46ContagemResultado_Demanda1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV46ContagemResultado_Demanda1 or T1.[ContagemResultado_DemandaFM] like @lV46ContagemResultado_Demanda1)";
         }
         else
         {
            GXv_int7[3] = 1;
            GXv_int7[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV44DynamicFiltersSelector1, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV45DynamicFiltersOperator1 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV46ContagemResultado_Demanda1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like '%' + @lV46ContagemResultado_Demanda1 or T1.[ContagemResultado_DemandaFM] like '%' + @lV46ContagemResultado_Demanda1)";
         }
         else
         {
            GXv_int7[5] = 1;
            GXv_int7[6] = 1;
         }
         if ( ( StringUtil.StrCmp(AV44DynamicFiltersSelector1, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV47ContagemResultado_DataDmn1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV47ContagemResultado_DataDmn1)";
         }
         else
         {
            GXv_int7[7] = 1;
         }
         if ( ( StringUtil.StrCmp(AV44DynamicFiltersSelector1, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV48ContagemResultado_DataDmn_To1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV48ContagemResultado_DataDmn_To1)";
         }
         else
         {
            GXv_int7[8] = 1;
         }
         if ( ( StringUtil.StrCmp(AV44DynamicFiltersSelector1, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49ContagemResultado_StatusDmn1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = @AV49ContagemResultado_StatusDmn1)";
         }
         else
         {
            GXv_int7[9] = 1;
         }
         if ( AV50DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV51DynamicFiltersSelector2, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV52DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53ContagemResultado_Demanda2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV53ContagemResultado_Demanda2 or T1.[ContagemResultado_DemandaFM] = @AV53ContagemResultado_Demanda2)";
         }
         else
         {
            GXv_int7[10] = 1;
            GXv_int7[11] = 1;
         }
         if ( AV50DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV51DynamicFiltersSelector2, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV52DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53ContagemResultado_Demanda2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV53ContagemResultado_Demanda2 or T1.[ContagemResultado_DemandaFM] like @lV53ContagemResultado_Demanda2)";
         }
         else
         {
            GXv_int7[12] = 1;
            GXv_int7[13] = 1;
         }
         if ( AV50DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV51DynamicFiltersSelector2, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV52DynamicFiltersOperator2 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53ContagemResultado_Demanda2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like '%' + @lV53ContagemResultado_Demanda2 or T1.[ContagemResultado_DemandaFM] like '%' + @lV53ContagemResultado_Demanda2)";
         }
         else
         {
            GXv_int7[14] = 1;
            GXv_int7[15] = 1;
         }
         if ( AV50DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV51DynamicFiltersSelector2, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV54ContagemResultado_DataDmn2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV54ContagemResultado_DataDmn2)";
         }
         else
         {
            GXv_int7[16] = 1;
         }
         if ( AV50DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV51DynamicFiltersSelector2, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV55ContagemResultado_DataDmn_To2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV55ContagemResultado_DataDmn_To2)";
         }
         else
         {
            GXv_int7[17] = 1;
         }
         if ( AV50DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV51DynamicFiltersSelector2, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56ContagemResultado_StatusDmn2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = @AV56ContagemResultado_StatusDmn2)";
         }
         else
         {
            GXv_int7[18] = 1;
         }
         if ( AV57DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV58DynamicFiltersSelector3, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV59DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60ContagemResultado_Demanda3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV60ContagemResultado_Demanda3 or T1.[ContagemResultado_DemandaFM] = @AV60ContagemResultado_Demanda3)";
         }
         else
         {
            GXv_int7[19] = 1;
            GXv_int7[20] = 1;
         }
         if ( AV57DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV58DynamicFiltersSelector3, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV59DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60ContagemResultado_Demanda3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV60ContagemResultado_Demanda3 or T1.[ContagemResultado_DemandaFM] like @lV60ContagemResultado_Demanda3)";
         }
         else
         {
            GXv_int7[21] = 1;
            GXv_int7[22] = 1;
         }
         if ( AV57DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV58DynamicFiltersSelector3, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV59DynamicFiltersOperator3 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60ContagemResultado_Demanda3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like '%' + @lV60ContagemResultado_Demanda3 or T1.[ContagemResultado_DemandaFM] like '%' + @lV60ContagemResultado_Demanda3)";
         }
         else
         {
            GXv_int7[23] = 1;
            GXv_int7[24] = 1;
         }
         if ( AV57DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV58DynamicFiltersSelector3, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV61ContagemResultado_DataDmn3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV61ContagemResultado_DataDmn3)";
         }
         else
         {
            GXv_int7[25] = 1;
         }
         if ( AV57DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV58DynamicFiltersSelector3, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV62ContagemResultado_DataDmn_To3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV62ContagemResultado_DataDmn_To3)";
         }
         else
         {
            GXv_int7[26] = 1;
         }
         if ( AV57DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV58DynamicFiltersSelector3, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63ContagemResultado_StatusDmn3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = @AV63ContagemResultado_StatusDmn3)";
         }
         else
         {
            GXv_int7[27] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFContagemResultado_DemandaFM_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFContagemResultado_DemandaFM)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DemandaFM] like @lV10TFContagemResultado_DemandaFM)";
         }
         else
         {
            GXv_int7[28] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFContagemResultado_DemandaFM_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DemandaFM] = @AV11TFContagemResultado_DemandaFM_Sel)";
         }
         else
         {
            GXv_int7[29] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFContagemResultado_Demanda_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFContagemResultado_Demanda)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV12TFContagemResultado_Demanda)";
         }
         else
         {
            GXv_int7[30] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFContagemResultado_Demanda_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV13TFContagemResultado_Demanda_Sel)";
         }
         else
         {
            GXv_int7[31] = 1;
         }
         if ( ! (DateTime.MinValue==AV14TFContagemResultado_DataDmn) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV14TFContagemResultado_DataDmn)";
         }
         else
         {
            GXv_int7[32] = 1;
         }
         if ( ! (DateTime.MinValue==AV15TFContagemResultado_DataDmn_To) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV15TFContagemResultado_DataDmn_To)";
         }
         else
         {
            GXv_int7[33] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV17TFContagemResultado_ContratadaPessoaNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV16TFContagemResultado_ContratadaPessoaNom)) ) )
         {
            sWhereString = sWhereString + " and (T9.[Pessoa_Nome] like @lV16TFContagemResultado_ContratadaPessoaNom)";
         }
         else
         {
            GXv_int7[34] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17TFContagemResultado_ContratadaPessoaNom_Sel)) )
         {
            sWhereString = sWhereString + " and (T9.[Pessoa_Nome] = @AV17TFContagemResultado_ContratadaPessoaNom_Sel)";
         }
         else
         {
            GXv_int7[35] = 1;
         }
         if ( (0==AV19TFContagemResultado_Servico_Sel) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18TFContagemResultado_Servico)) ) )
         {
            sWhereString = sWhereString + " and (T5.[Servico_Sigla] like @lV18TFContagemResultado_Servico)";
         }
         else
         {
            GXv_int7[36] = 1;
         }
         if ( ! (0==AV19TFContagemResultado_Servico_Sel) )
         {
            sWhereString = sWhereString + " and (T7.[Servico_Codigo] = @AV19TFContagemResultado_Servico_Sel)";
         }
         else
         {
            GXv_int7[37] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV21TFContagemrResultado_SistemaSigla_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV20TFContagemrResultado_SistemaSigla)) ) )
         {
            sWhereString = sWhereString + " and (T6.[Sistema_Sigla] like @lV20TFContagemrResultado_SistemaSigla)";
         }
         else
         {
            GXv_int7[38] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV21TFContagemrResultado_SistemaSigla_Sel)) )
         {
            sWhereString = sWhereString + " and (T6.[Sistema_Sigla] = @AV21TFContagemrResultado_SistemaSigla_Sel)";
         }
         else
         {
            GXv_int7[39] = 1;
         }
         if ( AV23TFContagemResultado_StatusDmn_Sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV23TFContagemResultado_StatusDmn_Sels, "T1.[ContagemResultado_StatusDmn] IN (", ")") + ")";
         }
         if ( AV9WWPContext_gxTpr_Contratada_codigo > 0 )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV9WWPCo_2Contratada_codigo)";
         }
         else
         {
            GXv_int7[40] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T7.[Servico_Codigo]";
         GXv_Object8[0] = scmdbuf;
         GXv_Object8[1] = GXv_int7;
         return GXv_Object8 ;
      }

      protected Object[] conditional_P00S46( IGxContext context ,
                                             String A484ContagemResultado_StatusDmn ,
                                             IGxCollection AV23TFContagemResultado_StatusDmn_Sels ,
                                             String AV44DynamicFiltersSelector1 ,
                                             short AV45DynamicFiltersOperator1 ,
                                             String AV46ContagemResultado_Demanda1 ,
                                             DateTime AV47ContagemResultado_DataDmn1 ,
                                             DateTime AV48ContagemResultado_DataDmn_To1 ,
                                             String AV49ContagemResultado_StatusDmn1 ,
                                             bool AV50DynamicFiltersEnabled2 ,
                                             String AV51DynamicFiltersSelector2 ,
                                             short AV52DynamicFiltersOperator2 ,
                                             String AV53ContagemResultado_Demanda2 ,
                                             DateTime AV54ContagemResultado_DataDmn2 ,
                                             DateTime AV55ContagemResultado_DataDmn_To2 ,
                                             String AV56ContagemResultado_StatusDmn2 ,
                                             bool AV57DynamicFiltersEnabled3 ,
                                             String AV58DynamicFiltersSelector3 ,
                                             short AV59DynamicFiltersOperator3 ,
                                             String AV60ContagemResultado_Demanda3 ,
                                             DateTime AV61ContagemResultado_DataDmn3 ,
                                             DateTime AV62ContagemResultado_DataDmn_To3 ,
                                             String AV63ContagemResultado_StatusDmn3 ,
                                             String AV11TFContagemResultado_DemandaFM_Sel ,
                                             String AV10TFContagemResultado_DemandaFM ,
                                             String AV13TFContagemResultado_Demanda_Sel ,
                                             String AV12TFContagemResultado_Demanda ,
                                             DateTime AV14TFContagemResultado_DataDmn ,
                                             DateTime AV15TFContagemResultado_DataDmn_To ,
                                             String AV17TFContagemResultado_ContratadaPessoaNom_Sel ,
                                             String AV16TFContagemResultado_ContratadaPessoaNom ,
                                             int AV19TFContagemResultado_Servico_Sel ,
                                             String AV18TFContagemResultado_Servico ,
                                             String AV21TFContagemrResultado_SistemaSigla_Sel ,
                                             String AV20TFContagemrResultado_SistemaSigla ,
                                             int AV23TFContagemResultado_StatusDmn_Sels_Count ,
                                             int AV9WWPContext_gxTpr_Contratada_codigo ,
                                             String A457ContagemResultado_Demanda ,
                                             String A493ContagemResultado_DemandaFM ,
                                             DateTime A471ContagemResultado_DataDmn ,
                                             String A500ContagemResultado_ContratadaPessoaNom ,
                                             String A605Servico_Sigla ,
                                             int A601ContagemResultado_Servico ,
                                             String A509ContagemrResultado_SistemaSigla ,
                                             int A490ContagemResultado_ContratadaCod ,
                                             int A52Contratada_AreaTrabalhoCod ,
                                             int AV9WWPContext_gxTpr_Areatrabalho_codigo ,
                                             short A1583ContagemResultado_TipoRegistro )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int9 ;
         GXv_int9 = new short [41] ;
         Object[] GXv_Object10 ;
         GXv_Object10 = new Object [2] ;
         scmdbuf = "SELECT T1.[Modulo_Codigo], T2.[Sistema_Codigo], T3.[Sistema_AreaTrabalhoCod] AS Sistema_AreaTrabalhoCod, T4.[AreaTrabalho_ServicoPadrao] AS AreaTrabalho_ServicoPadrao, T8.[Contratada_PessoaCod] AS ContagemResultado_ContratadaPessoaCod, T1.[ContagemResultado_SistemaCod] AS ContagemResultado_SistemaCod, T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T8.[Contratada_AreaTrabalhoCod], T1.[ContagemResultado_TipoRegistro], T6.[Sistema_Sigla] AS ContagemrResultado_SistemaSigla, T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T7.[Servico_Codigo] AS ContagemResultado_Servico, T5.[Servico_Sigla], T9.[Pessoa_Nome] AS ContagemResultado_ContratadaPessoaNom, T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_DataDmn], T1.[ContagemResultado_DemandaFM], T1.[ContagemResultado_Demanda], T1.[ContagemResultado_Codigo] FROM (((((((([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [Modulo] T2 WITH (NOLOCK) ON T2.[Modulo_Codigo] = T1.[Modulo_Codigo]) LEFT JOIN [Sistema] T3 WITH (NOLOCK) ON T3.[Sistema_Codigo] = T2.[Sistema_Codigo]) LEFT JOIN [AreaTrabalho] T4 WITH (NOLOCK) ON T4.[AreaTrabalho_Codigo] = T3.[Sistema_AreaTrabalhoCod]) LEFT JOIN [Servico] T5 WITH (NOLOCK) ON T5.[Servico_Codigo] = T4.[AreaTrabalho_ServicoPadrao]) LEFT JOIN [Sistema] T6 WITH (NOLOCK) ON T6.[Sistema_Codigo] = T1.[ContagemResultado_SistemaCod]) LEFT JOIN [ContratoServicos] T7 WITH (NOLOCK) ON T7.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [Contratada] T8 WITH (NOLOCK) ON T8.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod]) LEFT JOIN [Pessoa] T9 WITH (NOLOCK) ON T9.[Pessoa_Codigo] = T8.[Contratada_PessoaCod])";
         scmdbuf = scmdbuf + " WHERE (T8.[Contratada_AreaTrabalhoCod] = @AV9WWPCo_1Areatrabalho_codigo)";
         scmdbuf = scmdbuf + " and (T1.[ContagemResultado_TipoRegistro] = 1)";
         if ( ( StringUtil.StrCmp(AV44DynamicFiltersSelector1, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV45DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV46ContagemResultado_Demanda1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV46ContagemResultado_Demanda1 or T1.[ContagemResultado_DemandaFM] = @AV46ContagemResultado_Demanda1)";
         }
         else
         {
            GXv_int9[1] = 1;
            GXv_int9[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV44DynamicFiltersSelector1, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV45DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV46ContagemResultado_Demanda1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV46ContagemResultado_Demanda1 or T1.[ContagemResultado_DemandaFM] like @lV46ContagemResultado_Demanda1)";
         }
         else
         {
            GXv_int9[3] = 1;
            GXv_int9[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV44DynamicFiltersSelector1, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV45DynamicFiltersOperator1 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV46ContagemResultado_Demanda1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like '%' + @lV46ContagemResultado_Demanda1 or T1.[ContagemResultado_DemandaFM] like '%' + @lV46ContagemResultado_Demanda1)";
         }
         else
         {
            GXv_int9[5] = 1;
            GXv_int9[6] = 1;
         }
         if ( ( StringUtil.StrCmp(AV44DynamicFiltersSelector1, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV47ContagemResultado_DataDmn1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV47ContagemResultado_DataDmn1)";
         }
         else
         {
            GXv_int9[7] = 1;
         }
         if ( ( StringUtil.StrCmp(AV44DynamicFiltersSelector1, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV48ContagemResultado_DataDmn_To1) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV48ContagemResultado_DataDmn_To1)";
         }
         else
         {
            GXv_int9[8] = 1;
         }
         if ( ( StringUtil.StrCmp(AV44DynamicFiltersSelector1, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49ContagemResultado_StatusDmn1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = @AV49ContagemResultado_StatusDmn1)";
         }
         else
         {
            GXv_int9[9] = 1;
         }
         if ( AV50DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV51DynamicFiltersSelector2, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV52DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53ContagemResultado_Demanda2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV53ContagemResultado_Demanda2 or T1.[ContagemResultado_DemandaFM] = @AV53ContagemResultado_Demanda2)";
         }
         else
         {
            GXv_int9[10] = 1;
            GXv_int9[11] = 1;
         }
         if ( AV50DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV51DynamicFiltersSelector2, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV52DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53ContagemResultado_Demanda2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV53ContagemResultado_Demanda2 or T1.[ContagemResultado_DemandaFM] like @lV53ContagemResultado_Demanda2)";
         }
         else
         {
            GXv_int9[12] = 1;
            GXv_int9[13] = 1;
         }
         if ( AV50DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV51DynamicFiltersSelector2, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV52DynamicFiltersOperator2 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53ContagemResultado_Demanda2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like '%' + @lV53ContagemResultado_Demanda2 or T1.[ContagemResultado_DemandaFM] like '%' + @lV53ContagemResultado_Demanda2)";
         }
         else
         {
            GXv_int9[14] = 1;
            GXv_int9[15] = 1;
         }
         if ( AV50DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV51DynamicFiltersSelector2, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV54ContagemResultado_DataDmn2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV54ContagemResultado_DataDmn2)";
         }
         else
         {
            GXv_int9[16] = 1;
         }
         if ( AV50DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV51DynamicFiltersSelector2, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV55ContagemResultado_DataDmn_To2) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV55ContagemResultado_DataDmn_To2)";
         }
         else
         {
            GXv_int9[17] = 1;
         }
         if ( AV50DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV51DynamicFiltersSelector2, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56ContagemResultado_StatusDmn2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = @AV56ContagemResultado_StatusDmn2)";
         }
         else
         {
            GXv_int9[18] = 1;
         }
         if ( AV57DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV58DynamicFiltersSelector3, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV59DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60ContagemResultado_Demanda3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV60ContagemResultado_Demanda3 or T1.[ContagemResultado_DemandaFM] = @AV60ContagemResultado_Demanda3)";
         }
         else
         {
            GXv_int9[19] = 1;
            GXv_int9[20] = 1;
         }
         if ( AV57DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV58DynamicFiltersSelector3, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV59DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60ContagemResultado_Demanda3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV60ContagemResultado_Demanda3 or T1.[ContagemResultado_DemandaFM] like @lV60ContagemResultado_Demanda3)";
         }
         else
         {
            GXv_int9[21] = 1;
            GXv_int9[22] = 1;
         }
         if ( AV57DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV58DynamicFiltersSelector3, "CONTAGEMRESULTADO_DEMANDA") == 0 ) && ( AV59DynamicFiltersOperator3 == 2 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60ContagemResultado_Demanda3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like '%' + @lV60ContagemResultado_Demanda3 or T1.[ContagemResultado_DemandaFM] like '%' + @lV60ContagemResultado_Demanda3)";
         }
         else
         {
            GXv_int9[23] = 1;
            GXv_int9[24] = 1;
         }
         if ( AV57DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV58DynamicFiltersSelector3, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV61ContagemResultado_DataDmn3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV61ContagemResultado_DataDmn3)";
         }
         else
         {
            GXv_int9[25] = 1;
         }
         if ( AV57DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV58DynamicFiltersSelector3, "CONTAGEMRESULTADO_DATADMN") == 0 ) && ( ! (DateTime.MinValue==AV62ContagemResultado_DataDmn_To3) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV62ContagemResultado_DataDmn_To3)";
         }
         else
         {
            GXv_int9[26] = 1;
         }
         if ( AV57DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV58DynamicFiltersSelector3, "CONTAGEMRESULTADO_STATUSDMN") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63ContagemResultado_StatusDmn3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_StatusDmn] = @AV63ContagemResultado_StatusDmn3)";
         }
         else
         {
            GXv_int9[27] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFContagemResultado_DemandaFM_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFContagemResultado_DemandaFM)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DemandaFM] like @lV10TFContagemResultado_DemandaFM)";
         }
         else
         {
            GXv_int9[28] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFContagemResultado_DemandaFM_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DemandaFM] = @AV11TFContagemResultado_DemandaFM_Sel)";
         }
         else
         {
            GXv_int9[29] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFContagemResultado_Demanda_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFContagemResultado_Demanda)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] like @lV12TFContagemResultado_Demanda)";
         }
         else
         {
            GXv_int9[30] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFContagemResultado_Demanda_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_Demanda] = @AV13TFContagemResultado_Demanda_Sel)";
         }
         else
         {
            GXv_int9[31] = 1;
         }
         if ( ! (DateTime.MinValue==AV14TFContagemResultado_DataDmn) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] >= @AV14TFContagemResultado_DataDmn)";
         }
         else
         {
            GXv_int9[32] = 1;
         }
         if ( ! (DateTime.MinValue==AV15TFContagemResultado_DataDmn_To) )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_DataDmn] <= @AV15TFContagemResultado_DataDmn_To)";
         }
         else
         {
            GXv_int9[33] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV17TFContagemResultado_ContratadaPessoaNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV16TFContagemResultado_ContratadaPessoaNom)) ) )
         {
            sWhereString = sWhereString + " and (T9.[Pessoa_Nome] like @lV16TFContagemResultado_ContratadaPessoaNom)";
         }
         else
         {
            GXv_int9[34] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17TFContagemResultado_ContratadaPessoaNom_Sel)) )
         {
            sWhereString = sWhereString + " and (T9.[Pessoa_Nome] = @AV17TFContagemResultado_ContratadaPessoaNom_Sel)";
         }
         else
         {
            GXv_int9[35] = 1;
         }
         if ( (0==AV19TFContagemResultado_Servico_Sel) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18TFContagemResultado_Servico)) ) )
         {
            sWhereString = sWhereString + " and (T5.[Servico_Sigla] like @lV18TFContagemResultado_Servico)";
         }
         else
         {
            GXv_int9[36] = 1;
         }
         if ( ! (0==AV19TFContagemResultado_Servico_Sel) )
         {
            sWhereString = sWhereString + " and (T7.[Servico_Codigo] = @AV19TFContagemResultado_Servico_Sel)";
         }
         else
         {
            GXv_int9[37] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV21TFContagemrResultado_SistemaSigla_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV20TFContagemrResultado_SistemaSigla)) ) )
         {
            sWhereString = sWhereString + " and (T6.[Sistema_Sigla] like @lV20TFContagemrResultado_SistemaSigla)";
         }
         else
         {
            GXv_int9[38] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV21TFContagemrResultado_SistemaSigla_Sel)) )
         {
            sWhereString = sWhereString + " and (T6.[Sistema_Sigla] = @AV21TFContagemrResultado_SistemaSigla_Sel)";
         }
         else
         {
            GXv_int9[39] = 1;
         }
         if ( AV23TFContagemResultado_StatusDmn_Sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV23TFContagemResultado_StatusDmn_Sels, "T1.[ContagemResultado_StatusDmn] IN (", ")") + ")";
         }
         if ( AV9WWPContext_gxTpr_Contratada_codigo > 0 )
         {
            sWhereString = sWhereString + " and (T1.[ContagemResultado_ContratadaCod] = @AV9WWPCo_2Contratada_codigo)";
         }
         else
         {
            GXv_int9[40] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T6.[Sistema_Sigla]";
         GXv_Object10[0] = scmdbuf;
         GXv_Object10[1] = GXv_int9;
         return GXv_Object10 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00S42(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (short)dynConstraints[3] , (String)dynConstraints[4] , (DateTime)dynConstraints[5] , (DateTime)dynConstraints[6] , (String)dynConstraints[7] , (bool)dynConstraints[8] , (String)dynConstraints[9] , (short)dynConstraints[10] , (String)dynConstraints[11] , (DateTime)dynConstraints[12] , (DateTime)dynConstraints[13] , (String)dynConstraints[14] , (bool)dynConstraints[15] , (String)dynConstraints[16] , (short)dynConstraints[17] , (String)dynConstraints[18] , (DateTime)dynConstraints[19] , (DateTime)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (DateTime)dynConstraints[26] , (DateTime)dynConstraints[27] , (String)dynConstraints[28] , (String)dynConstraints[29] , (int)dynConstraints[30] , (String)dynConstraints[31] , (String)dynConstraints[32] , (String)dynConstraints[33] , (int)dynConstraints[34] , (int)dynConstraints[35] , (String)dynConstraints[36] , (String)dynConstraints[37] , (DateTime)dynConstraints[38] , (String)dynConstraints[39] , (String)dynConstraints[40] , (int)dynConstraints[41] , (String)dynConstraints[42] , (int)dynConstraints[43] , (int)dynConstraints[44] , (int)dynConstraints[45] , (short)dynConstraints[46] );
               case 1 :
                     return conditional_P00S43(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (short)dynConstraints[3] , (String)dynConstraints[4] , (DateTime)dynConstraints[5] , (DateTime)dynConstraints[6] , (String)dynConstraints[7] , (bool)dynConstraints[8] , (String)dynConstraints[9] , (short)dynConstraints[10] , (String)dynConstraints[11] , (DateTime)dynConstraints[12] , (DateTime)dynConstraints[13] , (String)dynConstraints[14] , (bool)dynConstraints[15] , (String)dynConstraints[16] , (short)dynConstraints[17] , (String)dynConstraints[18] , (DateTime)dynConstraints[19] , (DateTime)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (DateTime)dynConstraints[26] , (DateTime)dynConstraints[27] , (String)dynConstraints[28] , (String)dynConstraints[29] , (int)dynConstraints[30] , (String)dynConstraints[31] , (String)dynConstraints[32] , (String)dynConstraints[33] , (int)dynConstraints[34] , (int)dynConstraints[35] , (String)dynConstraints[36] , (String)dynConstraints[37] , (DateTime)dynConstraints[38] , (String)dynConstraints[39] , (String)dynConstraints[40] , (int)dynConstraints[41] , (String)dynConstraints[42] , (int)dynConstraints[43] , (int)dynConstraints[44] , (int)dynConstraints[45] , (short)dynConstraints[46] );
               case 2 :
                     return conditional_P00S44(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (short)dynConstraints[3] , (String)dynConstraints[4] , (DateTime)dynConstraints[5] , (DateTime)dynConstraints[6] , (String)dynConstraints[7] , (bool)dynConstraints[8] , (String)dynConstraints[9] , (short)dynConstraints[10] , (String)dynConstraints[11] , (DateTime)dynConstraints[12] , (DateTime)dynConstraints[13] , (String)dynConstraints[14] , (bool)dynConstraints[15] , (String)dynConstraints[16] , (short)dynConstraints[17] , (String)dynConstraints[18] , (DateTime)dynConstraints[19] , (DateTime)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (DateTime)dynConstraints[26] , (DateTime)dynConstraints[27] , (String)dynConstraints[28] , (String)dynConstraints[29] , (int)dynConstraints[30] , (String)dynConstraints[31] , (String)dynConstraints[32] , (String)dynConstraints[33] , (int)dynConstraints[34] , (int)dynConstraints[35] , (String)dynConstraints[36] , (String)dynConstraints[37] , (DateTime)dynConstraints[38] , (String)dynConstraints[39] , (String)dynConstraints[40] , (int)dynConstraints[41] , (String)dynConstraints[42] , (int)dynConstraints[43] , (int)dynConstraints[44] , (int)dynConstraints[45] , (short)dynConstraints[46] );
               case 3 :
                     return conditional_P00S45(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (short)dynConstraints[3] , (String)dynConstraints[4] , (DateTime)dynConstraints[5] , (DateTime)dynConstraints[6] , (String)dynConstraints[7] , (bool)dynConstraints[8] , (String)dynConstraints[9] , (short)dynConstraints[10] , (String)dynConstraints[11] , (DateTime)dynConstraints[12] , (DateTime)dynConstraints[13] , (String)dynConstraints[14] , (bool)dynConstraints[15] , (String)dynConstraints[16] , (short)dynConstraints[17] , (String)dynConstraints[18] , (DateTime)dynConstraints[19] , (DateTime)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (DateTime)dynConstraints[26] , (DateTime)dynConstraints[27] , (String)dynConstraints[28] , (String)dynConstraints[29] , (int)dynConstraints[30] , (String)dynConstraints[31] , (String)dynConstraints[32] , (String)dynConstraints[33] , (int)dynConstraints[34] , (int)dynConstraints[35] , (String)dynConstraints[36] , (String)dynConstraints[37] , (DateTime)dynConstraints[38] , (String)dynConstraints[39] , (String)dynConstraints[40] , (int)dynConstraints[41] , (String)dynConstraints[42] , (int)dynConstraints[43] , (int)dynConstraints[44] , (int)dynConstraints[45] , (short)dynConstraints[46] );
               case 4 :
                     return conditional_P00S46(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (short)dynConstraints[3] , (String)dynConstraints[4] , (DateTime)dynConstraints[5] , (DateTime)dynConstraints[6] , (String)dynConstraints[7] , (bool)dynConstraints[8] , (String)dynConstraints[9] , (short)dynConstraints[10] , (String)dynConstraints[11] , (DateTime)dynConstraints[12] , (DateTime)dynConstraints[13] , (String)dynConstraints[14] , (bool)dynConstraints[15] , (String)dynConstraints[16] , (short)dynConstraints[17] , (String)dynConstraints[18] , (DateTime)dynConstraints[19] , (DateTime)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (DateTime)dynConstraints[26] , (DateTime)dynConstraints[27] , (String)dynConstraints[28] , (String)dynConstraints[29] , (int)dynConstraints[30] , (String)dynConstraints[31] , (String)dynConstraints[32] , (String)dynConstraints[33] , (int)dynConstraints[34] , (int)dynConstraints[35] , (String)dynConstraints[36] , (String)dynConstraints[37] , (DateTime)dynConstraints[38] , (String)dynConstraints[39] , (String)dynConstraints[40] , (int)dynConstraints[41] , (String)dynConstraints[42] , (int)dynConstraints[43] , (int)dynConstraints[44] , (int)dynConstraints[45] , (short)dynConstraints[46] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00S42 ;
          prmP00S42 = new Object[] {
          new Object[] {"@AV9WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV46ContagemResultado_Demanda1",SqlDbType.Char,50,0} ,
          new Object[] {"@AV46ContagemResultado_Demanda1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV46ContagemResultado_Demanda1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV46ContagemResultado_Demanda1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV46ContagemResultado_Demanda1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV46ContagemResultado_Demanda1",SqlDbType.Char,50,0} ,
          new Object[] {"@AV47ContagemResultado_DataDmn1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV48ContagemResultado_DataDmn_To1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV49ContagemResultado_StatusDmn1",SqlDbType.Char,1,0} ,
          new Object[] {"@AV53ContagemResultado_Demanda2",SqlDbType.Char,50,0} ,
          new Object[] {"@AV53ContagemResultado_Demanda2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV53ContagemResultado_Demanda2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV53ContagemResultado_Demanda2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV53ContagemResultado_Demanda2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV53ContagemResultado_Demanda2",SqlDbType.Char,50,0} ,
          new Object[] {"@AV54ContagemResultado_DataDmn2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV55ContagemResultado_DataDmn_To2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV56ContagemResultado_StatusDmn2",SqlDbType.Char,1,0} ,
          new Object[] {"@AV60ContagemResultado_Demanda3",SqlDbType.Char,50,0} ,
          new Object[] {"@AV60ContagemResultado_Demanda3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV60ContagemResultado_Demanda3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV60ContagemResultado_Demanda3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV60ContagemResultado_Demanda3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV60ContagemResultado_Demanda3",SqlDbType.Char,50,0} ,
          new Object[] {"@AV61ContagemResultado_DataDmn3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV62ContagemResultado_DataDmn_To3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV63ContagemResultado_StatusDmn3",SqlDbType.Char,1,0} ,
          new Object[] {"@lV10TFContagemResultado_DemandaFM",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV11TFContagemResultado_DemandaFM_Sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV12TFContagemResultado_Demanda",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV13TFContagemResultado_Demanda_Sel",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV14TFContagemResultado_DataDmn",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV15TFContagemResultado_DataDmn_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV16TFContagemResultado_ContratadaPessoaNom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV17TFContagemResultado_ContratadaPessoaNom_Sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV18TFContagemResultado_Servico",SqlDbType.Char,15,0} ,
          new Object[] {"@AV19TFContagemResultado_Servico_Sel",SqlDbType.Int,6,0} ,
          new Object[] {"@lV20TFContagemrResultado_SistemaSigla",SqlDbType.Char,25,0} ,
          new Object[] {"@AV21TFContagemrResultado_SistemaSigla_Sel",SqlDbType.Char,25,0} ,
          new Object[] {"@AV9WWPCo_2Contratada_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00S43 ;
          prmP00S43 = new Object[] {
          new Object[] {"@AV9WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV46ContagemResultado_Demanda1",SqlDbType.Char,50,0} ,
          new Object[] {"@AV46ContagemResultado_Demanda1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV46ContagemResultado_Demanda1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV46ContagemResultado_Demanda1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV46ContagemResultado_Demanda1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV46ContagemResultado_Demanda1",SqlDbType.Char,50,0} ,
          new Object[] {"@AV47ContagemResultado_DataDmn1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV48ContagemResultado_DataDmn_To1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV49ContagemResultado_StatusDmn1",SqlDbType.Char,1,0} ,
          new Object[] {"@AV53ContagemResultado_Demanda2",SqlDbType.Char,50,0} ,
          new Object[] {"@AV53ContagemResultado_Demanda2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV53ContagemResultado_Demanda2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV53ContagemResultado_Demanda2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV53ContagemResultado_Demanda2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV53ContagemResultado_Demanda2",SqlDbType.Char,50,0} ,
          new Object[] {"@AV54ContagemResultado_DataDmn2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV55ContagemResultado_DataDmn_To2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV56ContagemResultado_StatusDmn2",SqlDbType.Char,1,0} ,
          new Object[] {"@AV60ContagemResultado_Demanda3",SqlDbType.Char,50,0} ,
          new Object[] {"@AV60ContagemResultado_Demanda3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV60ContagemResultado_Demanda3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV60ContagemResultado_Demanda3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV60ContagemResultado_Demanda3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV60ContagemResultado_Demanda3",SqlDbType.Char,50,0} ,
          new Object[] {"@AV61ContagemResultado_DataDmn3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV62ContagemResultado_DataDmn_To3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV63ContagemResultado_StatusDmn3",SqlDbType.Char,1,0} ,
          new Object[] {"@lV10TFContagemResultado_DemandaFM",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV11TFContagemResultado_DemandaFM_Sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV12TFContagemResultado_Demanda",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV13TFContagemResultado_Demanda_Sel",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV14TFContagemResultado_DataDmn",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV15TFContagemResultado_DataDmn_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV16TFContagemResultado_ContratadaPessoaNom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV17TFContagemResultado_ContratadaPessoaNom_Sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV18TFContagemResultado_Servico",SqlDbType.Char,15,0} ,
          new Object[] {"@AV19TFContagemResultado_Servico_Sel",SqlDbType.Int,6,0} ,
          new Object[] {"@lV20TFContagemrResultado_SistemaSigla",SqlDbType.Char,25,0} ,
          new Object[] {"@AV21TFContagemrResultado_SistemaSigla_Sel",SqlDbType.Char,25,0} ,
          new Object[] {"@AV9WWPCo_2Contratada_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00S44 ;
          prmP00S44 = new Object[] {
          new Object[] {"@AV9WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV46ContagemResultado_Demanda1",SqlDbType.Char,50,0} ,
          new Object[] {"@AV46ContagemResultado_Demanda1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV46ContagemResultado_Demanda1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV46ContagemResultado_Demanda1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV46ContagemResultado_Demanda1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV46ContagemResultado_Demanda1",SqlDbType.Char,50,0} ,
          new Object[] {"@AV47ContagemResultado_DataDmn1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV48ContagemResultado_DataDmn_To1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV49ContagemResultado_StatusDmn1",SqlDbType.Char,1,0} ,
          new Object[] {"@AV53ContagemResultado_Demanda2",SqlDbType.Char,50,0} ,
          new Object[] {"@AV53ContagemResultado_Demanda2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV53ContagemResultado_Demanda2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV53ContagemResultado_Demanda2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV53ContagemResultado_Demanda2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV53ContagemResultado_Demanda2",SqlDbType.Char,50,0} ,
          new Object[] {"@AV54ContagemResultado_DataDmn2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV55ContagemResultado_DataDmn_To2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV56ContagemResultado_StatusDmn2",SqlDbType.Char,1,0} ,
          new Object[] {"@AV60ContagemResultado_Demanda3",SqlDbType.Char,50,0} ,
          new Object[] {"@AV60ContagemResultado_Demanda3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV60ContagemResultado_Demanda3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV60ContagemResultado_Demanda3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV60ContagemResultado_Demanda3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV60ContagemResultado_Demanda3",SqlDbType.Char,50,0} ,
          new Object[] {"@AV61ContagemResultado_DataDmn3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV62ContagemResultado_DataDmn_To3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV63ContagemResultado_StatusDmn3",SqlDbType.Char,1,0} ,
          new Object[] {"@lV10TFContagemResultado_DemandaFM",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV11TFContagemResultado_DemandaFM_Sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV12TFContagemResultado_Demanda",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV13TFContagemResultado_Demanda_Sel",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV14TFContagemResultado_DataDmn",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV15TFContagemResultado_DataDmn_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV16TFContagemResultado_ContratadaPessoaNom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV17TFContagemResultado_ContratadaPessoaNom_Sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV18TFContagemResultado_Servico",SqlDbType.Char,15,0} ,
          new Object[] {"@AV19TFContagemResultado_Servico_Sel",SqlDbType.Int,6,0} ,
          new Object[] {"@lV20TFContagemrResultado_SistemaSigla",SqlDbType.Char,25,0} ,
          new Object[] {"@AV21TFContagemrResultado_SistemaSigla_Sel",SqlDbType.Char,25,0} ,
          new Object[] {"@AV9WWPCo_2Contratada_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00S45 ;
          prmP00S45 = new Object[] {
          new Object[] {"@AV9WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV46ContagemResultado_Demanda1",SqlDbType.Char,50,0} ,
          new Object[] {"@AV46ContagemResultado_Demanda1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV46ContagemResultado_Demanda1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV46ContagemResultado_Demanda1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV46ContagemResultado_Demanda1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV46ContagemResultado_Demanda1",SqlDbType.Char,50,0} ,
          new Object[] {"@AV47ContagemResultado_DataDmn1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV48ContagemResultado_DataDmn_To1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV49ContagemResultado_StatusDmn1",SqlDbType.Char,1,0} ,
          new Object[] {"@AV53ContagemResultado_Demanda2",SqlDbType.Char,50,0} ,
          new Object[] {"@AV53ContagemResultado_Demanda2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV53ContagemResultado_Demanda2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV53ContagemResultado_Demanda2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV53ContagemResultado_Demanda2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV53ContagemResultado_Demanda2",SqlDbType.Char,50,0} ,
          new Object[] {"@AV54ContagemResultado_DataDmn2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV55ContagemResultado_DataDmn_To2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV56ContagemResultado_StatusDmn2",SqlDbType.Char,1,0} ,
          new Object[] {"@AV60ContagemResultado_Demanda3",SqlDbType.Char,50,0} ,
          new Object[] {"@AV60ContagemResultado_Demanda3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV60ContagemResultado_Demanda3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV60ContagemResultado_Demanda3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV60ContagemResultado_Demanda3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV60ContagemResultado_Demanda3",SqlDbType.Char,50,0} ,
          new Object[] {"@AV61ContagemResultado_DataDmn3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV62ContagemResultado_DataDmn_To3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV63ContagemResultado_StatusDmn3",SqlDbType.Char,1,0} ,
          new Object[] {"@lV10TFContagemResultado_DemandaFM",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV11TFContagemResultado_DemandaFM_Sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV12TFContagemResultado_Demanda",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV13TFContagemResultado_Demanda_Sel",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV14TFContagemResultado_DataDmn",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV15TFContagemResultado_DataDmn_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV16TFContagemResultado_ContratadaPessoaNom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV17TFContagemResultado_ContratadaPessoaNom_Sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV18TFContagemResultado_Servico",SqlDbType.Char,15,0} ,
          new Object[] {"@AV19TFContagemResultado_Servico_Sel",SqlDbType.Int,6,0} ,
          new Object[] {"@lV20TFContagemrResultado_SistemaSigla",SqlDbType.Char,25,0} ,
          new Object[] {"@AV21TFContagemrResultado_SistemaSigla_Sel",SqlDbType.Char,25,0} ,
          new Object[] {"@AV9WWPCo_2Contratada_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00S46 ;
          prmP00S46 = new Object[] {
          new Object[] {"@AV9WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV46ContagemResultado_Demanda1",SqlDbType.Char,50,0} ,
          new Object[] {"@AV46ContagemResultado_Demanda1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV46ContagemResultado_Demanda1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV46ContagemResultado_Demanda1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV46ContagemResultado_Demanda1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV46ContagemResultado_Demanda1",SqlDbType.Char,50,0} ,
          new Object[] {"@AV47ContagemResultado_DataDmn1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV48ContagemResultado_DataDmn_To1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV49ContagemResultado_StatusDmn1",SqlDbType.Char,1,0} ,
          new Object[] {"@AV53ContagemResultado_Demanda2",SqlDbType.Char,50,0} ,
          new Object[] {"@AV53ContagemResultado_Demanda2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV53ContagemResultado_Demanda2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV53ContagemResultado_Demanda2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV53ContagemResultado_Demanda2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV53ContagemResultado_Demanda2",SqlDbType.Char,50,0} ,
          new Object[] {"@AV54ContagemResultado_DataDmn2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV55ContagemResultado_DataDmn_To2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV56ContagemResultado_StatusDmn2",SqlDbType.Char,1,0} ,
          new Object[] {"@AV60ContagemResultado_Demanda3",SqlDbType.Char,50,0} ,
          new Object[] {"@AV60ContagemResultado_Demanda3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV60ContagemResultado_Demanda3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV60ContagemResultado_Demanda3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV60ContagemResultado_Demanda3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV60ContagemResultado_Demanda3",SqlDbType.Char,50,0} ,
          new Object[] {"@AV61ContagemResultado_DataDmn3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV62ContagemResultado_DataDmn_To3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV63ContagemResultado_StatusDmn3",SqlDbType.Char,1,0} ,
          new Object[] {"@lV10TFContagemResultado_DemandaFM",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV11TFContagemResultado_DemandaFM_Sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV12TFContagemResultado_Demanda",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV13TFContagemResultado_Demanda_Sel",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV14TFContagemResultado_DataDmn",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV15TFContagemResultado_DataDmn_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV16TFContagemResultado_ContratadaPessoaNom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV17TFContagemResultado_ContratadaPessoaNom_Sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV18TFContagemResultado_Servico",SqlDbType.Char,15,0} ,
          new Object[] {"@AV19TFContagemResultado_Servico_Sel",SqlDbType.Int,6,0} ,
          new Object[] {"@lV20TFContagemrResultado_SistemaSigla",SqlDbType.Char,25,0} ,
          new Object[] {"@AV21TFContagemrResultado_SistemaSigla_Sel",SqlDbType.Char,25,0} ,
          new Object[] {"@AV9WWPCo_2Contratada_codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00S42", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00S42,100,0,true,false )
             ,new CursorDef("P00S43", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00S43,100,0,true,false )
             ,new CursorDef("P00S44", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00S44,100,0,true,false )
             ,new CursorDef("P00S45", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00S45,100,0,true,false )
             ,new CursorDef("P00S46", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00S46,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((int[]) buf[8])[0] = rslt.getInt(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((int[]) buf[10])[0] = rslt.getInt(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((int[]) buf[12])[0] = rslt.getInt(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((short[]) buf[14])[0] = rslt.getShort(9) ;
                ((String[]) buf[15])[0] = rslt.getVarchar(10) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(10);
                ((int[]) buf[17])[0] = rslt.getInt(11) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(11);
                ((String[]) buf[19])[0] = rslt.getString(12, 25) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(12);
                ((int[]) buf[21])[0] = rslt.getInt(13) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(13);
                ((String[]) buf[23])[0] = rslt.getString(14, 15) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(14);
                ((String[]) buf[25])[0] = rslt.getString(15, 100) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(15);
                ((String[]) buf[27])[0] = rslt.getString(16, 1) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(16);
                ((DateTime[]) buf[29])[0] = rslt.getGXDate(17) ;
                ((String[]) buf[30])[0] = rslt.getVarchar(18) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(18);
                ((int[]) buf[32])[0] = rslt.getInt(19) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((int[]) buf[8])[0] = rslt.getInt(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((int[]) buf[10])[0] = rslt.getInt(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((String[]) buf[12])[0] = rslt.getVarchar(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((short[]) buf[14])[0] = rslt.getShort(9) ;
                ((int[]) buf[15])[0] = rslt.getInt(10) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(10);
                ((String[]) buf[17])[0] = rslt.getString(11, 25) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(11);
                ((int[]) buf[19])[0] = rslt.getInt(12) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(12);
                ((String[]) buf[21])[0] = rslt.getString(13, 15) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(13);
                ((String[]) buf[23])[0] = rslt.getString(14, 100) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(14);
                ((String[]) buf[25])[0] = rslt.getString(15, 1) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(15);
                ((DateTime[]) buf[27])[0] = rslt.getGXDate(16) ;
                ((String[]) buf[28])[0] = rslt.getVarchar(17) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(17);
                ((int[]) buf[30])[0] = rslt.getInt(18) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(18);
                ((int[]) buf[32])[0] = rslt.getInt(19) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((int[]) buf[8])[0] = rslt.getInt(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((int[]) buf[10])[0] = rslt.getInt(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((int[]) buf[12])[0] = rslt.getInt(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((short[]) buf[14])[0] = rslt.getShort(9) ;
                ((String[]) buf[15])[0] = rslt.getString(10, 100) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(10);
                ((int[]) buf[17])[0] = rslt.getInt(11) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(11);
                ((String[]) buf[19])[0] = rslt.getString(12, 25) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(12);
                ((int[]) buf[21])[0] = rslt.getInt(13) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(13);
                ((String[]) buf[23])[0] = rslt.getString(14, 15) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(14);
                ((String[]) buf[25])[0] = rslt.getString(15, 1) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(15);
                ((DateTime[]) buf[27])[0] = rslt.getGXDate(16) ;
                ((String[]) buf[28])[0] = rslt.getVarchar(17) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(17);
                ((String[]) buf[30])[0] = rslt.getVarchar(18) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(18);
                ((int[]) buf[32])[0] = rslt.getInt(19) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((int[]) buf[8])[0] = rslt.getInt(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((int[]) buf[10])[0] = rslt.getInt(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((int[]) buf[12])[0] = rslt.getInt(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((short[]) buf[14])[0] = rslt.getShort(9) ;
                ((int[]) buf[15])[0] = rslt.getInt(10) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(10);
                ((int[]) buf[17])[0] = rslt.getInt(11) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(11);
                ((String[]) buf[19])[0] = rslt.getString(12, 25) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(12);
                ((String[]) buf[21])[0] = rslt.getString(13, 15) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(13);
                ((String[]) buf[23])[0] = rslt.getString(14, 100) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(14);
                ((String[]) buf[25])[0] = rslt.getString(15, 1) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(15);
                ((DateTime[]) buf[27])[0] = rslt.getGXDate(16) ;
                ((String[]) buf[28])[0] = rslt.getVarchar(17) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(17);
                ((String[]) buf[30])[0] = rslt.getVarchar(18) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(18);
                ((int[]) buf[32])[0] = rslt.getInt(19) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((int[]) buf[8])[0] = rslt.getInt(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((int[]) buf[10])[0] = rslt.getInt(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((int[]) buf[12])[0] = rslt.getInt(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((short[]) buf[14])[0] = rslt.getShort(9) ;
                ((String[]) buf[15])[0] = rslt.getString(10, 25) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(10);
                ((int[]) buf[17])[0] = rslt.getInt(11) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(11);
                ((int[]) buf[19])[0] = rslt.getInt(12) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(12);
                ((String[]) buf[21])[0] = rslt.getString(13, 15) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(13);
                ((String[]) buf[23])[0] = rslt.getString(14, 100) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(14);
                ((String[]) buf[25])[0] = rslt.getString(15, 1) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(15);
                ((DateTime[]) buf[27])[0] = rslt.getGXDate(16) ;
                ((String[]) buf[28])[0] = rslt.getVarchar(17) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(17);
                ((String[]) buf[30])[0] = rslt.getVarchar(18) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(18);
                ((int[]) buf[32])[0] = rslt.getInt(19) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[41]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[48]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[49]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[50]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[51]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[52]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[53]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[54]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[55]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[56]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[57]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[58]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[59]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[60]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[61]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[62]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[63]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[64]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[65]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[66]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[67]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[68]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[69]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[70]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[71]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[72]);
                }
                if ( (short)parms[32] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[73]);
                }
                if ( (short)parms[33] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[74]);
                }
                if ( (short)parms[34] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[75]);
                }
                if ( (short)parms[35] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[76]);
                }
                if ( (short)parms[36] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[77]);
                }
                if ( (short)parms[37] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[78]);
                }
                if ( (short)parms[38] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[79]);
                }
                if ( (short)parms[39] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[80]);
                }
                if ( (short)parms[40] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[81]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[41]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[48]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[49]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[50]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[51]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[52]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[53]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[54]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[55]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[56]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[57]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[58]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[59]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[60]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[61]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[62]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[63]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[64]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[65]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[66]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[67]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[68]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[69]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[70]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[71]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[72]);
                }
                if ( (short)parms[32] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[73]);
                }
                if ( (short)parms[33] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[74]);
                }
                if ( (short)parms[34] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[75]);
                }
                if ( (short)parms[35] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[76]);
                }
                if ( (short)parms[36] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[77]);
                }
                if ( (short)parms[37] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[78]);
                }
                if ( (short)parms[38] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[79]);
                }
                if ( (short)parms[39] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[80]);
                }
                if ( (short)parms[40] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[81]);
                }
                return;
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[41]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[48]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[49]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[50]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[51]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[52]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[53]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[54]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[55]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[56]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[57]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[58]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[59]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[60]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[61]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[62]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[63]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[64]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[65]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[66]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[67]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[68]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[69]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[70]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[71]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[72]);
                }
                if ( (short)parms[32] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[73]);
                }
                if ( (short)parms[33] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[74]);
                }
                if ( (short)parms[34] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[75]);
                }
                if ( (short)parms[35] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[76]);
                }
                if ( (short)parms[36] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[77]);
                }
                if ( (short)parms[37] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[78]);
                }
                if ( (short)parms[38] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[79]);
                }
                if ( (short)parms[39] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[80]);
                }
                if ( (short)parms[40] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[81]);
                }
                return;
             case 3 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[41]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[48]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[49]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[50]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[51]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[52]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[53]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[54]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[55]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[56]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[57]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[58]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[59]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[60]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[61]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[62]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[63]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[64]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[65]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[66]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[67]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[68]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[69]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[70]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[71]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[72]);
                }
                if ( (short)parms[32] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[73]);
                }
                if ( (short)parms[33] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[74]);
                }
                if ( (short)parms[34] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[75]);
                }
                if ( (short)parms[35] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[76]);
                }
                if ( (short)parms[36] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[77]);
                }
                if ( (short)parms[37] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[78]);
                }
                if ( (short)parms[38] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[79]);
                }
                if ( (short)parms[39] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[80]);
                }
                if ( (short)parms[40] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[81]);
                }
                return;
             case 4 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[41]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[48]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[49]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[50]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[51]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[52]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[53]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[54]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[55]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[56]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[57]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[58]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[59]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[60]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[61]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[62]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[63]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[64]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[65]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[66]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[67]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[68]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[69]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[70]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[71]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[72]);
                }
                if ( (short)parms[32] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[73]);
                }
                if ( (short)parms[33] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[74]);
                }
                if ( (short)parms[34] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[75]);
                }
                if ( (short)parms[35] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[76]);
                }
                if ( (short)parms[36] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[77]);
                }
                if ( (short)parms[37] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[78]);
                }
                if ( (short)parms[38] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[79]);
                }
                if ( (short)parms[39] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[80]);
                }
                if ( (short)parms[40] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[81]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getpromptcontagemresultadofilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getpromptcontagemresultadofilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getpromptcontagemresultadofilterdata") )
          {
             return  ;
          }
          getpromptcontagemresultadofilterdata worker = new getpromptcontagemresultadofilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
