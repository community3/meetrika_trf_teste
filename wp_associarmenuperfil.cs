/*
               File: WP_AssociarMenuPerfil
        Description: Associar
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/6/2020 23:57:23.26
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_associarmenuperfil : GXHttpHandler, System.Web.SessionState.IRequiresSessionState
   {
      public wp_associarmenuperfil( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_associarmenuperfil( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Perfil_Codigo )
      {
         this.AV7Perfil_Codigo = aP0_Perfil_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV7Perfil_Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7Perfil_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Perfil_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPERFIL_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7Perfil_Codigo), "ZZZZZ9")));
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            ValidateSpaRequest();
            PA922( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               context.Gx_err = 0;
               WS922( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  WE922( ) ;
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( "Associar") ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20205623572339");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery-1.4.2.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.ui.core.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.ui.widget.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.ui.mouse.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.ui.selectable.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.corner.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery-1.4.2.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.ui.core.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.ui.widget.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.ui.mouse.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.ui.selectable.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.corner.js", "");
         context.AddJavascriptSource("jQueryUI/jqSelectRender.js", "");
         context.AddJavascriptSource("jQueryUI/jqSelectRender.js", "");
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wp_associarmenuperfil.aspx") + "?" + UrlEncode("" +AV7Perfil_Codigo)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vJQNAOASSOCIADOS", AV28jqNaoAssociados);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vJQNAOASSOCIADOS", AV28jqNaoAssociados);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vJQASSOCIADOS", AV30jqAssociados);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vJQASSOCIADOS", AV30jqAssociados);
         }
         GxWebStd.gx_hidden_field( context, "PERFIL_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A3Perfil_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vPERFIL_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7Perfil_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "MENU_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A277Menu_Codigo), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vMENUPERFIL", AV11MenuPerfil);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vMENUPERFIL", AV11MenuPerfil);
         }
         GxWebStd.gx_hidden_field( context, "vMENU_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV8Menu_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "vALL", AV32All);
         GxWebStd.gx_hidden_field( context, "gxhash_PERFIL_NOME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A4Perfil_Nome, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_PERFIL_NOME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A4Perfil_Nome, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_PERFIL_NOME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A4Perfil_Nome, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vPERFIL_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7Perfil_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vPERFIL_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7Perfil_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "JQNAOASSOCIADOS_Width", StringUtil.LTrim( StringUtil.NToC( (decimal)(Jqnaoassociados_Width), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "JQNAOASSOCIADOS_Height", StringUtil.LTrim( StringUtil.NToC( (decimal)(Jqnaoassociados_Height), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "JQNAOASSOCIADOS_Rounding", StringUtil.RTrim( Jqnaoassociados_Rounding));
         GxWebStd.gx_hidden_field( context, "JQNAOASSOCIADOS_Theme", StringUtil.RTrim( Jqnaoassociados_Theme));
         GxWebStd.gx_hidden_field( context, "JQNAOASSOCIADOS_Unselectedicon", StringUtil.RTrim( Jqnaoassociados_Unselectedicon));
         GxWebStd.gx_hidden_field( context, "JQNAOASSOCIADOS_Selectedcolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(Jqnaoassociados_Selectedcolor), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "JQNAOASSOCIADOS_Itemwidth", StringUtil.LTrim( StringUtil.NToC( (decimal)(Jqnaoassociados_Itemwidth), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "JQNAOASSOCIADOS_Itemheight", StringUtil.LTrim( StringUtil.NToC( (decimal)(Jqnaoassociados_Itemheight), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "JQASSOCIADOS_Width", StringUtil.LTrim( StringUtil.NToC( (decimal)(Jqassociados_Width), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "JQASSOCIADOS_Height", StringUtil.LTrim( StringUtil.NToC( (decimal)(Jqassociados_Height), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "JQASSOCIADOS_Rounding", StringUtil.RTrim( Jqassociados_Rounding));
         GxWebStd.gx_hidden_field( context, "JQASSOCIADOS_Theme", StringUtil.RTrim( Jqassociados_Theme));
         GxWebStd.gx_hidden_field( context, "JQASSOCIADOS_Unselectedicon", StringUtil.RTrim( Jqassociados_Unselectedicon));
         GxWebStd.gx_hidden_field( context, "JQASSOCIADOS_Selectedcolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(Jqassociados_Selectedcolor), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "JQASSOCIADOS_Itemwidth", StringUtil.LTrim( StringUtil.NToC( (decimal)(Jqassociados_Itemwidth), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "JQASSOCIADOS_Itemheight", StringUtil.LTrim( StringUtil.NToC( (decimal)(Jqassociados_Itemheight), 9, 0, ".", "")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseForm922( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
         context.WriteHtmlTextNl( "</body>") ;
         context.WriteHtmlTextNl( "</html>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
      }

      public override String GetPgmname( )
      {
         return "WP_AssociarMenuPerfil" ;
      }

      public override String GetPgmdesc( )
      {
         return "Associar" ;
      }

      protected void WB920( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            RenderHtmlHeaders( ) ;
            RenderHtmlOpenForm( ) ;
            wb_table1_2_922( true) ;
         }
         else
         {
            wb_table1_2_922( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_922e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void START922( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Associar", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUP920( ) ;
      }

      protected void WS922( )
      {
         START922( ) ;
         EVT922( ) ;
      }

      protected void EVT922( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E11922 */
                           E11922 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E12922 */
                           E12922 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( ! wbErr )
                           {
                              Rfr0gs = false;
                              if ( ! Rfr0gs )
                              {
                                 /* Execute user event: E13922 */
                                 E13922 ();
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DISASSOCIATE SELECTED'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E14922 */
                           E14922 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ASSOCIATE SELECTED'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E15922 */
                           E15922 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ASSOCIATE ALL'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E16922 */
                           E16922 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DISASSOCIATE ALL'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E17922 */
                           E17922 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E18922 */
                           E18922 ();
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           dynload_actions( ) ;
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void WE922( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseForm922( ) ;
            }
         }
      }

      protected void PA922( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF922( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      protected void RF922( )
      {
         initialize_formulas( ) ;
         /* Execute user event: E12922 */
         E12922 ();
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Using cursor H00922 */
            pr_default.execute(0, new Object[] {AV7Perfil_Codigo});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A3Perfil_Codigo = H00922_A3Perfil_Codigo[0];
               A4Perfil_Nome = H00922_A4Perfil_Nome[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A4Perfil_Nome", A4Perfil_Nome);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_PERFIL_NOME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A4Perfil_Nome, "@!"))));
               /* Execute user event: E18922 */
               E18922 ();
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(0);
            WB920( ) ;
         }
      }

      protected void STRUP920( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11922 */
         E11922 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vJQNAOASSOCIADOS"), AV28jqNaoAssociados);
            ajax_req_read_hidden_sdt(cgiGet( "vJQASSOCIADOS"), AV30jqAssociados);
            /* Read variables values. */
            A4Perfil_Nome = StringUtil.Upper( cgiGet( edtPerfil_Nome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A4Perfil_Nome", A4Perfil_Nome);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_PERFIL_NOME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A4Perfil_Nome, "@!"))));
            /* Read saved values. */
            Jqnaoassociados_Width = (int)(context.localUtil.CToN( cgiGet( "JQNAOASSOCIADOS_Width"), ",", "."));
            Jqnaoassociados_Height = (int)(context.localUtil.CToN( cgiGet( "JQNAOASSOCIADOS_Height"), ",", "."));
            Jqnaoassociados_Rounding = cgiGet( "JQNAOASSOCIADOS_Rounding");
            Jqnaoassociados_Theme = cgiGet( "JQNAOASSOCIADOS_Theme");
            Jqnaoassociados_Unselectedicon = cgiGet( "JQNAOASSOCIADOS_Unselectedicon");
            Jqnaoassociados_Selectedcolor = (int)(context.localUtil.CToN( cgiGet( "JQNAOASSOCIADOS_Selectedcolor"), ",", "."));
            Jqnaoassociados_Itemwidth = (int)(context.localUtil.CToN( cgiGet( "JQNAOASSOCIADOS_Itemwidth"), ",", "."));
            Jqnaoassociados_Itemheight = (int)(context.localUtil.CToN( cgiGet( "JQNAOASSOCIADOS_Itemheight"), ",", "."));
            Jqassociados_Width = (int)(context.localUtil.CToN( cgiGet( "JQASSOCIADOS_Width"), ",", "."));
            Jqassociados_Height = (int)(context.localUtil.CToN( cgiGet( "JQASSOCIADOS_Height"), ",", "."));
            Jqassociados_Rounding = cgiGet( "JQASSOCIADOS_Rounding");
            Jqassociados_Theme = cgiGet( "JQASSOCIADOS_Theme");
            Jqassociados_Unselectedicon = cgiGet( "JQASSOCIADOS_Unselectedicon");
            Jqassociados_Selectedcolor = (int)(context.localUtil.CToN( cgiGet( "JQASSOCIADOS_Selectedcolor"), ",", "."));
            Jqassociados_Itemwidth = (int)(context.localUtil.CToN( cgiGet( "JQASSOCIADOS_Itemwidth"), ",", "."));
            Jqassociados_Itemheight = (int)(context.localUtil.CToN( cgiGet( "JQASSOCIADOS_Itemheight"), ",", "."));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11922 */
         E11922 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E11922( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         if ( StringUtil.StrCmp(AV9HTTPRequest.Method, "GET") == 0 )
         {
            AV35GXLvl6 = 0;
            /* Using cursor H00923 */
            pr_default.execute(1, new Object[] {AV7Perfil_Codigo});
            while ( (pr_default.getStatus(1) != 101) )
            {
               A3Perfil_Codigo = H00923_A3Perfil_Codigo[0];
               AV35GXLvl6 = 1;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(1);
            if ( AV35GXLvl6 == 0 )
            {
               GX_msglist.addItem("Perfil n�o encontrado.");
            }
            /* Using cursor H00924 */
            pr_default.execute(2);
            while ( (pr_default.getStatus(2) != 101) )
            {
               A277Menu_Codigo = H00924_A277Menu_Codigo[0];
               A285Menu_PaiCod = H00924_A285Menu_PaiCod[0];
               n285Menu_PaiCod = H00924_n285Menu_PaiCod[0];
               A284Menu_Ativo = H00924_A284Menu_Ativo[0];
               A278Menu_Nome = H00924_A278Menu_Nome[0];
               A283Menu_Ordem = H00924_A283Menu_Ordem[0];
               A280Menu_Tipo = H00924_A280Menu_Tipo[0];
               AV10Exist = false;
               /* Using cursor H00925 */
               pr_default.execute(3, new Object[] {A277Menu_Codigo, AV7Perfil_Codigo});
               while ( (pr_default.getStatus(3) != 101) )
               {
                  A3Perfil_Codigo = H00925_A3Perfil_Codigo[0];
                  AV10Exist = true;
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
                  /* Exiting from a For First loop. */
                  if (true) break;
               }
               pr_default.close(3);
               if ( AV10Exist )
               {
                  AV29jqItem = new SdtjqSelectData_Item(context);
                  AV29jqItem.gxTpr_Id = StringUtil.Str( (decimal)(A277Menu_Codigo), 6, 0);
                  AV29jqItem.gxTpr_Descr = "� "+StringUtil.Upper( A278Menu_Nome);
                  AV29jqItem.gxTpr_Selected = false;
                  AV30jqAssociados.Add(AV29jqItem, 0);
               }
               else
               {
                  AV29jqItem = new SdtjqSelectData_Item(context);
                  AV29jqItem.gxTpr_Id = StringUtil.Str( (decimal)(A277Menu_Codigo), 6, 0);
                  AV29jqItem.gxTpr_Descr = "� "+StringUtil.Upper( A278Menu_Nome);
                  AV29jqItem.gxTpr_Selected = false;
                  AV28jqNaoAssociados.Add(AV29jqItem, 0);
               }
               AV27Menu_PaiCod = A277Menu_Codigo;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27Menu_PaiCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27Menu_PaiCod), 6, 0)));
               /* Execute user subroutine: 'MENUFILHOS' */
               S114 ();
               if ( returnInSub )
               {
                  pr_default.close(2);
                  returnInSub = true;
                  if (true) return;
               }
               pr_default.readNext(2);
            }
            pr_default.close(2);
         }
         /* Execute user subroutine: 'TITLES' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S114( )
      {
         /* 'MENUFILHOS' Routine */
         /* Using cursor H00926 */
         pr_default.execute(4, new Object[] {AV27Menu_PaiCod});
         while ( (pr_default.getStatus(4) != 101) )
         {
            A285Menu_PaiCod = H00926_A285Menu_PaiCod[0];
            n285Menu_PaiCod = H00926_n285Menu_PaiCod[0];
            A277Menu_Codigo = H00926_A277Menu_Codigo[0];
            A284Menu_Ativo = H00926_A284Menu_Ativo[0];
            A278Menu_Nome = H00926_A278Menu_Nome[0];
            A283Menu_Ordem = H00926_A283Menu_Ordem[0];
            A280Menu_Tipo = H00926_A280Menu_Tipo[0];
            AV10Exist = false;
            /* Using cursor H00927 */
            pr_default.execute(5, new Object[] {A277Menu_Codigo, AV7Perfil_Codigo});
            while ( (pr_default.getStatus(5) != 101) )
            {
               A3Perfil_Codigo = H00927_A3Perfil_Codigo[0];
               AV10Exist = true;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(5);
            if ( AV10Exist )
            {
               AV29jqItem = new SdtjqSelectData_Item(context);
               AV29jqItem.gxTpr_Id = StringUtil.Str( (decimal)(A277Menu_Codigo), 6, 0);
               AV29jqItem.gxTpr_Descr = "��"+A278Menu_Nome;
               AV29jqItem.gxTpr_Selected = false;
               AV30jqAssociados.Add(AV29jqItem, 0);
            }
            else
            {
               AV29jqItem = new SdtjqSelectData_Item(context);
               AV29jqItem.gxTpr_Id = StringUtil.Str( (decimal)(A277Menu_Codigo), 6, 0);
               AV29jqItem.gxTpr_Descr = "��"+A278Menu_Nome;
               AV29jqItem.gxTpr_Selected = false;
               AV28jqNaoAssociados.Add(AV29jqItem, 0);
            }
            pr_default.readNext(4);
         }
         pr_default.close(4);
      }

      protected void E12922( )
      {
         /* Refresh Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         imgImageassociateselected_Visible = (AV6WWPContext.gxTpr_Insert ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgImageassociateselected_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgImageassociateselected_Visible), 5, 0)));
         imgImageassociateall_Visible = (AV6WWPContext.gxTpr_Insert ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgImageassociateall_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgImageassociateall_Visible), 5, 0)));
         imgImagedisassociateselected_Visible = (AV6WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgImagedisassociateselected_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgImagedisassociateselected_Visible), 5, 0)));
         imgImagedisassociateall_Visible = (AV6WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgImagedisassociateall_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgImagedisassociateall_Visible), 5, 0)));
         bttBtn_confirm_Visible = (AV6WWPContext.gxTpr_Insert||AV6WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_confirm_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_confirm_Visible), 5, 0)));
      }

      public void GXEnter( )
      {
         /* Execute user event: E13922 */
         E13922 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E13922( )
      {
         /* Enter Routine */
         AV12Success = true;
         AV40GXV1 = 1;
         while ( AV40GXV1 <= AV30jqAssociados.Count )
         {
            AV29jqItem = ((SdtjqSelectData_Item)AV30jqAssociados.Item(AV40GXV1));
            if ( AV12Success )
            {
               AV8Menu_Codigo = (int)(NumberUtil.Val( AV29jqItem.gxTpr_Id, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8Menu_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8Menu_Codigo), 6, 0)));
               AV41GXLvl88 = 0;
               /* Using cursor H00928 */
               pr_default.execute(6, new Object[] {AV8Menu_Codigo, AV7Perfil_Codigo});
               while ( (pr_default.getStatus(6) != 101) )
               {
                  A277Menu_Codigo = H00928_A277Menu_Codigo[0];
                  A3Perfil_Codigo = H00928_A3Perfil_Codigo[0];
                  AV41GXLvl88 = 1;
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
                  /* Exiting from a For First loop. */
                  if (true) break;
               }
               pr_default.close(6);
               if ( AV41GXLvl88 == 0 )
               {
                  AV11MenuPerfil = new SdtMenuPerfil(context);
                  AV11MenuPerfil.gxTpr_Perfil_codigo = AV7Perfil_Codigo;
                  AV11MenuPerfil.gxTpr_Menu_codigo = AV8Menu_Codigo;
                  AV11MenuPerfil.Save();
                  if ( ! AV11MenuPerfil.Success() )
                  {
                     AV12Success = false;
                  }
               }
            }
            AV40GXV1 = (int)(AV40GXV1+1);
         }
         AV42GXV2 = 1;
         while ( AV42GXV2 <= AV28jqNaoAssociados.Count )
         {
            AV29jqItem = ((SdtjqSelectData_Item)AV28jqNaoAssociados.Item(AV42GXV2));
            if ( AV12Success )
            {
               AV8Menu_Codigo = (int)(NumberUtil.Val( AV29jqItem.gxTpr_Id, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8Menu_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8Menu_Codigo), 6, 0)));
               /* Using cursor H00929 */
               pr_default.execute(7, new Object[] {AV8Menu_Codigo, AV7Perfil_Codigo});
               while ( (pr_default.getStatus(7) != 101) )
               {
                  A277Menu_Codigo = H00929_A277Menu_Codigo[0];
                  A3Perfil_Codigo = H00929_A3Perfil_Codigo[0];
                  AV11MenuPerfil = new SdtMenuPerfil(context);
                  AV11MenuPerfil.Load(AV8Menu_Codigo, AV7Perfil_Codigo);
                  if ( AV11MenuPerfil.Success() )
                  {
                     AV11MenuPerfil.Delete();
                  }
                  if ( ! AV11MenuPerfil.Success() )
                  {
                     AV12Success = false;
                  }
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
                  /* Exiting from a For First loop. */
                  if (true) break;
               }
               pr_default.close(7);
            }
            AV42GXV2 = (int)(AV42GXV2+1);
         }
         if ( AV12Success )
         {
            context.CommitDataStores( "WP_AssociarMenuPerfil");
            context.setWebReturnParms(new Object[] {});
            context.wjLocDisableFrm = 1;
            context.nUserReturn = 1;
            returnInSub = true;
            if (true) return;
         }
         else
         {
            context.RollbackDataStores( "WP_AssociarMenuPerfil");
            /* Execute user subroutine: 'SHOW ERROR MESSAGES' */
            S132 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV11MenuPerfil", AV11MenuPerfil);
      }

      protected void E14922( )
      {
         /* 'Disassociate Selected' Routine */
         bttBtn_cancel_Caption = "Cancelar";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_cancel_Internalname, "Caption", bttBtn_cancel_Caption);
         AV32All = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32All", AV32All);
         /* Execute user subroutine: 'DISASSOCIATESELECTED' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV30jqAssociados", AV30jqAssociados);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV28jqNaoAssociados", AV28jqNaoAssociados);
      }

      protected void E15922( )
      {
         /* 'Associate selected' Routine */
         bttBtn_cancel_Caption = "Cancelar";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_cancel_Internalname, "Caption", bttBtn_cancel_Caption);
         AV32All = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32All", AV32All);
         /* Execute user subroutine: 'ASSOCIATESELECTED' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV28jqNaoAssociados", AV28jqNaoAssociados);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV30jqAssociados", AV30jqAssociados);
      }

      protected void E16922( )
      {
         /* 'Associate All' Routine */
         bttBtn_cancel_Caption = "Cancelar";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_cancel_Internalname, "Caption", bttBtn_cancel_Caption);
         AV32All = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32All", AV32All);
         /* Execute user subroutine: 'ASSOCIATESELECTED' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV28jqNaoAssociados", AV28jqNaoAssociados);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV30jqAssociados", AV30jqAssociados);
      }

      protected void E17922( )
      {
         /* 'Disassociate All' Routine */
         bttBtn_cancel_Caption = "Cancelar";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_cancel_Internalname, "Caption", bttBtn_cancel_Caption);
         AV32All = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32All", AV32All);
         /* Execute user subroutine: 'DISASSOCIATESELECTED' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV30jqAssociados", AV30jqAssociados);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV28jqNaoAssociados", AV28jqNaoAssociados);
      }

      protected void S132( )
      {
         /* 'SHOW ERROR MESSAGES' Routine */
         AV45GXV4 = 1;
         AV44GXV3 = AV11MenuPerfil.GetMessages();
         while ( AV45GXV4 <= AV44GXV3.Count )
         {
            AV15Message = ((SdtMessages_Message)AV44GXV3.Item(AV45GXV4));
            if ( AV15Message.gxTpr_Type == 1 )
            {
               GX_msglist.addItem(AV15Message.gxTpr_Description);
            }
            AV45GXV4 = (int)(AV45GXV4+1);
         }
      }

      protected void S152( )
      {
         /* 'ASSOCIATESELECTED' Routine */
         AV14i = 1;
         while ( AV14i <= AV28jqNaoAssociados.Count )
         {
            if ( ((SdtjqSelectData_Item)AV28jqNaoAssociados.Item(AV14i)).gxTpr_Selected || AV32All )
            {
               ((SdtjqSelectData_Item)AV28jqNaoAssociados.Item(AV14i)).gxTpr_Selected = false;
               AV30jqAssociados.Add(((SdtjqSelectData_Item)AV28jqNaoAssociados.Item(AV14i)), 0);
               AV28jqNaoAssociados.RemoveItem(AV14i);
               AV14i = (int)(AV14i-1);
            }
            AV14i = (int)(AV14i+1);
         }
         /* Execute user subroutine: 'TITLES' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S142( )
      {
         /* 'DISASSOCIATESELECTED' Routine */
         AV14i = 1;
         while ( AV14i <= AV30jqAssociados.Count )
         {
            if ( ((SdtjqSelectData_Item)AV30jqAssociados.Item(AV14i)).gxTpr_Selected || AV32All )
            {
               ((SdtjqSelectData_Item)AV30jqAssociados.Item(AV14i)).gxTpr_Selected = false;
               AV28jqNaoAssociados.Add(((SdtjqSelectData_Item)AV30jqAssociados.Item(AV14i)), 0);
               AV30jqAssociados.RemoveItem(AV14i);
               AV14i = (int)(AV14i-1);
            }
            AV14i = (int)(AV14i+1);
         }
         /* Execute user subroutine: 'TITLES' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S122( )
      {
         /* 'TITLES' Routine */
         lblNotassociatedrecordstitle_Caption = "N�o Associados ("+StringUtil.Trim( StringUtil.Str( (decimal)(AV28jqNaoAssociados.Count), 9, 0))+")";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblNotassociatedrecordstitle_Internalname, "Caption", lblNotassociatedrecordstitle_Caption);
         lblAssociatedrecordstitle_Caption = "Associados ("+StringUtil.Trim( StringUtil.Str( (decimal)(AV30jqAssociados.Count), 9, 0))+")";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblAssociatedrecordstitle_Internalname, "Caption", lblAssociatedrecordstitle_Caption);
      }

      protected void nextLoad( )
      {
      }

      protected void E18922( )
      {
         /* Load Routine */
      }

      protected void wb_table1_2_922( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TextBlockTitleCell'>") ;
            wb_table2_8_922( true) ;
         }
         else
         {
            wb_table2_8_922( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_922e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_16_922( true) ;
         }
         else
         {
            wb_table3_16_922( false) ;
         }
         return  ;
      }

      protected void wb_table3_16_922e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_48_922( true) ;
         }
         else
         {
            wb_table4_48_922( false) ;
         }
         return  ;
      }

      protected void wb_table4_48_922e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_922e( true) ;
         }
         else
         {
            wb_table1_2_922e( false) ;
         }
      }

      protected void wb_table4_48_922( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 51,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_confirm_Internalname, "", "Confirmar", bttBtn_confirm_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_confirm_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_AssociarMenuPerfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 53,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_cancel_Internalname, "", bttBtn_cancel_Caption, bttBtn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_AssociarMenuPerfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_48_922e( true) ;
         }
         else
         {
            wb_table4_48_922e( false) ;
         }
      }

      protected void wb_table3_16_922( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefullcontent_Internalname, tblTablefullcontent_Internalname, "", "TableContent", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableAttributesCell'>") ;
            wb_table5_19_922( true) ;
         }
         else
         {
            wb_table5_19_922( false) ;
         }
         return  ;
      }

      protected void wb_table5_19_922e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_16_922e( true) ;
         }
         else
         {
            wb_table3_16_922e( false) ;
         }
      }

      protected void wb_table5_19_922( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableAssociation", 0, "", "", 4, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\" class='AssociationTitleCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblNotassociatedrecordstitle_Internalname, lblNotassociatedrecordstitle_Caption, "", "", lblNotassociatedrecordstitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "AssociationTitle", 0, "", 1, 1, 0, "HLP_WP_AssociarMenuPerfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\" class='AssociationTitleCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblCenterrecordstitle_Internalname, " ", "", "", lblCenterrecordstitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "AssociationTitle", 0, "", 1, 1, 0, "HLP_WP_AssociarMenuPerfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\" class='AssociationTitleCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblAssociatedrecordstitle_Internalname, lblAssociatedrecordstitle_Caption, "", "", lblAssociatedrecordstitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "AssociationTitle", 0, "", 1, 1, 0, "HLP_WP_AssociarMenuPerfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"JQNAOASSOCIADOSContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\">") ;
            wb_table6_31_922( true) ;
         }
         else
         {
            wb_table6_31_922( false) ;
         }
         return  ;
      }

      protected void wb_table6_31_922e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"JQASSOCIADOSContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_19_922e( true) ;
         }
         else
         {
            wb_table5_19_922e( false) ;
         }
      }

      protected void wb_table6_31_922( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 34,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgImageassociateall_Internalname, context.GetImagePath( "6591e2a3-49b6-43b7-b8e3-a292564a32a4", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgImageassociateall_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgImageassociateall_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ASSOCIATE ALL\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WP_AssociarMenuPerfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 37,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgImageassociateselected_Internalname, context.GetImagePath( "56a5f17b-0bc3-48b5-b303-afa6e0585b6d", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgImageassociateselected_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgImageassociateselected_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ASSOCIATE SELECTED\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WP_AssociarMenuPerfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgImagedisassociateselected_Internalname, context.GetImagePath( "a3800d0c-bf04-4575-bc01-11fe5d7b3525", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgImagedisassociateselected_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgImagedisassociateselected_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DISASSOCIATE SELECTED\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WP_AssociarMenuPerfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 43,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgImagedisassociateall_Internalname, context.GetImagePath( "c619e28f-4b32-4ff9-baaf-b3063fe4f782", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgImagedisassociateall_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgImagedisassociateall_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DISASSOCIATE ALL\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WP_AssociarMenuPerfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_31_922e( true) ;
         }
         else
         {
            wb_table6_31_922e( false) ;
         }
      }

      protected void wb_table2_8_922( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedassociationtitle_Internalname, tblTablemergedassociationtitle_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblAssociationtitle_Internalname, "Menus ao Perfil :: ", "", "", lblAssociationtitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WP_AssociarMenuPerfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtPerfil_Nome_Internalname, StringUtil.RTrim( A4Perfil_Nome), StringUtil.RTrim( context.localUtil.Format( A4Perfil_Nome, "@!")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtPerfil_Nome_Jsonclick, 0, "AttributeTitleWWP", "", "", "", 1, 0, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_WP_AssociarMenuPerfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_922e( true) ;
         }
         else
         {
            wb_table2_8_922e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7Perfil_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7Perfil_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Perfil_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPERFIL_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7Perfil_Codigo), "ZZZZZ9")));
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA922( ) ;
         WS922( ) ;
         WE922( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("jQueryUI/css/demos.css", "?1349420");
         AddStyleSheetFile("jQueryUI/css/demos.css", "?1349420");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20205623572466");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wp_associarmenuperfil.js", "?20205623572466");
         context.AddJavascriptSource("Shared/jquery/jquery-1.4.2.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.ui.core.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.ui.widget.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.ui.mouse.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.ui.selectable.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.corner.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery-1.4.2.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.ui.core.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.ui.widget.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.ui.mouse.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.ui.selectable.min.js", "");
         context.AddJavascriptSource("Shared/jqueryui/jquery.corner.js", "");
         context.AddJavascriptSource("jQueryUI/jqSelectRender.js", "");
         context.AddJavascriptSource("jQueryUI/jqSelectRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblAssociationtitle_Internalname = "ASSOCIATIONTITLE";
         edtPerfil_Nome_Internalname = "PERFIL_NOME";
         tblTablemergedassociationtitle_Internalname = "TABLEMERGEDASSOCIATIONTITLE";
         lblNotassociatedrecordstitle_Internalname = "NOTASSOCIATEDRECORDSTITLE";
         lblCenterrecordstitle_Internalname = "CENTERRECORDSTITLE";
         lblAssociatedrecordstitle_Internalname = "ASSOCIATEDRECORDSTITLE";
         Jqnaoassociados_Internalname = "JQNAOASSOCIADOS";
         imgImageassociateall_Internalname = "IMAGEASSOCIATEALL";
         imgImageassociateselected_Internalname = "IMAGEASSOCIATESELECTED";
         imgImagedisassociateselected_Internalname = "IMAGEDISASSOCIATESELECTED";
         imgImagedisassociateall_Internalname = "IMAGEDISASSOCIATEALL";
         tblTable1_Internalname = "TABLE1";
         Jqassociados_Internalname = "JQASSOCIADOS";
         tblTablecontent_Internalname = "TABLECONTENT";
         tblTablefullcontent_Internalname = "TABLEFULLCONTENT";
         bttBtn_confirm_Internalname = "BTN_CONFIRM";
         bttBtn_cancel_Internalname = "BTN_CANCEL";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtPerfil_Nome_Jsonclick = "";
         imgImagedisassociateall_Visible = 1;
         imgImagedisassociateselected_Visible = 1;
         imgImageassociateselected_Visible = 1;
         imgImageassociateall_Visible = 1;
         bttBtn_confirm_Visible = 1;
         lblAssociatedrecordstitle_Caption = "Associados";
         lblNotassociatedrecordstitle_Caption = "N�o Associados";
         bttBtn_cancel_Caption = "Fechar";
         Jqassociados_Itemheight = 17;
         Jqassociados_Itemwidth = 270;
         Jqassociados_Selectedcolor = (int)(0xE0E0E0);
         Jqassociados_Unselectedicon = "ui-icon-minus";
         Jqassociados_Theme = "base";
         Jqassociados_Rounding = "bevelfold bl";
         Jqassociados_Height = 300;
         Jqassociados_Width = 300;
         Jqnaoassociados_Itemheight = 17;
         Jqnaoassociados_Itemwidth = 270;
         Jqnaoassociados_Selectedcolor = (int)(0xE0E0E0);
         Jqnaoassociados_Unselectedicon = "ui-icon-minus";
         Jqnaoassociados_Theme = "base";
         Jqnaoassociados_Rounding = "bevelfold bl";
         Jqnaoassociados_Height = 300;
         Jqnaoassociados_Width = 300;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[{av:'imgImageassociateselected_Visible',ctrl:'IMAGEASSOCIATESELECTED',prop:'Visible'},{av:'imgImageassociateall_Visible',ctrl:'IMAGEASSOCIATEALL',prop:'Visible'},{av:'imgImagedisassociateselected_Visible',ctrl:'IMAGEDISASSOCIATESELECTED',prop:'Visible'},{av:'imgImagedisassociateall_Visible',ctrl:'IMAGEDISASSOCIATEALL',prop:'Visible'},{ctrl:'BTN_CONFIRM',prop:'Visible'}]}");
         setEventMetadata("ENTER","{handler:'E13922',iparms:[{av:'AV30jqAssociados',fld:'vJQASSOCIADOS',pic:'',nv:null},{av:'A3Perfil_Codigo',fld:'PERFIL_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV7Perfil_Codigo',fld:'vPERFIL_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A277Menu_Codigo',fld:'MENU_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV28jqNaoAssociados',fld:'vJQNAOASSOCIADOS',pic:'',nv:null},{av:'AV11MenuPerfil',fld:'vMENUPERFIL',pic:'',nv:null},{av:'AV8Menu_Codigo',fld:'vMENU_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV8Menu_Codigo',fld:'vMENU_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV11MenuPerfil',fld:'vMENUPERFIL',pic:'',nv:null}]}");
         setEventMetadata("'DISASSOCIATE SELECTED'","{handler:'E14922',iparms:[{av:'AV30jqAssociados',fld:'vJQASSOCIADOS',pic:'',nv:null},{av:'AV32All',fld:'vALL',pic:'',nv:false},{av:'AV28jqNaoAssociados',fld:'vJQNAOASSOCIADOS',pic:'',nv:null}],oparms:[{ctrl:'BTN_CANCEL',prop:'Caption'},{av:'AV32All',fld:'vALL',pic:'',nv:false},{av:'AV30jqAssociados',fld:'vJQASSOCIADOS',pic:'',nv:null},{av:'AV28jqNaoAssociados',fld:'vJQNAOASSOCIADOS',pic:'',nv:null},{av:'lblNotassociatedrecordstitle_Caption',ctrl:'NOTASSOCIATEDRECORDSTITLE',prop:'Caption'},{av:'lblAssociatedrecordstitle_Caption',ctrl:'ASSOCIATEDRECORDSTITLE',prop:'Caption'}]}");
         setEventMetadata("'ASSOCIATE SELECTED'","{handler:'E15922',iparms:[{av:'AV28jqNaoAssociados',fld:'vJQNAOASSOCIADOS',pic:'',nv:null},{av:'AV32All',fld:'vALL',pic:'',nv:false},{av:'AV30jqAssociados',fld:'vJQASSOCIADOS',pic:'',nv:null}],oparms:[{ctrl:'BTN_CANCEL',prop:'Caption'},{av:'AV32All',fld:'vALL',pic:'',nv:false},{av:'AV28jqNaoAssociados',fld:'vJQNAOASSOCIADOS',pic:'',nv:null},{av:'AV30jqAssociados',fld:'vJQASSOCIADOS',pic:'',nv:null},{av:'lblNotassociatedrecordstitle_Caption',ctrl:'NOTASSOCIATEDRECORDSTITLE',prop:'Caption'},{av:'lblAssociatedrecordstitle_Caption',ctrl:'ASSOCIATEDRECORDSTITLE',prop:'Caption'}]}");
         setEventMetadata("'ASSOCIATE ALL'","{handler:'E16922',iparms:[{av:'AV28jqNaoAssociados',fld:'vJQNAOASSOCIADOS',pic:'',nv:null},{av:'AV32All',fld:'vALL',pic:'',nv:false},{av:'AV30jqAssociados',fld:'vJQASSOCIADOS',pic:'',nv:null}],oparms:[{ctrl:'BTN_CANCEL',prop:'Caption'},{av:'AV32All',fld:'vALL',pic:'',nv:false},{av:'AV28jqNaoAssociados',fld:'vJQNAOASSOCIADOS',pic:'',nv:null},{av:'AV30jqAssociados',fld:'vJQASSOCIADOS',pic:'',nv:null},{av:'lblNotassociatedrecordstitle_Caption',ctrl:'NOTASSOCIATEDRECORDSTITLE',prop:'Caption'},{av:'lblAssociatedrecordstitle_Caption',ctrl:'ASSOCIATEDRECORDSTITLE',prop:'Caption'}]}");
         setEventMetadata("'DISASSOCIATE ALL'","{handler:'E17922',iparms:[{av:'AV30jqAssociados',fld:'vJQASSOCIADOS',pic:'',nv:null},{av:'AV32All',fld:'vALL',pic:'',nv:false},{av:'AV28jqNaoAssociados',fld:'vJQNAOASSOCIADOS',pic:'',nv:null}],oparms:[{ctrl:'BTN_CANCEL',prop:'Caption'},{av:'AV32All',fld:'vALL',pic:'',nv:false},{av:'AV30jqAssociados',fld:'vJQASSOCIADOS',pic:'',nv:null},{av:'AV28jqNaoAssociados',fld:'vJQNAOASSOCIADOS',pic:'',nv:null},{av:'lblNotassociatedrecordstitle_Caption',ctrl:'NOTASSOCIATEDRECORDSTITLE',prop:'Caption'},{av:'lblAssociatedrecordstitle_Caption',ctrl:'ASSOCIATEDRECORDSTITLE',prop:'Caption'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV28jqNaoAssociados = new GxObjectCollection( context, "jqSelectData.Item", "GxEv3Up14_MeetrikaVs3", "SdtjqSelectData_Item", "GeneXus.Programs");
         AV30jqAssociados = new GxObjectCollection( context, "jqSelectData.Item", "GxEv3Up14_MeetrikaVs3", "SdtjqSelectData_Item", "GeneXus.Programs");
         AV11MenuPerfil = new SdtMenuPerfil(context);
         A4Perfil_Nome = "";
         GXKey = "";
         GX_FocusControl = "";
         sPrefix = "";
         Form = new GXWebForm();
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         scmdbuf = "";
         H00922_A3Perfil_Codigo = new int[1] ;
         H00922_A4Perfil_Nome = new String[] {""} ;
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9HTTPRequest = new GxHttpRequest( context);
         H00923_A3Perfil_Codigo = new int[1] ;
         H00924_A277Menu_Codigo = new int[1] ;
         H00924_A285Menu_PaiCod = new int[1] ;
         H00924_n285Menu_PaiCod = new bool[] {false} ;
         H00924_A284Menu_Ativo = new bool[] {false} ;
         H00924_A278Menu_Nome = new String[] {""} ;
         H00924_A283Menu_Ordem = new short[1] ;
         H00924_A280Menu_Tipo = new short[1] ;
         A278Menu_Nome = "";
         H00925_A277Menu_Codigo = new int[1] ;
         H00925_A3Perfil_Codigo = new int[1] ;
         AV29jqItem = new SdtjqSelectData_Item(context);
         H00926_A285Menu_PaiCod = new int[1] ;
         H00926_n285Menu_PaiCod = new bool[] {false} ;
         H00926_A277Menu_Codigo = new int[1] ;
         H00926_A284Menu_Ativo = new bool[] {false} ;
         H00926_A278Menu_Nome = new String[] {""} ;
         H00926_A283Menu_Ordem = new short[1] ;
         H00926_A280Menu_Tipo = new short[1] ;
         H00927_A277Menu_Codigo = new int[1] ;
         H00927_A3Perfil_Codigo = new int[1] ;
         H00928_A277Menu_Codigo = new int[1] ;
         H00928_A3Perfil_Codigo = new int[1] ;
         H00929_A277Menu_Codigo = new int[1] ;
         H00929_A3Perfil_Codigo = new int[1] ;
         AV44GXV3 = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs");
         AV15Message = new SdtMessages_Message(context);
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtn_confirm_Jsonclick = "";
         bttBtn_cancel_Jsonclick = "";
         lblNotassociatedrecordstitle_Jsonclick = "";
         lblCenterrecordstitle_Jsonclick = "";
         lblAssociatedrecordstitle_Jsonclick = "";
         imgImageassociateall_Jsonclick = "";
         imgImageassociateselected_Jsonclick = "";
         imgImagedisassociateselected_Jsonclick = "";
         imgImagedisassociateall_Jsonclick = "";
         lblAssociationtitle_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wp_associarmenuperfil__default(),
            new Object[][] {
                new Object[] {
               H00922_A3Perfil_Codigo, H00922_A4Perfil_Nome
               }
               , new Object[] {
               H00923_A3Perfil_Codigo
               }
               , new Object[] {
               H00924_A277Menu_Codigo, H00924_A285Menu_PaiCod, H00924_n285Menu_PaiCod, H00924_A284Menu_Ativo, H00924_A278Menu_Nome, H00924_A283Menu_Ordem, H00924_A280Menu_Tipo
               }
               , new Object[] {
               H00925_A277Menu_Codigo, H00925_A3Perfil_Codigo
               }
               , new Object[] {
               H00926_A285Menu_PaiCod, H00926_n285Menu_PaiCod, H00926_A277Menu_Codigo, H00926_A284Menu_Ativo, H00926_A278Menu_Nome, H00926_A283Menu_Ordem, H00926_A280Menu_Tipo
               }
               , new Object[] {
               H00927_A277Menu_Codigo, H00927_A3Perfil_Codigo
               }
               , new Object[] {
               H00928_A277Menu_Codigo, H00928_A3Perfil_Codigo
               }
               , new Object[] {
               H00929_A277Menu_Codigo, H00929_A3Perfil_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short nRcdExists_9 ;
      private short nIsMod_9 ;
      private short nRcdExists_8 ;
      private short nIsMod_8 ;
      private short nGotPars ;
      private short GxWebError ;
      private short initialized ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short AV35GXLvl6 ;
      private short A283Menu_Ordem ;
      private short A280Menu_Tipo ;
      private short AV41GXLvl88 ;
      private short nGXWrapped ;
      private int AV7Perfil_Codigo ;
      private int wcpOAV7Perfil_Codigo ;
      private int A3Perfil_Codigo ;
      private int A277Menu_Codigo ;
      private int AV8Menu_Codigo ;
      private int Jqnaoassociados_Width ;
      private int Jqnaoassociados_Height ;
      private int Jqnaoassociados_Selectedcolor ;
      private int Jqnaoassociados_Itemwidth ;
      private int Jqnaoassociados_Itemheight ;
      private int Jqassociados_Width ;
      private int Jqassociados_Height ;
      private int Jqassociados_Selectedcolor ;
      private int Jqassociados_Itemwidth ;
      private int Jqassociados_Itemheight ;
      private int A285Menu_PaiCod ;
      private int AV27Menu_PaiCod ;
      private int imgImageassociateselected_Visible ;
      private int imgImageassociateall_Visible ;
      private int imgImagedisassociateselected_Visible ;
      private int imgImagedisassociateall_Visible ;
      private int bttBtn_confirm_Visible ;
      private int AV40GXV1 ;
      private int AV42GXV2 ;
      private int AV45GXV4 ;
      private int AV14i ;
      private int idxLst ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String A4Perfil_Nome ;
      private String Jqnaoassociados_Rounding ;
      private String Jqnaoassociados_Theme ;
      private String Jqnaoassociados_Unselectedicon ;
      private String Jqassociados_Rounding ;
      private String Jqassociados_Theme ;
      private String Jqassociados_Unselectedicon ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String scmdbuf ;
      private String edtPerfil_Nome_Internalname ;
      private String A278Menu_Nome ;
      private String imgImageassociateselected_Internalname ;
      private String imgImageassociateall_Internalname ;
      private String imgImagedisassociateselected_Internalname ;
      private String imgImagedisassociateall_Internalname ;
      private String bttBtn_confirm_Internalname ;
      private String bttBtn_cancel_Caption ;
      private String bttBtn_cancel_Internalname ;
      private String lblNotassociatedrecordstitle_Caption ;
      private String lblNotassociatedrecordstitle_Internalname ;
      private String lblAssociatedrecordstitle_Caption ;
      private String lblAssociatedrecordstitle_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String bttBtn_confirm_Jsonclick ;
      private String bttBtn_cancel_Jsonclick ;
      private String tblTablefullcontent_Internalname ;
      private String tblTablecontent_Internalname ;
      private String lblNotassociatedrecordstitle_Jsonclick ;
      private String lblCenterrecordstitle_Internalname ;
      private String lblCenterrecordstitle_Jsonclick ;
      private String lblAssociatedrecordstitle_Jsonclick ;
      private String tblTable1_Internalname ;
      private String imgImageassociateall_Jsonclick ;
      private String imgImageassociateselected_Jsonclick ;
      private String imgImagedisassociateselected_Jsonclick ;
      private String imgImagedisassociateall_Jsonclick ;
      private String tblTablemergedassociationtitle_Internalname ;
      private String lblAssociationtitle_Internalname ;
      private String lblAssociationtitle_Jsonclick ;
      private String edtPerfil_Nome_Jsonclick ;
      private String Jqnaoassociados_Internalname ;
      private String Jqassociados_Internalname ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool AV32All ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private bool n285Menu_PaiCod ;
      private bool A284Menu_Ativo ;
      private bool AV10Exist ;
      private bool AV12Success ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] H00922_A3Perfil_Codigo ;
      private String[] H00922_A4Perfil_Nome ;
      private int[] H00923_A3Perfil_Codigo ;
      private int[] H00924_A277Menu_Codigo ;
      private int[] H00924_A285Menu_PaiCod ;
      private bool[] H00924_n285Menu_PaiCod ;
      private bool[] H00924_A284Menu_Ativo ;
      private String[] H00924_A278Menu_Nome ;
      private short[] H00924_A283Menu_Ordem ;
      private short[] H00924_A280Menu_Tipo ;
      private int[] H00925_A277Menu_Codigo ;
      private int[] H00925_A3Perfil_Codigo ;
      private int[] H00926_A285Menu_PaiCod ;
      private bool[] H00926_n285Menu_PaiCod ;
      private int[] H00926_A277Menu_Codigo ;
      private bool[] H00926_A284Menu_Ativo ;
      private String[] H00926_A278Menu_Nome ;
      private short[] H00926_A283Menu_Ordem ;
      private short[] H00926_A280Menu_Tipo ;
      private int[] H00927_A277Menu_Codigo ;
      private int[] H00927_A3Perfil_Codigo ;
      private int[] H00928_A277Menu_Codigo ;
      private int[] H00928_A3Perfil_Codigo ;
      private int[] H00929_A277Menu_Codigo ;
      private int[] H00929_A3Perfil_Codigo ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV9HTTPRequest ;
      [ObjectCollection(ItemType=typeof( SdtjqSelectData_Item ))]
      private IGxCollection AV28jqNaoAssociados ;
      [ObjectCollection(ItemType=typeof( SdtjqSelectData_Item ))]
      private IGxCollection AV30jqAssociados ;
      [ObjectCollection(ItemType=typeof( SdtMessages_Message ))]
      private IGxCollection AV44GXV3 ;
      private SdtjqSelectData_Item AV29jqItem ;
      private SdtMenuPerfil AV11MenuPerfil ;
      private SdtMessages_Message AV15Message ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
   }

   public class wp_associarmenuperfil__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00922 ;
          prmH00922 = new Object[] {
          new Object[] {"@AV7Perfil_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00923 ;
          prmH00923 = new Object[] {
          new Object[] {"@AV7Perfil_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00924 ;
          prmH00924 = new Object[] {
          } ;
          Object[] prmH00925 ;
          prmH00925 = new Object[] {
          new Object[] {"@Menu_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV7Perfil_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00926 ;
          prmH00926 = new Object[] {
          new Object[] {"@AV27Menu_PaiCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00927 ;
          prmH00927 = new Object[] {
          new Object[] {"@Menu_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV7Perfil_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00928 ;
          prmH00928 = new Object[] {
          new Object[] {"@AV8Menu_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV7Perfil_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00929 ;
          prmH00929 = new Object[] {
          new Object[] {"@AV8Menu_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV7Perfil_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00922", "SELECT [Perfil_Codigo], [Perfil_Nome] FROM [Perfil] WITH (NOLOCK) WHERE [Perfil_Codigo] = @AV7Perfil_Codigo ORDER BY [Perfil_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00922,1,0,true,true )
             ,new CursorDef("H00923", "SELECT [Perfil_Codigo] FROM [Perfil] WITH (NOLOCK) WHERE [Perfil_Codigo] = @AV7Perfil_Codigo ORDER BY [Perfil_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00923,1,0,false,true )
             ,new CursorDef("H00924", "SELECT [Menu_Codigo], [Menu_PaiCod], [Menu_Ativo], [Menu_Nome], [Menu_Ordem], [Menu_Tipo] FROM [Menu] WITH (NOLOCK) WHERE ([Menu_Ativo] = 1) AND ([Menu_PaiCod] IS NULL) ORDER BY [Menu_PaiCod], [Menu_Tipo], [Menu_Ordem] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00924,100,0,true,false )
             ,new CursorDef("H00925", "SELECT TOP 1 [Menu_Codigo], [Perfil_Codigo] FROM [MenuPerfil] WITH (NOLOCK) WHERE [Menu_Codigo] = @Menu_Codigo and [Perfil_Codigo] = @AV7Perfil_Codigo ORDER BY [Menu_Codigo], [Perfil_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00925,1,0,false,true )
             ,new CursorDef("H00926", "SELECT [Menu_PaiCod], [Menu_Codigo], [Menu_Ativo], [Menu_Nome], [Menu_Ordem], [Menu_Tipo] FROM [Menu] WITH (NOLOCK) WHERE ([Menu_PaiCod] = @AV27Menu_PaiCod) AND ([Menu_Ativo] = 1) ORDER BY [Menu_PaiCod], [Menu_Tipo], [Menu_Ordem] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00926,100,0,true,false )
             ,new CursorDef("H00927", "SELECT TOP 1 [Menu_Codigo], [Perfil_Codigo] FROM [MenuPerfil] WITH (NOLOCK) WHERE [Menu_Codigo] = @Menu_Codigo and [Perfil_Codigo] = @AV7Perfil_Codigo ORDER BY [Menu_Codigo], [Perfil_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00927,1,0,false,true )
             ,new CursorDef("H00928", "SELECT TOP 1 [Menu_Codigo], [Perfil_Codigo] FROM [MenuPerfil] WITH (NOLOCK) WHERE [Menu_Codigo] = @AV8Menu_Codigo and [Perfil_Codigo] = @AV7Perfil_Codigo ORDER BY [Menu_Codigo], [Perfil_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00928,1,0,false,true )
             ,new CursorDef("H00929", "SELECT TOP 1 [Menu_Codigo], [Perfil_Codigo] FROM [MenuPerfil] WITH (NOLOCK) WHERE [Menu_Codigo] = @AV8Menu_Codigo and [Perfil_Codigo] = @AV7Perfil_Codigo ORDER BY [Menu_Codigo], [Perfil_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00929,1,0,true,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((bool[]) buf[3])[0] = rslt.getBool(3) ;
                ((String[]) buf[4])[0] = rslt.getString(4, 30) ;
                ((short[]) buf[5])[0] = rslt.getShort(5) ;
                ((short[]) buf[6])[0] = rslt.getShort(6) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.getBool(3) ;
                ((String[]) buf[4])[0] = rslt.getString(4, 30) ;
                ((short[]) buf[5])[0] = rslt.getShort(5) ;
                ((short[]) buf[6])[0] = rslt.getShort(6) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
       }
    }

 }

}
