/*
               File: WWContratoGarantia
        Description:  Contrato Garantia
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/18/2020 13:0:14.88
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wwcontratogarantia : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wwcontratogarantia( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wwcontratogarantia( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_142 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_142_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_142_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               AV15DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
               AV16ContratoGarantia_DataPagtoGarantia1 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16ContratoGarantia_DataPagtoGarantia1", context.localUtil.Format(AV16ContratoGarantia_DataPagtoGarantia1, "99/99/99"));
               AV17ContratoGarantia_DataPagtoGarantia_To1 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContratoGarantia_DataPagtoGarantia_To1", context.localUtil.Format(AV17ContratoGarantia_DataPagtoGarantia_To1, "99/99/99"));
               AV37ContratoGarantia_DataCaucao1 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37ContratoGarantia_DataCaucao1", context.localUtil.Format(AV37ContratoGarantia_DataCaucao1, "99/99/99"));
               AV38ContratoGarantia_DataCaucao_To1 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38ContratoGarantia_DataCaucao_To1", context.localUtil.Format(AV38ContratoGarantia_DataCaucao_To1, "99/99/99"));
               AV39ContratoGarantia_DataEncerramento1 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39ContratoGarantia_DataEncerramento1", context.localUtil.Format(AV39ContratoGarantia_DataEncerramento1, "99/99/99"));
               AV40ContratoGarantia_DataEncerramento_To1 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40ContratoGarantia_DataEncerramento_To1", context.localUtil.Format(AV40ContratoGarantia_DataEncerramento_To1, "99/99/99"));
               AV19DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
               AV20ContratoGarantia_DataPagtoGarantia2 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20ContratoGarantia_DataPagtoGarantia2", context.localUtil.Format(AV20ContratoGarantia_DataPagtoGarantia2, "99/99/99"));
               AV21ContratoGarantia_DataPagtoGarantia_To2 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ContratoGarantia_DataPagtoGarantia_To2", context.localUtil.Format(AV21ContratoGarantia_DataPagtoGarantia_To2, "99/99/99"));
               AV41ContratoGarantia_DataCaucao2 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41ContratoGarantia_DataCaucao2", context.localUtil.Format(AV41ContratoGarantia_DataCaucao2, "99/99/99"));
               AV42ContratoGarantia_DataCaucao_To2 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42ContratoGarantia_DataCaucao_To2", context.localUtil.Format(AV42ContratoGarantia_DataCaucao_To2, "99/99/99"));
               AV43ContratoGarantia_DataEncerramento2 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43ContratoGarantia_DataEncerramento2", context.localUtil.Format(AV43ContratoGarantia_DataEncerramento2, "99/99/99"));
               AV44ContratoGarantia_DataEncerramento_To2 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44ContratoGarantia_DataEncerramento_To2", context.localUtil.Format(AV44ContratoGarantia_DataEncerramento_To2, "99/99/99"));
               AV23DynamicFiltersSelector3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
               AV24ContratoGarantia_DataPagtoGarantia3 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24ContratoGarantia_DataPagtoGarantia3", context.localUtil.Format(AV24ContratoGarantia_DataPagtoGarantia3, "99/99/99"));
               AV25ContratoGarantia_DataPagtoGarantia_To3 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ContratoGarantia_DataPagtoGarantia_To3", context.localUtil.Format(AV25ContratoGarantia_DataPagtoGarantia_To3, "99/99/99"));
               AV45ContratoGarantia_DataCaucao3 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45ContratoGarantia_DataCaucao3", context.localUtil.Format(AV45ContratoGarantia_DataCaucao3, "99/99/99"));
               AV46ContratoGarantia_DataCaucao_To3 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46ContratoGarantia_DataCaucao_To3", context.localUtil.Format(AV46ContratoGarantia_DataCaucao_To3, "99/99/99"));
               AV47ContratoGarantia_DataEncerramento3 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47ContratoGarantia_DataEncerramento3", context.localUtil.Format(AV47ContratoGarantia_DataEncerramento3, "99/99/99"));
               AV48ContratoGarantia_DataEncerramento_To3 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48ContratoGarantia_DataEncerramento_To3", context.localUtil.Format(AV48ContratoGarantia_DataEncerramento_To3, "99/99/99"));
               AV18DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
               AV22DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
               AV52TFContrato_Numero = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52TFContrato_Numero", AV52TFContrato_Numero);
               AV53TFContrato_Numero_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53TFContrato_Numero_Sel", AV53TFContrato_Numero_Sel);
               AV56TFContratada_PessoaNom = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFContratada_PessoaNom", AV56TFContratada_PessoaNom);
               AV57TFContratada_PessoaNom_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57TFContratada_PessoaNom_Sel", AV57TFContratada_PessoaNom_Sel);
               AV60TFContratada_PessoaCNPJ = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFContratada_PessoaCNPJ", AV60TFContratada_PessoaCNPJ);
               AV61TFContratada_PessoaCNPJ_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61TFContratada_PessoaCNPJ_Sel", AV61TFContratada_PessoaCNPJ_Sel);
               AV64TFContratoGarantia_DataPagtoGarantia = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64TFContratoGarantia_DataPagtoGarantia", context.localUtil.Format(AV64TFContratoGarantia_DataPagtoGarantia, "99/99/99"));
               AV65TFContratoGarantia_DataPagtoGarantia_To = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65TFContratoGarantia_DataPagtoGarantia_To", context.localUtil.Format(AV65TFContratoGarantia_DataPagtoGarantia_To, "99/99/99"));
               AV70TFContratoGarantia_Percentual = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70TFContratoGarantia_Percentual", StringUtil.LTrim( StringUtil.Str( AV70TFContratoGarantia_Percentual, 6, 2)));
               AV71TFContratoGarantia_Percentual_To = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71TFContratoGarantia_Percentual_To", StringUtil.LTrim( StringUtil.Str( AV71TFContratoGarantia_Percentual_To, 6, 2)));
               AV74TFContratoGarantia_DataCaucao = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74TFContratoGarantia_DataCaucao", context.localUtil.Format(AV74TFContratoGarantia_DataCaucao, "99/99/99"));
               AV75TFContratoGarantia_DataCaucao_To = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75TFContratoGarantia_DataCaucao_To", context.localUtil.Format(AV75TFContratoGarantia_DataCaucao_To, "99/99/99"));
               AV80TFContratoGarantia_ValorGarantia = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80TFContratoGarantia_ValorGarantia", StringUtil.LTrim( StringUtil.Str( AV80TFContratoGarantia_ValorGarantia, 18, 5)));
               AV81TFContratoGarantia_ValorGarantia_To = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81TFContratoGarantia_ValorGarantia_To", StringUtil.LTrim( StringUtil.Str( AV81TFContratoGarantia_ValorGarantia_To, 18, 5)));
               AV84TFContratoGarantia_DataEncerramento = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV84TFContratoGarantia_DataEncerramento", context.localUtil.Format(AV84TFContratoGarantia_DataEncerramento, "99/99/99"));
               AV85TFContratoGarantia_DataEncerramento_To = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV85TFContratoGarantia_DataEncerramento_To", context.localUtil.Format(AV85TFContratoGarantia_DataEncerramento_To, "99/99/99"));
               AV90TFContratoGarantia_ValorEncerramento = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV90TFContratoGarantia_ValorEncerramento", StringUtil.LTrim( StringUtil.Str( AV90TFContratoGarantia_ValorEncerramento, 18, 5)));
               AV91TFContratoGarantia_ValorEncerramento_To = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV91TFContratoGarantia_ValorEncerramento_To", StringUtil.LTrim( StringUtil.Str( AV91TFContratoGarantia_ValorEncerramento_To, 18, 5)));
               AV54ddo_Contrato_NumeroTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54ddo_Contrato_NumeroTitleControlIdToReplace", AV54ddo_Contrato_NumeroTitleControlIdToReplace);
               AV58ddo_Contratada_PessoaNomTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58ddo_Contratada_PessoaNomTitleControlIdToReplace", AV58ddo_Contratada_PessoaNomTitleControlIdToReplace);
               AV62ddo_Contratada_PessoaCNPJTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62ddo_Contratada_PessoaCNPJTitleControlIdToReplace", AV62ddo_Contratada_PessoaCNPJTitleControlIdToReplace);
               AV68ddo_ContratoGarantia_DataPagtoGarantiaTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68ddo_ContratoGarantia_DataPagtoGarantiaTitleControlIdToReplace", AV68ddo_ContratoGarantia_DataPagtoGarantiaTitleControlIdToReplace);
               AV72ddo_ContratoGarantia_PercentualTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72ddo_ContratoGarantia_PercentualTitleControlIdToReplace", AV72ddo_ContratoGarantia_PercentualTitleControlIdToReplace);
               AV78ddo_ContratoGarantia_DataCaucaoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV78ddo_ContratoGarantia_DataCaucaoTitleControlIdToReplace", AV78ddo_ContratoGarantia_DataCaucaoTitleControlIdToReplace);
               AV82ddo_ContratoGarantia_ValorGarantiaTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV82ddo_ContratoGarantia_ValorGarantiaTitleControlIdToReplace", AV82ddo_ContratoGarantia_ValorGarantiaTitleControlIdToReplace);
               AV88ddo_ContratoGarantia_DataEncerramentoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV88ddo_ContratoGarantia_DataEncerramentoTitleControlIdToReplace", AV88ddo_ContratoGarantia_DataEncerramentoTitleControlIdToReplace);
               AV92ddo_ContratoGarantia_ValorEncerramentoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV92ddo_ContratoGarantia_ValorEncerramentoTitleControlIdToReplace", AV92ddo_ContratoGarantia_ValorEncerramentoTitleControlIdToReplace);
               AV157Pgmname = GetNextPar( );
               ajax_req_read_hidden_sdt(GetNextPar( ), AV10GridState);
               AV27DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
               AV26DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
               A101ContratoGarantia_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A101ContratoGarantia_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A101ContratoGarantia_Codigo), 6, 0)));
               A74Contrato_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
               A39Contratada_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A39Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A39Contratada_Codigo), 6, 0)));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16ContratoGarantia_DataPagtoGarantia1, AV17ContratoGarantia_DataPagtoGarantia_To1, AV37ContratoGarantia_DataCaucao1, AV38ContratoGarantia_DataCaucao_To1, AV39ContratoGarantia_DataEncerramento1, AV40ContratoGarantia_DataEncerramento_To1, AV19DynamicFiltersSelector2, AV20ContratoGarantia_DataPagtoGarantia2, AV21ContratoGarantia_DataPagtoGarantia_To2, AV41ContratoGarantia_DataCaucao2, AV42ContratoGarantia_DataCaucao_To2, AV43ContratoGarantia_DataEncerramento2, AV44ContratoGarantia_DataEncerramento_To2, AV23DynamicFiltersSelector3, AV24ContratoGarantia_DataPagtoGarantia3, AV25ContratoGarantia_DataPagtoGarantia_To3, AV45ContratoGarantia_DataCaucao3, AV46ContratoGarantia_DataCaucao_To3, AV47ContratoGarantia_DataEncerramento3, AV48ContratoGarantia_DataEncerramento_To3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV52TFContrato_Numero, AV53TFContrato_Numero_Sel, AV56TFContratada_PessoaNom, AV57TFContratada_PessoaNom_Sel, AV60TFContratada_PessoaCNPJ, AV61TFContratada_PessoaCNPJ_Sel, AV64TFContratoGarantia_DataPagtoGarantia, AV65TFContratoGarantia_DataPagtoGarantia_To, AV70TFContratoGarantia_Percentual, AV71TFContratoGarantia_Percentual_To, AV74TFContratoGarantia_DataCaucao, AV75TFContratoGarantia_DataCaucao_To, AV80TFContratoGarantia_ValorGarantia, AV81TFContratoGarantia_ValorGarantia_To, AV84TFContratoGarantia_DataEncerramento, AV85TFContratoGarantia_DataEncerramento_To, AV90TFContratoGarantia_ValorEncerramento, AV91TFContratoGarantia_ValorEncerramento_To, AV54ddo_Contrato_NumeroTitleControlIdToReplace, AV58ddo_Contratada_PessoaNomTitleControlIdToReplace, AV62ddo_Contratada_PessoaCNPJTitleControlIdToReplace, AV68ddo_ContratoGarantia_DataPagtoGarantiaTitleControlIdToReplace, AV72ddo_ContratoGarantia_PercentualTitleControlIdToReplace, AV78ddo_ContratoGarantia_DataCaucaoTitleControlIdToReplace, AV82ddo_ContratoGarantia_ValorGarantiaTitleControlIdToReplace, AV88ddo_ContratoGarantia_DataEncerramentoTitleControlIdToReplace, AV92ddo_ContratoGarantia_ValorEncerramentoTitleControlIdToReplace, AV157Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A101ContratoGarantia_Codigo, A74Contrato_Codigo, A39Contratada_Codigo) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PA6H2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            START6H2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20205181301592");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wwcontratogarantia.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV15DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATOGARANTIA_DATAPAGTOGARANTIA1", context.localUtil.Format(AV16ContratoGarantia_DataPagtoGarantia1, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO1", context.localUtil.Format(AV17ContratoGarantia_DataPagtoGarantia_To1, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATOGARANTIA_DATACAUCAO1", context.localUtil.Format(AV37ContratoGarantia_DataCaucao1, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATOGARANTIA_DATACAUCAO_TO1", context.localUtil.Format(AV38ContratoGarantia_DataCaucao_To1, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATOGARANTIA_DATAENCERRAMENTO1", context.localUtil.Format(AV39ContratoGarantia_DataEncerramento1, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATOGARANTIA_DATAENCERRAMENTO_TO1", context.localUtil.Format(AV40ContratoGarantia_DataEncerramento_To1, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV19DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATOGARANTIA_DATAPAGTOGARANTIA2", context.localUtil.Format(AV20ContratoGarantia_DataPagtoGarantia2, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO2", context.localUtil.Format(AV21ContratoGarantia_DataPagtoGarantia_To2, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATOGARANTIA_DATACAUCAO2", context.localUtil.Format(AV41ContratoGarantia_DataCaucao2, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATOGARANTIA_DATACAUCAO_TO2", context.localUtil.Format(AV42ContratoGarantia_DataCaucao_To2, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATOGARANTIA_DATAENCERRAMENTO2", context.localUtil.Format(AV43ContratoGarantia_DataEncerramento2, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATOGARANTIA_DATAENCERRAMENTO_TO2", context.localUtil.Format(AV44ContratoGarantia_DataEncerramento_To2, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR3", AV23DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATOGARANTIA_DATAPAGTOGARANTIA3", context.localUtil.Format(AV24ContratoGarantia_DataPagtoGarantia3, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO3", context.localUtil.Format(AV25ContratoGarantia_DataPagtoGarantia_To3, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATOGARANTIA_DATACAUCAO3", context.localUtil.Format(AV45ContratoGarantia_DataCaucao3, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATOGARANTIA_DATACAUCAO_TO3", context.localUtil.Format(AV46ContratoGarantia_DataCaucao_To3, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATOGARANTIA_DATAENCERRAMENTO3", context.localUtil.Format(AV47ContratoGarantia_DataEncerramento3, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATOGARANTIA_DATAENCERRAMENTO_TO3", context.localUtil.Format(AV48ContratoGarantia_DataEncerramento_To3, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV18DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV22DynamicFiltersEnabled3));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATO_NUMERO", StringUtil.RTrim( AV52TFContrato_Numero));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATO_NUMERO_SEL", StringUtil.RTrim( AV53TFContrato_Numero_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATADA_PESSOANOM", StringUtil.RTrim( AV56TFContratada_PessoaNom));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATADA_PESSOANOM_SEL", StringUtil.RTrim( AV57TFContratada_PessoaNom_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATADA_PESSOACNPJ", AV60TFContratada_PessoaCNPJ);
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATADA_PESSOACNPJ_SEL", AV61TFContratada_PessoaCNPJ_Sel);
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOGARANTIA_DATAPAGTOGARANTIA", context.localUtil.Format(AV64TFContratoGarantia_DataPagtoGarantia, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO", context.localUtil.Format(AV65TFContratoGarantia_DataPagtoGarantia_To, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOGARANTIA_PERCENTUAL", StringUtil.LTrim( StringUtil.NToC( AV70TFContratoGarantia_Percentual, 6, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOGARANTIA_PERCENTUAL_TO", StringUtil.LTrim( StringUtil.NToC( AV71TFContratoGarantia_Percentual_To, 6, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOGARANTIA_DATACAUCAO", context.localUtil.Format(AV74TFContratoGarantia_DataCaucao, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOGARANTIA_DATACAUCAO_TO", context.localUtil.Format(AV75TFContratoGarantia_DataCaucao_To, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOGARANTIA_VALORGARANTIA", StringUtil.LTrim( StringUtil.NToC( AV80TFContratoGarantia_ValorGarantia, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOGARANTIA_VALORGARANTIA_TO", StringUtil.LTrim( StringUtil.NToC( AV81TFContratoGarantia_ValorGarantia_To, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOGARANTIA_DATAENCERRAMENTO", context.localUtil.Format(AV84TFContratoGarantia_DataEncerramento, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOGARANTIA_DATAENCERRAMENTO_TO", context.localUtil.Format(AV85TFContratoGarantia_DataEncerramento_To, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOGARANTIA_VALORENCERRAMENTO", StringUtil.LTrim( StringUtil.NToC( AV90TFContratoGarantia_ValorEncerramento, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATOGARANTIA_VALORENCERRAMENTO_TO", StringUtil.LTrim( StringUtil.NToC( AV91TFContratoGarantia_ValorEncerramento_To, 18, 5, ",", "")));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_142", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_142), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV95GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV96GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV93DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV93DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATO_NUMEROTITLEFILTERDATA", AV51Contrato_NumeroTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATO_NUMEROTITLEFILTERDATA", AV51Contrato_NumeroTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATADA_PESSOANOMTITLEFILTERDATA", AV55Contratada_PessoaNomTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATADA_PESSOANOMTITLEFILTERDATA", AV55Contratada_PessoaNomTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATADA_PESSOACNPJTITLEFILTERDATA", AV59Contratada_PessoaCNPJTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATADA_PESSOACNPJTITLEFILTERDATA", AV59Contratada_PessoaCNPJTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATOGARANTIA_DATAPAGTOGARANTIATITLEFILTERDATA", AV63ContratoGarantia_DataPagtoGarantiaTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATOGARANTIA_DATAPAGTOGARANTIATITLEFILTERDATA", AV63ContratoGarantia_DataPagtoGarantiaTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATOGARANTIA_PERCENTUALTITLEFILTERDATA", AV69ContratoGarantia_PercentualTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATOGARANTIA_PERCENTUALTITLEFILTERDATA", AV69ContratoGarantia_PercentualTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATOGARANTIA_DATACAUCAOTITLEFILTERDATA", AV73ContratoGarantia_DataCaucaoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATOGARANTIA_DATACAUCAOTITLEFILTERDATA", AV73ContratoGarantia_DataCaucaoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATOGARANTIA_VALORGARANTIATITLEFILTERDATA", AV79ContratoGarantia_ValorGarantiaTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATOGARANTIA_VALORGARANTIATITLEFILTERDATA", AV79ContratoGarantia_ValorGarantiaTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATOGARANTIA_DATAENCERRAMENTOTITLEFILTERDATA", AV83ContratoGarantia_DataEncerramentoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATOGARANTIA_DATAENCERRAMENTOTITLEFILTERDATA", AV83ContratoGarantia_DataEncerramentoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATOGARANTIA_VALORENCERRAMENTOTITLEFILTERDATA", AV89ContratoGarantia_ValorEncerramentoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATOGARANTIA_VALORENCERRAMENTOTITLEFILTERDATA", AV89ContratoGarantia_ValorEncerramentoTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV157Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV27DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV26DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, "CONTRATOGARANTIA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A101ContratoGarantia_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A74Contrato_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATADA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A39Contratada_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Caption", StringUtil.RTrim( Ddo_contrato_numero_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Tooltip", StringUtil.RTrim( Ddo_contrato_numero_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Cls", StringUtil.RTrim( Ddo_contrato_numero_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Filteredtext_set", StringUtil.RTrim( Ddo_contrato_numero_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Selectedvalue_set", StringUtil.RTrim( Ddo_contrato_numero_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Dropdownoptionstype", StringUtil.RTrim( Ddo_contrato_numero_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contrato_numero_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Includesortasc", StringUtil.BoolToStr( Ddo_contrato_numero_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Includesortdsc", StringUtil.BoolToStr( Ddo_contrato_numero_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Sortedstatus", StringUtil.RTrim( Ddo_contrato_numero_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Includefilter", StringUtil.BoolToStr( Ddo_contrato_numero_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Filtertype", StringUtil.RTrim( Ddo_contrato_numero_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Filterisrange", StringUtil.BoolToStr( Ddo_contrato_numero_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Includedatalist", StringUtil.BoolToStr( Ddo_contrato_numero_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Datalisttype", StringUtil.RTrim( Ddo_contrato_numero_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Datalistproc", StringUtil.RTrim( Ddo_contrato_numero_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contrato_numero_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Sortasc", StringUtil.RTrim( Ddo_contrato_numero_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Sortdsc", StringUtil.RTrim( Ddo_contrato_numero_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Loadingdata", StringUtil.RTrim( Ddo_contrato_numero_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Cleanfilter", StringUtil.RTrim( Ddo_contrato_numero_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Noresultsfound", StringUtil.RTrim( Ddo_contrato_numero_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Searchbuttontext", StringUtil.RTrim( Ddo_contrato_numero_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Caption", StringUtil.RTrim( Ddo_contratada_pessoanom_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Tooltip", StringUtil.RTrim( Ddo_contratada_pessoanom_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Cls", StringUtil.RTrim( Ddo_contratada_pessoanom_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Filteredtext_set", StringUtil.RTrim( Ddo_contratada_pessoanom_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Selectedvalue_set", StringUtil.RTrim( Ddo_contratada_pessoanom_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratada_pessoanom_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratada_pessoanom_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Includesortasc", StringUtil.BoolToStr( Ddo_contratada_pessoanom_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Includesortdsc", StringUtil.BoolToStr( Ddo_contratada_pessoanom_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Sortedstatus", StringUtil.RTrim( Ddo_contratada_pessoanom_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Includefilter", StringUtil.BoolToStr( Ddo_contratada_pessoanom_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Filtertype", StringUtil.RTrim( Ddo_contratada_pessoanom_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Filterisrange", StringUtil.BoolToStr( Ddo_contratada_pessoanom_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Includedatalist", StringUtil.BoolToStr( Ddo_contratada_pessoanom_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Datalisttype", StringUtil.RTrim( Ddo_contratada_pessoanom_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Datalistproc", StringUtil.RTrim( Ddo_contratada_pessoanom_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contratada_pessoanom_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Sortasc", StringUtil.RTrim( Ddo_contratada_pessoanom_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Sortdsc", StringUtil.RTrim( Ddo_contratada_pessoanom_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Loadingdata", StringUtil.RTrim( Ddo_contratada_pessoanom_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Cleanfilter", StringUtil.RTrim( Ddo_contratada_pessoanom_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Noresultsfound", StringUtil.RTrim( Ddo_contratada_pessoanom_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Searchbuttontext", StringUtil.RTrim( Ddo_contratada_pessoanom_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Caption", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Tooltip", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Cls", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Filteredtext_set", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Selectedvalue_set", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Includesortasc", StringUtil.BoolToStr( Ddo_contratada_pessoacnpj_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Includesortdsc", StringUtil.BoolToStr( Ddo_contratada_pessoacnpj_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Sortedstatus", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Includefilter", StringUtil.BoolToStr( Ddo_contratada_pessoacnpj_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Filtertype", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Filterisrange", StringUtil.BoolToStr( Ddo_contratada_pessoacnpj_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Includedatalist", StringUtil.BoolToStr( Ddo_contratada_pessoacnpj_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Datalisttype", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Datalistproc", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contratada_pessoacnpj_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Sortasc", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Sortdsc", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Loadingdata", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Cleanfilter", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Noresultsfound", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Searchbuttontext", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA_Caption", StringUtil.RTrim( Ddo_contratogarantia_datapagtogarantia_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA_Tooltip", StringUtil.RTrim( Ddo_contratogarantia_datapagtogarantia_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA_Cls", StringUtil.RTrim( Ddo_contratogarantia_datapagtogarantia_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA_Filteredtext_set", StringUtil.RTrim( Ddo_contratogarantia_datapagtogarantia_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA_Filteredtextto_set", StringUtil.RTrim( Ddo_contratogarantia_datapagtogarantia_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratogarantia_datapagtogarantia_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratogarantia_datapagtogarantia_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA_Includesortasc", StringUtil.BoolToStr( Ddo_contratogarantia_datapagtogarantia_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA_Includesortdsc", StringUtil.BoolToStr( Ddo_contratogarantia_datapagtogarantia_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA_Sortedstatus", StringUtil.RTrim( Ddo_contratogarantia_datapagtogarantia_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA_Includefilter", StringUtil.BoolToStr( Ddo_contratogarantia_datapagtogarantia_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA_Filtertype", StringUtil.RTrim( Ddo_contratogarantia_datapagtogarantia_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA_Filterisrange", StringUtil.BoolToStr( Ddo_contratogarantia_datapagtogarantia_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA_Includedatalist", StringUtil.BoolToStr( Ddo_contratogarantia_datapagtogarantia_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA_Sortasc", StringUtil.RTrim( Ddo_contratogarantia_datapagtogarantia_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA_Sortdsc", StringUtil.RTrim( Ddo_contratogarantia_datapagtogarantia_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA_Cleanfilter", StringUtil.RTrim( Ddo_contratogarantia_datapagtogarantia_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA_Rangefilterfrom", StringUtil.RTrim( Ddo_contratogarantia_datapagtogarantia_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA_Rangefilterto", StringUtil.RTrim( Ddo_contratogarantia_datapagtogarantia_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA_Searchbuttontext", StringUtil.RTrim( Ddo_contratogarantia_datapagtogarantia_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_PERCENTUAL_Caption", StringUtil.RTrim( Ddo_contratogarantia_percentual_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_PERCENTUAL_Tooltip", StringUtil.RTrim( Ddo_contratogarantia_percentual_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_PERCENTUAL_Cls", StringUtil.RTrim( Ddo_contratogarantia_percentual_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_PERCENTUAL_Filteredtext_set", StringUtil.RTrim( Ddo_contratogarantia_percentual_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_PERCENTUAL_Filteredtextto_set", StringUtil.RTrim( Ddo_contratogarantia_percentual_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_PERCENTUAL_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratogarantia_percentual_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_PERCENTUAL_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratogarantia_percentual_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_PERCENTUAL_Includesortasc", StringUtil.BoolToStr( Ddo_contratogarantia_percentual_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_PERCENTUAL_Includesortdsc", StringUtil.BoolToStr( Ddo_contratogarantia_percentual_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_PERCENTUAL_Sortedstatus", StringUtil.RTrim( Ddo_contratogarantia_percentual_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_PERCENTUAL_Includefilter", StringUtil.BoolToStr( Ddo_contratogarantia_percentual_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_PERCENTUAL_Filtertype", StringUtil.RTrim( Ddo_contratogarantia_percentual_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_PERCENTUAL_Filterisrange", StringUtil.BoolToStr( Ddo_contratogarantia_percentual_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_PERCENTUAL_Includedatalist", StringUtil.BoolToStr( Ddo_contratogarantia_percentual_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_PERCENTUAL_Sortasc", StringUtil.RTrim( Ddo_contratogarantia_percentual_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_PERCENTUAL_Sortdsc", StringUtil.RTrim( Ddo_contratogarantia_percentual_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_PERCENTUAL_Cleanfilter", StringUtil.RTrim( Ddo_contratogarantia_percentual_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_PERCENTUAL_Rangefilterfrom", StringUtil.RTrim( Ddo_contratogarantia_percentual_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_PERCENTUAL_Rangefilterto", StringUtil.RTrim( Ddo_contratogarantia_percentual_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_PERCENTUAL_Searchbuttontext", StringUtil.RTrim( Ddo_contratogarantia_percentual_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATACAUCAO_Caption", StringUtil.RTrim( Ddo_contratogarantia_datacaucao_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATACAUCAO_Tooltip", StringUtil.RTrim( Ddo_contratogarantia_datacaucao_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATACAUCAO_Cls", StringUtil.RTrim( Ddo_contratogarantia_datacaucao_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATACAUCAO_Filteredtext_set", StringUtil.RTrim( Ddo_contratogarantia_datacaucao_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATACAUCAO_Filteredtextto_set", StringUtil.RTrim( Ddo_contratogarantia_datacaucao_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATACAUCAO_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratogarantia_datacaucao_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATACAUCAO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratogarantia_datacaucao_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATACAUCAO_Includesortasc", StringUtil.BoolToStr( Ddo_contratogarantia_datacaucao_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATACAUCAO_Includesortdsc", StringUtil.BoolToStr( Ddo_contratogarantia_datacaucao_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATACAUCAO_Sortedstatus", StringUtil.RTrim( Ddo_contratogarantia_datacaucao_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATACAUCAO_Includefilter", StringUtil.BoolToStr( Ddo_contratogarantia_datacaucao_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATACAUCAO_Filtertype", StringUtil.RTrim( Ddo_contratogarantia_datacaucao_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATACAUCAO_Filterisrange", StringUtil.BoolToStr( Ddo_contratogarantia_datacaucao_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATACAUCAO_Includedatalist", StringUtil.BoolToStr( Ddo_contratogarantia_datacaucao_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATACAUCAO_Sortasc", StringUtil.RTrim( Ddo_contratogarantia_datacaucao_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATACAUCAO_Sortdsc", StringUtil.RTrim( Ddo_contratogarantia_datacaucao_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATACAUCAO_Cleanfilter", StringUtil.RTrim( Ddo_contratogarantia_datacaucao_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATACAUCAO_Rangefilterfrom", StringUtil.RTrim( Ddo_contratogarantia_datacaucao_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATACAUCAO_Rangefilterto", StringUtil.RTrim( Ddo_contratogarantia_datacaucao_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATACAUCAO_Searchbuttontext", StringUtil.RTrim( Ddo_contratogarantia_datacaucao_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_VALORGARANTIA_Caption", StringUtil.RTrim( Ddo_contratogarantia_valorgarantia_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_VALORGARANTIA_Tooltip", StringUtil.RTrim( Ddo_contratogarantia_valorgarantia_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_VALORGARANTIA_Cls", StringUtil.RTrim( Ddo_contratogarantia_valorgarantia_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_VALORGARANTIA_Filteredtext_set", StringUtil.RTrim( Ddo_contratogarantia_valorgarantia_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_VALORGARANTIA_Filteredtextto_set", StringUtil.RTrim( Ddo_contratogarantia_valorgarantia_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_VALORGARANTIA_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratogarantia_valorgarantia_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_VALORGARANTIA_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratogarantia_valorgarantia_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_VALORGARANTIA_Includesortasc", StringUtil.BoolToStr( Ddo_contratogarantia_valorgarantia_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_VALORGARANTIA_Includesortdsc", StringUtil.BoolToStr( Ddo_contratogarantia_valorgarantia_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_VALORGARANTIA_Sortedstatus", StringUtil.RTrim( Ddo_contratogarantia_valorgarantia_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_VALORGARANTIA_Includefilter", StringUtil.BoolToStr( Ddo_contratogarantia_valorgarantia_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_VALORGARANTIA_Filtertype", StringUtil.RTrim( Ddo_contratogarantia_valorgarantia_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_VALORGARANTIA_Filterisrange", StringUtil.BoolToStr( Ddo_contratogarantia_valorgarantia_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_VALORGARANTIA_Includedatalist", StringUtil.BoolToStr( Ddo_contratogarantia_valorgarantia_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_VALORGARANTIA_Sortasc", StringUtil.RTrim( Ddo_contratogarantia_valorgarantia_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_VALORGARANTIA_Sortdsc", StringUtil.RTrim( Ddo_contratogarantia_valorgarantia_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_VALORGARANTIA_Cleanfilter", StringUtil.RTrim( Ddo_contratogarantia_valorgarantia_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_VALORGARANTIA_Rangefilterfrom", StringUtil.RTrim( Ddo_contratogarantia_valorgarantia_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_VALORGARANTIA_Rangefilterto", StringUtil.RTrim( Ddo_contratogarantia_valorgarantia_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_VALORGARANTIA_Searchbuttontext", StringUtil.RTrim( Ddo_contratogarantia_valorgarantia_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATAENCERRAMENTO_Caption", StringUtil.RTrim( Ddo_contratogarantia_dataencerramento_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATAENCERRAMENTO_Tooltip", StringUtil.RTrim( Ddo_contratogarantia_dataencerramento_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATAENCERRAMENTO_Cls", StringUtil.RTrim( Ddo_contratogarantia_dataencerramento_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATAENCERRAMENTO_Filteredtext_set", StringUtil.RTrim( Ddo_contratogarantia_dataencerramento_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATAENCERRAMENTO_Filteredtextto_set", StringUtil.RTrim( Ddo_contratogarantia_dataencerramento_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATAENCERRAMENTO_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratogarantia_dataencerramento_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATAENCERRAMENTO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratogarantia_dataencerramento_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATAENCERRAMENTO_Includesortasc", StringUtil.BoolToStr( Ddo_contratogarantia_dataencerramento_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATAENCERRAMENTO_Includesortdsc", StringUtil.BoolToStr( Ddo_contratogarantia_dataencerramento_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATAENCERRAMENTO_Sortedstatus", StringUtil.RTrim( Ddo_contratogarantia_dataencerramento_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATAENCERRAMENTO_Includefilter", StringUtil.BoolToStr( Ddo_contratogarantia_dataencerramento_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATAENCERRAMENTO_Filtertype", StringUtil.RTrim( Ddo_contratogarantia_dataencerramento_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATAENCERRAMENTO_Filterisrange", StringUtil.BoolToStr( Ddo_contratogarantia_dataencerramento_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATAENCERRAMENTO_Includedatalist", StringUtil.BoolToStr( Ddo_contratogarantia_dataencerramento_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATAENCERRAMENTO_Sortasc", StringUtil.RTrim( Ddo_contratogarantia_dataencerramento_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATAENCERRAMENTO_Sortdsc", StringUtil.RTrim( Ddo_contratogarantia_dataencerramento_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATAENCERRAMENTO_Cleanfilter", StringUtil.RTrim( Ddo_contratogarantia_dataencerramento_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATAENCERRAMENTO_Rangefilterfrom", StringUtil.RTrim( Ddo_contratogarantia_dataencerramento_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATAENCERRAMENTO_Rangefilterto", StringUtil.RTrim( Ddo_contratogarantia_dataencerramento_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATAENCERRAMENTO_Searchbuttontext", StringUtil.RTrim( Ddo_contratogarantia_dataencerramento_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_VALORENCERRAMENTO_Caption", StringUtil.RTrim( Ddo_contratogarantia_valorencerramento_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_VALORENCERRAMENTO_Tooltip", StringUtil.RTrim( Ddo_contratogarantia_valorencerramento_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_VALORENCERRAMENTO_Cls", StringUtil.RTrim( Ddo_contratogarantia_valorencerramento_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_VALORENCERRAMENTO_Filteredtext_set", StringUtil.RTrim( Ddo_contratogarantia_valorencerramento_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_VALORENCERRAMENTO_Filteredtextto_set", StringUtil.RTrim( Ddo_contratogarantia_valorencerramento_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_VALORENCERRAMENTO_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratogarantia_valorencerramento_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_VALORENCERRAMENTO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratogarantia_valorencerramento_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_VALORENCERRAMENTO_Includesortasc", StringUtil.BoolToStr( Ddo_contratogarantia_valorencerramento_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_VALORENCERRAMENTO_Includesortdsc", StringUtil.BoolToStr( Ddo_contratogarantia_valorencerramento_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_VALORENCERRAMENTO_Sortedstatus", StringUtil.RTrim( Ddo_contratogarantia_valorencerramento_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_VALORENCERRAMENTO_Includefilter", StringUtil.BoolToStr( Ddo_contratogarantia_valorencerramento_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_VALORENCERRAMENTO_Filtertype", StringUtil.RTrim( Ddo_contratogarantia_valorencerramento_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_VALORENCERRAMENTO_Filterisrange", StringUtil.BoolToStr( Ddo_contratogarantia_valorencerramento_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_VALORENCERRAMENTO_Includedatalist", StringUtil.BoolToStr( Ddo_contratogarantia_valorencerramento_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_VALORENCERRAMENTO_Sortasc", StringUtil.RTrim( Ddo_contratogarantia_valorencerramento_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_VALORENCERRAMENTO_Sortdsc", StringUtil.RTrim( Ddo_contratogarantia_valorencerramento_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_VALORENCERRAMENTO_Cleanfilter", StringUtil.RTrim( Ddo_contratogarantia_valorencerramento_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_VALORENCERRAMENTO_Rangefilterfrom", StringUtil.RTrim( Ddo_contratogarantia_valorencerramento_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_VALORENCERRAMENTO_Rangefilterto", StringUtil.RTrim( Ddo_contratogarantia_valorencerramento_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_VALORENCERRAMENTO_Searchbuttontext", StringUtil.RTrim( Ddo_contratogarantia_valorencerramento_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Activeeventkey", StringUtil.RTrim( Ddo_contrato_numero_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Filteredtext_get", StringUtil.RTrim( Ddo_contrato_numero_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_NUMERO_Selectedvalue_get", StringUtil.RTrim( Ddo_contrato_numero_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Activeeventkey", StringUtil.RTrim( Ddo_contratada_pessoanom_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Filteredtext_get", StringUtil.RTrim( Ddo_contratada_pessoanom_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOANOM_Selectedvalue_get", StringUtil.RTrim( Ddo_contratada_pessoanom_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Activeeventkey", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Filteredtext_get", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_PESSOACNPJ_Selectedvalue_get", StringUtil.RTrim( Ddo_contratada_pessoacnpj_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA_Activeeventkey", StringUtil.RTrim( Ddo_contratogarantia_datapagtogarantia_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA_Filteredtext_get", StringUtil.RTrim( Ddo_contratogarantia_datapagtogarantia_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA_Filteredtextto_get", StringUtil.RTrim( Ddo_contratogarantia_datapagtogarantia_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_PERCENTUAL_Activeeventkey", StringUtil.RTrim( Ddo_contratogarantia_percentual_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_PERCENTUAL_Filteredtext_get", StringUtil.RTrim( Ddo_contratogarantia_percentual_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_PERCENTUAL_Filteredtextto_get", StringUtil.RTrim( Ddo_contratogarantia_percentual_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATACAUCAO_Activeeventkey", StringUtil.RTrim( Ddo_contratogarantia_datacaucao_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATACAUCAO_Filteredtext_get", StringUtil.RTrim( Ddo_contratogarantia_datacaucao_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATACAUCAO_Filteredtextto_get", StringUtil.RTrim( Ddo_contratogarantia_datacaucao_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_VALORGARANTIA_Activeeventkey", StringUtil.RTrim( Ddo_contratogarantia_valorgarantia_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_VALORGARANTIA_Filteredtext_get", StringUtil.RTrim( Ddo_contratogarantia_valorgarantia_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_VALORGARANTIA_Filteredtextto_get", StringUtil.RTrim( Ddo_contratogarantia_valorgarantia_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATAENCERRAMENTO_Activeeventkey", StringUtil.RTrim( Ddo_contratogarantia_dataencerramento_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATAENCERRAMENTO_Filteredtext_get", StringUtil.RTrim( Ddo_contratogarantia_dataencerramento_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_DATAENCERRAMENTO_Filteredtextto_get", StringUtil.RTrim( Ddo_contratogarantia_dataencerramento_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_VALORENCERRAMENTO_Activeeventkey", StringUtil.RTrim( Ddo_contratogarantia_valorencerramento_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_VALORENCERRAMENTO_Filteredtext_get", StringUtil.RTrim( Ddo_contratogarantia_valorencerramento_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATOGARANTIA_VALORENCERRAMENTO_Filteredtextto_get", StringUtil.RTrim( Ddo_contratogarantia_valorencerramento_Filteredtextto_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WE6H2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVT6H2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wwcontratogarantia.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WWContratoGarantia" ;
      }

      public override String GetPgmdesc( )
      {
         return " Contrato Garantia" ;
      }

      protected void WB6H0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_6H2( true) ;
         }
         else
         {
            wb_table1_2_6H2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_6H2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 158,'',false,'" + sGXsfl_142_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV18DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(158, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,158);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 159,'',false,'" + sGXsfl_142_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV22DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(159, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,159);\"");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 160,'',false,'" + sGXsfl_142_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontrato_numero_Internalname, StringUtil.RTrim( AV52TFContrato_Numero), StringUtil.RTrim( context.localUtil.Format( AV52TFContrato_Numero, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,160);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontrato_numero_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontrato_numero_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContratoGarantia.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 161,'',false,'" + sGXsfl_142_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontrato_numero_sel_Internalname, StringUtil.RTrim( AV53TFContrato_Numero_Sel), StringUtil.RTrim( context.localUtil.Format( AV53TFContrato_Numero_Sel, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,161);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontrato_numero_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontrato_numero_sel_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContratoGarantia.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 162,'',false,'" + sGXsfl_142_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratada_pessoanom_Internalname, StringUtil.RTrim( AV56TFContratada_PessoaNom), StringUtil.RTrim( context.localUtil.Format( AV56TFContratada_PessoaNom, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,162);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratada_pessoanom_Jsonclick, 0, "BootstrapAttribute100", "", "", "", edtavTfcontratada_pessoanom_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContratoGarantia.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 163,'',false,'" + sGXsfl_142_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratada_pessoanom_sel_Internalname, StringUtil.RTrim( AV57TFContratada_PessoaNom_Sel), StringUtil.RTrim( context.localUtil.Format( AV57TFContratada_PessoaNom_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,163);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratada_pessoanom_sel_Jsonclick, 0, "BootstrapAttribute100", "", "", "", edtavTfcontratada_pessoanom_sel_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContratoGarantia.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 164,'',false,'" + sGXsfl_142_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratada_pessoacnpj_Internalname, AV60TFContratada_PessoaCNPJ, StringUtil.RTrim( context.localUtil.Format( AV60TFContratada_PessoaCNPJ, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,164);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratada_pessoacnpj_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratada_pessoacnpj_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContratoGarantia.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 165,'',false,'" + sGXsfl_142_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratada_pessoacnpj_sel_Internalname, AV61TFContratada_PessoaCNPJ_Sel, StringUtil.RTrim( context.localUtil.Format( AV61TFContratada_PessoaCNPJ_Sel, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,165);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratada_pessoacnpj_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratada_pessoacnpj_sel_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContratoGarantia.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 166,'',false,'" + sGXsfl_142_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfcontratogarantia_datapagtogarantia_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfcontratogarantia_datapagtogarantia_Internalname, context.localUtil.Format(AV64TFContratoGarantia_DataPagtoGarantia, "99/99/99"), context.localUtil.Format( AV64TFContratoGarantia_DataPagtoGarantia, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,166);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratogarantia_datapagtogarantia_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratogarantia_datapagtogarantia_Visible, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratoGarantia.htm");
            GxWebStd.gx_bitmap( context, edtavTfcontratogarantia_datapagtogarantia_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfcontratogarantia_datapagtogarantia_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContratoGarantia.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 167,'',false,'" + sGXsfl_142_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfcontratogarantia_datapagtogarantia_to_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfcontratogarantia_datapagtogarantia_to_Internalname, context.localUtil.Format(AV65TFContratoGarantia_DataPagtoGarantia_To, "99/99/99"), context.localUtil.Format( AV65TFContratoGarantia_DataPagtoGarantia_To, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,167);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratogarantia_datapagtogarantia_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratogarantia_datapagtogarantia_to_Visible, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratoGarantia.htm");
            GxWebStd.gx_bitmap( context, edtavTfcontratogarantia_datapagtogarantia_to_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfcontratogarantia_datapagtogarantia_to_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContratoGarantia.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divDdo_contratogarantia_datapagtogarantiaauxdates_Internalname, 1, 0, "px", 0, "px", "Invisible", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 169,'',false,'" + sGXsfl_142_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_contratogarantia_datapagtogarantiaauxdate_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_contratogarantia_datapagtogarantiaauxdate_Internalname, context.localUtil.Format(AV66DDO_ContratoGarantia_DataPagtoGarantiaAuxDate, "99/99/99"), context.localUtil.Format( AV66DDO_ContratoGarantia_DataPagtoGarantiaAuxDate, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,169);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_contratogarantia_datapagtogarantiaauxdate_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratoGarantia.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_contratogarantia_datapagtogarantiaauxdate_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContratoGarantia.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 170,'',false,'" + sGXsfl_142_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_contratogarantia_datapagtogarantiaauxdateto_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_contratogarantia_datapagtogarantiaauxdateto_Internalname, context.localUtil.Format(AV67DDO_ContratoGarantia_DataPagtoGarantiaAuxDateTo, "99/99/99"), context.localUtil.Format( AV67DDO_ContratoGarantia_DataPagtoGarantiaAuxDateTo, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,170);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_contratogarantia_datapagtogarantiaauxdateto_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratoGarantia.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_contratogarantia_datapagtogarantiaauxdateto_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContratoGarantia.htm");
            context.WriteHtmlTextNl( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 171,'',false,'" + sGXsfl_142_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratogarantia_percentual_Internalname, StringUtil.LTrim( StringUtil.NToC( AV70TFContratoGarantia_Percentual, 6, 2, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV70TFContratoGarantia_Percentual, "ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(this,171);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratogarantia_percentual_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratogarantia_percentual_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratoGarantia.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 172,'',false,'" + sGXsfl_142_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratogarantia_percentual_to_Internalname, StringUtil.LTrim( StringUtil.NToC( AV71TFContratoGarantia_Percentual_To, 6, 2, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV71TFContratoGarantia_Percentual_To, "ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(this,172);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratogarantia_percentual_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratogarantia_percentual_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratoGarantia.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 173,'',false,'" + sGXsfl_142_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfcontratogarantia_datacaucao_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfcontratogarantia_datacaucao_Internalname, context.localUtil.Format(AV74TFContratoGarantia_DataCaucao, "99/99/99"), context.localUtil.Format( AV74TFContratoGarantia_DataCaucao, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,173);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratogarantia_datacaucao_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratogarantia_datacaucao_Visible, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratoGarantia.htm");
            GxWebStd.gx_bitmap( context, edtavTfcontratogarantia_datacaucao_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfcontratogarantia_datacaucao_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContratoGarantia.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 174,'',false,'" + sGXsfl_142_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfcontratogarantia_datacaucao_to_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfcontratogarantia_datacaucao_to_Internalname, context.localUtil.Format(AV75TFContratoGarantia_DataCaucao_To, "99/99/99"), context.localUtil.Format( AV75TFContratoGarantia_DataCaucao_To, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,174);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratogarantia_datacaucao_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratogarantia_datacaucao_to_Visible, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratoGarantia.htm");
            GxWebStd.gx_bitmap( context, edtavTfcontratogarantia_datacaucao_to_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfcontratogarantia_datacaucao_to_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContratoGarantia.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divDdo_contratogarantia_datacaucaoauxdates_Internalname, 1, 0, "px", 0, "px", "Invisible", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 176,'',false,'" + sGXsfl_142_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_contratogarantia_datacaucaoauxdate_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_contratogarantia_datacaucaoauxdate_Internalname, context.localUtil.Format(AV76DDO_ContratoGarantia_DataCaucaoAuxDate, "99/99/99"), context.localUtil.Format( AV76DDO_ContratoGarantia_DataCaucaoAuxDate, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,176);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_contratogarantia_datacaucaoauxdate_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratoGarantia.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_contratogarantia_datacaucaoauxdate_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContratoGarantia.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 177,'',false,'" + sGXsfl_142_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_contratogarantia_datacaucaoauxdateto_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_contratogarantia_datacaucaoauxdateto_Internalname, context.localUtil.Format(AV77DDO_ContratoGarantia_DataCaucaoAuxDateTo, "99/99/99"), context.localUtil.Format( AV77DDO_ContratoGarantia_DataCaucaoAuxDateTo, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,177);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_contratogarantia_datacaucaoauxdateto_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratoGarantia.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_contratogarantia_datacaucaoauxdateto_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContratoGarantia.htm");
            context.WriteHtmlTextNl( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 178,'',false,'" + sGXsfl_142_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratogarantia_valorgarantia_Internalname, StringUtil.LTrim( StringUtil.NToC( AV80TFContratoGarantia_ValorGarantia, 18, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV80TFContratoGarantia_ValorGarantia, "ZZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,178);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratogarantia_valorgarantia_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratogarantia_valorgarantia_Visible, 1, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratoGarantia.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 179,'',false,'" + sGXsfl_142_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratogarantia_valorgarantia_to_Internalname, StringUtil.LTrim( StringUtil.NToC( AV81TFContratoGarantia_ValorGarantia_To, 18, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV81TFContratoGarantia_ValorGarantia_To, "ZZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,179);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratogarantia_valorgarantia_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratogarantia_valorgarantia_to_Visible, 1, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratoGarantia.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 180,'',false,'" + sGXsfl_142_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfcontratogarantia_dataencerramento_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfcontratogarantia_dataencerramento_Internalname, context.localUtil.Format(AV84TFContratoGarantia_DataEncerramento, "99/99/99"), context.localUtil.Format( AV84TFContratoGarantia_DataEncerramento, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,180);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratogarantia_dataencerramento_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratogarantia_dataencerramento_Visible, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratoGarantia.htm");
            GxWebStd.gx_bitmap( context, edtavTfcontratogarantia_dataencerramento_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfcontratogarantia_dataencerramento_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContratoGarantia.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 181,'',false,'" + sGXsfl_142_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfcontratogarantia_dataencerramento_to_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfcontratogarantia_dataencerramento_to_Internalname, context.localUtil.Format(AV85TFContratoGarantia_DataEncerramento_To, "99/99/99"), context.localUtil.Format( AV85TFContratoGarantia_DataEncerramento_To, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,181);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratogarantia_dataencerramento_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratogarantia_dataencerramento_to_Visible, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratoGarantia.htm");
            GxWebStd.gx_bitmap( context, edtavTfcontratogarantia_dataencerramento_to_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfcontratogarantia_dataencerramento_to_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContratoGarantia.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divDdo_contratogarantia_dataencerramentoauxdates_Internalname, 1, 0, "px", 0, "px", "Invisible", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 183,'',false,'" + sGXsfl_142_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_contratogarantia_dataencerramentoauxdate_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_contratogarantia_dataencerramentoauxdate_Internalname, context.localUtil.Format(AV86DDO_ContratoGarantia_DataEncerramentoAuxDate, "99/99/99"), context.localUtil.Format( AV86DDO_ContratoGarantia_DataEncerramentoAuxDate, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,183);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_contratogarantia_dataencerramentoauxdate_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratoGarantia.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_contratogarantia_dataencerramentoauxdate_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContratoGarantia.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 184,'',false,'" + sGXsfl_142_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_contratogarantia_dataencerramentoauxdateto_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_contratogarantia_dataencerramentoauxdateto_Internalname, context.localUtil.Format(AV87DDO_ContratoGarantia_DataEncerramentoAuxDateTo, "99/99/99"), context.localUtil.Format( AV87DDO_ContratoGarantia_DataEncerramentoAuxDateTo, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,184);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_contratogarantia_dataencerramentoauxdateto_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratoGarantia.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_contratogarantia_dataencerramentoauxdateto_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContratoGarantia.htm");
            context.WriteHtmlTextNl( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 185,'',false,'" + sGXsfl_142_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratogarantia_valorencerramento_Internalname, StringUtil.LTrim( StringUtil.NToC( AV90TFContratoGarantia_ValorEncerramento, 18, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV90TFContratoGarantia_ValorEncerramento, "ZZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,185);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratogarantia_valorencerramento_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratogarantia_valorencerramento_Visible, 1, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratoGarantia.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 186,'',false,'" + sGXsfl_142_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratogarantia_valorencerramento_to_Internalname, StringUtil.LTrim( StringUtil.NToC( AV91TFContratoGarantia_ValorEncerramento_To, 18, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV91TFContratoGarantia_ValorEncerramento_To, "ZZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,186);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratogarantia_valorencerramento_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratogarantia_valorencerramento_to_Visible, 1, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratoGarantia.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATO_NUMEROContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 188,'',false,'" + sGXsfl_142_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contrato_numerotitlecontrolidtoreplace_Internalname, AV54ddo_Contrato_NumeroTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,188);\"", 0, edtavDdo_contrato_numerotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWContratoGarantia.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATADA_PESSOANOMContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 190,'',false,'" + sGXsfl_142_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratada_pessoanomtitlecontrolidtoreplace_Internalname, AV58ddo_Contratada_PessoaNomTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,190);\"", 0, edtavDdo_contratada_pessoanomtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWContratoGarantia.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATADA_PESSOACNPJContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 192,'',false,'" + sGXsfl_142_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratada_pessoacnpjtitlecontrolidtoreplace_Internalname, AV62ddo_Contratada_PessoaCNPJTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,192);\"", 0, edtavDdo_contratada_pessoacnpjtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWContratoGarantia.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIAContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 194,'',false,'" + sGXsfl_142_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratogarantia_datapagtogarantiatitlecontrolidtoreplace_Internalname, AV68ddo_ContratoGarantia_DataPagtoGarantiaTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,194);\"", 0, edtavDdo_contratogarantia_datapagtogarantiatitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWContratoGarantia.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATOGARANTIA_PERCENTUALContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 196,'',false,'" + sGXsfl_142_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratogarantia_percentualtitlecontrolidtoreplace_Internalname, AV72ddo_ContratoGarantia_PercentualTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,196);\"", 0, edtavDdo_contratogarantia_percentualtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWContratoGarantia.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATOGARANTIA_DATACAUCAOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 198,'',false,'" + sGXsfl_142_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratogarantia_datacaucaotitlecontrolidtoreplace_Internalname, AV78ddo_ContratoGarantia_DataCaucaoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,198);\"", 0, edtavDdo_contratogarantia_datacaucaotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWContratoGarantia.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATOGARANTIA_VALORGARANTIAContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 200,'',false,'" + sGXsfl_142_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratogarantia_valorgarantiatitlecontrolidtoreplace_Internalname, AV82ddo_ContratoGarantia_ValorGarantiaTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,200);\"", 0, edtavDdo_contratogarantia_valorgarantiatitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWContratoGarantia.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATOGARANTIA_DATAENCERRAMENTOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 202,'',false,'" + sGXsfl_142_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratogarantia_dataencerramentotitlecontrolidtoreplace_Internalname, AV88ddo_ContratoGarantia_DataEncerramentoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,202);\"", 0, edtavDdo_contratogarantia_dataencerramentotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWContratoGarantia.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATOGARANTIA_VALORENCERRAMENTOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 204,'',false,'" + sGXsfl_142_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratogarantia_valorencerramentotitlecontrolidtoreplace_Internalname, AV92ddo_ContratoGarantia_ValorEncerramentoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,204);\"", 0, edtavDdo_contratogarantia_valorencerramentotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWContratoGarantia.htm");
         }
         wbLoad = true;
      }

      protected void START6H2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", " Contrato Garantia", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUP6H0( ) ;
      }

      protected void WS6H2( )
      {
         START6H2( ) ;
         EVT6H2( ) ;
      }

      protected void EVT6H2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E116H2 */
                              E116H2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATO_NUMERO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E126H2 */
                              E126H2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATADA_PESSOANOM.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E136H2 */
                              E136H2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATADA_PESSOACNPJ.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E146H2 */
                              E146H2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E156H2 */
                              E156H2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOGARANTIA_PERCENTUAL.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E166H2 */
                              E166H2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOGARANTIA_DATACAUCAO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E176H2 */
                              E176H2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOGARANTIA_VALORGARANTIA.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E186H2 */
                              E186H2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOGARANTIA_DATAENCERRAMENTO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E196H2 */
                              E196H2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOGARANTIA_VALORENCERRAMENTO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E206H2 */
                              E206H2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E216H2 */
                              E216H2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E226H2 */
                              E226H2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E236H2 */
                              E236H2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E246H2 */
                              E246H2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E256H2 */
                              E256H2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOINSERT'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E266H2 */
                              E266H2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E276H2 */
                              E276H2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E286H2 */
                              E286H2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E296H2 */
                              E296H2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E306H2 */
                              E306H2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR3.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E316H2 */
                              E316H2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              nGXsfl_142_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_142_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_142_idx), 4, 0)), 4, "0");
                              SubsflControlProps_1422( ) ;
                              AV28Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV28Update)) ? AV155Update_GXI : context.convertURL( context.PathToRelativeUrl( AV28Update))));
                              AV29Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete)) ? AV156Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV29Delete))));
                              A77Contrato_Numero = cgiGet( edtContrato_Numero_Internalname);
                              A41Contratada_PessoaNom = StringUtil.Upper( cgiGet( edtContratada_PessoaNom_Internalname));
                              n41Contratada_PessoaNom = false;
                              A42Contratada_PessoaCNPJ = cgiGet( edtContratada_PessoaCNPJ_Internalname);
                              n42Contratada_PessoaCNPJ = false;
                              A102ContratoGarantia_DataPagtoGarantia = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtContratoGarantia_DataPagtoGarantia_Internalname), 0));
                              A103ContratoGarantia_Percentual = context.localUtil.CToN( cgiGet( edtContratoGarantia_Percentual_Internalname), ",", ".");
                              A104ContratoGarantia_DataCaucao = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtContratoGarantia_DataCaucao_Internalname), 0));
                              A105ContratoGarantia_ValorGarantia = context.localUtil.CToN( cgiGet( edtContratoGarantia_ValorGarantia_Internalname), ",", ".");
                              A106ContratoGarantia_DataEncerramento = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtContratoGarantia_DataEncerramento_Internalname), 0));
                              A107ContratoGarantia_ValorEncerramento = context.localUtil.CToN( cgiGet( edtContratoGarantia_ValorEncerramento_Internalname), ",", ".");
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E326H2 */
                                    E326H2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E336H2 */
                                    E336H2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E346H2 */
                                    E346H2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       /* Set Refresh If Orderedby Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Ordereddsc Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contratogarantia_datapagtogarantia1 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOGARANTIA_DATAPAGTOGARANTIA1"), 0) != AV16ContratoGarantia_DataPagtoGarantia1 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contratogarantia_datapagtogarantia_to1 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO1"), 0) != AV17ContratoGarantia_DataPagtoGarantia_To1 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contratogarantia_datacaucao1 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOGARANTIA_DATACAUCAO1"), 0) != AV37ContratoGarantia_DataCaucao1 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contratogarantia_datacaucao_to1 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOGARANTIA_DATACAUCAO_TO1"), 0) != AV38ContratoGarantia_DataCaucao_To1 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contratogarantia_dataencerramento1 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOGARANTIA_DATAENCERRAMENTO1"), 0) != AV39ContratoGarantia_DataEncerramento1 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contratogarantia_dataencerramento_to1 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOGARANTIA_DATAENCERRAMENTO_TO1"), 0) != AV40ContratoGarantia_DataEncerramento_To1 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV19DynamicFiltersSelector2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contratogarantia_datapagtogarantia2 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOGARANTIA_DATAPAGTOGARANTIA2"), 0) != AV20ContratoGarantia_DataPagtoGarantia2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contratogarantia_datapagtogarantia_to2 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO2"), 0) != AV21ContratoGarantia_DataPagtoGarantia_To2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contratogarantia_datacaucao2 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOGARANTIA_DATACAUCAO2"), 0) != AV41ContratoGarantia_DataCaucao2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contratogarantia_datacaucao_to2 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOGARANTIA_DATACAUCAO_TO2"), 0) != AV42ContratoGarantia_DataCaucao_To2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contratogarantia_dataencerramento2 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOGARANTIA_DATAENCERRAMENTO2"), 0) != AV43ContratoGarantia_DataEncerramento2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contratogarantia_dataencerramento_to2 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOGARANTIA_DATAENCERRAMENTO_TO2"), 0) != AV44ContratoGarantia_DataEncerramento_To2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV23DynamicFiltersSelector3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contratogarantia_datapagtogarantia3 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOGARANTIA_DATAPAGTOGARANTIA3"), 0) != AV24ContratoGarantia_DataPagtoGarantia3 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contratogarantia_datapagtogarantia_to3 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO3"), 0) != AV25ContratoGarantia_DataPagtoGarantia_To3 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contratogarantia_datacaucao3 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOGARANTIA_DATACAUCAO3"), 0) != AV45ContratoGarantia_DataCaucao3 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contratogarantia_datacaucao_to3 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOGARANTIA_DATACAUCAO_TO3"), 0) != AV46ContratoGarantia_DataCaucao_To3 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contratogarantia_dataencerramento3 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOGARANTIA_DATAENCERRAMENTO3"), 0) != AV47ContratoGarantia_DataEncerramento3 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contratogarantia_dataencerramento_to3 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOGARANTIA_DATAENCERRAMENTO_TO3"), 0) != AV48ContratoGarantia_DataEncerramento_To3 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV18DynamicFiltersEnabled2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV22DynamicFiltersEnabled3 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontrato_numero Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATO_NUMERO"), AV52TFContrato_Numero) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontrato_numero_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATO_NUMERO_SEL"), AV53TFContrato_Numero_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratada_pessoanom Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATADA_PESSOANOM"), AV56TFContratada_PessoaNom) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratada_pessoanom_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATADA_PESSOANOM_SEL"), AV57TFContratada_PessoaNom_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratada_pessoacnpj Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATADA_PESSOACNPJ"), AV60TFContratada_PessoaCNPJ) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratada_pessoacnpj_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATADA_PESSOACNPJ_SEL"), AV61TFContratada_PessoaCNPJ_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratogarantia_datapagtogarantia Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTRATOGARANTIA_DATAPAGTOGARANTIA"), 0) != AV64TFContratoGarantia_DataPagtoGarantia )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratogarantia_datapagtogarantia_to Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO"), 0) != AV65TFContratoGarantia_DataPagtoGarantia_To )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratogarantia_percentual Changed */
                                       if ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOGARANTIA_PERCENTUAL"), ",", ".") != AV70TFContratoGarantia_Percentual )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratogarantia_percentual_to Changed */
                                       if ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOGARANTIA_PERCENTUAL_TO"), ",", ".") != AV71TFContratoGarantia_Percentual_To )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratogarantia_datacaucao Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTRATOGARANTIA_DATACAUCAO"), 0) != AV74TFContratoGarantia_DataCaucao )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratogarantia_datacaucao_to Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTRATOGARANTIA_DATACAUCAO_TO"), 0) != AV75TFContratoGarantia_DataCaucao_To )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratogarantia_valorgarantia Changed */
                                       if ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOGARANTIA_VALORGARANTIA"), ",", ".") != AV80TFContratoGarantia_ValorGarantia )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratogarantia_valorgarantia_to Changed */
                                       if ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOGARANTIA_VALORGARANTIA_TO"), ",", ".") != AV81TFContratoGarantia_ValorGarantia_To )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratogarantia_dataencerramento Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTRATOGARANTIA_DATAENCERRAMENTO"), 0) != AV84TFContratoGarantia_DataEncerramento )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratogarantia_dataencerramento_to Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTRATOGARANTIA_DATAENCERRAMENTO_TO"), 0) != AV85TFContratoGarantia_DataEncerramento_To )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratogarantia_valorencerramento Changed */
                                       if ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOGARANTIA_VALORENCERRAMENTO"), ",", ".") != AV90TFContratoGarantia_ValorEncerramento )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratogarantia_valorencerramento_to Changed */
                                       if ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOGARANTIA_VALORENCERRAMENTO_TO"), ",", ".") != AV91TFContratoGarantia_ValorEncerramento_To )
                                       {
                                          Rfr0gs = true;
                                       }
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE6H2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PA6H2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("CONTRATOGARANTIA_DATAPAGTOGARANTIA", "Data Pagamento", 0);
            cmbavDynamicfiltersselector1.addItem("CONTRATOGARANTIA_DATACAUCAO", "Data Cau��o", 0);
            cmbavDynamicfiltersselector1.addItem("CONTRATOGARANTIA_DATAENCERRAMENTO", "Data Encerramento", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("CONTRATOGARANTIA_DATAPAGTOGARANTIA", "Data Pagamento", 0);
            cmbavDynamicfiltersselector2.addItem("CONTRATOGARANTIA_DATACAUCAO", "Data Cau��o", 0);
            cmbavDynamicfiltersselector2.addItem("CONTRATOGARANTIA_DATAENCERRAMENTO", "Data Encerramento", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV19DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV19DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("CONTRATOGARANTIA_DATAPAGTOGARANTIA", "Data Pagamento", 0);
            cmbavDynamicfiltersselector3.addItem("CONTRATOGARANTIA_DATACAUCAO", "Data Cau��o", 0);
            cmbavDynamicfiltersselector3.addItem("CONTRATOGARANTIA_DATAENCERRAMENTO", "Data Encerramento", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV23DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV23DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
            }
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_1422( ) ;
         while ( nGXsfl_142_idx <= nRC_GXsfl_142 )
         {
            sendrow_1422( ) ;
            nGXsfl_142_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_142_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_142_idx+1));
            sGXsfl_142_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_142_idx), 4, 0)), 4, "0");
            SubsflControlProps_1422( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       String AV15DynamicFiltersSelector1 ,
                                       DateTime AV16ContratoGarantia_DataPagtoGarantia1 ,
                                       DateTime AV17ContratoGarantia_DataPagtoGarantia_To1 ,
                                       DateTime AV37ContratoGarantia_DataCaucao1 ,
                                       DateTime AV38ContratoGarantia_DataCaucao_To1 ,
                                       DateTime AV39ContratoGarantia_DataEncerramento1 ,
                                       DateTime AV40ContratoGarantia_DataEncerramento_To1 ,
                                       String AV19DynamicFiltersSelector2 ,
                                       DateTime AV20ContratoGarantia_DataPagtoGarantia2 ,
                                       DateTime AV21ContratoGarantia_DataPagtoGarantia_To2 ,
                                       DateTime AV41ContratoGarantia_DataCaucao2 ,
                                       DateTime AV42ContratoGarantia_DataCaucao_To2 ,
                                       DateTime AV43ContratoGarantia_DataEncerramento2 ,
                                       DateTime AV44ContratoGarantia_DataEncerramento_To2 ,
                                       String AV23DynamicFiltersSelector3 ,
                                       DateTime AV24ContratoGarantia_DataPagtoGarantia3 ,
                                       DateTime AV25ContratoGarantia_DataPagtoGarantia_To3 ,
                                       DateTime AV45ContratoGarantia_DataCaucao3 ,
                                       DateTime AV46ContratoGarantia_DataCaucao_To3 ,
                                       DateTime AV47ContratoGarantia_DataEncerramento3 ,
                                       DateTime AV48ContratoGarantia_DataEncerramento_To3 ,
                                       bool AV18DynamicFiltersEnabled2 ,
                                       bool AV22DynamicFiltersEnabled3 ,
                                       String AV52TFContrato_Numero ,
                                       String AV53TFContrato_Numero_Sel ,
                                       String AV56TFContratada_PessoaNom ,
                                       String AV57TFContratada_PessoaNom_Sel ,
                                       String AV60TFContratada_PessoaCNPJ ,
                                       String AV61TFContratada_PessoaCNPJ_Sel ,
                                       DateTime AV64TFContratoGarantia_DataPagtoGarantia ,
                                       DateTime AV65TFContratoGarantia_DataPagtoGarantia_To ,
                                       decimal AV70TFContratoGarantia_Percentual ,
                                       decimal AV71TFContratoGarantia_Percentual_To ,
                                       DateTime AV74TFContratoGarantia_DataCaucao ,
                                       DateTime AV75TFContratoGarantia_DataCaucao_To ,
                                       decimal AV80TFContratoGarantia_ValorGarantia ,
                                       decimal AV81TFContratoGarantia_ValorGarantia_To ,
                                       DateTime AV84TFContratoGarantia_DataEncerramento ,
                                       DateTime AV85TFContratoGarantia_DataEncerramento_To ,
                                       decimal AV90TFContratoGarantia_ValorEncerramento ,
                                       decimal AV91TFContratoGarantia_ValorEncerramento_To ,
                                       String AV54ddo_Contrato_NumeroTitleControlIdToReplace ,
                                       String AV58ddo_Contratada_PessoaNomTitleControlIdToReplace ,
                                       String AV62ddo_Contratada_PessoaCNPJTitleControlIdToReplace ,
                                       String AV68ddo_ContratoGarantia_DataPagtoGarantiaTitleControlIdToReplace ,
                                       String AV72ddo_ContratoGarantia_PercentualTitleControlIdToReplace ,
                                       String AV78ddo_ContratoGarantia_DataCaucaoTitleControlIdToReplace ,
                                       String AV82ddo_ContratoGarantia_ValorGarantiaTitleControlIdToReplace ,
                                       String AV88ddo_ContratoGarantia_DataEncerramentoTitleControlIdToReplace ,
                                       String AV92ddo_ContratoGarantia_ValorEncerramentoTitleControlIdToReplace ,
                                       String AV157Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV10GridState ,
                                       bool AV27DynamicFiltersIgnoreFirst ,
                                       bool AV26DynamicFiltersRemoving ,
                                       int A101ContratoGarantia_Codigo ,
                                       int A74Contrato_Codigo ,
                                       int A39Contratada_Codigo )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RF6H2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOGARANTIA_DATAPAGTOGARANTIA", GetSecureSignedToken( "", A102ContratoGarantia_DataPagtoGarantia));
         GxWebStd.gx_hidden_field( context, "CONTRATOGARANTIA_DATAPAGTOGARANTIA", context.localUtil.Format(A102ContratoGarantia_DataPagtoGarantia, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOGARANTIA_PERCENTUAL", GetSecureSignedToken( "", context.localUtil.Format( A103ContratoGarantia_Percentual, "ZZ9.99")));
         GxWebStd.gx_hidden_field( context, "CONTRATOGARANTIA_PERCENTUAL", StringUtil.LTrim( StringUtil.NToC( A103ContratoGarantia_Percentual, 6, 2, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOGARANTIA_DATACAUCAO", GetSecureSignedToken( "", A104ContratoGarantia_DataCaucao));
         GxWebStd.gx_hidden_field( context, "CONTRATOGARANTIA_DATACAUCAO", context.localUtil.Format(A104ContratoGarantia_DataCaucao, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOGARANTIA_VALORGARANTIA", GetSecureSignedToken( "", context.localUtil.Format( A105ContratoGarantia_ValorGarantia, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
         GxWebStd.gx_hidden_field( context, "CONTRATOGARANTIA_VALORGARANTIA", StringUtil.LTrim( StringUtil.NToC( A105ContratoGarantia_ValorGarantia, 18, 5, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOGARANTIA_DATAENCERRAMENTO", GetSecureSignedToken( "", A106ContratoGarantia_DataEncerramento));
         GxWebStd.gx_hidden_field( context, "CONTRATOGARANTIA_DATAENCERRAMENTO", context.localUtil.Format(A106ContratoGarantia_DataEncerramento, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOGARANTIA_VALORENCERRAMENTO", GetSecureSignedToken( "", context.localUtil.Format( A107ContratoGarantia_ValorEncerramento, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
         GxWebStd.gx_hidden_field( context, "CONTRATOGARANTIA_VALORENCERRAMENTO", StringUtil.LTrim( StringUtil.NToC( A107ContratoGarantia_ValorEncerramento, 18, 5, ".", "")));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV19DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV19DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV23DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV23DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF6H2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV157Pgmname = "WWContratoGarantia";
         context.Gx_err = 0;
      }

      protected void RF6H2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 142;
         /* Execute user event: E336H2 */
         E336H2 ();
         nGXsfl_142_idx = 1;
         sGXsfl_142_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_142_idx), 4, 0)), 4, "0");
         SubsflControlProps_1422( ) ;
         nGXsfl_142_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_1422( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV114WWContratoGarantiaDS_1_Dynamicfiltersselector1 ,
                                                 AV115WWContratoGarantiaDS_2_Contratogarantia_datapagtogarantia1 ,
                                                 AV116WWContratoGarantiaDS_3_Contratogarantia_datapagtogarantia_to1 ,
                                                 AV117WWContratoGarantiaDS_4_Contratogarantia_datacaucao1 ,
                                                 AV118WWContratoGarantiaDS_5_Contratogarantia_datacaucao_to1 ,
                                                 AV119WWContratoGarantiaDS_6_Contratogarantia_dataencerramento1 ,
                                                 AV120WWContratoGarantiaDS_7_Contratogarantia_dataencerramento_to1 ,
                                                 AV121WWContratoGarantiaDS_8_Dynamicfiltersenabled2 ,
                                                 AV122WWContratoGarantiaDS_9_Dynamicfiltersselector2 ,
                                                 AV123WWContratoGarantiaDS_10_Contratogarantia_datapagtogarantia2 ,
                                                 AV124WWContratoGarantiaDS_11_Contratogarantia_datapagtogarantia_to2 ,
                                                 AV125WWContratoGarantiaDS_12_Contratogarantia_datacaucao2 ,
                                                 AV126WWContratoGarantiaDS_13_Contratogarantia_datacaucao_to2 ,
                                                 AV127WWContratoGarantiaDS_14_Contratogarantia_dataencerramento2 ,
                                                 AV128WWContratoGarantiaDS_15_Contratogarantia_dataencerramento_to2 ,
                                                 AV129WWContratoGarantiaDS_16_Dynamicfiltersenabled3 ,
                                                 AV130WWContratoGarantiaDS_17_Dynamicfiltersselector3 ,
                                                 AV131WWContratoGarantiaDS_18_Contratogarantia_datapagtogarantia3 ,
                                                 AV132WWContratoGarantiaDS_19_Contratogarantia_datapagtogarantia_to3 ,
                                                 AV133WWContratoGarantiaDS_20_Contratogarantia_datacaucao3 ,
                                                 AV134WWContratoGarantiaDS_21_Contratogarantia_datacaucao_to3 ,
                                                 AV135WWContratoGarantiaDS_22_Contratogarantia_dataencerramento3 ,
                                                 AV136WWContratoGarantiaDS_23_Contratogarantia_dataencerramento_to3 ,
                                                 AV138WWContratoGarantiaDS_25_Tfcontrato_numero_sel ,
                                                 AV137WWContratoGarantiaDS_24_Tfcontrato_numero ,
                                                 AV140WWContratoGarantiaDS_27_Tfcontratada_pessoanom_sel ,
                                                 AV139WWContratoGarantiaDS_26_Tfcontratada_pessoanom ,
                                                 AV142WWContratoGarantiaDS_29_Tfcontratada_pessoacnpj_sel ,
                                                 AV141WWContratoGarantiaDS_28_Tfcontratada_pessoacnpj ,
                                                 AV143WWContratoGarantiaDS_30_Tfcontratogarantia_datapagtogarantia ,
                                                 AV144WWContratoGarantiaDS_31_Tfcontratogarantia_datapagtogarantia_to ,
                                                 AV145WWContratoGarantiaDS_32_Tfcontratogarantia_percentual ,
                                                 AV146WWContratoGarantiaDS_33_Tfcontratogarantia_percentual_to ,
                                                 AV147WWContratoGarantiaDS_34_Tfcontratogarantia_datacaucao ,
                                                 AV148WWContratoGarantiaDS_35_Tfcontratogarantia_datacaucao_to ,
                                                 AV149WWContratoGarantiaDS_36_Tfcontratogarantia_valorgarantia ,
                                                 AV150WWContratoGarantiaDS_37_Tfcontratogarantia_valorgarantia_to ,
                                                 AV151WWContratoGarantiaDS_38_Tfcontratogarantia_dataencerramento ,
                                                 AV152WWContratoGarantiaDS_39_Tfcontratogarantia_dataencerramento_to ,
                                                 AV153WWContratoGarantiaDS_40_Tfcontratogarantia_valorencerramento ,
                                                 AV154WWContratoGarantiaDS_41_Tfcontratogarantia_valorencerramento_to ,
                                                 A102ContratoGarantia_DataPagtoGarantia ,
                                                 A104ContratoGarantia_DataCaucao ,
                                                 A106ContratoGarantia_DataEncerramento ,
                                                 A77Contrato_Numero ,
                                                 A41Contratada_PessoaNom ,
                                                 A42Contratada_PessoaCNPJ ,
                                                 A103ContratoGarantia_Percentual ,
                                                 A105ContratoGarantia_ValorGarantia ,
                                                 A107ContratoGarantia_ValorEncerramento ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE,
                                                 TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE,
                                                 TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE,
                                                 TypeConstants.DATE, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DECIMAL,
                                                 TypeConstants.DECIMAL, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DECIMAL,
                                                 TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                                 }
            });
            lV137WWContratoGarantiaDS_24_Tfcontrato_numero = StringUtil.PadR( StringUtil.RTrim( AV137WWContratoGarantiaDS_24_Tfcontrato_numero), 20, "%");
            lV139WWContratoGarantiaDS_26_Tfcontratada_pessoanom = StringUtil.PadR( StringUtil.RTrim( AV139WWContratoGarantiaDS_26_Tfcontratada_pessoanom), 100, "%");
            lV141WWContratoGarantiaDS_28_Tfcontratada_pessoacnpj = StringUtil.Concat( StringUtil.RTrim( AV141WWContratoGarantiaDS_28_Tfcontratada_pessoacnpj), "%", "");
            /* Using cursor H006H2 */
            pr_default.execute(0, new Object[] {AV115WWContratoGarantiaDS_2_Contratogarantia_datapagtogarantia1, AV116WWContratoGarantiaDS_3_Contratogarantia_datapagtogarantia_to1, AV117WWContratoGarantiaDS_4_Contratogarantia_datacaucao1, AV118WWContratoGarantiaDS_5_Contratogarantia_datacaucao_to1, AV119WWContratoGarantiaDS_6_Contratogarantia_dataencerramento1, AV120WWContratoGarantiaDS_7_Contratogarantia_dataencerramento_to1, AV123WWContratoGarantiaDS_10_Contratogarantia_datapagtogarantia2, AV124WWContratoGarantiaDS_11_Contratogarantia_datapagtogarantia_to2, AV125WWContratoGarantiaDS_12_Contratogarantia_datacaucao2, AV126WWContratoGarantiaDS_13_Contratogarantia_datacaucao_to2, AV127WWContratoGarantiaDS_14_Contratogarantia_dataencerramento2, AV128WWContratoGarantiaDS_15_Contratogarantia_dataencerramento_to2, AV131WWContratoGarantiaDS_18_Contratogarantia_datapagtogarantia3, AV132WWContratoGarantiaDS_19_Contratogarantia_datapagtogarantia_to3, AV133WWContratoGarantiaDS_20_Contratogarantia_datacaucao3, AV134WWContratoGarantiaDS_21_Contratogarantia_datacaucao_to3, AV135WWContratoGarantiaDS_22_Contratogarantia_dataencerramento3, AV136WWContratoGarantiaDS_23_Contratogarantia_dataencerramento_to3, lV137WWContratoGarantiaDS_24_Tfcontrato_numero, AV138WWContratoGarantiaDS_25_Tfcontrato_numero_sel, lV139WWContratoGarantiaDS_26_Tfcontratada_pessoanom, AV140WWContratoGarantiaDS_27_Tfcontratada_pessoanom_sel, lV141WWContratoGarantiaDS_28_Tfcontratada_pessoacnpj, AV142WWContratoGarantiaDS_29_Tfcontratada_pessoacnpj_sel, AV143WWContratoGarantiaDS_30_Tfcontratogarantia_datapagtogarantia, AV144WWContratoGarantiaDS_31_Tfcontratogarantia_datapagtogarantia_to, AV145WWContratoGarantiaDS_32_Tfcontratogarantia_percentual, AV146WWContratoGarantiaDS_33_Tfcontratogarantia_percentual_to, AV147WWContratoGarantiaDS_34_Tfcontratogarantia_datacaucao, AV148WWContratoGarantiaDS_35_Tfcontratogarantia_datacaucao_to, AV149WWContratoGarantiaDS_36_Tfcontratogarantia_valorgarantia, AV150WWContratoGarantiaDS_37_Tfcontratogarantia_valorgarantia_to, AV151WWContratoGarantiaDS_38_Tfcontratogarantia_dataencerramento, AV152WWContratoGarantiaDS_39_Tfcontratogarantia_dataencerramento_to, AV153WWContratoGarantiaDS_40_Tfcontratogarantia_valorencerramento, AV154WWContratoGarantiaDS_41_Tfcontratogarantia_valorencerramento_to, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_142_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A40Contratada_PessoaCod = H006H2_A40Contratada_PessoaCod[0];
               A74Contrato_Codigo = H006H2_A74Contrato_Codigo[0];
               A101ContratoGarantia_Codigo = H006H2_A101ContratoGarantia_Codigo[0];
               A39Contratada_Codigo = H006H2_A39Contratada_Codigo[0];
               A107ContratoGarantia_ValorEncerramento = H006H2_A107ContratoGarantia_ValorEncerramento[0];
               A106ContratoGarantia_DataEncerramento = H006H2_A106ContratoGarantia_DataEncerramento[0];
               A105ContratoGarantia_ValorGarantia = H006H2_A105ContratoGarantia_ValorGarantia[0];
               A104ContratoGarantia_DataCaucao = H006H2_A104ContratoGarantia_DataCaucao[0];
               A103ContratoGarantia_Percentual = H006H2_A103ContratoGarantia_Percentual[0];
               A102ContratoGarantia_DataPagtoGarantia = H006H2_A102ContratoGarantia_DataPagtoGarantia[0];
               A42Contratada_PessoaCNPJ = H006H2_A42Contratada_PessoaCNPJ[0];
               n42Contratada_PessoaCNPJ = H006H2_n42Contratada_PessoaCNPJ[0];
               A41Contratada_PessoaNom = H006H2_A41Contratada_PessoaNom[0];
               n41Contratada_PessoaNom = H006H2_n41Contratada_PessoaNom[0];
               A77Contrato_Numero = H006H2_A77Contrato_Numero[0];
               A39Contratada_Codigo = H006H2_A39Contratada_Codigo[0];
               A77Contrato_Numero = H006H2_A77Contrato_Numero[0];
               A40Contratada_PessoaCod = H006H2_A40Contratada_PessoaCod[0];
               A42Contratada_PessoaCNPJ = H006H2_A42Contratada_PessoaCNPJ[0];
               n42Contratada_PessoaCNPJ = H006H2_n42Contratada_PessoaCNPJ[0];
               A41Contratada_PessoaNom = H006H2_A41Contratada_PessoaNom[0];
               n41Contratada_PessoaNom = H006H2_n41Contratada_PessoaNom[0];
               /* Execute user event: E346H2 */
               E346H2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 142;
            WB6H0( ) ;
         }
         nGXsfl_142_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         AV114WWContratoGarantiaDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV115WWContratoGarantiaDS_2_Contratogarantia_datapagtogarantia1 = AV16ContratoGarantia_DataPagtoGarantia1;
         AV116WWContratoGarantiaDS_3_Contratogarantia_datapagtogarantia_to1 = AV17ContratoGarantia_DataPagtoGarantia_To1;
         AV117WWContratoGarantiaDS_4_Contratogarantia_datacaucao1 = AV37ContratoGarantia_DataCaucao1;
         AV118WWContratoGarantiaDS_5_Contratogarantia_datacaucao_to1 = AV38ContratoGarantia_DataCaucao_To1;
         AV119WWContratoGarantiaDS_6_Contratogarantia_dataencerramento1 = AV39ContratoGarantia_DataEncerramento1;
         AV120WWContratoGarantiaDS_7_Contratogarantia_dataencerramento_to1 = AV40ContratoGarantia_DataEncerramento_To1;
         AV121WWContratoGarantiaDS_8_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV122WWContratoGarantiaDS_9_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV123WWContratoGarantiaDS_10_Contratogarantia_datapagtogarantia2 = AV20ContratoGarantia_DataPagtoGarantia2;
         AV124WWContratoGarantiaDS_11_Contratogarantia_datapagtogarantia_to2 = AV21ContratoGarantia_DataPagtoGarantia_To2;
         AV125WWContratoGarantiaDS_12_Contratogarantia_datacaucao2 = AV41ContratoGarantia_DataCaucao2;
         AV126WWContratoGarantiaDS_13_Contratogarantia_datacaucao_to2 = AV42ContratoGarantia_DataCaucao_To2;
         AV127WWContratoGarantiaDS_14_Contratogarantia_dataencerramento2 = AV43ContratoGarantia_DataEncerramento2;
         AV128WWContratoGarantiaDS_15_Contratogarantia_dataencerramento_to2 = AV44ContratoGarantia_DataEncerramento_To2;
         AV129WWContratoGarantiaDS_16_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV130WWContratoGarantiaDS_17_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV131WWContratoGarantiaDS_18_Contratogarantia_datapagtogarantia3 = AV24ContratoGarantia_DataPagtoGarantia3;
         AV132WWContratoGarantiaDS_19_Contratogarantia_datapagtogarantia_to3 = AV25ContratoGarantia_DataPagtoGarantia_To3;
         AV133WWContratoGarantiaDS_20_Contratogarantia_datacaucao3 = AV45ContratoGarantia_DataCaucao3;
         AV134WWContratoGarantiaDS_21_Contratogarantia_datacaucao_to3 = AV46ContratoGarantia_DataCaucao_To3;
         AV135WWContratoGarantiaDS_22_Contratogarantia_dataencerramento3 = AV47ContratoGarantia_DataEncerramento3;
         AV136WWContratoGarantiaDS_23_Contratogarantia_dataencerramento_to3 = AV48ContratoGarantia_DataEncerramento_To3;
         AV137WWContratoGarantiaDS_24_Tfcontrato_numero = AV52TFContrato_Numero;
         AV138WWContratoGarantiaDS_25_Tfcontrato_numero_sel = AV53TFContrato_Numero_Sel;
         AV139WWContratoGarantiaDS_26_Tfcontratada_pessoanom = AV56TFContratada_PessoaNom;
         AV140WWContratoGarantiaDS_27_Tfcontratada_pessoanom_sel = AV57TFContratada_PessoaNom_Sel;
         AV141WWContratoGarantiaDS_28_Tfcontratada_pessoacnpj = AV60TFContratada_PessoaCNPJ;
         AV142WWContratoGarantiaDS_29_Tfcontratada_pessoacnpj_sel = AV61TFContratada_PessoaCNPJ_Sel;
         AV143WWContratoGarantiaDS_30_Tfcontratogarantia_datapagtogarantia = AV64TFContratoGarantia_DataPagtoGarantia;
         AV144WWContratoGarantiaDS_31_Tfcontratogarantia_datapagtogarantia_to = AV65TFContratoGarantia_DataPagtoGarantia_To;
         AV145WWContratoGarantiaDS_32_Tfcontratogarantia_percentual = AV70TFContratoGarantia_Percentual;
         AV146WWContratoGarantiaDS_33_Tfcontratogarantia_percentual_to = AV71TFContratoGarantia_Percentual_To;
         AV147WWContratoGarantiaDS_34_Tfcontratogarantia_datacaucao = AV74TFContratoGarantia_DataCaucao;
         AV148WWContratoGarantiaDS_35_Tfcontratogarantia_datacaucao_to = AV75TFContratoGarantia_DataCaucao_To;
         AV149WWContratoGarantiaDS_36_Tfcontratogarantia_valorgarantia = AV80TFContratoGarantia_ValorGarantia;
         AV150WWContratoGarantiaDS_37_Tfcontratogarantia_valorgarantia_to = AV81TFContratoGarantia_ValorGarantia_To;
         AV151WWContratoGarantiaDS_38_Tfcontratogarantia_dataencerramento = AV84TFContratoGarantia_DataEncerramento;
         AV152WWContratoGarantiaDS_39_Tfcontratogarantia_dataencerramento_to = AV85TFContratoGarantia_DataEncerramento_To;
         AV153WWContratoGarantiaDS_40_Tfcontratogarantia_valorencerramento = AV90TFContratoGarantia_ValorEncerramento;
         AV154WWContratoGarantiaDS_41_Tfcontratogarantia_valorencerramento_to = AV91TFContratoGarantia_ValorEncerramento_To;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV114WWContratoGarantiaDS_1_Dynamicfiltersselector1 ,
                                              AV115WWContratoGarantiaDS_2_Contratogarantia_datapagtogarantia1 ,
                                              AV116WWContratoGarantiaDS_3_Contratogarantia_datapagtogarantia_to1 ,
                                              AV117WWContratoGarantiaDS_4_Contratogarantia_datacaucao1 ,
                                              AV118WWContratoGarantiaDS_5_Contratogarantia_datacaucao_to1 ,
                                              AV119WWContratoGarantiaDS_6_Contratogarantia_dataencerramento1 ,
                                              AV120WWContratoGarantiaDS_7_Contratogarantia_dataencerramento_to1 ,
                                              AV121WWContratoGarantiaDS_8_Dynamicfiltersenabled2 ,
                                              AV122WWContratoGarantiaDS_9_Dynamicfiltersselector2 ,
                                              AV123WWContratoGarantiaDS_10_Contratogarantia_datapagtogarantia2 ,
                                              AV124WWContratoGarantiaDS_11_Contratogarantia_datapagtogarantia_to2 ,
                                              AV125WWContratoGarantiaDS_12_Contratogarantia_datacaucao2 ,
                                              AV126WWContratoGarantiaDS_13_Contratogarantia_datacaucao_to2 ,
                                              AV127WWContratoGarantiaDS_14_Contratogarantia_dataencerramento2 ,
                                              AV128WWContratoGarantiaDS_15_Contratogarantia_dataencerramento_to2 ,
                                              AV129WWContratoGarantiaDS_16_Dynamicfiltersenabled3 ,
                                              AV130WWContratoGarantiaDS_17_Dynamicfiltersselector3 ,
                                              AV131WWContratoGarantiaDS_18_Contratogarantia_datapagtogarantia3 ,
                                              AV132WWContratoGarantiaDS_19_Contratogarantia_datapagtogarantia_to3 ,
                                              AV133WWContratoGarantiaDS_20_Contratogarantia_datacaucao3 ,
                                              AV134WWContratoGarantiaDS_21_Contratogarantia_datacaucao_to3 ,
                                              AV135WWContratoGarantiaDS_22_Contratogarantia_dataencerramento3 ,
                                              AV136WWContratoGarantiaDS_23_Contratogarantia_dataencerramento_to3 ,
                                              AV138WWContratoGarantiaDS_25_Tfcontrato_numero_sel ,
                                              AV137WWContratoGarantiaDS_24_Tfcontrato_numero ,
                                              AV140WWContratoGarantiaDS_27_Tfcontratada_pessoanom_sel ,
                                              AV139WWContratoGarantiaDS_26_Tfcontratada_pessoanom ,
                                              AV142WWContratoGarantiaDS_29_Tfcontratada_pessoacnpj_sel ,
                                              AV141WWContratoGarantiaDS_28_Tfcontratada_pessoacnpj ,
                                              AV143WWContratoGarantiaDS_30_Tfcontratogarantia_datapagtogarantia ,
                                              AV144WWContratoGarantiaDS_31_Tfcontratogarantia_datapagtogarantia_to ,
                                              AV145WWContratoGarantiaDS_32_Tfcontratogarantia_percentual ,
                                              AV146WWContratoGarantiaDS_33_Tfcontratogarantia_percentual_to ,
                                              AV147WWContratoGarantiaDS_34_Tfcontratogarantia_datacaucao ,
                                              AV148WWContratoGarantiaDS_35_Tfcontratogarantia_datacaucao_to ,
                                              AV149WWContratoGarantiaDS_36_Tfcontratogarantia_valorgarantia ,
                                              AV150WWContratoGarantiaDS_37_Tfcontratogarantia_valorgarantia_to ,
                                              AV151WWContratoGarantiaDS_38_Tfcontratogarantia_dataencerramento ,
                                              AV152WWContratoGarantiaDS_39_Tfcontratogarantia_dataencerramento_to ,
                                              AV153WWContratoGarantiaDS_40_Tfcontratogarantia_valorencerramento ,
                                              AV154WWContratoGarantiaDS_41_Tfcontratogarantia_valorencerramento_to ,
                                              A102ContratoGarantia_DataPagtoGarantia ,
                                              A104ContratoGarantia_DataCaucao ,
                                              A106ContratoGarantia_DataEncerramento ,
                                              A77Contrato_Numero ,
                                              A41Contratada_PessoaNom ,
                                              A42Contratada_PessoaCNPJ ,
                                              A103ContratoGarantia_Percentual ,
                                              A105ContratoGarantia_ValorGarantia ,
                                              A107ContratoGarantia_ValorEncerramento ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DECIMAL,
                                              TypeConstants.DECIMAL, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DECIMAL,
                                              TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                              }
         });
         lV137WWContratoGarantiaDS_24_Tfcontrato_numero = StringUtil.PadR( StringUtil.RTrim( AV137WWContratoGarantiaDS_24_Tfcontrato_numero), 20, "%");
         lV139WWContratoGarantiaDS_26_Tfcontratada_pessoanom = StringUtil.PadR( StringUtil.RTrim( AV139WWContratoGarantiaDS_26_Tfcontratada_pessoanom), 100, "%");
         lV141WWContratoGarantiaDS_28_Tfcontratada_pessoacnpj = StringUtil.Concat( StringUtil.RTrim( AV141WWContratoGarantiaDS_28_Tfcontratada_pessoacnpj), "%", "");
         /* Using cursor H006H3 */
         pr_default.execute(1, new Object[] {AV115WWContratoGarantiaDS_2_Contratogarantia_datapagtogarantia1, AV116WWContratoGarantiaDS_3_Contratogarantia_datapagtogarantia_to1, AV117WWContratoGarantiaDS_4_Contratogarantia_datacaucao1, AV118WWContratoGarantiaDS_5_Contratogarantia_datacaucao_to1, AV119WWContratoGarantiaDS_6_Contratogarantia_dataencerramento1, AV120WWContratoGarantiaDS_7_Contratogarantia_dataencerramento_to1, AV123WWContratoGarantiaDS_10_Contratogarantia_datapagtogarantia2, AV124WWContratoGarantiaDS_11_Contratogarantia_datapagtogarantia_to2, AV125WWContratoGarantiaDS_12_Contratogarantia_datacaucao2, AV126WWContratoGarantiaDS_13_Contratogarantia_datacaucao_to2, AV127WWContratoGarantiaDS_14_Contratogarantia_dataencerramento2, AV128WWContratoGarantiaDS_15_Contratogarantia_dataencerramento_to2, AV131WWContratoGarantiaDS_18_Contratogarantia_datapagtogarantia3, AV132WWContratoGarantiaDS_19_Contratogarantia_datapagtogarantia_to3, AV133WWContratoGarantiaDS_20_Contratogarantia_datacaucao3, AV134WWContratoGarantiaDS_21_Contratogarantia_datacaucao_to3, AV135WWContratoGarantiaDS_22_Contratogarantia_dataencerramento3, AV136WWContratoGarantiaDS_23_Contratogarantia_dataencerramento_to3, lV137WWContratoGarantiaDS_24_Tfcontrato_numero, AV138WWContratoGarantiaDS_25_Tfcontrato_numero_sel, lV139WWContratoGarantiaDS_26_Tfcontratada_pessoanom, AV140WWContratoGarantiaDS_27_Tfcontratada_pessoanom_sel, lV141WWContratoGarantiaDS_28_Tfcontratada_pessoacnpj, AV142WWContratoGarantiaDS_29_Tfcontratada_pessoacnpj_sel, AV143WWContratoGarantiaDS_30_Tfcontratogarantia_datapagtogarantia, AV144WWContratoGarantiaDS_31_Tfcontratogarantia_datapagtogarantia_to, AV145WWContratoGarantiaDS_32_Tfcontratogarantia_percentual, AV146WWContratoGarantiaDS_33_Tfcontratogarantia_percentual_to, AV147WWContratoGarantiaDS_34_Tfcontratogarantia_datacaucao, AV148WWContratoGarantiaDS_35_Tfcontratogarantia_datacaucao_to, AV149WWContratoGarantiaDS_36_Tfcontratogarantia_valorgarantia, AV150WWContratoGarantiaDS_37_Tfcontratogarantia_valorgarantia_to, AV151WWContratoGarantiaDS_38_Tfcontratogarantia_dataencerramento, AV152WWContratoGarantiaDS_39_Tfcontratogarantia_dataencerramento_to, AV153WWContratoGarantiaDS_40_Tfcontratogarantia_valorencerramento, AV154WWContratoGarantiaDS_41_Tfcontratogarantia_valorencerramento_to});
         GRID_nRecordCount = H006H3_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         AV114WWContratoGarantiaDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV115WWContratoGarantiaDS_2_Contratogarantia_datapagtogarantia1 = AV16ContratoGarantia_DataPagtoGarantia1;
         AV116WWContratoGarantiaDS_3_Contratogarantia_datapagtogarantia_to1 = AV17ContratoGarantia_DataPagtoGarantia_To1;
         AV117WWContratoGarantiaDS_4_Contratogarantia_datacaucao1 = AV37ContratoGarantia_DataCaucao1;
         AV118WWContratoGarantiaDS_5_Contratogarantia_datacaucao_to1 = AV38ContratoGarantia_DataCaucao_To1;
         AV119WWContratoGarantiaDS_6_Contratogarantia_dataencerramento1 = AV39ContratoGarantia_DataEncerramento1;
         AV120WWContratoGarantiaDS_7_Contratogarantia_dataencerramento_to1 = AV40ContratoGarantia_DataEncerramento_To1;
         AV121WWContratoGarantiaDS_8_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV122WWContratoGarantiaDS_9_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV123WWContratoGarantiaDS_10_Contratogarantia_datapagtogarantia2 = AV20ContratoGarantia_DataPagtoGarantia2;
         AV124WWContratoGarantiaDS_11_Contratogarantia_datapagtogarantia_to2 = AV21ContratoGarantia_DataPagtoGarantia_To2;
         AV125WWContratoGarantiaDS_12_Contratogarantia_datacaucao2 = AV41ContratoGarantia_DataCaucao2;
         AV126WWContratoGarantiaDS_13_Contratogarantia_datacaucao_to2 = AV42ContratoGarantia_DataCaucao_To2;
         AV127WWContratoGarantiaDS_14_Contratogarantia_dataencerramento2 = AV43ContratoGarantia_DataEncerramento2;
         AV128WWContratoGarantiaDS_15_Contratogarantia_dataencerramento_to2 = AV44ContratoGarantia_DataEncerramento_To2;
         AV129WWContratoGarantiaDS_16_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV130WWContratoGarantiaDS_17_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV131WWContratoGarantiaDS_18_Contratogarantia_datapagtogarantia3 = AV24ContratoGarantia_DataPagtoGarantia3;
         AV132WWContratoGarantiaDS_19_Contratogarantia_datapagtogarantia_to3 = AV25ContratoGarantia_DataPagtoGarantia_To3;
         AV133WWContratoGarantiaDS_20_Contratogarantia_datacaucao3 = AV45ContratoGarantia_DataCaucao3;
         AV134WWContratoGarantiaDS_21_Contratogarantia_datacaucao_to3 = AV46ContratoGarantia_DataCaucao_To3;
         AV135WWContratoGarantiaDS_22_Contratogarantia_dataencerramento3 = AV47ContratoGarantia_DataEncerramento3;
         AV136WWContratoGarantiaDS_23_Contratogarantia_dataencerramento_to3 = AV48ContratoGarantia_DataEncerramento_To3;
         AV137WWContratoGarantiaDS_24_Tfcontrato_numero = AV52TFContrato_Numero;
         AV138WWContratoGarantiaDS_25_Tfcontrato_numero_sel = AV53TFContrato_Numero_Sel;
         AV139WWContratoGarantiaDS_26_Tfcontratada_pessoanom = AV56TFContratada_PessoaNom;
         AV140WWContratoGarantiaDS_27_Tfcontratada_pessoanom_sel = AV57TFContratada_PessoaNom_Sel;
         AV141WWContratoGarantiaDS_28_Tfcontratada_pessoacnpj = AV60TFContratada_PessoaCNPJ;
         AV142WWContratoGarantiaDS_29_Tfcontratada_pessoacnpj_sel = AV61TFContratada_PessoaCNPJ_Sel;
         AV143WWContratoGarantiaDS_30_Tfcontratogarantia_datapagtogarantia = AV64TFContratoGarantia_DataPagtoGarantia;
         AV144WWContratoGarantiaDS_31_Tfcontratogarantia_datapagtogarantia_to = AV65TFContratoGarantia_DataPagtoGarantia_To;
         AV145WWContratoGarantiaDS_32_Tfcontratogarantia_percentual = AV70TFContratoGarantia_Percentual;
         AV146WWContratoGarantiaDS_33_Tfcontratogarantia_percentual_to = AV71TFContratoGarantia_Percentual_To;
         AV147WWContratoGarantiaDS_34_Tfcontratogarantia_datacaucao = AV74TFContratoGarantia_DataCaucao;
         AV148WWContratoGarantiaDS_35_Tfcontratogarantia_datacaucao_to = AV75TFContratoGarantia_DataCaucao_To;
         AV149WWContratoGarantiaDS_36_Tfcontratogarantia_valorgarantia = AV80TFContratoGarantia_ValorGarantia;
         AV150WWContratoGarantiaDS_37_Tfcontratogarantia_valorgarantia_to = AV81TFContratoGarantia_ValorGarantia_To;
         AV151WWContratoGarantiaDS_38_Tfcontratogarantia_dataencerramento = AV84TFContratoGarantia_DataEncerramento;
         AV152WWContratoGarantiaDS_39_Tfcontratogarantia_dataencerramento_to = AV85TFContratoGarantia_DataEncerramento_To;
         AV153WWContratoGarantiaDS_40_Tfcontratogarantia_valorencerramento = AV90TFContratoGarantia_ValorEncerramento;
         AV154WWContratoGarantiaDS_41_Tfcontratogarantia_valorencerramento_to = AV91TFContratoGarantia_ValorEncerramento_To;
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16ContratoGarantia_DataPagtoGarantia1, AV17ContratoGarantia_DataPagtoGarantia_To1, AV37ContratoGarantia_DataCaucao1, AV38ContratoGarantia_DataCaucao_To1, AV39ContratoGarantia_DataEncerramento1, AV40ContratoGarantia_DataEncerramento_To1, AV19DynamicFiltersSelector2, AV20ContratoGarantia_DataPagtoGarantia2, AV21ContratoGarantia_DataPagtoGarantia_To2, AV41ContratoGarantia_DataCaucao2, AV42ContratoGarantia_DataCaucao_To2, AV43ContratoGarantia_DataEncerramento2, AV44ContratoGarantia_DataEncerramento_To2, AV23DynamicFiltersSelector3, AV24ContratoGarantia_DataPagtoGarantia3, AV25ContratoGarantia_DataPagtoGarantia_To3, AV45ContratoGarantia_DataCaucao3, AV46ContratoGarantia_DataCaucao_To3, AV47ContratoGarantia_DataEncerramento3, AV48ContratoGarantia_DataEncerramento_To3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV52TFContrato_Numero, AV53TFContrato_Numero_Sel, AV56TFContratada_PessoaNom, AV57TFContratada_PessoaNom_Sel, AV60TFContratada_PessoaCNPJ, AV61TFContratada_PessoaCNPJ_Sel, AV64TFContratoGarantia_DataPagtoGarantia, AV65TFContratoGarantia_DataPagtoGarantia_To, AV70TFContratoGarantia_Percentual, AV71TFContratoGarantia_Percentual_To, AV74TFContratoGarantia_DataCaucao, AV75TFContratoGarantia_DataCaucao_To, AV80TFContratoGarantia_ValorGarantia, AV81TFContratoGarantia_ValorGarantia_To, AV84TFContratoGarantia_DataEncerramento, AV85TFContratoGarantia_DataEncerramento_To, AV90TFContratoGarantia_ValorEncerramento, AV91TFContratoGarantia_ValorEncerramento_To, AV54ddo_Contrato_NumeroTitleControlIdToReplace, AV58ddo_Contratada_PessoaNomTitleControlIdToReplace, AV62ddo_Contratada_PessoaCNPJTitleControlIdToReplace, AV68ddo_ContratoGarantia_DataPagtoGarantiaTitleControlIdToReplace, AV72ddo_ContratoGarantia_PercentualTitleControlIdToReplace, AV78ddo_ContratoGarantia_DataCaucaoTitleControlIdToReplace, AV82ddo_ContratoGarantia_ValorGarantiaTitleControlIdToReplace, AV88ddo_ContratoGarantia_DataEncerramentoTitleControlIdToReplace, AV92ddo_ContratoGarantia_ValorEncerramentoTitleControlIdToReplace, AV157Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A101ContratoGarantia_Codigo, A74Contrato_Codigo, A39Contratada_Codigo) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         AV114WWContratoGarantiaDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV115WWContratoGarantiaDS_2_Contratogarantia_datapagtogarantia1 = AV16ContratoGarantia_DataPagtoGarantia1;
         AV116WWContratoGarantiaDS_3_Contratogarantia_datapagtogarantia_to1 = AV17ContratoGarantia_DataPagtoGarantia_To1;
         AV117WWContratoGarantiaDS_4_Contratogarantia_datacaucao1 = AV37ContratoGarantia_DataCaucao1;
         AV118WWContratoGarantiaDS_5_Contratogarantia_datacaucao_to1 = AV38ContratoGarantia_DataCaucao_To1;
         AV119WWContratoGarantiaDS_6_Contratogarantia_dataencerramento1 = AV39ContratoGarantia_DataEncerramento1;
         AV120WWContratoGarantiaDS_7_Contratogarantia_dataencerramento_to1 = AV40ContratoGarantia_DataEncerramento_To1;
         AV121WWContratoGarantiaDS_8_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV122WWContratoGarantiaDS_9_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV123WWContratoGarantiaDS_10_Contratogarantia_datapagtogarantia2 = AV20ContratoGarantia_DataPagtoGarantia2;
         AV124WWContratoGarantiaDS_11_Contratogarantia_datapagtogarantia_to2 = AV21ContratoGarantia_DataPagtoGarantia_To2;
         AV125WWContratoGarantiaDS_12_Contratogarantia_datacaucao2 = AV41ContratoGarantia_DataCaucao2;
         AV126WWContratoGarantiaDS_13_Contratogarantia_datacaucao_to2 = AV42ContratoGarantia_DataCaucao_To2;
         AV127WWContratoGarantiaDS_14_Contratogarantia_dataencerramento2 = AV43ContratoGarantia_DataEncerramento2;
         AV128WWContratoGarantiaDS_15_Contratogarantia_dataencerramento_to2 = AV44ContratoGarantia_DataEncerramento_To2;
         AV129WWContratoGarantiaDS_16_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV130WWContratoGarantiaDS_17_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV131WWContratoGarantiaDS_18_Contratogarantia_datapagtogarantia3 = AV24ContratoGarantia_DataPagtoGarantia3;
         AV132WWContratoGarantiaDS_19_Contratogarantia_datapagtogarantia_to3 = AV25ContratoGarantia_DataPagtoGarantia_To3;
         AV133WWContratoGarantiaDS_20_Contratogarantia_datacaucao3 = AV45ContratoGarantia_DataCaucao3;
         AV134WWContratoGarantiaDS_21_Contratogarantia_datacaucao_to3 = AV46ContratoGarantia_DataCaucao_To3;
         AV135WWContratoGarantiaDS_22_Contratogarantia_dataencerramento3 = AV47ContratoGarantia_DataEncerramento3;
         AV136WWContratoGarantiaDS_23_Contratogarantia_dataencerramento_to3 = AV48ContratoGarantia_DataEncerramento_To3;
         AV137WWContratoGarantiaDS_24_Tfcontrato_numero = AV52TFContrato_Numero;
         AV138WWContratoGarantiaDS_25_Tfcontrato_numero_sel = AV53TFContrato_Numero_Sel;
         AV139WWContratoGarantiaDS_26_Tfcontratada_pessoanom = AV56TFContratada_PessoaNom;
         AV140WWContratoGarantiaDS_27_Tfcontratada_pessoanom_sel = AV57TFContratada_PessoaNom_Sel;
         AV141WWContratoGarantiaDS_28_Tfcontratada_pessoacnpj = AV60TFContratada_PessoaCNPJ;
         AV142WWContratoGarantiaDS_29_Tfcontratada_pessoacnpj_sel = AV61TFContratada_PessoaCNPJ_Sel;
         AV143WWContratoGarantiaDS_30_Tfcontratogarantia_datapagtogarantia = AV64TFContratoGarantia_DataPagtoGarantia;
         AV144WWContratoGarantiaDS_31_Tfcontratogarantia_datapagtogarantia_to = AV65TFContratoGarantia_DataPagtoGarantia_To;
         AV145WWContratoGarantiaDS_32_Tfcontratogarantia_percentual = AV70TFContratoGarantia_Percentual;
         AV146WWContratoGarantiaDS_33_Tfcontratogarantia_percentual_to = AV71TFContratoGarantia_Percentual_To;
         AV147WWContratoGarantiaDS_34_Tfcontratogarantia_datacaucao = AV74TFContratoGarantia_DataCaucao;
         AV148WWContratoGarantiaDS_35_Tfcontratogarantia_datacaucao_to = AV75TFContratoGarantia_DataCaucao_To;
         AV149WWContratoGarantiaDS_36_Tfcontratogarantia_valorgarantia = AV80TFContratoGarantia_ValorGarantia;
         AV150WWContratoGarantiaDS_37_Tfcontratogarantia_valorgarantia_to = AV81TFContratoGarantia_ValorGarantia_To;
         AV151WWContratoGarantiaDS_38_Tfcontratogarantia_dataencerramento = AV84TFContratoGarantia_DataEncerramento;
         AV152WWContratoGarantiaDS_39_Tfcontratogarantia_dataencerramento_to = AV85TFContratoGarantia_DataEncerramento_To;
         AV153WWContratoGarantiaDS_40_Tfcontratogarantia_valorencerramento = AV90TFContratoGarantia_ValorEncerramento;
         AV154WWContratoGarantiaDS_41_Tfcontratogarantia_valorencerramento_to = AV91TFContratoGarantia_ValorEncerramento_To;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16ContratoGarantia_DataPagtoGarantia1, AV17ContratoGarantia_DataPagtoGarantia_To1, AV37ContratoGarantia_DataCaucao1, AV38ContratoGarantia_DataCaucao_To1, AV39ContratoGarantia_DataEncerramento1, AV40ContratoGarantia_DataEncerramento_To1, AV19DynamicFiltersSelector2, AV20ContratoGarantia_DataPagtoGarantia2, AV21ContratoGarantia_DataPagtoGarantia_To2, AV41ContratoGarantia_DataCaucao2, AV42ContratoGarantia_DataCaucao_To2, AV43ContratoGarantia_DataEncerramento2, AV44ContratoGarantia_DataEncerramento_To2, AV23DynamicFiltersSelector3, AV24ContratoGarantia_DataPagtoGarantia3, AV25ContratoGarantia_DataPagtoGarantia_To3, AV45ContratoGarantia_DataCaucao3, AV46ContratoGarantia_DataCaucao_To3, AV47ContratoGarantia_DataEncerramento3, AV48ContratoGarantia_DataEncerramento_To3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV52TFContrato_Numero, AV53TFContrato_Numero_Sel, AV56TFContratada_PessoaNom, AV57TFContratada_PessoaNom_Sel, AV60TFContratada_PessoaCNPJ, AV61TFContratada_PessoaCNPJ_Sel, AV64TFContratoGarantia_DataPagtoGarantia, AV65TFContratoGarantia_DataPagtoGarantia_To, AV70TFContratoGarantia_Percentual, AV71TFContratoGarantia_Percentual_To, AV74TFContratoGarantia_DataCaucao, AV75TFContratoGarantia_DataCaucao_To, AV80TFContratoGarantia_ValorGarantia, AV81TFContratoGarantia_ValorGarantia_To, AV84TFContratoGarantia_DataEncerramento, AV85TFContratoGarantia_DataEncerramento_To, AV90TFContratoGarantia_ValorEncerramento, AV91TFContratoGarantia_ValorEncerramento_To, AV54ddo_Contrato_NumeroTitleControlIdToReplace, AV58ddo_Contratada_PessoaNomTitleControlIdToReplace, AV62ddo_Contratada_PessoaCNPJTitleControlIdToReplace, AV68ddo_ContratoGarantia_DataPagtoGarantiaTitleControlIdToReplace, AV72ddo_ContratoGarantia_PercentualTitleControlIdToReplace, AV78ddo_ContratoGarantia_DataCaucaoTitleControlIdToReplace, AV82ddo_ContratoGarantia_ValorGarantiaTitleControlIdToReplace, AV88ddo_ContratoGarantia_DataEncerramentoTitleControlIdToReplace, AV92ddo_ContratoGarantia_ValorEncerramentoTitleControlIdToReplace, AV157Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A101ContratoGarantia_Codigo, A74Contrato_Codigo, A39Contratada_Codigo) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         AV114WWContratoGarantiaDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV115WWContratoGarantiaDS_2_Contratogarantia_datapagtogarantia1 = AV16ContratoGarantia_DataPagtoGarantia1;
         AV116WWContratoGarantiaDS_3_Contratogarantia_datapagtogarantia_to1 = AV17ContratoGarantia_DataPagtoGarantia_To1;
         AV117WWContratoGarantiaDS_4_Contratogarantia_datacaucao1 = AV37ContratoGarantia_DataCaucao1;
         AV118WWContratoGarantiaDS_5_Contratogarantia_datacaucao_to1 = AV38ContratoGarantia_DataCaucao_To1;
         AV119WWContratoGarantiaDS_6_Contratogarantia_dataencerramento1 = AV39ContratoGarantia_DataEncerramento1;
         AV120WWContratoGarantiaDS_7_Contratogarantia_dataencerramento_to1 = AV40ContratoGarantia_DataEncerramento_To1;
         AV121WWContratoGarantiaDS_8_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV122WWContratoGarantiaDS_9_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV123WWContratoGarantiaDS_10_Contratogarantia_datapagtogarantia2 = AV20ContratoGarantia_DataPagtoGarantia2;
         AV124WWContratoGarantiaDS_11_Contratogarantia_datapagtogarantia_to2 = AV21ContratoGarantia_DataPagtoGarantia_To2;
         AV125WWContratoGarantiaDS_12_Contratogarantia_datacaucao2 = AV41ContratoGarantia_DataCaucao2;
         AV126WWContratoGarantiaDS_13_Contratogarantia_datacaucao_to2 = AV42ContratoGarantia_DataCaucao_To2;
         AV127WWContratoGarantiaDS_14_Contratogarantia_dataencerramento2 = AV43ContratoGarantia_DataEncerramento2;
         AV128WWContratoGarantiaDS_15_Contratogarantia_dataencerramento_to2 = AV44ContratoGarantia_DataEncerramento_To2;
         AV129WWContratoGarantiaDS_16_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV130WWContratoGarantiaDS_17_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV131WWContratoGarantiaDS_18_Contratogarantia_datapagtogarantia3 = AV24ContratoGarantia_DataPagtoGarantia3;
         AV132WWContratoGarantiaDS_19_Contratogarantia_datapagtogarantia_to3 = AV25ContratoGarantia_DataPagtoGarantia_To3;
         AV133WWContratoGarantiaDS_20_Contratogarantia_datacaucao3 = AV45ContratoGarantia_DataCaucao3;
         AV134WWContratoGarantiaDS_21_Contratogarantia_datacaucao_to3 = AV46ContratoGarantia_DataCaucao_To3;
         AV135WWContratoGarantiaDS_22_Contratogarantia_dataencerramento3 = AV47ContratoGarantia_DataEncerramento3;
         AV136WWContratoGarantiaDS_23_Contratogarantia_dataencerramento_to3 = AV48ContratoGarantia_DataEncerramento_To3;
         AV137WWContratoGarantiaDS_24_Tfcontrato_numero = AV52TFContrato_Numero;
         AV138WWContratoGarantiaDS_25_Tfcontrato_numero_sel = AV53TFContrato_Numero_Sel;
         AV139WWContratoGarantiaDS_26_Tfcontratada_pessoanom = AV56TFContratada_PessoaNom;
         AV140WWContratoGarantiaDS_27_Tfcontratada_pessoanom_sel = AV57TFContratada_PessoaNom_Sel;
         AV141WWContratoGarantiaDS_28_Tfcontratada_pessoacnpj = AV60TFContratada_PessoaCNPJ;
         AV142WWContratoGarantiaDS_29_Tfcontratada_pessoacnpj_sel = AV61TFContratada_PessoaCNPJ_Sel;
         AV143WWContratoGarantiaDS_30_Tfcontratogarantia_datapagtogarantia = AV64TFContratoGarantia_DataPagtoGarantia;
         AV144WWContratoGarantiaDS_31_Tfcontratogarantia_datapagtogarantia_to = AV65TFContratoGarantia_DataPagtoGarantia_To;
         AV145WWContratoGarantiaDS_32_Tfcontratogarantia_percentual = AV70TFContratoGarantia_Percentual;
         AV146WWContratoGarantiaDS_33_Tfcontratogarantia_percentual_to = AV71TFContratoGarantia_Percentual_To;
         AV147WWContratoGarantiaDS_34_Tfcontratogarantia_datacaucao = AV74TFContratoGarantia_DataCaucao;
         AV148WWContratoGarantiaDS_35_Tfcontratogarantia_datacaucao_to = AV75TFContratoGarantia_DataCaucao_To;
         AV149WWContratoGarantiaDS_36_Tfcontratogarantia_valorgarantia = AV80TFContratoGarantia_ValorGarantia;
         AV150WWContratoGarantiaDS_37_Tfcontratogarantia_valorgarantia_to = AV81TFContratoGarantia_ValorGarantia_To;
         AV151WWContratoGarantiaDS_38_Tfcontratogarantia_dataencerramento = AV84TFContratoGarantia_DataEncerramento;
         AV152WWContratoGarantiaDS_39_Tfcontratogarantia_dataencerramento_to = AV85TFContratoGarantia_DataEncerramento_To;
         AV153WWContratoGarantiaDS_40_Tfcontratogarantia_valorencerramento = AV90TFContratoGarantia_ValorEncerramento;
         AV154WWContratoGarantiaDS_41_Tfcontratogarantia_valorencerramento_to = AV91TFContratoGarantia_ValorEncerramento_To;
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16ContratoGarantia_DataPagtoGarantia1, AV17ContratoGarantia_DataPagtoGarantia_To1, AV37ContratoGarantia_DataCaucao1, AV38ContratoGarantia_DataCaucao_To1, AV39ContratoGarantia_DataEncerramento1, AV40ContratoGarantia_DataEncerramento_To1, AV19DynamicFiltersSelector2, AV20ContratoGarantia_DataPagtoGarantia2, AV21ContratoGarantia_DataPagtoGarantia_To2, AV41ContratoGarantia_DataCaucao2, AV42ContratoGarantia_DataCaucao_To2, AV43ContratoGarantia_DataEncerramento2, AV44ContratoGarantia_DataEncerramento_To2, AV23DynamicFiltersSelector3, AV24ContratoGarantia_DataPagtoGarantia3, AV25ContratoGarantia_DataPagtoGarantia_To3, AV45ContratoGarantia_DataCaucao3, AV46ContratoGarantia_DataCaucao_To3, AV47ContratoGarantia_DataEncerramento3, AV48ContratoGarantia_DataEncerramento_To3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV52TFContrato_Numero, AV53TFContrato_Numero_Sel, AV56TFContratada_PessoaNom, AV57TFContratada_PessoaNom_Sel, AV60TFContratada_PessoaCNPJ, AV61TFContratada_PessoaCNPJ_Sel, AV64TFContratoGarantia_DataPagtoGarantia, AV65TFContratoGarantia_DataPagtoGarantia_To, AV70TFContratoGarantia_Percentual, AV71TFContratoGarantia_Percentual_To, AV74TFContratoGarantia_DataCaucao, AV75TFContratoGarantia_DataCaucao_To, AV80TFContratoGarantia_ValorGarantia, AV81TFContratoGarantia_ValorGarantia_To, AV84TFContratoGarantia_DataEncerramento, AV85TFContratoGarantia_DataEncerramento_To, AV90TFContratoGarantia_ValorEncerramento, AV91TFContratoGarantia_ValorEncerramento_To, AV54ddo_Contrato_NumeroTitleControlIdToReplace, AV58ddo_Contratada_PessoaNomTitleControlIdToReplace, AV62ddo_Contratada_PessoaCNPJTitleControlIdToReplace, AV68ddo_ContratoGarantia_DataPagtoGarantiaTitleControlIdToReplace, AV72ddo_ContratoGarantia_PercentualTitleControlIdToReplace, AV78ddo_ContratoGarantia_DataCaucaoTitleControlIdToReplace, AV82ddo_ContratoGarantia_ValorGarantiaTitleControlIdToReplace, AV88ddo_ContratoGarantia_DataEncerramentoTitleControlIdToReplace, AV92ddo_ContratoGarantia_ValorEncerramentoTitleControlIdToReplace, AV157Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A101ContratoGarantia_Codigo, A74Contrato_Codigo, A39Contratada_Codigo) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         AV114WWContratoGarantiaDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV115WWContratoGarantiaDS_2_Contratogarantia_datapagtogarantia1 = AV16ContratoGarantia_DataPagtoGarantia1;
         AV116WWContratoGarantiaDS_3_Contratogarantia_datapagtogarantia_to1 = AV17ContratoGarantia_DataPagtoGarantia_To1;
         AV117WWContratoGarantiaDS_4_Contratogarantia_datacaucao1 = AV37ContratoGarantia_DataCaucao1;
         AV118WWContratoGarantiaDS_5_Contratogarantia_datacaucao_to1 = AV38ContratoGarantia_DataCaucao_To1;
         AV119WWContratoGarantiaDS_6_Contratogarantia_dataencerramento1 = AV39ContratoGarantia_DataEncerramento1;
         AV120WWContratoGarantiaDS_7_Contratogarantia_dataencerramento_to1 = AV40ContratoGarantia_DataEncerramento_To1;
         AV121WWContratoGarantiaDS_8_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV122WWContratoGarantiaDS_9_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV123WWContratoGarantiaDS_10_Contratogarantia_datapagtogarantia2 = AV20ContratoGarantia_DataPagtoGarantia2;
         AV124WWContratoGarantiaDS_11_Contratogarantia_datapagtogarantia_to2 = AV21ContratoGarantia_DataPagtoGarantia_To2;
         AV125WWContratoGarantiaDS_12_Contratogarantia_datacaucao2 = AV41ContratoGarantia_DataCaucao2;
         AV126WWContratoGarantiaDS_13_Contratogarantia_datacaucao_to2 = AV42ContratoGarantia_DataCaucao_To2;
         AV127WWContratoGarantiaDS_14_Contratogarantia_dataencerramento2 = AV43ContratoGarantia_DataEncerramento2;
         AV128WWContratoGarantiaDS_15_Contratogarantia_dataencerramento_to2 = AV44ContratoGarantia_DataEncerramento_To2;
         AV129WWContratoGarantiaDS_16_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV130WWContratoGarantiaDS_17_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV131WWContratoGarantiaDS_18_Contratogarantia_datapagtogarantia3 = AV24ContratoGarantia_DataPagtoGarantia3;
         AV132WWContratoGarantiaDS_19_Contratogarantia_datapagtogarantia_to3 = AV25ContratoGarantia_DataPagtoGarantia_To3;
         AV133WWContratoGarantiaDS_20_Contratogarantia_datacaucao3 = AV45ContratoGarantia_DataCaucao3;
         AV134WWContratoGarantiaDS_21_Contratogarantia_datacaucao_to3 = AV46ContratoGarantia_DataCaucao_To3;
         AV135WWContratoGarantiaDS_22_Contratogarantia_dataencerramento3 = AV47ContratoGarantia_DataEncerramento3;
         AV136WWContratoGarantiaDS_23_Contratogarantia_dataencerramento_to3 = AV48ContratoGarantia_DataEncerramento_To3;
         AV137WWContratoGarantiaDS_24_Tfcontrato_numero = AV52TFContrato_Numero;
         AV138WWContratoGarantiaDS_25_Tfcontrato_numero_sel = AV53TFContrato_Numero_Sel;
         AV139WWContratoGarantiaDS_26_Tfcontratada_pessoanom = AV56TFContratada_PessoaNom;
         AV140WWContratoGarantiaDS_27_Tfcontratada_pessoanom_sel = AV57TFContratada_PessoaNom_Sel;
         AV141WWContratoGarantiaDS_28_Tfcontratada_pessoacnpj = AV60TFContratada_PessoaCNPJ;
         AV142WWContratoGarantiaDS_29_Tfcontratada_pessoacnpj_sel = AV61TFContratada_PessoaCNPJ_Sel;
         AV143WWContratoGarantiaDS_30_Tfcontratogarantia_datapagtogarantia = AV64TFContratoGarantia_DataPagtoGarantia;
         AV144WWContratoGarantiaDS_31_Tfcontratogarantia_datapagtogarantia_to = AV65TFContratoGarantia_DataPagtoGarantia_To;
         AV145WWContratoGarantiaDS_32_Tfcontratogarantia_percentual = AV70TFContratoGarantia_Percentual;
         AV146WWContratoGarantiaDS_33_Tfcontratogarantia_percentual_to = AV71TFContratoGarantia_Percentual_To;
         AV147WWContratoGarantiaDS_34_Tfcontratogarantia_datacaucao = AV74TFContratoGarantia_DataCaucao;
         AV148WWContratoGarantiaDS_35_Tfcontratogarantia_datacaucao_to = AV75TFContratoGarantia_DataCaucao_To;
         AV149WWContratoGarantiaDS_36_Tfcontratogarantia_valorgarantia = AV80TFContratoGarantia_ValorGarantia;
         AV150WWContratoGarantiaDS_37_Tfcontratogarantia_valorgarantia_to = AV81TFContratoGarantia_ValorGarantia_To;
         AV151WWContratoGarantiaDS_38_Tfcontratogarantia_dataencerramento = AV84TFContratoGarantia_DataEncerramento;
         AV152WWContratoGarantiaDS_39_Tfcontratogarantia_dataencerramento_to = AV85TFContratoGarantia_DataEncerramento_To;
         AV153WWContratoGarantiaDS_40_Tfcontratogarantia_valorencerramento = AV90TFContratoGarantia_ValorEncerramento;
         AV154WWContratoGarantiaDS_41_Tfcontratogarantia_valorencerramento_to = AV91TFContratoGarantia_ValorEncerramento_To;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16ContratoGarantia_DataPagtoGarantia1, AV17ContratoGarantia_DataPagtoGarantia_To1, AV37ContratoGarantia_DataCaucao1, AV38ContratoGarantia_DataCaucao_To1, AV39ContratoGarantia_DataEncerramento1, AV40ContratoGarantia_DataEncerramento_To1, AV19DynamicFiltersSelector2, AV20ContratoGarantia_DataPagtoGarantia2, AV21ContratoGarantia_DataPagtoGarantia_To2, AV41ContratoGarantia_DataCaucao2, AV42ContratoGarantia_DataCaucao_To2, AV43ContratoGarantia_DataEncerramento2, AV44ContratoGarantia_DataEncerramento_To2, AV23DynamicFiltersSelector3, AV24ContratoGarantia_DataPagtoGarantia3, AV25ContratoGarantia_DataPagtoGarantia_To3, AV45ContratoGarantia_DataCaucao3, AV46ContratoGarantia_DataCaucao_To3, AV47ContratoGarantia_DataEncerramento3, AV48ContratoGarantia_DataEncerramento_To3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV52TFContrato_Numero, AV53TFContrato_Numero_Sel, AV56TFContratada_PessoaNom, AV57TFContratada_PessoaNom_Sel, AV60TFContratada_PessoaCNPJ, AV61TFContratada_PessoaCNPJ_Sel, AV64TFContratoGarantia_DataPagtoGarantia, AV65TFContratoGarantia_DataPagtoGarantia_To, AV70TFContratoGarantia_Percentual, AV71TFContratoGarantia_Percentual_To, AV74TFContratoGarantia_DataCaucao, AV75TFContratoGarantia_DataCaucao_To, AV80TFContratoGarantia_ValorGarantia, AV81TFContratoGarantia_ValorGarantia_To, AV84TFContratoGarantia_DataEncerramento, AV85TFContratoGarantia_DataEncerramento_To, AV90TFContratoGarantia_ValorEncerramento, AV91TFContratoGarantia_ValorEncerramento_To, AV54ddo_Contrato_NumeroTitleControlIdToReplace, AV58ddo_Contratada_PessoaNomTitleControlIdToReplace, AV62ddo_Contratada_PessoaCNPJTitleControlIdToReplace, AV68ddo_ContratoGarantia_DataPagtoGarantiaTitleControlIdToReplace, AV72ddo_ContratoGarantia_PercentualTitleControlIdToReplace, AV78ddo_ContratoGarantia_DataCaucaoTitleControlIdToReplace, AV82ddo_ContratoGarantia_ValorGarantiaTitleControlIdToReplace, AV88ddo_ContratoGarantia_DataEncerramentoTitleControlIdToReplace, AV92ddo_ContratoGarantia_ValorEncerramentoTitleControlIdToReplace, AV157Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A101ContratoGarantia_Codigo, A74Contrato_Codigo, A39Contratada_Codigo) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         AV114WWContratoGarantiaDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV115WWContratoGarantiaDS_2_Contratogarantia_datapagtogarantia1 = AV16ContratoGarantia_DataPagtoGarantia1;
         AV116WWContratoGarantiaDS_3_Contratogarantia_datapagtogarantia_to1 = AV17ContratoGarantia_DataPagtoGarantia_To1;
         AV117WWContratoGarantiaDS_4_Contratogarantia_datacaucao1 = AV37ContratoGarantia_DataCaucao1;
         AV118WWContratoGarantiaDS_5_Contratogarantia_datacaucao_to1 = AV38ContratoGarantia_DataCaucao_To1;
         AV119WWContratoGarantiaDS_6_Contratogarantia_dataencerramento1 = AV39ContratoGarantia_DataEncerramento1;
         AV120WWContratoGarantiaDS_7_Contratogarantia_dataencerramento_to1 = AV40ContratoGarantia_DataEncerramento_To1;
         AV121WWContratoGarantiaDS_8_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV122WWContratoGarantiaDS_9_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV123WWContratoGarantiaDS_10_Contratogarantia_datapagtogarantia2 = AV20ContratoGarantia_DataPagtoGarantia2;
         AV124WWContratoGarantiaDS_11_Contratogarantia_datapagtogarantia_to2 = AV21ContratoGarantia_DataPagtoGarantia_To2;
         AV125WWContratoGarantiaDS_12_Contratogarantia_datacaucao2 = AV41ContratoGarantia_DataCaucao2;
         AV126WWContratoGarantiaDS_13_Contratogarantia_datacaucao_to2 = AV42ContratoGarantia_DataCaucao_To2;
         AV127WWContratoGarantiaDS_14_Contratogarantia_dataencerramento2 = AV43ContratoGarantia_DataEncerramento2;
         AV128WWContratoGarantiaDS_15_Contratogarantia_dataencerramento_to2 = AV44ContratoGarantia_DataEncerramento_To2;
         AV129WWContratoGarantiaDS_16_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV130WWContratoGarantiaDS_17_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV131WWContratoGarantiaDS_18_Contratogarantia_datapagtogarantia3 = AV24ContratoGarantia_DataPagtoGarantia3;
         AV132WWContratoGarantiaDS_19_Contratogarantia_datapagtogarantia_to3 = AV25ContratoGarantia_DataPagtoGarantia_To3;
         AV133WWContratoGarantiaDS_20_Contratogarantia_datacaucao3 = AV45ContratoGarantia_DataCaucao3;
         AV134WWContratoGarantiaDS_21_Contratogarantia_datacaucao_to3 = AV46ContratoGarantia_DataCaucao_To3;
         AV135WWContratoGarantiaDS_22_Contratogarantia_dataencerramento3 = AV47ContratoGarantia_DataEncerramento3;
         AV136WWContratoGarantiaDS_23_Contratogarantia_dataencerramento_to3 = AV48ContratoGarantia_DataEncerramento_To3;
         AV137WWContratoGarantiaDS_24_Tfcontrato_numero = AV52TFContrato_Numero;
         AV138WWContratoGarantiaDS_25_Tfcontrato_numero_sel = AV53TFContrato_Numero_Sel;
         AV139WWContratoGarantiaDS_26_Tfcontratada_pessoanom = AV56TFContratada_PessoaNom;
         AV140WWContratoGarantiaDS_27_Tfcontratada_pessoanom_sel = AV57TFContratada_PessoaNom_Sel;
         AV141WWContratoGarantiaDS_28_Tfcontratada_pessoacnpj = AV60TFContratada_PessoaCNPJ;
         AV142WWContratoGarantiaDS_29_Tfcontratada_pessoacnpj_sel = AV61TFContratada_PessoaCNPJ_Sel;
         AV143WWContratoGarantiaDS_30_Tfcontratogarantia_datapagtogarantia = AV64TFContratoGarantia_DataPagtoGarantia;
         AV144WWContratoGarantiaDS_31_Tfcontratogarantia_datapagtogarantia_to = AV65TFContratoGarantia_DataPagtoGarantia_To;
         AV145WWContratoGarantiaDS_32_Tfcontratogarantia_percentual = AV70TFContratoGarantia_Percentual;
         AV146WWContratoGarantiaDS_33_Tfcontratogarantia_percentual_to = AV71TFContratoGarantia_Percentual_To;
         AV147WWContratoGarantiaDS_34_Tfcontratogarantia_datacaucao = AV74TFContratoGarantia_DataCaucao;
         AV148WWContratoGarantiaDS_35_Tfcontratogarantia_datacaucao_to = AV75TFContratoGarantia_DataCaucao_To;
         AV149WWContratoGarantiaDS_36_Tfcontratogarantia_valorgarantia = AV80TFContratoGarantia_ValorGarantia;
         AV150WWContratoGarantiaDS_37_Tfcontratogarantia_valorgarantia_to = AV81TFContratoGarantia_ValorGarantia_To;
         AV151WWContratoGarantiaDS_38_Tfcontratogarantia_dataencerramento = AV84TFContratoGarantia_DataEncerramento;
         AV152WWContratoGarantiaDS_39_Tfcontratogarantia_dataencerramento_to = AV85TFContratoGarantia_DataEncerramento_To;
         AV153WWContratoGarantiaDS_40_Tfcontratogarantia_valorencerramento = AV90TFContratoGarantia_ValorEncerramento;
         AV154WWContratoGarantiaDS_41_Tfcontratogarantia_valorencerramento_to = AV91TFContratoGarantia_ValorEncerramento_To;
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16ContratoGarantia_DataPagtoGarantia1, AV17ContratoGarantia_DataPagtoGarantia_To1, AV37ContratoGarantia_DataCaucao1, AV38ContratoGarantia_DataCaucao_To1, AV39ContratoGarantia_DataEncerramento1, AV40ContratoGarantia_DataEncerramento_To1, AV19DynamicFiltersSelector2, AV20ContratoGarantia_DataPagtoGarantia2, AV21ContratoGarantia_DataPagtoGarantia_To2, AV41ContratoGarantia_DataCaucao2, AV42ContratoGarantia_DataCaucao_To2, AV43ContratoGarantia_DataEncerramento2, AV44ContratoGarantia_DataEncerramento_To2, AV23DynamicFiltersSelector3, AV24ContratoGarantia_DataPagtoGarantia3, AV25ContratoGarantia_DataPagtoGarantia_To3, AV45ContratoGarantia_DataCaucao3, AV46ContratoGarantia_DataCaucao_To3, AV47ContratoGarantia_DataEncerramento3, AV48ContratoGarantia_DataEncerramento_To3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV52TFContrato_Numero, AV53TFContrato_Numero_Sel, AV56TFContratada_PessoaNom, AV57TFContratada_PessoaNom_Sel, AV60TFContratada_PessoaCNPJ, AV61TFContratada_PessoaCNPJ_Sel, AV64TFContratoGarantia_DataPagtoGarantia, AV65TFContratoGarantia_DataPagtoGarantia_To, AV70TFContratoGarantia_Percentual, AV71TFContratoGarantia_Percentual_To, AV74TFContratoGarantia_DataCaucao, AV75TFContratoGarantia_DataCaucao_To, AV80TFContratoGarantia_ValorGarantia, AV81TFContratoGarantia_ValorGarantia_To, AV84TFContratoGarantia_DataEncerramento, AV85TFContratoGarantia_DataEncerramento_To, AV90TFContratoGarantia_ValorEncerramento, AV91TFContratoGarantia_ValorEncerramento_To, AV54ddo_Contrato_NumeroTitleControlIdToReplace, AV58ddo_Contratada_PessoaNomTitleControlIdToReplace, AV62ddo_Contratada_PessoaCNPJTitleControlIdToReplace, AV68ddo_ContratoGarantia_DataPagtoGarantiaTitleControlIdToReplace, AV72ddo_ContratoGarantia_PercentualTitleControlIdToReplace, AV78ddo_ContratoGarantia_DataCaucaoTitleControlIdToReplace, AV82ddo_ContratoGarantia_ValorGarantiaTitleControlIdToReplace, AV88ddo_ContratoGarantia_DataEncerramentoTitleControlIdToReplace, AV92ddo_ContratoGarantia_ValorEncerramentoTitleControlIdToReplace, AV157Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A101ContratoGarantia_Codigo, A74Contrato_Codigo, A39Contratada_Codigo) ;
         }
         return (int)(0) ;
      }

      protected void STRUP6H0( )
      {
         /* Before Start, stand alone formulas. */
         AV157Pgmname = "WWContratoGarantia";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E326H2 */
         E326H2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV93DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATO_NUMEROTITLEFILTERDATA"), AV51Contrato_NumeroTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATADA_PESSOANOMTITLEFILTERDATA"), AV55Contratada_PessoaNomTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATADA_PESSOACNPJTITLEFILTERDATA"), AV59Contratada_PessoaCNPJTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATOGARANTIA_DATAPAGTOGARANTIATITLEFILTERDATA"), AV63ContratoGarantia_DataPagtoGarantiaTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATOGARANTIA_PERCENTUALTITLEFILTERDATA"), AV69ContratoGarantia_PercentualTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATOGARANTIA_DATACAUCAOTITLEFILTERDATA"), AV73ContratoGarantia_DataCaucaoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATOGARANTIA_VALORGARANTIATITLEFILTERDATA"), AV79ContratoGarantia_ValorGarantiaTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATOGARANTIA_DATAENCERRAMENTOTITLEFILTERDATA"), AV83ContratoGarantia_DataEncerramentoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATOGARANTIA_VALORENCERRAMENTOTITLEFILTERDATA"), AV89ContratoGarantia_ValorEncerramentoTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV13OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV15DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( context.localUtil.VCDateTime( cgiGet( edtavContratogarantia_datapagtogarantia1_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contrato Garantia_Data Pagto Garantia1"}), 1, "vCONTRATOGARANTIA_DATAPAGTOGARANTIA1");
               GX_FocusControl = edtavContratogarantia_datapagtogarantia1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV16ContratoGarantia_DataPagtoGarantia1 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16ContratoGarantia_DataPagtoGarantia1", context.localUtil.Format(AV16ContratoGarantia_DataPagtoGarantia1, "99/99/99"));
            }
            else
            {
               AV16ContratoGarantia_DataPagtoGarantia1 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContratogarantia_datapagtogarantia1_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16ContratoGarantia_DataPagtoGarantia1", context.localUtil.Format(AV16ContratoGarantia_DataPagtoGarantia1, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContratogarantia_datapagtogarantia_to1_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contrato Garantia_Data Pagto Garantia_To1"}), 1, "vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO1");
               GX_FocusControl = edtavContratogarantia_datapagtogarantia_to1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV17ContratoGarantia_DataPagtoGarantia_To1 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContratoGarantia_DataPagtoGarantia_To1", context.localUtil.Format(AV17ContratoGarantia_DataPagtoGarantia_To1, "99/99/99"));
            }
            else
            {
               AV17ContratoGarantia_DataPagtoGarantia_To1 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContratogarantia_datapagtogarantia_to1_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContratoGarantia_DataPagtoGarantia_To1", context.localUtil.Format(AV17ContratoGarantia_DataPagtoGarantia_To1, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContratogarantia_datacaucao1_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contrato Garantia_Data Caucao1"}), 1, "vCONTRATOGARANTIA_DATACAUCAO1");
               GX_FocusControl = edtavContratogarantia_datacaucao1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV37ContratoGarantia_DataCaucao1 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37ContratoGarantia_DataCaucao1", context.localUtil.Format(AV37ContratoGarantia_DataCaucao1, "99/99/99"));
            }
            else
            {
               AV37ContratoGarantia_DataCaucao1 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContratogarantia_datacaucao1_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37ContratoGarantia_DataCaucao1", context.localUtil.Format(AV37ContratoGarantia_DataCaucao1, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContratogarantia_datacaucao_to1_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contrato Garantia_Data Caucao_To1"}), 1, "vCONTRATOGARANTIA_DATACAUCAO_TO1");
               GX_FocusControl = edtavContratogarantia_datacaucao_to1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV38ContratoGarantia_DataCaucao_To1 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38ContratoGarantia_DataCaucao_To1", context.localUtil.Format(AV38ContratoGarantia_DataCaucao_To1, "99/99/99"));
            }
            else
            {
               AV38ContratoGarantia_DataCaucao_To1 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContratogarantia_datacaucao_to1_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38ContratoGarantia_DataCaucao_To1", context.localUtil.Format(AV38ContratoGarantia_DataCaucao_To1, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContratogarantia_dataencerramento1_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contrato Garantia_Data Encerramento1"}), 1, "vCONTRATOGARANTIA_DATAENCERRAMENTO1");
               GX_FocusControl = edtavContratogarantia_dataencerramento1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV39ContratoGarantia_DataEncerramento1 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39ContratoGarantia_DataEncerramento1", context.localUtil.Format(AV39ContratoGarantia_DataEncerramento1, "99/99/99"));
            }
            else
            {
               AV39ContratoGarantia_DataEncerramento1 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContratogarantia_dataencerramento1_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39ContratoGarantia_DataEncerramento1", context.localUtil.Format(AV39ContratoGarantia_DataEncerramento1, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContratogarantia_dataencerramento_to1_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contrato Garantia_Data Encerramento_To1"}), 1, "vCONTRATOGARANTIA_DATAENCERRAMENTO_TO1");
               GX_FocusControl = edtavContratogarantia_dataencerramento_to1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV40ContratoGarantia_DataEncerramento_To1 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40ContratoGarantia_DataEncerramento_To1", context.localUtil.Format(AV40ContratoGarantia_DataEncerramento_To1, "99/99/99"));
            }
            else
            {
               AV40ContratoGarantia_DataEncerramento_To1 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContratogarantia_dataencerramento_to1_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40ContratoGarantia_DataEncerramento_To1", context.localUtil.Format(AV40ContratoGarantia_DataEncerramento_To1, "99/99/99"));
            }
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV19DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
            if ( context.localUtil.VCDateTime( cgiGet( edtavContratogarantia_datapagtogarantia2_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contrato Garantia_Data Pagto Garantia2"}), 1, "vCONTRATOGARANTIA_DATAPAGTOGARANTIA2");
               GX_FocusControl = edtavContratogarantia_datapagtogarantia2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV20ContratoGarantia_DataPagtoGarantia2 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20ContratoGarantia_DataPagtoGarantia2", context.localUtil.Format(AV20ContratoGarantia_DataPagtoGarantia2, "99/99/99"));
            }
            else
            {
               AV20ContratoGarantia_DataPagtoGarantia2 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContratogarantia_datapagtogarantia2_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20ContratoGarantia_DataPagtoGarantia2", context.localUtil.Format(AV20ContratoGarantia_DataPagtoGarantia2, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContratogarantia_datapagtogarantia_to2_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contrato Garantia_Data Pagto Garantia_To2"}), 1, "vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO2");
               GX_FocusControl = edtavContratogarantia_datapagtogarantia_to2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV21ContratoGarantia_DataPagtoGarantia_To2 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ContratoGarantia_DataPagtoGarantia_To2", context.localUtil.Format(AV21ContratoGarantia_DataPagtoGarantia_To2, "99/99/99"));
            }
            else
            {
               AV21ContratoGarantia_DataPagtoGarantia_To2 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContratogarantia_datapagtogarantia_to2_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ContratoGarantia_DataPagtoGarantia_To2", context.localUtil.Format(AV21ContratoGarantia_DataPagtoGarantia_To2, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContratogarantia_datacaucao2_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contrato Garantia_Data Caucao2"}), 1, "vCONTRATOGARANTIA_DATACAUCAO2");
               GX_FocusControl = edtavContratogarantia_datacaucao2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV41ContratoGarantia_DataCaucao2 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41ContratoGarantia_DataCaucao2", context.localUtil.Format(AV41ContratoGarantia_DataCaucao2, "99/99/99"));
            }
            else
            {
               AV41ContratoGarantia_DataCaucao2 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContratogarantia_datacaucao2_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41ContratoGarantia_DataCaucao2", context.localUtil.Format(AV41ContratoGarantia_DataCaucao2, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContratogarantia_datacaucao_to2_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contrato Garantia_Data Caucao_To2"}), 1, "vCONTRATOGARANTIA_DATACAUCAO_TO2");
               GX_FocusControl = edtavContratogarantia_datacaucao_to2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV42ContratoGarantia_DataCaucao_To2 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42ContratoGarantia_DataCaucao_To2", context.localUtil.Format(AV42ContratoGarantia_DataCaucao_To2, "99/99/99"));
            }
            else
            {
               AV42ContratoGarantia_DataCaucao_To2 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContratogarantia_datacaucao_to2_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42ContratoGarantia_DataCaucao_To2", context.localUtil.Format(AV42ContratoGarantia_DataCaucao_To2, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContratogarantia_dataencerramento2_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contrato Garantia_Data Encerramento2"}), 1, "vCONTRATOGARANTIA_DATAENCERRAMENTO2");
               GX_FocusControl = edtavContratogarantia_dataencerramento2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV43ContratoGarantia_DataEncerramento2 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43ContratoGarantia_DataEncerramento2", context.localUtil.Format(AV43ContratoGarantia_DataEncerramento2, "99/99/99"));
            }
            else
            {
               AV43ContratoGarantia_DataEncerramento2 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContratogarantia_dataencerramento2_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43ContratoGarantia_DataEncerramento2", context.localUtil.Format(AV43ContratoGarantia_DataEncerramento2, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContratogarantia_dataencerramento_to2_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contrato Garantia_Data Encerramento_To2"}), 1, "vCONTRATOGARANTIA_DATAENCERRAMENTO_TO2");
               GX_FocusControl = edtavContratogarantia_dataencerramento_to2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV44ContratoGarantia_DataEncerramento_To2 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44ContratoGarantia_DataEncerramento_To2", context.localUtil.Format(AV44ContratoGarantia_DataEncerramento_To2, "99/99/99"));
            }
            else
            {
               AV44ContratoGarantia_DataEncerramento_To2 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContratogarantia_dataencerramento_to2_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44ContratoGarantia_DataEncerramento_To2", context.localUtil.Format(AV44ContratoGarantia_DataEncerramento_To2, "99/99/99"));
            }
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV23DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
            if ( context.localUtil.VCDateTime( cgiGet( edtavContratogarantia_datapagtogarantia3_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contrato Garantia_Data Pagto Garantia3"}), 1, "vCONTRATOGARANTIA_DATAPAGTOGARANTIA3");
               GX_FocusControl = edtavContratogarantia_datapagtogarantia3_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV24ContratoGarantia_DataPagtoGarantia3 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24ContratoGarantia_DataPagtoGarantia3", context.localUtil.Format(AV24ContratoGarantia_DataPagtoGarantia3, "99/99/99"));
            }
            else
            {
               AV24ContratoGarantia_DataPagtoGarantia3 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContratogarantia_datapagtogarantia3_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24ContratoGarantia_DataPagtoGarantia3", context.localUtil.Format(AV24ContratoGarantia_DataPagtoGarantia3, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContratogarantia_datapagtogarantia_to3_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contrato Garantia_Data Pagto Garantia_To3"}), 1, "vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO3");
               GX_FocusControl = edtavContratogarantia_datapagtogarantia_to3_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV25ContratoGarantia_DataPagtoGarantia_To3 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ContratoGarantia_DataPagtoGarantia_To3", context.localUtil.Format(AV25ContratoGarantia_DataPagtoGarantia_To3, "99/99/99"));
            }
            else
            {
               AV25ContratoGarantia_DataPagtoGarantia_To3 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContratogarantia_datapagtogarantia_to3_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ContratoGarantia_DataPagtoGarantia_To3", context.localUtil.Format(AV25ContratoGarantia_DataPagtoGarantia_To3, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContratogarantia_datacaucao3_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contrato Garantia_Data Caucao3"}), 1, "vCONTRATOGARANTIA_DATACAUCAO3");
               GX_FocusControl = edtavContratogarantia_datacaucao3_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV45ContratoGarantia_DataCaucao3 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45ContratoGarantia_DataCaucao3", context.localUtil.Format(AV45ContratoGarantia_DataCaucao3, "99/99/99"));
            }
            else
            {
               AV45ContratoGarantia_DataCaucao3 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContratogarantia_datacaucao3_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45ContratoGarantia_DataCaucao3", context.localUtil.Format(AV45ContratoGarantia_DataCaucao3, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContratogarantia_datacaucao_to3_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contrato Garantia_Data Caucao_To3"}), 1, "vCONTRATOGARANTIA_DATACAUCAO_TO3");
               GX_FocusControl = edtavContratogarantia_datacaucao_to3_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV46ContratoGarantia_DataCaucao_To3 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46ContratoGarantia_DataCaucao_To3", context.localUtil.Format(AV46ContratoGarantia_DataCaucao_To3, "99/99/99"));
            }
            else
            {
               AV46ContratoGarantia_DataCaucao_To3 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContratogarantia_datacaucao_to3_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46ContratoGarantia_DataCaucao_To3", context.localUtil.Format(AV46ContratoGarantia_DataCaucao_To3, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContratogarantia_dataencerramento3_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contrato Garantia_Data Encerramento3"}), 1, "vCONTRATOGARANTIA_DATAENCERRAMENTO3");
               GX_FocusControl = edtavContratogarantia_dataencerramento3_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV47ContratoGarantia_DataEncerramento3 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47ContratoGarantia_DataEncerramento3", context.localUtil.Format(AV47ContratoGarantia_DataEncerramento3, "99/99/99"));
            }
            else
            {
               AV47ContratoGarantia_DataEncerramento3 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContratogarantia_dataencerramento3_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47ContratoGarantia_DataEncerramento3", context.localUtil.Format(AV47ContratoGarantia_DataEncerramento3, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavContratogarantia_dataencerramento_to3_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Contrato Garantia_Data Encerramento_To3"}), 1, "vCONTRATOGARANTIA_DATAENCERRAMENTO_TO3");
               GX_FocusControl = edtavContratogarantia_dataencerramento_to3_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV48ContratoGarantia_DataEncerramento_To3 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48ContratoGarantia_DataEncerramento_To3", context.localUtil.Format(AV48ContratoGarantia_DataEncerramento_To3, "99/99/99"));
            }
            else
            {
               AV48ContratoGarantia_DataEncerramento_To3 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContratogarantia_dataencerramento_to3_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48ContratoGarantia_DataEncerramento_To3", context.localUtil.Format(AV48ContratoGarantia_DataEncerramento_To3, "99/99/99"));
            }
            AV18DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
            AV22DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
            AV52TFContrato_Numero = cgiGet( edtavTfcontrato_numero_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52TFContrato_Numero", AV52TFContrato_Numero);
            AV53TFContrato_Numero_Sel = cgiGet( edtavTfcontrato_numero_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53TFContrato_Numero_Sel", AV53TFContrato_Numero_Sel);
            AV56TFContratada_PessoaNom = StringUtil.Upper( cgiGet( edtavTfcontratada_pessoanom_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFContratada_PessoaNom", AV56TFContratada_PessoaNom);
            AV57TFContratada_PessoaNom_Sel = StringUtil.Upper( cgiGet( edtavTfcontratada_pessoanom_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57TFContratada_PessoaNom_Sel", AV57TFContratada_PessoaNom_Sel);
            AV60TFContratada_PessoaCNPJ = cgiGet( edtavTfcontratada_pessoacnpj_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFContratada_PessoaCNPJ", AV60TFContratada_PessoaCNPJ);
            AV61TFContratada_PessoaCNPJ_Sel = cgiGet( edtavTfcontratada_pessoacnpj_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61TFContratada_PessoaCNPJ_Sel", AV61TFContratada_PessoaCNPJ_Sel);
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfcontratogarantia_datapagtogarantia_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"TFContrato Garantia_Data Pagto Garantia"}), 1, "vTFCONTRATOGARANTIA_DATAPAGTOGARANTIA");
               GX_FocusControl = edtavTfcontratogarantia_datapagtogarantia_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV64TFContratoGarantia_DataPagtoGarantia = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64TFContratoGarantia_DataPagtoGarantia", context.localUtil.Format(AV64TFContratoGarantia_DataPagtoGarantia, "99/99/99"));
            }
            else
            {
               AV64TFContratoGarantia_DataPagtoGarantia = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavTfcontratogarantia_datapagtogarantia_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64TFContratoGarantia_DataPagtoGarantia", context.localUtil.Format(AV64TFContratoGarantia_DataPagtoGarantia, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfcontratogarantia_datapagtogarantia_to_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"TFContrato Garantia_Data Pagto Garantia_To"}), 1, "vTFCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO");
               GX_FocusControl = edtavTfcontratogarantia_datapagtogarantia_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV65TFContratoGarantia_DataPagtoGarantia_To = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65TFContratoGarantia_DataPagtoGarantia_To", context.localUtil.Format(AV65TFContratoGarantia_DataPagtoGarantia_To, "99/99/99"));
            }
            else
            {
               AV65TFContratoGarantia_DataPagtoGarantia_To = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavTfcontratogarantia_datapagtogarantia_to_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65TFContratoGarantia_DataPagtoGarantia_To", context.localUtil.Format(AV65TFContratoGarantia_DataPagtoGarantia_To, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_contratogarantia_datapagtogarantiaauxdate_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Contrato Garantia_Data Pagto Garantia Aux Date"}), 1, "vDDO_CONTRATOGARANTIA_DATAPAGTOGARANTIAAUXDATE");
               GX_FocusControl = edtavDdo_contratogarantia_datapagtogarantiaauxdate_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV66DDO_ContratoGarantia_DataPagtoGarantiaAuxDate = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66DDO_ContratoGarantia_DataPagtoGarantiaAuxDate", context.localUtil.Format(AV66DDO_ContratoGarantia_DataPagtoGarantiaAuxDate, "99/99/99"));
            }
            else
            {
               AV66DDO_ContratoGarantia_DataPagtoGarantiaAuxDate = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_contratogarantia_datapagtogarantiaauxdate_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66DDO_ContratoGarantia_DataPagtoGarantiaAuxDate", context.localUtil.Format(AV66DDO_ContratoGarantia_DataPagtoGarantiaAuxDate, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_contratogarantia_datapagtogarantiaauxdateto_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Contrato Garantia_Data Pagto Garantia Aux Date To"}), 1, "vDDO_CONTRATOGARANTIA_DATAPAGTOGARANTIAAUXDATETO");
               GX_FocusControl = edtavDdo_contratogarantia_datapagtogarantiaauxdateto_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV67DDO_ContratoGarantia_DataPagtoGarantiaAuxDateTo = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67DDO_ContratoGarantia_DataPagtoGarantiaAuxDateTo", context.localUtil.Format(AV67DDO_ContratoGarantia_DataPagtoGarantiaAuxDateTo, "99/99/99"));
            }
            else
            {
               AV67DDO_ContratoGarantia_DataPagtoGarantiaAuxDateTo = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_contratogarantia_datapagtogarantiaauxdateto_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67DDO_ContratoGarantia_DataPagtoGarantiaAuxDateTo", context.localUtil.Format(AV67DDO_ContratoGarantia_DataPagtoGarantiaAuxDateTo, "99/99/99"));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratogarantia_percentual_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratogarantia_percentual_Internalname), ",", ".") > 999.99m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOGARANTIA_PERCENTUAL");
               GX_FocusControl = edtavTfcontratogarantia_percentual_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV70TFContratoGarantia_Percentual = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70TFContratoGarantia_Percentual", StringUtil.LTrim( StringUtil.Str( AV70TFContratoGarantia_Percentual, 6, 2)));
            }
            else
            {
               AV70TFContratoGarantia_Percentual = context.localUtil.CToN( cgiGet( edtavTfcontratogarantia_percentual_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70TFContratoGarantia_Percentual", StringUtil.LTrim( StringUtil.Str( AV70TFContratoGarantia_Percentual, 6, 2)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratogarantia_percentual_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratogarantia_percentual_to_Internalname), ",", ".") > 999.99m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOGARANTIA_PERCENTUAL_TO");
               GX_FocusControl = edtavTfcontratogarantia_percentual_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV71TFContratoGarantia_Percentual_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71TFContratoGarantia_Percentual_To", StringUtil.LTrim( StringUtil.Str( AV71TFContratoGarantia_Percentual_To, 6, 2)));
            }
            else
            {
               AV71TFContratoGarantia_Percentual_To = context.localUtil.CToN( cgiGet( edtavTfcontratogarantia_percentual_to_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71TFContratoGarantia_Percentual_To", StringUtil.LTrim( StringUtil.Str( AV71TFContratoGarantia_Percentual_To, 6, 2)));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfcontratogarantia_datacaucao_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"TFContrato Garantia_Data Caucao"}), 1, "vTFCONTRATOGARANTIA_DATACAUCAO");
               GX_FocusControl = edtavTfcontratogarantia_datacaucao_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV74TFContratoGarantia_DataCaucao = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74TFContratoGarantia_DataCaucao", context.localUtil.Format(AV74TFContratoGarantia_DataCaucao, "99/99/99"));
            }
            else
            {
               AV74TFContratoGarantia_DataCaucao = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavTfcontratogarantia_datacaucao_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74TFContratoGarantia_DataCaucao", context.localUtil.Format(AV74TFContratoGarantia_DataCaucao, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfcontratogarantia_datacaucao_to_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"TFContrato Garantia_Data Caucao_To"}), 1, "vTFCONTRATOGARANTIA_DATACAUCAO_TO");
               GX_FocusControl = edtavTfcontratogarantia_datacaucao_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV75TFContratoGarantia_DataCaucao_To = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75TFContratoGarantia_DataCaucao_To", context.localUtil.Format(AV75TFContratoGarantia_DataCaucao_To, "99/99/99"));
            }
            else
            {
               AV75TFContratoGarantia_DataCaucao_To = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavTfcontratogarantia_datacaucao_to_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75TFContratoGarantia_DataCaucao_To", context.localUtil.Format(AV75TFContratoGarantia_DataCaucao_To, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_contratogarantia_datacaucaoauxdate_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Contrato Garantia_Data Caucao Aux Date"}), 1, "vDDO_CONTRATOGARANTIA_DATACAUCAOAUXDATE");
               GX_FocusControl = edtavDdo_contratogarantia_datacaucaoauxdate_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV76DDO_ContratoGarantia_DataCaucaoAuxDate = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76DDO_ContratoGarantia_DataCaucaoAuxDate", context.localUtil.Format(AV76DDO_ContratoGarantia_DataCaucaoAuxDate, "99/99/99"));
            }
            else
            {
               AV76DDO_ContratoGarantia_DataCaucaoAuxDate = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_contratogarantia_datacaucaoauxdate_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76DDO_ContratoGarantia_DataCaucaoAuxDate", context.localUtil.Format(AV76DDO_ContratoGarantia_DataCaucaoAuxDate, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_contratogarantia_datacaucaoauxdateto_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Contrato Garantia_Data Caucao Aux Date To"}), 1, "vDDO_CONTRATOGARANTIA_DATACAUCAOAUXDATETO");
               GX_FocusControl = edtavDdo_contratogarantia_datacaucaoauxdateto_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV77DDO_ContratoGarantia_DataCaucaoAuxDateTo = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77DDO_ContratoGarantia_DataCaucaoAuxDateTo", context.localUtil.Format(AV77DDO_ContratoGarantia_DataCaucaoAuxDateTo, "99/99/99"));
            }
            else
            {
               AV77DDO_ContratoGarantia_DataCaucaoAuxDateTo = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_contratogarantia_datacaucaoauxdateto_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77DDO_ContratoGarantia_DataCaucaoAuxDateTo", context.localUtil.Format(AV77DDO_ContratoGarantia_DataCaucaoAuxDateTo, "99/99/99"));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratogarantia_valorgarantia_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratogarantia_valorgarantia_Internalname), ",", ".") > 999999999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOGARANTIA_VALORGARANTIA");
               GX_FocusControl = edtavTfcontratogarantia_valorgarantia_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV80TFContratoGarantia_ValorGarantia = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80TFContratoGarantia_ValorGarantia", StringUtil.LTrim( StringUtil.Str( AV80TFContratoGarantia_ValorGarantia, 18, 5)));
            }
            else
            {
               AV80TFContratoGarantia_ValorGarantia = context.localUtil.CToN( cgiGet( edtavTfcontratogarantia_valorgarantia_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80TFContratoGarantia_ValorGarantia", StringUtil.LTrim( StringUtil.Str( AV80TFContratoGarantia_ValorGarantia, 18, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratogarantia_valorgarantia_to_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratogarantia_valorgarantia_to_Internalname), ",", ".") > 999999999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOGARANTIA_VALORGARANTIA_TO");
               GX_FocusControl = edtavTfcontratogarantia_valorgarantia_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV81TFContratoGarantia_ValorGarantia_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81TFContratoGarantia_ValorGarantia_To", StringUtil.LTrim( StringUtil.Str( AV81TFContratoGarantia_ValorGarantia_To, 18, 5)));
            }
            else
            {
               AV81TFContratoGarantia_ValorGarantia_To = context.localUtil.CToN( cgiGet( edtavTfcontratogarantia_valorgarantia_to_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81TFContratoGarantia_ValorGarantia_To", StringUtil.LTrim( StringUtil.Str( AV81TFContratoGarantia_ValorGarantia_To, 18, 5)));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfcontratogarantia_dataencerramento_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"TFContrato Garantia_Data Encerramento"}), 1, "vTFCONTRATOGARANTIA_DATAENCERRAMENTO");
               GX_FocusControl = edtavTfcontratogarantia_dataencerramento_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV84TFContratoGarantia_DataEncerramento = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV84TFContratoGarantia_DataEncerramento", context.localUtil.Format(AV84TFContratoGarantia_DataEncerramento, "99/99/99"));
            }
            else
            {
               AV84TFContratoGarantia_DataEncerramento = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavTfcontratogarantia_dataencerramento_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV84TFContratoGarantia_DataEncerramento", context.localUtil.Format(AV84TFContratoGarantia_DataEncerramento, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfcontratogarantia_dataencerramento_to_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"TFContrato Garantia_Data Encerramento_To"}), 1, "vTFCONTRATOGARANTIA_DATAENCERRAMENTO_TO");
               GX_FocusControl = edtavTfcontratogarantia_dataencerramento_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV85TFContratoGarantia_DataEncerramento_To = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV85TFContratoGarantia_DataEncerramento_To", context.localUtil.Format(AV85TFContratoGarantia_DataEncerramento_To, "99/99/99"));
            }
            else
            {
               AV85TFContratoGarantia_DataEncerramento_To = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavTfcontratogarantia_dataencerramento_to_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV85TFContratoGarantia_DataEncerramento_To", context.localUtil.Format(AV85TFContratoGarantia_DataEncerramento_To, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_contratogarantia_dataencerramentoauxdate_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Contrato Garantia_Data Encerramento Aux Date"}), 1, "vDDO_CONTRATOGARANTIA_DATAENCERRAMENTOAUXDATE");
               GX_FocusControl = edtavDdo_contratogarantia_dataencerramentoauxdate_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV86DDO_ContratoGarantia_DataEncerramentoAuxDate = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV86DDO_ContratoGarantia_DataEncerramentoAuxDate", context.localUtil.Format(AV86DDO_ContratoGarantia_DataEncerramentoAuxDate, "99/99/99"));
            }
            else
            {
               AV86DDO_ContratoGarantia_DataEncerramentoAuxDate = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_contratogarantia_dataencerramentoauxdate_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV86DDO_ContratoGarantia_DataEncerramentoAuxDate", context.localUtil.Format(AV86DDO_ContratoGarantia_DataEncerramentoAuxDate, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_contratogarantia_dataencerramentoauxdateto_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Contrato Garantia_Data Encerramento Aux Date To"}), 1, "vDDO_CONTRATOGARANTIA_DATAENCERRAMENTOAUXDATETO");
               GX_FocusControl = edtavDdo_contratogarantia_dataencerramentoauxdateto_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV87DDO_ContratoGarantia_DataEncerramentoAuxDateTo = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV87DDO_ContratoGarantia_DataEncerramentoAuxDateTo", context.localUtil.Format(AV87DDO_ContratoGarantia_DataEncerramentoAuxDateTo, "99/99/99"));
            }
            else
            {
               AV87DDO_ContratoGarantia_DataEncerramentoAuxDateTo = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_contratogarantia_dataencerramentoauxdateto_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV87DDO_ContratoGarantia_DataEncerramentoAuxDateTo", context.localUtil.Format(AV87DDO_ContratoGarantia_DataEncerramentoAuxDateTo, "99/99/99"));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratogarantia_valorencerramento_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratogarantia_valorencerramento_Internalname), ",", ".") > 999999999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOGARANTIA_VALORENCERRAMENTO");
               GX_FocusControl = edtavTfcontratogarantia_valorencerramento_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV90TFContratoGarantia_ValorEncerramento = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV90TFContratoGarantia_ValorEncerramento", StringUtil.LTrim( StringUtil.Str( AV90TFContratoGarantia_ValorEncerramento, 18, 5)));
            }
            else
            {
               AV90TFContratoGarantia_ValorEncerramento = context.localUtil.CToN( cgiGet( edtavTfcontratogarantia_valorencerramento_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV90TFContratoGarantia_ValorEncerramento", StringUtil.LTrim( StringUtil.Str( AV90TFContratoGarantia_ValorEncerramento, 18, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratogarantia_valorencerramento_to_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratogarantia_valorencerramento_to_Internalname), ",", ".") > 999999999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOGARANTIA_VALORENCERRAMENTO_TO");
               GX_FocusControl = edtavTfcontratogarantia_valorencerramento_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV91TFContratoGarantia_ValorEncerramento_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV91TFContratoGarantia_ValorEncerramento_To", StringUtil.LTrim( StringUtil.Str( AV91TFContratoGarantia_ValorEncerramento_To, 18, 5)));
            }
            else
            {
               AV91TFContratoGarantia_ValorEncerramento_To = context.localUtil.CToN( cgiGet( edtavTfcontratogarantia_valorencerramento_to_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV91TFContratoGarantia_ValorEncerramento_To", StringUtil.LTrim( StringUtil.Str( AV91TFContratoGarantia_ValorEncerramento_To, 18, 5)));
            }
            AV54ddo_Contrato_NumeroTitleControlIdToReplace = cgiGet( edtavDdo_contrato_numerotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54ddo_Contrato_NumeroTitleControlIdToReplace", AV54ddo_Contrato_NumeroTitleControlIdToReplace);
            AV58ddo_Contratada_PessoaNomTitleControlIdToReplace = cgiGet( edtavDdo_contratada_pessoanomtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58ddo_Contratada_PessoaNomTitleControlIdToReplace", AV58ddo_Contratada_PessoaNomTitleControlIdToReplace);
            AV62ddo_Contratada_PessoaCNPJTitleControlIdToReplace = cgiGet( edtavDdo_contratada_pessoacnpjtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62ddo_Contratada_PessoaCNPJTitleControlIdToReplace", AV62ddo_Contratada_PessoaCNPJTitleControlIdToReplace);
            AV68ddo_ContratoGarantia_DataPagtoGarantiaTitleControlIdToReplace = cgiGet( edtavDdo_contratogarantia_datapagtogarantiatitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68ddo_ContratoGarantia_DataPagtoGarantiaTitleControlIdToReplace", AV68ddo_ContratoGarantia_DataPagtoGarantiaTitleControlIdToReplace);
            AV72ddo_ContratoGarantia_PercentualTitleControlIdToReplace = cgiGet( edtavDdo_contratogarantia_percentualtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72ddo_ContratoGarantia_PercentualTitleControlIdToReplace", AV72ddo_ContratoGarantia_PercentualTitleControlIdToReplace);
            AV78ddo_ContratoGarantia_DataCaucaoTitleControlIdToReplace = cgiGet( edtavDdo_contratogarantia_datacaucaotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV78ddo_ContratoGarantia_DataCaucaoTitleControlIdToReplace", AV78ddo_ContratoGarantia_DataCaucaoTitleControlIdToReplace);
            AV82ddo_ContratoGarantia_ValorGarantiaTitleControlIdToReplace = cgiGet( edtavDdo_contratogarantia_valorgarantiatitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV82ddo_ContratoGarantia_ValorGarantiaTitleControlIdToReplace", AV82ddo_ContratoGarantia_ValorGarantiaTitleControlIdToReplace);
            AV88ddo_ContratoGarantia_DataEncerramentoTitleControlIdToReplace = cgiGet( edtavDdo_contratogarantia_dataencerramentotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV88ddo_ContratoGarantia_DataEncerramentoTitleControlIdToReplace", AV88ddo_ContratoGarantia_DataEncerramentoTitleControlIdToReplace);
            AV92ddo_ContratoGarantia_ValorEncerramentoTitleControlIdToReplace = cgiGet( edtavDdo_contratogarantia_valorencerramentotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV92ddo_ContratoGarantia_ValorEncerramentoTitleControlIdToReplace", AV92ddo_ContratoGarantia_ValorEncerramentoTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_142 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_142"), ",", "."));
            AV95GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV96GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_contrato_numero_Caption = cgiGet( "DDO_CONTRATO_NUMERO_Caption");
            Ddo_contrato_numero_Tooltip = cgiGet( "DDO_CONTRATO_NUMERO_Tooltip");
            Ddo_contrato_numero_Cls = cgiGet( "DDO_CONTRATO_NUMERO_Cls");
            Ddo_contrato_numero_Filteredtext_set = cgiGet( "DDO_CONTRATO_NUMERO_Filteredtext_set");
            Ddo_contrato_numero_Selectedvalue_set = cgiGet( "DDO_CONTRATO_NUMERO_Selectedvalue_set");
            Ddo_contrato_numero_Dropdownoptionstype = cgiGet( "DDO_CONTRATO_NUMERO_Dropdownoptionstype");
            Ddo_contrato_numero_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATO_NUMERO_Titlecontrolidtoreplace");
            Ddo_contrato_numero_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATO_NUMERO_Includesortasc"));
            Ddo_contrato_numero_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATO_NUMERO_Includesortdsc"));
            Ddo_contrato_numero_Sortedstatus = cgiGet( "DDO_CONTRATO_NUMERO_Sortedstatus");
            Ddo_contrato_numero_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATO_NUMERO_Includefilter"));
            Ddo_contrato_numero_Filtertype = cgiGet( "DDO_CONTRATO_NUMERO_Filtertype");
            Ddo_contrato_numero_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATO_NUMERO_Filterisrange"));
            Ddo_contrato_numero_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATO_NUMERO_Includedatalist"));
            Ddo_contrato_numero_Datalisttype = cgiGet( "DDO_CONTRATO_NUMERO_Datalisttype");
            Ddo_contrato_numero_Datalistproc = cgiGet( "DDO_CONTRATO_NUMERO_Datalistproc");
            Ddo_contrato_numero_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTRATO_NUMERO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contrato_numero_Sortasc = cgiGet( "DDO_CONTRATO_NUMERO_Sortasc");
            Ddo_contrato_numero_Sortdsc = cgiGet( "DDO_CONTRATO_NUMERO_Sortdsc");
            Ddo_contrato_numero_Loadingdata = cgiGet( "DDO_CONTRATO_NUMERO_Loadingdata");
            Ddo_contrato_numero_Cleanfilter = cgiGet( "DDO_CONTRATO_NUMERO_Cleanfilter");
            Ddo_contrato_numero_Noresultsfound = cgiGet( "DDO_CONTRATO_NUMERO_Noresultsfound");
            Ddo_contrato_numero_Searchbuttontext = cgiGet( "DDO_CONTRATO_NUMERO_Searchbuttontext");
            Ddo_contratada_pessoanom_Caption = cgiGet( "DDO_CONTRATADA_PESSOANOM_Caption");
            Ddo_contratada_pessoanom_Tooltip = cgiGet( "DDO_CONTRATADA_PESSOANOM_Tooltip");
            Ddo_contratada_pessoanom_Cls = cgiGet( "DDO_CONTRATADA_PESSOANOM_Cls");
            Ddo_contratada_pessoanom_Filteredtext_set = cgiGet( "DDO_CONTRATADA_PESSOANOM_Filteredtext_set");
            Ddo_contratada_pessoanom_Selectedvalue_set = cgiGet( "DDO_CONTRATADA_PESSOANOM_Selectedvalue_set");
            Ddo_contratada_pessoanom_Dropdownoptionstype = cgiGet( "DDO_CONTRATADA_PESSOANOM_Dropdownoptionstype");
            Ddo_contratada_pessoanom_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATADA_PESSOANOM_Titlecontrolidtoreplace");
            Ddo_contratada_pessoanom_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADA_PESSOANOM_Includesortasc"));
            Ddo_contratada_pessoanom_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADA_PESSOANOM_Includesortdsc"));
            Ddo_contratada_pessoanom_Sortedstatus = cgiGet( "DDO_CONTRATADA_PESSOANOM_Sortedstatus");
            Ddo_contratada_pessoanom_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADA_PESSOANOM_Includefilter"));
            Ddo_contratada_pessoanom_Filtertype = cgiGet( "DDO_CONTRATADA_PESSOANOM_Filtertype");
            Ddo_contratada_pessoanom_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADA_PESSOANOM_Filterisrange"));
            Ddo_contratada_pessoanom_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADA_PESSOANOM_Includedatalist"));
            Ddo_contratada_pessoanom_Datalisttype = cgiGet( "DDO_CONTRATADA_PESSOANOM_Datalisttype");
            Ddo_contratada_pessoanom_Datalistproc = cgiGet( "DDO_CONTRATADA_PESSOANOM_Datalistproc");
            Ddo_contratada_pessoanom_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTRATADA_PESSOANOM_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contratada_pessoanom_Sortasc = cgiGet( "DDO_CONTRATADA_PESSOANOM_Sortasc");
            Ddo_contratada_pessoanom_Sortdsc = cgiGet( "DDO_CONTRATADA_PESSOANOM_Sortdsc");
            Ddo_contratada_pessoanom_Loadingdata = cgiGet( "DDO_CONTRATADA_PESSOANOM_Loadingdata");
            Ddo_contratada_pessoanom_Cleanfilter = cgiGet( "DDO_CONTRATADA_PESSOANOM_Cleanfilter");
            Ddo_contratada_pessoanom_Noresultsfound = cgiGet( "DDO_CONTRATADA_PESSOANOM_Noresultsfound");
            Ddo_contratada_pessoanom_Searchbuttontext = cgiGet( "DDO_CONTRATADA_PESSOANOM_Searchbuttontext");
            Ddo_contratada_pessoacnpj_Caption = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Caption");
            Ddo_contratada_pessoacnpj_Tooltip = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Tooltip");
            Ddo_contratada_pessoacnpj_Cls = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Cls");
            Ddo_contratada_pessoacnpj_Filteredtext_set = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Filteredtext_set");
            Ddo_contratada_pessoacnpj_Selectedvalue_set = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Selectedvalue_set");
            Ddo_contratada_pessoacnpj_Dropdownoptionstype = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Dropdownoptionstype");
            Ddo_contratada_pessoacnpj_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Titlecontrolidtoreplace");
            Ddo_contratada_pessoacnpj_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Includesortasc"));
            Ddo_contratada_pessoacnpj_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Includesortdsc"));
            Ddo_contratada_pessoacnpj_Sortedstatus = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Sortedstatus");
            Ddo_contratada_pessoacnpj_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Includefilter"));
            Ddo_contratada_pessoacnpj_Filtertype = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Filtertype");
            Ddo_contratada_pessoacnpj_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Filterisrange"));
            Ddo_contratada_pessoacnpj_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Includedatalist"));
            Ddo_contratada_pessoacnpj_Datalisttype = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Datalisttype");
            Ddo_contratada_pessoacnpj_Datalistproc = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Datalistproc");
            Ddo_contratada_pessoacnpj_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contratada_pessoacnpj_Sortasc = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Sortasc");
            Ddo_contratada_pessoacnpj_Sortdsc = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Sortdsc");
            Ddo_contratada_pessoacnpj_Loadingdata = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Loadingdata");
            Ddo_contratada_pessoacnpj_Cleanfilter = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Cleanfilter");
            Ddo_contratada_pessoacnpj_Noresultsfound = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Noresultsfound");
            Ddo_contratada_pessoacnpj_Searchbuttontext = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Searchbuttontext");
            Ddo_contratogarantia_datapagtogarantia_Caption = cgiGet( "DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA_Caption");
            Ddo_contratogarantia_datapagtogarantia_Tooltip = cgiGet( "DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA_Tooltip");
            Ddo_contratogarantia_datapagtogarantia_Cls = cgiGet( "DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA_Cls");
            Ddo_contratogarantia_datapagtogarantia_Filteredtext_set = cgiGet( "DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA_Filteredtext_set");
            Ddo_contratogarantia_datapagtogarantia_Filteredtextto_set = cgiGet( "DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA_Filteredtextto_set");
            Ddo_contratogarantia_datapagtogarantia_Dropdownoptionstype = cgiGet( "DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA_Dropdownoptionstype");
            Ddo_contratogarantia_datapagtogarantia_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA_Titlecontrolidtoreplace");
            Ddo_contratogarantia_datapagtogarantia_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA_Includesortasc"));
            Ddo_contratogarantia_datapagtogarantia_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA_Includesortdsc"));
            Ddo_contratogarantia_datapagtogarantia_Sortedstatus = cgiGet( "DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA_Sortedstatus");
            Ddo_contratogarantia_datapagtogarantia_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA_Includefilter"));
            Ddo_contratogarantia_datapagtogarantia_Filtertype = cgiGet( "DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA_Filtertype");
            Ddo_contratogarantia_datapagtogarantia_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA_Filterisrange"));
            Ddo_contratogarantia_datapagtogarantia_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA_Includedatalist"));
            Ddo_contratogarantia_datapagtogarantia_Sortasc = cgiGet( "DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA_Sortasc");
            Ddo_contratogarantia_datapagtogarantia_Sortdsc = cgiGet( "DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA_Sortdsc");
            Ddo_contratogarantia_datapagtogarantia_Cleanfilter = cgiGet( "DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA_Cleanfilter");
            Ddo_contratogarantia_datapagtogarantia_Rangefilterfrom = cgiGet( "DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA_Rangefilterfrom");
            Ddo_contratogarantia_datapagtogarantia_Rangefilterto = cgiGet( "DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA_Rangefilterto");
            Ddo_contratogarantia_datapagtogarantia_Searchbuttontext = cgiGet( "DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA_Searchbuttontext");
            Ddo_contratogarantia_percentual_Caption = cgiGet( "DDO_CONTRATOGARANTIA_PERCENTUAL_Caption");
            Ddo_contratogarantia_percentual_Tooltip = cgiGet( "DDO_CONTRATOGARANTIA_PERCENTUAL_Tooltip");
            Ddo_contratogarantia_percentual_Cls = cgiGet( "DDO_CONTRATOGARANTIA_PERCENTUAL_Cls");
            Ddo_contratogarantia_percentual_Filteredtext_set = cgiGet( "DDO_CONTRATOGARANTIA_PERCENTUAL_Filteredtext_set");
            Ddo_contratogarantia_percentual_Filteredtextto_set = cgiGet( "DDO_CONTRATOGARANTIA_PERCENTUAL_Filteredtextto_set");
            Ddo_contratogarantia_percentual_Dropdownoptionstype = cgiGet( "DDO_CONTRATOGARANTIA_PERCENTUAL_Dropdownoptionstype");
            Ddo_contratogarantia_percentual_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATOGARANTIA_PERCENTUAL_Titlecontrolidtoreplace");
            Ddo_contratogarantia_percentual_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOGARANTIA_PERCENTUAL_Includesortasc"));
            Ddo_contratogarantia_percentual_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOGARANTIA_PERCENTUAL_Includesortdsc"));
            Ddo_contratogarantia_percentual_Sortedstatus = cgiGet( "DDO_CONTRATOGARANTIA_PERCENTUAL_Sortedstatus");
            Ddo_contratogarantia_percentual_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOGARANTIA_PERCENTUAL_Includefilter"));
            Ddo_contratogarantia_percentual_Filtertype = cgiGet( "DDO_CONTRATOGARANTIA_PERCENTUAL_Filtertype");
            Ddo_contratogarantia_percentual_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOGARANTIA_PERCENTUAL_Filterisrange"));
            Ddo_contratogarantia_percentual_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOGARANTIA_PERCENTUAL_Includedatalist"));
            Ddo_contratogarantia_percentual_Sortasc = cgiGet( "DDO_CONTRATOGARANTIA_PERCENTUAL_Sortasc");
            Ddo_contratogarantia_percentual_Sortdsc = cgiGet( "DDO_CONTRATOGARANTIA_PERCENTUAL_Sortdsc");
            Ddo_contratogarantia_percentual_Cleanfilter = cgiGet( "DDO_CONTRATOGARANTIA_PERCENTUAL_Cleanfilter");
            Ddo_contratogarantia_percentual_Rangefilterfrom = cgiGet( "DDO_CONTRATOGARANTIA_PERCENTUAL_Rangefilterfrom");
            Ddo_contratogarantia_percentual_Rangefilterto = cgiGet( "DDO_CONTRATOGARANTIA_PERCENTUAL_Rangefilterto");
            Ddo_contratogarantia_percentual_Searchbuttontext = cgiGet( "DDO_CONTRATOGARANTIA_PERCENTUAL_Searchbuttontext");
            Ddo_contratogarantia_datacaucao_Caption = cgiGet( "DDO_CONTRATOGARANTIA_DATACAUCAO_Caption");
            Ddo_contratogarantia_datacaucao_Tooltip = cgiGet( "DDO_CONTRATOGARANTIA_DATACAUCAO_Tooltip");
            Ddo_contratogarantia_datacaucao_Cls = cgiGet( "DDO_CONTRATOGARANTIA_DATACAUCAO_Cls");
            Ddo_contratogarantia_datacaucao_Filteredtext_set = cgiGet( "DDO_CONTRATOGARANTIA_DATACAUCAO_Filteredtext_set");
            Ddo_contratogarantia_datacaucao_Filteredtextto_set = cgiGet( "DDO_CONTRATOGARANTIA_DATACAUCAO_Filteredtextto_set");
            Ddo_contratogarantia_datacaucao_Dropdownoptionstype = cgiGet( "DDO_CONTRATOGARANTIA_DATACAUCAO_Dropdownoptionstype");
            Ddo_contratogarantia_datacaucao_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATOGARANTIA_DATACAUCAO_Titlecontrolidtoreplace");
            Ddo_contratogarantia_datacaucao_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOGARANTIA_DATACAUCAO_Includesortasc"));
            Ddo_contratogarantia_datacaucao_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOGARANTIA_DATACAUCAO_Includesortdsc"));
            Ddo_contratogarantia_datacaucao_Sortedstatus = cgiGet( "DDO_CONTRATOGARANTIA_DATACAUCAO_Sortedstatus");
            Ddo_contratogarantia_datacaucao_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOGARANTIA_DATACAUCAO_Includefilter"));
            Ddo_contratogarantia_datacaucao_Filtertype = cgiGet( "DDO_CONTRATOGARANTIA_DATACAUCAO_Filtertype");
            Ddo_contratogarantia_datacaucao_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOGARANTIA_DATACAUCAO_Filterisrange"));
            Ddo_contratogarantia_datacaucao_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOGARANTIA_DATACAUCAO_Includedatalist"));
            Ddo_contratogarantia_datacaucao_Sortasc = cgiGet( "DDO_CONTRATOGARANTIA_DATACAUCAO_Sortasc");
            Ddo_contratogarantia_datacaucao_Sortdsc = cgiGet( "DDO_CONTRATOGARANTIA_DATACAUCAO_Sortdsc");
            Ddo_contratogarantia_datacaucao_Cleanfilter = cgiGet( "DDO_CONTRATOGARANTIA_DATACAUCAO_Cleanfilter");
            Ddo_contratogarantia_datacaucao_Rangefilterfrom = cgiGet( "DDO_CONTRATOGARANTIA_DATACAUCAO_Rangefilterfrom");
            Ddo_contratogarantia_datacaucao_Rangefilterto = cgiGet( "DDO_CONTRATOGARANTIA_DATACAUCAO_Rangefilterto");
            Ddo_contratogarantia_datacaucao_Searchbuttontext = cgiGet( "DDO_CONTRATOGARANTIA_DATACAUCAO_Searchbuttontext");
            Ddo_contratogarantia_valorgarantia_Caption = cgiGet( "DDO_CONTRATOGARANTIA_VALORGARANTIA_Caption");
            Ddo_contratogarantia_valorgarantia_Tooltip = cgiGet( "DDO_CONTRATOGARANTIA_VALORGARANTIA_Tooltip");
            Ddo_contratogarantia_valorgarantia_Cls = cgiGet( "DDO_CONTRATOGARANTIA_VALORGARANTIA_Cls");
            Ddo_contratogarantia_valorgarantia_Filteredtext_set = cgiGet( "DDO_CONTRATOGARANTIA_VALORGARANTIA_Filteredtext_set");
            Ddo_contratogarantia_valorgarantia_Filteredtextto_set = cgiGet( "DDO_CONTRATOGARANTIA_VALORGARANTIA_Filteredtextto_set");
            Ddo_contratogarantia_valorgarantia_Dropdownoptionstype = cgiGet( "DDO_CONTRATOGARANTIA_VALORGARANTIA_Dropdownoptionstype");
            Ddo_contratogarantia_valorgarantia_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATOGARANTIA_VALORGARANTIA_Titlecontrolidtoreplace");
            Ddo_contratogarantia_valorgarantia_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOGARANTIA_VALORGARANTIA_Includesortasc"));
            Ddo_contratogarantia_valorgarantia_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOGARANTIA_VALORGARANTIA_Includesortdsc"));
            Ddo_contratogarantia_valorgarantia_Sortedstatus = cgiGet( "DDO_CONTRATOGARANTIA_VALORGARANTIA_Sortedstatus");
            Ddo_contratogarantia_valorgarantia_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOGARANTIA_VALORGARANTIA_Includefilter"));
            Ddo_contratogarantia_valorgarantia_Filtertype = cgiGet( "DDO_CONTRATOGARANTIA_VALORGARANTIA_Filtertype");
            Ddo_contratogarantia_valorgarantia_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOGARANTIA_VALORGARANTIA_Filterisrange"));
            Ddo_contratogarantia_valorgarantia_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOGARANTIA_VALORGARANTIA_Includedatalist"));
            Ddo_contratogarantia_valorgarantia_Sortasc = cgiGet( "DDO_CONTRATOGARANTIA_VALORGARANTIA_Sortasc");
            Ddo_contratogarantia_valorgarantia_Sortdsc = cgiGet( "DDO_CONTRATOGARANTIA_VALORGARANTIA_Sortdsc");
            Ddo_contratogarantia_valorgarantia_Cleanfilter = cgiGet( "DDO_CONTRATOGARANTIA_VALORGARANTIA_Cleanfilter");
            Ddo_contratogarantia_valorgarantia_Rangefilterfrom = cgiGet( "DDO_CONTRATOGARANTIA_VALORGARANTIA_Rangefilterfrom");
            Ddo_contratogarantia_valorgarantia_Rangefilterto = cgiGet( "DDO_CONTRATOGARANTIA_VALORGARANTIA_Rangefilterto");
            Ddo_contratogarantia_valorgarantia_Searchbuttontext = cgiGet( "DDO_CONTRATOGARANTIA_VALORGARANTIA_Searchbuttontext");
            Ddo_contratogarantia_dataencerramento_Caption = cgiGet( "DDO_CONTRATOGARANTIA_DATAENCERRAMENTO_Caption");
            Ddo_contratogarantia_dataencerramento_Tooltip = cgiGet( "DDO_CONTRATOGARANTIA_DATAENCERRAMENTO_Tooltip");
            Ddo_contratogarantia_dataencerramento_Cls = cgiGet( "DDO_CONTRATOGARANTIA_DATAENCERRAMENTO_Cls");
            Ddo_contratogarantia_dataencerramento_Filteredtext_set = cgiGet( "DDO_CONTRATOGARANTIA_DATAENCERRAMENTO_Filteredtext_set");
            Ddo_contratogarantia_dataencerramento_Filteredtextto_set = cgiGet( "DDO_CONTRATOGARANTIA_DATAENCERRAMENTO_Filteredtextto_set");
            Ddo_contratogarantia_dataencerramento_Dropdownoptionstype = cgiGet( "DDO_CONTRATOGARANTIA_DATAENCERRAMENTO_Dropdownoptionstype");
            Ddo_contratogarantia_dataencerramento_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATOGARANTIA_DATAENCERRAMENTO_Titlecontrolidtoreplace");
            Ddo_contratogarantia_dataencerramento_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOGARANTIA_DATAENCERRAMENTO_Includesortasc"));
            Ddo_contratogarantia_dataencerramento_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOGARANTIA_DATAENCERRAMENTO_Includesortdsc"));
            Ddo_contratogarantia_dataencerramento_Sortedstatus = cgiGet( "DDO_CONTRATOGARANTIA_DATAENCERRAMENTO_Sortedstatus");
            Ddo_contratogarantia_dataencerramento_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOGARANTIA_DATAENCERRAMENTO_Includefilter"));
            Ddo_contratogarantia_dataencerramento_Filtertype = cgiGet( "DDO_CONTRATOGARANTIA_DATAENCERRAMENTO_Filtertype");
            Ddo_contratogarantia_dataencerramento_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOGARANTIA_DATAENCERRAMENTO_Filterisrange"));
            Ddo_contratogarantia_dataencerramento_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOGARANTIA_DATAENCERRAMENTO_Includedatalist"));
            Ddo_contratogarantia_dataencerramento_Sortasc = cgiGet( "DDO_CONTRATOGARANTIA_DATAENCERRAMENTO_Sortasc");
            Ddo_contratogarantia_dataencerramento_Sortdsc = cgiGet( "DDO_CONTRATOGARANTIA_DATAENCERRAMENTO_Sortdsc");
            Ddo_contratogarantia_dataencerramento_Cleanfilter = cgiGet( "DDO_CONTRATOGARANTIA_DATAENCERRAMENTO_Cleanfilter");
            Ddo_contratogarantia_dataencerramento_Rangefilterfrom = cgiGet( "DDO_CONTRATOGARANTIA_DATAENCERRAMENTO_Rangefilterfrom");
            Ddo_contratogarantia_dataencerramento_Rangefilterto = cgiGet( "DDO_CONTRATOGARANTIA_DATAENCERRAMENTO_Rangefilterto");
            Ddo_contratogarantia_dataencerramento_Searchbuttontext = cgiGet( "DDO_CONTRATOGARANTIA_DATAENCERRAMENTO_Searchbuttontext");
            Ddo_contratogarantia_valorencerramento_Caption = cgiGet( "DDO_CONTRATOGARANTIA_VALORENCERRAMENTO_Caption");
            Ddo_contratogarantia_valorencerramento_Tooltip = cgiGet( "DDO_CONTRATOGARANTIA_VALORENCERRAMENTO_Tooltip");
            Ddo_contratogarantia_valorencerramento_Cls = cgiGet( "DDO_CONTRATOGARANTIA_VALORENCERRAMENTO_Cls");
            Ddo_contratogarantia_valorencerramento_Filteredtext_set = cgiGet( "DDO_CONTRATOGARANTIA_VALORENCERRAMENTO_Filteredtext_set");
            Ddo_contratogarantia_valorencerramento_Filteredtextto_set = cgiGet( "DDO_CONTRATOGARANTIA_VALORENCERRAMENTO_Filteredtextto_set");
            Ddo_contratogarantia_valorencerramento_Dropdownoptionstype = cgiGet( "DDO_CONTRATOGARANTIA_VALORENCERRAMENTO_Dropdownoptionstype");
            Ddo_contratogarantia_valorencerramento_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATOGARANTIA_VALORENCERRAMENTO_Titlecontrolidtoreplace");
            Ddo_contratogarantia_valorencerramento_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOGARANTIA_VALORENCERRAMENTO_Includesortasc"));
            Ddo_contratogarantia_valorencerramento_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOGARANTIA_VALORENCERRAMENTO_Includesortdsc"));
            Ddo_contratogarantia_valorencerramento_Sortedstatus = cgiGet( "DDO_CONTRATOGARANTIA_VALORENCERRAMENTO_Sortedstatus");
            Ddo_contratogarantia_valorencerramento_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOGARANTIA_VALORENCERRAMENTO_Includefilter"));
            Ddo_contratogarantia_valorencerramento_Filtertype = cgiGet( "DDO_CONTRATOGARANTIA_VALORENCERRAMENTO_Filtertype");
            Ddo_contratogarantia_valorencerramento_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOGARANTIA_VALORENCERRAMENTO_Filterisrange"));
            Ddo_contratogarantia_valorencerramento_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATOGARANTIA_VALORENCERRAMENTO_Includedatalist"));
            Ddo_contratogarantia_valorencerramento_Sortasc = cgiGet( "DDO_CONTRATOGARANTIA_VALORENCERRAMENTO_Sortasc");
            Ddo_contratogarantia_valorencerramento_Sortdsc = cgiGet( "DDO_CONTRATOGARANTIA_VALORENCERRAMENTO_Sortdsc");
            Ddo_contratogarantia_valorencerramento_Cleanfilter = cgiGet( "DDO_CONTRATOGARANTIA_VALORENCERRAMENTO_Cleanfilter");
            Ddo_contratogarantia_valorencerramento_Rangefilterfrom = cgiGet( "DDO_CONTRATOGARANTIA_VALORENCERRAMENTO_Rangefilterfrom");
            Ddo_contratogarantia_valorencerramento_Rangefilterto = cgiGet( "DDO_CONTRATOGARANTIA_VALORENCERRAMENTO_Rangefilterto");
            Ddo_contratogarantia_valorencerramento_Searchbuttontext = cgiGet( "DDO_CONTRATOGARANTIA_VALORENCERRAMENTO_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_contrato_numero_Activeeventkey = cgiGet( "DDO_CONTRATO_NUMERO_Activeeventkey");
            Ddo_contrato_numero_Filteredtext_get = cgiGet( "DDO_CONTRATO_NUMERO_Filteredtext_get");
            Ddo_contrato_numero_Selectedvalue_get = cgiGet( "DDO_CONTRATO_NUMERO_Selectedvalue_get");
            Ddo_contratada_pessoanom_Activeeventkey = cgiGet( "DDO_CONTRATADA_PESSOANOM_Activeeventkey");
            Ddo_contratada_pessoanom_Filteredtext_get = cgiGet( "DDO_CONTRATADA_PESSOANOM_Filteredtext_get");
            Ddo_contratada_pessoanom_Selectedvalue_get = cgiGet( "DDO_CONTRATADA_PESSOANOM_Selectedvalue_get");
            Ddo_contratada_pessoacnpj_Activeeventkey = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Activeeventkey");
            Ddo_contratada_pessoacnpj_Filteredtext_get = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Filteredtext_get");
            Ddo_contratada_pessoacnpj_Selectedvalue_get = cgiGet( "DDO_CONTRATADA_PESSOACNPJ_Selectedvalue_get");
            Ddo_contratogarantia_datapagtogarantia_Activeeventkey = cgiGet( "DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA_Activeeventkey");
            Ddo_contratogarantia_datapagtogarantia_Filteredtext_get = cgiGet( "DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA_Filteredtext_get");
            Ddo_contratogarantia_datapagtogarantia_Filteredtextto_get = cgiGet( "DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA_Filteredtextto_get");
            Ddo_contratogarantia_percentual_Activeeventkey = cgiGet( "DDO_CONTRATOGARANTIA_PERCENTUAL_Activeeventkey");
            Ddo_contratogarantia_percentual_Filteredtext_get = cgiGet( "DDO_CONTRATOGARANTIA_PERCENTUAL_Filteredtext_get");
            Ddo_contratogarantia_percentual_Filteredtextto_get = cgiGet( "DDO_CONTRATOGARANTIA_PERCENTUAL_Filteredtextto_get");
            Ddo_contratogarantia_datacaucao_Activeeventkey = cgiGet( "DDO_CONTRATOGARANTIA_DATACAUCAO_Activeeventkey");
            Ddo_contratogarantia_datacaucao_Filteredtext_get = cgiGet( "DDO_CONTRATOGARANTIA_DATACAUCAO_Filteredtext_get");
            Ddo_contratogarantia_datacaucao_Filteredtextto_get = cgiGet( "DDO_CONTRATOGARANTIA_DATACAUCAO_Filteredtextto_get");
            Ddo_contratogarantia_valorgarantia_Activeeventkey = cgiGet( "DDO_CONTRATOGARANTIA_VALORGARANTIA_Activeeventkey");
            Ddo_contratogarantia_valorgarantia_Filteredtext_get = cgiGet( "DDO_CONTRATOGARANTIA_VALORGARANTIA_Filteredtext_get");
            Ddo_contratogarantia_valorgarantia_Filteredtextto_get = cgiGet( "DDO_CONTRATOGARANTIA_VALORGARANTIA_Filteredtextto_get");
            Ddo_contratogarantia_dataencerramento_Activeeventkey = cgiGet( "DDO_CONTRATOGARANTIA_DATAENCERRAMENTO_Activeeventkey");
            Ddo_contratogarantia_dataencerramento_Filteredtext_get = cgiGet( "DDO_CONTRATOGARANTIA_DATAENCERRAMENTO_Filteredtext_get");
            Ddo_contratogarantia_dataencerramento_Filteredtextto_get = cgiGet( "DDO_CONTRATOGARANTIA_DATAENCERRAMENTO_Filteredtextto_get");
            Ddo_contratogarantia_valorencerramento_Activeeventkey = cgiGet( "DDO_CONTRATOGARANTIA_VALORENCERRAMENTO_Activeeventkey");
            Ddo_contratogarantia_valorencerramento_Filteredtext_get = cgiGet( "DDO_CONTRATOGARANTIA_VALORENCERRAMENTO_Filteredtext_get");
            Ddo_contratogarantia_valorencerramento_Filteredtextto_get = cgiGet( "DDO_CONTRATOGARANTIA_VALORENCERRAMENTO_Filteredtextto_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOGARANTIA_DATAPAGTOGARANTIA1"), 0) != AV16ContratoGarantia_DataPagtoGarantia1 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO1"), 0) != AV17ContratoGarantia_DataPagtoGarantia_To1 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOGARANTIA_DATACAUCAO1"), 0) != AV37ContratoGarantia_DataCaucao1 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOGARANTIA_DATACAUCAO_TO1"), 0) != AV38ContratoGarantia_DataCaucao_To1 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOGARANTIA_DATAENCERRAMENTO1"), 0) != AV39ContratoGarantia_DataEncerramento1 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOGARANTIA_DATAENCERRAMENTO_TO1"), 0) != AV40ContratoGarantia_DataEncerramento_To1 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV19DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOGARANTIA_DATAPAGTOGARANTIA2"), 0) != AV20ContratoGarantia_DataPagtoGarantia2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO2"), 0) != AV21ContratoGarantia_DataPagtoGarantia_To2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOGARANTIA_DATACAUCAO2"), 0) != AV41ContratoGarantia_DataCaucao2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOGARANTIA_DATACAUCAO_TO2"), 0) != AV42ContratoGarantia_DataCaucao_To2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOGARANTIA_DATAENCERRAMENTO2"), 0) != AV43ContratoGarantia_DataEncerramento2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOGARANTIA_DATAENCERRAMENTO_TO2"), 0) != AV44ContratoGarantia_DataEncerramento_To2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV23DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOGARANTIA_DATAPAGTOGARANTIA3"), 0) != AV24ContratoGarantia_DataPagtoGarantia3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO3"), 0) != AV25ContratoGarantia_DataPagtoGarantia_To3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOGARANTIA_DATACAUCAO3"), 0) != AV45ContratoGarantia_DataCaucao3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOGARANTIA_DATACAUCAO_TO3"), 0) != AV46ContratoGarantia_DataCaucao_To3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOGARANTIA_DATAENCERRAMENTO3"), 0) != AV47ContratoGarantia_DataEncerramento3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCONTRATOGARANTIA_DATAENCERRAMENTO_TO3"), 0) != AV48ContratoGarantia_DataEncerramento_To3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV18DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV22DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATO_NUMERO"), AV52TFContrato_Numero) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATO_NUMERO_SEL"), AV53TFContrato_Numero_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATADA_PESSOANOM"), AV56TFContratada_PessoaNom) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATADA_PESSOANOM_SEL"), AV57TFContratada_PessoaNom_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATADA_PESSOACNPJ"), AV60TFContratada_PessoaCNPJ) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATADA_PESSOACNPJ_SEL"), AV61TFContratada_PessoaCNPJ_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTRATOGARANTIA_DATAPAGTOGARANTIA"), 0) != AV64TFContratoGarantia_DataPagtoGarantia )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO"), 0) != AV65TFContratoGarantia_DataPagtoGarantia_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOGARANTIA_PERCENTUAL"), ",", ".") != AV70TFContratoGarantia_Percentual )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOGARANTIA_PERCENTUAL_TO"), ",", ".") != AV71TFContratoGarantia_Percentual_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTRATOGARANTIA_DATACAUCAO"), 0) != AV74TFContratoGarantia_DataCaucao )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTRATOGARANTIA_DATACAUCAO_TO"), 0) != AV75TFContratoGarantia_DataCaucao_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOGARANTIA_VALORGARANTIA"), ",", ".") != AV80TFContratoGarantia_ValorGarantia )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOGARANTIA_VALORGARANTIA_TO"), ",", ".") != AV81TFContratoGarantia_ValorGarantia_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTRATOGARANTIA_DATAENCERRAMENTO"), 0) != AV84TFContratoGarantia_DataEncerramento )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFCONTRATOGARANTIA_DATAENCERRAMENTO_TO"), 0) != AV85TFContratoGarantia_DataEncerramento_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOGARANTIA_VALORENCERRAMENTO"), ",", ".") != AV90TFContratoGarantia_ValorEncerramento )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATOGARANTIA_VALORENCERRAMENTO_TO"), ",", ".") != AV91TFContratoGarantia_ValorEncerramento_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E326H2 */
         E326H2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E326H2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV15DynamicFiltersSelector1 = "CONTRATOGARANTIA_DATAPAGTOGARANTIA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV19DynamicFiltersSelector2 = "CONTRATOGARANTIA_DATAPAGTOGARANTIA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV23DynamicFiltersSelector3 = "CONTRATOGARANTIA_DATAPAGTOGARANTIA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         edtavTfcontrato_numero_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontrato_numero_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontrato_numero_Visible), 5, 0)));
         edtavTfcontrato_numero_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontrato_numero_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontrato_numero_sel_Visible), 5, 0)));
         edtavTfcontratada_pessoanom_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratada_pessoanom_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratada_pessoanom_Visible), 5, 0)));
         edtavTfcontratada_pessoanom_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratada_pessoanom_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratada_pessoanom_sel_Visible), 5, 0)));
         edtavTfcontratada_pessoacnpj_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratada_pessoacnpj_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratada_pessoacnpj_Visible), 5, 0)));
         edtavTfcontratada_pessoacnpj_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratada_pessoacnpj_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratada_pessoacnpj_sel_Visible), 5, 0)));
         edtavTfcontratogarantia_datapagtogarantia_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratogarantia_datapagtogarantia_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratogarantia_datapagtogarantia_Visible), 5, 0)));
         edtavTfcontratogarantia_datapagtogarantia_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratogarantia_datapagtogarantia_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratogarantia_datapagtogarantia_to_Visible), 5, 0)));
         edtavTfcontratogarantia_percentual_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratogarantia_percentual_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratogarantia_percentual_Visible), 5, 0)));
         edtavTfcontratogarantia_percentual_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratogarantia_percentual_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratogarantia_percentual_to_Visible), 5, 0)));
         edtavTfcontratogarantia_datacaucao_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratogarantia_datacaucao_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratogarantia_datacaucao_Visible), 5, 0)));
         edtavTfcontratogarantia_datacaucao_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratogarantia_datacaucao_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratogarantia_datacaucao_to_Visible), 5, 0)));
         edtavTfcontratogarantia_valorgarantia_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratogarantia_valorgarantia_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratogarantia_valorgarantia_Visible), 5, 0)));
         edtavTfcontratogarantia_valorgarantia_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratogarantia_valorgarantia_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratogarantia_valorgarantia_to_Visible), 5, 0)));
         edtavTfcontratogarantia_dataencerramento_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratogarantia_dataencerramento_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratogarantia_dataencerramento_Visible), 5, 0)));
         edtavTfcontratogarantia_dataencerramento_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratogarantia_dataencerramento_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratogarantia_dataencerramento_to_Visible), 5, 0)));
         edtavTfcontratogarantia_valorencerramento_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratogarantia_valorencerramento_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratogarantia_valorencerramento_Visible), 5, 0)));
         edtavTfcontratogarantia_valorencerramento_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratogarantia_valorencerramento_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratogarantia_valorencerramento_to_Visible), 5, 0)));
         Ddo_contrato_numero_Titlecontrolidtoreplace = subGrid_Internalname+"_Contrato_Numero";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_numero_Internalname, "TitleControlIdToReplace", Ddo_contrato_numero_Titlecontrolidtoreplace);
         AV54ddo_Contrato_NumeroTitleControlIdToReplace = Ddo_contrato_numero_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54ddo_Contrato_NumeroTitleControlIdToReplace", AV54ddo_Contrato_NumeroTitleControlIdToReplace);
         edtavDdo_contrato_numerotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contrato_numerotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contrato_numerotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratada_pessoanom_Titlecontrolidtoreplace = subGrid_Internalname+"_Contratada_PessoaNom";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoanom_Internalname, "TitleControlIdToReplace", Ddo_contratada_pessoanom_Titlecontrolidtoreplace);
         AV58ddo_Contratada_PessoaNomTitleControlIdToReplace = Ddo_contratada_pessoanom_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58ddo_Contratada_PessoaNomTitleControlIdToReplace", AV58ddo_Contratada_PessoaNomTitleControlIdToReplace);
         edtavDdo_contratada_pessoanomtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratada_pessoanomtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratada_pessoanomtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratada_pessoacnpj_Titlecontrolidtoreplace = subGrid_Internalname+"_Contratada_PessoaCNPJ";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoacnpj_Internalname, "TitleControlIdToReplace", Ddo_contratada_pessoacnpj_Titlecontrolidtoreplace);
         AV62ddo_Contratada_PessoaCNPJTitleControlIdToReplace = Ddo_contratada_pessoacnpj_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62ddo_Contratada_PessoaCNPJTitleControlIdToReplace", AV62ddo_Contratada_PessoaCNPJTitleControlIdToReplace);
         edtavDdo_contratada_pessoacnpjtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratada_pessoacnpjtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratada_pessoacnpjtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratogarantia_datapagtogarantia_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoGarantia_DataPagtoGarantia";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratogarantia_datapagtogarantia_Internalname, "TitleControlIdToReplace", Ddo_contratogarantia_datapagtogarantia_Titlecontrolidtoreplace);
         AV68ddo_ContratoGarantia_DataPagtoGarantiaTitleControlIdToReplace = Ddo_contratogarantia_datapagtogarantia_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68ddo_ContratoGarantia_DataPagtoGarantiaTitleControlIdToReplace", AV68ddo_ContratoGarantia_DataPagtoGarantiaTitleControlIdToReplace);
         edtavDdo_contratogarantia_datapagtogarantiatitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratogarantia_datapagtogarantiatitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratogarantia_datapagtogarantiatitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratogarantia_percentual_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoGarantia_Percentual";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratogarantia_percentual_Internalname, "TitleControlIdToReplace", Ddo_contratogarantia_percentual_Titlecontrolidtoreplace);
         AV72ddo_ContratoGarantia_PercentualTitleControlIdToReplace = Ddo_contratogarantia_percentual_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72ddo_ContratoGarantia_PercentualTitleControlIdToReplace", AV72ddo_ContratoGarantia_PercentualTitleControlIdToReplace);
         edtavDdo_contratogarantia_percentualtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratogarantia_percentualtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratogarantia_percentualtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratogarantia_datacaucao_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoGarantia_DataCaucao";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratogarantia_datacaucao_Internalname, "TitleControlIdToReplace", Ddo_contratogarantia_datacaucao_Titlecontrolidtoreplace);
         AV78ddo_ContratoGarantia_DataCaucaoTitleControlIdToReplace = Ddo_contratogarantia_datacaucao_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV78ddo_ContratoGarantia_DataCaucaoTitleControlIdToReplace", AV78ddo_ContratoGarantia_DataCaucaoTitleControlIdToReplace);
         edtavDdo_contratogarantia_datacaucaotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratogarantia_datacaucaotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratogarantia_datacaucaotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratogarantia_valorgarantia_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoGarantia_ValorGarantia";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratogarantia_valorgarantia_Internalname, "TitleControlIdToReplace", Ddo_contratogarantia_valorgarantia_Titlecontrolidtoreplace);
         AV82ddo_ContratoGarantia_ValorGarantiaTitleControlIdToReplace = Ddo_contratogarantia_valorgarantia_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV82ddo_ContratoGarantia_ValorGarantiaTitleControlIdToReplace", AV82ddo_ContratoGarantia_ValorGarantiaTitleControlIdToReplace);
         edtavDdo_contratogarantia_valorgarantiatitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratogarantia_valorgarantiatitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratogarantia_valorgarantiatitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratogarantia_dataencerramento_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoGarantia_DataEncerramento";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratogarantia_dataencerramento_Internalname, "TitleControlIdToReplace", Ddo_contratogarantia_dataencerramento_Titlecontrolidtoreplace);
         AV88ddo_ContratoGarantia_DataEncerramentoTitleControlIdToReplace = Ddo_contratogarantia_dataencerramento_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV88ddo_ContratoGarantia_DataEncerramentoTitleControlIdToReplace", AV88ddo_ContratoGarantia_DataEncerramentoTitleControlIdToReplace);
         edtavDdo_contratogarantia_dataencerramentotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratogarantia_dataencerramentotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratogarantia_dataencerramentotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratogarantia_valorencerramento_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoGarantia_ValorEncerramento";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratogarantia_valorencerramento_Internalname, "TitleControlIdToReplace", Ddo_contratogarantia_valorencerramento_Titlecontrolidtoreplace);
         AV92ddo_ContratoGarantia_ValorEncerramentoTitleControlIdToReplace = Ddo_contratogarantia_valorencerramento_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV92ddo_ContratoGarantia_ValorEncerramentoTitleControlIdToReplace", AV92ddo_ContratoGarantia_ValorEncerramentoTitleControlIdToReplace);
         edtavDdo_contratogarantia_valorencerramentotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratogarantia_valorencerramentotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratogarantia_valorencerramentotitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = " Contrato Garantia";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Data Pagamento", 0);
         cmbavOrderedby.addItem("2", "N� Contrato", 0);
         cmbavOrderedby.addItem("3", "Contratada", 0);
         cmbavOrderedby.addItem("4", "CNPJ", 0);
         cmbavOrderedby.addItem("5", "Percentual", 0);
         cmbavOrderedby.addItem("6", "D. da Cau��o", 0);
         cmbavOrderedby.addItem("7", "Valor", 0);
         cmbavOrderedby.addItem("8", "Data Encerramento", 0);
         cmbavOrderedby.addItem("9", "Valor Encerramento", 0);
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV93DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV93DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E336H2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV51Contrato_NumeroTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV55Contratada_PessoaNomTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV59Contratada_PessoaCNPJTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV63ContratoGarantia_DataPagtoGarantiaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV69ContratoGarantia_PercentualTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV73ContratoGarantia_DataCaucaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV79ContratoGarantia_ValorGarantiaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV83ContratoGarantia_DataEncerramentoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV89ContratoGarantia_ValorEncerramentoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtContrato_Numero_Titleformat = 2;
         edtContrato_Numero_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "N� Contrato", AV54ddo_Contrato_NumeroTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContrato_Numero_Internalname, "Title", edtContrato_Numero_Title);
         edtContratada_PessoaNom_Titleformat = 2;
         edtContratada_PessoaNom_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Contratada", AV58ddo_Contratada_PessoaNomTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratada_PessoaNom_Internalname, "Title", edtContratada_PessoaNom_Title);
         edtContratada_PessoaCNPJ_Titleformat = 2;
         edtContratada_PessoaCNPJ_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "CNPJ", AV62ddo_Contratada_PessoaCNPJTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratada_PessoaCNPJ_Internalname, "Title", edtContratada_PessoaCNPJ_Title);
         edtContratoGarantia_DataPagtoGarantia_Titleformat = 2;
         edtContratoGarantia_DataPagtoGarantia_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Data Pagamento", AV68ddo_ContratoGarantia_DataPagtoGarantiaTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoGarantia_DataPagtoGarantia_Internalname, "Title", edtContratoGarantia_DataPagtoGarantia_Title);
         edtContratoGarantia_Percentual_Titleformat = 2;
         edtContratoGarantia_Percentual_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Percentual", AV72ddo_ContratoGarantia_PercentualTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoGarantia_Percentual_Internalname, "Title", edtContratoGarantia_Percentual_Title);
         edtContratoGarantia_DataCaucao_Titleformat = 2;
         edtContratoGarantia_DataCaucao_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "D. da Cau��o", AV78ddo_ContratoGarantia_DataCaucaoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoGarantia_DataCaucao_Internalname, "Title", edtContratoGarantia_DataCaucao_Title);
         edtContratoGarantia_ValorGarantia_Titleformat = 2;
         edtContratoGarantia_ValorGarantia_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Valor", AV82ddo_ContratoGarantia_ValorGarantiaTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoGarantia_ValorGarantia_Internalname, "Title", edtContratoGarantia_ValorGarantia_Title);
         edtContratoGarantia_DataEncerramento_Titleformat = 2;
         edtContratoGarantia_DataEncerramento_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Data Encerramento", AV88ddo_ContratoGarantia_DataEncerramentoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoGarantia_DataEncerramento_Internalname, "Title", edtContratoGarantia_DataEncerramento_Title);
         edtContratoGarantia_ValorEncerramento_Titleformat = 2;
         edtContratoGarantia_ValorEncerramento_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Valor Encerramento", AV92ddo_ContratoGarantia_ValorEncerramentoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoGarantia_ValorEncerramento_Internalname, "Title", edtContratoGarantia_ValorEncerramento_Title);
         AV95GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV95GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV95GridCurrentPage), 10, 0)));
         AV96GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV96GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV96GridPageCount), 10, 0)));
         AV114WWContratoGarantiaDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV115WWContratoGarantiaDS_2_Contratogarantia_datapagtogarantia1 = AV16ContratoGarantia_DataPagtoGarantia1;
         AV116WWContratoGarantiaDS_3_Contratogarantia_datapagtogarantia_to1 = AV17ContratoGarantia_DataPagtoGarantia_To1;
         AV117WWContratoGarantiaDS_4_Contratogarantia_datacaucao1 = AV37ContratoGarantia_DataCaucao1;
         AV118WWContratoGarantiaDS_5_Contratogarantia_datacaucao_to1 = AV38ContratoGarantia_DataCaucao_To1;
         AV119WWContratoGarantiaDS_6_Contratogarantia_dataencerramento1 = AV39ContratoGarantia_DataEncerramento1;
         AV120WWContratoGarantiaDS_7_Contratogarantia_dataencerramento_to1 = AV40ContratoGarantia_DataEncerramento_To1;
         AV121WWContratoGarantiaDS_8_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV122WWContratoGarantiaDS_9_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV123WWContratoGarantiaDS_10_Contratogarantia_datapagtogarantia2 = AV20ContratoGarantia_DataPagtoGarantia2;
         AV124WWContratoGarantiaDS_11_Contratogarantia_datapagtogarantia_to2 = AV21ContratoGarantia_DataPagtoGarantia_To2;
         AV125WWContratoGarantiaDS_12_Contratogarantia_datacaucao2 = AV41ContratoGarantia_DataCaucao2;
         AV126WWContratoGarantiaDS_13_Contratogarantia_datacaucao_to2 = AV42ContratoGarantia_DataCaucao_To2;
         AV127WWContratoGarantiaDS_14_Contratogarantia_dataencerramento2 = AV43ContratoGarantia_DataEncerramento2;
         AV128WWContratoGarantiaDS_15_Contratogarantia_dataencerramento_to2 = AV44ContratoGarantia_DataEncerramento_To2;
         AV129WWContratoGarantiaDS_16_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV130WWContratoGarantiaDS_17_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV131WWContratoGarantiaDS_18_Contratogarantia_datapagtogarantia3 = AV24ContratoGarantia_DataPagtoGarantia3;
         AV132WWContratoGarantiaDS_19_Contratogarantia_datapagtogarantia_to3 = AV25ContratoGarantia_DataPagtoGarantia_To3;
         AV133WWContratoGarantiaDS_20_Contratogarantia_datacaucao3 = AV45ContratoGarantia_DataCaucao3;
         AV134WWContratoGarantiaDS_21_Contratogarantia_datacaucao_to3 = AV46ContratoGarantia_DataCaucao_To3;
         AV135WWContratoGarantiaDS_22_Contratogarantia_dataencerramento3 = AV47ContratoGarantia_DataEncerramento3;
         AV136WWContratoGarantiaDS_23_Contratogarantia_dataencerramento_to3 = AV48ContratoGarantia_DataEncerramento_To3;
         AV137WWContratoGarantiaDS_24_Tfcontrato_numero = AV52TFContrato_Numero;
         AV138WWContratoGarantiaDS_25_Tfcontrato_numero_sel = AV53TFContrato_Numero_Sel;
         AV139WWContratoGarantiaDS_26_Tfcontratada_pessoanom = AV56TFContratada_PessoaNom;
         AV140WWContratoGarantiaDS_27_Tfcontratada_pessoanom_sel = AV57TFContratada_PessoaNom_Sel;
         AV141WWContratoGarantiaDS_28_Tfcontratada_pessoacnpj = AV60TFContratada_PessoaCNPJ;
         AV142WWContratoGarantiaDS_29_Tfcontratada_pessoacnpj_sel = AV61TFContratada_PessoaCNPJ_Sel;
         AV143WWContratoGarantiaDS_30_Tfcontratogarantia_datapagtogarantia = AV64TFContratoGarantia_DataPagtoGarantia;
         AV144WWContratoGarantiaDS_31_Tfcontratogarantia_datapagtogarantia_to = AV65TFContratoGarantia_DataPagtoGarantia_To;
         AV145WWContratoGarantiaDS_32_Tfcontratogarantia_percentual = AV70TFContratoGarantia_Percentual;
         AV146WWContratoGarantiaDS_33_Tfcontratogarantia_percentual_to = AV71TFContratoGarantia_Percentual_To;
         AV147WWContratoGarantiaDS_34_Tfcontratogarantia_datacaucao = AV74TFContratoGarantia_DataCaucao;
         AV148WWContratoGarantiaDS_35_Tfcontratogarantia_datacaucao_to = AV75TFContratoGarantia_DataCaucao_To;
         AV149WWContratoGarantiaDS_36_Tfcontratogarantia_valorgarantia = AV80TFContratoGarantia_ValorGarantia;
         AV150WWContratoGarantiaDS_37_Tfcontratogarantia_valorgarantia_to = AV81TFContratoGarantia_ValorGarantia_To;
         AV151WWContratoGarantiaDS_38_Tfcontratogarantia_dataencerramento = AV84TFContratoGarantia_DataEncerramento;
         AV152WWContratoGarantiaDS_39_Tfcontratogarantia_dataencerramento_to = AV85TFContratoGarantia_DataEncerramento_To;
         AV153WWContratoGarantiaDS_40_Tfcontratogarantia_valorencerramento = AV90TFContratoGarantia_ValorEncerramento;
         AV154WWContratoGarantiaDS_41_Tfcontratogarantia_valorencerramento_to = AV91TFContratoGarantia_ValorEncerramento_To;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV51Contrato_NumeroTitleFilterData", AV51Contrato_NumeroTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV55Contratada_PessoaNomTitleFilterData", AV55Contratada_PessoaNomTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV59Contratada_PessoaCNPJTitleFilterData", AV59Contratada_PessoaCNPJTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV63ContratoGarantia_DataPagtoGarantiaTitleFilterData", AV63ContratoGarantia_DataPagtoGarantiaTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV69ContratoGarantia_PercentualTitleFilterData", AV69ContratoGarantia_PercentualTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV73ContratoGarantia_DataCaucaoTitleFilterData", AV73ContratoGarantia_DataCaucaoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV79ContratoGarantia_ValorGarantiaTitleFilterData", AV79ContratoGarantia_ValorGarantiaTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV83ContratoGarantia_DataEncerramentoTitleFilterData", AV83ContratoGarantia_DataEncerramentoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV89ContratoGarantia_ValorEncerramentoTitleFilterData", AV89ContratoGarantia_ValorEncerramentoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void E116H2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV94PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV94PageToGo) ;
         }
      }

      protected void E126H2( )
      {
         /* Ddo_contrato_numero_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contrato_numero_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contrato_numero_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_numero_Internalname, "SortedStatus", Ddo_contrato_numero_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contrato_numero_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contrato_numero_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_numero_Internalname, "SortedStatus", Ddo_contrato_numero_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contrato_numero_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV52TFContrato_Numero = Ddo_contrato_numero_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52TFContrato_Numero", AV52TFContrato_Numero);
            AV53TFContrato_Numero_Sel = Ddo_contrato_numero_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53TFContrato_Numero_Sel", AV53TFContrato_Numero_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E136H2( )
      {
         /* Ddo_contratada_pessoanom_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratada_pessoanom_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratada_pessoanom_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoanom_Internalname, "SortedStatus", Ddo_contratada_pessoanom_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratada_pessoanom_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratada_pessoanom_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoanom_Internalname, "SortedStatus", Ddo_contratada_pessoanom_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratada_pessoanom_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV56TFContratada_PessoaNom = Ddo_contratada_pessoanom_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFContratada_PessoaNom", AV56TFContratada_PessoaNom);
            AV57TFContratada_PessoaNom_Sel = Ddo_contratada_pessoanom_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57TFContratada_PessoaNom_Sel", AV57TFContratada_PessoaNom_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E146H2( )
      {
         /* Ddo_contratada_pessoacnpj_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratada_pessoacnpj_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratada_pessoacnpj_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoacnpj_Internalname, "SortedStatus", Ddo_contratada_pessoacnpj_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratada_pessoacnpj_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratada_pessoacnpj_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoacnpj_Internalname, "SortedStatus", Ddo_contratada_pessoacnpj_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratada_pessoacnpj_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV60TFContratada_PessoaCNPJ = Ddo_contratada_pessoacnpj_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFContratada_PessoaCNPJ", AV60TFContratada_PessoaCNPJ);
            AV61TFContratada_PessoaCNPJ_Sel = Ddo_contratada_pessoacnpj_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61TFContratada_PessoaCNPJ_Sel", AV61TFContratada_PessoaCNPJ_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E156H2( )
      {
         /* Ddo_contratogarantia_datapagtogarantia_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratogarantia_datapagtogarantia_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratogarantia_datapagtogarantia_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratogarantia_datapagtogarantia_Internalname, "SortedStatus", Ddo_contratogarantia_datapagtogarantia_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratogarantia_datapagtogarantia_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratogarantia_datapagtogarantia_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratogarantia_datapagtogarantia_Internalname, "SortedStatus", Ddo_contratogarantia_datapagtogarantia_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratogarantia_datapagtogarantia_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV64TFContratoGarantia_DataPagtoGarantia = context.localUtil.CToD( Ddo_contratogarantia_datapagtogarantia_Filteredtext_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64TFContratoGarantia_DataPagtoGarantia", context.localUtil.Format(AV64TFContratoGarantia_DataPagtoGarantia, "99/99/99"));
            AV65TFContratoGarantia_DataPagtoGarantia_To = context.localUtil.CToD( Ddo_contratogarantia_datapagtogarantia_Filteredtextto_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65TFContratoGarantia_DataPagtoGarantia_To", context.localUtil.Format(AV65TFContratoGarantia_DataPagtoGarantia_To, "99/99/99"));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E166H2( )
      {
         /* Ddo_contratogarantia_percentual_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratogarantia_percentual_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratogarantia_percentual_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratogarantia_percentual_Internalname, "SortedStatus", Ddo_contratogarantia_percentual_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratogarantia_percentual_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratogarantia_percentual_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratogarantia_percentual_Internalname, "SortedStatus", Ddo_contratogarantia_percentual_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratogarantia_percentual_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV70TFContratoGarantia_Percentual = NumberUtil.Val( Ddo_contratogarantia_percentual_Filteredtext_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70TFContratoGarantia_Percentual", StringUtil.LTrim( StringUtil.Str( AV70TFContratoGarantia_Percentual, 6, 2)));
            AV71TFContratoGarantia_Percentual_To = NumberUtil.Val( Ddo_contratogarantia_percentual_Filteredtextto_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71TFContratoGarantia_Percentual_To", StringUtil.LTrim( StringUtil.Str( AV71TFContratoGarantia_Percentual_To, 6, 2)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E176H2( )
      {
         /* Ddo_contratogarantia_datacaucao_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratogarantia_datacaucao_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 6;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratogarantia_datacaucao_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratogarantia_datacaucao_Internalname, "SortedStatus", Ddo_contratogarantia_datacaucao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratogarantia_datacaucao_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 6;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratogarantia_datacaucao_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratogarantia_datacaucao_Internalname, "SortedStatus", Ddo_contratogarantia_datacaucao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratogarantia_datacaucao_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV74TFContratoGarantia_DataCaucao = context.localUtil.CToD( Ddo_contratogarantia_datacaucao_Filteredtext_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74TFContratoGarantia_DataCaucao", context.localUtil.Format(AV74TFContratoGarantia_DataCaucao, "99/99/99"));
            AV75TFContratoGarantia_DataCaucao_To = context.localUtil.CToD( Ddo_contratogarantia_datacaucao_Filteredtextto_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75TFContratoGarantia_DataCaucao_To", context.localUtil.Format(AV75TFContratoGarantia_DataCaucao_To, "99/99/99"));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E186H2( )
      {
         /* Ddo_contratogarantia_valorgarantia_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratogarantia_valorgarantia_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 7;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratogarantia_valorgarantia_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratogarantia_valorgarantia_Internalname, "SortedStatus", Ddo_contratogarantia_valorgarantia_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratogarantia_valorgarantia_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 7;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratogarantia_valorgarantia_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratogarantia_valorgarantia_Internalname, "SortedStatus", Ddo_contratogarantia_valorgarantia_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratogarantia_valorgarantia_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV80TFContratoGarantia_ValorGarantia = NumberUtil.Val( Ddo_contratogarantia_valorgarantia_Filteredtext_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80TFContratoGarantia_ValorGarantia", StringUtil.LTrim( StringUtil.Str( AV80TFContratoGarantia_ValorGarantia, 18, 5)));
            AV81TFContratoGarantia_ValorGarantia_To = NumberUtil.Val( Ddo_contratogarantia_valorgarantia_Filteredtextto_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81TFContratoGarantia_ValorGarantia_To", StringUtil.LTrim( StringUtil.Str( AV81TFContratoGarantia_ValorGarantia_To, 18, 5)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E196H2( )
      {
         /* Ddo_contratogarantia_dataencerramento_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratogarantia_dataencerramento_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 8;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratogarantia_dataencerramento_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratogarantia_dataencerramento_Internalname, "SortedStatus", Ddo_contratogarantia_dataencerramento_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratogarantia_dataencerramento_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 8;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratogarantia_dataencerramento_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratogarantia_dataencerramento_Internalname, "SortedStatus", Ddo_contratogarantia_dataencerramento_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratogarantia_dataencerramento_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV84TFContratoGarantia_DataEncerramento = context.localUtil.CToD( Ddo_contratogarantia_dataencerramento_Filteredtext_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV84TFContratoGarantia_DataEncerramento", context.localUtil.Format(AV84TFContratoGarantia_DataEncerramento, "99/99/99"));
            AV85TFContratoGarantia_DataEncerramento_To = context.localUtil.CToD( Ddo_contratogarantia_dataencerramento_Filteredtextto_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV85TFContratoGarantia_DataEncerramento_To", context.localUtil.Format(AV85TFContratoGarantia_DataEncerramento_To, "99/99/99"));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E206H2( )
      {
         /* Ddo_contratogarantia_valorencerramento_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratogarantia_valorencerramento_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 9;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratogarantia_valorencerramento_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratogarantia_valorencerramento_Internalname, "SortedStatus", Ddo_contratogarantia_valorencerramento_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratogarantia_valorencerramento_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 9;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratogarantia_valorencerramento_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratogarantia_valorencerramento_Internalname, "SortedStatus", Ddo_contratogarantia_valorencerramento_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratogarantia_valorencerramento_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV90TFContratoGarantia_ValorEncerramento = NumberUtil.Val( Ddo_contratogarantia_valorencerramento_Filteredtext_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV90TFContratoGarantia_ValorEncerramento", StringUtil.LTrim( StringUtil.Str( AV90TFContratoGarantia_ValorEncerramento, 18, 5)));
            AV91TFContratoGarantia_ValorEncerramento_To = NumberUtil.Val( Ddo_contratogarantia_valorencerramento_Filteredtextto_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV91TFContratoGarantia_ValorEncerramento_To", StringUtil.LTrim( StringUtil.Str( AV91TFContratoGarantia_ValorEncerramento_To, 18, 5)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      private void E346H2( )
      {
         /* Grid_Load Routine */
         AV28Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUpdate_Internalname, AV28Update);
         AV155Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = formatLink("contratogarantia.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A101ContratoGarantia_Codigo) + "," + UrlEncode("" +A74Contrato_Codigo);
         AV29Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDelete_Internalname, AV29Delete);
         AV156Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = formatLink("contratogarantia.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A101ContratoGarantia_Codigo) + "," + UrlEncode("" +A74Contrato_Codigo);
         edtContratada_PessoaNom_Link = formatLink("viewcontratada.aspx") + "?" + UrlEncode("" +A39Contratada_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         edtContratoGarantia_DataPagtoGarantia_Link = formatLink("viewcontratogarantia.aspx") + "?" + UrlEncode("" +A101ContratoGarantia_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 142;
         }
         sendrow_1422( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_142_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(142, GridRow);
         }
      }

      protected void E216H2( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E276H2( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV18DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
      }

      protected void E226H2( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV27DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV27DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16ContratoGarantia_DataPagtoGarantia1, AV17ContratoGarantia_DataPagtoGarantia_To1, AV37ContratoGarantia_DataCaucao1, AV38ContratoGarantia_DataCaucao_To1, AV39ContratoGarantia_DataEncerramento1, AV40ContratoGarantia_DataEncerramento_To1, AV19DynamicFiltersSelector2, AV20ContratoGarantia_DataPagtoGarantia2, AV21ContratoGarantia_DataPagtoGarantia_To2, AV41ContratoGarantia_DataCaucao2, AV42ContratoGarantia_DataCaucao_To2, AV43ContratoGarantia_DataEncerramento2, AV44ContratoGarantia_DataEncerramento_To2, AV23DynamicFiltersSelector3, AV24ContratoGarantia_DataPagtoGarantia3, AV25ContratoGarantia_DataPagtoGarantia_To3, AV45ContratoGarantia_DataCaucao3, AV46ContratoGarantia_DataCaucao_To3, AV47ContratoGarantia_DataEncerramento3, AV48ContratoGarantia_DataEncerramento_To3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV52TFContrato_Numero, AV53TFContrato_Numero_Sel, AV56TFContratada_PessoaNom, AV57TFContratada_PessoaNom_Sel, AV60TFContratada_PessoaCNPJ, AV61TFContratada_PessoaCNPJ_Sel, AV64TFContratoGarantia_DataPagtoGarantia, AV65TFContratoGarantia_DataPagtoGarantia_To, AV70TFContratoGarantia_Percentual, AV71TFContratoGarantia_Percentual_To, AV74TFContratoGarantia_DataCaucao, AV75TFContratoGarantia_DataCaucao_To, AV80TFContratoGarantia_ValorGarantia, AV81TFContratoGarantia_ValorGarantia_To, AV84TFContratoGarantia_DataEncerramento, AV85TFContratoGarantia_DataEncerramento_To, AV90TFContratoGarantia_ValorEncerramento, AV91TFContratoGarantia_ValorEncerramento_To, AV54ddo_Contrato_NumeroTitleControlIdToReplace, AV58ddo_Contratada_PessoaNomTitleControlIdToReplace, AV62ddo_Contratada_PessoaCNPJTitleControlIdToReplace, AV68ddo_ContratoGarantia_DataPagtoGarantiaTitleControlIdToReplace, AV72ddo_ContratoGarantia_PercentualTitleControlIdToReplace, AV78ddo_ContratoGarantia_DataCaucaoTitleControlIdToReplace, AV82ddo_ContratoGarantia_ValorGarantiaTitleControlIdToReplace, AV88ddo_ContratoGarantia_DataEncerramentoTitleControlIdToReplace, AV92ddo_ContratoGarantia_ValorEncerramentoTitleControlIdToReplace, AV157Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A101ContratoGarantia_Codigo, A74Contrato_Codigo, A39Contratada_Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E286H2( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E296H2( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV22DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
      }

      protected void E236H2( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV18DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16ContratoGarantia_DataPagtoGarantia1, AV17ContratoGarantia_DataPagtoGarantia_To1, AV37ContratoGarantia_DataCaucao1, AV38ContratoGarantia_DataCaucao_To1, AV39ContratoGarantia_DataEncerramento1, AV40ContratoGarantia_DataEncerramento_To1, AV19DynamicFiltersSelector2, AV20ContratoGarantia_DataPagtoGarantia2, AV21ContratoGarantia_DataPagtoGarantia_To2, AV41ContratoGarantia_DataCaucao2, AV42ContratoGarantia_DataCaucao_To2, AV43ContratoGarantia_DataEncerramento2, AV44ContratoGarantia_DataEncerramento_To2, AV23DynamicFiltersSelector3, AV24ContratoGarantia_DataPagtoGarantia3, AV25ContratoGarantia_DataPagtoGarantia_To3, AV45ContratoGarantia_DataCaucao3, AV46ContratoGarantia_DataCaucao_To3, AV47ContratoGarantia_DataEncerramento3, AV48ContratoGarantia_DataEncerramento_To3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV52TFContrato_Numero, AV53TFContrato_Numero_Sel, AV56TFContratada_PessoaNom, AV57TFContratada_PessoaNom_Sel, AV60TFContratada_PessoaCNPJ, AV61TFContratada_PessoaCNPJ_Sel, AV64TFContratoGarantia_DataPagtoGarantia, AV65TFContratoGarantia_DataPagtoGarantia_To, AV70TFContratoGarantia_Percentual, AV71TFContratoGarantia_Percentual_To, AV74TFContratoGarantia_DataCaucao, AV75TFContratoGarantia_DataCaucao_To, AV80TFContratoGarantia_ValorGarantia, AV81TFContratoGarantia_ValorGarantia_To, AV84TFContratoGarantia_DataEncerramento, AV85TFContratoGarantia_DataEncerramento_To, AV90TFContratoGarantia_ValorEncerramento, AV91TFContratoGarantia_ValorEncerramento_To, AV54ddo_Contrato_NumeroTitleControlIdToReplace, AV58ddo_Contratada_PessoaNomTitleControlIdToReplace, AV62ddo_Contratada_PessoaCNPJTitleControlIdToReplace, AV68ddo_ContratoGarantia_DataPagtoGarantiaTitleControlIdToReplace, AV72ddo_ContratoGarantia_PercentualTitleControlIdToReplace, AV78ddo_ContratoGarantia_DataCaucaoTitleControlIdToReplace, AV82ddo_ContratoGarantia_ValorGarantiaTitleControlIdToReplace, AV88ddo_ContratoGarantia_DataEncerramentoTitleControlIdToReplace, AV92ddo_ContratoGarantia_ValorEncerramentoTitleControlIdToReplace, AV157Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A101ContratoGarantia_Codigo, A74Contrato_Codigo, A39Contratada_Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E306H2( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E246H2( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV22DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16ContratoGarantia_DataPagtoGarantia1, AV17ContratoGarantia_DataPagtoGarantia_To1, AV37ContratoGarantia_DataCaucao1, AV38ContratoGarantia_DataCaucao_To1, AV39ContratoGarantia_DataEncerramento1, AV40ContratoGarantia_DataEncerramento_To1, AV19DynamicFiltersSelector2, AV20ContratoGarantia_DataPagtoGarantia2, AV21ContratoGarantia_DataPagtoGarantia_To2, AV41ContratoGarantia_DataCaucao2, AV42ContratoGarantia_DataCaucao_To2, AV43ContratoGarantia_DataEncerramento2, AV44ContratoGarantia_DataEncerramento_To2, AV23DynamicFiltersSelector3, AV24ContratoGarantia_DataPagtoGarantia3, AV25ContratoGarantia_DataPagtoGarantia_To3, AV45ContratoGarantia_DataCaucao3, AV46ContratoGarantia_DataCaucao_To3, AV47ContratoGarantia_DataEncerramento3, AV48ContratoGarantia_DataEncerramento_To3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV52TFContrato_Numero, AV53TFContrato_Numero_Sel, AV56TFContratada_PessoaNom, AV57TFContratada_PessoaNom_Sel, AV60TFContratada_PessoaCNPJ, AV61TFContratada_PessoaCNPJ_Sel, AV64TFContratoGarantia_DataPagtoGarantia, AV65TFContratoGarantia_DataPagtoGarantia_To, AV70TFContratoGarantia_Percentual, AV71TFContratoGarantia_Percentual_To, AV74TFContratoGarantia_DataCaucao, AV75TFContratoGarantia_DataCaucao_To, AV80TFContratoGarantia_ValorGarantia, AV81TFContratoGarantia_ValorGarantia_To, AV84TFContratoGarantia_DataEncerramento, AV85TFContratoGarantia_DataEncerramento_To, AV90TFContratoGarantia_ValorEncerramento, AV91TFContratoGarantia_ValorEncerramento_To, AV54ddo_Contrato_NumeroTitleControlIdToReplace, AV58ddo_Contratada_PessoaNomTitleControlIdToReplace, AV62ddo_Contratada_PessoaCNPJTitleControlIdToReplace, AV68ddo_ContratoGarantia_DataPagtoGarantiaTitleControlIdToReplace, AV72ddo_ContratoGarantia_PercentualTitleControlIdToReplace, AV78ddo_ContratoGarantia_DataCaucaoTitleControlIdToReplace, AV82ddo_ContratoGarantia_ValorGarantiaTitleControlIdToReplace, AV88ddo_ContratoGarantia_DataEncerramentoTitleControlIdToReplace, AV92ddo_ContratoGarantia_ValorEncerramentoTitleControlIdToReplace, AV157Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A101ContratoGarantia_Codigo, A74Contrato_Codigo, A39Contratada_Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E316H2( )
      {
         /* Dynamicfiltersselector3_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E256H2( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefresh();
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
      }

      protected void E266H2( )
      {
         /* 'DoInsert' Routine */
         context.wjLoc = formatLink("contratogarantia.aspx") + "?" + UrlEncode(StringUtil.RTrim("INS")) + "," + UrlEncode("" +0) + "," + UrlEncode("" +0);
         context.wjLocDisableFrm = 1;
      }

      protected void S182( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_contrato_numero_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_numero_Internalname, "SortedStatus", Ddo_contrato_numero_Sortedstatus);
         Ddo_contratada_pessoanom_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoanom_Internalname, "SortedStatus", Ddo_contratada_pessoanom_Sortedstatus);
         Ddo_contratada_pessoacnpj_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoacnpj_Internalname, "SortedStatus", Ddo_contratada_pessoacnpj_Sortedstatus);
         Ddo_contratogarantia_datapagtogarantia_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratogarantia_datapagtogarantia_Internalname, "SortedStatus", Ddo_contratogarantia_datapagtogarantia_Sortedstatus);
         Ddo_contratogarantia_percentual_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratogarantia_percentual_Internalname, "SortedStatus", Ddo_contratogarantia_percentual_Sortedstatus);
         Ddo_contratogarantia_datacaucao_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratogarantia_datacaucao_Internalname, "SortedStatus", Ddo_contratogarantia_datacaucao_Sortedstatus);
         Ddo_contratogarantia_valorgarantia_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratogarantia_valorgarantia_Internalname, "SortedStatus", Ddo_contratogarantia_valorgarantia_Sortedstatus);
         Ddo_contratogarantia_dataencerramento_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratogarantia_dataencerramento_Internalname, "SortedStatus", Ddo_contratogarantia_dataencerramento_Sortedstatus);
         Ddo_contratogarantia_valorencerramento_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratogarantia_valorencerramento_Internalname, "SortedStatus", Ddo_contratogarantia_valorencerramento_Sortedstatus);
      }

      protected void S162( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 2 )
         {
            Ddo_contrato_numero_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_numero_Internalname, "SortedStatus", Ddo_contrato_numero_Sortedstatus);
         }
         else if ( AV13OrderedBy == 3 )
         {
            Ddo_contratada_pessoanom_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoanom_Internalname, "SortedStatus", Ddo_contratada_pessoanom_Sortedstatus);
         }
         else if ( AV13OrderedBy == 4 )
         {
            Ddo_contratada_pessoacnpj_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoacnpj_Internalname, "SortedStatus", Ddo_contratada_pessoacnpj_Sortedstatus);
         }
         else if ( AV13OrderedBy == 1 )
         {
            Ddo_contratogarantia_datapagtogarantia_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratogarantia_datapagtogarantia_Internalname, "SortedStatus", Ddo_contratogarantia_datapagtogarantia_Sortedstatus);
         }
         else if ( AV13OrderedBy == 5 )
         {
            Ddo_contratogarantia_percentual_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratogarantia_percentual_Internalname, "SortedStatus", Ddo_contratogarantia_percentual_Sortedstatus);
         }
         else if ( AV13OrderedBy == 6 )
         {
            Ddo_contratogarantia_datacaucao_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratogarantia_datacaucao_Internalname, "SortedStatus", Ddo_contratogarantia_datacaucao_Sortedstatus);
         }
         else if ( AV13OrderedBy == 7 )
         {
            Ddo_contratogarantia_valorgarantia_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratogarantia_valorgarantia_Internalname, "SortedStatus", Ddo_contratogarantia_valorgarantia_Sortedstatus);
         }
         else if ( AV13OrderedBy == 8 )
         {
            Ddo_contratogarantia_dataencerramento_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratogarantia_dataencerramento_Internalname, "SortedStatus", Ddo_contratogarantia_dataencerramento_Sortedstatus);
         }
         else if ( AV13OrderedBy == 9 )
         {
            Ddo_contratogarantia_valorencerramento_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratogarantia_valorencerramento_Internalname, "SortedStatus", Ddo_contratogarantia_valorencerramento_Sortedstatus);
         }
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         tblTablemergeddynamicfilterscontratogarantia_datapagtogarantia1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontratogarantia_datapagtogarantia1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontratogarantia_datapagtogarantia1_Visible), 5, 0)));
         tblTablemergeddynamicfilterscontratogarantia_datacaucao1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontratogarantia_datacaucao1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontratogarantia_datacaucao1_Visible), 5, 0)));
         tblTablemergeddynamicfilterscontratogarantia_dataencerramento1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontratogarantia_dataencerramento1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontratogarantia_dataencerramento1_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOGARANTIA_DATAPAGTOGARANTIA") == 0 )
         {
            tblTablemergeddynamicfilterscontratogarantia_datapagtogarantia1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontratogarantia_datapagtogarantia1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontratogarantia_datapagtogarantia1_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOGARANTIA_DATACAUCAO") == 0 )
         {
            tblTablemergeddynamicfilterscontratogarantia_datacaucao1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontratogarantia_datacaucao1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontratogarantia_datacaucao1_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOGARANTIA_DATAENCERRAMENTO") == 0 )
         {
            tblTablemergeddynamicfilterscontratogarantia_dataencerramento1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontratogarantia_dataencerramento1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontratogarantia_dataencerramento1_Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         tblTablemergeddynamicfilterscontratogarantia_datapagtogarantia2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontratogarantia_datapagtogarantia2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontratogarantia_datapagtogarantia2_Visible), 5, 0)));
         tblTablemergeddynamicfilterscontratogarantia_datacaucao2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontratogarantia_datacaucao2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontratogarantia_datacaucao2_Visible), 5, 0)));
         tblTablemergeddynamicfilterscontratogarantia_dataencerramento2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontratogarantia_dataencerramento2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontratogarantia_dataencerramento2_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATOGARANTIA_DATAPAGTOGARANTIA") == 0 )
         {
            tblTablemergeddynamicfilterscontratogarantia_datapagtogarantia2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontratogarantia_datapagtogarantia2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontratogarantia_datapagtogarantia2_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATOGARANTIA_DATACAUCAO") == 0 )
         {
            tblTablemergeddynamicfilterscontratogarantia_datacaucao2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontratogarantia_datacaucao2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontratogarantia_datacaucao2_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATOGARANTIA_DATAENCERRAMENTO") == 0 )
         {
            tblTablemergeddynamicfilterscontratogarantia_dataencerramento2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontratogarantia_dataencerramento2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontratogarantia_dataencerramento2_Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         tblTablemergeddynamicfilterscontratogarantia_datapagtogarantia3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontratogarantia_datapagtogarantia3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontratogarantia_datapagtogarantia3_Visible), 5, 0)));
         tblTablemergeddynamicfilterscontratogarantia_datacaucao3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontratogarantia_datacaucao3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontratogarantia_datacaucao3_Visible), 5, 0)));
         tblTablemergeddynamicfilterscontratogarantia_dataencerramento3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontratogarantia_dataencerramento3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontratogarantia_dataencerramento3_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTRATOGARANTIA_DATAPAGTOGARANTIA") == 0 )
         {
            tblTablemergeddynamicfilterscontratogarantia_datapagtogarantia3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontratogarantia_datapagtogarantia3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontratogarantia_datapagtogarantia3_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTRATOGARANTIA_DATACAUCAO") == 0 )
         {
            tblTablemergeddynamicfilterscontratogarantia_datacaucao3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontratogarantia_datacaucao3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontratogarantia_datacaucao3_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTRATOGARANTIA_DATAENCERRAMENTO") == 0 )
         {
            tblTablemergeddynamicfilterscontratogarantia_dataencerramento3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontratogarantia_dataencerramento3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontratogarantia_dataencerramento3_Visible), 5, 0)));
         }
      }

      protected void S202( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV18DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         AV19DynamicFiltersSelector2 = "CONTRATOGARANTIA_DATAPAGTOGARANTIA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         AV20ContratoGarantia_DataPagtoGarantia2 = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20ContratoGarantia_DataPagtoGarantia2", context.localUtil.Format(AV20ContratoGarantia_DataPagtoGarantia2, "99/99/99"));
         AV21ContratoGarantia_DataPagtoGarantia_To2 = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ContratoGarantia_DataPagtoGarantia_To2", context.localUtil.Format(AV21ContratoGarantia_DataPagtoGarantia_To2, "99/99/99"));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV22DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         AV23DynamicFiltersSelector3 = "CONTRATOGARANTIA_DATAPAGTOGARANTIA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         AV24ContratoGarantia_DataPagtoGarantia3 = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24ContratoGarantia_DataPagtoGarantia3", context.localUtil.Format(AV24ContratoGarantia_DataPagtoGarantia3, "99/99/99"));
         AV25ContratoGarantia_DataPagtoGarantia_To3 = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ContratoGarantia_DataPagtoGarantia_To3", context.localUtil.Format(AV25ContratoGarantia_DataPagtoGarantia_To3, "99/99/99"));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S222( )
      {
         /* 'CLEANFILTERS' Routine */
         AV52TFContrato_Numero = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52TFContrato_Numero", AV52TFContrato_Numero);
         Ddo_contrato_numero_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_numero_Internalname, "FilteredText_set", Ddo_contrato_numero_Filteredtext_set);
         AV53TFContrato_Numero_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53TFContrato_Numero_Sel", AV53TFContrato_Numero_Sel);
         Ddo_contrato_numero_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_numero_Internalname, "SelectedValue_set", Ddo_contrato_numero_Selectedvalue_set);
         AV56TFContratada_PessoaNom = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFContratada_PessoaNom", AV56TFContratada_PessoaNom);
         Ddo_contratada_pessoanom_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoanom_Internalname, "FilteredText_set", Ddo_contratada_pessoanom_Filteredtext_set);
         AV57TFContratada_PessoaNom_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57TFContratada_PessoaNom_Sel", AV57TFContratada_PessoaNom_Sel);
         Ddo_contratada_pessoanom_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoanom_Internalname, "SelectedValue_set", Ddo_contratada_pessoanom_Selectedvalue_set);
         AV60TFContratada_PessoaCNPJ = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFContratada_PessoaCNPJ", AV60TFContratada_PessoaCNPJ);
         Ddo_contratada_pessoacnpj_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoacnpj_Internalname, "FilteredText_set", Ddo_contratada_pessoacnpj_Filteredtext_set);
         AV61TFContratada_PessoaCNPJ_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61TFContratada_PessoaCNPJ_Sel", AV61TFContratada_PessoaCNPJ_Sel);
         Ddo_contratada_pessoacnpj_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoacnpj_Internalname, "SelectedValue_set", Ddo_contratada_pessoacnpj_Selectedvalue_set);
         AV64TFContratoGarantia_DataPagtoGarantia = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64TFContratoGarantia_DataPagtoGarantia", context.localUtil.Format(AV64TFContratoGarantia_DataPagtoGarantia, "99/99/99"));
         Ddo_contratogarantia_datapagtogarantia_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratogarantia_datapagtogarantia_Internalname, "FilteredText_set", Ddo_contratogarantia_datapagtogarantia_Filteredtext_set);
         AV65TFContratoGarantia_DataPagtoGarantia_To = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65TFContratoGarantia_DataPagtoGarantia_To", context.localUtil.Format(AV65TFContratoGarantia_DataPagtoGarantia_To, "99/99/99"));
         Ddo_contratogarantia_datapagtogarantia_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratogarantia_datapagtogarantia_Internalname, "FilteredTextTo_set", Ddo_contratogarantia_datapagtogarantia_Filteredtextto_set);
         AV70TFContratoGarantia_Percentual = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70TFContratoGarantia_Percentual", StringUtil.LTrim( StringUtil.Str( AV70TFContratoGarantia_Percentual, 6, 2)));
         Ddo_contratogarantia_percentual_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratogarantia_percentual_Internalname, "FilteredText_set", Ddo_contratogarantia_percentual_Filteredtext_set);
         AV71TFContratoGarantia_Percentual_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71TFContratoGarantia_Percentual_To", StringUtil.LTrim( StringUtil.Str( AV71TFContratoGarantia_Percentual_To, 6, 2)));
         Ddo_contratogarantia_percentual_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratogarantia_percentual_Internalname, "FilteredTextTo_set", Ddo_contratogarantia_percentual_Filteredtextto_set);
         AV74TFContratoGarantia_DataCaucao = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74TFContratoGarantia_DataCaucao", context.localUtil.Format(AV74TFContratoGarantia_DataCaucao, "99/99/99"));
         Ddo_contratogarantia_datacaucao_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratogarantia_datacaucao_Internalname, "FilteredText_set", Ddo_contratogarantia_datacaucao_Filteredtext_set);
         AV75TFContratoGarantia_DataCaucao_To = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75TFContratoGarantia_DataCaucao_To", context.localUtil.Format(AV75TFContratoGarantia_DataCaucao_To, "99/99/99"));
         Ddo_contratogarantia_datacaucao_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratogarantia_datacaucao_Internalname, "FilteredTextTo_set", Ddo_contratogarantia_datacaucao_Filteredtextto_set);
         AV80TFContratoGarantia_ValorGarantia = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80TFContratoGarantia_ValorGarantia", StringUtil.LTrim( StringUtil.Str( AV80TFContratoGarantia_ValorGarantia, 18, 5)));
         Ddo_contratogarantia_valorgarantia_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratogarantia_valorgarantia_Internalname, "FilteredText_set", Ddo_contratogarantia_valorgarantia_Filteredtext_set);
         AV81TFContratoGarantia_ValorGarantia_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81TFContratoGarantia_ValorGarantia_To", StringUtil.LTrim( StringUtil.Str( AV81TFContratoGarantia_ValorGarantia_To, 18, 5)));
         Ddo_contratogarantia_valorgarantia_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratogarantia_valorgarantia_Internalname, "FilteredTextTo_set", Ddo_contratogarantia_valorgarantia_Filteredtextto_set);
         AV84TFContratoGarantia_DataEncerramento = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV84TFContratoGarantia_DataEncerramento", context.localUtil.Format(AV84TFContratoGarantia_DataEncerramento, "99/99/99"));
         Ddo_contratogarantia_dataencerramento_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratogarantia_dataencerramento_Internalname, "FilteredText_set", Ddo_contratogarantia_dataencerramento_Filteredtext_set);
         AV85TFContratoGarantia_DataEncerramento_To = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV85TFContratoGarantia_DataEncerramento_To", context.localUtil.Format(AV85TFContratoGarantia_DataEncerramento_To, "99/99/99"));
         Ddo_contratogarantia_dataencerramento_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratogarantia_dataencerramento_Internalname, "FilteredTextTo_set", Ddo_contratogarantia_dataencerramento_Filteredtextto_set);
         AV90TFContratoGarantia_ValorEncerramento = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV90TFContratoGarantia_ValorEncerramento", StringUtil.LTrim( StringUtil.Str( AV90TFContratoGarantia_ValorEncerramento, 18, 5)));
         Ddo_contratogarantia_valorencerramento_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratogarantia_valorencerramento_Internalname, "FilteredText_set", Ddo_contratogarantia_valorencerramento_Filteredtext_set);
         AV91TFContratoGarantia_ValorEncerramento_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV91TFContratoGarantia_ValorEncerramento_To", StringUtil.LTrim( StringUtil.Str( AV91TFContratoGarantia_ValorEncerramento_To, 18, 5)));
         Ddo_contratogarantia_valorencerramento_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratogarantia_valorencerramento_Internalname, "FilteredTextTo_set", Ddo_contratogarantia_valorencerramento_Filteredtextto_set);
         AV15DynamicFiltersSelector1 = "CONTRATOGARANTIA_DATAPAGTOGARANTIA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         AV16ContratoGarantia_DataPagtoGarantia1 = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16ContratoGarantia_DataPagtoGarantia1", context.localUtil.Format(AV16ContratoGarantia_DataPagtoGarantia1, "99/99/99"));
         AV17ContratoGarantia_DataPagtoGarantia_To1 = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContratoGarantia_DataPagtoGarantia_To1", context.localUtil.Format(AV17ContratoGarantia_DataPagtoGarantia_To1, "99/99/99"));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S152( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV30Session.Get(AV157Pgmname+"GridState"), "") == 0 )
         {
            AV10GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV157Pgmname+"GridState"), "");
         }
         else
         {
            AV10GridState.FromXml(AV30Session.Get(AV157Pgmname+"GridState"), "");
         }
         AV13OrderedBy = AV10GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         AV14OrderedDsc = AV10GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADREGFILTERSSTATE' */
         S232 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_gotopage( AV10GridState.gxTpr_Currentpage) ;
      }

      protected void S232( )
      {
         /* 'LOADREGFILTERSSTATE' Routine */
         AV158GXV1 = 1;
         while ( AV158GXV1 <= AV10GridState.gxTpr_Filtervalues.Count )
         {
            AV11GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV10GridState.gxTpr_Filtervalues.Item(AV158GXV1));
            if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATO_NUMERO") == 0 )
            {
               AV52TFContrato_Numero = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52TFContrato_Numero", AV52TFContrato_Numero);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52TFContrato_Numero)) )
               {
                  Ddo_contrato_numero_Filteredtext_set = AV52TFContrato_Numero;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_numero_Internalname, "FilteredText_set", Ddo_contrato_numero_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATO_NUMERO_SEL") == 0 )
            {
               AV53TFContrato_Numero_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53TFContrato_Numero_Sel", AV53TFContrato_Numero_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53TFContrato_Numero_Sel)) )
               {
                  Ddo_contrato_numero_Selectedvalue_set = AV53TFContrato_Numero_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_numero_Internalname, "SelectedValue_set", Ddo_contrato_numero_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATADA_PESSOANOM") == 0 )
            {
               AV56TFContratada_PessoaNom = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFContratada_PessoaNom", AV56TFContratada_PessoaNom);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56TFContratada_PessoaNom)) )
               {
                  Ddo_contratada_pessoanom_Filteredtext_set = AV56TFContratada_PessoaNom;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoanom_Internalname, "FilteredText_set", Ddo_contratada_pessoanom_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATADA_PESSOANOM_SEL") == 0 )
            {
               AV57TFContratada_PessoaNom_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57TFContratada_PessoaNom_Sel", AV57TFContratada_PessoaNom_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV57TFContratada_PessoaNom_Sel)) )
               {
                  Ddo_contratada_pessoanom_Selectedvalue_set = AV57TFContratada_PessoaNom_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoanom_Internalname, "SelectedValue_set", Ddo_contratada_pessoanom_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATADA_PESSOACNPJ") == 0 )
            {
               AV60TFContratada_PessoaCNPJ = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFContratada_PessoaCNPJ", AV60TFContratada_PessoaCNPJ);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60TFContratada_PessoaCNPJ)) )
               {
                  Ddo_contratada_pessoacnpj_Filteredtext_set = AV60TFContratada_PessoaCNPJ;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoacnpj_Internalname, "FilteredText_set", Ddo_contratada_pessoacnpj_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATADA_PESSOACNPJ_SEL") == 0 )
            {
               AV61TFContratada_PessoaCNPJ_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61TFContratada_PessoaCNPJ_Sel", AV61TFContratada_PessoaCNPJ_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61TFContratada_PessoaCNPJ_Sel)) )
               {
                  Ddo_contratada_pessoacnpj_Selectedvalue_set = AV61TFContratada_PessoaCNPJ_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_pessoacnpj_Internalname, "SelectedValue_set", Ddo_contratada_pessoacnpj_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATOGARANTIA_DATAPAGTOGARANTIA") == 0 )
            {
               AV64TFContratoGarantia_DataPagtoGarantia = context.localUtil.CToD( AV11GridStateFilterValue.gxTpr_Value, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64TFContratoGarantia_DataPagtoGarantia", context.localUtil.Format(AV64TFContratoGarantia_DataPagtoGarantia, "99/99/99"));
               AV65TFContratoGarantia_DataPagtoGarantia_To = context.localUtil.CToD( AV11GridStateFilterValue.gxTpr_Valueto, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65TFContratoGarantia_DataPagtoGarantia_To", context.localUtil.Format(AV65TFContratoGarantia_DataPagtoGarantia_To, "99/99/99"));
               if ( ! (DateTime.MinValue==AV64TFContratoGarantia_DataPagtoGarantia) )
               {
                  Ddo_contratogarantia_datapagtogarantia_Filteredtext_set = context.localUtil.DToC( AV64TFContratoGarantia_DataPagtoGarantia, 2, "/");
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratogarantia_datapagtogarantia_Internalname, "FilteredText_set", Ddo_contratogarantia_datapagtogarantia_Filteredtext_set);
               }
               if ( ! (DateTime.MinValue==AV65TFContratoGarantia_DataPagtoGarantia_To) )
               {
                  Ddo_contratogarantia_datapagtogarantia_Filteredtextto_set = context.localUtil.DToC( AV65TFContratoGarantia_DataPagtoGarantia_To, 2, "/");
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratogarantia_datapagtogarantia_Internalname, "FilteredTextTo_set", Ddo_contratogarantia_datapagtogarantia_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATOGARANTIA_PERCENTUAL") == 0 )
            {
               AV70TFContratoGarantia_Percentual = NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70TFContratoGarantia_Percentual", StringUtil.LTrim( StringUtil.Str( AV70TFContratoGarantia_Percentual, 6, 2)));
               AV71TFContratoGarantia_Percentual_To = NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Valueto, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71TFContratoGarantia_Percentual_To", StringUtil.LTrim( StringUtil.Str( AV71TFContratoGarantia_Percentual_To, 6, 2)));
               if ( ! (Convert.ToDecimal(0)==AV70TFContratoGarantia_Percentual) )
               {
                  Ddo_contratogarantia_percentual_Filteredtext_set = StringUtil.Str( AV70TFContratoGarantia_Percentual, 6, 2);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratogarantia_percentual_Internalname, "FilteredText_set", Ddo_contratogarantia_percentual_Filteredtext_set);
               }
               if ( ! (Convert.ToDecimal(0)==AV71TFContratoGarantia_Percentual_To) )
               {
                  Ddo_contratogarantia_percentual_Filteredtextto_set = StringUtil.Str( AV71TFContratoGarantia_Percentual_To, 6, 2);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratogarantia_percentual_Internalname, "FilteredTextTo_set", Ddo_contratogarantia_percentual_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATOGARANTIA_DATACAUCAO") == 0 )
            {
               AV74TFContratoGarantia_DataCaucao = context.localUtil.CToD( AV11GridStateFilterValue.gxTpr_Value, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74TFContratoGarantia_DataCaucao", context.localUtil.Format(AV74TFContratoGarantia_DataCaucao, "99/99/99"));
               AV75TFContratoGarantia_DataCaucao_To = context.localUtil.CToD( AV11GridStateFilterValue.gxTpr_Valueto, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75TFContratoGarantia_DataCaucao_To", context.localUtil.Format(AV75TFContratoGarantia_DataCaucao_To, "99/99/99"));
               if ( ! (DateTime.MinValue==AV74TFContratoGarantia_DataCaucao) )
               {
                  Ddo_contratogarantia_datacaucao_Filteredtext_set = context.localUtil.DToC( AV74TFContratoGarantia_DataCaucao, 2, "/");
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratogarantia_datacaucao_Internalname, "FilteredText_set", Ddo_contratogarantia_datacaucao_Filteredtext_set);
               }
               if ( ! (DateTime.MinValue==AV75TFContratoGarantia_DataCaucao_To) )
               {
                  Ddo_contratogarantia_datacaucao_Filteredtextto_set = context.localUtil.DToC( AV75TFContratoGarantia_DataCaucao_To, 2, "/");
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratogarantia_datacaucao_Internalname, "FilteredTextTo_set", Ddo_contratogarantia_datacaucao_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATOGARANTIA_VALORGARANTIA") == 0 )
            {
               AV80TFContratoGarantia_ValorGarantia = NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80TFContratoGarantia_ValorGarantia", StringUtil.LTrim( StringUtil.Str( AV80TFContratoGarantia_ValorGarantia, 18, 5)));
               AV81TFContratoGarantia_ValorGarantia_To = NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Valueto, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81TFContratoGarantia_ValorGarantia_To", StringUtil.LTrim( StringUtil.Str( AV81TFContratoGarantia_ValorGarantia_To, 18, 5)));
               if ( ! (Convert.ToDecimal(0)==AV80TFContratoGarantia_ValorGarantia) )
               {
                  Ddo_contratogarantia_valorgarantia_Filteredtext_set = StringUtil.Str( AV80TFContratoGarantia_ValorGarantia, 18, 5);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratogarantia_valorgarantia_Internalname, "FilteredText_set", Ddo_contratogarantia_valorgarantia_Filteredtext_set);
               }
               if ( ! (Convert.ToDecimal(0)==AV81TFContratoGarantia_ValorGarantia_To) )
               {
                  Ddo_contratogarantia_valorgarantia_Filteredtextto_set = StringUtil.Str( AV81TFContratoGarantia_ValorGarantia_To, 18, 5);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratogarantia_valorgarantia_Internalname, "FilteredTextTo_set", Ddo_contratogarantia_valorgarantia_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATOGARANTIA_DATAENCERRAMENTO") == 0 )
            {
               AV84TFContratoGarantia_DataEncerramento = context.localUtil.CToD( AV11GridStateFilterValue.gxTpr_Value, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV84TFContratoGarantia_DataEncerramento", context.localUtil.Format(AV84TFContratoGarantia_DataEncerramento, "99/99/99"));
               AV85TFContratoGarantia_DataEncerramento_To = context.localUtil.CToD( AV11GridStateFilterValue.gxTpr_Valueto, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV85TFContratoGarantia_DataEncerramento_To", context.localUtil.Format(AV85TFContratoGarantia_DataEncerramento_To, "99/99/99"));
               if ( ! (DateTime.MinValue==AV84TFContratoGarantia_DataEncerramento) )
               {
                  Ddo_contratogarantia_dataencerramento_Filteredtext_set = context.localUtil.DToC( AV84TFContratoGarantia_DataEncerramento, 2, "/");
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratogarantia_dataencerramento_Internalname, "FilteredText_set", Ddo_contratogarantia_dataencerramento_Filteredtext_set);
               }
               if ( ! (DateTime.MinValue==AV85TFContratoGarantia_DataEncerramento_To) )
               {
                  Ddo_contratogarantia_dataencerramento_Filteredtextto_set = context.localUtil.DToC( AV85TFContratoGarantia_DataEncerramento_To, 2, "/");
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratogarantia_dataencerramento_Internalname, "FilteredTextTo_set", Ddo_contratogarantia_dataencerramento_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATOGARANTIA_VALORENCERRAMENTO") == 0 )
            {
               AV90TFContratoGarantia_ValorEncerramento = NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV90TFContratoGarantia_ValorEncerramento", StringUtil.LTrim( StringUtil.Str( AV90TFContratoGarantia_ValorEncerramento, 18, 5)));
               AV91TFContratoGarantia_ValorEncerramento_To = NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Valueto, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV91TFContratoGarantia_ValorEncerramento_To", StringUtil.LTrim( StringUtil.Str( AV91TFContratoGarantia_ValorEncerramento_To, 18, 5)));
               if ( ! (Convert.ToDecimal(0)==AV90TFContratoGarantia_ValorEncerramento) )
               {
                  Ddo_contratogarantia_valorencerramento_Filteredtext_set = StringUtil.Str( AV90TFContratoGarantia_ValorEncerramento, 18, 5);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratogarantia_valorencerramento_Internalname, "FilteredText_set", Ddo_contratogarantia_valorencerramento_Filteredtext_set);
               }
               if ( ! (Convert.ToDecimal(0)==AV91TFContratoGarantia_ValorEncerramento_To) )
               {
                  Ddo_contratogarantia_valorencerramento_Filteredtextto_set = StringUtil.Str( AV91TFContratoGarantia_ValorEncerramento_To, 18, 5);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratogarantia_valorencerramento_Internalname, "FilteredTextTo_set", Ddo_contratogarantia_valorencerramento_Filteredtextto_set);
               }
            }
            AV158GXV1 = (int)(AV158GXV1+1);
         }
      }

      protected void S212( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV15DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOGARANTIA_DATAPAGTOGARANTIA") == 0 )
            {
               AV16ContratoGarantia_DataPagtoGarantia1 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Value, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16ContratoGarantia_DataPagtoGarantia1", context.localUtil.Format(AV16ContratoGarantia_DataPagtoGarantia1, "99/99/99"));
               AV17ContratoGarantia_DataPagtoGarantia_To1 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContratoGarantia_DataPagtoGarantia_To1", context.localUtil.Format(AV17ContratoGarantia_DataPagtoGarantia_To1, "99/99/99"));
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOGARANTIA_DATACAUCAO") == 0 )
            {
               AV37ContratoGarantia_DataCaucao1 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Value, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37ContratoGarantia_DataCaucao1", context.localUtil.Format(AV37ContratoGarantia_DataCaucao1, "99/99/99"));
               AV38ContratoGarantia_DataCaucao_To1 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38ContratoGarantia_DataCaucao_To1", context.localUtil.Format(AV38ContratoGarantia_DataCaucao_To1, "99/99/99"));
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOGARANTIA_DATAENCERRAMENTO") == 0 )
            {
               AV39ContratoGarantia_DataEncerramento1 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Value, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39ContratoGarantia_DataEncerramento1", context.localUtil.Format(AV39ContratoGarantia_DataEncerramento1, "99/99/99"));
               AV40ContratoGarantia_DataEncerramento_To1 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40ContratoGarantia_DataEncerramento_To1", context.localUtil.Format(AV40ContratoGarantia_DataEncerramento_To1, "99/99/99"));
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV18DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
               AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(2));
               AV19DynamicFiltersSelector2 = AV12GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATOGARANTIA_DATAPAGTOGARANTIA") == 0 )
               {
                  AV20ContratoGarantia_DataPagtoGarantia2 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Value, 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20ContratoGarantia_DataPagtoGarantia2", context.localUtil.Format(AV20ContratoGarantia_DataPagtoGarantia2, "99/99/99"));
                  AV21ContratoGarantia_DataPagtoGarantia_To2 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ContratoGarantia_DataPagtoGarantia_To2", context.localUtil.Format(AV21ContratoGarantia_DataPagtoGarantia_To2, "99/99/99"));
               }
               else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATOGARANTIA_DATACAUCAO") == 0 )
               {
                  AV41ContratoGarantia_DataCaucao2 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Value, 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41ContratoGarantia_DataCaucao2", context.localUtil.Format(AV41ContratoGarantia_DataCaucao2, "99/99/99"));
                  AV42ContratoGarantia_DataCaucao_To2 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42ContratoGarantia_DataCaucao_To2", context.localUtil.Format(AV42ContratoGarantia_DataCaucao_To2, "99/99/99"));
               }
               else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATOGARANTIA_DATAENCERRAMENTO") == 0 )
               {
                  AV43ContratoGarantia_DataEncerramento2 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Value, 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43ContratoGarantia_DataEncerramento2", context.localUtil.Format(AV43ContratoGarantia_DataEncerramento2, "99/99/99"));
                  AV44ContratoGarantia_DataEncerramento_To2 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44ContratoGarantia_DataEncerramento_To2", context.localUtil.Format(AV44ContratoGarantia_DataEncerramento_To2, "99/99/99"));
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV22DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
                  AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(3));
                  AV23DynamicFiltersSelector3 = AV12GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTRATOGARANTIA_DATAPAGTOGARANTIA") == 0 )
                  {
                     AV24ContratoGarantia_DataPagtoGarantia3 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Value, 2);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24ContratoGarantia_DataPagtoGarantia3", context.localUtil.Format(AV24ContratoGarantia_DataPagtoGarantia3, "99/99/99"));
                     AV25ContratoGarantia_DataPagtoGarantia_To3 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ContratoGarantia_DataPagtoGarantia_To3", context.localUtil.Format(AV25ContratoGarantia_DataPagtoGarantia_To3, "99/99/99"));
                  }
                  else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTRATOGARANTIA_DATACAUCAO") == 0 )
                  {
                     AV45ContratoGarantia_DataCaucao3 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Value, 2);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45ContratoGarantia_DataCaucao3", context.localUtil.Format(AV45ContratoGarantia_DataCaucao3, "99/99/99"));
                     AV46ContratoGarantia_DataCaucao_To3 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46ContratoGarantia_DataCaucao_To3", context.localUtil.Format(AV46ContratoGarantia_DataCaucao_To3, "99/99/99"));
                  }
                  else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTRATOGARANTIA_DATAENCERRAMENTO") == 0 )
                  {
                     AV47ContratoGarantia_DataEncerramento3 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Value, 2);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47ContratoGarantia_DataEncerramento3", context.localUtil.Format(AV47ContratoGarantia_DataEncerramento3, "99/99/99"));
                     AV48ContratoGarantia_DataEncerramento_To3 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48ContratoGarantia_DataEncerramento_To3", context.localUtil.Format(AV48ContratoGarantia_DataEncerramento_To3, "99/99/99"));
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S132 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV26DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S172( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV10GridState.FromXml(AV30Session.Get(AV157Pgmname+"GridState"), "");
         AV10GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV10GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV10GridState.gxTpr_Filtervalues.Clear();
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52TFContrato_Numero)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATO_NUMERO";
            AV11GridStateFilterValue.gxTpr_Value = AV52TFContrato_Numero;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53TFContrato_Numero_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATO_NUMERO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV53TFContrato_Numero_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56TFContratada_PessoaNom)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATADA_PESSOANOM";
            AV11GridStateFilterValue.gxTpr_Value = AV56TFContratada_PessoaNom;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV57TFContratada_PessoaNom_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATADA_PESSOANOM_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV57TFContratada_PessoaNom_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60TFContratada_PessoaCNPJ)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATADA_PESSOACNPJ";
            AV11GridStateFilterValue.gxTpr_Value = AV60TFContratada_PessoaCNPJ;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61TFContratada_PessoaCNPJ_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATADA_PESSOACNPJ_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV61TFContratada_PessoaCNPJ_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (DateTime.MinValue==AV64TFContratoGarantia_DataPagtoGarantia) && (DateTime.MinValue==AV65TFContratoGarantia_DataPagtoGarantia_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOGARANTIA_DATAPAGTOGARANTIA";
            AV11GridStateFilterValue.gxTpr_Value = context.localUtil.DToC( AV64TFContratoGarantia_DataPagtoGarantia, 2, "/");
            AV11GridStateFilterValue.gxTpr_Valueto = context.localUtil.DToC( AV65TFContratoGarantia_DataPagtoGarantia_To, 2, "/");
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (Convert.ToDecimal(0)==AV70TFContratoGarantia_Percentual) && (Convert.ToDecimal(0)==AV71TFContratoGarantia_Percentual_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOGARANTIA_PERCENTUAL";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( AV70TFContratoGarantia_Percentual, 6, 2);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( AV71TFContratoGarantia_Percentual_To, 6, 2);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (DateTime.MinValue==AV74TFContratoGarantia_DataCaucao) && (DateTime.MinValue==AV75TFContratoGarantia_DataCaucao_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOGARANTIA_DATACAUCAO";
            AV11GridStateFilterValue.gxTpr_Value = context.localUtil.DToC( AV74TFContratoGarantia_DataCaucao, 2, "/");
            AV11GridStateFilterValue.gxTpr_Valueto = context.localUtil.DToC( AV75TFContratoGarantia_DataCaucao_To, 2, "/");
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (Convert.ToDecimal(0)==AV80TFContratoGarantia_ValorGarantia) && (Convert.ToDecimal(0)==AV81TFContratoGarantia_ValorGarantia_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOGARANTIA_VALORGARANTIA";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( AV80TFContratoGarantia_ValorGarantia, 18, 5);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( AV81TFContratoGarantia_ValorGarantia_To, 18, 5);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (DateTime.MinValue==AV84TFContratoGarantia_DataEncerramento) && (DateTime.MinValue==AV85TFContratoGarantia_DataEncerramento_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOGARANTIA_DATAENCERRAMENTO";
            AV11GridStateFilterValue.gxTpr_Value = context.localUtil.DToC( AV84TFContratoGarantia_DataEncerramento, 2, "/");
            AV11GridStateFilterValue.gxTpr_Valueto = context.localUtil.DToC( AV85TFContratoGarantia_DataEncerramento_To, 2, "/");
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (Convert.ToDecimal(0)==AV90TFContratoGarantia_ValorEncerramento) && (Convert.ToDecimal(0)==AV91TFContratoGarantia_ValorEncerramento_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATOGARANTIA_VALORENCERRAMENTO";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( AV90TFContratoGarantia_ValorEncerramento, 18, 5);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( AV91TFContratoGarantia_ValorEncerramento_To, 18, 5);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Currentpage = (short)(subGrid_Currentpage( ));
         new wwpbaseobjects.savegridstate(context ).execute(  AV157Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_MeetrikaVs3")) ;
      }

      protected void S192( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV27DynamicFiltersIgnoreFirst )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV15DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOGARANTIA_DATAPAGTOGARANTIA") == 0 ) && ! ( (DateTime.MinValue==AV16ContratoGarantia_DataPagtoGarantia1) && (DateTime.MinValue==AV17ContratoGarantia_DataPagtoGarantia_To1) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.DToC( AV16ContratoGarantia_DataPagtoGarantia1, 2, "/");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.DToC( AV17ContratoGarantia_DataPagtoGarantia_To1, 2, "/");
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOGARANTIA_DATACAUCAO") == 0 ) && ! ( (DateTime.MinValue==AV37ContratoGarantia_DataCaucao1) && (DateTime.MinValue==AV38ContratoGarantia_DataCaucao_To1) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.DToC( AV37ContratoGarantia_DataCaucao1, 2, "/");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.DToC( AV38ContratoGarantia_DataCaucao_To1, 2, "/");
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOGARANTIA_DATAENCERRAMENTO") == 0 ) && ! ( (DateTime.MinValue==AV39ContratoGarantia_DataEncerramento1) && (DateTime.MinValue==AV40ContratoGarantia_DataEncerramento_To1) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.DToC( AV39ContratoGarantia_DataEncerramento1, 2, "/");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.DToC( AV40ContratoGarantia_DataEncerramento_To1, 2, "/");
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Valueto)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV18DynamicFiltersEnabled2 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV19DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATOGARANTIA_DATAPAGTOGARANTIA") == 0 ) && ! ( (DateTime.MinValue==AV20ContratoGarantia_DataPagtoGarantia2) && (DateTime.MinValue==AV21ContratoGarantia_DataPagtoGarantia_To2) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.DToC( AV20ContratoGarantia_DataPagtoGarantia2, 2, "/");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.DToC( AV21ContratoGarantia_DataPagtoGarantia_To2, 2, "/");
            }
            else if ( ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATOGARANTIA_DATACAUCAO") == 0 ) && ! ( (DateTime.MinValue==AV41ContratoGarantia_DataCaucao2) && (DateTime.MinValue==AV42ContratoGarantia_DataCaucao_To2) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.DToC( AV41ContratoGarantia_DataCaucao2, 2, "/");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.DToC( AV42ContratoGarantia_DataCaucao_To2, 2, "/");
            }
            else if ( ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATOGARANTIA_DATAENCERRAMENTO") == 0 ) && ! ( (DateTime.MinValue==AV43ContratoGarantia_DataEncerramento2) && (DateTime.MinValue==AV44ContratoGarantia_DataEncerramento_To2) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.DToC( AV43ContratoGarantia_DataEncerramento2, 2, "/");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.DToC( AV44ContratoGarantia_DataEncerramento_To2, 2, "/");
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Valueto)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV22DynamicFiltersEnabled3 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV23DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTRATOGARANTIA_DATAPAGTOGARANTIA") == 0 ) && ! ( (DateTime.MinValue==AV24ContratoGarantia_DataPagtoGarantia3) && (DateTime.MinValue==AV25ContratoGarantia_DataPagtoGarantia_To3) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.DToC( AV24ContratoGarantia_DataPagtoGarantia3, 2, "/");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.DToC( AV25ContratoGarantia_DataPagtoGarantia_To3, 2, "/");
            }
            else if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTRATOGARANTIA_DATACAUCAO") == 0 ) && ! ( (DateTime.MinValue==AV45ContratoGarantia_DataCaucao3) && (DateTime.MinValue==AV46ContratoGarantia_DataCaucao_To3) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.DToC( AV45ContratoGarantia_DataCaucao3, 2, "/");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.DToC( AV46ContratoGarantia_DataCaucao_To3, 2, "/");
            }
            else if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTRATOGARANTIA_DATAENCERRAMENTO") == 0 ) && ! ( (DateTime.MinValue==AV47ContratoGarantia_DataEncerramento3) && (DateTime.MinValue==AV48ContratoGarantia_DataEncerramento_To3) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.DToC( AV47ContratoGarantia_DataEncerramento3, 2, "/");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.DToC( AV48ContratoGarantia_DataEncerramento_To3, 2, "/");
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Valueto)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
      }

      protected void S142( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV157Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = true;
         AV8TrnContext.gxTpr_Callerurl = AV7HTTPRequest.ScriptName+"?"+AV7HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "ContratoGarantia";
         AV30Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_MeetrikaVs3"));
      }

      protected void wb_table1_2_6H2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_6H2( true) ;
         }
         else
         {
            wb_table2_8_6H2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_6H2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_136_6H2( true) ;
         }
         else
         {
            wb_table3_136_6H2( false) ;
         }
         return  ;
      }

      protected void wb_table3_136_6H2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_6H2e( true) ;
         }
         else
         {
            wb_table1_2_6H2e( false) ;
         }
      }

      protected void wb_table3_136_6H2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "TableGridHeader", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_139_6H2( true) ;
         }
         else
         {
            wb_table4_139_6H2( false) ;
         }
         return  ;
      }

      protected void wb_table4_139_6H2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_136_6H2e( true) ;
         }
         else
         {
            wb_table3_136_6H2e( false) ;
         }
      }

      protected void wb_table4_139_6H2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"142\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(110), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContrato_Numero_Titleformat == 0 )
               {
                  context.SendWebValue( edtContrato_Numero_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContrato_Numero_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratada_PessoaNom_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratada_PessoaNom_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratada_PessoaNom_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratada_PessoaCNPJ_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratada_PessoaCNPJ_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratada_PessoaCNPJ_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoGarantia_DataPagtoGarantia_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoGarantia_DataPagtoGarantia_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoGarantia_DataPagtoGarantia_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoGarantia_Percentual_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoGarantia_Percentual_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoGarantia_Percentual_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoGarantia_DataCaucao_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoGarantia_DataCaucao_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoGarantia_DataCaucao_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoGarantia_ValorGarantia_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoGarantia_ValorGarantia_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoGarantia_ValorGarantia_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoGarantia_DataEncerramento_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoGarantia_DataEncerramento_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoGarantia_DataEncerramento_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoGarantia_ValorEncerramento_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoGarantia_ValorEncerramento_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoGarantia_ValorEncerramento_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV28Update));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavUpdate_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV29Delete));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDelete_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A77Contrato_Numero));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContrato_Numero_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContrato_Numero_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A41Contratada_PessoaNom));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratada_PessoaNom_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratada_PessoaNom_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtContratada_PessoaNom_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A42Contratada_PessoaCNPJ);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratada_PessoaCNPJ_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratada_PessoaCNPJ_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.Format(A102ContratoGarantia_DataPagtoGarantia, "99/99/99"));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoGarantia_DataPagtoGarantia_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoGarantia_DataPagtoGarantia_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtContratoGarantia_DataPagtoGarantia_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A103ContratoGarantia_Percentual, 6, 2, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoGarantia_Percentual_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoGarantia_Percentual_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.Format(A104ContratoGarantia_DataCaucao, "99/99/99"));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoGarantia_DataCaucao_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoGarantia_DataCaucao_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A105ContratoGarantia_ValorGarantia, 18, 5, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoGarantia_ValorGarantia_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoGarantia_ValorGarantia_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.Format(A106ContratoGarantia_DataEncerramento, "99/99/99"));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoGarantia_DataEncerramento_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoGarantia_DataEncerramento_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A107ContratoGarantia_ValorEncerramento, 18, 5, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoGarantia_ValorEncerramento_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoGarantia_ValorEncerramento_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 142 )
         {
            wbEnd = 0;
            nRC_GXsfl_142 = (short)(nGXsfl_142_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_139_6H2e( true) ;
         }
         else
         {
            wb_table4_139_6H2e( false) ;
         }
      }

      protected void wb_table2_8_6H2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableheader_Internalname, tblTableheader_Internalname, "", "TableSearch", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='GridHeaderCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblContratogarantiatitle_Internalname, "Contrato Garantia", "", "", lblContratogarantiatitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WWContratoGarantia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='Width100'>") ;
            wb_table5_13_6H2( true) ;
         }
         else
         {
            wb_table5_13_6H2( false) ;
         }
         return  ;
      }

      protected void wb_table5_13_6H2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_WWContratoGarantia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'" + sGXsfl_142_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,20);\"", "", true, "HLP_WWContratoGarantia.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'" + sGXsfl_142_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,21);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_WWContratoGarantia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table6_23_6H2( true) ;
         }
         else
         {
            wb_table6_23_6H2( false) ;
         }
         return  ;
      }

      protected void wb_table6_23_6H2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_6H2e( true) ;
         }
         else
         {
            wb_table2_8_6H2e( false) ;
         }
      }

      protected void wb_table6_23_6H2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 26,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWContratoGarantia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_28_6H2( true) ;
         }
         else
         {
            wb_table7_28_6H2( false) ;
         }
         return  ;
      }

      protected void wb_table7_28_6H2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_WWContratoGarantia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_23_6H2e( true) ;
         }
         else
         {
            wb_table6_23_6H2e( false) ;
         }
      }

      protected void wb_table7_28_6H2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWContratoGarantia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'" + sGXsfl_142_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV15DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,33);\"", "", true, "HLP_WWContratoGarantia.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWContratoGarantia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table8_37_6H2( true) ;
         }
         else
         {
            wb_table8_37_6H2( false) ;
         }
         return  ;
      }

      protected void wb_table8_37_6H2e( bool wbgen )
      {
         if ( wbgen )
         {
            wb_table9_45_6H2( true) ;
         }
         else
         {
            wb_table9_45_6H2( false) ;
         }
         return  ;
      }

      protected void wb_table9_45_6H2e( bool wbgen )
      {
         if ( wbgen )
         {
            wb_table10_53_6H2( true) ;
         }
         else
         {
            wb_table10_53_6H2( false) ;
         }
         return  ;
      }

      protected void wb_table10_53_6H2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 62,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWContratoGarantia.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 63,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWContratoGarantia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWContratoGarantia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 68,'',false,'" + sGXsfl_142_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV19DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,68);\"", "", true, "HLP_WWContratoGarantia.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWContratoGarantia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table11_72_6H2( true) ;
         }
         else
         {
            wb_table11_72_6H2( false) ;
         }
         return  ;
      }

      protected void wb_table11_72_6H2e( bool wbgen )
      {
         if ( wbgen )
         {
            wb_table12_80_6H2( true) ;
         }
         else
         {
            wb_table12_80_6H2( false) ;
         }
         return  ;
      }

      protected void wb_table12_80_6H2e( bool wbgen )
      {
         if ( wbgen )
         {
            wb_table13_88_6H2( true) ;
         }
         else
         {
            wb_table13_88_6H2( false) ;
         }
         return  ;
      }

      protected void wb_table13_88_6H2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 97,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWContratoGarantia.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 98,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWContratoGarantia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWContratoGarantia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 103,'',false,'" + sGXsfl_142_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV23DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR3.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,103);\"", "", true, "HLP_WWContratoGarantia.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWContratoGarantia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table14_107_6H2( true) ;
         }
         else
         {
            wb_table14_107_6H2( false) ;
         }
         return  ;
      }

      protected void wb_table14_107_6H2e( bool wbgen )
      {
         if ( wbgen )
         {
            wb_table15_115_6H2( true) ;
         }
         else
         {
            wb_table15_115_6H2( false) ;
         }
         return  ;
      }

      protected void wb_table15_115_6H2e( bool wbgen )
      {
         if ( wbgen )
         {
            wb_table16_123_6H2( true) ;
         }
         else
         {
            wb_table16_123_6H2( false) ;
         }
         return  ;
      }

      protected void wb_table16_123_6H2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 132,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWContratoGarantia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_28_6H2e( true) ;
         }
         else
         {
            wb_table7_28_6H2e( false) ;
         }
      }

      protected void wb_table16_123_6H2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterscontratogarantia_dataencerramento3_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterscontratogarantia_dataencerramento3_Internalname, tblTablemergeddynamicfilterscontratogarantia_dataencerramento3_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 126,'',false,'" + sGXsfl_142_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContratogarantia_dataencerramento3_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContratogarantia_dataencerramento3_Internalname, context.localUtil.Format(AV47ContratoGarantia_DataEncerramento3, "99/99/99"), context.localUtil.Format( AV47ContratoGarantia_DataEncerramento3, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,126);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratogarantia_dataencerramento3_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratoGarantia.htm");
            GxWebStd.gx_bitmap( context, edtavContratogarantia_dataencerramento3_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContratoGarantia.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfilterscontratogarantia_dataencerramento_rangemiddletext3_Internalname, "at�", "", "", lblDynamicfilterscontratogarantia_dataencerramento_rangemiddletext3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWContratoGarantia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 130,'',false,'" + sGXsfl_142_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContratogarantia_dataencerramento_to3_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContratogarantia_dataencerramento_to3_Internalname, context.localUtil.Format(AV48ContratoGarantia_DataEncerramento_To3, "99/99/99"), context.localUtil.Format( AV48ContratoGarantia_DataEncerramento_To3, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,130);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratogarantia_dataencerramento_to3_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratoGarantia.htm");
            GxWebStd.gx_bitmap( context, edtavContratogarantia_dataencerramento_to3_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContratoGarantia.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table16_123_6H2e( true) ;
         }
         else
         {
            wb_table16_123_6H2e( false) ;
         }
      }

      protected void wb_table15_115_6H2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterscontratogarantia_datacaucao3_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterscontratogarantia_datacaucao3_Internalname, tblTablemergeddynamicfilterscontratogarantia_datacaucao3_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 118,'',false,'" + sGXsfl_142_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContratogarantia_datacaucao3_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContratogarantia_datacaucao3_Internalname, context.localUtil.Format(AV45ContratoGarantia_DataCaucao3, "99/99/99"), context.localUtil.Format( AV45ContratoGarantia_DataCaucao3, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,118);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratogarantia_datacaucao3_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratoGarantia.htm");
            GxWebStd.gx_bitmap( context, edtavContratogarantia_datacaucao3_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContratoGarantia.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfilterscontratogarantia_datacaucao_rangemiddletext3_Internalname, "at�", "", "", lblDynamicfilterscontratogarantia_datacaucao_rangemiddletext3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWContratoGarantia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 122,'',false,'" + sGXsfl_142_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContratogarantia_datacaucao_to3_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContratogarantia_datacaucao_to3_Internalname, context.localUtil.Format(AV46ContratoGarantia_DataCaucao_To3, "99/99/99"), context.localUtil.Format( AV46ContratoGarantia_DataCaucao_To3, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,122);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratogarantia_datacaucao_to3_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratoGarantia.htm");
            GxWebStd.gx_bitmap( context, edtavContratogarantia_datacaucao_to3_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContratoGarantia.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table15_115_6H2e( true) ;
         }
         else
         {
            wb_table15_115_6H2e( false) ;
         }
      }

      protected void wb_table14_107_6H2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterscontratogarantia_datapagtogarantia3_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterscontratogarantia_datapagtogarantia3_Internalname, tblTablemergeddynamicfilterscontratogarantia_datapagtogarantia3_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 110,'',false,'" + sGXsfl_142_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContratogarantia_datapagtogarantia3_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContratogarantia_datapagtogarantia3_Internalname, context.localUtil.Format(AV24ContratoGarantia_DataPagtoGarantia3, "99/99/99"), context.localUtil.Format( AV24ContratoGarantia_DataPagtoGarantia3, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,110);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratogarantia_datapagtogarantia3_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratoGarantia.htm");
            GxWebStd.gx_bitmap( context, edtavContratogarantia_datapagtogarantia3_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContratoGarantia.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfilterscontratogarantia_datapagtogarantia_rangemiddletext3_Internalname, "at�", "", "", lblDynamicfilterscontratogarantia_datapagtogarantia_rangemiddletext3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWContratoGarantia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 114,'',false,'" + sGXsfl_142_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContratogarantia_datapagtogarantia_to3_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContratogarantia_datapagtogarantia_to3_Internalname, context.localUtil.Format(AV25ContratoGarantia_DataPagtoGarantia_To3, "99/99/99"), context.localUtil.Format( AV25ContratoGarantia_DataPagtoGarantia_To3, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,114);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratogarantia_datapagtogarantia_to3_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratoGarantia.htm");
            GxWebStd.gx_bitmap( context, edtavContratogarantia_datapagtogarantia_to3_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContratoGarantia.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table14_107_6H2e( true) ;
         }
         else
         {
            wb_table14_107_6H2e( false) ;
         }
      }

      protected void wb_table13_88_6H2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterscontratogarantia_dataencerramento2_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterscontratogarantia_dataencerramento2_Internalname, tblTablemergeddynamicfilterscontratogarantia_dataencerramento2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 91,'',false,'" + sGXsfl_142_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContratogarantia_dataencerramento2_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContratogarantia_dataencerramento2_Internalname, context.localUtil.Format(AV43ContratoGarantia_DataEncerramento2, "99/99/99"), context.localUtil.Format( AV43ContratoGarantia_DataEncerramento2, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,91);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratogarantia_dataencerramento2_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratoGarantia.htm");
            GxWebStd.gx_bitmap( context, edtavContratogarantia_dataencerramento2_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContratoGarantia.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfilterscontratogarantia_dataencerramento_rangemiddletext2_Internalname, "at�", "", "", lblDynamicfilterscontratogarantia_dataencerramento_rangemiddletext2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWContratoGarantia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 95,'',false,'" + sGXsfl_142_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContratogarantia_dataencerramento_to2_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContratogarantia_dataencerramento_to2_Internalname, context.localUtil.Format(AV44ContratoGarantia_DataEncerramento_To2, "99/99/99"), context.localUtil.Format( AV44ContratoGarantia_DataEncerramento_To2, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,95);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratogarantia_dataencerramento_to2_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratoGarantia.htm");
            GxWebStd.gx_bitmap( context, edtavContratogarantia_dataencerramento_to2_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContratoGarantia.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table13_88_6H2e( true) ;
         }
         else
         {
            wb_table13_88_6H2e( false) ;
         }
      }

      protected void wb_table12_80_6H2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterscontratogarantia_datacaucao2_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterscontratogarantia_datacaucao2_Internalname, tblTablemergeddynamicfilterscontratogarantia_datacaucao2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 83,'',false,'" + sGXsfl_142_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContratogarantia_datacaucao2_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContratogarantia_datacaucao2_Internalname, context.localUtil.Format(AV41ContratoGarantia_DataCaucao2, "99/99/99"), context.localUtil.Format( AV41ContratoGarantia_DataCaucao2, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,83);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratogarantia_datacaucao2_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratoGarantia.htm");
            GxWebStd.gx_bitmap( context, edtavContratogarantia_datacaucao2_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContratoGarantia.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfilterscontratogarantia_datacaucao_rangemiddletext2_Internalname, "at�", "", "", lblDynamicfilterscontratogarantia_datacaucao_rangemiddletext2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWContratoGarantia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 87,'',false,'" + sGXsfl_142_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContratogarantia_datacaucao_to2_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContratogarantia_datacaucao_to2_Internalname, context.localUtil.Format(AV42ContratoGarantia_DataCaucao_To2, "99/99/99"), context.localUtil.Format( AV42ContratoGarantia_DataCaucao_To2, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,87);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratogarantia_datacaucao_to2_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratoGarantia.htm");
            GxWebStd.gx_bitmap( context, edtavContratogarantia_datacaucao_to2_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContratoGarantia.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table12_80_6H2e( true) ;
         }
         else
         {
            wb_table12_80_6H2e( false) ;
         }
      }

      protected void wb_table11_72_6H2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterscontratogarantia_datapagtogarantia2_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterscontratogarantia_datapagtogarantia2_Internalname, tblTablemergeddynamicfilterscontratogarantia_datapagtogarantia2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 75,'',false,'" + sGXsfl_142_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContratogarantia_datapagtogarantia2_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContratogarantia_datapagtogarantia2_Internalname, context.localUtil.Format(AV20ContratoGarantia_DataPagtoGarantia2, "99/99/99"), context.localUtil.Format( AV20ContratoGarantia_DataPagtoGarantia2, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,75);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratogarantia_datapagtogarantia2_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratoGarantia.htm");
            GxWebStd.gx_bitmap( context, edtavContratogarantia_datapagtogarantia2_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContratoGarantia.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfilterscontratogarantia_datapagtogarantia_rangemiddletext2_Internalname, "at�", "", "", lblDynamicfilterscontratogarantia_datapagtogarantia_rangemiddletext2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWContratoGarantia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 79,'',false,'" + sGXsfl_142_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContratogarantia_datapagtogarantia_to2_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContratogarantia_datapagtogarantia_to2_Internalname, context.localUtil.Format(AV21ContratoGarantia_DataPagtoGarantia_To2, "99/99/99"), context.localUtil.Format( AV21ContratoGarantia_DataPagtoGarantia_To2, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,79);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratogarantia_datapagtogarantia_to2_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratoGarantia.htm");
            GxWebStd.gx_bitmap( context, edtavContratogarantia_datapagtogarantia_to2_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContratoGarantia.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table11_72_6H2e( true) ;
         }
         else
         {
            wb_table11_72_6H2e( false) ;
         }
      }

      protected void wb_table10_53_6H2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterscontratogarantia_dataencerramento1_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterscontratogarantia_dataencerramento1_Internalname, tblTablemergeddynamicfilterscontratogarantia_dataencerramento1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 56,'',false,'" + sGXsfl_142_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContratogarantia_dataencerramento1_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContratogarantia_dataencerramento1_Internalname, context.localUtil.Format(AV39ContratoGarantia_DataEncerramento1, "99/99/99"), context.localUtil.Format( AV39ContratoGarantia_DataEncerramento1, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,56);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratogarantia_dataencerramento1_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratoGarantia.htm");
            GxWebStd.gx_bitmap( context, edtavContratogarantia_dataencerramento1_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContratoGarantia.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfilterscontratogarantia_dataencerramento_rangemiddletext1_Internalname, "at�", "", "", lblDynamicfilterscontratogarantia_dataencerramento_rangemiddletext1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWContratoGarantia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 60,'',false,'" + sGXsfl_142_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContratogarantia_dataencerramento_to1_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContratogarantia_dataencerramento_to1_Internalname, context.localUtil.Format(AV40ContratoGarantia_DataEncerramento_To1, "99/99/99"), context.localUtil.Format( AV40ContratoGarantia_DataEncerramento_To1, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,60);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratogarantia_dataencerramento_to1_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratoGarantia.htm");
            GxWebStd.gx_bitmap( context, edtavContratogarantia_dataencerramento_to1_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContratoGarantia.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table10_53_6H2e( true) ;
         }
         else
         {
            wb_table10_53_6H2e( false) ;
         }
      }

      protected void wb_table9_45_6H2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterscontratogarantia_datacaucao1_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterscontratogarantia_datacaucao1_Internalname, tblTablemergeddynamicfilterscontratogarantia_datacaucao1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 48,'',false,'" + sGXsfl_142_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContratogarantia_datacaucao1_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContratogarantia_datacaucao1_Internalname, context.localUtil.Format(AV37ContratoGarantia_DataCaucao1, "99/99/99"), context.localUtil.Format( AV37ContratoGarantia_DataCaucao1, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,48);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratogarantia_datacaucao1_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratoGarantia.htm");
            GxWebStd.gx_bitmap( context, edtavContratogarantia_datacaucao1_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContratoGarantia.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfilterscontratogarantia_datacaucao_rangemiddletext1_Internalname, "at�", "", "", lblDynamicfilterscontratogarantia_datacaucao_rangemiddletext1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWContratoGarantia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 52,'',false,'" + sGXsfl_142_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContratogarantia_datacaucao_to1_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContratogarantia_datacaucao_to1_Internalname, context.localUtil.Format(AV38ContratoGarantia_DataCaucao_To1, "99/99/99"), context.localUtil.Format( AV38ContratoGarantia_DataCaucao_To1, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,52);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratogarantia_datacaucao_to1_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratoGarantia.htm");
            GxWebStd.gx_bitmap( context, edtavContratogarantia_datacaucao_to1_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContratoGarantia.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_45_6H2e( true) ;
         }
         else
         {
            wb_table9_45_6H2e( false) ;
         }
      }

      protected void wb_table8_37_6H2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterscontratogarantia_datapagtogarantia1_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterscontratogarantia_datapagtogarantia1_Internalname, tblTablemergeddynamicfilterscontratogarantia_datapagtogarantia1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'',false,'" + sGXsfl_142_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContratogarantia_datapagtogarantia1_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContratogarantia_datapagtogarantia1_Internalname, context.localUtil.Format(AV16ContratoGarantia_DataPagtoGarantia1, "99/99/99"), context.localUtil.Format( AV16ContratoGarantia_DataPagtoGarantia1, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,40);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratogarantia_datapagtogarantia1_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratoGarantia.htm");
            GxWebStd.gx_bitmap( context, edtavContratogarantia_datapagtogarantia1_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContratoGarantia.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfilterscontratogarantia_datapagtogarantia_rangemiddletext1_Internalname, "at�", "", "", lblDynamicfilterscontratogarantia_datapagtogarantia_rangemiddletext1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWContratoGarantia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'" + sGXsfl_142_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContratogarantia_datapagtogarantia_to1_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContratogarantia_datapagtogarantia_to1_Internalname, context.localUtil.Format(AV17ContratoGarantia_DataPagtoGarantia_To1, "99/99/99"), context.localUtil.Format( AV17ContratoGarantia_DataPagtoGarantia_To1, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,44);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratogarantia_datapagtogarantia_to1_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratoGarantia.htm");
            GxWebStd.gx_bitmap( context, edtavContratogarantia_datapagtogarantia_to1_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWContratoGarantia.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_37_6H2e( true) ;
         }
         else
         {
            wb_table8_37_6H2e( false) ;
         }
      }

      protected void wb_table5_13_6H2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgInsert_Internalname, context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Inserir", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgInsert_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOINSERT\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWContratoGarantia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_13_6H2e( true) ;
         }
         else
         {
            wb_table5_13_6H2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA6H2( ) ;
         WS6H2( ) ;
         WE6H2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20205181304666");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxdec.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wwcontratogarantia.js", "?20205181304667");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_1422( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_142_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_142_idx;
         edtContrato_Numero_Internalname = "CONTRATO_NUMERO_"+sGXsfl_142_idx;
         edtContratada_PessoaNom_Internalname = "CONTRATADA_PESSOANOM_"+sGXsfl_142_idx;
         edtContratada_PessoaCNPJ_Internalname = "CONTRATADA_PESSOACNPJ_"+sGXsfl_142_idx;
         edtContratoGarantia_DataPagtoGarantia_Internalname = "CONTRATOGARANTIA_DATAPAGTOGARANTIA_"+sGXsfl_142_idx;
         edtContratoGarantia_Percentual_Internalname = "CONTRATOGARANTIA_PERCENTUAL_"+sGXsfl_142_idx;
         edtContratoGarantia_DataCaucao_Internalname = "CONTRATOGARANTIA_DATACAUCAO_"+sGXsfl_142_idx;
         edtContratoGarantia_ValorGarantia_Internalname = "CONTRATOGARANTIA_VALORGARANTIA_"+sGXsfl_142_idx;
         edtContratoGarantia_DataEncerramento_Internalname = "CONTRATOGARANTIA_DATAENCERRAMENTO_"+sGXsfl_142_idx;
         edtContratoGarantia_ValorEncerramento_Internalname = "CONTRATOGARANTIA_VALORENCERRAMENTO_"+sGXsfl_142_idx;
      }

      protected void SubsflControlProps_fel_1422( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_142_fel_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_142_fel_idx;
         edtContrato_Numero_Internalname = "CONTRATO_NUMERO_"+sGXsfl_142_fel_idx;
         edtContratada_PessoaNom_Internalname = "CONTRATADA_PESSOANOM_"+sGXsfl_142_fel_idx;
         edtContratada_PessoaCNPJ_Internalname = "CONTRATADA_PESSOACNPJ_"+sGXsfl_142_fel_idx;
         edtContratoGarantia_DataPagtoGarantia_Internalname = "CONTRATOGARANTIA_DATAPAGTOGARANTIA_"+sGXsfl_142_fel_idx;
         edtContratoGarantia_Percentual_Internalname = "CONTRATOGARANTIA_PERCENTUAL_"+sGXsfl_142_fel_idx;
         edtContratoGarantia_DataCaucao_Internalname = "CONTRATOGARANTIA_DATACAUCAO_"+sGXsfl_142_fel_idx;
         edtContratoGarantia_ValorGarantia_Internalname = "CONTRATOGARANTIA_VALORGARANTIA_"+sGXsfl_142_fel_idx;
         edtContratoGarantia_DataEncerramento_Internalname = "CONTRATOGARANTIA_DATAENCERRAMENTO_"+sGXsfl_142_fel_idx;
         edtContratoGarantia_ValorEncerramento_Internalname = "CONTRATOGARANTIA_VALORENCERRAMENTO_"+sGXsfl_142_fel_idx;
      }

      protected void sendrow_1422( )
      {
         SubsflControlProps_1422( ) ;
         WB6H0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_142_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_142_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_142_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV28Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV28Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV155Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV28Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV28Update)) ? AV155Update_GXI : context.PathToRelativeUrl( AV28Update)),(String)edtavUpdate_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV28Update_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV29Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV156Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete)) ? AV156Delete_GXI : context.PathToRelativeUrl( AV29Delete)),(String)edtavDelete_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV29Delete_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContrato_Numero_Internalname,StringUtil.RTrim( A77Contrato_Numero),(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContrato_Numero_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)110,(String)"px",(short)17,(String)"px",(short)20,(short)0,(short)0,(short)142,(short)1,(short)-1,(short)-1,(bool)true,(String)"NumeroContrato",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratada_PessoaNom_Internalname,StringUtil.RTrim( A41Contratada_PessoaNom),StringUtil.RTrim( context.localUtil.Format( A41Contratada_PessoaNom, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)edtContratada_PessoaNom_Link,(String)"",(String)"",(String)"",(String)edtContratada_PessoaNom_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)100,(short)0,(short)0,(short)142,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome100",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratada_PessoaCNPJ_Internalname,(String)A42Contratada_PessoaCNPJ,(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratada_PessoaCNPJ_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)15,(short)0,(short)0,(short)142,(short)1,(short)-1,(short)-1,(bool)true,(String)"Docto",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoGarantia_DataPagtoGarantia_Internalname,context.localUtil.Format(A102ContratoGarantia_DataPagtoGarantia, "99/99/99"),context.localUtil.Format( A102ContratoGarantia_DataPagtoGarantia, "99/99/99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)edtContratoGarantia_DataPagtoGarantia_Link,(String)"",(String)"",(String)"",(String)edtContratoGarantia_DataPagtoGarantia_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)8,(short)0,(short)0,(short)142,(short)1,(short)-1,(short)0,(bool)true,(String)"Data",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoGarantia_Percentual_Internalname,StringUtil.LTrim( StringUtil.NToC( A103ContratoGarantia_Percentual, 6, 2, ",", "")),context.localUtil.Format( A103ContratoGarantia_Percentual, "ZZ9.99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoGarantia_Percentual_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)142,(short)1,(short)-1,(short)0,(bool)true,(String)"Percentual",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoGarantia_DataCaucao_Internalname,context.localUtil.Format(A104ContratoGarantia_DataCaucao, "99/99/99"),context.localUtil.Format( A104ContratoGarantia_DataCaucao, "99/99/99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoGarantia_DataCaucao_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)8,(short)0,(short)0,(short)142,(short)1,(short)-1,(short)0,(bool)true,(String)"Data",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoGarantia_ValorGarantia_Internalname,StringUtil.LTrim( StringUtil.NToC( A105ContratoGarantia_ValorGarantia, 18, 5, ",", "")),context.localUtil.Format( A105ContratoGarantia_ValorGarantia, "ZZZ,ZZZ,ZZZ,ZZ9.99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoGarantia_ValorGarantia_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)18,(short)0,(short)0,(short)142,(short)1,(short)-1,(short)0,(bool)true,(String)"Valor",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoGarantia_DataEncerramento_Internalname,context.localUtil.Format(A106ContratoGarantia_DataEncerramento, "99/99/99"),context.localUtil.Format( A106ContratoGarantia_DataEncerramento, "99/99/99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoGarantia_DataEncerramento_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)8,(short)0,(short)0,(short)142,(short)1,(short)-1,(short)0,(bool)true,(String)"Data",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoGarantia_ValorEncerramento_Internalname,StringUtil.LTrim( StringUtil.NToC( A107ContratoGarantia_ValorEncerramento, 18, 5, ",", "")),context.localUtil.Format( A107ContratoGarantia_ValorEncerramento, "ZZZ,ZZZ,ZZZ,ZZ9.99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoGarantia_ValorEncerramento_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)18,(short)0,(short)0,(short)142,(short)1,(short)-1,(short)0,(bool)true,(String)"Valor",(String)"right",(bool)false});
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOGARANTIA_DATAPAGTOGARANTIA"+"_"+sGXsfl_142_idx, GetSecureSignedToken( sGXsfl_142_idx, A102ContratoGarantia_DataPagtoGarantia));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOGARANTIA_PERCENTUAL"+"_"+sGXsfl_142_idx, GetSecureSignedToken( sGXsfl_142_idx, context.localUtil.Format( A103ContratoGarantia_Percentual, "ZZ9.99")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOGARANTIA_DATACAUCAO"+"_"+sGXsfl_142_idx, GetSecureSignedToken( sGXsfl_142_idx, A104ContratoGarantia_DataCaucao));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOGARANTIA_VALORGARANTIA"+"_"+sGXsfl_142_idx, GetSecureSignedToken( sGXsfl_142_idx, context.localUtil.Format( A105ContratoGarantia_ValorGarantia, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOGARANTIA_DATAENCERRAMENTO"+"_"+sGXsfl_142_idx, GetSecureSignedToken( sGXsfl_142_idx, A106ContratoGarantia_DataEncerramento));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOGARANTIA_VALORENCERRAMENTO"+"_"+sGXsfl_142_idx, GetSecureSignedToken( sGXsfl_142_idx, context.localUtil.Format( A107ContratoGarantia_ValorEncerramento, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
            GridContainer.AddRow(GridRow);
            nGXsfl_142_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_142_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_142_idx+1));
            sGXsfl_142_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_142_idx), 4, 0)), 4, "0");
            SubsflControlProps_1422( ) ;
         }
         /* End function sendrow_1422 */
      }

      protected void init_default_properties( )
      {
         lblContratogarantiatitle_Internalname = "CONTRATOGARANTIATITLE";
         imgInsert_Internalname = "INSERT";
         tblTableactions_Internalname = "TABLEACTIONS";
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         imgCleanfilters_Internalname = "CLEANFILTERS";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         edtavContratogarantia_datapagtogarantia1_Internalname = "vCONTRATOGARANTIA_DATAPAGTOGARANTIA1";
         lblDynamicfilterscontratogarantia_datapagtogarantia_rangemiddletext1_Internalname = "DYNAMICFILTERSCONTRATOGARANTIA_DATAPAGTOGARANTIA_RANGEMIDDLETEXT1";
         edtavContratogarantia_datapagtogarantia_to1_Internalname = "vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO1";
         tblTablemergeddynamicfilterscontratogarantia_datapagtogarantia1_Internalname = "TABLEMERGEDDYNAMICFILTERSCONTRATOGARANTIA_DATAPAGTOGARANTIA1";
         edtavContratogarantia_datacaucao1_Internalname = "vCONTRATOGARANTIA_DATACAUCAO1";
         lblDynamicfilterscontratogarantia_datacaucao_rangemiddletext1_Internalname = "DYNAMICFILTERSCONTRATOGARANTIA_DATACAUCAO_RANGEMIDDLETEXT1";
         edtavContratogarantia_datacaucao_to1_Internalname = "vCONTRATOGARANTIA_DATACAUCAO_TO1";
         tblTablemergeddynamicfilterscontratogarantia_datacaucao1_Internalname = "TABLEMERGEDDYNAMICFILTERSCONTRATOGARANTIA_DATACAUCAO1";
         edtavContratogarantia_dataencerramento1_Internalname = "vCONTRATOGARANTIA_DATAENCERRAMENTO1";
         lblDynamicfilterscontratogarantia_dataencerramento_rangemiddletext1_Internalname = "DYNAMICFILTERSCONTRATOGARANTIA_DATAENCERRAMENTO_RANGEMIDDLETEXT1";
         edtavContratogarantia_dataencerramento_to1_Internalname = "vCONTRATOGARANTIA_DATAENCERRAMENTO_TO1";
         tblTablemergeddynamicfilterscontratogarantia_dataencerramento1_Internalname = "TABLEMERGEDDYNAMICFILTERSCONTRATOGARANTIA_DATAENCERRAMENTO1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         edtavContratogarantia_datapagtogarantia2_Internalname = "vCONTRATOGARANTIA_DATAPAGTOGARANTIA2";
         lblDynamicfilterscontratogarantia_datapagtogarantia_rangemiddletext2_Internalname = "DYNAMICFILTERSCONTRATOGARANTIA_DATAPAGTOGARANTIA_RANGEMIDDLETEXT2";
         edtavContratogarantia_datapagtogarantia_to2_Internalname = "vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO2";
         tblTablemergeddynamicfilterscontratogarantia_datapagtogarantia2_Internalname = "TABLEMERGEDDYNAMICFILTERSCONTRATOGARANTIA_DATAPAGTOGARANTIA2";
         edtavContratogarantia_datacaucao2_Internalname = "vCONTRATOGARANTIA_DATACAUCAO2";
         lblDynamicfilterscontratogarantia_datacaucao_rangemiddletext2_Internalname = "DYNAMICFILTERSCONTRATOGARANTIA_DATACAUCAO_RANGEMIDDLETEXT2";
         edtavContratogarantia_datacaucao_to2_Internalname = "vCONTRATOGARANTIA_DATACAUCAO_TO2";
         tblTablemergeddynamicfilterscontratogarantia_datacaucao2_Internalname = "TABLEMERGEDDYNAMICFILTERSCONTRATOGARANTIA_DATACAUCAO2";
         edtavContratogarantia_dataencerramento2_Internalname = "vCONTRATOGARANTIA_DATAENCERRAMENTO2";
         lblDynamicfilterscontratogarantia_dataencerramento_rangemiddletext2_Internalname = "DYNAMICFILTERSCONTRATOGARANTIA_DATAENCERRAMENTO_RANGEMIDDLETEXT2";
         edtavContratogarantia_dataencerramento_to2_Internalname = "vCONTRATOGARANTIA_DATAENCERRAMENTO_TO2";
         tblTablemergeddynamicfilterscontratogarantia_dataencerramento2_Internalname = "TABLEMERGEDDYNAMICFILTERSCONTRATOGARANTIA_DATAENCERRAMENTO2";
         imgAdddynamicfilters2_Internalname = "ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = "DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = "vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = "DYNAMICFILTERSMIDDLE3";
         edtavContratogarantia_datapagtogarantia3_Internalname = "vCONTRATOGARANTIA_DATAPAGTOGARANTIA3";
         lblDynamicfilterscontratogarantia_datapagtogarantia_rangemiddletext3_Internalname = "DYNAMICFILTERSCONTRATOGARANTIA_DATAPAGTOGARANTIA_RANGEMIDDLETEXT3";
         edtavContratogarantia_datapagtogarantia_to3_Internalname = "vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO3";
         tblTablemergeddynamicfilterscontratogarantia_datapagtogarantia3_Internalname = "TABLEMERGEDDYNAMICFILTERSCONTRATOGARANTIA_DATAPAGTOGARANTIA3";
         edtavContratogarantia_datacaucao3_Internalname = "vCONTRATOGARANTIA_DATACAUCAO3";
         lblDynamicfilterscontratogarantia_datacaucao_rangemiddletext3_Internalname = "DYNAMICFILTERSCONTRATOGARANTIA_DATACAUCAO_RANGEMIDDLETEXT3";
         edtavContratogarantia_datacaucao_to3_Internalname = "vCONTRATOGARANTIA_DATACAUCAO_TO3";
         tblTablemergeddynamicfilterscontratogarantia_datacaucao3_Internalname = "TABLEMERGEDDYNAMICFILTERSCONTRATOGARANTIA_DATACAUCAO3";
         edtavContratogarantia_dataencerramento3_Internalname = "vCONTRATOGARANTIA_DATAENCERRAMENTO3";
         lblDynamicfilterscontratogarantia_dataencerramento_rangemiddletext3_Internalname = "DYNAMICFILTERSCONTRATOGARANTIA_DATAENCERRAMENTO_RANGEMIDDLETEXT3";
         edtavContratogarantia_dataencerramento_to3_Internalname = "vCONTRATOGARANTIA_DATAENCERRAMENTO_TO3";
         tblTablemergeddynamicfilterscontratogarantia_dataencerramento3_Internalname = "TABLEMERGEDDYNAMICFILTERSCONTRATOGARANTIA_DATAENCERRAMENTO3";
         imgRemovedynamicfilters3_Internalname = "REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTableheader_Internalname = "TABLEHEADER";
         edtavUpdate_Internalname = "vUPDATE";
         edtavDelete_Internalname = "vDELETE";
         edtContrato_Numero_Internalname = "CONTRATO_NUMERO";
         edtContratada_PessoaNom_Internalname = "CONTRATADA_PESSOANOM";
         edtContratada_PessoaCNPJ_Internalname = "CONTRATADA_PESSOACNPJ";
         edtContratoGarantia_DataPagtoGarantia_Internalname = "CONTRATOGARANTIA_DATAPAGTOGARANTIA";
         edtContratoGarantia_Percentual_Internalname = "CONTRATOGARANTIA_PERCENTUAL";
         edtContratoGarantia_DataCaucao_Internalname = "CONTRATOGARANTIA_DATACAUCAO";
         edtContratoGarantia_ValorGarantia_Internalname = "CONTRATOGARANTIA_VALORGARANTIA";
         edtContratoGarantia_DataEncerramento_Internalname = "CONTRATOGARANTIA_DATAENCERRAMENTO";
         edtContratoGarantia_ValorEncerramento_Internalname = "CONTRATOGARANTIA_VALORENCERRAMENTO";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = "vDYNAMICFILTERSENABLED3";
         edtavTfcontrato_numero_Internalname = "vTFCONTRATO_NUMERO";
         edtavTfcontrato_numero_sel_Internalname = "vTFCONTRATO_NUMERO_SEL";
         edtavTfcontratada_pessoanom_Internalname = "vTFCONTRATADA_PESSOANOM";
         edtavTfcontratada_pessoanom_sel_Internalname = "vTFCONTRATADA_PESSOANOM_SEL";
         edtavTfcontratada_pessoacnpj_Internalname = "vTFCONTRATADA_PESSOACNPJ";
         edtavTfcontratada_pessoacnpj_sel_Internalname = "vTFCONTRATADA_PESSOACNPJ_SEL";
         edtavTfcontratogarantia_datapagtogarantia_Internalname = "vTFCONTRATOGARANTIA_DATAPAGTOGARANTIA";
         edtavTfcontratogarantia_datapagtogarantia_to_Internalname = "vTFCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO";
         edtavDdo_contratogarantia_datapagtogarantiaauxdate_Internalname = "vDDO_CONTRATOGARANTIA_DATAPAGTOGARANTIAAUXDATE";
         edtavDdo_contratogarantia_datapagtogarantiaauxdateto_Internalname = "vDDO_CONTRATOGARANTIA_DATAPAGTOGARANTIAAUXDATETO";
         divDdo_contratogarantia_datapagtogarantiaauxdates_Internalname = "DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIAAUXDATES";
         edtavTfcontratogarantia_percentual_Internalname = "vTFCONTRATOGARANTIA_PERCENTUAL";
         edtavTfcontratogarantia_percentual_to_Internalname = "vTFCONTRATOGARANTIA_PERCENTUAL_TO";
         edtavTfcontratogarantia_datacaucao_Internalname = "vTFCONTRATOGARANTIA_DATACAUCAO";
         edtavTfcontratogarantia_datacaucao_to_Internalname = "vTFCONTRATOGARANTIA_DATACAUCAO_TO";
         edtavDdo_contratogarantia_datacaucaoauxdate_Internalname = "vDDO_CONTRATOGARANTIA_DATACAUCAOAUXDATE";
         edtavDdo_contratogarantia_datacaucaoauxdateto_Internalname = "vDDO_CONTRATOGARANTIA_DATACAUCAOAUXDATETO";
         divDdo_contratogarantia_datacaucaoauxdates_Internalname = "DDO_CONTRATOGARANTIA_DATACAUCAOAUXDATES";
         edtavTfcontratogarantia_valorgarantia_Internalname = "vTFCONTRATOGARANTIA_VALORGARANTIA";
         edtavTfcontratogarantia_valorgarantia_to_Internalname = "vTFCONTRATOGARANTIA_VALORGARANTIA_TO";
         edtavTfcontratogarantia_dataencerramento_Internalname = "vTFCONTRATOGARANTIA_DATAENCERRAMENTO";
         edtavTfcontratogarantia_dataencerramento_to_Internalname = "vTFCONTRATOGARANTIA_DATAENCERRAMENTO_TO";
         edtavDdo_contratogarantia_dataencerramentoauxdate_Internalname = "vDDO_CONTRATOGARANTIA_DATAENCERRAMENTOAUXDATE";
         edtavDdo_contratogarantia_dataencerramentoauxdateto_Internalname = "vDDO_CONTRATOGARANTIA_DATAENCERRAMENTOAUXDATETO";
         divDdo_contratogarantia_dataencerramentoauxdates_Internalname = "DDO_CONTRATOGARANTIA_DATAENCERRAMENTOAUXDATES";
         edtavTfcontratogarantia_valorencerramento_Internalname = "vTFCONTRATOGARANTIA_VALORENCERRAMENTO";
         edtavTfcontratogarantia_valorencerramento_to_Internalname = "vTFCONTRATOGARANTIA_VALORENCERRAMENTO_TO";
         Ddo_contrato_numero_Internalname = "DDO_CONTRATO_NUMERO";
         edtavDdo_contrato_numerotitlecontrolidtoreplace_Internalname = "vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE";
         Ddo_contratada_pessoanom_Internalname = "DDO_CONTRATADA_PESSOANOM";
         edtavDdo_contratada_pessoanomtitlecontrolidtoreplace_Internalname = "vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE";
         Ddo_contratada_pessoacnpj_Internalname = "DDO_CONTRATADA_PESSOACNPJ";
         edtavDdo_contratada_pessoacnpjtitlecontrolidtoreplace_Internalname = "vDDO_CONTRATADA_PESSOACNPJTITLECONTROLIDTOREPLACE";
         Ddo_contratogarantia_datapagtogarantia_Internalname = "DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA";
         edtavDdo_contratogarantia_datapagtogarantiatitlecontrolidtoreplace_Internalname = "vDDO_CONTRATOGARANTIA_DATAPAGTOGARANTIATITLECONTROLIDTOREPLACE";
         Ddo_contratogarantia_percentual_Internalname = "DDO_CONTRATOGARANTIA_PERCENTUAL";
         edtavDdo_contratogarantia_percentualtitlecontrolidtoreplace_Internalname = "vDDO_CONTRATOGARANTIA_PERCENTUALTITLECONTROLIDTOREPLACE";
         Ddo_contratogarantia_datacaucao_Internalname = "DDO_CONTRATOGARANTIA_DATACAUCAO";
         edtavDdo_contratogarantia_datacaucaotitlecontrolidtoreplace_Internalname = "vDDO_CONTRATOGARANTIA_DATACAUCAOTITLECONTROLIDTOREPLACE";
         Ddo_contratogarantia_valorgarantia_Internalname = "DDO_CONTRATOGARANTIA_VALORGARANTIA";
         edtavDdo_contratogarantia_valorgarantiatitlecontrolidtoreplace_Internalname = "vDDO_CONTRATOGARANTIA_VALORGARANTIATITLECONTROLIDTOREPLACE";
         Ddo_contratogarantia_dataencerramento_Internalname = "DDO_CONTRATOGARANTIA_DATAENCERRAMENTO";
         edtavDdo_contratogarantia_dataencerramentotitlecontrolidtoreplace_Internalname = "vDDO_CONTRATOGARANTIA_DATAENCERRAMENTOTITLECONTROLIDTOREPLACE";
         Ddo_contratogarantia_valorencerramento_Internalname = "DDO_CONTRATOGARANTIA_VALORENCERRAMENTO";
         edtavDdo_contratogarantia_valorencerramentotitlecontrolidtoreplace_Internalname = "vDDO_CONTRATOGARANTIA_VALORENCERRAMENTOTITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtContratoGarantia_ValorEncerramento_Jsonclick = "";
         edtContratoGarantia_DataEncerramento_Jsonclick = "";
         edtContratoGarantia_ValorGarantia_Jsonclick = "";
         edtContratoGarantia_DataCaucao_Jsonclick = "";
         edtContratoGarantia_Percentual_Jsonclick = "";
         edtContratoGarantia_DataPagtoGarantia_Jsonclick = "";
         edtContratada_PessoaCNPJ_Jsonclick = "";
         edtContratada_PessoaNom_Jsonclick = "";
         edtContrato_Numero_Jsonclick = "";
         edtavContratogarantia_datapagtogarantia_to1_Jsonclick = "";
         edtavContratogarantia_datapagtogarantia1_Jsonclick = "";
         edtavContratogarantia_datacaucao_to1_Jsonclick = "";
         edtavContratogarantia_datacaucao1_Jsonclick = "";
         edtavContratogarantia_dataencerramento_to1_Jsonclick = "";
         edtavContratogarantia_dataencerramento1_Jsonclick = "";
         edtavContratogarantia_datapagtogarantia_to2_Jsonclick = "";
         edtavContratogarantia_datapagtogarantia2_Jsonclick = "";
         edtavContratogarantia_datacaucao_to2_Jsonclick = "";
         edtavContratogarantia_datacaucao2_Jsonclick = "";
         edtavContratogarantia_dataencerramento_to2_Jsonclick = "";
         edtavContratogarantia_dataencerramento2_Jsonclick = "";
         edtavContratogarantia_datapagtogarantia_to3_Jsonclick = "";
         edtavContratogarantia_datapagtogarantia3_Jsonclick = "";
         edtavContratogarantia_datacaucao_to3_Jsonclick = "";
         edtavContratogarantia_datacaucao3_Jsonclick = "";
         edtavContratogarantia_dataencerramento_to3_Jsonclick = "";
         edtavContratogarantia_dataencerramento3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtContratoGarantia_DataPagtoGarantia_Link = "";
         edtContratada_PessoaNom_Link = "";
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = "";
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = "";
         edtContratoGarantia_ValorEncerramento_Titleformat = 0;
         edtContratoGarantia_DataEncerramento_Titleformat = 0;
         edtContratoGarantia_ValorGarantia_Titleformat = 0;
         edtContratoGarantia_DataCaucao_Titleformat = 0;
         edtContratoGarantia_Percentual_Titleformat = 0;
         edtContratoGarantia_DataPagtoGarantia_Titleformat = 0;
         edtContratada_PessoaCNPJ_Titleformat = 0;
         edtContratada_PessoaNom_Titleformat = 0;
         edtContrato_Numero_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         tblTablemergeddynamicfilterscontratogarantia_dataencerramento3_Visible = 1;
         tblTablemergeddynamicfilterscontratogarantia_datacaucao3_Visible = 1;
         tblTablemergeddynamicfilterscontratogarantia_datapagtogarantia3_Visible = 1;
         tblTablemergeddynamicfilterscontratogarantia_dataencerramento2_Visible = 1;
         tblTablemergeddynamicfilterscontratogarantia_datacaucao2_Visible = 1;
         tblTablemergeddynamicfilterscontratogarantia_datapagtogarantia2_Visible = 1;
         tblTablemergeddynamicfilterscontratogarantia_dataencerramento1_Visible = 1;
         tblTablemergeddynamicfilterscontratogarantia_datacaucao1_Visible = 1;
         tblTablemergeddynamicfilterscontratogarantia_datapagtogarantia1_Visible = 1;
         edtContratoGarantia_ValorEncerramento_Title = "Valor Encerramento";
         edtContratoGarantia_DataEncerramento_Title = "Data Encerramento";
         edtContratoGarantia_ValorGarantia_Title = "Valor";
         edtContratoGarantia_DataCaucao_Title = "D. da Cau��o";
         edtContratoGarantia_Percentual_Title = "Percentual";
         edtContratoGarantia_DataPagtoGarantia_Title = "Data Pagamento";
         edtContratada_PessoaCNPJ_Title = "CNPJ";
         edtContratada_PessoaNom_Title = "Contratada";
         edtContrato_Numero_Title = "N� Contrato";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         edtavDdo_contratogarantia_valorencerramentotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratogarantia_dataencerramentotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratogarantia_valorgarantiatitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratogarantia_datacaucaotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratogarantia_percentualtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratogarantia_datapagtogarantiatitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratada_pessoacnpjtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratada_pessoanomtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contrato_numerotitlecontrolidtoreplace_Visible = 1;
         edtavTfcontratogarantia_valorencerramento_to_Jsonclick = "";
         edtavTfcontratogarantia_valorencerramento_to_Visible = 1;
         edtavTfcontratogarantia_valorencerramento_Jsonclick = "";
         edtavTfcontratogarantia_valorencerramento_Visible = 1;
         edtavDdo_contratogarantia_dataencerramentoauxdateto_Jsonclick = "";
         edtavDdo_contratogarantia_dataencerramentoauxdate_Jsonclick = "";
         edtavTfcontratogarantia_dataencerramento_to_Jsonclick = "";
         edtavTfcontratogarantia_dataencerramento_to_Visible = 1;
         edtavTfcontratogarantia_dataencerramento_Jsonclick = "";
         edtavTfcontratogarantia_dataencerramento_Visible = 1;
         edtavTfcontratogarantia_valorgarantia_to_Jsonclick = "";
         edtavTfcontratogarantia_valorgarantia_to_Visible = 1;
         edtavTfcontratogarantia_valorgarantia_Jsonclick = "";
         edtavTfcontratogarantia_valorgarantia_Visible = 1;
         edtavDdo_contratogarantia_datacaucaoauxdateto_Jsonclick = "";
         edtavDdo_contratogarantia_datacaucaoauxdate_Jsonclick = "";
         edtavTfcontratogarantia_datacaucao_to_Jsonclick = "";
         edtavTfcontratogarantia_datacaucao_to_Visible = 1;
         edtavTfcontratogarantia_datacaucao_Jsonclick = "";
         edtavTfcontratogarantia_datacaucao_Visible = 1;
         edtavTfcontratogarantia_percentual_to_Jsonclick = "";
         edtavTfcontratogarantia_percentual_to_Visible = 1;
         edtavTfcontratogarantia_percentual_Jsonclick = "";
         edtavTfcontratogarantia_percentual_Visible = 1;
         edtavDdo_contratogarantia_datapagtogarantiaauxdateto_Jsonclick = "";
         edtavDdo_contratogarantia_datapagtogarantiaauxdate_Jsonclick = "";
         edtavTfcontratogarantia_datapagtogarantia_to_Jsonclick = "";
         edtavTfcontratogarantia_datapagtogarantia_to_Visible = 1;
         edtavTfcontratogarantia_datapagtogarantia_Jsonclick = "";
         edtavTfcontratogarantia_datapagtogarantia_Visible = 1;
         edtavTfcontratada_pessoacnpj_sel_Jsonclick = "";
         edtavTfcontratada_pessoacnpj_sel_Visible = 1;
         edtavTfcontratada_pessoacnpj_Jsonclick = "";
         edtavTfcontratada_pessoacnpj_Visible = 1;
         edtavTfcontratada_pessoanom_sel_Jsonclick = "";
         edtavTfcontratada_pessoanom_sel_Visible = 1;
         edtavTfcontratada_pessoanom_Jsonclick = "";
         edtavTfcontratada_pessoanom_Visible = 1;
         edtavTfcontrato_numero_sel_Jsonclick = "";
         edtavTfcontrato_numero_sel_Visible = 1;
         edtavTfcontrato_numero_Jsonclick = "";
         edtavTfcontrato_numero_Visible = 1;
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         Ddo_contratogarantia_valorencerramento_Searchbuttontext = "Pesquisar";
         Ddo_contratogarantia_valorencerramento_Rangefilterto = "At�";
         Ddo_contratogarantia_valorencerramento_Rangefilterfrom = "Desde";
         Ddo_contratogarantia_valorencerramento_Cleanfilter = "Limpar pesquisa";
         Ddo_contratogarantia_valorencerramento_Sortdsc = "Ordenar de Z � A";
         Ddo_contratogarantia_valorencerramento_Sortasc = "Ordenar de A � Z";
         Ddo_contratogarantia_valorencerramento_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contratogarantia_valorencerramento_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contratogarantia_valorencerramento_Filtertype = "Numeric";
         Ddo_contratogarantia_valorencerramento_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratogarantia_valorencerramento_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratogarantia_valorencerramento_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratogarantia_valorencerramento_Titlecontrolidtoreplace = "";
         Ddo_contratogarantia_valorencerramento_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratogarantia_valorencerramento_Cls = "ColumnSettings";
         Ddo_contratogarantia_valorencerramento_Tooltip = "Op��es";
         Ddo_contratogarantia_valorencerramento_Caption = "";
         Ddo_contratogarantia_dataencerramento_Searchbuttontext = "Pesquisar";
         Ddo_contratogarantia_dataencerramento_Rangefilterto = "At�";
         Ddo_contratogarantia_dataencerramento_Rangefilterfrom = "Desde";
         Ddo_contratogarantia_dataencerramento_Cleanfilter = "Limpar pesquisa";
         Ddo_contratogarantia_dataencerramento_Sortdsc = "Ordenar de Z � A";
         Ddo_contratogarantia_dataencerramento_Sortasc = "Ordenar de A � Z";
         Ddo_contratogarantia_dataencerramento_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contratogarantia_dataencerramento_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contratogarantia_dataencerramento_Filtertype = "Date";
         Ddo_contratogarantia_dataencerramento_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratogarantia_dataencerramento_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratogarantia_dataencerramento_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratogarantia_dataencerramento_Titlecontrolidtoreplace = "";
         Ddo_contratogarantia_dataencerramento_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratogarantia_dataencerramento_Cls = "ColumnSettings";
         Ddo_contratogarantia_dataencerramento_Tooltip = "Op��es";
         Ddo_contratogarantia_dataencerramento_Caption = "";
         Ddo_contratogarantia_valorgarantia_Searchbuttontext = "Pesquisar";
         Ddo_contratogarantia_valorgarantia_Rangefilterto = "At�";
         Ddo_contratogarantia_valorgarantia_Rangefilterfrom = "Desde";
         Ddo_contratogarantia_valorgarantia_Cleanfilter = "Limpar pesquisa";
         Ddo_contratogarantia_valorgarantia_Sortdsc = "Ordenar de Z � A";
         Ddo_contratogarantia_valorgarantia_Sortasc = "Ordenar de A � Z";
         Ddo_contratogarantia_valorgarantia_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contratogarantia_valorgarantia_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contratogarantia_valorgarantia_Filtertype = "Numeric";
         Ddo_contratogarantia_valorgarantia_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratogarantia_valorgarantia_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratogarantia_valorgarantia_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratogarantia_valorgarantia_Titlecontrolidtoreplace = "";
         Ddo_contratogarantia_valorgarantia_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratogarantia_valorgarantia_Cls = "ColumnSettings";
         Ddo_contratogarantia_valorgarantia_Tooltip = "Op��es";
         Ddo_contratogarantia_valorgarantia_Caption = "";
         Ddo_contratogarantia_datacaucao_Searchbuttontext = "Pesquisar";
         Ddo_contratogarantia_datacaucao_Rangefilterto = "At�";
         Ddo_contratogarantia_datacaucao_Rangefilterfrom = "Desde";
         Ddo_contratogarantia_datacaucao_Cleanfilter = "Limpar pesquisa";
         Ddo_contratogarantia_datacaucao_Sortdsc = "Ordenar de Z � A";
         Ddo_contratogarantia_datacaucao_Sortasc = "Ordenar de A � Z";
         Ddo_contratogarantia_datacaucao_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contratogarantia_datacaucao_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contratogarantia_datacaucao_Filtertype = "Date";
         Ddo_contratogarantia_datacaucao_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratogarantia_datacaucao_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratogarantia_datacaucao_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratogarantia_datacaucao_Titlecontrolidtoreplace = "";
         Ddo_contratogarantia_datacaucao_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratogarantia_datacaucao_Cls = "ColumnSettings";
         Ddo_contratogarantia_datacaucao_Tooltip = "Op��es";
         Ddo_contratogarantia_datacaucao_Caption = "";
         Ddo_contratogarantia_percentual_Searchbuttontext = "Pesquisar";
         Ddo_contratogarantia_percentual_Rangefilterto = "At�";
         Ddo_contratogarantia_percentual_Rangefilterfrom = "Desde";
         Ddo_contratogarantia_percentual_Cleanfilter = "Limpar pesquisa";
         Ddo_contratogarantia_percentual_Sortdsc = "Ordenar de Z � A";
         Ddo_contratogarantia_percentual_Sortasc = "Ordenar de A � Z";
         Ddo_contratogarantia_percentual_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contratogarantia_percentual_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contratogarantia_percentual_Filtertype = "Numeric";
         Ddo_contratogarantia_percentual_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratogarantia_percentual_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratogarantia_percentual_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratogarantia_percentual_Titlecontrolidtoreplace = "";
         Ddo_contratogarantia_percentual_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratogarantia_percentual_Cls = "ColumnSettings";
         Ddo_contratogarantia_percentual_Tooltip = "Op��es";
         Ddo_contratogarantia_percentual_Caption = "";
         Ddo_contratogarantia_datapagtogarantia_Searchbuttontext = "Pesquisar";
         Ddo_contratogarantia_datapagtogarantia_Rangefilterto = "At�";
         Ddo_contratogarantia_datapagtogarantia_Rangefilterfrom = "Desde";
         Ddo_contratogarantia_datapagtogarantia_Cleanfilter = "Limpar pesquisa";
         Ddo_contratogarantia_datapagtogarantia_Sortdsc = "Ordenar de Z � A";
         Ddo_contratogarantia_datapagtogarantia_Sortasc = "Ordenar de A � Z";
         Ddo_contratogarantia_datapagtogarantia_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contratogarantia_datapagtogarantia_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contratogarantia_datapagtogarantia_Filtertype = "Date";
         Ddo_contratogarantia_datapagtogarantia_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratogarantia_datapagtogarantia_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratogarantia_datapagtogarantia_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratogarantia_datapagtogarantia_Titlecontrolidtoreplace = "";
         Ddo_contratogarantia_datapagtogarantia_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratogarantia_datapagtogarantia_Cls = "ColumnSettings";
         Ddo_contratogarantia_datapagtogarantia_Tooltip = "Op��es";
         Ddo_contratogarantia_datapagtogarantia_Caption = "";
         Ddo_contratada_pessoacnpj_Searchbuttontext = "Pesquisar";
         Ddo_contratada_pessoacnpj_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contratada_pessoacnpj_Cleanfilter = "Limpar pesquisa";
         Ddo_contratada_pessoacnpj_Loadingdata = "Carregando dados...";
         Ddo_contratada_pessoacnpj_Sortdsc = "Ordenar de Z � A";
         Ddo_contratada_pessoacnpj_Sortasc = "Ordenar de A � Z";
         Ddo_contratada_pessoacnpj_Datalistupdateminimumcharacters = 0;
         Ddo_contratada_pessoacnpj_Datalistproc = "GetWWContratoGarantiaFilterData";
         Ddo_contratada_pessoacnpj_Datalisttype = "Dynamic";
         Ddo_contratada_pessoacnpj_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contratada_pessoacnpj_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contratada_pessoacnpj_Filtertype = "Character";
         Ddo_contratada_pessoacnpj_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratada_pessoacnpj_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratada_pessoacnpj_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratada_pessoacnpj_Titlecontrolidtoreplace = "";
         Ddo_contratada_pessoacnpj_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratada_pessoacnpj_Cls = "ColumnSettings";
         Ddo_contratada_pessoacnpj_Tooltip = "Op��es";
         Ddo_contratada_pessoacnpj_Caption = "";
         Ddo_contratada_pessoanom_Searchbuttontext = "Pesquisar";
         Ddo_contratada_pessoanom_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contratada_pessoanom_Cleanfilter = "Limpar pesquisa";
         Ddo_contratada_pessoanom_Loadingdata = "Carregando dados...";
         Ddo_contratada_pessoanom_Sortdsc = "Ordenar de Z � A";
         Ddo_contratada_pessoanom_Sortasc = "Ordenar de A � Z";
         Ddo_contratada_pessoanom_Datalistupdateminimumcharacters = 0;
         Ddo_contratada_pessoanom_Datalistproc = "GetWWContratoGarantiaFilterData";
         Ddo_contratada_pessoanom_Datalisttype = "Dynamic";
         Ddo_contratada_pessoanom_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contratada_pessoanom_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contratada_pessoanom_Filtertype = "Character";
         Ddo_contratada_pessoanom_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratada_pessoanom_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratada_pessoanom_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratada_pessoanom_Titlecontrolidtoreplace = "";
         Ddo_contratada_pessoanom_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratada_pessoanom_Cls = "ColumnSettings";
         Ddo_contratada_pessoanom_Tooltip = "Op��es";
         Ddo_contratada_pessoanom_Caption = "";
         Ddo_contrato_numero_Searchbuttontext = "Pesquisar";
         Ddo_contrato_numero_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contrato_numero_Cleanfilter = "Limpar pesquisa";
         Ddo_contrato_numero_Loadingdata = "Carregando dados...";
         Ddo_contrato_numero_Sortdsc = "Ordenar de Z � A";
         Ddo_contrato_numero_Sortasc = "Ordenar de A � Z";
         Ddo_contrato_numero_Datalistupdateminimumcharacters = 0;
         Ddo_contrato_numero_Datalistproc = "GetWWContratoGarantiaFilterData";
         Ddo_contrato_numero_Datalisttype = "Dynamic";
         Ddo_contrato_numero_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contrato_numero_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contrato_numero_Filtertype = "Character";
         Ddo_contrato_numero_Includefilter = Convert.ToBoolean( -1);
         Ddo_contrato_numero_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contrato_numero_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contrato_numero_Titlecontrolidtoreplace = "";
         Ddo_contrato_numero_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contrato_numero_Cls = "ColumnSettings";
         Ddo_contrato_numero_Tooltip = "Op��es";
         Ddo_contrato_numero_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = " Contrato Garantia";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A101ContratoGarantia_Codigo',fld:'CONTRATOGARANTIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV54ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_Contratada_PessoaCNPJTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_ContratoGarantia_DataPagtoGarantiaTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_DATAPAGTOGARANTIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_ContratoGarantia_PercentualTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_PERCENTUALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_ContratoGarantia_DataCaucaoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_DATACAUCAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV82ddo_ContratoGarantia_ValorGarantiaTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_VALORGARANTIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV88ddo_ContratoGarantia_DataEncerramentoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_DATAENCERRAMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV92ddo_ContratoGarantia_ValorEncerramentoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_VALORENCERRAMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContratoGarantia_DataPagtoGarantia1',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA1',pic:'',nv:''},{av:'AV17ContratoGarantia_DataPagtoGarantia_To1',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO1',pic:'',nv:''},{av:'AV37ContratoGarantia_DataCaucao1',fld:'vCONTRATOGARANTIA_DATACAUCAO1',pic:'',nv:''},{av:'AV38ContratoGarantia_DataCaucao_To1',fld:'vCONTRATOGARANTIA_DATACAUCAO_TO1',pic:'',nv:''},{av:'AV39ContratoGarantia_DataEncerramento1',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO1',pic:'',nv:''},{av:'AV40ContratoGarantia_DataEncerramento_To1',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO_TO1',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20ContratoGarantia_DataPagtoGarantia2',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA2',pic:'',nv:''},{av:'AV21ContratoGarantia_DataPagtoGarantia_To2',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO2',pic:'',nv:''},{av:'AV41ContratoGarantia_DataCaucao2',fld:'vCONTRATOGARANTIA_DATACAUCAO2',pic:'',nv:''},{av:'AV42ContratoGarantia_DataCaucao_To2',fld:'vCONTRATOGARANTIA_DATACAUCAO_TO2',pic:'',nv:''},{av:'AV43ContratoGarantia_DataEncerramento2',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO2',pic:'',nv:''},{av:'AV44ContratoGarantia_DataEncerramento_To2',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO_TO2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24ContratoGarantia_DataPagtoGarantia3',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA3',pic:'',nv:''},{av:'AV25ContratoGarantia_DataPagtoGarantia_To3',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO3',pic:'',nv:''},{av:'AV45ContratoGarantia_DataCaucao3',fld:'vCONTRATOGARANTIA_DATACAUCAO3',pic:'',nv:''},{av:'AV46ContratoGarantia_DataCaucao_To3',fld:'vCONTRATOGARANTIA_DATACAUCAO_TO3',pic:'',nv:''},{av:'AV47ContratoGarantia_DataEncerramento3',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO3',pic:'',nv:''},{av:'AV48ContratoGarantia_DataEncerramento_To3',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO_TO3',pic:'',nv:''},{av:'AV52TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV53TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV56TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV57TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV60TFContratada_PessoaCNPJ',fld:'vTFCONTRATADA_PESSOACNPJ',pic:'',nv:''},{av:'AV61TFContratada_PessoaCNPJ_Sel',fld:'vTFCONTRATADA_PESSOACNPJ_SEL',pic:'',nv:''},{av:'AV64TFContratoGarantia_DataPagtoGarantia',fld:'vTFCONTRATOGARANTIA_DATAPAGTOGARANTIA',pic:'',nv:''},{av:'AV65TFContratoGarantia_DataPagtoGarantia_To',fld:'vTFCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO',pic:'',nv:''},{av:'AV70TFContratoGarantia_Percentual',fld:'vTFCONTRATOGARANTIA_PERCENTUAL',pic:'ZZ9.99',nv:0.0},{av:'AV71TFContratoGarantia_Percentual_To',fld:'vTFCONTRATOGARANTIA_PERCENTUAL_TO',pic:'ZZ9.99',nv:0.0},{av:'AV74TFContratoGarantia_DataCaucao',fld:'vTFCONTRATOGARANTIA_DATACAUCAO',pic:'',nv:''},{av:'AV75TFContratoGarantia_DataCaucao_To',fld:'vTFCONTRATOGARANTIA_DATACAUCAO_TO',pic:'',nv:''},{av:'AV80TFContratoGarantia_ValorGarantia',fld:'vTFCONTRATOGARANTIA_VALORGARANTIA',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV81TFContratoGarantia_ValorGarantia_To',fld:'vTFCONTRATOGARANTIA_VALORGARANTIA_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV84TFContratoGarantia_DataEncerramento',fld:'vTFCONTRATOGARANTIA_DATAENCERRAMENTO',pic:'',nv:''},{av:'AV85TFContratoGarantia_DataEncerramento_To',fld:'vTFCONTRATOGARANTIA_DATAENCERRAMENTO_TO',pic:'',nv:''},{av:'AV90TFContratoGarantia_ValorEncerramento',fld:'vTFCONTRATOGARANTIA_VALORENCERRAMENTO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV91TFContratoGarantia_ValorEncerramento_To',fld:'vTFCONTRATOGARANTIA_VALORENCERRAMENTO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV157Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV51Contrato_NumeroTitleFilterData',fld:'vCONTRATO_NUMEROTITLEFILTERDATA',pic:'',nv:null},{av:'AV55Contratada_PessoaNomTitleFilterData',fld:'vCONTRATADA_PESSOANOMTITLEFILTERDATA',pic:'',nv:null},{av:'AV59Contratada_PessoaCNPJTitleFilterData',fld:'vCONTRATADA_PESSOACNPJTITLEFILTERDATA',pic:'',nv:null},{av:'AV63ContratoGarantia_DataPagtoGarantiaTitleFilterData',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIATITLEFILTERDATA',pic:'',nv:null},{av:'AV69ContratoGarantia_PercentualTitleFilterData',fld:'vCONTRATOGARANTIA_PERCENTUALTITLEFILTERDATA',pic:'',nv:null},{av:'AV73ContratoGarantia_DataCaucaoTitleFilterData',fld:'vCONTRATOGARANTIA_DATACAUCAOTITLEFILTERDATA',pic:'',nv:null},{av:'AV79ContratoGarantia_ValorGarantiaTitleFilterData',fld:'vCONTRATOGARANTIA_VALORGARANTIATITLEFILTERDATA',pic:'',nv:null},{av:'AV83ContratoGarantia_DataEncerramentoTitleFilterData',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTOTITLEFILTERDATA',pic:'',nv:null},{av:'AV89ContratoGarantia_ValorEncerramentoTitleFilterData',fld:'vCONTRATOGARANTIA_VALORENCERRAMENTOTITLEFILTERDATA',pic:'',nv:null},{av:'edtContrato_Numero_Titleformat',ctrl:'CONTRATO_NUMERO',prop:'Titleformat'},{av:'edtContrato_Numero_Title',ctrl:'CONTRATO_NUMERO',prop:'Title'},{av:'edtContratada_PessoaNom_Titleformat',ctrl:'CONTRATADA_PESSOANOM',prop:'Titleformat'},{av:'edtContratada_PessoaNom_Title',ctrl:'CONTRATADA_PESSOANOM',prop:'Title'},{av:'edtContratada_PessoaCNPJ_Titleformat',ctrl:'CONTRATADA_PESSOACNPJ',prop:'Titleformat'},{av:'edtContratada_PessoaCNPJ_Title',ctrl:'CONTRATADA_PESSOACNPJ',prop:'Title'},{av:'edtContratoGarantia_DataPagtoGarantia_Titleformat',ctrl:'CONTRATOGARANTIA_DATAPAGTOGARANTIA',prop:'Titleformat'},{av:'edtContratoGarantia_DataPagtoGarantia_Title',ctrl:'CONTRATOGARANTIA_DATAPAGTOGARANTIA',prop:'Title'},{av:'edtContratoGarantia_Percentual_Titleformat',ctrl:'CONTRATOGARANTIA_PERCENTUAL',prop:'Titleformat'},{av:'edtContratoGarantia_Percentual_Title',ctrl:'CONTRATOGARANTIA_PERCENTUAL',prop:'Title'},{av:'edtContratoGarantia_DataCaucao_Titleformat',ctrl:'CONTRATOGARANTIA_DATACAUCAO',prop:'Titleformat'},{av:'edtContratoGarantia_DataCaucao_Title',ctrl:'CONTRATOGARANTIA_DATACAUCAO',prop:'Title'},{av:'edtContratoGarantia_ValorGarantia_Titleformat',ctrl:'CONTRATOGARANTIA_VALORGARANTIA',prop:'Titleformat'},{av:'edtContratoGarantia_ValorGarantia_Title',ctrl:'CONTRATOGARANTIA_VALORGARANTIA',prop:'Title'},{av:'edtContratoGarantia_DataEncerramento_Titleformat',ctrl:'CONTRATOGARANTIA_DATAENCERRAMENTO',prop:'Titleformat'},{av:'edtContratoGarantia_DataEncerramento_Title',ctrl:'CONTRATOGARANTIA_DATAENCERRAMENTO',prop:'Title'},{av:'edtContratoGarantia_ValorEncerramento_Titleformat',ctrl:'CONTRATOGARANTIA_VALORENCERRAMENTO',prop:'Titleformat'},{av:'edtContratoGarantia_ValorEncerramento_Title',ctrl:'CONTRATOGARANTIA_VALORENCERRAMENTO',prop:'Title'},{av:'AV95GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV96GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E116H2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContratoGarantia_DataPagtoGarantia1',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA1',pic:'',nv:''},{av:'AV17ContratoGarantia_DataPagtoGarantia_To1',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO1',pic:'',nv:''},{av:'AV37ContratoGarantia_DataCaucao1',fld:'vCONTRATOGARANTIA_DATACAUCAO1',pic:'',nv:''},{av:'AV38ContratoGarantia_DataCaucao_To1',fld:'vCONTRATOGARANTIA_DATACAUCAO_TO1',pic:'',nv:''},{av:'AV39ContratoGarantia_DataEncerramento1',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO1',pic:'',nv:''},{av:'AV40ContratoGarantia_DataEncerramento_To1',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20ContratoGarantia_DataPagtoGarantia2',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA2',pic:'',nv:''},{av:'AV21ContratoGarantia_DataPagtoGarantia_To2',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO2',pic:'',nv:''},{av:'AV41ContratoGarantia_DataCaucao2',fld:'vCONTRATOGARANTIA_DATACAUCAO2',pic:'',nv:''},{av:'AV42ContratoGarantia_DataCaucao_To2',fld:'vCONTRATOGARANTIA_DATACAUCAO_TO2',pic:'',nv:''},{av:'AV43ContratoGarantia_DataEncerramento2',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO2',pic:'',nv:''},{av:'AV44ContratoGarantia_DataEncerramento_To2',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24ContratoGarantia_DataPagtoGarantia3',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA3',pic:'',nv:''},{av:'AV25ContratoGarantia_DataPagtoGarantia_To3',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO3',pic:'',nv:''},{av:'AV45ContratoGarantia_DataCaucao3',fld:'vCONTRATOGARANTIA_DATACAUCAO3',pic:'',nv:''},{av:'AV46ContratoGarantia_DataCaucao_To3',fld:'vCONTRATOGARANTIA_DATACAUCAO_TO3',pic:'',nv:''},{av:'AV47ContratoGarantia_DataEncerramento3',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO3',pic:'',nv:''},{av:'AV48ContratoGarantia_DataEncerramento_To3',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV52TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV53TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV56TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV57TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV60TFContratada_PessoaCNPJ',fld:'vTFCONTRATADA_PESSOACNPJ',pic:'',nv:''},{av:'AV61TFContratada_PessoaCNPJ_Sel',fld:'vTFCONTRATADA_PESSOACNPJ_SEL',pic:'',nv:''},{av:'AV64TFContratoGarantia_DataPagtoGarantia',fld:'vTFCONTRATOGARANTIA_DATAPAGTOGARANTIA',pic:'',nv:''},{av:'AV65TFContratoGarantia_DataPagtoGarantia_To',fld:'vTFCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO',pic:'',nv:''},{av:'AV70TFContratoGarantia_Percentual',fld:'vTFCONTRATOGARANTIA_PERCENTUAL',pic:'ZZ9.99',nv:0.0},{av:'AV71TFContratoGarantia_Percentual_To',fld:'vTFCONTRATOGARANTIA_PERCENTUAL_TO',pic:'ZZ9.99',nv:0.0},{av:'AV74TFContratoGarantia_DataCaucao',fld:'vTFCONTRATOGARANTIA_DATACAUCAO',pic:'',nv:''},{av:'AV75TFContratoGarantia_DataCaucao_To',fld:'vTFCONTRATOGARANTIA_DATACAUCAO_TO',pic:'',nv:''},{av:'AV80TFContratoGarantia_ValorGarantia',fld:'vTFCONTRATOGARANTIA_VALORGARANTIA',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV81TFContratoGarantia_ValorGarantia_To',fld:'vTFCONTRATOGARANTIA_VALORGARANTIA_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV84TFContratoGarantia_DataEncerramento',fld:'vTFCONTRATOGARANTIA_DATAENCERRAMENTO',pic:'',nv:''},{av:'AV85TFContratoGarantia_DataEncerramento_To',fld:'vTFCONTRATOGARANTIA_DATAENCERRAMENTO_TO',pic:'',nv:''},{av:'AV90TFContratoGarantia_ValorEncerramento',fld:'vTFCONTRATOGARANTIA_VALORENCERRAMENTO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV91TFContratoGarantia_ValorEncerramento_To',fld:'vTFCONTRATOGARANTIA_VALORENCERRAMENTO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV54ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_Contratada_PessoaCNPJTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_ContratoGarantia_DataPagtoGarantiaTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_DATAPAGTOGARANTIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_ContratoGarantia_PercentualTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_PERCENTUALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_ContratoGarantia_DataCaucaoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_DATACAUCAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV82ddo_ContratoGarantia_ValorGarantiaTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_VALORGARANTIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV88ddo_ContratoGarantia_DataEncerramentoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_DATAENCERRAMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV92ddo_ContratoGarantia_ValorEncerramentoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_VALORENCERRAMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV157Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A101ContratoGarantia_Codigo',fld:'CONTRATOGARANTIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_CONTRATO_NUMERO.ONOPTIONCLICKED","{handler:'E126H2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContratoGarantia_DataPagtoGarantia1',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA1',pic:'',nv:''},{av:'AV17ContratoGarantia_DataPagtoGarantia_To1',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO1',pic:'',nv:''},{av:'AV37ContratoGarantia_DataCaucao1',fld:'vCONTRATOGARANTIA_DATACAUCAO1',pic:'',nv:''},{av:'AV38ContratoGarantia_DataCaucao_To1',fld:'vCONTRATOGARANTIA_DATACAUCAO_TO1',pic:'',nv:''},{av:'AV39ContratoGarantia_DataEncerramento1',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO1',pic:'',nv:''},{av:'AV40ContratoGarantia_DataEncerramento_To1',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20ContratoGarantia_DataPagtoGarantia2',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA2',pic:'',nv:''},{av:'AV21ContratoGarantia_DataPagtoGarantia_To2',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO2',pic:'',nv:''},{av:'AV41ContratoGarantia_DataCaucao2',fld:'vCONTRATOGARANTIA_DATACAUCAO2',pic:'',nv:''},{av:'AV42ContratoGarantia_DataCaucao_To2',fld:'vCONTRATOGARANTIA_DATACAUCAO_TO2',pic:'',nv:''},{av:'AV43ContratoGarantia_DataEncerramento2',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO2',pic:'',nv:''},{av:'AV44ContratoGarantia_DataEncerramento_To2',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24ContratoGarantia_DataPagtoGarantia3',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA3',pic:'',nv:''},{av:'AV25ContratoGarantia_DataPagtoGarantia_To3',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO3',pic:'',nv:''},{av:'AV45ContratoGarantia_DataCaucao3',fld:'vCONTRATOGARANTIA_DATACAUCAO3',pic:'',nv:''},{av:'AV46ContratoGarantia_DataCaucao_To3',fld:'vCONTRATOGARANTIA_DATACAUCAO_TO3',pic:'',nv:''},{av:'AV47ContratoGarantia_DataEncerramento3',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO3',pic:'',nv:''},{av:'AV48ContratoGarantia_DataEncerramento_To3',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV52TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV53TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV56TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV57TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV60TFContratada_PessoaCNPJ',fld:'vTFCONTRATADA_PESSOACNPJ',pic:'',nv:''},{av:'AV61TFContratada_PessoaCNPJ_Sel',fld:'vTFCONTRATADA_PESSOACNPJ_SEL',pic:'',nv:''},{av:'AV64TFContratoGarantia_DataPagtoGarantia',fld:'vTFCONTRATOGARANTIA_DATAPAGTOGARANTIA',pic:'',nv:''},{av:'AV65TFContratoGarantia_DataPagtoGarantia_To',fld:'vTFCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO',pic:'',nv:''},{av:'AV70TFContratoGarantia_Percentual',fld:'vTFCONTRATOGARANTIA_PERCENTUAL',pic:'ZZ9.99',nv:0.0},{av:'AV71TFContratoGarantia_Percentual_To',fld:'vTFCONTRATOGARANTIA_PERCENTUAL_TO',pic:'ZZ9.99',nv:0.0},{av:'AV74TFContratoGarantia_DataCaucao',fld:'vTFCONTRATOGARANTIA_DATACAUCAO',pic:'',nv:''},{av:'AV75TFContratoGarantia_DataCaucao_To',fld:'vTFCONTRATOGARANTIA_DATACAUCAO_TO',pic:'',nv:''},{av:'AV80TFContratoGarantia_ValorGarantia',fld:'vTFCONTRATOGARANTIA_VALORGARANTIA',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV81TFContratoGarantia_ValorGarantia_To',fld:'vTFCONTRATOGARANTIA_VALORGARANTIA_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV84TFContratoGarantia_DataEncerramento',fld:'vTFCONTRATOGARANTIA_DATAENCERRAMENTO',pic:'',nv:''},{av:'AV85TFContratoGarantia_DataEncerramento_To',fld:'vTFCONTRATOGARANTIA_DATAENCERRAMENTO_TO',pic:'',nv:''},{av:'AV90TFContratoGarantia_ValorEncerramento',fld:'vTFCONTRATOGARANTIA_VALORENCERRAMENTO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV91TFContratoGarantia_ValorEncerramento_To',fld:'vTFCONTRATOGARANTIA_VALORENCERRAMENTO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV54ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_Contratada_PessoaCNPJTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_ContratoGarantia_DataPagtoGarantiaTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_DATAPAGTOGARANTIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_ContratoGarantia_PercentualTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_PERCENTUALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_ContratoGarantia_DataCaucaoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_DATACAUCAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV82ddo_ContratoGarantia_ValorGarantiaTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_VALORGARANTIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV88ddo_ContratoGarantia_DataEncerramentoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_DATAENCERRAMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV92ddo_ContratoGarantia_ValorEncerramentoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_VALORENCERRAMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV157Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A101ContratoGarantia_Codigo',fld:'CONTRATOGARANTIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contrato_numero_Activeeventkey',ctrl:'DDO_CONTRATO_NUMERO',prop:'ActiveEventKey'},{av:'Ddo_contrato_numero_Filteredtext_get',ctrl:'DDO_CONTRATO_NUMERO',prop:'FilteredText_get'},{av:'Ddo_contrato_numero_Selectedvalue_get',ctrl:'DDO_CONTRATO_NUMERO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contrato_numero_Sortedstatus',ctrl:'DDO_CONTRATO_NUMERO',prop:'SortedStatus'},{av:'AV52TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV53TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'Ddo_contratada_pessoanom_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOANOM',prop:'SortedStatus'},{av:'Ddo_contratada_pessoacnpj_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOACNPJ',prop:'SortedStatus'},{av:'Ddo_contratogarantia_datapagtogarantia_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA',prop:'SortedStatus'},{av:'Ddo_contratogarantia_percentual_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_PERCENTUAL',prop:'SortedStatus'},{av:'Ddo_contratogarantia_datacaucao_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_DATACAUCAO',prop:'SortedStatus'},{av:'Ddo_contratogarantia_valorgarantia_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_VALORGARANTIA',prop:'SortedStatus'},{av:'Ddo_contratogarantia_dataencerramento_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_DATAENCERRAMENTO',prop:'SortedStatus'},{av:'Ddo_contratogarantia_valorencerramento_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_VALORENCERRAMENTO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATADA_PESSOANOM.ONOPTIONCLICKED","{handler:'E136H2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContratoGarantia_DataPagtoGarantia1',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA1',pic:'',nv:''},{av:'AV17ContratoGarantia_DataPagtoGarantia_To1',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO1',pic:'',nv:''},{av:'AV37ContratoGarantia_DataCaucao1',fld:'vCONTRATOGARANTIA_DATACAUCAO1',pic:'',nv:''},{av:'AV38ContratoGarantia_DataCaucao_To1',fld:'vCONTRATOGARANTIA_DATACAUCAO_TO1',pic:'',nv:''},{av:'AV39ContratoGarantia_DataEncerramento1',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO1',pic:'',nv:''},{av:'AV40ContratoGarantia_DataEncerramento_To1',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20ContratoGarantia_DataPagtoGarantia2',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA2',pic:'',nv:''},{av:'AV21ContratoGarantia_DataPagtoGarantia_To2',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO2',pic:'',nv:''},{av:'AV41ContratoGarantia_DataCaucao2',fld:'vCONTRATOGARANTIA_DATACAUCAO2',pic:'',nv:''},{av:'AV42ContratoGarantia_DataCaucao_To2',fld:'vCONTRATOGARANTIA_DATACAUCAO_TO2',pic:'',nv:''},{av:'AV43ContratoGarantia_DataEncerramento2',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO2',pic:'',nv:''},{av:'AV44ContratoGarantia_DataEncerramento_To2',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24ContratoGarantia_DataPagtoGarantia3',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA3',pic:'',nv:''},{av:'AV25ContratoGarantia_DataPagtoGarantia_To3',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO3',pic:'',nv:''},{av:'AV45ContratoGarantia_DataCaucao3',fld:'vCONTRATOGARANTIA_DATACAUCAO3',pic:'',nv:''},{av:'AV46ContratoGarantia_DataCaucao_To3',fld:'vCONTRATOGARANTIA_DATACAUCAO_TO3',pic:'',nv:''},{av:'AV47ContratoGarantia_DataEncerramento3',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO3',pic:'',nv:''},{av:'AV48ContratoGarantia_DataEncerramento_To3',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV52TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV53TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV56TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV57TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV60TFContratada_PessoaCNPJ',fld:'vTFCONTRATADA_PESSOACNPJ',pic:'',nv:''},{av:'AV61TFContratada_PessoaCNPJ_Sel',fld:'vTFCONTRATADA_PESSOACNPJ_SEL',pic:'',nv:''},{av:'AV64TFContratoGarantia_DataPagtoGarantia',fld:'vTFCONTRATOGARANTIA_DATAPAGTOGARANTIA',pic:'',nv:''},{av:'AV65TFContratoGarantia_DataPagtoGarantia_To',fld:'vTFCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO',pic:'',nv:''},{av:'AV70TFContratoGarantia_Percentual',fld:'vTFCONTRATOGARANTIA_PERCENTUAL',pic:'ZZ9.99',nv:0.0},{av:'AV71TFContratoGarantia_Percentual_To',fld:'vTFCONTRATOGARANTIA_PERCENTUAL_TO',pic:'ZZ9.99',nv:0.0},{av:'AV74TFContratoGarantia_DataCaucao',fld:'vTFCONTRATOGARANTIA_DATACAUCAO',pic:'',nv:''},{av:'AV75TFContratoGarantia_DataCaucao_To',fld:'vTFCONTRATOGARANTIA_DATACAUCAO_TO',pic:'',nv:''},{av:'AV80TFContratoGarantia_ValorGarantia',fld:'vTFCONTRATOGARANTIA_VALORGARANTIA',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV81TFContratoGarantia_ValorGarantia_To',fld:'vTFCONTRATOGARANTIA_VALORGARANTIA_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV84TFContratoGarantia_DataEncerramento',fld:'vTFCONTRATOGARANTIA_DATAENCERRAMENTO',pic:'',nv:''},{av:'AV85TFContratoGarantia_DataEncerramento_To',fld:'vTFCONTRATOGARANTIA_DATAENCERRAMENTO_TO',pic:'',nv:''},{av:'AV90TFContratoGarantia_ValorEncerramento',fld:'vTFCONTRATOGARANTIA_VALORENCERRAMENTO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV91TFContratoGarantia_ValorEncerramento_To',fld:'vTFCONTRATOGARANTIA_VALORENCERRAMENTO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV54ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_Contratada_PessoaCNPJTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_ContratoGarantia_DataPagtoGarantiaTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_DATAPAGTOGARANTIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_ContratoGarantia_PercentualTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_PERCENTUALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_ContratoGarantia_DataCaucaoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_DATACAUCAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV82ddo_ContratoGarantia_ValorGarantiaTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_VALORGARANTIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV88ddo_ContratoGarantia_DataEncerramentoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_DATAENCERRAMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV92ddo_ContratoGarantia_ValorEncerramentoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_VALORENCERRAMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV157Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A101ContratoGarantia_Codigo',fld:'CONTRATOGARANTIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contratada_pessoanom_Activeeventkey',ctrl:'DDO_CONTRATADA_PESSOANOM',prop:'ActiveEventKey'},{av:'Ddo_contratada_pessoanom_Filteredtext_get',ctrl:'DDO_CONTRATADA_PESSOANOM',prop:'FilteredText_get'},{av:'Ddo_contratada_pessoanom_Selectedvalue_get',ctrl:'DDO_CONTRATADA_PESSOANOM',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratada_pessoanom_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOANOM',prop:'SortedStatus'},{av:'AV56TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV57TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'Ddo_contrato_numero_Sortedstatus',ctrl:'DDO_CONTRATO_NUMERO',prop:'SortedStatus'},{av:'Ddo_contratada_pessoacnpj_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOACNPJ',prop:'SortedStatus'},{av:'Ddo_contratogarantia_datapagtogarantia_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA',prop:'SortedStatus'},{av:'Ddo_contratogarantia_percentual_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_PERCENTUAL',prop:'SortedStatus'},{av:'Ddo_contratogarantia_datacaucao_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_DATACAUCAO',prop:'SortedStatus'},{av:'Ddo_contratogarantia_valorgarantia_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_VALORGARANTIA',prop:'SortedStatus'},{av:'Ddo_contratogarantia_dataencerramento_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_DATAENCERRAMENTO',prop:'SortedStatus'},{av:'Ddo_contratogarantia_valorencerramento_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_VALORENCERRAMENTO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATADA_PESSOACNPJ.ONOPTIONCLICKED","{handler:'E146H2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContratoGarantia_DataPagtoGarantia1',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA1',pic:'',nv:''},{av:'AV17ContratoGarantia_DataPagtoGarantia_To1',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO1',pic:'',nv:''},{av:'AV37ContratoGarantia_DataCaucao1',fld:'vCONTRATOGARANTIA_DATACAUCAO1',pic:'',nv:''},{av:'AV38ContratoGarantia_DataCaucao_To1',fld:'vCONTRATOGARANTIA_DATACAUCAO_TO1',pic:'',nv:''},{av:'AV39ContratoGarantia_DataEncerramento1',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO1',pic:'',nv:''},{av:'AV40ContratoGarantia_DataEncerramento_To1',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20ContratoGarantia_DataPagtoGarantia2',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA2',pic:'',nv:''},{av:'AV21ContratoGarantia_DataPagtoGarantia_To2',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO2',pic:'',nv:''},{av:'AV41ContratoGarantia_DataCaucao2',fld:'vCONTRATOGARANTIA_DATACAUCAO2',pic:'',nv:''},{av:'AV42ContratoGarantia_DataCaucao_To2',fld:'vCONTRATOGARANTIA_DATACAUCAO_TO2',pic:'',nv:''},{av:'AV43ContratoGarantia_DataEncerramento2',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO2',pic:'',nv:''},{av:'AV44ContratoGarantia_DataEncerramento_To2',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24ContratoGarantia_DataPagtoGarantia3',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA3',pic:'',nv:''},{av:'AV25ContratoGarantia_DataPagtoGarantia_To3',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO3',pic:'',nv:''},{av:'AV45ContratoGarantia_DataCaucao3',fld:'vCONTRATOGARANTIA_DATACAUCAO3',pic:'',nv:''},{av:'AV46ContratoGarantia_DataCaucao_To3',fld:'vCONTRATOGARANTIA_DATACAUCAO_TO3',pic:'',nv:''},{av:'AV47ContratoGarantia_DataEncerramento3',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO3',pic:'',nv:''},{av:'AV48ContratoGarantia_DataEncerramento_To3',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV52TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV53TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV56TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV57TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV60TFContratada_PessoaCNPJ',fld:'vTFCONTRATADA_PESSOACNPJ',pic:'',nv:''},{av:'AV61TFContratada_PessoaCNPJ_Sel',fld:'vTFCONTRATADA_PESSOACNPJ_SEL',pic:'',nv:''},{av:'AV64TFContratoGarantia_DataPagtoGarantia',fld:'vTFCONTRATOGARANTIA_DATAPAGTOGARANTIA',pic:'',nv:''},{av:'AV65TFContratoGarantia_DataPagtoGarantia_To',fld:'vTFCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO',pic:'',nv:''},{av:'AV70TFContratoGarantia_Percentual',fld:'vTFCONTRATOGARANTIA_PERCENTUAL',pic:'ZZ9.99',nv:0.0},{av:'AV71TFContratoGarantia_Percentual_To',fld:'vTFCONTRATOGARANTIA_PERCENTUAL_TO',pic:'ZZ9.99',nv:0.0},{av:'AV74TFContratoGarantia_DataCaucao',fld:'vTFCONTRATOGARANTIA_DATACAUCAO',pic:'',nv:''},{av:'AV75TFContratoGarantia_DataCaucao_To',fld:'vTFCONTRATOGARANTIA_DATACAUCAO_TO',pic:'',nv:''},{av:'AV80TFContratoGarantia_ValorGarantia',fld:'vTFCONTRATOGARANTIA_VALORGARANTIA',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV81TFContratoGarantia_ValorGarantia_To',fld:'vTFCONTRATOGARANTIA_VALORGARANTIA_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV84TFContratoGarantia_DataEncerramento',fld:'vTFCONTRATOGARANTIA_DATAENCERRAMENTO',pic:'',nv:''},{av:'AV85TFContratoGarantia_DataEncerramento_To',fld:'vTFCONTRATOGARANTIA_DATAENCERRAMENTO_TO',pic:'',nv:''},{av:'AV90TFContratoGarantia_ValorEncerramento',fld:'vTFCONTRATOGARANTIA_VALORENCERRAMENTO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV91TFContratoGarantia_ValorEncerramento_To',fld:'vTFCONTRATOGARANTIA_VALORENCERRAMENTO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV54ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_Contratada_PessoaCNPJTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_ContratoGarantia_DataPagtoGarantiaTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_DATAPAGTOGARANTIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_ContratoGarantia_PercentualTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_PERCENTUALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_ContratoGarantia_DataCaucaoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_DATACAUCAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV82ddo_ContratoGarantia_ValorGarantiaTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_VALORGARANTIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV88ddo_ContratoGarantia_DataEncerramentoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_DATAENCERRAMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV92ddo_ContratoGarantia_ValorEncerramentoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_VALORENCERRAMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV157Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A101ContratoGarantia_Codigo',fld:'CONTRATOGARANTIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contratada_pessoacnpj_Activeeventkey',ctrl:'DDO_CONTRATADA_PESSOACNPJ',prop:'ActiveEventKey'},{av:'Ddo_contratada_pessoacnpj_Filteredtext_get',ctrl:'DDO_CONTRATADA_PESSOACNPJ',prop:'FilteredText_get'},{av:'Ddo_contratada_pessoacnpj_Selectedvalue_get',ctrl:'DDO_CONTRATADA_PESSOACNPJ',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratada_pessoacnpj_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOACNPJ',prop:'SortedStatus'},{av:'AV60TFContratada_PessoaCNPJ',fld:'vTFCONTRATADA_PESSOACNPJ',pic:'',nv:''},{av:'AV61TFContratada_PessoaCNPJ_Sel',fld:'vTFCONTRATADA_PESSOACNPJ_SEL',pic:'',nv:''},{av:'Ddo_contrato_numero_Sortedstatus',ctrl:'DDO_CONTRATO_NUMERO',prop:'SortedStatus'},{av:'Ddo_contratada_pessoanom_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOANOM',prop:'SortedStatus'},{av:'Ddo_contratogarantia_datapagtogarantia_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA',prop:'SortedStatus'},{av:'Ddo_contratogarantia_percentual_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_PERCENTUAL',prop:'SortedStatus'},{av:'Ddo_contratogarantia_datacaucao_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_DATACAUCAO',prop:'SortedStatus'},{av:'Ddo_contratogarantia_valorgarantia_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_VALORGARANTIA',prop:'SortedStatus'},{av:'Ddo_contratogarantia_dataencerramento_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_DATAENCERRAMENTO',prop:'SortedStatus'},{av:'Ddo_contratogarantia_valorencerramento_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_VALORENCERRAMENTO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA.ONOPTIONCLICKED","{handler:'E156H2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContratoGarantia_DataPagtoGarantia1',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA1',pic:'',nv:''},{av:'AV17ContratoGarantia_DataPagtoGarantia_To1',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO1',pic:'',nv:''},{av:'AV37ContratoGarantia_DataCaucao1',fld:'vCONTRATOGARANTIA_DATACAUCAO1',pic:'',nv:''},{av:'AV38ContratoGarantia_DataCaucao_To1',fld:'vCONTRATOGARANTIA_DATACAUCAO_TO1',pic:'',nv:''},{av:'AV39ContratoGarantia_DataEncerramento1',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO1',pic:'',nv:''},{av:'AV40ContratoGarantia_DataEncerramento_To1',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20ContratoGarantia_DataPagtoGarantia2',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA2',pic:'',nv:''},{av:'AV21ContratoGarantia_DataPagtoGarantia_To2',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO2',pic:'',nv:''},{av:'AV41ContratoGarantia_DataCaucao2',fld:'vCONTRATOGARANTIA_DATACAUCAO2',pic:'',nv:''},{av:'AV42ContratoGarantia_DataCaucao_To2',fld:'vCONTRATOGARANTIA_DATACAUCAO_TO2',pic:'',nv:''},{av:'AV43ContratoGarantia_DataEncerramento2',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO2',pic:'',nv:''},{av:'AV44ContratoGarantia_DataEncerramento_To2',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24ContratoGarantia_DataPagtoGarantia3',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA3',pic:'',nv:''},{av:'AV25ContratoGarantia_DataPagtoGarantia_To3',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO3',pic:'',nv:''},{av:'AV45ContratoGarantia_DataCaucao3',fld:'vCONTRATOGARANTIA_DATACAUCAO3',pic:'',nv:''},{av:'AV46ContratoGarantia_DataCaucao_To3',fld:'vCONTRATOGARANTIA_DATACAUCAO_TO3',pic:'',nv:''},{av:'AV47ContratoGarantia_DataEncerramento3',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO3',pic:'',nv:''},{av:'AV48ContratoGarantia_DataEncerramento_To3',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV52TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV53TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV56TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV57TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV60TFContratada_PessoaCNPJ',fld:'vTFCONTRATADA_PESSOACNPJ',pic:'',nv:''},{av:'AV61TFContratada_PessoaCNPJ_Sel',fld:'vTFCONTRATADA_PESSOACNPJ_SEL',pic:'',nv:''},{av:'AV64TFContratoGarantia_DataPagtoGarantia',fld:'vTFCONTRATOGARANTIA_DATAPAGTOGARANTIA',pic:'',nv:''},{av:'AV65TFContratoGarantia_DataPagtoGarantia_To',fld:'vTFCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO',pic:'',nv:''},{av:'AV70TFContratoGarantia_Percentual',fld:'vTFCONTRATOGARANTIA_PERCENTUAL',pic:'ZZ9.99',nv:0.0},{av:'AV71TFContratoGarantia_Percentual_To',fld:'vTFCONTRATOGARANTIA_PERCENTUAL_TO',pic:'ZZ9.99',nv:0.0},{av:'AV74TFContratoGarantia_DataCaucao',fld:'vTFCONTRATOGARANTIA_DATACAUCAO',pic:'',nv:''},{av:'AV75TFContratoGarantia_DataCaucao_To',fld:'vTFCONTRATOGARANTIA_DATACAUCAO_TO',pic:'',nv:''},{av:'AV80TFContratoGarantia_ValorGarantia',fld:'vTFCONTRATOGARANTIA_VALORGARANTIA',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV81TFContratoGarantia_ValorGarantia_To',fld:'vTFCONTRATOGARANTIA_VALORGARANTIA_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV84TFContratoGarantia_DataEncerramento',fld:'vTFCONTRATOGARANTIA_DATAENCERRAMENTO',pic:'',nv:''},{av:'AV85TFContratoGarantia_DataEncerramento_To',fld:'vTFCONTRATOGARANTIA_DATAENCERRAMENTO_TO',pic:'',nv:''},{av:'AV90TFContratoGarantia_ValorEncerramento',fld:'vTFCONTRATOGARANTIA_VALORENCERRAMENTO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV91TFContratoGarantia_ValorEncerramento_To',fld:'vTFCONTRATOGARANTIA_VALORENCERRAMENTO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV54ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_Contratada_PessoaCNPJTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_ContratoGarantia_DataPagtoGarantiaTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_DATAPAGTOGARANTIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_ContratoGarantia_PercentualTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_PERCENTUALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_ContratoGarantia_DataCaucaoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_DATACAUCAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV82ddo_ContratoGarantia_ValorGarantiaTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_VALORGARANTIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV88ddo_ContratoGarantia_DataEncerramentoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_DATAENCERRAMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV92ddo_ContratoGarantia_ValorEncerramentoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_VALORENCERRAMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV157Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A101ContratoGarantia_Codigo',fld:'CONTRATOGARANTIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contratogarantia_datapagtogarantia_Activeeventkey',ctrl:'DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA',prop:'ActiveEventKey'},{av:'Ddo_contratogarantia_datapagtogarantia_Filteredtext_get',ctrl:'DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA',prop:'FilteredText_get'},{av:'Ddo_contratogarantia_datapagtogarantia_Filteredtextto_get',ctrl:'DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratogarantia_datapagtogarantia_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA',prop:'SortedStatus'},{av:'AV64TFContratoGarantia_DataPagtoGarantia',fld:'vTFCONTRATOGARANTIA_DATAPAGTOGARANTIA',pic:'',nv:''},{av:'AV65TFContratoGarantia_DataPagtoGarantia_To',fld:'vTFCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO',pic:'',nv:''},{av:'Ddo_contrato_numero_Sortedstatus',ctrl:'DDO_CONTRATO_NUMERO',prop:'SortedStatus'},{av:'Ddo_contratada_pessoanom_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOANOM',prop:'SortedStatus'},{av:'Ddo_contratada_pessoacnpj_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOACNPJ',prop:'SortedStatus'},{av:'Ddo_contratogarantia_percentual_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_PERCENTUAL',prop:'SortedStatus'},{av:'Ddo_contratogarantia_datacaucao_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_DATACAUCAO',prop:'SortedStatus'},{av:'Ddo_contratogarantia_valorgarantia_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_VALORGARANTIA',prop:'SortedStatus'},{av:'Ddo_contratogarantia_dataencerramento_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_DATAENCERRAMENTO',prop:'SortedStatus'},{av:'Ddo_contratogarantia_valorencerramento_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_VALORENCERRAMENTO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOGARANTIA_PERCENTUAL.ONOPTIONCLICKED","{handler:'E166H2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContratoGarantia_DataPagtoGarantia1',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA1',pic:'',nv:''},{av:'AV17ContratoGarantia_DataPagtoGarantia_To1',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO1',pic:'',nv:''},{av:'AV37ContratoGarantia_DataCaucao1',fld:'vCONTRATOGARANTIA_DATACAUCAO1',pic:'',nv:''},{av:'AV38ContratoGarantia_DataCaucao_To1',fld:'vCONTRATOGARANTIA_DATACAUCAO_TO1',pic:'',nv:''},{av:'AV39ContratoGarantia_DataEncerramento1',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO1',pic:'',nv:''},{av:'AV40ContratoGarantia_DataEncerramento_To1',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20ContratoGarantia_DataPagtoGarantia2',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA2',pic:'',nv:''},{av:'AV21ContratoGarantia_DataPagtoGarantia_To2',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO2',pic:'',nv:''},{av:'AV41ContratoGarantia_DataCaucao2',fld:'vCONTRATOGARANTIA_DATACAUCAO2',pic:'',nv:''},{av:'AV42ContratoGarantia_DataCaucao_To2',fld:'vCONTRATOGARANTIA_DATACAUCAO_TO2',pic:'',nv:''},{av:'AV43ContratoGarantia_DataEncerramento2',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO2',pic:'',nv:''},{av:'AV44ContratoGarantia_DataEncerramento_To2',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24ContratoGarantia_DataPagtoGarantia3',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA3',pic:'',nv:''},{av:'AV25ContratoGarantia_DataPagtoGarantia_To3',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO3',pic:'',nv:''},{av:'AV45ContratoGarantia_DataCaucao3',fld:'vCONTRATOGARANTIA_DATACAUCAO3',pic:'',nv:''},{av:'AV46ContratoGarantia_DataCaucao_To3',fld:'vCONTRATOGARANTIA_DATACAUCAO_TO3',pic:'',nv:''},{av:'AV47ContratoGarantia_DataEncerramento3',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO3',pic:'',nv:''},{av:'AV48ContratoGarantia_DataEncerramento_To3',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV52TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV53TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV56TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV57TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV60TFContratada_PessoaCNPJ',fld:'vTFCONTRATADA_PESSOACNPJ',pic:'',nv:''},{av:'AV61TFContratada_PessoaCNPJ_Sel',fld:'vTFCONTRATADA_PESSOACNPJ_SEL',pic:'',nv:''},{av:'AV64TFContratoGarantia_DataPagtoGarantia',fld:'vTFCONTRATOGARANTIA_DATAPAGTOGARANTIA',pic:'',nv:''},{av:'AV65TFContratoGarantia_DataPagtoGarantia_To',fld:'vTFCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO',pic:'',nv:''},{av:'AV70TFContratoGarantia_Percentual',fld:'vTFCONTRATOGARANTIA_PERCENTUAL',pic:'ZZ9.99',nv:0.0},{av:'AV71TFContratoGarantia_Percentual_To',fld:'vTFCONTRATOGARANTIA_PERCENTUAL_TO',pic:'ZZ9.99',nv:0.0},{av:'AV74TFContratoGarantia_DataCaucao',fld:'vTFCONTRATOGARANTIA_DATACAUCAO',pic:'',nv:''},{av:'AV75TFContratoGarantia_DataCaucao_To',fld:'vTFCONTRATOGARANTIA_DATACAUCAO_TO',pic:'',nv:''},{av:'AV80TFContratoGarantia_ValorGarantia',fld:'vTFCONTRATOGARANTIA_VALORGARANTIA',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV81TFContratoGarantia_ValorGarantia_To',fld:'vTFCONTRATOGARANTIA_VALORGARANTIA_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV84TFContratoGarantia_DataEncerramento',fld:'vTFCONTRATOGARANTIA_DATAENCERRAMENTO',pic:'',nv:''},{av:'AV85TFContratoGarantia_DataEncerramento_To',fld:'vTFCONTRATOGARANTIA_DATAENCERRAMENTO_TO',pic:'',nv:''},{av:'AV90TFContratoGarantia_ValorEncerramento',fld:'vTFCONTRATOGARANTIA_VALORENCERRAMENTO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV91TFContratoGarantia_ValorEncerramento_To',fld:'vTFCONTRATOGARANTIA_VALORENCERRAMENTO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV54ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_Contratada_PessoaCNPJTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_ContratoGarantia_DataPagtoGarantiaTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_DATAPAGTOGARANTIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_ContratoGarantia_PercentualTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_PERCENTUALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_ContratoGarantia_DataCaucaoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_DATACAUCAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV82ddo_ContratoGarantia_ValorGarantiaTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_VALORGARANTIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV88ddo_ContratoGarantia_DataEncerramentoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_DATAENCERRAMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV92ddo_ContratoGarantia_ValorEncerramentoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_VALORENCERRAMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV157Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A101ContratoGarantia_Codigo',fld:'CONTRATOGARANTIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contratogarantia_percentual_Activeeventkey',ctrl:'DDO_CONTRATOGARANTIA_PERCENTUAL',prop:'ActiveEventKey'},{av:'Ddo_contratogarantia_percentual_Filteredtext_get',ctrl:'DDO_CONTRATOGARANTIA_PERCENTUAL',prop:'FilteredText_get'},{av:'Ddo_contratogarantia_percentual_Filteredtextto_get',ctrl:'DDO_CONTRATOGARANTIA_PERCENTUAL',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratogarantia_percentual_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_PERCENTUAL',prop:'SortedStatus'},{av:'AV70TFContratoGarantia_Percentual',fld:'vTFCONTRATOGARANTIA_PERCENTUAL',pic:'ZZ9.99',nv:0.0},{av:'AV71TFContratoGarantia_Percentual_To',fld:'vTFCONTRATOGARANTIA_PERCENTUAL_TO',pic:'ZZ9.99',nv:0.0},{av:'Ddo_contrato_numero_Sortedstatus',ctrl:'DDO_CONTRATO_NUMERO',prop:'SortedStatus'},{av:'Ddo_contratada_pessoanom_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOANOM',prop:'SortedStatus'},{av:'Ddo_contratada_pessoacnpj_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOACNPJ',prop:'SortedStatus'},{av:'Ddo_contratogarantia_datapagtogarantia_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA',prop:'SortedStatus'},{av:'Ddo_contratogarantia_datacaucao_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_DATACAUCAO',prop:'SortedStatus'},{av:'Ddo_contratogarantia_valorgarantia_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_VALORGARANTIA',prop:'SortedStatus'},{av:'Ddo_contratogarantia_dataencerramento_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_DATAENCERRAMENTO',prop:'SortedStatus'},{av:'Ddo_contratogarantia_valorencerramento_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_VALORENCERRAMENTO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOGARANTIA_DATACAUCAO.ONOPTIONCLICKED","{handler:'E176H2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContratoGarantia_DataPagtoGarantia1',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA1',pic:'',nv:''},{av:'AV17ContratoGarantia_DataPagtoGarantia_To1',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO1',pic:'',nv:''},{av:'AV37ContratoGarantia_DataCaucao1',fld:'vCONTRATOGARANTIA_DATACAUCAO1',pic:'',nv:''},{av:'AV38ContratoGarantia_DataCaucao_To1',fld:'vCONTRATOGARANTIA_DATACAUCAO_TO1',pic:'',nv:''},{av:'AV39ContratoGarantia_DataEncerramento1',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO1',pic:'',nv:''},{av:'AV40ContratoGarantia_DataEncerramento_To1',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20ContratoGarantia_DataPagtoGarantia2',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA2',pic:'',nv:''},{av:'AV21ContratoGarantia_DataPagtoGarantia_To2',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO2',pic:'',nv:''},{av:'AV41ContratoGarantia_DataCaucao2',fld:'vCONTRATOGARANTIA_DATACAUCAO2',pic:'',nv:''},{av:'AV42ContratoGarantia_DataCaucao_To2',fld:'vCONTRATOGARANTIA_DATACAUCAO_TO2',pic:'',nv:''},{av:'AV43ContratoGarantia_DataEncerramento2',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO2',pic:'',nv:''},{av:'AV44ContratoGarantia_DataEncerramento_To2',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24ContratoGarantia_DataPagtoGarantia3',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA3',pic:'',nv:''},{av:'AV25ContratoGarantia_DataPagtoGarantia_To3',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO3',pic:'',nv:''},{av:'AV45ContratoGarantia_DataCaucao3',fld:'vCONTRATOGARANTIA_DATACAUCAO3',pic:'',nv:''},{av:'AV46ContratoGarantia_DataCaucao_To3',fld:'vCONTRATOGARANTIA_DATACAUCAO_TO3',pic:'',nv:''},{av:'AV47ContratoGarantia_DataEncerramento3',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO3',pic:'',nv:''},{av:'AV48ContratoGarantia_DataEncerramento_To3',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV52TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV53TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV56TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV57TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV60TFContratada_PessoaCNPJ',fld:'vTFCONTRATADA_PESSOACNPJ',pic:'',nv:''},{av:'AV61TFContratada_PessoaCNPJ_Sel',fld:'vTFCONTRATADA_PESSOACNPJ_SEL',pic:'',nv:''},{av:'AV64TFContratoGarantia_DataPagtoGarantia',fld:'vTFCONTRATOGARANTIA_DATAPAGTOGARANTIA',pic:'',nv:''},{av:'AV65TFContratoGarantia_DataPagtoGarantia_To',fld:'vTFCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO',pic:'',nv:''},{av:'AV70TFContratoGarantia_Percentual',fld:'vTFCONTRATOGARANTIA_PERCENTUAL',pic:'ZZ9.99',nv:0.0},{av:'AV71TFContratoGarantia_Percentual_To',fld:'vTFCONTRATOGARANTIA_PERCENTUAL_TO',pic:'ZZ9.99',nv:0.0},{av:'AV74TFContratoGarantia_DataCaucao',fld:'vTFCONTRATOGARANTIA_DATACAUCAO',pic:'',nv:''},{av:'AV75TFContratoGarantia_DataCaucao_To',fld:'vTFCONTRATOGARANTIA_DATACAUCAO_TO',pic:'',nv:''},{av:'AV80TFContratoGarantia_ValorGarantia',fld:'vTFCONTRATOGARANTIA_VALORGARANTIA',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV81TFContratoGarantia_ValorGarantia_To',fld:'vTFCONTRATOGARANTIA_VALORGARANTIA_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV84TFContratoGarantia_DataEncerramento',fld:'vTFCONTRATOGARANTIA_DATAENCERRAMENTO',pic:'',nv:''},{av:'AV85TFContratoGarantia_DataEncerramento_To',fld:'vTFCONTRATOGARANTIA_DATAENCERRAMENTO_TO',pic:'',nv:''},{av:'AV90TFContratoGarantia_ValorEncerramento',fld:'vTFCONTRATOGARANTIA_VALORENCERRAMENTO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV91TFContratoGarantia_ValorEncerramento_To',fld:'vTFCONTRATOGARANTIA_VALORENCERRAMENTO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV54ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_Contratada_PessoaCNPJTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_ContratoGarantia_DataPagtoGarantiaTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_DATAPAGTOGARANTIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_ContratoGarantia_PercentualTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_PERCENTUALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_ContratoGarantia_DataCaucaoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_DATACAUCAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV82ddo_ContratoGarantia_ValorGarantiaTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_VALORGARANTIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV88ddo_ContratoGarantia_DataEncerramentoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_DATAENCERRAMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV92ddo_ContratoGarantia_ValorEncerramentoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_VALORENCERRAMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV157Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A101ContratoGarantia_Codigo',fld:'CONTRATOGARANTIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contratogarantia_datacaucao_Activeeventkey',ctrl:'DDO_CONTRATOGARANTIA_DATACAUCAO',prop:'ActiveEventKey'},{av:'Ddo_contratogarantia_datacaucao_Filteredtext_get',ctrl:'DDO_CONTRATOGARANTIA_DATACAUCAO',prop:'FilteredText_get'},{av:'Ddo_contratogarantia_datacaucao_Filteredtextto_get',ctrl:'DDO_CONTRATOGARANTIA_DATACAUCAO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratogarantia_datacaucao_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_DATACAUCAO',prop:'SortedStatus'},{av:'AV74TFContratoGarantia_DataCaucao',fld:'vTFCONTRATOGARANTIA_DATACAUCAO',pic:'',nv:''},{av:'AV75TFContratoGarantia_DataCaucao_To',fld:'vTFCONTRATOGARANTIA_DATACAUCAO_TO',pic:'',nv:''},{av:'Ddo_contrato_numero_Sortedstatus',ctrl:'DDO_CONTRATO_NUMERO',prop:'SortedStatus'},{av:'Ddo_contratada_pessoanom_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOANOM',prop:'SortedStatus'},{av:'Ddo_contratada_pessoacnpj_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOACNPJ',prop:'SortedStatus'},{av:'Ddo_contratogarantia_datapagtogarantia_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA',prop:'SortedStatus'},{av:'Ddo_contratogarantia_percentual_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_PERCENTUAL',prop:'SortedStatus'},{av:'Ddo_contratogarantia_valorgarantia_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_VALORGARANTIA',prop:'SortedStatus'},{av:'Ddo_contratogarantia_dataencerramento_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_DATAENCERRAMENTO',prop:'SortedStatus'},{av:'Ddo_contratogarantia_valorencerramento_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_VALORENCERRAMENTO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOGARANTIA_VALORGARANTIA.ONOPTIONCLICKED","{handler:'E186H2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContratoGarantia_DataPagtoGarantia1',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA1',pic:'',nv:''},{av:'AV17ContratoGarantia_DataPagtoGarantia_To1',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO1',pic:'',nv:''},{av:'AV37ContratoGarantia_DataCaucao1',fld:'vCONTRATOGARANTIA_DATACAUCAO1',pic:'',nv:''},{av:'AV38ContratoGarantia_DataCaucao_To1',fld:'vCONTRATOGARANTIA_DATACAUCAO_TO1',pic:'',nv:''},{av:'AV39ContratoGarantia_DataEncerramento1',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO1',pic:'',nv:''},{av:'AV40ContratoGarantia_DataEncerramento_To1',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20ContratoGarantia_DataPagtoGarantia2',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA2',pic:'',nv:''},{av:'AV21ContratoGarantia_DataPagtoGarantia_To2',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO2',pic:'',nv:''},{av:'AV41ContratoGarantia_DataCaucao2',fld:'vCONTRATOGARANTIA_DATACAUCAO2',pic:'',nv:''},{av:'AV42ContratoGarantia_DataCaucao_To2',fld:'vCONTRATOGARANTIA_DATACAUCAO_TO2',pic:'',nv:''},{av:'AV43ContratoGarantia_DataEncerramento2',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO2',pic:'',nv:''},{av:'AV44ContratoGarantia_DataEncerramento_To2',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24ContratoGarantia_DataPagtoGarantia3',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA3',pic:'',nv:''},{av:'AV25ContratoGarantia_DataPagtoGarantia_To3',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO3',pic:'',nv:''},{av:'AV45ContratoGarantia_DataCaucao3',fld:'vCONTRATOGARANTIA_DATACAUCAO3',pic:'',nv:''},{av:'AV46ContratoGarantia_DataCaucao_To3',fld:'vCONTRATOGARANTIA_DATACAUCAO_TO3',pic:'',nv:''},{av:'AV47ContratoGarantia_DataEncerramento3',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO3',pic:'',nv:''},{av:'AV48ContratoGarantia_DataEncerramento_To3',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV52TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV53TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV56TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV57TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV60TFContratada_PessoaCNPJ',fld:'vTFCONTRATADA_PESSOACNPJ',pic:'',nv:''},{av:'AV61TFContratada_PessoaCNPJ_Sel',fld:'vTFCONTRATADA_PESSOACNPJ_SEL',pic:'',nv:''},{av:'AV64TFContratoGarantia_DataPagtoGarantia',fld:'vTFCONTRATOGARANTIA_DATAPAGTOGARANTIA',pic:'',nv:''},{av:'AV65TFContratoGarantia_DataPagtoGarantia_To',fld:'vTFCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO',pic:'',nv:''},{av:'AV70TFContratoGarantia_Percentual',fld:'vTFCONTRATOGARANTIA_PERCENTUAL',pic:'ZZ9.99',nv:0.0},{av:'AV71TFContratoGarantia_Percentual_To',fld:'vTFCONTRATOGARANTIA_PERCENTUAL_TO',pic:'ZZ9.99',nv:0.0},{av:'AV74TFContratoGarantia_DataCaucao',fld:'vTFCONTRATOGARANTIA_DATACAUCAO',pic:'',nv:''},{av:'AV75TFContratoGarantia_DataCaucao_To',fld:'vTFCONTRATOGARANTIA_DATACAUCAO_TO',pic:'',nv:''},{av:'AV80TFContratoGarantia_ValorGarantia',fld:'vTFCONTRATOGARANTIA_VALORGARANTIA',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV81TFContratoGarantia_ValorGarantia_To',fld:'vTFCONTRATOGARANTIA_VALORGARANTIA_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV84TFContratoGarantia_DataEncerramento',fld:'vTFCONTRATOGARANTIA_DATAENCERRAMENTO',pic:'',nv:''},{av:'AV85TFContratoGarantia_DataEncerramento_To',fld:'vTFCONTRATOGARANTIA_DATAENCERRAMENTO_TO',pic:'',nv:''},{av:'AV90TFContratoGarantia_ValorEncerramento',fld:'vTFCONTRATOGARANTIA_VALORENCERRAMENTO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV91TFContratoGarantia_ValorEncerramento_To',fld:'vTFCONTRATOGARANTIA_VALORENCERRAMENTO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV54ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_Contratada_PessoaCNPJTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_ContratoGarantia_DataPagtoGarantiaTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_DATAPAGTOGARANTIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_ContratoGarantia_PercentualTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_PERCENTUALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_ContratoGarantia_DataCaucaoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_DATACAUCAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV82ddo_ContratoGarantia_ValorGarantiaTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_VALORGARANTIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV88ddo_ContratoGarantia_DataEncerramentoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_DATAENCERRAMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV92ddo_ContratoGarantia_ValorEncerramentoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_VALORENCERRAMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV157Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A101ContratoGarantia_Codigo',fld:'CONTRATOGARANTIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contratogarantia_valorgarantia_Activeeventkey',ctrl:'DDO_CONTRATOGARANTIA_VALORGARANTIA',prop:'ActiveEventKey'},{av:'Ddo_contratogarantia_valorgarantia_Filteredtext_get',ctrl:'DDO_CONTRATOGARANTIA_VALORGARANTIA',prop:'FilteredText_get'},{av:'Ddo_contratogarantia_valorgarantia_Filteredtextto_get',ctrl:'DDO_CONTRATOGARANTIA_VALORGARANTIA',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratogarantia_valorgarantia_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_VALORGARANTIA',prop:'SortedStatus'},{av:'AV80TFContratoGarantia_ValorGarantia',fld:'vTFCONTRATOGARANTIA_VALORGARANTIA',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV81TFContratoGarantia_ValorGarantia_To',fld:'vTFCONTRATOGARANTIA_VALORGARANTIA_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'Ddo_contrato_numero_Sortedstatus',ctrl:'DDO_CONTRATO_NUMERO',prop:'SortedStatus'},{av:'Ddo_contratada_pessoanom_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOANOM',prop:'SortedStatus'},{av:'Ddo_contratada_pessoacnpj_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOACNPJ',prop:'SortedStatus'},{av:'Ddo_contratogarantia_datapagtogarantia_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA',prop:'SortedStatus'},{av:'Ddo_contratogarantia_percentual_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_PERCENTUAL',prop:'SortedStatus'},{av:'Ddo_contratogarantia_datacaucao_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_DATACAUCAO',prop:'SortedStatus'},{av:'Ddo_contratogarantia_dataencerramento_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_DATAENCERRAMENTO',prop:'SortedStatus'},{av:'Ddo_contratogarantia_valorencerramento_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_VALORENCERRAMENTO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOGARANTIA_DATAENCERRAMENTO.ONOPTIONCLICKED","{handler:'E196H2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContratoGarantia_DataPagtoGarantia1',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA1',pic:'',nv:''},{av:'AV17ContratoGarantia_DataPagtoGarantia_To1',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO1',pic:'',nv:''},{av:'AV37ContratoGarantia_DataCaucao1',fld:'vCONTRATOGARANTIA_DATACAUCAO1',pic:'',nv:''},{av:'AV38ContratoGarantia_DataCaucao_To1',fld:'vCONTRATOGARANTIA_DATACAUCAO_TO1',pic:'',nv:''},{av:'AV39ContratoGarantia_DataEncerramento1',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO1',pic:'',nv:''},{av:'AV40ContratoGarantia_DataEncerramento_To1',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20ContratoGarantia_DataPagtoGarantia2',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA2',pic:'',nv:''},{av:'AV21ContratoGarantia_DataPagtoGarantia_To2',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO2',pic:'',nv:''},{av:'AV41ContratoGarantia_DataCaucao2',fld:'vCONTRATOGARANTIA_DATACAUCAO2',pic:'',nv:''},{av:'AV42ContratoGarantia_DataCaucao_To2',fld:'vCONTRATOGARANTIA_DATACAUCAO_TO2',pic:'',nv:''},{av:'AV43ContratoGarantia_DataEncerramento2',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO2',pic:'',nv:''},{av:'AV44ContratoGarantia_DataEncerramento_To2',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24ContratoGarantia_DataPagtoGarantia3',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA3',pic:'',nv:''},{av:'AV25ContratoGarantia_DataPagtoGarantia_To3',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO3',pic:'',nv:''},{av:'AV45ContratoGarantia_DataCaucao3',fld:'vCONTRATOGARANTIA_DATACAUCAO3',pic:'',nv:''},{av:'AV46ContratoGarantia_DataCaucao_To3',fld:'vCONTRATOGARANTIA_DATACAUCAO_TO3',pic:'',nv:''},{av:'AV47ContratoGarantia_DataEncerramento3',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO3',pic:'',nv:''},{av:'AV48ContratoGarantia_DataEncerramento_To3',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV52TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV53TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV56TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV57TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV60TFContratada_PessoaCNPJ',fld:'vTFCONTRATADA_PESSOACNPJ',pic:'',nv:''},{av:'AV61TFContratada_PessoaCNPJ_Sel',fld:'vTFCONTRATADA_PESSOACNPJ_SEL',pic:'',nv:''},{av:'AV64TFContratoGarantia_DataPagtoGarantia',fld:'vTFCONTRATOGARANTIA_DATAPAGTOGARANTIA',pic:'',nv:''},{av:'AV65TFContratoGarantia_DataPagtoGarantia_To',fld:'vTFCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO',pic:'',nv:''},{av:'AV70TFContratoGarantia_Percentual',fld:'vTFCONTRATOGARANTIA_PERCENTUAL',pic:'ZZ9.99',nv:0.0},{av:'AV71TFContratoGarantia_Percentual_To',fld:'vTFCONTRATOGARANTIA_PERCENTUAL_TO',pic:'ZZ9.99',nv:0.0},{av:'AV74TFContratoGarantia_DataCaucao',fld:'vTFCONTRATOGARANTIA_DATACAUCAO',pic:'',nv:''},{av:'AV75TFContratoGarantia_DataCaucao_To',fld:'vTFCONTRATOGARANTIA_DATACAUCAO_TO',pic:'',nv:''},{av:'AV80TFContratoGarantia_ValorGarantia',fld:'vTFCONTRATOGARANTIA_VALORGARANTIA',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV81TFContratoGarantia_ValorGarantia_To',fld:'vTFCONTRATOGARANTIA_VALORGARANTIA_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV84TFContratoGarantia_DataEncerramento',fld:'vTFCONTRATOGARANTIA_DATAENCERRAMENTO',pic:'',nv:''},{av:'AV85TFContratoGarantia_DataEncerramento_To',fld:'vTFCONTRATOGARANTIA_DATAENCERRAMENTO_TO',pic:'',nv:''},{av:'AV90TFContratoGarantia_ValorEncerramento',fld:'vTFCONTRATOGARANTIA_VALORENCERRAMENTO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV91TFContratoGarantia_ValorEncerramento_To',fld:'vTFCONTRATOGARANTIA_VALORENCERRAMENTO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV54ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_Contratada_PessoaCNPJTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_ContratoGarantia_DataPagtoGarantiaTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_DATAPAGTOGARANTIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_ContratoGarantia_PercentualTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_PERCENTUALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_ContratoGarantia_DataCaucaoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_DATACAUCAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV82ddo_ContratoGarantia_ValorGarantiaTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_VALORGARANTIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV88ddo_ContratoGarantia_DataEncerramentoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_DATAENCERRAMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV92ddo_ContratoGarantia_ValorEncerramentoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_VALORENCERRAMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV157Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A101ContratoGarantia_Codigo',fld:'CONTRATOGARANTIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contratogarantia_dataencerramento_Activeeventkey',ctrl:'DDO_CONTRATOGARANTIA_DATAENCERRAMENTO',prop:'ActiveEventKey'},{av:'Ddo_contratogarantia_dataencerramento_Filteredtext_get',ctrl:'DDO_CONTRATOGARANTIA_DATAENCERRAMENTO',prop:'FilteredText_get'},{av:'Ddo_contratogarantia_dataencerramento_Filteredtextto_get',ctrl:'DDO_CONTRATOGARANTIA_DATAENCERRAMENTO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratogarantia_dataencerramento_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_DATAENCERRAMENTO',prop:'SortedStatus'},{av:'AV84TFContratoGarantia_DataEncerramento',fld:'vTFCONTRATOGARANTIA_DATAENCERRAMENTO',pic:'',nv:''},{av:'AV85TFContratoGarantia_DataEncerramento_To',fld:'vTFCONTRATOGARANTIA_DATAENCERRAMENTO_TO',pic:'',nv:''},{av:'Ddo_contrato_numero_Sortedstatus',ctrl:'DDO_CONTRATO_NUMERO',prop:'SortedStatus'},{av:'Ddo_contratada_pessoanom_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOANOM',prop:'SortedStatus'},{av:'Ddo_contratada_pessoacnpj_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOACNPJ',prop:'SortedStatus'},{av:'Ddo_contratogarantia_datapagtogarantia_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA',prop:'SortedStatus'},{av:'Ddo_contratogarantia_percentual_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_PERCENTUAL',prop:'SortedStatus'},{av:'Ddo_contratogarantia_datacaucao_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_DATACAUCAO',prop:'SortedStatus'},{av:'Ddo_contratogarantia_valorgarantia_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_VALORGARANTIA',prop:'SortedStatus'},{av:'Ddo_contratogarantia_valorencerramento_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_VALORENCERRAMENTO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOGARANTIA_VALORENCERRAMENTO.ONOPTIONCLICKED","{handler:'E206H2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContratoGarantia_DataPagtoGarantia1',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA1',pic:'',nv:''},{av:'AV17ContratoGarantia_DataPagtoGarantia_To1',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO1',pic:'',nv:''},{av:'AV37ContratoGarantia_DataCaucao1',fld:'vCONTRATOGARANTIA_DATACAUCAO1',pic:'',nv:''},{av:'AV38ContratoGarantia_DataCaucao_To1',fld:'vCONTRATOGARANTIA_DATACAUCAO_TO1',pic:'',nv:''},{av:'AV39ContratoGarantia_DataEncerramento1',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO1',pic:'',nv:''},{av:'AV40ContratoGarantia_DataEncerramento_To1',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20ContratoGarantia_DataPagtoGarantia2',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA2',pic:'',nv:''},{av:'AV21ContratoGarantia_DataPagtoGarantia_To2',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO2',pic:'',nv:''},{av:'AV41ContratoGarantia_DataCaucao2',fld:'vCONTRATOGARANTIA_DATACAUCAO2',pic:'',nv:''},{av:'AV42ContratoGarantia_DataCaucao_To2',fld:'vCONTRATOGARANTIA_DATACAUCAO_TO2',pic:'',nv:''},{av:'AV43ContratoGarantia_DataEncerramento2',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO2',pic:'',nv:''},{av:'AV44ContratoGarantia_DataEncerramento_To2',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24ContratoGarantia_DataPagtoGarantia3',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA3',pic:'',nv:''},{av:'AV25ContratoGarantia_DataPagtoGarantia_To3',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO3',pic:'',nv:''},{av:'AV45ContratoGarantia_DataCaucao3',fld:'vCONTRATOGARANTIA_DATACAUCAO3',pic:'',nv:''},{av:'AV46ContratoGarantia_DataCaucao_To3',fld:'vCONTRATOGARANTIA_DATACAUCAO_TO3',pic:'',nv:''},{av:'AV47ContratoGarantia_DataEncerramento3',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO3',pic:'',nv:''},{av:'AV48ContratoGarantia_DataEncerramento_To3',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV52TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV53TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV56TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV57TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV60TFContratada_PessoaCNPJ',fld:'vTFCONTRATADA_PESSOACNPJ',pic:'',nv:''},{av:'AV61TFContratada_PessoaCNPJ_Sel',fld:'vTFCONTRATADA_PESSOACNPJ_SEL',pic:'',nv:''},{av:'AV64TFContratoGarantia_DataPagtoGarantia',fld:'vTFCONTRATOGARANTIA_DATAPAGTOGARANTIA',pic:'',nv:''},{av:'AV65TFContratoGarantia_DataPagtoGarantia_To',fld:'vTFCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO',pic:'',nv:''},{av:'AV70TFContratoGarantia_Percentual',fld:'vTFCONTRATOGARANTIA_PERCENTUAL',pic:'ZZ9.99',nv:0.0},{av:'AV71TFContratoGarantia_Percentual_To',fld:'vTFCONTRATOGARANTIA_PERCENTUAL_TO',pic:'ZZ9.99',nv:0.0},{av:'AV74TFContratoGarantia_DataCaucao',fld:'vTFCONTRATOGARANTIA_DATACAUCAO',pic:'',nv:''},{av:'AV75TFContratoGarantia_DataCaucao_To',fld:'vTFCONTRATOGARANTIA_DATACAUCAO_TO',pic:'',nv:''},{av:'AV80TFContratoGarantia_ValorGarantia',fld:'vTFCONTRATOGARANTIA_VALORGARANTIA',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV81TFContratoGarantia_ValorGarantia_To',fld:'vTFCONTRATOGARANTIA_VALORGARANTIA_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV84TFContratoGarantia_DataEncerramento',fld:'vTFCONTRATOGARANTIA_DATAENCERRAMENTO',pic:'',nv:''},{av:'AV85TFContratoGarantia_DataEncerramento_To',fld:'vTFCONTRATOGARANTIA_DATAENCERRAMENTO_TO',pic:'',nv:''},{av:'AV90TFContratoGarantia_ValorEncerramento',fld:'vTFCONTRATOGARANTIA_VALORENCERRAMENTO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV91TFContratoGarantia_ValorEncerramento_To',fld:'vTFCONTRATOGARANTIA_VALORENCERRAMENTO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV54ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_Contratada_PessoaCNPJTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_ContratoGarantia_DataPagtoGarantiaTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_DATAPAGTOGARANTIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_ContratoGarantia_PercentualTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_PERCENTUALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_ContratoGarantia_DataCaucaoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_DATACAUCAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV82ddo_ContratoGarantia_ValorGarantiaTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_VALORGARANTIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV88ddo_ContratoGarantia_DataEncerramentoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_DATAENCERRAMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV92ddo_ContratoGarantia_ValorEncerramentoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_VALORENCERRAMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV157Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A101ContratoGarantia_Codigo',fld:'CONTRATOGARANTIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contratogarantia_valorencerramento_Activeeventkey',ctrl:'DDO_CONTRATOGARANTIA_VALORENCERRAMENTO',prop:'ActiveEventKey'},{av:'Ddo_contratogarantia_valorencerramento_Filteredtext_get',ctrl:'DDO_CONTRATOGARANTIA_VALORENCERRAMENTO',prop:'FilteredText_get'},{av:'Ddo_contratogarantia_valorencerramento_Filteredtextto_get',ctrl:'DDO_CONTRATOGARANTIA_VALORENCERRAMENTO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratogarantia_valorencerramento_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_VALORENCERRAMENTO',prop:'SortedStatus'},{av:'AV90TFContratoGarantia_ValorEncerramento',fld:'vTFCONTRATOGARANTIA_VALORENCERRAMENTO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV91TFContratoGarantia_ValorEncerramento_To',fld:'vTFCONTRATOGARANTIA_VALORENCERRAMENTO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'Ddo_contrato_numero_Sortedstatus',ctrl:'DDO_CONTRATO_NUMERO',prop:'SortedStatus'},{av:'Ddo_contratada_pessoanom_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOANOM',prop:'SortedStatus'},{av:'Ddo_contratada_pessoacnpj_Sortedstatus',ctrl:'DDO_CONTRATADA_PESSOACNPJ',prop:'SortedStatus'},{av:'Ddo_contratogarantia_datapagtogarantia_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA',prop:'SortedStatus'},{av:'Ddo_contratogarantia_percentual_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_PERCENTUAL',prop:'SortedStatus'},{av:'Ddo_contratogarantia_datacaucao_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_DATACAUCAO',prop:'SortedStatus'},{av:'Ddo_contratogarantia_valorgarantia_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_VALORGARANTIA',prop:'SortedStatus'},{av:'Ddo_contratogarantia_dataencerramento_Sortedstatus',ctrl:'DDO_CONTRATOGARANTIA_DATAENCERRAMENTO',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E346H2',iparms:[{av:'A101ContratoGarantia_Codigo',fld:'CONTRATOGARANTIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV28Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'edtavUpdate_Link',ctrl:'vUPDATE',prop:'Link'},{av:'AV29Delete',fld:'vDELETE',pic:'',nv:''},{av:'edtavDelete_Tooltiptext',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'edtavDelete_Link',ctrl:'vDELETE',prop:'Link'},{av:'edtContratada_PessoaNom_Link',ctrl:'CONTRATADA_PESSOANOM',prop:'Link'},{av:'edtContratoGarantia_DataPagtoGarantia_Link',ctrl:'CONTRATOGARANTIA_DATAPAGTOGARANTIA',prop:'Link'}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E216H2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContratoGarantia_DataPagtoGarantia1',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA1',pic:'',nv:''},{av:'AV17ContratoGarantia_DataPagtoGarantia_To1',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO1',pic:'',nv:''},{av:'AV37ContratoGarantia_DataCaucao1',fld:'vCONTRATOGARANTIA_DATACAUCAO1',pic:'',nv:''},{av:'AV38ContratoGarantia_DataCaucao_To1',fld:'vCONTRATOGARANTIA_DATACAUCAO_TO1',pic:'',nv:''},{av:'AV39ContratoGarantia_DataEncerramento1',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO1',pic:'',nv:''},{av:'AV40ContratoGarantia_DataEncerramento_To1',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20ContratoGarantia_DataPagtoGarantia2',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA2',pic:'',nv:''},{av:'AV21ContratoGarantia_DataPagtoGarantia_To2',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO2',pic:'',nv:''},{av:'AV41ContratoGarantia_DataCaucao2',fld:'vCONTRATOGARANTIA_DATACAUCAO2',pic:'',nv:''},{av:'AV42ContratoGarantia_DataCaucao_To2',fld:'vCONTRATOGARANTIA_DATACAUCAO_TO2',pic:'',nv:''},{av:'AV43ContratoGarantia_DataEncerramento2',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO2',pic:'',nv:''},{av:'AV44ContratoGarantia_DataEncerramento_To2',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24ContratoGarantia_DataPagtoGarantia3',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA3',pic:'',nv:''},{av:'AV25ContratoGarantia_DataPagtoGarantia_To3',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO3',pic:'',nv:''},{av:'AV45ContratoGarantia_DataCaucao3',fld:'vCONTRATOGARANTIA_DATACAUCAO3',pic:'',nv:''},{av:'AV46ContratoGarantia_DataCaucao_To3',fld:'vCONTRATOGARANTIA_DATACAUCAO_TO3',pic:'',nv:''},{av:'AV47ContratoGarantia_DataEncerramento3',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO3',pic:'',nv:''},{av:'AV48ContratoGarantia_DataEncerramento_To3',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV52TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV53TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV56TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV57TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV60TFContratada_PessoaCNPJ',fld:'vTFCONTRATADA_PESSOACNPJ',pic:'',nv:''},{av:'AV61TFContratada_PessoaCNPJ_Sel',fld:'vTFCONTRATADA_PESSOACNPJ_SEL',pic:'',nv:''},{av:'AV64TFContratoGarantia_DataPagtoGarantia',fld:'vTFCONTRATOGARANTIA_DATAPAGTOGARANTIA',pic:'',nv:''},{av:'AV65TFContratoGarantia_DataPagtoGarantia_To',fld:'vTFCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO',pic:'',nv:''},{av:'AV70TFContratoGarantia_Percentual',fld:'vTFCONTRATOGARANTIA_PERCENTUAL',pic:'ZZ9.99',nv:0.0},{av:'AV71TFContratoGarantia_Percentual_To',fld:'vTFCONTRATOGARANTIA_PERCENTUAL_TO',pic:'ZZ9.99',nv:0.0},{av:'AV74TFContratoGarantia_DataCaucao',fld:'vTFCONTRATOGARANTIA_DATACAUCAO',pic:'',nv:''},{av:'AV75TFContratoGarantia_DataCaucao_To',fld:'vTFCONTRATOGARANTIA_DATACAUCAO_TO',pic:'',nv:''},{av:'AV80TFContratoGarantia_ValorGarantia',fld:'vTFCONTRATOGARANTIA_VALORGARANTIA',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV81TFContratoGarantia_ValorGarantia_To',fld:'vTFCONTRATOGARANTIA_VALORGARANTIA_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV84TFContratoGarantia_DataEncerramento',fld:'vTFCONTRATOGARANTIA_DATAENCERRAMENTO',pic:'',nv:''},{av:'AV85TFContratoGarantia_DataEncerramento_To',fld:'vTFCONTRATOGARANTIA_DATAENCERRAMENTO_TO',pic:'',nv:''},{av:'AV90TFContratoGarantia_ValorEncerramento',fld:'vTFCONTRATOGARANTIA_VALORENCERRAMENTO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV91TFContratoGarantia_ValorEncerramento_To',fld:'vTFCONTRATOGARANTIA_VALORENCERRAMENTO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV54ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_Contratada_PessoaCNPJTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_ContratoGarantia_DataPagtoGarantiaTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_DATAPAGTOGARANTIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_ContratoGarantia_PercentualTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_PERCENTUALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_ContratoGarantia_DataCaucaoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_DATACAUCAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV82ddo_ContratoGarantia_ValorGarantiaTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_VALORGARANTIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV88ddo_ContratoGarantia_DataEncerramentoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_DATAENCERRAMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV92ddo_ContratoGarantia_ValorEncerramentoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_VALORENCERRAMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV157Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A101ContratoGarantia_Codigo',fld:'CONTRATOGARANTIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E276H2',iparms:[],oparms:[{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E226H2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContratoGarantia_DataPagtoGarantia1',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA1',pic:'',nv:''},{av:'AV17ContratoGarantia_DataPagtoGarantia_To1',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO1',pic:'',nv:''},{av:'AV37ContratoGarantia_DataCaucao1',fld:'vCONTRATOGARANTIA_DATACAUCAO1',pic:'',nv:''},{av:'AV38ContratoGarantia_DataCaucao_To1',fld:'vCONTRATOGARANTIA_DATACAUCAO_TO1',pic:'',nv:''},{av:'AV39ContratoGarantia_DataEncerramento1',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO1',pic:'',nv:''},{av:'AV40ContratoGarantia_DataEncerramento_To1',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20ContratoGarantia_DataPagtoGarantia2',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA2',pic:'',nv:''},{av:'AV21ContratoGarantia_DataPagtoGarantia_To2',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO2',pic:'',nv:''},{av:'AV41ContratoGarantia_DataCaucao2',fld:'vCONTRATOGARANTIA_DATACAUCAO2',pic:'',nv:''},{av:'AV42ContratoGarantia_DataCaucao_To2',fld:'vCONTRATOGARANTIA_DATACAUCAO_TO2',pic:'',nv:''},{av:'AV43ContratoGarantia_DataEncerramento2',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO2',pic:'',nv:''},{av:'AV44ContratoGarantia_DataEncerramento_To2',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24ContratoGarantia_DataPagtoGarantia3',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA3',pic:'',nv:''},{av:'AV25ContratoGarantia_DataPagtoGarantia_To3',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO3',pic:'',nv:''},{av:'AV45ContratoGarantia_DataCaucao3',fld:'vCONTRATOGARANTIA_DATACAUCAO3',pic:'',nv:''},{av:'AV46ContratoGarantia_DataCaucao_To3',fld:'vCONTRATOGARANTIA_DATACAUCAO_TO3',pic:'',nv:''},{av:'AV47ContratoGarantia_DataEncerramento3',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO3',pic:'',nv:''},{av:'AV48ContratoGarantia_DataEncerramento_To3',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV52TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV53TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV56TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV57TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV60TFContratada_PessoaCNPJ',fld:'vTFCONTRATADA_PESSOACNPJ',pic:'',nv:''},{av:'AV61TFContratada_PessoaCNPJ_Sel',fld:'vTFCONTRATADA_PESSOACNPJ_SEL',pic:'',nv:''},{av:'AV64TFContratoGarantia_DataPagtoGarantia',fld:'vTFCONTRATOGARANTIA_DATAPAGTOGARANTIA',pic:'',nv:''},{av:'AV65TFContratoGarantia_DataPagtoGarantia_To',fld:'vTFCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO',pic:'',nv:''},{av:'AV70TFContratoGarantia_Percentual',fld:'vTFCONTRATOGARANTIA_PERCENTUAL',pic:'ZZ9.99',nv:0.0},{av:'AV71TFContratoGarantia_Percentual_To',fld:'vTFCONTRATOGARANTIA_PERCENTUAL_TO',pic:'ZZ9.99',nv:0.0},{av:'AV74TFContratoGarantia_DataCaucao',fld:'vTFCONTRATOGARANTIA_DATACAUCAO',pic:'',nv:''},{av:'AV75TFContratoGarantia_DataCaucao_To',fld:'vTFCONTRATOGARANTIA_DATACAUCAO_TO',pic:'',nv:''},{av:'AV80TFContratoGarantia_ValorGarantia',fld:'vTFCONTRATOGARANTIA_VALORGARANTIA',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV81TFContratoGarantia_ValorGarantia_To',fld:'vTFCONTRATOGARANTIA_VALORGARANTIA_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV84TFContratoGarantia_DataEncerramento',fld:'vTFCONTRATOGARANTIA_DATAENCERRAMENTO',pic:'',nv:''},{av:'AV85TFContratoGarantia_DataEncerramento_To',fld:'vTFCONTRATOGARANTIA_DATAENCERRAMENTO_TO',pic:'',nv:''},{av:'AV90TFContratoGarantia_ValorEncerramento',fld:'vTFCONTRATOGARANTIA_VALORENCERRAMENTO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV91TFContratoGarantia_ValorEncerramento_To',fld:'vTFCONTRATOGARANTIA_VALORENCERRAMENTO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV54ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_Contratada_PessoaCNPJTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_ContratoGarantia_DataPagtoGarantiaTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_DATAPAGTOGARANTIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_ContratoGarantia_PercentualTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_PERCENTUALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_ContratoGarantia_DataCaucaoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_DATACAUCAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV82ddo_ContratoGarantia_ValorGarantiaTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_VALORGARANTIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV88ddo_ContratoGarantia_DataEncerramentoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_DATAENCERRAMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV92ddo_ContratoGarantia_ValorEncerramentoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_VALORENCERRAMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV157Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A101ContratoGarantia_Codigo',fld:'CONTRATOGARANTIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20ContratoGarantia_DataPagtoGarantia2',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA2',pic:'',nv:''},{av:'AV21ContratoGarantia_DataPagtoGarantia_To2',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24ContratoGarantia_DataPagtoGarantia3',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA3',pic:'',nv:''},{av:'AV25ContratoGarantia_DataPagtoGarantia_To3',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContratoGarantia_DataPagtoGarantia1',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA1',pic:'',nv:''},{av:'AV17ContratoGarantia_DataPagtoGarantia_To1',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO1',pic:'',nv:''},{av:'AV37ContratoGarantia_DataCaucao1',fld:'vCONTRATOGARANTIA_DATACAUCAO1',pic:'',nv:''},{av:'AV38ContratoGarantia_DataCaucao_To1',fld:'vCONTRATOGARANTIA_DATACAUCAO_TO1',pic:'',nv:''},{av:'AV39ContratoGarantia_DataEncerramento1',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO1',pic:'',nv:''},{av:'AV40ContratoGarantia_DataEncerramento_To1',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO_TO1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV41ContratoGarantia_DataCaucao2',fld:'vCONTRATOGARANTIA_DATACAUCAO2',pic:'',nv:''},{av:'AV42ContratoGarantia_DataCaucao_To2',fld:'vCONTRATOGARANTIA_DATACAUCAO_TO2',pic:'',nv:''},{av:'AV43ContratoGarantia_DataEncerramento2',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO2',pic:'',nv:''},{av:'AV44ContratoGarantia_DataEncerramento_To2',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO_TO2',pic:'',nv:''},{av:'AV45ContratoGarantia_DataCaucao3',fld:'vCONTRATOGARANTIA_DATACAUCAO3',pic:'',nv:''},{av:'AV46ContratoGarantia_DataCaucao_To3',fld:'vCONTRATOGARANTIA_DATACAUCAO_TO3',pic:'',nv:''},{av:'AV47ContratoGarantia_DataEncerramento3',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO3',pic:'',nv:''},{av:'AV48ContratoGarantia_DataEncerramento_To3',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO_TO3',pic:'',nv:''},{av:'tblTablemergeddynamicfilterscontratogarantia_datapagtogarantia2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOGARANTIA_DATAPAGTOGARANTIA2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratogarantia_datacaucao2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOGARANTIA_DATACAUCAO2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratogarantia_dataencerramento2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOGARANTIA_DATAENCERRAMENTO2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratogarantia_datapagtogarantia3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOGARANTIA_DATAPAGTOGARANTIA3',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratogarantia_datacaucao3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOGARANTIA_DATACAUCAO3',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratogarantia_dataencerramento3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOGARANTIA_DATAENCERRAMENTO3',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratogarantia_datapagtogarantia1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOGARANTIA_DATAPAGTOGARANTIA1',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratogarantia_datacaucao1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOGARANTIA_DATACAUCAO1',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratogarantia_dataencerramento1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOGARANTIA_DATAENCERRAMENTO1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E286H2',iparms:[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'tblTablemergeddynamicfilterscontratogarantia_datapagtogarantia1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOGARANTIA_DATAPAGTOGARANTIA1',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratogarantia_datacaucao1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOGARANTIA_DATACAUCAO1',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratogarantia_dataencerramento1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOGARANTIA_DATAENCERRAMENTO1',prop:'Visible'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E296H2',iparms:[],oparms:[{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E236H2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContratoGarantia_DataPagtoGarantia1',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA1',pic:'',nv:''},{av:'AV17ContratoGarantia_DataPagtoGarantia_To1',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO1',pic:'',nv:''},{av:'AV37ContratoGarantia_DataCaucao1',fld:'vCONTRATOGARANTIA_DATACAUCAO1',pic:'',nv:''},{av:'AV38ContratoGarantia_DataCaucao_To1',fld:'vCONTRATOGARANTIA_DATACAUCAO_TO1',pic:'',nv:''},{av:'AV39ContratoGarantia_DataEncerramento1',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO1',pic:'',nv:''},{av:'AV40ContratoGarantia_DataEncerramento_To1',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20ContratoGarantia_DataPagtoGarantia2',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA2',pic:'',nv:''},{av:'AV21ContratoGarantia_DataPagtoGarantia_To2',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO2',pic:'',nv:''},{av:'AV41ContratoGarantia_DataCaucao2',fld:'vCONTRATOGARANTIA_DATACAUCAO2',pic:'',nv:''},{av:'AV42ContratoGarantia_DataCaucao_To2',fld:'vCONTRATOGARANTIA_DATACAUCAO_TO2',pic:'',nv:''},{av:'AV43ContratoGarantia_DataEncerramento2',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO2',pic:'',nv:''},{av:'AV44ContratoGarantia_DataEncerramento_To2',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24ContratoGarantia_DataPagtoGarantia3',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA3',pic:'',nv:''},{av:'AV25ContratoGarantia_DataPagtoGarantia_To3',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO3',pic:'',nv:''},{av:'AV45ContratoGarantia_DataCaucao3',fld:'vCONTRATOGARANTIA_DATACAUCAO3',pic:'',nv:''},{av:'AV46ContratoGarantia_DataCaucao_To3',fld:'vCONTRATOGARANTIA_DATACAUCAO_TO3',pic:'',nv:''},{av:'AV47ContratoGarantia_DataEncerramento3',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO3',pic:'',nv:''},{av:'AV48ContratoGarantia_DataEncerramento_To3',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV52TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV53TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV56TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV57TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV60TFContratada_PessoaCNPJ',fld:'vTFCONTRATADA_PESSOACNPJ',pic:'',nv:''},{av:'AV61TFContratada_PessoaCNPJ_Sel',fld:'vTFCONTRATADA_PESSOACNPJ_SEL',pic:'',nv:''},{av:'AV64TFContratoGarantia_DataPagtoGarantia',fld:'vTFCONTRATOGARANTIA_DATAPAGTOGARANTIA',pic:'',nv:''},{av:'AV65TFContratoGarantia_DataPagtoGarantia_To',fld:'vTFCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO',pic:'',nv:''},{av:'AV70TFContratoGarantia_Percentual',fld:'vTFCONTRATOGARANTIA_PERCENTUAL',pic:'ZZ9.99',nv:0.0},{av:'AV71TFContratoGarantia_Percentual_To',fld:'vTFCONTRATOGARANTIA_PERCENTUAL_TO',pic:'ZZ9.99',nv:0.0},{av:'AV74TFContratoGarantia_DataCaucao',fld:'vTFCONTRATOGARANTIA_DATACAUCAO',pic:'',nv:''},{av:'AV75TFContratoGarantia_DataCaucao_To',fld:'vTFCONTRATOGARANTIA_DATACAUCAO_TO',pic:'',nv:''},{av:'AV80TFContratoGarantia_ValorGarantia',fld:'vTFCONTRATOGARANTIA_VALORGARANTIA',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV81TFContratoGarantia_ValorGarantia_To',fld:'vTFCONTRATOGARANTIA_VALORGARANTIA_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV84TFContratoGarantia_DataEncerramento',fld:'vTFCONTRATOGARANTIA_DATAENCERRAMENTO',pic:'',nv:''},{av:'AV85TFContratoGarantia_DataEncerramento_To',fld:'vTFCONTRATOGARANTIA_DATAENCERRAMENTO_TO',pic:'',nv:''},{av:'AV90TFContratoGarantia_ValorEncerramento',fld:'vTFCONTRATOGARANTIA_VALORENCERRAMENTO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV91TFContratoGarantia_ValorEncerramento_To',fld:'vTFCONTRATOGARANTIA_VALORENCERRAMENTO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV54ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_Contratada_PessoaCNPJTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_ContratoGarantia_DataPagtoGarantiaTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_DATAPAGTOGARANTIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_ContratoGarantia_PercentualTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_PERCENTUALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_ContratoGarantia_DataCaucaoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_DATACAUCAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV82ddo_ContratoGarantia_ValorGarantiaTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_VALORGARANTIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV88ddo_ContratoGarantia_DataEncerramentoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_DATAENCERRAMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV92ddo_ContratoGarantia_ValorEncerramentoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_VALORENCERRAMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV157Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A101ContratoGarantia_Codigo',fld:'CONTRATOGARANTIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20ContratoGarantia_DataPagtoGarantia2',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA2',pic:'',nv:''},{av:'AV21ContratoGarantia_DataPagtoGarantia_To2',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24ContratoGarantia_DataPagtoGarantia3',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA3',pic:'',nv:''},{av:'AV25ContratoGarantia_DataPagtoGarantia_To3',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContratoGarantia_DataPagtoGarantia1',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA1',pic:'',nv:''},{av:'AV17ContratoGarantia_DataPagtoGarantia_To1',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO1',pic:'',nv:''},{av:'AV37ContratoGarantia_DataCaucao1',fld:'vCONTRATOGARANTIA_DATACAUCAO1',pic:'',nv:''},{av:'AV38ContratoGarantia_DataCaucao_To1',fld:'vCONTRATOGARANTIA_DATACAUCAO_TO1',pic:'',nv:''},{av:'AV39ContratoGarantia_DataEncerramento1',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO1',pic:'',nv:''},{av:'AV40ContratoGarantia_DataEncerramento_To1',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO_TO1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV41ContratoGarantia_DataCaucao2',fld:'vCONTRATOGARANTIA_DATACAUCAO2',pic:'',nv:''},{av:'AV42ContratoGarantia_DataCaucao_To2',fld:'vCONTRATOGARANTIA_DATACAUCAO_TO2',pic:'',nv:''},{av:'AV43ContratoGarantia_DataEncerramento2',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO2',pic:'',nv:''},{av:'AV44ContratoGarantia_DataEncerramento_To2',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO_TO2',pic:'',nv:''},{av:'AV45ContratoGarantia_DataCaucao3',fld:'vCONTRATOGARANTIA_DATACAUCAO3',pic:'',nv:''},{av:'AV46ContratoGarantia_DataCaucao_To3',fld:'vCONTRATOGARANTIA_DATACAUCAO_TO3',pic:'',nv:''},{av:'AV47ContratoGarantia_DataEncerramento3',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO3',pic:'',nv:''},{av:'AV48ContratoGarantia_DataEncerramento_To3',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO_TO3',pic:'',nv:''},{av:'tblTablemergeddynamicfilterscontratogarantia_datapagtogarantia2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOGARANTIA_DATAPAGTOGARANTIA2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratogarantia_datacaucao2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOGARANTIA_DATACAUCAO2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratogarantia_dataencerramento2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOGARANTIA_DATAENCERRAMENTO2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratogarantia_datapagtogarantia3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOGARANTIA_DATAPAGTOGARANTIA3',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratogarantia_datacaucao3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOGARANTIA_DATACAUCAO3',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratogarantia_dataencerramento3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOGARANTIA_DATAENCERRAMENTO3',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratogarantia_datapagtogarantia1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOGARANTIA_DATAPAGTOGARANTIA1',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratogarantia_datacaucao1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOGARANTIA_DATACAUCAO1',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratogarantia_dataencerramento1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOGARANTIA_DATAENCERRAMENTO1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E306H2',iparms:[{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'tblTablemergeddynamicfilterscontratogarantia_datapagtogarantia2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOGARANTIA_DATAPAGTOGARANTIA2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratogarantia_datacaucao2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOGARANTIA_DATACAUCAO2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratogarantia_dataencerramento2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOGARANTIA_DATAENCERRAMENTO2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E246H2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContratoGarantia_DataPagtoGarantia1',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA1',pic:'',nv:''},{av:'AV17ContratoGarantia_DataPagtoGarantia_To1',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO1',pic:'',nv:''},{av:'AV37ContratoGarantia_DataCaucao1',fld:'vCONTRATOGARANTIA_DATACAUCAO1',pic:'',nv:''},{av:'AV38ContratoGarantia_DataCaucao_To1',fld:'vCONTRATOGARANTIA_DATACAUCAO_TO1',pic:'',nv:''},{av:'AV39ContratoGarantia_DataEncerramento1',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO1',pic:'',nv:''},{av:'AV40ContratoGarantia_DataEncerramento_To1',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20ContratoGarantia_DataPagtoGarantia2',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA2',pic:'',nv:''},{av:'AV21ContratoGarantia_DataPagtoGarantia_To2',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO2',pic:'',nv:''},{av:'AV41ContratoGarantia_DataCaucao2',fld:'vCONTRATOGARANTIA_DATACAUCAO2',pic:'',nv:''},{av:'AV42ContratoGarantia_DataCaucao_To2',fld:'vCONTRATOGARANTIA_DATACAUCAO_TO2',pic:'',nv:''},{av:'AV43ContratoGarantia_DataEncerramento2',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO2',pic:'',nv:''},{av:'AV44ContratoGarantia_DataEncerramento_To2',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24ContratoGarantia_DataPagtoGarantia3',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA3',pic:'',nv:''},{av:'AV25ContratoGarantia_DataPagtoGarantia_To3',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO3',pic:'',nv:''},{av:'AV45ContratoGarantia_DataCaucao3',fld:'vCONTRATOGARANTIA_DATACAUCAO3',pic:'',nv:''},{av:'AV46ContratoGarantia_DataCaucao_To3',fld:'vCONTRATOGARANTIA_DATACAUCAO_TO3',pic:'',nv:''},{av:'AV47ContratoGarantia_DataEncerramento3',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO3',pic:'',nv:''},{av:'AV48ContratoGarantia_DataEncerramento_To3',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV52TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV53TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV56TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV57TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV60TFContratada_PessoaCNPJ',fld:'vTFCONTRATADA_PESSOACNPJ',pic:'',nv:''},{av:'AV61TFContratada_PessoaCNPJ_Sel',fld:'vTFCONTRATADA_PESSOACNPJ_SEL',pic:'',nv:''},{av:'AV64TFContratoGarantia_DataPagtoGarantia',fld:'vTFCONTRATOGARANTIA_DATAPAGTOGARANTIA',pic:'',nv:''},{av:'AV65TFContratoGarantia_DataPagtoGarantia_To',fld:'vTFCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO',pic:'',nv:''},{av:'AV70TFContratoGarantia_Percentual',fld:'vTFCONTRATOGARANTIA_PERCENTUAL',pic:'ZZ9.99',nv:0.0},{av:'AV71TFContratoGarantia_Percentual_To',fld:'vTFCONTRATOGARANTIA_PERCENTUAL_TO',pic:'ZZ9.99',nv:0.0},{av:'AV74TFContratoGarantia_DataCaucao',fld:'vTFCONTRATOGARANTIA_DATACAUCAO',pic:'',nv:''},{av:'AV75TFContratoGarantia_DataCaucao_To',fld:'vTFCONTRATOGARANTIA_DATACAUCAO_TO',pic:'',nv:''},{av:'AV80TFContratoGarantia_ValorGarantia',fld:'vTFCONTRATOGARANTIA_VALORGARANTIA',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV81TFContratoGarantia_ValorGarantia_To',fld:'vTFCONTRATOGARANTIA_VALORGARANTIA_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV84TFContratoGarantia_DataEncerramento',fld:'vTFCONTRATOGARANTIA_DATAENCERRAMENTO',pic:'',nv:''},{av:'AV85TFContratoGarantia_DataEncerramento_To',fld:'vTFCONTRATOGARANTIA_DATAENCERRAMENTO_TO',pic:'',nv:''},{av:'AV90TFContratoGarantia_ValorEncerramento',fld:'vTFCONTRATOGARANTIA_VALORENCERRAMENTO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV91TFContratoGarantia_ValorEncerramento_To',fld:'vTFCONTRATOGARANTIA_VALORENCERRAMENTO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV54ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_Contratada_PessoaCNPJTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_ContratoGarantia_DataPagtoGarantiaTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_DATAPAGTOGARANTIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_ContratoGarantia_PercentualTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_PERCENTUALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_ContratoGarantia_DataCaucaoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_DATACAUCAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV82ddo_ContratoGarantia_ValorGarantiaTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_VALORGARANTIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV88ddo_ContratoGarantia_DataEncerramentoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_DATAENCERRAMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV92ddo_ContratoGarantia_ValorEncerramentoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_VALORENCERRAMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV157Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A101ContratoGarantia_Codigo',fld:'CONTRATOGARANTIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20ContratoGarantia_DataPagtoGarantia2',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA2',pic:'',nv:''},{av:'AV21ContratoGarantia_DataPagtoGarantia_To2',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24ContratoGarantia_DataPagtoGarantia3',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA3',pic:'',nv:''},{av:'AV25ContratoGarantia_DataPagtoGarantia_To3',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContratoGarantia_DataPagtoGarantia1',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA1',pic:'',nv:''},{av:'AV17ContratoGarantia_DataPagtoGarantia_To1',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO1',pic:'',nv:''},{av:'AV37ContratoGarantia_DataCaucao1',fld:'vCONTRATOGARANTIA_DATACAUCAO1',pic:'',nv:''},{av:'AV38ContratoGarantia_DataCaucao_To1',fld:'vCONTRATOGARANTIA_DATACAUCAO_TO1',pic:'',nv:''},{av:'AV39ContratoGarantia_DataEncerramento1',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO1',pic:'',nv:''},{av:'AV40ContratoGarantia_DataEncerramento_To1',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO_TO1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV41ContratoGarantia_DataCaucao2',fld:'vCONTRATOGARANTIA_DATACAUCAO2',pic:'',nv:''},{av:'AV42ContratoGarantia_DataCaucao_To2',fld:'vCONTRATOGARANTIA_DATACAUCAO_TO2',pic:'',nv:''},{av:'AV43ContratoGarantia_DataEncerramento2',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO2',pic:'',nv:''},{av:'AV44ContratoGarantia_DataEncerramento_To2',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO_TO2',pic:'',nv:''},{av:'AV45ContratoGarantia_DataCaucao3',fld:'vCONTRATOGARANTIA_DATACAUCAO3',pic:'',nv:''},{av:'AV46ContratoGarantia_DataCaucao_To3',fld:'vCONTRATOGARANTIA_DATACAUCAO_TO3',pic:'',nv:''},{av:'AV47ContratoGarantia_DataEncerramento3',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO3',pic:'',nv:''},{av:'AV48ContratoGarantia_DataEncerramento_To3',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO_TO3',pic:'',nv:''},{av:'tblTablemergeddynamicfilterscontratogarantia_datapagtogarantia2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOGARANTIA_DATAPAGTOGARANTIA2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratogarantia_datacaucao2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOGARANTIA_DATACAUCAO2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratogarantia_dataencerramento2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOGARANTIA_DATAENCERRAMENTO2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratogarantia_datapagtogarantia3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOGARANTIA_DATAPAGTOGARANTIA3',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratogarantia_datacaucao3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOGARANTIA_DATACAUCAO3',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratogarantia_dataencerramento3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOGARANTIA_DATAENCERRAMENTO3',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratogarantia_datapagtogarantia1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOGARANTIA_DATAPAGTOGARANTIA1',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratogarantia_datacaucao1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOGARANTIA_DATACAUCAO1',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratogarantia_dataencerramento1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOGARANTIA_DATAENCERRAMENTO1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E316H2',iparms:[{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'tblTablemergeddynamicfilterscontratogarantia_datapagtogarantia3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOGARANTIA_DATAPAGTOGARANTIA3',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratogarantia_datacaucao3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOGARANTIA_DATACAUCAO3',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratogarantia_dataencerramento3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOGARANTIA_DATAENCERRAMENTO3',prop:'Visible'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E256H2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContratoGarantia_DataPagtoGarantia1',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA1',pic:'',nv:''},{av:'AV17ContratoGarantia_DataPagtoGarantia_To1',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO1',pic:'',nv:''},{av:'AV37ContratoGarantia_DataCaucao1',fld:'vCONTRATOGARANTIA_DATACAUCAO1',pic:'',nv:''},{av:'AV38ContratoGarantia_DataCaucao_To1',fld:'vCONTRATOGARANTIA_DATACAUCAO_TO1',pic:'',nv:''},{av:'AV39ContratoGarantia_DataEncerramento1',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO1',pic:'',nv:''},{av:'AV40ContratoGarantia_DataEncerramento_To1',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO_TO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20ContratoGarantia_DataPagtoGarantia2',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA2',pic:'',nv:''},{av:'AV21ContratoGarantia_DataPagtoGarantia_To2',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO2',pic:'',nv:''},{av:'AV41ContratoGarantia_DataCaucao2',fld:'vCONTRATOGARANTIA_DATACAUCAO2',pic:'',nv:''},{av:'AV42ContratoGarantia_DataCaucao_To2',fld:'vCONTRATOGARANTIA_DATACAUCAO_TO2',pic:'',nv:''},{av:'AV43ContratoGarantia_DataEncerramento2',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO2',pic:'',nv:''},{av:'AV44ContratoGarantia_DataEncerramento_To2',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO_TO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24ContratoGarantia_DataPagtoGarantia3',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA3',pic:'',nv:''},{av:'AV25ContratoGarantia_DataPagtoGarantia_To3',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO3',pic:'',nv:''},{av:'AV45ContratoGarantia_DataCaucao3',fld:'vCONTRATOGARANTIA_DATACAUCAO3',pic:'',nv:''},{av:'AV46ContratoGarantia_DataCaucao_To3',fld:'vCONTRATOGARANTIA_DATACAUCAO_TO3',pic:'',nv:''},{av:'AV47ContratoGarantia_DataEncerramento3',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO3',pic:'',nv:''},{av:'AV48ContratoGarantia_DataEncerramento_To3',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO_TO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV52TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'AV53TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'AV56TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'AV57TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV60TFContratada_PessoaCNPJ',fld:'vTFCONTRATADA_PESSOACNPJ',pic:'',nv:''},{av:'AV61TFContratada_PessoaCNPJ_Sel',fld:'vTFCONTRATADA_PESSOACNPJ_SEL',pic:'',nv:''},{av:'AV64TFContratoGarantia_DataPagtoGarantia',fld:'vTFCONTRATOGARANTIA_DATAPAGTOGARANTIA',pic:'',nv:''},{av:'AV65TFContratoGarantia_DataPagtoGarantia_To',fld:'vTFCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO',pic:'',nv:''},{av:'AV70TFContratoGarantia_Percentual',fld:'vTFCONTRATOGARANTIA_PERCENTUAL',pic:'ZZ9.99',nv:0.0},{av:'AV71TFContratoGarantia_Percentual_To',fld:'vTFCONTRATOGARANTIA_PERCENTUAL_TO',pic:'ZZ9.99',nv:0.0},{av:'AV74TFContratoGarantia_DataCaucao',fld:'vTFCONTRATOGARANTIA_DATACAUCAO',pic:'',nv:''},{av:'AV75TFContratoGarantia_DataCaucao_To',fld:'vTFCONTRATOGARANTIA_DATACAUCAO_TO',pic:'',nv:''},{av:'AV80TFContratoGarantia_ValorGarantia',fld:'vTFCONTRATOGARANTIA_VALORGARANTIA',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV81TFContratoGarantia_ValorGarantia_To',fld:'vTFCONTRATOGARANTIA_VALORGARANTIA_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV84TFContratoGarantia_DataEncerramento',fld:'vTFCONTRATOGARANTIA_DATAENCERRAMENTO',pic:'',nv:''},{av:'AV85TFContratoGarantia_DataEncerramento_To',fld:'vTFCONTRATOGARANTIA_DATAENCERRAMENTO_TO',pic:'',nv:''},{av:'AV90TFContratoGarantia_ValorEncerramento',fld:'vTFCONTRATOGARANTIA_VALORENCERRAMENTO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV91TFContratoGarantia_ValorEncerramento_To',fld:'vTFCONTRATOGARANTIA_VALORENCERRAMENTO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV54ddo_Contrato_NumeroTitleControlIdToReplace',fld:'vDDO_CONTRATO_NUMEROTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV58ddo_Contratada_PessoaNomTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_Contratada_PessoaCNPJTitleControlIdToReplace',fld:'vDDO_CONTRATADA_PESSOACNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_ContratoGarantia_DataPagtoGarantiaTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_DATAPAGTOGARANTIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_ContratoGarantia_PercentualTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_PERCENTUALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_ContratoGarantia_DataCaucaoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_DATACAUCAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV82ddo_ContratoGarantia_ValorGarantiaTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_VALORGARANTIATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV88ddo_ContratoGarantia_DataEncerramentoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_DATAENCERRAMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV92ddo_ContratoGarantia_ValorEncerramentoTitleControlIdToReplace',fld:'vDDO_CONTRATOGARANTIA_VALORENCERRAMENTOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV157Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A101ContratoGarantia_Codigo',fld:'CONTRATOGARANTIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV52TFContrato_Numero',fld:'vTFCONTRATO_NUMERO',pic:'',nv:''},{av:'Ddo_contrato_numero_Filteredtext_set',ctrl:'DDO_CONTRATO_NUMERO',prop:'FilteredText_set'},{av:'AV53TFContrato_Numero_Sel',fld:'vTFCONTRATO_NUMERO_SEL',pic:'',nv:''},{av:'Ddo_contrato_numero_Selectedvalue_set',ctrl:'DDO_CONTRATO_NUMERO',prop:'SelectedValue_set'},{av:'AV56TFContratada_PessoaNom',fld:'vTFCONTRATADA_PESSOANOM',pic:'@!',nv:''},{av:'Ddo_contratada_pessoanom_Filteredtext_set',ctrl:'DDO_CONTRATADA_PESSOANOM',prop:'FilteredText_set'},{av:'AV57TFContratada_PessoaNom_Sel',fld:'vTFCONTRATADA_PESSOANOM_SEL',pic:'@!',nv:''},{av:'Ddo_contratada_pessoanom_Selectedvalue_set',ctrl:'DDO_CONTRATADA_PESSOANOM',prop:'SelectedValue_set'},{av:'AV60TFContratada_PessoaCNPJ',fld:'vTFCONTRATADA_PESSOACNPJ',pic:'',nv:''},{av:'Ddo_contratada_pessoacnpj_Filteredtext_set',ctrl:'DDO_CONTRATADA_PESSOACNPJ',prop:'FilteredText_set'},{av:'AV61TFContratada_PessoaCNPJ_Sel',fld:'vTFCONTRATADA_PESSOACNPJ_SEL',pic:'',nv:''},{av:'Ddo_contratada_pessoacnpj_Selectedvalue_set',ctrl:'DDO_CONTRATADA_PESSOACNPJ',prop:'SelectedValue_set'},{av:'AV64TFContratoGarantia_DataPagtoGarantia',fld:'vTFCONTRATOGARANTIA_DATAPAGTOGARANTIA',pic:'',nv:''},{av:'Ddo_contratogarantia_datapagtogarantia_Filteredtext_set',ctrl:'DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA',prop:'FilteredText_set'},{av:'AV65TFContratoGarantia_DataPagtoGarantia_To',fld:'vTFCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO',pic:'',nv:''},{av:'Ddo_contratogarantia_datapagtogarantia_Filteredtextto_set',ctrl:'DDO_CONTRATOGARANTIA_DATAPAGTOGARANTIA',prop:'FilteredTextTo_set'},{av:'AV70TFContratoGarantia_Percentual',fld:'vTFCONTRATOGARANTIA_PERCENTUAL',pic:'ZZ9.99',nv:0.0},{av:'Ddo_contratogarantia_percentual_Filteredtext_set',ctrl:'DDO_CONTRATOGARANTIA_PERCENTUAL',prop:'FilteredText_set'},{av:'AV71TFContratoGarantia_Percentual_To',fld:'vTFCONTRATOGARANTIA_PERCENTUAL_TO',pic:'ZZ9.99',nv:0.0},{av:'Ddo_contratogarantia_percentual_Filteredtextto_set',ctrl:'DDO_CONTRATOGARANTIA_PERCENTUAL',prop:'FilteredTextTo_set'},{av:'AV74TFContratoGarantia_DataCaucao',fld:'vTFCONTRATOGARANTIA_DATACAUCAO',pic:'',nv:''},{av:'Ddo_contratogarantia_datacaucao_Filteredtext_set',ctrl:'DDO_CONTRATOGARANTIA_DATACAUCAO',prop:'FilteredText_set'},{av:'AV75TFContratoGarantia_DataCaucao_To',fld:'vTFCONTRATOGARANTIA_DATACAUCAO_TO',pic:'',nv:''},{av:'Ddo_contratogarantia_datacaucao_Filteredtextto_set',ctrl:'DDO_CONTRATOGARANTIA_DATACAUCAO',prop:'FilteredTextTo_set'},{av:'AV80TFContratoGarantia_ValorGarantia',fld:'vTFCONTRATOGARANTIA_VALORGARANTIA',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'Ddo_contratogarantia_valorgarantia_Filteredtext_set',ctrl:'DDO_CONTRATOGARANTIA_VALORGARANTIA',prop:'FilteredText_set'},{av:'AV81TFContratoGarantia_ValorGarantia_To',fld:'vTFCONTRATOGARANTIA_VALORGARANTIA_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'Ddo_contratogarantia_valorgarantia_Filteredtextto_set',ctrl:'DDO_CONTRATOGARANTIA_VALORGARANTIA',prop:'FilteredTextTo_set'},{av:'AV84TFContratoGarantia_DataEncerramento',fld:'vTFCONTRATOGARANTIA_DATAENCERRAMENTO',pic:'',nv:''},{av:'Ddo_contratogarantia_dataencerramento_Filteredtext_set',ctrl:'DDO_CONTRATOGARANTIA_DATAENCERRAMENTO',prop:'FilteredText_set'},{av:'AV85TFContratoGarantia_DataEncerramento_To',fld:'vTFCONTRATOGARANTIA_DATAENCERRAMENTO_TO',pic:'',nv:''},{av:'Ddo_contratogarantia_dataencerramento_Filteredtextto_set',ctrl:'DDO_CONTRATOGARANTIA_DATAENCERRAMENTO',prop:'FilteredTextTo_set'},{av:'AV90TFContratoGarantia_ValorEncerramento',fld:'vTFCONTRATOGARANTIA_VALORENCERRAMENTO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'Ddo_contratogarantia_valorencerramento_Filteredtext_set',ctrl:'DDO_CONTRATOGARANTIA_VALORENCERRAMENTO',prop:'FilteredText_set'},{av:'AV91TFContratoGarantia_ValorEncerramento_To',fld:'vTFCONTRATOGARANTIA_VALORENCERRAMENTO_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'Ddo_contratogarantia_valorencerramento_Filteredtextto_set',ctrl:'DDO_CONTRATOGARANTIA_VALORENCERRAMENTO',prop:'FilteredTextTo_set'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16ContratoGarantia_DataPagtoGarantia1',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA1',pic:'',nv:''},{av:'AV17ContratoGarantia_DataPagtoGarantia_To1',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO1',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'tblTablemergeddynamicfilterscontratogarantia_datapagtogarantia1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOGARANTIA_DATAPAGTOGARANTIA1',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratogarantia_datacaucao1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOGARANTIA_DATACAUCAO1',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratogarantia_dataencerramento1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOGARANTIA_DATAENCERRAMENTO1',prop:'Visible'},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20ContratoGarantia_DataPagtoGarantia2',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA2',pic:'',nv:''},{av:'AV21ContratoGarantia_DataPagtoGarantia_To2',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24ContratoGarantia_DataPagtoGarantia3',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA3',pic:'',nv:''},{av:'AV25ContratoGarantia_DataPagtoGarantia_To3',fld:'vCONTRATOGARANTIA_DATAPAGTOGARANTIA_TO3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV37ContratoGarantia_DataCaucao1',fld:'vCONTRATOGARANTIA_DATACAUCAO1',pic:'',nv:''},{av:'AV38ContratoGarantia_DataCaucao_To1',fld:'vCONTRATOGARANTIA_DATACAUCAO_TO1',pic:'',nv:''},{av:'AV39ContratoGarantia_DataEncerramento1',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO1',pic:'',nv:''},{av:'AV40ContratoGarantia_DataEncerramento_To1',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO_TO1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV41ContratoGarantia_DataCaucao2',fld:'vCONTRATOGARANTIA_DATACAUCAO2',pic:'',nv:''},{av:'AV42ContratoGarantia_DataCaucao_To2',fld:'vCONTRATOGARANTIA_DATACAUCAO_TO2',pic:'',nv:''},{av:'AV43ContratoGarantia_DataEncerramento2',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO2',pic:'',nv:''},{av:'AV44ContratoGarantia_DataEncerramento_To2',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO_TO2',pic:'',nv:''},{av:'AV45ContratoGarantia_DataCaucao3',fld:'vCONTRATOGARANTIA_DATACAUCAO3',pic:'',nv:''},{av:'AV46ContratoGarantia_DataCaucao_To3',fld:'vCONTRATOGARANTIA_DATACAUCAO_TO3',pic:'',nv:''},{av:'AV47ContratoGarantia_DataEncerramento3',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO3',pic:'',nv:''},{av:'AV48ContratoGarantia_DataEncerramento_To3',fld:'vCONTRATOGARANTIA_DATAENCERRAMENTO_TO3',pic:'',nv:''},{av:'tblTablemergeddynamicfilterscontratogarantia_datapagtogarantia2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOGARANTIA_DATAPAGTOGARANTIA2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratogarantia_datacaucao2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOGARANTIA_DATACAUCAO2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratogarantia_dataencerramento2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOGARANTIA_DATAENCERRAMENTO2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratogarantia_datapagtogarantia3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOGARANTIA_DATAPAGTOGARANTIA3',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratogarantia_datacaucao3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOGARANTIA_DATACAUCAO3',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontratogarantia_dataencerramento3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATOGARANTIA_DATAENCERRAMENTO3',prop:'Visible'}]}");
         setEventMetadata("'DOINSERT'","{handler:'E266H2',iparms:[{av:'A101ContratoGarantia_Codigo',fld:'CONTRATOGARANTIA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_contrato_numero_Activeeventkey = "";
         Ddo_contrato_numero_Filteredtext_get = "";
         Ddo_contrato_numero_Selectedvalue_get = "";
         Ddo_contratada_pessoanom_Activeeventkey = "";
         Ddo_contratada_pessoanom_Filteredtext_get = "";
         Ddo_contratada_pessoanom_Selectedvalue_get = "";
         Ddo_contratada_pessoacnpj_Activeeventkey = "";
         Ddo_contratada_pessoacnpj_Filteredtext_get = "";
         Ddo_contratada_pessoacnpj_Selectedvalue_get = "";
         Ddo_contratogarantia_datapagtogarantia_Activeeventkey = "";
         Ddo_contratogarantia_datapagtogarantia_Filteredtext_get = "";
         Ddo_contratogarantia_datapagtogarantia_Filteredtextto_get = "";
         Ddo_contratogarantia_percentual_Activeeventkey = "";
         Ddo_contratogarantia_percentual_Filteredtext_get = "";
         Ddo_contratogarantia_percentual_Filteredtextto_get = "";
         Ddo_contratogarantia_datacaucao_Activeeventkey = "";
         Ddo_contratogarantia_datacaucao_Filteredtext_get = "";
         Ddo_contratogarantia_datacaucao_Filteredtextto_get = "";
         Ddo_contratogarantia_valorgarantia_Activeeventkey = "";
         Ddo_contratogarantia_valorgarantia_Filteredtext_get = "";
         Ddo_contratogarantia_valorgarantia_Filteredtextto_get = "";
         Ddo_contratogarantia_dataencerramento_Activeeventkey = "";
         Ddo_contratogarantia_dataencerramento_Filteredtext_get = "";
         Ddo_contratogarantia_dataencerramento_Filteredtextto_get = "";
         Ddo_contratogarantia_valorencerramento_Activeeventkey = "";
         Ddo_contratogarantia_valorencerramento_Filteredtext_get = "";
         Ddo_contratogarantia_valorencerramento_Filteredtextto_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV15DynamicFiltersSelector1 = "";
         AV16ContratoGarantia_DataPagtoGarantia1 = DateTime.MinValue;
         AV17ContratoGarantia_DataPagtoGarantia_To1 = DateTime.MinValue;
         AV37ContratoGarantia_DataCaucao1 = DateTime.MinValue;
         AV38ContratoGarantia_DataCaucao_To1 = DateTime.MinValue;
         AV39ContratoGarantia_DataEncerramento1 = DateTime.MinValue;
         AV40ContratoGarantia_DataEncerramento_To1 = DateTime.MinValue;
         AV19DynamicFiltersSelector2 = "";
         AV20ContratoGarantia_DataPagtoGarantia2 = DateTime.MinValue;
         AV21ContratoGarantia_DataPagtoGarantia_To2 = DateTime.MinValue;
         AV41ContratoGarantia_DataCaucao2 = DateTime.MinValue;
         AV42ContratoGarantia_DataCaucao_To2 = DateTime.MinValue;
         AV43ContratoGarantia_DataEncerramento2 = DateTime.MinValue;
         AV44ContratoGarantia_DataEncerramento_To2 = DateTime.MinValue;
         AV23DynamicFiltersSelector3 = "";
         AV24ContratoGarantia_DataPagtoGarantia3 = DateTime.MinValue;
         AV25ContratoGarantia_DataPagtoGarantia_To3 = DateTime.MinValue;
         AV45ContratoGarantia_DataCaucao3 = DateTime.MinValue;
         AV46ContratoGarantia_DataCaucao_To3 = DateTime.MinValue;
         AV47ContratoGarantia_DataEncerramento3 = DateTime.MinValue;
         AV48ContratoGarantia_DataEncerramento_To3 = DateTime.MinValue;
         AV52TFContrato_Numero = "";
         AV53TFContrato_Numero_Sel = "";
         AV56TFContratada_PessoaNom = "";
         AV57TFContratada_PessoaNom_Sel = "";
         AV60TFContratada_PessoaCNPJ = "";
         AV61TFContratada_PessoaCNPJ_Sel = "";
         AV64TFContratoGarantia_DataPagtoGarantia = DateTime.MinValue;
         AV65TFContratoGarantia_DataPagtoGarantia_To = DateTime.MinValue;
         AV74TFContratoGarantia_DataCaucao = DateTime.MinValue;
         AV75TFContratoGarantia_DataCaucao_To = DateTime.MinValue;
         AV84TFContratoGarantia_DataEncerramento = DateTime.MinValue;
         AV85TFContratoGarantia_DataEncerramento_To = DateTime.MinValue;
         AV54ddo_Contrato_NumeroTitleControlIdToReplace = "";
         AV58ddo_Contratada_PessoaNomTitleControlIdToReplace = "";
         AV62ddo_Contratada_PessoaCNPJTitleControlIdToReplace = "";
         AV68ddo_ContratoGarantia_DataPagtoGarantiaTitleControlIdToReplace = "";
         AV72ddo_ContratoGarantia_PercentualTitleControlIdToReplace = "";
         AV78ddo_ContratoGarantia_DataCaucaoTitleControlIdToReplace = "";
         AV82ddo_ContratoGarantia_ValorGarantiaTitleControlIdToReplace = "";
         AV88ddo_ContratoGarantia_DataEncerramentoTitleControlIdToReplace = "";
         AV92ddo_ContratoGarantia_ValorEncerramentoTitleControlIdToReplace = "";
         AV157Pgmname = "";
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV93DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV51Contrato_NumeroTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV55Contratada_PessoaNomTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV59Contratada_PessoaCNPJTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV63ContratoGarantia_DataPagtoGarantiaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV69ContratoGarantia_PercentualTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV73ContratoGarantia_DataCaucaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV79ContratoGarantia_ValorGarantiaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV83ContratoGarantia_DataEncerramentoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV89ContratoGarantia_ValorEncerramentoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_contrato_numero_Filteredtext_set = "";
         Ddo_contrato_numero_Selectedvalue_set = "";
         Ddo_contrato_numero_Sortedstatus = "";
         Ddo_contratada_pessoanom_Filteredtext_set = "";
         Ddo_contratada_pessoanom_Selectedvalue_set = "";
         Ddo_contratada_pessoanom_Sortedstatus = "";
         Ddo_contratada_pessoacnpj_Filteredtext_set = "";
         Ddo_contratada_pessoacnpj_Selectedvalue_set = "";
         Ddo_contratada_pessoacnpj_Sortedstatus = "";
         Ddo_contratogarantia_datapagtogarantia_Filteredtext_set = "";
         Ddo_contratogarantia_datapagtogarantia_Filteredtextto_set = "";
         Ddo_contratogarantia_datapagtogarantia_Sortedstatus = "";
         Ddo_contratogarantia_percentual_Filteredtext_set = "";
         Ddo_contratogarantia_percentual_Filteredtextto_set = "";
         Ddo_contratogarantia_percentual_Sortedstatus = "";
         Ddo_contratogarantia_datacaucao_Filteredtext_set = "";
         Ddo_contratogarantia_datacaucao_Filteredtextto_set = "";
         Ddo_contratogarantia_datacaucao_Sortedstatus = "";
         Ddo_contratogarantia_valorgarantia_Filteredtext_set = "";
         Ddo_contratogarantia_valorgarantia_Filteredtextto_set = "";
         Ddo_contratogarantia_valorgarantia_Sortedstatus = "";
         Ddo_contratogarantia_dataencerramento_Filteredtext_set = "";
         Ddo_contratogarantia_dataencerramento_Filteredtextto_set = "";
         Ddo_contratogarantia_dataencerramento_Sortedstatus = "";
         Ddo_contratogarantia_valorencerramento_Filteredtext_set = "";
         Ddo_contratogarantia_valorencerramento_Filteredtextto_set = "";
         Ddo_contratogarantia_valorencerramento_Sortedstatus = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         AV66DDO_ContratoGarantia_DataPagtoGarantiaAuxDate = DateTime.MinValue;
         AV67DDO_ContratoGarantia_DataPagtoGarantiaAuxDateTo = DateTime.MinValue;
         AV76DDO_ContratoGarantia_DataCaucaoAuxDate = DateTime.MinValue;
         AV77DDO_ContratoGarantia_DataCaucaoAuxDateTo = DateTime.MinValue;
         AV86DDO_ContratoGarantia_DataEncerramentoAuxDate = DateTime.MinValue;
         AV87DDO_ContratoGarantia_DataEncerramentoAuxDateTo = DateTime.MinValue;
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV28Update = "";
         AV155Update_GXI = "";
         AV29Delete = "";
         AV156Delete_GXI = "";
         A77Contrato_Numero = "";
         A41Contratada_PessoaNom = "";
         A42Contratada_PessoaCNPJ = "";
         A102ContratoGarantia_DataPagtoGarantia = DateTime.MinValue;
         A104ContratoGarantia_DataCaucao = DateTime.MinValue;
         A106ContratoGarantia_DataEncerramento = DateTime.MinValue;
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV137WWContratoGarantiaDS_24_Tfcontrato_numero = "";
         lV139WWContratoGarantiaDS_26_Tfcontratada_pessoanom = "";
         lV141WWContratoGarantiaDS_28_Tfcontratada_pessoacnpj = "";
         AV114WWContratoGarantiaDS_1_Dynamicfiltersselector1 = "";
         AV115WWContratoGarantiaDS_2_Contratogarantia_datapagtogarantia1 = DateTime.MinValue;
         AV116WWContratoGarantiaDS_3_Contratogarantia_datapagtogarantia_to1 = DateTime.MinValue;
         AV117WWContratoGarantiaDS_4_Contratogarantia_datacaucao1 = DateTime.MinValue;
         AV118WWContratoGarantiaDS_5_Contratogarantia_datacaucao_to1 = DateTime.MinValue;
         AV119WWContratoGarantiaDS_6_Contratogarantia_dataencerramento1 = DateTime.MinValue;
         AV120WWContratoGarantiaDS_7_Contratogarantia_dataencerramento_to1 = DateTime.MinValue;
         AV122WWContratoGarantiaDS_9_Dynamicfiltersselector2 = "";
         AV123WWContratoGarantiaDS_10_Contratogarantia_datapagtogarantia2 = DateTime.MinValue;
         AV124WWContratoGarantiaDS_11_Contratogarantia_datapagtogarantia_to2 = DateTime.MinValue;
         AV125WWContratoGarantiaDS_12_Contratogarantia_datacaucao2 = DateTime.MinValue;
         AV126WWContratoGarantiaDS_13_Contratogarantia_datacaucao_to2 = DateTime.MinValue;
         AV127WWContratoGarantiaDS_14_Contratogarantia_dataencerramento2 = DateTime.MinValue;
         AV128WWContratoGarantiaDS_15_Contratogarantia_dataencerramento_to2 = DateTime.MinValue;
         AV130WWContratoGarantiaDS_17_Dynamicfiltersselector3 = "";
         AV131WWContratoGarantiaDS_18_Contratogarantia_datapagtogarantia3 = DateTime.MinValue;
         AV132WWContratoGarantiaDS_19_Contratogarantia_datapagtogarantia_to3 = DateTime.MinValue;
         AV133WWContratoGarantiaDS_20_Contratogarantia_datacaucao3 = DateTime.MinValue;
         AV134WWContratoGarantiaDS_21_Contratogarantia_datacaucao_to3 = DateTime.MinValue;
         AV135WWContratoGarantiaDS_22_Contratogarantia_dataencerramento3 = DateTime.MinValue;
         AV136WWContratoGarantiaDS_23_Contratogarantia_dataencerramento_to3 = DateTime.MinValue;
         AV138WWContratoGarantiaDS_25_Tfcontrato_numero_sel = "";
         AV137WWContratoGarantiaDS_24_Tfcontrato_numero = "";
         AV140WWContratoGarantiaDS_27_Tfcontratada_pessoanom_sel = "";
         AV139WWContratoGarantiaDS_26_Tfcontratada_pessoanom = "";
         AV142WWContratoGarantiaDS_29_Tfcontratada_pessoacnpj_sel = "";
         AV141WWContratoGarantiaDS_28_Tfcontratada_pessoacnpj = "";
         AV143WWContratoGarantiaDS_30_Tfcontratogarantia_datapagtogarantia = DateTime.MinValue;
         AV144WWContratoGarantiaDS_31_Tfcontratogarantia_datapagtogarantia_to = DateTime.MinValue;
         AV147WWContratoGarantiaDS_34_Tfcontratogarantia_datacaucao = DateTime.MinValue;
         AV148WWContratoGarantiaDS_35_Tfcontratogarantia_datacaucao_to = DateTime.MinValue;
         AV151WWContratoGarantiaDS_38_Tfcontratogarantia_dataencerramento = DateTime.MinValue;
         AV152WWContratoGarantiaDS_39_Tfcontratogarantia_dataencerramento_to = DateTime.MinValue;
         H006H2_A40Contratada_PessoaCod = new int[1] ;
         H006H2_A74Contrato_Codigo = new int[1] ;
         H006H2_A101ContratoGarantia_Codigo = new int[1] ;
         H006H2_A39Contratada_Codigo = new int[1] ;
         H006H2_A107ContratoGarantia_ValorEncerramento = new decimal[1] ;
         H006H2_A106ContratoGarantia_DataEncerramento = new DateTime[] {DateTime.MinValue} ;
         H006H2_A105ContratoGarantia_ValorGarantia = new decimal[1] ;
         H006H2_A104ContratoGarantia_DataCaucao = new DateTime[] {DateTime.MinValue} ;
         H006H2_A103ContratoGarantia_Percentual = new decimal[1] ;
         H006H2_A102ContratoGarantia_DataPagtoGarantia = new DateTime[] {DateTime.MinValue} ;
         H006H2_A42Contratada_PessoaCNPJ = new String[] {""} ;
         H006H2_n42Contratada_PessoaCNPJ = new bool[] {false} ;
         H006H2_A41Contratada_PessoaNom = new String[] {""} ;
         H006H2_n41Contratada_PessoaNom = new bool[] {false} ;
         H006H2_A77Contrato_Numero = new String[] {""} ;
         H006H3_AGRID_nRecordCount = new long[1] ;
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GridRow = new GXWebRow();
         AV30Session = context.GetSession();
         AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV7HTTPRequest = new GxHttpRequest( context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblContratogarantiatitle_Jsonclick = "";
         lblOrderedtext_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         lblDynamicfilterscontratogarantia_dataencerramento_rangemiddletext3_Jsonclick = "";
         lblDynamicfilterscontratogarantia_datacaucao_rangemiddletext3_Jsonclick = "";
         lblDynamicfilterscontratogarantia_datapagtogarantia_rangemiddletext3_Jsonclick = "";
         lblDynamicfilterscontratogarantia_dataencerramento_rangemiddletext2_Jsonclick = "";
         lblDynamicfilterscontratogarantia_datacaucao_rangemiddletext2_Jsonclick = "";
         lblDynamicfilterscontratogarantia_datapagtogarantia_rangemiddletext2_Jsonclick = "";
         lblDynamicfilterscontratogarantia_dataencerramento_rangemiddletext1_Jsonclick = "";
         lblDynamicfilterscontratogarantia_datacaucao_rangemiddletext1_Jsonclick = "";
         lblDynamicfilterscontratogarantia_datapagtogarantia_rangemiddletext1_Jsonclick = "";
         imgInsert_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wwcontratogarantia__default(),
            new Object[][] {
                new Object[] {
               H006H2_A40Contratada_PessoaCod, H006H2_A74Contrato_Codigo, H006H2_A101ContratoGarantia_Codigo, H006H2_A39Contratada_Codigo, H006H2_A107ContratoGarantia_ValorEncerramento, H006H2_A106ContratoGarantia_DataEncerramento, H006H2_A105ContratoGarantia_ValorGarantia, H006H2_A104ContratoGarantia_DataCaucao, H006H2_A103ContratoGarantia_Percentual, H006H2_A102ContratoGarantia_DataPagtoGarantia,
               H006H2_A42Contratada_PessoaCNPJ, H006H2_n42Contratada_PessoaCNPJ, H006H2_A41Contratada_PessoaNom, H006H2_n41Contratada_PessoaNom, H006H2_A77Contrato_Numero
               }
               , new Object[] {
               H006H3_AGRID_nRecordCount
               }
            }
         );
         AV157Pgmname = "WWContratoGarantia";
         /* GeneXus formulas. */
         AV157Pgmname = "WWContratoGarantia";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_142 ;
      private short nGXsfl_142_idx=1 ;
      private short AV13OrderedBy ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_142_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtContrato_Numero_Titleformat ;
      private short edtContratada_PessoaNom_Titleformat ;
      private short edtContratada_PessoaCNPJ_Titleformat ;
      private short edtContratoGarantia_DataPagtoGarantia_Titleformat ;
      private short edtContratoGarantia_Percentual_Titleformat ;
      private short edtContratoGarantia_DataCaucao_Titleformat ;
      private short edtContratoGarantia_ValorGarantia_Titleformat ;
      private short edtContratoGarantia_DataEncerramento_Titleformat ;
      private short edtContratoGarantia_ValorEncerramento_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int subGrid_Rows ;
      private int A101ContratoGarantia_Codigo ;
      private int A74Contrato_Codigo ;
      private int A39Contratada_Codigo ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_contrato_numero_Datalistupdateminimumcharacters ;
      private int Ddo_contratada_pessoanom_Datalistupdateminimumcharacters ;
      private int Ddo_contratada_pessoacnpj_Datalistupdateminimumcharacters ;
      private int edtavTfcontrato_numero_Visible ;
      private int edtavTfcontrato_numero_sel_Visible ;
      private int edtavTfcontratada_pessoanom_Visible ;
      private int edtavTfcontratada_pessoanom_sel_Visible ;
      private int edtavTfcontratada_pessoacnpj_Visible ;
      private int edtavTfcontratada_pessoacnpj_sel_Visible ;
      private int edtavTfcontratogarantia_datapagtogarantia_Visible ;
      private int edtavTfcontratogarantia_datapagtogarantia_to_Visible ;
      private int edtavTfcontratogarantia_percentual_Visible ;
      private int edtavTfcontratogarantia_percentual_to_Visible ;
      private int edtavTfcontratogarantia_datacaucao_Visible ;
      private int edtavTfcontratogarantia_datacaucao_to_Visible ;
      private int edtavTfcontratogarantia_valorgarantia_Visible ;
      private int edtavTfcontratogarantia_valorgarantia_to_Visible ;
      private int edtavTfcontratogarantia_dataencerramento_Visible ;
      private int edtavTfcontratogarantia_dataencerramento_to_Visible ;
      private int edtavTfcontratogarantia_valorencerramento_Visible ;
      private int edtavTfcontratogarantia_valorencerramento_to_Visible ;
      private int edtavDdo_contrato_numerotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratada_pessoanomtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratada_pessoacnpjtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratogarantia_datapagtogarantiatitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratogarantia_percentualtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratogarantia_datacaucaotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratogarantia_valorgarantiatitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratogarantia_dataencerramentotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratogarantia_valorencerramentotitlecontrolidtoreplace_Visible ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int A40Contratada_PessoaCod ;
      private int edtavOrdereddsc_Visible ;
      private int AV94PageToGo ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int tblTablemergeddynamicfilterscontratogarantia_datapagtogarantia1_Visible ;
      private int tblTablemergeddynamicfilterscontratogarantia_datacaucao1_Visible ;
      private int tblTablemergeddynamicfilterscontratogarantia_dataencerramento1_Visible ;
      private int tblTablemergeddynamicfilterscontratogarantia_datapagtogarantia2_Visible ;
      private int tblTablemergeddynamicfilterscontratogarantia_datacaucao2_Visible ;
      private int tblTablemergeddynamicfilterscontratogarantia_dataencerramento2_Visible ;
      private int tblTablemergeddynamicfilterscontratogarantia_datapagtogarantia3_Visible ;
      private int tblTablemergeddynamicfilterscontratogarantia_datacaucao3_Visible ;
      private int tblTablemergeddynamicfilterscontratogarantia_dataencerramento3_Visible ;
      private int AV158GXV1 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long AV95GridCurrentPage ;
      private long AV96GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private decimal AV70TFContratoGarantia_Percentual ;
      private decimal AV71TFContratoGarantia_Percentual_To ;
      private decimal AV80TFContratoGarantia_ValorGarantia ;
      private decimal AV81TFContratoGarantia_ValorGarantia_To ;
      private decimal AV90TFContratoGarantia_ValorEncerramento ;
      private decimal AV91TFContratoGarantia_ValorEncerramento_To ;
      private decimal A103ContratoGarantia_Percentual ;
      private decimal A105ContratoGarantia_ValorGarantia ;
      private decimal A107ContratoGarantia_ValorEncerramento ;
      private decimal AV145WWContratoGarantiaDS_32_Tfcontratogarantia_percentual ;
      private decimal AV146WWContratoGarantiaDS_33_Tfcontratogarantia_percentual_to ;
      private decimal AV149WWContratoGarantiaDS_36_Tfcontratogarantia_valorgarantia ;
      private decimal AV150WWContratoGarantiaDS_37_Tfcontratogarantia_valorgarantia_to ;
      private decimal AV153WWContratoGarantiaDS_40_Tfcontratogarantia_valorencerramento ;
      private decimal AV154WWContratoGarantiaDS_41_Tfcontratogarantia_valorencerramento_to ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_contrato_numero_Activeeventkey ;
      private String Ddo_contrato_numero_Filteredtext_get ;
      private String Ddo_contrato_numero_Selectedvalue_get ;
      private String Ddo_contratada_pessoanom_Activeeventkey ;
      private String Ddo_contratada_pessoanom_Filteredtext_get ;
      private String Ddo_contratada_pessoanom_Selectedvalue_get ;
      private String Ddo_contratada_pessoacnpj_Activeeventkey ;
      private String Ddo_contratada_pessoacnpj_Filteredtext_get ;
      private String Ddo_contratada_pessoacnpj_Selectedvalue_get ;
      private String Ddo_contratogarantia_datapagtogarantia_Activeeventkey ;
      private String Ddo_contratogarantia_datapagtogarantia_Filteredtext_get ;
      private String Ddo_contratogarantia_datapagtogarantia_Filteredtextto_get ;
      private String Ddo_contratogarantia_percentual_Activeeventkey ;
      private String Ddo_contratogarantia_percentual_Filteredtext_get ;
      private String Ddo_contratogarantia_percentual_Filteredtextto_get ;
      private String Ddo_contratogarantia_datacaucao_Activeeventkey ;
      private String Ddo_contratogarantia_datacaucao_Filteredtext_get ;
      private String Ddo_contratogarantia_datacaucao_Filteredtextto_get ;
      private String Ddo_contratogarantia_valorgarantia_Activeeventkey ;
      private String Ddo_contratogarantia_valorgarantia_Filteredtext_get ;
      private String Ddo_contratogarantia_valorgarantia_Filteredtextto_get ;
      private String Ddo_contratogarantia_dataencerramento_Activeeventkey ;
      private String Ddo_contratogarantia_dataencerramento_Filteredtext_get ;
      private String Ddo_contratogarantia_dataencerramento_Filteredtextto_get ;
      private String Ddo_contratogarantia_valorencerramento_Activeeventkey ;
      private String Ddo_contratogarantia_valorencerramento_Filteredtext_get ;
      private String Ddo_contratogarantia_valorencerramento_Filteredtextto_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_142_idx="0001" ;
      private String AV52TFContrato_Numero ;
      private String AV53TFContrato_Numero_Sel ;
      private String AV56TFContratada_PessoaNom ;
      private String AV57TFContratada_PessoaNom_Sel ;
      private String AV157Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_contrato_numero_Caption ;
      private String Ddo_contrato_numero_Tooltip ;
      private String Ddo_contrato_numero_Cls ;
      private String Ddo_contrato_numero_Filteredtext_set ;
      private String Ddo_contrato_numero_Selectedvalue_set ;
      private String Ddo_contrato_numero_Dropdownoptionstype ;
      private String Ddo_contrato_numero_Titlecontrolidtoreplace ;
      private String Ddo_contrato_numero_Sortedstatus ;
      private String Ddo_contrato_numero_Filtertype ;
      private String Ddo_contrato_numero_Datalisttype ;
      private String Ddo_contrato_numero_Datalistproc ;
      private String Ddo_contrato_numero_Sortasc ;
      private String Ddo_contrato_numero_Sortdsc ;
      private String Ddo_contrato_numero_Loadingdata ;
      private String Ddo_contrato_numero_Cleanfilter ;
      private String Ddo_contrato_numero_Noresultsfound ;
      private String Ddo_contrato_numero_Searchbuttontext ;
      private String Ddo_contratada_pessoanom_Caption ;
      private String Ddo_contratada_pessoanom_Tooltip ;
      private String Ddo_contratada_pessoanom_Cls ;
      private String Ddo_contratada_pessoanom_Filteredtext_set ;
      private String Ddo_contratada_pessoanom_Selectedvalue_set ;
      private String Ddo_contratada_pessoanom_Dropdownoptionstype ;
      private String Ddo_contratada_pessoanom_Titlecontrolidtoreplace ;
      private String Ddo_contratada_pessoanom_Sortedstatus ;
      private String Ddo_contratada_pessoanom_Filtertype ;
      private String Ddo_contratada_pessoanom_Datalisttype ;
      private String Ddo_contratada_pessoanom_Datalistproc ;
      private String Ddo_contratada_pessoanom_Sortasc ;
      private String Ddo_contratada_pessoanom_Sortdsc ;
      private String Ddo_contratada_pessoanom_Loadingdata ;
      private String Ddo_contratada_pessoanom_Cleanfilter ;
      private String Ddo_contratada_pessoanom_Noresultsfound ;
      private String Ddo_contratada_pessoanom_Searchbuttontext ;
      private String Ddo_contratada_pessoacnpj_Caption ;
      private String Ddo_contratada_pessoacnpj_Tooltip ;
      private String Ddo_contratada_pessoacnpj_Cls ;
      private String Ddo_contratada_pessoacnpj_Filteredtext_set ;
      private String Ddo_contratada_pessoacnpj_Selectedvalue_set ;
      private String Ddo_contratada_pessoacnpj_Dropdownoptionstype ;
      private String Ddo_contratada_pessoacnpj_Titlecontrolidtoreplace ;
      private String Ddo_contratada_pessoacnpj_Sortedstatus ;
      private String Ddo_contratada_pessoacnpj_Filtertype ;
      private String Ddo_contratada_pessoacnpj_Datalisttype ;
      private String Ddo_contratada_pessoacnpj_Datalistproc ;
      private String Ddo_contratada_pessoacnpj_Sortasc ;
      private String Ddo_contratada_pessoacnpj_Sortdsc ;
      private String Ddo_contratada_pessoacnpj_Loadingdata ;
      private String Ddo_contratada_pessoacnpj_Cleanfilter ;
      private String Ddo_contratada_pessoacnpj_Noresultsfound ;
      private String Ddo_contratada_pessoacnpj_Searchbuttontext ;
      private String Ddo_contratogarantia_datapagtogarantia_Caption ;
      private String Ddo_contratogarantia_datapagtogarantia_Tooltip ;
      private String Ddo_contratogarantia_datapagtogarantia_Cls ;
      private String Ddo_contratogarantia_datapagtogarantia_Filteredtext_set ;
      private String Ddo_contratogarantia_datapagtogarantia_Filteredtextto_set ;
      private String Ddo_contratogarantia_datapagtogarantia_Dropdownoptionstype ;
      private String Ddo_contratogarantia_datapagtogarantia_Titlecontrolidtoreplace ;
      private String Ddo_contratogarantia_datapagtogarantia_Sortedstatus ;
      private String Ddo_contratogarantia_datapagtogarantia_Filtertype ;
      private String Ddo_contratogarantia_datapagtogarantia_Sortasc ;
      private String Ddo_contratogarantia_datapagtogarantia_Sortdsc ;
      private String Ddo_contratogarantia_datapagtogarantia_Cleanfilter ;
      private String Ddo_contratogarantia_datapagtogarantia_Rangefilterfrom ;
      private String Ddo_contratogarantia_datapagtogarantia_Rangefilterto ;
      private String Ddo_contratogarantia_datapagtogarantia_Searchbuttontext ;
      private String Ddo_contratogarantia_percentual_Caption ;
      private String Ddo_contratogarantia_percentual_Tooltip ;
      private String Ddo_contratogarantia_percentual_Cls ;
      private String Ddo_contratogarantia_percentual_Filteredtext_set ;
      private String Ddo_contratogarantia_percentual_Filteredtextto_set ;
      private String Ddo_contratogarantia_percentual_Dropdownoptionstype ;
      private String Ddo_contratogarantia_percentual_Titlecontrolidtoreplace ;
      private String Ddo_contratogarantia_percentual_Sortedstatus ;
      private String Ddo_contratogarantia_percentual_Filtertype ;
      private String Ddo_contratogarantia_percentual_Sortasc ;
      private String Ddo_contratogarantia_percentual_Sortdsc ;
      private String Ddo_contratogarantia_percentual_Cleanfilter ;
      private String Ddo_contratogarantia_percentual_Rangefilterfrom ;
      private String Ddo_contratogarantia_percentual_Rangefilterto ;
      private String Ddo_contratogarantia_percentual_Searchbuttontext ;
      private String Ddo_contratogarantia_datacaucao_Caption ;
      private String Ddo_contratogarantia_datacaucao_Tooltip ;
      private String Ddo_contratogarantia_datacaucao_Cls ;
      private String Ddo_contratogarantia_datacaucao_Filteredtext_set ;
      private String Ddo_contratogarantia_datacaucao_Filteredtextto_set ;
      private String Ddo_contratogarantia_datacaucao_Dropdownoptionstype ;
      private String Ddo_contratogarantia_datacaucao_Titlecontrolidtoreplace ;
      private String Ddo_contratogarantia_datacaucao_Sortedstatus ;
      private String Ddo_contratogarantia_datacaucao_Filtertype ;
      private String Ddo_contratogarantia_datacaucao_Sortasc ;
      private String Ddo_contratogarantia_datacaucao_Sortdsc ;
      private String Ddo_contratogarantia_datacaucao_Cleanfilter ;
      private String Ddo_contratogarantia_datacaucao_Rangefilterfrom ;
      private String Ddo_contratogarantia_datacaucao_Rangefilterto ;
      private String Ddo_contratogarantia_datacaucao_Searchbuttontext ;
      private String Ddo_contratogarantia_valorgarantia_Caption ;
      private String Ddo_contratogarantia_valorgarantia_Tooltip ;
      private String Ddo_contratogarantia_valorgarantia_Cls ;
      private String Ddo_contratogarantia_valorgarantia_Filteredtext_set ;
      private String Ddo_contratogarantia_valorgarantia_Filteredtextto_set ;
      private String Ddo_contratogarantia_valorgarantia_Dropdownoptionstype ;
      private String Ddo_contratogarantia_valorgarantia_Titlecontrolidtoreplace ;
      private String Ddo_contratogarantia_valorgarantia_Sortedstatus ;
      private String Ddo_contratogarantia_valorgarantia_Filtertype ;
      private String Ddo_contratogarantia_valorgarantia_Sortasc ;
      private String Ddo_contratogarantia_valorgarantia_Sortdsc ;
      private String Ddo_contratogarantia_valorgarantia_Cleanfilter ;
      private String Ddo_contratogarantia_valorgarantia_Rangefilterfrom ;
      private String Ddo_contratogarantia_valorgarantia_Rangefilterto ;
      private String Ddo_contratogarantia_valorgarantia_Searchbuttontext ;
      private String Ddo_contratogarantia_dataencerramento_Caption ;
      private String Ddo_contratogarantia_dataencerramento_Tooltip ;
      private String Ddo_contratogarantia_dataencerramento_Cls ;
      private String Ddo_contratogarantia_dataencerramento_Filteredtext_set ;
      private String Ddo_contratogarantia_dataencerramento_Filteredtextto_set ;
      private String Ddo_contratogarantia_dataencerramento_Dropdownoptionstype ;
      private String Ddo_contratogarantia_dataencerramento_Titlecontrolidtoreplace ;
      private String Ddo_contratogarantia_dataencerramento_Sortedstatus ;
      private String Ddo_contratogarantia_dataencerramento_Filtertype ;
      private String Ddo_contratogarantia_dataencerramento_Sortasc ;
      private String Ddo_contratogarantia_dataencerramento_Sortdsc ;
      private String Ddo_contratogarantia_dataencerramento_Cleanfilter ;
      private String Ddo_contratogarantia_dataencerramento_Rangefilterfrom ;
      private String Ddo_contratogarantia_dataencerramento_Rangefilterto ;
      private String Ddo_contratogarantia_dataencerramento_Searchbuttontext ;
      private String Ddo_contratogarantia_valorencerramento_Caption ;
      private String Ddo_contratogarantia_valorencerramento_Tooltip ;
      private String Ddo_contratogarantia_valorencerramento_Cls ;
      private String Ddo_contratogarantia_valorencerramento_Filteredtext_set ;
      private String Ddo_contratogarantia_valorencerramento_Filteredtextto_set ;
      private String Ddo_contratogarantia_valorencerramento_Dropdownoptionstype ;
      private String Ddo_contratogarantia_valorencerramento_Titlecontrolidtoreplace ;
      private String Ddo_contratogarantia_valorencerramento_Sortedstatus ;
      private String Ddo_contratogarantia_valorencerramento_Filtertype ;
      private String Ddo_contratogarantia_valorencerramento_Sortasc ;
      private String Ddo_contratogarantia_valorencerramento_Sortdsc ;
      private String Ddo_contratogarantia_valorencerramento_Cleanfilter ;
      private String Ddo_contratogarantia_valorencerramento_Rangefilterfrom ;
      private String Ddo_contratogarantia_valorencerramento_Rangefilterto ;
      private String Ddo_contratogarantia_valorencerramento_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String edtavTfcontrato_numero_Internalname ;
      private String edtavTfcontrato_numero_Jsonclick ;
      private String edtavTfcontrato_numero_sel_Internalname ;
      private String edtavTfcontrato_numero_sel_Jsonclick ;
      private String edtavTfcontratada_pessoanom_Internalname ;
      private String edtavTfcontratada_pessoanom_Jsonclick ;
      private String edtavTfcontratada_pessoanom_sel_Internalname ;
      private String edtavTfcontratada_pessoanom_sel_Jsonclick ;
      private String edtavTfcontratada_pessoacnpj_Internalname ;
      private String edtavTfcontratada_pessoacnpj_Jsonclick ;
      private String edtavTfcontratada_pessoacnpj_sel_Internalname ;
      private String edtavTfcontratada_pessoacnpj_sel_Jsonclick ;
      private String edtavTfcontratogarantia_datapagtogarantia_Internalname ;
      private String edtavTfcontratogarantia_datapagtogarantia_Jsonclick ;
      private String edtavTfcontratogarantia_datapagtogarantia_to_Internalname ;
      private String edtavTfcontratogarantia_datapagtogarantia_to_Jsonclick ;
      private String divDdo_contratogarantia_datapagtogarantiaauxdates_Internalname ;
      private String edtavDdo_contratogarantia_datapagtogarantiaauxdate_Internalname ;
      private String edtavDdo_contratogarantia_datapagtogarantiaauxdate_Jsonclick ;
      private String edtavDdo_contratogarantia_datapagtogarantiaauxdateto_Internalname ;
      private String edtavDdo_contratogarantia_datapagtogarantiaauxdateto_Jsonclick ;
      private String edtavTfcontratogarantia_percentual_Internalname ;
      private String edtavTfcontratogarantia_percentual_Jsonclick ;
      private String edtavTfcontratogarantia_percentual_to_Internalname ;
      private String edtavTfcontratogarantia_percentual_to_Jsonclick ;
      private String edtavTfcontratogarantia_datacaucao_Internalname ;
      private String edtavTfcontratogarantia_datacaucao_Jsonclick ;
      private String edtavTfcontratogarantia_datacaucao_to_Internalname ;
      private String edtavTfcontratogarantia_datacaucao_to_Jsonclick ;
      private String divDdo_contratogarantia_datacaucaoauxdates_Internalname ;
      private String edtavDdo_contratogarantia_datacaucaoauxdate_Internalname ;
      private String edtavDdo_contratogarantia_datacaucaoauxdate_Jsonclick ;
      private String edtavDdo_contratogarantia_datacaucaoauxdateto_Internalname ;
      private String edtavDdo_contratogarantia_datacaucaoauxdateto_Jsonclick ;
      private String edtavTfcontratogarantia_valorgarantia_Internalname ;
      private String edtavTfcontratogarantia_valorgarantia_Jsonclick ;
      private String edtavTfcontratogarantia_valorgarantia_to_Internalname ;
      private String edtavTfcontratogarantia_valorgarantia_to_Jsonclick ;
      private String edtavTfcontratogarantia_dataencerramento_Internalname ;
      private String edtavTfcontratogarantia_dataencerramento_Jsonclick ;
      private String edtavTfcontratogarantia_dataencerramento_to_Internalname ;
      private String edtavTfcontratogarantia_dataencerramento_to_Jsonclick ;
      private String divDdo_contratogarantia_dataencerramentoauxdates_Internalname ;
      private String edtavDdo_contratogarantia_dataencerramentoauxdate_Internalname ;
      private String edtavDdo_contratogarantia_dataencerramentoauxdate_Jsonclick ;
      private String edtavDdo_contratogarantia_dataencerramentoauxdateto_Internalname ;
      private String edtavDdo_contratogarantia_dataencerramentoauxdateto_Jsonclick ;
      private String edtavTfcontratogarantia_valorencerramento_Internalname ;
      private String edtavTfcontratogarantia_valorencerramento_Jsonclick ;
      private String edtavTfcontratogarantia_valorencerramento_to_Internalname ;
      private String edtavTfcontratogarantia_valorencerramento_to_Jsonclick ;
      private String edtavDdo_contrato_numerotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratada_pessoanomtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratada_pessoacnpjtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratogarantia_datapagtogarantiatitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratogarantia_percentualtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratogarantia_datacaucaotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratogarantia_valorgarantiatitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratogarantia_dataencerramentotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratogarantia_valorencerramentotitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavUpdate_Internalname ;
      private String edtavDelete_Internalname ;
      private String A77Contrato_Numero ;
      private String edtContrato_Numero_Internalname ;
      private String A41Contratada_PessoaNom ;
      private String edtContratada_PessoaNom_Internalname ;
      private String edtContratada_PessoaCNPJ_Internalname ;
      private String edtContratoGarantia_DataPagtoGarantia_Internalname ;
      private String edtContratoGarantia_Percentual_Internalname ;
      private String edtContratoGarantia_DataCaucao_Internalname ;
      private String edtContratoGarantia_ValorGarantia_Internalname ;
      private String edtContratoGarantia_DataEncerramento_Internalname ;
      private String edtContratoGarantia_ValorEncerramento_Internalname ;
      private String cmbavOrderedby_Internalname ;
      private String scmdbuf ;
      private String lV137WWContratoGarantiaDS_24_Tfcontrato_numero ;
      private String lV139WWContratoGarantiaDS_26_Tfcontratada_pessoanom ;
      private String AV138WWContratoGarantiaDS_25_Tfcontrato_numero_sel ;
      private String AV137WWContratoGarantiaDS_24_Tfcontrato_numero ;
      private String AV140WWContratoGarantiaDS_27_Tfcontratada_pessoanom_sel ;
      private String AV139WWContratoGarantiaDS_26_Tfcontratada_pessoanom ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String edtavContratogarantia_datapagtogarantia1_Internalname ;
      private String edtavContratogarantia_datapagtogarantia_to1_Internalname ;
      private String edtavContratogarantia_datacaucao1_Internalname ;
      private String edtavContratogarantia_datacaucao_to1_Internalname ;
      private String edtavContratogarantia_dataencerramento1_Internalname ;
      private String edtavContratogarantia_dataencerramento_to1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String edtavContratogarantia_datapagtogarantia2_Internalname ;
      private String edtavContratogarantia_datapagtogarantia_to2_Internalname ;
      private String edtavContratogarantia_datacaucao2_Internalname ;
      private String edtavContratogarantia_datacaucao_to2_Internalname ;
      private String edtavContratogarantia_dataencerramento2_Internalname ;
      private String edtavContratogarantia_dataencerramento_to2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String edtavContratogarantia_datapagtogarantia3_Internalname ;
      private String edtavContratogarantia_datapagtogarantia_to3_Internalname ;
      private String edtavContratogarantia_datacaucao3_Internalname ;
      private String edtavContratogarantia_datacaucao_to3_Internalname ;
      private String edtavContratogarantia_dataencerramento3_Internalname ;
      private String edtavContratogarantia_dataencerramento_to3_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_contrato_numero_Internalname ;
      private String Ddo_contratada_pessoanom_Internalname ;
      private String Ddo_contratada_pessoacnpj_Internalname ;
      private String Ddo_contratogarantia_datapagtogarantia_Internalname ;
      private String Ddo_contratogarantia_percentual_Internalname ;
      private String Ddo_contratogarantia_datacaucao_Internalname ;
      private String Ddo_contratogarantia_valorgarantia_Internalname ;
      private String Ddo_contratogarantia_dataencerramento_Internalname ;
      private String Ddo_contratogarantia_valorencerramento_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String edtContrato_Numero_Title ;
      private String edtContratada_PessoaNom_Title ;
      private String edtContratada_PessoaCNPJ_Title ;
      private String edtContratoGarantia_DataPagtoGarantia_Title ;
      private String edtContratoGarantia_Percentual_Title ;
      private String edtContratoGarantia_DataCaucao_Title ;
      private String edtContratoGarantia_ValorGarantia_Title ;
      private String edtContratoGarantia_DataEncerramento_Title ;
      private String edtContratoGarantia_ValorEncerramento_Title ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavUpdate_Link ;
      private String edtavDelete_Tooltiptext ;
      private String edtavDelete_Link ;
      private String edtContratada_PessoaNom_Link ;
      private String edtContratoGarantia_DataPagtoGarantia_Link ;
      private String tblTablemergeddynamicfilterscontratogarantia_datapagtogarantia1_Internalname ;
      private String tblTablemergeddynamicfilterscontratogarantia_datacaucao1_Internalname ;
      private String tblTablemergeddynamicfilterscontratogarantia_dataencerramento1_Internalname ;
      private String tblTablemergeddynamicfilterscontratogarantia_datapagtogarantia2_Internalname ;
      private String tblTablemergeddynamicfilterscontratogarantia_datacaucao2_Internalname ;
      private String tblTablemergeddynamicfilterscontratogarantia_dataencerramento2_Internalname ;
      private String tblTablemergeddynamicfilterscontratogarantia_datapagtogarantia3_Internalname ;
      private String tblTablemergeddynamicfilterscontratogarantia_datacaucao3_Internalname ;
      private String tblTablemergeddynamicfilterscontratogarantia_dataencerramento3_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTableheader_Internalname ;
      private String lblContratogarantiatitle_Internalname ;
      private String lblContratogarantiatitle_Jsonclick ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String edtavContratogarantia_dataencerramento3_Jsonclick ;
      private String lblDynamicfilterscontratogarantia_dataencerramento_rangemiddletext3_Internalname ;
      private String lblDynamicfilterscontratogarantia_dataencerramento_rangemiddletext3_Jsonclick ;
      private String edtavContratogarantia_dataencerramento_to3_Jsonclick ;
      private String edtavContratogarantia_datacaucao3_Jsonclick ;
      private String lblDynamicfilterscontratogarantia_datacaucao_rangemiddletext3_Internalname ;
      private String lblDynamicfilterscontratogarantia_datacaucao_rangemiddletext3_Jsonclick ;
      private String edtavContratogarantia_datacaucao_to3_Jsonclick ;
      private String edtavContratogarantia_datapagtogarantia3_Jsonclick ;
      private String lblDynamicfilterscontratogarantia_datapagtogarantia_rangemiddletext3_Internalname ;
      private String lblDynamicfilterscontratogarantia_datapagtogarantia_rangemiddletext3_Jsonclick ;
      private String edtavContratogarantia_datapagtogarantia_to3_Jsonclick ;
      private String edtavContratogarantia_dataencerramento2_Jsonclick ;
      private String lblDynamicfilterscontratogarantia_dataencerramento_rangemiddletext2_Internalname ;
      private String lblDynamicfilterscontratogarantia_dataencerramento_rangemiddletext2_Jsonclick ;
      private String edtavContratogarantia_dataencerramento_to2_Jsonclick ;
      private String edtavContratogarantia_datacaucao2_Jsonclick ;
      private String lblDynamicfilterscontratogarantia_datacaucao_rangemiddletext2_Internalname ;
      private String lblDynamicfilterscontratogarantia_datacaucao_rangemiddletext2_Jsonclick ;
      private String edtavContratogarantia_datacaucao_to2_Jsonclick ;
      private String edtavContratogarantia_datapagtogarantia2_Jsonclick ;
      private String lblDynamicfilterscontratogarantia_datapagtogarantia_rangemiddletext2_Internalname ;
      private String lblDynamicfilterscontratogarantia_datapagtogarantia_rangemiddletext2_Jsonclick ;
      private String edtavContratogarantia_datapagtogarantia_to2_Jsonclick ;
      private String edtavContratogarantia_dataencerramento1_Jsonclick ;
      private String lblDynamicfilterscontratogarantia_dataencerramento_rangemiddletext1_Internalname ;
      private String lblDynamicfilterscontratogarantia_dataencerramento_rangemiddletext1_Jsonclick ;
      private String edtavContratogarantia_dataencerramento_to1_Jsonclick ;
      private String edtavContratogarantia_datacaucao1_Jsonclick ;
      private String lblDynamicfilterscontratogarantia_datacaucao_rangemiddletext1_Internalname ;
      private String lblDynamicfilterscontratogarantia_datacaucao_rangemiddletext1_Jsonclick ;
      private String edtavContratogarantia_datacaucao_to1_Jsonclick ;
      private String edtavContratogarantia_datapagtogarantia1_Jsonclick ;
      private String lblDynamicfilterscontratogarantia_datapagtogarantia_rangemiddletext1_Internalname ;
      private String lblDynamicfilterscontratogarantia_datapagtogarantia_rangemiddletext1_Jsonclick ;
      private String edtavContratogarantia_datapagtogarantia_to1_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String imgInsert_Internalname ;
      private String imgInsert_Jsonclick ;
      private String sGXsfl_142_fel_idx="0001" ;
      private String ROClassString ;
      private String edtContrato_Numero_Jsonclick ;
      private String edtContratada_PessoaNom_Jsonclick ;
      private String edtContratada_PessoaCNPJ_Jsonclick ;
      private String edtContratoGarantia_DataPagtoGarantia_Jsonclick ;
      private String edtContratoGarantia_Percentual_Jsonclick ;
      private String edtContratoGarantia_DataCaucao_Jsonclick ;
      private String edtContratoGarantia_ValorGarantia_Jsonclick ;
      private String edtContratoGarantia_DataEncerramento_Jsonclick ;
      private String edtContratoGarantia_ValorEncerramento_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private DateTime AV16ContratoGarantia_DataPagtoGarantia1 ;
      private DateTime AV17ContratoGarantia_DataPagtoGarantia_To1 ;
      private DateTime AV37ContratoGarantia_DataCaucao1 ;
      private DateTime AV38ContratoGarantia_DataCaucao_To1 ;
      private DateTime AV39ContratoGarantia_DataEncerramento1 ;
      private DateTime AV40ContratoGarantia_DataEncerramento_To1 ;
      private DateTime AV20ContratoGarantia_DataPagtoGarantia2 ;
      private DateTime AV21ContratoGarantia_DataPagtoGarantia_To2 ;
      private DateTime AV41ContratoGarantia_DataCaucao2 ;
      private DateTime AV42ContratoGarantia_DataCaucao_To2 ;
      private DateTime AV43ContratoGarantia_DataEncerramento2 ;
      private DateTime AV44ContratoGarantia_DataEncerramento_To2 ;
      private DateTime AV24ContratoGarantia_DataPagtoGarantia3 ;
      private DateTime AV25ContratoGarantia_DataPagtoGarantia_To3 ;
      private DateTime AV45ContratoGarantia_DataCaucao3 ;
      private DateTime AV46ContratoGarantia_DataCaucao_To3 ;
      private DateTime AV47ContratoGarantia_DataEncerramento3 ;
      private DateTime AV48ContratoGarantia_DataEncerramento_To3 ;
      private DateTime AV64TFContratoGarantia_DataPagtoGarantia ;
      private DateTime AV65TFContratoGarantia_DataPagtoGarantia_To ;
      private DateTime AV74TFContratoGarantia_DataCaucao ;
      private DateTime AV75TFContratoGarantia_DataCaucao_To ;
      private DateTime AV84TFContratoGarantia_DataEncerramento ;
      private DateTime AV85TFContratoGarantia_DataEncerramento_To ;
      private DateTime AV66DDO_ContratoGarantia_DataPagtoGarantiaAuxDate ;
      private DateTime AV67DDO_ContratoGarantia_DataPagtoGarantiaAuxDateTo ;
      private DateTime AV76DDO_ContratoGarantia_DataCaucaoAuxDate ;
      private DateTime AV77DDO_ContratoGarantia_DataCaucaoAuxDateTo ;
      private DateTime AV86DDO_ContratoGarantia_DataEncerramentoAuxDate ;
      private DateTime AV87DDO_ContratoGarantia_DataEncerramentoAuxDateTo ;
      private DateTime A102ContratoGarantia_DataPagtoGarantia ;
      private DateTime A104ContratoGarantia_DataCaucao ;
      private DateTime A106ContratoGarantia_DataEncerramento ;
      private DateTime AV115WWContratoGarantiaDS_2_Contratogarantia_datapagtogarantia1 ;
      private DateTime AV116WWContratoGarantiaDS_3_Contratogarantia_datapagtogarantia_to1 ;
      private DateTime AV117WWContratoGarantiaDS_4_Contratogarantia_datacaucao1 ;
      private DateTime AV118WWContratoGarantiaDS_5_Contratogarantia_datacaucao_to1 ;
      private DateTime AV119WWContratoGarantiaDS_6_Contratogarantia_dataencerramento1 ;
      private DateTime AV120WWContratoGarantiaDS_7_Contratogarantia_dataencerramento_to1 ;
      private DateTime AV123WWContratoGarantiaDS_10_Contratogarantia_datapagtogarantia2 ;
      private DateTime AV124WWContratoGarantiaDS_11_Contratogarantia_datapagtogarantia_to2 ;
      private DateTime AV125WWContratoGarantiaDS_12_Contratogarantia_datacaucao2 ;
      private DateTime AV126WWContratoGarantiaDS_13_Contratogarantia_datacaucao_to2 ;
      private DateTime AV127WWContratoGarantiaDS_14_Contratogarantia_dataencerramento2 ;
      private DateTime AV128WWContratoGarantiaDS_15_Contratogarantia_dataencerramento_to2 ;
      private DateTime AV131WWContratoGarantiaDS_18_Contratogarantia_datapagtogarantia3 ;
      private DateTime AV132WWContratoGarantiaDS_19_Contratogarantia_datapagtogarantia_to3 ;
      private DateTime AV133WWContratoGarantiaDS_20_Contratogarantia_datacaucao3 ;
      private DateTime AV134WWContratoGarantiaDS_21_Contratogarantia_datacaucao_to3 ;
      private DateTime AV135WWContratoGarantiaDS_22_Contratogarantia_dataencerramento3 ;
      private DateTime AV136WWContratoGarantiaDS_23_Contratogarantia_dataencerramento_to3 ;
      private DateTime AV143WWContratoGarantiaDS_30_Tfcontratogarantia_datapagtogarantia ;
      private DateTime AV144WWContratoGarantiaDS_31_Tfcontratogarantia_datapagtogarantia_to ;
      private DateTime AV147WWContratoGarantiaDS_34_Tfcontratogarantia_datacaucao ;
      private DateTime AV148WWContratoGarantiaDS_35_Tfcontratogarantia_datacaucao_to ;
      private DateTime AV151WWContratoGarantiaDS_38_Tfcontratogarantia_dataencerramento ;
      private DateTime AV152WWContratoGarantiaDS_39_Tfcontratogarantia_dataencerramento_to ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool AV18DynamicFiltersEnabled2 ;
      private bool AV22DynamicFiltersEnabled3 ;
      private bool AV27DynamicFiltersIgnoreFirst ;
      private bool AV26DynamicFiltersRemoving ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_contrato_numero_Includesortasc ;
      private bool Ddo_contrato_numero_Includesortdsc ;
      private bool Ddo_contrato_numero_Includefilter ;
      private bool Ddo_contrato_numero_Filterisrange ;
      private bool Ddo_contrato_numero_Includedatalist ;
      private bool Ddo_contratada_pessoanom_Includesortasc ;
      private bool Ddo_contratada_pessoanom_Includesortdsc ;
      private bool Ddo_contratada_pessoanom_Includefilter ;
      private bool Ddo_contratada_pessoanom_Filterisrange ;
      private bool Ddo_contratada_pessoanom_Includedatalist ;
      private bool Ddo_contratada_pessoacnpj_Includesortasc ;
      private bool Ddo_contratada_pessoacnpj_Includesortdsc ;
      private bool Ddo_contratada_pessoacnpj_Includefilter ;
      private bool Ddo_contratada_pessoacnpj_Filterisrange ;
      private bool Ddo_contratada_pessoacnpj_Includedatalist ;
      private bool Ddo_contratogarantia_datapagtogarantia_Includesortasc ;
      private bool Ddo_contratogarantia_datapagtogarantia_Includesortdsc ;
      private bool Ddo_contratogarantia_datapagtogarantia_Includefilter ;
      private bool Ddo_contratogarantia_datapagtogarantia_Filterisrange ;
      private bool Ddo_contratogarantia_datapagtogarantia_Includedatalist ;
      private bool Ddo_contratogarantia_percentual_Includesortasc ;
      private bool Ddo_contratogarantia_percentual_Includesortdsc ;
      private bool Ddo_contratogarantia_percentual_Includefilter ;
      private bool Ddo_contratogarantia_percentual_Filterisrange ;
      private bool Ddo_contratogarantia_percentual_Includedatalist ;
      private bool Ddo_contratogarantia_datacaucao_Includesortasc ;
      private bool Ddo_contratogarantia_datacaucao_Includesortdsc ;
      private bool Ddo_contratogarantia_datacaucao_Includefilter ;
      private bool Ddo_contratogarantia_datacaucao_Filterisrange ;
      private bool Ddo_contratogarantia_datacaucao_Includedatalist ;
      private bool Ddo_contratogarantia_valorgarantia_Includesortasc ;
      private bool Ddo_contratogarantia_valorgarantia_Includesortdsc ;
      private bool Ddo_contratogarantia_valorgarantia_Includefilter ;
      private bool Ddo_contratogarantia_valorgarantia_Filterisrange ;
      private bool Ddo_contratogarantia_valorgarantia_Includedatalist ;
      private bool Ddo_contratogarantia_dataencerramento_Includesortasc ;
      private bool Ddo_contratogarantia_dataencerramento_Includesortdsc ;
      private bool Ddo_contratogarantia_dataencerramento_Includefilter ;
      private bool Ddo_contratogarantia_dataencerramento_Filterisrange ;
      private bool Ddo_contratogarantia_dataencerramento_Includedatalist ;
      private bool Ddo_contratogarantia_valorencerramento_Includesortasc ;
      private bool Ddo_contratogarantia_valorencerramento_Includesortdsc ;
      private bool Ddo_contratogarantia_valorencerramento_Includefilter ;
      private bool Ddo_contratogarantia_valorencerramento_Filterisrange ;
      private bool Ddo_contratogarantia_valorencerramento_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n41Contratada_PessoaNom ;
      private bool n42Contratada_PessoaCNPJ ;
      private bool AV121WWContratoGarantiaDS_8_Dynamicfiltersenabled2 ;
      private bool AV129WWContratoGarantiaDS_16_Dynamicfiltersenabled3 ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV28Update_IsBlob ;
      private bool AV29Delete_IsBlob ;
      private String AV15DynamicFiltersSelector1 ;
      private String AV19DynamicFiltersSelector2 ;
      private String AV23DynamicFiltersSelector3 ;
      private String AV60TFContratada_PessoaCNPJ ;
      private String AV61TFContratada_PessoaCNPJ_Sel ;
      private String AV54ddo_Contrato_NumeroTitleControlIdToReplace ;
      private String AV58ddo_Contratada_PessoaNomTitleControlIdToReplace ;
      private String AV62ddo_Contratada_PessoaCNPJTitleControlIdToReplace ;
      private String AV68ddo_ContratoGarantia_DataPagtoGarantiaTitleControlIdToReplace ;
      private String AV72ddo_ContratoGarantia_PercentualTitleControlIdToReplace ;
      private String AV78ddo_ContratoGarantia_DataCaucaoTitleControlIdToReplace ;
      private String AV82ddo_ContratoGarantia_ValorGarantiaTitleControlIdToReplace ;
      private String AV88ddo_ContratoGarantia_DataEncerramentoTitleControlIdToReplace ;
      private String AV92ddo_ContratoGarantia_ValorEncerramentoTitleControlIdToReplace ;
      private String AV155Update_GXI ;
      private String AV156Delete_GXI ;
      private String A42Contratada_PessoaCNPJ ;
      private String lV141WWContratoGarantiaDS_28_Tfcontratada_pessoacnpj ;
      private String AV114WWContratoGarantiaDS_1_Dynamicfiltersselector1 ;
      private String AV122WWContratoGarantiaDS_9_Dynamicfiltersselector2 ;
      private String AV130WWContratoGarantiaDS_17_Dynamicfiltersselector3 ;
      private String AV142WWContratoGarantiaDS_29_Tfcontratada_pessoacnpj_sel ;
      private String AV141WWContratoGarantiaDS_28_Tfcontratada_pessoacnpj ;
      private String AV28Update ;
      private String AV29Delete ;
      private IGxSession AV30Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private int[] H006H2_A40Contratada_PessoaCod ;
      private int[] H006H2_A74Contrato_Codigo ;
      private int[] H006H2_A101ContratoGarantia_Codigo ;
      private int[] H006H2_A39Contratada_Codigo ;
      private decimal[] H006H2_A107ContratoGarantia_ValorEncerramento ;
      private DateTime[] H006H2_A106ContratoGarantia_DataEncerramento ;
      private decimal[] H006H2_A105ContratoGarantia_ValorGarantia ;
      private DateTime[] H006H2_A104ContratoGarantia_DataCaucao ;
      private decimal[] H006H2_A103ContratoGarantia_Percentual ;
      private DateTime[] H006H2_A102ContratoGarantia_DataPagtoGarantia ;
      private String[] H006H2_A42Contratada_PessoaCNPJ ;
      private bool[] H006H2_n42Contratada_PessoaCNPJ ;
      private String[] H006H2_A41Contratada_PessoaNom ;
      private bool[] H006H2_n41Contratada_PessoaNom ;
      private String[] H006H2_A77Contrato_Numero ;
      private long[] H006H3_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV7HTTPRequest ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV51Contrato_NumeroTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV55Contratada_PessoaNomTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV59Contratada_PessoaCNPJTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV63ContratoGarantia_DataPagtoGarantiaTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV69ContratoGarantia_PercentualTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV73ContratoGarantia_DataCaucaoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV79ContratoGarantia_ValorGarantiaTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV83ContratoGarantia_DataEncerramentoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV89ContratoGarantia_ValorEncerramentoTitleFilterData ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV11GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV93DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class wwcontratogarantia__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H006H2( IGxContext context ,
                                             String AV114WWContratoGarantiaDS_1_Dynamicfiltersselector1 ,
                                             DateTime AV115WWContratoGarantiaDS_2_Contratogarantia_datapagtogarantia1 ,
                                             DateTime AV116WWContratoGarantiaDS_3_Contratogarantia_datapagtogarantia_to1 ,
                                             DateTime AV117WWContratoGarantiaDS_4_Contratogarantia_datacaucao1 ,
                                             DateTime AV118WWContratoGarantiaDS_5_Contratogarantia_datacaucao_to1 ,
                                             DateTime AV119WWContratoGarantiaDS_6_Contratogarantia_dataencerramento1 ,
                                             DateTime AV120WWContratoGarantiaDS_7_Contratogarantia_dataencerramento_to1 ,
                                             bool AV121WWContratoGarantiaDS_8_Dynamicfiltersenabled2 ,
                                             String AV122WWContratoGarantiaDS_9_Dynamicfiltersselector2 ,
                                             DateTime AV123WWContratoGarantiaDS_10_Contratogarantia_datapagtogarantia2 ,
                                             DateTime AV124WWContratoGarantiaDS_11_Contratogarantia_datapagtogarantia_to2 ,
                                             DateTime AV125WWContratoGarantiaDS_12_Contratogarantia_datacaucao2 ,
                                             DateTime AV126WWContratoGarantiaDS_13_Contratogarantia_datacaucao_to2 ,
                                             DateTime AV127WWContratoGarantiaDS_14_Contratogarantia_dataencerramento2 ,
                                             DateTime AV128WWContratoGarantiaDS_15_Contratogarantia_dataencerramento_to2 ,
                                             bool AV129WWContratoGarantiaDS_16_Dynamicfiltersenabled3 ,
                                             String AV130WWContratoGarantiaDS_17_Dynamicfiltersselector3 ,
                                             DateTime AV131WWContratoGarantiaDS_18_Contratogarantia_datapagtogarantia3 ,
                                             DateTime AV132WWContratoGarantiaDS_19_Contratogarantia_datapagtogarantia_to3 ,
                                             DateTime AV133WWContratoGarantiaDS_20_Contratogarantia_datacaucao3 ,
                                             DateTime AV134WWContratoGarantiaDS_21_Contratogarantia_datacaucao_to3 ,
                                             DateTime AV135WWContratoGarantiaDS_22_Contratogarantia_dataencerramento3 ,
                                             DateTime AV136WWContratoGarantiaDS_23_Contratogarantia_dataencerramento_to3 ,
                                             String AV138WWContratoGarantiaDS_25_Tfcontrato_numero_sel ,
                                             String AV137WWContratoGarantiaDS_24_Tfcontrato_numero ,
                                             String AV140WWContratoGarantiaDS_27_Tfcontratada_pessoanom_sel ,
                                             String AV139WWContratoGarantiaDS_26_Tfcontratada_pessoanom ,
                                             String AV142WWContratoGarantiaDS_29_Tfcontratada_pessoacnpj_sel ,
                                             String AV141WWContratoGarantiaDS_28_Tfcontratada_pessoacnpj ,
                                             DateTime AV143WWContratoGarantiaDS_30_Tfcontratogarantia_datapagtogarantia ,
                                             DateTime AV144WWContratoGarantiaDS_31_Tfcontratogarantia_datapagtogarantia_to ,
                                             decimal AV145WWContratoGarantiaDS_32_Tfcontratogarantia_percentual ,
                                             decimal AV146WWContratoGarantiaDS_33_Tfcontratogarantia_percentual_to ,
                                             DateTime AV147WWContratoGarantiaDS_34_Tfcontratogarantia_datacaucao ,
                                             DateTime AV148WWContratoGarantiaDS_35_Tfcontratogarantia_datacaucao_to ,
                                             decimal AV149WWContratoGarantiaDS_36_Tfcontratogarantia_valorgarantia ,
                                             decimal AV150WWContratoGarantiaDS_37_Tfcontratogarantia_valorgarantia_to ,
                                             DateTime AV151WWContratoGarantiaDS_38_Tfcontratogarantia_dataencerramento ,
                                             DateTime AV152WWContratoGarantiaDS_39_Tfcontratogarantia_dataencerramento_to ,
                                             decimal AV153WWContratoGarantiaDS_40_Tfcontratogarantia_valorencerramento ,
                                             decimal AV154WWContratoGarantiaDS_41_Tfcontratogarantia_valorencerramento_to ,
                                             DateTime A102ContratoGarantia_DataPagtoGarantia ,
                                             DateTime A104ContratoGarantia_DataCaucao ,
                                             DateTime A106ContratoGarantia_DataEncerramento ,
                                             String A77Contrato_Numero ,
                                             String A41Contratada_PessoaNom ,
                                             String A42Contratada_PessoaCNPJ ,
                                             decimal A103ContratoGarantia_Percentual ,
                                             decimal A105ContratoGarantia_ValorGarantia ,
                                             decimal A107ContratoGarantia_ValorEncerramento ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [41] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T3.[Contratada_PessoaCod] AS Contratada_PessoaCod, T1.[Contrato_Codigo], T1.[ContratoGarantia_Codigo], T2.[Contratada_Codigo], T1.[ContratoGarantia_ValorEncerramento], T1.[ContratoGarantia_DataEncerramento], T1.[ContratoGarantia_ValorGarantia], T1.[ContratoGarantia_DataCaucao], T1.[ContratoGarantia_Percentual], T1.[ContratoGarantia_DataPagtoGarantia], T4.[Pessoa_Docto] AS Contratada_PessoaCNPJ, T4.[Pessoa_Nome] AS Contratada_PessoaNom, T2.[Contrato_Numero]";
         sFromString = " FROM ((([ContratoGarantia] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo]) INNER JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T2.[Contratada_Codigo]) INNER JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T3.[Contratada_PessoaCod])";
         sOrderString = "";
         if ( ( StringUtil.StrCmp(AV114WWContratoGarantiaDS_1_Dynamicfiltersselector1, "CONTRATOGARANTIA_DATAPAGTOGARANTIA") == 0 ) && ( ! (DateTime.MinValue==AV115WWContratoGarantiaDS_2_Contratogarantia_datapagtogarantia1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataPagtoGarantia] >= @AV115WWContratoGarantiaDS_2_Contratogarantia_datapagtogarantia1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataPagtoGarantia] >= @AV115WWContratoGarantiaDS_2_Contratogarantia_datapagtogarantia1)";
            }
         }
         else
         {
            GXv_int2[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV114WWContratoGarantiaDS_1_Dynamicfiltersselector1, "CONTRATOGARANTIA_DATAPAGTOGARANTIA") == 0 ) && ( ! (DateTime.MinValue==AV116WWContratoGarantiaDS_3_Contratogarantia_datapagtogarantia_to1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataPagtoGarantia] <= @AV116WWContratoGarantiaDS_3_Contratogarantia_datapagtogarantia_to1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataPagtoGarantia] <= @AV116WWContratoGarantiaDS_3_Contratogarantia_datapagtogarantia_to1)";
            }
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV114WWContratoGarantiaDS_1_Dynamicfiltersselector1, "CONTRATOGARANTIA_DATACAUCAO") == 0 ) && ( ! (DateTime.MinValue==AV117WWContratoGarantiaDS_4_Contratogarantia_datacaucao1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataCaucao] >= @AV117WWContratoGarantiaDS_4_Contratogarantia_datacaucao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataCaucao] >= @AV117WWContratoGarantiaDS_4_Contratogarantia_datacaucao1)";
            }
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV114WWContratoGarantiaDS_1_Dynamicfiltersselector1, "CONTRATOGARANTIA_DATACAUCAO") == 0 ) && ( ! (DateTime.MinValue==AV118WWContratoGarantiaDS_5_Contratogarantia_datacaucao_to1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataCaucao] <= @AV118WWContratoGarantiaDS_5_Contratogarantia_datacaucao_to1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataCaucao] <= @AV118WWContratoGarantiaDS_5_Contratogarantia_datacaucao_to1)";
            }
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV114WWContratoGarantiaDS_1_Dynamicfiltersselector1, "CONTRATOGARANTIA_DATAENCERRAMENTO") == 0 ) && ( ! (DateTime.MinValue==AV119WWContratoGarantiaDS_6_Contratogarantia_dataencerramento1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataEncerramento] >= @AV119WWContratoGarantiaDS_6_Contratogarantia_dataencerramento1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataEncerramento] >= @AV119WWContratoGarantiaDS_6_Contratogarantia_dataencerramento1)";
            }
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV114WWContratoGarantiaDS_1_Dynamicfiltersselector1, "CONTRATOGARANTIA_DATAENCERRAMENTO") == 0 ) && ( ! (DateTime.MinValue==AV120WWContratoGarantiaDS_7_Contratogarantia_dataencerramento_to1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataEncerramento] <= @AV120WWContratoGarantiaDS_7_Contratogarantia_dataencerramento_to1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataEncerramento] <= @AV120WWContratoGarantiaDS_7_Contratogarantia_dataencerramento_to1)";
            }
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( AV121WWContratoGarantiaDS_8_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV122WWContratoGarantiaDS_9_Dynamicfiltersselector2, "CONTRATOGARANTIA_DATAPAGTOGARANTIA") == 0 ) && ( ! (DateTime.MinValue==AV123WWContratoGarantiaDS_10_Contratogarantia_datapagtogarantia2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataPagtoGarantia] >= @AV123WWContratoGarantiaDS_10_Contratogarantia_datapagtogarantia2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataPagtoGarantia] >= @AV123WWContratoGarantiaDS_10_Contratogarantia_datapagtogarantia2)";
            }
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( AV121WWContratoGarantiaDS_8_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV122WWContratoGarantiaDS_9_Dynamicfiltersselector2, "CONTRATOGARANTIA_DATAPAGTOGARANTIA") == 0 ) && ( ! (DateTime.MinValue==AV124WWContratoGarantiaDS_11_Contratogarantia_datapagtogarantia_to2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataPagtoGarantia] <= @AV124WWContratoGarantiaDS_11_Contratogarantia_datapagtogarantia_to2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataPagtoGarantia] <= @AV124WWContratoGarantiaDS_11_Contratogarantia_datapagtogarantia_to2)";
            }
         }
         else
         {
            GXv_int2[7] = 1;
         }
         if ( AV121WWContratoGarantiaDS_8_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV122WWContratoGarantiaDS_9_Dynamicfiltersselector2, "CONTRATOGARANTIA_DATACAUCAO") == 0 ) && ( ! (DateTime.MinValue==AV125WWContratoGarantiaDS_12_Contratogarantia_datacaucao2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataCaucao] >= @AV125WWContratoGarantiaDS_12_Contratogarantia_datacaucao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataCaucao] >= @AV125WWContratoGarantiaDS_12_Contratogarantia_datacaucao2)";
            }
         }
         else
         {
            GXv_int2[8] = 1;
         }
         if ( AV121WWContratoGarantiaDS_8_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV122WWContratoGarantiaDS_9_Dynamicfiltersselector2, "CONTRATOGARANTIA_DATACAUCAO") == 0 ) && ( ! (DateTime.MinValue==AV126WWContratoGarantiaDS_13_Contratogarantia_datacaucao_to2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataCaucao] <= @AV126WWContratoGarantiaDS_13_Contratogarantia_datacaucao_to2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataCaucao] <= @AV126WWContratoGarantiaDS_13_Contratogarantia_datacaucao_to2)";
            }
         }
         else
         {
            GXv_int2[9] = 1;
         }
         if ( AV121WWContratoGarantiaDS_8_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV122WWContratoGarantiaDS_9_Dynamicfiltersselector2, "CONTRATOGARANTIA_DATAENCERRAMENTO") == 0 ) && ( ! (DateTime.MinValue==AV127WWContratoGarantiaDS_14_Contratogarantia_dataencerramento2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataEncerramento] >= @AV127WWContratoGarantiaDS_14_Contratogarantia_dataencerramento2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataEncerramento] >= @AV127WWContratoGarantiaDS_14_Contratogarantia_dataencerramento2)";
            }
         }
         else
         {
            GXv_int2[10] = 1;
         }
         if ( AV121WWContratoGarantiaDS_8_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV122WWContratoGarantiaDS_9_Dynamicfiltersselector2, "CONTRATOGARANTIA_DATAENCERRAMENTO") == 0 ) && ( ! (DateTime.MinValue==AV128WWContratoGarantiaDS_15_Contratogarantia_dataencerramento_to2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataEncerramento] <= @AV128WWContratoGarantiaDS_15_Contratogarantia_dataencerramento_to2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataEncerramento] <= @AV128WWContratoGarantiaDS_15_Contratogarantia_dataencerramento_to2)";
            }
         }
         else
         {
            GXv_int2[11] = 1;
         }
         if ( AV129WWContratoGarantiaDS_16_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV130WWContratoGarantiaDS_17_Dynamicfiltersselector3, "CONTRATOGARANTIA_DATAPAGTOGARANTIA") == 0 ) && ( ! (DateTime.MinValue==AV131WWContratoGarantiaDS_18_Contratogarantia_datapagtogarantia3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataPagtoGarantia] >= @AV131WWContratoGarantiaDS_18_Contratogarantia_datapagtogarantia3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataPagtoGarantia] >= @AV131WWContratoGarantiaDS_18_Contratogarantia_datapagtogarantia3)";
            }
         }
         else
         {
            GXv_int2[12] = 1;
         }
         if ( AV129WWContratoGarantiaDS_16_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV130WWContratoGarantiaDS_17_Dynamicfiltersselector3, "CONTRATOGARANTIA_DATAPAGTOGARANTIA") == 0 ) && ( ! (DateTime.MinValue==AV132WWContratoGarantiaDS_19_Contratogarantia_datapagtogarantia_to3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataPagtoGarantia] <= @AV132WWContratoGarantiaDS_19_Contratogarantia_datapagtogarantia_to3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataPagtoGarantia] <= @AV132WWContratoGarantiaDS_19_Contratogarantia_datapagtogarantia_to3)";
            }
         }
         else
         {
            GXv_int2[13] = 1;
         }
         if ( AV129WWContratoGarantiaDS_16_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV130WWContratoGarantiaDS_17_Dynamicfiltersselector3, "CONTRATOGARANTIA_DATACAUCAO") == 0 ) && ( ! (DateTime.MinValue==AV133WWContratoGarantiaDS_20_Contratogarantia_datacaucao3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataCaucao] >= @AV133WWContratoGarantiaDS_20_Contratogarantia_datacaucao3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataCaucao] >= @AV133WWContratoGarantiaDS_20_Contratogarantia_datacaucao3)";
            }
         }
         else
         {
            GXv_int2[14] = 1;
         }
         if ( AV129WWContratoGarantiaDS_16_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV130WWContratoGarantiaDS_17_Dynamicfiltersselector3, "CONTRATOGARANTIA_DATACAUCAO") == 0 ) && ( ! (DateTime.MinValue==AV134WWContratoGarantiaDS_21_Contratogarantia_datacaucao_to3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataCaucao] <= @AV134WWContratoGarantiaDS_21_Contratogarantia_datacaucao_to3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataCaucao] <= @AV134WWContratoGarantiaDS_21_Contratogarantia_datacaucao_to3)";
            }
         }
         else
         {
            GXv_int2[15] = 1;
         }
         if ( AV129WWContratoGarantiaDS_16_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV130WWContratoGarantiaDS_17_Dynamicfiltersselector3, "CONTRATOGARANTIA_DATAENCERRAMENTO") == 0 ) && ( ! (DateTime.MinValue==AV135WWContratoGarantiaDS_22_Contratogarantia_dataencerramento3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataEncerramento] >= @AV135WWContratoGarantiaDS_22_Contratogarantia_dataencerramento3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataEncerramento] >= @AV135WWContratoGarantiaDS_22_Contratogarantia_dataencerramento3)";
            }
         }
         else
         {
            GXv_int2[16] = 1;
         }
         if ( AV129WWContratoGarantiaDS_16_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV130WWContratoGarantiaDS_17_Dynamicfiltersselector3, "CONTRATOGARANTIA_DATAENCERRAMENTO") == 0 ) && ( ! (DateTime.MinValue==AV136WWContratoGarantiaDS_23_Contratogarantia_dataencerramento_to3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataEncerramento] <= @AV136WWContratoGarantiaDS_23_Contratogarantia_dataencerramento_to3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataEncerramento] <= @AV136WWContratoGarantiaDS_23_Contratogarantia_dataencerramento_to3)";
            }
         }
         else
         {
            GXv_int2[17] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV138WWContratoGarantiaDS_25_Tfcontrato_numero_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV137WWContratoGarantiaDS_24_Tfcontrato_numero)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] like @lV137WWContratoGarantiaDS_24_Tfcontrato_numero)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] like @lV137WWContratoGarantiaDS_24_Tfcontrato_numero)";
            }
         }
         else
         {
            GXv_int2[18] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV138WWContratoGarantiaDS_25_Tfcontrato_numero_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] = @AV138WWContratoGarantiaDS_25_Tfcontrato_numero_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] = @AV138WWContratoGarantiaDS_25_Tfcontrato_numero_sel)";
            }
         }
         else
         {
            GXv_int2[19] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV140WWContratoGarantiaDS_27_Tfcontratada_pessoanom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV139WWContratoGarantiaDS_26_Tfcontratada_pessoanom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like @lV139WWContratoGarantiaDS_26_Tfcontratada_pessoanom)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like @lV139WWContratoGarantiaDS_26_Tfcontratada_pessoanom)";
            }
         }
         else
         {
            GXv_int2[20] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV140WWContratoGarantiaDS_27_Tfcontratada_pessoanom_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] = @AV140WWContratoGarantiaDS_27_Tfcontratada_pessoanom_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] = @AV140WWContratoGarantiaDS_27_Tfcontratada_pessoanom_sel)";
            }
         }
         else
         {
            GXv_int2[21] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV142WWContratoGarantiaDS_29_Tfcontratada_pessoacnpj_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV141WWContratoGarantiaDS_28_Tfcontratada_pessoacnpj)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Docto] like @lV141WWContratoGarantiaDS_28_Tfcontratada_pessoacnpj)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Docto] like @lV141WWContratoGarantiaDS_28_Tfcontratada_pessoacnpj)";
            }
         }
         else
         {
            GXv_int2[22] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV142WWContratoGarantiaDS_29_Tfcontratada_pessoacnpj_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Docto] = @AV142WWContratoGarantiaDS_29_Tfcontratada_pessoacnpj_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Docto] = @AV142WWContratoGarantiaDS_29_Tfcontratada_pessoacnpj_sel)";
            }
         }
         else
         {
            GXv_int2[23] = 1;
         }
         if ( ! (DateTime.MinValue==AV143WWContratoGarantiaDS_30_Tfcontratogarantia_datapagtogarantia) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataPagtoGarantia] >= @AV143WWContratoGarantiaDS_30_Tfcontratogarantia_datapagtogarantia)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataPagtoGarantia] >= @AV143WWContratoGarantiaDS_30_Tfcontratogarantia_datapagtogarantia)";
            }
         }
         else
         {
            GXv_int2[24] = 1;
         }
         if ( ! (DateTime.MinValue==AV144WWContratoGarantiaDS_31_Tfcontratogarantia_datapagtogarantia_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataPagtoGarantia] <= @AV144WWContratoGarantiaDS_31_Tfcontratogarantia_datapagtogarantia_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataPagtoGarantia] <= @AV144WWContratoGarantiaDS_31_Tfcontratogarantia_datapagtogarantia_to)";
            }
         }
         else
         {
            GXv_int2[25] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV145WWContratoGarantiaDS_32_Tfcontratogarantia_percentual) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_Percentual] >= @AV145WWContratoGarantiaDS_32_Tfcontratogarantia_percentual)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_Percentual] >= @AV145WWContratoGarantiaDS_32_Tfcontratogarantia_percentual)";
            }
         }
         else
         {
            GXv_int2[26] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV146WWContratoGarantiaDS_33_Tfcontratogarantia_percentual_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_Percentual] <= @AV146WWContratoGarantiaDS_33_Tfcontratogarantia_percentual_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_Percentual] <= @AV146WWContratoGarantiaDS_33_Tfcontratogarantia_percentual_to)";
            }
         }
         else
         {
            GXv_int2[27] = 1;
         }
         if ( ! (DateTime.MinValue==AV147WWContratoGarantiaDS_34_Tfcontratogarantia_datacaucao) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataCaucao] >= @AV147WWContratoGarantiaDS_34_Tfcontratogarantia_datacaucao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataCaucao] >= @AV147WWContratoGarantiaDS_34_Tfcontratogarantia_datacaucao)";
            }
         }
         else
         {
            GXv_int2[28] = 1;
         }
         if ( ! (DateTime.MinValue==AV148WWContratoGarantiaDS_35_Tfcontratogarantia_datacaucao_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataCaucao] <= @AV148WWContratoGarantiaDS_35_Tfcontratogarantia_datacaucao_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataCaucao] <= @AV148WWContratoGarantiaDS_35_Tfcontratogarantia_datacaucao_to)";
            }
         }
         else
         {
            GXv_int2[29] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV149WWContratoGarantiaDS_36_Tfcontratogarantia_valorgarantia) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_ValorGarantia] >= @AV149WWContratoGarantiaDS_36_Tfcontratogarantia_valorgarantia)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_ValorGarantia] >= @AV149WWContratoGarantiaDS_36_Tfcontratogarantia_valorgarantia)";
            }
         }
         else
         {
            GXv_int2[30] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV150WWContratoGarantiaDS_37_Tfcontratogarantia_valorgarantia_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_ValorGarantia] <= @AV150WWContratoGarantiaDS_37_Tfcontratogarantia_valorgarantia_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_ValorGarantia] <= @AV150WWContratoGarantiaDS_37_Tfcontratogarantia_valorgarantia_to)";
            }
         }
         else
         {
            GXv_int2[31] = 1;
         }
         if ( ! (DateTime.MinValue==AV151WWContratoGarantiaDS_38_Tfcontratogarantia_dataencerramento) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataEncerramento] >= @AV151WWContratoGarantiaDS_38_Tfcontratogarantia_dataencerramento)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataEncerramento] >= @AV151WWContratoGarantiaDS_38_Tfcontratogarantia_dataencerramento)";
            }
         }
         else
         {
            GXv_int2[32] = 1;
         }
         if ( ! (DateTime.MinValue==AV152WWContratoGarantiaDS_39_Tfcontratogarantia_dataencerramento_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataEncerramento] <= @AV152WWContratoGarantiaDS_39_Tfcontratogarantia_dataencerramento_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataEncerramento] <= @AV152WWContratoGarantiaDS_39_Tfcontratogarantia_dataencerramento_to)";
            }
         }
         else
         {
            GXv_int2[33] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV153WWContratoGarantiaDS_40_Tfcontratogarantia_valorencerramento) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_ValorEncerramento] >= @AV153WWContratoGarantiaDS_40_Tfcontratogarantia_valorencerramento)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_ValorEncerramento] >= @AV153WWContratoGarantiaDS_40_Tfcontratogarantia_valorencerramento)";
            }
         }
         else
         {
            GXv_int2[34] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV154WWContratoGarantiaDS_41_Tfcontratogarantia_valorencerramento_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_ValorEncerramento] <= @AV154WWContratoGarantiaDS_41_Tfcontratogarantia_valorencerramento_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_ValorEncerramento] <= @AV154WWContratoGarantiaDS_41_Tfcontratogarantia_valorencerramento_to)";
            }
         }
         else
         {
            GXv_int2[35] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            sWhereString = " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoGarantia_DataPagtoGarantia]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoGarantia_DataPagtoGarantia] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Contrato_Numero]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Contrato_Numero] DESC";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T4.[Pessoa_Nome]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T4.[Pessoa_Nome] DESC";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T4.[Pessoa_Docto]";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T4.[Pessoa_Docto] DESC";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoGarantia_Percentual]";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoGarantia_Percentual] DESC";
         }
         else if ( ( AV13OrderedBy == 6 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoGarantia_DataCaucao]";
         }
         else if ( ( AV13OrderedBy == 6 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoGarantia_DataCaucao] DESC";
         }
         else if ( ( AV13OrderedBy == 7 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoGarantia_ValorGarantia]";
         }
         else if ( ( AV13OrderedBy == 7 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoGarantia_ValorGarantia] DESC";
         }
         else if ( ( AV13OrderedBy == 8 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoGarantia_DataEncerramento]";
         }
         else if ( ( AV13OrderedBy == 8 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoGarantia_DataEncerramento] DESC";
         }
         else if ( ( AV13OrderedBy == 9 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoGarantia_ValorEncerramento]";
         }
         else if ( ( AV13OrderedBy == 9 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoGarantia_ValorEncerramento] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoGarantia_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H006H3( IGxContext context ,
                                             String AV114WWContratoGarantiaDS_1_Dynamicfiltersselector1 ,
                                             DateTime AV115WWContratoGarantiaDS_2_Contratogarantia_datapagtogarantia1 ,
                                             DateTime AV116WWContratoGarantiaDS_3_Contratogarantia_datapagtogarantia_to1 ,
                                             DateTime AV117WWContratoGarantiaDS_4_Contratogarantia_datacaucao1 ,
                                             DateTime AV118WWContratoGarantiaDS_5_Contratogarantia_datacaucao_to1 ,
                                             DateTime AV119WWContratoGarantiaDS_6_Contratogarantia_dataencerramento1 ,
                                             DateTime AV120WWContratoGarantiaDS_7_Contratogarantia_dataencerramento_to1 ,
                                             bool AV121WWContratoGarantiaDS_8_Dynamicfiltersenabled2 ,
                                             String AV122WWContratoGarantiaDS_9_Dynamicfiltersselector2 ,
                                             DateTime AV123WWContratoGarantiaDS_10_Contratogarantia_datapagtogarantia2 ,
                                             DateTime AV124WWContratoGarantiaDS_11_Contratogarantia_datapagtogarantia_to2 ,
                                             DateTime AV125WWContratoGarantiaDS_12_Contratogarantia_datacaucao2 ,
                                             DateTime AV126WWContratoGarantiaDS_13_Contratogarantia_datacaucao_to2 ,
                                             DateTime AV127WWContratoGarantiaDS_14_Contratogarantia_dataencerramento2 ,
                                             DateTime AV128WWContratoGarantiaDS_15_Contratogarantia_dataencerramento_to2 ,
                                             bool AV129WWContratoGarantiaDS_16_Dynamicfiltersenabled3 ,
                                             String AV130WWContratoGarantiaDS_17_Dynamicfiltersselector3 ,
                                             DateTime AV131WWContratoGarantiaDS_18_Contratogarantia_datapagtogarantia3 ,
                                             DateTime AV132WWContratoGarantiaDS_19_Contratogarantia_datapagtogarantia_to3 ,
                                             DateTime AV133WWContratoGarantiaDS_20_Contratogarantia_datacaucao3 ,
                                             DateTime AV134WWContratoGarantiaDS_21_Contratogarantia_datacaucao_to3 ,
                                             DateTime AV135WWContratoGarantiaDS_22_Contratogarantia_dataencerramento3 ,
                                             DateTime AV136WWContratoGarantiaDS_23_Contratogarantia_dataencerramento_to3 ,
                                             String AV138WWContratoGarantiaDS_25_Tfcontrato_numero_sel ,
                                             String AV137WWContratoGarantiaDS_24_Tfcontrato_numero ,
                                             String AV140WWContratoGarantiaDS_27_Tfcontratada_pessoanom_sel ,
                                             String AV139WWContratoGarantiaDS_26_Tfcontratada_pessoanom ,
                                             String AV142WWContratoGarantiaDS_29_Tfcontratada_pessoacnpj_sel ,
                                             String AV141WWContratoGarantiaDS_28_Tfcontratada_pessoacnpj ,
                                             DateTime AV143WWContratoGarantiaDS_30_Tfcontratogarantia_datapagtogarantia ,
                                             DateTime AV144WWContratoGarantiaDS_31_Tfcontratogarantia_datapagtogarantia_to ,
                                             decimal AV145WWContratoGarantiaDS_32_Tfcontratogarantia_percentual ,
                                             decimal AV146WWContratoGarantiaDS_33_Tfcontratogarantia_percentual_to ,
                                             DateTime AV147WWContratoGarantiaDS_34_Tfcontratogarantia_datacaucao ,
                                             DateTime AV148WWContratoGarantiaDS_35_Tfcontratogarantia_datacaucao_to ,
                                             decimal AV149WWContratoGarantiaDS_36_Tfcontratogarantia_valorgarantia ,
                                             decimal AV150WWContratoGarantiaDS_37_Tfcontratogarantia_valorgarantia_to ,
                                             DateTime AV151WWContratoGarantiaDS_38_Tfcontratogarantia_dataencerramento ,
                                             DateTime AV152WWContratoGarantiaDS_39_Tfcontratogarantia_dataencerramento_to ,
                                             decimal AV153WWContratoGarantiaDS_40_Tfcontratogarantia_valorencerramento ,
                                             decimal AV154WWContratoGarantiaDS_41_Tfcontratogarantia_valorencerramento_to ,
                                             DateTime A102ContratoGarantia_DataPagtoGarantia ,
                                             DateTime A104ContratoGarantia_DataCaucao ,
                                             DateTime A106ContratoGarantia_DataEncerramento ,
                                             String A77Contrato_Numero ,
                                             String A41Contratada_PessoaNom ,
                                             String A42Contratada_PessoaCNPJ ,
                                             decimal A103ContratoGarantia_Percentual ,
                                             decimal A105ContratoGarantia_ValorGarantia ,
                                             decimal A107ContratoGarantia_ValorEncerramento ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [36] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM ((([ContratoGarantia] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo]) INNER JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T2.[Contratada_Codigo]) INNER JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T3.[Contratada_PessoaCod])";
         if ( ( StringUtil.StrCmp(AV114WWContratoGarantiaDS_1_Dynamicfiltersselector1, "CONTRATOGARANTIA_DATAPAGTOGARANTIA") == 0 ) && ( ! (DateTime.MinValue==AV115WWContratoGarantiaDS_2_Contratogarantia_datapagtogarantia1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataPagtoGarantia] >= @AV115WWContratoGarantiaDS_2_Contratogarantia_datapagtogarantia1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataPagtoGarantia] >= @AV115WWContratoGarantiaDS_2_Contratogarantia_datapagtogarantia1)";
            }
         }
         else
         {
            GXv_int4[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV114WWContratoGarantiaDS_1_Dynamicfiltersselector1, "CONTRATOGARANTIA_DATAPAGTOGARANTIA") == 0 ) && ( ! (DateTime.MinValue==AV116WWContratoGarantiaDS_3_Contratogarantia_datapagtogarantia_to1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataPagtoGarantia] <= @AV116WWContratoGarantiaDS_3_Contratogarantia_datapagtogarantia_to1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataPagtoGarantia] <= @AV116WWContratoGarantiaDS_3_Contratogarantia_datapagtogarantia_to1)";
            }
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV114WWContratoGarantiaDS_1_Dynamicfiltersselector1, "CONTRATOGARANTIA_DATACAUCAO") == 0 ) && ( ! (DateTime.MinValue==AV117WWContratoGarantiaDS_4_Contratogarantia_datacaucao1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataCaucao] >= @AV117WWContratoGarantiaDS_4_Contratogarantia_datacaucao1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataCaucao] >= @AV117WWContratoGarantiaDS_4_Contratogarantia_datacaucao1)";
            }
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV114WWContratoGarantiaDS_1_Dynamicfiltersselector1, "CONTRATOGARANTIA_DATACAUCAO") == 0 ) && ( ! (DateTime.MinValue==AV118WWContratoGarantiaDS_5_Contratogarantia_datacaucao_to1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataCaucao] <= @AV118WWContratoGarantiaDS_5_Contratogarantia_datacaucao_to1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataCaucao] <= @AV118WWContratoGarantiaDS_5_Contratogarantia_datacaucao_to1)";
            }
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV114WWContratoGarantiaDS_1_Dynamicfiltersselector1, "CONTRATOGARANTIA_DATAENCERRAMENTO") == 0 ) && ( ! (DateTime.MinValue==AV119WWContratoGarantiaDS_6_Contratogarantia_dataencerramento1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataEncerramento] >= @AV119WWContratoGarantiaDS_6_Contratogarantia_dataencerramento1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataEncerramento] >= @AV119WWContratoGarantiaDS_6_Contratogarantia_dataencerramento1)";
            }
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV114WWContratoGarantiaDS_1_Dynamicfiltersselector1, "CONTRATOGARANTIA_DATAENCERRAMENTO") == 0 ) && ( ! (DateTime.MinValue==AV120WWContratoGarantiaDS_7_Contratogarantia_dataencerramento_to1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataEncerramento] <= @AV120WWContratoGarantiaDS_7_Contratogarantia_dataencerramento_to1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataEncerramento] <= @AV120WWContratoGarantiaDS_7_Contratogarantia_dataencerramento_to1)";
            }
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( AV121WWContratoGarantiaDS_8_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV122WWContratoGarantiaDS_9_Dynamicfiltersselector2, "CONTRATOGARANTIA_DATAPAGTOGARANTIA") == 0 ) && ( ! (DateTime.MinValue==AV123WWContratoGarantiaDS_10_Contratogarantia_datapagtogarantia2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataPagtoGarantia] >= @AV123WWContratoGarantiaDS_10_Contratogarantia_datapagtogarantia2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataPagtoGarantia] >= @AV123WWContratoGarantiaDS_10_Contratogarantia_datapagtogarantia2)";
            }
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( AV121WWContratoGarantiaDS_8_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV122WWContratoGarantiaDS_9_Dynamicfiltersselector2, "CONTRATOGARANTIA_DATAPAGTOGARANTIA") == 0 ) && ( ! (DateTime.MinValue==AV124WWContratoGarantiaDS_11_Contratogarantia_datapagtogarantia_to2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataPagtoGarantia] <= @AV124WWContratoGarantiaDS_11_Contratogarantia_datapagtogarantia_to2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataPagtoGarantia] <= @AV124WWContratoGarantiaDS_11_Contratogarantia_datapagtogarantia_to2)";
            }
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( AV121WWContratoGarantiaDS_8_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV122WWContratoGarantiaDS_9_Dynamicfiltersselector2, "CONTRATOGARANTIA_DATACAUCAO") == 0 ) && ( ! (DateTime.MinValue==AV125WWContratoGarantiaDS_12_Contratogarantia_datacaucao2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataCaucao] >= @AV125WWContratoGarantiaDS_12_Contratogarantia_datacaucao2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataCaucao] >= @AV125WWContratoGarantiaDS_12_Contratogarantia_datacaucao2)";
            }
         }
         else
         {
            GXv_int4[8] = 1;
         }
         if ( AV121WWContratoGarantiaDS_8_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV122WWContratoGarantiaDS_9_Dynamicfiltersselector2, "CONTRATOGARANTIA_DATACAUCAO") == 0 ) && ( ! (DateTime.MinValue==AV126WWContratoGarantiaDS_13_Contratogarantia_datacaucao_to2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataCaucao] <= @AV126WWContratoGarantiaDS_13_Contratogarantia_datacaucao_to2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataCaucao] <= @AV126WWContratoGarantiaDS_13_Contratogarantia_datacaucao_to2)";
            }
         }
         else
         {
            GXv_int4[9] = 1;
         }
         if ( AV121WWContratoGarantiaDS_8_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV122WWContratoGarantiaDS_9_Dynamicfiltersselector2, "CONTRATOGARANTIA_DATAENCERRAMENTO") == 0 ) && ( ! (DateTime.MinValue==AV127WWContratoGarantiaDS_14_Contratogarantia_dataencerramento2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataEncerramento] >= @AV127WWContratoGarantiaDS_14_Contratogarantia_dataencerramento2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataEncerramento] >= @AV127WWContratoGarantiaDS_14_Contratogarantia_dataencerramento2)";
            }
         }
         else
         {
            GXv_int4[10] = 1;
         }
         if ( AV121WWContratoGarantiaDS_8_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV122WWContratoGarantiaDS_9_Dynamicfiltersselector2, "CONTRATOGARANTIA_DATAENCERRAMENTO") == 0 ) && ( ! (DateTime.MinValue==AV128WWContratoGarantiaDS_15_Contratogarantia_dataencerramento_to2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataEncerramento] <= @AV128WWContratoGarantiaDS_15_Contratogarantia_dataencerramento_to2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataEncerramento] <= @AV128WWContratoGarantiaDS_15_Contratogarantia_dataencerramento_to2)";
            }
         }
         else
         {
            GXv_int4[11] = 1;
         }
         if ( AV129WWContratoGarantiaDS_16_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV130WWContratoGarantiaDS_17_Dynamicfiltersselector3, "CONTRATOGARANTIA_DATAPAGTOGARANTIA") == 0 ) && ( ! (DateTime.MinValue==AV131WWContratoGarantiaDS_18_Contratogarantia_datapagtogarantia3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataPagtoGarantia] >= @AV131WWContratoGarantiaDS_18_Contratogarantia_datapagtogarantia3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataPagtoGarantia] >= @AV131WWContratoGarantiaDS_18_Contratogarantia_datapagtogarantia3)";
            }
         }
         else
         {
            GXv_int4[12] = 1;
         }
         if ( AV129WWContratoGarantiaDS_16_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV130WWContratoGarantiaDS_17_Dynamicfiltersselector3, "CONTRATOGARANTIA_DATAPAGTOGARANTIA") == 0 ) && ( ! (DateTime.MinValue==AV132WWContratoGarantiaDS_19_Contratogarantia_datapagtogarantia_to3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataPagtoGarantia] <= @AV132WWContratoGarantiaDS_19_Contratogarantia_datapagtogarantia_to3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataPagtoGarantia] <= @AV132WWContratoGarantiaDS_19_Contratogarantia_datapagtogarantia_to3)";
            }
         }
         else
         {
            GXv_int4[13] = 1;
         }
         if ( AV129WWContratoGarantiaDS_16_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV130WWContratoGarantiaDS_17_Dynamicfiltersselector3, "CONTRATOGARANTIA_DATACAUCAO") == 0 ) && ( ! (DateTime.MinValue==AV133WWContratoGarantiaDS_20_Contratogarantia_datacaucao3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataCaucao] >= @AV133WWContratoGarantiaDS_20_Contratogarantia_datacaucao3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataCaucao] >= @AV133WWContratoGarantiaDS_20_Contratogarantia_datacaucao3)";
            }
         }
         else
         {
            GXv_int4[14] = 1;
         }
         if ( AV129WWContratoGarantiaDS_16_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV130WWContratoGarantiaDS_17_Dynamicfiltersselector3, "CONTRATOGARANTIA_DATACAUCAO") == 0 ) && ( ! (DateTime.MinValue==AV134WWContratoGarantiaDS_21_Contratogarantia_datacaucao_to3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataCaucao] <= @AV134WWContratoGarantiaDS_21_Contratogarantia_datacaucao_to3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataCaucao] <= @AV134WWContratoGarantiaDS_21_Contratogarantia_datacaucao_to3)";
            }
         }
         else
         {
            GXv_int4[15] = 1;
         }
         if ( AV129WWContratoGarantiaDS_16_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV130WWContratoGarantiaDS_17_Dynamicfiltersselector3, "CONTRATOGARANTIA_DATAENCERRAMENTO") == 0 ) && ( ! (DateTime.MinValue==AV135WWContratoGarantiaDS_22_Contratogarantia_dataencerramento3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataEncerramento] >= @AV135WWContratoGarantiaDS_22_Contratogarantia_dataencerramento3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataEncerramento] >= @AV135WWContratoGarantiaDS_22_Contratogarantia_dataencerramento3)";
            }
         }
         else
         {
            GXv_int4[16] = 1;
         }
         if ( AV129WWContratoGarantiaDS_16_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV130WWContratoGarantiaDS_17_Dynamicfiltersselector3, "CONTRATOGARANTIA_DATAENCERRAMENTO") == 0 ) && ( ! (DateTime.MinValue==AV136WWContratoGarantiaDS_23_Contratogarantia_dataencerramento_to3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataEncerramento] <= @AV136WWContratoGarantiaDS_23_Contratogarantia_dataencerramento_to3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataEncerramento] <= @AV136WWContratoGarantiaDS_23_Contratogarantia_dataencerramento_to3)";
            }
         }
         else
         {
            GXv_int4[17] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV138WWContratoGarantiaDS_25_Tfcontrato_numero_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV137WWContratoGarantiaDS_24_Tfcontrato_numero)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] like @lV137WWContratoGarantiaDS_24_Tfcontrato_numero)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] like @lV137WWContratoGarantiaDS_24_Tfcontrato_numero)";
            }
         }
         else
         {
            GXv_int4[18] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV138WWContratoGarantiaDS_25_Tfcontrato_numero_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] = @AV138WWContratoGarantiaDS_25_Tfcontrato_numero_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] = @AV138WWContratoGarantiaDS_25_Tfcontrato_numero_sel)";
            }
         }
         else
         {
            GXv_int4[19] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV140WWContratoGarantiaDS_27_Tfcontratada_pessoanom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV139WWContratoGarantiaDS_26_Tfcontratada_pessoanom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like @lV139WWContratoGarantiaDS_26_Tfcontratada_pessoanom)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like @lV139WWContratoGarantiaDS_26_Tfcontratada_pessoanom)";
            }
         }
         else
         {
            GXv_int4[20] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV140WWContratoGarantiaDS_27_Tfcontratada_pessoanom_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] = @AV140WWContratoGarantiaDS_27_Tfcontratada_pessoanom_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] = @AV140WWContratoGarantiaDS_27_Tfcontratada_pessoanom_sel)";
            }
         }
         else
         {
            GXv_int4[21] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV142WWContratoGarantiaDS_29_Tfcontratada_pessoacnpj_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV141WWContratoGarantiaDS_28_Tfcontratada_pessoacnpj)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Docto] like @lV141WWContratoGarantiaDS_28_Tfcontratada_pessoacnpj)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Docto] like @lV141WWContratoGarantiaDS_28_Tfcontratada_pessoacnpj)";
            }
         }
         else
         {
            GXv_int4[22] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV142WWContratoGarantiaDS_29_Tfcontratada_pessoacnpj_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Docto] = @AV142WWContratoGarantiaDS_29_Tfcontratada_pessoacnpj_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Docto] = @AV142WWContratoGarantiaDS_29_Tfcontratada_pessoacnpj_sel)";
            }
         }
         else
         {
            GXv_int4[23] = 1;
         }
         if ( ! (DateTime.MinValue==AV143WWContratoGarantiaDS_30_Tfcontratogarantia_datapagtogarantia) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataPagtoGarantia] >= @AV143WWContratoGarantiaDS_30_Tfcontratogarantia_datapagtogarantia)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataPagtoGarantia] >= @AV143WWContratoGarantiaDS_30_Tfcontratogarantia_datapagtogarantia)";
            }
         }
         else
         {
            GXv_int4[24] = 1;
         }
         if ( ! (DateTime.MinValue==AV144WWContratoGarantiaDS_31_Tfcontratogarantia_datapagtogarantia_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataPagtoGarantia] <= @AV144WWContratoGarantiaDS_31_Tfcontratogarantia_datapagtogarantia_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataPagtoGarantia] <= @AV144WWContratoGarantiaDS_31_Tfcontratogarantia_datapagtogarantia_to)";
            }
         }
         else
         {
            GXv_int4[25] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV145WWContratoGarantiaDS_32_Tfcontratogarantia_percentual) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_Percentual] >= @AV145WWContratoGarantiaDS_32_Tfcontratogarantia_percentual)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_Percentual] >= @AV145WWContratoGarantiaDS_32_Tfcontratogarantia_percentual)";
            }
         }
         else
         {
            GXv_int4[26] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV146WWContratoGarantiaDS_33_Tfcontratogarantia_percentual_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_Percentual] <= @AV146WWContratoGarantiaDS_33_Tfcontratogarantia_percentual_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_Percentual] <= @AV146WWContratoGarantiaDS_33_Tfcontratogarantia_percentual_to)";
            }
         }
         else
         {
            GXv_int4[27] = 1;
         }
         if ( ! (DateTime.MinValue==AV147WWContratoGarantiaDS_34_Tfcontratogarantia_datacaucao) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataCaucao] >= @AV147WWContratoGarantiaDS_34_Tfcontratogarantia_datacaucao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataCaucao] >= @AV147WWContratoGarantiaDS_34_Tfcontratogarantia_datacaucao)";
            }
         }
         else
         {
            GXv_int4[28] = 1;
         }
         if ( ! (DateTime.MinValue==AV148WWContratoGarantiaDS_35_Tfcontratogarantia_datacaucao_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataCaucao] <= @AV148WWContratoGarantiaDS_35_Tfcontratogarantia_datacaucao_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataCaucao] <= @AV148WWContratoGarantiaDS_35_Tfcontratogarantia_datacaucao_to)";
            }
         }
         else
         {
            GXv_int4[29] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV149WWContratoGarantiaDS_36_Tfcontratogarantia_valorgarantia) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_ValorGarantia] >= @AV149WWContratoGarantiaDS_36_Tfcontratogarantia_valorgarantia)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_ValorGarantia] >= @AV149WWContratoGarantiaDS_36_Tfcontratogarantia_valorgarantia)";
            }
         }
         else
         {
            GXv_int4[30] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV150WWContratoGarantiaDS_37_Tfcontratogarantia_valorgarantia_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_ValorGarantia] <= @AV150WWContratoGarantiaDS_37_Tfcontratogarantia_valorgarantia_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_ValorGarantia] <= @AV150WWContratoGarantiaDS_37_Tfcontratogarantia_valorgarantia_to)";
            }
         }
         else
         {
            GXv_int4[31] = 1;
         }
         if ( ! (DateTime.MinValue==AV151WWContratoGarantiaDS_38_Tfcontratogarantia_dataencerramento) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataEncerramento] >= @AV151WWContratoGarantiaDS_38_Tfcontratogarantia_dataencerramento)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataEncerramento] >= @AV151WWContratoGarantiaDS_38_Tfcontratogarantia_dataencerramento)";
            }
         }
         else
         {
            GXv_int4[32] = 1;
         }
         if ( ! (DateTime.MinValue==AV152WWContratoGarantiaDS_39_Tfcontratogarantia_dataencerramento_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataEncerramento] <= @AV152WWContratoGarantiaDS_39_Tfcontratogarantia_dataencerramento_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataEncerramento] <= @AV152WWContratoGarantiaDS_39_Tfcontratogarantia_dataencerramento_to)";
            }
         }
         else
         {
            GXv_int4[33] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV153WWContratoGarantiaDS_40_Tfcontratogarantia_valorencerramento) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_ValorEncerramento] >= @AV153WWContratoGarantiaDS_40_Tfcontratogarantia_valorencerramento)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_ValorEncerramento] >= @AV153WWContratoGarantiaDS_40_Tfcontratogarantia_valorencerramento)";
            }
         }
         else
         {
            GXv_int4[34] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV154WWContratoGarantiaDS_41_Tfcontratogarantia_valorencerramento_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_ValorEncerramento] <= @AV154WWContratoGarantiaDS_41_Tfcontratogarantia_valorencerramento_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_ValorEncerramento] <= @AV154WWContratoGarantiaDS_41_Tfcontratogarantia_valorencerramento_to)";
            }
         }
         else
         {
            GXv_int4[35] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 6 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 6 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 7 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 7 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 8 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 8 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 9 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 9 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H006H2(context, (String)dynConstraints[0] , (DateTime)dynConstraints[1] , (DateTime)dynConstraints[2] , (DateTime)dynConstraints[3] , (DateTime)dynConstraints[4] , (DateTime)dynConstraints[5] , (DateTime)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (DateTime)dynConstraints[9] , (DateTime)dynConstraints[10] , (DateTime)dynConstraints[11] , (DateTime)dynConstraints[12] , (DateTime)dynConstraints[13] , (DateTime)dynConstraints[14] , (bool)dynConstraints[15] , (String)dynConstraints[16] , (DateTime)dynConstraints[17] , (DateTime)dynConstraints[18] , (DateTime)dynConstraints[19] , (DateTime)dynConstraints[20] , (DateTime)dynConstraints[21] , (DateTime)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (String)dynConstraints[28] , (DateTime)dynConstraints[29] , (DateTime)dynConstraints[30] , (decimal)dynConstraints[31] , (decimal)dynConstraints[32] , (DateTime)dynConstraints[33] , (DateTime)dynConstraints[34] , (decimal)dynConstraints[35] , (decimal)dynConstraints[36] , (DateTime)dynConstraints[37] , (DateTime)dynConstraints[38] , (decimal)dynConstraints[39] , (decimal)dynConstraints[40] , (DateTime)dynConstraints[41] , (DateTime)dynConstraints[42] , (DateTime)dynConstraints[43] , (String)dynConstraints[44] , (String)dynConstraints[45] , (String)dynConstraints[46] , (decimal)dynConstraints[47] , (decimal)dynConstraints[48] , (decimal)dynConstraints[49] , (short)dynConstraints[50] , (bool)dynConstraints[51] );
               case 1 :
                     return conditional_H006H3(context, (String)dynConstraints[0] , (DateTime)dynConstraints[1] , (DateTime)dynConstraints[2] , (DateTime)dynConstraints[3] , (DateTime)dynConstraints[4] , (DateTime)dynConstraints[5] , (DateTime)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (DateTime)dynConstraints[9] , (DateTime)dynConstraints[10] , (DateTime)dynConstraints[11] , (DateTime)dynConstraints[12] , (DateTime)dynConstraints[13] , (DateTime)dynConstraints[14] , (bool)dynConstraints[15] , (String)dynConstraints[16] , (DateTime)dynConstraints[17] , (DateTime)dynConstraints[18] , (DateTime)dynConstraints[19] , (DateTime)dynConstraints[20] , (DateTime)dynConstraints[21] , (DateTime)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (String)dynConstraints[28] , (DateTime)dynConstraints[29] , (DateTime)dynConstraints[30] , (decimal)dynConstraints[31] , (decimal)dynConstraints[32] , (DateTime)dynConstraints[33] , (DateTime)dynConstraints[34] , (decimal)dynConstraints[35] , (decimal)dynConstraints[36] , (DateTime)dynConstraints[37] , (DateTime)dynConstraints[38] , (decimal)dynConstraints[39] , (decimal)dynConstraints[40] , (DateTime)dynConstraints[41] , (DateTime)dynConstraints[42] , (DateTime)dynConstraints[43] , (String)dynConstraints[44] , (String)dynConstraints[45] , (String)dynConstraints[46] , (decimal)dynConstraints[47] , (decimal)dynConstraints[48] , (decimal)dynConstraints[49] , (short)dynConstraints[50] , (bool)dynConstraints[51] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH006H2 ;
          prmH006H2 = new Object[] {
          new Object[] {"@AV115WWContratoGarantiaDS_2_Contratogarantia_datapagtogarantia1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV116WWContratoGarantiaDS_3_Contratogarantia_datapagtogarantia_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV117WWContratoGarantiaDS_4_Contratogarantia_datacaucao1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV118WWContratoGarantiaDS_5_Contratogarantia_datacaucao_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV119WWContratoGarantiaDS_6_Contratogarantia_dataencerramento1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV120WWContratoGarantiaDS_7_Contratogarantia_dataencerramento_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV123WWContratoGarantiaDS_10_Contratogarantia_datapagtogarantia2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV124WWContratoGarantiaDS_11_Contratogarantia_datapagtogarantia_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV125WWContratoGarantiaDS_12_Contratogarantia_datacaucao2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV126WWContratoGarantiaDS_13_Contratogarantia_datacaucao_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV127WWContratoGarantiaDS_14_Contratogarantia_dataencerramento2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV128WWContratoGarantiaDS_15_Contratogarantia_dataencerramento_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV131WWContratoGarantiaDS_18_Contratogarantia_datapagtogarantia3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV132WWContratoGarantiaDS_19_Contratogarantia_datapagtogarantia_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV133WWContratoGarantiaDS_20_Contratogarantia_datacaucao3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV134WWContratoGarantiaDS_21_Contratogarantia_datacaucao_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV135WWContratoGarantiaDS_22_Contratogarantia_dataencerramento3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV136WWContratoGarantiaDS_23_Contratogarantia_dataencerramento_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV137WWContratoGarantiaDS_24_Tfcontrato_numero",SqlDbType.Char,20,0} ,
          new Object[] {"@AV138WWContratoGarantiaDS_25_Tfcontrato_numero_sel",SqlDbType.Char,20,0} ,
          new Object[] {"@lV139WWContratoGarantiaDS_26_Tfcontratada_pessoanom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV140WWContratoGarantiaDS_27_Tfcontratada_pessoanom_sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV141WWContratoGarantiaDS_28_Tfcontratada_pessoacnpj",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV142WWContratoGarantiaDS_29_Tfcontratada_pessoacnpj_sel",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV143WWContratoGarantiaDS_30_Tfcontratogarantia_datapagtogarantia",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV144WWContratoGarantiaDS_31_Tfcontratogarantia_datapagtogarantia_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV145WWContratoGarantiaDS_32_Tfcontratogarantia_percentual",SqlDbType.Decimal,6,2} ,
          new Object[] {"@AV146WWContratoGarantiaDS_33_Tfcontratogarantia_percentual_to",SqlDbType.Decimal,6,2} ,
          new Object[] {"@AV147WWContratoGarantiaDS_34_Tfcontratogarantia_datacaucao",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV148WWContratoGarantiaDS_35_Tfcontratogarantia_datacaucao_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV149WWContratoGarantiaDS_36_Tfcontratogarantia_valorgarantia",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV150WWContratoGarantiaDS_37_Tfcontratogarantia_valorgarantia_to",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV151WWContratoGarantiaDS_38_Tfcontratogarantia_dataencerramento",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV152WWContratoGarantiaDS_39_Tfcontratogarantia_dataencerramento_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV153WWContratoGarantiaDS_40_Tfcontratogarantia_valorencerramento",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV154WWContratoGarantiaDS_41_Tfcontratogarantia_valorencerramento_to",SqlDbType.Decimal,18,5} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH006H3 ;
          prmH006H3 = new Object[] {
          new Object[] {"@AV115WWContratoGarantiaDS_2_Contratogarantia_datapagtogarantia1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV116WWContratoGarantiaDS_3_Contratogarantia_datapagtogarantia_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV117WWContratoGarantiaDS_4_Contratogarantia_datacaucao1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV118WWContratoGarantiaDS_5_Contratogarantia_datacaucao_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV119WWContratoGarantiaDS_6_Contratogarantia_dataencerramento1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV120WWContratoGarantiaDS_7_Contratogarantia_dataencerramento_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV123WWContratoGarantiaDS_10_Contratogarantia_datapagtogarantia2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV124WWContratoGarantiaDS_11_Contratogarantia_datapagtogarantia_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV125WWContratoGarantiaDS_12_Contratogarantia_datacaucao2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV126WWContratoGarantiaDS_13_Contratogarantia_datacaucao_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV127WWContratoGarantiaDS_14_Contratogarantia_dataencerramento2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV128WWContratoGarantiaDS_15_Contratogarantia_dataencerramento_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV131WWContratoGarantiaDS_18_Contratogarantia_datapagtogarantia3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV132WWContratoGarantiaDS_19_Contratogarantia_datapagtogarantia_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV133WWContratoGarantiaDS_20_Contratogarantia_datacaucao3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV134WWContratoGarantiaDS_21_Contratogarantia_datacaucao_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV135WWContratoGarantiaDS_22_Contratogarantia_dataencerramento3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV136WWContratoGarantiaDS_23_Contratogarantia_dataencerramento_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV137WWContratoGarantiaDS_24_Tfcontrato_numero",SqlDbType.Char,20,0} ,
          new Object[] {"@AV138WWContratoGarantiaDS_25_Tfcontrato_numero_sel",SqlDbType.Char,20,0} ,
          new Object[] {"@lV139WWContratoGarantiaDS_26_Tfcontratada_pessoanom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV140WWContratoGarantiaDS_27_Tfcontratada_pessoanom_sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV141WWContratoGarantiaDS_28_Tfcontratada_pessoacnpj",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV142WWContratoGarantiaDS_29_Tfcontratada_pessoacnpj_sel",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV143WWContratoGarantiaDS_30_Tfcontratogarantia_datapagtogarantia",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV144WWContratoGarantiaDS_31_Tfcontratogarantia_datapagtogarantia_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV145WWContratoGarantiaDS_32_Tfcontratogarantia_percentual",SqlDbType.Decimal,6,2} ,
          new Object[] {"@AV146WWContratoGarantiaDS_33_Tfcontratogarantia_percentual_to",SqlDbType.Decimal,6,2} ,
          new Object[] {"@AV147WWContratoGarantiaDS_34_Tfcontratogarantia_datacaucao",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV148WWContratoGarantiaDS_35_Tfcontratogarantia_datacaucao_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV149WWContratoGarantiaDS_36_Tfcontratogarantia_valorgarantia",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV150WWContratoGarantiaDS_37_Tfcontratogarantia_valorgarantia_to",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV151WWContratoGarantiaDS_38_Tfcontratogarantia_dataencerramento",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV152WWContratoGarantiaDS_39_Tfcontratogarantia_dataencerramento_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV153WWContratoGarantiaDS_40_Tfcontratogarantia_valorencerramento",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV154WWContratoGarantiaDS_41_Tfcontratogarantia_valorencerramento_to",SqlDbType.Decimal,18,5}
          } ;
          def= new CursorDef[] {
              new CursorDef("H006H2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH006H2,11,0,true,false )
             ,new CursorDef("H006H3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH006H3,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((decimal[]) buf[4])[0] = rslt.getDecimal(5) ;
                ((DateTime[]) buf[5])[0] = rslt.getGXDate(6) ;
                ((decimal[]) buf[6])[0] = rslt.getDecimal(7) ;
                ((DateTime[]) buf[7])[0] = rslt.getGXDate(8) ;
                ((decimal[]) buf[8])[0] = rslt.getDecimal(9) ;
                ((DateTime[]) buf[9])[0] = rslt.getGXDate(10) ;
                ((String[]) buf[10])[0] = rslt.getVarchar(11) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(11);
                ((String[]) buf[12])[0] = rslt.getString(12, 100) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(12);
                ((String[]) buf[14])[0] = rslt.getString(13, 20) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[41]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[42]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[43]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[44]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[45]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[46]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[47]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[48]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[49]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[50]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[51]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[52]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[53]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[54]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[55]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[56]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[57]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[58]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[59]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[60]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[61]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[62]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[63]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[64]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[65]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[66]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[67]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[68]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[69]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[70]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[71]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[72]);
                }
                if ( (short)parms[32] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[73]);
                }
                if ( (short)parms[33] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[74]);
                }
                if ( (short)parms[34] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[75]);
                }
                if ( (short)parms[35] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[76]);
                }
                if ( (short)parms[36] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[77]);
                }
                if ( (short)parms[37] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[78]);
                }
                if ( (short)parms[38] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[79]);
                }
                if ( (short)parms[39] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[80]);
                }
                if ( (short)parms[40] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[81]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[36]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[37]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[38]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[39]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[40]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[41]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[42]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[43]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[44]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[45]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[46]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[47]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[48]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[49]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[50]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[51]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[52]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[53]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[54]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[55]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[56]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[57]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[58]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[59]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[60]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[61]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[62]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[63]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[64]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[65]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[66]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[67]);
                }
                if ( (short)parms[32] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[68]);
                }
                if ( (short)parms[33] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[69]);
                }
                if ( (short)parms[34] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[70]);
                }
                if ( (short)parms[35] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[71]);
                }
                return;
       }
    }

 }

}
