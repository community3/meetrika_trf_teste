/*
               File: GetWWContagemResultadoContagensFilterData
        Description: Get WWContagem Resultado Contagens Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/15/2020 22:34:15.39
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getwwcontagemresultadocontagensfilterdata : GXProcedure
   {
      public getwwcontagemresultadocontagensfilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getwwcontagemresultadocontagensfilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV38DDOName = aP0_DDOName;
         this.AV36SearchTxt = aP1_SearchTxt;
         this.AV37SearchTxtTo = aP2_SearchTxtTo;
         this.AV42OptionsJson = "" ;
         this.AV45OptionsDescJson = "" ;
         this.AV47OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV42OptionsJson;
         aP4_OptionsDescJson=this.AV45OptionsDescJson;
         aP5_OptionIndexesJson=this.AV47OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV38DDOName = aP0_DDOName;
         this.AV36SearchTxt = aP1_SearchTxt;
         this.AV37SearchTxtTo = aP2_SearchTxtTo;
         this.AV42OptionsJson = "" ;
         this.AV45OptionsDescJson = "" ;
         this.AV47OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV42OptionsJson;
         aP4_OptionsDescJson=this.AV45OptionsDescJson;
         aP5_OptionIndexesJson=this.AV47OptionIndexesJson;
         return AV47OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getwwcontagemresultadocontagensfilterdata objgetwwcontagemresultadocontagensfilterdata;
         objgetwwcontagemresultadocontagensfilterdata = new getwwcontagemresultadocontagensfilterdata();
         objgetwwcontagemresultadocontagensfilterdata.AV38DDOName = aP0_DDOName;
         objgetwwcontagemresultadocontagensfilterdata.AV36SearchTxt = aP1_SearchTxt;
         objgetwwcontagemresultadocontagensfilterdata.AV37SearchTxtTo = aP2_SearchTxtTo;
         objgetwwcontagemresultadocontagensfilterdata.AV42OptionsJson = "" ;
         objgetwwcontagemresultadocontagensfilterdata.AV45OptionsDescJson = "" ;
         objgetwwcontagemresultadocontagensfilterdata.AV47OptionIndexesJson = "" ;
         objgetwwcontagemresultadocontagensfilterdata.context.SetSubmitInitialConfig(context);
         objgetwwcontagemresultadocontagensfilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetwwcontagemresultadocontagensfilterdata);
         aP3_OptionsJson=this.AV42OptionsJson;
         aP4_OptionsDescJson=this.AV45OptionsDescJson;
         aP5_OptionIndexesJson=this.AV47OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getwwcontagemresultadocontagensfilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV42OptionsJson = "";
         AV45OptionsDescJson = "";
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private String AV47OptionIndexesJson ;
      private String AV42OptionsJson ;
      private String AV45OptionsDescJson ;
      private String AV38DDOName ;
      private String AV36SearchTxt ;
      private String AV37SearchTxtTo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
   }

   [ServiceContract(Namespace = "GeneXus.Programs.getwwcontagemresultadocontagensfilterdata_services")]
   [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
   [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
   public class getwwcontagemresultadocontagensfilterdata_services : GxRestService
   {
      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      [OperationContract]
      [WebInvoke(Method =  "POST" ,
      	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
      	ResponseFormat = WebMessageFormat.Json,
      	UriTemplate = "/")]
      public void execute( String DDOName ,
                           String SearchTxt ,
                           String SearchTxtTo ,
                           out String OptionsJson ,
                           out String OptionsDescJson ,
                           out String OptionIndexesJson )
      {
         OptionsJson = "" ;
         OptionsDescJson = "" ;
         OptionIndexesJson = "" ;
         try
         {
            permissionPrefix = "";
            if ( ! IsAuthenticated() )
            {
               return  ;
            }
            if ( ! ProcessHeaders("getwwcontagemresultadocontagensfilterdata") )
            {
               return  ;
            }
            getwwcontagemresultadocontagensfilterdata worker = new getwwcontagemresultadocontagensfilterdata(context) ;
            worker.IsMain = RunAsMain ;
            worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
            worker.cleanup( );
         }
         catch ( Exception e )
         {
            WebException(e);
         }
         finally
         {
            Cleanup();
         }
      }

   }

}
