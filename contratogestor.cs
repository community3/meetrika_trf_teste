/*
               File: ContratoGestor
        Description: Gestor do Contrato
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/29/2020 9:31:1.46
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contratogestor : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxAggSel2"+"_"+"CONTRATOGESTOR_USUARIOEHCONTRATANTE") == 0 )
         {
            A1446ContratoGestor_ContratadaAreaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n1446ContratoGestor_ContratadaAreaCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1446ContratoGestor_ContratadaAreaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1446ContratoGestor_ContratadaAreaCod), 6, 0)));
            A1079ContratoGestor_UsuarioCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1079ContratoGestor_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1079ContratoGestor_UsuarioCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GX2ASACONTRATOGESTOR_USUARIOEHCONTRATANTE35129( A1446ContratoGestor_ContratadaAreaCod, A1079ContratoGestor_UsuarioCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_4") == 0 )
         {
            A1078ContratoGestor_ContratoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1078ContratoGestor_ContratoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1078ContratoGestor_ContratoCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_4( A1078ContratoGestor_ContratoCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_6") == 0 )
         {
            A1136ContratoGestor_ContratadaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n1136ContratoGestor_ContratadaCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1136ContratoGestor_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1136ContratoGestor_ContratadaCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_6( A1136ContratoGestor_ContratadaCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_7") == 0 )
         {
            A1446ContratoGestor_ContratadaAreaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n1446ContratoGestor_ContratadaAreaCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1446ContratoGestor_ContratadaAreaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1446ContratoGestor_ContratadaAreaCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_7( A1446ContratoGestor_ContratadaAreaCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_5") == 0 )
         {
            A1079ContratoGestor_UsuarioCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1079ContratoGestor_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1079ContratoGestor_UsuarioCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_5( A1079ContratoGestor_UsuarioCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_8") == 0 )
         {
            A1080ContratoGestor_UsuarioPesCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n1080ContratoGestor_UsuarioPesCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1080ContratoGestor_UsuarioPesCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1080ContratoGestor_UsuarioPesCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_8( A1080ContratoGestor_UsuarioPesCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         cmbContratoGestor_ContratadaTipo.Name = "CONTRATOGESTOR_CONTRATADATIPO";
         cmbContratoGestor_ContratadaTipo.WebTags = "";
         cmbContratoGestor_ContratadaTipo.addItem("", "(Nenhuma)", 0);
         cmbContratoGestor_ContratadaTipo.addItem("S", "F�brica de Software", 0);
         cmbContratoGestor_ContratadaTipo.addItem("M", "F�brica de M�trica", 0);
         cmbContratoGestor_ContratadaTipo.addItem("I", "Departamento / Setor", 0);
         cmbContratoGestor_ContratadaTipo.addItem("O", "Outras", 0);
         if ( cmbContratoGestor_ContratadaTipo.ItemCount > 0 )
         {
            A1137ContratoGestor_ContratadaTipo = cmbContratoGestor_ContratadaTipo.getValidValue(A1137ContratoGestor_ContratadaTipo);
            n1137ContratoGestor_ContratadaTipo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1137ContratoGestor_ContratadaTipo", A1137ContratoGestor_ContratadaTipo);
         }
         chkContratoGestor_UsuarioEhContratante.Name = "CONTRATOGESTOR_USUARIOEHCONTRATANTE";
         chkContratoGestor_UsuarioEhContratante.WebTags = "";
         chkContratoGestor_UsuarioEhContratante.Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkContratoGestor_UsuarioEhContratante_Internalname, "TitleCaption", chkContratoGestor_UsuarioEhContratante.Caption);
         chkContratoGestor_UsuarioEhContratante.CheckedValue = "false";
         chkContratoGestor_UsuarioAtv.Name = "CONTRATOGESTOR_USUARIOATV";
         chkContratoGestor_UsuarioAtv.WebTags = "";
         chkContratoGestor_UsuarioAtv.Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkContratoGestor_UsuarioAtv_Internalname, "TitleCaption", chkContratoGestor_UsuarioAtv.Caption);
         chkContratoGestor_UsuarioAtv.CheckedValue = "false";
         cmbContratoGestor_Tipo.Name = "CONTRATOGESTOR_TIPO";
         cmbContratoGestor_Tipo.WebTags = "";
         cmbContratoGestor_Tipo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 1, 0)), "N�o informado", 0);
         cmbContratoGestor_Tipo.addItem("1", "Titular", 0);
         cmbContratoGestor_Tipo.addItem("2", "Substituto", 0);
         if ( cmbContratoGestor_Tipo.ItemCount > 0 )
         {
            A2009ContratoGestor_Tipo = (short)(NumberUtil.Val( cmbContratoGestor_Tipo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A2009ContratoGestor_Tipo), 1, 0))), "."));
            n2009ContratoGestor_Tipo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2009ContratoGestor_Tipo", StringUtil.Str( (decimal)(A2009ContratoGestor_Tipo), 1, 0));
         }
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Gestor do Contrato", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtContratoGestor_ContratoCod_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public contratogestor( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public contratogestor( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbContratoGestor_ContratadaTipo = new GXCombobox();
         chkContratoGestor_UsuarioEhContratante = new GXCheckbox();
         chkContratoGestor_UsuarioAtv = new GXCheckbox();
         cmbContratoGestor_Tipo = new GXCombobox();
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbContratoGestor_ContratadaTipo.ItemCount > 0 )
         {
            A1137ContratoGestor_ContratadaTipo = cmbContratoGestor_ContratadaTipo.getValidValue(A1137ContratoGestor_ContratadaTipo);
            n1137ContratoGestor_ContratadaTipo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1137ContratoGestor_ContratadaTipo", A1137ContratoGestor_ContratadaTipo);
         }
         if ( cmbContratoGestor_Tipo.ItemCount > 0 )
         {
            A2009ContratoGestor_Tipo = (short)(NumberUtil.Val( cmbContratoGestor_Tipo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A2009ContratoGestor_Tipo), 1, 0))), "."));
            n2009ContratoGestor_Tipo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2009ContratoGestor_Tipo", StringUtil.Str( (decimal)(A2009ContratoGestor_Tipo), 1, 0));
         }
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_35129( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_35129e( bool wbgen )
      {
         if ( wbgen )
         {
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_35129( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableBorder100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_35129( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_35129e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Control Group */
            GxWebStd.gx_group_start( context, grpGroupdata_Internalname, "Gestor do Contrato", 1, 0, "px", 0, "px", "Group", " "+"style=\"-moz-border-radius: 3pt;;\""+" ", "HLP_ContratoGestor.htm");
            wb_table3_28_35129( true) ;
         }
         return  ;
      }

      protected void wb_table3_28_35129e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</fieldset>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_35129e( true) ;
         }
         else
         {
            wb_table1_2_35129e( false) ;
         }
      }

      protected void wb_table3_28_35129( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_34_35129( true) ;
         }
         return  ;
      }

      protected void wb_table4_34_35129e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 97,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_enter_Internalname, "", "Confirmar", bttBtn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_enter_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContratoGestor.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 98,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_cancel_Internalname, "", "Fechar", bttBtn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContratoGestor.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 99,'',false,'',0)\"";
            ClassString = "BtnDelete";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_delete_Internalname, "", "Eliminar", bttBtn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_delete_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContratoGestor.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_28_35129e( true) ;
         }
         else
         {
            wb_table3_28_35129e( false) ;
         }
      }

      protected void wb_table4_34_35129( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable2_Internalname, tblTable2_Internalname, "", "Container", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratogestor_contratocod_Internalname, "Contrato", "", "", lblTextblockcontratogestor_contratocod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContratoGestor.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContratoGestor_ContratoCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1078ContratoGestor_ContratoCod), 6, 0, ",", "")), ((edtContratoGestor_ContratoCod_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1078ContratoGestor_ContratoCod), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A1078ContratoGestor_ContratoCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,39);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoGestor_ContratoCod_Jsonclick, 0, "Attribute", "", "", "", 1, edtContratoGestor_ContratoCod_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContratoGestor.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratogestor_contratadacod_Internalname, "Nome", "", "", lblTextblockcontratogestor_contratadacod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContratoGestor.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratoGestor_ContratadaCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1136ContratoGestor_ContratadaCod), 6, 0, ",", "")), ((edtContratoGestor_ContratadaCod_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1136ContratoGestor_ContratadaCod), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A1136ContratoGestor_ContratadaCod), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoGestor_ContratadaCod_Jsonclick, 0, "Attribute", "", "", "", 1, edtContratoGestor_ContratadaCod_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoGestor.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratogestor_contratadasigla_Internalname, "Contratada", "", "", lblTextblockcontratogestor_contratadasigla_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContratoGestor.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratoGestor_ContratadaSigla_Internalname, StringUtil.RTrim( A1223ContratoGestor_ContratadaSigla), StringUtil.RTrim( context.localUtil.Format( A1223ContratoGestor_ContratadaSigla, "@!")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoGestor_ContratadaSigla_Jsonclick, 0, "Attribute", "", "", "", 1, edtContratoGestor_ContratadaSigla_Enabled, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "Sigla", "left", true, "HLP_ContratoGestor.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratogestor_contratadatipo_Internalname, "de F�brica", "", "", lblTextblockcontratogestor_contratadatipo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContratoGestor.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContratoGestor_ContratadaTipo, cmbContratoGestor_ContratadaTipo_Internalname, StringUtil.RTrim( A1137ContratoGestor_ContratadaTipo), 1, cmbContratoGestor_ContratadaTipo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, cmbContratoGestor_ContratadaTipo.Enabled, 1, 0, 0, "em", 0, "", "", "Attribute", "", "", "", true, "HLP_ContratoGestor.htm");
            cmbContratoGestor_ContratadaTipo.CurrentValue = StringUtil.RTrim( A1137ContratoGestor_ContratadaTipo);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratoGestor_ContratadaTipo_Internalname, "Values", (String)(cmbContratoGestor_ContratadaTipo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratogestor_contratadaareacod_Internalname, "de Trabalho", "", "", lblTextblockcontratogestor_contratadaareacod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContratoGestor.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratoGestor_ContratadaAreaCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1446ContratoGestor_ContratadaAreaCod), 6, 0, ",", "")), ((edtContratoGestor_ContratadaAreaCod_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1446ContratoGestor_ContratadaAreaCod), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A1446ContratoGestor_ContratadaAreaCod), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoGestor_ContratadaAreaCod_Jsonclick, 0, "Attribute", "", "", "", 1, edtContratoGestor_ContratadaAreaCod_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContratoGestor.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratogestor_contratadaareades_Internalname, "de Trabalho", "", "", lblTextblockcontratogestor_contratadaareades_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContratoGestor.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratoGestor_ContratadaAreaDes_Internalname, A1447ContratoGestor_ContratadaAreaDes, StringUtil.RTrim( context.localUtil.Format( A1447ContratoGestor_ContratadaAreaDes, "@!")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoGestor_ContratadaAreaDes_Jsonclick, 0, "Attribute", "", "", "", 1, edtContratoGestor_ContratadaAreaDes_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Descricao", "left", true, "HLP_ContratoGestor.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratogestor_usuariocod_Internalname, "Gestor", "", "", lblTextblockcontratogestor_usuariocod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContratoGestor.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContratoGestor_UsuarioCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1079ContratoGestor_UsuarioCod), 6, 0, ",", "")), ((edtContratoGestor_UsuarioCod_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1079ContratoGestor_UsuarioCod), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A1079ContratoGestor_UsuarioCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,69);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoGestor_UsuarioCod_Jsonclick, 0, "Attribute", "", "", "", 1, edtContratoGestor_UsuarioCod_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContratoGestor.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratogestor_usuariopescod_Internalname, "Gestor", "", "", lblTextblockcontratogestor_usuariopescod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContratoGestor.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratoGestor_UsuarioPesCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1080ContratoGestor_UsuarioPesCod), 6, 0, ",", "")), ((edtContratoGestor_UsuarioPesCod_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1080ContratoGestor_UsuarioPesCod), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A1080ContratoGestor_UsuarioPesCod), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoGestor_UsuarioPesCod_Jsonclick, 0, "Attribute", "", "", "", 1, edtContratoGestor_UsuarioPesCod_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContratoGestor.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratogestor_usuariopesnom_Internalname, "Gestor", "", "", lblTextblockcontratogestor_usuariopesnom_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContratoGestor.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratoGestor_UsuarioPesNom_Internalname, StringUtil.RTrim( A1081ContratoGestor_UsuarioPesNom), StringUtil.RTrim( context.localUtil.Format( A1081ContratoGestor_UsuarioPesNom, "@!")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoGestor_UsuarioPesNom_Jsonclick, 0, "BootstrapAttribute100", "", "", "", 1, edtContratoGestor_UsuarioPesNom_Enabled, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "Nome100", "left", true, "HLP_ContratoGestor.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratogestor_usuarioehcontratante_Internalname, "Eh Contratante", "", "", lblTextblockcontratogestor_usuarioehcontratante_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContratoGestor.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Check box */
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkContratoGestor_UsuarioEhContratante_Internalname, StringUtil.BoolToStr( A1135ContratoGestor_UsuarioEhContratante), "", "", 1, chkContratoGestor_UsuarioEhContratante.Enabled, "true", "", StyleString, ClassString, "", "");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratogestor_usuarioatv_Internalname, "ativo", "", "", lblTextblockcontratogestor_usuarioatv_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContratoGestor.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Check box */
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkContratoGestor_UsuarioAtv_Internalname, StringUtil.BoolToStr( A2033ContratoGestor_UsuarioAtv), "", "", 1, chkContratoGestor_UsuarioAtv.Enabled, "true", "", StyleString, ClassString, "", "");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratogestor_tipo_Internalname, "Tipo", "", "", lblTextblockcontratogestor_tipo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContratoGestor.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 94,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContratoGestor_Tipo, cmbContratoGestor_Tipo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A2009ContratoGestor_Tipo), 1, 0)), 1, cmbContratoGestor_Tipo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, cmbContratoGestor_Tipo.Enabled, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,94);\"", "", true, "HLP_ContratoGestor.htm");
            cmbContratoGestor_Tipo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A2009ContratoGestor_Tipo), 1, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratoGestor_Tipo_Internalname, "Values", (String)(cmbContratoGestor_Tipo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_34_35129e( true) ;
         }
         else
         {
            wb_table4_34_35129e( false) ;
         }
      }

      protected void wb_table2_5_35129( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabletoolbar_Internalname, tblTabletoolbar_Internalname, "", "ViewTable", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divSectiontoolbar_Internalname, 1, 0, "px", 0, "px", "ToolbarMain", "left", "top", "", "WHITE-SPACE: nowrap;", "div");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 9,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_first_Internalname, context.GetImagePath( "b9e06284-17ac-4c88-8937-5dbd84ad5d80", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_first_Visible, 1, "", "Primeiro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_first_Jsonclick, "'"+""+"'"+",false,"+"'"+"EFIRST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContratoGestor.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_first_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_first_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_first_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EFIRST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContratoGestor.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_previous_Internalname, context.GetImagePath( "7d212604-db7b-4785-9c0d-5faffe71aa33", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_previous_Visible, 1, "", "Anterior", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_previous_Jsonclick, "'"+""+"'"+",false,"+"'"+"EPREVIOUS."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContratoGestor.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 12,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_previous_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_previous_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_previous_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EPREVIOUS."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContratoGestor.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 13,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_next_Internalname, context.GetImagePath( "1ae947cf-1354-41a9-8d59-d91daebf554f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_next_Visible, 1, "", "Pr�ximo", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_next_Jsonclick, "'"+""+"'"+",false,"+"'"+"ENEXT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContratoGestor.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 14,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_next_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_next_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_next_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ENEXT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContratoGestor.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 15,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_last_Internalname, context.GetImagePath( "29211874-e613-48e5-9011-8017d984217e", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_last_Visible, 1, "", "�ltimo", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_last_Jsonclick, "'"+""+"'"+",false,"+"'"+"ELAST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContratoGestor.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_last_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_last_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_last_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ELAST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContratoGestor.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_select_Internalname, context.GetImagePath( "1ca03f75-9947-4d2c-90a4-e8ab9c5cedea", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_select_Visible, 1, "", "Selecionar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_select_Jsonclick, "'"+""+"'"+",false,"+"'"+"ESELECT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContratoGestor.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_select_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_select_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_select_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ESELECT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContratoGestor.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_enter2_Internalname, context.GetImagePath( "2061cf2c-bd33-4433-a13e-34af954142e9", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_enter2_Visible, imgBtn_enter2_Enabled, "", "Confirmar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_enter2_Jsonclick, "'"+""+"'"+",false,"+"'"+"EENTER."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContratoGestor.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_enter2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_enter2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_enter2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EENTER."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContratoGestor.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_cancel2_Internalname, context.GetImagePath( "0e94ced8-bc34-47ff-9a53-bc683736a686", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_cancel2_Visible, 1, "", "Fechar", 0, 0, 0, "px", 0, "px", 0, 0, 1, imgBtn_cancel2_Jsonclick, "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContratoGestor.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 22,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_cancel2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_cancel2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 1, imgBtn_cancel2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContratoGestor.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_delete2_Internalname, context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_delete2_Visible, imgBtn_delete2_Enabled, "", "Eliminar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_delete2_Jsonclick, "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContratoGestor.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_delete2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_delete2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_delete2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContratoGestor.htm");
            GxWebStd.gx_div_end( context, "left", "top");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_35129e( true) ;
         }
         else
         {
            wb_table2_5_35129e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               if ( ( ( context.localUtil.CToN( cgiGet( edtContratoGestor_ContratoCod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContratoGestor_ContratoCod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTRATOGESTOR_CONTRATOCOD");
                  AnyError = 1;
                  GX_FocusControl = edtContratoGestor_ContratoCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1078ContratoGestor_ContratoCod = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1078ContratoGestor_ContratoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1078ContratoGestor_ContratoCod), 6, 0)));
               }
               else
               {
                  A1078ContratoGestor_ContratoCod = (int)(context.localUtil.CToN( cgiGet( edtContratoGestor_ContratoCod_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1078ContratoGestor_ContratoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1078ContratoGestor_ContratoCod), 6, 0)));
               }
               A1136ContratoGestor_ContratadaCod = (int)(context.localUtil.CToN( cgiGet( edtContratoGestor_ContratadaCod_Internalname), ",", "."));
               n1136ContratoGestor_ContratadaCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1136ContratoGestor_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1136ContratoGestor_ContratadaCod), 6, 0)));
               A1223ContratoGestor_ContratadaSigla = StringUtil.Upper( cgiGet( edtContratoGestor_ContratadaSigla_Internalname));
               n1223ContratoGestor_ContratadaSigla = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1223ContratoGestor_ContratadaSigla", A1223ContratoGestor_ContratadaSigla);
               cmbContratoGestor_ContratadaTipo.CurrentValue = cgiGet( cmbContratoGestor_ContratadaTipo_Internalname);
               A1137ContratoGestor_ContratadaTipo = cgiGet( cmbContratoGestor_ContratadaTipo_Internalname);
               n1137ContratoGestor_ContratadaTipo = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1137ContratoGestor_ContratadaTipo", A1137ContratoGestor_ContratadaTipo);
               A1446ContratoGestor_ContratadaAreaCod = (int)(context.localUtil.CToN( cgiGet( edtContratoGestor_ContratadaAreaCod_Internalname), ",", "."));
               n1446ContratoGestor_ContratadaAreaCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1446ContratoGestor_ContratadaAreaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1446ContratoGestor_ContratadaAreaCod), 6, 0)));
               A1447ContratoGestor_ContratadaAreaDes = StringUtil.Upper( cgiGet( edtContratoGestor_ContratadaAreaDes_Internalname));
               n1447ContratoGestor_ContratadaAreaDes = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1447ContratoGestor_ContratadaAreaDes", A1447ContratoGestor_ContratadaAreaDes);
               if ( ( ( context.localUtil.CToN( cgiGet( edtContratoGestor_UsuarioCod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContratoGestor_UsuarioCod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTRATOGESTOR_USUARIOCOD");
                  AnyError = 1;
                  GX_FocusControl = edtContratoGestor_UsuarioCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1079ContratoGestor_UsuarioCod = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1079ContratoGestor_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1079ContratoGestor_UsuarioCod), 6, 0)));
               }
               else
               {
                  A1079ContratoGestor_UsuarioCod = (int)(context.localUtil.CToN( cgiGet( edtContratoGestor_UsuarioCod_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1079ContratoGestor_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1079ContratoGestor_UsuarioCod), 6, 0)));
               }
               A1080ContratoGestor_UsuarioPesCod = (int)(context.localUtil.CToN( cgiGet( edtContratoGestor_UsuarioPesCod_Internalname), ",", "."));
               n1080ContratoGestor_UsuarioPesCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1080ContratoGestor_UsuarioPesCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1080ContratoGestor_UsuarioPesCod), 6, 0)));
               A1081ContratoGestor_UsuarioPesNom = StringUtil.Upper( cgiGet( edtContratoGestor_UsuarioPesNom_Internalname));
               n1081ContratoGestor_UsuarioPesNom = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1081ContratoGestor_UsuarioPesNom", A1081ContratoGestor_UsuarioPesNom);
               A1135ContratoGestor_UsuarioEhContratante = StringUtil.StrToBool( cgiGet( chkContratoGestor_UsuarioEhContratante_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1135ContratoGestor_UsuarioEhContratante", A1135ContratoGestor_UsuarioEhContratante);
               A2033ContratoGestor_UsuarioAtv = StringUtil.StrToBool( cgiGet( chkContratoGestor_UsuarioAtv_Internalname));
               n2033ContratoGestor_UsuarioAtv = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2033ContratoGestor_UsuarioAtv", A2033ContratoGestor_UsuarioAtv);
               cmbContratoGestor_Tipo.CurrentValue = cgiGet( cmbContratoGestor_Tipo_Internalname);
               A2009ContratoGestor_Tipo = (short)(NumberUtil.Val( cgiGet( cmbContratoGestor_Tipo_Internalname), "."));
               n2009ContratoGestor_Tipo = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2009ContratoGestor_Tipo", StringUtil.Str( (decimal)(A2009ContratoGestor_Tipo), 1, 0));
               n2009ContratoGestor_Tipo = ((0==A2009ContratoGestor_Tipo) ? true : false);
               /* Read saved values. */
               Z1078ContratoGestor_ContratoCod = (int)(context.localUtil.CToN( cgiGet( "Z1078ContratoGestor_ContratoCod"), ",", "."));
               Z1079ContratoGestor_UsuarioCod = (int)(context.localUtil.CToN( cgiGet( "Z1079ContratoGestor_UsuarioCod"), ",", "."));
               Z2009ContratoGestor_Tipo = (short)(context.localUtil.CToN( cgiGet( "Z2009ContratoGestor_Tipo"), ",", "."));
               n2009ContratoGestor_Tipo = ((0==A2009ContratoGestor_Tipo) ? true : false);
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               Gx_mode = cgiGet( "vMODE");
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  A1078ContratoGestor_ContratoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1078ContratoGestor_ContratoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1078ContratoGestor_ContratoCod), 6, 0)));
                  A1079ContratoGestor_UsuarioCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1079ContratoGestor_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1079ContratoGestor_UsuarioCod), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  disable_std_buttons_dsp( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  standaloneModal( ) ;
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_enter( ) ;
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                        else if ( StringUtil.StrCmp(sEvt, "FIRST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_first( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "PREVIOUS") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_previous( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "NEXT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_next( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LAST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_last( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "SELECT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_select( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DELETE") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_delete( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           AfterKeyLoadScreen( ) ;
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll35129( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            imgBtn_delete2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
      }

      protected void disable_std_buttons_dsp( )
      {
         imgBtn_delete2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Visible), 5, 0)));
         imgBtn_delete2_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_separator_Visible), 5, 0)));
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)));
         imgBtn_first_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_first_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_first_Visible), 5, 0)));
         imgBtn_first_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_first_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_first_separator_Visible), 5, 0)));
         imgBtn_previous_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_previous_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_previous_Visible), 5, 0)));
         imgBtn_previous_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_previous_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_previous_separator_Visible), 5, 0)));
         imgBtn_next_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_next_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_next_Visible), 5, 0)));
         imgBtn_next_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_next_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_next_separator_Visible), 5, 0)));
         imgBtn_last_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_last_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_last_Visible), 5, 0)));
         imgBtn_last_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_last_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_last_separator_Visible), 5, 0)));
         imgBtn_select_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_select_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_select_Visible), 5, 0)));
         imgBtn_select_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_select_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_select_separator_Visible), 5, 0)));
         imgBtn_delete2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Visible), 5, 0)));
         imgBtn_delete2_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_separator_Visible), 5, 0)));
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)));
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            imgBtn_enter2_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Visible), 5, 0)));
            imgBtn_enter2_separator_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_separator_Visible), 5, 0)));
            bttBtn_enter_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_enter_Visible), 5, 0)));
         }
         DisableAttributes35129( ) ;
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void ResetCaption350( )
      {
      }

      protected void ZM35129( short GX_JID )
      {
         if ( ( GX_JID == 3 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z2009ContratoGestor_Tipo = T00353_A2009ContratoGestor_Tipo[0];
            }
            else
            {
               Z2009ContratoGestor_Tipo = A2009ContratoGestor_Tipo;
            }
         }
         if ( GX_JID == -3 )
         {
            Z2009ContratoGestor_Tipo = A2009ContratoGestor_Tipo;
            Z1078ContratoGestor_ContratoCod = A1078ContratoGestor_ContratoCod;
            Z1079ContratoGestor_UsuarioCod = A1079ContratoGestor_UsuarioCod;
            Z1136ContratoGestor_ContratadaCod = A1136ContratoGestor_ContratadaCod;
            Z1446ContratoGestor_ContratadaAreaCod = A1446ContratoGestor_ContratadaAreaCod;
            Z1223ContratoGestor_ContratadaSigla = A1223ContratoGestor_ContratadaSigla;
            Z1137ContratoGestor_ContratadaTipo = A1137ContratoGestor_ContratadaTipo;
            Z1447ContratoGestor_ContratadaAreaDes = A1447ContratoGestor_ContratadaAreaDes;
            Z2033ContratoGestor_UsuarioAtv = A2033ContratoGestor_UsuarioAtv;
            Z1080ContratoGestor_UsuarioPesCod = A1080ContratoGestor_UsuarioPesCod;
            Z1081ContratoGestor_UsuarioPesNom = A1081ContratoGestor_UsuarioPesNom;
         }
      }

      protected void standaloneNotModal( )
      {
      }

      protected void standaloneModal( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            imgBtn_delete2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
         else
         {
            imgBtn_delete2_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            imgBtn_enter2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Enabled), 5, 0)));
         }
         else
         {
            imgBtn_enter2_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Enabled), 5, 0)));
         }
      }

      protected void Load35129( )
      {
         /* Using cursor T00359 */
         pr_default.execute(7, new Object[] {A1078ContratoGestor_ContratoCod, A1079ContratoGestor_UsuarioCod});
         if ( (pr_default.getStatus(7) != 101) )
         {
            RcdFound129 = 1;
            A1223ContratoGestor_ContratadaSigla = T00359_A1223ContratoGestor_ContratadaSigla[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1223ContratoGestor_ContratadaSigla", A1223ContratoGestor_ContratadaSigla);
            n1223ContratoGestor_ContratadaSigla = T00359_n1223ContratoGestor_ContratadaSigla[0];
            A1137ContratoGestor_ContratadaTipo = T00359_A1137ContratoGestor_ContratadaTipo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1137ContratoGestor_ContratadaTipo", A1137ContratoGestor_ContratadaTipo);
            n1137ContratoGestor_ContratadaTipo = T00359_n1137ContratoGestor_ContratadaTipo[0];
            A1447ContratoGestor_ContratadaAreaDes = T00359_A1447ContratoGestor_ContratadaAreaDes[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1447ContratoGestor_ContratadaAreaDes", A1447ContratoGestor_ContratadaAreaDes);
            n1447ContratoGestor_ContratadaAreaDes = T00359_n1447ContratoGestor_ContratadaAreaDes[0];
            A1081ContratoGestor_UsuarioPesNom = T00359_A1081ContratoGestor_UsuarioPesNom[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1081ContratoGestor_UsuarioPesNom", A1081ContratoGestor_UsuarioPesNom);
            n1081ContratoGestor_UsuarioPesNom = T00359_n1081ContratoGestor_UsuarioPesNom[0];
            A2033ContratoGestor_UsuarioAtv = T00359_A2033ContratoGestor_UsuarioAtv[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2033ContratoGestor_UsuarioAtv", A2033ContratoGestor_UsuarioAtv);
            n2033ContratoGestor_UsuarioAtv = T00359_n2033ContratoGestor_UsuarioAtv[0];
            A2009ContratoGestor_Tipo = T00359_A2009ContratoGestor_Tipo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2009ContratoGestor_Tipo", StringUtil.Str( (decimal)(A2009ContratoGestor_Tipo), 1, 0));
            n2009ContratoGestor_Tipo = T00359_n2009ContratoGestor_Tipo[0];
            A1136ContratoGestor_ContratadaCod = T00359_A1136ContratoGestor_ContratadaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1136ContratoGestor_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1136ContratoGestor_ContratadaCod), 6, 0)));
            n1136ContratoGestor_ContratadaCod = T00359_n1136ContratoGestor_ContratadaCod[0];
            A1446ContratoGestor_ContratadaAreaCod = T00359_A1446ContratoGestor_ContratadaAreaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1446ContratoGestor_ContratadaAreaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1446ContratoGestor_ContratadaAreaCod), 6, 0)));
            n1446ContratoGestor_ContratadaAreaCod = T00359_n1446ContratoGestor_ContratadaAreaCod[0];
            A1080ContratoGestor_UsuarioPesCod = T00359_A1080ContratoGestor_UsuarioPesCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1080ContratoGestor_UsuarioPesCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1080ContratoGestor_UsuarioPesCod), 6, 0)));
            n1080ContratoGestor_UsuarioPesCod = T00359_n1080ContratoGestor_UsuarioPesCod[0];
            ZM35129( -3) ;
         }
         pr_default.close(7);
         OnLoadActions35129( ) ;
      }

      protected void OnLoadActions35129( )
      {
         GXt_boolean1 = A1135ContratoGestor_UsuarioEhContratante;
         new prc_usuarioehcontratantedaarea(context ).execute( ref  A1446ContratoGestor_ContratadaAreaCod, ref  A1079ContratoGestor_UsuarioCod, out  GXt_boolean1) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1446ContratoGestor_ContratadaAreaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1446ContratoGestor_ContratadaAreaCod), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1079ContratoGestor_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1079ContratoGestor_UsuarioCod), 6, 0)));
         A1135ContratoGestor_UsuarioEhContratante = GXt_boolean1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1135ContratoGestor_UsuarioEhContratante", A1135ContratoGestor_UsuarioEhContratante);
      }

      protected void CheckExtendedTable35129( )
      {
         Gx_BScreen = 1;
         standaloneModal( ) ;
         /* Using cursor T00354 */
         pr_default.execute(2, new Object[] {A1078ContratoGestor_ContratoCod});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato Gestor_Contrato'.", "ForeignKeyNotFound", 1, "CONTRATOGESTOR_CONTRATOCOD");
            AnyError = 1;
            GX_FocusControl = edtContratoGestor_ContratoCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A1136ContratoGestor_ContratadaCod = T00354_A1136ContratoGestor_ContratadaCod[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1136ContratoGestor_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1136ContratoGestor_ContratadaCod), 6, 0)));
         n1136ContratoGestor_ContratadaCod = T00354_n1136ContratoGestor_ContratadaCod[0];
         A1446ContratoGestor_ContratadaAreaCod = T00354_A1446ContratoGestor_ContratadaAreaCod[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1446ContratoGestor_ContratadaAreaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1446ContratoGestor_ContratadaAreaCod), 6, 0)));
         n1446ContratoGestor_ContratadaAreaCod = T00354_n1446ContratoGestor_ContratadaAreaCod[0];
         pr_default.close(2);
         /* Using cursor T00356 */
         pr_default.execute(4, new Object[] {n1136ContratoGestor_ContratadaCod, A1136ContratoGestor_ContratadaCod});
         if ( (pr_default.getStatus(4) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato Gestor_Contrato'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A1223ContratoGestor_ContratadaSigla = T00356_A1223ContratoGestor_ContratadaSigla[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1223ContratoGestor_ContratadaSigla", A1223ContratoGestor_ContratadaSigla);
         n1223ContratoGestor_ContratadaSigla = T00356_n1223ContratoGestor_ContratadaSigla[0];
         A1137ContratoGestor_ContratadaTipo = T00356_A1137ContratoGestor_ContratadaTipo[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1137ContratoGestor_ContratadaTipo", A1137ContratoGestor_ContratadaTipo);
         n1137ContratoGestor_ContratadaTipo = T00356_n1137ContratoGestor_ContratadaTipo[0];
         pr_default.close(4);
         /* Using cursor T00357 */
         pr_default.execute(5, new Object[] {n1446ContratoGestor_ContratadaAreaCod, A1446ContratoGestor_ContratadaAreaCod});
         if ( (pr_default.getStatus(5) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Area de Trabalho'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A1447ContratoGestor_ContratadaAreaDes = T00357_A1447ContratoGestor_ContratadaAreaDes[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1447ContratoGestor_ContratadaAreaDes", A1447ContratoGestor_ContratadaAreaDes);
         n1447ContratoGestor_ContratadaAreaDes = T00357_n1447ContratoGestor_ContratadaAreaDes[0];
         pr_default.close(5);
         /* Using cursor T00355 */
         pr_default.execute(3, new Object[] {A1079ContratoGestor_UsuarioCod});
         if ( (pr_default.getStatus(3) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato Gestor_Usuario'.", "ForeignKeyNotFound", 1, "CONTRATOGESTOR_USUARIOCOD");
            AnyError = 1;
            GX_FocusControl = edtContratoGestor_UsuarioCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A2033ContratoGestor_UsuarioAtv = T00355_A2033ContratoGestor_UsuarioAtv[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2033ContratoGestor_UsuarioAtv", A2033ContratoGestor_UsuarioAtv);
         n2033ContratoGestor_UsuarioAtv = T00355_n2033ContratoGestor_UsuarioAtv[0];
         A1080ContratoGestor_UsuarioPesCod = T00355_A1080ContratoGestor_UsuarioPesCod[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1080ContratoGestor_UsuarioPesCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1080ContratoGestor_UsuarioPesCod), 6, 0)));
         n1080ContratoGestor_UsuarioPesCod = T00355_n1080ContratoGestor_UsuarioPesCod[0];
         pr_default.close(3);
         /* Using cursor T00358 */
         pr_default.execute(6, new Object[] {n1080ContratoGestor_UsuarioPesCod, A1080ContratoGestor_UsuarioPesCod});
         if ( (pr_default.getStatus(6) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Pessoa'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A1081ContratoGestor_UsuarioPesNom = T00358_A1081ContratoGestor_UsuarioPesNom[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1081ContratoGestor_UsuarioPesNom", A1081ContratoGestor_UsuarioPesNom);
         n1081ContratoGestor_UsuarioPesNom = T00358_n1081ContratoGestor_UsuarioPesNom[0];
         pr_default.close(6);
         GXt_boolean1 = A1135ContratoGestor_UsuarioEhContratante;
         new prc_usuarioehcontratantedaarea(context ).execute( ref  A1446ContratoGestor_ContratadaAreaCod, ref  A1079ContratoGestor_UsuarioCod, out  GXt_boolean1) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1446ContratoGestor_ContratadaAreaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1446ContratoGestor_ContratadaAreaCod), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1079ContratoGestor_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1079ContratoGestor_UsuarioCod), 6, 0)));
         A1135ContratoGestor_UsuarioEhContratante = GXt_boolean1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1135ContratoGestor_UsuarioEhContratante", A1135ContratoGestor_UsuarioEhContratante);
         if ( ! ( ( A2009ContratoGestor_Tipo == 1 ) || ( A2009ContratoGestor_Tipo == 2 ) || (0==A2009ContratoGestor_Tipo) ) )
         {
            GX_msglist.addItem("Campo Tipo fora do intervalo", "OutOfRange", 1, "CONTRATOGESTOR_TIPO");
            AnyError = 1;
            GX_FocusControl = cmbContratoGestor_Tipo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
      }

      protected void CloseExtendedTableCursors35129( )
      {
         pr_default.close(2);
         pr_default.close(4);
         pr_default.close(5);
         pr_default.close(3);
         pr_default.close(6);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_4( int A1078ContratoGestor_ContratoCod )
      {
         /* Using cursor T003510 */
         pr_default.execute(8, new Object[] {A1078ContratoGestor_ContratoCod});
         if ( (pr_default.getStatus(8) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato Gestor_Contrato'.", "ForeignKeyNotFound", 1, "CONTRATOGESTOR_CONTRATOCOD");
            AnyError = 1;
            GX_FocusControl = edtContratoGestor_ContratoCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A1136ContratoGestor_ContratadaCod = T003510_A1136ContratoGestor_ContratadaCod[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1136ContratoGestor_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1136ContratoGestor_ContratadaCod), 6, 0)));
         n1136ContratoGestor_ContratadaCod = T003510_n1136ContratoGestor_ContratadaCod[0];
         A1446ContratoGestor_ContratadaAreaCod = T003510_A1446ContratoGestor_ContratadaAreaCod[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1446ContratoGestor_ContratadaAreaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1446ContratoGestor_ContratadaAreaCod), 6, 0)));
         n1446ContratoGestor_ContratadaAreaCod = T003510_n1446ContratoGestor_ContratadaAreaCod[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A1136ContratoGestor_ContratadaCod), 6, 0, ".", "")))+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A1446ContratoGestor_ContratadaAreaCod), 6, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(8) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(8);
      }

      protected void gxLoad_6( int A1136ContratoGestor_ContratadaCod )
      {
         /* Using cursor T003511 */
         pr_default.execute(9, new Object[] {n1136ContratoGestor_ContratadaCod, A1136ContratoGestor_ContratadaCod});
         if ( (pr_default.getStatus(9) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato Gestor_Contrato'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A1223ContratoGestor_ContratadaSigla = T003511_A1223ContratoGestor_ContratadaSigla[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1223ContratoGestor_ContratadaSigla", A1223ContratoGestor_ContratadaSigla);
         n1223ContratoGestor_ContratadaSigla = T003511_n1223ContratoGestor_ContratadaSigla[0];
         A1137ContratoGestor_ContratadaTipo = T003511_A1137ContratoGestor_ContratadaTipo[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1137ContratoGestor_ContratadaTipo", A1137ContratoGestor_ContratadaTipo);
         n1137ContratoGestor_ContratadaTipo = T003511_n1137ContratoGestor_ContratadaTipo[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A1223ContratoGestor_ContratadaSigla))+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A1137ContratoGestor_ContratadaTipo))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(9) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(9);
      }

      protected void gxLoad_7( int A1446ContratoGestor_ContratadaAreaCod )
      {
         /* Using cursor T003512 */
         pr_default.execute(10, new Object[] {n1446ContratoGestor_ContratadaAreaCod, A1446ContratoGestor_ContratadaAreaCod});
         if ( (pr_default.getStatus(10) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Area de Trabalho'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A1447ContratoGestor_ContratadaAreaDes = T003512_A1447ContratoGestor_ContratadaAreaDes[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1447ContratoGestor_ContratadaAreaDes", A1447ContratoGestor_ContratadaAreaDes);
         n1447ContratoGestor_ContratadaAreaDes = T003512_n1447ContratoGestor_ContratadaAreaDes[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( A1447ContratoGestor_ContratadaAreaDes)+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(10) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(10);
      }

      protected void gxLoad_5( int A1079ContratoGestor_UsuarioCod )
      {
         /* Using cursor T003513 */
         pr_default.execute(11, new Object[] {A1079ContratoGestor_UsuarioCod});
         if ( (pr_default.getStatus(11) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato Gestor_Usuario'.", "ForeignKeyNotFound", 1, "CONTRATOGESTOR_USUARIOCOD");
            AnyError = 1;
            GX_FocusControl = edtContratoGestor_UsuarioCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A2033ContratoGestor_UsuarioAtv = T003513_A2033ContratoGestor_UsuarioAtv[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2033ContratoGestor_UsuarioAtv", A2033ContratoGestor_UsuarioAtv);
         n2033ContratoGestor_UsuarioAtv = T003513_n2033ContratoGestor_UsuarioAtv[0];
         A1080ContratoGestor_UsuarioPesCod = T003513_A1080ContratoGestor_UsuarioPesCod[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1080ContratoGestor_UsuarioPesCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1080ContratoGestor_UsuarioPesCod), 6, 0)));
         n1080ContratoGestor_UsuarioPesCod = T003513_n1080ContratoGestor_UsuarioPesCod[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.BoolToStr( A2033ContratoGestor_UsuarioAtv))+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A1080ContratoGestor_UsuarioPesCod), 6, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(11) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(11);
      }

      protected void gxLoad_8( int A1080ContratoGestor_UsuarioPesCod )
      {
         /* Using cursor T003514 */
         pr_default.execute(12, new Object[] {n1080ContratoGestor_UsuarioPesCod, A1080ContratoGestor_UsuarioPesCod});
         if ( (pr_default.getStatus(12) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Pessoa'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A1081ContratoGestor_UsuarioPesNom = T003514_A1081ContratoGestor_UsuarioPesNom[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1081ContratoGestor_UsuarioPesNom", A1081ContratoGestor_UsuarioPesNom);
         n1081ContratoGestor_UsuarioPesNom = T003514_n1081ContratoGestor_UsuarioPesNom[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A1081ContratoGestor_UsuarioPesNom))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(12) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(12);
      }

      protected void GetKey35129( )
      {
         /* Using cursor T003515 */
         pr_default.execute(13, new Object[] {A1078ContratoGestor_ContratoCod, A1079ContratoGestor_UsuarioCod});
         if ( (pr_default.getStatus(13) != 101) )
         {
            RcdFound129 = 1;
         }
         else
         {
            RcdFound129 = 0;
         }
         pr_default.close(13);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T00353 */
         pr_default.execute(1, new Object[] {A1078ContratoGestor_ContratoCod, A1079ContratoGestor_UsuarioCod});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM35129( 3) ;
            RcdFound129 = 1;
            A2009ContratoGestor_Tipo = T00353_A2009ContratoGestor_Tipo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2009ContratoGestor_Tipo", StringUtil.Str( (decimal)(A2009ContratoGestor_Tipo), 1, 0));
            n2009ContratoGestor_Tipo = T00353_n2009ContratoGestor_Tipo[0];
            A1078ContratoGestor_ContratoCod = T00353_A1078ContratoGestor_ContratoCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1078ContratoGestor_ContratoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1078ContratoGestor_ContratoCod), 6, 0)));
            A1079ContratoGestor_UsuarioCod = T00353_A1079ContratoGestor_UsuarioCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1079ContratoGestor_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1079ContratoGestor_UsuarioCod), 6, 0)));
            Z1078ContratoGestor_ContratoCod = A1078ContratoGestor_ContratoCod;
            Z1079ContratoGestor_UsuarioCod = A1079ContratoGestor_UsuarioCod;
            sMode129 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Load35129( ) ;
            if ( AnyError == 1 )
            {
               RcdFound129 = 0;
               InitializeNonKey35129( ) ;
            }
            Gx_mode = sMode129;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            RcdFound129 = 0;
            InitializeNonKey35129( ) ;
            sMode129 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Gx_mode = sMode129;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey35129( ) ;
         if ( RcdFound129 == 0 )
         {
            Gx_mode = "INS";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound129 = 0;
         /* Using cursor T003516 */
         pr_default.execute(14, new Object[] {A1078ContratoGestor_ContratoCod, A1079ContratoGestor_UsuarioCod});
         if ( (pr_default.getStatus(14) != 101) )
         {
            while ( (pr_default.getStatus(14) != 101) && ( ( T003516_A1078ContratoGestor_ContratoCod[0] < A1078ContratoGestor_ContratoCod ) || ( T003516_A1078ContratoGestor_ContratoCod[0] == A1078ContratoGestor_ContratoCod ) && ( T003516_A1079ContratoGestor_UsuarioCod[0] < A1079ContratoGestor_UsuarioCod ) ) )
            {
               pr_default.readNext(14);
            }
            if ( (pr_default.getStatus(14) != 101) && ( ( T003516_A1078ContratoGestor_ContratoCod[0] > A1078ContratoGestor_ContratoCod ) || ( T003516_A1078ContratoGestor_ContratoCod[0] == A1078ContratoGestor_ContratoCod ) && ( T003516_A1079ContratoGestor_UsuarioCod[0] > A1079ContratoGestor_UsuarioCod ) ) )
            {
               A1078ContratoGestor_ContratoCod = T003516_A1078ContratoGestor_ContratoCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1078ContratoGestor_ContratoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1078ContratoGestor_ContratoCod), 6, 0)));
               A1079ContratoGestor_UsuarioCod = T003516_A1079ContratoGestor_UsuarioCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1079ContratoGestor_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1079ContratoGestor_UsuarioCod), 6, 0)));
               RcdFound129 = 1;
            }
         }
         pr_default.close(14);
      }

      protected void move_previous( )
      {
         RcdFound129 = 0;
         /* Using cursor T003517 */
         pr_default.execute(15, new Object[] {A1078ContratoGestor_ContratoCod, A1079ContratoGestor_UsuarioCod});
         if ( (pr_default.getStatus(15) != 101) )
         {
            while ( (pr_default.getStatus(15) != 101) && ( ( T003517_A1078ContratoGestor_ContratoCod[0] > A1078ContratoGestor_ContratoCod ) || ( T003517_A1078ContratoGestor_ContratoCod[0] == A1078ContratoGestor_ContratoCod ) && ( T003517_A1079ContratoGestor_UsuarioCod[0] > A1079ContratoGestor_UsuarioCod ) ) )
            {
               pr_default.readNext(15);
            }
            if ( (pr_default.getStatus(15) != 101) && ( ( T003517_A1078ContratoGestor_ContratoCod[0] < A1078ContratoGestor_ContratoCod ) || ( T003517_A1078ContratoGestor_ContratoCod[0] == A1078ContratoGestor_ContratoCod ) && ( T003517_A1079ContratoGestor_UsuarioCod[0] < A1079ContratoGestor_UsuarioCod ) ) )
            {
               A1078ContratoGestor_ContratoCod = T003517_A1078ContratoGestor_ContratoCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1078ContratoGestor_ContratoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1078ContratoGestor_ContratoCod), 6, 0)));
               A1079ContratoGestor_UsuarioCod = T003517_A1079ContratoGestor_UsuarioCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1079ContratoGestor_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1079ContratoGestor_UsuarioCod), 6, 0)));
               RcdFound129 = 1;
            }
         }
         pr_default.close(15);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey35129( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtContratoGestor_ContratoCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert35129( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound129 == 1 )
            {
               if ( ( A1078ContratoGestor_ContratoCod != Z1078ContratoGestor_ContratoCod ) || ( A1079ContratoGestor_UsuarioCod != Z1079ContratoGestor_UsuarioCod ) )
               {
                  A1078ContratoGestor_ContratoCod = Z1078ContratoGestor_ContratoCod;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1078ContratoGestor_ContratoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1078ContratoGestor_ContratoCod), 6, 0)));
                  A1079ContratoGestor_UsuarioCod = Z1079ContratoGestor_UsuarioCod;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1079ContratoGestor_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1079ContratoGestor_UsuarioCod), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "CONTRATOGESTOR_CONTRATOCOD");
                  AnyError = 1;
                  GX_FocusControl = edtContratoGestor_ContratoCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtContratoGestor_ContratoCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  Gx_mode = "UPD";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Update record */
                  Update35129( ) ;
                  GX_FocusControl = edtContratoGestor_ContratoCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( ( A1078ContratoGestor_ContratoCod != Z1078ContratoGestor_ContratoCod ) || ( A1079ContratoGestor_UsuarioCod != Z1079ContratoGestor_UsuarioCod ) )
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Insert record */
                  GX_FocusControl = edtContratoGestor_ContratoCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert35129( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "CONTRATOGESTOR_CONTRATOCOD");
                     AnyError = 1;
                     GX_FocusControl = edtContratoGestor_ContratoCod_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     Gx_mode = "INS";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     /* Insert record */
                     GX_FocusControl = edtContratoGestor_ContratoCod_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert35129( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
      }

      protected void btn_delete( )
      {
         if ( ( A1078ContratoGestor_ContratoCod != Z1078ContratoGestor_ContratoCod ) || ( A1079ContratoGestor_UsuarioCod != Z1079ContratoGestor_UsuarioCod ) )
         {
            A1078ContratoGestor_ContratoCod = Z1078ContratoGestor_ContratoCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1078ContratoGestor_ContratoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1078ContratoGestor_ContratoCod), 6, 0)));
            A1079ContratoGestor_UsuarioCod = Z1079ContratoGestor_UsuarioCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1079ContratoGestor_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1079ContratoGestor_UsuarioCod), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "CONTRATOGESTOR_CONTRATOCOD");
            AnyError = 1;
            GX_FocusControl = edtContratoGestor_ContratoCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtContratoGestor_ContratoCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            getByPrimaryKey( ) ;
         }
         CloseOpenCursors();
      }

      protected void btn_get( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         if ( RcdFound129 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "CONTRATOGESTOR_CONTRATOCOD");
            AnyError = 1;
            GX_FocusControl = edtContratoGestor_ContratoCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GX_FocusControl = cmbContratoGestor_Tipo_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_first( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart35129( ) ;
         if ( RcdFound129 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = cmbContratoGestor_Tipo_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         ScanEnd35129( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_previous( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_previous( ) ;
         if ( RcdFound129 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = cmbContratoGestor_Tipo_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_next( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_next( ) ;
         if ( RcdFound129 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = cmbContratoGestor_Tipo_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_last( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart35129( ) ;
         if ( RcdFound129 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            while ( RcdFound129 != 0 )
            {
               ScanNext35129( ) ;
            }
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = cmbContratoGestor_Tipo_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         ScanEnd35129( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_select( )
      {
         getEqualNoModal( ) ;
      }

      protected void CheckOptimisticConcurrency35129( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T00352 */
            pr_default.execute(0, new Object[] {A1078ContratoGestor_ContratoCod, A1079ContratoGestor_UsuarioCod});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContratoGestor"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( Z2009ContratoGestor_Tipo != T00352_A2009ContratoGestor_Tipo[0] ) )
            {
               if ( Z2009ContratoGestor_Tipo != T00352_A2009ContratoGestor_Tipo[0] )
               {
                  GXUtil.WriteLog("contratogestor:[seudo value changed for attri]"+"ContratoGestor_Tipo");
                  GXUtil.WriteLogRaw("Old: ",Z2009ContratoGestor_Tipo);
                  GXUtil.WriteLogRaw("Current: ",T00352_A2009ContratoGestor_Tipo[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"ContratoGestor"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert35129( )
      {
         BeforeValidate35129( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable35129( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM35129( 0) ;
            CheckOptimisticConcurrency35129( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm35129( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert35129( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T003518 */
                     pr_default.execute(16, new Object[] {n2009ContratoGestor_Tipo, A2009ContratoGestor_Tipo, A1078ContratoGestor_ContratoCod, A1079ContratoGestor_UsuarioCod});
                     pr_default.close(16);
                     dsDefault.SmartCacheProvider.SetUpdated("ContratoGestor") ;
                     if ( (pr_default.getStatus(16) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption350( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load35129( ) ;
            }
            EndLevel35129( ) ;
         }
         CloseExtendedTableCursors35129( ) ;
      }

      protected void Update35129( )
      {
         BeforeValidate35129( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable35129( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency35129( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm35129( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate35129( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T003519 */
                     pr_default.execute(17, new Object[] {n2009ContratoGestor_Tipo, A2009ContratoGestor_Tipo, A1078ContratoGestor_ContratoCod, A1079ContratoGestor_UsuarioCod});
                     pr_default.close(17);
                     dsDefault.SmartCacheProvider.SetUpdated("ContratoGestor") ;
                     if ( (pr_default.getStatus(17) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContratoGestor"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate35129( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                           ResetCaption350( ) ;
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel35129( ) ;
         }
         CloseExtendedTableCursors35129( ) ;
      }

      protected void DeferredUpdate35129( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         BeforeValidate35129( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency35129( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls35129( ) ;
            AfterConfirm35129( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete35129( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T003520 */
                  pr_default.execute(18, new Object[] {A1078ContratoGestor_ContratoCod, A1079ContratoGestor_UsuarioCod});
                  pr_default.close(18);
                  dsDefault.SmartCacheProvider.SetUpdated("ContratoGestor") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        move_next( ) ;
                        if ( RcdFound129 == 0 )
                        {
                           InitAll35129( ) ;
                           Gx_mode = "INS";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        }
                        else
                        {
                           getByPrimaryKey( ) ;
                           Gx_mode = "UPD";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        }
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                        ResetCaption350( ) ;
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode129 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         EndLevel35129( ) ;
         Gx_mode = sMode129;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
      }

      protected void OnDeleteControls35129( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor T003521 */
            pr_default.execute(19, new Object[] {A1078ContratoGestor_ContratoCod});
            A1136ContratoGestor_ContratadaCod = T003521_A1136ContratoGestor_ContratadaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1136ContratoGestor_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1136ContratoGestor_ContratadaCod), 6, 0)));
            n1136ContratoGestor_ContratadaCod = T003521_n1136ContratoGestor_ContratadaCod[0];
            A1446ContratoGestor_ContratadaAreaCod = T003521_A1446ContratoGestor_ContratadaAreaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1446ContratoGestor_ContratadaAreaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1446ContratoGestor_ContratadaAreaCod), 6, 0)));
            n1446ContratoGestor_ContratadaAreaCod = T003521_n1446ContratoGestor_ContratadaAreaCod[0];
            pr_default.close(19);
            /* Using cursor T003522 */
            pr_default.execute(20, new Object[] {n1136ContratoGestor_ContratadaCod, A1136ContratoGestor_ContratadaCod});
            A1223ContratoGestor_ContratadaSigla = T003522_A1223ContratoGestor_ContratadaSigla[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1223ContratoGestor_ContratadaSigla", A1223ContratoGestor_ContratadaSigla);
            n1223ContratoGestor_ContratadaSigla = T003522_n1223ContratoGestor_ContratadaSigla[0];
            A1137ContratoGestor_ContratadaTipo = T003522_A1137ContratoGestor_ContratadaTipo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1137ContratoGestor_ContratadaTipo", A1137ContratoGestor_ContratadaTipo);
            n1137ContratoGestor_ContratadaTipo = T003522_n1137ContratoGestor_ContratadaTipo[0];
            pr_default.close(20);
            /* Using cursor T003523 */
            pr_default.execute(21, new Object[] {n1446ContratoGestor_ContratadaAreaCod, A1446ContratoGestor_ContratadaAreaCod});
            A1447ContratoGestor_ContratadaAreaDes = T003523_A1447ContratoGestor_ContratadaAreaDes[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1447ContratoGestor_ContratadaAreaDes", A1447ContratoGestor_ContratadaAreaDes);
            n1447ContratoGestor_ContratadaAreaDes = T003523_n1447ContratoGestor_ContratadaAreaDes[0];
            pr_default.close(21);
            /* Using cursor T003524 */
            pr_default.execute(22, new Object[] {A1079ContratoGestor_UsuarioCod});
            A2033ContratoGestor_UsuarioAtv = T003524_A2033ContratoGestor_UsuarioAtv[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2033ContratoGestor_UsuarioAtv", A2033ContratoGestor_UsuarioAtv);
            n2033ContratoGestor_UsuarioAtv = T003524_n2033ContratoGestor_UsuarioAtv[0];
            A1080ContratoGestor_UsuarioPesCod = T003524_A1080ContratoGestor_UsuarioPesCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1080ContratoGestor_UsuarioPesCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1080ContratoGestor_UsuarioPesCod), 6, 0)));
            n1080ContratoGestor_UsuarioPesCod = T003524_n1080ContratoGestor_UsuarioPesCod[0];
            pr_default.close(22);
            /* Using cursor T003525 */
            pr_default.execute(23, new Object[] {n1080ContratoGestor_UsuarioPesCod, A1080ContratoGestor_UsuarioPesCod});
            A1081ContratoGestor_UsuarioPesNom = T003525_A1081ContratoGestor_UsuarioPesNom[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1081ContratoGestor_UsuarioPesNom", A1081ContratoGestor_UsuarioPesNom);
            n1081ContratoGestor_UsuarioPesNom = T003525_n1081ContratoGestor_UsuarioPesNom[0];
            pr_default.close(23);
            GXt_boolean1 = A1135ContratoGestor_UsuarioEhContratante;
            new prc_usuarioehcontratantedaarea(context ).execute( ref  A1446ContratoGestor_ContratadaAreaCod, ref  A1079ContratoGestor_UsuarioCod, out  GXt_boolean1) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1446ContratoGestor_ContratadaAreaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1446ContratoGestor_ContratadaAreaCod), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1079ContratoGestor_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1079ContratoGestor_UsuarioCod), 6, 0)));
            A1135ContratoGestor_UsuarioEhContratante = GXt_boolean1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1135ContratoGestor_UsuarioEhContratante", A1135ContratoGestor_UsuarioEhContratante);
         }
      }

      protected void EndLevel35129( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete35129( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            pr_default.close(19);
            pr_default.close(22);
            pr_default.close(20);
            pr_default.close(21);
            pr_default.close(23);
            context.CommitDataStores( "ContratoGestor");
            if ( AnyError == 0 )
            {
               ConfirmValues350( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            pr_default.close(19);
            pr_default.close(22);
            pr_default.close(20);
            pr_default.close(21);
            pr_default.close(23);
            context.RollbackDataStores( "ContratoGestor");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart35129( )
      {
         /* Using cursor T003526 */
         pr_default.execute(24);
         RcdFound129 = 0;
         if ( (pr_default.getStatus(24) != 101) )
         {
            RcdFound129 = 1;
            A1078ContratoGestor_ContratoCod = T003526_A1078ContratoGestor_ContratoCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1078ContratoGestor_ContratoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1078ContratoGestor_ContratoCod), 6, 0)));
            A1079ContratoGestor_UsuarioCod = T003526_A1079ContratoGestor_UsuarioCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1079ContratoGestor_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1079ContratoGestor_UsuarioCod), 6, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext35129( )
      {
         /* Scan next routine */
         pr_default.readNext(24);
         RcdFound129 = 0;
         if ( (pr_default.getStatus(24) != 101) )
         {
            RcdFound129 = 1;
            A1078ContratoGestor_ContratoCod = T003526_A1078ContratoGestor_ContratoCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1078ContratoGestor_ContratoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1078ContratoGestor_ContratoCod), 6, 0)));
            A1079ContratoGestor_UsuarioCod = T003526_A1079ContratoGestor_UsuarioCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1079ContratoGestor_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1079ContratoGestor_UsuarioCod), 6, 0)));
         }
      }

      protected void ScanEnd35129( )
      {
         pr_default.close(24);
      }

      protected void AfterConfirm35129( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert35129( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate35129( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete35129( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete35129( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate35129( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes35129( )
      {
         edtContratoGestor_ContratoCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoGestor_ContratoCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoGestor_ContratoCod_Enabled), 5, 0)));
         edtContratoGestor_ContratadaCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoGestor_ContratadaCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoGestor_ContratadaCod_Enabled), 5, 0)));
         edtContratoGestor_ContratadaSigla_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoGestor_ContratadaSigla_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoGestor_ContratadaSigla_Enabled), 5, 0)));
         cmbContratoGestor_ContratadaTipo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratoGestor_ContratadaTipo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbContratoGestor_ContratadaTipo.Enabled), 5, 0)));
         edtContratoGestor_ContratadaAreaCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoGestor_ContratadaAreaCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoGestor_ContratadaAreaCod_Enabled), 5, 0)));
         edtContratoGestor_ContratadaAreaDes_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoGestor_ContratadaAreaDes_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoGestor_ContratadaAreaDes_Enabled), 5, 0)));
         edtContratoGestor_UsuarioCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoGestor_UsuarioCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoGestor_UsuarioCod_Enabled), 5, 0)));
         edtContratoGestor_UsuarioPesCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoGestor_UsuarioPesCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoGestor_UsuarioPesCod_Enabled), 5, 0)));
         edtContratoGestor_UsuarioPesNom_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoGestor_UsuarioPesNom_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoGestor_UsuarioPesNom_Enabled), 5, 0)));
         chkContratoGestor_UsuarioEhContratante.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkContratoGestor_UsuarioEhContratante_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkContratoGestor_UsuarioEhContratante.Enabled), 5, 0)));
         chkContratoGestor_UsuarioAtv.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkContratoGestor_UsuarioAtv_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkContratoGestor_UsuarioAtv.Enabled), 5, 0)));
         cmbContratoGestor_Tipo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContratoGestor_Tipo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbContratoGestor_Tipo.Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues350( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020529931327");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("contratogestor.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z1078ContratoGestor_ContratoCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1078ContratoGestor_ContratoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1079ContratoGestor_UsuarioCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1079ContratoGestor_UsuarioCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z2009ContratoGestor_Tipo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z2009ContratoGestor_Tipo), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("contratogestor.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "ContratoGestor" ;
      }

      public override String GetPgmdesc( )
      {
         return "Gestor do Contrato" ;
      }

      protected void InitializeNonKey35129( )
      {
         A1135ContratoGestor_UsuarioEhContratante = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1135ContratoGestor_UsuarioEhContratante", A1135ContratoGestor_UsuarioEhContratante);
         A1136ContratoGestor_ContratadaCod = 0;
         n1136ContratoGestor_ContratadaCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1136ContratoGestor_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1136ContratoGestor_ContratadaCod), 6, 0)));
         A1223ContratoGestor_ContratadaSigla = "";
         n1223ContratoGestor_ContratadaSigla = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1223ContratoGestor_ContratadaSigla", A1223ContratoGestor_ContratadaSigla);
         A1137ContratoGestor_ContratadaTipo = "";
         n1137ContratoGestor_ContratadaTipo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1137ContratoGestor_ContratadaTipo", A1137ContratoGestor_ContratadaTipo);
         A1446ContratoGestor_ContratadaAreaCod = 0;
         n1446ContratoGestor_ContratadaAreaCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1446ContratoGestor_ContratadaAreaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1446ContratoGestor_ContratadaAreaCod), 6, 0)));
         A1447ContratoGestor_ContratadaAreaDes = "";
         n1447ContratoGestor_ContratadaAreaDes = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1447ContratoGestor_ContratadaAreaDes", A1447ContratoGestor_ContratadaAreaDes);
         A1080ContratoGestor_UsuarioPesCod = 0;
         n1080ContratoGestor_UsuarioPesCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1080ContratoGestor_UsuarioPesCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1080ContratoGestor_UsuarioPesCod), 6, 0)));
         A1081ContratoGestor_UsuarioPesNom = "";
         n1081ContratoGestor_UsuarioPesNom = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1081ContratoGestor_UsuarioPesNom", A1081ContratoGestor_UsuarioPesNom);
         A2033ContratoGestor_UsuarioAtv = false;
         n2033ContratoGestor_UsuarioAtv = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2033ContratoGestor_UsuarioAtv", A2033ContratoGestor_UsuarioAtv);
         A2009ContratoGestor_Tipo = 0;
         n2009ContratoGestor_Tipo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2009ContratoGestor_Tipo", StringUtil.Str( (decimal)(A2009ContratoGestor_Tipo), 1, 0));
         n2009ContratoGestor_Tipo = ((0==A2009ContratoGestor_Tipo) ? true : false);
         Z2009ContratoGestor_Tipo = 0;
      }

      protected void InitAll35129( )
      {
         A1078ContratoGestor_ContratoCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1078ContratoGestor_ContratoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1078ContratoGestor_ContratoCod), 6, 0)));
         A1079ContratoGestor_UsuarioCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1079ContratoGestor_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1079ContratoGestor_UsuarioCod), 6, 0)));
         InitializeNonKey35129( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020529931333");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("contratogestor.js", "?2020529931333");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         imgBtn_first_Internalname = "BTN_FIRST";
         imgBtn_first_separator_Internalname = "BTN_FIRST_SEPARATOR";
         imgBtn_previous_Internalname = "BTN_PREVIOUS";
         imgBtn_previous_separator_Internalname = "BTN_PREVIOUS_SEPARATOR";
         imgBtn_next_Internalname = "BTN_NEXT";
         imgBtn_next_separator_Internalname = "BTN_NEXT_SEPARATOR";
         imgBtn_last_Internalname = "BTN_LAST";
         imgBtn_last_separator_Internalname = "BTN_LAST_SEPARATOR";
         imgBtn_select_Internalname = "BTN_SELECT";
         imgBtn_select_separator_Internalname = "BTN_SELECT_SEPARATOR";
         imgBtn_enter2_Internalname = "BTN_ENTER2";
         imgBtn_enter2_separator_Internalname = "BTN_ENTER2_SEPARATOR";
         imgBtn_cancel2_Internalname = "BTN_CANCEL2";
         imgBtn_cancel2_separator_Internalname = "BTN_CANCEL2_SEPARATOR";
         imgBtn_delete2_Internalname = "BTN_DELETE2";
         imgBtn_delete2_separator_Internalname = "BTN_DELETE2_SEPARATOR";
         divSectiontoolbar_Internalname = "SECTIONTOOLBAR";
         tblTabletoolbar_Internalname = "TABLETOOLBAR";
         lblTextblockcontratogestor_contratocod_Internalname = "TEXTBLOCKCONTRATOGESTOR_CONTRATOCOD";
         edtContratoGestor_ContratoCod_Internalname = "CONTRATOGESTOR_CONTRATOCOD";
         lblTextblockcontratogestor_contratadacod_Internalname = "TEXTBLOCKCONTRATOGESTOR_CONTRATADACOD";
         edtContratoGestor_ContratadaCod_Internalname = "CONTRATOGESTOR_CONTRATADACOD";
         lblTextblockcontratogestor_contratadasigla_Internalname = "TEXTBLOCKCONTRATOGESTOR_CONTRATADASIGLA";
         edtContratoGestor_ContratadaSigla_Internalname = "CONTRATOGESTOR_CONTRATADASIGLA";
         lblTextblockcontratogestor_contratadatipo_Internalname = "TEXTBLOCKCONTRATOGESTOR_CONTRATADATIPO";
         cmbContratoGestor_ContratadaTipo_Internalname = "CONTRATOGESTOR_CONTRATADATIPO";
         lblTextblockcontratogestor_contratadaareacod_Internalname = "TEXTBLOCKCONTRATOGESTOR_CONTRATADAAREACOD";
         edtContratoGestor_ContratadaAreaCod_Internalname = "CONTRATOGESTOR_CONTRATADAAREACOD";
         lblTextblockcontratogestor_contratadaareades_Internalname = "TEXTBLOCKCONTRATOGESTOR_CONTRATADAAREADES";
         edtContratoGestor_ContratadaAreaDes_Internalname = "CONTRATOGESTOR_CONTRATADAAREADES";
         lblTextblockcontratogestor_usuariocod_Internalname = "TEXTBLOCKCONTRATOGESTOR_USUARIOCOD";
         edtContratoGestor_UsuarioCod_Internalname = "CONTRATOGESTOR_USUARIOCOD";
         lblTextblockcontratogestor_usuariopescod_Internalname = "TEXTBLOCKCONTRATOGESTOR_USUARIOPESCOD";
         edtContratoGestor_UsuarioPesCod_Internalname = "CONTRATOGESTOR_USUARIOPESCOD";
         lblTextblockcontratogestor_usuariopesnom_Internalname = "TEXTBLOCKCONTRATOGESTOR_USUARIOPESNOM";
         edtContratoGestor_UsuarioPesNom_Internalname = "CONTRATOGESTOR_USUARIOPESNOM";
         lblTextblockcontratogestor_usuarioehcontratante_Internalname = "TEXTBLOCKCONTRATOGESTOR_USUARIOEHCONTRATANTE";
         chkContratoGestor_UsuarioEhContratante_Internalname = "CONTRATOGESTOR_USUARIOEHCONTRATANTE";
         lblTextblockcontratogestor_usuarioatv_Internalname = "TEXTBLOCKCONTRATOGESTOR_USUARIOATV";
         chkContratoGestor_UsuarioAtv_Internalname = "CONTRATOGESTOR_USUARIOATV";
         lblTextblockcontratogestor_tipo_Internalname = "TEXTBLOCKCONTRATOGESTOR_TIPO";
         cmbContratoGestor_Tipo_Internalname = "CONTRATOGESTOR_TIPO";
         tblTable2_Internalname = "TABLE2";
         bttBtn_enter_Internalname = "BTN_ENTER";
         bttBtn_cancel_Internalname = "BTN_CANCEL";
         bttBtn_delete_Internalname = "BTN_DELETE";
         tblTable1_Internalname = "TABLE1";
         grpGroupdata_Internalname = "GROUPDATA";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Gestor do Contrato";
         imgBtn_delete2_separator_Visible = 1;
         imgBtn_delete2_Enabled = 1;
         imgBtn_delete2_Visible = 1;
         imgBtn_cancel2_separator_Visible = 1;
         imgBtn_cancel2_Visible = 1;
         imgBtn_enter2_separator_Visible = 1;
         imgBtn_enter2_Enabled = 1;
         imgBtn_enter2_Visible = 1;
         imgBtn_select_separator_Visible = 1;
         imgBtn_select_Visible = 1;
         imgBtn_last_separator_Visible = 1;
         imgBtn_last_Visible = 1;
         imgBtn_next_separator_Visible = 1;
         imgBtn_next_Visible = 1;
         imgBtn_previous_separator_Visible = 1;
         imgBtn_previous_Visible = 1;
         imgBtn_first_separator_Visible = 1;
         imgBtn_first_Visible = 1;
         cmbContratoGestor_Tipo_Jsonclick = "";
         cmbContratoGestor_Tipo.Enabled = 1;
         chkContratoGestor_UsuarioAtv.Enabled = 0;
         chkContratoGestor_UsuarioEhContratante.Enabled = 0;
         edtContratoGestor_UsuarioPesNom_Jsonclick = "";
         edtContratoGestor_UsuarioPesNom_Enabled = 0;
         edtContratoGestor_UsuarioPesCod_Jsonclick = "";
         edtContratoGestor_UsuarioPesCod_Enabled = 0;
         edtContratoGestor_UsuarioCod_Jsonclick = "";
         edtContratoGestor_UsuarioCod_Enabled = 1;
         edtContratoGestor_ContratadaAreaDes_Jsonclick = "";
         edtContratoGestor_ContratadaAreaDes_Enabled = 0;
         edtContratoGestor_ContratadaAreaCod_Jsonclick = "";
         edtContratoGestor_ContratadaAreaCod_Enabled = 0;
         cmbContratoGestor_ContratadaTipo_Jsonclick = "";
         cmbContratoGestor_ContratadaTipo.Enabled = 0;
         edtContratoGestor_ContratadaSigla_Jsonclick = "";
         edtContratoGestor_ContratadaSigla_Enabled = 0;
         edtContratoGestor_ContratadaCod_Jsonclick = "";
         edtContratoGestor_ContratadaCod_Enabled = 0;
         edtContratoGestor_ContratoCod_Jsonclick = "";
         edtContratoGestor_ContratoCod_Enabled = 1;
         bttBtn_delete_Visible = 1;
         bttBtn_cancel_Visible = 1;
         bttBtn_enter_Visible = 1;
         chkContratoGestor_UsuarioAtv.Caption = "";
         chkContratoGestor_UsuarioEhContratante.Caption = "";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GX2ASACONTRATOGESTOR_USUARIOEHCONTRATANTE35129( int A1446ContratoGestor_ContratadaAreaCod ,
                                                                     int A1079ContratoGestor_UsuarioCod )
      {
         GXt_boolean1 = A1135ContratoGestor_UsuarioEhContratante;
         new prc_usuarioehcontratantedaarea(context ).execute( ref  A1446ContratoGestor_ContratadaAreaCod, ref  A1079ContratoGestor_UsuarioCod, out  GXt_boolean1) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1446ContratoGestor_ContratadaAreaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1446ContratoGestor_ContratadaAreaCod), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1079ContratoGestor_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1079ContratoGestor_UsuarioCod), 6, 0)));
         A1135ContratoGestor_UsuarioEhContratante = GXt_boolean1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1135ContratoGestor_UsuarioEhContratante", A1135ContratoGestor_UsuarioEhContratante);
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.BoolToStr( A1135ContratoGestor_UsuarioEhContratante))+"\"");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      protected void AfterKeyLoadScreen( )
      {
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         /* Using cursor T003521 */
         pr_default.execute(19, new Object[] {A1078ContratoGestor_ContratoCod});
         if ( (pr_default.getStatus(19) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato Gestor_Contrato'.", "ForeignKeyNotFound", 1, "CONTRATOGESTOR_CONTRATOCOD");
            AnyError = 1;
            GX_FocusControl = edtContratoGestor_ContratoCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A1136ContratoGestor_ContratadaCod = T003521_A1136ContratoGestor_ContratadaCod[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1136ContratoGestor_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1136ContratoGestor_ContratadaCod), 6, 0)));
         n1136ContratoGestor_ContratadaCod = T003521_n1136ContratoGestor_ContratadaCod[0];
         A1446ContratoGestor_ContratadaAreaCod = T003521_A1446ContratoGestor_ContratadaAreaCod[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1446ContratoGestor_ContratadaAreaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1446ContratoGestor_ContratadaAreaCod), 6, 0)));
         n1446ContratoGestor_ContratadaAreaCod = T003521_n1446ContratoGestor_ContratadaAreaCod[0];
         pr_default.close(19);
         /* Using cursor T003522 */
         pr_default.execute(20, new Object[] {n1136ContratoGestor_ContratadaCod, A1136ContratoGestor_ContratadaCod});
         if ( (pr_default.getStatus(20) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato Gestor_Contrato'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A1223ContratoGestor_ContratadaSigla = T003522_A1223ContratoGestor_ContratadaSigla[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1223ContratoGestor_ContratadaSigla", A1223ContratoGestor_ContratadaSigla);
         n1223ContratoGestor_ContratadaSigla = T003522_n1223ContratoGestor_ContratadaSigla[0];
         A1137ContratoGestor_ContratadaTipo = T003522_A1137ContratoGestor_ContratadaTipo[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1137ContratoGestor_ContratadaTipo", A1137ContratoGestor_ContratadaTipo);
         n1137ContratoGestor_ContratadaTipo = T003522_n1137ContratoGestor_ContratadaTipo[0];
         pr_default.close(20);
         /* Using cursor T003523 */
         pr_default.execute(21, new Object[] {n1446ContratoGestor_ContratadaAreaCod, A1446ContratoGestor_ContratadaAreaCod});
         if ( (pr_default.getStatus(21) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Area de Trabalho'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A1447ContratoGestor_ContratadaAreaDes = T003523_A1447ContratoGestor_ContratadaAreaDes[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1447ContratoGestor_ContratadaAreaDes", A1447ContratoGestor_ContratadaAreaDes);
         n1447ContratoGestor_ContratadaAreaDes = T003523_n1447ContratoGestor_ContratadaAreaDes[0];
         pr_default.close(21);
         /* Using cursor T003524 */
         pr_default.execute(22, new Object[] {A1079ContratoGestor_UsuarioCod});
         if ( (pr_default.getStatus(22) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato Gestor_Usuario'.", "ForeignKeyNotFound", 1, "CONTRATOGESTOR_USUARIOCOD");
            AnyError = 1;
            GX_FocusControl = edtContratoGestor_UsuarioCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A2033ContratoGestor_UsuarioAtv = T003524_A2033ContratoGestor_UsuarioAtv[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2033ContratoGestor_UsuarioAtv", A2033ContratoGestor_UsuarioAtv);
         n2033ContratoGestor_UsuarioAtv = T003524_n2033ContratoGestor_UsuarioAtv[0];
         A1080ContratoGestor_UsuarioPesCod = T003524_A1080ContratoGestor_UsuarioPesCod[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1080ContratoGestor_UsuarioPesCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1080ContratoGestor_UsuarioPesCod), 6, 0)));
         n1080ContratoGestor_UsuarioPesCod = T003524_n1080ContratoGestor_UsuarioPesCod[0];
         pr_default.close(22);
         /* Using cursor T003525 */
         pr_default.execute(23, new Object[] {n1080ContratoGestor_UsuarioPesCod, A1080ContratoGestor_UsuarioPesCod});
         if ( (pr_default.getStatus(23) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Pessoa'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A1081ContratoGestor_UsuarioPesNom = T003525_A1081ContratoGestor_UsuarioPesNom[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1081ContratoGestor_UsuarioPesNom", A1081ContratoGestor_UsuarioPesNom);
         n1081ContratoGestor_UsuarioPesNom = T003525_n1081ContratoGestor_UsuarioPesNom[0];
         pr_default.close(23);
         GX_FocusControl = cmbContratoGestor_Tipo_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         standaloneNotModal( ) ;
         standaloneModal( ) ;
         /* End function AfterKeyLoadScreen */
      }

      public void Valid_Contratogestor_contratocod( int GX_Parm1 ,
                                                    int GX_Parm2 ,
                                                    int GX_Parm3 ,
                                                    String GX_Parm4 ,
                                                    GXCombobox cmbGX_Parm5 ,
                                                    String GX_Parm6 )
      {
         A1078ContratoGestor_ContratoCod = GX_Parm1;
         A1136ContratoGestor_ContratadaCod = GX_Parm2;
         n1136ContratoGestor_ContratadaCod = false;
         A1446ContratoGestor_ContratadaAreaCod = GX_Parm3;
         n1446ContratoGestor_ContratadaAreaCod = false;
         A1223ContratoGestor_ContratadaSigla = GX_Parm4;
         n1223ContratoGestor_ContratadaSigla = false;
         cmbContratoGestor_ContratadaTipo = cmbGX_Parm5;
         A1137ContratoGestor_ContratadaTipo = cmbContratoGestor_ContratadaTipo.CurrentValue;
         n1137ContratoGestor_ContratadaTipo = false;
         cmbContratoGestor_ContratadaTipo.CurrentValue = A1137ContratoGestor_ContratadaTipo;
         A1447ContratoGestor_ContratadaAreaDes = GX_Parm6;
         n1447ContratoGestor_ContratadaAreaDes = false;
         /* Using cursor T003521 */
         pr_default.execute(19, new Object[] {A1078ContratoGestor_ContratoCod});
         if ( (pr_default.getStatus(19) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato Gestor_Contrato'.", "ForeignKeyNotFound", 1, "CONTRATOGESTOR_CONTRATOCOD");
            AnyError = 1;
            GX_FocusControl = edtContratoGestor_ContratoCod_Internalname;
         }
         A1136ContratoGestor_ContratadaCod = T003521_A1136ContratoGestor_ContratadaCod[0];
         n1136ContratoGestor_ContratadaCod = T003521_n1136ContratoGestor_ContratadaCod[0];
         A1446ContratoGestor_ContratadaAreaCod = T003521_A1446ContratoGestor_ContratadaAreaCod[0];
         n1446ContratoGestor_ContratadaAreaCod = T003521_n1446ContratoGestor_ContratadaAreaCod[0];
         pr_default.close(19);
         /* Using cursor T003522 */
         pr_default.execute(20, new Object[] {n1136ContratoGestor_ContratadaCod, A1136ContratoGestor_ContratadaCod});
         if ( (pr_default.getStatus(20) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato Gestor_Contrato'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A1223ContratoGestor_ContratadaSigla = T003522_A1223ContratoGestor_ContratadaSigla[0];
         n1223ContratoGestor_ContratadaSigla = T003522_n1223ContratoGestor_ContratadaSigla[0];
         A1137ContratoGestor_ContratadaTipo = T003522_A1137ContratoGestor_ContratadaTipo[0];
         cmbContratoGestor_ContratadaTipo.CurrentValue = A1137ContratoGestor_ContratadaTipo;
         n1137ContratoGestor_ContratadaTipo = T003522_n1137ContratoGestor_ContratadaTipo[0];
         pr_default.close(20);
         /* Using cursor T003523 */
         pr_default.execute(21, new Object[] {n1446ContratoGestor_ContratadaAreaCod, A1446ContratoGestor_ContratadaAreaCod});
         if ( (pr_default.getStatus(21) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Area de Trabalho'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A1447ContratoGestor_ContratadaAreaDes = T003523_A1447ContratoGestor_ContratadaAreaDes[0];
         n1447ContratoGestor_ContratadaAreaDes = T003523_n1447ContratoGestor_ContratadaAreaDes[0];
         pr_default.close(21);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A1136ContratoGestor_ContratadaCod = 0;
            n1136ContratoGestor_ContratadaCod = false;
            A1446ContratoGestor_ContratadaAreaCod = 0;
            n1446ContratoGestor_ContratadaAreaCod = false;
            A1223ContratoGestor_ContratadaSigla = "";
            n1223ContratoGestor_ContratadaSigla = false;
            A1137ContratoGestor_ContratadaTipo = "";
            n1137ContratoGestor_ContratadaTipo = false;
            cmbContratoGestor_ContratadaTipo.CurrentValue = A1137ContratoGestor_ContratadaTipo;
            A1447ContratoGestor_ContratadaAreaDes = "";
            n1447ContratoGestor_ContratadaAreaDes = false;
         }
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A1136ContratoGestor_ContratadaCod), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A1446ContratoGestor_ContratadaAreaCod), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.RTrim( A1223ContratoGestor_ContratadaSigla));
         cmbContratoGestor_ContratadaTipo.CurrentValue = A1137ContratoGestor_ContratadaTipo;
         isValidOutput.Add(cmbContratoGestor_ContratadaTipo);
         isValidOutput.Add(A1447ContratoGestor_ContratadaAreaDes);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Contratogestor_usuariocod( GXCombobox cmbGX_Parm1 ,
                                                   int GX_Parm2 ,
                                                   int GX_Parm3 ,
                                                   int GX_Parm4 ,
                                                   int GX_Parm5 ,
                                                   GXCombobox cmbGX_Parm6 ,
                                                   bool GX_Parm7 ,
                                                   String GX_Parm8 ,
                                                   bool GX_Parm9 )
      {
         cmbContratoGestor_ContratadaTipo = cmbGX_Parm1;
         A1137ContratoGestor_ContratadaTipo = cmbContratoGestor_ContratadaTipo.CurrentValue;
         n1137ContratoGestor_ContratadaTipo = false;
         cmbContratoGestor_ContratadaTipo.CurrentValue = A1137ContratoGestor_ContratadaTipo;
         A1078ContratoGestor_ContratoCod = GX_Parm2;
         A1079ContratoGestor_UsuarioCod = GX_Parm3;
         A1080ContratoGestor_UsuarioPesCod = GX_Parm4;
         n1080ContratoGestor_UsuarioPesCod = false;
         A1446ContratoGestor_ContratadaAreaCod = GX_Parm5;
         n1446ContratoGestor_ContratadaAreaCod = false;
         cmbContratoGestor_Tipo = cmbGX_Parm6;
         A2009ContratoGestor_Tipo = (short)(NumberUtil.Val( cmbContratoGestor_Tipo.CurrentValue, "."));
         n2009ContratoGestor_Tipo = false;
         cmbContratoGestor_Tipo.CurrentValue = StringUtil.Str( (decimal)(A2009ContratoGestor_Tipo), 1, 0);
         A2033ContratoGestor_UsuarioAtv = GX_Parm7;
         n2033ContratoGestor_UsuarioAtv = false;
         A1081ContratoGestor_UsuarioPesNom = GX_Parm8;
         n1081ContratoGestor_UsuarioPesNom = false;
         A1135ContratoGestor_UsuarioEhContratante = GX_Parm9;
         context.wbHandled = 1;
         AfterKeyLoadScreen( ) ;
         Draw( ) ;
         SendCloseFormHiddens( ) ;
         /* Using cursor T003524 */
         pr_default.execute(22, new Object[] {A1079ContratoGestor_UsuarioCod});
         if ( (pr_default.getStatus(22) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato Gestor_Usuario'.", "ForeignKeyNotFound", 1, "CONTRATOGESTOR_USUARIOCOD");
            AnyError = 1;
            GX_FocusControl = edtContratoGestor_UsuarioCod_Internalname;
         }
         A2033ContratoGestor_UsuarioAtv = T003524_A2033ContratoGestor_UsuarioAtv[0];
         n2033ContratoGestor_UsuarioAtv = T003524_n2033ContratoGestor_UsuarioAtv[0];
         A1080ContratoGestor_UsuarioPesCod = T003524_A1080ContratoGestor_UsuarioPesCod[0];
         n1080ContratoGestor_UsuarioPesCod = T003524_n1080ContratoGestor_UsuarioPesCod[0];
         pr_default.close(22);
         /* Using cursor T003525 */
         pr_default.execute(23, new Object[] {n1080ContratoGestor_UsuarioPesCod, A1080ContratoGestor_UsuarioPesCod});
         if ( (pr_default.getStatus(23) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Pessoa'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A1081ContratoGestor_UsuarioPesNom = T003525_A1081ContratoGestor_UsuarioPesNom[0];
         n1081ContratoGestor_UsuarioPesNom = T003525_n1081ContratoGestor_UsuarioPesNom[0];
         pr_default.close(23);
         GXt_boolean1 = A1135ContratoGestor_UsuarioEhContratante;
         new prc_usuarioehcontratantedaarea(context ).execute( ref  A1446ContratoGestor_ContratadaAreaCod, ref  A1079ContratoGestor_UsuarioCod, out  GXt_boolean1) ;
         A1135ContratoGestor_UsuarioEhContratante = GXt_boolean1;
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A1136ContratoGestor_ContratadaCod = 0;
            n1136ContratoGestor_ContratadaCod = false;
            A1446ContratoGestor_ContratadaAreaCod = 0;
            n1446ContratoGestor_ContratadaAreaCod = false;
            A1223ContratoGestor_ContratadaSigla = "";
            n1223ContratoGestor_ContratadaSigla = false;
            A1137ContratoGestor_ContratadaTipo = "";
            n1137ContratoGestor_ContratadaTipo = false;
            cmbContratoGestor_ContratadaTipo.CurrentValue = A1137ContratoGestor_ContratadaTipo;
            A1447ContratoGestor_ContratadaAreaDes = "";
            n1447ContratoGestor_ContratadaAreaDes = false;
            A2033ContratoGestor_UsuarioAtv = false;
            n2033ContratoGestor_UsuarioAtv = false;
            A1080ContratoGestor_UsuarioPesCod = 0;
            n1080ContratoGestor_UsuarioPesCod = false;
            A1081ContratoGestor_UsuarioPesNom = "";
            n1081ContratoGestor_UsuarioPesNom = false;
         }
         cmbContratoGestor_Tipo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A2009ContratoGestor_Tipo), 1, 0));
         isValidOutput.Add(cmbContratoGestor_Tipo);
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A1136ContratoGestor_ContratadaCod), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A1446ContratoGestor_ContratadaAreaCod), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.RTrim( A1223ContratoGestor_ContratadaSigla));
         cmbContratoGestor_ContratadaTipo.CurrentValue = A1137ContratoGestor_ContratadaTipo;
         isValidOutput.Add(cmbContratoGestor_ContratadaTipo);
         isValidOutput.Add(A1447ContratoGestor_ContratadaAreaDes);
         isValidOutput.Add(A2033ContratoGestor_UsuarioAtv);
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A1080ContratoGestor_UsuarioPesCod), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.RTrim( A1081ContratoGestor_UsuarioPesNom));
         isValidOutput.Add(A1135ContratoGestor_UsuarioEhContratante);
         isValidOutput.Add(StringUtil.RTrim( Gx_mode));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1078ContratoGestor_ContratoCod), 6, 0, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1079ContratoGestor_UsuarioCod), 6, 0, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z2009ContratoGestor_Tipo), 1, 0, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1136ContratoGestor_ContratadaCod), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1446ContratoGestor_ContratadaAreaCod), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.RTrim( Z1223ContratoGestor_ContratadaSigla));
         isValidOutput.Add(StringUtil.RTrim( Z1137ContratoGestor_ContratadaTipo));
         isValidOutput.Add(Z1447ContratoGestor_ContratadaAreaDes);
         isValidOutput.Add(Z2033ContratoGestor_UsuarioAtv);
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1080ContratoGestor_UsuarioPesCod), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.RTrim( Z1081ContratoGestor_UsuarioPesNom));
         isValidOutput.Add(Z1135ContratoGestor_UsuarioEhContratante);
         isValidOutput.Add(imgBtn_delete2_Enabled);
         isValidOutput.Add(imgBtn_enter2_Enabled);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(19);
         pr_default.close(22);
         pr_default.close(20);
         pr_default.close(21);
         pr_default.close(23);
      }

      public override void initialize( )
      {
         sPrefix = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         A1137ContratoGestor_ContratadaTipo = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtn_enter_Jsonclick = "";
         bttBtn_cancel_Jsonclick = "";
         bttBtn_delete_Jsonclick = "";
         lblTextblockcontratogestor_contratocod_Jsonclick = "";
         lblTextblockcontratogestor_contratadacod_Jsonclick = "";
         lblTextblockcontratogestor_contratadasigla_Jsonclick = "";
         A1223ContratoGestor_ContratadaSigla = "";
         lblTextblockcontratogestor_contratadatipo_Jsonclick = "";
         lblTextblockcontratogestor_contratadaareacod_Jsonclick = "";
         lblTextblockcontratogestor_contratadaareades_Jsonclick = "";
         A1447ContratoGestor_ContratadaAreaDes = "";
         lblTextblockcontratogestor_usuariocod_Jsonclick = "";
         lblTextblockcontratogestor_usuariopescod_Jsonclick = "";
         lblTextblockcontratogestor_usuariopesnom_Jsonclick = "";
         A1081ContratoGestor_UsuarioPesNom = "";
         lblTextblockcontratogestor_usuarioehcontratante_Jsonclick = "";
         lblTextblockcontratogestor_usuarioatv_Jsonclick = "";
         lblTextblockcontratogestor_tipo_Jsonclick = "";
         imgBtn_first_Jsonclick = "";
         imgBtn_first_separator_Jsonclick = "";
         imgBtn_previous_Jsonclick = "";
         imgBtn_previous_separator_Jsonclick = "";
         imgBtn_next_Jsonclick = "";
         imgBtn_next_separator_Jsonclick = "";
         imgBtn_last_Jsonclick = "";
         imgBtn_last_separator_Jsonclick = "";
         imgBtn_select_Jsonclick = "";
         imgBtn_select_separator_Jsonclick = "";
         imgBtn_enter2_Jsonclick = "";
         imgBtn_enter2_separator_Jsonclick = "";
         imgBtn_cancel2_Jsonclick = "";
         imgBtn_cancel2_separator_Jsonclick = "";
         imgBtn_delete2_Jsonclick = "";
         imgBtn_delete2_separator_Jsonclick = "";
         Gx_mode = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         Z1223ContratoGestor_ContratadaSigla = "";
         Z1137ContratoGestor_ContratadaTipo = "";
         Z1447ContratoGestor_ContratadaAreaDes = "";
         Z1081ContratoGestor_UsuarioPesNom = "";
         T00359_A1223ContratoGestor_ContratadaSigla = new String[] {""} ;
         T00359_n1223ContratoGestor_ContratadaSigla = new bool[] {false} ;
         T00359_A1137ContratoGestor_ContratadaTipo = new String[] {""} ;
         T00359_n1137ContratoGestor_ContratadaTipo = new bool[] {false} ;
         T00359_A1447ContratoGestor_ContratadaAreaDes = new String[] {""} ;
         T00359_n1447ContratoGestor_ContratadaAreaDes = new bool[] {false} ;
         T00359_A1081ContratoGestor_UsuarioPesNom = new String[] {""} ;
         T00359_n1081ContratoGestor_UsuarioPesNom = new bool[] {false} ;
         T00359_A2033ContratoGestor_UsuarioAtv = new bool[] {false} ;
         T00359_n2033ContratoGestor_UsuarioAtv = new bool[] {false} ;
         T00359_A2009ContratoGestor_Tipo = new short[1] ;
         T00359_n2009ContratoGestor_Tipo = new bool[] {false} ;
         T00359_A1078ContratoGestor_ContratoCod = new int[1] ;
         T00359_A1079ContratoGestor_UsuarioCod = new int[1] ;
         T00359_A1136ContratoGestor_ContratadaCod = new int[1] ;
         T00359_n1136ContratoGestor_ContratadaCod = new bool[] {false} ;
         T00359_A1446ContratoGestor_ContratadaAreaCod = new int[1] ;
         T00359_n1446ContratoGestor_ContratadaAreaCod = new bool[] {false} ;
         T00359_A1080ContratoGestor_UsuarioPesCod = new int[1] ;
         T00359_n1080ContratoGestor_UsuarioPesCod = new bool[] {false} ;
         T00354_A1136ContratoGestor_ContratadaCod = new int[1] ;
         T00354_n1136ContratoGestor_ContratadaCod = new bool[] {false} ;
         T00354_A1446ContratoGestor_ContratadaAreaCod = new int[1] ;
         T00354_n1446ContratoGestor_ContratadaAreaCod = new bool[] {false} ;
         T00356_A1223ContratoGestor_ContratadaSigla = new String[] {""} ;
         T00356_n1223ContratoGestor_ContratadaSigla = new bool[] {false} ;
         T00356_A1137ContratoGestor_ContratadaTipo = new String[] {""} ;
         T00356_n1137ContratoGestor_ContratadaTipo = new bool[] {false} ;
         T00357_A1447ContratoGestor_ContratadaAreaDes = new String[] {""} ;
         T00357_n1447ContratoGestor_ContratadaAreaDes = new bool[] {false} ;
         T00355_A2033ContratoGestor_UsuarioAtv = new bool[] {false} ;
         T00355_n2033ContratoGestor_UsuarioAtv = new bool[] {false} ;
         T00355_A1080ContratoGestor_UsuarioPesCod = new int[1] ;
         T00355_n1080ContratoGestor_UsuarioPesCod = new bool[] {false} ;
         T00358_A1081ContratoGestor_UsuarioPesNom = new String[] {""} ;
         T00358_n1081ContratoGestor_UsuarioPesNom = new bool[] {false} ;
         T003510_A1136ContratoGestor_ContratadaCod = new int[1] ;
         T003510_n1136ContratoGestor_ContratadaCod = new bool[] {false} ;
         T003510_A1446ContratoGestor_ContratadaAreaCod = new int[1] ;
         T003510_n1446ContratoGestor_ContratadaAreaCod = new bool[] {false} ;
         T003511_A1223ContratoGestor_ContratadaSigla = new String[] {""} ;
         T003511_n1223ContratoGestor_ContratadaSigla = new bool[] {false} ;
         T003511_A1137ContratoGestor_ContratadaTipo = new String[] {""} ;
         T003511_n1137ContratoGestor_ContratadaTipo = new bool[] {false} ;
         T003512_A1447ContratoGestor_ContratadaAreaDes = new String[] {""} ;
         T003512_n1447ContratoGestor_ContratadaAreaDes = new bool[] {false} ;
         T003513_A2033ContratoGestor_UsuarioAtv = new bool[] {false} ;
         T003513_n2033ContratoGestor_UsuarioAtv = new bool[] {false} ;
         T003513_A1080ContratoGestor_UsuarioPesCod = new int[1] ;
         T003513_n1080ContratoGestor_UsuarioPesCod = new bool[] {false} ;
         T003514_A1081ContratoGestor_UsuarioPesNom = new String[] {""} ;
         T003514_n1081ContratoGestor_UsuarioPesNom = new bool[] {false} ;
         T003515_A1078ContratoGestor_ContratoCod = new int[1] ;
         T003515_A1079ContratoGestor_UsuarioCod = new int[1] ;
         T00353_A2009ContratoGestor_Tipo = new short[1] ;
         T00353_n2009ContratoGestor_Tipo = new bool[] {false} ;
         T00353_A1078ContratoGestor_ContratoCod = new int[1] ;
         T00353_A1079ContratoGestor_UsuarioCod = new int[1] ;
         sMode129 = "";
         T003516_A1078ContratoGestor_ContratoCod = new int[1] ;
         T003516_A1079ContratoGestor_UsuarioCod = new int[1] ;
         T003517_A1078ContratoGestor_ContratoCod = new int[1] ;
         T003517_A1079ContratoGestor_UsuarioCod = new int[1] ;
         T00352_A2009ContratoGestor_Tipo = new short[1] ;
         T00352_n2009ContratoGestor_Tipo = new bool[] {false} ;
         T00352_A1078ContratoGestor_ContratoCod = new int[1] ;
         T00352_A1079ContratoGestor_UsuarioCod = new int[1] ;
         T003521_A1136ContratoGestor_ContratadaCod = new int[1] ;
         T003521_n1136ContratoGestor_ContratadaCod = new bool[] {false} ;
         T003521_A1446ContratoGestor_ContratadaAreaCod = new int[1] ;
         T003521_n1446ContratoGestor_ContratadaAreaCod = new bool[] {false} ;
         T003522_A1223ContratoGestor_ContratadaSigla = new String[] {""} ;
         T003522_n1223ContratoGestor_ContratadaSigla = new bool[] {false} ;
         T003522_A1137ContratoGestor_ContratadaTipo = new String[] {""} ;
         T003522_n1137ContratoGestor_ContratadaTipo = new bool[] {false} ;
         T003523_A1447ContratoGestor_ContratadaAreaDes = new String[] {""} ;
         T003523_n1447ContratoGestor_ContratadaAreaDes = new bool[] {false} ;
         T003524_A2033ContratoGestor_UsuarioAtv = new bool[] {false} ;
         T003524_n2033ContratoGestor_UsuarioAtv = new bool[] {false} ;
         T003524_A1080ContratoGestor_UsuarioPesCod = new int[1] ;
         T003524_n1080ContratoGestor_UsuarioPesCod = new bool[] {false} ;
         T003525_A1081ContratoGestor_UsuarioPesNom = new String[] {""} ;
         T003525_n1081ContratoGestor_UsuarioPesNom = new bool[] {false} ;
         T003526_A1078ContratoGestor_ContratoCod = new int[1] ;
         T003526_A1079ContratoGestor_UsuarioCod = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         isValidOutput = new GxUnknownObjectCollection();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contratogestor__default(),
            new Object[][] {
                new Object[] {
               T00352_A2009ContratoGestor_Tipo, T00352_n2009ContratoGestor_Tipo, T00352_A1078ContratoGestor_ContratoCod, T00352_A1079ContratoGestor_UsuarioCod
               }
               , new Object[] {
               T00353_A2009ContratoGestor_Tipo, T00353_n2009ContratoGestor_Tipo, T00353_A1078ContratoGestor_ContratoCod, T00353_A1079ContratoGestor_UsuarioCod
               }
               , new Object[] {
               T00354_A1136ContratoGestor_ContratadaCod, T00354_n1136ContratoGestor_ContratadaCod, T00354_A1446ContratoGestor_ContratadaAreaCod, T00354_n1446ContratoGestor_ContratadaAreaCod
               }
               , new Object[] {
               T00355_A2033ContratoGestor_UsuarioAtv, T00355_n2033ContratoGestor_UsuarioAtv, T00355_A1080ContratoGestor_UsuarioPesCod, T00355_n1080ContratoGestor_UsuarioPesCod
               }
               , new Object[] {
               T00356_A1223ContratoGestor_ContratadaSigla, T00356_n1223ContratoGestor_ContratadaSigla, T00356_A1137ContratoGestor_ContratadaTipo, T00356_n1137ContratoGestor_ContratadaTipo
               }
               , new Object[] {
               T00357_A1447ContratoGestor_ContratadaAreaDes, T00357_n1447ContratoGestor_ContratadaAreaDes
               }
               , new Object[] {
               T00358_A1081ContratoGestor_UsuarioPesNom, T00358_n1081ContratoGestor_UsuarioPesNom
               }
               , new Object[] {
               T00359_A1223ContratoGestor_ContratadaSigla, T00359_n1223ContratoGestor_ContratadaSigla, T00359_A1137ContratoGestor_ContratadaTipo, T00359_n1137ContratoGestor_ContratadaTipo, T00359_A1447ContratoGestor_ContratadaAreaDes, T00359_n1447ContratoGestor_ContratadaAreaDes, T00359_A1081ContratoGestor_UsuarioPesNom, T00359_n1081ContratoGestor_UsuarioPesNom, T00359_A2033ContratoGestor_UsuarioAtv, T00359_n2033ContratoGestor_UsuarioAtv,
               T00359_A2009ContratoGestor_Tipo, T00359_n2009ContratoGestor_Tipo, T00359_A1078ContratoGestor_ContratoCod, T00359_A1079ContratoGestor_UsuarioCod, T00359_A1136ContratoGestor_ContratadaCod, T00359_n1136ContratoGestor_ContratadaCod, T00359_A1446ContratoGestor_ContratadaAreaCod, T00359_n1446ContratoGestor_ContratadaAreaCod, T00359_A1080ContratoGestor_UsuarioPesCod, T00359_n1080ContratoGestor_UsuarioPesCod
               }
               , new Object[] {
               T003510_A1136ContratoGestor_ContratadaCod, T003510_n1136ContratoGestor_ContratadaCod, T003510_A1446ContratoGestor_ContratadaAreaCod, T003510_n1446ContratoGestor_ContratadaAreaCod
               }
               , new Object[] {
               T003511_A1223ContratoGestor_ContratadaSigla, T003511_n1223ContratoGestor_ContratadaSigla, T003511_A1137ContratoGestor_ContratadaTipo, T003511_n1137ContratoGestor_ContratadaTipo
               }
               , new Object[] {
               T003512_A1447ContratoGestor_ContratadaAreaDes, T003512_n1447ContratoGestor_ContratadaAreaDes
               }
               , new Object[] {
               T003513_A2033ContratoGestor_UsuarioAtv, T003513_n2033ContratoGestor_UsuarioAtv, T003513_A1080ContratoGestor_UsuarioPesCod, T003513_n1080ContratoGestor_UsuarioPesCod
               }
               , new Object[] {
               T003514_A1081ContratoGestor_UsuarioPesNom, T003514_n1081ContratoGestor_UsuarioPesNom
               }
               , new Object[] {
               T003515_A1078ContratoGestor_ContratoCod, T003515_A1079ContratoGestor_UsuarioCod
               }
               , new Object[] {
               T003516_A1078ContratoGestor_ContratoCod, T003516_A1079ContratoGestor_UsuarioCod
               }
               , new Object[] {
               T003517_A1078ContratoGestor_ContratoCod, T003517_A1079ContratoGestor_UsuarioCod
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T003521_A1136ContratoGestor_ContratadaCod, T003521_n1136ContratoGestor_ContratadaCod, T003521_A1446ContratoGestor_ContratadaAreaCod, T003521_n1446ContratoGestor_ContratadaAreaCod
               }
               , new Object[] {
               T003522_A1223ContratoGestor_ContratadaSigla, T003522_n1223ContratoGestor_ContratadaSigla, T003522_A1137ContratoGestor_ContratadaTipo, T003522_n1137ContratoGestor_ContratadaTipo
               }
               , new Object[] {
               T003523_A1447ContratoGestor_ContratadaAreaDes, T003523_n1447ContratoGestor_ContratadaAreaDes
               }
               , new Object[] {
               T003524_A2033ContratoGestor_UsuarioAtv, T003524_n2033ContratoGestor_UsuarioAtv, T003524_A1080ContratoGestor_UsuarioPesCod, T003524_n1080ContratoGestor_UsuarioPesCod
               }
               , new Object[] {
               T003525_A1081ContratoGestor_UsuarioPesNom, T003525_n1081ContratoGestor_UsuarioPesNom
               }
               , new Object[] {
               T003526_A1078ContratoGestor_ContratoCod, T003526_A1079ContratoGestor_UsuarioCod
               }
            }
         );
      }

      private short Z2009ContratoGestor_Tipo ;
      private short GxWebError ;
      private short gxcookieaux ;
      private short A2009ContratoGestor_Tipo ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short GX_JID ;
      private short RcdFound129 ;
      private short Gx_BScreen ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int Z1078ContratoGestor_ContratoCod ;
      private int Z1079ContratoGestor_UsuarioCod ;
      private int A1446ContratoGestor_ContratadaAreaCod ;
      private int A1079ContratoGestor_UsuarioCod ;
      private int A1078ContratoGestor_ContratoCod ;
      private int A1136ContratoGestor_ContratadaCod ;
      private int A1080ContratoGestor_UsuarioPesCod ;
      private int trnEnded ;
      private int bttBtn_enter_Visible ;
      private int bttBtn_cancel_Visible ;
      private int bttBtn_delete_Visible ;
      private int edtContratoGestor_ContratoCod_Enabled ;
      private int edtContratoGestor_ContratadaCod_Enabled ;
      private int edtContratoGestor_ContratadaSigla_Enabled ;
      private int edtContratoGestor_ContratadaAreaCod_Enabled ;
      private int edtContratoGestor_ContratadaAreaDes_Enabled ;
      private int edtContratoGestor_UsuarioCod_Enabled ;
      private int edtContratoGestor_UsuarioPesCod_Enabled ;
      private int edtContratoGestor_UsuarioPesNom_Enabled ;
      private int imgBtn_first_Visible ;
      private int imgBtn_first_separator_Visible ;
      private int imgBtn_previous_Visible ;
      private int imgBtn_previous_separator_Visible ;
      private int imgBtn_next_Visible ;
      private int imgBtn_next_separator_Visible ;
      private int imgBtn_last_Visible ;
      private int imgBtn_last_separator_Visible ;
      private int imgBtn_select_Visible ;
      private int imgBtn_select_separator_Visible ;
      private int imgBtn_enter2_Visible ;
      private int imgBtn_enter2_Enabled ;
      private int imgBtn_enter2_separator_Visible ;
      private int imgBtn_cancel2_Visible ;
      private int imgBtn_cancel2_separator_Visible ;
      private int imgBtn_delete2_Visible ;
      private int imgBtn_delete2_Enabled ;
      private int imgBtn_delete2_separator_Visible ;
      private int Z1136ContratoGestor_ContratadaCod ;
      private int Z1446ContratoGestor_ContratadaAreaCod ;
      private int Z1080ContratoGestor_UsuarioPesCod ;
      private int idxLst ;
      private String sPrefix ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String GXKey ;
      private String A1137ContratoGestor_ContratadaTipo ;
      private String chkContratoGestor_UsuarioEhContratante_Internalname ;
      private String chkContratoGestor_UsuarioAtv_Internalname ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtContratoGestor_ContratoCod_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String grpGroupdata_Internalname ;
      private String tblTable1_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String TempTags ;
      private String bttBtn_enter_Internalname ;
      private String bttBtn_enter_Jsonclick ;
      private String bttBtn_cancel_Internalname ;
      private String bttBtn_cancel_Jsonclick ;
      private String bttBtn_delete_Internalname ;
      private String bttBtn_delete_Jsonclick ;
      private String tblTable2_Internalname ;
      private String lblTextblockcontratogestor_contratocod_Internalname ;
      private String lblTextblockcontratogestor_contratocod_Jsonclick ;
      private String edtContratoGestor_ContratoCod_Jsonclick ;
      private String lblTextblockcontratogestor_contratadacod_Internalname ;
      private String lblTextblockcontratogestor_contratadacod_Jsonclick ;
      private String edtContratoGestor_ContratadaCod_Internalname ;
      private String edtContratoGestor_ContratadaCod_Jsonclick ;
      private String lblTextblockcontratogestor_contratadasigla_Internalname ;
      private String lblTextblockcontratogestor_contratadasigla_Jsonclick ;
      private String edtContratoGestor_ContratadaSigla_Internalname ;
      private String A1223ContratoGestor_ContratadaSigla ;
      private String edtContratoGestor_ContratadaSigla_Jsonclick ;
      private String lblTextblockcontratogestor_contratadatipo_Internalname ;
      private String lblTextblockcontratogestor_contratadatipo_Jsonclick ;
      private String cmbContratoGestor_ContratadaTipo_Internalname ;
      private String cmbContratoGestor_ContratadaTipo_Jsonclick ;
      private String lblTextblockcontratogestor_contratadaareacod_Internalname ;
      private String lblTextblockcontratogestor_contratadaareacod_Jsonclick ;
      private String edtContratoGestor_ContratadaAreaCod_Internalname ;
      private String edtContratoGestor_ContratadaAreaCod_Jsonclick ;
      private String lblTextblockcontratogestor_contratadaareades_Internalname ;
      private String lblTextblockcontratogestor_contratadaareades_Jsonclick ;
      private String edtContratoGestor_ContratadaAreaDes_Internalname ;
      private String edtContratoGestor_ContratadaAreaDes_Jsonclick ;
      private String lblTextblockcontratogestor_usuariocod_Internalname ;
      private String lblTextblockcontratogestor_usuariocod_Jsonclick ;
      private String edtContratoGestor_UsuarioCod_Internalname ;
      private String edtContratoGestor_UsuarioCod_Jsonclick ;
      private String lblTextblockcontratogestor_usuariopescod_Internalname ;
      private String lblTextblockcontratogestor_usuariopescod_Jsonclick ;
      private String edtContratoGestor_UsuarioPesCod_Internalname ;
      private String edtContratoGestor_UsuarioPesCod_Jsonclick ;
      private String lblTextblockcontratogestor_usuariopesnom_Internalname ;
      private String lblTextblockcontratogestor_usuariopesnom_Jsonclick ;
      private String edtContratoGestor_UsuarioPesNom_Internalname ;
      private String A1081ContratoGestor_UsuarioPesNom ;
      private String edtContratoGestor_UsuarioPesNom_Jsonclick ;
      private String lblTextblockcontratogestor_usuarioehcontratante_Internalname ;
      private String lblTextblockcontratogestor_usuarioehcontratante_Jsonclick ;
      private String lblTextblockcontratogestor_usuarioatv_Internalname ;
      private String lblTextblockcontratogestor_usuarioatv_Jsonclick ;
      private String lblTextblockcontratogestor_tipo_Internalname ;
      private String lblTextblockcontratogestor_tipo_Jsonclick ;
      private String cmbContratoGestor_Tipo_Internalname ;
      private String cmbContratoGestor_Tipo_Jsonclick ;
      private String tblTabletoolbar_Internalname ;
      private String divSectiontoolbar_Internalname ;
      private String imgBtn_first_Internalname ;
      private String imgBtn_first_Jsonclick ;
      private String imgBtn_first_separator_Internalname ;
      private String imgBtn_first_separator_Jsonclick ;
      private String imgBtn_previous_Internalname ;
      private String imgBtn_previous_Jsonclick ;
      private String imgBtn_previous_separator_Internalname ;
      private String imgBtn_previous_separator_Jsonclick ;
      private String imgBtn_next_Internalname ;
      private String imgBtn_next_Jsonclick ;
      private String imgBtn_next_separator_Internalname ;
      private String imgBtn_next_separator_Jsonclick ;
      private String imgBtn_last_Internalname ;
      private String imgBtn_last_Jsonclick ;
      private String imgBtn_last_separator_Internalname ;
      private String imgBtn_last_separator_Jsonclick ;
      private String imgBtn_select_Internalname ;
      private String imgBtn_select_Jsonclick ;
      private String imgBtn_select_separator_Internalname ;
      private String imgBtn_select_separator_Jsonclick ;
      private String imgBtn_enter2_Internalname ;
      private String imgBtn_enter2_Jsonclick ;
      private String imgBtn_enter2_separator_Internalname ;
      private String imgBtn_enter2_separator_Jsonclick ;
      private String imgBtn_cancel2_Internalname ;
      private String imgBtn_cancel2_Jsonclick ;
      private String imgBtn_cancel2_separator_Internalname ;
      private String imgBtn_cancel2_separator_Jsonclick ;
      private String imgBtn_delete2_Internalname ;
      private String imgBtn_delete2_Jsonclick ;
      private String imgBtn_delete2_separator_Internalname ;
      private String imgBtn_delete2_separator_Jsonclick ;
      private String Gx_mode ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String Z1223ContratoGestor_ContratadaSigla ;
      private String Z1137ContratoGestor_ContratadaTipo ;
      private String Z1081ContratoGestor_UsuarioPesNom ;
      private String sMode129 ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private bool entryPointCalled ;
      private bool n1446ContratoGestor_ContratadaAreaCod ;
      private bool n1136ContratoGestor_ContratadaCod ;
      private bool n1080ContratoGestor_UsuarioPesCod ;
      private bool toggleJsOutput ;
      private bool n1137ContratoGestor_ContratadaTipo ;
      private bool n2009ContratoGestor_Tipo ;
      private bool wbErr ;
      private bool A1135ContratoGestor_UsuarioEhContratante ;
      private bool A2033ContratoGestor_UsuarioAtv ;
      private bool n1223ContratoGestor_ContratadaSigla ;
      private bool n1447ContratoGestor_ContratadaAreaDes ;
      private bool n1081ContratoGestor_UsuarioPesNom ;
      private bool n2033ContratoGestor_UsuarioAtv ;
      private bool Z2033ContratoGestor_UsuarioAtv ;
      private bool Z1135ContratoGestor_UsuarioEhContratante ;
      private bool GXt_boolean1 ;
      private String A1447ContratoGestor_ContratadaAreaDes ;
      private String Z1447ContratoGestor_ContratadaAreaDes ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbContratoGestor_ContratadaTipo ;
      private GXCheckbox chkContratoGestor_UsuarioEhContratante ;
      private GXCheckbox chkContratoGestor_UsuarioAtv ;
      private GXCombobox cmbContratoGestor_Tipo ;
      private IDataStoreProvider pr_default ;
      private String[] T00359_A1223ContratoGestor_ContratadaSigla ;
      private bool[] T00359_n1223ContratoGestor_ContratadaSigla ;
      private String[] T00359_A1137ContratoGestor_ContratadaTipo ;
      private bool[] T00359_n1137ContratoGestor_ContratadaTipo ;
      private String[] T00359_A1447ContratoGestor_ContratadaAreaDes ;
      private bool[] T00359_n1447ContratoGestor_ContratadaAreaDes ;
      private String[] T00359_A1081ContratoGestor_UsuarioPesNom ;
      private bool[] T00359_n1081ContratoGestor_UsuarioPesNom ;
      private bool[] T00359_A2033ContratoGestor_UsuarioAtv ;
      private bool[] T00359_n2033ContratoGestor_UsuarioAtv ;
      private short[] T00359_A2009ContratoGestor_Tipo ;
      private bool[] T00359_n2009ContratoGestor_Tipo ;
      private int[] T00359_A1078ContratoGestor_ContratoCod ;
      private int[] T00359_A1079ContratoGestor_UsuarioCod ;
      private int[] T00359_A1136ContratoGestor_ContratadaCod ;
      private bool[] T00359_n1136ContratoGestor_ContratadaCod ;
      private int[] T00359_A1446ContratoGestor_ContratadaAreaCod ;
      private bool[] T00359_n1446ContratoGestor_ContratadaAreaCod ;
      private int[] T00359_A1080ContratoGestor_UsuarioPesCod ;
      private bool[] T00359_n1080ContratoGestor_UsuarioPesCod ;
      private int[] T00354_A1136ContratoGestor_ContratadaCod ;
      private bool[] T00354_n1136ContratoGestor_ContratadaCod ;
      private int[] T00354_A1446ContratoGestor_ContratadaAreaCod ;
      private bool[] T00354_n1446ContratoGestor_ContratadaAreaCod ;
      private String[] T00356_A1223ContratoGestor_ContratadaSigla ;
      private bool[] T00356_n1223ContratoGestor_ContratadaSigla ;
      private String[] T00356_A1137ContratoGestor_ContratadaTipo ;
      private bool[] T00356_n1137ContratoGestor_ContratadaTipo ;
      private String[] T00357_A1447ContratoGestor_ContratadaAreaDes ;
      private bool[] T00357_n1447ContratoGestor_ContratadaAreaDes ;
      private bool[] T00355_A2033ContratoGestor_UsuarioAtv ;
      private bool[] T00355_n2033ContratoGestor_UsuarioAtv ;
      private int[] T00355_A1080ContratoGestor_UsuarioPesCod ;
      private bool[] T00355_n1080ContratoGestor_UsuarioPesCod ;
      private String[] T00358_A1081ContratoGestor_UsuarioPesNom ;
      private bool[] T00358_n1081ContratoGestor_UsuarioPesNom ;
      private int[] T003510_A1136ContratoGestor_ContratadaCod ;
      private bool[] T003510_n1136ContratoGestor_ContratadaCod ;
      private int[] T003510_A1446ContratoGestor_ContratadaAreaCod ;
      private bool[] T003510_n1446ContratoGestor_ContratadaAreaCod ;
      private String[] T003511_A1223ContratoGestor_ContratadaSigla ;
      private bool[] T003511_n1223ContratoGestor_ContratadaSigla ;
      private String[] T003511_A1137ContratoGestor_ContratadaTipo ;
      private bool[] T003511_n1137ContratoGestor_ContratadaTipo ;
      private String[] T003512_A1447ContratoGestor_ContratadaAreaDes ;
      private bool[] T003512_n1447ContratoGestor_ContratadaAreaDes ;
      private bool[] T003513_A2033ContratoGestor_UsuarioAtv ;
      private bool[] T003513_n2033ContratoGestor_UsuarioAtv ;
      private int[] T003513_A1080ContratoGestor_UsuarioPesCod ;
      private bool[] T003513_n1080ContratoGestor_UsuarioPesCod ;
      private String[] T003514_A1081ContratoGestor_UsuarioPesNom ;
      private bool[] T003514_n1081ContratoGestor_UsuarioPesNom ;
      private int[] T003515_A1078ContratoGestor_ContratoCod ;
      private int[] T003515_A1079ContratoGestor_UsuarioCod ;
      private short[] T00353_A2009ContratoGestor_Tipo ;
      private bool[] T00353_n2009ContratoGestor_Tipo ;
      private int[] T00353_A1078ContratoGestor_ContratoCod ;
      private int[] T00353_A1079ContratoGestor_UsuarioCod ;
      private int[] T003516_A1078ContratoGestor_ContratoCod ;
      private int[] T003516_A1079ContratoGestor_UsuarioCod ;
      private int[] T003517_A1078ContratoGestor_ContratoCod ;
      private int[] T003517_A1079ContratoGestor_UsuarioCod ;
      private short[] T00352_A2009ContratoGestor_Tipo ;
      private bool[] T00352_n2009ContratoGestor_Tipo ;
      private int[] T00352_A1078ContratoGestor_ContratoCod ;
      private int[] T00352_A1079ContratoGestor_UsuarioCod ;
      private int[] T003521_A1136ContratoGestor_ContratadaCod ;
      private bool[] T003521_n1136ContratoGestor_ContratadaCod ;
      private int[] T003521_A1446ContratoGestor_ContratadaAreaCod ;
      private bool[] T003521_n1446ContratoGestor_ContratadaAreaCod ;
      private String[] T003522_A1223ContratoGestor_ContratadaSigla ;
      private bool[] T003522_n1223ContratoGestor_ContratadaSigla ;
      private String[] T003522_A1137ContratoGestor_ContratadaTipo ;
      private bool[] T003522_n1137ContratoGestor_ContratadaTipo ;
      private String[] T003523_A1447ContratoGestor_ContratadaAreaDes ;
      private bool[] T003523_n1447ContratoGestor_ContratadaAreaDes ;
      private bool[] T003524_A2033ContratoGestor_UsuarioAtv ;
      private bool[] T003524_n2033ContratoGestor_UsuarioAtv ;
      private int[] T003524_A1080ContratoGestor_UsuarioPesCod ;
      private bool[] T003524_n1080ContratoGestor_UsuarioPesCod ;
      private String[] T003525_A1081ContratoGestor_UsuarioPesNom ;
      private bool[] T003525_n1081ContratoGestor_UsuarioPesNom ;
      private int[] T003526_A1078ContratoGestor_ContratoCod ;
      private int[] T003526_A1079ContratoGestor_UsuarioCod ;
      private GXWebForm Form ;
   }

   public class contratogestor__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
         ,new UpdateCursor(def[16])
         ,new UpdateCursor(def[17])
         ,new UpdateCursor(def[18])
         ,new ForEachCursor(def[19])
         ,new ForEachCursor(def[20])
         ,new ForEachCursor(def[21])
         ,new ForEachCursor(def[22])
         ,new ForEachCursor(def[23])
         ,new ForEachCursor(def[24])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT00359 ;
          prmT00359 = new Object[] {
          new Object[] {"@ContratoGestor_ContratoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoGestor_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00354 ;
          prmT00354 = new Object[] {
          new Object[] {"@ContratoGestor_ContratoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00356 ;
          prmT00356 = new Object[] {
          new Object[] {"@ContratoGestor_ContratadaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00357 ;
          prmT00357 = new Object[] {
          new Object[] {"@ContratoGestor_ContratadaAreaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00355 ;
          prmT00355 = new Object[] {
          new Object[] {"@ContratoGestor_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00358 ;
          prmT00358 = new Object[] {
          new Object[] {"@ContratoGestor_UsuarioPesCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003510 ;
          prmT003510 = new Object[] {
          new Object[] {"@ContratoGestor_ContratoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003511 ;
          prmT003511 = new Object[] {
          new Object[] {"@ContratoGestor_ContratadaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003512 ;
          prmT003512 = new Object[] {
          new Object[] {"@ContratoGestor_ContratadaAreaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003513 ;
          prmT003513 = new Object[] {
          new Object[] {"@ContratoGestor_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003514 ;
          prmT003514 = new Object[] {
          new Object[] {"@ContratoGestor_UsuarioPesCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003515 ;
          prmT003515 = new Object[] {
          new Object[] {"@ContratoGestor_ContratoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoGestor_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00353 ;
          prmT00353 = new Object[] {
          new Object[] {"@ContratoGestor_ContratoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoGestor_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003516 ;
          prmT003516 = new Object[] {
          new Object[] {"@ContratoGestor_ContratoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoGestor_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003517 ;
          prmT003517 = new Object[] {
          new Object[] {"@ContratoGestor_ContratoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoGestor_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00352 ;
          prmT00352 = new Object[] {
          new Object[] {"@ContratoGestor_ContratoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoGestor_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003518 ;
          prmT003518 = new Object[] {
          new Object[] {"@ContratoGestor_Tipo",SqlDbType.SmallInt,1,0} ,
          new Object[] {"@ContratoGestor_ContratoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoGestor_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003519 ;
          prmT003519 = new Object[] {
          new Object[] {"@ContratoGestor_Tipo",SqlDbType.SmallInt,1,0} ,
          new Object[] {"@ContratoGestor_ContratoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoGestor_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003520 ;
          prmT003520 = new Object[] {
          new Object[] {"@ContratoGestor_ContratoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoGestor_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003526 ;
          prmT003526 = new Object[] {
          } ;
          Object[] prmT003521 ;
          prmT003521 = new Object[] {
          new Object[] {"@ContratoGestor_ContratoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003522 ;
          prmT003522 = new Object[] {
          new Object[] {"@ContratoGestor_ContratadaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003523 ;
          prmT003523 = new Object[] {
          new Object[] {"@ContratoGestor_ContratadaAreaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003524 ;
          prmT003524 = new Object[] {
          new Object[] {"@ContratoGestor_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003525 ;
          prmT003525 = new Object[] {
          new Object[] {"@ContratoGestor_UsuarioPesCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T00352", "SELECT [ContratoGestor_Tipo], [ContratoGestor_ContratoCod] AS ContratoGestor_ContratoCod, [ContratoGestor_UsuarioCod] AS ContratoGestor_UsuarioCod FROM [ContratoGestor] WITH (UPDLOCK) WHERE [ContratoGestor_ContratoCod] = @ContratoGestor_ContratoCod AND [ContratoGestor_UsuarioCod] = @ContratoGestor_UsuarioCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT00352,1,0,true,false )
             ,new CursorDef("T00353", "SELECT [ContratoGestor_Tipo], [ContratoGestor_ContratoCod] AS ContratoGestor_ContratoCod, [ContratoGestor_UsuarioCod] AS ContratoGestor_UsuarioCod FROM [ContratoGestor] WITH (NOLOCK) WHERE [ContratoGestor_ContratoCod] = @ContratoGestor_ContratoCod AND [ContratoGestor_UsuarioCod] = @ContratoGestor_UsuarioCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT00353,1,0,true,false )
             ,new CursorDef("T00354", "SELECT [Contratada_Codigo] AS ContratoGestor_ContratadaCod, [Contrato_AreaTrabalhoCod] AS ContratoGestor_ContratadaAreaCod FROM [Contrato] WITH (NOLOCK) WHERE [Contrato_Codigo] = @ContratoGestor_ContratoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT00354,1,0,true,false )
             ,new CursorDef("T00355", "SELECT [Usuario_Ativo] AS ContratoGestor_UsuarioAtv, [Usuario_PessoaCod] AS ContratoGestor_UsuarioPesCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @ContratoGestor_UsuarioCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT00355,1,0,true,false )
             ,new CursorDef("T00356", "SELECT [Contratada_Sigla] AS ContratoGestor_ContratadaSigla, [Contratada_TipoFabrica] AS ContratoGestor_ContratadaTipo FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_Codigo] = @ContratoGestor_ContratadaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT00356,1,0,true,false )
             ,new CursorDef("T00357", "SELECT [AreaTrabalho_Descricao] AS ContratoGestor_ContratadaAreaDes FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @ContratoGestor_ContratadaAreaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT00357,1,0,true,false )
             ,new CursorDef("T00358", "SELECT [Pessoa_Nome] AS ContratoGestor_UsuarioPesNom FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @ContratoGestor_UsuarioPesCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT00358,1,0,true,false )
             ,new CursorDef("T00359", "SELECT T3.[Contratada_Sigla] AS ContratoGestor_ContratadaSigla, T3.[Contratada_TipoFabrica] AS ContratoGestor_ContratadaTipo, T4.[AreaTrabalho_Descricao] AS ContratoGestor_ContratadaAreaDes, T6.[Pessoa_Nome] AS ContratoGestor_UsuarioPesNom, T5.[Usuario_Ativo] AS ContratoGestor_UsuarioAtv, TM1.[ContratoGestor_Tipo], TM1.[ContratoGestor_ContratoCod] AS ContratoGestor_ContratoCod, TM1.[ContratoGestor_UsuarioCod] AS ContratoGestor_UsuarioCod, T2.[Contratada_Codigo] AS ContratoGestor_ContratadaCod, T2.[Contrato_AreaTrabalhoCod] AS ContratoGestor_ContratadaAreaCod, T5.[Usuario_PessoaCod] AS ContratoGestor_UsuarioPesCod FROM ((((([ContratoGestor] TM1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = TM1.[ContratoGestor_ContratoCod]) LEFT JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T2.[Contratada_Codigo]) LEFT JOIN [AreaTrabalho] T4 WITH (NOLOCK) ON T4.[AreaTrabalho_Codigo] = T2.[Contrato_AreaTrabalhoCod]) INNER JOIN [Usuario] T5 WITH (NOLOCK) ON T5.[Usuario_Codigo] = TM1.[ContratoGestor_UsuarioCod]) LEFT JOIN [Pessoa] T6 WITH (NOLOCK) ON T6.[Pessoa_Codigo] = T5.[Usuario_PessoaCod]) WHERE TM1.[ContratoGestor_ContratoCod] = @ContratoGestor_ContratoCod and TM1.[ContratoGestor_UsuarioCod] = @ContratoGestor_UsuarioCod ORDER BY TM1.[ContratoGestor_ContratoCod], TM1.[ContratoGestor_UsuarioCod]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT00359,100,0,true,false )
             ,new CursorDef("T003510", "SELECT [Contratada_Codigo] AS ContratoGestor_ContratadaCod, [Contrato_AreaTrabalhoCod] AS ContratoGestor_ContratadaAreaCod FROM [Contrato] WITH (NOLOCK) WHERE [Contrato_Codigo] = @ContratoGestor_ContratoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003510,1,0,true,false )
             ,new CursorDef("T003511", "SELECT [Contratada_Sigla] AS ContratoGestor_ContratadaSigla, [Contratada_TipoFabrica] AS ContratoGestor_ContratadaTipo FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_Codigo] = @ContratoGestor_ContratadaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003511,1,0,true,false )
             ,new CursorDef("T003512", "SELECT [AreaTrabalho_Descricao] AS ContratoGestor_ContratadaAreaDes FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @ContratoGestor_ContratadaAreaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003512,1,0,true,false )
             ,new CursorDef("T003513", "SELECT [Usuario_Ativo] AS ContratoGestor_UsuarioAtv, [Usuario_PessoaCod] AS ContratoGestor_UsuarioPesCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @ContratoGestor_UsuarioCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003513,1,0,true,false )
             ,new CursorDef("T003514", "SELECT [Pessoa_Nome] AS ContratoGestor_UsuarioPesNom FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @ContratoGestor_UsuarioPesCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003514,1,0,true,false )
             ,new CursorDef("T003515", "SELECT [ContratoGestor_ContratoCod] AS ContratoGestor_ContratoCod, [ContratoGestor_UsuarioCod] AS ContratoGestor_UsuarioCod FROM [ContratoGestor] WITH (NOLOCK) WHERE [ContratoGestor_ContratoCod] = @ContratoGestor_ContratoCod AND [ContratoGestor_UsuarioCod] = @ContratoGestor_UsuarioCod  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT003515,1,0,true,false )
             ,new CursorDef("T003516", "SELECT TOP 1 [ContratoGestor_ContratoCod] AS ContratoGestor_ContratoCod, [ContratoGestor_UsuarioCod] AS ContratoGestor_UsuarioCod FROM [ContratoGestor] WITH (NOLOCK) WHERE ( [ContratoGestor_ContratoCod] > @ContratoGestor_ContratoCod or [ContratoGestor_ContratoCod] = @ContratoGestor_ContratoCod and [ContratoGestor_UsuarioCod] > @ContratoGestor_UsuarioCod) ORDER BY [ContratoGestor_ContratoCod], [ContratoGestor_UsuarioCod]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT003516,1,0,true,true )
             ,new CursorDef("T003517", "SELECT TOP 1 [ContratoGestor_ContratoCod] AS ContratoGestor_ContratoCod, [ContratoGestor_UsuarioCod] AS ContratoGestor_UsuarioCod FROM [ContratoGestor] WITH (NOLOCK) WHERE ( [ContratoGestor_ContratoCod] < @ContratoGestor_ContratoCod or [ContratoGestor_ContratoCod] = @ContratoGestor_ContratoCod and [ContratoGestor_UsuarioCod] < @ContratoGestor_UsuarioCod) ORDER BY [ContratoGestor_ContratoCod] DESC, [ContratoGestor_UsuarioCod] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT003517,1,0,true,true )
             ,new CursorDef("T003518", "INSERT INTO [ContratoGestor]([ContratoGestor_Tipo], [ContratoGestor_ContratoCod], [ContratoGestor_UsuarioCod]) VALUES(@ContratoGestor_Tipo, @ContratoGestor_ContratoCod, @ContratoGestor_UsuarioCod)", GxErrorMask.GX_NOMASK,prmT003518)
             ,new CursorDef("T003519", "UPDATE [ContratoGestor] SET [ContratoGestor_Tipo]=@ContratoGestor_Tipo  WHERE [ContratoGestor_ContratoCod] = @ContratoGestor_ContratoCod AND [ContratoGestor_UsuarioCod] = @ContratoGestor_UsuarioCod", GxErrorMask.GX_NOMASK,prmT003519)
             ,new CursorDef("T003520", "DELETE FROM [ContratoGestor]  WHERE [ContratoGestor_ContratoCod] = @ContratoGestor_ContratoCod AND [ContratoGestor_UsuarioCod] = @ContratoGestor_UsuarioCod", GxErrorMask.GX_NOMASK,prmT003520)
             ,new CursorDef("T003521", "SELECT [Contratada_Codigo] AS ContratoGestor_ContratadaCod, [Contrato_AreaTrabalhoCod] AS ContratoGestor_ContratadaAreaCod FROM [Contrato] WITH (NOLOCK) WHERE [Contrato_Codigo] = @ContratoGestor_ContratoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003521,1,0,true,false )
             ,new CursorDef("T003522", "SELECT [Contratada_Sigla] AS ContratoGestor_ContratadaSigla, [Contratada_TipoFabrica] AS ContratoGestor_ContratadaTipo FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_Codigo] = @ContratoGestor_ContratadaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003522,1,0,true,false )
             ,new CursorDef("T003523", "SELECT [AreaTrabalho_Descricao] AS ContratoGestor_ContratadaAreaDes FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @ContratoGestor_ContratadaAreaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003523,1,0,true,false )
             ,new CursorDef("T003524", "SELECT [Usuario_Ativo] AS ContratoGestor_UsuarioAtv, [Usuario_PessoaCod] AS ContratoGestor_UsuarioPesCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @ContratoGestor_UsuarioCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003524,1,0,true,false )
             ,new CursorDef("T003525", "SELECT [Pessoa_Nome] AS ContratoGestor_UsuarioPesNom FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @ContratoGestor_UsuarioPesCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003525,1,0,true,false )
             ,new CursorDef("T003526", "SELECT [ContratoGestor_ContratoCod] AS ContratoGestor_ContratoCod, [ContratoGestor_UsuarioCod] AS ContratoGestor_UsuarioCod FROM [ContratoGestor] WITH (NOLOCK) ORDER BY [ContratoGestor_ContratoCod], [ContratoGestor_UsuarioCod]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT003526,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                return;
             case 1 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 3 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getString(1, 15) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 1) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 5 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 6 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 7 :
                ((String[]) buf[0])[0] = rslt.getString(1, 15) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 1) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((String[]) buf[6])[0] = rslt.getString(4, 100) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((bool[]) buf[8])[0] = rslt.getBool(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((short[]) buf[10])[0] = rslt.getShort(6) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((int[]) buf[12])[0] = rslt.getInt(7) ;
                ((int[]) buf[13])[0] = rslt.getInt(8) ;
                ((int[]) buf[14])[0] = rslt.getInt(9) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((int[]) buf[16])[0] = rslt.getInt(10) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((int[]) buf[18])[0] = rslt.getInt(11) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(11);
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 9 :
                ((String[]) buf[0])[0] = rslt.getString(1, 15) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 1) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 10 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 11 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 12 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 13 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 14 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 15 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 19 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 20 :
                ((String[]) buf[0])[0] = rslt.getString(1, 15) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 1) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 21 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 22 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 23 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 24 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 5 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 6 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 10 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 12 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 14 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 15 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 16 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(1, (short)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                stmt.SetParameter(3, (int)parms[3]);
                return;
             case 17 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(1, (short)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                stmt.SetParameter(3, (int)parms[3]);
                return;
             case 18 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 19 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 20 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 21 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 22 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 23 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
       }
    }

 }

}
