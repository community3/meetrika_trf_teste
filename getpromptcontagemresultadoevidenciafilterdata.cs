/*
               File: GetPromptContagemResultadoEvidenciaFilterData
        Description: Get Prompt Contagem Resultado Evidencia Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:53:57.39
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getpromptcontagemresultadoevidenciafilterdata : GXProcedure
   {
      public getpromptcontagemresultadoevidenciafilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getpromptcontagemresultadoevidenciafilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV22DDOName = aP0_DDOName;
         this.AV20SearchTxt = aP1_SearchTxt;
         this.AV21SearchTxtTo = aP2_SearchTxtTo;
         this.AV26OptionsJson = "" ;
         this.AV29OptionsDescJson = "" ;
         this.AV31OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV26OptionsJson;
         aP4_OptionsDescJson=this.AV29OptionsDescJson;
         aP5_OptionIndexesJson=this.AV31OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV22DDOName = aP0_DDOName;
         this.AV20SearchTxt = aP1_SearchTxt;
         this.AV21SearchTxtTo = aP2_SearchTxtTo;
         this.AV26OptionsJson = "" ;
         this.AV29OptionsDescJson = "" ;
         this.AV31OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV26OptionsJson;
         aP4_OptionsDescJson=this.AV29OptionsDescJson;
         aP5_OptionIndexesJson=this.AV31OptionIndexesJson;
         return AV31OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getpromptcontagemresultadoevidenciafilterdata objgetpromptcontagemresultadoevidenciafilterdata;
         objgetpromptcontagemresultadoevidenciafilterdata = new getpromptcontagemresultadoevidenciafilterdata();
         objgetpromptcontagemresultadoevidenciafilterdata.AV22DDOName = aP0_DDOName;
         objgetpromptcontagemresultadoevidenciafilterdata.AV20SearchTxt = aP1_SearchTxt;
         objgetpromptcontagemresultadoevidenciafilterdata.AV21SearchTxtTo = aP2_SearchTxtTo;
         objgetpromptcontagemresultadoevidenciafilterdata.AV26OptionsJson = "" ;
         objgetpromptcontagemresultadoevidenciafilterdata.AV29OptionsDescJson = "" ;
         objgetpromptcontagemresultadoevidenciafilterdata.AV31OptionIndexesJson = "" ;
         objgetpromptcontagemresultadoevidenciafilterdata.context.SetSubmitInitialConfig(context);
         objgetpromptcontagemresultadoevidenciafilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetpromptcontagemresultadoevidenciafilterdata);
         aP3_OptionsJson=this.AV26OptionsJson;
         aP4_OptionsDescJson=this.AV29OptionsDescJson;
         aP5_OptionIndexesJson=this.AV31OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getpromptcontagemresultadoevidenciafilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV25Options = (IGxCollection)(new GxSimpleCollection());
         AV28OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV30OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV22DDOName), "DDO_CONTAGEMRESULTADOEVIDENCIA_NOMEARQ") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTAGEMRESULTADOEVIDENCIA_NOMEARQOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV22DDOName), "DDO_CONTAGEMRESULTADOEVIDENCIA_TIPOARQ") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTAGEMRESULTADOEVIDENCIA_TIPOARQOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV26OptionsJson = AV25Options.ToJSonString(false);
         AV29OptionsDescJson = AV28OptionsDesc.ToJSonString(false);
         AV31OptionIndexesJson = AV30OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV33Session.Get("PromptContagemResultadoEvidenciaGridState"), "") == 0 )
         {
            AV35GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "PromptContagemResultadoEvidenciaGridState"), "");
         }
         else
         {
            AV35GridState.FromXml(AV33Session.Get("PromptContagemResultadoEvidenciaGridState"), "");
         }
         AV48GXV1 = 1;
         while ( AV48GXV1 <= AV35GridState.gxTpr_Filtervalues.Count )
         {
            AV36GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV35GridState.gxTpr_Filtervalues.Item(AV48GXV1));
            if ( StringUtil.StrCmp(AV36GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADO_CODIGO") == 0 )
            {
               AV10TFContagemResultado_Codigo = (int)(NumberUtil.Val( AV36GridStateFilterValue.gxTpr_Value, "."));
               AV11TFContagemResultado_Codigo_To = (int)(NumberUtil.Val( AV36GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV36GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADOEVIDENCIA_CODIGO") == 0 )
            {
               AV12TFContagemResultadoEvidencia_Codigo = (int)(NumberUtil.Val( AV36GridStateFilterValue.gxTpr_Value, "."));
               AV13TFContagemResultadoEvidencia_Codigo_To = (int)(NumberUtil.Val( AV36GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV36GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADOEVIDENCIA_NOMEARQ") == 0 )
            {
               AV14TFContagemResultadoEvidencia_NomeArq = AV36GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV36GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADOEVIDENCIA_NOMEARQ_SEL") == 0 )
            {
               AV15TFContagemResultadoEvidencia_NomeArq_Sel = AV36GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV36GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADOEVIDENCIA_TIPOARQ") == 0 )
            {
               AV16TFContagemResultadoEvidencia_TipoArq = AV36GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV36GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADOEVIDENCIA_TIPOARQ_SEL") == 0 )
            {
               AV17TFContagemResultadoEvidencia_TipoArq_Sel = AV36GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV36GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADOEVIDENCIA_DATA") == 0 )
            {
               AV18TFContagemResultadoEvidencia_Data = context.localUtil.CToT( AV36GridStateFilterValue.gxTpr_Value, 2);
               AV19TFContagemResultadoEvidencia_Data_To = context.localUtil.CToT( AV36GridStateFilterValue.gxTpr_Valueto, 2);
            }
            AV48GXV1 = (int)(AV48GXV1+1);
         }
         if ( AV35GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV37GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV35GridState.gxTpr_Dynamicfilters.Item(1));
            AV38DynamicFiltersSelector1 = AV37GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV38DynamicFiltersSelector1, "CONTAGEMRESULTADOEVIDENCIA_NOMEARQ") == 0 )
            {
               AV39ContagemResultadoEvidencia_NomeArq1 = AV37GridStateDynamicFilter.gxTpr_Value;
            }
            if ( AV35GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV40DynamicFiltersEnabled2 = true;
               AV37GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV35GridState.gxTpr_Dynamicfilters.Item(2));
               AV41DynamicFiltersSelector2 = AV37GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV41DynamicFiltersSelector2, "CONTAGEMRESULTADOEVIDENCIA_NOMEARQ") == 0 )
               {
                  AV42ContagemResultadoEvidencia_NomeArq2 = AV37GridStateDynamicFilter.gxTpr_Value;
               }
               if ( AV35GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV43DynamicFiltersEnabled3 = true;
                  AV37GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV35GridState.gxTpr_Dynamicfilters.Item(3));
                  AV44DynamicFiltersSelector3 = AV37GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV44DynamicFiltersSelector3, "CONTAGEMRESULTADOEVIDENCIA_NOMEARQ") == 0 )
                  {
                     AV45ContagemResultadoEvidencia_NomeArq3 = AV37GridStateDynamicFilter.gxTpr_Value;
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADCONTAGEMRESULTADOEVIDENCIA_NOMEARQOPTIONS' Routine */
         AV14TFContagemResultadoEvidencia_NomeArq = AV20SearchTxt;
         AV15TFContagemResultadoEvidencia_NomeArq_Sel = "";
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV38DynamicFiltersSelector1 ,
                                              AV39ContagemResultadoEvidencia_NomeArq1 ,
                                              AV40DynamicFiltersEnabled2 ,
                                              AV41DynamicFiltersSelector2 ,
                                              AV42ContagemResultadoEvidencia_NomeArq2 ,
                                              AV43DynamicFiltersEnabled3 ,
                                              AV44DynamicFiltersSelector3 ,
                                              AV45ContagemResultadoEvidencia_NomeArq3 ,
                                              AV10TFContagemResultado_Codigo ,
                                              AV11TFContagemResultado_Codigo_To ,
                                              AV12TFContagemResultadoEvidencia_Codigo ,
                                              AV13TFContagemResultadoEvidencia_Codigo_To ,
                                              AV15TFContagemResultadoEvidencia_NomeArq_Sel ,
                                              AV14TFContagemResultadoEvidencia_NomeArq ,
                                              AV17TFContagemResultadoEvidencia_TipoArq_Sel ,
                                              AV16TFContagemResultadoEvidencia_TipoArq ,
                                              AV18TFContagemResultadoEvidencia_Data ,
                                              AV19TFContagemResultadoEvidencia_Data_To ,
                                              A589ContagemResultadoEvidencia_NomeArq ,
                                              A456ContagemResultado_Codigo ,
                                              A586ContagemResultadoEvidencia_Codigo ,
                                              A590ContagemResultadoEvidencia_TipoArq ,
                                              A591ContagemResultadoEvidencia_Data },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.BOOLEAN
                                              }
         });
         lV39ContagemResultadoEvidencia_NomeArq1 = StringUtil.PadR( StringUtil.RTrim( AV39ContagemResultadoEvidencia_NomeArq1), 50, "%");
         lV42ContagemResultadoEvidencia_NomeArq2 = StringUtil.PadR( StringUtil.RTrim( AV42ContagemResultadoEvidencia_NomeArq2), 50, "%");
         lV45ContagemResultadoEvidencia_NomeArq3 = StringUtil.PadR( StringUtil.RTrim( AV45ContagemResultadoEvidencia_NomeArq3), 50, "%");
         lV14TFContagemResultadoEvidencia_NomeArq = StringUtil.PadR( StringUtil.RTrim( AV14TFContagemResultadoEvidencia_NomeArq), 50, "%");
         lV16TFContagemResultadoEvidencia_TipoArq = StringUtil.PadR( StringUtil.RTrim( AV16TFContagemResultadoEvidencia_TipoArq), 10, "%");
         /* Using cursor P00MW2 */
         pr_default.execute(0, new Object[] {lV39ContagemResultadoEvidencia_NomeArq1, lV42ContagemResultadoEvidencia_NomeArq2, lV45ContagemResultadoEvidencia_NomeArq3, AV10TFContagemResultado_Codigo, AV11TFContagemResultado_Codigo_To, AV12TFContagemResultadoEvidencia_Codigo, AV13TFContagemResultadoEvidencia_Codigo_To, lV14TFContagemResultadoEvidencia_NomeArq, AV15TFContagemResultadoEvidencia_NomeArq_Sel, lV16TFContagemResultadoEvidencia_TipoArq, AV17TFContagemResultadoEvidencia_TipoArq_Sel, AV18TFContagemResultadoEvidencia_Data, AV19TFContagemResultadoEvidencia_Data_To});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKMW2 = false;
            A589ContagemResultadoEvidencia_NomeArq = P00MW2_A589ContagemResultadoEvidencia_NomeArq[0];
            n589ContagemResultadoEvidencia_NomeArq = P00MW2_n589ContagemResultadoEvidencia_NomeArq[0];
            A591ContagemResultadoEvidencia_Data = P00MW2_A591ContagemResultadoEvidencia_Data[0];
            n591ContagemResultadoEvidencia_Data = P00MW2_n591ContagemResultadoEvidencia_Data[0];
            A590ContagemResultadoEvidencia_TipoArq = P00MW2_A590ContagemResultadoEvidencia_TipoArq[0];
            n590ContagemResultadoEvidencia_TipoArq = P00MW2_n590ContagemResultadoEvidencia_TipoArq[0];
            A586ContagemResultadoEvidencia_Codigo = P00MW2_A586ContagemResultadoEvidencia_Codigo[0];
            A456ContagemResultado_Codigo = P00MW2_A456ContagemResultado_Codigo[0];
            AV32count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00MW2_A589ContagemResultadoEvidencia_NomeArq[0], A589ContagemResultadoEvidencia_NomeArq) == 0 ) )
            {
               BRKMW2 = false;
               A586ContagemResultadoEvidencia_Codigo = P00MW2_A586ContagemResultadoEvidencia_Codigo[0];
               AV32count = (long)(AV32count+1);
               BRKMW2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A589ContagemResultadoEvidencia_NomeArq)) )
            {
               AV24Option = A589ContagemResultadoEvidencia_NomeArq;
               AV25Options.Add(AV24Option, 0);
               AV30OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV32count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV25Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKMW2 )
            {
               BRKMW2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADCONTAGEMRESULTADOEVIDENCIA_TIPOARQOPTIONS' Routine */
         AV16TFContagemResultadoEvidencia_TipoArq = AV20SearchTxt;
         AV17TFContagemResultadoEvidencia_TipoArq_Sel = "";
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV38DynamicFiltersSelector1 ,
                                              AV39ContagemResultadoEvidencia_NomeArq1 ,
                                              AV40DynamicFiltersEnabled2 ,
                                              AV41DynamicFiltersSelector2 ,
                                              AV42ContagemResultadoEvidencia_NomeArq2 ,
                                              AV43DynamicFiltersEnabled3 ,
                                              AV44DynamicFiltersSelector3 ,
                                              AV45ContagemResultadoEvidencia_NomeArq3 ,
                                              AV10TFContagemResultado_Codigo ,
                                              AV11TFContagemResultado_Codigo_To ,
                                              AV12TFContagemResultadoEvidencia_Codigo ,
                                              AV13TFContagemResultadoEvidencia_Codigo_To ,
                                              AV15TFContagemResultadoEvidencia_NomeArq_Sel ,
                                              AV14TFContagemResultadoEvidencia_NomeArq ,
                                              AV17TFContagemResultadoEvidencia_TipoArq_Sel ,
                                              AV16TFContagemResultadoEvidencia_TipoArq ,
                                              AV18TFContagemResultadoEvidencia_Data ,
                                              AV19TFContagemResultadoEvidencia_Data_To ,
                                              A589ContagemResultadoEvidencia_NomeArq ,
                                              A456ContagemResultado_Codigo ,
                                              A586ContagemResultadoEvidencia_Codigo ,
                                              A590ContagemResultadoEvidencia_TipoArq ,
                                              A591ContagemResultadoEvidencia_Data },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.BOOLEAN
                                              }
         });
         lV39ContagemResultadoEvidencia_NomeArq1 = StringUtil.PadR( StringUtil.RTrim( AV39ContagemResultadoEvidencia_NomeArq1), 50, "%");
         lV42ContagemResultadoEvidencia_NomeArq2 = StringUtil.PadR( StringUtil.RTrim( AV42ContagemResultadoEvidencia_NomeArq2), 50, "%");
         lV45ContagemResultadoEvidencia_NomeArq3 = StringUtil.PadR( StringUtil.RTrim( AV45ContagemResultadoEvidencia_NomeArq3), 50, "%");
         lV14TFContagemResultadoEvidencia_NomeArq = StringUtil.PadR( StringUtil.RTrim( AV14TFContagemResultadoEvidencia_NomeArq), 50, "%");
         lV16TFContagemResultadoEvidencia_TipoArq = StringUtil.PadR( StringUtil.RTrim( AV16TFContagemResultadoEvidencia_TipoArq), 10, "%");
         /* Using cursor P00MW3 */
         pr_default.execute(1, new Object[] {lV39ContagemResultadoEvidencia_NomeArq1, lV42ContagemResultadoEvidencia_NomeArq2, lV45ContagemResultadoEvidencia_NomeArq3, AV10TFContagemResultado_Codigo, AV11TFContagemResultado_Codigo_To, AV12TFContagemResultadoEvidencia_Codigo, AV13TFContagemResultadoEvidencia_Codigo_To, lV14TFContagemResultadoEvidencia_NomeArq, AV15TFContagemResultadoEvidencia_NomeArq_Sel, lV16TFContagemResultadoEvidencia_TipoArq, AV17TFContagemResultadoEvidencia_TipoArq_Sel, AV18TFContagemResultadoEvidencia_Data, AV19TFContagemResultadoEvidencia_Data_To});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKMW4 = false;
            A590ContagemResultadoEvidencia_TipoArq = P00MW3_A590ContagemResultadoEvidencia_TipoArq[0];
            n590ContagemResultadoEvidencia_TipoArq = P00MW3_n590ContagemResultadoEvidencia_TipoArq[0];
            A591ContagemResultadoEvidencia_Data = P00MW3_A591ContagemResultadoEvidencia_Data[0];
            n591ContagemResultadoEvidencia_Data = P00MW3_n591ContagemResultadoEvidencia_Data[0];
            A586ContagemResultadoEvidencia_Codigo = P00MW3_A586ContagemResultadoEvidencia_Codigo[0];
            A456ContagemResultado_Codigo = P00MW3_A456ContagemResultado_Codigo[0];
            A589ContagemResultadoEvidencia_NomeArq = P00MW3_A589ContagemResultadoEvidencia_NomeArq[0];
            n589ContagemResultadoEvidencia_NomeArq = P00MW3_n589ContagemResultadoEvidencia_NomeArq[0];
            AV32count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( StringUtil.StrCmp(P00MW3_A590ContagemResultadoEvidencia_TipoArq[0], A590ContagemResultadoEvidencia_TipoArq) == 0 ) )
            {
               BRKMW4 = false;
               A586ContagemResultadoEvidencia_Codigo = P00MW3_A586ContagemResultadoEvidencia_Codigo[0];
               AV32count = (long)(AV32count+1);
               BRKMW4 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A590ContagemResultadoEvidencia_TipoArq)) )
            {
               AV24Option = A590ContagemResultadoEvidencia_TipoArq;
               AV25Options.Add(AV24Option, 0);
               AV30OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV32count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV25Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKMW4 )
            {
               BRKMW4 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV25Options = new GxSimpleCollection();
         AV28OptionsDesc = new GxSimpleCollection();
         AV30OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV33Session = context.GetSession();
         AV35GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV36GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV14TFContagemResultadoEvidencia_NomeArq = "";
         AV15TFContagemResultadoEvidencia_NomeArq_Sel = "";
         AV16TFContagemResultadoEvidencia_TipoArq = "";
         AV17TFContagemResultadoEvidencia_TipoArq_Sel = "";
         AV18TFContagemResultadoEvidencia_Data = DateTimeUtil.ServerNow( context, "DEFAULT");
         AV19TFContagemResultadoEvidencia_Data_To = DateTimeUtil.ServerNow( context, "DEFAULT");
         AV37GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV38DynamicFiltersSelector1 = "";
         AV39ContagemResultadoEvidencia_NomeArq1 = "";
         AV41DynamicFiltersSelector2 = "";
         AV42ContagemResultadoEvidencia_NomeArq2 = "";
         AV44DynamicFiltersSelector3 = "";
         AV45ContagemResultadoEvidencia_NomeArq3 = "";
         scmdbuf = "";
         lV39ContagemResultadoEvidencia_NomeArq1 = "";
         lV42ContagemResultadoEvidencia_NomeArq2 = "";
         lV45ContagemResultadoEvidencia_NomeArq3 = "";
         lV14TFContagemResultadoEvidencia_NomeArq = "";
         lV16TFContagemResultadoEvidencia_TipoArq = "";
         A589ContagemResultadoEvidencia_NomeArq = "";
         A590ContagemResultadoEvidencia_TipoArq = "";
         A591ContagemResultadoEvidencia_Data = (DateTime)(DateTime.MinValue);
         P00MW2_A589ContagemResultadoEvidencia_NomeArq = new String[] {""} ;
         P00MW2_n589ContagemResultadoEvidencia_NomeArq = new bool[] {false} ;
         P00MW2_A591ContagemResultadoEvidencia_Data = new DateTime[] {DateTime.MinValue} ;
         P00MW2_n591ContagemResultadoEvidencia_Data = new bool[] {false} ;
         P00MW2_A590ContagemResultadoEvidencia_TipoArq = new String[] {""} ;
         P00MW2_n590ContagemResultadoEvidencia_TipoArq = new bool[] {false} ;
         P00MW2_A586ContagemResultadoEvidencia_Codigo = new int[1] ;
         P00MW2_A456ContagemResultado_Codigo = new int[1] ;
         AV24Option = "";
         P00MW3_A590ContagemResultadoEvidencia_TipoArq = new String[] {""} ;
         P00MW3_n590ContagemResultadoEvidencia_TipoArq = new bool[] {false} ;
         P00MW3_A591ContagemResultadoEvidencia_Data = new DateTime[] {DateTime.MinValue} ;
         P00MW3_n591ContagemResultadoEvidencia_Data = new bool[] {false} ;
         P00MW3_A586ContagemResultadoEvidencia_Codigo = new int[1] ;
         P00MW3_A456ContagemResultado_Codigo = new int[1] ;
         P00MW3_A589ContagemResultadoEvidencia_NomeArq = new String[] {""} ;
         P00MW3_n589ContagemResultadoEvidencia_NomeArq = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getpromptcontagemresultadoevidenciafilterdata__default(),
            new Object[][] {
                new Object[] {
               P00MW2_A589ContagemResultadoEvidencia_NomeArq, P00MW2_n589ContagemResultadoEvidencia_NomeArq, P00MW2_A591ContagemResultadoEvidencia_Data, P00MW2_n591ContagemResultadoEvidencia_Data, P00MW2_A590ContagemResultadoEvidencia_TipoArq, P00MW2_n590ContagemResultadoEvidencia_TipoArq, P00MW2_A586ContagemResultadoEvidencia_Codigo, P00MW2_A456ContagemResultado_Codigo
               }
               , new Object[] {
               P00MW3_A590ContagemResultadoEvidencia_TipoArq, P00MW3_n590ContagemResultadoEvidencia_TipoArq, P00MW3_A591ContagemResultadoEvidencia_Data, P00MW3_n591ContagemResultadoEvidencia_Data, P00MW3_A586ContagemResultadoEvidencia_Codigo, P00MW3_A456ContagemResultado_Codigo, P00MW3_A589ContagemResultadoEvidencia_NomeArq, P00MW3_n589ContagemResultadoEvidencia_NomeArq
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV48GXV1 ;
      private int AV10TFContagemResultado_Codigo ;
      private int AV11TFContagemResultado_Codigo_To ;
      private int AV12TFContagemResultadoEvidencia_Codigo ;
      private int AV13TFContagemResultadoEvidencia_Codigo_To ;
      private int A456ContagemResultado_Codigo ;
      private int A586ContagemResultadoEvidencia_Codigo ;
      private long AV32count ;
      private String AV14TFContagemResultadoEvidencia_NomeArq ;
      private String AV15TFContagemResultadoEvidencia_NomeArq_Sel ;
      private String AV16TFContagemResultadoEvidencia_TipoArq ;
      private String AV17TFContagemResultadoEvidencia_TipoArq_Sel ;
      private String AV39ContagemResultadoEvidencia_NomeArq1 ;
      private String AV42ContagemResultadoEvidencia_NomeArq2 ;
      private String AV45ContagemResultadoEvidencia_NomeArq3 ;
      private String scmdbuf ;
      private String lV39ContagemResultadoEvidencia_NomeArq1 ;
      private String lV42ContagemResultadoEvidencia_NomeArq2 ;
      private String lV45ContagemResultadoEvidencia_NomeArq3 ;
      private String lV14TFContagemResultadoEvidencia_NomeArq ;
      private String lV16TFContagemResultadoEvidencia_TipoArq ;
      private String A589ContagemResultadoEvidencia_NomeArq ;
      private String A590ContagemResultadoEvidencia_TipoArq ;
      private DateTime AV18TFContagemResultadoEvidencia_Data ;
      private DateTime AV19TFContagemResultadoEvidencia_Data_To ;
      private DateTime A591ContagemResultadoEvidencia_Data ;
      private bool returnInSub ;
      private bool AV40DynamicFiltersEnabled2 ;
      private bool AV43DynamicFiltersEnabled3 ;
      private bool BRKMW2 ;
      private bool n589ContagemResultadoEvidencia_NomeArq ;
      private bool n591ContagemResultadoEvidencia_Data ;
      private bool n590ContagemResultadoEvidencia_TipoArq ;
      private bool BRKMW4 ;
      private String AV31OptionIndexesJson ;
      private String AV26OptionsJson ;
      private String AV29OptionsDescJson ;
      private String AV22DDOName ;
      private String AV20SearchTxt ;
      private String AV21SearchTxtTo ;
      private String AV38DynamicFiltersSelector1 ;
      private String AV41DynamicFiltersSelector2 ;
      private String AV44DynamicFiltersSelector3 ;
      private String AV24Option ;
      private IGxSession AV33Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] P00MW2_A589ContagemResultadoEvidencia_NomeArq ;
      private bool[] P00MW2_n589ContagemResultadoEvidencia_NomeArq ;
      private DateTime[] P00MW2_A591ContagemResultadoEvidencia_Data ;
      private bool[] P00MW2_n591ContagemResultadoEvidencia_Data ;
      private String[] P00MW2_A590ContagemResultadoEvidencia_TipoArq ;
      private bool[] P00MW2_n590ContagemResultadoEvidencia_TipoArq ;
      private int[] P00MW2_A586ContagemResultadoEvidencia_Codigo ;
      private int[] P00MW2_A456ContagemResultado_Codigo ;
      private String[] P00MW3_A590ContagemResultadoEvidencia_TipoArq ;
      private bool[] P00MW3_n590ContagemResultadoEvidencia_TipoArq ;
      private DateTime[] P00MW3_A591ContagemResultadoEvidencia_Data ;
      private bool[] P00MW3_n591ContagemResultadoEvidencia_Data ;
      private int[] P00MW3_A586ContagemResultadoEvidencia_Codigo ;
      private int[] P00MW3_A456ContagemResultado_Codigo ;
      private String[] P00MW3_A589ContagemResultadoEvidencia_NomeArq ;
      private bool[] P00MW3_n589ContagemResultadoEvidencia_NomeArq ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV25Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV28OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV30OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV35GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV36GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV37GridStateDynamicFilter ;
   }

   public class getpromptcontagemresultadoevidenciafilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00MW2( IGxContext context ,
                                             String AV38DynamicFiltersSelector1 ,
                                             String AV39ContagemResultadoEvidencia_NomeArq1 ,
                                             bool AV40DynamicFiltersEnabled2 ,
                                             String AV41DynamicFiltersSelector2 ,
                                             String AV42ContagemResultadoEvidencia_NomeArq2 ,
                                             bool AV43DynamicFiltersEnabled3 ,
                                             String AV44DynamicFiltersSelector3 ,
                                             String AV45ContagemResultadoEvidencia_NomeArq3 ,
                                             int AV10TFContagemResultado_Codigo ,
                                             int AV11TFContagemResultado_Codigo_To ,
                                             int AV12TFContagemResultadoEvidencia_Codigo ,
                                             int AV13TFContagemResultadoEvidencia_Codigo_To ,
                                             String AV15TFContagemResultadoEvidencia_NomeArq_Sel ,
                                             String AV14TFContagemResultadoEvidencia_NomeArq ,
                                             String AV17TFContagemResultadoEvidencia_TipoArq_Sel ,
                                             String AV16TFContagemResultadoEvidencia_TipoArq ,
                                             DateTime AV18TFContagemResultadoEvidencia_Data ,
                                             DateTime AV19TFContagemResultadoEvidencia_Data_To ,
                                             String A589ContagemResultadoEvidencia_NomeArq ,
                                             int A456ContagemResultado_Codigo ,
                                             int A586ContagemResultadoEvidencia_Codigo ,
                                             String A590ContagemResultadoEvidencia_TipoArq ,
                                             DateTime A591ContagemResultadoEvidencia_Data )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [13] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT [ContagemResultadoEvidencia_NomeArq], [ContagemResultadoEvidencia_Data], [ContagemResultadoEvidencia_TipoArq], [ContagemResultadoEvidencia_Codigo], [ContagemResultado_Codigo] FROM [ContagemResultadoEvidencia] WITH (NOLOCK)";
         if ( ( StringUtil.StrCmp(AV38DynamicFiltersSelector1, "CONTAGEMRESULTADOEVIDENCIA_NOMEARQ") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV39ContagemResultadoEvidencia_NomeArq1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultadoEvidencia_NomeArq] like '%' + @lV39ContagemResultadoEvidencia_NomeArq1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultadoEvidencia_NomeArq] like '%' + @lV39ContagemResultadoEvidencia_NomeArq1 + '%')";
            }
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( AV40DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV41DynamicFiltersSelector2, "CONTAGEMRESULTADOEVIDENCIA_NOMEARQ") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42ContagemResultadoEvidencia_NomeArq2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultadoEvidencia_NomeArq] like '%' + @lV42ContagemResultadoEvidencia_NomeArq2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultadoEvidencia_NomeArq] like '%' + @lV42ContagemResultadoEvidencia_NomeArq2 + '%')";
            }
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( AV43DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV44DynamicFiltersSelector3, "CONTAGEMRESULTADOEVIDENCIA_NOMEARQ") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV45ContagemResultadoEvidencia_NomeArq3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultadoEvidencia_NomeArq] like '%' + @lV45ContagemResultadoEvidencia_NomeArq3 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultadoEvidencia_NomeArq] like '%' + @lV45ContagemResultadoEvidencia_NomeArq3 + '%')";
            }
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( ! (0==AV10TFContagemResultado_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultado_Codigo] >= @AV10TFContagemResultado_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultado_Codigo] >= @AV10TFContagemResultado_Codigo)";
            }
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( ! (0==AV11TFContagemResultado_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultado_Codigo] <= @AV11TFContagemResultado_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultado_Codigo] <= @AV11TFContagemResultado_Codigo_To)";
            }
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( ! (0==AV12TFContagemResultadoEvidencia_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultadoEvidencia_Codigo] >= @AV12TFContagemResultadoEvidencia_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultadoEvidencia_Codigo] >= @AV12TFContagemResultadoEvidencia_Codigo)";
            }
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( ! (0==AV13TFContagemResultadoEvidencia_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultadoEvidencia_Codigo] <= @AV13TFContagemResultadoEvidencia_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultadoEvidencia_Codigo] <= @AV13TFContagemResultadoEvidencia_Codigo_To)";
            }
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFContagemResultadoEvidencia_NomeArq_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFContagemResultadoEvidencia_NomeArq)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultadoEvidencia_NomeArq] like @lV14TFContagemResultadoEvidencia_NomeArq)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultadoEvidencia_NomeArq] like @lV14TFContagemResultadoEvidencia_NomeArq)";
            }
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFContagemResultadoEvidencia_NomeArq_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultadoEvidencia_NomeArq] = @AV15TFContagemResultadoEvidencia_NomeArq_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultadoEvidencia_NomeArq] = @AV15TFContagemResultadoEvidencia_NomeArq_Sel)";
            }
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV17TFContagemResultadoEvidencia_TipoArq_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV16TFContagemResultadoEvidencia_TipoArq)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultadoEvidencia_TipoArq] like @lV16TFContagemResultadoEvidencia_TipoArq)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultadoEvidencia_TipoArq] like @lV16TFContagemResultadoEvidencia_TipoArq)";
            }
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17TFContagemResultadoEvidencia_TipoArq_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultadoEvidencia_TipoArq] = @AV17TFContagemResultadoEvidencia_TipoArq_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultadoEvidencia_TipoArq] = @AV17TFContagemResultadoEvidencia_TipoArq_Sel)";
            }
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( ! (DateTime.MinValue==AV18TFContagemResultadoEvidencia_Data) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultadoEvidencia_Data] >= @AV18TFContagemResultadoEvidencia_Data)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultadoEvidencia_Data] >= @AV18TFContagemResultadoEvidencia_Data)";
            }
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( ! (DateTime.MinValue==AV19TFContagemResultadoEvidencia_Data_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultadoEvidencia_Data] <= @AV19TFContagemResultadoEvidencia_Data_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultadoEvidencia_Data] <= @AV19TFContagemResultadoEvidencia_Data_To)";
            }
         }
         else
         {
            GXv_int1[12] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY [ContagemResultadoEvidencia_NomeArq]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00MW3( IGxContext context ,
                                             String AV38DynamicFiltersSelector1 ,
                                             String AV39ContagemResultadoEvidencia_NomeArq1 ,
                                             bool AV40DynamicFiltersEnabled2 ,
                                             String AV41DynamicFiltersSelector2 ,
                                             String AV42ContagemResultadoEvidencia_NomeArq2 ,
                                             bool AV43DynamicFiltersEnabled3 ,
                                             String AV44DynamicFiltersSelector3 ,
                                             String AV45ContagemResultadoEvidencia_NomeArq3 ,
                                             int AV10TFContagemResultado_Codigo ,
                                             int AV11TFContagemResultado_Codigo_To ,
                                             int AV12TFContagemResultadoEvidencia_Codigo ,
                                             int AV13TFContagemResultadoEvidencia_Codigo_To ,
                                             String AV15TFContagemResultadoEvidencia_NomeArq_Sel ,
                                             String AV14TFContagemResultadoEvidencia_NomeArq ,
                                             String AV17TFContagemResultadoEvidencia_TipoArq_Sel ,
                                             String AV16TFContagemResultadoEvidencia_TipoArq ,
                                             DateTime AV18TFContagemResultadoEvidencia_Data ,
                                             DateTime AV19TFContagemResultadoEvidencia_Data_To ,
                                             String A589ContagemResultadoEvidencia_NomeArq ,
                                             int A456ContagemResultado_Codigo ,
                                             int A586ContagemResultadoEvidencia_Codigo ,
                                             String A590ContagemResultadoEvidencia_TipoArq ,
                                             DateTime A591ContagemResultadoEvidencia_Data )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [13] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT [ContagemResultadoEvidencia_TipoArq], [ContagemResultadoEvidencia_Data], [ContagemResultadoEvidencia_Codigo], [ContagemResultado_Codigo], [ContagemResultadoEvidencia_NomeArq] FROM [ContagemResultadoEvidencia] WITH (NOLOCK)";
         if ( ( StringUtil.StrCmp(AV38DynamicFiltersSelector1, "CONTAGEMRESULTADOEVIDENCIA_NOMEARQ") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV39ContagemResultadoEvidencia_NomeArq1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultadoEvidencia_NomeArq] like '%' + @lV39ContagemResultadoEvidencia_NomeArq1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultadoEvidencia_NomeArq] like '%' + @lV39ContagemResultadoEvidencia_NomeArq1 + '%')";
            }
         }
         else
         {
            GXv_int3[0] = 1;
         }
         if ( AV40DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV41DynamicFiltersSelector2, "CONTAGEMRESULTADOEVIDENCIA_NOMEARQ") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42ContagemResultadoEvidencia_NomeArq2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultadoEvidencia_NomeArq] like '%' + @lV42ContagemResultadoEvidencia_NomeArq2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultadoEvidencia_NomeArq] like '%' + @lV42ContagemResultadoEvidencia_NomeArq2 + '%')";
            }
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( AV43DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV44DynamicFiltersSelector3, "CONTAGEMRESULTADOEVIDENCIA_NOMEARQ") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV45ContagemResultadoEvidencia_NomeArq3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultadoEvidencia_NomeArq] like '%' + @lV45ContagemResultadoEvidencia_NomeArq3 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultadoEvidencia_NomeArq] like '%' + @lV45ContagemResultadoEvidencia_NomeArq3 + '%')";
            }
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( ! (0==AV10TFContagemResultado_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultado_Codigo] >= @AV10TFContagemResultado_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultado_Codigo] >= @AV10TFContagemResultado_Codigo)";
            }
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( ! (0==AV11TFContagemResultado_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultado_Codigo] <= @AV11TFContagemResultado_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultado_Codigo] <= @AV11TFContagemResultado_Codigo_To)";
            }
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( ! (0==AV12TFContagemResultadoEvidencia_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultadoEvidencia_Codigo] >= @AV12TFContagemResultadoEvidencia_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultadoEvidencia_Codigo] >= @AV12TFContagemResultadoEvidencia_Codigo)";
            }
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( ! (0==AV13TFContagemResultadoEvidencia_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultadoEvidencia_Codigo] <= @AV13TFContagemResultadoEvidencia_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultadoEvidencia_Codigo] <= @AV13TFContagemResultadoEvidencia_Codigo_To)";
            }
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFContagemResultadoEvidencia_NomeArq_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFContagemResultadoEvidencia_NomeArq)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultadoEvidencia_NomeArq] like @lV14TFContagemResultadoEvidencia_NomeArq)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultadoEvidencia_NomeArq] like @lV14TFContagemResultadoEvidencia_NomeArq)";
            }
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFContagemResultadoEvidencia_NomeArq_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultadoEvidencia_NomeArq] = @AV15TFContagemResultadoEvidencia_NomeArq_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultadoEvidencia_NomeArq] = @AV15TFContagemResultadoEvidencia_NomeArq_Sel)";
            }
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV17TFContagemResultadoEvidencia_TipoArq_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV16TFContagemResultadoEvidencia_TipoArq)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultadoEvidencia_TipoArq] like @lV16TFContagemResultadoEvidencia_TipoArq)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultadoEvidencia_TipoArq] like @lV16TFContagemResultadoEvidencia_TipoArq)";
            }
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17TFContagemResultadoEvidencia_TipoArq_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultadoEvidencia_TipoArq] = @AV17TFContagemResultadoEvidencia_TipoArq_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultadoEvidencia_TipoArq] = @AV17TFContagemResultadoEvidencia_TipoArq_Sel)";
            }
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( ! (DateTime.MinValue==AV18TFContagemResultadoEvidencia_Data) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultadoEvidencia_Data] >= @AV18TFContagemResultadoEvidencia_Data)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultadoEvidencia_Data] >= @AV18TFContagemResultadoEvidencia_Data)";
            }
         }
         else
         {
            GXv_int3[11] = 1;
         }
         if ( ! (DateTime.MinValue==AV19TFContagemResultadoEvidencia_Data_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContagemResultadoEvidencia_Data] <= @AV19TFContagemResultadoEvidencia_Data_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContagemResultadoEvidencia_Data] <= @AV19TFContagemResultadoEvidencia_Data_To)";
            }
         }
         else
         {
            GXv_int3[12] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY [ContagemResultadoEvidencia_TipoArq]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00MW2(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (bool)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (int)dynConstraints[8] , (int)dynConstraints[9] , (int)dynConstraints[10] , (int)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (DateTime)dynConstraints[16] , (DateTime)dynConstraints[17] , (String)dynConstraints[18] , (int)dynConstraints[19] , (int)dynConstraints[20] , (String)dynConstraints[21] , (DateTime)dynConstraints[22] );
               case 1 :
                     return conditional_P00MW3(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (bool)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (int)dynConstraints[8] , (int)dynConstraints[9] , (int)dynConstraints[10] , (int)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (DateTime)dynConstraints[16] , (DateTime)dynConstraints[17] , (String)dynConstraints[18] , (int)dynConstraints[19] , (int)dynConstraints[20] , (String)dynConstraints[21] , (DateTime)dynConstraints[22] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00MW2 ;
          prmP00MW2 = new Object[] {
          new Object[] {"@lV39ContagemResultadoEvidencia_NomeArq1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV42ContagemResultadoEvidencia_NomeArq2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV45ContagemResultadoEvidencia_NomeArq3",SqlDbType.Char,50,0} ,
          new Object[] {"@AV10TFContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV11TFContagemResultado_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV12TFContagemResultadoEvidencia_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV13TFContagemResultadoEvidencia_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV14TFContagemResultadoEvidencia_NomeArq",SqlDbType.Char,50,0} ,
          new Object[] {"@AV15TFContagemResultadoEvidencia_NomeArq_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV16TFContagemResultadoEvidencia_TipoArq",SqlDbType.Char,10,0} ,
          new Object[] {"@AV17TFContagemResultadoEvidencia_TipoArq_Sel",SqlDbType.Char,10,0} ,
          new Object[] {"@AV18TFContagemResultadoEvidencia_Data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV19TFContagemResultadoEvidencia_Data_To",SqlDbType.DateTime,8,5}
          } ;
          Object[] prmP00MW3 ;
          prmP00MW3 = new Object[] {
          new Object[] {"@lV39ContagemResultadoEvidencia_NomeArq1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV42ContagemResultadoEvidencia_NomeArq2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV45ContagemResultadoEvidencia_NomeArq3",SqlDbType.Char,50,0} ,
          new Object[] {"@AV10TFContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV11TFContagemResultado_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV12TFContagemResultadoEvidencia_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV13TFContagemResultadoEvidencia_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV14TFContagemResultadoEvidencia_NomeArq",SqlDbType.Char,50,0} ,
          new Object[] {"@AV15TFContagemResultadoEvidencia_NomeArq_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV16TFContagemResultadoEvidencia_TipoArq",SqlDbType.Char,10,0} ,
          new Object[] {"@AV17TFContagemResultadoEvidencia_TipoArq_Sel",SqlDbType.Char,10,0} ,
          new Object[] {"@AV18TFContagemResultadoEvidencia_Data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV19TFContagemResultadoEvidencia_Data_To",SqlDbType.DateTime,8,5}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00MW2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00MW2,100,0,true,false )
             ,new CursorDef("P00MW3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00MW3,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((DateTime[]) buf[2])[0] = rslt.getGXDateTime(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getString(3, 10) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                return;
             case 1 :
                ((String[]) buf[0])[0] = rslt.getString(1, 10) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((DateTime[]) buf[2])[0] = rslt.getGXDateTime(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((String[]) buf[6])[0] = rslt.getString(5, 50) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[16]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[17]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[18]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[19]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[24]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[25]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[16]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[17]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[18]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[19]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[24]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[25]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getpromptcontagemresultadoevidenciafilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getpromptcontagemresultadoevidenciafilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getpromptcontagemresultadoevidenciafilterdata") )
          {
             return  ;
          }
          getpromptcontagemresultadoevidenciafilterdata worker = new getpromptcontagemresultadoevidenciafilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
