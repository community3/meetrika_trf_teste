/*
               File: UsuarioPerfil_BC
        Description: Usuario x Perfil
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/21/2020 18:6:58.79
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class usuarioperfil_bc : GXHttpHandler, IGxSilentTrn
   {
      public usuarioperfil_bc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public usuarioperfil_bc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      protected void INITTRN( )
      {
      }

      public void GetInsDefault( )
      {
         ReadRow023( ) ;
         standaloneNotModal( ) ;
         InitializeNonKey023( ) ;
         standaloneModal( ) ;
         AddRow023( ) ;
         Gx_mode = "INS";
         return  ;
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E11022 */
            E11022 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               Z1Usuario_Codigo = A1Usuario_Codigo;
               Z3Perfil_Codigo = A3Perfil_Codigo;
               SetMode( "UPD") ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      public bool Reindex( )
      {
         return true ;
      }

      protected void CONFIRM_020( )
      {
         BeforeValidate023( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls023( ) ;
            }
            else
            {
               CheckExtendedTable023( ) ;
               if ( AnyError == 0 )
               {
                  ZM023( 3) ;
                  ZM023( 4) ;
                  ZM023( 5) ;
               }
               CloseExtendedTableCursors023( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
         }
      }

      protected void E12022( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         AV10TrnContext.FromXml(AV11WebSession.Get("TrnContext"), "");
      }

      protected void E11022( )
      {
         /* After Trn Routine */
      }

      protected void ZM023( short GX_JID )
      {
         if ( ( GX_JID == 2 ) || ( GX_JID == 0 ) )
         {
            Z543UsuarioPerfil_Display = A543UsuarioPerfil_Display;
            Z544UsuarioPerfil_Insert = A544UsuarioPerfil_Insert;
            Z659UsuarioPerfil_Update = A659UsuarioPerfil_Update;
            Z546UsuarioPerfil_Delete = A546UsuarioPerfil_Delete;
         }
         if ( ( GX_JID == 3 ) || ( GX_JID == 0 ) )
         {
            Z2Usuario_Nome = A2Usuario_Nome;
            Z341Usuario_UserGamGuid = A341Usuario_UserGamGuid;
            Z57Usuario_PessoaCod = A57Usuario_PessoaCod;
         }
         if ( ( GX_JID == 4 ) || ( GX_JID == 0 ) )
         {
            Z4Perfil_Nome = A4Perfil_Nome;
            Z275Perfil_Tipo = A275Perfil_Tipo;
            Z329Perfil_GamId = A329Perfil_GamId;
            Z276Perfil_Ativo = A276Perfil_Ativo;
            Z7Perfil_AreaTrabalhoCod = A7Perfil_AreaTrabalhoCod;
         }
         if ( ( GX_JID == 5 ) || ( GX_JID == 0 ) )
         {
            Z58Usuario_PessoaNom = A58Usuario_PessoaNom;
         }
         if ( GX_JID == -2 )
         {
            Z543UsuarioPerfil_Display = A543UsuarioPerfil_Display;
            Z544UsuarioPerfil_Insert = A544UsuarioPerfil_Insert;
            Z659UsuarioPerfil_Update = A659UsuarioPerfil_Update;
            Z546UsuarioPerfil_Delete = A546UsuarioPerfil_Delete;
            Z1Usuario_Codigo = A1Usuario_Codigo;
            Z3Perfil_Codigo = A3Perfil_Codigo;
            Z2Usuario_Nome = A2Usuario_Nome;
            Z341Usuario_UserGamGuid = A341Usuario_UserGamGuid;
            Z57Usuario_PessoaCod = A57Usuario_PessoaCod;
            Z58Usuario_PessoaNom = A58Usuario_PessoaNom;
            Z4Perfil_Nome = A4Perfil_Nome;
            Z275Perfil_Tipo = A275Perfil_Tipo;
            Z329Perfil_GamId = A329Perfil_GamId;
            Z276Perfil_Ativo = A276Perfil_Ativo;
            Z7Perfil_AreaTrabalhoCod = A7Perfil_AreaTrabalhoCod;
         }
      }

      protected void standaloneNotModal( )
      {
         Gx_BScreen = 0;
      }

      protected void standaloneModal( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (false==A543UsuarioPerfil_Display) && ( Gx_BScreen == 0 ) )
         {
            A543UsuarioPerfil_Display = true;
            n543UsuarioPerfil_Display = false;
         }
      }

      protected void Load023( )
      {
         /* Using cursor BC00027 */
         pr_default.execute(5, new Object[] {A1Usuario_Codigo, A3Perfil_Codigo});
         if ( (pr_default.getStatus(5) != 101) )
         {
            RcdFound3 = 1;
            A58Usuario_PessoaNom = BC00027_A58Usuario_PessoaNom[0];
            n58Usuario_PessoaNom = BC00027_n58Usuario_PessoaNom[0];
            A2Usuario_Nome = BC00027_A2Usuario_Nome[0];
            n2Usuario_Nome = BC00027_n2Usuario_Nome[0];
            A341Usuario_UserGamGuid = BC00027_A341Usuario_UserGamGuid[0];
            A4Perfil_Nome = BC00027_A4Perfil_Nome[0];
            A275Perfil_Tipo = BC00027_A275Perfil_Tipo[0];
            A329Perfil_GamId = BC00027_A329Perfil_GamId[0];
            A543UsuarioPerfil_Display = BC00027_A543UsuarioPerfil_Display[0];
            n543UsuarioPerfil_Display = BC00027_n543UsuarioPerfil_Display[0];
            A544UsuarioPerfil_Insert = BC00027_A544UsuarioPerfil_Insert[0];
            n544UsuarioPerfil_Insert = BC00027_n544UsuarioPerfil_Insert[0];
            A659UsuarioPerfil_Update = BC00027_A659UsuarioPerfil_Update[0];
            n659UsuarioPerfil_Update = BC00027_n659UsuarioPerfil_Update[0];
            A546UsuarioPerfil_Delete = BC00027_A546UsuarioPerfil_Delete[0];
            n546UsuarioPerfil_Delete = BC00027_n546UsuarioPerfil_Delete[0];
            A276Perfil_Ativo = BC00027_A276Perfil_Ativo[0];
            A57Usuario_PessoaCod = BC00027_A57Usuario_PessoaCod[0];
            A7Perfil_AreaTrabalhoCod = BC00027_A7Perfil_AreaTrabalhoCod[0];
            ZM023( -2) ;
         }
         pr_default.close(5);
         OnLoadActions023( ) ;
      }

      protected void OnLoadActions023( )
      {
      }

      protected void CheckExtendedTable023( )
      {
         standaloneModal( ) ;
         /* Using cursor BC00024 */
         pr_default.execute(2, new Object[] {A1Usuario_Codigo});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Usuario'.", "ForeignKeyNotFound", 1, "USUARIO_CODIGO");
            AnyError = 1;
         }
         A2Usuario_Nome = BC00024_A2Usuario_Nome[0];
         n2Usuario_Nome = BC00024_n2Usuario_Nome[0];
         A341Usuario_UserGamGuid = BC00024_A341Usuario_UserGamGuid[0];
         A57Usuario_PessoaCod = BC00024_A57Usuario_PessoaCod[0];
         pr_default.close(2);
         /* Using cursor BC00026 */
         pr_default.execute(4, new Object[] {A57Usuario_PessoaCod});
         if ( (pr_default.getStatus(4) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Usuario_Pessoa'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A58Usuario_PessoaNom = BC00026_A58Usuario_PessoaNom[0];
         n58Usuario_PessoaNom = BC00026_n58Usuario_PessoaNom[0];
         pr_default.close(4);
         /* Using cursor BC00025 */
         pr_default.execute(3, new Object[] {A3Perfil_Codigo});
         if ( (pr_default.getStatus(3) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Perfil'.", "ForeignKeyNotFound", 1, "PERFIL_CODIGO");
            AnyError = 1;
         }
         A4Perfil_Nome = BC00025_A4Perfil_Nome[0];
         A275Perfil_Tipo = BC00025_A275Perfil_Tipo[0];
         A329Perfil_GamId = BC00025_A329Perfil_GamId[0];
         A276Perfil_Ativo = BC00025_A276Perfil_Ativo[0];
         A7Perfil_AreaTrabalhoCod = BC00025_A7Perfil_AreaTrabalhoCod[0];
         pr_default.close(3);
      }

      protected void CloseExtendedTableCursors023( )
      {
         pr_default.close(2);
         pr_default.close(4);
         pr_default.close(3);
      }

      protected void enableDisable( )
      {
      }

      protected void GetKey023( )
      {
         /* Using cursor BC00028 */
         pr_default.execute(6, new Object[] {A1Usuario_Codigo, A3Perfil_Codigo});
         if ( (pr_default.getStatus(6) != 101) )
         {
            RcdFound3 = 1;
         }
         else
         {
            RcdFound3 = 0;
         }
         pr_default.close(6);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor BC00023 */
         pr_default.execute(1, new Object[] {A1Usuario_Codigo, A3Perfil_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM023( 2) ;
            RcdFound3 = 1;
            A543UsuarioPerfil_Display = BC00023_A543UsuarioPerfil_Display[0];
            n543UsuarioPerfil_Display = BC00023_n543UsuarioPerfil_Display[0];
            A544UsuarioPerfil_Insert = BC00023_A544UsuarioPerfil_Insert[0];
            n544UsuarioPerfil_Insert = BC00023_n544UsuarioPerfil_Insert[0];
            A659UsuarioPerfil_Update = BC00023_A659UsuarioPerfil_Update[0];
            n659UsuarioPerfil_Update = BC00023_n659UsuarioPerfil_Update[0];
            A546UsuarioPerfil_Delete = BC00023_A546UsuarioPerfil_Delete[0];
            n546UsuarioPerfil_Delete = BC00023_n546UsuarioPerfil_Delete[0];
            A1Usuario_Codigo = BC00023_A1Usuario_Codigo[0];
            A3Perfil_Codigo = BC00023_A3Perfil_Codigo[0];
            Z1Usuario_Codigo = A1Usuario_Codigo;
            Z3Perfil_Codigo = A3Perfil_Codigo;
            sMode3 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Load023( ) ;
            if ( AnyError == 1 )
            {
               RcdFound3 = 0;
               InitializeNonKey023( ) ;
            }
            Gx_mode = sMode3;
         }
         else
         {
            RcdFound3 = 0;
            InitializeNonKey023( ) ;
            sMode3 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Gx_mode = sMode3;
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey023( ) ;
         if ( RcdFound3 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
         }
         getByPrimaryKey( ) ;
      }

      protected void insert_Check( )
      {
         CONFIRM_020( ) ;
         IsConfirmed = 0;
      }

      protected void update_Check( )
      {
         insert_Check( ) ;
      }

      protected void delete_Check( )
      {
         insert_Check( ) ;
      }

      protected void CheckOptimisticConcurrency023( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor BC00022 */
            pr_default.execute(0, new Object[] {A1Usuario_Codigo, A3Perfil_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"UsuarioPerfil"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( Z543UsuarioPerfil_Display != BC00022_A543UsuarioPerfil_Display[0] ) || ( Z544UsuarioPerfil_Insert != BC00022_A544UsuarioPerfil_Insert[0] ) || ( Z659UsuarioPerfil_Update != BC00022_A659UsuarioPerfil_Update[0] ) || ( Z546UsuarioPerfil_Delete != BC00022_A546UsuarioPerfil_Delete[0] ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"UsuarioPerfil"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert023( )
      {
         BeforeValidate023( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable023( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM023( 0) ;
            CheckOptimisticConcurrency023( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm023( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert023( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC00029 */
                     pr_default.execute(7, new Object[] {n543UsuarioPerfil_Display, A543UsuarioPerfil_Display, n544UsuarioPerfil_Insert, A544UsuarioPerfil_Insert, n659UsuarioPerfil_Update, A659UsuarioPerfil_Update, n546UsuarioPerfil_Delete, A546UsuarioPerfil_Delete, A1Usuario_Codigo, A3Perfil_Codigo});
                     pr_default.close(7);
                     dsDefault.SmartCacheProvider.SetUpdated("UsuarioPerfil") ;
                     if ( (pr_default.getStatus(7) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load023( ) ;
            }
            EndLevel023( ) ;
         }
         CloseExtendedTableCursors023( ) ;
      }

      protected void Update023( )
      {
         BeforeValidate023( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable023( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency023( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm023( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate023( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC000210 */
                     pr_default.execute(8, new Object[] {n543UsuarioPerfil_Display, A543UsuarioPerfil_Display, n544UsuarioPerfil_Insert, A544UsuarioPerfil_Insert, n659UsuarioPerfil_Update, A659UsuarioPerfil_Update, n546UsuarioPerfil_Delete, A546UsuarioPerfil_Delete, A1Usuario_Codigo, A3Perfil_Codigo});
                     pr_default.close(8);
                     dsDefault.SmartCacheProvider.SetUpdated("UsuarioPerfil") ;
                     if ( (pr_default.getStatus(8) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"UsuarioPerfil"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate023( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel023( ) ;
         }
         CloseExtendedTableCursors023( ) ;
      }

      protected void DeferredUpdate023( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         BeforeValidate023( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency023( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls023( ) ;
            AfterConfirm023( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete023( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor BC000211 */
                  pr_default.execute(9, new Object[] {A1Usuario_Codigo, A3Perfil_Codigo});
                  pr_default.close(9);
                  dsDefault.SmartCacheProvider.SetUpdated("UsuarioPerfil") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode3 = Gx_mode;
         Gx_mode = "DLT";
         EndLevel023( ) ;
         Gx_mode = sMode3;
      }

      protected void OnDeleteControls023( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor BC000212 */
            pr_default.execute(10, new Object[] {A1Usuario_Codigo});
            A2Usuario_Nome = BC000212_A2Usuario_Nome[0];
            n2Usuario_Nome = BC000212_n2Usuario_Nome[0];
            A341Usuario_UserGamGuid = BC000212_A341Usuario_UserGamGuid[0];
            A57Usuario_PessoaCod = BC000212_A57Usuario_PessoaCod[0];
            pr_default.close(10);
            /* Using cursor BC000213 */
            pr_default.execute(11, new Object[] {A57Usuario_PessoaCod});
            A58Usuario_PessoaNom = BC000213_A58Usuario_PessoaNom[0];
            n58Usuario_PessoaNom = BC000213_n58Usuario_PessoaNom[0];
            pr_default.close(11);
            /* Using cursor BC000214 */
            pr_default.execute(12, new Object[] {A3Perfil_Codigo});
            A4Perfil_Nome = BC000214_A4Perfil_Nome[0];
            A275Perfil_Tipo = BC000214_A275Perfil_Tipo[0];
            A329Perfil_GamId = BC000214_A329Perfil_GamId[0];
            A276Perfil_Ativo = BC000214_A276Perfil_Ativo[0];
            A7Perfil_AreaTrabalhoCod = BC000214_A7Perfil_AreaTrabalhoCod[0];
            pr_default.close(12);
         }
      }

      protected void EndLevel023( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete023( ) ;
         }
         if ( AnyError == 0 )
         {
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanKeyStart023( )
      {
         /* Scan By routine */
         /* Using cursor BC000215 */
         pr_default.execute(13, new Object[] {A1Usuario_Codigo, A3Perfil_Codigo});
         RcdFound3 = 0;
         if ( (pr_default.getStatus(13) != 101) )
         {
            RcdFound3 = 1;
            A58Usuario_PessoaNom = BC000215_A58Usuario_PessoaNom[0];
            n58Usuario_PessoaNom = BC000215_n58Usuario_PessoaNom[0];
            A2Usuario_Nome = BC000215_A2Usuario_Nome[0];
            n2Usuario_Nome = BC000215_n2Usuario_Nome[0];
            A341Usuario_UserGamGuid = BC000215_A341Usuario_UserGamGuid[0];
            A4Perfil_Nome = BC000215_A4Perfil_Nome[0];
            A275Perfil_Tipo = BC000215_A275Perfil_Tipo[0];
            A329Perfil_GamId = BC000215_A329Perfil_GamId[0];
            A543UsuarioPerfil_Display = BC000215_A543UsuarioPerfil_Display[0];
            n543UsuarioPerfil_Display = BC000215_n543UsuarioPerfil_Display[0];
            A544UsuarioPerfil_Insert = BC000215_A544UsuarioPerfil_Insert[0];
            n544UsuarioPerfil_Insert = BC000215_n544UsuarioPerfil_Insert[0];
            A659UsuarioPerfil_Update = BC000215_A659UsuarioPerfil_Update[0];
            n659UsuarioPerfil_Update = BC000215_n659UsuarioPerfil_Update[0];
            A546UsuarioPerfil_Delete = BC000215_A546UsuarioPerfil_Delete[0];
            n546UsuarioPerfil_Delete = BC000215_n546UsuarioPerfil_Delete[0];
            A276Perfil_Ativo = BC000215_A276Perfil_Ativo[0];
            A1Usuario_Codigo = BC000215_A1Usuario_Codigo[0];
            A3Perfil_Codigo = BC000215_A3Perfil_Codigo[0];
            A57Usuario_PessoaCod = BC000215_A57Usuario_PessoaCod[0];
            A7Perfil_AreaTrabalhoCod = BC000215_A7Perfil_AreaTrabalhoCod[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanKeyNext023( )
      {
         /* Scan next routine */
         pr_default.readNext(13);
         RcdFound3 = 0;
         ScanKeyLoad023( ) ;
      }

      protected void ScanKeyLoad023( )
      {
         sMode3 = Gx_mode;
         Gx_mode = "DSP";
         if ( (pr_default.getStatus(13) != 101) )
         {
            RcdFound3 = 1;
            A58Usuario_PessoaNom = BC000215_A58Usuario_PessoaNom[0];
            n58Usuario_PessoaNom = BC000215_n58Usuario_PessoaNom[0];
            A2Usuario_Nome = BC000215_A2Usuario_Nome[0];
            n2Usuario_Nome = BC000215_n2Usuario_Nome[0];
            A341Usuario_UserGamGuid = BC000215_A341Usuario_UserGamGuid[0];
            A4Perfil_Nome = BC000215_A4Perfil_Nome[0];
            A275Perfil_Tipo = BC000215_A275Perfil_Tipo[0];
            A329Perfil_GamId = BC000215_A329Perfil_GamId[0];
            A543UsuarioPerfil_Display = BC000215_A543UsuarioPerfil_Display[0];
            n543UsuarioPerfil_Display = BC000215_n543UsuarioPerfil_Display[0];
            A544UsuarioPerfil_Insert = BC000215_A544UsuarioPerfil_Insert[0];
            n544UsuarioPerfil_Insert = BC000215_n544UsuarioPerfil_Insert[0];
            A659UsuarioPerfil_Update = BC000215_A659UsuarioPerfil_Update[0];
            n659UsuarioPerfil_Update = BC000215_n659UsuarioPerfil_Update[0];
            A546UsuarioPerfil_Delete = BC000215_A546UsuarioPerfil_Delete[0];
            n546UsuarioPerfil_Delete = BC000215_n546UsuarioPerfil_Delete[0];
            A276Perfil_Ativo = BC000215_A276Perfil_Ativo[0];
            A1Usuario_Codigo = BC000215_A1Usuario_Codigo[0];
            A3Perfil_Codigo = BC000215_A3Perfil_Codigo[0];
            A57Usuario_PessoaCod = BC000215_A57Usuario_PessoaCod[0];
            A7Perfil_AreaTrabalhoCod = BC000215_A7Perfil_AreaTrabalhoCod[0];
         }
         Gx_mode = sMode3;
      }

      protected void ScanKeyEnd023( )
      {
         pr_default.close(13);
      }

      protected void AfterConfirm023( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert023( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate023( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete023( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete023( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate023( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes023( )
      {
      }

      protected void AddRow023( )
      {
         VarsToRow3( bcUsuarioPerfil) ;
      }

      protected void ReadRow023( )
      {
         RowToVars3( bcUsuarioPerfil, 1) ;
      }

      protected void InitializeNonKey023( )
      {
         A57Usuario_PessoaCod = 0;
         A58Usuario_PessoaNom = "";
         n58Usuario_PessoaNom = false;
         A2Usuario_Nome = "";
         n2Usuario_Nome = false;
         A341Usuario_UserGamGuid = "";
         A7Perfil_AreaTrabalhoCod = 0;
         A4Perfil_Nome = "";
         A275Perfil_Tipo = 0;
         A329Perfil_GamId = 0;
         A544UsuarioPerfil_Insert = false;
         n544UsuarioPerfil_Insert = false;
         A659UsuarioPerfil_Update = false;
         n659UsuarioPerfil_Update = false;
         A546UsuarioPerfil_Delete = false;
         n546UsuarioPerfil_Delete = false;
         A276Perfil_Ativo = false;
         A543UsuarioPerfil_Display = true;
         n543UsuarioPerfil_Display = false;
         Z543UsuarioPerfil_Display = false;
         Z544UsuarioPerfil_Insert = false;
         Z659UsuarioPerfil_Update = false;
         Z546UsuarioPerfil_Delete = false;
      }

      protected void InitAll023( )
      {
         A1Usuario_Codigo = 0;
         A3Perfil_Codigo = 0;
         InitializeNonKey023( ) ;
      }

      protected void StandaloneModalInsert( )
      {
         A543UsuarioPerfil_Display = i543UsuarioPerfil_Display;
         n543UsuarioPerfil_Display = false;
      }

      public void VarsToRow3( SdtUsuarioPerfil obj3 )
      {
         obj3.gxTpr_Mode = Gx_mode;
         obj3.gxTpr_Usuario_pessoacod = A57Usuario_PessoaCod;
         obj3.gxTpr_Usuario_pessoanom = A58Usuario_PessoaNom;
         obj3.gxTpr_Usuario_nome = A2Usuario_Nome;
         obj3.gxTpr_Usuario_usergamguid = A341Usuario_UserGamGuid;
         obj3.gxTpr_Perfil_areatrabalhocod = A7Perfil_AreaTrabalhoCod;
         obj3.gxTpr_Perfil_nome = A4Perfil_Nome;
         obj3.gxTpr_Perfil_tipo = A275Perfil_Tipo;
         obj3.gxTpr_Perfil_gamid = A329Perfil_GamId;
         obj3.gxTpr_Usuarioperfil_insert = A544UsuarioPerfil_Insert;
         obj3.gxTpr_Usuarioperfil_update = A659UsuarioPerfil_Update;
         obj3.gxTpr_Usuarioperfil_delete = A546UsuarioPerfil_Delete;
         obj3.gxTpr_Perfil_ativo = A276Perfil_Ativo;
         obj3.gxTpr_Usuarioperfil_display = A543UsuarioPerfil_Display;
         obj3.gxTpr_Usuario_codigo = A1Usuario_Codigo;
         obj3.gxTpr_Perfil_codigo = A3Perfil_Codigo;
         obj3.gxTpr_Usuario_codigo_Z = Z1Usuario_Codigo;
         obj3.gxTpr_Usuario_pessoacod_Z = Z57Usuario_PessoaCod;
         obj3.gxTpr_Usuario_pessoanom_Z = Z58Usuario_PessoaNom;
         obj3.gxTpr_Usuario_nome_Z = Z2Usuario_Nome;
         obj3.gxTpr_Usuario_usergamguid_Z = Z341Usuario_UserGamGuid;
         obj3.gxTpr_Perfil_codigo_Z = Z3Perfil_Codigo;
         obj3.gxTpr_Perfil_areatrabalhocod_Z = Z7Perfil_AreaTrabalhoCod;
         obj3.gxTpr_Perfil_nome_Z = Z4Perfil_Nome;
         obj3.gxTpr_Perfil_tipo_Z = Z275Perfil_Tipo;
         obj3.gxTpr_Perfil_gamid_Z = Z329Perfil_GamId;
         obj3.gxTpr_Usuarioperfil_display_Z = Z543UsuarioPerfil_Display;
         obj3.gxTpr_Usuarioperfil_insert_Z = Z544UsuarioPerfil_Insert;
         obj3.gxTpr_Usuarioperfil_update_Z = Z659UsuarioPerfil_Update;
         obj3.gxTpr_Usuarioperfil_delete_Z = Z546UsuarioPerfil_Delete;
         obj3.gxTpr_Perfil_ativo_Z = Z276Perfil_Ativo;
         obj3.gxTpr_Usuario_pessoanom_N = (short)(Convert.ToInt16(n58Usuario_PessoaNom));
         obj3.gxTpr_Usuario_nome_N = (short)(Convert.ToInt16(n2Usuario_Nome));
         obj3.gxTpr_Usuarioperfil_display_N = (short)(Convert.ToInt16(n543UsuarioPerfil_Display));
         obj3.gxTpr_Usuarioperfil_insert_N = (short)(Convert.ToInt16(n544UsuarioPerfil_Insert));
         obj3.gxTpr_Usuarioperfil_update_N = (short)(Convert.ToInt16(n659UsuarioPerfil_Update));
         obj3.gxTpr_Usuarioperfil_delete_N = (short)(Convert.ToInt16(n546UsuarioPerfil_Delete));
         obj3.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void KeyVarsToRow3( SdtUsuarioPerfil obj3 )
      {
         obj3.gxTpr_Usuario_codigo = A1Usuario_Codigo;
         obj3.gxTpr_Perfil_codigo = A3Perfil_Codigo;
         return  ;
      }

      public void RowToVars3( SdtUsuarioPerfil obj3 ,
                              int forceLoad )
      {
         Gx_mode = obj3.gxTpr_Mode;
         A57Usuario_PessoaCod = obj3.gxTpr_Usuario_pessoacod;
         A58Usuario_PessoaNom = obj3.gxTpr_Usuario_pessoanom;
         n58Usuario_PessoaNom = false;
         A2Usuario_Nome = obj3.gxTpr_Usuario_nome;
         n2Usuario_Nome = false;
         A341Usuario_UserGamGuid = obj3.gxTpr_Usuario_usergamguid;
         A7Perfil_AreaTrabalhoCod = obj3.gxTpr_Perfil_areatrabalhocod;
         A4Perfil_Nome = obj3.gxTpr_Perfil_nome;
         A275Perfil_Tipo = obj3.gxTpr_Perfil_tipo;
         A329Perfil_GamId = obj3.gxTpr_Perfil_gamid;
         A544UsuarioPerfil_Insert = obj3.gxTpr_Usuarioperfil_insert;
         n544UsuarioPerfil_Insert = false;
         A659UsuarioPerfil_Update = obj3.gxTpr_Usuarioperfil_update;
         n659UsuarioPerfil_Update = false;
         A546UsuarioPerfil_Delete = obj3.gxTpr_Usuarioperfil_delete;
         n546UsuarioPerfil_Delete = false;
         A276Perfil_Ativo = obj3.gxTpr_Perfil_ativo;
         A543UsuarioPerfil_Display = obj3.gxTpr_Usuarioperfil_display;
         n543UsuarioPerfil_Display = false;
         A1Usuario_Codigo = obj3.gxTpr_Usuario_codigo;
         A3Perfil_Codigo = obj3.gxTpr_Perfil_codigo;
         Z1Usuario_Codigo = obj3.gxTpr_Usuario_codigo_Z;
         Z57Usuario_PessoaCod = obj3.gxTpr_Usuario_pessoacod_Z;
         Z58Usuario_PessoaNom = obj3.gxTpr_Usuario_pessoanom_Z;
         Z2Usuario_Nome = obj3.gxTpr_Usuario_nome_Z;
         Z341Usuario_UserGamGuid = obj3.gxTpr_Usuario_usergamguid_Z;
         Z3Perfil_Codigo = obj3.gxTpr_Perfil_codigo_Z;
         Z7Perfil_AreaTrabalhoCod = obj3.gxTpr_Perfil_areatrabalhocod_Z;
         Z4Perfil_Nome = obj3.gxTpr_Perfil_nome_Z;
         Z275Perfil_Tipo = obj3.gxTpr_Perfil_tipo_Z;
         Z329Perfil_GamId = obj3.gxTpr_Perfil_gamid_Z;
         Z543UsuarioPerfil_Display = obj3.gxTpr_Usuarioperfil_display_Z;
         Z544UsuarioPerfil_Insert = obj3.gxTpr_Usuarioperfil_insert_Z;
         Z659UsuarioPerfil_Update = obj3.gxTpr_Usuarioperfil_update_Z;
         Z546UsuarioPerfil_Delete = obj3.gxTpr_Usuarioperfil_delete_Z;
         Z276Perfil_Ativo = obj3.gxTpr_Perfil_ativo_Z;
         n58Usuario_PessoaNom = (bool)(Convert.ToBoolean(obj3.gxTpr_Usuario_pessoanom_N));
         n2Usuario_Nome = (bool)(Convert.ToBoolean(obj3.gxTpr_Usuario_nome_N));
         n543UsuarioPerfil_Display = (bool)(Convert.ToBoolean(obj3.gxTpr_Usuarioperfil_display_N));
         n544UsuarioPerfil_Insert = (bool)(Convert.ToBoolean(obj3.gxTpr_Usuarioperfil_insert_N));
         n659UsuarioPerfil_Update = (bool)(Convert.ToBoolean(obj3.gxTpr_Usuarioperfil_update_N));
         n546UsuarioPerfil_Delete = (bool)(Convert.ToBoolean(obj3.gxTpr_Usuarioperfil_delete_N));
         Gx_mode = obj3.gxTpr_Mode;
         return  ;
      }

      public void LoadKey( Object[] obj )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         A1Usuario_Codigo = (int)getParm(obj,0);
         A3Perfil_Codigo = (int)getParm(obj,1);
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         InitializeNonKey023( ) ;
         ScanKeyStart023( ) ;
         if ( RcdFound3 == 0 )
         {
            Gx_mode = "INS";
            /* Using cursor BC000212 */
            pr_default.execute(10, new Object[] {A1Usuario_Codigo});
            if ( (pr_default.getStatus(10) == 101) )
            {
               GX_msglist.addItem("N�o existe 'Usuario'.", "ForeignKeyNotFound", 1, "USUARIO_CODIGO");
               AnyError = 1;
            }
            A2Usuario_Nome = BC000212_A2Usuario_Nome[0];
            n2Usuario_Nome = BC000212_n2Usuario_Nome[0];
            A341Usuario_UserGamGuid = BC000212_A341Usuario_UserGamGuid[0];
            A57Usuario_PessoaCod = BC000212_A57Usuario_PessoaCod[0];
            pr_default.close(10);
            /* Using cursor BC000213 */
            pr_default.execute(11, new Object[] {A57Usuario_PessoaCod});
            if ( (pr_default.getStatus(11) == 101) )
            {
               GX_msglist.addItem("N�o existe 'ST_Usuario_Pessoa'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
            A58Usuario_PessoaNom = BC000213_A58Usuario_PessoaNom[0];
            n58Usuario_PessoaNom = BC000213_n58Usuario_PessoaNom[0];
            pr_default.close(11);
            /* Using cursor BC000214 */
            pr_default.execute(12, new Object[] {A3Perfil_Codigo});
            if ( (pr_default.getStatus(12) == 101) )
            {
               GX_msglist.addItem("N�o existe 'Perfil'.", "ForeignKeyNotFound", 1, "PERFIL_CODIGO");
               AnyError = 1;
            }
            A4Perfil_Nome = BC000214_A4Perfil_Nome[0];
            A275Perfil_Tipo = BC000214_A275Perfil_Tipo[0];
            A329Perfil_GamId = BC000214_A329Perfil_GamId[0];
            A276Perfil_Ativo = BC000214_A276Perfil_Ativo[0];
            A7Perfil_AreaTrabalhoCod = BC000214_A7Perfil_AreaTrabalhoCod[0];
            pr_default.close(12);
         }
         else
         {
            Gx_mode = "UPD";
            Z1Usuario_Codigo = A1Usuario_Codigo;
            Z3Perfil_Codigo = A3Perfil_Codigo;
         }
         ZM023( -2) ;
         OnLoadActions023( ) ;
         AddRow023( ) ;
         ScanKeyEnd023( ) ;
         if ( RcdFound3 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Load( )
      {
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         RowToVars3( bcUsuarioPerfil, 0) ;
         ScanKeyStart023( ) ;
         if ( RcdFound3 == 0 )
         {
            Gx_mode = "INS";
            /* Using cursor BC000212 */
            pr_default.execute(10, new Object[] {A1Usuario_Codigo});
            if ( (pr_default.getStatus(10) == 101) )
            {
               GX_msglist.addItem("N�o existe 'Usuario'.", "ForeignKeyNotFound", 1, "USUARIO_CODIGO");
               AnyError = 1;
            }
            A2Usuario_Nome = BC000212_A2Usuario_Nome[0];
            n2Usuario_Nome = BC000212_n2Usuario_Nome[0];
            A341Usuario_UserGamGuid = BC000212_A341Usuario_UserGamGuid[0];
            A57Usuario_PessoaCod = BC000212_A57Usuario_PessoaCod[0];
            pr_default.close(10);
            /* Using cursor BC000213 */
            pr_default.execute(11, new Object[] {A57Usuario_PessoaCod});
            if ( (pr_default.getStatus(11) == 101) )
            {
               GX_msglist.addItem("N�o existe 'ST_Usuario_Pessoa'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
            A58Usuario_PessoaNom = BC000213_A58Usuario_PessoaNom[0];
            n58Usuario_PessoaNom = BC000213_n58Usuario_PessoaNom[0];
            pr_default.close(11);
            /* Using cursor BC000214 */
            pr_default.execute(12, new Object[] {A3Perfil_Codigo});
            if ( (pr_default.getStatus(12) == 101) )
            {
               GX_msglist.addItem("N�o existe 'Perfil'.", "ForeignKeyNotFound", 1, "PERFIL_CODIGO");
               AnyError = 1;
            }
            A4Perfil_Nome = BC000214_A4Perfil_Nome[0];
            A275Perfil_Tipo = BC000214_A275Perfil_Tipo[0];
            A329Perfil_GamId = BC000214_A329Perfil_GamId[0];
            A276Perfil_Ativo = BC000214_A276Perfil_Ativo[0];
            A7Perfil_AreaTrabalhoCod = BC000214_A7Perfil_AreaTrabalhoCod[0];
            pr_default.close(12);
         }
         else
         {
            Gx_mode = "UPD";
            Z1Usuario_Codigo = A1Usuario_Codigo;
            Z3Perfil_Codigo = A3Perfil_Codigo;
         }
         ZM023( -2) ;
         OnLoadActions023( ) ;
         AddRow023( ) ;
         ScanKeyEnd023( ) ;
         if ( RcdFound3 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Save( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         IsConfirmed = 1;
         RowToVars3( bcUsuarioPerfil, 0) ;
         nKeyPressed = 1;
         GetKey023( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            Insert023( ) ;
         }
         else
         {
            if ( RcdFound3 == 1 )
            {
               if ( ( A1Usuario_Codigo != Z1Usuario_Codigo ) || ( A3Perfil_Codigo != Z3Perfil_Codigo ) )
               {
                  A1Usuario_Codigo = Z1Usuario_Codigo;
                  A3Perfil_Codigo = Z3Perfil_Codigo;
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
               }
               else
               {
                  Gx_mode = "UPD";
                  /* Update record */
                  Update023( ) ;
               }
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else
               {
                  if ( ( A1Usuario_Codigo != Z1Usuario_Codigo ) || ( A3Perfil_Codigo != Z3Perfil_Codigo ) )
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert023( ) ;
                     }
                  }
                  else
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert023( ) ;
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         VarsToRow3( bcUsuarioPerfil) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public void Check( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         RowToVars3( bcUsuarioPerfil, 0) ;
         nKeyPressed = 3;
         IsConfirmed = 0;
         GetKey023( ) ;
         if ( RcdFound3 == 1 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( ( A1Usuario_Codigo != Z1Usuario_Codigo ) || ( A3Perfil_Codigo != Z3Perfil_Codigo ) )
            {
               A1Usuario_Codigo = Z1Usuario_Codigo;
               A3Perfil_Codigo = Z3Perfil_Codigo;
               GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               delete_Check( ) ;
            }
            else
            {
               Gx_mode = "UPD";
               update_Check( ) ;
            }
         }
         else
         {
            if ( ( A1Usuario_Codigo != Z1Usuario_Codigo ) || ( A3Perfil_Codigo != Z3Perfil_Codigo ) )
            {
               Gx_mode = "INS";
               insert_Check( ) ;
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                  AnyError = 1;
               }
               else
               {
                  Gx_mode = "INS";
                  insert_Check( ) ;
               }
            }
         }
         pr_default.close(1);
         pr_default.close(10);
         pr_default.close(12);
         pr_default.close(11);
         context.RollbackDataStores( "UsuarioPerfil_BC");
         VarsToRow3( bcUsuarioPerfil) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public int Errors( )
      {
         if ( AnyError == 0 )
         {
            return (int)(0) ;
         }
         return (int)(1) ;
      }

      public msglist GetMessages( )
      {
         return LclMsgLst ;
      }

      public String GetMode( )
      {
         Gx_mode = bcUsuarioPerfil.gxTpr_Mode;
         return Gx_mode ;
      }

      public void SetMode( String lMode )
      {
         Gx_mode = lMode;
         bcUsuarioPerfil.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void SetSDT( GxSilentTrnSdt sdt ,
                          short sdtToBc )
      {
         if ( sdt != bcUsuarioPerfil )
         {
            bcUsuarioPerfil = (SdtUsuarioPerfil)(sdt);
            if ( StringUtil.StrCmp(bcUsuarioPerfil.gxTpr_Mode, "") == 0 )
            {
               bcUsuarioPerfil.gxTpr_Mode = "INS";
            }
            if ( sdtToBc == 1 )
            {
               VarsToRow3( bcUsuarioPerfil) ;
            }
            else
            {
               RowToVars3( bcUsuarioPerfil, 1) ;
            }
         }
         else
         {
            if ( StringUtil.StrCmp(bcUsuarioPerfil.gxTpr_Mode, "") == 0 )
            {
               bcUsuarioPerfil.gxTpr_Mode = "INS";
            }
         }
         return  ;
      }

      public void ReloadFromSDT( )
      {
         RowToVars3( bcUsuarioPerfil, 1) ;
         return  ;
      }

      public SdtUsuarioPerfil UsuarioPerfil_BC
      {
         get {
            return bcUsuarioPerfil ;
         }

      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         createObjects();
         initialize();
      }

      protected override void createObjects( )
      {
      }

      protected void Process( )
      {
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(10);
         pr_default.close(12);
         pr_default.close(11);
      }

      public override void initialize( )
      {
         scmdbuf = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Gx_mode = "";
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV10TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV11WebSession = context.GetSession();
         Z2Usuario_Nome = "";
         A2Usuario_Nome = "";
         Z341Usuario_UserGamGuid = "";
         A341Usuario_UserGamGuid = "";
         Z4Perfil_Nome = "";
         A4Perfil_Nome = "";
         Z58Usuario_PessoaNom = "";
         A58Usuario_PessoaNom = "";
         BC00027_A58Usuario_PessoaNom = new String[] {""} ;
         BC00027_n58Usuario_PessoaNom = new bool[] {false} ;
         BC00027_A2Usuario_Nome = new String[] {""} ;
         BC00027_n2Usuario_Nome = new bool[] {false} ;
         BC00027_A341Usuario_UserGamGuid = new String[] {""} ;
         BC00027_A4Perfil_Nome = new String[] {""} ;
         BC00027_A275Perfil_Tipo = new short[1] ;
         BC00027_A329Perfil_GamId = new long[1] ;
         BC00027_A543UsuarioPerfil_Display = new bool[] {false} ;
         BC00027_n543UsuarioPerfil_Display = new bool[] {false} ;
         BC00027_A544UsuarioPerfil_Insert = new bool[] {false} ;
         BC00027_n544UsuarioPerfil_Insert = new bool[] {false} ;
         BC00027_A659UsuarioPerfil_Update = new bool[] {false} ;
         BC00027_n659UsuarioPerfil_Update = new bool[] {false} ;
         BC00027_A546UsuarioPerfil_Delete = new bool[] {false} ;
         BC00027_n546UsuarioPerfil_Delete = new bool[] {false} ;
         BC00027_A276Perfil_Ativo = new bool[] {false} ;
         BC00027_A1Usuario_Codigo = new int[1] ;
         BC00027_A3Perfil_Codigo = new int[1] ;
         BC00027_A57Usuario_PessoaCod = new int[1] ;
         BC00027_A7Perfil_AreaTrabalhoCod = new int[1] ;
         BC00024_A2Usuario_Nome = new String[] {""} ;
         BC00024_n2Usuario_Nome = new bool[] {false} ;
         BC00024_A341Usuario_UserGamGuid = new String[] {""} ;
         BC00024_A57Usuario_PessoaCod = new int[1] ;
         BC00026_A58Usuario_PessoaNom = new String[] {""} ;
         BC00026_n58Usuario_PessoaNom = new bool[] {false} ;
         BC00025_A4Perfil_Nome = new String[] {""} ;
         BC00025_A275Perfil_Tipo = new short[1] ;
         BC00025_A329Perfil_GamId = new long[1] ;
         BC00025_A276Perfil_Ativo = new bool[] {false} ;
         BC00025_A7Perfil_AreaTrabalhoCod = new int[1] ;
         BC00028_A1Usuario_Codigo = new int[1] ;
         BC00028_A3Perfil_Codigo = new int[1] ;
         BC00023_A543UsuarioPerfil_Display = new bool[] {false} ;
         BC00023_n543UsuarioPerfil_Display = new bool[] {false} ;
         BC00023_A544UsuarioPerfil_Insert = new bool[] {false} ;
         BC00023_n544UsuarioPerfil_Insert = new bool[] {false} ;
         BC00023_A659UsuarioPerfil_Update = new bool[] {false} ;
         BC00023_n659UsuarioPerfil_Update = new bool[] {false} ;
         BC00023_A546UsuarioPerfil_Delete = new bool[] {false} ;
         BC00023_n546UsuarioPerfil_Delete = new bool[] {false} ;
         BC00023_A1Usuario_Codigo = new int[1] ;
         BC00023_A3Perfil_Codigo = new int[1] ;
         sMode3 = "";
         BC00022_A543UsuarioPerfil_Display = new bool[] {false} ;
         BC00022_n543UsuarioPerfil_Display = new bool[] {false} ;
         BC00022_A544UsuarioPerfil_Insert = new bool[] {false} ;
         BC00022_n544UsuarioPerfil_Insert = new bool[] {false} ;
         BC00022_A659UsuarioPerfil_Update = new bool[] {false} ;
         BC00022_n659UsuarioPerfil_Update = new bool[] {false} ;
         BC00022_A546UsuarioPerfil_Delete = new bool[] {false} ;
         BC00022_n546UsuarioPerfil_Delete = new bool[] {false} ;
         BC00022_A1Usuario_Codigo = new int[1] ;
         BC00022_A3Perfil_Codigo = new int[1] ;
         BC000212_A2Usuario_Nome = new String[] {""} ;
         BC000212_n2Usuario_Nome = new bool[] {false} ;
         BC000212_A341Usuario_UserGamGuid = new String[] {""} ;
         BC000212_A57Usuario_PessoaCod = new int[1] ;
         BC000213_A58Usuario_PessoaNom = new String[] {""} ;
         BC000213_n58Usuario_PessoaNom = new bool[] {false} ;
         BC000214_A4Perfil_Nome = new String[] {""} ;
         BC000214_A275Perfil_Tipo = new short[1] ;
         BC000214_A329Perfil_GamId = new long[1] ;
         BC000214_A276Perfil_Ativo = new bool[] {false} ;
         BC000214_A7Perfil_AreaTrabalhoCod = new int[1] ;
         BC000215_A58Usuario_PessoaNom = new String[] {""} ;
         BC000215_n58Usuario_PessoaNom = new bool[] {false} ;
         BC000215_A2Usuario_Nome = new String[] {""} ;
         BC000215_n2Usuario_Nome = new bool[] {false} ;
         BC000215_A341Usuario_UserGamGuid = new String[] {""} ;
         BC000215_A4Perfil_Nome = new String[] {""} ;
         BC000215_A275Perfil_Tipo = new short[1] ;
         BC000215_A329Perfil_GamId = new long[1] ;
         BC000215_A543UsuarioPerfil_Display = new bool[] {false} ;
         BC000215_n543UsuarioPerfil_Display = new bool[] {false} ;
         BC000215_A544UsuarioPerfil_Insert = new bool[] {false} ;
         BC000215_n544UsuarioPerfil_Insert = new bool[] {false} ;
         BC000215_A659UsuarioPerfil_Update = new bool[] {false} ;
         BC000215_n659UsuarioPerfil_Update = new bool[] {false} ;
         BC000215_A546UsuarioPerfil_Delete = new bool[] {false} ;
         BC000215_n546UsuarioPerfil_Delete = new bool[] {false} ;
         BC000215_A276Perfil_Ativo = new bool[] {false} ;
         BC000215_A1Usuario_Codigo = new int[1] ;
         BC000215_A3Perfil_Codigo = new int[1] ;
         BC000215_A57Usuario_PessoaCod = new int[1] ;
         BC000215_A7Perfil_AreaTrabalhoCod = new int[1] ;
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.usuarioperfil_bc__default(),
            new Object[][] {
                new Object[] {
               BC00022_A543UsuarioPerfil_Display, BC00022_n543UsuarioPerfil_Display, BC00022_A544UsuarioPerfil_Insert, BC00022_n544UsuarioPerfil_Insert, BC00022_A659UsuarioPerfil_Update, BC00022_n659UsuarioPerfil_Update, BC00022_A546UsuarioPerfil_Delete, BC00022_n546UsuarioPerfil_Delete, BC00022_A1Usuario_Codigo, BC00022_A3Perfil_Codigo
               }
               , new Object[] {
               BC00023_A543UsuarioPerfil_Display, BC00023_n543UsuarioPerfil_Display, BC00023_A544UsuarioPerfil_Insert, BC00023_n544UsuarioPerfil_Insert, BC00023_A659UsuarioPerfil_Update, BC00023_n659UsuarioPerfil_Update, BC00023_A546UsuarioPerfil_Delete, BC00023_n546UsuarioPerfil_Delete, BC00023_A1Usuario_Codigo, BC00023_A3Perfil_Codigo
               }
               , new Object[] {
               BC00024_A2Usuario_Nome, BC00024_n2Usuario_Nome, BC00024_A341Usuario_UserGamGuid, BC00024_A57Usuario_PessoaCod
               }
               , new Object[] {
               BC00025_A4Perfil_Nome, BC00025_A275Perfil_Tipo, BC00025_A329Perfil_GamId, BC00025_A276Perfil_Ativo, BC00025_A7Perfil_AreaTrabalhoCod
               }
               , new Object[] {
               BC00026_A58Usuario_PessoaNom, BC00026_n58Usuario_PessoaNom
               }
               , new Object[] {
               BC00027_A58Usuario_PessoaNom, BC00027_n58Usuario_PessoaNom, BC00027_A2Usuario_Nome, BC00027_n2Usuario_Nome, BC00027_A341Usuario_UserGamGuid, BC00027_A4Perfil_Nome, BC00027_A275Perfil_Tipo, BC00027_A329Perfil_GamId, BC00027_A543UsuarioPerfil_Display, BC00027_n543UsuarioPerfil_Display,
               BC00027_A544UsuarioPerfil_Insert, BC00027_n544UsuarioPerfil_Insert, BC00027_A659UsuarioPerfil_Update, BC00027_n659UsuarioPerfil_Update, BC00027_A546UsuarioPerfil_Delete, BC00027_n546UsuarioPerfil_Delete, BC00027_A276Perfil_Ativo, BC00027_A1Usuario_Codigo, BC00027_A3Perfil_Codigo, BC00027_A57Usuario_PessoaCod,
               BC00027_A7Perfil_AreaTrabalhoCod
               }
               , new Object[] {
               BC00028_A1Usuario_Codigo, BC00028_A3Perfil_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               BC000212_A2Usuario_Nome, BC000212_n2Usuario_Nome, BC000212_A341Usuario_UserGamGuid, BC000212_A57Usuario_PessoaCod
               }
               , new Object[] {
               BC000213_A58Usuario_PessoaNom, BC000213_n58Usuario_PessoaNom
               }
               , new Object[] {
               BC000214_A4Perfil_Nome, BC000214_A275Perfil_Tipo, BC000214_A329Perfil_GamId, BC000214_A276Perfil_Ativo, BC000214_A7Perfil_AreaTrabalhoCod
               }
               , new Object[] {
               BC000215_A58Usuario_PessoaNom, BC000215_n58Usuario_PessoaNom, BC000215_A2Usuario_Nome, BC000215_n2Usuario_Nome, BC000215_A341Usuario_UserGamGuid, BC000215_A4Perfil_Nome, BC000215_A275Perfil_Tipo, BC000215_A329Perfil_GamId, BC000215_A543UsuarioPerfil_Display, BC000215_n543UsuarioPerfil_Display,
               BC000215_A544UsuarioPerfil_Insert, BC000215_n544UsuarioPerfil_Insert, BC000215_A659UsuarioPerfil_Update, BC000215_n659UsuarioPerfil_Update, BC000215_A546UsuarioPerfil_Delete, BC000215_n546UsuarioPerfil_Delete, BC000215_A276Perfil_Ativo, BC000215_A1Usuario_Codigo, BC000215_A3Perfil_Codigo, BC000215_A57Usuario_PessoaCod,
               BC000215_A7Perfil_AreaTrabalhoCod
               }
            }
         );
         Z543UsuarioPerfil_Display = true;
         n543UsuarioPerfil_Display = false;
         A543UsuarioPerfil_Display = true;
         n543UsuarioPerfil_Display = false;
         i543UsuarioPerfil_Display = true;
         n543UsuarioPerfil_Display = false;
         INITTRN();
         /* Execute Start event if defined. */
         /* Execute user event: E12022 */
         E12022 ();
         standaloneNotModal( ) ;
      }

      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short GX_JID ;
      private short Z275Perfil_Tipo ;
      private short A275Perfil_Tipo ;
      private short Gx_BScreen ;
      private short RcdFound3 ;
      private int trnEnded ;
      private int Z1Usuario_Codigo ;
      private int A1Usuario_Codigo ;
      private int Z3Perfil_Codigo ;
      private int A3Perfil_Codigo ;
      private int Z57Usuario_PessoaCod ;
      private int A57Usuario_PessoaCod ;
      private int Z7Perfil_AreaTrabalhoCod ;
      private int A7Perfil_AreaTrabalhoCod ;
      private long Z329Perfil_GamId ;
      private long A329Perfil_GamId ;
      private String scmdbuf ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String Gx_mode ;
      private String Z2Usuario_Nome ;
      private String A2Usuario_Nome ;
      private String Z341Usuario_UserGamGuid ;
      private String A341Usuario_UserGamGuid ;
      private String Z4Perfil_Nome ;
      private String A4Perfil_Nome ;
      private String Z58Usuario_PessoaNom ;
      private String A58Usuario_PessoaNom ;
      private String sMode3 ;
      private bool Z543UsuarioPerfil_Display ;
      private bool A543UsuarioPerfil_Display ;
      private bool Z544UsuarioPerfil_Insert ;
      private bool A544UsuarioPerfil_Insert ;
      private bool Z659UsuarioPerfil_Update ;
      private bool A659UsuarioPerfil_Update ;
      private bool Z546UsuarioPerfil_Delete ;
      private bool A546UsuarioPerfil_Delete ;
      private bool Z276Perfil_Ativo ;
      private bool A276Perfil_Ativo ;
      private bool n543UsuarioPerfil_Display ;
      private bool n58Usuario_PessoaNom ;
      private bool n2Usuario_Nome ;
      private bool n544UsuarioPerfil_Insert ;
      private bool n659UsuarioPerfil_Update ;
      private bool n546UsuarioPerfil_Delete ;
      private bool i543UsuarioPerfil_Display ;
      private IGxSession AV11WebSession ;
      private SdtUsuarioPerfil bcUsuarioPerfil ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] BC00027_A58Usuario_PessoaNom ;
      private bool[] BC00027_n58Usuario_PessoaNom ;
      private String[] BC00027_A2Usuario_Nome ;
      private bool[] BC00027_n2Usuario_Nome ;
      private String[] BC00027_A341Usuario_UserGamGuid ;
      private String[] BC00027_A4Perfil_Nome ;
      private short[] BC00027_A275Perfil_Tipo ;
      private long[] BC00027_A329Perfil_GamId ;
      private bool[] BC00027_A543UsuarioPerfil_Display ;
      private bool[] BC00027_n543UsuarioPerfil_Display ;
      private bool[] BC00027_A544UsuarioPerfil_Insert ;
      private bool[] BC00027_n544UsuarioPerfil_Insert ;
      private bool[] BC00027_A659UsuarioPerfil_Update ;
      private bool[] BC00027_n659UsuarioPerfil_Update ;
      private bool[] BC00027_A546UsuarioPerfil_Delete ;
      private bool[] BC00027_n546UsuarioPerfil_Delete ;
      private bool[] BC00027_A276Perfil_Ativo ;
      private int[] BC00027_A1Usuario_Codigo ;
      private int[] BC00027_A3Perfil_Codigo ;
      private int[] BC00027_A57Usuario_PessoaCod ;
      private int[] BC00027_A7Perfil_AreaTrabalhoCod ;
      private String[] BC00024_A2Usuario_Nome ;
      private bool[] BC00024_n2Usuario_Nome ;
      private String[] BC00024_A341Usuario_UserGamGuid ;
      private int[] BC00024_A57Usuario_PessoaCod ;
      private String[] BC00026_A58Usuario_PessoaNom ;
      private bool[] BC00026_n58Usuario_PessoaNom ;
      private String[] BC00025_A4Perfil_Nome ;
      private short[] BC00025_A275Perfil_Tipo ;
      private long[] BC00025_A329Perfil_GamId ;
      private bool[] BC00025_A276Perfil_Ativo ;
      private int[] BC00025_A7Perfil_AreaTrabalhoCod ;
      private int[] BC00028_A1Usuario_Codigo ;
      private int[] BC00028_A3Perfil_Codigo ;
      private bool[] BC00023_A543UsuarioPerfil_Display ;
      private bool[] BC00023_n543UsuarioPerfil_Display ;
      private bool[] BC00023_A544UsuarioPerfil_Insert ;
      private bool[] BC00023_n544UsuarioPerfil_Insert ;
      private bool[] BC00023_A659UsuarioPerfil_Update ;
      private bool[] BC00023_n659UsuarioPerfil_Update ;
      private bool[] BC00023_A546UsuarioPerfil_Delete ;
      private bool[] BC00023_n546UsuarioPerfil_Delete ;
      private int[] BC00023_A1Usuario_Codigo ;
      private int[] BC00023_A3Perfil_Codigo ;
      private bool[] BC00022_A543UsuarioPerfil_Display ;
      private bool[] BC00022_n543UsuarioPerfil_Display ;
      private bool[] BC00022_A544UsuarioPerfil_Insert ;
      private bool[] BC00022_n544UsuarioPerfil_Insert ;
      private bool[] BC00022_A659UsuarioPerfil_Update ;
      private bool[] BC00022_n659UsuarioPerfil_Update ;
      private bool[] BC00022_A546UsuarioPerfil_Delete ;
      private bool[] BC00022_n546UsuarioPerfil_Delete ;
      private int[] BC00022_A1Usuario_Codigo ;
      private int[] BC00022_A3Perfil_Codigo ;
      private String[] BC000212_A2Usuario_Nome ;
      private bool[] BC000212_n2Usuario_Nome ;
      private String[] BC000212_A341Usuario_UserGamGuid ;
      private int[] BC000212_A57Usuario_PessoaCod ;
      private String[] BC000213_A58Usuario_PessoaNom ;
      private bool[] BC000213_n58Usuario_PessoaNom ;
      private String[] BC000214_A4Perfil_Nome ;
      private short[] BC000214_A275Perfil_Tipo ;
      private long[] BC000214_A329Perfil_GamId ;
      private bool[] BC000214_A276Perfil_Ativo ;
      private int[] BC000214_A7Perfil_AreaTrabalhoCod ;
      private String[] BC000215_A58Usuario_PessoaNom ;
      private bool[] BC000215_n58Usuario_PessoaNom ;
      private String[] BC000215_A2Usuario_Nome ;
      private bool[] BC000215_n2Usuario_Nome ;
      private String[] BC000215_A341Usuario_UserGamGuid ;
      private String[] BC000215_A4Perfil_Nome ;
      private short[] BC000215_A275Perfil_Tipo ;
      private long[] BC000215_A329Perfil_GamId ;
      private bool[] BC000215_A543UsuarioPerfil_Display ;
      private bool[] BC000215_n543UsuarioPerfil_Display ;
      private bool[] BC000215_A544UsuarioPerfil_Insert ;
      private bool[] BC000215_n544UsuarioPerfil_Insert ;
      private bool[] BC000215_A659UsuarioPerfil_Update ;
      private bool[] BC000215_n659UsuarioPerfil_Update ;
      private bool[] BC000215_A546UsuarioPerfil_Delete ;
      private bool[] BC000215_n546UsuarioPerfil_Delete ;
      private bool[] BC000215_A276Perfil_Ativo ;
      private int[] BC000215_A1Usuario_Codigo ;
      private int[] BC000215_A3Perfil_Codigo ;
      private int[] BC000215_A57Usuario_PessoaCod ;
      private int[] BC000215_A7Perfil_AreaTrabalhoCod ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV10TrnContext ;
   }

   public class usuarioperfil_bc__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new UpdateCursor(def[7])
         ,new UpdateCursor(def[8])
         ,new UpdateCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmBC00027 ;
          prmBC00027 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Perfil_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00024 ;
          prmBC00024 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00026 ;
          prmBC00026 = new Object[] {
          new Object[] {"@Usuario_PessoaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00025 ;
          prmBC00025 = new Object[] {
          new Object[] {"@Perfil_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00028 ;
          prmBC00028 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Perfil_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00023 ;
          prmBC00023 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Perfil_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00022 ;
          prmBC00022 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Perfil_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00029 ;
          prmBC00029 = new Object[] {
          new Object[] {"@UsuarioPerfil_Display",SqlDbType.Bit,4,0} ,
          new Object[] {"@UsuarioPerfil_Insert",SqlDbType.Bit,4,0} ,
          new Object[] {"@UsuarioPerfil_Update",SqlDbType.Bit,4,0} ,
          new Object[] {"@UsuarioPerfil_Delete",SqlDbType.Bit,4,0} ,
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Perfil_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000210 ;
          prmBC000210 = new Object[] {
          new Object[] {"@UsuarioPerfil_Display",SqlDbType.Bit,4,0} ,
          new Object[] {"@UsuarioPerfil_Insert",SqlDbType.Bit,4,0} ,
          new Object[] {"@UsuarioPerfil_Update",SqlDbType.Bit,4,0} ,
          new Object[] {"@UsuarioPerfil_Delete",SqlDbType.Bit,4,0} ,
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Perfil_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000211 ;
          prmBC000211 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Perfil_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000215 ;
          prmBC000215 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Perfil_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000212 ;
          prmBC000212 = new Object[] {
          new Object[] {"@Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000213 ;
          prmBC000213 = new Object[] {
          new Object[] {"@Usuario_PessoaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000214 ;
          prmBC000214 = new Object[] {
          new Object[] {"@Perfil_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("BC00022", "SELECT [UsuarioPerfil_Display], [UsuarioPerfil_Insert], [UsuarioPerfil_Update], [UsuarioPerfil_Delete], [Usuario_Codigo], [Perfil_Codigo] FROM [UsuarioPerfil] WITH (UPDLOCK) WHERE [Usuario_Codigo] = @Usuario_Codigo AND [Perfil_Codigo] = @Perfil_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00022,1,0,true,false )
             ,new CursorDef("BC00023", "SELECT [UsuarioPerfil_Display], [UsuarioPerfil_Insert], [UsuarioPerfil_Update], [UsuarioPerfil_Delete], [Usuario_Codigo], [Perfil_Codigo] FROM [UsuarioPerfil] WITH (NOLOCK) WHERE [Usuario_Codigo] = @Usuario_Codigo AND [Perfil_Codigo] = @Perfil_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00023,1,0,true,false )
             ,new CursorDef("BC00024", "SELECT [Usuario_Nome], [Usuario_UserGamGuid], [Usuario_PessoaCod] AS Usuario_PessoaCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @Usuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00024,1,0,true,false )
             ,new CursorDef("BC00025", "SELECT [Perfil_Nome], [Perfil_Tipo], [Perfil_GamId], [Perfil_Ativo], [Perfil_AreaTrabalhoCod] FROM [Perfil] WITH (NOLOCK) WHERE [Perfil_Codigo] = @Perfil_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00025,1,0,true,false )
             ,new CursorDef("BC00026", "SELECT [Pessoa_Nome] AS Usuario_PessoaNom FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @Usuario_PessoaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00026,1,0,true,false )
             ,new CursorDef("BC00027", "SELECT T3.[Pessoa_Nome] AS Usuario_PessoaNom, T2.[Usuario_Nome], T2.[Usuario_UserGamGuid], T4.[Perfil_Nome], T4.[Perfil_Tipo], T4.[Perfil_GamId], TM1.[UsuarioPerfil_Display], TM1.[UsuarioPerfil_Insert], TM1.[UsuarioPerfil_Update], TM1.[UsuarioPerfil_Delete], T4.[Perfil_Ativo], TM1.[Usuario_Codigo], TM1.[Perfil_Codigo], T2.[Usuario_PessoaCod] AS Usuario_PessoaCod, T4.[Perfil_AreaTrabalhoCod] FROM ((([UsuarioPerfil] TM1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = TM1.[Usuario_Codigo]) INNER JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod]) INNER JOIN [Perfil] T4 WITH (NOLOCK) ON T4.[Perfil_Codigo] = TM1.[Perfil_Codigo]) WHERE TM1.[Usuario_Codigo] = @Usuario_Codigo and TM1.[Perfil_Codigo] = @Perfil_Codigo ORDER BY TM1.[Usuario_Codigo], TM1.[Perfil_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC00027,100,0,true,false )
             ,new CursorDef("BC00028", "SELECT [Usuario_Codigo], [Perfil_Codigo] FROM [UsuarioPerfil] WITH (NOLOCK) WHERE [Usuario_Codigo] = @Usuario_Codigo AND [Perfil_Codigo] = @Perfil_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmBC00028,1,0,true,false )
             ,new CursorDef("BC00029", "INSERT INTO [UsuarioPerfil]([UsuarioPerfil_Display], [UsuarioPerfil_Insert], [UsuarioPerfil_Update], [UsuarioPerfil_Delete], [Usuario_Codigo], [Perfil_Codigo]) VALUES(@UsuarioPerfil_Display, @UsuarioPerfil_Insert, @UsuarioPerfil_Update, @UsuarioPerfil_Delete, @Usuario_Codigo, @Perfil_Codigo)", GxErrorMask.GX_NOMASK,prmBC00029)
             ,new CursorDef("BC000210", "UPDATE [UsuarioPerfil] SET [UsuarioPerfil_Display]=@UsuarioPerfil_Display, [UsuarioPerfil_Insert]=@UsuarioPerfil_Insert, [UsuarioPerfil_Update]=@UsuarioPerfil_Update, [UsuarioPerfil_Delete]=@UsuarioPerfil_Delete  WHERE [Usuario_Codigo] = @Usuario_Codigo AND [Perfil_Codigo] = @Perfil_Codigo", GxErrorMask.GX_NOMASK,prmBC000210)
             ,new CursorDef("BC000211", "DELETE FROM [UsuarioPerfil]  WHERE [Usuario_Codigo] = @Usuario_Codigo AND [Perfil_Codigo] = @Perfil_Codigo", GxErrorMask.GX_NOMASK,prmBC000211)
             ,new CursorDef("BC000212", "SELECT [Usuario_Nome], [Usuario_UserGamGuid], [Usuario_PessoaCod] AS Usuario_PessoaCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @Usuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000212,1,0,true,false )
             ,new CursorDef("BC000213", "SELECT [Pessoa_Nome] AS Usuario_PessoaNom FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @Usuario_PessoaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000213,1,0,true,false )
             ,new CursorDef("BC000214", "SELECT [Perfil_Nome], [Perfil_Tipo], [Perfil_GamId], [Perfil_Ativo], [Perfil_AreaTrabalhoCod] FROM [Perfil] WITH (NOLOCK) WHERE [Perfil_Codigo] = @Perfil_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000214,1,0,true,false )
             ,new CursorDef("BC000215", "SELECT T3.[Pessoa_Nome] AS Usuario_PessoaNom, T2.[Usuario_Nome], T2.[Usuario_UserGamGuid], T4.[Perfil_Nome], T4.[Perfil_Tipo], T4.[Perfil_GamId], TM1.[UsuarioPerfil_Display], TM1.[UsuarioPerfil_Insert], TM1.[UsuarioPerfil_Update], TM1.[UsuarioPerfil_Delete], T4.[Perfil_Ativo], TM1.[Usuario_Codigo], TM1.[Perfil_Codigo], T2.[Usuario_PessoaCod] AS Usuario_PessoaCod, T4.[Perfil_AreaTrabalhoCod] FROM ((([UsuarioPerfil] TM1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = TM1.[Usuario_Codigo]) INNER JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod]) INNER JOIN [Perfil] T4 WITH (NOLOCK) ON T4.[Perfil_Codigo] = TM1.[Perfil_Codigo]) WHERE TM1.[Usuario_Codigo] = @Usuario_Codigo and TM1.[Perfil_Codigo] = @Perfil_Codigo ORDER BY TM1.[Usuario_Codigo], TM1.[Perfil_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC000215,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((bool[]) buf[2])[0] = rslt.getBool(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((bool[]) buf[4])[0] = rslt.getBool(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((bool[]) buf[6])[0] = rslt.getBool(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((int[]) buf[8])[0] = rslt.getInt(5) ;
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                return;
             case 1 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((bool[]) buf[2])[0] = rslt.getBool(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((bool[]) buf[4])[0] = rslt.getBool(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((bool[]) buf[6])[0] = rslt.getBool(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((int[]) buf[8])[0] = rslt.getInt(5) ;
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 40) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                return;
             case 3 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((long[]) buf[2])[0] = rslt.getLong(3) ;
                ((bool[]) buf[3])[0] = rslt.getBool(4) ;
                ((int[]) buf[4])[0] = rslt.getInt(5) ;
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 5 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getString(3, 40) ;
                ((String[]) buf[5])[0] = rslt.getString(4, 50) ;
                ((short[]) buf[6])[0] = rslt.getShort(5) ;
                ((long[]) buf[7])[0] = rslt.getLong(6) ;
                ((bool[]) buf[8])[0] = rslt.getBool(7) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(7);
                ((bool[]) buf[10])[0] = rslt.getBool(8) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(8);
                ((bool[]) buf[12])[0] = rslt.getBool(9) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(9);
                ((bool[]) buf[14])[0] = rslt.getBool(10) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(10);
                ((bool[]) buf[16])[0] = rslt.getBool(11) ;
                ((int[]) buf[17])[0] = rslt.getInt(12) ;
                ((int[]) buf[18])[0] = rslt.getInt(13) ;
                ((int[]) buf[19])[0] = rslt.getInt(14) ;
                ((int[]) buf[20])[0] = rslt.getInt(15) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 10 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 40) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                return;
             case 11 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 12 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((long[]) buf[2])[0] = rslt.getLong(3) ;
                ((bool[]) buf[3])[0] = rslt.getBool(4) ;
                ((int[]) buf[4])[0] = rslt.getInt(5) ;
                return;
             case 13 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getString(3, 40) ;
                ((String[]) buf[5])[0] = rslt.getString(4, 50) ;
                ((short[]) buf[6])[0] = rslt.getShort(5) ;
                ((long[]) buf[7])[0] = rslt.getLong(6) ;
                ((bool[]) buf[8])[0] = rslt.getBool(7) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(7);
                ((bool[]) buf[10])[0] = rslt.getBool(8) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(8);
                ((bool[]) buf[12])[0] = rslt.getBool(9) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(9);
                ((bool[]) buf[14])[0] = rslt.getBool(10) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(10);
                ((bool[]) buf[16])[0] = rslt.getBool(11) ;
                ((int[]) buf[17])[0] = rslt.getInt(12) ;
                ((int[]) buf[18])[0] = rslt.getInt(13) ;
                ((int[]) buf[19])[0] = rslt.getInt(14) ;
                ((int[]) buf[20])[0] = rslt.getInt(15) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 7 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(1, (bool)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(2, (bool)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(3, (bool)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 4 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(4, (bool)parms[7]);
                }
                stmt.SetParameter(5, (int)parms[8]);
                stmt.SetParameter(6, (int)parms[9]);
                return;
             case 8 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(1, (bool)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(2, (bool)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(3, (bool)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 4 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(4, (bool)parms[7]);
                }
                stmt.SetParameter(5, (int)parms[8]);
                stmt.SetParameter(6, (int)parms[9]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
       }
    }

 }

}
