/*
               File: WWContrato
        Description: Work With Contrato
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/29/2020 9:34:56.93
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wwcontrato : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wwcontrato( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wwcontrato( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         dynavContrato_areatrabalhocod = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersoperator1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersoperator2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         cmbavDynamicfiltersoperator3 = new GXCombobox();
         chkContrato_Ativo = new GXCheckbox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_128 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_128_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_128_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               AV50Contrato_AreaTrabalhoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50Contrato_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV50Contrato_AreaTrabalhoCod), 6, 0)));
               AV15DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
               AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV40Contrato_Numero1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40Contrato_Numero1", AV40Contrato_Numero1);
               AV51Contrato_NumeroAta1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51Contrato_NumeroAta1", AV51Contrato_NumeroAta1);
               AV133Contrato_PrepostoNom1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV133Contrato_PrepostoNom1", AV133Contrato_PrepostoNom1);
               AV18Contratada_PessoaNom1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Contratada_PessoaNom1", AV18Contratada_PessoaNom1);
               AV43Contrato_Ano1 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43Contrato_Ano1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV43Contrato_Ano1), 4, 0)));
               AV44Contrato_Ano_To1 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44Contrato_Ano_To1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44Contrato_Ano_To1), 4, 0)));
               AV20DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
               AV21DynamicFiltersOperator2 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
               AV41Contrato_Numero2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41Contrato_Numero2", AV41Contrato_Numero2);
               AV52Contrato_NumeroAta2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52Contrato_NumeroAta2", AV52Contrato_NumeroAta2);
               AV134Contrato_PrepostoNom2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV134Contrato_PrepostoNom2", AV134Contrato_PrepostoNom2);
               AV23Contratada_PessoaNom2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Contratada_PessoaNom2", AV23Contratada_PessoaNom2);
               AV45Contrato_Ano2 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45Contrato_Ano2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV45Contrato_Ano2), 4, 0)));
               AV46Contrato_Ano_To2 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46Contrato_Ano_To2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV46Contrato_Ano_To2), 4, 0)));
               AV25DynamicFiltersSelector3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
               AV26DynamicFiltersOperator3 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0)));
               AV42Contrato_Numero3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42Contrato_Numero3", AV42Contrato_Numero3);
               AV53Contrato_NumeroAta3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53Contrato_NumeroAta3", AV53Contrato_NumeroAta3);
               AV135Contrato_PrepostoNom3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV135Contrato_PrepostoNom3", AV135Contrato_PrepostoNom3);
               AV28Contratada_PessoaNom3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28Contratada_PessoaNom3", AV28Contratada_PessoaNom3);
               AV47Contrato_Ano3 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47Contrato_Ano3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV47Contrato_Ano3), 4, 0)));
               AV48Contrato_Ano_To3 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48Contrato_Ano_To3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV48Contrato_Ano_To3), 4, 0)));
               AV19DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
               AV24DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersEnabled3", AV24DynamicFiltersEnabled3);
               AV121TFContrato_Ativo_Sel = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV121TFContrato_Ativo_Sel", StringUtil.Str( (decimal)(AV121TFContrato_Ativo_Sel), 1, 0));
               AV132ddo_Contrato_ValorUndCntAtualTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV132ddo_Contrato_ValorUndCntAtualTitleControlIdToReplace", AV132ddo_Contrato_ValorUndCntAtualTitleControlIdToReplace);
               AV122ddo_Contrato_AtivoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV122ddo_Contrato_AtivoTitleControlIdToReplace", AV122ddo_Contrato_AtivoTitleControlIdToReplace);
               AV130TFContrato_ValorUndCntAtual = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV130TFContrato_ValorUndCntAtual", StringUtil.LTrim( StringUtil.Str( AV130TFContrato_ValorUndCntAtual, 18, 5)));
               AV131TFContrato_ValorUndCntAtual_To = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV131TFContrato_ValorUndCntAtual_To", StringUtil.LTrim( StringUtil.Str( AV131TFContrato_ValorUndCntAtual_To, 18, 5)));
               AV189Pgmname = GetNextPar( );
               ajax_req_read_hidden_sdt(GetNextPar( ), AV10GridState);
               AV30DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersIgnoreFirst", AV30DynamicFiltersIgnoreFirst);
               AV29DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
               A39Contratada_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A39Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A39Contratada_Codigo), 6, 0)));
               ajax_req_read_hidden_sdt(GetNextPar( ), AV6WWPContext);
               A74Contrato_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               n74Contrato_Codigo = false;
               A92Contrato_Ativo = (bool)(BooleanUtil.Val(GetNextPar( )));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV50Contrato_AreaTrabalhoCod, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV40Contrato_Numero1, AV51Contrato_NumeroAta1, AV133Contrato_PrepostoNom1, AV18Contratada_PessoaNom1, AV43Contrato_Ano1, AV44Contrato_Ano_To1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV41Contrato_Numero2, AV52Contrato_NumeroAta2, AV134Contrato_PrepostoNom2, AV23Contratada_PessoaNom2, AV45Contrato_Ano2, AV46Contrato_Ano_To2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV42Contrato_Numero3, AV53Contrato_NumeroAta3, AV135Contrato_PrepostoNom3, AV28Contratada_PessoaNom3, AV47Contrato_Ano3, AV48Contrato_Ano_To3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV121TFContrato_Ativo_Sel, AV132ddo_Contrato_ValorUndCntAtualTitleControlIdToReplace, AV122ddo_Contrato_AtivoTitleControlIdToReplace, AV130TFContrato_ValorUndCntAtual, AV131TFContrato_ValorUndCntAtual_To, AV189Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving, A39Contratada_Codigo, AV6WWPContext, A74Contrato_Codigo, A92Contrato_Ativo) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PA652( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            START652( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20205299345744");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wwcontrato.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATO_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV50Contrato_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV15DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV16DynamicFiltersOperator1), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATO_NUMERO1", StringUtil.RTrim( AV40Contrato_Numero1));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATO_NUMEROATA1", StringUtil.RTrim( AV51Contrato_NumeroAta1));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATO_PREPOSTONOM1", StringUtil.RTrim( AV133Contrato_PrepostoNom1));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATADA_PESSOANOM1", StringUtil.RTrim( AV18Contratada_PessoaNom1));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATO_ANO1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV43Contrato_Ano1), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATO_ANO_TO1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV44Contrato_Ano_To1), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV20DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV21DynamicFiltersOperator2), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATO_NUMERO2", StringUtil.RTrim( AV41Contrato_Numero2));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATO_NUMEROATA2", StringUtil.RTrim( AV52Contrato_NumeroAta2));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATO_PREPOSTONOM2", StringUtil.RTrim( AV134Contrato_PrepostoNom2));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATADA_PESSOANOM2", StringUtil.RTrim( AV23Contratada_PessoaNom2));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATO_ANO2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV45Contrato_Ano2), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATO_ANO_TO2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV46Contrato_Ano_To2), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR3", AV25DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR3", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV26DynamicFiltersOperator3), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATO_NUMERO3", StringUtil.RTrim( AV42Contrato_Numero3));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATO_NUMEROATA3", StringUtil.RTrim( AV53Contrato_NumeroAta3));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATO_PREPOSTONOM3", StringUtil.RTrim( AV135Contrato_PrepostoNom3));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATADA_PESSOANOM3", StringUtil.RTrim( AV28Contratada_PessoaNom3));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATO_ANO3", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV47Contrato_Ano3), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATO_ANO_TO3", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV48Contrato_Ano_To3), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV19DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV24DynamicFiltersEnabled3));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATO_ATIVO_SEL", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV121TFContrato_Ativo_Sel), 1, 0, ",", "")));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_128", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_128), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV117GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV118GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV115DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV115DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATO_VALORUNDCNTATUALTITLEFILTERDATA", AV129Contrato_ValorUndCntAtualTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATO_VALORUNDCNTATUALTITLEFILTERDATA", AV129Contrato_ValorUndCntAtualTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATO_ATIVOTITLEFILTERDATA", AV120Contrato_AtivoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATO_ATIVOTITLEFILTERDATA", AV120Contrato_AtivoTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV189Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV30DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV29DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, "CONTRATADA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A39Contratada_Codigo), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vWWPCONTEXT", AV6WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vWWPCONTEXT", AV6WWPContext);
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATO_DATAVIGENCIATERMINO", context.localUtil.DToC( A83Contrato_DataVigenciaTermino, 0, "/"));
         GxWebStd.gx_hidden_field( context, "CONTRATO_DATAFIMTA", context.localUtil.DToC( A843Contrato_DataFimTA, 0, "/"));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_VALORUNDCNTATUAL_Caption", StringUtil.RTrim( Ddo_contrato_valorundcntatual_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_VALORUNDCNTATUAL_Tooltip", StringUtil.RTrim( Ddo_contrato_valorundcntatual_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_VALORUNDCNTATUAL_Cls", StringUtil.RTrim( Ddo_contrato_valorundcntatual_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_VALORUNDCNTATUAL_Filteredtext_set", StringUtil.RTrim( Ddo_contrato_valorundcntatual_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_VALORUNDCNTATUAL_Filteredtextto_set", StringUtil.RTrim( Ddo_contrato_valorundcntatual_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_VALORUNDCNTATUAL_Dropdownoptionstype", StringUtil.RTrim( Ddo_contrato_valorundcntatual_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_VALORUNDCNTATUAL_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contrato_valorundcntatual_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_VALORUNDCNTATUAL_Includesortasc", StringUtil.BoolToStr( Ddo_contrato_valorundcntatual_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_VALORUNDCNTATUAL_Includesortdsc", StringUtil.BoolToStr( Ddo_contrato_valorundcntatual_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_VALORUNDCNTATUAL_Includefilter", StringUtil.BoolToStr( Ddo_contrato_valorundcntatual_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_VALORUNDCNTATUAL_Filtertype", StringUtil.RTrim( Ddo_contrato_valorundcntatual_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_VALORUNDCNTATUAL_Filterisrange", StringUtil.BoolToStr( Ddo_contrato_valorundcntatual_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_VALORUNDCNTATUAL_Includedatalist", StringUtil.BoolToStr( Ddo_contrato_valorundcntatual_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_VALORUNDCNTATUAL_Cleanfilter", StringUtil.RTrim( Ddo_contrato_valorundcntatual_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_VALORUNDCNTATUAL_Rangefilterfrom", StringUtil.RTrim( Ddo_contrato_valorundcntatual_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_VALORUNDCNTATUAL_Rangefilterto", StringUtil.RTrim( Ddo_contrato_valorundcntatual_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_VALORUNDCNTATUAL_Searchbuttontext", StringUtil.RTrim( Ddo_contrato_valorundcntatual_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_ATIVO_Caption", StringUtil.RTrim( Ddo_contrato_ativo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_ATIVO_Tooltip", StringUtil.RTrim( Ddo_contrato_ativo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_ATIVO_Cls", StringUtil.RTrim( Ddo_contrato_ativo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_ATIVO_Selectedvalue_set", StringUtil.RTrim( Ddo_contrato_ativo_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_ATIVO_Dropdownoptionstype", StringUtil.RTrim( Ddo_contrato_ativo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_ATIVO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contrato_ativo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_ATIVO_Includesortasc", StringUtil.BoolToStr( Ddo_contrato_ativo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_ATIVO_Includesortdsc", StringUtil.BoolToStr( Ddo_contrato_ativo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_ATIVO_Sortedstatus", StringUtil.RTrim( Ddo_contrato_ativo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_ATIVO_Includefilter", StringUtil.BoolToStr( Ddo_contrato_ativo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_ATIVO_Includedatalist", StringUtil.BoolToStr( Ddo_contrato_ativo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_ATIVO_Datalisttype", StringUtil.RTrim( Ddo_contrato_ativo_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_ATIVO_Datalistfixedvalues", StringUtil.RTrim( Ddo_contrato_ativo_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_ATIVO_Sortasc", StringUtil.RTrim( Ddo_contrato_ativo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_ATIVO_Sortdsc", StringUtil.RTrim( Ddo_contrato_ativo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_ATIVO_Cleanfilter", StringUtil.RTrim( Ddo_contrato_ativo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_ATIVO_Searchbuttontext", StringUtil.RTrim( Ddo_contrato_ativo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_VALORUNDCNTATUAL_Activeeventkey", StringUtil.RTrim( Ddo_contrato_valorundcntatual_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_VALORUNDCNTATUAL_Filteredtext_get", StringUtil.RTrim( Ddo_contrato_valorundcntatual_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_VALORUNDCNTATUAL_Filteredtextto_get", StringUtil.RTrim( Ddo_contrato_valorundcntatual_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_ATIVO_Activeeventkey", StringUtil.RTrim( Ddo_contrato_ativo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATO_ATIVO_Selectedvalue_get", StringUtil.RTrim( Ddo_contrato_ativo_Selectedvalue_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WE652( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVT652( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wwcontrato.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WWContrato" ;
      }

      public override String GetPgmdesc( )
      {
         return "Work With Contrato" ;
      }

      protected void WB650( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_652( true) ;
         }
         else
         {
            wb_table1_2_652( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_652e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 149,'',false,'" + sGXsfl_128_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV19DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(149, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,149);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 150,'',false,'" + sGXsfl_128_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV24DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(150, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,150);\"");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 151,'',false,'" + sGXsfl_128_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontrato_valorundcntatual_Internalname, StringUtil.LTrim( StringUtil.NToC( AV130TFContrato_ValorUndCntAtual, 18, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV130TFContrato_ValorUndCntAtual, "ZZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,151);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontrato_valorundcntatual_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontrato_valorundcntatual_Visible, 1, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContrato.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 152,'',false,'" + sGXsfl_128_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontrato_valorundcntatual_to_Internalname, StringUtil.LTrim( StringUtil.NToC( AV131TFContrato_ValorUndCntAtual_To, 18, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV131TFContrato_ValorUndCntAtual_To, "ZZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,152);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontrato_valorundcntatual_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontrato_valorundcntatual_to_Visible, 1, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContrato.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 153,'',false,'" + sGXsfl_128_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontrato_ativo_sel_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV121TFContrato_Ativo_Sel), 1, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV121TFContrato_Ativo_Sel), "9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,153);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontrato_ativo_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontrato_ativo_sel_Visible, 1, 0, "text", "", 1, "chr", 1, "row", 1, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContrato.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATO_VALORUNDCNTATUALContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 155,'',false,'" + sGXsfl_128_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contrato_valorundcntatualtitlecontrolidtoreplace_Internalname, AV132ddo_Contrato_ValorUndCntAtualTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,155);\"", 0, edtavDdo_contrato_valorundcntatualtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWContrato.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATO_ATIVOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 157,'',false,'" + sGXsfl_128_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contrato_ativotitlecontrolidtoreplace_Internalname, AV122ddo_Contrato_AtivoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,157);\"", 0, edtavDdo_contrato_ativotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWContrato.htm");
         }
         wbLoad = true;
      }

      protected void START652( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Work With Contrato", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUP650( ) ;
      }

      protected void WS652( )
      {
         START652( ) ;
         EVT652( ) ;
      }

      protected void EVT652( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11652 */
                              E11652 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATO_VALORUNDCNTATUAL.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12652 */
                              E12652 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATO_ATIVO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E13652 */
                              E13652 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E14652 */
                              E14652 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E15652 */
                              E15652 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E16652 */
                              E16652 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E17652 */
                              E17652 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E18652 */
                              E18652 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E19652 */
                              E19652 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E20652 */
                              E20652 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E21652 */
                              E21652 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E22652 */
                              E22652 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR3.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E23652 */
                              E23652 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              nGXsfl_128_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_128_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_128_idx), 4, 0)), 4, "0");
                              SubsflControlProps_1282( ) ;
                              AV31Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV31Update)) ? AV183Update_GXI : context.convertURL( context.PathToRelativeUrl( AV31Update))));
                              AV32Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV32Delete)) ? AV184Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV32Delete))));
                              AV49Display = cgiGet( edtavDisplay_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDisplay_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV49Display)) ? AV185Display_GXI : context.convertURL( context.PathToRelativeUrl( AV49Display))));
                              A74Contrato_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContrato_Codigo_Internalname), ",", "."));
                              n74Contrato_Codigo = false;
                              A41Contratada_PessoaNom = StringUtil.Upper( cgiGet( edtContratada_PessoaNom_Internalname));
                              n41Contratada_PessoaNom = false;
                              A77Contrato_Numero = cgiGet( edtContrato_Numero_Internalname);
                              A78Contrato_NumeroAta = cgiGet( edtContrato_NumeroAta_Internalname);
                              n78Contrato_NumeroAta = false;
                              A79Contrato_Ano = (short)(context.localUtil.CToN( cgiGet( edtContrato_Ano_Internalname), ",", "."));
                              A1869Contrato_DataTermino = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtContrato_DataTermino_Internalname), 0));
                              A1870Contrato_ValorUndCntAtual = context.localUtil.CToN( cgiGet( edtContrato_ValorUndCntAtual_Internalname), ",", ".");
                              n1870Contrato_ValorUndCntAtual = false;
                              if ( ( ( context.localUtil.CToN( cgiGet( edtavSaldocontrato_saldo_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavSaldocontrato_saldo_Internalname), ",", ".") > 999999999999.99999m ) ) )
                              {
                                 GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vSALDOCONTRATO_SALDO");
                                 GX_FocusControl = edtavSaldocontrato_saldo_Internalname;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                                 wbErr = true;
                                 AV78SaldoContrato_Saldo = 0;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavSaldocontrato_saldo_Internalname, StringUtil.LTrim( StringUtil.Str( AV78SaldoContrato_Saldo, 18, 5)));
                              }
                              else
                              {
                                 AV78SaldoContrato_Saldo = context.localUtil.CToN( cgiGet( edtavSaldocontrato_saldo_Internalname), ",", ".");
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavSaldocontrato_saldo_Internalname, StringUtil.LTrim( StringUtil.Str( AV78SaldoContrato_Saldo, 18, 5)));
                              }
                              A42Contratada_PessoaCNPJ = cgiGet( edtContratada_PessoaCNPJ_Internalname);
                              n42Contratada_PessoaCNPJ = false;
                              A92Contrato_Ativo = StringUtil.StrToBool( cgiGet( chkContrato_Ativo_Internalname));
                              AV75AssociarGestor = cgiGet( edtavAssociargestor_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAssociargestor_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV75AssociarGestor)) ? AV186Associargestor_GXI : context.convertURL( context.PathToRelativeUrl( AV75AssociarGestor))));
                              AV119AssociarAuxiliar = cgiGet( edtavAssociarauxiliar_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAssociarauxiliar_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV119AssociarAuxiliar)) ? AV187Associarauxiliar_GXI : context.convertURL( context.PathToRelativeUrl( AV119AssociarAuxiliar))));
                              AV80AssociarSistema = cgiGet( edtavAssociarsistema_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAssociarsistema_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV80AssociarSistema)) ? AV188Associarsistema_GXI : context.convertURL( context.PathToRelativeUrl( AV80AssociarSistema))));
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E24652 */
                                    E24652 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E25652 */
                                    E25652 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E26652 */
                                    E26652 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       /* Set Refresh If Orderedby Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Ordereddsc Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contrato_areatrabalhocod Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTRATO_AREATRABALHOCOD"), ",", ".") != Convert.ToDecimal( AV50Contrato_AreaTrabalhoCod )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersoperator1 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV16DynamicFiltersOperator1 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contrato_numero1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATO_NUMERO1"), AV40Contrato_Numero1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contrato_numeroata1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATO_NUMEROATA1"), AV51Contrato_NumeroAta1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contrato_prepostonom1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATO_PREPOSTONOM1"), AV133Contrato_PrepostoNom1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contratada_pessoanom1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATADA_PESSOANOM1"), AV18Contratada_PessoaNom1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contrato_ano1 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTRATO_ANO1"), ",", ".") != Convert.ToDecimal( AV43Contrato_Ano1 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contrato_ano_to1 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTRATO_ANO_TO1"), ",", ".") != Convert.ToDecimal( AV44Contrato_Ano_To1 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV20DynamicFiltersSelector2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersoperator2 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV21DynamicFiltersOperator2 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contrato_numero2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATO_NUMERO2"), AV41Contrato_Numero2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contrato_numeroata2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATO_NUMEROATA2"), AV52Contrato_NumeroAta2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contrato_prepostonom2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATO_PREPOSTONOM2"), AV134Contrato_PrepostoNom2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contratada_pessoanom2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATADA_PESSOANOM2"), AV23Contratada_PessoaNom2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contrato_ano2 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTRATO_ANO2"), ",", ".") != Convert.ToDecimal( AV45Contrato_Ano2 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contrato_ano_to2 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTRATO_ANO_TO2"), ",", ".") != Convert.ToDecimal( AV46Contrato_Ano_To2 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV25DynamicFiltersSelector3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersoperator3 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV26DynamicFiltersOperator3 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contrato_numero3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATO_NUMERO3"), AV42Contrato_Numero3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contrato_numeroata3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATO_NUMEROATA3"), AV53Contrato_NumeroAta3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contrato_prepostonom3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATO_PREPOSTONOM3"), AV135Contrato_PrepostoNom3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contratada_pessoanom3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATADA_PESSOANOM3"), AV28Contratada_PessoaNom3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contrato_ano3 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTRATO_ANO3"), ",", ".") != Convert.ToDecimal( AV47Contrato_Ano3 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contrato_ano_to3 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTRATO_ANO_TO3"), ",", ".") != Convert.ToDecimal( AV48Contrato_Ano_To3 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV19DynamicFiltersEnabled2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV24DynamicFiltersEnabled3 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontrato_ativo_sel Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATO_ATIVO_SEL"), ",", ".") != Convert.ToDecimal( AV121TFContrato_Ativo_Sel )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE652( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PA652( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            dynavContrato_areatrabalhocod.Name = "vCONTRATO_AREATRABALHOCOD";
            dynavContrato_areatrabalhocod.WebTags = "";
            dynavContrato_areatrabalhocod.removeAllItems();
            /* Using cursor H00652 */
            pr_default.execute(0);
            while ( (pr_default.getStatus(0) != 101) )
            {
               dynavContrato_areatrabalhocod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(H00652_A5AreaTrabalho_Codigo[0]), 6, 0)), H00652_A6AreaTrabalho_Descricao[0], 0);
               pr_default.readNext(0);
            }
            pr_default.close(0);
            if ( dynavContrato_areatrabalhocod.ItemCount > 0 )
            {
               AV50Contrato_AreaTrabalhoCod = (int)(NumberUtil.Val( dynavContrato_areatrabalhocod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV50Contrato_AreaTrabalhoCod), 6, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50Contrato_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV50Contrato_AreaTrabalhoCod), 6, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("CONTRATO_NUMERO", "N� de Contrato", 0);
            cmbavDynamicfiltersselector1.addItem("CONTRATO_NUMEROATA", "N�mero da Ata", 0);
            cmbavDynamicfiltersselector1.addItem("CONTRATO_PREPOSTONOM", "Preposto", 0);
            cmbavDynamicfiltersselector1.addItem("CONTRATADA_PESSOANOM", "Contratada", 0);
            cmbavDynamicfiltersselector1.addItem("CONTRATO_ANO", "Ano", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersoperator1.Name = "vDYNAMICFILTERSOPERATOR1";
            cmbavDynamicfiltersoperator1.WebTags = "";
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
            {
               AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("CONTRATO_NUMERO", "N� de Contrato", 0);
            cmbavDynamicfiltersselector2.addItem("CONTRATO_NUMEROATA", "N�mero da Ata", 0);
            cmbavDynamicfiltersselector2.addItem("CONTRATO_PREPOSTONOM", "Preposto", 0);
            cmbavDynamicfiltersselector2.addItem("CONTRATADA_PESSOANOM", "Contratada", 0);
            cmbavDynamicfiltersselector2.addItem("CONTRATO_ANO", "Ano", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV20DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV20DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersoperator2.Name = "vDYNAMICFILTERSOPERATOR2";
            cmbavDynamicfiltersoperator2.WebTags = "";
            cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
            {
               AV21DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
            }
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("CONTRATO_NUMERO", "N� de Contrato", 0);
            cmbavDynamicfiltersselector3.addItem("CONTRATO_NUMEROATA", "N�mero da Ata", 0);
            cmbavDynamicfiltersselector3.addItem("CONTRATO_PREPOSTONOM", "Preposto", 0);
            cmbavDynamicfiltersselector3.addItem("CONTRATADA_PESSOANOM", "Contratada", 0);
            cmbavDynamicfiltersselector3.addItem("CONTRATO_ANO", "Ano", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV25DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV25DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
            }
            cmbavDynamicfiltersoperator3.Name = "vDYNAMICFILTERSOPERATOR3";
            cmbavDynamicfiltersoperator3.WebTags = "";
            cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
            {
               AV26DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0)));
            }
            GXCCtl = "CONTRATO_ATIVO_" + sGXsfl_128_idx;
            chkContrato_Ativo.Name = GXCCtl;
            chkContrato_Ativo.WebTags = "";
            chkContrato_Ativo.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkContrato_Ativo_Internalname, "TitleCaption", chkContrato_Ativo.Caption);
            chkContrato_Ativo.CheckedValue = "false";
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLVvCONTRATO_AREATRABALHOCOD651( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvCONTRATO_AREATRABALHOCOD_data651( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvCONTRATO_AREATRABALHOCOD_html651( )
      {
         int gxdynajaxvalue ;
         GXDLVvCONTRATO_AREATRABALHOCOD_data651( ) ;
         gxdynajaxindex = 1;
         dynavContrato_areatrabalhocod.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavContrato_areatrabalhocod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavContrato_areatrabalhocod.ItemCount > 0 )
         {
            AV50Contrato_AreaTrabalhoCod = (int)(NumberUtil.Val( dynavContrato_areatrabalhocod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV50Contrato_AreaTrabalhoCod), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50Contrato_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV50Contrato_AreaTrabalhoCod), 6, 0)));
         }
      }

      protected void GXDLVvCONTRATO_AREATRABALHOCOD_data651( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         /* Using cursor H00653 */
         pr_default.execute(1);
         while ( (pr_default.getStatus(1) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00653_A5AreaTrabalho_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(H00653_A6AreaTrabalho_Descricao[0]);
            pr_default.readNext(1);
         }
         pr_default.close(1);
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_1282( ) ;
         while ( nGXsfl_128_idx <= nRC_GXsfl_128 )
         {
            sendrow_1282( ) ;
            nGXsfl_128_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_128_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_128_idx+1));
            sGXsfl_128_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_128_idx), 4, 0)), 4, "0");
            SubsflControlProps_1282( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       int AV50Contrato_AreaTrabalhoCod ,
                                       String AV15DynamicFiltersSelector1 ,
                                       short AV16DynamicFiltersOperator1 ,
                                       String AV40Contrato_Numero1 ,
                                       String AV51Contrato_NumeroAta1 ,
                                       String AV133Contrato_PrepostoNom1 ,
                                       String AV18Contratada_PessoaNom1 ,
                                       short AV43Contrato_Ano1 ,
                                       short AV44Contrato_Ano_To1 ,
                                       String AV20DynamicFiltersSelector2 ,
                                       short AV21DynamicFiltersOperator2 ,
                                       String AV41Contrato_Numero2 ,
                                       String AV52Contrato_NumeroAta2 ,
                                       String AV134Contrato_PrepostoNom2 ,
                                       String AV23Contratada_PessoaNom2 ,
                                       short AV45Contrato_Ano2 ,
                                       short AV46Contrato_Ano_To2 ,
                                       String AV25DynamicFiltersSelector3 ,
                                       short AV26DynamicFiltersOperator3 ,
                                       String AV42Contrato_Numero3 ,
                                       String AV53Contrato_NumeroAta3 ,
                                       String AV135Contrato_PrepostoNom3 ,
                                       String AV28Contratada_PessoaNom3 ,
                                       short AV47Contrato_Ano3 ,
                                       short AV48Contrato_Ano_To3 ,
                                       bool AV19DynamicFiltersEnabled2 ,
                                       bool AV24DynamicFiltersEnabled3 ,
                                       short AV121TFContrato_Ativo_Sel ,
                                       String AV132ddo_Contrato_ValorUndCntAtualTitleControlIdToReplace ,
                                       String AV122ddo_Contrato_AtivoTitleControlIdToReplace ,
                                       decimal AV130TFContrato_ValorUndCntAtual ,
                                       decimal AV131TFContrato_ValorUndCntAtual_To ,
                                       String AV189Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV10GridState ,
                                       bool AV30DynamicFiltersIgnoreFirst ,
                                       bool AV29DynamicFiltersRemoving ,
                                       int A39Contratada_Codigo ,
                                       wwpbaseobjects.SdtWWPContext AV6WWPContext ,
                                       int A74Contrato_Codigo ,
                                       bool A92Contrato_Ativo )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RF652( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A74Contrato_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTRATO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A74Contrato_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATO_NUMERO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A77Contrato_Numero, ""))));
         GxWebStd.gx_hidden_field( context, "CONTRATO_NUMERO", StringUtil.RTrim( A77Contrato_Numero));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATO_NUMEROATA", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A78Contrato_NumeroAta, ""))));
         GxWebStd.gx_hidden_field( context, "CONTRATO_NUMEROATA", StringUtil.RTrim( A78Contrato_NumeroAta));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATO_ANO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A79Contrato_Ano), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTRATO_ANO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A79Contrato_Ano), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATO_DATATERMINO", GetSecureSignedToken( "", A1869Contrato_DataTermino));
         GxWebStd.gx_hidden_field( context, "CONTRATO_DATATERMINO", context.localUtil.Format(A1869Contrato_DataTermino, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATO_ATIVO", GetSecureSignedToken( "", A92Contrato_Ativo));
         GxWebStd.gx_hidden_field( context, "CONTRATO_ATIVO", StringUtil.BoolToStr( A92Contrato_Ativo));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         if ( dynavContrato_areatrabalhocod.ItemCount > 0 )
         {
            AV50Contrato_AreaTrabalhoCod = (int)(NumberUtil.Val( dynavContrato_areatrabalhocod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV50Contrato_AreaTrabalhoCod), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50Contrato_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV50Contrato_AreaTrabalhoCod), 6, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
         {
            AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV20DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV20DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
         {
            AV21DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV25DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV25DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
         }
         if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
         {
            AV26DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF652( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV189Pgmname = "WWContrato";
         context.Gx_err = 0;
         edtavSaldocontrato_saldo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSaldocontrato_saldo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSaldocontrato_saldo_Enabled), 5, 0)));
      }

      protected void RF652( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 128;
         /* Execute user event: E25652 */
         E25652 ();
         nGXsfl_128_idx = 1;
         sGXsfl_128_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_128_idx), 4, 0)), 4, "0");
         SubsflControlProps_1282( ) ;
         nGXsfl_128_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_1282( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(2, new Object[]{ new Object[]{
                                                 AV153WWContratoDS_1_Contrato_areatrabalhocod ,
                                                 AV6WWPContext.gxTpr_Contratada_codigo ,
                                                 AV154WWContratoDS_2_Dynamicfiltersselector1 ,
                                                 AV156WWContratoDS_4_Contrato_numero1 ,
                                                 AV157WWContratoDS_5_Contrato_numeroata1 ,
                                                 AV155WWContratoDS_3_Dynamicfiltersoperator1 ,
                                                 AV158WWContratoDS_6_Contrato_prepostonom1 ,
                                                 AV159WWContratoDS_7_Contratada_pessoanom1 ,
                                                 AV160WWContratoDS_8_Contrato_ano1 ,
                                                 AV161WWContratoDS_9_Contrato_ano_to1 ,
                                                 AV162WWContratoDS_10_Dynamicfiltersenabled2 ,
                                                 AV163WWContratoDS_11_Dynamicfiltersselector2 ,
                                                 AV165WWContratoDS_13_Contrato_numero2 ,
                                                 AV166WWContratoDS_14_Contrato_numeroata2 ,
                                                 AV164WWContratoDS_12_Dynamicfiltersoperator2 ,
                                                 AV167WWContratoDS_15_Contrato_prepostonom2 ,
                                                 AV168WWContratoDS_16_Contratada_pessoanom2 ,
                                                 AV169WWContratoDS_17_Contrato_ano2 ,
                                                 AV170WWContratoDS_18_Contrato_ano_to2 ,
                                                 AV171WWContratoDS_19_Dynamicfiltersenabled3 ,
                                                 AV172WWContratoDS_20_Dynamicfiltersselector3 ,
                                                 AV174WWContratoDS_22_Contrato_numero3 ,
                                                 AV175WWContratoDS_23_Contrato_numeroata3 ,
                                                 AV173WWContratoDS_21_Dynamicfiltersoperator3 ,
                                                 AV176WWContratoDS_24_Contrato_prepostonom3 ,
                                                 AV177WWContratoDS_25_Contratada_pessoanom3 ,
                                                 AV178WWContratoDS_26_Contrato_ano3 ,
                                                 AV179WWContratoDS_27_Contrato_ano_to3 ,
                                                 AV182WWContratoDS_30_Tfcontrato_ativo_sel ,
                                                 A75Contrato_AreaTrabalhoCod ,
                                                 A39Contratada_Codigo ,
                                                 A77Contrato_Numero ,
                                                 A78Contrato_NumeroAta ,
                                                 A1015Contrato_PrepostoNom ,
                                                 A41Contratada_PessoaNom ,
                                                 A79Contrato_Ano ,
                                                 A92Contrato_Ativo ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc ,
                                                 AV180WWContratoDS_28_Tfcontrato_valorundcntatual ,
                                                 A1870Contrato_ValorUndCntAtual ,
                                                 AV181WWContratoDS_29_Tfcontrato_valorundcntatual_to },
                                                 new int[] {
                                                 TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT,
                                                 TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.BOOLEAN,
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.INT,
                                                 TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN,
                                                 TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.DECIMAL
                                                 }
            });
            lV156WWContratoDS_4_Contrato_numero1 = StringUtil.PadR( StringUtil.RTrim( AV156WWContratoDS_4_Contrato_numero1), 20, "%");
            lV157WWContratoDS_5_Contrato_numeroata1 = StringUtil.PadR( StringUtil.RTrim( AV157WWContratoDS_5_Contrato_numeroata1), 10, "%");
            lV158WWContratoDS_6_Contrato_prepostonom1 = StringUtil.PadR( StringUtil.RTrim( AV158WWContratoDS_6_Contrato_prepostonom1), 100, "%");
            lV158WWContratoDS_6_Contrato_prepostonom1 = StringUtil.PadR( StringUtil.RTrim( AV158WWContratoDS_6_Contrato_prepostonom1), 100, "%");
            lV159WWContratoDS_7_Contratada_pessoanom1 = StringUtil.PadR( StringUtil.RTrim( AV159WWContratoDS_7_Contratada_pessoanom1), 100, "%");
            lV165WWContratoDS_13_Contrato_numero2 = StringUtil.PadR( StringUtil.RTrim( AV165WWContratoDS_13_Contrato_numero2), 20, "%");
            lV166WWContratoDS_14_Contrato_numeroata2 = StringUtil.PadR( StringUtil.RTrim( AV166WWContratoDS_14_Contrato_numeroata2), 10, "%");
            lV167WWContratoDS_15_Contrato_prepostonom2 = StringUtil.PadR( StringUtil.RTrim( AV167WWContratoDS_15_Contrato_prepostonom2), 100, "%");
            lV167WWContratoDS_15_Contrato_prepostonom2 = StringUtil.PadR( StringUtil.RTrim( AV167WWContratoDS_15_Contrato_prepostonom2), 100, "%");
            lV168WWContratoDS_16_Contratada_pessoanom2 = StringUtil.PadR( StringUtil.RTrim( AV168WWContratoDS_16_Contratada_pessoanom2), 100, "%");
            lV174WWContratoDS_22_Contrato_numero3 = StringUtil.PadR( StringUtil.RTrim( AV174WWContratoDS_22_Contrato_numero3), 20, "%");
            lV175WWContratoDS_23_Contrato_numeroata3 = StringUtil.PadR( StringUtil.RTrim( AV175WWContratoDS_23_Contrato_numeroata3), 10, "%");
            lV176WWContratoDS_24_Contrato_prepostonom3 = StringUtil.PadR( StringUtil.RTrim( AV176WWContratoDS_24_Contrato_prepostonom3), 100, "%");
            lV176WWContratoDS_24_Contrato_prepostonom3 = StringUtil.PadR( StringUtil.RTrim( AV176WWContratoDS_24_Contrato_prepostonom3), 100, "%");
            lV177WWContratoDS_25_Contratada_pessoanom3 = StringUtil.PadR( StringUtil.RTrim( AV177WWContratoDS_25_Contratada_pessoanom3), 100, "%");
            /* Using cursor H006510 */
            pr_default.execute(2, new Object[] {AV180WWContratoDS_28_Tfcontrato_valorundcntatual, AV180WWContratoDS_28_Tfcontrato_valorundcntatual, AV181WWContratoDS_29_Tfcontrato_valorundcntatual_to, AV181WWContratoDS_29_Tfcontrato_valorundcntatual_to, AV153WWContratoDS_1_Contrato_areatrabalhocod, AV6WWPContext.gxTpr_Contratada_codigo, lV156WWContratoDS_4_Contrato_numero1, lV157WWContratoDS_5_Contrato_numeroata1, lV158WWContratoDS_6_Contrato_prepostonom1, lV158WWContratoDS_6_Contrato_prepostonom1, lV159WWContratoDS_7_Contratada_pessoanom1, AV160WWContratoDS_8_Contrato_ano1, AV161WWContratoDS_9_Contrato_ano_to1, lV165WWContratoDS_13_Contrato_numero2, lV166WWContratoDS_14_Contrato_numeroata2, lV167WWContratoDS_15_Contrato_prepostonom2, lV167WWContratoDS_15_Contrato_prepostonom2, lV168WWContratoDS_16_Contratada_pessoanom2, AV169WWContratoDS_17_Contrato_ano2, AV170WWContratoDS_18_Contrato_ano_to2, lV174WWContratoDS_22_Contrato_numero3, lV175WWContratoDS_23_Contrato_numeroata3, lV176WWContratoDS_24_Contrato_prepostonom3, lV176WWContratoDS_24_Contrato_prepostonom3, lV177WWContratoDS_25_Contratada_pessoanom3, AV178WWContratoDS_26_Contrato_ano3, AV179WWContratoDS_27_Contrato_ano_to3, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_128_idx = 1;
            while ( ( (pr_default.getStatus(2) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A40Contratada_PessoaCod = H006510_A40Contratada_PessoaCod[0];
               A1013Contrato_PrepostoCod = H006510_A1013Contrato_PrepostoCod[0];
               n1013Contrato_PrepostoCod = H006510_n1013Contrato_PrepostoCod[0];
               A1016Contrato_PrepostoPesCod = H006510_A1016Contrato_PrepostoPesCod[0];
               n1016Contrato_PrepostoPesCod = H006510_n1016Contrato_PrepostoPesCod[0];
               A1015Contrato_PrepostoNom = H006510_A1015Contrato_PrepostoNom[0];
               n1015Contrato_PrepostoNom = H006510_n1015Contrato_PrepostoNom[0];
               A75Contrato_AreaTrabalhoCod = H006510_A75Contrato_AreaTrabalhoCod[0];
               A39Contratada_Codigo = H006510_A39Contratada_Codigo[0];
               A92Contrato_Ativo = H006510_A92Contrato_Ativo[0];
               A42Contratada_PessoaCNPJ = H006510_A42Contratada_PessoaCNPJ[0];
               n42Contratada_PessoaCNPJ = H006510_n42Contratada_PessoaCNPJ[0];
               A79Contrato_Ano = H006510_A79Contrato_Ano[0];
               A78Contrato_NumeroAta = H006510_A78Contrato_NumeroAta[0];
               n78Contrato_NumeroAta = H006510_n78Contrato_NumeroAta[0];
               A77Contrato_Numero = H006510_A77Contrato_Numero[0];
               A41Contratada_PessoaNom = H006510_A41Contratada_PessoaNom[0];
               n41Contratada_PessoaNom = H006510_n41Contratada_PessoaNom[0];
               A74Contrato_Codigo = H006510_A74Contrato_Codigo[0];
               n74Contrato_Codigo = H006510_n74Contrato_Codigo[0];
               A1870Contrato_ValorUndCntAtual = H006510_A1870Contrato_ValorUndCntAtual[0];
               n1870Contrato_ValorUndCntAtual = H006510_n1870Contrato_ValorUndCntAtual[0];
               A40000GXC1 = H006510_A40000GXC1[0];
               n40000GXC1 = H006510_n40000GXC1[0];
               A843Contrato_DataFimTA = H006510_A843Contrato_DataFimTA[0];
               n843Contrato_DataFimTA = H006510_n843Contrato_DataFimTA[0];
               A83Contrato_DataVigenciaTermino = H006510_A83Contrato_DataVigenciaTermino[0];
               A1016Contrato_PrepostoPesCod = H006510_A1016Contrato_PrepostoPesCod[0];
               n1016Contrato_PrepostoPesCod = H006510_n1016Contrato_PrepostoPesCod[0];
               A1015Contrato_PrepostoNom = H006510_A1015Contrato_PrepostoNom[0];
               n1015Contrato_PrepostoNom = H006510_n1015Contrato_PrepostoNom[0];
               A40Contratada_PessoaCod = H006510_A40Contratada_PessoaCod[0];
               A42Contratada_PessoaCNPJ = H006510_A42Contratada_PessoaCNPJ[0];
               n42Contratada_PessoaCNPJ = H006510_n42Contratada_PessoaCNPJ[0];
               A41Contratada_PessoaNom = H006510_A41Contratada_PessoaNom[0];
               n41Contratada_PessoaNom = H006510_n41Contratada_PessoaNom[0];
               A1870Contrato_ValorUndCntAtual = H006510_A1870Contrato_ValorUndCntAtual[0];
               n1870Contrato_ValorUndCntAtual = H006510_n1870Contrato_ValorUndCntAtual[0];
               A843Contrato_DataFimTA = H006510_A843Contrato_DataFimTA[0];
               n843Contrato_DataFimTA = H006510_n843Contrato_DataFimTA[0];
               A40000GXC1 = H006510_A40000GXC1[0];
               n40000GXC1 = H006510_n40000GXC1[0];
               A1869Contrato_DataTermino = ((A83Contrato_DataVigenciaTermino>A843Contrato_DataFimTA) ? A83Contrato_DataVigenciaTermino : A843Contrato_DataFimTA);
               /* Execute user event: E26652 */
               E26652 ();
               pr_default.readNext(2);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(2) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(2);
            wbEnd = 128;
            WB650( ) ;
         }
         nGXsfl_128_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         AV153WWContratoDS_1_Contrato_areatrabalhocod = AV50Contrato_AreaTrabalhoCod;
         AV154WWContratoDS_2_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV155WWContratoDS_3_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV156WWContratoDS_4_Contrato_numero1 = AV40Contrato_Numero1;
         AV157WWContratoDS_5_Contrato_numeroata1 = AV51Contrato_NumeroAta1;
         AV158WWContratoDS_6_Contrato_prepostonom1 = AV133Contrato_PrepostoNom1;
         AV159WWContratoDS_7_Contratada_pessoanom1 = AV18Contratada_PessoaNom1;
         AV160WWContratoDS_8_Contrato_ano1 = AV43Contrato_Ano1;
         AV161WWContratoDS_9_Contrato_ano_to1 = AV44Contrato_Ano_To1;
         AV162WWContratoDS_10_Dynamicfiltersenabled2 = AV19DynamicFiltersEnabled2;
         AV163WWContratoDS_11_Dynamicfiltersselector2 = AV20DynamicFiltersSelector2;
         AV164WWContratoDS_12_Dynamicfiltersoperator2 = AV21DynamicFiltersOperator2;
         AV165WWContratoDS_13_Contrato_numero2 = AV41Contrato_Numero2;
         AV166WWContratoDS_14_Contrato_numeroata2 = AV52Contrato_NumeroAta2;
         AV167WWContratoDS_15_Contrato_prepostonom2 = AV134Contrato_PrepostoNom2;
         AV168WWContratoDS_16_Contratada_pessoanom2 = AV23Contratada_PessoaNom2;
         AV169WWContratoDS_17_Contrato_ano2 = AV45Contrato_Ano2;
         AV170WWContratoDS_18_Contrato_ano_to2 = AV46Contrato_Ano_To2;
         AV171WWContratoDS_19_Dynamicfiltersenabled3 = AV24DynamicFiltersEnabled3;
         AV172WWContratoDS_20_Dynamicfiltersselector3 = AV25DynamicFiltersSelector3;
         AV173WWContratoDS_21_Dynamicfiltersoperator3 = AV26DynamicFiltersOperator3;
         AV174WWContratoDS_22_Contrato_numero3 = AV42Contrato_Numero3;
         AV175WWContratoDS_23_Contrato_numeroata3 = AV53Contrato_NumeroAta3;
         AV176WWContratoDS_24_Contrato_prepostonom3 = AV135Contrato_PrepostoNom3;
         AV177WWContratoDS_25_Contratada_pessoanom3 = AV28Contratada_PessoaNom3;
         AV178WWContratoDS_26_Contrato_ano3 = AV47Contrato_Ano3;
         AV179WWContratoDS_27_Contrato_ano_to3 = AV48Contrato_Ano_To3;
         AV180WWContratoDS_28_Tfcontrato_valorundcntatual = AV130TFContrato_ValorUndCntAtual;
         AV181WWContratoDS_29_Tfcontrato_valorundcntatual_to = AV131TFContrato_ValorUndCntAtual_To;
         AV182WWContratoDS_30_Tfcontrato_ativo_sel = AV121TFContrato_Ativo_Sel;
         pr_default.dynParam(3, new Object[]{ new Object[]{
                                              AV153WWContratoDS_1_Contrato_areatrabalhocod ,
                                              AV6WWPContext.gxTpr_Contratada_codigo ,
                                              AV154WWContratoDS_2_Dynamicfiltersselector1 ,
                                              AV156WWContratoDS_4_Contrato_numero1 ,
                                              AV157WWContratoDS_5_Contrato_numeroata1 ,
                                              AV155WWContratoDS_3_Dynamicfiltersoperator1 ,
                                              AV158WWContratoDS_6_Contrato_prepostonom1 ,
                                              AV159WWContratoDS_7_Contratada_pessoanom1 ,
                                              AV160WWContratoDS_8_Contrato_ano1 ,
                                              AV161WWContratoDS_9_Contrato_ano_to1 ,
                                              AV162WWContratoDS_10_Dynamicfiltersenabled2 ,
                                              AV163WWContratoDS_11_Dynamicfiltersselector2 ,
                                              AV165WWContratoDS_13_Contrato_numero2 ,
                                              AV166WWContratoDS_14_Contrato_numeroata2 ,
                                              AV164WWContratoDS_12_Dynamicfiltersoperator2 ,
                                              AV167WWContratoDS_15_Contrato_prepostonom2 ,
                                              AV168WWContratoDS_16_Contratada_pessoanom2 ,
                                              AV169WWContratoDS_17_Contrato_ano2 ,
                                              AV170WWContratoDS_18_Contrato_ano_to2 ,
                                              AV171WWContratoDS_19_Dynamicfiltersenabled3 ,
                                              AV172WWContratoDS_20_Dynamicfiltersselector3 ,
                                              AV174WWContratoDS_22_Contrato_numero3 ,
                                              AV175WWContratoDS_23_Contrato_numeroata3 ,
                                              AV173WWContratoDS_21_Dynamicfiltersoperator3 ,
                                              AV176WWContratoDS_24_Contrato_prepostonom3 ,
                                              AV177WWContratoDS_25_Contratada_pessoanom3 ,
                                              AV178WWContratoDS_26_Contrato_ano3 ,
                                              AV179WWContratoDS_27_Contrato_ano_to3 ,
                                              AV182WWContratoDS_30_Tfcontrato_ativo_sel ,
                                              A75Contrato_AreaTrabalhoCod ,
                                              A39Contratada_Codigo ,
                                              A77Contrato_Numero ,
                                              A78Contrato_NumeroAta ,
                                              A1015Contrato_PrepostoNom ,
                                              A41Contratada_PessoaNom ,
                                              A79Contrato_Ano ,
                                              A92Contrato_Ativo ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc ,
                                              AV180WWContratoDS_28_Tfcontrato_valorundcntatual ,
                                              A1870Contrato_ValorUndCntAtual ,
                                              AV181WWContratoDS_29_Tfcontrato_valorundcntatual_to },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN,
                                              TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.DECIMAL
                                              }
         });
         lV156WWContratoDS_4_Contrato_numero1 = StringUtil.PadR( StringUtil.RTrim( AV156WWContratoDS_4_Contrato_numero1), 20, "%");
         lV157WWContratoDS_5_Contrato_numeroata1 = StringUtil.PadR( StringUtil.RTrim( AV157WWContratoDS_5_Contrato_numeroata1), 10, "%");
         lV158WWContratoDS_6_Contrato_prepostonom1 = StringUtil.PadR( StringUtil.RTrim( AV158WWContratoDS_6_Contrato_prepostonom1), 100, "%");
         lV158WWContratoDS_6_Contrato_prepostonom1 = StringUtil.PadR( StringUtil.RTrim( AV158WWContratoDS_6_Contrato_prepostonom1), 100, "%");
         lV159WWContratoDS_7_Contratada_pessoanom1 = StringUtil.PadR( StringUtil.RTrim( AV159WWContratoDS_7_Contratada_pessoanom1), 100, "%");
         lV165WWContratoDS_13_Contrato_numero2 = StringUtil.PadR( StringUtil.RTrim( AV165WWContratoDS_13_Contrato_numero2), 20, "%");
         lV166WWContratoDS_14_Contrato_numeroata2 = StringUtil.PadR( StringUtil.RTrim( AV166WWContratoDS_14_Contrato_numeroata2), 10, "%");
         lV167WWContratoDS_15_Contrato_prepostonom2 = StringUtil.PadR( StringUtil.RTrim( AV167WWContratoDS_15_Contrato_prepostonom2), 100, "%");
         lV167WWContratoDS_15_Contrato_prepostonom2 = StringUtil.PadR( StringUtil.RTrim( AV167WWContratoDS_15_Contrato_prepostonom2), 100, "%");
         lV168WWContratoDS_16_Contratada_pessoanom2 = StringUtil.PadR( StringUtil.RTrim( AV168WWContratoDS_16_Contratada_pessoanom2), 100, "%");
         lV174WWContratoDS_22_Contrato_numero3 = StringUtil.PadR( StringUtil.RTrim( AV174WWContratoDS_22_Contrato_numero3), 20, "%");
         lV175WWContratoDS_23_Contrato_numeroata3 = StringUtil.PadR( StringUtil.RTrim( AV175WWContratoDS_23_Contrato_numeroata3), 10, "%");
         lV176WWContratoDS_24_Contrato_prepostonom3 = StringUtil.PadR( StringUtil.RTrim( AV176WWContratoDS_24_Contrato_prepostonom3), 100, "%");
         lV176WWContratoDS_24_Contrato_prepostonom3 = StringUtil.PadR( StringUtil.RTrim( AV176WWContratoDS_24_Contrato_prepostonom3), 100, "%");
         lV177WWContratoDS_25_Contratada_pessoanom3 = StringUtil.PadR( StringUtil.RTrim( AV177WWContratoDS_25_Contratada_pessoanom3), 100, "%");
         /* Using cursor H006517 */
         pr_default.execute(3, new Object[] {AV180WWContratoDS_28_Tfcontrato_valorundcntatual, AV180WWContratoDS_28_Tfcontrato_valorundcntatual, AV181WWContratoDS_29_Tfcontrato_valorundcntatual_to, AV181WWContratoDS_29_Tfcontrato_valorundcntatual_to, AV153WWContratoDS_1_Contrato_areatrabalhocod, AV6WWPContext.gxTpr_Contratada_codigo, lV156WWContratoDS_4_Contrato_numero1, lV157WWContratoDS_5_Contrato_numeroata1, lV158WWContratoDS_6_Contrato_prepostonom1, lV158WWContratoDS_6_Contrato_prepostonom1, lV159WWContratoDS_7_Contratada_pessoanom1, AV160WWContratoDS_8_Contrato_ano1, AV161WWContratoDS_9_Contrato_ano_to1, lV165WWContratoDS_13_Contrato_numero2, lV166WWContratoDS_14_Contrato_numeroata2, lV167WWContratoDS_15_Contrato_prepostonom2, lV167WWContratoDS_15_Contrato_prepostonom2, lV168WWContratoDS_16_Contratada_pessoanom2, AV169WWContratoDS_17_Contrato_ano2, AV170WWContratoDS_18_Contrato_ano_to2, lV174WWContratoDS_22_Contrato_numero3, lV175WWContratoDS_23_Contrato_numeroata3, lV176WWContratoDS_24_Contrato_prepostonom3, lV176WWContratoDS_24_Contrato_prepostonom3, lV177WWContratoDS_25_Contratada_pessoanom3, AV178WWContratoDS_26_Contrato_ano3, AV179WWContratoDS_27_Contrato_ano_to3});
         GRID_nRecordCount = H006517_AGRID_nRecordCount[0];
         pr_default.close(3);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         AV153WWContratoDS_1_Contrato_areatrabalhocod = AV50Contrato_AreaTrabalhoCod;
         AV154WWContratoDS_2_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV155WWContratoDS_3_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV156WWContratoDS_4_Contrato_numero1 = AV40Contrato_Numero1;
         AV157WWContratoDS_5_Contrato_numeroata1 = AV51Contrato_NumeroAta1;
         AV158WWContratoDS_6_Contrato_prepostonom1 = AV133Contrato_PrepostoNom1;
         AV159WWContratoDS_7_Contratada_pessoanom1 = AV18Contratada_PessoaNom1;
         AV160WWContratoDS_8_Contrato_ano1 = AV43Contrato_Ano1;
         AV161WWContratoDS_9_Contrato_ano_to1 = AV44Contrato_Ano_To1;
         AV162WWContratoDS_10_Dynamicfiltersenabled2 = AV19DynamicFiltersEnabled2;
         AV163WWContratoDS_11_Dynamicfiltersselector2 = AV20DynamicFiltersSelector2;
         AV164WWContratoDS_12_Dynamicfiltersoperator2 = AV21DynamicFiltersOperator2;
         AV165WWContratoDS_13_Contrato_numero2 = AV41Contrato_Numero2;
         AV166WWContratoDS_14_Contrato_numeroata2 = AV52Contrato_NumeroAta2;
         AV167WWContratoDS_15_Contrato_prepostonom2 = AV134Contrato_PrepostoNom2;
         AV168WWContratoDS_16_Contratada_pessoanom2 = AV23Contratada_PessoaNom2;
         AV169WWContratoDS_17_Contrato_ano2 = AV45Contrato_Ano2;
         AV170WWContratoDS_18_Contrato_ano_to2 = AV46Contrato_Ano_To2;
         AV171WWContratoDS_19_Dynamicfiltersenabled3 = AV24DynamicFiltersEnabled3;
         AV172WWContratoDS_20_Dynamicfiltersselector3 = AV25DynamicFiltersSelector3;
         AV173WWContratoDS_21_Dynamicfiltersoperator3 = AV26DynamicFiltersOperator3;
         AV174WWContratoDS_22_Contrato_numero3 = AV42Contrato_Numero3;
         AV175WWContratoDS_23_Contrato_numeroata3 = AV53Contrato_NumeroAta3;
         AV176WWContratoDS_24_Contrato_prepostonom3 = AV135Contrato_PrepostoNom3;
         AV177WWContratoDS_25_Contratada_pessoanom3 = AV28Contratada_PessoaNom3;
         AV178WWContratoDS_26_Contrato_ano3 = AV47Contrato_Ano3;
         AV179WWContratoDS_27_Contrato_ano_to3 = AV48Contrato_Ano_To3;
         AV180WWContratoDS_28_Tfcontrato_valorundcntatual = AV130TFContrato_ValorUndCntAtual;
         AV181WWContratoDS_29_Tfcontrato_valorundcntatual_to = AV131TFContrato_ValorUndCntAtual_To;
         AV182WWContratoDS_30_Tfcontrato_ativo_sel = AV121TFContrato_Ativo_Sel;
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV50Contrato_AreaTrabalhoCod, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV40Contrato_Numero1, AV51Contrato_NumeroAta1, AV133Contrato_PrepostoNom1, AV18Contratada_PessoaNom1, AV43Contrato_Ano1, AV44Contrato_Ano_To1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV41Contrato_Numero2, AV52Contrato_NumeroAta2, AV134Contrato_PrepostoNom2, AV23Contratada_PessoaNom2, AV45Contrato_Ano2, AV46Contrato_Ano_To2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV42Contrato_Numero3, AV53Contrato_NumeroAta3, AV135Contrato_PrepostoNom3, AV28Contratada_PessoaNom3, AV47Contrato_Ano3, AV48Contrato_Ano_To3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV121TFContrato_Ativo_Sel, AV132ddo_Contrato_ValorUndCntAtualTitleControlIdToReplace, AV122ddo_Contrato_AtivoTitleControlIdToReplace, AV130TFContrato_ValorUndCntAtual, AV131TFContrato_ValorUndCntAtual_To, AV189Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving, A39Contratada_Codigo, AV6WWPContext, A74Contrato_Codigo, A92Contrato_Ativo) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         AV153WWContratoDS_1_Contrato_areatrabalhocod = AV50Contrato_AreaTrabalhoCod;
         AV154WWContratoDS_2_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV155WWContratoDS_3_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV156WWContratoDS_4_Contrato_numero1 = AV40Contrato_Numero1;
         AV157WWContratoDS_5_Contrato_numeroata1 = AV51Contrato_NumeroAta1;
         AV158WWContratoDS_6_Contrato_prepostonom1 = AV133Contrato_PrepostoNom1;
         AV159WWContratoDS_7_Contratada_pessoanom1 = AV18Contratada_PessoaNom1;
         AV160WWContratoDS_8_Contrato_ano1 = AV43Contrato_Ano1;
         AV161WWContratoDS_9_Contrato_ano_to1 = AV44Contrato_Ano_To1;
         AV162WWContratoDS_10_Dynamicfiltersenabled2 = AV19DynamicFiltersEnabled2;
         AV163WWContratoDS_11_Dynamicfiltersselector2 = AV20DynamicFiltersSelector2;
         AV164WWContratoDS_12_Dynamicfiltersoperator2 = AV21DynamicFiltersOperator2;
         AV165WWContratoDS_13_Contrato_numero2 = AV41Contrato_Numero2;
         AV166WWContratoDS_14_Contrato_numeroata2 = AV52Contrato_NumeroAta2;
         AV167WWContratoDS_15_Contrato_prepostonom2 = AV134Contrato_PrepostoNom2;
         AV168WWContratoDS_16_Contratada_pessoanom2 = AV23Contratada_PessoaNom2;
         AV169WWContratoDS_17_Contrato_ano2 = AV45Contrato_Ano2;
         AV170WWContratoDS_18_Contrato_ano_to2 = AV46Contrato_Ano_To2;
         AV171WWContratoDS_19_Dynamicfiltersenabled3 = AV24DynamicFiltersEnabled3;
         AV172WWContratoDS_20_Dynamicfiltersselector3 = AV25DynamicFiltersSelector3;
         AV173WWContratoDS_21_Dynamicfiltersoperator3 = AV26DynamicFiltersOperator3;
         AV174WWContratoDS_22_Contrato_numero3 = AV42Contrato_Numero3;
         AV175WWContratoDS_23_Contrato_numeroata3 = AV53Contrato_NumeroAta3;
         AV176WWContratoDS_24_Contrato_prepostonom3 = AV135Contrato_PrepostoNom3;
         AV177WWContratoDS_25_Contratada_pessoanom3 = AV28Contratada_PessoaNom3;
         AV178WWContratoDS_26_Contrato_ano3 = AV47Contrato_Ano3;
         AV179WWContratoDS_27_Contrato_ano_to3 = AV48Contrato_Ano_To3;
         AV180WWContratoDS_28_Tfcontrato_valorundcntatual = AV130TFContrato_ValorUndCntAtual;
         AV181WWContratoDS_29_Tfcontrato_valorundcntatual_to = AV131TFContrato_ValorUndCntAtual_To;
         AV182WWContratoDS_30_Tfcontrato_ativo_sel = AV121TFContrato_Ativo_Sel;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV50Contrato_AreaTrabalhoCod, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV40Contrato_Numero1, AV51Contrato_NumeroAta1, AV133Contrato_PrepostoNom1, AV18Contratada_PessoaNom1, AV43Contrato_Ano1, AV44Contrato_Ano_To1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV41Contrato_Numero2, AV52Contrato_NumeroAta2, AV134Contrato_PrepostoNom2, AV23Contratada_PessoaNom2, AV45Contrato_Ano2, AV46Contrato_Ano_To2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV42Contrato_Numero3, AV53Contrato_NumeroAta3, AV135Contrato_PrepostoNom3, AV28Contratada_PessoaNom3, AV47Contrato_Ano3, AV48Contrato_Ano_To3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV121TFContrato_Ativo_Sel, AV132ddo_Contrato_ValorUndCntAtualTitleControlIdToReplace, AV122ddo_Contrato_AtivoTitleControlIdToReplace, AV130TFContrato_ValorUndCntAtual, AV131TFContrato_ValorUndCntAtual_To, AV189Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving, A39Contratada_Codigo, AV6WWPContext, A74Contrato_Codigo, A92Contrato_Ativo) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         AV153WWContratoDS_1_Contrato_areatrabalhocod = AV50Contrato_AreaTrabalhoCod;
         AV154WWContratoDS_2_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV155WWContratoDS_3_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV156WWContratoDS_4_Contrato_numero1 = AV40Contrato_Numero1;
         AV157WWContratoDS_5_Contrato_numeroata1 = AV51Contrato_NumeroAta1;
         AV158WWContratoDS_6_Contrato_prepostonom1 = AV133Contrato_PrepostoNom1;
         AV159WWContratoDS_7_Contratada_pessoanom1 = AV18Contratada_PessoaNom1;
         AV160WWContratoDS_8_Contrato_ano1 = AV43Contrato_Ano1;
         AV161WWContratoDS_9_Contrato_ano_to1 = AV44Contrato_Ano_To1;
         AV162WWContratoDS_10_Dynamicfiltersenabled2 = AV19DynamicFiltersEnabled2;
         AV163WWContratoDS_11_Dynamicfiltersselector2 = AV20DynamicFiltersSelector2;
         AV164WWContratoDS_12_Dynamicfiltersoperator2 = AV21DynamicFiltersOperator2;
         AV165WWContratoDS_13_Contrato_numero2 = AV41Contrato_Numero2;
         AV166WWContratoDS_14_Contrato_numeroata2 = AV52Contrato_NumeroAta2;
         AV167WWContratoDS_15_Contrato_prepostonom2 = AV134Contrato_PrepostoNom2;
         AV168WWContratoDS_16_Contratada_pessoanom2 = AV23Contratada_PessoaNom2;
         AV169WWContratoDS_17_Contrato_ano2 = AV45Contrato_Ano2;
         AV170WWContratoDS_18_Contrato_ano_to2 = AV46Contrato_Ano_To2;
         AV171WWContratoDS_19_Dynamicfiltersenabled3 = AV24DynamicFiltersEnabled3;
         AV172WWContratoDS_20_Dynamicfiltersselector3 = AV25DynamicFiltersSelector3;
         AV173WWContratoDS_21_Dynamicfiltersoperator3 = AV26DynamicFiltersOperator3;
         AV174WWContratoDS_22_Contrato_numero3 = AV42Contrato_Numero3;
         AV175WWContratoDS_23_Contrato_numeroata3 = AV53Contrato_NumeroAta3;
         AV176WWContratoDS_24_Contrato_prepostonom3 = AV135Contrato_PrepostoNom3;
         AV177WWContratoDS_25_Contratada_pessoanom3 = AV28Contratada_PessoaNom3;
         AV178WWContratoDS_26_Contrato_ano3 = AV47Contrato_Ano3;
         AV179WWContratoDS_27_Contrato_ano_to3 = AV48Contrato_Ano_To3;
         AV180WWContratoDS_28_Tfcontrato_valorundcntatual = AV130TFContrato_ValorUndCntAtual;
         AV181WWContratoDS_29_Tfcontrato_valorundcntatual_to = AV131TFContrato_ValorUndCntAtual_To;
         AV182WWContratoDS_30_Tfcontrato_ativo_sel = AV121TFContrato_Ativo_Sel;
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV50Contrato_AreaTrabalhoCod, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV40Contrato_Numero1, AV51Contrato_NumeroAta1, AV133Contrato_PrepostoNom1, AV18Contratada_PessoaNom1, AV43Contrato_Ano1, AV44Contrato_Ano_To1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV41Contrato_Numero2, AV52Contrato_NumeroAta2, AV134Contrato_PrepostoNom2, AV23Contratada_PessoaNom2, AV45Contrato_Ano2, AV46Contrato_Ano_To2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV42Contrato_Numero3, AV53Contrato_NumeroAta3, AV135Contrato_PrepostoNom3, AV28Contratada_PessoaNom3, AV47Contrato_Ano3, AV48Contrato_Ano_To3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV121TFContrato_Ativo_Sel, AV132ddo_Contrato_ValorUndCntAtualTitleControlIdToReplace, AV122ddo_Contrato_AtivoTitleControlIdToReplace, AV130TFContrato_ValorUndCntAtual, AV131TFContrato_ValorUndCntAtual_To, AV189Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving, A39Contratada_Codigo, AV6WWPContext, A74Contrato_Codigo, A92Contrato_Ativo) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         AV153WWContratoDS_1_Contrato_areatrabalhocod = AV50Contrato_AreaTrabalhoCod;
         AV154WWContratoDS_2_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV155WWContratoDS_3_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV156WWContratoDS_4_Contrato_numero1 = AV40Contrato_Numero1;
         AV157WWContratoDS_5_Contrato_numeroata1 = AV51Contrato_NumeroAta1;
         AV158WWContratoDS_6_Contrato_prepostonom1 = AV133Contrato_PrepostoNom1;
         AV159WWContratoDS_7_Contratada_pessoanom1 = AV18Contratada_PessoaNom1;
         AV160WWContratoDS_8_Contrato_ano1 = AV43Contrato_Ano1;
         AV161WWContratoDS_9_Contrato_ano_to1 = AV44Contrato_Ano_To1;
         AV162WWContratoDS_10_Dynamicfiltersenabled2 = AV19DynamicFiltersEnabled2;
         AV163WWContratoDS_11_Dynamicfiltersselector2 = AV20DynamicFiltersSelector2;
         AV164WWContratoDS_12_Dynamicfiltersoperator2 = AV21DynamicFiltersOperator2;
         AV165WWContratoDS_13_Contrato_numero2 = AV41Contrato_Numero2;
         AV166WWContratoDS_14_Contrato_numeroata2 = AV52Contrato_NumeroAta2;
         AV167WWContratoDS_15_Contrato_prepostonom2 = AV134Contrato_PrepostoNom2;
         AV168WWContratoDS_16_Contratada_pessoanom2 = AV23Contratada_PessoaNom2;
         AV169WWContratoDS_17_Contrato_ano2 = AV45Contrato_Ano2;
         AV170WWContratoDS_18_Contrato_ano_to2 = AV46Contrato_Ano_To2;
         AV171WWContratoDS_19_Dynamicfiltersenabled3 = AV24DynamicFiltersEnabled3;
         AV172WWContratoDS_20_Dynamicfiltersselector3 = AV25DynamicFiltersSelector3;
         AV173WWContratoDS_21_Dynamicfiltersoperator3 = AV26DynamicFiltersOperator3;
         AV174WWContratoDS_22_Contrato_numero3 = AV42Contrato_Numero3;
         AV175WWContratoDS_23_Contrato_numeroata3 = AV53Contrato_NumeroAta3;
         AV176WWContratoDS_24_Contrato_prepostonom3 = AV135Contrato_PrepostoNom3;
         AV177WWContratoDS_25_Contratada_pessoanom3 = AV28Contratada_PessoaNom3;
         AV178WWContratoDS_26_Contrato_ano3 = AV47Contrato_Ano3;
         AV179WWContratoDS_27_Contrato_ano_to3 = AV48Contrato_Ano_To3;
         AV180WWContratoDS_28_Tfcontrato_valorundcntatual = AV130TFContrato_ValorUndCntAtual;
         AV181WWContratoDS_29_Tfcontrato_valorundcntatual_to = AV131TFContrato_ValorUndCntAtual_To;
         AV182WWContratoDS_30_Tfcontrato_ativo_sel = AV121TFContrato_Ativo_Sel;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV50Contrato_AreaTrabalhoCod, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV40Contrato_Numero1, AV51Contrato_NumeroAta1, AV133Contrato_PrepostoNom1, AV18Contratada_PessoaNom1, AV43Contrato_Ano1, AV44Contrato_Ano_To1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV41Contrato_Numero2, AV52Contrato_NumeroAta2, AV134Contrato_PrepostoNom2, AV23Contratada_PessoaNom2, AV45Contrato_Ano2, AV46Contrato_Ano_To2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV42Contrato_Numero3, AV53Contrato_NumeroAta3, AV135Contrato_PrepostoNom3, AV28Contratada_PessoaNom3, AV47Contrato_Ano3, AV48Contrato_Ano_To3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV121TFContrato_Ativo_Sel, AV132ddo_Contrato_ValorUndCntAtualTitleControlIdToReplace, AV122ddo_Contrato_AtivoTitleControlIdToReplace, AV130TFContrato_ValorUndCntAtual, AV131TFContrato_ValorUndCntAtual_To, AV189Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving, A39Contratada_Codigo, AV6WWPContext, A74Contrato_Codigo, A92Contrato_Ativo) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         AV153WWContratoDS_1_Contrato_areatrabalhocod = AV50Contrato_AreaTrabalhoCod;
         AV154WWContratoDS_2_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV155WWContratoDS_3_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV156WWContratoDS_4_Contrato_numero1 = AV40Contrato_Numero1;
         AV157WWContratoDS_5_Contrato_numeroata1 = AV51Contrato_NumeroAta1;
         AV158WWContratoDS_6_Contrato_prepostonom1 = AV133Contrato_PrepostoNom1;
         AV159WWContratoDS_7_Contratada_pessoanom1 = AV18Contratada_PessoaNom1;
         AV160WWContratoDS_8_Contrato_ano1 = AV43Contrato_Ano1;
         AV161WWContratoDS_9_Contrato_ano_to1 = AV44Contrato_Ano_To1;
         AV162WWContratoDS_10_Dynamicfiltersenabled2 = AV19DynamicFiltersEnabled2;
         AV163WWContratoDS_11_Dynamicfiltersselector2 = AV20DynamicFiltersSelector2;
         AV164WWContratoDS_12_Dynamicfiltersoperator2 = AV21DynamicFiltersOperator2;
         AV165WWContratoDS_13_Contrato_numero2 = AV41Contrato_Numero2;
         AV166WWContratoDS_14_Contrato_numeroata2 = AV52Contrato_NumeroAta2;
         AV167WWContratoDS_15_Contrato_prepostonom2 = AV134Contrato_PrepostoNom2;
         AV168WWContratoDS_16_Contratada_pessoanom2 = AV23Contratada_PessoaNom2;
         AV169WWContratoDS_17_Contrato_ano2 = AV45Contrato_Ano2;
         AV170WWContratoDS_18_Contrato_ano_to2 = AV46Contrato_Ano_To2;
         AV171WWContratoDS_19_Dynamicfiltersenabled3 = AV24DynamicFiltersEnabled3;
         AV172WWContratoDS_20_Dynamicfiltersselector3 = AV25DynamicFiltersSelector3;
         AV173WWContratoDS_21_Dynamicfiltersoperator3 = AV26DynamicFiltersOperator3;
         AV174WWContratoDS_22_Contrato_numero3 = AV42Contrato_Numero3;
         AV175WWContratoDS_23_Contrato_numeroata3 = AV53Contrato_NumeroAta3;
         AV176WWContratoDS_24_Contrato_prepostonom3 = AV135Contrato_PrepostoNom3;
         AV177WWContratoDS_25_Contratada_pessoanom3 = AV28Contratada_PessoaNom3;
         AV178WWContratoDS_26_Contrato_ano3 = AV47Contrato_Ano3;
         AV179WWContratoDS_27_Contrato_ano_to3 = AV48Contrato_Ano_To3;
         AV180WWContratoDS_28_Tfcontrato_valorundcntatual = AV130TFContrato_ValorUndCntAtual;
         AV181WWContratoDS_29_Tfcontrato_valorundcntatual_to = AV131TFContrato_ValorUndCntAtual_To;
         AV182WWContratoDS_30_Tfcontrato_ativo_sel = AV121TFContrato_Ativo_Sel;
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV50Contrato_AreaTrabalhoCod, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV40Contrato_Numero1, AV51Contrato_NumeroAta1, AV133Contrato_PrepostoNom1, AV18Contratada_PessoaNom1, AV43Contrato_Ano1, AV44Contrato_Ano_To1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV41Contrato_Numero2, AV52Contrato_NumeroAta2, AV134Contrato_PrepostoNom2, AV23Contratada_PessoaNom2, AV45Contrato_Ano2, AV46Contrato_Ano_To2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV42Contrato_Numero3, AV53Contrato_NumeroAta3, AV135Contrato_PrepostoNom3, AV28Contratada_PessoaNom3, AV47Contrato_Ano3, AV48Contrato_Ano_To3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV121TFContrato_Ativo_Sel, AV132ddo_Contrato_ValorUndCntAtualTitleControlIdToReplace, AV122ddo_Contrato_AtivoTitleControlIdToReplace, AV130TFContrato_ValorUndCntAtual, AV131TFContrato_ValorUndCntAtual_To, AV189Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving, A39Contratada_Codigo, AV6WWPContext, A74Contrato_Codigo, A92Contrato_Ativo) ;
         }
         return (int)(0) ;
      }

      protected void STRUP650( )
      {
         /* Before Start, stand alone formulas. */
         AV189Pgmname = "WWContrato";
         context.Gx_err = 0;
         edtavSaldocontrato_saldo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSaldocontrato_saldo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSaldocontrato_saldo_Enabled), 5, 0)));
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E24652 */
         E24652 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV115DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATO_VALORUNDCNTATUALTITLEFILTERDATA"), AV129Contrato_ValorUndCntAtualTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATO_ATIVOTITLEFILTERDATA"), AV120Contrato_AtivoTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV13OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            dynavContrato_areatrabalhocod.Name = dynavContrato_areatrabalhocod_Internalname;
            dynavContrato_areatrabalhocod.CurrentValue = cgiGet( dynavContrato_areatrabalhocod_Internalname);
            AV50Contrato_AreaTrabalhoCod = (int)(NumberUtil.Val( cgiGet( dynavContrato_areatrabalhocod_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50Contrato_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV50Contrato_AreaTrabalhoCod), 6, 0)));
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV15DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            cmbavDynamicfiltersoperator1.Name = cmbavDynamicfiltersoperator1_Internalname;
            cmbavDynamicfiltersoperator1.CurrentValue = cgiGet( cmbavDynamicfiltersoperator1_Internalname);
            AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator1_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
            AV40Contrato_Numero1 = cgiGet( edtavContrato_numero1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40Contrato_Numero1", AV40Contrato_Numero1);
            AV51Contrato_NumeroAta1 = cgiGet( edtavContrato_numeroata1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51Contrato_NumeroAta1", AV51Contrato_NumeroAta1);
            AV133Contrato_PrepostoNom1 = StringUtil.Upper( cgiGet( edtavContrato_prepostonom1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV133Contrato_PrepostoNom1", AV133Contrato_PrepostoNom1);
            AV18Contratada_PessoaNom1 = StringUtil.Upper( cgiGet( edtavContratada_pessoanom1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Contratada_PessoaNom1", AV18Contratada_PessoaNom1);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavContrato_ano1_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavContrato_ano1_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTRATO_ANO1");
               GX_FocusControl = edtavContrato_ano1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV43Contrato_Ano1 = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43Contrato_Ano1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV43Contrato_Ano1), 4, 0)));
            }
            else
            {
               AV43Contrato_Ano1 = (short)(context.localUtil.CToN( cgiGet( edtavContrato_ano1_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43Contrato_Ano1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV43Contrato_Ano1), 4, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavContrato_ano_to1_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavContrato_ano_to1_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTRATO_ANO_TO1");
               GX_FocusControl = edtavContrato_ano_to1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV44Contrato_Ano_To1 = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44Contrato_Ano_To1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44Contrato_Ano_To1), 4, 0)));
            }
            else
            {
               AV44Contrato_Ano_To1 = (short)(context.localUtil.CToN( cgiGet( edtavContrato_ano_to1_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44Contrato_Ano_To1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44Contrato_Ano_To1), 4, 0)));
            }
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV20DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
            cmbavDynamicfiltersoperator2.Name = cmbavDynamicfiltersoperator2_Internalname;
            cmbavDynamicfiltersoperator2.CurrentValue = cgiGet( cmbavDynamicfiltersoperator2_Internalname);
            AV21DynamicFiltersOperator2 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator2_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
            AV41Contrato_Numero2 = cgiGet( edtavContrato_numero2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41Contrato_Numero2", AV41Contrato_Numero2);
            AV52Contrato_NumeroAta2 = cgiGet( edtavContrato_numeroata2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52Contrato_NumeroAta2", AV52Contrato_NumeroAta2);
            AV134Contrato_PrepostoNom2 = StringUtil.Upper( cgiGet( edtavContrato_prepostonom2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV134Contrato_PrepostoNom2", AV134Contrato_PrepostoNom2);
            AV23Contratada_PessoaNom2 = StringUtil.Upper( cgiGet( edtavContratada_pessoanom2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Contratada_PessoaNom2", AV23Contratada_PessoaNom2);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavContrato_ano2_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavContrato_ano2_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTRATO_ANO2");
               GX_FocusControl = edtavContrato_ano2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV45Contrato_Ano2 = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45Contrato_Ano2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV45Contrato_Ano2), 4, 0)));
            }
            else
            {
               AV45Contrato_Ano2 = (short)(context.localUtil.CToN( cgiGet( edtavContrato_ano2_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45Contrato_Ano2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV45Contrato_Ano2), 4, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavContrato_ano_to2_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavContrato_ano_to2_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTRATO_ANO_TO2");
               GX_FocusControl = edtavContrato_ano_to2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV46Contrato_Ano_To2 = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46Contrato_Ano_To2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV46Contrato_Ano_To2), 4, 0)));
            }
            else
            {
               AV46Contrato_Ano_To2 = (short)(context.localUtil.CToN( cgiGet( edtavContrato_ano_to2_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46Contrato_Ano_To2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV46Contrato_Ano_To2), 4, 0)));
            }
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV25DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
            cmbavDynamicfiltersoperator3.Name = cmbavDynamicfiltersoperator3_Internalname;
            cmbavDynamicfiltersoperator3.CurrentValue = cgiGet( cmbavDynamicfiltersoperator3_Internalname);
            AV26DynamicFiltersOperator3 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator3_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0)));
            AV42Contrato_Numero3 = cgiGet( edtavContrato_numero3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42Contrato_Numero3", AV42Contrato_Numero3);
            AV53Contrato_NumeroAta3 = cgiGet( edtavContrato_numeroata3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53Contrato_NumeroAta3", AV53Contrato_NumeroAta3);
            AV135Contrato_PrepostoNom3 = StringUtil.Upper( cgiGet( edtavContrato_prepostonom3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV135Contrato_PrepostoNom3", AV135Contrato_PrepostoNom3);
            AV28Contratada_PessoaNom3 = StringUtil.Upper( cgiGet( edtavContratada_pessoanom3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28Contratada_PessoaNom3", AV28Contratada_PessoaNom3);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavContrato_ano3_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavContrato_ano3_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTRATO_ANO3");
               GX_FocusControl = edtavContrato_ano3_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV47Contrato_Ano3 = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47Contrato_Ano3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV47Contrato_Ano3), 4, 0)));
            }
            else
            {
               AV47Contrato_Ano3 = (short)(context.localUtil.CToN( cgiGet( edtavContrato_ano3_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47Contrato_Ano3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV47Contrato_Ano3), 4, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavContrato_ano_to3_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavContrato_ano_to3_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTRATO_ANO_TO3");
               GX_FocusControl = edtavContrato_ano_to3_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV48Contrato_Ano_To3 = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48Contrato_Ano_To3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV48Contrato_Ano_To3), 4, 0)));
            }
            else
            {
               AV48Contrato_Ano_To3 = (short)(context.localUtil.CToN( cgiGet( edtavContrato_ano_to3_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48Contrato_Ano_To3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV48Contrato_Ano_To3), 4, 0)));
            }
            AV19DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
            AV24DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersEnabled3", AV24DynamicFiltersEnabled3);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontrato_valorundcntatual_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontrato_valorundcntatual_Internalname), ",", ".") > 999999999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATO_VALORUNDCNTATUAL");
               GX_FocusControl = edtavTfcontrato_valorundcntatual_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV130TFContrato_ValorUndCntAtual = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV130TFContrato_ValorUndCntAtual", StringUtil.LTrim( StringUtil.Str( AV130TFContrato_ValorUndCntAtual, 18, 5)));
            }
            else
            {
               AV130TFContrato_ValorUndCntAtual = context.localUtil.CToN( cgiGet( edtavTfcontrato_valorundcntatual_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV130TFContrato_ValorUndCntAtual", StringUtil.LTrim( StringUtil.Str( AV130TFContrato_ValorUndCntAtual, 18, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontrato_valorundcntatual_to_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontrato_valorundcntatual_to_Internalname), ",", ".") > 999999999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATO_VALORUNDCNTATUAL_TO");
               GX_FocusControl = edtavTfcontrato_valorundcntatual_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV131TFContrato_ValorUndCntAtual_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV131TFContrato_ValorUndCntAtual_To", StringUtil.LTrim( StringUtil.Str( AV131TFContrato_ValorUndCntAtual_To, 18, 5)));
            }
            else
            {
               AV131TFContrato_ValorUndCntAtual_To = context.localUtil.CToN( cgiGet( edtavTfcontrato_valorundcntatual_to_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV131TFContrato_ValorUndCntAtual_To", StringUtil.LTrim( StringUtil.Str( AV131TFContrato_ValorUndCntAtual_To, 18, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontrato_ativo_sel_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontrato_ativo_sel_Internalname), ",", ".") > Convert.ToDecimal( 9 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATO_ATIVO_SEL");
               GX_FocusControl = edtavTfcontrato_ativo_sel_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV121TFContrato_Ativo_Sel = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV121TFContrato_Ativo_Sel", StringUtil.Str( (decimal)(AV121TFContrato_Ativo_Sel), 1, 0));
            }
            else
            {
               AV121TFContrato_Ativo_Sel = (short)(context.localUtil.CToN( cgiGet( edtavTfcontrato_ativo_sel_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV121TFContrato_Ativo_Sel", StringUtil.Str( (decimal)(AV121TFContrato_Ativo_Sel), 1, 0));
            }
            AV132ddo_Contrato_ValorUndCntAtualTitleControlIdToReplace = cgiGet( edtavDdo_contrato_valorundcntatualtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV132ddo_Contrato_ValorUndCntAtualTitleControlIdToReplace", AV132ddo_Contrato_ValorUndCntAtualTitleControlIdToReplace);
            AV122ddo_Contrato_AtivoTitleControlIdToReplace = cgiGet( edtavDdo_contrato_ativotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV122ddo_Contrato_AtivoTitleControlIdToReplace", AV122ddo_Contrato_AtivoTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_128 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_128"), ",", "."));
            AV117GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV118GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_contrato_valorundcntatual_Caption = cgiGet( "DDO_CONTRATO_VALORUNDCNTATUAL_Caption");
            Ddo_contrato_valorundcntatual_Tooltip = cgiGet( "DDO_CONTRATO_VALORUNDCNTATUAL_Tooltip");
            Ddo_contrato_valorundcntatual_Cls = cgiGet( "DDO_CONTRATO_VALORUNDCNTATUAL_Cls");
            Ddo_contrato_valorundcntatual_Filteredtext_set = cgiGet( "DDO_CONTRATO_VALORUNDCNTATUAL_Filteredtext_set");
            Ddo_contrato_valorundcntatual_Filteredtextto_set = cgiGet( "DDO_CONTRATO_VALORUNDCNTATUAL_Filteredtextto_set");
            Ddo_contrato_valorundcntatual_Dropdownoptionstype = cgiGet( "DDO_CONTRATO_VALORUNDCNTATUAL_Dropdownoptionstype");
            Ddo_contrato_valorundcntatual_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATO_VALORUNDCNTATUAL_Titlecontrolidtoreplace");
            Ddo_contrato_valorundcntatual_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATO_VALORUNDCNTATUAL_Includesortasc"));
            Ddo_contrato_valorundcntatual_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATO_VALORUNDCNTATUAL_Includesortdsc"));
            Ddo_contrato_valorundcntatual_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATO_VALORUNDCNTATUAL_Includefilter"));
            Ddo_contrato_valorundcntatual_Filtertype = cgiGet( "DDO_CONTRATO_VALORUNDCNTATUAL_Filtertype");
            Ddo_contrato_valorundcntatual_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATO_VALORUNDCNTATUAL_Filterisrange"));
            Ddo_contrato_valorundcntatual_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATO_VALORUNDCNTATUAL_Includedatalist"));
            Ddo_contrato_valorundcntatual_Cleanfilter = cgiGet( "DDO_CONTRATO_VALORUNDCNTATUAL_Cleanfilter");
            Ddo_contrato_valorundcntatual_Rangefilterfrom = cgiGet( "DDO_CONTRATO_VALORUNDCNTATUAL_Rangefilterfrom");
            Ddo_contrato_valorundcntatual_Rangefilterto = cgiGet( "DDO_CONTRATO_VALORUNDCNTATUAL_Rangefilterto");
            Ddo_contrato_valorundcntatual_Searchbuttontext = cgiGet( "DDO_CONTRATO_VALORUNDCNTATUAL_Searchbuttontext");
            Ddo_contrato_ativo_Caption = cgiGet( "DDO_CONTRATO_ATIVO_Caption");
            Ddo_contrato_ativo_Tooltip = cgiGet( "DDO_CONTRATO_ATIVO_Tooltip");
            Ddo_contrato_ativo_Cls = cgiGet( "DDO_CONTRATO_ATIVO_Cls");
            Ddo_contrato_ativo_Selectedvalue_set = cgiGet( "DDO_CONTRATO_ATIVO_Selectedvalue_set");
            Ddo_contrato_ativo_Dropdownoptionstype = cgiGet( "DDO_CONTRATO_ATIVO_Dropdownoptionstype");
            Ddo_contrato_ativo_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATO_ATIVO_Titlecontrolidtoreplace");
            Ddo_contrato_ativo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATO_ATIVO_Includesortasc"));
            Ddo_contrato_ativo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATO_ATIVO_Includesortdsc"));
            Ddo_contrato_ativo_Sortedstatus = cgiGet( "DDO_CONTRATO_ATIVO_Sortedstatus");
            Ddo_contrato_ativo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATO_ATIVO_Includefilter"));
            Ddo_contrato_ativo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATO_ATIVO_Includedatalist"));
            Ddo_contrato_ativo_Datalisttype = cgiGet( "DDO_CONTRATO_ATIVO_Datalisttype");
            Ddo_contrato_ativo_Datalistfixedvalues = cgiGet( "DDO_CONTRATO_ATIVO_Datalistfixedvalues");
            Ddo_contrato_ativo_Sortasc = cgiGet( "DDO_CONTRATO_ATIVO_Sortasc");
            Ddo_contrato_ativo_Sortdsc = cgiGet( "DDO_CONTRATO_ATIVO_Sortdsc");
            Ddo_contrato_ativo_Cleanfilter = cgiGet( "DDO_CONTRATO_ATIVO_Cleanfilter");
            Ddo_contrato_ativo_Searchbuttontext = cgiGet( "DDO_CONTRATO_ATIVO_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_contrato_valorundcntatual_Activeeventkey = cgiGet( "DDO_CONTRATO_VALORUNDCNTATUAL_Activeeventkey");
            Ddo_contrato_valorundcntatual_Filteredtext_get = cgiGet( "DDO_CONTRATO_VALORUNDCNTATUAL_Filteredtext_get");
            Ddo_contrato_valorundcntatual_Filteredtextto_get = cgiGet( "DDO_CONTRATO_VALORUNDCNTATUAL_Filteredtextto_get");
            Ddo_contrato_ativo_Activeeventkey = cgiGet( "DDO_CONTRATO_ATIVO_Activeeventkey");
            Ddo_contrato_ativo_Selectedvalue_get = cgiGet( "DDO_CONTRATO_ATIVO_Selectedvalue_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTRATO_AREATRABALHOCOD"), ",", ".") != Convert.ToDecimal( AV50Contrato_AreaTrabalhoCod )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV16DynamicFiltersOperator1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATO_NUMERO1"), AV40Contrato_Numero1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATO_NUMEROATA1"), AV51Contrato_NumeroAta1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATO_PREPOSTONOM1"), AV133Contrato_PrepostoNom1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATADA_PESSOANOM1"), AV18Contratada_PessoaNom1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTRATO_ANO1"), ",", ".") != Convert.ToDecimal( AV43Contrato_Ano1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTRATO_ANO_TO1"), ",", ".") != Convert.ToDecimal( AV44Contrato_Ano_To1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV20DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV21DynamicFiltersOperator2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATO_NUMERO2"), AV41Contrato_Numero2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATO_NUMEROATA2"), AV52Contrato_NumeroAta2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATO_PREPOSTONOM2"), AV134Contrato_PrepostoNom2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATADA_PESSOANOM2"), AV23Contratada_PessoaNom2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTRATO_ANO2"), ",", ".") != Convert.ToDecimal( AV45Contrato_Ano2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTRATO_ANO_TO2"), ",", ".") != Convert.ToDecimal( AV46Contrato_Ano_To2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV25DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV26DynamicFiltersOperator3 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATO_NUMERO3"), AV42Contrato_Numero3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATO_NUMEROATA3"), AV53Contrato_NumeroAta3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATO_PREPOSTONOM3"), AV135Contrato_PrepostoNom3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCONTRATADA_PESSOANOM3"), AV28Contratada_PessoaNom3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTRATO_ANO3"), ",", ".") != Convert.ToDecimal( AV47Contrato_Ano3 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTRATO_ANO_TO3"), ",", ".") != Convert.ToDecimal( AV48Contrato_Ano_To3 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV19DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV24DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATO_ATIVO_SEL"), ",", ".") != Convert.ToDecimal( AV121TFContrato_Ativo_Sel )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E24652 */
         E24652 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E24652( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV15DynamicFiltersSelector1 = "CONTRATO_NUMERO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV20DynamicFiltersSelector2 = "CONTRATO_NUMERO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV25DynamicFiltersSelector3 = "CONTRATO_NUMERO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         edtavTfcontrato_valorundcntatual_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontrato_valorundcntatual_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontrato_valorundcntatual_Visible), 5, 0)));
         edtavTfcontrato_valorundcntatual_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontrato_valorundcntatual_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontrato_valorundcntatual_to_Visible), 5, 0)));
         edtavTfcontrato_ativo_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontrato_ativo_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontrato_ativo_sel_Visible), 5, 0)));
         Ddo_contrato_valorundcntatual_Titlecontrolidtoreplace = subGrid_Internalname+"_Contrato_ValorUndCntAtual";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_valorundcntatual_Internalname, "TitleControlIdToReplace", Ddo_contrato_valorundcntatual_Titlecontrolidtoreplace);
         AV132ddo_Contrato_ValorUndCntAtualTitleControlIdToReplace = Ddo_contrato_valorundcntatual_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV132ddo_Contrato_ValorUndCntAtualTitleControlIdToReplace", AV132ddo_Contrato_ValorUndCntAtualTitleControlIdToReplace);
         edtavDdo_contrato_valorundcntatualtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contrato_valorundcntatualtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contrato_valorundcntatualtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contrato_ativo_Titlecontrolidtoreplace = subGrid_Internalname+"_Contrato_Ativo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_ativo_Internalname, "TitleControlIdToReplace", Ddo_contrato_ativo_Titlecontrolidtoreplace);
         AV122ddo_Contrato_AtivoTitleControlIdToReplace = Ddo_contrato_ativo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV122ddo_Contrato_AtivoTitleControlIdToReplace", AV122ddo_Contrato_AtivoTitleControlIdToReplace);
         edtavDdo_contrato_ativotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contrato_ativotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contrato_ativotitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = " Contrato";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "N� de Contrato", 0);
         cmbavOrderedby.addItem("2", "N�mero da Ata", 0);
         cmbavOrderedby.addItem("3", "Contratada", 0);
         cmbavOrderedby.addItem("4", "Ano", 0);
         cmbavOrderedby.addItem("5", "CNPJ", 0);
         cmbavOrderedby.addItem("6", "Ativo", 0);
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV115DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV115DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E25652( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV129Contrato_ValorUndCntAtualTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV120Contrato_AtivoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtContratada_PessoaNom_Titleformat = 2;
         edtContratada_PessoaNom_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 3);' >%5</span>", ((AV13OrderedBy==3) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Contratada", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratada_PessoaNom_Internalname, "Title", edtContratada_PessoaNom_Title);
         edtContrato_Numero_Titleformat = 2;
         edtContrato_Numero_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 1);' >%5</span>", ((AV13OrderedBy==1) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "N� Contrato", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContrato_Numero_Internalname, "Title", edtContrato_Numero_Title);
         edtContrato_NumeroAta_Titleformat = 2;
         edtContrato_NumeroAta_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 2);' >%5</span>", ((AV13OrderedBy==2) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "N� da Ata", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContrato_NumeroAta_Internalname, "Title", edtContrato_NumeroAta_Title);
         edtContrato_Ano_Titleformat = 2;
         edtContrato_Ano_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 4);' >%5</span>", ((AV13OrderedBy==4) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Ano", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContrato_Ano_Internalname, "Title", edtContrato_Ano_Title);
         edtContrato_ValorUndCntAtual_Titleformat = 2;
         edtContrato_ValorUndCntAtual_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Valor Und.", AV132ddo_Contrato_ValorUndCntAtualTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContrato_ValorUndCntAtual_Internalname, "Title", edtContrato_ValorUndCntAtual_Title);
         edtContratada_PessoaCNPJ_Titleformat = 2;
         edtContratada_PessoaCNPJ_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 5);' >%5</span>", ((AV13OrderedBy==5) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "CNPJ", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratada_PessoaCNPJ_Internalname, "Title", edtContratada_PessoaCNPJ_Title);
         chkContrato_Ativo_Titleformat = 2;
         chkContrato_Ativo.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Ativo", AV122ddo_Contrato_AtivoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkContrato_Ativo_Internalname, "Title", chkContrato_Ativo.Title.Text);
         AV117GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV117GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV117GridCurrentPage), 10, 0)));
         AV118GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV118GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV118GridPageCount), 10, 0)));
         AV50Contrato_AreaTrabalhoCod = AV6WWPContext.gxTpr_Areatrabalho_codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50Contrato_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV50Contrato_AreaTrabalhoCod), 6, 0)));
         edtavAssociargestor_Title = "Gestores";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAssociargestor_Internalname, "Title", edtavAssociargestor_Title);
         edtavAssociarauxiliar_Title = "Auxiliares";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAssociarauxiliar_Internalname, "Title", edtavAssociarauxiliar_Title);
         edtavAssociarsistema_Title = "Sistemas";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAssociarsistema_Internalname, "Title", edtavAssociarsistema_Title);
         AV153WWContratoDS_1_Contrato_areatrabalhocod = AV50Contrato_AreaTrabalhoCod;
         AV154WWContratoDS_2_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV155WWContratoDS_3_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV156WWContratoDS_4_Contrato_numero1 = AV40Contrato_Numero1;
         AV157WWContratoDS_5_Contrato_numeroata1 = AV51Contrato_NumeroAta1;
         AV158WWContratoDS_6_Contrato_prepostonom1 = AV133Contrato_PrepostoNom1;
         AV159WWContratoDS_7_Contratada_pessoanom1 = AV18Contratada_PessoaNom1;
         AV160WWContratoDS_8_Contrato_ano1 = AV43Contrato_Ano1;
         AV161WWContratoDS_9_Contrato_ano_to1 = AV44Contrato_Ano_To1;
         AV162WWContratoDS_10_Dynamicfiltersenabled2 = AV19DynamicFiltersEnabled2;
         AV163WWContratoDS_11_Dynamicfiltersselector2 = AV20DynamicFiltersSelector2;
         AV164WWContratoDS_12_Dynamicfiltersoperator2 = AV21DynamicFiltersOperator2;
         AV165WWContratoDS_13_Contrato_numero2 = AV41Contrato_Numero2;
         AV166WWContratoDS_14_Contrato_numeroata2 = AV52Contrato_NumeroAta2;
         AV167WWContratoDS_15_Contrato_prepostonom2 = AV134Contrato_PrepostoNom2;
         AV168WWContratoDS_16_Contratada_pessoanom2 = AV23Contratada_PessoaNom2;
         AV169WWContratoDS_17_Contrato_ano2 = AV45Contrato_Ano2;
         AV170WWContratoDS_18_Contrato_ano_to2 = AV46Contrato_Ano_To2;
         AV171WWContratoDS_19_Dynamicfiltersenabled3 = AV24DynamicFiltersEnabled3;
         AV172WWContratoDS_20_Dynamicfiltersselector3 = AV25DynamicFiltersSelector3;
         AV173WWContratoDS_21_Dynamicfiltersoperator3 = AV26DynamicFiltersOperator3;
         AV174WWContratoDS_22_Contrato_numero3 = AV42Contrato_Numero3;
         AV175WWContratoDS_23_Contrato_numeroata3 = AV53Contrato_NumeroAta3;
         AV176WWContratoDS_24_Contrato_prepostonom3 = AV135Contrato_PrepostoNom3;
         AV177WWContratoDS_25_Contratada_pessoanom3 = AV28Contratada_PessoaNom3;
         AV178WWContratoDS_26_Contrato_ano3 = AV47Contrato_Ano3;
         AV179WWContratoDS_27_Contrato_ano_to3 = AV48Contrato_Ano_To3;
         AV180WWContratoDS_28_Tfcontrato_valorundcntatual = AV130TFContrato_ValorUndCntAtual;
         AV181WWContratoDS_29_Tfcontrato_valorundcntatual_to = AV131TFContrato_ValorUndCntAtual_To;
         AV182WWContratoDS_30_Tfcontrato_ativo_sel = AV121TFContrato_Ativo_Sel;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV129Contrato_ValorUndCntAtualTitleFilterData", AV129Contrato_ValorUndCntAtualTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV120Contrato_AtivoTitleFilterData", AV120Contrato_AtivoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV6WWPContext", AV6WWPContext);
         dynavContrato_areatrabalhocod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV50Contrato_AreaTrabalhoCod), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContrato_areatrabalhocod_Internalname, "Values", dynavContrato_areatrabalhocod.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void E11652( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV116PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV116PageToGo) ;
         }
      }

      protected void E12652( )
      {
         /* Ddo_contrato_valorundcntatual_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contrato_valorundcntatual_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV130TFContrato_ValorUndCntAtual = NumberUtil.Val( Ddo_contrato_valorundcntatual_Filteredtext_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV130TFContrato_ValorUndCntAtual", StringUtil.LTrim( StringUtil.Str( AV130TFContrato_ValorUndCntAtual, 18, 5)));
            AV131TFContrato_ValorUndCntAtual_To = NumberUtil.Val( Ddo_contrato_valorundcntatual_Filteredtextto_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV131TFContrato_ValorUndCntAtual_To", StringUtil.LTrim( StringUtil.Str( AV131TFContrato_ValorUndCntAtual_To, 18, 5)));
            subgrid_firstpage( ) ;
         }
      }

      protected void E13652( )
      {
         /* Ddo_contrato_ativo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contrato_ativo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 6;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contrato_ativo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_ativo_Internalname, "SortedStatus", Ddo_contrato_ativo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contrato_ativo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 6;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contrato_ativo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_ativo_Internalname, "SortedStatus", Ddo_contrato_ativo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contrato_ativo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV121TFContrato_Ativo_Sel = (short)(NumberUtil.Val( Ddo_contrato_ativo_Selectedvalue_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV121TFContrato_Ativo_Sel", StringUtil.Str( (decimal)(AV121TFContrato_Ativo_Sel), 1, 0));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      private void E26652( )
      {
         /* Grid_Load Routine */
         AV76Contratada_Codigo = A39Contratada_Codigo;
         edtavUpdate_Tooltiptext = "Modifica";
         if ( ( AV6WWPContext.gxTpr_Insert && AV6WWPContext.gxTpr_Update && AV6WWPContext.gxTpr_Delete ) || AV6WWPContext.gxTpr_Userehadministradorgam )
         {
            edtavUpdate_Link = formatLink("contrato.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A74Contrato_Codigo) + "," + UrlEncode("" +AV76Contratada_Codigo);
            AV31Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUpdate_Internalname, AV31Update);
            AV183Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
            edtavUpdate_Enabled = 1;
         }
         else
         {
            edtavUpdate_Link = "";
            AV31Update = context.GetImagePath( "0211baea-a1e2-4bb5-849c-f3afb69b09ee", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUpdate_Internalname, AV31Update);
            AV183Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "0211baea-a1e2-4bb5-849c-f3afb69b09ee", "", context.GetTheme( )));
            edtavUpdate_Enabled = 0;
         }
         edtavDelete_Tooltiptext = "Eliminar";
         if ( ( AV6WWPContext.gxTpr_Insert && AV6WWPContext.gxTpr_Update && AV6WWPContext.gxTpr_Delete ) || AV6WWPContext.gxTpr_Userehadministradorgam )
         {
            edtavDelete_Link = formatLink("contrato.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A74Contrato_Codigo) + "," + UrlEncode("" +AV76Contratada_Codigo);
            AV32Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDelete_Internalname, AV32Delete);
            AV184Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
            edtavDelete_Enabled = 1;
         }
         else
         {
            edtavDelete_Link = "";
            AV32Delete = context.GetImagePath( "138fdfa8-ae9b-4664-8007-e04d47468418", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDelete_Internalname, AV32Delete);
            AV184Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "138fdfa8-ae9b-4664-8007-e04d47468418", "", context.GetTheme( )));
            edtavDelete_Enabled = 0;
         }
         AV49Display = context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDisplay_Internalname, AV49Display);
         AV185Display_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( )));
         edtavDisplay_Tooltiptext = "Cl�sulas, Servi�os, Garant�as, Anexos, Dados Certame, Termo Aditivo, Ocorr�ncias, Obriga��es";
         edtavDisplay_Link = formatLink("wp_contrato.aspx") + "?" + UrlEncode("" +A74Contrato_Codigo);
         AV75AssociarGestor = context.GetImagePath( "6591e2a3-49b6-43b7-b8e3-a292564a32a4", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavAssociargestor_Internalname, AV75AssociarGestor);
         AV186Associargestor_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "6591e2a3-49b6-43b7-b8e3-a292564a32a4", "", context.GetTheme( )));
         edtavAssociargestor_Tooltiptext = "Associar os gestores deste contrato";
         AV119AssociarAuxiliar = context.GetImagePath( "6591e2a3-49b6-43b7-b8e3-a292564a32a4", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavAssociarauxiliar_Internalname, AV119AssociarAuxiliar);
         AV187Associarauxiliar_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "6591e2a3-49b6-43b7-b8e3-a292564a32a4", "", context.GetTheme( )));
         edtavAssociarauxiliar_Tooltiptext = "Associar os auxiliares deste contrato";
         AV80AssociarSistema = context.GetImagePath( "6591e2a3-49b6-43b7-b8e3-a292564a32a4", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavAssociarsistema_Internalname, AV80AssociarSistema);
         AV188Associarsistema_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "6591e2a3-49b6-43b7-b8e3-a292564a32a4", "", context.GetTheme( )));
         edtavAssociarsistema_Tooltiptext = "Associar os sistemas deste contrato";
         edtContratada_PessoaNom_Link = formatLink("viewcontratada.aspx") + "?" + UrlEncode("" +A39Contratada_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         edtContrato_NumeroAta_Link = formatLink("viewcontrato.aspx") + "?" + UrlEncode("" +A74Contrato_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         if ( A92Contrato_Ativo )
         {
            AV150Color = GXUtil.RGB( 0, 0, 0);
         }
         else
         {
            AV150Color = GXUtil.RGB( 255, 0, 0);
         }
         edtContratada_PessoaNom_Forecolor = (int)(AV150Color);
         edtContrato_Numero_Forecolor = (int)(AV150Color);
         edtContrato_NumeroAta_Forecolor = (int)(AV150Color);
         edtContrato_Ano_Forecolor = (int)(AV150Color);
         edtContrato_DataTermino_Forecolor = (int)(AV150Color);
         edtContrato_ValorUndCntAtual_Forecolor = (int)(AV150Color);
         edtavSaldocontrato_saldo_Forecolor = (int)(AV150Color);
         edtContratada_PessoaCNPJ_Forecolor = (int)(AV150Color);
         AV78SaldoContrato_Saldo = A40000GXC1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavSaldocontrato_saldo_Internalname, StringUtil.LTrim( StringUtil.Str( AV78SaldoContrato_Saldo, 18, 5)));
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 128;
         }
         sendrow_1282( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_128_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(128, GridRow);
         }
      }

      protected void E14652( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E19652( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV19DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
      }

      protected void E15652( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV29DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
         AV30DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersIgnoreFirst", AV30DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV29DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
         AV30DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersIgnoreFirst", AV30DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV50Contrato_AreaTrabalhoCod, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV40Contrato_Numero1, AV51Contrato_NumeroAta1, AV133Contrato_PrepostoNom1, AV18Contratada_PessoaNom1, AV43Contrato_Ano1, AV44Contrato_Ano_To1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV41Contrato_Numero2, AV52Contrato_NumeroAta2, AV134Contrato_PrepostoNom2, AV23Contratada_PessoaNom2, AV45Contrato_Ano2, AV46Contrato_Ano_To2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV42Contrato_Numero3, AV53Contrato_NumeroAta3, AV135Contrato_PrepostoNom3, AV28Contratada_PessoaNom3, AV47Contrato_Ano3, AV48Contrato_Ano_To3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV121TFContrato_Ativo_Sel, AV132ddo_Contrato_ValorUndCntAtualTitleControlIdToReplace, AV122ddo_Contrato_AtivoTitleControlIdToReplace, AV130TFContrato_ValorUndCntAtual, AV131TFContrato_ValorUndCntAtual_To, AV189Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving, A39Contratada_Codigo, AV6WWPContext, A74Contrato_Codigo, A92Contrato_Ativo) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV25DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
      }

      protected void E20652( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E21652( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV24DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersEnabled3", AV24DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
      }

      protected void E16652( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV29DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
         AV19DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV29DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV50Contrato_AreaTrabalhoCod, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV40Contrato_Numero1, AV51Contrato_NumeroAta1, AV133Contrato_PrepostoNom1, AV18Contratada_PessoaNom1, AV43Contrato_Ano1, AV44Contrato_Ano_To1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV41Contrato_Numero2, AV52Contrato_NumeroAta2, AV134Contrato_PrepostoNom2, AV23Contratada_PessoaNom2, AV45Contrato_Ano2, AV46Contrato_Ano_To2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV42Contrato_Numero3, AV53Contrato_NumeroAta3, AV135Contrato_PrepostoNom3, AV28Contratada_PessoaNom3, AV47Contrato_Ano3, AV48Contrato_Ano_To3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV121TFContrato_Ativo_Sel, AV132ddo_Contrato_ValorUndCntAtualTitleControlIdToReplace, AV122ddo_Contrato_AtivoTitleControlIdToReplace, AV130TFContrato_ValorUndCntAtual, AV131TFContrato_ValorUndCntAtual_To, AV189Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving, A39Contratada_Codigo, AV6WWPContext, A74Contrato_Codigo, A92Contrato_Ativo) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV25DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
      }

      protected void E22652( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E17652( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV29DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
         AV24DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersEnabled3", AV24DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV29DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV50Contrato_AreaTrabalhoCod, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV40Contrato_Numero1, AV51Contrato_NumeroAta1, AV133Contrato_PrepostoNom1, AV18Contratada_PessoaNom1, AV43Contrato_Ano1, AV44Contrato_Ano_To1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV41Contrato_Numero2, AV52Contrato_NumeroAta2, AV134Contrato_PrepostoNom2, AV23Contratada_PessoaNom2, AV45Contrato_Ano2, AV46Contrato_Ano_To2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV42Contrato_Numero3, AV53Contrato_NumeroAta3, AV135Contrato_PrepostoNom3, AV28Contratada_PessoaNom3, AV47Contrato_Ano3, AV48Contrato_Ano_To3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV121TFContrato_Ativo_Sel, AV132ddo_Contrato_ValorUndCntAtualTitleControlIdToReplace, AV122ddo_Contrato_AtivoTitleControlIdToReplace, AV130TFContrato_ValorUndCntAtual, AV131TFContrato_ValorUndCntAtual_To, AV189Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving, A39Contratada_Codigo, AV6WWPContext, A74Contrato_Codigo, A92Contrato_Ativo) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV25DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
      }

      protected void E23652( )
      {
         /* Dynamicfiltersselector3_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E18652( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefresh();
         dynavContrato_areatrabalhocod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV50Contrato_AreaTrabalhoCod), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContrato_areatrabalhocod_Internalname, "Values", dynavContrato_areatrabalhocod.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV25DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
      }

      protected void S182( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_contrato_ativo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_ativo_Internalname, "SortedStatus", Ddo_contrato_ativo_Sortedstatus);
      }

      protected void S162( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 6 )
         {
            Ddo_contrato_ativo_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_ativo_Internalname, "SortedStatus", Ddo_contrato_ativo_Sortedstatus);
         }
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavContrato_numero1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContrato_numero1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContrato_numero1_Visible), 5, 0)));
         edtavContrato_numeroata1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContrato_numeroata1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContrato_numeroata1_Visible), 5, 0)));
         edtavContrato_prepostonom1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContrato_prepostonom1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContrato_prepostonom1_Visible), 5, 0)));
         edtavContratada_pessoanom1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratada_pessoanom1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratada_pessoanom1_Visible), 5, 0)));
         tblTablemergeddynamicfilterscontrato_ano1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontrato_ano1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontrato_ano1_Visible), 5, 0)));
         cmbavDynamicfiltersoperator1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATO_NUMERO") == 0 )
         {
            edtavContrato_numero1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContrato_numero1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContrato_numero1_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATO_NUMEROATA") == 0 )
         {
            edtavContrato_numeroata1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContrato_numeroata1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContrato_numeroata1_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATO_PREPOSTONOM") == 0 )
         {
            edtavContrato_prepostonom1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContrato_prepostonom1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContrato_prepostonom1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATADA_PESSOANOM") == 0 )
         {
            edtavContratada_pessoanom1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratada_pessoanom1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratada_pessoanom1_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATO_ANO") == 0 )
         {
            tblTablemergeddynamicfilterscontrato_ano1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontrato_ano1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontrato_ano1_Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         edtavContrato_numero2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContrato_numero2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContrato_numero2_Visible), 5, 0)));
         edtavContrato_numeroata2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContrato_numeroata2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContrato_numeroata2_Visible), 5, 0)));
         edtavContrato_prepostonom2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContrato_prepostonom2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContrato_prepostonom2_Visible), 5, 0)));
         edtavContratada_pessoanom2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratada_pessoanom2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratada_pessoanom2_Visible), 5, 0)));
         tblTablemergeddynamicfilterscontrato_ano2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontrato_ano2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontrato_ano2_Visible), 5, 0)));
         cmbavDynamicfiltersoperator2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTRATO_NUMERO") == 0 )
         {
            edtavContrato_numero2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContrato_numero2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContrato_numero2_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTRATO_NUMEROATA") == 0 )
         {
            edtavContrato_numeroata2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContrato_numeroata2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContrato_numeroata2_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTRATO_PREPOSTONOM") == 0 )
         {
            edtavContrato_prepostonom2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContrato_prepostonom2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContrato_prepostonom2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTRATADA_PESSOANOM") == 0 )
         {
            edtavContratada_pessoanom2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratada_pessoanom2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratada_pessoanom2_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTRATO_ANO") == 0 )
         {
            tblTablemergeddynamicfilterscontrato_ano2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontrato_ano2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontrato_ano2_Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         edtavContrato_numero3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContrato_numero3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContrato_numero3_Visible), 5, 0)));
         edtavContrato_numeroata3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContrato_numeroata3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContrato_numeroata3_Visible), 5, 0)));
         edtavContrato_prepostonom3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContrato_prepostonom3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContrato_prepostonom3_Visible), 5, 0)));
         edtavContratada_pessoanom3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratada_pessoanom3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratada_pessoanom3_Visible), 5, 0)));
         tblTablemergeddynamicfilterscontrato_ano3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontrato_ano3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontrato_ano3_Visible), 5, 0)));
         cmbavDynamicfiltersoperator3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "CONTRATO_NUMERO") == 0 )
         {
            edtavContrato_numero3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContrato_numero3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContrato_numero3_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "CONTRATO_NUMEROATA") == 0 )
         {
            edtavContrato_numeroata3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContrato_numeroata3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContrato_numeroata3_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "CONTRATO_PREPOSTONOM") == 0 )
         {
            edtavContrato_prepostonom3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContrato_prepostonom3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContrato_prepostonom3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "CONTRATADA_PESSOANOM") == 0 )
         {
            edtavContratada_pessoanom3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratada_pessoanom3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratada_pessoanom3_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "CONTRATO_ANO") == 0 )
         {
            tblTablemergeddynamicfilterscontrato_ano3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfilterscontrato_ano3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfilterscontrato_ano3_Visible), 5, 0)));
         }
      }

      protected void S202( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV19DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
         AV20DynamicFiltersSelector2 = "CONTRATO_NUMERO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
         AV41Contrato_Numero2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41Contrato_Numero2", AV41Contrato_Numero2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV24DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersEnabled3", AV24DynamicFiltersEnabled3);
         AV25DynamicFiltersSelector3 = "CONTRATO_NUMERO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
         AV42Contrato_Numero3 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42Contrato_Numero3", AV42Contrato_Numero3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S222( )
      {
         /* 'CLEANFILTERS' Routine */
         AV50Contrato_AreaTrabalhoCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50Contrato_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV50Contrato_AreaTrabalhoCod), 6, 0)));
         AV130TFContrato_ValorUndCntAtual = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV130TFContrato_ValorUndCntAtual", StringUtil.LTrim( StringUtil.Str( AV130TFContrato_ValorUndCntAtual, 18, 5)));
         Ddo_contrato_valorundcntatual_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_valorundcntatual_Internalname, "FilteredText_set", Ddo_contrato_valorundcntatual_Filteredtext_set);
         AV131TFContrato_ValorUndCntAtual_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV131TFContrato_ValorUndCntAtual_To", StringUtil.LTrim( StringUtil.Str( AV131TFContrato_ValorUndCntAtual_To, 18, 5)));
         Ddo_contrato_valorundcntatual_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_valorundcntatual_Internalname, "FilteredTextTo_set", Ddo_contrato_valorundcntatual_Filteredtextto_set);
         AV121TFContrato_Ativo_Sel = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV121TFContrato_Ativo_Sel", StringUtil.Str( (decimal)(AV121TFContrato_Ativo_Sel), 1, 0));
         Ddo_contrato_ativo_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_ativo_Internalname, "SelectedValue_set", Ddo_contrato_ativo_Selectedvalue_set);
         AV15DynamicFiltersSelector1 = "CONTRATO_NUMERO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         AV40Contrato_Numero1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40Contrato_Numero1", AV40Contrato_Numero1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         context.DoAjaxRefresh();
      }

      protected void S152( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV33Session.Get(AV189Pgmname+"GridState"), "") == 0 )
         {
            AV10GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV189Pgmname+"GridState"), "");
         }
         else
         {
            AV10GridState.FromXml(AV33Session.Get(AV189Pgmname+"GridState"), "");
         }
         AV13OrderedBy = AV10GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         AV14OrderedDsc = AV10GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADREGFILTERSSTATE' */
         S232 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_gotopage( AV10GridState.gxTpr_Currentpage) ;
      }

      protected void S232( )
      {
         /* 'LOADREGFILTERSSTATE' Routine */
         AV190GXV1 = 1;
         while ( AV190GXV1 <= AV10GridState.gxTpr_Filtervalues.Count )
         {
            AV11GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV10GridState.gxTpr_Filtervalues.Item(AV190GXV1));
            if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "CONTRATO_AREATRABALHOCOD") == 0 )
            {
               AV50Contrato_AreaTrabalhoCod = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50Contrato_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV50Contrato_AreaTrabalhoCod), 6, 0)));
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATO_VALORUNDCNTATUAL") == 0 )
            {
               AV130TFContrato_ValorUndCntAtual = NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV130TFContrato_ValorUndCntAtual", StringUtil.LTrim( StringUtil.Str( AV130TFContrato_ValorUndCntAtual, 18, 5)));
               AV131TFContrato_ValorUndCntAtual_To = NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Valueto, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV131TFContrato_ValorUndCntAtual_To", StringUtil.LTrim( StringUtil.Str( AV131TFContrato_ValorUndCntAtual_To, 18, 5)));
               if ( ! (Convert.ToDecimal(0)==AV130TFContrato_ValorUndCntAtual) )
               {
                  Ddo_contrato_valorundcntatual_Filteredtext_set = StringUtil.Str( AV130TFContrato_ValorUndCntAtual, 18, 5);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_valorundcntatual_Internalname, "FilteredText_set", Ddo_contrato_valorundcntatual_Filteredtext_set);
               }
               if ( ! (Convert.ToDecimal(0)==AV131TFContrato_ValorUndCntAtual_To) )
               {
                  Ddo_contrato_valorundcntatual_Filteredtextto_set = StringUtil.Str( AV131TFContrato_ValorUndCntAtual_To, 18, 5);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_valorundcntatual_Internalname, "FilteredTextTo_set", Ddo_contrato_valorundcntatual_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATO_ATIVO_SEL") == 0 )
            {
               AV121TFContrato_Ativo_Sel = (short)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV121TFContrato_Ativo_Sel", StringUtil.Str( (decimal)(AV121TFContrato_Ativo_Sel), 1, 0));
               if ( ! (0==AV121TFContrato_Ativo_Sel) )
               {
                  Ddo_contrato_ativo_Selectedvalue_set = StringUtil.Str( (decimal)(AV121TFContrato_Ativo_Sel), 1, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contrato_ativo_Internalname, "SelectedValue_set", Ddo_contrato_ativo_Selectedvalue_set);
               }
            }
            AV190GXV1 = (int)(AV190GXV1+1);
         }
      }

      protected void S212( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         cmbavDynamicfiltersoperator1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         cmbavDynamicfiltersoperator2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         cmbavDynamicfiltersoperator3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV15DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATO_NUMERO") == 0 )
            {
               AV40Contrato_Numero1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40Contrato_Numero1", AV40Contrato_Numero1);
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATO_NUMEROATA") == 0 )
            {
               AV51Contrato_NumeroAta1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51Contrato_NumeroAta1", AV51Contrato_NumeroAta1);
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATO_PREPOSTONOM") == 0 )
            {
               AV16DynamicFiltersOperator1 = AV12GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV133Contrato_PrepostoNom1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV133Contrato_PrepostoNom1", AV133Contrato_PrepostoNom1);
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATADA_PESSOANOM") == 0 )
            {
               AV18Contratada_PessoaNom1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Contratada_PessoaNom1", AV18Contratada_PessoaNom1);
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATO_ANO") == 0 )
            {
               AV43Contrato_Ano1 = (short)(NumberUtil.Val( AV12GridStateDynamicFilter.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43Contrato_Ano1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV43Contrato_Ano1), 4, 0)));
               AV44Contrato_Ano_To1 = (short)(NumberUtil.Val( AV12GridStateDynamicFilter.gxTpr_Valueto, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44Contrato_Ano_To1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44Contrato_Ano_To1), 4, 0)));
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV19DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
               AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(2));
               AV20DynamicFiltersSelector2 = AV12GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTRATO_NUMERO") == 0 )
               {
                  AV41Contrato_Numero2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41Contrato_Numero2", AV41Contrato_Numero2);
               }
               else if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTRATO_NUMEROATA") == 0 )
               {
                  AV52Contrato_NumeroAta2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52Contrato_NumeroAta2", AV52Contrato_NumeroAta2);
               }
               else if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTRATO_PREPOSTONOM") == 0 )
               {
                  AV21DynamicFiltersOperator2 = AV12GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
                  AV134Contrato_PrepostoNom2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV134Contrato_PrepostoNom2", AV134Contrato_PrepostoNom2);
               }
               else if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTRATADA_PESSOANOM") == 0 )
               {
                  AV23Contratada_PessoaNom2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Contratada_PessoaNom2", AV23Contratada_PessoaNom2);
               }
               else if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTRATO_ANO") == 0 )
               {
                  AV45Contrato_Ano2 = (short)(NumberUtil.Val( AV12GridStateDynamicFilter.gxTpr_Value, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45Contrato_Ano2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV45Contrato_Ano2), 4, 0)));
                  AV46Contrato_Ano_To2 = (short)(NumberUtil.Val( AV12GridStateDynamicFilter.gxTpr_Valueto, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46Contrato_Ano_To2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV46Contrato_Ano_To2), 4, 0)));
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV24DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersEnabled3", AV24DynamicFiltersEnabled3);
                  AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(3));
                  AV25DynamicFiltersSelector3 = AV12GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "CONTRATO_NUMERO") == 0 )
                  {
                     AV42Contrato_Numero3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42Contrato_Numero3", AV42Contrato_Numero3);
                  }
                  else if ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "CONTRATO_NUMEROATA") == 0 )
                  {
                     AV53Contrato_NumeroAta3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53Contrato_NumeroAta3", AV53Contrato_NumeroAta3);
                  }
                  else if ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "CONTRATO_PREPOSTONOM") == 0 )
                  {
                     AV26DynamicFiltersOperator3 = AV12GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0)));
                     AV135Contrato_PrepostoNom3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV135Contrato_PrepostoNom3", AV135Contrato_PrepostoNom3);
                  }
                  else if ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "CONTRATADA_PESSOANOM") == 0 )
                  {
                     AV28Contratada_PessoaNom3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28Contratada_PessoaNom3", AV28Contratada_PessoaNom3);
                  }
                  else if ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "CONTRATO_ANO") == 0 )
                  {
                     AV47Contrato_Ano3 = (short)(NumberUtil.Val( AV12GridStateDynamicFilter.gxTpr_Value, "."));
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47Contrato_Ano3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV47Contrato_Ano3), 4, 0)));
                     AV48Contrato_Ano_To3 = (short)(NumberUtil.Val( AV12GridStateDynamicFilter.gxTpr_Valueto, "."));
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48Contrato_Ano_To3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV48Contrato_Ano_To3), 4, 0)));
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S132 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV29DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S172( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV10GridState.FromXml(AV33Session.Get(AV189Pgmname+"GridState"), "");
         AV10GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV10GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV10GridState.gxTpr_Filtervalues.Clear();
         if ( ! (0==AV50Contrato_AreaTrabalhoCod) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "CONTRATO_AREATRABALHOCOD";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV50Contrato_AreaTrabalhoCod), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (Convert.ToDecimal(0)==AV130TFContrato_ValorUndCntAtual) && (Convert.ToDecimal(0)==AV131TFContrato_ValorUndCntAtual_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATO_VALORUNDCNTATUAL";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( AV130TFContrato_ValorUndCntAtual, 18, 5);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( AV131TFContrato_ValorUndCntAtual_To, 18, 5);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! (0==AV121TFContrato_Ativo_Sel) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATO_ATIVO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV121TFContrato_Ativo_Sel), 1, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Currentpage = (short)(subGrid_Currentpage( ));
         new wwpbaseobjects.savegridstate(context ).execute(  AV189Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_MeetrikaVs3")) ;
      }

      protected void S192( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV30DynamicFiltersIgnoreFirst )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV15DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATO_NUMERO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV40Contrato_Numero1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV40Contrato_Numero1;
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATO_NUMEROATA") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV51Contrato_NumeroAta1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV51Contrato_NumeroAta1;
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATO_PREPOSTONOM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV133Contrato_PrepostoNom1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV133Contrato_PrepostoNom1;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV16DynamicFiltersOperator1;
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATADA_PESSOANOM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV18Contratada_PessoaNom1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV18Contratada_PessoaNom1;
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATO_ANO") == 0 ) && ! ( (0==AV43Contrato_Ano1) && (0==AV44Contrato_Ano_To1) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV43Contrato_Ano1), 4, 0);
               AV12GridStateDynamicFilter.gxTpr_Valueto = StringUtil.Str( (decimal)(AV44Contrato_Ano_To1), 4, 0);
            }
            if ( AV29DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Valueto)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV19DynamicFiltersEnabled2 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV20DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTRATO_NUMERO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV41Contrato_Numero2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV41Contrato_Numero2;
            }
            else if ( ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTRATO_NUMEROATA") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV52Contrato_NumeroAta2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV52Contrato_NumeroAta2;
            }
            else if ( ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTRATO_PREPOSTONOM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV134Contrato_PrepostoNom2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV134Contrato_PrepostoNom2;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV21DynamicFiltersOperator2;
            }
            else if ( ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTRATADA_PESSOANOM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV23Contratada_PessoaNom2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV23Contratada_PessoaNom2;
            }
            else if ( ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "CONTRATO_ANO") == 0 ) && ! ( (0==AV45Contrato_Ano2) && (0==AV46Contrato_Ano_To2) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV45Contrato_Ano2), 4, 0);
               AV12GridStateDynamicFilter.gxTpr_Valueto = StringUtil.Str( (decimal)(AV46Contrato_Ano_To2), 4, 0);
            }
            if ( AV29DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Valueto)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV24DynamicFiltersEnabled3 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV25DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "CONTRATO_NUMERO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV42Contrato_Numero3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV42Contrato_Numero3;
            }
            else if ( ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "CONTRATO_NUMEROATA") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV53Contrato_NumeroAta3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV53Contrato_NumeroAta3;
            }
            else if ( ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "CONTRATO_PREPOSTONOM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV135Contrato_PrepostoNom3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV135Contrato_PrepostoNom3;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV26DynamicFiltersOperator3;
            }
            else if ( ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "CONTRATADA_PESSOANOM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV28Contratada_PessoaNom3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV28Contratada_PessoaNom3;
            }
            else if ( ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "CONTRATO_ANO") == 0 ) && ! ( (0==AV47Contrato_Ano3) && (0==AV48Contrato_Ano_To3) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV47Contrato_Ano3), 4, 0);
               AV12GridStateDynamicFilter.gxTpr_Valueto = StringUtil.Str( (decimal)(AV48Contrato_Ano_To3), 4, 0);
            }
            if ( AV29DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Valueto)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
      }

      protected void S142( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV189Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = true;
         AV8TrnContext.gxTpr_Callerurl = AV7HTTPRequest.ScriptName+"?"+AV7HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "Contrato";
         AV33Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_MeetrikaVs3"));
      }

      protected void wb_table1_2_652( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_652( true) ;
         }
         else
         {
            wb_table2_8_652( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_652e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            wb_table3_122_652( true) ;
         }
         else
         {
            wb_table3_122_652( false) ;
         }
         return  ;
      }

      protected void wb_table3_122_652e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_652e( true) ;
         }
         else
         {
            wb_table1_2_652e( false) ;
         }
      }

      protected void wb_table3_122_652( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_125_652( true) ;
         }
         else
         {
            wb_table4_125_652( false) ;
         }
         return  ;
      }

      protected void wb_table4_125_652e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_122_652e( true) ;
         }
         else
         {
            wb_table3_122_652e( false) ;
         }
      }

      protected void wb_table4_125_652( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"128\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "C�digo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratada_PessoaNom_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratada_PessoaNom_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratada_PessoaNom_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(110), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContrato_Numero_Titleformat == 0 )
               {
                  context.SendWebValue( edtContrato_Numero_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContrato_Numero_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(60), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContrato_NumeroAta_Titleformat == 0 )
               {
                  context.SendWebValue( edtContrato_NumeroAta_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContrato_NumeroAta_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(34), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContrato_Ano_Titleformat == 0 )
               {
                  context.SendWebValue( edtContrato_Ano_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContrato_Ano_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Vig�ncia") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContrato_ValorUndCntAtual_Titleformat == 0 )
               {
                  context.SendWebValue( edtContrato_ValorUndCntAtual_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContrato_ValorUndCntAtual_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Saldo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratada_PessoaCNPJ_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratada_PessoaCNPJ_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratada_PessoaCNPJ_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( chkContrato_Ativo_Titleformat == 0 )
               {
                  context.SendWebValue( chkContrato_Ativo.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( chkContrato_Ativo.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( edtavAssociargestor_Title) ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( edtavAssociarauxiliar_Title) ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( edtavAssociarsistema_Title) ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV31Update));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavUpdate_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavUpdate_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV32Delete));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDelete_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDelete_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV49Display));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDisplay_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDisplay_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A74Contrato_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A41Contratada_PessoaNom));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratada_PessoaNom_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratada_PessoaNom_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Forecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratada_PessoaNom_Forecolor), 9, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtContratada_PessoaNom_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A77Contrato_Numero));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContrato_Numero_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContrato_Numero_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Forecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContrato_Numero_Forecolor), 9, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A78Contrato_NumeroAta));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContrato_NumeroAta_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContrato_NumeroAta_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Forecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContrato_NumeroAta_Forecolor), 9, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtContrato_NumeroAta_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A79Contrato_Ano), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContrato_Ano_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContrato_Ano_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Forecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContrato_Ano_Forecolor), 9, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.Format(A1869Contrato_DataTermino, "99/99/99"));
               GridColumn.AddObjectProperty("Forecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContrato_DataTermino_Forecolor), 9, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A1870Contrato_ValorUndCntAtual, 18, 5, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContrato_ValorUndCntAtual_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContrato_ValorUndCntAtual_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Forecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContrato_ValorUndCntAtual_Forecolor), 9, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( AV78SaldoContrato_Saldo, 18, 5, ".", "")));
               GridColumn.AddObjectProperty("Forecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavSaldocontrato_saldo_Forecolor), 9, 0, ".", "")));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavSaldocontrato_saldo_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A42Contratada_PessoaCNPJ);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratada_PessoaCNPJ_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratada_PessoaCNPJ_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Forecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratada_PessoaCNPJ_Forecolor), 9, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.BoolToStr( A92Contrato_Ativo));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( chkContrato_Ativo.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(chkContrato_Ativo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV75AssociarGestor));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtavAssociargestor_Title));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavAssociargestor_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV119AssociarAuxiliar));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtavAssociarauxiliar_Title));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavAssociarauxiliar_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV80AssociarSistema));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtavAssociarsistema_Title));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavAssociarsistema_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 128 )
         {
            wbEnd = 0;
            nRC_GXsfl_128 = (short)(nGXsfl_128_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_125_652e( true) ;
         }
         else
         {
            wb_table4_125_652e( false) ;
         }
      }

      protected void wb_table2_8_652( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableheader_Internalname, tblTableheader_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table5_11_652( true) ;
         }
         else
         {
            wb_table5_11_652( false) ;
         }
         return  ;
      }

      protected void wb_table5_11_652e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_652e( true) ;
         }
         else
         {
            wb_table2_8_652e( false) ;
         }
      }

      protected void wb_table5_11_652( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='GridHeaderCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblContratotitle_Internalname, "Contratos", "", "", lblContratotitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WWContrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_WWContrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'" + sGXsfl_128_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,18);\"", "", true, "HLP_WWContrato.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'',false,'" + sGXsfl_128_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,19);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_WWContrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table6_21_652( true) ;
         }
         else
         {
            wb_table6_21_652( false) ;
         }
         return  ;
      }

      protected void wb_table6_21_652e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_11_652e( true) ;
         }
         else
         {
            wb_table5_11_652e( false) ;
         }
      }

      protected void wb_table6_21_652( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "TableFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\">") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWContrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_26_652( true) ;
         }
         else
         {
            wb_table7_26_652( false) ;
         }
         return  ;
      }

      protected void wb_table7_26_652e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_21_652e( true) ;
         }
         else
         {
            wb_table6_21_652e( false) ;
         }
      }

      protected void wb_table7_26_652( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable1_Internalname, tblUnnamedtable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblFiltertextcontrato_areatrabalhocod_Internalname, "Area de Trabalho", "", "", lblFiltertextcontrato_areatrabalhocod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Invisible", 0, "", 1, 1, 0, "HLP_WWContrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 31,'',false,'" + sGXsfl_128_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavContrato_areatrabalhocod, dynavContrato_areatrabalhocod_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV50Contrato_AreaTrabalhoCod), 6, 0)), 1, dynavContrato_areatrabalhocod_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Invisible", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,31);\"", "", true, "HLP_WWContrato.htm");
            dynavContrato_areatrabalhocod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV50Contrato_AreaTrabalhoCod), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContrato_areatrabalhocod_Internalname, "Values", (String)(dynavContrato_areatrabalhocod.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table8_34_652( true) ;
         }
         else
         {
            wb_table8_34_652( false) ;
         }
         return  ;
      }

      protected void wb_table8_34_652e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_WWContrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_26_652e( true) ;
         }
         else
         {
            wb_table7_26_652e( false) ;
         }
      }

      protected void wb_table8_34_652( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWContrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'" + sGXsfl_128_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV15DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,39);\"", "", true, "HLP_WWContrato.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWContrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table9_43_652( true) ;
         }
         else
         {
            wb_table9_43_652( false) ;
         }
         return  ;
      }

      protected void wb_table9_43_652e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 61,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWContrato.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 62,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWContrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWContrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 67,'',false,'" + sGXsfl_128_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV20DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,67);\"", "", true, "HLP_WWContrato.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWContrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table10_71_652( true) ;
         }
         else
         {
            wb_table10_71_652( false) ;
         }
         return  ;
      }

      protected void wb_table10_71_652e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 89,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWContrato.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 90,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWContrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWContrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 95,'',false,'" + sGXsfl_128_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV25DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR3.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,95);\"", "", true, "HLP_WWContrato.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV25DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWContrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table11_99_652( true) ;
         }
         else
         {
            wb_table11_99_652( false) ;
         }
         return  ;
      }

      protected void wb_table11_99_652e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 117,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWContrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_34_652e( true) ;
         }
         else
         {
            wb_table8_34_652e( false) ;
         }
      }

      protected void wb_table11_99_652( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters3_Internalname, tblTablemergeddynamicfilters3_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 102,'',false,'" + sGXsfl_128_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator3, cmbavDynamicfiltersoperator3_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0)), 1, cmbavDynamicfiltersoperator3_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator3.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,102);\"", "", true, "HLP_WWContrato.htm");
            cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", (String)(cmbavDynamicfiltersoperator3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 104,'',false,'" + sGXsfl_128_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContrato_numero3_Internalname, StringUtil.RTrim( AV42Contrato_Numero3), StringUtil.RTrim( context.localUtil.Format( AV42Contrato_Numero3, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,104);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContrato_numero3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContrato_numero3_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContrato.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 105,'',false,'" + sGXsfl_128_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContrato_numeroata3_Internalname, StringUtil.RTrim( AV53Contrato_NumeroAta3), StringUtil.RTrim( context.localUtil.Format( AV53Contrato_NumeroAta3, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,105);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContrato_numeroata3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContrato_numeroata3_Visible, 1, 0, "text", "", 10, "chr", 1, "row", 10, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContrato.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 106,'',false,'" + sGXsfl_128_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContrato_prepostonom3_Internalname, StringUtil.RTrim( AV135Contrato_PrepostoNom3), StringUtil.RTrim( context.localUtil.Format( AV135Contrato_PrepostoNom3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,106);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContrato_prepostonom3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContrato_prepostonom3_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContrato.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 107,'',false,'" + sGXsfl_128_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratada_pessoanom3_Internalname, StringUtil.RTrim( AV28Contratada_PessoaNom3), StringUtil.RTrim( context.localUtil.Format( AV28Contratada_PessoaNom3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,107);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratada_pessoanom3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContratada_pessoanom3_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContrato.htm");
            wb_table12_108_652( true) ;
         }
         else
         {
            wb_table12_108_652( false) ;
         }
         return  ;
      }

      protected void wb_table12_108_652e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table11_99_652e( true) ;
         }
         else
         {
            wb_table11_99_652e( false) ;
         }
      }

      protected void wb_table12_108_652( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterscontrato_ano3_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterscontrato_ano3_Internalname, tblTablemergeddynamicfilterscontrato_ano3_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 111,'',false,'" + sGXsfl_128_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContrato_ano3_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV47Contrato_Ano3), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV47Contrato_Ano3), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,111);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContrato_ano3_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 50, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfilterscontrato_ano_rangemiddletext3_Internalname, "at�", "", "", lblDynamicfilterscontrato_ano_rangemiddletext3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWContrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 115,'',false,'" + sGXsfl_128_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContrato_ano_to3_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV48Contrato_Ano_To3), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV48Contrato_Ano_To3), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,115);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContrato_ano_to3_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 50, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table12_108_652e( true) ;
         }
         else
         {
            wb_table12_108_652e( false) ;
         }
      }

      protected void wb_table10_71_652( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters2_Internalname, tblTablemergeddynamicfilters2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 74,'',false,'" + sGXsfl_128_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator2, cmbavDynamicfiltersoperator2_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)), 1, cmbavDynamicfiltersoperator2_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,74);\"", "", true, "HLP_WWContrato.htm");
            cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", (String)(cmbavDynamicfiltersoperator2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 76,'',false,'" + sGXsfl_128_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContrato_numero2_Internalname, StringUtil.RTrim( AV41Contrato_Numero2), StringUtil.RTrim( context.localUtil.Format( AV41Contrato_Numero2, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,76);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContrato_numero2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContrato_numero2_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContrato.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 77,'',false,'" + sGXsfl_128_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContrato_numeroata2_Internalname, StringUtil.RTrim( AV52Contrato_NumeroAta2), StringUtil.RTrim( context.localUtil.Format( AV52Contrato_NumeroAta2, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,77);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContrato_numeroata2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContrato_numeroata2_Visible, 1, 0, "text", "", 10, "chr", 1, "row", 10, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContrato.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 78,'',false,'" + sGXsfl_128_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContrato_prepostonom2_Internalname, StringUtil.RTrim( AV134Contrato_PrepostoNom2), StringUtil.RTrim( context.localUtil.Format( AV134Contrato_PrepostoNom2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,78);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContrato_prepostonom2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContrato_prepostonom2_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContrato.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 79,'',false,'" + sGXsfl_128_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratada_pessoanom2_Internalname, StringUtil.RTrim( AV23Contratada_PessoaNom2), StringUtil.RTrim( context.localUtil.Format( AV23Contratada_PessoaNom2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,79);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratada_pessoanom2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContratada_pessoanom2_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContrato.htm");
            wb_table13_80_652( true) ;
         }
         else
         {
            wb_table13_80_652( false) ;
         }
         return  ;
      }

      protected void wb_table13_80_652e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table10_71_652e( true) ;
         }
         else
         {
            wb_table10_71_652e( false) ;
         }
      }

      protected void wb_table13_80_652( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterscontrato_ano2_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterscontrato_ano2_Internalname, tblTablemergeddynamicfilterscontrato_ano2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 83,'',false,'" + sGXsfl_128_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContrato_ano2_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV45Contrato_Ano2), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV45Contrato_Ano2), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,83);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContrato_ano2_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 50, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfilterscontrato_ano_rangemiddletext2_Internalname, "at�", "", "", lblDynamicfilterscontrato_ano_rangemiddletext2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWContrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 87,'',false,'" + sGXsfl_128_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContrato_ano_to2_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV46Contrato_Ano_To2), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV46Contrato_Ano_To2), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,87);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContrato_ano_to2_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 50, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table13_80_652e( true) ;
         }
         else
         {
            wb_table13_80_652e( false) ;
         }
      }

      protected void wb_table9_43_652( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters1_Internalname, tblTablemergeddynamicfilters1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'',false,'" + sGXsfl_128_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator1, cmbavDynamicfiltersoperator1_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)), 1, cmbavDynamicfiltersoperator1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,46);\"", "", true, "HLP_WWContrato.htm");
            cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", (String)(cmbavDynamicfiltersoperator1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 48,'',false,'" + sGXsfl_128_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContrato_numero1_Internalname, StringUtil.RTrim( AV40Contrato_Numero1), StringUtil.RTrim( context.localUtil.Format( AV40Contrato_Numero1, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,48);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContrato_numero1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContrato_numero1_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContrato.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'" + sGXsfl_128_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContrato_numeroata1_Internalname, StringUtil.RTrim( AV51Contrato_NumeroAta1), StringUtil.RTrim( context.localUtil.Format( AV51Contrato_NumeroAta1, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,49);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContrato_numeroata1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContrato_numeroata1_Visible, 1, 0, "text", "", 10, "chr", 1, "row", 10, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContrato.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 50,'',false,'" + sGXsfl_128_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContrato_prepostonom1_Internalname, StringUtil.RTrim( AV133Contrato_PrepostoNom1), StringUtil.RTrim( context.localUtil.Format( AV133Contrato_PrepostoNom1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,50);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContrato_prepostonom1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContrato_prepostonom1_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContrato.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 51,'',false,'" + sGXsfl_128_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratada_pessoanom1_Internalname, StringUtil.RTrim( AV18Contratada_PessoaNom1), StringUtil.RTrim( context.localUtil.Format( AV18Contratada_PessoaNom1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,51);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratada_pessoanom1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContratada_pessoanom1_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWContrato.htm");
            wb_table14_52_652( true) ;
         }
         else
         {
            wb_table14_52_652( false) ;
         }
         return  ;
      }

      protected void wb_table14_52_652e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_43_652e( true) ;
         }
         else
         {
            wb_table9_43_652e( false) ;
         }
      }

      protected void wb_table14_52_652( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfilterscontrato_ano1_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilterscontrato_ano1_Internalname, tblTablemergeddynamicfilterscontrato_ano1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 55,'',false,'" + sGXsfl_128_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContrato_ano1_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV43Contrato_Ano1), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV43Contrato_Ano1), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,55);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContrato_ano1_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 50, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfilterscontrato_ano_rangemiddletext1_Internalname, "at�", "", "", lblDynamicfilterscontrato_ano_rangemiddletext1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWContrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'" + sGXsfl_128_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContrato_ano_to1_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV44Contrato_Ano_To1), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV44Contrato_Ano_To1), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,59);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContrato_ano_to1_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 50, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContrato.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table14_52_652e( true) ;
         }
         else
         {
            wb_table14_52_652e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA652( ) ;
         WS652( ) ;
         WE652( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202052993550");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxdec.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wwcontrato.js", "?202052993550");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_1282( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_128_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_128_idx;
         edtavDisplay_Internalname = "vDISPLAY_"+sGXsfl_128_idx;
         edtContrato_Codigo_Internalname = "CONTRATO_CODIGO_"+sGXsfl_128_idx;
         edtContratada_PessoaNom_Internalname = "CONTRATADA_PESSOANOM_"+sGXsfl_128_idx;
         edtContrato_Numero_Internalname = "CONTRATO_NUMERO_"+sGXsfl_128_idx;
         edtContrato_NumeroAta_Internalname = "CONTRATO_NUMEROATA_"+sGXsfl_128_idx;
         edtContrato_Ano_Internalname = "CONTRATO_ANO_"+sGXsfl_128_idx;
         edtContrato_DataTermino_Internalname = "CONTRATO_DATATERMINO_"+sGXsfl_128_idx;
         edtContrato_ValorUndCntAtual_Internalname = "CONTRATO_VALORUNDCNTATUAL_"+sGXsfl_128_idx;
         edtavSaldocontrato_saldo_Internalname = "vSALDOCONTRATO_SALDO_"+sGXsfl_128_idx;
         edtContratada_PessoaCNPJ_Internalname = "CONTRATADA_PESSOACNPJ_"+sGXsfl_128_idx;
         chkContrato_Ativo_Internalname = "CONTRATO_ATIVO_"+sGXsfl_128_idx;
         edtavAssociargestor_Internalname = "vASSOCIARGESTOR_"+sGXsfl_128_idx;
         edtavAssociarauxiliar_Internalname = "vASSOCIARAUXILIAR_"+sGXsfl_128_idx;
         edtavAssociarsistema_Internalname = "vASSOCIARSISTEMA_"+sGXsfl_128_idx;
      }

      protected void SubsflControlProps_fel_1282( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_128_fel_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_128_fel_idx;
         edtavDisplay_Internalname = "vDISPLAY_"+sGXsfl_128_fel_idx;
         edtContrato_Codigo_Internalname = "CONTRATO_CODIGO_"+sGXsfl_128_fel_idx;
         edtContratada_PessoaNom_Internalname = "CONTRATADA_PESSOANOM_"+sGXsfl_128_fel_idx;
         edtContrato_Numero_Internalname = "CONTRATO_NUMERO_"+sGXsfl_128_fel_idx;
         edtContrato_NumeroAta_Internalname = "CONTRATO_NUMEROATA_"+sGXsfl_128_fel_idx;
         edtContrato_Ano_Internalname = "CONTRATO_ANO_"+sGXsfl_128_fel_idx;
         edtContrato_DataTermino_Internalname = "CONTRATO_DATATERMINO_"+sGXsfl_128_fel_idx;
         edtContrato_ValorUndCntAtual_Internalname = "CONTRATO_VALORUNDCNTATUAL_"+sGXsfl_128_fel_idx;
         edtavSaldocontrato_saldo_Internalname = "vSALDOCONTRATO_SALDO_"+sGXsfl_128_fel_idx;
         edtContratada_PessoaCNPJ_Internalname = "CONTRATADA_PESSOACNPJ_"+sGXsfl_128_fel_idx;
         chkContrato_Ativo_Internalname = "CONTRATO_ATIVO_"+sGXsfl_128_fel_idx;
         edtavAssociargestor_Internalname = "vASSOCIARGESTOR_"+sGXsfl_128_fel_idx;
         edtavAssociarauxiliar_Internalname = "vASSOCIARAUXILIAR_"+sGXsfl_128_fel_idx;
         edtavAssociarsistema_Internalname = "vASSOCIARSISTEMA_"+sGXsfl_128_fel_idx;
      }

      protected void sendrow_1282( )
      {
         SubsflControlProps_1282( ) ;
         WB650( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_128_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_128_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_128_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV31Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV31Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV183Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV31Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV31Update)) ? AV183Update_GXI : context.PathToRelativeUrl( AV31Update)),(String)edtavUpdate_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(int)edtavUpdate_Enabled,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV31Update_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV32Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV32Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV184Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV32Delete)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV32Delete)) ? AV184Delete_GXI : context.PathToRelativeUrl( AV32Delete)),(String)edtavDelete_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(int)edtavDelete_Enabled,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV32Delete_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV49Display_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV49Display))&&String.IsNullOrEmpty(StringUtil.RTrim( AV185Display_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV49Display)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDisplay_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV49Display)) ? AV185Display_GXI : context.PathToRelativeUrl( AV49Display)),(String)edtavDisplay_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavDisplay_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV49Display_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContrato_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A74Contrato_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A74Contrato_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContrato_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)128,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratada_PessoaNom_Internalname,StringUtil.RTrim( A41Contratada_PessoaNom),StringUtil.RTrim( context.localUtil.Format( A41Contratada_PessoaNom, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)edtContratada_PessoaNom_Link,(String)"",(String)"",(String)"",(String)edtContratada_PessoaNom_Jsonclick,(short)0,(String)"BootstrapAttribute","color:"+context.BuildHTMLColor( edtContratada_PessoaNom_Forecolor)+";",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)100,(short)0,(short)0,(short)128,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome100",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContrato_Numero_Internalname,StringUtil.RTrim( A77Contrato_Numero),(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContrato_Numero_Jsonclick,(short)0,(String)"BootstrapAttribute","color:"+context.BuildHTMLColor( edtContrato_Numero_Forecolor)+";",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)110,(String)"px",(short)17,(String)"px",(short)20,(short)0,(short)0,(short)128,(short)1,(short)-1,(short)-1,(bool)true,(String)"NumeroContrato",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContrato_NumeroAta_Internalname,StringUtil.RTrim( A78Contrato_NumeroAta),(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)edtContrato_NumeroAta_Link,(String)"",(String)"",(String)"",(String)edtContrato_NumeroAta_Jsonclick,(short)0,(String)"BootstrapAttribute","color:"+context.BuildHTMLColor( edtContrato_NumeroAta_Forecolor)+";",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)60,(String)"px",(short)17,(String)"px",(short)10,(short)0,(short)0,(short)128,(short)1,(short)-1,(short)-1,(bool)true,(String)"NumeroAta",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContrato_Ano_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A79Contrato_Ano), 4, 0, ",", "")),context.localUtil.Format( (decimal)(A79Contrato_Ano), "ZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContrato_Ano_Jsonclick,(short)0,(String)"BootstrapAttribute","color:"+context.BuildHTMLColor( edtContrato_Ano_Forecolor)+";",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)34,(String)"px",(short)17,(String)"px",(short)4,(short)0,(short)0,(short)128,(short)1,(short)-1,(short)0,(bool)true,(String)"Ano",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContrato_DataTermino_Internalname,context.localUtil.Format(A1869Contrato_DataTermino, "99/99/99"),context.localUtil.Format( A1869Contrato_DataTermino, "99/99/99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContrato_DataTermino_Jsonclick,(short)0,(String)"BootstrapAttribute","color:"+context.BuildHTMLColor( edtContrato_DataTermino_Forecolor)+";",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)8,(short)0,(short)0,(short)128,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContrato_ValorUndCntAtual_Internalname,StringUtil.LTrim( StringUtil.NToC( A1870Contrato_ValorUndCntAtual, 18, 5, ",", "")),context.localUtil.Format( A1870Contrato_ValorUndCntAtual, "ZZZ,ZZZ,ZZZ,ZZ9.99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContrato_ValorUndCntAtual_Jsonclick,(short)0,(String)"BootstrapAttribute","color:"+context.BuildHTMLColor( edtContrato_ValorUndCntAtual_Forecolor)+";",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)18,(short)0,(short)0,(short)128,(short)1,(short)-1,(short)0,(bool)true,(String)"Valor",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            TempTags = " " + ((edtavSaldocontrato_saldo_Enabled!=0)&&(edtavSaldocontrato_saldo_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 139,'',false,'"+sGXsfl_128_idx+"',128)\"" : " ");
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavSaldocontrato_saldo_Internalname,StringUtil.LTrim( StringUtil.NToC( AV78SaldoContrato_Saldo, 18, 5, ",", "")),((edtavSaldocontrato_saldo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV78SaldoContrato_Saldo, "ZZZ,ZZZ,ZZZ,ZZ9.99")) : context.localUtil.Format( AV78SaldoContrato_Saldo, "ZZZ,ZZZ,ZZZ,ZZ9.99")),TempTags+((edtavSaldocontrato_saldo_Enabled!=0)&&(edtavSaldocontrato_saldo_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavSaldocontrato_saldo_Enabled!=0)&&(edtavSaldocontrato_saldo_Visible!=0) ? " onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,139);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavSaldocontrato_saldo_Jsonclick,(short)0,(String)"BootstrapAttribute","color:"+context.BuildHTMLColor( edtavSaldocontrato_saldo_Forecolor)+";",(String)ROClassString,(String)"",(short)-1,(int)edtavSaldocontrato_saldo_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)18,(short)0,(short)0,(short)128,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratada_PessoaCNPJ_Internalname,(String)A42Contratada_PessoaCNPJ,(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratada_PessoaCNPJ_Jsonclick,(short)0,(String)"BootstrapAttribute","color:"+context.BuildHTMLColor( edtContratada_PessoaCNPJ_Forecolor)+";",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)15,(short)0,(short)0,(short)128,(short)1,(short)-1,(short)-1,(bool)true,(String)"Docto",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Check box */
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GridRow.AddColumnProperties("checkbox", 1, isAjaxCallMode( ), new Object[] {(String)chkContrato_Ativo_Internalname,StringUtil.BoolToStr( A92Contrato_Ativo),(String)"",(String)"",(short)-1,(short)0,(String)"true",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)""});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavAssociargestor_Enabled!=0)&&(edtavAssociargestor_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 142,'',false,'',128)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV75AssociarGestor_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV75AssociarGestor))&&String.IsNullOrEmpty(StringUtil.RTrim( AV186Associargestor_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV75AssociarGestor)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavAssociargestor_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV75AssociarGestor)) ? AV186Associargestor_GXI : context.PathToRelativeUrl( AV75AssociarGestor)),(String)"",(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavAssociargestor_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)7,(String)edtavAssociargestor_Jsonclick,(String)"'"+""+"'"+",false,"+"'"+"e27652_client"+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV75AssociarGestor_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavAssociarauxiliar_Enabled!=0)&&(edtavAssociarauxiliar_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 143,'',false,'',128)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV119AssociarAuxiliar_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV119AssociarAuxiliar))&&String.IsNullOrEmpty(StringUtil.RTrim( AV187Associarauxiliar_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV119AssociarAuxiliar)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavAssociarauxiliar_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV119AssociarAuxiliar)) ? AV187Associarauxiliar_GXI : context.PathToRelativeUrl( AV119AssociarAuxiliar)),(String)"",(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavAssociarauxiliar_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)7,(String)edtavAssociarauxiliar_Jsonclick,(String)"'"+""+"'"+",false,"+"'"+"e28652_client"+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV119AssociarAuxiliar_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavAssociarsistema_Enabled!=0)&&(edtavAssociarsistema_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 144,'',false,'',128)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV80AssociarSistema_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV80AssociarSistema))&&String.IsNullOrEmpty(StringUtil.RTrim( AV188Associarsistema_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV80AssociarSistema)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavAssociarsistema_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV80AssociarSistema)) ? AV188Associarsistema_GXI : context.PathToRelativeUrl( AV80AssociarSistema)),(String)"",(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavAssociarsistema_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)7,(String)edtavAssociarsistema_Jsonclick,(String)"'"+""+"'"+",false,"+"'"+"e29652_client"+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV80AssociarSistema_IsBlob,(bool)false});
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATO_CODIGO"+"_"+sGXsfl_128_idx, GetSecureSignedToken( sGXsfl_128_idx, context.localUtil.Format( (decimal)(A74Contrato_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATO_NUMERO"+"_"+sGXsfl_128_idx, GetSecureSignedToken( sGXsfl_128_idx, StringUtil.RTrim( context.localUtil.Format( A77Contrato_Numero, ""))));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATO_NUMEROATA"+"_"+sGXsfl_128_idx, GetSecureSignedToken( sGXsfl_128_idx, StringUtil.RTrim( context.localUtil.Format( A78Contrato_NumeroAta, ""))));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATO_ANO"+"_"+sGXsfl_128_idx, GetSecureSignedToken( sGXsfl_128_idx, context.localUtil.Format( (decimal)(A79Contrato_Ano), "ZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATO_DATATERMINO"+"_"+sGXsfl_128_idx, GetSecureSignedToken( sGXsfl_128_idx, A1869Contrato_DataTermino));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATO_ATIVO"+"_"+sGXsfl_128_idx, GetSecureSignedToken( sGXsfl_128_idx, A92Contrato_Ativo));
            GridContainer.AddRow(GridRow);
            nGXsfl_128_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_128_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_128_idx+1));
            sGXsfl_128_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_128_idx), 4, 0)), 4, "0");
            SubsflControlProps_1282( ) ;
         }
         /* End function sendrow_1282 */
      }

      protected void init_default_properties( )
      {
         lblContratotitle_Internalname = "CONTRATOTITLE";
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         imgCleanfilters_Internalname = "CLEANFILTERS";
         lblFiltertextcontrato_areatrabalhocod_Internalname = "FILTERTEXTCONTRATO_AREATRABALHOCOD";
         dynavContrato_areatrabalhocod_Internalname = "vCONTRATO_AREATRABALHOCOD";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         cmbavDynamicfiltersoperator1_Internalname = "vDYNAMICFILTERSOPERATOR1";
         edtavContrato_numero1_Internalname = "vCONTRATO_NUMERO1";
         edtavContrato_numeroata1_Internalname = "vCONTRATO_NUMEROATA1";
         edtavContrato_prepostonom1_Internalname = "vCONTRATO_PREPOSTONOM1";
         edtavContratada_pessoanom1_Internalname = "vCONTRATADA_PESSOANOM1";
         edtavContrato_ano1_Internalname = "vCONTRATO_ANO1";
         lblDynamicfilterscontrato_ano_rangemiddletext1_Internalname = "DYNAMICFILTERSCONTRATO_ANO_RANGEMIDDLETEXT1";
         edtavContrato_ano_to1_Internalname = "vCONTRATO_ANO_TO1";
         tblTablemergeddynamicfilterscontrato_ano1_Internalname = "TABLEMERGEDDYNAMICFILTERSCONTRATO_ANO1";
         tblTablemergeddynamicfilters1_Internalname = "TABLEMERGEDDYNAMICFILTERS1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         cmbavDynamicfiltersoperator2_Internalname = "vDYNAMICFILTERSOPERATOR2";
         edtavContrato_numero2_Internalname = "vCONTRATO_NUMERO2";
         edtavContrato_numeroata2_Internalname = "vCONTRATO_NUMEROATA2";
         edtavContrato_prepostonom2_Internalname = "vCONTRATO_PREPOSTONOM2";
         edtavContratada_pessoanom2_Internalname = "vCONTRATADA_PESSOANOM2";
         edtavContrato_ano2_Internalname = "vCONTRATO_ANO2";
         lblDynamicfilterscontrato_ano_rangemiddletext2_Internalname = "DYNAMICFILTERSCONTRATO_ANO_RANGEMIDDLETEXT2";
         edtavContrato_ano_to2_Internalname = "vCONTRATO_ANO_TO2";
         tblTablemergeddynamicfilterscontrato_ano2_Internalname = "TABLEMERGEDDYNAMICFILTERSCONTRATO_ANO2";
         tblTablemergeddynamicfilters2_Internalname = "TABLEMERGEDDYNAMICFILTERS2";
         imgAdddynamicfilters2_Internalname = "ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = "DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = "vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = "DYNAMICFILTERSMIDDLE3";
         cmbavDynamicfiltersoperator3_Internalname = "vDYNAMICFILTERSOPERATOR3";
         edtavContrato_numero3_Internalname = "vCONTRATO_NUMERO3";
         edtavContrato_numeroata3_Internalname = "vCONTRATO_NUMEROATA3";
         edtavContrato_prepostonom3_Internalname = "vCONTRATO_PREPOSTONOM3";
         edtavContratada_pessoanom3_Internalname = "vCONTRATADA_PESSOANOM3";
         edtavContrato_ano3_Internalname = "vCONTRATO_ANO3";
         lblDynamicfilterscontrato_ano_rangemiddletext3_Internalname = "DYNAMICFILTERSCONTRATO_ANO_RANGEMIDDLETEXT3";
         edtavContrato_ano_to3_Internalname = "vCONTRATO_ANO_TO3";
         tblTablemergeddynamicfilterscontrato_ano3_Internalname = "TABLEMERGEDDYNAMICFILTERSCONTRATO_ANO3";
         tblTablemergeddynamicfilters3_Internalname = "TABLEMERGEDDYNAMICFILTERS3";
         imgRemovedynamicfilters3_Internalname = "REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         tblUnnamedtable1_Internalname = "UNNAMEDTABLE1";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTableheader_Internalname = "TABLEHEADER";
         edtavUpdate_Internalname = "vUPDATE";
         edtavDelete_Internalname = "vDELETE";
         edtavDisplay_Internalname = "vDISPLAY";
         edtContrato_Codigo_Internalname = "CONTRATO_CODIGO";
         edtContratada_PessoaNom_Internalname = "CONTRATADA_PESSOANOM";
         edtContrato_Numero_Internalname = "CONTRATO_NUMERO";
         edtContrato_NumeroAta_Internalname = "CONTRATO_NUMEROATA";
         edtContrato_Ano_Internalname = "CONTRATO_ANO";
         edtContrato_DataTermino_Internalname = "CONTRATO_DATATERMINO";
         edtContrato_ValorUndCntAtual_Internalname = "CONTRATO_VALORUNDCNTATUAL";
         edtavSaldocontrato_saldo_Internalname = "vSALDOCONTRATO_SALDO";
         edtContratada_PessoaCNPJ_Internalname = "CONTRATADA_PESSOACNPJ";
         chkContrato_Ativo_Internalname = "CONTRATO_ATIVO";
         edtavAssociargestor_Internalname = "vASSOCIARGESTOR";
         edtavAssociarauxiliar_Internalname = "vASSOCIARAUXILIAR";
         edtavAssociarsistema_Internalname = "vASSOCIARSISTEMA";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = "vDYNAMICFILTERSENABLED3";
         edtavTfcontrato_valorundcntatual_Internalname = "vTFCONTRATO_VALORUNDCNTATUAL";
         edtavTfcontrato_valorundcntatual_to_Internalname = "vTFCONTRATO_VALORUNDCNTATUAL_TO";
         edtavTfcontrato_ativo_sel_Internalname = "vTFCONTRATO_ATIVO_SEL";
         Ddo_contrato_valorundcntatual_Internalname = "DDO_CONTRATO_VALORUNDCNTATUAL";
         edtavDdo_contrato_valorundcntatualtitlecontrolidtoreplace_Internalname = "vDDO_CONTRATO_VALORUNDCNTATUALTITLECONTROLIDTOREPLACE";
         Ddo_contrato_ativo_Internalname = "DDO_CONTRATO_ATIVO";
         edtavDdo_contrato_ativotitlecontrolidtoreplace_Internalname = "vDDO_CONTRATO_ATIVOTITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtavAssociarsistema_Jsonclick = "";
         edtavAssociarsistema_Visible = -1;
         edtavAssociarsistema_Enabled = 1;
         edtavAssociarauxiliar_Jsonclick = "";
         edtavAssociarauxiliar_Visible = -1;
         edtavAssociarauxiliar_Enabled = 1;
         edtavAssociargestor_Jsonclick = "";
         edtavAssociargestor_Visible = -1;
         edtavAssociargestor_Enabled = 1;
         edtContratada_PessoaCNPJ_Jsonclick = "";
         edtavSaldocontrato_saldo_Jsonclick = "";
         edtavSaldocontrato_saldo_Visible = -1;
         edtContrato_ValorUndCntAtual_Jsonclick = "";
         edtContrato_DataTermino_Jsonclick = "";
         edtContrato_Ano_Jsonclick = "";
         edtContrato_NumeroAta_Jsonclick = "";
         edtContrato_Numero_Jsonclick = "";
         edtContratada_PessoaNom_Jsonclick = "";
         edtContrato_Codigo_Jsonclick = "";
         edtavContrato_ano_to1_Jsonclick = "";
         edtavContrato_ano1_Jsonclick = "";
         edtavContratada_pessoanom1_Jsonclick = "";
         edtavContrato_prepostonom1_Jsonclick = "";
         edtavContrato_numeroata1_Jsonclick = "";
         edtavContrato_numero1_Jsonclick = "";
         cmbavDynamicfiltersoperator1_Jsonclick = "";
         edtavContrato_ano_to2_Jsonclick = "";
         edtavContrato_ano2_Jsonclick = "";
         edtavContratada_pessoanom2_Jsonclick = "";
         edtavContrato_prepostonom2_Jsonclick = "";
         edtavContrato_numeroata2_Jsonclick = "";
         edtavContrato_numero2_Jsonclick = "";
         cmbavDynamicfiltersoperator2_Jsonclick = "";
         edtavContrato_ano_to3_Jsonclick = "";
         edtavContrato_ano3_Jsonclick = "";
         edtavContratada_pessoanom3_Jsonclick = "";
         edtavContrato_prepostonom3_Jsonclick = "";
         edtavContrato_numeroata3_Jsonclick = "";
         edtavContrato_numero3_Jsonclick = "";
         cmbavDynamicfiltersoperator3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         cmbavDynamicfiltersselector1_Jsonclick = "";
         dynavContrato_areatrabalhocod_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavAssociarsistema_Tooltiptext = "Associar os sistemas deste contrato";
         edtavAssociarauxiliar_Tooltiptext = "Associar os auxiliares deste contrato";
         edtavAssociargestor_Tooltiptext = "Associar os gestores deste contrato";
         edtContratada_PessoaCNPJ_Forecolor = (int)(0xFFFFFF);
         edtavSaldocontrato_saldo_Enabled = 1;
         edtavSaldocontrato_saldo_Forecolor = (int)(0xFFFFFF);
         edtContrato_ValorUndCntAtual_Forecolor = (int)(0xFFFFFF);
         edtContrato_DataTermino_Forecolor = (int)(0xFFFFFF);
         edtContrato_Ano_Forecolor = (int)(0xFFFFFF);
         edtContrato_NumeroAta_Link = "";
         edtContrato_NumeroAta_Forecolor = (int)(0xFFFFFF);
         edtContrato_Numero_Forecolor = (int)(0xFFFFFF);
         edtContratada_PessoaNom_Link = "";
         edtContratada_PessoaNom_Forecolor = (int)(0xFFFFFF);
         edtavDisplay_Tooltiptext = "Cl�sulas, Servi�os, Garant�as, Anexos, Dados Certame, Termo Aditivo, Ocorr�ncias, Obriga��es";
         edtavDisplay_Link = "";
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = "";
         edtavDelete_Enabled = 1;
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = "";
         edtavUpdate_Enabled = 1;
         chkContrato_Ativo_Titleformat = 0;
         edtContratada_PessoaCNPJ_Titleformat = 0;
         edtContrato_ValorUndCntAtual_Titleformat = 0;
         edtContrato_Ano_Titleformat = 0;
         edtContrato_NumeroAta_Titleformat = 0;
         edtContrato_Numero_Titleformat = 0;
         edtContratada_PessoaNom_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         cmbavDynamicfiltersoperator3.Visible = 1;
         tblTablemergeddynamicfilterscontrato_ano3_Visible = 1;
         edtavContratada_pessoanom3_Visible = 1;
         edtavContrato_prepostonom3_Visible = 1;
         edtavContrato_numeroata3_Visible = 1;
         edtavContrato_numero3_Visible = 1;
         cmbavDynamicfiltersoperator2.Visible = 1;
         tblTablemergeddynamicfilterscontrato_ano2_Visible = 1;
         edtavContratada_pessoanom2_Visible = 1;
         edtavContrato_prepostonom2_Visible = 1;
         edtavContrato_numeroata2_Visible = 1;
         edtavContrato_numero2_Visible = 1;
         cmbavDynamicfiltersoperator1.Visible = 1;
         tblTablemergeddynamicfilterscontrato_ano1_Visible = 1;
         edtavContratada_pessoanom1_Visible = 1;
         edtavContrato_prepostonom1_Visible = 1;
         edtavContrato_numeroata1_Visible = 1;
         edtavContrato_numero1_Visible = 1;
         edtavAssociarsistema_Title = "";
         edtavAssociarauxiliar_Title = "";
         edtavAssociargestor_Title = "";
         chkContrato_Ativo.Title.Text = "Ativo";
         edtContratada_PessoaCNPJ_Title = "CNPJ";
         edtContrato_ValorUndCntAtual_Title = "Valor Und.";
         edtContrato_Ano_Title = "Ano";
         edtContrato_NumeroAta_Title = "N� da Ata";
         edtContrato_Numero_Title = "N� Contrato";
         edtContratada_PessoaNom_Title = "Contratada";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         chkContrato_Ativo.Caption = "";
         edtavDdo_contrato_ativotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contrato_valorundcntatualtitlecontrolidtoreplace_Visible = 1;
         edtavTfcontrato_ativo_sel_Jsonclick = "";
         edtavTfcontrato_ativo_sel_Visible = 1;
         edtavTfcontrato_valorundcntatual_to_Jsonclick = "";
         edtavTfcontrato_valorundcntatual_to_Visible = 1;
         edtavTfcontrato_valorundcntatual_Jsonclick = "";
         edtavTfcontrato_valorundcntatual_Visible = 1;
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         Ddo_contrato_ativo_Searchbuttontext = "Pesquisar";
         Ddo_contrato_ativo_Cleanfilter = "Limpar pesquisa";
         Ddo_contrato_ativo_Sortdsc = "Ordenar de Z � A";
         Ddo_contrato_ativo_Sortasc = "Ordenar de A � Z";
         Ddo_contrato_ativo_Datalistfixedvalues = "1:Marcado,2:Desmarcado";
         Ddo_contrato_ativo_Datalisttype = "FixedValues";
         Ddo_contrato_ativo_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contrato_ativo_Includefilter = Convert.ToBoolean( 0);
         Ddo_contrato_ativo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contrato_ativo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contrato_ativo_Titlecontrolidtoreplace = "";
         Ddo_contrato_ativo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contrato_ativo_Cls = "ColumnSettings";
         Ddo_contrato_ativo_Tooltip = "Op��es";
         Ddo_contrato_ativo_Caption = "";
         Ddo_contrato_valorundcntatual_Searchbuttontext = "Pesquisar";
         Ddo_contrato_valorundcntatual_Rangefilterto = "At�";
         Ddo_contrato_valorundcntatual_Rangefilterfrom = "Desde";
         Ddo_contrato_valorundcntatual_Cleanfilter = "Limpar pesquisa";
         Ddo_contrato_valorundcntatual_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contrato_valorundcntatual_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contrato_valorundcntatual_Filtertype = "Numeric";
         Ddo_contrato_valorundcntatual_Includefilter = Convert.ToBoolean( -1);
         Ddo_contrato_valorundcntatual_Includesortdsc = Convert.ToBoolean( 0);
         Ddo_contrato_valorundcntatual_Includesortasc = Convert.ToBoolean( 0);
         Ddo_contrato_valorundcntatual_Titlecontrolidtoreplace = "";
         Ddo_contrato_valorundcntatual_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contrato_valorundcntatual_Cls = "ColumnSettings";
         Ddo_contrato_valorundcntatual_Tooltip = "Op��es";
         Ddo_contrato_valorundcntatual_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Work With Contrato";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A92Contrato_Ativo',fld:'CONTRATO_ATIVO',pic:'',hsh:true,nv:false},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV132ddo_Contrato_ValorUndCntAtualTitleControlIdToReplace',fld:'vDDO_CONTRATO_VALORUNDCNTATUALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV122ddo_Contrato_AtivoTitleControlIdToReplace',fld:'vDDO_CONTRATO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50Contrato_AreaTrabalhoCod',fld:'vCONTRATO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV40Contrato_Numero1',fld:'vCONTRATO_NUMERO1',pic:'',nv:''},{av:'AV51Contrato_NumeroAta1',fld:'vCONTRATO_NUMEROATA1',pic:'',nv:''},{av:'AV133Contrato_PrepostoNom1',fld:'vCONTRATO_PREPOSTONOM1',pic:'@!',nv:''},{av:'AV18Contratada_PessoaNom1',fld:'vCONTRATADA_PESSOANOM1',pic:'@!',nv:''},{av:'AV43Contrato_Ano1',fld:'vCONTRATO_ANO1',pic:'ZZZ9',nv:0},{av:'AV44Contrato_Ano_To1',fld:'vCONTRATO_ANO_TO1',pic:'ZZZ9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV41Contrato_Numero2',fld:'vCONTRATO_NUMERO2',pic:'',nv:''},{av:'AV52Contrato_NumeroAta2',fld:'vCONTRATO_NUMEROATA2',pic:'',nv:''},{av:'AV134Contrato_PrepostoNom2',fld:'vCONTRATO_PREPOSTONOM2',pic:'@!',nv:''},{av:'AV23Contratada_PessoaNom2',fld:'vCONTRATADA_PESSOANOM2',pic:'@!',nv:''},{av:'AV45Contrato_Ano2',fld:'vCONTRATO_ANO2',pic:'ZZZ9',nv:0},{av:'AV46Contrato_Ano_To2',fld:'vCONTRATO_ANO_TO2',pic:'ZZZ9',nv:0},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV42Contrato_Numero3',fld:'vCONTRATO_NUMERO3',pic:'',nv:''},{av:'AV53Contrato_NumeroAta3',fld:'vCONTRATO_NUMEROATA3',pic:'',nv:''},{av:'AV135Contrato_PrepostoNom3',fld:'vCONTRATO_PREPOSTONOM3',pic:'@!',nv:''},{av:'AV28Contratada_PessoaNom3',fld:'vCONTRATADA_PESSOANOM3',pic:'@!',nv:''},{av:'AV47Contrato_Ano3',fld:'vCONTRATO_ANO3',pic:'ZZZ9',nv:0},{av:'AV48Contrato_Ano_To3',fld:'vCONTRATO_ANO_TO3',pic:'ZZZ9',nv:0},{av:'AV130TFContrato_ValorUndCntAtual',fld:'vTFCONTRATO_VALORUNDCNTATUAL',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV131TFContrato_ValorUndCntAtual_To',fld:'vTFCONTRATO_VALORUNDCNTATUAL_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV121TFContrato_Ativo_Sel',fld:'vTFCONTRATO_ATIVO_SEL',pic:'9',nv:0},{av:'AV189Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV129Contrato_ValorUndCntAtualTitleFilterData',fld:'vCONTRATO_VALORUNDCNTATUALTITLEFILTERDATA',pic:'',nv:null},{av:'AV120Contrato_AtivoTitleFilterData',fld:'vCONTRATO_ATIVOTITLEFILTERDATA',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'edtContratada_PessoaNom_Titleformat',ctrl:'CONTRATADA_PESSOANOM',prop:'Titleformat'},{av:'edtContratada_PessoaNom_Title',ctrl:'CONTRATADA_PESSOANOM',prop:'Title'},{av:'edtContrato_Numero_Titleformat',ctrl:'CONTRATO_NUMERO',prop:'Titleformat'},{av:'edtContrato_Numero_Title',ctrl:'CONTRATO_NUMERO',prop:'Title'},{av:'edtContrato_NumeroAta_Titleformat',ctrl:'CONTRATO_NUMEROATA',prop:'Titleformat'},{av:'edtContrato_NumeroAta_Title',ctrl:'CONTRATO_NUMEROATA',prop:'Title'},{av:'edtContrato_Ano_Titleformat',ctrl:'CONTRATO_ANO',prop:'Titleformat'},{av:'edtContrato_Ano_Title',ctrl:'CONTRATO_ANO',prop:'Title'},{av:'edtContrato_ValorUndCntAtual_Titleformat',ctrl:'CONTRATO_VALORUNDCNTATUAL',prop:'Titleformat'},{av:'edtContrato_ValorUndCntAtual_Title',ctrl:'CONTRATO_VALORUNDCNTATUAL',prop:'Title'},{av:'edtContratada_PessoaCNPJ_Titleformat',ctrl:'CONTRATADA_PESSOACNPJ',prop:'Titleformat'},{av:'edtContratada_PessoaCNPJ_Title',ctrl:'CONTRATADA_PESSOACNPJ',prop:'Title'},{av:'chkContrato_Ativo_Titleformat',ctrl:'CONTRATO_ATIVO',prop:'Titleformat'},{av:'chkContrato_Ativo.Title.Text',ctrl:'CONTRATO_ATIVO',prop:'Title'},{av:'AV117GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV118GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'AV50Contrato_AreaTrabalhoCod',fld:'vCONTRATO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'edtavAssociargestor_Title',ctrl:'vASSOCIARGESTOR',prop:'Title'},{av:'edtavAssociarauxiliar_Title',ctrl:'vASSOCIARAUXILIAR',prop:'Title'},{av:'edtavAssociarsistema_Title',ctrl:'vASSOCIARSISTEMA',prop:'Title'},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11652',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV50Contrato_AreaTrabalhoCod',fld:'vCONTRATO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV40Contrato_Numero1',fld:'vCONTRATO_NUMERO1',pic:'',nv:''},{av:'AV51Contrato_NumeroAta1',fld:'vCONTRATO_NUMEROATA1',pic:'',nv:''},{av:'AV133Contrato_PrepostoNom1',fld:'vCONTRATO_PREPOSTONOM1',pic:'@!',nv:''},{av:'AV18Contratada_PessoaNom1',fld:'vCONTRATADA_PESSOANOM1',pic:'@!',nv:''},{av:'AV43Contrato_Ano1',fld:'vCONTRATO_ANO1',pic:'ZZZ9',nv:0},{av:'AV44Contrato_Ano_To1',fld:'vCONTRATO_ANO_TO1',pic:'ZZZ9',nv:0},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV41Contrato_Numero2',fld:'vCONTRATO_NUMERO2',pic:'',nv:''},{av:'AV52Contrato_NumeroAta2',fld:'vCONTRATO_NUMEROATA2',pic:'',nv:''},{av:'AV134Contrato_PrepostoNom2',fld:'vCONTRATO_PREPOSTONOM2',pic:'@!',nv:''},{av:'AV23Contratada_PessoaNom2',fld:'vCONTRATADA_PESSOANOM2',pic:'@!',nv:''},{av:'AV45Contrato_Ano2',fld:'vCONTRATO_ANO2',pic:'ZZZ9',nv:0},{av:'AV46Contrato_Ano_To2',fld:'vCONTRATO_ANO_TO2',pic:'ZZZ9',nv:0},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV42Contrato_Numero3',fld:'vCONTRATO_NUMERO3',pic:'',nv:''},{av:'AV53Contrato_NumeroAta3',fld:'vCONTRATO_NUMEROATA3',pic:'',nv:''},{av:'AV135Contrato_PrepostoNom3',fld:'vCONTRATO_PREPOSTONOM3',pic:'@!',nv:''},{av:'AV28Contratada_PessoaNom3',fld:'vCONTRATADA_PESSOANOM3',pic:'@!',nv:''},{av:'AV47Contrato_Ano3',fld:'vCONTRATO_ANO3',pic:'ZZZ9',nv:0},{av:'AV48Contrato_Ano_To3',fld:'vCONTRATO_ANO_TO3',pic:'ZZZ9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV121TFContrato_Ativo_Sel',fld:'vTFCONTRATO_ATIVO_SEL',pic:'9',nv:0},{av:'AV132ddo_Contrato_ValorUndCntAtualTitleControlIdToReplace',fld:'vDDO_CONTRATO_VALORUNDCNTATUALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV122ddo_Contrato_AtivoTitleControlIdToReplace',fld:'vDDO_CONTRATO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV130TFContrato_ValorUndCntAtual',fld:'vTFCONTRATO_VALORUNDCNTATUAL',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV131TFContrato_ValorUndCntAtual_To',fld:'vTFCONTRATO_VALORUNDCNTATUAL_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV189Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A92Contrato_Ativo',fld:'CONTRATO_ATIVO',pic:'',hsh:true,nv:false},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_CONTRATO_VALORUNDCNTATUAL.ONOPTIONCLICKED","{handler:'E12652',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV50Contrato_AreaTrabalhoCod',fld:'vCONTRATO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV40Contrato_Numero1',fld:'vCONTRATO_NUMERO1',pic:'',nv:''},{av:'AV51Contrato_NumeroAta1',fld:'vCONTRATO_NUMEROATA1',pic:'',nv:''},{av:'AV133Contrato_PrepostoNom1',fld:'vCONTRATO_PREPOSTONOM1',pic:'@!',nv:''},{av:'AV18Contratada_PessoaNom1',fld:'vCONTRATADA_PESSOANOM1',pic:'@!',nv:''},{av:'AV43Contrato_Ano1',fld:'vCONTRATO_ANO1',pic:'ZZZ9',nv:0},{av:'AV44Contrato_Ano_To1',fld:'vCONTRATO_ANO_TO1',pic:'ZZZ9',nv:0},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV41Contrato_Numero2',fld:'vCONTRATO_NUMERO2',pic:'',nv:''},{av:'AV52Contrato_NumeroAta2',fld:'vCONTRATO_NUMEROATA2',pic:'',nv:''},{av:'AV134Contrato_PrepostoNom2',fld:'vCONTRATO_PREPOSTONOM2',pic:'@!',nv:''},{av:'AV23Contratada_PessoaNom2',fld:'vCONTRATADA_PESSOANOM2',pic:'@!',nv:''},{av:'AV45Contrato_Ano2',fld:'vCONTRATO_ANO2',pic:'ZZZ9',nv:0},{av:'AV46Contrato_Ano_To2',fld:'vCONTRATO_ANO_TO2',pic:'ZZZ9',nv:0},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV42Contrato_Numero3',fld:'vCONTRATO_NUMERO3',pic:'',nv:''},{av:'AV53Contrato_NumeroAta3',fld:'vCONTRATO_NUMEROATA3',pic:'',nv:''},{av:'AV135Contrato_PrepostoNom3',fld:'vCONTRATO_PREPOSTONOM3',pic:'@!',nv:''},{av:'AV28Contratada_PessoaNom3',fld:'vCONTRATADA_PESSOANOM3',pic:'@!',nv:''},{av:'AV47Contrato_Ano3',fld:'vCONTRATO_ANO3',pic:'ZZZ9',nv:0},{av:'AV48Contrato_Ano_To3',fld:'vCONTRATO_ANO_TO3',pic:'ZZZ9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV121TFContrato_Ativo_Sel',fld:'vTFCONTRATO_ATIVO_SEL',pic:'9',nv:0},{av:'AV132ddo_Contrato_ValorUndCntAtualTitleControlIdToReplace',fld:'vDDO_CONTRATO_VALORUNDCNTATUALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV122ddo_Contrato_AtivoTitleControlIdToReplace',fld:'vDDO_CONTRATO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV130TFContrato_ValorUndCntAtual',fld:'vTFCONTRATO_VALORUNDCNTATUAL',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV131TFContrato_ValorUndCntAtual_To',fld:'vTFCONTRATO_VALORUNDCNTATUAL_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV189Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A92Contrato_Ativo',fld:'CONTRATO_ATIVO',pic:'',hsh:true,nv:false},{av:'Ddo_contrato_valorundcntatual_Activeeventkey',ctrl:'DDO_CONTRATO_VALORUNDCNTATUAL',prop:'ActiveEventKey'},{av:'Ddo_contrato_valorundcntatual_Filteredtext_get',ctrl:'DDO_CONTRATO_VALORUNDCNTATUAL',prop:'FilteredText_get'},{av:'Ddo_contrato_valorundcntatual_Filteredtextto_get',ctrl:'DDO_CONTRATO_VALORUNDCNTATUAL',prop:'FilteredTextTo_get'}],oparms:[{av:'AV130TFContrato_ValorUndCntAtual',fld:'vTFCONTRATO_VALORUNDCNTATUAL',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV131TFContrato_ValorUndCntAtual_To',fld:'vTFCONTRATO_VALORUNDCNTATUAL_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0}]}");
         setEventMetadata("DDO_CONTRATO_ATIVO.ONOPTIONCLICKED","{handler:'E13652',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV50Contrato_AreaTrabalhoCod',fld:'vCONTRATO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV40Contrato_Numero1',fld:'vCONTRATO_NUMERO1',pic:'',nv:''},{av:'AV51Contrato_NumeroAta1',fld:'vCONTRATO_NUMEROATA1',pic:'',nv:''},{av:'AV133Contrato_PrepostoNom1',fld:'vCONTRATO_PREPOSTONOM1',pic:'@!',nv:''},{av:'AV18Contratada_PessoaNom1',fld:'vCONTRATADA_PESSOANOM1',pic:'@!',nv:''},{av:'AV43Contrato_Ano1',fld:'vCONTRATO_ANO1',pic:'ZZZ9',nv:0},{av:'AV44Contrato_Ano_To1',fld:'vCONTRATO_ANO_TO1',pic:'ZZZ9',nv:0},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV41Contrato_Numero2',fld:'vCONTRATO_NUMERO2',pic:'',nv:''},{av:'AV52Contrato_NumeroAta2',fld:'vCONTRATO_NUMEROATA2',pic:'',nv:''},{av:'AV134Contrato_PrepostoNom2',fld:'vCONTRATO_PREPOSTONOM2',pic:'@!',nv:''},{av:'AV23Contratada_PessoaNom2',fld:'vCONTRATADA_PESSOANOM2',pic:'@!',nv:''},{av:'AV45Contrato_Ano2',fld:'vCONTRATO_ANO2',pic:'ZZZ9',nv:0},{av:'AV46Contrato_Ano_To2',fld:'vCONTRATO_ANO_TO2',pic:'ZZZ9',nv:0},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV42Contrato_Numero3',fld:'vCONTRATO_NUMERO3',pic:'',nv:''},{av:'AV53Contrato_NumeroAta3',fld:'vCONTRATO_NUMEROATA3',pic:'',nv:''},{av:'AV135Contrato_PrepostoNom3',fld:'vCONTRATO_PREPOSTONOM3',pic:'@!',nv:''},{av:'AV28Contratada_PessoaNom3',fld:'vCONTRATADA_PESSOANOM3',pic:'@!',nv:''},{av:'AV47Contrato_Ano3',fld:'vCONTRATO_ANO3',pic:'ZZZ9',nv:0},{av:'AV48Contrato_Ano_To3',fld:'vCONTRATO_ANO_TO3',pic:'ZZZ9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV121TFContrato_Ativo_Sel',fld:'vTFCONTRATO_ATIVO_SEL',pic:'9',nv:0},{av:'AV132ddo_Contrato_ValorUndCntAtualTitleControlIdToReplace',fld:'vDDO_CONTRATO_VALORUNDCNTATUALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV122ddo_Contrato_AtivoTitleControlIdToReplace',fld:'vDDO_CONTRATO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV130TFContrato_ValorUndCntAtual',fld:'vTFCONTRATO_VALORUNDCNTATUAL',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV131TFContrato_ValorUndCntAtual_To',fld:'vTFCONTRATO_VALORUNDCNTATUAL_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV189Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A92Contrato_Ativo',fld:'CONTRATO_ATIVO',pic:'',hsh:true,nv:false},{av:'Ddo_contrato_ativo_Activeeventkey',ctrl:'DDO_CONTRATO_ATIVO',prop:'ActiveEventKey'},{av:'Ddo_contrato_ativo_Selectedvalue_get',ctrl:'DDO_CONTRATO_ATIVO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contrato_ativo_Sortedstatus',ctrl:'DDO_CONTRATO_ATIVO',prop:'SortedStatus'},{av:'AV121TFContrato_Ativo_Sel',fld:'vTFCONTRATO_ATIVO_SEL',pic:'9',nv:0}]}");
         setEventMetadata("GRID.LOAD","{handler:'E26652',iparms:[{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A92Contrato_Ativo',fld:'CONTRATO_ATIVO',pic:'',hsh:true,nv:false}],oparms:[{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'edtavUpdate_Link',ctrl:'vUPDATE',prop:'Link'},{av:'AV31Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Enabled',ctrl:'vUPDATE',prop:'Enabled'},{av:'edtavDelete_Tooltiptext',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'edtavDelete_Link',ctrl:'vDELETE',prop:'Link'},{av:'AV32Delete',fld:'vDELETE',pic:'',nv:''},{av:'edtavDelete_Enabled',ctrl:'vDELETE',prop:'Enabled'},{av:'AV49Display',fld:'vDISPLAY',pic:'',nv:''},{av:'edtavDisplay_Tooltiptext',ctrl:'vDISPLAY',prop:'Tooltiptext'},{av:'edtavDisplay_Link',ctrl:'vDISPLAY',prop:'Link'},{av:'AV75AssociarGestor',fld:'vASSOCIARGESTOR',pic:'',nv:''},{av:'edtavAssociargestor_Tooltiptext',ctrl:'vASSOCIARGESTOR',prop:'Tooltiptext'},{av:'AV119AssociarAuxiliar',fld:'vASSOCIARAUXILIAR',pic:'',nv:''},{av:'edtavAssociarauxiliar_Tooltiptext',ctrl:'vASSOCIARAUXILIAR',prop:'Tooltiptext'},{av:'AV80AssociarSistema',fld:'vASSOCIARSISTEMA',pic:'',nv:''},{av:'edtavAssociarsistema_Tooltiptext',ctrl:'vASSOCIARSISTEMA',prop:'Tooltiptext'},{av:'edtContratada_PessoaNom_Link',ctrl:'CONTRATADA_PESSOANOM',prop:'Link'},{av:'edtContrato_NumeroAta_Link',ctrl:'CONTRATO_NUMEROATA',prop:'Link'},{av:'edtContratada_PessoaNom_Forecolor',ctrl:'CONTRATADA_PESSOANOM',prop:'Forecolor'},{av:'edtContrato_Numero_Forecolor',ctrl:'CONTRATO_NUMERO',prop:'Forecolor'},{av:'edtContrato_NumeroAta_Forecolor',ctrl:'CONTRATO_NUMEROATA',prop:'Forecolor'},{av:'edtContrato_Ano_Forecolor',ctrl:'CONTRATO_ANO',prop:'Forecolor'},{av:'edtContrato_DataTermino_Forecolor',ctrl:'CONTRATO_DATATERMINO',prop:'Forecolor'},{av:'edtContrato_ValorUndCntAtual_Forecolor',ctrl:'CONTRATO_VALORUNDCNTATUAL',prop:'Forecolor'},{av:'edtavSaldocontrato_saldo_Forecolor',ctrl:'vSALDOCONTRATO_SALDO',prop:'Forecolor'},{av:'edtContratada_PessoaCNPJ_Forecolor',ctrl:'CONTRATADA_PESSOACNPJ',prop:'Forecolor'},{av:'AV78SaldoContrato_Saldo',fld:'vSALDOCONTRATO_SALDO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E14652',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV50Contrato_AreaTrabalhoCod',fld:'vCONTRATO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV40Contrato_Numero1',fld:'vCONTRATO_NUMERO1',pic:'',nv:''},{av:'AV51Contrato_NumeroAta1',fld:'vCONTRATO_NUMEROATA1',pic:'',nv:''},{av:'AV133Contrato_PrepostoNom1',fld:'vCONTRATO_PREPOSTONOM1',pic:'@!',nv:''},{av:'AV18Contratada_PessoaNom1',fld:'vCONTRATADA_PESSOANOM1',pic:'@!',nv:''},{av:'AV43Contrato_Ano1',fld:'vCONTRATO_ANO1',pic:'ZZZ9',nv:0},{av:'AV44Contrato_Ano_To1',fld:'vCONTRATO_ANO_TO1',pic:'ZZZ9',nv:0},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV41Contrato_Numero2',fld:'vCONTRATO_NUMERO2',pic:'',nv:''},{av:'AV52Contrato_NumeroAta2',fld:'vCONTRATO_NUMEROATA2',pic:'',nv:''},{av:'AV134Contrato_PrepostoNom2',fld:'vCONTRATO_PREPOSTONOM2',pic:'@!',nv:''},{av:'AV23Contratada_PessoaNom2',fld:'vCONTRATADA_PESSOANOM2',pic:'@!',nv:''},{av:'AV45Contrato_Ano2',fld:'vCONTRATO_ANO2',pic:'ZZZ9',nv:0},{av:'AV46Contrato_Ano_To2',fld:'vCONTRATO_ANO_TO2',pic:'ZZZ9',nv:0},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV42Contrato_Numero3',fld:'vCONTRATO_NUMERO3',pic:'',nv:''},{av:'AV53Contrato_NumeroAta3',fld:'vCONTRATO_NUMEROATA3',pic:'',nv:''},{av:'AV135Contrato_PrepostoNom3',fld:'vCONTRATO_PREPOSTONOM3',pic:'@!',nv:''},{av:'AV28Contratada_PessoaNom3',fld:'vCONTRATADA_PESSOANOM3',pic:'@!',nv:''},{av:'AV47Contrato_Ano3',fld:'vCONTRATO_ANO3',pic:'ZZZ9',nv:0},{av:'AV48Contrato_Ano_To3',fld:'vCONTRATO_ANO_TO3',pic:'ZZZ9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV121TFContrato_Ativo_Sel',fld:'vTFCONTRATO_ATIVO_SEL',pic:'9',nv:0},{av:'AV132ddo_Contrato_ValorUndCntAtualTitleControlIdToReplace',fld:'vDDO_CONTRATO_VALORUNDCNTATUALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV122ddo_Contrato_AtivoTitleControlIdToReplace',fld:'vDDO_CONTRATO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV130TFContrato_ValorUndCntAtual',fld:'vTFCONTRATO_VALORUNDCNTATUAL',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV131TFContrato_ValorUndCntAtual_To',fld:'vTFCONTRATO_VALORUNDCNTATUAL_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV189Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A92Contrato_Ativo',fld:'CONTRATO_ATIVO',pic:'',hsh:true,nv:false}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E19652',iparms:[],oparms:[{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E15652',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV50Contrato_AreaTrabalhoCod',fld:'vCONTRATO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV40Contrato_Numero1',fld:'vCONTRATO_NUMERO1',pic:'',nv:''},{av:'AV51Contrato_NumeroAta1',fld:'vCONTRATO_NUMEROATA1',pic:'',nv:''},{av:'AV133Contrato_PrepostoNom1',fld:'vCONTRATO_PREPOSTONOM1',pic:'@!',nv:''},{av:'AV18Contratada_PessoaNom1',fld:'vCONTRATADA_PESSOANOM1',pic:'@!',nv:''},{av:'AV43Contrato_Ano1',fld:'vCONTRATO_ANO1',pic:'ZZZ9',nv:0},{av:'AV44Contrato_Ano_To1',fld:'vCONTRATO_ANO_TO1',pic:'ZZZ9',nv:0},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV41Contrato_Numero2',fld:'vCONTRATO_NUMERO2',pic:'',nv:''},{av:'AV52Contrato_NumeroAta2',fld:'vCONTRATO_NUMEROATA2',pic:'',nv:''},{av:'AV134Contrato_PrepostoNom2',fld:'vCONTRATO_PREPOSTONOM2',pic:'@!',nv:''},{av:'AV23Contratada_PessoaNom2',fld:'vCONTRATADA_PESSOANOM2',pic:'@!',nv:''},{av:'AV45Contrato_Ano2',fld:'vCONTRATO_ANO2',pic:'ZZZ9',nv:0},{av:'AV46Contrato_Ano_To2',fld:'vCONTRATO_ANO_TO2',pic:'ZZZ9',nv:0},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV42Contrato_Numero3',fld:'vCONTRATO_NUMERO3',pic:'',nv:''},{av:'AV53Contrato_NumeroAta3',fld:'vCONTRATO_NUMEROATA3',pic:'',nv:''},{av:'AV135Contrato_PrepostoNom3',fld:'vCONTRATO_PREPOSTONOM3',pic:'@!',nv:''},{av:'AV28Contratada_PessoaNom3',fld:'vCONTRATADA_PESSOANOM3',pic:'@!',nv:''},{av:'AV47Contrato_Ano3',fld:'vCONTRATO_ANO3',pic:'ZZZ9',nv:0},{av:'AV48Contrato_Ano_To3',fld:'vCONTRATO_ANO_TO3',pic:'ZZZ9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV121TFContrato_Ativo_Sel',fld:'vTFCONTRATO_ATIVO_SEL',pic:'9',nv:0},{av:'AV132ddo_Contrato_ValorUndCntAtualTitleControlIdToReplace',fld:'vDDO_CONTRATO_VALORUNDCNTATUALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV122ddo_Contrato_AtivoTitleControlIdToReplace',fld:'vDDO_CONTRATO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV130TFContrato_ValorUndCntAtual',fld:'vTFCONTRATO_VALORUNDCNTATUAL',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV131TFContrato_ValorUndCntAtual_To',fld:'vTFCONTRATO_VALORUNDCNTATUAL_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV189Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A92Contrato_Ativo',fld:'CONTRATO_ATIVO',pic:'',hsh:true,nv:false}],oparms:[{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV41Contrato_Numero2',fld:'vCONTRATO_NUMERO2',pic:'',nv:''},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV42Contrato_Numero3',fld:'vCONTRATO_NUMERO3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'cmbavDynamicfiltersoperator3'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV40Contrato_Numero1',fld:'vCONTRATO_NUMERO1',pic:'',nv:''},{av:'AV51Contrato_NumeroAta1',fld:'vCONTRATO_NUMEROATA1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV133Contrato_PrepostoNom1',fld:'vCONTRATO_PREPOSTONOM1',pic:'@!',nv:''},{av:'AV18Contratada_PessoaNom1',fld:'vCONTRATADA_PESSOANOM1',pic:'@!',nv:''},{av:'AV43Contrato_Ano1',fld:'vCONTRATO_ANO1',pic:'ZZZ9',nv:0},{av:'AV44Contrato_Ano_To1',fld:'vCONTRATO_ANO_TO1',pic:'ZZZ9',nv:0},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV52Contrato_NumeroAta2',fld:'vCONTRATO_NUMEROATA2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV134Contrato_PrepostoNom2',fld:'vCONTRATO_PREPOSTONOM2',pic:'@!',nv:''},{av:'AV23Contratada_PessoaNom2',fld:'vCONTRATADA_PESSOANOM2',pic:'@!',nv:''},{av:'AV45Contrato_Ano2',fld:'vCONTRATO_ANO2',pic:'ZZZ9',nv:0},{av:'AV46Contrato_Ano_To2',fld:'vCONTRATO_ANO_TO2',pic:'ZZZ9',nv:0},{av:'AV53Contrato_NumeroAta3',fld:'vCONTRATO_NUMEROATA3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV135Contrato_PrepostoNom3',fld:'vCONTRATO_PREPOSTONOM3',pic:'@!',nv:''},{av:'AV28Contratada_PessoaNom3',fld:'vCONTRATADA_PESSOANOM3',pic:'@!',nv:''},{av:'AV47Contrato_Ano3',fld:'vCONTRATO_ANO3',pic:'ZZZ9',nv:0},{av:'AV48Contrato_Ano_To3',fld:'vCONTRATO_ANO_TO3',pic:'ZZZ9',nv:0},{av:'edtavContrato_numero2_Visible',ctrl:'vCONTRATO_NUMERO2',prop:'Visible'},{av:'edtavContrato_numeroata2_Visible',ctrl:'vCONTRATO_NUMEROATA2',prop:'Visible'},{av:'edtavContrato_prepostonom2_Visible',ctrl:'vCONTRATO_PREPOSTONOM2',prop:'Visible'},{av:'edtavContratada_pessoanom2_Visible',ctrl:'vCONTRATADA_PESSOANOM2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontrato_ano2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATO_ANO2',prop:'Visible'},{av:'edtavContrato_numero3_Visible',ctrl:'vCONTRATO_NUMERO3',prop:'Visible'},{av:'edtavContrato_numeroata3_Visible',ctrl:'vCONTRATO_NUMEROATA3',prop:'Visible'},{av:'edtavContrato_prepostonom3_Visible',ctrl:'vCONTRATO_PREPOSTONOM3',prop:'Visible'},{av:'edtavContratada_pessoanom3_Visible',ctrl:'vCONTRATADA_PESSOANOM3',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontrato_ano3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATO_ANO3',prop:'Visible'},{av:'edtavContrato_numero1_Visible',ctrl:'vCONTRATO_NUMERO1',prop:'Visible'},{av:'edtavContrato_numeroata1_Visible',ctrl:'vCONTRATO_NUMEROATA1',prop:'Visible'},{av:'edtavContrato_prepostonom1_Visible',ctrl:'vCONTRATO_PREPOSTONOM1',prop:'Visible'},{av:'edtavContratada_pessoanom1_Visible',ctrl:'vCONTRATADA_PESSOANOM1',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontrato_ano1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATO_ANO1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E20652',iparms:[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'edtavContrato_numero1_Visible',ctrl:'vCONTRATO_NUMERO1',prop:'Visible'},{av:'edtavContrato_numeroata1_Visible',ctrl:'vCONTRATO_NUMEROATA1',prop:'Visible'},{av:'edtavContrato_prepostonom1_Visible',ctrl:'vCONTRATO_PREPOSTONOM1',prop:'Visible'},{av:'edtavContratada_pessoanom1_Visible',ctrl:'vCONTRATADA_PESSOANOM1',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontrato_ano1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATO_ANO1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E21652',iparms:[],oparms:[{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E16652',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV50Contrato_AreaTrabalhoCod',fld:'vCONTRATO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV40Contrato_Numero1',fld:'vCONTRATO_NUMERO1',pic:'',nv:''},{av:'AV51Contrato_NumeroAta1',fld:'vCONTRATO_NUMEROATA1',pic:'',nv:''},{av:'AV133Contrato_PrepostoNom1',fld:'vCONTRATO_PREPOSTONOM1',pic:'@!',nv:''},{av:'AV18Contratada_PessoaNom1',fld:'vCONTRATADA_PESSOANOM1',pic:'@!',nv:''},{av:'AV43Contrato_Ano1',fld:'vCONTRATO_ANO1',pic:'ZZZ9',nv:0},{av:'AV44Contrato_Ano_To1',fld:'vCONTRATO_ANO_TO1',pic:'ZZZ9',nv:0},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV41Contrato_Numero2',fld:'vCONTRATO_NUMERO2',pic:'',nv:''},{av:'AV52Contrato_NumeroAta2',fld:'vCONTRATO_NUMEROATA2',pic:'',nv:''},{av:'AV134Contrato_PrepostoNom2',fld:'vCONTRATO_PREPOSTONOM2',pic:'@!',nv:''},{av:'AV23Contratada_PessoaNom2',fld:'vCONTRATADA_PESSOANOM2',pic:'@!',nv:''},{av:'AV45Contrato_Ano2',fld:'vCONTRATO_ANO2',pic:'ZZZ9',nv:0},{av:'AV46Contrato_Ano_To2',fld:'vCONTRATO_ANO_TO2',pic:'ZZZ9',nv:0},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV42Contrato_Numero3',fld:'vCONTRATO_NUMERO3',pic:'',nv:''},{av:'AV53Contrato_NumeroAta3',fld:'vCONTRATO_NUMEROATA3',pic:'',nv:''},{av:'AV135Contrato_PrepostoNom3',fld:'vCONTRATO_PREPOSTONOM3',pic:'@!',nv:''},{av:'AV28Contratada_PessoaNom3',fld:'vCONTRATADA_PESSOANOM3',pic:'@!',nv:''},{av:'AV47Contrato_Ano3',fld:'vCONTRATO_ANO3',pic:'ZZZ9',nv:0},{av:'AV48Contrato_Ano_To3',fld:'vCONTRATO_ANO_TO3',pic:'ZZZ9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV121TFContrato_Ativo_Sel',fld:'vTFCONTRATO_ATIVO_SEL',pic:'9',nv:0},{av:'AV132ddo_Contrato_ValorUndCntAtualTitleControlIdToReplace',fld:'vDDO_CONTRATO_VALORUNDCNTATUALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV122ddo_Contrato_AtivoTitleControlIdToReplace',fld:'vDDO_CONTRATO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV130TFContrato_ValorUndCntAtual',fld:'vTFCONTRATO_VALORUNDCNTATUAL',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV131TFContrato_ValorUndCntAtual_To',fld:'vTFCONTRATO_VALORUNDCNTATUAL_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV189Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A92Contrato_Ativo',fld:'CONTRATO_ATIVO',pic:'',hsh:true,nv:false}],oparms:[{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV41Contrato_Numero2',fld:'vCONTRATO_NUMERO2',pic:'',nv:''},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV42Contrato_Numero3',fld:'vCONTRATO_NUMERO3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'cmbavDynamicfiltersoperator3'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV40Contrato_Numero1',fld:'vCONTRATO_NUMERO1',pic:'',nv:''},{av:'AV51Contrato_NumeroAta1',fld:'vCONTRATO_NUMEROATA1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV133Contrato_PrepostoNom1',fld:'vCONTRATO_PREPOSTONOM1',pic:'@!',nv:''},{av:'AV18Contratada_PessoaNom1',fld:'vCONTRATADA_PESSOANOM1',pic:'@!',nv:''},{av:'AV43Contrato_Ano1',fld:'vCONTRATO_ANO1',pic:'ZZZ9',nv:0},{av:'AV44Contrato_Ano_To1',fld:'vCONTRATO_ANO_TO1',pic:'ZZZ9',nv:0},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV52Contrato_NumeroAta2',fld:'vCONTRATO_NUMEROATA2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV134Contrato_PrepostoNom2',fld:'vCONTRATO_PREPOSTONOM2',pic:'@!',nv:''},{av:'AV23Contratada_PessoaNom2',fld:'vCONTRATADA_PESSOANOM2',pic:'@!',nv:''},{av:'AV45Contrato_Ano2',fld:'vCONTRATO_ANO2',pic:'ZZZ9',nv:0},{av:'AV46Contrato_Ano_To2',fld:'vCONTRATO_ANO_TO2',pic:'ZZZ9',nv:0},{av:'AV53Contrato_NumeroAta3',fld:'vCONTRATO_NUMEROATA3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV135Contrato_PrepostoNom3',fld:'vCONTRATO_PREPOSTONOM3',pic:'@!',nv:''},{av:'AV28Contratada_PessoaNom3',fld:'vCONTRATADA_PESSOANOM3',pic:'@!',nv:''},{av:'AV47Contrato_Ano3',fld:'vCONTRATO_ANO3',pic:'ZZZ9',nv:0},{av:'AV48Contrato_Ano_To3',fld:'vCONTRATO_ANO_TO3',pic:'ZZZ9',nv:0},{av:'edtavContrato_numero2_Visible',ctrl:'vCONTRATO_NUMERO2',prop:'Visible'},{av:'edtavContrato_numeroata2_Visible',ctrl:'vCONTRATO_NUMEROATA2',prop:'Visible'},{av:'edtavContrato_prepostonom2_Visible',ctrl:'vCONTRATO_PREPOSTONOM2',prop:'Visible'},{av:'edtavContratada_pessoanom2_Visible',ctrl:'vCONTRATADA_PESSOANOM2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontrato_ano2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATO_ANO2',prop:'Visible'},{av:'edtavContrato_numero3_Visible',ctrl:'vCONTRATO_NUMERO3',prop:'Visible'},{av:'edtavContrato_numeroata3_Visible',ctrl:'vCONTRATO_NUMEROATA3',prop:'Visible'},{av:'edtavContrato_prepostonom3_Visible',ctrl:'vCONTRATO_PREPOSTONOM3',prop:'Visible'},{av:'edtavContratada_pessoanom3_Visible',ctrl:'vCONTRATADA_PESSOANOM3',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontrato_ano3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATO_ANO3',prop:'Visible'},{av:'edtavContrato_numero1_Visible',ctrl:'vCONTRATO_NUMERO1',prop:'Visible'},{av:'edtavContrato_numeroata1_Visible',ctrl:'vCONTRATO_NUMEROATA1',prop:'Visible'},{av:'edtavContrato_prepostonom1_Visible',ctrl:'vCONTRATO_PREPOSTONOM1',prop:'Visible'},{av:'edtavContratada_pessoanom1_Visible',ctrl:'vCONTRATADA_PESSOANOM1',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontrato_ano1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATO_ANO1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E22652',iparms:[{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'edtavContrato_numero2_Visible',ctrl:'vCONTRATO_NUMERO2',prop:'Visible'},{av:'edtavContrato_numeroata2_Visible',ctrl:'vCONTRATO_NUMEROATA2',prop:'Visible'},{av:'edtavContrato_prepostonom2_Visible',ctrl:'vCONTRATO_PREPOSTONOM2',prop:'Visible'},{av:'edtavContratada_pessoanom2_Visible',ctrl:'vCONTRATADA_PESSOANOM2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontrato_ano2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATO_ANO2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E17652',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV50Contrato_AreaTrabalhoCod',fld:'vCONTRATO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV40Contrato_Numero1',fld:'vCONTRATO_NUMERO1',pic:'',nv:''},{av:'AV51Contrato_NumeroAta1',fld:'vCONTRATO_NUMEROATA1',pic:'',nv:''},{av:'AV133Contrato_PrepostoNom1',fld:'vCONTRATO_PREPOSTONOM1',pic:'@!',nv:''},{av:'AV18Contratada_PessoaNom1',fld:'vCONTRATADA_PESSOANOM1',pic:'@!',nv:''},{av:'AV43Contrato_Ano1',fld:'vCONTRATO_ANO1',pic:'ZZZ9',nv:0},{av:'AV44Contrato_Ano_To1',fld:'vCONTRATO_ANO_TO1',pic:'ZZZ9',nv:0},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV41Contrato_Numero2',fld:'vCONTRATO_NUMERO2',pic:'',nv:''},{av:'AV52Contrato_NumeroAta2',fld:'vCONTRATO_NUMEROATA2',pic:'',nv:''},{av:'AV134Contrato_PrepostoNom2',fld:'vCONTRATO_PREPOSTONOM2',pic:'@!',nv:''},{av:'AV23Contratada_PessoaNom2',fld:'vCONTRATADA_PESSOANOM2',pic:'@!',nv:''},{av:'AV45Contrato_Ano2',fld:'vCONTRATO_ANO2',pic:'ZZZ9',nv:0},{av:'AV46Contrato_Ano_To2',fld:'vCONTRATO_ANO_TO2',pic:'ZZZ9',nv:0},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV42Contrato_Numero3',fld:'vCONTRATO_NUMERO3',pic:'',nv:''},{av:'AV53Contrato_NumeroAta3',fld:'vCONTRATO_NUMEROATA3',pic:'',nv:''},{av:'AV135Contrato_PrepostoNom3',fld:'vCONTRATO_PREPOSTONOM3',pic:'@!',nv:''},{av:'AV28Contratada_PessoaNom3',fld:'vCONTRATADA_PESSOANOM3',pic:'@!',nv:''},{av:'AV47Contrato_Ano3',fld:'vCONTRATO_ANO3',pic:'ZZZ9',nv:0},{av:'AV48Contrato_Ano_To3',fld:'vCONTRATO_ANO_TO3',pic:'ZZZ9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV121TFContrato_Ativo_Sel',fld:'vTFCONTRATO_ATIVO_SEL',pic:'9',nv:0},{av:'AV132ddo_Contrato_ValorUndCntAtualTitleControlIdToReplace',fld:'vDDO_CONTRATO_VALORUNDCNTATUALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV122ddo_Contrato_AtivoTitleControlIdToReplace',fld:'vDDO_CONTRATO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV130TFContrato_ValorUndCntAtual',fld:'vTFCONTRATO_VALORUNDCNTATUAL',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV131TFContrato_ValorUndCntAtual_To',fld:'vTFCONTRATO_VALORUNDCNTATUAL_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV189Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A92Contrato_Ativo',fld:'CONTRATO_ATIVO',pic:'',hsh:true,nv:false}],oparms:[{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV41Contrato_Numero2',fld:'vCONTRATO_NUMERO2',pic:'',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV42Contrato_Numero3',fld:'vCONTRATO_NUMERO3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'cmbavDynamicfiltersoperator3'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV40Contrato_Numero1',fld:'vCONTRATO_NUMERO1',pic:'',nv:''},{av:'AV51Contrato_NumeroAta1',fld:'vCONTRATO_NUMEROATA1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV133Contrato_PrepostoNom1',fld:'vCONTRATO_PREPOSTONOM1',pic:'@!',nv:''},{av:'AV18Contratada_PessoaNom1',fld:'vCONTRATADA_PESSOANOM1',pic:'@!',nv:''},{av:'AV43Contrato_Ano1',fld:'vCONTRATO_ANO1',pic:'ZZZ9',nv:0},{av:'AV44Contrato_Ano_To1',fld:'vCONTRATO_ANO_TO1',pic:'ZZZ9',nv:0},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV52Contrato_NumeroAta2',fld:'vCONTRATO_NUMEROATA2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV134Contrato_PrepostoNom2',fld:'vCONTRATO_PREPOSTONOM2',pic:'@!',nv:''},{av:'AV23Contratada_PessoaNom2',fld:'vCONTRATADA_PESSOANOM2',pic:'@!',nv:''},{av:'AV45Contrato_Ano2',fld:'vCONTRATO_ANO2',pic:'ZZZ9',nv:0},{av:'AV46Contrato_Ano_To2',fld:'vCONTRATO_ANO_TO2',pic:'ZZZ9',nv:0},{av:'AV53Contrato_NumeroAta3',fld:'vCONTRATO_NUMEROATA3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV135Contrato_PrepostoNom3',fld:'vCONTRATO_PREPOSTONOM3',pic:'@!',nv:''},{av:'AV28Contratada_PessoaNom3',fld:'vCONTRATADA_PESSOANOM3',pic:'@!',nv:''},{av:'AV47Contrato_Ano3',fld:'vCONTRATO_ANO3',pic:'ZZZ9',nv:0},{av:'AV48Contrato_Ano_To3',fld:'vCONTRATO_ANO_TO3',pic:'ZZZ9',nv:0},{av:'edtavContrato_numero2_Visible',ctrl:'vCONTRATO_NUMERO2',prop:'Visible'},{av:'edtavContrato_numeroata2_Visible',ctrl:'vCONTRATO_NUMEROATA2',prop:'Visible'},{av:'edtavContrato_prepostonom2_Visible',ctrl:'vCONTRATO_PREPOSTONOM2',prop:'Visible'},{av:'edtavContratada_pessoanom2_Visible',ctrl:'vCONTRATADA_PESSOANOM2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontrato_ano2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATO_ANO2',prop:'Visible'},{av:'edtavContrato_numero3_Visible',ctrl:'vCONTRATO_NUMERO3',prop:'Visible'},{av:'edtavContrato_numeroata3_Visible',ctrl:'vCONTRATO_NUMEROATA3',prop:'Visible'},{av:'edtavContrato_prepostonom3_Visible',ctrl:'vCONTRATO_PREPOSTONOM3',prop:'Visible'},{av:'edtavContratada_pessoanom3_Visible',ctrl:'vCONTRATADA_PESSOANOM3',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontrato_ano3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATO_ANO3',prop:'Visible'},{av:'edtavContrato_numero1_Visible',ctrl:'vCONTRATO_NUMERO1',prop:'Visible'},{av:'edtavContrato_numeroata1_Visible',ctrl:'vCONTRATO_NUMEROATA1',prop:'Visible'},{av:'edtavContrato_prepostonom1_Visible',ctrl:'vCONTRATO_PREPOSTONOM1',prop:'Visible'},{av:'edtavContratada_pessoanom1_Visible',ctrl:'vCONTRATADA_PESSOANOM1',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontrato_ano1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATO_ANO1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E23652',iparms:[{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'edtavContrato_numero3_Visible',ctrl:'vCONTRATO_NUMERO3',prop:'Visible'},{av:'edtavContrato_numeroata3_Visible',ctrl:'vCONTRATO_NUMEROATA3',prop:'Visible'},{av:'edtavContrato_prepostonom3_Visible',ctrl:'vCONTRATO_PREPOSTONOM3',prop:'Visible'},{av:'edtavContratada_pessoanom3_Visible',ctrl:'vCONTRATADA_PESSOANOM3',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontrato_ano3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATO_ANO3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E18652',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV50Contrato_AreaTrabalhoCod',fld:'vCONTRATO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV40Contrato_Numero1',fld:'vCONTRATO_NUMERO1',pic:'',nv:''},{av:'AV51Contrato_NumeroAta1',fld:'vCONTRATO_NUMEROATA1',pic:'',nv:''},{av:'AV133Contrato_PrepostoNom1',fld:'vCONTRATO_PREPOSTONOM1',pic:'@!',nv:''},{av:'AV18Contratada_PessoaNom1',fld:'vCONTRATADA_PESSOANOM1',pic:'@!',nv:''},{av:'AV43Contrato_Ano1',fld:'vCONTRATO_ANO1',pic:'ZZZ9',nv:0},{av:'AV44Contrato_Ano_To1',fld:'vCONTRATO_ANO_TO1',pic:'ZZZ9',nv:0},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV41Contrato_Numero2',fld:'vCONTRATO_NUMERO2',pic:'',nv:''},{av:'AV52Contrato_NumeroAta2',fld:'vCONTRATO_NUMEROATA2',pic:'',nv:''},{av:'AV134Contrato_PrepostoNom2',fld:'vCONTRATO_PREPOSTONOM2',pic:'@!',nv:''},{av:'AV23Contratada_PessoaNom2',fld:'vCONTRATADA_PESSOANOM2',pic:'@!',nv:''},{av:'AV45Contrato_Ano2',fld:'vCONTRATO_ANO2',pic:'ZZZ9',nv:0},{av:'AV46Contrato_Ano_To2',fld:'vCONTRATO_ANO_TO2',pic:'ZZZ9',nv:0},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV42Contrato_Numero3',fld:'vCONTRATO_NUMERO3',pic:'',nv:''},{av:'AV53Contrato_NumeroAta3',fld:'vCONTRATO_NUMEROATA3',pic:'',nv:''},{av:'AV135Contrato_PrepostoNom3',fld:'vCONTRATO_PREPOSTONOM3',pic:'@!',nv:''},{av:'AV28Contratada_PessoaNom3',fld:'vCONTRATADA_PESSOANOM3',pic:'@!',nv:''},{av:'AV47Contrato_Ano3',fld:'vCONTRATO_ANO3',pic:'ZZZ9',nv:0},{av:'AV48Contrato_Ano_To3',fld:'vCONTRATO_ANO_TO3',pic:'ZZZ9',nv:0},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV121TFContrato_Ativo_Sel',fld:'vTFCONTRATO_ATIVO_SEL',pic:'9',nv:0},{av:'AV132ddo_Contrato_ValorUndCntAtualTitleControlIdToReplace',fld:'vDDO_CONTRATO_VALORUNDCNTATUALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV122ddo_Contrato_AtivoTitleControlIdToReplace',fld:'vDDO_CONTRATO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV130TFContrato_ValorUndCntAtual',fld:'vTFCONTRATO_VALORUNDCNTATUAL',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV131TFContrato_ValorUndCntAtual_To',fld:'vTFCONTRATO_VALORUNDCNTATUAL_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV189Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A92Contrato_Ativo',fld:'CONTRATO_ATIVO',pic:'',hsh:true,nv:false}],oparms:[{av:'AV50Contrato_AreaTrabalhoCod',fld:'vCONTRATO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV130TFContrato_ValorUndCntAtual',fld:'vTFCONTRATO_VALORUNDCNTATUAL',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'Ddo_contrato_valorundcntatual_Filteredtext_set',ctrl:'DDO_CONTRATO_VALORUNDCNTATUAL',prop:'FilteredText_set'},{av:'AV131TFContrato_ValorUndCntAtual_To',fld:'vTFCONTRATO_VALORUNDCNTATUAL_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'Ddo_contrato_valorundcntatual_Filteredtextto_set',ctrl:'DDO_CONTRATO_VALORUNDCNTATUAL',prop:'FilteredTextTo_set'},{av:'AV121TFContrato_Ativo_Sel',fld:'vTFCONTRATO_ATIVO_SEL',pic:'9',nv:0},{av:'Ddo_contrato_ativo_Selectedvalue_set',ctrl:'DDO_CONTRATO_ATIVO',prop:'SelectedValue_set'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV40Contrato_Numero1',fld:'vCONTRATO_NUMERO1',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'edtavContrato_numero1_Visible',ctrl:'vCONTRATO_NUMERO1',prop:'Visible'},{av:'edtavContrato_numeroata1_Visible',ctrl:'vCONTRATO_NUMEROATA1',prop:'Visible'},{av:'edtavContrato_prepostonom1_Visible',ctrl:'vCONTRATO_PREPOSTONOM1',prop:'Visible'},{av:'edtavContratada_pessoanom1_Visible',ctrl:'vCONTRATADA_PESSOANOM1',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontrato_ano1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATO_ANO1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV41Contrato_Numero2',fld:'vCONTRATO_NUMERO2',pic:'',nv:''},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV42Contrato_Numero3',fld:'vCONTRATO_NUMERO3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'cmbavDynamicfiltersoperator3'},{av:'AV51Contrato_NumeroAta1',fld:'vCONTRATO_NUMEROATA1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV133Contrato_PrepostoNom1',fld:'vCONTRATO_PREPOSTONOM1',pic:'@!',nv:''},{av:'AV18Contratada_PessoaNom1',fld:'vCONTRATADA_PESSOANOM1',pic:'@!',nv:''},{av:'AV43Contrato_Ano1',fld:'vCONTRATO_ANO1',pic:'ZZZ9',nv:0},{av:'AV44Contrato_Ano_To1',fld:'vCONTRATO_ANO_TO1',pic:'ZZZ9',nv:0},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV52Contrato_NumeroAta2',fld:'vCONTRATO_NUMEROATA2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV134Contrato_PrepostoNom2',fld:'vCONTRATO_PREPOSTONOM2',pic:'@!',nv:''},{av:'AV23Contratada_PessoaNom2',fld:'vCONTRATADA_PESSOANOM2',pic:'@!',nv:''},{av:'AV45Contrato_Ano2',fld:'vCONTRATO_ANO2',pic:'ZZZ9',nv:0},{av:'AV46Contrato_Ano_To2',fld:'vCONTRATO_ANO_TO2',pic:'ZZZ9',nv:0},{av:'AV53Contrato_NumeroAta3',fld:'vCONTRATO_NUMEROATA3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV135Contrato_PrepostoNom3',fld:'vCONTRATO_PREPOSTONOM3',pic:'@!',nv:''},{av:'AV28Contratada_PessoaNom3',fld:'vCONTRATADA_PESSOANOM3',pic:'@!',nv:''},{av:'AV47Contrato_Ano3',fld:'vCONTRATO_ANO3',pic:'ZZZ9',nv:0},{av:'AV48Contrato_Ano_To3',fld:'vCONTRATO_ANO_TO3',pic:'ZZZ9',nv:0},{av:'edtavContrato_numero2_Visible',ctrl:'vCONTRATO_NUMERO2',prop:'Visible'},{av:'edtavContrato_numeroata2_Visible',ctrl:'vCONTRATO_NUMEROATA2',prop:'Visible'},{av:'edtavContrato_prepostonom2_Visible',ctrl:'vCONTRATO_PREPOSTONOM2',prop:'Visible'},{av:'edtavContratada_pessoanom2_Visible',ctrl:'vCONTRATADA_PESSOANOM2',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontrato_ano2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATO_ANO2',prop:'Visible'},{av:'edtavContrato_numero3_Visible',ctrl:'vCONTRATO_NUMERO3',prop:'Visible'},{av:'edtavContrato_numeroata3_Visible',ctrl:'vCONTRATO_NUMEROATA3',prop:'Visible'},{av:'edtavContrato_prepostonom3_Visible',ctrl:'vCONTRATO_PREPOSTONOM3',prop:'Visible'},{av:'edtavContratada_pessoanom3_Visible',ctrl:'vCONTRATADA_PESSOANOM3',prop:'Visible'},{av:'tblTablemergeddynamicfilterscontrato_ano3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSCONTRATO_ANO3',prop:'Visible'}]}");
         setEventMetadata("'DOASSOCIARGESTOR'","{handler:'E27652',iparms:[{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("'DOASSOCIARAUXILIAR'","{handler:'E28652',iparms:[{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("'DOASSOCIARSISTEMA'","{handler:'E29652',iparms:[{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_contrato_valorundcntatual_Activeeventkey = "";
         Ddo_contrato_valorundcntatual_Filteredtext_get = "";
         Ddo_contrato_valorundcntatual_Filteredtextto_get = "";
         Ddo_contrato_ativo_Activeeventkey = "";
         Ddo_contrato_ativo_Selectedvalue_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV15DynamicFiltersSelector1 = "";
         AV40Contrato_Numero1 = "";
         AV51Contrato_NumeroAta1 = "";
         AV133Contrato_PrepostoNom1 = "";
         AV18Contratada_PessoaNom1 = "";
         AV20DynamicFiltersSelector2 = "";
         AV41Contrato_Numero2 = "";
         AV52Contrato_NumeroAta2 = "";
         AV134Contrato_PrepostoNom2 = "";
         AV23Contratada_PessoaNom2 = "";
         AV25DynamicFiltersSelector3 = "";
         AV42Contrato_Numero3 = "";
         AV53Contrato_NumeroAta3 = "";
         AV135Contrato_PrepostoNom3 = "";
         AV28Contratada_PessoaNom3 = "";
         AV132ddo_Contrato_ValorUndCntAtualTitleControlIdToReplace = "";
         AV122ddo_Contrato_AtivoTitleControlIdToReplace = "";
         AV189Pgmname = "";
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV115DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV129Contrato_ValorUndCntAtualTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV120Contrato_AtivoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         A83Contrato_DataVigenciaTermino = DateTime.MinValue;
         A843Contrato_DataFimTA = DateTime.MinValue;
         Ddo_contrato_valorundcntatual_Filteredtext_set = "";
         Ddo_contrato_valorundcntatual_Filteredtextto_set = "";
         Ddo_contrato_ativo_Selectedvalue_set = "";
         Ddo_contrato_ativo_Sortedstatus = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV31Update = "";
         AV183Update_GXI = "";
         AV32Delete = "";
         AV184Delete_GXI = "";
         AV49Display = "";
         AV185Display_GXI = "";
         A41Contratada_PessoaNom = "";
         A77Contrato_Numero = "";
         A78Contrato_NumeroAta = "";
         A1869Contrato_DataTermino = DateTime.MinValue;
         A42Contratada_PessoaCNPJ = "";
         AV75AssociarGestor = "";
         AV186Associargestor_GXI = "";
         AV119AssociarAuxiliar = "";
         AV187Associarauxiliar_GXI = "";
         AV80AssociarSistema = "";
         AV188Associarsistema_GXI = "";
         scmdbuf = "";
         H00652_A5AreaTrabalho_Codigo = new int[1] ;
         H00652_A6AreaTrabalho_Descricao = new String[] {""} ;
         GXCCtl = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         H00653_A5AreaTrabalho_Codigo = new int[1] ;
         H00653_A6AreaTrabalho_Descricao = new String[] {""} ;
         GridContainer = new GXWebGrid( context);
         lV156WWContratoDS_4_Contrato_numero1 = "";
         lV157WWContratoDS_5_Contrato_numeroata1 = "";
         lV158WWContratoDS_6_Contrato_prepostonom1 = "";
         lV159WWContratoDS_7_Contratada_pessoanom1 = "";
         lV165WWContratoDS_13_Contrato_numero2 = "";
         lV166WWContratoDS_14_Contrato_numeroata2 = "";
         lV167WWContratoDS_15_Contrato_prepostonom2 = "";
         lV168WWContratoDS_16_Contratada_pessoanom2 = "";
         lV174WWContratoDS_22_Contrato_numero3 = "";
         lV175WWContratoDS_23_Contrato_numeroata3 = "";
         lV176WWContratoDS_24_Contrato_prepostonom3 = "";
         lV177WWContratoDS_25_Contratada_pessoanom3 = "";
         AV154WWContratoDS_2_Dynamicfiltersselector1 = "";
         AV156WWContratoDS_4_Contrato_numero1 = "";
         AV157WWContratoDS_5_Contrato_numeroata1 = "";
         AV158WWContratoDS_6_Contrato_prepostonom1 = "";
         AV159WWContratoDS_7_Contratada_pessoanom1 = "";
         AV163WWContratoDS_11_Dynamicfiltersselector2 = "";
         AV165WWContratoDS_13_Contrato_numero2 = "";
         AV166WWContratoDS_14_Contrato_numeroata2 = "";
         AV167WWContratoDS_15_Contrato_prepostonom2 = "";
         AV168WWContratoDS_16_Contratada_pessoanom2 = "";
         AV172WWContratoDS_20_Dynamicfiltersselector3 = "";
         AV174WWContratoDS_22_Contrato_numero3 = "";
         AV175WWContratoDS_23_Contrato_numeroata3 = "";
         AV176WWContratoDS_24_Contrato_prepostonom3 = "";
         AV177WWContratoDS_25_Contratada_pessoanom3 = "";
         A1015Contrato_PrepostoNom = "";
         H006510_A40Contratada_PessoaCod = new int[1] ;
         H006510_A1013Contrato_PrepostoCod = new int[1] ;
         H006510_n1013Contrato_PrepostoCod = new bool[] {false} ;
         H006510_A1016Contrato_PrepostoPesCod = new int[1] ;
         H006510_n1016Contrato_PrepostoPesCod = new bool[] {false} ;
         H006510_A1015Contrato_PrepostoNom = new String[] {""} ;
         H006510_n1015Contrato_PrepostoNom = new bool[] {false} ;
         H006510_A75Contrato_AreaTrabalhoCod = new int[1] ;
         H006510_A39Contratada_Codigo = new int[1] ;
         H006510_A92Contrato_Ativo = new bool[] {false} ;
         H006510_A42Contratada_PessoaCNPJ = new String[] {""} ;
         H006510_n42Contratada_PessoaCNPJ = new bool[] {false} ;
         H006510_A79Contrato_Ano = new short[1] ;
         H006510_A78Contrato_NumeroAta = new String[] {""} ;
         H006510_n78Contrato_NumeroAta = new bool[] {false} ;
         H006510_A77Contrato_Numero = new String[] {""} ;
         H006510_A41Contratada_PessoaNom = new String[] {""} ;
         H006510_n41Contratada_PessoaNom = new bool[] {false} ;
         H006510_A74Contrato_Codigo = new int[1] ;
         H006510_n74Contrato_Codigo = new bool[] {false} ;
         H006510_A1870Contrato_ValorUndCntAtual = new decimal[1] ;
         H006510_n1870Contrato_ValorUndCntAtual = new bool[] {false} ;
         H006510_A40000GXC1 = new decimal[1] ;
         H006510_n40000GXC1 = new bool[] {false} ;
         H006510_A843Contrato_DataFimTA = new DateTime[] {DateTime.MinValue} ;
         H006510_n843Contrato_DataFimTA = new bool[] {false} ;
         H006510_A83Contrato_DataVigenciaTermino = new DateTime[] {DateTime.MinValue} ;
         H006517_AGRID_nRecordCount = new long[1] ;
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         GridRow = new GXWebRow();
         AV33Session = context.GetSession();
         AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV7HTTPRequest = new GxHttpRequest( context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblContratotitle_Jsonclick = "";
         lblOrderedtext_Jsonclick = "";
         lblFiltertextcontrato_areatrabalhocod_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         lblDynamicfilterscontrato_ano_rangemiddletext3_Jsonclick = "";
         lblDynamicfilterscontrato_ano_rangemiddletext2_Jsonclick = "";
         lblDynamicfilterscontrato_ano_rangemiddletext1_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wwcontrato__default(),
            new Object[][] {
                new Object[] {
               H00652_A5AreaTrabalho_Codigo, H00652_A6AreaTrabalho_Descricao
               }
               , new Object[] {
               H00653_A5AreaTrabalho_Codigo, H00653_A6AreaTrabalho_Descricao
               }
               , new Object[] {
               H006510_A40Contratada_PessoaCod, H006510_A1013Contrato_PrepostoCod, H006510_n1013Contrato_PrepostoCod, H006510_A1016Contrato_PrepostoPesCod, H006510_n1016Contrato_PrepostoPesCod, H006510_A1015Contrato_PrepostoNom, H006510_n1015Contrato_PrepostoNom, H006510_A75Contrato_AreaTrabalhoCod, H006510_A39Contratada_Codigo, H006510_A92Contrato_Ativo,
               H006510_A42Contratada_PessoaCNPJ, H006510_n42Contratada_PessoaCNPJ, H006510_A79Contrato_Ano, H006510_A78Contrato_NumeroAta, H006510_n78Contrato_NumeroAta, H006510_A77Contrato_Numero, H006510_A41Contratada_PessoaNom, H006510_n41Contratada_PessoaNom, H006510_A74Contrato_Codigo, H006510_A1870Contrato_ValorUndCntAtual,
               H006510_n1870Contrato_ValorUndCntAtual, H006510_A40000GXC1, H006510_n40000GXC1, H006510_A843Contrato_DataFimTA, H006510_n843Contrato_DataFimTA, H006510_A83Contrato_DataVigenciaTermino
               }
               , new Object[] {
               H006517_AGRID_nRecordCount
               }
            }
         );
         AV189Pgmname = "WWContrato";
         /* GeneXus formulas. */
         AV189Pgmname = "WWContrato";
         context.Gx_err = 0;
         edtavSaldocontrato_saldo_Enabled = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_128 ;
      private short nGXsfl_128_idx=1 ;
      private short AV13OrderedBy ;
      private short AV16DynamicFiltersOperator1 ;
      private short AV43Contrato_Ano1 ;
      private short AV44Contrato_Ano_To1 ;
      private short AV21DynamicFiltersOperator2 ;
      private short AV45Contrato_Ano2 ;
      private short AV46Contrato_Ano_To2 ;
      private short AV26DynamicFiltersOperator3 ;
      private short AV47Contrato_Ano3 ;
      private short AV48Contrato_Ano_To3 ;
      private short AV121TFContrato_Ativo_Sel ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short A79Contrato_Ano ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_128_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short AV155WWContratoDS_3_Dynamicfiltersoperator1 ;
      private short AV160WWContratoDS_8_Contrato_ano1 ;
      private short AV161WWContratoDS_9_Contrato_ano_to1 ;
      private short AV164WWContratoDS_12_Dynamicfiltersoperator2 ;
      private short AV169WWContratoDS_17_Contrato_ano2 ;
      private short AV170WWContratoDS_18_Contrato_ano_to2 ;
      private short AV173WWContratoDS_21_Dynamicfiltersoperator3 ;
      private short AV178WWContratoDS_26_Contrato_ano3 ;
      private short AV179WWContratoDS_27_Contrato_ano_to3 ;
      private short AV182WWContratoDS_30_Tfcontrato_ativo_sel ;
      private short edtContratada_PessoaNom_Titleformat ;
      private short edtContrato_Numero_Titleformat ;
      private short edtContrato_NumeroAta_Titleformat ;
      private short edtContrato_Ano_Titleformat ;
      private short edtContrato_ValorUndCntAtual_Titleformat ;
      private short edtContratada_PessoaCNPJ_Titleformat ;
      private short chkContrato_Ativo_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int subGrid_Rows ;
      private int AV50Contrato_AreaTrabalhoCod ;
      private int A39Contratada_Codigo ;
      private int A74Contrato_Codigo ;
      private int Gridpaginationbar_Pagestoshow ;
      private int edtavTfcontrato_valorundcntatual_Visible ;
      private int edtavTfcontrato_valorundcntatual_to_Visible ;
      private int edtavTfcontrato_ativo_sel_Visible ;
      private int edtavDdo_contrato_valorundcntatualtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contrato_ativotitlecontrolidtoreplace_Visible ;
      private int gxdynajaxindex ;
      private int subGrid_Islastpage ;
      private int edtavSaldocontrato_saldo_Enabled ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int AV6WWPContext_gxTpr_Contratada_codigo ;
      private int AV153WWContratoDS_1_Contrato_areatrabalhocod ;
      private int A75Contrato_AreaTrabalhoCod ;
      private int A40Contratada_PessoaCod ;
      private int A1013Contrato_PrepostoCod ;
      private int A1016Contrato_PrepostoPesCod ;
      private int edtavOrdereddsc_Visible ;
      private int AV116PageToGo ;
      private int AV76Contratada_Codigo ;
      private int edtavUpdate_Enabled ;
      private int edtavDelete_Enabled ;
      private int edtContratada_PessoaNom_Forecolor ;
      private int edtContrato_Numero_Forecolor ;
      private int edtContrato_NumeroAta_Forecolor ;
      private int edtContrato_Ano_Forecolor ;
      private int edtContrato_DataTermino_Forecolor ;
      private int edtContrato_ValorUndCntAtual_Forecolor ;
      private int edtavSaldocontrato_saldo_Forecolor ;
      private int edtContratada_PessoaCNPJ_Forecolor ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int edtavContrato_numero1_Visible ;
      private int edtavContrato_numeroata1_Visible ;
      private int edtavContrato_prepostonom1_Visible ;
      private int edtavContratada_pessoanom1_Visible ;
      private int tblTablemergeddynamicfilterscontrato_ano1_Visible ;
      private int edtavContrato_numero2_Visible ;
      private int edtavContrato_numeroata2_Visible ;
      private int edtavContrato_prepostonom2_Visible ;
      private int edtavContratada_pessoanom2_Visible ;
      private int tblTablemergeddynamicfilterscontrato_ano2_Visible ;
      private int edtavContrato_numero3_Visible ;
      private int edtavContrato_numeroata3_Visible ;
      private int edtavContrato_prepostonom3_Visible ;
      private int edtavContratada_pessoanom3_Visible ;
      private int tblTablemergeddynamicfilterscontrato_ano3_Visible ;
      private int AV190GXV1 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private int edtavSaldocontrato_saldo_Visible ;
      private int edtavAssociargestor_Enabled ;
      private int edtavAssociargestor_Visible ;
      private int edtavAssociarauxiliar_Enabled ;
      private int edtavAssociarauxiliar_Visible ;
      private int edtavAssociarsistema_Enabled ;
      private int edtavAssociarsistema_Visible ;
      private long GRID_nFirstRecordOnPage ;
      private long AV117GridCurrentPage ;
      private long AV118GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private long AV150Color ;
      private decimal AV130TFContrato_ValorUndCntAtual ;
      private decimal AV131TFContrato_ValorUndCntAtual_To ;
      private decimal A1870Contrato_ValorUndCntAtual ;
      private decimal AV78SaldoContrato_Saldo ;
      private decimal AV180WWContratoDS_28_Tfcontrato_valorundcntatual ;
      private decimal AV181WWContratoDS_29_Tfcontrato_valorundcntatual_to ;
      private decimal A40000GXC1 ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_contrato_valorundcntatual_Activeeventkey ;
      private String Ddo_contrato_valorundcntatual_Filteredtext_get ;
      private String Ddo_contrato_valorundcntatual_Filteredtextto_get ;
      private String Ddo_contrato_ativo_Activeeventkey ;
      private String Ddo_contrato_ativo_Selectedvalue_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_128_idx="0001" ;
      private String AV40Contrato_Numero1 ;
      private String AV51Contrato_NumeroAta1 ;
      private String AV133Contrato_PrepostoNom1 ;
      private String AV18Contratada_PessoaNom1 ;
      private String AV41Contrato_Numero2 ;
      private String AV52Contrato_NumeroAta2 ;
      private String AV134Contrato_PrepostoNom2 ;
      private String AV23Contratada_PessoaNom2 ;
      private String AV42Contrato_Numero3 ;
      private String AV53Contrato_NumeroAta3 ;
      private String AV135Contrato_PrepostoNom3 ;
      private String AV28Contratada_PessoaNom3 ;
      private String AV189Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_contrato_valorundcntatual_Caption ;
      private String Ddo_contrato_valorundcntatual_Tooltip ;
      private String Ddo_contrato_valorundcntatual_Cls ;
      private String Ddo_contrato_valorundcntatual_Filteredtext_set ;
      private String Ddo_contrato_valorundcntatual_Filteredtextto_set ;
      private String Ddo_contrato_valorundcntatual_Dropdownoptionstype ;
      private String Ddo_contrato_valorundcntatual_Titlecontrolidtoreplace ;
      private String Ddo_contrato_valorundcntatual_Filtertype ;
      private String Ddo_contrato_valorundcntatual_Cleanfilter ;
      private String Ddo_contrato_valorundcntatual_Rangefilterfrom ;
      private String Ddo_contrato_valorundcntatual_Rangefilterto ;
      private String Ddo_contrato_valorundcntatual_Searchbuttontext ;
      private String Ddo_contrato_ativo_Caption ;
      private String Ddo_contrato_ativo_Tooltip ;
      private String Ddo_contrato_ativo_Cls ;
      private String Ddo_contrato_ativo_Selectedvalue_set ;
      private String Ddo_contrato_ativo_Dropdownoptionstype ;
      private String Ddo_contrato_ativo_Titlecontrolidtoreplace ;
      private String Ddo_contrato_ativo_Sortedstatus ;
      private String Ddo_contrato_ativo_Datalisttype ;
      private String Ddo_contrato_ativo_Datalistfixedvalues ;
      private String Ddo_contrato_ativo_Sortasc ;
      private String Ddo_contrato_ativo_Sortdsc ;
      private String Ddo_contrato_ativo_Cleanfilter ;
      private String Ddo_contrato_ativo_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String edtavTfcontrato_valorundcntatual_Internalname ;
      private String edtavTfcontrato_valorundcntatual_Jsonclick ;
      private String edtavTfcontrato_valorundcntatual_to_Internalname ;
      private String edtavTfcontrato_valorundcntatual_to_Jsonclick ;
      private String edtavTfcontrato_ativo_sel_Internalname ;
      private String edtavTfcontrato_ativo_sel_Jsonclick ;
      private String edtavDdo_contrato_valorundcntatualtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contrato_ativotitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavUpdate_Internalname ;
      private String edtavDelete_Internalname ;
      private String edtavDisplay_Internalname ;
      private String edtContrato_Codigo_Internalname ;
      private String A41Contratada_PessoaNom ;
      private String edtContratada_PessoaNom_Internalname ;
      private String A77Contrato_Numero ;
      private String edtContrato_Numero_Internalname ;
      private String A78Contrato_NumeroAta ;
      private String edtContrato_NumeroAta_Internalname ;
      private String edtContrato_Ano_Internalname ;
      private String edtContrato_DataTermino_Internalname ;
      private String edtContrato_ValorUndCntAtual_Internalname ;
      private String edtavSaldocontrato_saldo_Internalname ;
      private String edtContratada_PessoaCNPJ_Internalname ;
      private String chkContrato_Ativo_Internalname ;
      private String edtavAssociargestor_Internalname ;
      private String edtavAssociarauxiliar_Internalname ;
      private String edtavAssociarsistema_Internalname ;
      private String scmdbuf ;
      private String GXCCtl ;
      private String cmbavOrderedby_Internalname ;
      private String gxwrpcisep ;
      private String lV156WWContratoDS_4_Contrato_numero1 ;
      private String lV157WWContratoDS_5_Contrato_numeroata1 ;
      private String lV158WWContratoDS_6_Contrato_prepostonom1 ;
      private String lV159WWContratoDS_7_Contratada_pessoanom1 ;
      private String lV165WWContratoDS_13_Contrato_numero2 ;
      private String lV166WWContratoDS_14_Contrato_numeroata2 ;
      private String lV167WWContratoDS_15_Contrato_prepostonom2 ;
      private String lV168WWContratoDS_16_Contratada_pessoanom2 ;
      private String lV174WWContratoDS_22_Contrato_numero3 ;
      private String lV175WWContratoDS_23_Contrato_numeroata3 ;
      private String lV176WWContratoDS_24_Contrato_prepostonom3 ;
      private String lV177WWContratoDS_25_Contratada_pessoanom3 ;
      private String AV156WWContratoDS_4_Contrato_numero1 ;
      private String AV157WWContratoDS_5_Contrato_numeroata1 ;
      private String AV158WWContratoDS_6_Contrato_prepostonom1 ;
      private String AV159WWContratoDS_7_Contratada_pessoanom1 ;
      private String AV165WWContratoDS_13_Contrato_numero2 ;
      private String AV166WWContratoDS_14_Contrato_numeroata2 ;
      private String AV167WWContratoDS_15_Contrato_prepostonom2 ;
      private String AV168WWContratoDS_16_Contratada_pessoanom2 ;
      private String AV174WWContratoDS_22_Contrato_numero3 ;
      private String AV175WWContratoDS_23_Contrato_numeroata3 ;
      private String AV176WWContratoDS_24_Contrato_prepostonom3 ;
      private String AV177WWContratoDS_25_Contratada_pessoanom3 ;
      private String A1015Contrato_PrepostoNom ;
      private String edtavOrdereddsc_Internalname ;
      private String dynavContrato_areatrabalhocod_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Internalname ;
      private String edtavContrato_numero1_Internalname ;
      private String edtavContrato_numeroata1_Internalname ;
      private String edtavContrato_prepostonom1_Internalname ;
      private String edtavContratada_pessoanom1_Internalname ;
      private String edtavContrato_ano1_Internalname ;
      private String edtavContrato_ano_to1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Internalname ;
      private String edtavContrato_numero2_Internalname ;
      private String edtavContrato_numeroata2_Internalname ;
      private String edtavContrato_prepostonom2_Internalname ;
      private String edtavContratada_pessoanom2_Internalname ;
      private String edtavContrato_ano2_Internalname ;
      private String edtavContrato_ano_to2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Internalname ;
      private String edtavContrato_numero3_Internalname ;
      private String edtavContrato_numeroata3_Internalname ;
      private String edtavContrato_prepostonom3_Internalname ;
      private String edtavContratada_pessoanom3_Internalname ;
      private String edtavContrato_ano3_Internalname ;
      private String edtavContrato_ano_to3_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_contrato_valorundcntatual_Internalname ;
      private String Ddo_contrato_ativo_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String edtContratada_PessoaNom_Title ;
      private String edtContrato_Numero_Title ;
      private String edtContrato_NumeroAta_Title ;
      private String edtContrato_Ano_Title ;
      private String edtContrato_ValorUndCntAtual_Title ;
      private String edtContratada_PessoaCNPJ_Title ;
      private String edtavAssociargestor_Title ;
      private String edtavAssociarauxiliar_Title ;
      private String edtavAssociarsistema_Title ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavUpdate_Link ;
      private String edtavDelete_Tooltiptext ;
      private String edtavDelete_Link ;
      private String edtavDisplay_Tooltiptext ;
      private String edtavDisplay_Link ;
      private String edtavAssociargestor_Tooltiptext ;
      private String edtavAssociarauxiliar_Tooltiptext ;
      private String edtavAssociarsistema_Tooltiptext ;
      private String edtContratada_PessoaNom_Link ;
      private String edtContrato_NumeroAta_Link ;
      private String tblTablemergeddynamicfilterscontrato_ano1_Internalname ;
      private String tblTablemergeddynamicfilterscontrato_ano2_Internalname ;
      private String tblTablemergeddynamicfilterscontrato_ano3_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTableheader_Internalname ;
      private String tblTableactions_Internalname ;
      private String lblContratotitle_Internalname ;
      private String lblContratotitle_Jsonclick ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String tblUnnamedtable1_Internalname ;
      private String lblFiltertextcontrato_areatrabalhocod_Internalname ;
      private String lblFiltertextcontrato_areatrabalhocod_Jsonclick ;
      private String dynavContrato_areatrabalhocod_Jsonclick ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String tblTablemergeddynamicfilters3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Jsonclick ;
      private String edtavContrato_numero3_Jsonclick ;
      private String edtavContrato_numeroata3_Jsonclick ;
      private String edtavContrato_prepostonom3_Jsonclick ;
      private String edtavContratada_pessoanom3_Jsonclick ;
      private String edtavContrato_ano3_Jsonclick ;
      private String lblDynamicfilterscontrato_ano_rangemiddletext3_Internalname ;
      private String lblDynamicfilterscontrato_ano_rangemiddletext3_Jsonclick ;
      private String edtavContrato_ano_to3_Jsonclick ;
      private String tblTablemergeddynamicfilters2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Jsonclick ;
      private String edtavContrato_numero2_Jsonclick ;
      private String edtavContrato_numeroata2_Jsonclick ;
      private String edtavContrato_prepostonom2_Jsonclick ;
      private String edtavContratada_pessoanom2_Jsonclick ;
      private String edtavContrato_ano2_Jsonclick ;
      private String lblDynamicfilterscontrato_ano_rangemiddletext2_Internalname ;
      private String lblDynamicfilterscontrato_ano_rangemiddletext2_Jsonclick ;
      private String edtavContrato_ano_to2_Jsonclick ;
      private String tblTablemergeddynamicfilters1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Jsonclick ;
      private String edtavContrato_numero1_Jsonclick ;
      private String edtavContrato_numeroata1_Jsonclick ;
      private String edtavContrato_prepostonom1_Jsonclick ;
      private String edtavContratada_pessoanom1_Jsonclick ;
      private String edtavContrato_ano1_Jsonclick ;
      private String lblDynamicfilterscontrato_ano_rangemiddletext1_Internalname ;
      private String lblDynamicfilterscontrato_ano_rangemiddletext1_Jsonclick ;
      private String edtavContrato_ano_to1_Jsonclick ;
      private String sGXsfl_128_fel_idx="0001" ;
      private String ROClassString ;
      private String edtContrato_Codigo_Jsonclick ;
      private String edtContratada_PessoaNom_Jsonclick ;
      private String edtContrato_Numero_Jsonclick ;
      private String edtContrato_NumeroAta_Jsonclick ;
      private String edtContrato_Ano_Jsonclick ;
      private String edtContrato_DataTermino_Jsonclick ;
      private String edtContrato_ValorUndCntAtual_Jsonclick ;
      private String edtavSaldocontrato_saldo_Jsonclick ;
      private String edtContratada_PessoaCNPJ_Jsonclick ;
      private String edtavAssociargestor_Jsonclick ;
      private String edtavAssociarauxiliar_Jsonclick ;
      private String edtavAssociarsistema_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private DateTime A83Contrato_DataVigenciaTermino ;
      private DateTime A843Contrato_DataFimTA ;
      private DateTime A1869Contrato_DataTermino ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool AV19DynamicFiltersEnabled2 ;
      private bool AV24DynamicFiltersEnabled3 ;
      private bool AV30DynamicFiltersIgnoreFirst ;
      private bool AV29DynamicFiltersRemoving ;
      private bool n74Contrato_Codigo ;
      private bool A92Contrato_Ativo ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_contrato_valorundcntatual_Includesortasc ;
      private bool Ddo_contrato_valorundcntatual_Includesortdsc ;
      private bool Ddo_contrato_valorundcntatual_Includefilter ;
      private bool Ddo_contrato_valorundcntatual_Filterisrange ;
      private bool Ddo_contrato_valorundcntatual_Includedatalist ;
      private bool Ddo_contrato_ativo_Includesortasc ;
      private bool Ddo_contrato_ativo_Includesortdsc ;
      private bool Ddo_contrato_ativo_Includefilter ;
      private bool Ddo_contrato_ativo_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n41Contratada_PessoaNom ;
      private bool n78Contrato_NumeroAta ;
      private bool n1870Contrato_ValorUndCntAtual ;
      private bool n42Contratada_PessoaCNPJ ;
      private bool AV162WWContratoDS_10_Dynamicfiltersenabled2 ;
      private bool AV171WWContratoDS_19_Dynamicfiltersenabled3 ;
      private bool n1013Contrato_PrepostoCod ;
      private bool n1016Contrato_PrepostoPesCod ;
      private bool n1015Contrato_PrepostoNom ;
      private bool n40000GXC1 ;
      private bool n843Contrato_DataFimTA ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV31Update_IsBlob ;
      private bool AV32Delete_IsBlob ;
      private bool AV49Display_IsBlob ;
      private bool AV75AssociarGestor_IsBlob ;
      private bool AV119AssociarAuxiliar_IsBlob ;
      private bool AV80AssociarSistema_IsBlob ;
      private String AV15DynamicFiltersSelector1 ;
      private String AV20DynamicFiltersSelector2 ;
      private String AV25DynamicFiltersSelector3 ;
      private String AV132ddo_Contrato_ValorUndCntAtualTitleControlIdToReplace ;
      private String AV122ddo_Contrato_AtivoTitleControlIdToReplace ;
      private String AV183Update_GXI ;
      private String AV184Delete_GXI ;
      private String AV185Display_GXI ;
      private String A42Contratada_PessoaCNPJ ;
      private String AV186Associargestor_GXI ;
      private String AV187Associarauxiliar_GXI ;
      private String AV188Associarsistema_GXI ;
      private String AV154WWContratoDS_2_Dynamicfiltersselector1 ;
      private String AV163WWContratoDS_11_Dynamicfiltersselector2 ;
      private String AV172WWContratoDS_20_Dynamicfiltersselector3 ;
      private String AV31Update ;
      private String AV32Delete ;
      private String AV49Display ;
      private String AV75AssociarGestor ;
      private String AV119AssociarAuxiliar ;
      private String AV80AssociarSistema ;
      private IGxSession AV33Session ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox dynavContrato_areatrabalhocod ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersoperator1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersoperator2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCombobox cmbavDynamicfiltersoperator3 ;
      private GXCheckbox chkContrato_Ativo ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private int[] H00652_A5AreaTrabalho_Codigo ;
      private String[] H00652_A6AreaTrabalho_Descricao ;
      private int[] H00653_A5AreaTrabalho_Codigo ;
      private String[] H00653_A6AreaTrabalho_Descricao ;
      private int[] H006510_A40Contratada_PessoaCod ;
      private int[] H006510_A1013Contrato_PrepostoCod ;
      private bool[] H006510_n1013Contrato_PrepostoCod ;
      private int[] H006510_A1016Contrato_PrepostoPesCod ;
      private bool[] H006510_n1016Contrato_PrepostoPesCod ;
      private String[] H006510_A1015Contrato_PrepostoNom ;
      private bool[] H006510_n1015Contrato_PrepostoNom ;
      private int[] H006510_A75Contrato_AreaTrabalhoCod ;
      private int[] H006510_A39Contratada_Codigo ;
      private bool[] H006510_A92Contrato_Ativo ;
      private String[] H006510_A42Contratada_PessoaCNPJ ;
      private bool[] H006510_n42Contratada_PessoaCNPJ ;
      private short[] H006510_A79Contrato_Ano ;
      private String[] H006510_A78Contrato_NumeroAta ;
      private bool[] H006510_n78Contrato_NumeroAta ;
      private String[] H006510_A77Contrato_Numero ;
      private String[] H006510_A41Contratada_PessoaNom ;
      private bool[] H006510_n41Contratada_PessoaNom ;
      private int[] H006510_A74Contrato_Codigo ;
      private bool[] H006510_n74Contrato_Codigo ;
      private decimal[] H006510_A1870Contrato_ValorUndCntAtual ;
      private bool[] H006510_n1870Contrato_ValorUndCntAtual ;
      private decimal[] H006510_A40000GXC1 ;
      private bool[] H006510_n40000GXC1 ;
      private DateTime[] H006510_A843Contrato_DataFimTA ;
      private bool[] H006510_n843Contrato_DataFimTA ;
      private DateTime[] H006510_A83Contrato_DataVigenciaTermino ;
      private long[] H006517_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV7HTTPRequest ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV129Contrato_ValorUndCntAtualTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV120Contrato_AtivoTitleFilterData ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV115DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV11GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
   }

   public class wwcontrato__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H006510( IGxContext context ,
                                              int AV153WWContratoDS_1_Contrato_areatrabalhocod ,
                                              int AV6WWPContext_gxTpr_Contratada_codigo ,
                                              String AV154WWContratoDS_2_Dynamicfiltersselector1 ,
                                              String AV156WWContratoDS_4_Contrato_numero1 ,
                                              String AV157WWContratoDS_5_Contrato_numeroata1 ,
                                              short AV155WWContratoDS_3_Dynamicfiltersoperator1 ,
                                              String AV158WWContratoDS_6_Contrato_prepostonom1 ,
                                              String AV159WWContratoDS_7_Contratada_pessoanom1 ,
                                              short AV160WWContratoDS_8_Contrato_ano1 ,
                                              short AV161WWContratoDS_9_Contrato_ano_to1 ,
                                              bool AV162WWContratoDS_10_Dynamicfiltersenabled2 ,
                                              String AV163WWContratoDS_11_Dynamicfiltersselector2 ,
                                              String AV165WWContratoDS_13_Contrato_numero2 ,
                                              String AV166WWContratoDS_14_Contrato_numeroata2 ,
                                              short AV164WWContratoDS_12_Dynamicfiltersoperator2 ,
                                              String AV167WWContratoDS_15_Contrato_prepostonom2 ,
                                              String AV168WWContratoDS_16_Contratada_pessoanom2 ,
                                              short AV169WWContratoDS_17_Contrato_ano2 ,
                                              short AV170WWContratoDS_18_Contrato_ano_to2 ,
                                              bool AV171WWContratoDS_19_Dynamicfiltersenabled3 ,
                                              String AV172WWContratoDS_20_Dynamicfiltersselector3 ,
                                              String AV174WWContratoDS_22_Contrato_numero3 ,
                                              String AV175WWContratoDS_23_Contrato_numeroata3 ,
                                              short AV173WWContratoDS_21_Dynamicfiltersoperator3 ,
                                              String AV176WWContratoDS_24_Contrato_prepostonom3 ,
                                              String AV177WWContratoDS_25_Contratada_pessoanom3 ,
                                              short AV178WWContratoDS_26_Contrato_ano3 ,
                                              short AV179WWContratoDS_27_Contrato_ano_to3 ,
                                              short AV182WWContratoDS_30_Tfcontrato_ativo_sel ,
                                              int A75Contrato_AreaTrabalhoCod ,
                                              int A39Contratada_Codigo ,
                                              String A77Contrato_Numero ,
                                              String A78Contrato_NumeroAta ,
                                              String A1015Contrato_PrepostoNom ,
                                              String A41Contratada_PessoaNom ,
                                              short A79Contrato_Ano ,
                                              bool A92Contrato_Ativo ,
                                              short AV13OrderedBy ,
                                              bool AV14OrderedDsc ,
                                              decimal AV180WWContratoDS_28_Tfcontrato_valorundcntatual ,
                                              decimal A1870Contrato_ValorUndCntAtual ,
                                              decimal AV181WWContratoDS_29_Tfcontrato_valorundcntatual_to )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [32] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T4.[Contratada_PessoaCod] AS Contratada_PessoaCod, T1.[Contrato_PrepostoCod] AS Contrato_PrepostoCod, T2.[Usuario_PessoaCod] AS Contrato_PrepostoPesCod, T3.[Pessoa_Nome] AS Contrato_PrepostoNom, T1.[Contrato_AreaTrabalhoCod], T1.[Contratada_Codigo], T1.[Contrato_Ativo], T5.[Pessoa_Docto] AS Contratada_PessoaCNPJ, T1.[Contrato_Ano], T1.[Contrato_NumeroAta], T1.[Contrato_Numero], T5.[Pessoa_Nome] AS Contratada_PessoaNom, T1.[Contrato_Codigo], COALESCE( T6.[Contrato_ValorUndCntAtual], 0) AS Contrato_ValorUndCntAtual, COALESCE( T8.[GXC1], 0) AS GXC1, COALESCE( T7.[ContratoTermoAditivo_DataFim], convert( DATETIME, '17530101', 112 )) AS Contrato_DataFimTA, T1.[Contrato_DataVigenciaTermino]";
         sFromString = " FROM ((((((([Contrato] T1 WITH (NOLOCK) LEFT JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[Contrato_PrepostoCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod]) INNER JOIN [Contratada] T4 WITH (NOLOCK) ON T4.[Contratada_Codigo] = T1.[Contratada_Codigo]) INNER JOIN [Pessoa] T5 WITH (NOLOCK) ON T5.[Pessoa_Codigo] = T4.[Contratada_PessoaCod]) LEFT JOIN (SELECT CASE  WHEN COALESCE( T10.[GXC4], 0) > 0 THEN COALESCE( T10.[GXC4], 0) ELSE T9.[Contrato_ValorUnidadeContratacao] END AS Contrato_ValorUndCntAtual, T9.[Contrato_Codigo] FROM ([Contrato] T9 WITH (NOLOCK) LEFT JOIN (SELECT T11.[ContratoTermoAditivo_VlrUntUndCnt] AS GXC4, T11.[Contrato_Codigo], T11.[ContratoTermoAditivo_Codigo], T12.[GXC6] AS GXC6 FROM ([ContratoTermoAditivo] T11 WITH (NOLOCK) INNER JOIN (SELECT MAX([ContratoTermoAditivo_Codigo]) AS GXC6, [Contrato_Codigo] FROM [ContratoTermoAditivo] WITH (NOLOCK) GROUP BY [Contrato_Codigo] ) T12 ON T12.[Contrato_Codigo] = T11.[Contrato_Codigo]) WHERE T11.[ContratoTermoAditivo_Codigo] = T12.[GXC6] ) T10 ON T10.[Contrato_Codigo] = T9.[Contrato_Codigo]) ) T6 ON T6.[Contrato_Codigo] = T1.[Contrato_Codigo]) LEFT JOIN (SELECT T9.[ContratoTermoAditivo_DataFim], T9.[Contrato_Codigo], T9.[ContratoTermoAditivo_Codigo], T10.[GXC8] AS GXC8 FROM ([ContratoTermoAditivo] T9 WITH (NOLOCK) INNER JOIN (SELECT MAX([ContratoTermoAditivo_Codigo]) AS GXC8, [Contrato_Codigo] FROM [ContratoTermoAditivo] WITH (NOLOCK) GROUP BY [Contrato_Codigo] ) T10 ON T10.[Contrato_Codigo] = T9.[Contrato_Codigo]) WHERE T9.[ContratoTermoAditivo_Codigo] = T10.[GXC8] ) T7 ON T7.[Contrato_Codigo] = T1.[Contrato_Codigo]) LEFT JOIN (SELECT SUM([SaldoContrato_Saldo]) AS GXC1, [Contrato_Codigo] FROM [SaldoContrato] WITH (NOLOCK) GROUP BY [Contrato_Codigo] ) T8 ON T8.[Contrato_Codigo]";
         sFromString = sFromString + " = T1.[Contrato_Codigo])";
         sOrderString = "";
         sWhereString = sWhereString + " WHERE ((@AV180WWContratoDS_28_Tfcontrato_valorundcntatual = convert(int, 0)) or ( COALESCE( T6.[Contrato_ValorUndCntAtual], 0) >= @AV180WWContratoDS_28_Tfcontrato_valorundcntatual))";
         sWhereString = sWhereString + " and ((@AV181WWContratoDS_29_Tfcontrato_valorundcntatual_to = convert(int, 0)) or ( COALESCE( T6.[Contrato_ValorUndCntAtual], 0) <= @AV181WWContratoDS_29_Tfcontrato_valorundcntatual_to))";
         if ( ! (0==AV153WWContratoDS_1_Contrato_areatrabalhocod) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_AreaTrabalhoCod] = @AV153WWContratoDS_1_Contrato_areatrabalhocod)";
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( AV6WWPContext_gxTpr_Contratada_codigo > 0 )
         {
            sWhereString = sWhereString + " and (T1.[Contratada_Codigo] = @AV6WWPCo_1Contratada_codigo)";
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( ( StringUtil.StrCmp(AV154WWContratoDS_2_Dynamicfiltersselector1, "CONTRATO_NUMERO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV156WWContratoDS_4_Contrato_numero1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_Numero] like @lV156WWContratoDS_4_Contrato_numero1)";
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( ( StringUtil.StrCmp(AV154WWContratoDS_2_Dynamicfiltersselector1, "CONTRATO_NUMEROATA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV157WWContratoDS_5_Contrato_numeroata1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_NumeroAta] like '%' + @lV157WWContratoDS_5_Contrato_numeroata1 + '%')";
         }
         else
         {
            GXv_int2[7] = 1;
         }
         if ( ( StringUtil.StrCmp(AV154WWContratoDS_2_Dynamicfiltersselector1, "CONTRATO_PREPOSTONOM") == 0 ) && ( AV155WWContratoDS_3_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV158WWContratoDS_6_Contrato_prepostonom1)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV158WWContratoDS_6_Contrato_prepostonom1)";
         }
         else
         {
            GXv_int2[8] = 1;
         }
         if ( ( StringUtil.StrCmp(AV154WWContratoDS_2_Dynamicfiltersselector1, "CONTRATO_PREPOSTONOM") == 0 ) && ( AV155WWContratoDS_3_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV158WWContratoDS_6_Contrato_prepostonom1)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV158WWContratoDS_6_Contrato_prepostonom1)";
         }
         else
         {
            GXv_int2[9] = 1;
         }
         if ( ( StringUtil.StrCmp(AV154WWContratoDS_2_Dynamicfiltersselector1, "CONTRATADA_PESSOANOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV159WWContratoDS_7_Contratada_pessoanom1)) ) )
         {
            sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like '%' + @lV159WWContratoDS_7_Contratada_pessoanom1 + '%')";
         }
         else
         {
            GXv_int2[10] = 1;
         }
         if ( ( StringUtil.StrCmp(AV154WWContratoDS_2_Dynamicfiltersselector1, "CONTRATO_ANO") == 0 ) && ( ! (0==AV160WWContratoDS_8_Contrato_ano1) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_Ano] >= @AV160WWContratoDS_8_Contrato_ano1)";
         }
         else
         {
            GXv_int2[11] = 1;
         }
         if ( ( StringUtil.StrCmp(AV154WWContratoDS_2_Dynamicfiltersselector1, "CONTRATO_ANO") == 0 ) && ( ! (0==AV161WWContratoDS_9_Contrato_ano_to1) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_Ano] <= @AV161WWContratoDS_9_Contrato_ano_to1)";
         }
         else
         {
            GXv_int2[12] = 1;
         }
         if ( AV162WWContratoDS_10_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV163WWContratoDS_11_Dynamicfiltersselector2, "CONTRATO_NUMERO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV165WWContratoDS_13_Contrato_numero2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_Numero] like @lV165WWContratoDS_13_Contrato_numero2)";
         }
         else
         {
            GXv_int2[13] = 1;
         }
         if ( AV162WWContratoDS_10_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV163WWContratoDS_11_Dynamicfiltersselector2, "CONTRATO_NUMEROATA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV166WWContratoDS_14_Contrato_numeroata2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_NumeroAta] like '%' + @lV166WWContratoDS_14_Contrato_numeroata2 + '%')";
         }
         else
         {
            GXv_int2[14] = 1;
         }
         if ( AV162WWContratoDS_10_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV163WWContratoDS_11_Dynamicfiltersselector2, "CONTRATO_PREPOSTONOM") == 0 ) && ( AV164WWContratoDS_12_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV167WWContratoDS_15_Contrato_prepostonom2)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV167WWContratoDS_15_Contrato_prepostonom2)";
         }
         else
         {
            GXv_int2[15] = 1;
         }
         if ( AV162WWContratoDS_10_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV163WWContratoDS_11_Dynamicfiltersselector2, "CONTRATO_PREPOSTONOM") == 0 ) && ( AV164WWContratoDS_12_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV167WWContratoDS_15_Contrato_prepostonom2)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV167WWContratoDS_15_Contrato_prepostonom2)";
         }
         else
         {
            GXv_int2[16] = 1;
         }
         if ( AV162WWContratoDS_10_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV163WWContratoDS_11_Dynamicfiltersselector2, "CONTRATADA_PESSOANOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV168WWContratoDS_16_Contratada_pessoanom2)) ) )
         {
            sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like '%' + @lV168WWContratoDS_16_Contratada_pessoanom2 + '%')";
         }
         else
         {
            GXv_int2[17] = 1;
         }
         if ( AV162WWContratoDS_10_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV163WWContratoDS_11_Dynamicfiltersselector2, "CONTRATO_ANO") == 0 ) && ( ! (0==AV169WWContratoDS_17_Contrato_ano2) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_Ano] >= @AV169WWContratoDS_17_Contrato_ano2)";
         }
         else
         {
            GXv_int2[18] = 1;
         }
         if ( AV162WWContratoDS_10_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV163WWContratoDS_11_Dynamicfiltersselector2, "CONTRATO_ANO") == 0 ) && ( ! (0==AV170WWContratoDS_18_Contrato_ano_to2) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_Ano] <= @AV170WWContratoDS_18_Contrato_ano_to2)";
         }
         else
         {
            GXv_int2[19] = 1;
         }
         if ( AV171WWContratoDS_19_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV172WWContratoDS_20_Dynamicfiltersselector3, "CONTRATO_NUMERO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV174WWContratoDS_22_Contrato_numero3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_Numero] like @lV174WWContratoDS_22_Contrato_numero3)";
         }
         else
         {
            GXv_int2[20] = 1;
         }
         if ( AV171WWContratoDS_19_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV172WWContratoDS_20_Dynamicfiltersselector3, "CONTRATO_NUMEROATA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV175WWContratoDS_23_Contrato_numeroata3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_NumeroAta] like '%' + @lV175WWContratoDS_23_Contrato_numeroata3 + '%')";
         }
         else
         {
            GXv_int2[21] = 1;
         }
         if ( AV171WWContratoDS_19_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV172WWContratoDS_20_Dynamicfiltersselector3, "CONTRATO_PREPOSTONOM") == 0 ) && ( AV173WWContratoDS_21_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV176WWContratoDS_24_Contrato_prepostonom3)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV176WWContratoDS_24_Contrato_prepostonom3)";
         }
         else
         {
            GXv_int2[22] = 1;
         }
         if ( AV171WWContratoDS_19_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV172WWContratoDS_20_Dynamicfiltersselector3, "CONTRATO_PREPOSTONOM") == 0 ) && ( AV173WWContratoDS_21_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV176WWContratoDS_24_Contrato_prepostonom3)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV176WWContratoDS_24_Contrato_prepostonom3)";
         }
         else
         {
            GXv_int2[23] = 1;
         }
         if ( AV171WWContratoDS_19_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV172WWContratoDS_20_Dynamicfiltersselector3, "CONTRATADA_PESSOANOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV177WWContratoDS_25_Contratada_pessoanom3)) ) )
         {
            sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like '%' + @lV177WWContratoDS_25_Contratada_pessoanom3 + '%')";
         }
         else
         {
            GXv_int2[24] = 1;
         }
         if ( AV171WWContratoDS_19_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV172WWContratoDS_20_Dynamicfiltersselector3, "CONTRATO_ANO") == 0 ) && ( ! (0==AV178WWContratoDS_26_Contrato_ano3) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_Ano] >= @AV178WWContratoDS_26_Contrato_ano3)";
         }
         else
         {
            GXv_int2[25] = 1;
         }
         if ( AV171WWContratoDS_19_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV172WWContratoDS_20_Dynamicfiltersselector3, "CONTRATO_ANO") == 0 ) && ( ! (0==AV179WWContratoDS_27_Contrato_ano_to3) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_Ano] <= @AV179WWContratoDS_27_Contrato_ano_to3)";
         }
         else
         {
            GXv_int2[26] = 1;
         }
         if ( AV182WWContratoDS_30_Tfcontrato_ativo_sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_Ativo] = 1)";
         }
         if ( AV182WWContratoDS_30_Tfcontrato_ativo_sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_Ativo] = 0)";
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contrato_Numero]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contrato_Numero] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contrato_NumeroAta]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contrato_NumeroAta] DESC";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T5.[Pessoa_Nome]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T5.[Pessoa_Nome] DESC";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contrato_Ano]";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contrato_Ano] DESC";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T5.[Pessoa_Docto]";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T5.[Pessoa_Docto] DESC";
         }
         else if ( ( AV13OrderedBy == 6 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contrato_Ativo]";
         }
         else if ( ( AV13OrderedBy == 6 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contrato_Ativo] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contrato_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H006517( IGxContext context ,
                                              int AV153WWContratoDS_1_Contrato_areatrabalhocod ,
                                              int AV6WWPContext_gxTpr_Contratada_codigo ,
                                              String AV154WWContratoDS_2_Dynamicfiltersselector1 ,
                                              String AV156WWContratoDS_4_Contrato_numero1 ,
                                              String AV157WWContratoDS_5_Contrato_numeroata1 ,
                                              short AV155WWContratoDS_3_Dynamicfiltersoperator1 ,
                                              String AV158WWContratoDS_6_Contrato_prepostonom1 ,
                                              String AV159WWContratoDS_7_Contratada_pessoanom1 ,
                                              short AV160WWContratoDS_8_Contrato_ano1 ,
                                              short AV161WWContratoDS_9_Contrato_ano_to1 ,
                                              bool AV162WWContratoDS_10_Dynamicfiltersenabled2 ,
                                              String AV163WWContratoDS_11_Dynamicfiltersselector2 ,
                                              String AV165WWContratoDS_13_Contrato_numero2 ,
                                              String AV166WWContratoDS_14_Contrato_numeroata2 ,
                                              short AV164WWContratoDS_12_Dynamicfiltersoperator2 ,
                                              String AV167WWContratoDS_15_Contrato_prepostonom2 ,
                                              String AV168WWContratoDS_16_Contratada_pessoanom2 ,
                                              short AV169WWContratoDS_17_Contrato_ano2 ,
                                              short AV170WWContratoDS_18_Contrato_ano_to2 ,
                                              bool AV171WWContratoDS_19_Dynamicfiltersenabled3 ,
                                              String AV172WWContratoDS_20_Dynamicfiltersselector3 ,
                                              String AV174WWContratoDS_22_Contrato_numero3 ,
                                              String AV175WWContratoDS_23_Contrato_numeroata3 ,
                                              short AV173WWContratoDS_21_Dynamicfiltersoperator3 ,
                                              String AV176WWContratoDS_24_Contrato_prepostonom3 ,
                                              String AV177WWContratoDS_25_Contratada_pessoanom3 ,
                                              short AV178WWContratoDS_26_Contrato_ano3 ,
                                              short AV179WWContratoDS_27_Contrato_ano_to3 ,
                                              short AV182WWContratoDS_30_Tfcontrato_ativo_sel ,
                                              int A75Contrato_AreaTrabalhoCod ,
                                              int A39Contratada_Codigo ,
                                              String A77Contrato_Numero ,
                                              String A78Contrato_NumeroAta ,
                                              String A1015Contrato_PrepostoNom ,
                                              String A41Contratada_PessoaNom ,
                                              short A79Contrato_Ano ,
                                              bool A92Contrato_Ativo ,
                                              short AV13OrderedBy ,
                                              bool AV14OrderedDsc ,
                                              decimal AV180WWContratoDS_28_Tfcontrato_valorundcntatual ,
                                              decimal A1870Contrato_ValorUndCntAtual ,
                                              decimal AV181WWContratoDS_29_Tfcontrato_valorundcntatual_to )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [27] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM ((((((([Contrato] T1 WITH (NOLOCK) LEFT JOIN [Usuario] T4 WITH (NOLOCK) ON T4.[Usuario_Codigo] = T1.[Contrato_PrepostoCod]) LEFT JOIN [Pessoa] T5 WITH (NOLOCK) ON T5.[Pessoa_Codigo] = T4.[Usuario_PessoaCod]) INNER JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[Contratada_Codigo]) INNER JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Contratada_PessoaCod]) LEFT JOIN (SELECT CASE  WHEN COALESCE( T10.[GXC4], 0) > 0 THEN COALESCE( T10.[GXC4], 0) ELSE T9.[Contrato_ValorUnidadeContratacao] END AS Contrato_ValorUndCntAtual, T9.[Contrato_Codigo] FROM ([Contrato] T9 WITH (NOLOCK) LEFT JOIN (SELECT T11.[ContratoTermoAditivo_VlrUntUndCnt] AS GXC4, T11.[Contrato_Codigo], T11.[ContratoTermoAditivo_Codigo], T12.[GXC6] AS GXC6 FROM ([ContratoTermoAditivo] T11 WITH (NOLOCK) INNER JOIN (SELECT MAX([ContratoTermoAditivo_Codigo]) AS GXC6, [Contrato_Codigo] FROM [ContratoTermoAditivo] WITH (NOLOCK) GROUP BY [Contrato_Codigo] ) T12 ON T12.[Contrato_Codigo] = T11.[Contrato_Codigo]) WHERE T11.[ContratoTermoAditivo_Codigo] = T12.[GXC6] ) T10 ON T10.[Contrato_Codigo] = T9.[Contrato_Codigo]) ) T6 ON T6.[Contrato_Codigo] = T1.[Contrato_Codigo]) LEFT JOIN (SELECT T9.[ContratoTermoAditivo_DataFim], T9.[Contrato_Codigo], T9.[ContratoTermoAditivo_Codigo], T10.[GXC8] AS GXC8 FROM ([ContratoTermoAditivo] T9 WITH (NOLOCK) INNER JOIN (SELECT MAX([ContratoTermoAditivo_Codigo]) AS GXC8, [Contrato_Codigo] FROM [ContratoTermoAditivo] WITH (NOLOCK) GROUP BY [Contrato_Codigo] ) T10 ON T10.[Contrato_Codigo] = T9.[Contrato_Codigo]) WHERE T9.[ContratoTermoAditivo_Codigo] = T10.[GXC8] ) T7 ON T7.[Contrato_Codigo] = T1.[Contrato_Codigo]) LEFT JOIN (SELECT SUM([SaldoContrato_Saldo]) AS GXC1, [Contrato_Codigo] FROM [SaldoContrato] WITH (NOLOCK) GROUP BY [Contrato_Codigo]";
         scmdbuf = scmdbuf + " ) T8 ON T8.[Contrato_Codigo] = T1.[Contrato_Codigo])";
         scmdbuf = scmdbuf + " WHERE ((@AV180WWContratoDS_28_Tfcontrato_valorundcntatual = convert(int, 0)) or ( COALESCE( T6.[Contrato_ValorUndCntAtual], 0) >= @AV180WWContratoDS_28_Tfcontrato_valorundcntatual))";
         scmdbuf = scmdbuf + " and ((@AV181WWContratoDS_29_Tfcontrato_valorundcntatual_to = convert(int, 0)) or ( COALESCE( T6.[Contrato_ValorUndCntAtual], 0) <= @AV181WWContratoDS_29_Tfcontrato_valorundcntatual_to))";
         if ( ! (0==AV153WWContratoDS_1_Contrato_areatrabalhocod) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_AreaTrabalhoCod] = @AV153WWContratoDS_1_Contrato_areatrabalhocod)";
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( AV6WWPContext_gxTpr_Contratada_codigo > 0 )
         {
            sWhereString = sWhereString + " and (T1.[Contratada_Codigo] = @AV6WWPCo_1Contratada_codigo)";
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( ( StringUtil.StrCmp(AV154WWContratoDS_2_Dynamicfiltersselector1, "CONTRATO_NUMERO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV156WWContratoDS_4_Contrato_numero1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_Numero] like @lV156WWContratoDS_4_Contrato_numero1)";
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( ( StringUtil.StrCmp(AV154WWContratoDS_2_Dynamicfiltersselector1, "CONTRATO_NUMEROATA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV157WWContratoDS_5_Contrato_numeroata1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_NumeroAta] like '%' + @lV157WWContratoDS_5_Contrato_numeroata1 + '%')";
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( ( StringUtil.StrCmp(AV154WWContratoDS_2_Dynamicfiltersselector1, "CONTRATO_PREPOSTONOM") == 0 ) && ( AV155WWContratoDS_3_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV158WWContratoDS_6_Contrato_prepostonom1)) ) )
         {
            sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like @lV158WWContratoDS_6_Contrato_prepostonom1)";
         }
         else
         {
            GXv_int4[8] = 1;
         }
         if ( ( StringUtil.StrCmp(AV154WWContratoDS_2_Dynamicfiltersselector1, "CONTRATO_PREPOSTONOM") == 0 ) && ( AV155WWContratoDS_3_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV158WWContratoDS_6_Contrato_prepostonom1)) ) )
         {
            sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like '%' + @lV158WWContratoDS_6_Contrato_prepostonom1)";
         }
         else
         {
            GXv_int4[9] = 1;
         }
         if ( ( StringUtil.StrCmp(AV154WWContratoDS_2_Dynamicfiltersselector1, "CONTRATADA_PESSOANOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV159WWContratoDS_7_Contratada_pessoanom1)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV159WWContratoDS_7_Contratada_pessoanom1 + '%')";
         }
         else
         {
            GXv_int4[10] = 1;
         }
         if ( ( StringUtil.StrCmp(AV154WWContratoDS_2_Dynamicfiltersselector1, "CONTRATO_ANO") == 0 ) && ( ! (0==AV160WWContratoDS_8_Contrato_ano1) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_Ano] >= @AV160WWContratoDS_8_Contrato_ano1)";
         }
         else
         {
            GXv_int4[11] = 1;
         }
         if ( ( StringUtil.StrCmp(AV154WWContratoDS_2_Dynamicfiltersselector1, "CONTRATO_ANO") == 0 ) && ( ! (0==AV161WWContratoDS_9_Contrato_ano_to1) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_Ano] <= @AV161WWContratoDS_9_Contrato_ano_to1)";
         }
         else
         {
            GXv_int4[12] = 1;
         }
         if ( AV162WWContratoDS_10_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV163WWContratoDS_11_Dynamicfiltersselector2, "CONTRATO_NUMERO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV165WWContratoDS_13_Contrato_numero2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_Numero] like @lV165WWContratoDS_13_Contrato_numero2)";
         }
         else
         {
            GXv_int4[13] = 1;
         }
         if ( AV162WWContratoDS_10_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV163WWContratoDS_11_Dynamicfiltersselector2, "CONTRATO_NUMEROATA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV166WWContratoDS_14_Contrato_numeroata2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_NumeroAta] like '%' + @lV166WWContratoDS_14_Contrato_numeroata2 + '%')";
         }
         else
         {
            GXv_int4[14] = 1;
         }
         if ( AV162WWContratoDS_10_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV163WWContratoDS_11_Dynamicfiltersselector2, "CONTRATO_PREPOSTONOM") == 0 ) && ( AV164WWContratoDS_12_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV167WWContratoDS_15_Contrato_prepostonom2)) ) )
         {
            sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like @lV167WWContratoDS_15_Contrato_prepostonom2)";
         }
         else
         {
            GXv_int4[15] = 1;
         }
         if ( AV162WWContratoDS_10_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV163WWContratoDS_11_Dynamicfiltersselector2, "CONTRATO_PREPOSTONOM") == 0 ) && ( AV164WWContratoDS_12_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV167WWContratoDS_15_Contrato_prepostonom2)) ) )
         {
            sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like '%' + @lV167WWContratoDS_15_Contrato_prepostonom2)";
         }
         else
         {
            GXv_int4[16] = 1;
         }
         if ( AV162WWContratoDS_10_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV163WWContratoDS_11_Dynamicfiltersselector2, "CONTRATADA_PESSOANOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV168WWContratoDS_16_Contratada_pessoanom2)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV168WWContratoDS_16_Contratada_pessoanom2 + '%')";
         }
         else
         {
            GXv_int4[17] = 1;
         }
         if ( AV162WWContratoDS_10_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV163WWContratoDS_11_Dynamicfiltersselector2, "CONTRATO_ANO") == 0 ) && ( ! (0==AV169WWContratoDS_17_Contrato_ano2) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_Ano] >= @AV169WWContratoDS_17_Contrato_ano2)";
         }
         else
         {
            GXv_int4[18] = 1;
         }
         if ( AV162WWContratoDS_10_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV163WWContratoDS_11_Dynamicfiltersselector2, "CONTRATO_ANO") == 0 ) && ( ! (0==AV170WWContratoDS_18_Contrato_ano_to2) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_Ano] <= @AV170WWContratoDS_18_Contrato_ano_to2)";
         }
         else
         {
            GXv_int4[19] = 1;
         }
         if ( AV171WWContratoDS_19_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV172WWContratoDS_20_Dynamicfiltersselector3, "CONTRATO_NUMERO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV174WWContratoDS_22_Contrato_numero3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_Numero] like @lV174WWContratoDS_22_Contrato_numero3)";
         }
         else
         {
            GXv_int4[20] = 1;
         }
         if ( AV171WWContratoDS_19_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV172WWContratoDS_20_Dynamicfiltersselector3, "CONTRATO_NUMEROATA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV175WWContratoDS_23_Contrato_numeroata3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_NumeroAta] like '%' + @lV175WWContratoDS_23_Contrato_numeroata3 + '%')";
         }
         else
         {
            GXv_int4[21] = 1;
         }
         if ( AV171WWContratoDS_19_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV172WWContratoDS_20_Dynamicfiltersselector3, "CONTRATO_PREPOSTONOM") == 0 ) && ( AV173WWContratoDS_21_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV176WWContratoDS_24_Contrato_prepostonom3)) ) )
         {
            sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like @lV176WWContratoDS_24_Contrato_prepostonom3)";
         }
         else
         {
            GXv_int4[22] = 1;
         }
         if ( AV171WWContratoDS_19_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV172WWContratoDS_20_Dynamicfiltersselector3, "CONTRATO_PREPOSTONOM") == 0 ) && ( AV173WWContratoDS_21_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV176WWContratoDS_24_Contrato_prepostonom3)) ) )
         {
            sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like '%' + @lV176WWContratoDS_24_Contrato_prepostonom3)";
         }
         else
         {
            GXv_int4[23] = 1;
         }
         if ( AV171WWContratoDS_19_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV172WWContratoDS_20_Dynamicfiltersselector3, "CONTRATADA_PESSOANOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV177WWContratoDS_25_Contratada_pessoanom3)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV177WWContratoDS_25_Contratada_pessoanom3 + '%')";
         }
         else
         {
            GXv_int4[24] = 1;
         }
         if ( AV171WWContratoDS_19_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV172WWContratoDS_20_Dynamicfiltersselector3, "CONTRATO_ANO") == 0 ) && ( ! (0==AV178WWContratoDS_26_Contrato_ano3) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_Ano] >= @AV178WWContratoDS_26_Contrato_ano3)";
         }
         else
         {
            GXv_int4[25] = 1;
         }
         if ( AV171WWContratoDS_19_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV172WWContratoDS_20_Dynamicfiltersselector3, "CONTRATO_ANO") == 0 ) && ( ! (0==AV179WWContratoDS_27_Contrato_ano_to3) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_Ano] <= @AV179WWContratoDS_27_Contrato_ano_to3)";
         }
         else
         {
            GXv_int4[26] = 1;
         }
         if ( AV182WWContratoDS_30_Tfcontrato_ativo_sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_Ativo] = 1)";
         }
         if ( AV182WWContratoDS_30_Tfcontrato_ativo_sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[Contrato_Ativo] = 0)";
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 6 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 6 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 2 :
                     return conditional_H006510(context, (int)dynConstraints[0] , (int)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (short)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (short)dynConstraints[8] , (short)dynConstraints[9] , (bool)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (short)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (short)dynConstraints[17] , (short)dynConstraints[18] , (bool)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (short)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (short)dynConstraints[26] , (short)dynConstraints[27] , (short)dynConstraints[28] , (int)dynConstraints[29] , (int)dynConstraints[30] , (String)dynConstraints[31] , (String)dynConstraints[32] , (String)dynConstraints[33] , (String)dynConstraints[34] , (short)dynConstraints[35] , (bool)dynConstraints[36] , (short)dynConstraints[37] , (bool)dynConstraints[38] , (decimal)dynConstraints[39] , (decimal)dynConstraints[40] , (decimal)dynConstraints[41] );
               case 3 :
                     return conditional_H006517(context, (int)dynConstraints[0] , (int)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (short)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (short)dynConstraints[8] , (short)dynConstraints[9] , (bool)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (short)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (short)dynConstraints[17] , (short)dynConstraints[18] , (bool)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (short)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (short)dynConstraints[26] , (short)dynConstraints[27] , (short)dynConstraints[28] , (int)dynConstraints[29] , (int)dynConstraints[30] , (String)dynConstraints[31] , (String)dynConstraints[32] , (String)dynConstraints[33] , (String)dynConstraints[34] , (short)dynConstraints[35] , (bool)dynConstraints[36] , (short)dynConstraints[37] , (bool)dynConstraints[38] , (decimal)dynConstraints[39] , (decimal)dynConstraints[40] , (decimal)dynConstraints[41] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00652 ;
          prmH00652 = new Object[] {
          } ;
          Object[] prmH00653 ;
          prmH00653 = new Object[] {
          } ;
          Object[] prmH006510 ;
          prmH006510 = new Object[] {
          new Object[] {"@AV180WWContratoDS_28_Tfcontrato_valorundcntatual",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV180WWContratoDS_28_Tfcontrato_valorundcntatual",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV181WWContratoDS_29_Tfcontrato_valorundcntatual_to",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV181WWContratoDS_29_Tfcontrato_valorundcntatual_to",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV153WWContratoDS_1_Contrato_areatrabalhocod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV6WWPCo_1Contratada_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV156WWContratoDS_4_Contrato_numero1",SqlDbType.Char,20,0} ,
          new Object[] {"@lV157WWContratoDS_5_Contrato_numeroata1",SqlDbType.Char,10,0} ,
          new Object[] {"@lV158WWContratoDS_6_Contrato_prepostonom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV158WWContratoDS_6_Contrato_prepostonom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV159WWContratoDS_7_Contratada_pessoanom1",SqlDbType.Char,100,0} ,
          new Object[] {"@AV160WWContratoDS_8_Contrato_ano1",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV161WWContratoDS_9_Contrato_ano_to1",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@lV165WWContratoDS_13_Contrato_numero2",SqlDbType.Char,20,0} ,
          new Object[] {"@lV166WWContratoDS_14_Contrato_numeroata2",SqlDbType.Char,10,0} ,
          new Object[] {"@lV167WWContratoDS_15_Contrato_prepostonom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV167WWContratoDS_15_Contrato_prepostonom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV168WWContratoDS_16_Contratada_pessoanom2",SqlDbType.Char,100,0} ,
          new Object[] {"@AV169WWContratoDS_17_Contrato_ano2",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV170WWContratoDS_18_Contrato_ano_to2",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@lV174WWContratoDS_22_Contrato_numero3",SqlDbType.Char,20,0} ,
          new Object[] {"@lV175WWContratoDS_23_Contrato_numeroata3",SqlDbType.Char,10,0} ,
          new Object[] {"@lV176WWContratoDS_24_Contrato_prepostonom3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV176WWContratoDS_24_Contrato_prepostonom3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV177WWContratoDS_25_Contratada_pessoanom3",SqlDbType.Char,100,0} ,
          new Object[] {"@AV178WWContratoDS_26_Contrato_ano3",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV179WWContratoDS_27_Contrato_ano_to3",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH006517 ;
          prmH006517 = new Object[] {
          new Object[] {"@AV180WWContratoDS_28_Tfcontrato_valorundcntatual",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV180WWContratoDS_28_Tfcontrato_valorundcntatual",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV181WWContratoDS_29_Tfcontrato_valorundcntatual_to",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV181WWContratoDS_29_Tfcontrato_valorundcntatual_to",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV153WWContratoDS_1_Contrato_areatrabalhocod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV6WWPCo_1Contratada_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV156WWContratoDS_4_Contrato_numero1",SqlDbType.Char,20,0} ,
          new Object[] {"@lV157WWContratoDS_5_Contrato_numeroata1",SqlDbType.Char,10,0} ,
          new Object[] {"@lV158WWContratoDS_6_Contrato_prepostonom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV158WWContratoDS_6_Contrato_prepostonom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV159WWContratoDS_7_Contratada_pessoanom1",SqlDbType.Char,100,0} ,
          new Object[] {"@AV160WWContratoDS_8_Contrato_ano1",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV161WWContratoDS_9_Contrato_ano_to1",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@lV165WWContratoDS_13_Contrato_numero2",SqlDbType.Char,20,0} ,
          new Object[] {"@lV166WWContratoDS_14_Contrato_numeroata2",SqlDbType.Char,10,0} ,
          new Object[] {"@lV167WWContratoDS_15_Contrato_prepostonom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV167WWContratoDS_15_Contrato_prepostonom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV168WWContratoDS_16_Contratada_pessoanom2",SqlDbType.Char,100,0} ,
          new Object[] {"@AV169WWContratoDS_17_Contrato_ano2",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV170WWContratoDS_18_Contrato_ano_to2",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@lV174WWContratoDS_22_Contrato_numero3",SqlDbType.Char,20,0} ,
          new Object[] {"@lV175WWContratoDS_23_Contrato_numeroata3",SqlDbType.Char,10,0} ,
          new Object[] {"@lV176WWContratoDS_24_Contrato_prepostonom3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV176WWContratoDS_24_Contrato_prepostonom3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV177WWContratoDS_25_Contratada_pessoanom3",SqlDbType.Char,100,0} ,
          new Object[] {"@AV178WWContratoDS_26_Contrato_ano3",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV179WWContratoDS_27_Contrato_ano_to3",SqlDbType.SmallInt,4,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00652", "SELECT [AreaTrabalho_Codigo], [AreaTrabalho_Descricao] FROM [AreaTrabalho] WITH (NOLOCK) ORDER BY [AreaTrabalho_Descricao] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00652,0,0,true,false )
             ,new CursorDef("H00653", "SELECT [AreaTrabalho_Codigo], [AreaTrabalho_Descricao] FROM [AreaTrabalho] WITH (NOLOCK) ORDER BY [AreaTrabalho_Descricao] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00653,0,0,true,false )
             ,new CursorDef("H006510", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH006510,11,0,true,false )
             ,new CursorDef("H006517", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH006517,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getString(4, 100) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                ((int[]) buf[8])[0] = rslt.getInt(6) ;
                ((bool[]) buf[9])[0] = rslt.getBool(7) ;
                ((String[]) buf[10])[0] = rslt.getVarchar(8) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(8);
                ((short[]) buf[12])[0] = rslt.getShort(9) ;
                ((String[]) buf[13])[0] = rslt.getString(10, 10) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(10);
                ((String[]) buf[15])[0] = rslt.getString(11, 20) ;
                ((String[]) buf[16])[0] = rslt.getString(12, 100) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(12);
                ((int[]) buf[18])[0] = rslt.getInt(13) ;
                ((decimal[]) buf[19])[0] = rslt.getDecimal(14) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(14);
                ((decimal[]) buf[21])[0] = rslt.getDecimal(15) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(15);
                ((DateTime[]) buf[23])[0] = rslt.getGXDate(16) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(16);
                ((DateTime[]) buf[25])[0] = rslt.getGXDate(17) ;
                return;
             case 3 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[32]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[33]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[34]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[35]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[36]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[37]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[43]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[44]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[48]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[49]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[50]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[51]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[52]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[53]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[54]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[55]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[56]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[57]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[58]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[59]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[60]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[61]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[62]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[63]);
                }
                return;
             case 3 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[27]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[28]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[29]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[30]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[31]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[32]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[38]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[39]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[45]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[46]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[48]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[49]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[50]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[51]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[52]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[53]);
                }
                return;
       }
    }

 }

}
