/*
               File: WC_ContagemResultado_Contagens
        Description: Contagem Resultado_Contagens
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:11:20.21
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wc_contagemresultado_contagens : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public wc_contagemresultado_contagens( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public wc_contagemresultado_contagens( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_ContagemResultado_Codigo )
      {
         this.A456ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         executePrivate();
         aP0_ContagemResultado_Codigo=this.A456ContagemResultado_Codigo;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
         cmbContagemResultado_StatusCnt = new GXCombobox();
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  A456ContagemResultado_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)A456ContagemResultado_Codigo});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
               {
                  nRC_GXsfl_8 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  nGXsfl_8_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  sGXsfl_8_idx = GetNextPar( );
                  sPrefix = GetNextPar( );
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxnrGrid_newrow( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
               {
                  subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  A456ContagemResultado_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  sPrefix = GetNextPar( );
                  init_default_properties( ) ;
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxgrGrid_refresh( subGrid_Rows, A456ContagemResultado_Codigo, sPrefix) ;
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
                  return  ;
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PAB02( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               context.Gx_err = 0;
               /* Using cursor H00B02 */
               pr_default.execute(0, new Object[] {A456ContagemResultado_Codigo});
               A457ContagemResultado_Demanda = H00B02_A457ContagemResultado_Demanda[0];
               n457ContagemResultado_Demanda = H00B02_n457ContagemResultado_Demanda[0];
               pr_default.close(0);
               WSB02( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( Form.Caption) ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20206216112029");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = ((nGXWrapped==0) ? " data-HasEnter=\"false\" data-Skiponenter=\"false\"" : "");
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            if ( nGXWrapped != 1 )
            {
               context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wc_contagemresultado_contagens.aspx") + "?" + UrlEncode("" +A456ContagemResultado_Codigo)+"\">") ;
               GxWebStd.gx_hidden_field( context, "_EventName", "");
               GxWebStd.gx_hidden_field( context, "_EventGridId", "");
               GxWebStd.gx_hidden_field( context, "_EventRowId", "");
               context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
            }
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"nRC_GXsfl_8", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_8), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV8GridPageCount), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOA456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOA456ContagemResultado_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormB02( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("wc_contagemresultado_contagens.js", "?20206216112054");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( nGXWrapped != 1 )
            {
               context.WriteHtmlTextNl( "</form>") ;
            }
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "WC_ContagemResultado_Contagens" ;
      }

      public override String GetPgmdesc( )
      {
         return "Contagem Resultado_Contagens" ;
      }

      protected void WBB00( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "wc_contagemresultado_contagens.aspx");
               context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
            }
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", sPrefix, "false");
            wb_table1_2_B02( true) ;
         }
         else
         {
            wb_table1_2_B02( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_B02e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void STARTB02( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Contagem Resultado_Contagens", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPB00( ) ;
            }
         }
      }

      protected void WSB02( )
      {
         STARTB02( ) ;
         EVTB02( ) ;
      }

      protected void EVTB02( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPB00( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPB00( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11B02 */
                                    E11B02 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPB00( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    GX_FocusControl = edtavGridcurrentpage_Internalname;
                                    context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPB00( ) ;
                              }
                              nGXsfl_8_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_8_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_8_idx), 4, 0)), 4, "0");
                              SubsflControlProps_82( ) ;
                              if ( StringUtil.Len( sPrefix) == 0 )
                              {
                                 A456ContagemResultado_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContagemResultado_Codigo_Internalname), ",", "."));
                              }
                              A473ContagemResultado_DataCnt = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtContagemResultado_DataCnt_Internalname), 0));
                              A511ContagemResultado_HoraCnt = cgiGet( edtContagemResultado_HoraCnt_Internalname);
                              A474ContagemResultado_ContadorFMNom = StringUtil.Upper( cgiGet( edtContagemResultado_ContadorFMNom_Internalname));
                              n474ContagemResultado_ContadorFMNom = false;
                              A482ContagemResultadoContagens_Esforco = (short)(context.localUtil.CToN( cgiGet( edtContagemResultadoContagens_Esforco_Internalname), ",", "."));
                              A458ContagemResultado_PFBFS = context.localUtil.CToN( cgiGet( edtContagemResultado_PFBFS_Internalname), ",", ".");
                              n458ContagemResultado_PFBFS = false;
                              A459ContagemResultado_PFLFS = context.localUtil.CToN( cgiGet( edtContagemResultado_PFLFS_Internalname), ",", ".");
                              n459ContagemResultado_PFLFS = false;
                              A460ContagemResultado_PFBFM = context.localUtil.CToN( cgiGet( edtContagemResultado_PFBFM_Internalname), ",", ".");
                              n460ContagemResultado_PFBFM = false;
                              A461ContagemResultado_PFLFM = context.localUtil.CToN( cgiGet( edtContagemResultado_PFLFM_Internalname), ",", ".");
                              n461ContagemResultado_PFLFM = false;
                              A462ContagemResultado_Divergencia = context.localUtil.CToN( cgiGet( edtContagemResultado_Divergencia_Internalname), ",", ".");
                              cmbContagemResultado_StatusCnt.Name = cmbContagemResultado_StatusCnt_Internalname;
                              cmbContagemResultado_StatusCnt.CurrentValue = cgiGet( cmbContagemResultado_StatusCnt_Internalname);
                              A483ContagemResultado_StatusCnt = (short)(NumberUtil.Val( cgiGet( cmbContagemResultado_StatusCnt_Internalname), "."));
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavGridcurrentpage_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E12B02 */
                                          E12B02 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavGridcurrentpage_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E13B02 */
                                          E13B02 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavGridcurrentpage_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E14B02 */
                                          E14B02 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          if ( ! wbErr )
                                          {
                                             Rfr0gs = false;
                                             if ( ! Rfr0gs )
                                             {
                                             }
                                             dynload_actions( ) ;
                                          }
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavGridcurrentpage_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                                    {
                                       STRUPB00( ) ;
                                    }
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavGridcurrentpage_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEB02( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormB02( ) ;
            }
         }
      }

      protected void PAB02( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            GXCCtl = "CONTAGEMRESULTADO_STATUSCNT_" + sGXsfl_8_idx;
            cmbContagemResultado_StatusCnt.Name = GXCCtl;
            cmbContagemResultado_StatusCnt.WebTags = "";
            cmbContagemResultado_StatusCnt.addItem("1", "Inicial", 0);
            cmbContagemResultado_StatusCnt.addItem("2", "Auditoria", 0);
            cmbContagemResultado_StatusCnt.addItem("3", "Negociac�o", 0);
            cmbContagemResultado_StatusCnt.addItem("4", "N�o Aprovada", 0);
            cmbContagemResultado_StatusCnt.addItem("5", "Entregue", 0);
            cmbContagemResultado_StatusCnt.addItem("6", "Pend�ncias", 0);
            cmbContagemResultado_StatusCnt.addItem("7", "Diverg�ncia", 0);
            cmbContagemResultado_StatusCnt.addItem("8", "Aprovada", 0);
            if ( cmbContagemResultado_StatusCnt.ItemCount > 0 )
            {
               A483ContagemResultado_StatusCnt = (short)(NumberUtil.Val( cmbContagemResultado_StatusCnt.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A483ContagemResultado_StatusCnt), 2, 0))), "."));
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavGridcurrentpage_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_82( ) ;
         while ( nGXsfl_8_idx <= nRC_GXsfl_8 )
         {
            sendrow_82( ) ;
            nGXsfl_8_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_8_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_8_idx+1));
            sGXsfl_8_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_8_idx), 4, 0)), 4, "0");
            SubsflControlProps_82( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       int A456ContagemResultado_Codigo ,
                                       String sPrefix )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFB02( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADO_DATACNT", GetSecureSignedToken( sPrefix, A473ContagemResultado_DataCnt));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADO_DATACNT", context.localUtil.Format(A473ContagemResultado_DataCnt, "99/99/99"));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADO_HORACNT", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A511ContagemResultado_HoraCnt, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADO_HORACNT", StringUtil.RTrim( A511ContagemResultado_HoraCnt));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADOCONTAGENS_ESFORCO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A482ContagemResultadoContagens_Esforco), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADOCONTAGENS_ESFORCO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A482ContagemResultadoContagens_Esforco), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADO_PFBFS", GetSecureSignedToken( sPrefix, context.localUtil.Format( A458ContagemResultado_PFBFS, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADO_PFBFS", StringUtil.LTrim( StringUtil.NToC( A458ContagemResultado_PFBFS, 14, 5, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADO_PFLFS", GetSecureSignedToken( sPrefix, context.localUtil.Format( A459ContagemResultado_PFLFS, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADO_PFLFS", StringUtil.LTrim( StringUtil.NToC( A459ContagemResultado_PFLFS, 14, 5, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADO_PFBFM", GetSecureSignedToken( sPrefix, context.localUtil.Format( A460ContagemResultado_PFBFM, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADO_PFBFM", StringUtil.LTrim( StringUtil.NToC( A460ContagemResultado_PFBFM, 14, 5, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADO_PFLFM", GetSecureSignedToken( sPrefix, context.localUtil.Format( A461ContagemResultado_PFLFM, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADO_PFLFM", StringUtil.LTrim( StringUtil.NToC( A461ContagemResultado_PFLFM, 14, 5, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADO_DIVERGENCIA", GetSecureSignedToken( sPrefix, context.localUtil.Format( A462ContagemResultado_Divergencia, "ZZ9.99")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADO_DIVERGENCIA", StringUtil.LTrim( StringUtil.NToC( A462ContagemResultado_Divergencia, 6, 2, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADO_STATUSCNT", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A483ContagemResultado_StatusCnt), "Z9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADO_STATUSCNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(A483ContagemResultado_StatusCnt), 2, 0, ".", "")));
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFB02( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      protected void RFB02( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 8;
         /* Execute user event: E13B02 */
         E13B02 ();
         nGXsfl_8_idx = 1;
         sGXsfl_8_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_8_idx), 4, 0)), 4, "0");
         SubsflControlProps_82( ) ;
         nGXsfl_8_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", sPrefix);
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_82( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            /* Using cursor H00B03 */
            pr_default.execute(1, new Object[] {A456ContagemResultado_Codigo, GXPagingFrom2, GXPagingTo2});
            nGXsfl_8_idx = 1;
            while ( ( (pr_default.getStatus(1) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A470ContagemResultado_ContadorFMCod = H00B03_A470ContagemResultado_ContadorFMCod[0];
               A479ContagemResultado_CrFMPessoaCod = H00B03_A479ContagemResultado_CrFMPessoaCod[0];
               n479ContagemResultado_CrFMPessoaCod = H00B03_n479ContagemResultado_CrFMPessoaCod[0];
               A483ContagemResultado_StatusCnt = H00B03_A483ContagemResultado_StatusCnt[0];
               A462ContagemResultado_Divergencia = H00B03_A462ContagemResultado_Divergencia[0];
               A461ContagemResultado_PFLFM = H00B03_A461ContagemResultado_PFLFM[0];
               n461ContagemResultado_PFLFM = H00B03_n461ContagemResultado_PFLFM[0];
               A460ContagemResultado_PFBFM = H00B03_A460ContagemResultado_PFBFM[0];
               n460ContagemResultado_PFBFM = H00B03_n460ContagemResultado_PFBFM[0];
               A459ContagemResultado_PFLFS = H00B03_A459ContagemResultado_PFLFS[0];
               n459ContagemResultado_PFLFS = H00B03_n459ContagemResultado_PFLFS[0];
               A458ContagemResultado_PFBFS = H00B03_A458ContagemResultado_PFBFS[0];
               n458ContagemResultado_PFBFS = H00B03_n458ContagemResultado_PFBFS[0];
               A482ContagemResultadoContagens_Esforco = H00B03_A482ContagemResultadoContagens_Esforco[0];
               A474ContagemResultado_ContadorFMNom = H00B03_A474ContagemResultado_ContadorFMNom[0];
               n474ContagemResultado_ContadorFMNom = H00B03_n474ContagemResultado_ContadorFMNom[0];
               A511ContagemResultado_HoraCnt = H00B03_A511ContagemResultado_HoraCnt[0];
               A473ContagemResultado_DataCnt = H00B03_A473ContagemResultado_DataCnt[0];
               A479ContagemResultado_CrFMPessoaCod = H00B03_A479ContagemResultado_CrFMPessoaCod[0];
               n479ContagemResultado_CrFMPessoaCod = H00B03_n479ContagemResultado_CrFMPessoaCod[0];
               A474ContagemResultado_ContadorFMNom = H00B03_A474ContagemResultado_ContadorFMNom[0];
               n474ContagemResultado_ContadorFMNom = H00B03_n474ContagemResultado_ContadorFMNom[0];
               /* Execute user event: E14B02 */
               E14B02 ();
               pr_default.readNext(1);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(1) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(1);
            wbEnd = 8;
            WBB00( ) ;
         }
         nGXsfl_8_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         /* Using cursor H00B04 */
         pr_default.execute(2, new Object[] {A456ContagemResultado_Codigo});
         GRID_nRecordCount = H00B04_AGRID_nRecordCount[0];
         pr_default.close(2);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, A456ContagemResultado_Codigo, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, A456ContagemResultado_Codigo, sPrefix) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, A456ContagemResultado_Codigo, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, A456ContagemResultado_Codigo, sPrefix) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, A456ContagemResultado_Codigo, sPrefix) ;
         }
         return (int)(0) ;
      }

      protected void STRUPB00( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         /* Using cursor H00B05 */
         pr_default.execute(3, new Object[] {A456ContagemResultado_Codigo});
         A457ContagemResultado_Demanda = H00B05_A457ContagemResultado_Demanda[0];
         n457ContagemResultado_Demanda = H00B05_n457ContagemResultado_Demanda[0];
         pr_default.close(3);
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E12B02 */
         E12B02 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            if ( ( ( context.localUtil.CToN( cgiGet( edtavGridcurrentpage_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavGridcurrentpage_Internalname), ",", ".") > Convert.ToDecimal( 9999999999L )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vGRIDCURRENTPAGE");
               GX_FocusControl = edtavGridcurrentpage_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV7GridCurrentPage = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7GridCurrentPage), 10, 0)));
            }
            else
            {
               AV7GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( edtavGridcurrentpage_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7GridCurrentPage), 10, 0)));
            }
            /* Read saved values. */
            nRC_GXsfl_8 = (short)(context.localUtil.CToN( cgiGet( sPrefix+"nRC_GXsfl_8"), ",", "."));
            AV8GridPageCount = (long)(context.localUtil.CToN( cgiGet( sPrefix+"vGRIDPAGECOUNT"), ",", "."));
            wcpOA456ContagemResultado_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA456ContagemResultado_Codigo"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( sPrefix+"GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Emptygridcaption");
            Gridpaginationbar_Selectedpage = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Selectedpage");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E12B02 */
         E12B02 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E12B02( )
      {
         /* Start Routine */
         Form.Caption = "Contagens da demanda "+StringUtil.Trim( A457ContagemResultado_Demanda);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, "FORM", "Caption", Form.Caption);
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         AV7GridCurrentPage = 1;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7GridCurrentPage), 10, 0)));
         edtavGridcurrentpage_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavGridcurrentpage_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavGridcurrentpage_Visible), 5, 0)));
         AV8GridPageCount = -1;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV8GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8GridPageCount), 10, 0)));
         lblTbjava_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblTbjava_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTbjava_Visible), 5, 0)));
         lblTbjava_Caption = "<script type='text/javascript'> document.body.style.overflow = 'hidden'; </script>";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblTbjava_Internalname, "Caption", lblTbjava_Caption);
      }

      protected void E13B02( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV5WWPContext) ;
         edtContagemResultado_ContadorFMNom_Visible = (!AV5WWPContext.gxTpr_Userehcontratante ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContagemResultado_ContadorFMNom_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultado_ContadorFMNom_Visible), 5, 0)));
      }

      private void E14B02( )
      {
         /* Grid_Load Routine */
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 8;
         }
         sendrow_82( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_8_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(8, GridRow);
         }
      }

      protected void E11B02( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            AV7GridCurrentPage = (long)(AV7GridCurrentPage-1);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7GridCurrentPage), 10, 0)));
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            AV7GridCurrentPage = (long)(AV7GridCurrentPage+1);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7GridCurrentPage), 10, 0)));
            subgrid_nextpage( ) ;
         }
         else
         {
            AV6PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            AV7GridCurrentPage = AV6PageToGo;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7GridCurrentPage), 10, 0)));
            subgrid_gotopage( AV6PageToGo) ;
         }
         context.DoAjaxRefreshCmp(sPrefix);
      }

      protected void wb_table1_2_B02( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            wb_table2_5_B02( true) ;
         }
         else
         {
            wb_table2_5_B02( false) ;
         }
         return  ;
      }

      protected void wb_table2_5_B02e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbjava_Internalname, lblTbjava_Caption, "", "", lblTbjava_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", lblTbjava_Visible, 1, 1, "HLP_WC_ContagemResultado_Contagens.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_B02e( true) ;
         }
         else
         {
            wb_table1_2_B02e( false) ;
         }
      }

      protected void wb_table2_5_B02( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"DivS\" data-gxgridid=\"8\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Contagem Resultado_Codigo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Data") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Hora") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(250), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtContagemResultado_ContadorFMNom_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "Contador") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(45), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Esfor�o") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(94), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "PFB FS") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(94), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "PFL FS") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(94), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "PFB FM") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(94), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "PFL FM") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Diverg�ncia") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Status") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", sPrefix);
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A456ContagemResultado_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.Format(A473ContagemResultado_DataCnt, "99/99/99"));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A511ContagemResultado_HoraCnt));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A474ContagemResultado_ContadorFMNom));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContagemResultado_ContadorFMNom_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A482ContagemResultadoContagens_Esforco), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A458ContagemResultado_PFBFS, 14, 5, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A459ContagemResultado_PFLFS, 14, 5, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A460ContagemResultado_PFBFM, 14, 5, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A461ContagemResultado_PFLFM, 14, 5, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A462ContagemResultado_Divergencia, 6, 2, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A483ContagemResultado_StatusCnt), 2, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 8 )
         {
            wbEnd = 0;
            nRC_GXsfl_8 = (short)(nGXsfl_8_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid(sPrefix+"_"+"Grid", GridContainer);
               if ( ! isAjaxCallMode( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+sPrefix+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'" + sPrefix + "',false,'" + sGXsfl_8_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavGridcurrentpage_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7GridCurrentPage), 10, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV7GridCurrentPage), "ZZZZZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,23);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavGridcurrentpage_Jsonclick, 0, "Attribute", "", "", "", edtavGridcurrentpage_Visible, 1, 0, "text", "", 10, "chr", 1, "row", 10, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WC_ContagemResultado_Contagens.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_B02e( true) ;
         }
         else
         {
            wb_table2_5_B02e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         A456ContagemResultado_Codigo = Convert.ToInt32(getParm(obj,0));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAB02( ) ;
         WSB02( ) ;
         WEB02( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlA456ContagemResultado_Codigo = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PAB02( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "wc_contagemresultado_contagens");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PAB02( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            A456ContagemResultado_Codigo = Convert.ToInt32(getParm(obj,2));
         }
         wcpOA456ContagemResultado_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA456ContagemResultado_Codigo"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( A456ContagemResultado_Codigo != wcpOA456ContagemResultado_Codigo ) ) )
         {
            setjustcreated();
         }
         wcpOA456ContagemResultado_Codigo = A456ContagemResultado_Codigo;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlA456ContagemResultado_Codigo = cgiGet( sPrefix+"A456ContagemResultado_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlA456ContagemResultado_Codigo) > 0 )
         {
            A456ContagemResultado_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlA456ContagemResultado_Codigo), ",", "."));
         }
         else
         {
            A456ContagemResultado_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"A456ContagemResultado_Codigo_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PAB02( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSB02( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSB02( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"A456ContagemResultado_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(A456ContagemResultado_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlA456ContagemResultado_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"A456ContagemResultado_Codigo_CTRL", StringUtil.RTrim( sCtrlA456ContagemResultado_Codigo));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WEB02( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20206216112140");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         if ( nGXWrapped != 1 )
         {
            context.AddJavascriptSource("wc_contagemresultado_contagens.js", "?20206216112140");
            context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         }
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_82( )
      {
         edtContagemResultado_Codigo_Internalname = sPrefix+"CONTAGEMRESULTADO_CODIGO_"+sGXsfl_8_idx;
         edtContagemResultado_DataCnt_Internalname = sPrefix+"CONTAGEMRESULTADO_DATACNT_"+sGXsfl_8_idx;
         edtContagemResultado_HoraCnt_Internalname = sPrefix+"CONTAGEMRESULTADO_HORACNT_"+sGXsfl_8_idx;
         edtContagemResultado_ContadorFMNom_Internalname = sPrefix+"CONTAGEMRESULTADO_CONTADORFMNOM_"+sGXsfl_8_idx;
         edtContagemResultadoContagens_Esforco_Internalname = sPrefix+"CONTAGEMRESULTADOCONTAGENS_ESFORCO_"+sGXsfl_8_idx;
         edtContagemResultado_PFBFS_Internalname = sPrefix+"CONTAGEMRESULTADO_PFBFS_"+sGXsfl_8_idx;
         edtContagemResultado_PFLFS_Internalname = sPrefix+"CONTAGEMRESULTADO_PFLFS_"+sGXsfl_8_idx;
         edtContagemResultado_PFBFM_Internalname = sPrefix+"CONTAGEMRESULTADO_PFBFM_"+sGXsfl_8_idx;
         edtContagemResultado_PFLFM_Internalname = sPrefix+"CONTAGEMRESULTADO_PFLFM_"+sGXsfl_8_idx;
         edtContagemResultado_Divergencia_Internalname = sPrefix+"CONTAGEMRESULTADO_DIVERGENCIA_"+sGXsfl_8_idx;
         cmbContagemResultado_StatusCnt_Internalname = sPrefix+"CONTAGEMRESULTADO_STATUSCNT_"+sGXsfl_8_idx;
      }

      protected void SubsflControlProps_fel_82( )
      {
         edtContagemResultado_Codigo_Internalname = sPrefix+"CONTAGEMRESULTADO_CODIGO_"+sGXsfl_8_fel_idx;
         edtContagemResultado_DataCnt_Internalname = sPrefix+"CONTAGEMRESULTADO_DATACNT_"+sGXsfl_8_fel_idx;
         edtContagemResultado_HoraCnt_Internalname = sPrefix+"CONTAGEMRESULTADO_HORACNT_"+sGXsfl_8_fel_idx;
         edtContagemResultado_ContadorFMNom_Internalname = sPrefix+"CONTAGEMRESULTADO_CONTADORFMNOM_"+sGXsfl_8_fel_idx;
         edtContagemResultadoContagens_Esforco_Internalname = sPrefix+"CONTAGEMRESULTADOCONTAGENS_ESFORCO_"+sGXsfl_8_fel_idx;
         edtContagemResultado_PFBFS_Internalname = sPrefix+"CONTAGEMRESULTADO_PFBFS_"+sGXsfl_8_fel_idx;
         edtContagemResultado_PFLFS_Internalname = sPrefix+"CONTAGEMRESULTADO_PFLFS_"+sGXsfl_8_fel_idx;
         edtContagemResultado_PFBFM_Internalname = sPrefix+"CONTAGEMRESULTADO_PFBFM_"+sGXsfl_8_fel_idx;
         edtContagemResultado_PFLFM_Internalname = sPrefix+"CONTAGEMRESULTADO_PFLFM_"+sGXsfl_8_fel_idx;
         edtContagemResultado_Divergencia_Internalname = sPrefix+"CONTAGEMRESULTADO_DIVERGENCIA_"+sGXsfl_8_fel_idx;
         cmbContagemResultado_StatusCnt_Internalname = sPrefix+"CONTAGEMRESULTADO_STATUSCNT_"+sGXsfl_8_fel_idx;
      }

      protected void sendrow_82( )
      {
         SubsflControlProps_82( ) ;
         WBB00( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_8_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_8_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_8_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultado_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A456ContagemResultado_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A456ContagemResultado_Codigo), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultado_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)8,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultado_DataCnt_Internalname,context.localUtil.Format(A473ContagemResultado_DataCnt, "99/99/99"),context.localUtil.Format( A473ContagemResultado_DataCnt, "99/99/99"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultado_DataCnt_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)8,(short)0,(short)0,(short)8,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultado_HoraCnt_Internalname,StringUtil.RTrim( A511ContagemResultado_HoraCnt),(String)"",(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultado_HoraCnt_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)5,(short)0,(short)0,(short)8,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+((edtContagemResultado_ContadorFMNom_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultado_ContadorFMNom_Internalname,StringUtil.RTrim( A474ContagemResultado_ContadorFMNom),StringUtil.RTrim( context.localUtil.Format( A474ContagemResultado_ContadorFMNom, "@!")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultado_ContadorFMNom_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(int)edtContagemResultado_ContadorFMNom_Visible,(short)0,(short)0,(String)"text",(String)"",(short)250,(String)"px",(short)17,(String)"px",(short)100,(short)0,(short)0,(short)8,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome100",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultadoContagens_Esforco_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A482ContagemResultadoContagens_Esforco), 4, 0, ",", "")),context.localUtil.Format( (decimal)(A482ContagemResultadoContagens_Esforco), "ZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultadoContagens_Esforco_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)45,(String)"px",(short)17,(String)"px",(short)4,(short)0,(short)0,(short)8,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultado_PFBFS_Internalname,StringUtil.LTrim( StringUtil.NToC( A458ContagemResultado_PFBFS, 14, 5, ",", "")),context.localUtil.Format( A458ContagemResultado_PFBFS, "ZZ,ZZZ,ZZ9.999"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultado_PFBFS_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)94,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)8,(short)1,(short)-1,(short)0,(bool)true,(String)"PontosDeFuncao",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultado_PFLFS_Internalname,StringUtil.LTrim( StringUtil.NToC( A459ContagemResultado_PFLFS, 14, 5, ",", "")),context.localUtil.Format( A459ContagemResultado_PFLFS, "ZZ,ZZZ,ZZ9.999"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultado_PFLFS_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)94,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)8,(short)1,(short)-1,(short)0,(bool)true,(String)"PontosDeFuncao",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultado_PFBFM_Internalname,StringUtil.LTrim( StringUtil.NToC( A460ContagemResultado_PFBFM, 14, 5, ",", "")),context.localUtil.Format( A460ContagemResultado_PFBFM, "ZZ,ZZZ,ZZ9.999"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultado_PFBFM_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)94,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)8,(short)1,(short)-1,(short)0,(bool)true,(String)"PontosDeFuncao",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultado_PFLFM_Internalname,StringUtil.LTrim( StringUtil.NToC( A461ContagemResultado_PFLFM, 14, 5, ",", "")),context.localUtil.Format( A461ContagemResultado_PFLFM, "ZZ,ZZZ,ZZ9.999"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultado_PFLFM_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)94,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)8,(short)1,(short)-1,(short)0,(bool)true,(String)"PontosDeFuncao",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultado_Divergencia_Internalname,StringUtil.LTrim( StringUtil.NToC( A462ContagemResultado_Divergencia, 6, 2, ",", "")),context.localUtil.Format( A462ContagemResultado_Divergencia, "ZZ9.99"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultado_Divergencia_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)8,(short)1,(short)-1,(short)0,(bool)true,(String)"Percentual",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_8_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "CONTAGEMRESULTADO_STATUSCNT_" + sGXsfl_8_idx;
               cmbContagemResultado_StatusCnt.Name = GXCCtl;
               cmbContagemResultado_StatusCnt.WebTags = "";
               cmbContagemResultado_StatusCnt.addItem("1", "Inicial", 0);
               cmbContagemResultado_StatusCnt.addItem("2", "Auditoria", 0);
               cmbContagemResultado_StatusCnt.addItem("3", "Negociac�o", 0);
               cmbContagemResultado_StatusCnt.addItem("4", "N�o Aprovada", 0);
               cmbContagemResultado_StatusCnt.addItem("5", "Entregue", 0);
               cmbContagemResultado_StatusCnt.addItem("6", "Pend�ncias", 0);
               cmbContagemResultado_StatusCnt.addItem("7", "Diverg�ncia", 0);
               cmbContagemResultado_StatusCnt.addItem("8", "Aprovada", 0);
               if ( cmbContagemResultado_StatusCnt.ItemCount > 0 )
               {
                  A483ContagemResultado_StatusCnt = (short)(NumberUtil.Val( cmbContagemResultado_StatusCnt.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A483ContagemResultado_StatusCnt), 2, 0))), "."));
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbContagemResultado_StatusCnt,(String)cmbContagemResultado_StatusCnt_Internalname,StringUtil.Trim( StringUtil.Str( (decimal)(A483ContagemResultado_StatusCnt), 2, 0)),(short)1,(String)cmbContagemResultado_StatusCnt_Jsonclick,(short)0,(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"int",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbContagemResultado_StatusCnt.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A483ContagemResultado_StatusCnt), 2, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbContagemResultado_StatusCnt_Internalname, "Values", (String)(cmbContagemResultado_StatusCnt.ToJavascriptSource()));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADO_DATACNT"+"_"+sGXsfl_8_idx, GetSecureSignedToken( sPrefix+sGXsfl_8_idx, A473ContagemResultado_DataCnt));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADO_HORACNT"+"_"+sGXsfl_8_idx, GetSecureSignedToken( sPrefix+sGXsfl_8_idx, StringUtil.RTrim( context.localUtil.Format( A511ContagemResultado_HoraCnt, ""))));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADOCONTAGENS_ESFORCO"+"_"+sGXsfl_8_idx, GetSecureSignedToken( sPrefix+sGXsfl_8_idx, context.localUtil.Format( (decimal)(A482ContagemResultadoContagens_Esforco), "ZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADO_PFBFS"+"_"+sGXsfl_8_idx, GetSecureSignedToken( sPrefix+sGXsfl_8_idx, context.localUtil.Format( A458ContagemResultado_PFBFS, "ZZ,ZZZ,ZZ9.999")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADO_PFLFS"+"_"+sGXsfl_8_idx, GetSecureSignedToken( sPrefix+sGXsfl_8_idx, context.localUtil.Format( A459ContagemResultado_PFLFS, "ZZ,ZZZ,ZZ9.999")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADO_PFBFM"+"_"+sGXsfl_8_idx, GetSecureSignedToken( sPrefix+sGXsfl_8_idx, context.localUtil.Format( A460ContagemResultado_PFBFM, "ZZ,ZZZ,ZZ9.999")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADO_PFLFM"+"_"+sGXsfl_8_idx, GetSecureSignedToken( sPrefix+sGXsfl_8_idx, context.localUtil.Format( A461ContagemResultado_PFLFM, "ZZ,ZZZ,ZZ9.999")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADO_DIVERGENCIA"+"_"+sGXsfl_8_idx, GetSecureSignedToken( sPrefix+sGXsfl_8_idx, context.localUtil.Format( A462ContagemResultado_Divergencia, "ZZ9.99")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADO_STATUSCNT"+"_"+sGXsfl_8_idx, GetSecureSignedToken( sPrefix+sGXsfl_8_idx, context.localUtil.Format( (decimal)(A483ContagemResultado_StatusCnt), "Z9")));
            GridContainer.AddRow(GridRow);
            nGXsfl_8_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_8_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_8_idx+1));
            sGXsfl_8_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_8_idx), 4, 0)), 4, "0");
            SubsflControlProps_82( ) ;
         }
         /* End function sendrow_82 */
      }

      protected void init_default_properties( )
      {
         edtContagemResultado_Codigo_Internalname = sPrefix+"CONTAGEMRESULTADO_CODIGO";
         edtContagemResultado_DataCnt_Internalname = sPrefix+"CONTAGEMRESULTADO_DATACNT";
         edtContagemResultado_HoraCnt_Internalname = sPrefix+"CONTAGEMRESULTADO_HORACNT";
         edtContagemResultado_ContadorFMNom_Internalname = sPrefix+"CONTAGEMRESULTADO_CONTADORFMNOM";
         edtContagemResultadoContagens_Esforco_Internalname = sPrefix+"CONTAGEMRESULTADOCONTAGENS_ESFORCO";
         edtContagemResultado_PFBFS_Internalname = sPrefix+"CONTAGEMRESULTADO_PFBFS";
         edtContagemResultado_PFLFS_Internalname = sPrefix+"CONTAGEMRESULTADO_PFLFS";
         edtContagemResultado_PFBFM_Internalname = sPrefix+"CONTAGEMRESULTADO_PFBFM";
         edtContagemResultado_PFLFM_Internalname = sPrefix+"CONTAGEMRESULTADO_PFLFM";
         edtContagemResultado_Divergencia_Internalname = sPrefix+"CONTAGEMRESULTADO_DIVERGENCIA";
         cmbContagemResultado_StatusCnt_Internalname = sPrefix+"CONTAGEMRESULTADO_STATUSCNT";
         Gridpaginationbar_Internalname = sPrefix+"GRIDPAGINATIONBAR";
         edtavGridcurrentpage_Internalname = sPrefix+"vGRIDCURRENTPAGE";
         tblGridtablewithpaginationbar_Internalname = sPrefix+"GRIDTABLEWITHPAGINATIONBAR";
         lblTbjava_Internalname = sPrefix+"TBJAVA";
         tblTablemain_Internalname = sPrefix+"TABLEMAIN";
         Form.Internalname = sPrefix+"FORM";
         subGrid_Internalname = sPrefix+"GRID";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         cmbContagemResultado_StatusCnt_Jsonclick = "";
         edtContagemResultado_Divergencia_Jsonclick = "";
         edtContagemResultado_PFLFM_Jsonclick = "";
         edtContagemResultado_PFBFM_Jsonclick = "";
         edtContagemResultado_PFLFS_Jsonclick = "";
         edtContagemResultado_PFBFS_Jsonclick = "";
         edtContagemResultadoContagens_Esforco_Jsonclick = "";
         edtContagemResultado_ContadorFMNom_Jsonclick = "";
         edtContagemResultado_HoraCnt_Jsonclick = "";
         edtContagemResultado_DataCnt_Jsonclick = "";
         edtContagemResultado_Codigo_Jsonclick = "";
         edtavGridcurrentpage_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         lblTbjava_Visible = 1;
         edtContagemResultado_ContadorFMNom_Visible = -1;
         lblTbjava_Caption = "tbJava";
         edtavGridcurrentpage_Visible = 1;
         subGrid_Backcolorstyle = 3;
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Caption = "Contagem Resultado_Contagens";
         subGrid_Rows = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''}],oparms:[{av:'edtContagemResultado_ContadorFMNom_Visible',ctrl:'CONTAGEMRESULTADO_CONTADORFMNOM',prop:'Visible'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E14B02',iparms:[],oparms:[]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11B02',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'},{av:'AV7GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0}],oparms:[{av:'AV7GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         GXKey = "";
         scmdbuf = "";
         H00B02_A457ContagemResultado_Demanda = new String[] {""} ;
         H00B02_n457ContagemResultado_Demanda = new bool[] {false} ;
         A457ContagemResultado_Demanda = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         A473ContagemResultado_DataCnt = DateTime.MinValue;
         A511ContagemResultado_HoraCnt = "";
         A474ContagemResultado_ContadorFMNom = "";
         GXCCtl = "";
         GridContainer = new GXWebGrid( context);
         H00B03_A470ContagemResultado_ContadorFMCod = new int[1] ;
         H00B03_A479ContagemResultado_CrFMPessoaCod = new int[1] ;
         H00B03_n479ContagemResultado_CrFMPessoaCod = new bool[] {false} ;
         H00B03_A456ContagemResultado_Codigo = new int[1] ;
         H00B03_A457ContagemResultado_Demanda = new String[] {""} ;
         H00B03_n457ContagemResultado_Demanda = new bool[] {false} ;
         H00B03_A483ContagemResultado_StatusCnt = new short[1] ;
         H00B03_A462ContagemResultado_Divergencia = new decimal[1] ;
         H00B03_A461ContagemResultado_PFLFM = new decimal[1] ;
         H00B03_n461ContagemResultado_PFLFM = new bool[] {false} ;
         H00B03_A460ContagemResultado_PFBFM = new decimal[1] ;
         H00B03_n460ContagemResultado_PFBFM = new bool[] {false} ;
         H00B03_A459ContagemResultado_PFLFS = new decimal[1] ;
         H00B03_n459ContagemResultado_PFLFS = new bool[] {false} ;
         H00B03_A458ContagemResultado_PFBFS = new decimal[1] ;
         H00B03_n458ContagemResultado_PFBFS = new bool[] {false} ;
         H00B03_A482ContagemResultadoContagens_Esforco = new short[1] ;
         H00B03_A474ContagemResultado_ContadorFMNom = new String[] {""} ;
         H00B03_n474ContagemResultado_ContadorFMNom = new bool[] {false} ;
         H00B03_A511ContagemResultado_HoraCnt = new String[] {""} ;
         H00B03_A473ContagemResultado_DataCnt = new DateTime[] {DateTime.MinValue} ;
         H00B04_AGRID_nRecordCount = new long[1] ;
         H00B05_A457ContagemResultado_Demanda = new String[] {""} ;
         H00B05_n457ContagemResultado_Demanda = new bool[] {false} ;
         AV5WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GridRow = new GXWebRow();
         sStyleString = "";
         lblTbjava_Jsonclick = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         TempTags = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlA456ContagemResultado_Codigo = "";
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wc_contagemresultado_contagens__default(),
            new Object[][] {
                new Object[] {
               H00B02_A457ContagemResultado_Demanda, H00B02_n457ContagemResultado_Demanda
               }
               , new Object[] {
               H00B03_A470ContagemResultado_ContadorFMCod, H00B03_A479ContagemResultado_CrFMPessoaCod, H00B03_n479ContagemResultado_CrFMPessoaCod, H00B03_A456ContagemResultado_Codigo, H00B03_A457ContagemResultado_Demanda, H00B03_n457ContagemResultado_Demanda, H00B03_A483ContagemResultado_StatusCnt, H00B03_A462ContagemResultado_Divergencia, H00B03_A461ContagemResultado_PFLFM, H00B03_n461ContagemResultado_PFLFM,
               H00B03_A460ContagemResultado_PFBFM, H00B03_n460ContagemResultado_PFBFM, H00B03_A459ContagemResultado_PFLFS, H00B03_n459ContagemResultado_PFLFS, H00B03_A458ContagemResultado_PFBFS, H00B03_n458ContagemResultado_PFBFS, H00B03_A482ContagemResultadoContagens_Esforco, H00B03_A474ContagemResultado_ContadorFMNom, H00B03_n474ContagemResultado_ContadorFMNom, H00B03_A511ContagemResultado_HoraCnt,
               H00B03_A473ContagemResultado_DataCnt
               }
               , new Object[] {
               H00B04_AGRID_nRecordCount
               }
               , new Object[] {
               H00B05_A457ContagemResultado_Demanda, H00B05_n457ContagemResultado_Demanda
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short nRC_GXsfl_8 ;
      private short nGXsfl_8_idx=1 ;
      private short initialized ;
      private short nGXWrapped ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short A482ContagemResultadoContagens_Esforco ;
      private short A483ContagemResultado_StatusCnt ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_8_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short subGrid_Backstyle ;
      private int A456ContagemResultado_Codigo ;
      private int wcpOA456ContagemResultado_Codigo ;
      private int subGrid_Rows ;
      private int Gridpaginationbar_Pagestoshow ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int A470ContagemResultado_ContadorFMCod ;
      private int A479ContagemResultado_CrFMPessoaCod ;
      private int edtavGridcurrentpage_Visible ;
      private int lblTbjava_Visible ;
      private int edtContagemResultado_ContadorFMNom_Visible ;
      private int AV6PageToGo ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long AV8GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private long AV7GridCurrentPage ;
      private decimal A458ContagemResultado_PFBFS ;
      private decimal A459ContagemResultado_PFLFS ;
      private decimal A460ContagemResultado_PFBFM ;
      private decimal A461ContagemResultado_PFLFM ;
      private decimal A462ContagemResultado_Divergencia ;
      private String Gridpaginationbar_Selectedpage ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String sGXsfl_8_idx="0001" ;
      private String GXKey ;
      private String scmdbuf ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String GX_FocusControl ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavGridcurrentpage_Internalname ;
      private String edtContagemResultado_Codigo_Internalname ;
      private String edtContagemResultado_DataCnt_Internalname ;
      private String A511ContagemResultado_HoraCnt ;
      private String edtContagemResultado_HoraCnt_Internalname ;
      private String A474ContagemResultado_ContadorFMNom ;
      private String edtContagemResultado_ContadorFMNom_Internalname ;
      private String edtContagemResultadoContagens_Esforco_Internalname ;
      private String edtContagemResultado_PFBFS_Internalname ;
      private String edtContagemResultado_PFLFS_Internalname ;
      private String edtContagemResultado_PFBFM_Internalname ;
      private String edtContagemResultado_PFLFM_Internalname ;
      private String edtContagemResultado_Divergencia_Internalname ;
      private String cmbContagemResultado_StatusCnt_Internalname ;
      private String GXCCtl ;
      private String lblTbjava_Internalname ;
      private String lblTbjava_Caption ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String lblTbjava_Jsonclick ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String TempTags ;
      private String edtavGridcurrentpage_Jsonclick ;
      private String sCtrlA456ContagemResultado_Codigo ;
      private String sGXsfl_8_fel_idx="0001" ;
      private String ROClassString ;
      private String edtContagemResultado_Codigo_Jsonclick ;
      private String edtContagemResultado_DataCnt_Jsonclick ;
      private String edtContagemResultado_HoraCnt_Jsonclick ;
      private String edtContagemResultado_ContadorFMNom_Jsonclick ;
      private String edtContagemResultadoContagens_Esforco_Jsonclick ;
      private String edtContagemResultado_PFBFS_Jsonclick ;
      private String edtContagemResultado_PFLFS_Jsonclick ;
      private String edtContagemResultado_PFBFM_Jsonclick ;
      private String edtContagemResultado_PFLFM_Jsonclick ;
      private String edtContagemResultado_Divergencia_Jsonclick ;
      private String cmbContagemResultado_StatusCnt_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private DateTime A473ContagemResultado_DataCnt ;
      private bool entryPointCalled ;
      private bool n457ContagemResultado_Demanda ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n474ContagemResultado_ContadorFMNom ;
      private bool n458ContagemResultado_PFBFS ;
      private bool n459ContagemResultado_PFLFS ;
      private bool n460ContagemResultado_PFBFM ;
      private bool n461ContagemResultado_PFLFM ;
      private bool n479ContagemResultado_CrFMPessoaCod ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private String A457ContagemResultado_Demanda ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_ContagemResultado_Codigo ;
      private GXCombobox cmbContagemResultado_StatusCnt ;
      private IDataStoreProvider pr_default ;
      private String[] H00B02_A457ContagemResultado_Demanda ;
      private bool[] H00B02_n457ContagemResultado_Demanda ;
      private int[] H00B03_A470ContagemResultado_ContadorFMCod ;
      private int[] H00B03_A479ContagemResultado_CrFMPessoaCod ;
      private bool[] H00B03_n479ContagemResultado_CrFMPessoaCod ;
      private int[] H00B03_A456ContagemResultado_Codigo ;
      private String[] H00B03_A457ContagemResultado_Demanda ;
      private bool[] H00B03_n457ContagemResultado_Demanda ;
      private short[] H00B03_A483ContagemResultado_StatusCnt ;
      private decimal[] H00B03_A462ContagemResultado_Divergencia ;
      private decimal[] H00B03_A461ContagemResultado_PFLFM ;
      private bool[] H00B03_n461ContagemResultado_PFLFM ;
      private decimal[] H00B03_A460ContagemResultado_PFBFM ;
      private bool[] H00B03_n460ContagemResultado_PFBFM ;
      private decimal[] H00B03_A459ContagemResultado_PFLFS ;
      private bool[] H00B03_n459ContagemResultado_PFLFS ;
      private decimal[] H00B03_A458ContagemResultado_PFBFS ;
      private bool[] H00B03_n458ContagemResultado_PFBFS ;
      private short[] H00B03_A482ContagemResultadoContagens_Esforco ;
      private String[] H00B03_A474ContagemResultado_ContadorFMNom ;
      private bool[] H00B03_n474ContagemResultado_ContadorFMNom ;
      private String[] H00B03_A511ContagemResultado_HoraCnt ;
      private DateTime[] H00B03_A473ContagemResultado_DataCnt ;
      private long[] H00B04_AGRID_nRecordCount ;
      private String[] H00B05_A457ContagemResultado_Demanda ;
      private bool[] H00B05_n457ContagemResultado_Demanda ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private wwpbaseobjects.SdtWWPContext AV5WWPContext ;
   }

   public class wc_contagemresultado_contagens__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00B02 ;
          prmH00B02 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00B03 ;
          prmH00B03 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00B04 ;
          prmH00B04 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00B05 ;
          prmH00B05 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00B02", "SELECT [ContagemResultado_Demanda] FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00B02,1,0,true,false )
             ,new CursorDef("H00B03", "SELECT * FROM (SELECT  T1.[ContagemResultado_ContadorFMCod] AS ContagemResultado_ContadorFMCod, T2.[Usuario_PessoaCod] AS ContagemResultado_CrFMPessoaCod, T1.[ContagemResultado_Codigo], T4.[ContagemResultado_Demanda], T1.[ContagemResultado_StatusCnt], T1.[ContagemResultado_Divergencia], T1.[ContagemResultado_PFLFM], T1.[ContagemResultado_PFBFM], T1.[ContagemResultado_PFLFS], T1.[ContagemResultado_PFBFS], T1.[ContagemResultadoContagens_Esforco], T3.[Pessoa_Nome] AS ContagemResultado_ContadorFMNom, T1.[ContagemResultado_HoraCnt], T1.[ContagemResultado_DataCnt], ROW_NUMBER() OVER ( ORDER BY T1.[ContagemResultado_Codigo] ) AS GX_ROW_NUMBER FROM ((([ContagemResultadoContagens] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[ContagemResultado_ContadorFMCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod]) INNER JOIN [ContagemResultado] T4 WITH (NOLOCK) ON T4.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) WHERE T1.[ContagemResultado_Codigo] = @ContagemResultado_Codigo) AS GX_CTE WHERE GX_ROW_NUMBER BETWEEN @GXPagingFrom2 AND @GXPagingTo2 OR @GXPagingTo2 < @GXPagingFrom2 AND GX_ROW_NUMBER >= @GXPagingFrom2",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00B03,11,0,true,false )
             ,new CursorDef("H00B04", "SELECT COUNT(*) FROM ((([ContagemResultadoContagens] T1 WITH (NOLOCK) INNER JOIN [Usuario] T3 WITH (NOLOCK) ON T3.[Usuario_Codigo] = T1.[ContagemResultado_ContadorFMCod]) LEFT JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T3.[Usuario_PessoaCod]) INNER JOIN [ContagemResultado] T2 WITH (NOLOCK) ON T2.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) WHERE T1.[ContagemResultado_Codigo] = @ContagemResultado_Codigo ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00B04,1,0,true,false )
             ,new CursorDef("H00B05", "SELECT [ContagemResultado_Demanda] FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00B05,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((String[]) buf[4])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((short[]) buf[6])[0] = rslt.getShort(5) ;
                ((decimal[]) buf[7])[0] = rslt.getDecimal(6) ;
                ((decimal[]) buf[8])[0] = rslt.getDecimal(7) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(7);
                ((decimal[]) buf[10])[0] = rslt.getDecimal(8) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(8);
                ((decimal[]) buf[12])[0] = rslt.getDecimal(9) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(9);
                ((decimal[]) buf[14])[0] = rslt.getDecimal(10) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(10);
                ((short[]) buf[16])[0] = rslt.getShort(11) ;
                ((String[]) buf[17])[0] = rslt.getString(12, 100) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(12);
                ((String[]) buf[19])[0] = rslt.getString(13, 5) ;
                ((DateTime[]) buf[20])[0] = rslt.getGXDate(14) ;
                return;
             case 2 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
             case 3 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
