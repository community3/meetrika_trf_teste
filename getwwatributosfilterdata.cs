/*
               File: GetWWAtributosFilterData
        Description: Get WWAtributos Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/4/2020 8:33:49.78
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getwwatributosfilterdata : GXProcedure
   {
      public getwwatributosfilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getwwatributosfilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV23DDOName = aP0_DDOName;
         this.AV21SearchTxt = aP1_SearchTxt;
         this.AV22SearchTxtTo = aP2_SearchTxtTo;
         this.AV27OptionsJson = "" ;
         this.AV30OptionsDescJson = "" ;
         this.AV32OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV27OptionsJson;
         aP4_OptionsDescJson=this.AV30OptionsDescJson;
         aP5_OptionIndexesJson=this.AV32OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV23DDOName = aP0_DDOName;
         this.AV21SearchTxt = aP1_SearchTxt;
         this.AV22SearchTxtTo = aP2_SearchTxtTo;
         this.AV27OptionsJson = "" ;
         this.AV30OptionsDescJson = "" ;
         this.AV32OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV27OptionsJson;
         aP4_OptionsDescJson=this.AV30OptionsDescJson;
         aP5_OptionIndexesJson=this.AV32OptionIndexesJson;
         return AV32OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getwwatributosfilterdata objgetwwatributosfilterdata;
         objgetwwatributosfilterdata = new getwwatributosfilterdata();
         objgetwwatributosfilterdata.AV23DDOName = aP0_DDOName;
         objgetwwatributosfilterdata.AV21SearchTxt = aP1_SearchTxt;
         objgetwwatributosfilterdata.AV22SearchTxtTo = aP2_SearchTxtTo;
         objgetwwatributosfilterdata.AV27OptionsJson = "" ;
         objgetwwatributosfilterdata.AV30OptionsDescJson = "" ;
         objgetwwatributosfilterdata.AV32OptionIndexesJson = "" ;
         objgetwwatributosfilterdata.context.SetSubmitInitialConfig(context);
         objgetwwatributosfilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetwwatributosfilterdata);
         aP3_OptionsJson=this.AV27OptionsJson;
         aP4_OptionsDescJson=this.AV30OptionsDescJson;
         aP5_OptionIndexesJson=this.AV32OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getwwatributosfilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV26Options = (IGxCollection)(new GxSimpleCollection());
         AV29OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV31OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV23DDOName), "DDO_ATRIBUTOS_NOME") == 0 )
         {
            /* Execute user subroutine: 'LOADATRIBUTOS_NOMEOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV23DDOName), "DDO_ATRIBUTOS_DETALHES") == 0 )
         {
            /* Execute user subroutine: 'LOADATRIBUTOS_DETALHESOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV23DDOName), "DDO_ATRIBUTOS_TABELANOM") == 0 )
         {
            /* Execute user subroutine: 'LOADATRIBUTOS_TABELANOMOPTIONS' */
            S141 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV27OptionsJson = AV26Options.ToJSonString(false);
         AV30OptionsDescJson = AV29OptionsDesc.ToJSonString(false);
         AV32OptionIndexesJson = AV31OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV34Session.Get("WWAtributosGridState"), "") == 0 )
         {
            AV36GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "WWAtributosGridState"), "");
         }
         else
         {
            AV36GridState.FromXml(AV34Session.Get("WWAtributosGridState"), "");
         }
         AV55GXV1 = 1;
         while ( AV55GXV1 <= AV36GridState.gxTpr_Filtervalues.Count )
         {
            AV37GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV36GridState.gxTpr_Filtervalues.Item(AV55GXV1));
            if ( StringUtil.StrCmp(AV37GridStateFilterValue.gxTpr_Name, "SISTEMA_AREATRABALHOCOD") == 0 )
            {
               AV39Sistema_AreaTrabalhoCod = (int)(NumberUtil.Val( AV37GridStateFilterValue.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV37GridStateFilterValue.gxTpr_Name, "TFATRIBUTOS_NOME") == 0 )
            {
               AV10TFAtributos_Nome = AV37GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV37GridStateFilterValue.gxTpr_Name, "TFATRIBUTOS_NOME_SEL") == 0 )
            {
               AV11TFAtributos_Nome_Sel = AV37GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV37GridStateFilterValue.gxTpr_Name, "TFATRIBUTOS_TIPODADOS_SEL") == 0 )
            {
               AV12TFAtributos_TipoDados_SelsJson = AV37GridStateFilterValue.gxTpr_Value;
               AV13TFAtributos_TipoDados_Sels.FromJSonString(AV12TFAtributos_TipoDados_SelsJson);
            }
            else if ( StringUtil.StrCmp(AV37GridStateFilterValue.gxTpr_Name, "TFATRIBUTOS_DETALHES") == 0 )
            {
               AV14TFAtributos_Detalhes = AV37GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV37GridStateFilterValue.gxTpr_Name, "TFATRIBUTOS_DETALHES_SEL") == 0 )
            {
               AV15TFAtributos_Detalhes_Sel = AV37GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV37GridStateFilterValue.gxTpr_Name, "TFATRIBUTOS_TABELANOM") == 0 )
            {
               AV16TFAtributos_TabelaNom = AV37GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV37GridStateFilterValue.gxTpr_Name, "TFATRIBUTOS_TABELANOM_SEL") == 0 )
            {
               AV17TFAtributos_TabelaNom_Sel = AV37GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV37GridStateFilterValue.gxTpr_Name, "TFATRIBUTOS_ATIVO_SEL") == 0 )
            {
               AV18TFAtributos_Ativo_Sel = (short)(NumberUtil.Val( AV37GridStateFilterValue.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV37GridStateFilterValue.gxTpr_Name, "TFATRIBUTOS_PK_SEL") == 0 )
            {
               AV19TFAtributos_PK_Sel = (short)(NumberUtil.Val( AV37GridStateFilterValue.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV37GridStateFilterValue.gxTpr_Name, "TFATRIBUTOS_FK_SEL") == 0 )
            {
               AV20TFAtributos_FK_Sel = (short)(NumberUtil.Val( AV37GridStateFilterValue.gxTpr_Value, "."));
            }
            AV55GXV1 = (int)(AV55GXV1+1);
         }
         if ( AV36GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV38GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV36GridState.gxTpr_Dynamicfilters.Item(1));
            AV40DynamicFiltersSelector1 = AV38GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV40DynamicFiltersSelector1, "ATRIBUTOS_NOME") == 0 )
            {
               AV41DynamicFiltersOperator1 = AV38GridStateDynamicFilter.gxTpr_Operator;
               AV42Atributos_Nome1 = AV38GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV40DynamicFiltersSelector1, "ATRIBUTOS_TABELANOM") == 0 )
            {
               AV41DynamicFiltersOperator1 = AV38GridStateDynamicFilter.gxTpr_Operator;
               AV43Atributos_TabelaNom1 = AV38GridStateDynamicFilter.gxTpr_Value;
            }
            if ( AV36GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV44DynamicFiltersEnabled2 = true;
               AV38GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV36GridState.gxTpr_Dynamicfilters.Item(2));
               AV45DynamicFiltersSelector2 = AV38GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV45DynamicFiltersSelector2, "ATRIBUTOS_NOME") == 0 )
               {
                  AV46DynamicFiltersOperator2 = AV38GridStateDynamicFilter.gxTpr_Operator;
                  AV47Atributos_Nome2 = AV38GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV45DynamicFiltersSelector2, "ATRIBUTOS_TABELANOM") == 0 )
               {
                  AV46DynamicFiltersOperator2 = AV38GridStateDynamicFilter.gxTpr_Operator;
                  AV48Atributos_TabelaNom2 = AV38GridStateDynamicFilter.gxTpr_Value;
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADATRIBUTOS_NOMEOPTIONS' Routine */
         AV10TFAtributos_Nome = AV21SearchTxt;
         AV11TFAtributos_Nome_Sel = "";
         AV57WWAtributosDS_1_Sistema_areatrabalhocod = AV39Sistema_AreaTrabalhoCod;
         AV58WWAtributosDS_2_Dynamicfiltersselector1 = AV40DynamicFiltersSelector1;
         AV59WWAtributosDS_3_Dynamicfiltersoperator1 = AV41DynamicFiltersOperator1;
         AV60WWAtributosDS_4_Atributos_nome1 = AV42Atributos_Nome1;
         AV61WWAtributosDS_5_Atributos_tabelanom1 = AV43Atributos_TabelaNom1;
         AV62WWAtributosDS_6_Dynamicfiltersenabled2 = AV44DynamicFiltersEnabled2;
         AV63WWAtributosDS_7_Dynamicfiltersselector2 = AV45DynamicFiltersSelector2;
         AV64WWAtributosDS_8_Dynamicfiltersoperator2 = AV46DynamicFiltersOperator2;
         AV65WWAtributosDS_9_Atributos_nome2 = AV47Atributos_Nome2;
         AV66WWAtributosDS_10_Atributos_tabelanom2 = AV48Atributos_TabelaNom2;
         AV67WWAtributosDS_11_Tfatributos_nome = AV10TFAtributos_Nome;
         AV68WWAtributosDS_12_Tfatributos_nome_sel = AV11TFAtributos_Nome_Sel;
         AV69WWAtributosDS_13_Tfatributos_tipodados_sels = AV13TFAtributos_TipoDados_Sels;
         AV70WWAtributosDS_14_Tfatributos_detalhes = AV14TFAtributos_Detalhes;
         AV71WWAtributosDS_15_Tfatributos_detalhes_sel = AV15TFAtributos_Detalhes_Sel;
         AV72WWAtributosDS_16_Tfatributos_tabelanom = AV16TFAtributos_TabelaNom;
         AV73WWAtributosDS_17_Tfatributos_tabelanom_sel = AV17TFAtributos_TabelaNom_Sel;
         AV74WWAtributosDS_18_Tfatributos_ativo_sel = AV18TFAtributos_Ativo_Sel;
         AV75WWAtributosDS_19_Tfatributos_pk_sel = AV19TFAtributos_PK_Sel;
         AV76WWAtributosDS_20_Tfatributos_fk_sel = AV20TFAtributos_FK_Sel;
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              A178Atributos_TipoDados ,
                                              AV69WWAtributosDS_13_Tfatributos_tipodados_sels ,
                                              AV57WWAtributosDS_1_Sistema_areatrabalhocod ,
                                              AV58WWAtributosDS_2_Dynamicfiltersselector1 ,
                                              AV59WWAtributosDS_3_Dynamicfiltersoperator1 ,
                                              AV60WWAtributosDS_4_Atributos_nome1 ,
                                              AV61WWAtributosDS_5_Atributos_tabelanom1 ,
                                              AV62WWAtributosDS_6_Dynamicfiltersenabled2 ,
                                              AV63WWAtributosDS_7_Dynamicfiltersselector2 ,
                                              AV64WWAtributosDS_8_Dynamicfiltersoperator2 ,
                                              AV65WWAtributosDS_9_Atributos_nome2 ,
                                              AV66WWAtributosDS_10_Atributos_tabelanom2 ,
                                              AV68WWAtributosDS_12_Tfatributos_nome_sel ,
                                              AV67WWAtributosDS_11_Tfatributos_nome ,
                                              AV69WWAtributosDS_13_Tfatributos_tipodados_sels.Count ,
                                              AV71WWAtributosDS_15_Tfatributos_detalhes_sel ,
                                              AV70WWAtributosDS_14_Tfatributos_detalhes ,
                                              AV73WWAtributosDS_17_Tfatributos_tabelanom_sel ,
                                              AV72WWAtributosDS_16_Tfatributos_tabelanom ,
                                              AV74WWAtributosDS_18_Tfatributos_ativo_sel ,
                                              AV75WWAtributosDS_19_Tfatributos_pk_sel ,
                                              AV76WWAtributosDS_20_Tfatributos_fk_sel ,
                                              A135Sistema_AreaTrabalhoCod ,
                                              A177Atributos_Nome ,
                                              A357Atributos_TabelaNom ,
                                              A390Atributos_Detalhes ,
                                              A180Atributos_Ativo ,
                                              A400Atributos_PK ,
                                              A401Atributos_FK },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT,
                                              TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN,
                                              TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN
                                              }
         });
         lV60WWAtributosDS_4_Atributos_nome1 = StringUtil.PadR( StringUtil.RTrim( AV60WWAtributosDS_4_Atributos_nome1), 50, "%");
         lV60WWAtributosDS_4_Atributos_nome1 = StringUtil.PadR( StringUtil.RTrim( AV60WWAtributosDS_4_Atributos_nome1), 50, "%");
         lV61WWAtributosDS_5_Atributos_tabelanom1 = StringUtil.PadR( StringUtil.RTrim( AV61WWAtributosDS_5_Atributos_tabelanom1), 50, "%");
         lV61WWAtributosDS_5_Atributos_tabelanom1 = StringUtil.PadR( StringUtil.RTrim( AV61WWAtributosDS_5_Atributos_tabelanom1), 50, "%");
         lV65WWAtributosDS_9_Atributos_nome2 = StringUtil.PadR( StringUtil.RTrim( AV65WWAtributosDS_9_Atributos_nome2), 50, "%");
         lV65WWAtributosDS_9_Atributos_nome2 = StringUtil.PadR( StringUtil.RTrim( AV65WWAtributosDS_9_Atributos_nome2), 50, "%");
         lV66WWAtributosDS_10_Atributos_tabelanom2 = StringUtil.PadR( StringUtil.RTrim( AV66WWAtributosDS_10_Atributos_tabelanom2), 50, "%");
         lV66WWAtributosDS_10_Atributos_tabelanom2 = StringUtil.PadR( StringUtil.RTrim( AV66WWAtributosDS_10_Atributos_tabelanom2), 50, "%");
         lV67WWAtributosDS_11_Tfatributos_nome = StringUtil.PadR( StringUtil.RTrim( AV67WWAtributosDS_11_Tfatributos_nome), 50, "%");
         lV70WWAtributosDS_14_Tfatributos_detalhes = StringUtil.PadR( StringUtil.RTrim( AV70WWAtributosDS_14_Tfatributos_detalhes), 10, "%");
         lV72WWAtributosDS_16_Tfatributos_tabelanom = StringUtil.PadR( StringUtil.RTrim( AV72WWAtributosDS_16_Tfatributos_tabelanom), 50, "%");
         /* Using cursor P00H32 */
         pr_default.execute(0, new Object[] {AV57WWAtributosDS_1_Sistema_areatrabalhocod, lV60WWAtributosDS_4_Atributos_nome1, lV60WWAtributosDS_4_Atributos_nome1, lV61WWAtributosDS_5_Atributos_tabelanom1, lV61WWAtributosDS_5_Atributos_tabelanom1, lV65WWAtributosDS_9_Atributos_nome2, lV65WWAtributosDS_9_Atributos_nome2, lV66WWAtributosDS_10_Atributos_tabelanom2, lV66WWAtributosDS_10_Atributos_tabelanom2, lV67WWAtributosDS_11_Tfatributos_nome, AV68WWAtributosDS_12_Tfatributos_nome_sel, lV70WWAtributosDS_14_Tfatributos_detalhes, AV71WWAtributosDS_15_Tfatributos_detalhes_sel, lV72WWAtributosDS_16_Tfatributos_tabelanom, AV73WWAtributosDS_17_Tfatributos_tabelanom_sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKH32 = false;
            A356Atributos_TabelaCod = P00H32_A356Atributos_TabelaCod[0];
            A190Tabela_SistemaCod = P00H32_A190Tabela_SistemaCod[0];
            n190Tabela_SistemaCod = P00H32_n190Tabela_SistemaCod[0];
            A177Atributos_Nome = P00H32_A177Atributos_Nome[0];
            A401Atributos_FK = P00H32_A401Atributos_FK[0];
            n401Atributos_FK = P00H32_n401Atributos_FK[0];
            A400Atributos_PK = P00H32_A400Atributos_PK[0];
            n400Atributos_PK = P00H32_n400Atributos_PK[0];
            A180Atributos_Ativo = P00H32_A180Atributos_Ativo[0];
            A390Atributos_Detalhes = P00H32_A390Atributos_Detalhes[0];
            n390Atributos_Detalhes = P00H32_n390Atributos_Detalhes[0];
            A178Atributos_TipoDados = P00H32_A178Atributos_TipoDados[0];
            n178Atributos_TipoDados = P00H32_n178Atributos_TipoDados[0];
            A357Atributos_TabelaNom = P00H32_A357Atributos_TabelaNom[0];
            n357Atributos_TabelaNom = P00H32_n357Atributos_TabelaNom[0];
            A135Sistema_AreaTrabalhoCod = P00H32_A135Sistema_AreaTrabalhoCod[0];
            n135Sistema_AreaTrabalhoCod = P00H32_n135Sistema_AreaTrabalhoCod[0];
            A176Atributos_Codigo = P00H32_A176Atributos_Codigo[0];
            A190Tabela_SistemaCod = P00H32_A190Tabela_SistemaCod[0];
            n190Tabela_SistemaCod = P00H32_n190Tabela_SistemaCod[0];
            A357Atributos_TabelaNom = P00H32_A357Atributos_TabelaNom[0];
            n357Atributos_TabelaNom = P00H32_n357Atributos_TabelaNom[0];
            A135Sistema_AreaTrabalhoCod = P00H32_A135Sistema_AreaTrabalhoCod[0];
            n135Sistema_AreaTrabalhoCod = P00H32_n135Sistema_AreaTrabalhoCod[0];
            AV33count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00H32_A177Atributos_Nome[0], A177Atributos_Nome) == 0 ) )
            {
               BRKH32 = false;
               A176Atributos_Codigo = P00H32_A176Atributos_Codigo[0];
               AV33count = (long)(AV33count+1);
               BRKH32 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A177Atributos_Nome)) )
            {
               AV25Option = A177Atributos_Nome;
               AV26Options.Add(AV25Option, 0);
               AV31OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV33count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV26Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKH32 )
            {
               BRKH32 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADATRIBUTOS_DETALHESOPTIONS' Routine */
         AV14TFAtributos_Detalhes = AV21SearchTxt;
         AV15TFAtributos_Detalhes_Sel = "";
         AV57WWAtributosDS_1_Sistema_areatrabalhocod = AV39Sistema_AreaTrabalhoCod;
         AV58WWAtributosDS_2_Dynamicfiltersselector1 = AV40DynamicFiltersSelector1;
         AV59WWAtributosDS_3_Dynamicfiltersoperator1 = AV41DynamicFiltersOperator1;
         AV60WWAtributosDS_4_Atributos_nome1 = AV42Atributos_Nome1;
         AV61WWAtributosDS_5_Atributos_tabelanom1 = AV43Atributos_TabelaNom1;
         AV62WWAtributosDS_6_Dynamicfiltersenabled2 = AV44DynamicFiltersEnabled2;
         AV63WWAtributosDS_7_Dynamicfiltersselector2 = AV45DynamicFiltersSelector2;
         AV64WWAtributosDS_8_Dynamicfiltersoperator2 = AV46DynamicFiltersOperator2;
         AV65WWAtributosDS_9_Atributos_nome2 = AV47Atributos_Nome2;
         AV66WWAtributosDS_10_Atributos_tabelanom2 = AV48Atributos_TabelaNom2;
         AV67WWAtributosDS_11_Tfatributos_nome = AV10TFAtributos_Nome;
         AV68WWAtributosDS_12_Tfatributos_nome_sel = AV11TFAtributos_Nome_Sel;
         AV69WWAtributosDS_13_Tfatributos_tipodados_sels = AV13TFAtributos_TipoDados_Sels;
         AV70WWAtributosDS_14_Tfatributos_detalhes = AV14TFAtributos_Detalhes;
         AV71WWAtributosDS_15_Tfatributos_detalhes_sel = AV15TFAtributos_Detalhes_Sel;
         AV72WWAtributosDS_16_Tfatributos_tabelanom = AV16TFAtributos_TabelaNom;
         AV73WWAtributosDS_17_Tfatributos_tabelanom_sel = AV17TFAtributos_TabelaNom_Sel;
         AV74WWAtributosDS_18_Tfatributos_ativo_sel = AV18TFAtributos_Ativo_Sel;
         AV75WWAtributosDS_19_Tfatributos_pk_sel = AV19TFAtributos_PK_Sel;
         AV76WWAtributosDS_20_Tfatributos_fk_sel = AV20TFAtributos_FK_Sel;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              A178Atributos_TipoDados ,
                                              AV69WWAtributosDS_13_Tfatributos_tipodados_sels ,
                                              AV57WWAtributosDS_1_Sistema_areatrabalhocod ,
                                              AV58WWAtributosDS_2_Dynamicfiltersselector1 ,
                                              AV59WWAtributosDS_3_Dynamicfiltersoperator1 ,
                                              AV60WWAtributosDS_4_Atributos_nome1 ,
                                              AV61WWAtributosDS_5_Atributos_tabelanom1 ,
                                              AV62WWAtributosDS_6_Dynamicfiltersenabled2 ,
                                              AV63WWAtributosDS_7_Dynamicfiltersselector2 ,
                                              AV64WWAtributosDS_8_Dynamicfiltersoperator2 ,
                                              AV65WWAtributosDS_9_Atributos_nome2 ,
                                              AV66WWAtributosDS_10_Atributos_tabelanom2 ,
                                              AV68WWAtributosDS_12_Tfatributos_nome_sel ,
                                              AV67WWAtributosDS_11_Tfatributos_nome ,
                                              AV69WWAtributosDS_13_Tfatributos_tipodados_sels.Count ,
                                              AV71WWAtributosDS_15_Tfatributos_detalhes_sel ,
                                              AV70WWAtributosDS_14_Tfatributos_detalhes ,
                                              AV73WWAtributosDS_17_Tfatributos_tabelanom_sel ,
                                              AV72WWAtributosDS_16_Tfatributos_tabelanom ,
                                              AV74WWAtributosDS_18_Tfatributos_ativo_sel ,
                                              AV75WWAtributosDS_19_Tfatributos_pk_sel ,
                                              AV76WWAtributosDS_20_Tfatributos_fk_sel ,
                                              A135Sistema_AreaTrabalhoCod ,
                                              A177Atributos_Nome ,
                                              A357Atributos_TabelaNom ,
                                              A390Atributos_Detalhes ,
                                              A180Atributos_Ativo ,
                                              A400Atributos_PK ,
                                              A401Atributos_FK },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT,
                                              TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN,
                                              TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN
                                              }
         });
         lV60WWAtributosDS_4_Atributos_nome1 = StringUtil.PadR( StringUtil.RTrim( AV60WWAtributosDS_4_Atributos_nome1), 50, "%");
         lV60WWAtributosDS_4_Atributos_nome1 = StringUtil.PadR( StringUtil.RTrim( AV60WWAtributosDS_4_Atributos_nome1), 50, "%");
         lV61WWAtributosDS_5_Atributos_tabelanom1 = StringUtil.PadR( StringUtil.RTrim( AV61WWAtributosDS_5_Atributos_tabelanom1), 50, "%");
         lV61WWAtributosDS_5_Atributos_tabelanom1 = StringUtil.PadR( StringUtil.RTrim( AV61WWAtributosDS_5_Atributos_tabelanom1), 50, "%");
         lV65WWAtributosDS_9_Atributos_nome2 = StringUtil.PadR( StringUtil.RTrim( AV65WWAtributosDS_9_Atributos_nome2), 50, "%");
         lV65WWAtributosDS_9_Atributos_nome2 = StringUtil.PadR( StringUtil.RTrim( AV65WWAtributosDS_9_Atributos_nome2), 50, "%");
         lV66WWAtributosDS_10_Atributos_tabelanom2 = StringUtil.PadR( StringUtil.RTrim( AV66WWAtributosDS_10_Atributos_tabelanom2), 50, "%");
         lV66WWAtributosDS_10_Atributos_tabelanom2 = StringUtil.PadR( StringUtil.RTrim( AV66WWAtributosDS_10_Atributos_tabelanom2), 50, "%");
         lV67WWAtributosDS_11_Tfatributos_nome = StringUtil.PadR( StringUtil.RTrim( AV67WWAtributosDS_11_Tfatributos_nome), 50, "%");
         lV70WWAtributosDS_14_Tfatributos_detalhes = StringUtil.PadR( StringUtil.RTrim( AV70WWAtributosDS_14_Tfatributos_detalhes), 10, "%");
         lV72WWAtributosDS_16_Tfatributos_tabelanom = StringUtil.PadR( StringUtil.RTrim( AV72WWAtributosDS_16_Tfatributos_tabelanom), 50, "%");
         /* Using cursor P00H33 */
         pr_default.execute(1, new Object[] {AV57WWAtributosDS_1_Sistema_areatrabalhocod, lV60WWAtributosDS_4_Atributos_nome1, lV60WWAtributosDS_4_Atributos_nome1, lV61WWAtributosDS_5_Atributos_tabelanom1, lV61WWAtributosDS_5_Atributos_tabelanom1, lV65WWAtributosDS_9_Atributos_nome2, lV65WWAtributosDS_9_Atributos_nome2, lV66WWAtributosDS_10_Atributos_tabelanom2, lV66WWAtributosDS_10_Atributos_tabelanom2, lV67WWAtributosDS_11_Tfatributos_nome, AV68WWAtributosDS_12_Tfatributos_nome_sel, lV70WWAtributosDS_14_Tfatributos_detalhes, AV71WWAtributosDS_15_Tfatributos_detalhes_sel, lV72WWAtributosDS_16_Tfatributos_tabelanom, AV73WWAtributosDS_17_Tfatributos_tabelanom_sel});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKH34 = false;
            A356Atributos_TabelaCod = P00H33_A356Atributos_TabelaCod[0];
            A190Tabela_SistemaCod = P00H33_A190Tabela_SistemaCod[0];
            n190Tabela_SistemaCod = P00H33_n190Tabela_SistemaCod[0];
            A390Atributos_Detalhes = P00H33_A390Atributos_Detalhes[0];
            n390Atributos_Detalhes = P00H33_n390Atributos_Detalhes[0];
            A401Atributos_FK = P00H33_A401Atributos_FK[0];
            n401Atributos_FK = P00H33_n401Atributos_FK[0];
            A400Atributos_PK = P00H33_A400Atributos_PK[0];
            n400Atributos_PK = P00H33_n400Atributos_PK[0];
            A180Atributos_Ativo = P00H33_A180Atributos_Ativo[0];
            A178Atributos_TipoDados = P00H33_A178Atributos_TipoDados[0];
            n178Atributos_TipoDados = P00H33_n178Atributos_TipoDados[0];
            A357Atributos_TabelaNom = P00H33_A357Atributos_TabelaNom[0];
            n357Atributos_TabelaNom = P00H33_n357Atributos_TabelaNom[0];
            A177Atributos_Nome = P00H33_A177Atributos_Nome[0];
            A135Sistema_AreaTrabalhoCod = P00H33_A135Sistema_AreaTrabalhoCod[0];
            n135Sistema_AreaTrabalhoCod = P00H33_n135Sistema_AreaTrabalhoCod[0];
            A176Atributos_Codigo = P00H33_A176Atributos_Codigo[0];
            A190Tabela_SistemaCod = P00H33_A190Tabela_SistemaCod[0];
            n190Tabela_SistemaCod = P00H33_n190Tabela_SistemaCod[0];
            A357Atributos_TabelaNom = P00H33_A357Atributos_TabelaNom[0];
            n357Atributos_TabelaNom = P00H33_n357Atributos_TabelaNom[0];
            A135Sistema_AreaTrabalhoCod = P00H33_A135Sistema_AreaTrabalhoCod[0];
            n135Sistema_AreaTrabalhoCod = P00H33_n135Sistema_AreaTrabalhoCod[0];
            AV33count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( StringUtil.StrCmp(P00H33_A390Atributos_Detalhes[0], A390Atributos_Detalhes) == 0 ) )
            {
               BRKH34 = false;
               A176Atributos_Codigo = P00H33_A176Atributos_Codigo[0];
               AV33count = (long)(AV33count+1);
               BRKH34 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A390Atributos_Detalhes)) )
            {
               AV25Option = A390Atributos_Detalhes;
               AV26Options.Add(AV25Option, 0);
               AV31OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV33count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV26Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKH34 )
            {
               BRKH34 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      protected void S141( )
      {
         /* 'LOADATRIBUTOS_TABELANOMOPTIONS' Routine */
         AV16TFAtributos_TabelaNom = AV21SearchTxt;
         AV17TFAtributos_TabelaNom_Sel = "";
         AV57WWAtributosDS_1_Sistema_areatrabalhocod = AV39Sistema_AreaTrabalhoCod;
         AV58WWAtributosDS_2_Dynamicfiltersselector1 = AV40DynamicFiltersSelector1;
         AV59WWAtributosDS_3_Dynamicfiltersoperator1 = AV41DynamicFiltersOperator1;
         AV60WWAtributosDS_4_Atributos_nome1 = AV42Atributos_Nome1;
         AV61WWAtributosDS_5_Atributos_tabelanom1 = AV43Atributos_TabelaNom1;
         AV62WWAtributosDS_6_Dynamicfiltersenabled2 = AV44DynamicFiltersEnabled2;
         AV63WWAtributosDS_7_Dynamicfiltersselector2 = AV45DynamicFiltersSelector2;
         AV64WWAtributosDS_8_Dynamicfiltersoperator2 = AV46DynamicFiltersOperator2;
         AV65WWAtributosDS_9_Atributos_nome2 = AV47Atributos_Nome2;
         AV66WWAtributosDS_10_Atributos_tabelanom2 = AV48Atributos_TabelaNom2;
         AV67WWAtributosDS_11_Tfatributos_nome = AV10TFAtributos_Nome;
         AV68WWAtributosDS_12_Tfatributos_nome_sel = AV11TFAtributos_Nome_Sel;
         AV69WWAtributosDS_13_Tfatributos_tipodados_sels = AV13TFAtributos_TipoDados_Sels;
         AV70WWAtributosDS_14_Tfatributos_detalhes = AV14TFAtributos_Detalhes;
         AV71WWAtributosDS_15_Tfatributos_detalhes_sel = AV15TFAtributos_Detalhes_Sel;
         AV72WWAtributosDS_16_Tfatributos_tabelanom = AV16TFAtributos_TabelaNom;
         AV73WWAtributosDS_17_Tfatributos_tabelanom_sel = AV17TFAtributos_TabelaNom_Sel;
         AV74WWAtributosDS_18_Tfatributos_ativo_sel = AV18TFAtributos_Ativo_Sel;
         AV75WWAtributosDS_19_Tfatributos_pk_sel = AV19TFAtributos_PK_Sel;
         AV76WWAtributosDS_20_Tfatributos_fk_sel = AV20TFAtributos_FK_Sel;
         pr_default.dynParam(2, new Object[]{ new Object[]{
                                              A178Atributos_TipoDados ,
                                              AV69WWAtributosDS_13_Tfatributos_tipodados_sels ,
                                              AV57WWAtributosDS_1_Sistema_areatrabalhocod ,
                                              AV58WWAtributosDS_2_Dynamicfiltersselector1 ,
                                              AV59WWAtributosDS_3_Dynamicfiltersoperator1 ,
                                              AV60WWAtributosDS_4_Atributos_nome1 ,
                                              AV61WWAtributosDS_5_Atributos_tabelanom1 ,
                                              AV62WWAtributosDS_6_Dynamicfiltersenabled2 ,
                                              AV63WWAtributosDS_7_Dynamicfiltersselector2 ,
                                              AV64WWAtributosDS_8_Dynamicfiltersoperator2 ,
                                              AV65WWAtributosDS_9_Atributos_nome2 ,
                                              AV66WWAtributosDS_10_Atributos_tabelanom2 ,
                                              AV68WWAtributosDS_12_Tfatributos_nome_sel ,
                                              AV67WWAtributosDS_11_Tfatributos_nome ,
                                              AV69WWAtributosDS_13_Tfatributos_tipodados_sels.Count ,
                                              AV71WWAtributosDS_15_Tfatributos_detalhes_sel ,
                                              AV70WWAtributosDS_14_Tfatributos_detalhes ,
                                              AV73WWAtributosDS_17_Tfatributos_tabelanom_sel ,
                                              AV72WWAtributosDS_16_Tfatributos_tabelanom ,
                                              AV74WWAtributosDS_18_Tfatributos_ativo_sel ,
                                              AV75WWAtributosDS_19_Tfatributos_pk_sel ,
                                              AV76WWAtributosDS_20_Tfatributos_fk_sel ,
                                              A135Sistema_AreaTrabalhoCod ,
                                              A177Atributos_Nome ,
                                              A357Atributos_TabelaNom ,
                                              A390Atributos_Detalhes ,
                                              A180Atributos_Ativo ,
                                              A400Atributos_PK ,
                                              A401Atributos_FK },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT,
                                              TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN,
                                              TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN
                                              }
         });
         lV60WWAtributosDS_4_Atributos_nome1 = StringUtil.PadR( StringUtil.RTrim( AV60WWAtributosDS_4_Atributos_nome1), 50, "%");
         lV60WWAtributosDS_4_Atributos_nome1 = StringUtil.PadR( StringUtil.RTrim( AV60WWAtributosDS_4_Atributos_nome1), 50, "%");
         lV61WWAtributosDS_5_Atributos_tabelanom1 = StringUtil.PadR( StringUtil.RTrim( AV61WWAtributosDS_5_Atributos_tabelanom1), 50, "%");
         lV61WWAtributosDS_5_Atributos_tabelanom1 = StringUtil.PadR( StringUtil.RTrim( AV61WWAtributosDS_5_Atributos_tabelanom1), 50, "%");
         lV65WWAtributosDS_9_Atributos_nome2 = StringUtil.PadR( StringUtil.RTrim( AV65WWAtributosDS_9_Atributos_nome2), 50, "%");
         lV65WWAtributosDS_9_Atributos_nome2 = StringUtil.PadR( StringUtil.RTrim( AV65WWAtributosDS_9_Atributos_nome2), 50, "%");
         lV66WWAtributosDS_10_Atributos_tabelanom2 = StringUtil.PadR( StringUtil.RTrim( AV66WWAtributosDS_10_Atributos_tabelanom2), 50, "%");
         lV66WWAtributosDS_10_Atributos_tabelanom2 = StringUtil.PadR( StringUtil.RTrim( AV66WWAtributosDS_10_Atributos_tabelanom2), 50, "%");
         lV67WWAtributosDS_11_Tfatributos_nome = StringUtil.PadR( StringUtil.RTrim( AV67WWAtributosDS_11_Tfatributos_nome), 50, "%");
         lV70WWAtributosDS_14_Tfatributos_detalhes = StringUtil.PadR( StringUtil.RTrim( AV70WWAtributosDS_14_Tfatributos_detalhes), 10, "%");
         lV72WWAtributosDS_16_Tfatributos_tabelanom = StringUtil.PadR( StringUtil.RTrim( AV72WWAtributosDS_16_Tfatributos_tabelanom), 50, "%");
         /* Using cursor P00H34 */
         pr_default.execute(2, new Object[] {AV57WWAtributosDS_1_Sistema_areatrabalhocod, lV60WWAtributosDS_4_Atributos_nome1, lV60WWAtributosDS_4_Atributos_nome1, lV61WWAtributosDS_5_Atributos_tabelanom1, lV61WWAtributosDS_5_Atributos_tabelanom1, lV65WWAtributosDS_9_Atributos_nome2, lV65WWAtributosDS_9_Atributos_nome2, lV66WWAtributosDS_10_Atributos_tabelanom2, lV66WWAtributosDS_10_Atributos_tabelanom2, lV67WWAtributosDS_11_Tfatributos_nome, AV68WWAtributosDS_12_Tfatributos_nome_sel, lV70WWAtributosDS_14_Tfatributos_detalhes, AV71WWAtributosDS_15_Tfatributos_detalhes_sel, lV72WWAtributosDS_16_Tfatributos_tabelanom, AV73WWAtributosDS_17_Tfatributos_tabelanom_sel});
         while ( (pr_default.getStatus(2) != 101) )
         {
            BRKH36 = false;
            A190Tabela_SistemaCod = P00H34_A190Tabela_SistemaCod[0];
            n190Tabela_SistemaCod = P00H34_n190Tabela_SistemaCod[0];
            A356Atributos_TabelaCod = P00H34_A356Atributos_TabelaCod[0];
            A401Atributos_FK = P00H34_A401Atributos_FK[0];
            n401Atributos_FK = P00H34_n401Atributos_FK[0];
            A400Atributos_PK = P00H34_A400Atributos_PK[0];
            n400Atributos_PK = P00H34_n400Atributos_PK[0];
            A180Atributos_Ativo = P00H34_A180Atributos_Ativo[0];
            A390Atributos_Detalhes = P00H34_A390Atributos_Detalhes[0];
            n390Atributos_Detalhes = P00H34_n390Atributos_Detalhes[0];
            A178Atributos_TipoDados = P00H34_A178Atributos_TipoDados[0];
            n178Atributos_TipoDados = P00H34_n178Atributos_TipoDados[0];
            A357Atributos_TabelaNom = P00H34_A357Atributos_TabelaNom[0];
            n357Atributos_TabelaNom = P00H34_n357Atributos_TabelaNom[0];
            A177Atributos_Nome = P00H34_A177Atributos_Nome[0];
            A135Sistema_AreaTrabalhoCod = P00H34_A135Sistema_AreaTrabalhoCod[0];
            n135Sistema_AreaTrabalhoCod = P00H34_n135Sistema_AreaTrabalhoCod[0];
            A176Atributos_Codigo = P00H34_A176Atributos_Codigo[0];
            A190Tabela_SistemaCod = P00H34_A190Tabela_SistemaCod[0];
            n190Tabela_SistemaCod = P00H34_n190Tabela_SistemaCod[0];
            A357Atributos_TabelaNom = P00H34_A357Atributos_TabelaNom[0];
            n357Atributos_TabelaNom = P00H34_n357Atributos_TabelaNom[0];
            A135Sistema_AreaTrabalhoCod = P00H34_A135Sistema_AreaTrabalhoCod[0];
            n135Sistema_AreaTrabalhoCod = P00H34_n135Sistema_AreaTrabalhoCod[0];
            AV33count = 0;
            while ( (pr_default.getStatus(2) != 101) && ( P00H34_A356Atributos_TabelaCod[0] == A356Atributos_TabelaCod ) )
            {
               BRKH36 = false;
               A176Atributos_Codigo = P00H34_A176Atributos_Codigo[0];
               AV33count = (long)(AV33count+1);
               BRKH36 = true;
               pr_default.readNext(2);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A357Atributos_TabelaNom)) )
            {
               AV25Option = A357Atributos_TabelaNom;
               AV24InsertIndex = 1;
               while ( ( AV24InsertIndex <= AV26Options.Count ) && ( StringUtil.StrCmp(((String)AV26Options.Item(AV24InsertIndex)), AV25Option) < 0 ) )
               {
                  AV24InsertIndex = (int)(AV24InsertIndex+1);
               }
               AV26Options.Add(AV25Option, AV24InsertIndex);
               AV31OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV33count), "Z,ZZZ,ZZZ,ZZ9")), AV24InsertIndex);
            }
            if ( AV26Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKH36 )
            {
               BRKH36 = true;
               pr_default.readNext(2);
            }
         }
         pr_default.close(2);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV26Options = new GxSimpleCollection();
         AV29OptionsDesc = new GxSimpleCollection();
         AV31OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV34Session = context.GetSession();
         AV36GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV37GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFAtributos_Nome = "";
         AV11TFAtributos_Nome_Sel = "";
         AV12TFAtributos_TipoDados_SelsJson = "";
         AV13TFAtributos_TipoDados_Sels = new GxSimpleCollection();
         AV14TFAtributos_Detalhes = "";
         AV15TFAtributos_Detalhes_Sel = "";
         AV16TFAtributos_TabelaNom = "";
         AV17TFAtributos_TabelaNom_Sel = "";
         AV38GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV40DynamicFiltersSelector1 = "";
         AV42Atributos_Nome1 = "";
         AV43Atributos_TabelaNom1 = "";
         AV45DynamicFiltersSelector2 = "";
         AV47Atributos_Nome2 = "";
         AV48Atributos_TabelaNom2 = "";
         AV58WWAtributosDS_2_Dynamicfiltersselector1 = "";
         AV60WWAtributosDS_4_Atributos_nome1 = "";
         AV61WWAtributosDS_5_Atributos_tabelanom1 = "";
         AV63WWAtributosDS_7_Dynamicfiltersselector2 = "";
         AV65WWAtributosDS_9_Atributos_nome2 = "";
         AV66WWAtributosDS_10_Atributos_tabelanom2 = "";
         AV67WWAtributosDS_11_Tfatributos_nome = "";
         AV68WWAtributosDS_12_Tfatributos_nome_sel = "";
         AV69WWAtributosDS_13_Tfatributos_tipodados_sels = new GxSimpleCollection();
         AV70WWAtributosDS_14_Tfatributos_detalhes = "";
         AV71WWAtributosDS_15_Tfatributos_detalhes_sel = "";
         AV72WWAtributosDS_16_Tfatributos_tabelanom = "";
         AV73WWAtributosDS_17_Tfatributos_tabelanom_sel = "";
         scmdbuf = "";
         lV60WWAtributosDS_4_Atributos_nome1 = "";
         lV61WWAtributosDS_5_Atributos_tabelanom1 = "";
         lV65WWAtributosDS_9_Atributos_nome2 = "";
         lV66WWAtributosDS_10_Atributos_tabelanom2 = "";
         lV67WWAtributosDS_11_Tfatributos_nome = "";
         lV70WWAtributosDS_14_Tfatributos_detalhes = "";
         lV72WWAtributosDS_16_Tfatributos_tabelanom = "";
         A178Atributos_TipoDados = "";
         A177Atributos_Nome = "";
         A357Atributos_TabelaNom = "";
         A390Atributos_Detalhes = "";
         P00H32_A356Atributos_TabelaCod = new int[1] ;
         P00H32_A190Tabela_SistemaCod = new int[1] ;
         P00H32_n190Tabela_SistemaCod = new bool[] {false} ;
         P00H32_A177Atributos_Nome = new String[] {""} ;
         P00H32_A401Atributos_FK = new bool[] {false} ;
         P00H32_n401Atributos_FK = new bool[] {false} ;
         P00H32_A400Atributos_PK = new bool[] {false} ;
         P00H32_n400Atributos_PK = new bool[] {false} ;
         P00H32_A180Atributos_Ativo = new bool[] {false} ;
         P00H32_A390Atributos_Detalhes = new String[] {""} ;
         P00H32_n390Atributos_Detalhes = new bool[] {false} ;
         P00H32_A178Atributos_TipoDados = new String[] {""} ;
         P00H32_n178Atributos_TipoDados = new bool[] {false} ;
         P00H32_A357Atributos_TabelaNom = new String[] {""} ;
         P00H32_n357Atributos_TabelaNom = new bool[] {false} ;
         P00H32_A135Sistema_AreaTrabalhoCod = new int[1] ;
         P00H32_n135Sistema_AreaTrabalhoCod = new bool[] {false} ;
         P00H32_A176Atributos_Codigo = new int[1] ;
         AV25Option = "";
         P00H33_A356Atributos_TabelaCod = new int[1] ;
         P00H33_A190Tabela_SistemaCod = new int[1] ;
         P00H33_n190Tabela_SistemaCod = new bool[] {false} ;
         P00H33_A390Atributos_Detalhes = new String[] {""} ;
         P00H33_n390Atributos_Detalhes = new bool[] {false} ;
         P00H33_A401Atributos_FK = new bool[] {false} ;
         P00H33_n401Atributos_FK = new bool[] {false} ;
         P00H33_A400Atributos_PK = new bool[] {false} ;
         P00H33_n400Atributos_PK = new bool[] {false} ;
         P00H33_A180Atributos_Ativo = new bool[] {false} ;
         P00H33_A178Atributos_TipoDados = new String[] {""} ;
         P00H33_n178Atributos_TipoDados = new bool[] {false} ;
         P00H33_A357Atributos_TabelaNom = new String[] {""} ;
         P00H33_n357Atributos_TabelaNom = new bool[] {false} ;
         P00H33_A177Atributos_Nome = new String[] {""} ;
         P00H33_A135Sistema_AreaTrabalhoCod = new int[1] ;
         P00H33_n135Sistema_AreaTrabalhoCod = new bool[] {false} ;
         P00H33_A176Atributos_Codigo = new int[1] ;
         P00H34_A190Tabela_SistemaCod = new int[1] ;
         P00H34_n190Tabela_SistemaCod = new bool[] {false} ;
         P00H34_A356Atributos_TabelaCod = new int[1] ;
         P00H34_A401Atributos_FK = new bool[] {false} ;
         P00H34_n401Atributos_FK = new bool[] {false} ;
         P00H34_A400Atributos_PK = new bool[] {false} ;
         P00H34_n400Atributos_PK = new bool[] {false} ;
         P00H34_A180Atributos_Ativo = new bool[] {false} ;
         P00H34_A390Atributos_Detalhes = new String[] {""} ;
         P00H34_n390Atributos_Detalhes = new bool[] {false} ;
         P00H34_A178Atributos_TipoDados = new String[] {""} ;
         P00H34_n178Atributos_TipoDados = new bool[] {false} ;
         P00H34_A357Atributos_TabelaNom = new String[] {""} ;
         P00H34_n357Atributos_TabelaNom = new bool[] {false} ;
         P00H34_A177Atributos_Nome = new String[] {""} ;
         P00H34_A135Sistema_AreaTrabalhoCod = new int[1] ;
         P00H34_n135Sistema_AreaTrabalhoCod = new bool[] {false} ;
         P00H34_A176Atributos_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getwwatributosfilterdata__default(),
            new Object[][] {
                new Object[] {
               P00H32_A356Atributos_TabelaCod, P00H32_A190Tabela_SistemaCod, P00H32_n190Tabela_SistemaCod, P00H32_A177Atributos_Nome, P00H32_A401Atributos_FK, P00H32_n401Atributos_FK, P00H32_A400Atributos_PK, P00H32_n400Atributos_PK, P00H32_A180Atributos_Ativo, P00H32_A390Atributos_Detalhes,
               P00H32_n390Atributos_Detalhes, P00H32_A178Atributos_TipoDados, P00H32_n178Atributos_TipoDados, P00H32_A357Atributos_TabelaNom, P00H32_n357Atributos_TabelaNom, P00H32_A135Sistema_AreaTrabalhoCod, P00H32_n135Sistema_AreaTrabalhoCod, P00H32_A176Atributos_Codigo
               }
               , new Object[] {
               P00H33_A356Atributos_TabelaCod, P00H33_A190Tabela_SistemaCod, P00H33_n190Tabela_SistemaCod, P00H33_A390Atributos_Detalhes, P00H33_n390Atributos_Detalhes, P00H33_A401Atributos_FK, P00H33_n401Atributos_FK, P00H33_A400Atributos_PK, P00H33_n400Atributos_PK, P00H33_A180Atributos_Ativo,
               P00H33_A178Atributos_TipoDados, P00H33_n178Atributos_TipoDados, P00H33_A357Atributos_TabelaNom, P00H33_n357Atributos_TabelaNom, P00H33_A177Atributos_Nome, P00H33_A135Sistema_AreaTrabalhoCod, P00H33_n135Sistema_AreaTrabalhoCod, P00H33_A176Atributos_Codigo
               }
               , new Object[] {
               P00H34_A190Tabela_SistemaCod, P00H34_n190Tabela_SistemaCod, P00H34_A356Atributos_TabelaCod, P00H34_A401Atributos_FK, P00H34_n401Atributos_FK, P00H34_A400Atributos_PK, P00H34_n400Atributos_PK, P00H34_A180Atributos_Ativo, P00H34_A390Atributos_Detalhes, P00H34_n390Atributos_Detalhes,
               P00H34_A178Atributos_TipoDados, P00H34_n178Atributos_TipoDados, P00H34_A357Atributos_TabelaNom, P00H34_n357Atributos_TabelaNom, P00H34_A177Atributos_Nome, P00H34_A135Sistema_AreaTrabalhoCod, P00H34_n135Sistema_AreaTrabalhoCod, P00H34_A176Atributos_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV18TFAtributos_Ativo_Sel ;
      private short AV19TFAtributos_PK_Sel ;
      private short AV20TFAtributos_FK_Sel ;
      private short AV41DynamicFiltersOperator1 ;
      private short AV46DynamicFiltersOperator2 ;
      private short AV59WWAtributosDS_3_Dynamicfiltersoperator1 ;
      private short AV64WWAtributosDS_8_Dynamicfiltersoperator2 ;
      private short AV74WWAtributosDS_18_Tfatributos_ativo_sel ;
      private short AV75WWAtributosDS_19_Tfatributos_pk_sel ;
      private short AV76WWAtributosDS_20_Tfatributos_fk_sel ;
      private int AV55GXV1 ;
      private int AV39Sistema_AreaTrabalhoCod ;
      private int AV57WWAtributosDS_1_Sistema_areatrabalhocod ;
      private int AV69WWAtributosDS_13_Tfatributos_tipodados_sels_Count ;
      private int A135Sistema_AreaTrabalhoCod ;
      private int A356Atributos_TabelaCod ;
      private int A190Tabela_SistemaCod ;
      private int A176Atributos_Codigo ;
      private int AV24InsertIndex ;
      private long AV33count ;
      private String AV10TFAtributos_Nome ;
      private String AV11TFAtributos_Nome_Sel ;
      private String AV14TFAtributos_Detalhes ;
      private String AV15TFAtributos_Detalhes_Sel ;
      private String AV16TFAtributos_TabelaNom ;
      private String AV17TFAtributos_TabelaNom_Sel ;
      private String AV42Atributos_Nome1 ;
      private String AV43Atributos_TabelaNom1 ;
      private String AV47Atributos_Nome2 ;
      private String AV48Atributos_TabelaNom2 ;
      private String AV60WWAtributosDS_4_Atributos_nome1 ;
      private String AV61WWAtributosDS_5_Atributos_tabelanom1 ;
      private String AV65WWAtributosDS_9_Atributos_nome2 ;
      private String AV66WWAtributosDS_10_Atributos_tabelanom2 ;
      private String AV67WWAtributosDS_11_Tfatributos_nome ;
      private String AV68WWAtributosDS_12_Tfatributos_nome_sel ;
      private String AV70WWAtributosDS_14_Tfatributos_detalhes ;
      private String AV71WWAtributosDS_15_Tfatributos_detalhes_sel ;
      private String AV72WWAtributosDS_16_Tfatributos_tabelanom ;
      private String AV73WWAtributosDS_17_Tfatributos_tabelanom_sel ;
      private String scmdbuf ;
      private String lV60WWAtributosDS_4_Atributos_nome1 ;
      private String lV61WWAtributosDS_5_Atributos_tabelanom1 ;
      private String lV65WWAtributosDS_9_Atributos_nome2 ;
      private String lV66WWAtributosDS_10_Atributos_tabelanom2 ;
      private String lV67WWAtributosDS_11_Tfatributos_nome ;
      private String lV70WWAtributosDS_14_Tfatributos_detalhes ;
      private String lV72WWAtributosDS_16_Tfatributos_tabelanom ;
      private String A178Atributos_TipoDados ;
      private String A177Atributos_Nome ;
      private String A357Atributos_TabelaNom ;
      private String A390Atributos_Detalhes ;
      private bool returnInSub ;
      private bool AV44DynamicFiltersEnabled2 ;
      private bool AV62WWAtributosDS_6_Dynamicfiltersenabled2 ;
      private bool A180Atributos_Ativo ;
      private bool A400Atributos_PK ;
      private bool A401Atributos_FK ;
      private bool BRKH32 ;
      private bool n190Tabela_SistemaCod ;
      private bool n401Atributos_FK ;
      private bool n400Atributos_PK ;
      private bool n390Atributos_Detalhes ;
      private bool n178Atributos_TipoDados ;
      private bool n357Atributos_TabelaNom ;
      private bool n135Sistema_AreaTrabalhoCod ;
      private bool BRKH34 ;
      private bool BRKH36 ;
      private String AV32OptionIndexesJson ;
      private String AV27OptionsJson ;
      private String AV30OptionsDescJson ;
      private String AV12TFAtributos_TipoDados_SelsJson ;
      private String AV23DDOName ;
      private String AV21SearchTxt ;
      private String AV22SearchTxtTo ;
      private String AV40DynamicFiltersSelector1 ;
      private String AV45DynamicFiltersSelector2 ;
      private String AV58WWAtributosDS_2_Dynamicfiltersselector1 ;
      private String AV63WWAtributosDS_7_Dynamicfiltersselector2 ;
      private String AV25Option ;
      private IGxSession AV34Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00H32_A356Atributos_TabelaCod ;
      private int[] P00H32_A190Tabela_SistemaCod ;
      private bool[] P00H32_n190Tabela_SistemaCod ;
      private String[] P00H32_A177Atributos_Nome ;
      private bool[] P00H32_A401Atributos_FK ;
      private bool[] P00H32_n401Atributos_FK ;
      private bool[] P00H32_A400Atributos_PK ;
      private bool[] P00H32_n400Atributos_PK ;
      private bool[] P00H32_A180Atributos_Ativo ;
      private String[] P00H32_A390Atributos_Detalhes ;
      private bool[] P00H32_n390Atributos_Detalhes ;
      private String[] P00H32_A178Atributos_TipoDados ;
      private bool[] P00H32_n178Atributos_TipoDados ;
      private String[] P00H32_A357Atributos_TabelaNom ;
      private bool[] P00H32_n357Atributos_TabelaNom ;
      private int[] P00H32_A135Sistema_AreaTrabalhoCod ;
      private bool[] P00H32_n135Sistema_AreaTrabalhoCod ;
      private int[] P00H32_A176Atributos_Codigo ;
      private int[] P00H33_A356Atributos_TabelaCod ;
      private int[] P00H33_A190Tabela_SistemaCod ;
      private bool[] P00H33_n190Tabela_SistemaCod ;
      private String[] P00H33_A390Atributos_Detalhes ;
      private bool[] P00H33_n390Atributos_Detalhes ;
      private bool[] P00H33_A401Atributos_FK ;
      private bool[] P00H33_n401Atributos_FK ;
      private bool[] P00H33_A400Atributos_PK ;
      private bool[] P00H33_n400Atributos_PK ;
      private bool[] P00H33_A180Atributos_Ativo ;
      private String[] P00H33_A178Atributos_TipoDados ;
      private bool[] P00H33_n178Atributos_TipoDados ;
      private String[] P00H33_A357Atributos_TabelaNom ;
      private bool[] P00H33_n357Atributos_TabelaNom ;
      private String[] P00H33_A177Atributos_Nome ;
      private int[] P00H33_A135Sistema_AreaTrabalhoCod ;
      private bool[] P00H33_n135Sistema_AreaTrabalhoCod ;
      private int[] P00H33_A176Atributos_Codigo ;
      private int[] P00H34_A190Tabela_SistemaCod ;
      private bool[] P00H34_n190Tabela_SistemaCod ;
      private int[] P00H34_A356Atributos_TabelaCod ;
      private bool[] P00H34_A401Atributos_FK ;
      private bool[] P00H34_n401Atributos_FK ;
      private bool[] P00H34_A400Atributos_PK ;
      private bool[] P00H34_n400Atributos_PK ;
      private bool[] P00H34_A180Atributos_Ativo ;
      private String[] P00H34_A390Atributos_Detalhes ;
      private bool[] P00H34_n390Atributos_Detalhes ;
      private String[] P00H34_A178Atributos_TipoDados ;
      private bool[] P00H34_n178Atributos_TipoDados ;
      private String[] P00H34_A357Atributos_TabelaNom ;
      private bool[] P00H34_n357Atributos_TabelaNom ;
      private String[] P00H34_A177Atributos_Nome ;
      private int[] P00H34_A135Sistema_AreaTrabalhoCod ;
      private bool[] P00H34_n135Sistema_AreaTrabalhoCod ;
      private int[] P00H34_A176Atributos_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV13TFAtributos_TipoDados_Sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV69WWAtributosDS_13_Tfatributos_tipodados_sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV26Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV29OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV31OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV36GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV37GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV38GridStateDynamicFilter ;
   }

   public class getwwatributosfilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00H32( IGxContext context ,
                                             String A178Atributos_TipoDados ,
                                             IGxCollection AV69WWAtributosDS_13_Tfatributos_tipodados_sels ,
                                             int AV57WWAtributosDS_1_Sistema_areatrabalhocod ,
                                             String AV58WWAtributosDS_2_Dynamicfiltersselector1 ,
                                             short AV59WWAtributosDS_3_Dynamicfiltersoperator1 ,
                                             String AV60WWAtributosDS_4_Atributos_nome1 ,
                                             String AV61WWAtributosDS_5_Atributos_tabelanom1 ,
                                             bool AV62WWAtributosDS_6_Dynamicfiltersenabled2 ,
                                             String AV63WWAtributosDS_7_Dynamicfiltersselector2 ,
                                             short AV64WWAtributosDS_8_Dynamicfiltersoperator2 ,
                                             String AV65WWAtributosDS_9_Atributos_nome2 ,
                                             String AV66WWAtributosDS_10_Atributos_tabelanom2 ,
                                             String AV68WWAtributosDS_12_Tfatributos_nome_sel ,
                                             String AV67WWAtributosDS_11_Tfatributos_nome ,
                                             int AV69WWAtributosDS_13_Tfatributos_tipodados_sels_Count ,
                                             String AV71WWAtributosDS_15_Tfatributos_detalhes_sel ,
                                             String AV70WWAtributosDS_14_Tfatributos_detalhes ,
                                             String AV73WWAtributosDS_17_Tfatributos_tabelanom_sel ,
                                             String AV72WWAtributosDS_16_Tfatributos_tabelanom ,
                                             short AV74WWAtributosDS_18_Tfatributos_ativo_sel ,
                                             short AV75WWAtributosDS_19_Tfatributos_pk_sel ,
                                             short AV76WWAtributosDS_20_Tfatributos_fk_sel ,
                                             int A135Sistema_AreaTrabalhoCod ,
                                             String A177Atributos_Nome ,
                                             String A357Atributos_TabelaNom ,
                                             String A390Atributos_Detalhes ,
                                             bool A180Atributos_Ativo ,
                                             bool A400Atributos_PK ,
                                             bool A401Atributos_FK )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [15] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[Atributos_TabelaCod] AS Atributos_TabelaCod, T2.[Tabela_SistemaCod] AS Tabela_SistemaCod, T1.[Atributos_Nome], T1.[Atributos_FK], T1.[Atributos_PK], T1.[Atributos_Ativo], T1.[Atributos_Detalhes], T1.[Atributos_TipoDados], T2.[Tabela_Nome] AS Atributos_TabelaNom, T3.[Sistema_AreaTrabalhoCod], T1.[Atributos_Codigo] FROM (([Atributos] T1 WITH (NOLOCK) INNER JOIN [Tabela] T2 WITH (NOLOCK) ON T2.[Tabela_Codigo] = T1.[Atributos_TabelaCod]) LEFT JOIN [Sistema] T3 WITH (NOLOCK) ON T3.[Sistema_Codigo] = T2.[Tabela_SistemaCod])";
         if ( ! (0==AV57WWAtributosDS_1_Sistema_areatrabalhocod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Sistema_AreaTrabalhoCod] = @AV57WWAtributosDS_1_Sistema_areatrabalhocod)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Sistema_AreaTrabalhoCod] = @AV57WWAtributosDS_1_Sistema_areatrabalhocod)";
            }
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV58WWAtributosDS_2_Dynamicfiltersselector1, "ATRIBUTOS_NOME") == 0 ) && ( AV59WWAtributosDS_3_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60WWAtributosDS_4_Atributos_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Atributos_Nome] like @lV60WWAtributosDS_4_Atributos_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Atributos_Nome] like @lV60WWAtributosDS_4_Atributos_nome1)";
            }
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV58WWAtributosDS_2_Dynamicfiltersselector1, "ATRIBUTOS_NOME") == 0 ) && ( AV59WWAtributosDS_3_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60WWAtributosDS_4_Atributos_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Atributos_Nome] like '%' + @lV60WWAtributosDS_4_Atributos_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Atributos_Nome] like '%' + @lV60WWAtributosDS_4_Atributos_nome1)";
            }
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV58WWAtributosDS_2_Dynamicfiltersselector1, "ATRIBUTOS_TABELANOM") == 0 ) && ( AV59WWAtributosDS_3_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61WWAtributosDS_5_Atributos_tabelanom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Tabela_Nome] like @lV61WWAtributosDS_5_Atributos_tabelanom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Tabela_Nome] like @lV61WWAtributosDS_5_Atributos_tabelanom1)";
            }
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV58WWAtributosDS_2_Dynamicfiltersselector1, "ATRIBUTOS_TABELANOM") == 0 ) && ( AV59WWAtributosDS_3_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61WWAtributosDS_5_Atributos_tabelanom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Tabela_Nome] like '%' + @lV61WWAtributosDS_5_Atributos_tabelanom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Tabela_Nome] like '%' + @lV61WWAtributosDS_5_Atributos_tabelanom1)";
            }
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( AV62WWAtributosDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV63WWAtributosDS_7_Dynamicfiltersselector2, "ATRIBUTOS_NOME") == 0 ) && ( AV64WWAtributosDS_8_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65WWAtributosDS_9_Atributos_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Atributos_Nome] like @lV65WWAtributosDS_9_Atributos_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Atributos_Nome] like @lV65WWAtributosDS_9_Atributos_nome2)";
            }
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( AV62WWAtributosDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV63WWAtributosDS_7_Dynamicfiltersselector2, "ATRIBUTOS_NOME") == 0 ) && ( AV64WWAtributosDS_8_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65WWAtributosDS_9_Atributos_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Atributos_Nome] like '%' + @lV65WWAtributosDS_9_Atributos_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Atributos_Nome] like '%' + @lV65WWAtributosDS_9_Atributos_nome2)";
            }
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( AV62WWAtributosDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV63WWAtributosDS_7_Dynamicfiltersselector2, "ATRIBUTOS_TABELANOM") == 0 ) && ( AV64WWAtributosDS_8_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66WWAtributosDS_10_Atributos_tabelanom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Tabela_Nome] like @lV66WWAtributosDS_10_Atributos_tabelanom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Tabela_Nome] like @lV66WWAtributosDS_10_Atributos_tabelanom2)";
            }
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( AV62WWAtributosDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV63WWAtributosDS_7_Dynamicfiltersselector2, "ATRIBUTOS_TABELANOM") == 0 ) && ( AV64WWAtributosDS_8_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66WWAtributosDS_10_Atributos_tabelanom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Tabela_Nome] like '%' + @lV66WWAtributosDS_10_Atributos_tabelanom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Tabela_Nome] like '%' + @lV66WWAtributosDS_10_Atributos_tabelanom2)";
            }
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV68WWAtributosDS_12_Tfatributos_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67WWAtributosDS_11_Tfatributos_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Atributos_Nome] like @lV67WWAtributosDS_11_Tfatributos_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Atributos_Nome] like @lV67WWAtributosDS_11_Tfatributos_nome)";
            }
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV68WWAtributosDS_12_Tfatributos_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Atributos_Nome] = @AV68WWAtributosDS_12_Tfatributos_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Atributos_Nome] = @AV68WWAtributosDS_12_Tfatributos_nome_sel)";
            }
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( AV69WWAtributosDS_13_Tfatributos_tipodados_sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV69WWAtributosDS_13_Tfatributos_tipodados_sels, "T1.[Atributos_TipoDados] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV69WWAtributosDS_13_Tfatributos_tipodados_sels, "T1.[Atributos_TipoDados] IN (", ")") + ")";
            }
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV71WWAtributosDS_15_Tfatributos_detalhes_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV70WWAtributosDS_14_Tfatributos_detalhes)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Atributos_Detalhes] like @lV70WWAtributosDS_14_Tfatributos_detalhes)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Atributos_Detalhes] like @lV70WWAtributosDS_14_Tfatributos_detalhes)";
            }
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV71WWAtributosDS_15_Tfatributos_detalhes_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Atributos_Detalhes] = @AV71WWAtributosDS_15_Tfatributos_detalhes_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Atributos_Detalhes] = @AV71WWAtributosDS_15_Tfatributos_detalhes_sel)";
            }
         }
         else
         {
            GXv_int1[12] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV73WWAtributosDS_17_Tfatributos_tabelanom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV72WWAtributosDS_16_Tfatributos_tabelanom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Tabela_Nome] like @lV72WWAtributosDS_16_Tfatributos_tabelanom)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Tabela_Nome] like @lV72WWAtributosDS_16_Tfatributos_tabelanom)";
            }
         }
         else
         {
            GXv_int1[13] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73WWAtributosDS_17_Tfatributos_tabelanom_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Tabela_Nome] = @AV73WWAtributosDS_17_Tfatributos_tabelanom_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Tabela_Nome] = @AV73WWAtributosDS_17_Tfatributos_tabelanom_sel)";
            }
         }
         else
         {
            GXv_int1[14] = 1;
         }
         if ( AV74WWAtributosDS_18_Tfatributos_ativo_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Atributos_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Atributos_Ativo] = 1)";
            }
         }
         if ( AV74WWAtributosDS_18_Tfatributos_ativo_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Atributos_Ativo] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Atributos_Ativo] = 0)";
            }
         }
         if ( AV75WWAtributosDS_19_Tfatributos_pk_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Atributos_PK] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Atributos_PK] = 1)";
            }
         }
         if ( AV75WWAtributosDS_19_Tfatributos_pk_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Atributos_PK] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Atributos_PK] = 0)";
            }
         }
         if ( AV76WWAtributosDS_20_Tfatributos_fk_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Atributos_FK] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Atributos_FK] = 1)";
            }
         }
         if ( AV76WWAtributosDS_20_Tfatributos_fk_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Atributos_FK] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Atributos_FK] = 0)";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[Atributos_Nome]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00H33( IGxContext context ,
                                             String A178Atributos_TipoDados ,
                                             IGxCollection AV69WWAtributosDS_13_Tfatributos_tipodados_sels ,
                                             int AV57WWAtributosDS_1_Sistema_areatrabalhocod ,
                                             String AV58WWAtributosDS_2_Dynamicfiltersselector1 ,
                                             short AV59WWAtributosDS_3_Dynamicfiltersoperator1 ,
                                             String AV60WWAtributosDS_4_Atributos_nome1 ,
                                             String AV61WWAtributosDS_5_Atributos_tabelanom1 ,
                                             bool AV62WWAtributosDS_6_Dynamicfiltersenabled2 ,
                                             String AV63WWAtributosDS_7_Dynamicfiltersselector2 ,
                                             short AV64WWAtributosDS_8_Dynamicfiltersoperator2 ,
                                             String AV65WWAtributosDS_9_Atributos_nome2 ,
                                             String AV66WWAtributosDS_10_Atributos_tabelanom2 ,
                                             String AV68WWAtributosDS_12_Tfatributos_nome_sel ,
                                             String AV67WWAtributosDS_11_Tfatributos_nome ,
                                             int AV69WWAtributosDS_13_Tfatributos_tipodados_sels_Count ,
                                             String AV71WWAtributosDS_15_Tfatributos_detalhes_sel ,
                                             String AV70WWAtributosDS_14_Tfatributos_detalhes ,
                                             String AV73WWAtributosDS_17_Tfatributos_tabelanom_sel ,
                                             String AV72WWAtributosDS_16_Tfatributos_tabelanom ,
                                             short AV74WWAtributosDS_18_Tfatributos_ativo_sel ,
                                             short AV75WWAtributosDS_19_Tfatributos_pk_sel ,
                                             short AV76WWAtributosDS_20_Tfatributos_fk_sel ,
                                             int A135Sistema_AreaTrabalhoCod ,
                                             String A177Atributos_Nome ,
                                             String A357Atributos_TabelaNom ,
                                             String A390Atributos_Detalhes ,
                                             bool A180Atributos_Ativo ,
                                             bool A400Atributos_PK ,
                                             bool A401Atributos_FK )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [15] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT T1.[Atributos_TabelaCod] AS Atributos_TabelaCod, T2.[Tabela_SistemaCod] AS Tabela_SistemaCod, T1.[Atributos_Detalhes], T1.[Atributos_FK], T1.[Atributos_PK], T1.[Atributos_Ativo], T1.[Atributos_TipoDados], T2.[Tabela_Nome] AS Atributos_TabelaNom, T1.[Atributos_Nome], T3.[Sistema_AreaTrabalhoCod], T1.[Atributos_Codigo] FROM (([Atributos] T1 WITH (NOLOCK) INNER JOIN [Tabela] T2 WITH (NOLOCK) ON T2.[Tabela_Codigo] = T1.[Atributos_TabelaCod]) LEFT JOIN [Sistema] T3 WITH (NOLOCK) ON T3.[Sistema_Codigo] = T2.[Tabela_SistemaCod])";
         if ( ! (0==AV57WWAtributosDS_1_Sistema_areatrabalhocod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Sistema_AreaTrabalhoCod] = @AV57WWAtributosDS_1_Sistema_areatrabalhocod)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Sistema_AreaTrabalhoCod] = @AV57WWAtributosDS_1_Sistema_areatrabalhocod)";
            }
         }
         else
         {
            GXv_int3[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV58WWAtributosDS_2_Dynamicfiltersselector1, "ATRIBUTOS_NOME") == 0 ) && ( AV59WWAtributosDS_3_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60WWAtributosDS_4_Atributos_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Atributos_Nome] like @lV60WWAtributosDS_4_Atributos_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Atributos_Nome] like @lV60WWAtributosDS_4_Atributos_nome1)";
            }
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV58WWAtributosDS_2_Dynamicfiltersselector1, "ATRIBUTOS_NOME") == 0 ) && ( AV59WWAtributosDS_3_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60WWAtributosDS_4_Atributos_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Atributos_Nome] like '%' + @lV60WWAtributosDS_4_Atributos_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Atributos_Nome] like '%' + @lV60WWAtributosDS_4_Atributos_nome1)";
            }
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV58WWAtributosDS_2_Dynamicfiltersselector1, "ATRIBUTOS_TABELANOM") == 0 ) && ( AV59WWAtributosDS_3_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61WWAtributosDS_5_Atributos_tabelanom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Tabela_Nome] like @lV61WWAtributosDS_5_Atributos_tabelanom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Tabela_Nome] like @lV61WWAtributosDS_5_Atributos_tabelanom1)";
            }
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV58WWAtributosDS_2_Dynamicfiltersselector1, "ATRIBUTOS_TABELANOM") == 0 ) && ( AV59WWAtributosDS_3_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61WWAtributosDS_5_Atributos_tabelanom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Tabela_Nome] like '%' + @lV61WWAtributosDS_5_Atributos_tabelanom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Tabela_Nome] like '%' + @lV61WWAtributosDS_5_Atributos_tabelanom1)";
            }
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( AV62WWAtributosDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV63WWAtributosDS_7_Dynamicfiltersselector2, "ATRIBUTOS_NOME") == 0 ) && ( AV64WWAtributosDS_8_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65WWAtributosDS_9_Atributos_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Atributos_Nome] like @lV65WWAtributosDS_9_Atributos_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Atributos_Nome] like @lV65WWAtributosDS_9_Atributos_nome2)";
            }
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( AV62WWAtributosDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV63WWAtributosDS_7_Dynamicfiltersselector2, "ATRIBUTOS_NOME") == 0 ) && ( AV64WWAtributosDS_8_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65WWAtributosDS_9_Atributos_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Atributos_Nome] like '%' + @lV65WWAtributosDS_9_Atributos_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Atributos_Nome] like '%' + @lV65WWAtributosDS_9_Atributos_nome2)";
            }
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( AV62WWAtributosDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV63WWAtributosDS_7_Dynamicfiltersselector2, "ATRIBUTOS_TABELANOM") == 0 ) && ( AV64WWAtributosDS_8_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66WWAtributosDS_10_Atributos_tabelanom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Tabela_Nome] like @lV66WWAtributosDS_10_Atributos_tabelanom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Tabela_Nome] like @lV66WWAtributosDS_10_Atributos_tabelanom2)";
            }
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( AV62WWAtributosDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV63WWAtributosDS_7_Dynamicfiltersselector2, "ATRIBUTOS_TABELANOM") == 0 ) && ( AV64WWAtributosDS_8_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66WWAtributosDS_10_Atributos_tabelanom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Tabela_Nome] like '%' + @lV66WWAtributosDS_10_Atributos_tabelanom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Tabela_Nome] like '%' + @lV66WWAtributosDS_10_Atributos_tabelanom2)";
            }
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV68WWAtributosDS_12_Tfatributos_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67WWAtributosDS_11_Tfatributos_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Atributos_Nome] like @lV67WWAtributosDS_11_Tfatributos_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Atributos_Nome] like @lV67WWAtributosDS_11_Tfatributos_nome)";
            }
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV68WWAtributosDS_12_Tfatributos_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Atributos_Nome] = @AV68WWAtributosDS_12_Tfatributos_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Atributos_Nome] = @AV68WWAtributosDS_12_Tfatributos_nome_sel)";
            }
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( AV69WWAtributosDS_13_Tfatributos_tipodados_sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV69WWAtributosDS_13_Tfatributos_tipodados_sels, "T1.[Atributos_TipoDados] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV69WWAtributosDS_13_Tfatributos_tipodados_sels, "T1.[Atributos_TipoDados] IN (", ")") + ")";
            }
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV71WWAtributosDS_15_Tfatributos_detalhes_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV70WWAtributosDS_14_Tfatributos_detalhes)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Atributos_Detalhes] like @lV70WWAtributosDS_14_Tfatributos_detalhes)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Atributos_Detalhes] like @lV70WWAtributosDS_14_Tfatributos_detalhes)";
            }
         }
         else
         {
            GXv_int3[11] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV71WWAtributosDS_15_Tfatributos_detalhes_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Atributos_Detalhes] = @AV71WWAtributosDS_15_Tfatributos_detalhes_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Atributos_Detalhes] = @AV71WWAtributosDS_15_Tfatributos_detalhes_sel)";
            }
         }
         else
         {
            GXv_int3[12] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV73WWAtributosDS_17_Tfatributos_tabelanom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV72WWAtributosDS_16_Tfatributos_tabelanom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Tabela_Nome] like @lV72WWAtributosDS_16_Tfatributos_tabelanom)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Tabela_Nome] like @lV72WWAtributosDS_16_Tfatributos_tabelanom)";
            }
         }
         else
         {
            GXv_int3[13] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73WWAtributosDS_17_Tfatributos_tabelanom_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Tabela_Nome] = @AV73WWAtributosDS_17_Tfatributos_tabelanom_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Tabela_Nome] = @AV73WWAtributosDS_17_Tfatributos_tabelanom_sel)";
            }
         }
         else
         {
            GXv_int3[14] = 1;
         }
         if ( AV74WWAtributosDS_18_Tfatributos_ativo_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Atributos_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Atributos_Ativo] = 1)";
            }
         }
         if ( AV74WWAtributosDS_18_Tfatributos_ativo_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Atributos_Ativo] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Atributos_Ativo] = 0)";
            }
         }
         if ( AV75WWAtributosDS_19_Tfatributos_pk_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Atributos_PK] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Atributos_PK] = 1)";
            }
         }
         if ( AV75WWAtributosDS_19_Tfatributos_pk_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Atributos_PK] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Atributos_PK] = 0)";
            }
         }
         if ( AV76WWAtributosDS_20_Tfatributos_fk_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Atributos_FK] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Atributos_FK] = 1)";
            }
         }
         if ( AV76WWAtributosDS_20_Tfatributos_fk_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Atributos_FK] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Atributos_FK] = 0)";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[Atributos_Detalhes]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      protected Object[] conditional_P00H34( IGxContext context ,
                                             String A178Atributos_TipoDados ,
                                             IGxCollection AV69WWAtributosDS_13_Tfatributos_tipodados_sels ,
                                             int AV57WWAtributosDS_1_Sistema_areatrabalhocod ,
                                             String AV58WWAtributosDS_2_Dynamicfiltersselector1 ,
                                             short AV59WWAtributosDS_3_Dynamicfiltersoperator1 ,
                                             String AV60WWAtributosDS_4_Atributos_nome1 ,
                                             String AV61WWAtributosDS_5_Atributos_tabelanom1 ,
                                             bool AV62WWAtributosDS_6_Dynamicfiltersenabled2 ,
                                             String AV63WWAtributosDS_7_Dynamicfiltersselector2 ,
                                             short AV64WWAtributosDS_8_Dynamicfiltersoperator2 ,
                                             String AV65WWAtributosDS_9_Atributos_nome2 ,
                                             String AV66WWAtributosDS_10_Atributos_tabelanom2 ,
                                             String AV68WWAtributosDS_12_Tfatributos_nome_sel ,
                                             String AV67WWAtributosDS_11_Tfatributos_nome ,
                                             int AV69WWAtributosDS_13_Tfatributos_tipodados_sels_Count ,
                                             String AV71WWAtributosDS_15_Tfatributos_detalhes_sel ,
                                             String AV70WWAtributosDS_14_Tfatributos_detalhes ,
                                             String AV73WWAtributosDS_17_Tfatributos_tabelanom_sel ,
                                             String AV72WWAtributosDS_16_Tfatributos_tabelanom ,
                                             short AV74WWAtributosDS_18_Tfatributos_ativo_sel ,
                                             short AV75WWAtributosDS_19_Tfatributos_pk_sel ,
                                             short AV76WWAtributosDS_20_Tfatributos_fk_sel ,
                                             int A135Sistema_AreaTrabalhoCod ,
                                             String A177Atributos_Nome ,
                                             String A357Atributos_TabelaNom ,
                                             String A390Atributos_Detalhes ,
                                             bool A180Atributos_Ativo ,
                                             bool A400Atributos_PK ,
                                             bool A401Atributos_FK )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int5 ;
         GXv_int5 = new short [15] ;
         Object[] GXv_Object6 ;
         GXv_Object6 = new Object [2] ;
         scmdbuf = "SELECT T2.[Tabela_SistemaCod] AS Tabela_SistemaCod, T1.[Atributos_TabelaCod] AS Atributos_TabelaCod, T1.[Atributos_FK], T1.[Atributos_PK], T1.[Atributos_Ativo], T1.[Atributos_Detalhes], T1.[Atributos_TipoDados], T2.[Tabela_Nome] AS Atributos_TabelaNom, T1.[Atributos_Nome], T3.[Sistema_AreaTrabalhoCod], T1.[Atributos_Codigo] FROM (([Atributos] T1 WITH (NOLOCK) INNER JOIN [Tabela] T2 WITH (NOLOCK) ON T2.[Tabela_Codigo] = T1.[Atributos_TabelaCod]) LEFT JOIN [Sistema] T3 WITH (NOLOCK) ON T3.[Sistema_Codigo] = T2.[Tabela_SistemaCod])";
         if ( ! (0==AV57WWAtributosDS_1_Sistema_areatrabalhocod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Sistema_AreaTrabalhoCod] = @AV57WWAtributosDS_1_Sistema_areatrabalhocod)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Sistema_AreaTrabalhoCod] = @AV57WWAtributosDS_1_Sistema_areatrabalhocod)";
            }
         }
         else
         {
            GXv_int5[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV58WWAtributosDS_2_Dynamicfiltersselector1, "ATRIBUTOS_NOME") == 0 ) && ( AV59WWAtributosDS_3_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60WWAtributosDS_4_Atributos_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Atributos_Nome] like @lV60WWAtributosDS_4_Atributos_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Atributos_Nome] like @lV60WWAtributosDS_4_Atributos_nome1)";
            }
         }
         else
         {
            GXv_int5[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV58WWAtributosDS_2_Dynamicfiltersselector1, "ATRIBUTOS_NOME") == 0 ) && ( AV59WWAtributosDS_3_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60WWAtributosDS_4_Atributos_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Atributos_Nome] like '%' + @lV60WWAtributosDS_4_Atributos_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Atributos_Nome] like '%' + @lV60WWAtributosDS_4_Atributos_nome1)";
            }
         }
         else
         {
            GXv_int5[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV58WWAtributosDS_2_Dynamicfiltersselector1, "ATRIBUTOS_TABELANOM") == 0 ) && ( AV59WWAtributosDS_3_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61WWAtributosDS_5_Atributos_tabelanom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Tabela_Nome] like @lV61WWAtributosDS_5_Atributos_tabelanom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Tabela_Nome] like @lV61WWAtributosDS_5_Atributos_tabelanom1)";
            }
         }
         else
         {
            GXv_int5[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV58WWAtributosDS_2_Dynamicfiltersselector1, "ATRIBUTOS_TABELANOM") == 0 ) && ( AV59WWAtributosDS_3_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61WWAtributosDS_5_Atributos_tabelanom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Tabela_Nome] like '%' + @lV61WWAtributosDS_5_Atributos_tabelanom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Tabela_Nome] like '%' + @lV61WWAtributosDS_5_Atributos_tabelanom1)";
            }
         }
         else
         {
            GXv_int5[4] = 1;
         }
         if ( AV62WWAtributosDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV63WWAtributosDS_7_Dynamicfiltersselector2, "ATRIBUTOS_NOME") == 0 ) && ( AV64WWAtributosDS_8_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65WWAtributosDS_9_Atributos_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Atributos_Nome] like @lV65WWAtributosDS_9_Atributos_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Atributos_Nome] like @lV65WWAtributosDS_9_Atributos_nome2)";
            }
         }
         else
         {
            GXv_int5[5] = 1;
         }
         if ( AV62WWAtributosDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV63WWAtributosDS_7_Dynamicfiltersselector2, "ATRIBUTOS_NOME") == 0 ) && ( AV64WWAtributosDS_8_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65WWAtributosDS_9_Atributos_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Atributos_Nome] like '%' + @lV65WWAtributosDS_9_Atributos_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Atributos_Nome] like '%' + @lV65WWAtributosDS_9_Atributos_nome2)";
            }
         }
         else
         {
            GXv_int5[6] = 1;
         }
         if ( AV62WWAtributosDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV63WWAtributosDS_7_Dynamicfiltersselector2, "ATRIBUTOS_TABELANOM") == 0 ) && ( AV64WWAtributosDS_8_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66WWAtributosDS_10_Atributos_tabelanom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Tabela_Nome] like @lV66WWAtributosDS_10_Atributos_tabelanom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Tabela_Nome] like @lV66WWAtributosDS_10_Atributos_tabelanom2)";
            }
         }
         else
         {
            GXv_int5[7] = 1;
         }
         if ( AV62WWAtributosDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV63WWAtributosDS_7_Dynamicfiltersselector2, "ATRIBUTOS_TABELANOM") == 0 ) && ( AV64WWAtributosDS_8_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66WWAtributosDS_10_Atributos_tabelanom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Tabela_Nome] like '%' + @lV66WWAtributosDS_10_Atributos_tabelanom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Tabela_Nome] like '%' + @lV66WWAtributosDS_10_Atributos_tabelanom2)";
            }
         }
         else
         {
            GXv_int5[8] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV68WWAtributosDS_12_Tfatributos_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67WWAtributosDS_11_Tfatributos_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Atributos_Nome] like @lV67WWAtributosDS_11_Tfatributos_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Atributos_Nome] like @lV67WWAtributosDS_11_Tfatributos_nome)";
            }
         }
         else
         {
            GXv_int5[9] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV68WWAtributosDS_12_Tfatributos_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Atributos_Nome] = @AV68WWAtributosDS_12_Tfatributos_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Atributos_Nome] = @AV68WWAtributosDS_12_Tfatributos_nome_sel)";
            }
         }
         else
         {
            GXv_int5[10] = 1;
         }
         if ( AV69WWAtributosDS_13_Tfatributos_tipodados_sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV69WWAtributosDS_13_Tfatributos_tipodados_sels, "T1.[Atributos_TipoDados] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV69WWAtributosDS_13_Tfatributos_tipodados_sels, "T1.[Atributos_TipoDados] IN (", ")") + ")";
            }
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV71WWAtributosDS_15_Tfatributos_detalhes_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV70WWAtributosDS_14_Tfatributos_detalhes)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Atributos_Detalhes] like @lV70WWAtributosDS_14_Tfatributos_detalhes)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Atributos_Detalhes] like @lV70WWAtributosDS_14_Tfatributos_detalhes)";
            }
         }
         else
         {
            GXv_int5[11] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV71WWAtributosDS_15_Tfatributos_detalhes_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Atributos_Detalhes] = @AV71WWAtributosDS_15_Tfatributos_detalhes_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Atributos_Detalhes] = @AV71WWAtributosDS_15_Tfatributos_detalhes_sel)";
            }
         }
         else
         {
            GXv_int5[12] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV73WWAtributosDS_17_Tfatributos_tabelanom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV72WWAtributosDS_16_Tfatributos_tabelanom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Tabela_Nome] like @lV72WWAtributosDS_16_Tfatributos_tabelanom)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Tabela_Nome] like @lV72WWAtributosDS_16_Tfatributos_tabelanom)";
            }
         }
         else
         {
            GXv_int5[13] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73WWAtributosDS_17_Tfatributos_tabelanom_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Tabela_Nome] = @AV73WWAtributosDS_17_Tfatributos_tabelanom_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Tabela_Nome] = @AV73WWAtributosDS_17_Tfatributos_tabelanom_sel)";
            }
         }
         else
         {
            GXv_int5[14] = 1;
         }
         if ( AV74WWAtributosDS_18_Tfatributos_ativo_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Atributos_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Atributos_Ativo] = 1)";
            }
         }
         if ( AV74WWAtributosDS_18_Tfatributos_ativo_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Atributos_Ativo] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Atributos_Ativo] = 0)";
            }
         }
         if ( AV75WWAtributosDS_19_Tfatributos_pk_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Atributos_PK] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Atributos_PK] = 1)";
            }
         }
         if ( AV75WWAtributosDS_19_Tfatributos_pk_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Atributos_PK] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Atributos_PK] = 0)";
            }
         }
         if ( AV76WWAtributosDS_20_Tfatributos_fk_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Atributos_FK] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Atributos_FK] = 1)";
            }
         }
         if ( AV76WWAtributosDS_20_Tfatributos_fk_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Atributos_FK] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Atributos_FK] = 0)";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[Atributos_TabelaCod]";
         GXv_Object6[0] = scmdbuf;
         GXv_Object6[1] = GXv_int5;
         return GXv_Object6 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00H32(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (int)dynConstraints[2] , (String)dynConstraints[3] , (short)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (int)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (short)dynConstraints[19] , (short)dynConstraints[20] , (short)dynConstraints[21] , (int)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (bool)dynConstraints[26] , (bool)dynConstraints[27] , (bool)dynConstraints[28] );
               case 1 :
                     return conditional_P00H33(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (int)dynConstraints[2] , (String)dynConstraints[3] , (short)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (int)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (short)dynConstraints[19] , (short)dynConstraints[20] , (short)dynConstraints[21] , (int)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (bool)dynConstraints[26] , (bool)dynConstraints[27] , (bool)dynConstraints[28] );
               case 2 :
                     return conditional_P00H34(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (int)dynConstraints[2] , (String)dynConstraints[3] , (short)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (int)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (short)dynConstraints[19] , (short)dynConstraints[20] , (short)dynConstraints[21] , (int)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (bool)dynConstraints[26] , (bool)dynConstraints[27] , (bool)dynConstraints[28] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00H32 ;
          prmP00H32 = new Object[] {
          new Object[] {"@AV57WWAtributosDS_1_Sistema_areatrabalhocod",SqlDbType.Int,6,0} ,
          new Object[] {"@lV60WWAtributosDS_4_Atributos_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV60WWAtributosDS_4_Atributos_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV61WWAtributosDS_5_Atributos_tabelanom1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV61WWAtributosDS_5_Atributos_tabelanom1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV65WWAtributosDS_9_Atributos_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV65WWAtributosDS_9_Atributos_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV66WWAtributosDS_10_Atributos_tabelanom2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV66WWAtributosDS_10_Atributos_tabelanom2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV67WWAtributosDS_11_Tfatributos_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV68WWAtributosDS_12_Tfatributos_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV70WWAtributosDS_14_Tfatributos_detalhes",SqlDbType.Char,10,0} ,
          new Object[] {"@AV71WWAtributosDS_15_Tfatributos_detalhes_sel",SqlDbType.Char,10,0} ,
          new Object[] {"@lV72WWAtributosDS_16_Tfatributos_tabelanom",SqlDbType.Char,50,0} ,
          new Object[] {"@AV73WWAtributosDS_17_Tfatributos_tabelanom_sel",SqlDbType.Char,50,0}
          } ;
          Object[] prmP00H33 ;
          prmP00H33 = new Object[] {
          new Object[] {"@AV57WWAtributosDS_1_Sistema_areatrabalhocod",SqlDbType.Int,6,0} ,
          new Object[] {"@lV60WWAtributosDS_4_Atributos_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV60WWAtributosDS_4_Atributos_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV61WWAtributosDS_5_Atributos_tabelanom1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV61WWAtributosDS_5_Atributos_tabelanom1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV65WWAtributosDS_9_Atributos_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV65WWAtributosDS_9_Atributos_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV66WWAtributosDS_10_Atributos_tabelanom2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV66WWAtributosDS_10_Atributos_tabelanom2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV67WWAtributosDS_11_Tfatributos_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV68WWAtributosDS_12_Tfatributos_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV70WWAtributosDS_14_Tfatributos_detalhes",SqlDbType.Char,10,0} ,
          new Object[] {"@AV71WWAtributosDS_15_Tfatributos_detalhes_sel",SqlDbType.Char,10,0} ,
          new Object[] {"@lV72WWAtributosDS_16_Tfatributos_tabelanom",SqlDbType.Char,50,0} ,
          new Object[] {"@AV73WWAtributosDS_17_Tfatributos_tabelanom_sel",SqlDbType.Char,50,0}
          } ;
          Object[] prmP00H34 ;
          prmP00H34 = new Object[] {
          new Object[] {"@AV57WWAtributosDS_1_Sistema_areatrabalhocod",SqlDbType.Int,6,0} ,
          new Object[] {"@lV60WWAtributosDS_4_Atributos_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV60WWAtributosDS_4_Atributos_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV61WWAtributosDS_5_Atributos_tabelanom1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV61WWAtributosDS_5_Atributos_tabelanom1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV65WWAtributosDS_9_Atributos_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV65WWAtributosDS_9_Atributos_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV66WWAtributosDS_10_Atributos_tabelanom2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV66WWAtributosDS_10_Atributos_tabelanom2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV67WWAtributosDS_11_Tfatributos_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV68WWAtributosDS_12_Tfatributos_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV70WWAtributosDS_14_Tfatributos_detalhes",SqlDbType.Char,10,0} ,
          new Object[] {"@AV71WWAtributosDS_15_Tfatributos_detalhes_sel",SqlDbType.Char,10,0} ,
          new Object[] {"@lV72WWAtributosDS_16_Tfatributos_tabelanom",SqlDbType.Char,50,0} ,
          new Object[] {"@AV73WWAtributosDS_17_Tfatributos_tabelanom_sel",SqlDbType.Char,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00H32", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00H32,100,0,true,false )
             ,new CursorDef("P00H33", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00H33,100,0,true,false )
             ,new CursorDef("P00H34", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00H34,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 50) ;
                ((bool[]) buf[4])[0] = rslt.getBool(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((bool[]) buf[6])[0] = rslt.getBool(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((bool[]) buf[8])[0] = rslt.getBool(6) ;
                ((String[]) buf[9])[0] = rslt.getString(7, 10) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((String[]) buf[11])[0] = rslt.getString(8, 4) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((String[]) buf[13])[0] = rslt.getString(9, 50) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                ((int[]) buf[15])[0] = rslt.getInt(10) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(10);
                ((int[]) buf[17])[0] = rslt.getInt(11) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 10) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((bool[]) buf[5])[0] = rslt.getBool(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((bool[]) buf[7])[0] = rslt.getBool(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((bool[]) buf[9])[0] = rslt.getBool(6) ;
                ((String[]) buf[10])[0] = rslt.getString(7, 4) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((String[]) buf[12])[0] = rslt.getString(8, 50) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((String[]) buf[14])[0] = rslt.getString(9, 50) ;
                ((int[]) buf[15])[0] = rslt.getInt(10) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(10);
                ((int[]) buf[17])[0] = rslt.getInt(11) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.getBool(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((bool[]) buf[5])[0] = rslt.getBool(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((bool[]) buf[7])[0] = rslt.getBool(5) ;
                ((String[]) buf[8])[0] = rslt.getString(6, 10) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((String[]) buf[10])[0] = rslt.getString(7, 4) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((String[]) buf[12])[0] = rslt.getString(8, 50) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((String[]) buf[14])[0] = rslt.getString(9, 50) ;
                ((int[]) buf[15])[0] = rslt.getInt(10) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(10);
                ((int[]) buf[17])[0] = rslt.getInt(11) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[15]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[15]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                return;
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[15]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getwwatributosfilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getwwatributosfilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getwwatributosfilterdata") )
          {
             return  ;
          }
          getwwatributosfilterdata worker = new getwwatributosfilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
