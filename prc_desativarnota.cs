/*
               File: PRC_DesativarNota
        Description: Desativar Nota
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:13:56.53
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_desativarnota : GXProcedure
   {
      public prc_desativarnota( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_desativarnota( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_ContagemResultadoNota_Codigo )
      {
         this.A1331ContagemResultadoNota_Codigo = aP0_ContagemResultadoNota_Codigo;
         initialize();
         executePrivate();
         aP0_ContagemResultadoNota_Codigo=this.A1331ContagemResultadoNota_Codigo;
      }

      public int executeUdp( )
      {
         this.A1331ContagemResultadoNota_Codigo = aP0_ContagemResultadoNota_Codigo;
         initialize();
         executePrivate();
         aP0_ContagemResultadoNota_Codigo=this.A1331ContagemResultadoNota_Codigo;
         return A1331ContagemResultadoNota_Codigo ;
      }

      public void executeSubmit( ref int aP0_ContagemResultadoNota_Codigo )
      {
         prc_desativarnota objprc_desativarnota;
         objprc_desativarnota = new prc_desativarnota();
         objprc_desativarnota.A1331ContagemResultadoNota_Codigo = aP0_ContagemResultadoNota_Codigo;
         objprc_desativarnota.context.SetSubmitInitialConfig(context);
         objprc_desativarnota.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_desativarnota);
         aP0_ContagemResultadoNota_Codigo=this.A1331ContagemResultadoNota_Codigo;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_desativarnota)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00TF2 */
         pr_default.execute(0, new Object[] {A1331ContagemResultadoNota_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1857ContagemResultadoNotas_Ativo = P00TF2_A1857ContagemResultadoNotas_Ativo[0];
            n1857ContagemResultadoNotas_Ativo = P00TF2_n1857ContagemResultadoNotas_Ativo[0];
            A1327ContagemResultadoNota_DemandaCod = P00TF2_A1327ContagemResultadoNota_DemandaCod[0];
            A1328ContagemResultadoNota_UsuarioCod = P00TF2_A1328ContagemResultadoNota_UsuarioCod[0];
            A1332ContagemResultadoNota_DataHora = P00TF2_A1332ContagemResultadoNota_DataHora[0];
            A484ContagemResultado_StatusDmn = P00TF2_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P00TF2_n484ContagemResultado_StatusDmn[0];
            A472ContagemResultado_DataEntrega = P00TF2_A472ContagemResultado_DataEntrega[0];
            n472ContagemResultado_DataEntrega = P00TF2_n472ContagemResultado_DataEntrega[0];
            A912ContagemResultado_HoraEntrega = P00TF2_A912ContagemResultado_HoraEntrega[0];
            n912ContagemResultado_HoraEntrega = P00TF2_n912ContagemResultado_HoraEntrega[0];
            A484ContagemResultado_StatusDmn = P00TF2_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P00TF2_n484ContagemResultado_StatusDmn[0];
            A472ContagemResultado_DataEntrega = P00TF2_A472ContagemResultado_DataEntrega[0];
            n472ContagemResultado_DataEntrega = P00TF2_n472ContagemResultado_DataEntrega[0];
            A912ContagemResultado_HoraEntrega = P00TF2_A912ContagemResultado_HoraEntrega[0];
            n912ContagemResultado_HoraEntrega = P00TF2_n912ContagemResultado_HoraEntrega[0];
            A1857ContagemResultadoNotas_Ativo = false;
            n1857ContagemResultadoNotas_Ativo = false;
            AV12Codigo = A1327ContagemResultadoNota_DemandaCod;
            AV9UserId = A1328ContagemResultadoNota_UsuarioCod;
            AV13Observacao = "Nota do " + context.localUtil.TToC( A1332ContagemResultadoNota_DataHora, 8, 5, 0, 3, "/", ":", " ") + " eliminada.";
            AV10Status = A484ContagemResultado_StatusDmn;
            AV11Prazo = DateTimeUtil.ResetTime( A472ContagemResultado_DataEntrega ) ;
            AV11Prazo = DateTimeUtil.TAdd( AV11Prazo, 3600*(DateTimeUtil.Hour( A912ContagemResultado_HoraEntrega)));
            AV11Prazo = DateTimeUtil.TAdd( AV11Prazo, 60*(DateTimeUtil.Minute( A912ContagemResultado_HoraEntrega)));
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            /* Using cursor P00TF3 */
            pr_default.execute(1, new Object[] {n1857ContagemResultadoNotas_Ativo, A1857ContagemResultadoNotas_Ativo, A1331ContagemResultadoNota_Codigo});
            pr_default.close(1);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoNotas") ;
            if (true) break;
            /* Using cursor P00TF4 */
            pr_default.execute(2, new Object[] {n1857ContagemResultadoNotas_Ativo, A1857ContagemResultadoNotas_Ativo, A1331ContagemResultadoNota_Codigo});
            pr_default.close(2);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoNotas") ;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         new prc_inslogresponsavel(context ).execute( ref  AV12Codigo,  0,  "NO",  "D",  (int)(AV9UserId),  0,  AV10Status,  AV10Status,  AV13Observacao,  AV11Prazo,  true) ;
         this.cleanup();
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "PRC_DesativarNota");
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00TF2_A1331ContagemResultadoNota_Codigo = new int[1] ;
         P00TF2_A1857ContagemResultadoNotas_Ativo = new bool[] {false} ;
         P00TF2_n1857ContagemResultadoNotas_Ativo = new bool[] {false} ;
         P00TF2_A1327ContagemResultadoNota_DemandaCod = new int[1] ;
         P00TF2_A1328ContagemResultadoNota_UsuarioCod = new int[1] ;
         P00TF2_A1332ContagemResultadoNota_DataHora = new DateTime[] {DateTime.MinValue} ;
         P00TF2_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P00TF2_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P00TF2_A472ContagemResultado_DataEntrega = new DateTime[] {DateTime.MinValue} ;
         P00TF2_n472ContagemResultado_DataEntrega = new bool[] {false} ;
         P00TF2_A912ContagemResultado_HoraEntrega = new DateTime[] {DateTime.MinValue} ;
         P00TF2_n912ContagemResultado_HoraEntrega = new bool[] {false} ;
         A1332ContagemResultadoNota_DataHora = (DateTime)(DateTime.MinValue);
         A484ContagemResultado_StatusDmn = "";
         A472ContagemResultado_DataEntrega = DateTime.MinValue;
         A912ContagemResultado_HoraEntrega = (DateTime)(DateTime.MinValue);
         AV13Observacao = "";
         AV10Status = "";
         AV11Prazo = (DateTime)(DateTime.MinValue);
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_desativarnota__default(),
            new Object[][] {
                new Object[] {
               P00TF2_A1331ContagemResultadoNota_Codigo, P00TF2_A1857ContagemResultadoNotas_Ativo, P00TF2_n1857ContagemResultadoNotas_Ativo, P00TF2_A1327ContagemResultadoNota_DemandaCod, P00TF2_A1328ContagemResultadoNota_UsuarioCod, P00TF2_A1332ContagemResultadoNota_DataHora, P00TF2_A484ContagemResultado_StatusDmn, P00TF2_n484ContagemResultado_StatusDmn, P00TF2_A472ContagemResultado_DataEntrega, P00TF2_n472ContagemResultado_DataEntrega,
               P00TF2_A912ContagemResultado_HoraEntrega, P00TF2_n912ContagemResultado_HoraEntrega
               }
               , new Object[] {
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A1331ContagemResultadoNota_Codigo ;
      private int A1327ContagemResultadoNota_DemandaCod ;
      private int A1328ContagemResultadoNota_UsuarioCod ;
      private int AV12Codigo ;
      private long AV9UserId ;
      private String scmdbuf ;
      private String A484ContagemResultado_StatusDmn ;
      private String AV10Status ;
      private DateTime A1332ContagemResultadoNota_DataHora ;
      private DateTime A912ContagemResultado_HoraEntrega ;
      private DateTime AV11Prazo ;
      private DateTime A472ContagemResultado_DataEntrega ;
      private bool A1857ContagemResultadoNotas_Ativo ;
      private bool n1857ContagemResultadoNotas_Ativo ;
      private bool n484ContagemResultado_StatusDmn ;
      private bool n472ContagemResultado_DataEntrega ;
      private bool n912ContagemResultado_HoraEntrega ;
      private String AV13Observacao ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_ContagemResultadoNota_Codigo ;
      private IDataStoreProvider pr_default ;
      private int[] P00TF2_A1331ContagemResultadoNota_Codigo ;
      private bool[] P00TF2_A1857ContagemResultadoNotas_Ativo ;
      private bool[] P00TF2_n1857ContagemResultadoNotas_Ativo ;
      private int[] P00TF2_A1327ContagemResultadoNota_DemandaCod ;
      private int[] P00TF2_A1328ContagemResultadoNota_UsuarioCod ;
      private DateTime[] P00TF2_A1332ContagemResultadoNota_DataHora ;
      private String[] P00TF2_A484ContagemResultado_StatusDmn ;
      private bool[] P00TF2_n484ContagemResultado_StatusDmn ;
      private DateTime[] P00TF2_A472ContagemResultado_DataEntrega ;
      private bool[] P00TF2_n472ContagemResultado_DataEntrega ;
      private DateTime[] P00TF2_A912ContagemResultado_HoraEntrega ;
      private bool[] P00TF2_n912ContagemResultado_HoraEntrega ;
   }

   public class prc_desativarnota__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new UpdateCursor(def[1])
         ,new UpdateCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00TF2 ;
          prmP00TF2 = new Object[] {
          new Object[] {"@ContagemResultadoNota_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00TF3 ;
          prmP00TF3 = new Object[] {
          new Object[] {"@ContagemResultadoNotas_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContagemResultadoNota_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00TF4 ;
          prmP00TF4 = new Object[] {
          new Object[] {"@ContagemResultadoNotas_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContagemResultadoNota_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00TF2", "SELECT TOP 1 T1.[ContagemResultadoNota_Codigo], T1.[ContagemResultadoNotas_Ativo], T1.[ContagemResultadoNota_DemandaCod] AS ContagemResultadoNota_DemandaCod, T1.[ContagemResultadoNota_UsuarioCod], T1.[ContagemResultadoNota_DataHora], T2.[ContagemResultado_StatusDmn], T2.[ContagemResultado_DataEntrega], T2.[ContagemResultado_HoraEntrega] FROM ([ContagemResultadoNotas] T1 WITH (UPDLOCK) INNER JOIN [ContagemResultado] T2 WITH (NOLOCK) ON T2.[ContagemResultado_Codigo] = T1.[ContagemResultadoNota_DemandaCod]) WHERE T1.[ContagemResultadoNota_Codigo] = @ContagemResultadoNota_Codigo ORDER BY T1.[ContagemResultadoNota_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00TF2,1,0,true,true )
             ,new CursorDef("P00TF3", "UPDATE [ContagemResultadoNotas] SET [ContagemResultadoNotas_Ativo]=@ContagemResultadoNotas_Ativo  WHERE [ContagemResultadoNota_Codigo] = @ContagemResultadoNota_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00TF3)
             ,new CursorDef("P00TF4", "UPDATE [ContagemResultadoNotas] SET [ContagemResultadoNotas_Ativo]=@ContagemResultadoNotas_Ativo  WHERE [ContagemResultadoNota_Codigo] = @ContagemResultadoNota_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00TF4)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((DateTime[]) buf[5])[0] = rslt.getGXDateTime(5) ;
                ((String[]) buf[6])[0] = rslt.getString(6, 1) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(6);
                ((DateTime[]) buf[8])[0] = rslt.getGXDate(7) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(7);
                ((DateTime[]) buf[10])[0] = rslt.getGXDateTime(8) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(8);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(1, (bool)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(1, (bool)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
       }
    }

 }

}
