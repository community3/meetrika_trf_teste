/*
               File: PRC_NewPerfilGestor
        Description: New Perfil Gestor
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:55:56.32
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_newperfilgestor : GXProcedure
   {
      public prc_newperfilgestor( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_newperfilgestor( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_Perfil_AreaTrabalhoCod )
      {
         this.A7Perfil_AreaTrabalhoCod = aP0_Perfil_AreaTrabalhoCod;
         initialize();
         executePrivate();
         aP0_Perfil_AreaTrabalhoCod=this.A7Perfil_AreaTrabalhoCod;
      }

      public int executeUdp( )
      {
         this.A7Perfil_AreaTrabalhoCod = aP0_Perfil_AreaTrabalhoCod;
         initialize();
         executePrivate();
         aP0_Perfil_AreaTrabalhoCod=this.A7Perfil_AreaTrabalhoCod;
         return A7Perfil_AreaTrabalhoCod ;
      }

      public void executeSubmit( ref int aP0_Perfil_AreaTrabalhoCod )
      {
         prc_newperfilgestor objprc_newperfilgestor;
         objprc_newperfilgestor = new prc_newperfilgestor();
         objprc_newperfilgestor.A7Perfil_AreaTrabalhoCod = aP0_Perfil_AreaTrabalhoCod;
         objprc_newperfilgestor.context.SetSubmitInitialConfig(context);
         objprc_newperfilgestor.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_newperfilgestor);
         aP0_Perfil_AreaTrabalhoCod=this.A7Perfil_AreaTrabalhoCod;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_newperfilgestor)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV11Role.load( 1);
         /*
            INSERT RECORD ON TABLE Perfil

         */
         A4Perfil_Nome = "GESTOR";
         A275Perfil_Tipo = 0;
         A328Perfil_GamGUID = AV11Role.gxTpr_Guid;
         A329Perfil_GamId = AV11Role.gxTpr_Id;
         A276Perfil_Ativo = true;
         /* Using cursor P00VN2 */
         pr_default.execute(0, new Object[] {A4Perfil_Nome, A7Perfil_AreaTrabalhoCod, A275Perfil_Tipo, A276Perfil_Ativo, A328Perfil_GamGUID, A329Perfil_GamId});
         A3Perfil_Codigo = P00VN2_A3Perfil_Codigo[0];
         pr_default.close(0);
         dsDefault.SmartCacheProvider.SetUpdated("Perfil") ;
         if ( (pr_default.getStatus(0) == 1) )
         {
            context.Gx_err = 1;
            Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
         }
         else
         {
            context.Gx_err = 0;
            Gx_emsg = "";
         }
         /* End Insert */
         AV8Perfil_Codigo = A3Perfil_Codigo;
         /* Using cursor P00VN3 */
         pr_default.execute(1);
         while ( (pr_default.getStatus(1) != 101) )
         {
            A284Menu_Ativo = P00VN3_A284Menu_Ativo[0];
            A277Menu_Codigo = P00VN3_A277Menu_Codigo[0];
            AV10Menu_Codigo = A277Menu_Codigo;
            /*
               INSERT RECORD ON TABLE MenuPerfil

            */
            W277Menu_Codigo = A277Menu_Codigo;
            A277Menu_Codigo = AV10Menu_Codigo;
            A3Perfil_Codigo = AV8Perfil_Codigo;
            BatchSize = 50;
            pr_default.initializeBatch( 2, BatchSize, this, "Executebatchp00vn4");
            /* Using cursor P00VN4 */
            pr_default.addRecord(2, new Object[] {A277Menu_Codigo, A3Perfil_Codigo});
            if ( pr_default.recordCount(2) == pr_default.getBatchSize(2) )
            {
               Executebatchp00vn4( ) ;
            }
            dsDefault.SmartCacheProvider.SetUpdated("MenuPerfil") ;
            pr_default.readNext(1);
         }
         pr_default.close(1);
         this.cleanup();
      }

      protected void Executebatchp00vn4( )
      {
         pr_default .setErrorBuffers( 2 ,
         new Object[] {
         P00VN4_A277Menu_Codigo, P00VN4_A3Perfil_Codigo
         });
         /* Using cursor P00VN4 */
         pr_default.executeBatch(2);
         if ( (pr_default.getStatus(2) == 1) )
         {
            while ( pr_default.readNextErrorRecord(2) == 1 )
            {
               A277Menu_Codigo = P00VN4_A277Menu_Codigo[0];
               A3Perfil_Codigo = P00VN4_A3Perfil_Codigo[0];
               /* Using cursor P00VN5 */
               pr_default.execute(3, new Object[] {A277Menu_Codigo, A3Perfil_Codigo});
               pr_default.close(3);
               dsDefault.SmartCacheProvider.SetUpdated("MenuPerfil") ;
               if ( (pr_default.getStatus(3) == 1) )
               {
                  context.Gx_err = 1;
                  Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
               }
               else
               {
                  context.Gx_err = 0;
                  Gx_emsg = "";
               }
               A277Menu_Codigo = W277Menu_Codigo;
               /* End Insert */
            }
         }
         pr_default.close(2);
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "PRC_NewPerfilGestor");
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(2);
      }

      public override void initialize( )
      {
         AV11Role = new SdtGAMRole(context);
         A4Perfil_Nome = "";
         A328Perfil_GamGUID = "";
         P00VN2_A3Perfil_Codigo = new int[1] ;
         Gx_emsg = "";
         scmdbuf = "";
         P00VN3_A284Menu_Ativo = new bool[] {false} ;
         P00VN3_A277Menu_Codigo = new int[1] ;
         P00VN4_A277Menu_Codigo = new int[1] ;
         P00VN4_A3Perfil_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_newperfilgestor__default(),
            new Object[][] {
                new Object[] {
               P00VN2_A3Perfil_Codigo
               }
               , new Object[] {
               P00VN3_A284Menu_Ativo, P00VN3_A277Menu_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short A275Perfil_Tipo ;
      private int A7Perfil_AreaTrabalhoCod ;
      private int GX_INS2 ;
      private int A3Perfil_Codigo ;
      private int AV8Perfil_Codigo ;
      private int A277Menu_Codigo ;
      private int AV10Menu_Codigo ;
      private int GX_INS49 ;
      private int W277Menu_Codigo ;
      private int BatchSize ;
      private long A329Perfil_GamId ;
      private String A4Perfil_Nome ;
      private String A328Perfil_GamGUID ;
      private String Gx_emsg ;
      private String scmdbuf ;
      private bool A276Perfil_Ativo ;
      private bool A284Menu_Ativo ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_Perfil_AreaTrabalhoCod ;
      private IDataStoreProvider pr_default ;
      private int[] P00VN2_A3Perfil_Codigo ;
      private bool[] P00VN3_A284Menu_Ativo ;
      private int[] P00VN3_A277Menu_Codigo ;
      private int[] P00VN4_A277Menu_Codigo ;
      private int[] P00VN4_A3Perfil_Codigo ;
      private SdtGAMRole AV11Role ;
   }

   public class prc_newperfilgestor__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new BatchUpdateCursor(def[2])
         ,new UpdateCursor(def[3])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00VN2 ;
          prmP00VN2 = new Object[] {
          new Object[] {"@Perfil_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@Perfil_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Perfil_Tipo",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Perfil_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@Perfil_GamGUID",SqlDbType.Char,40,0} ,
          new Object[] {"@Perfil_GamId",SqlDbType.Decimal,12,0}
          } ;
          Object[] prmP00VN3 ;
          prmP00VN3 = new Object[] {
          } ;
          Object[] prmP00VN4 ;
          prmP00VN4 = new Object[] {
          new Object[] {"@Menu_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Perfil_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00VN5 ;
          prmP00VN5 = new Object[] {
          new Object[] {"@Menu_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Perfil_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00VN2", "INSERT INTO [Perfil]([Perfil_Nome], [Perfil_AreaTrabalhoCod], [Perfil_Tipo], [Perfil_Ativo], [Perfil_GamGUID], [Perfil_GamId]) VALUES(@Perfil_Nome, @Perfil_AreaTrabalhoCod, @Perfil_Tipo, @Perfil_Ativo, @Perfil_GamGUID, @Perfil_GamId); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmP00VN2)
             ,new CursorDef("P00VN3", "SELECT [Menu_Ativo], [Menu_Codigo] FROM [Menu] WITH (NOLOCK) WHERE [Menu_Ativo] = 1 ORDER BY [Menu_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00VN3,100,0,true,false )
             ,new CursorDef("P00VN4", "INSERT INTO [MenuPerfil]([Menu_Codigo], [Perfil_Codigo]) VALUES(@Menu_Codigo, @Perfil_Codigo)", GxErrorMask.GX_NOMASK,prmP00VN4)
             ,new CursorDef("P00VN5", "INSERT INTO [MenuPerfil]([Menu_Codigo], [Perfil_Codigo]) VALUES(@Menu_Codigo, @Perfil_Codigo)", GxErrorMask.GX_NOMASK,prmP00VN5)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 1 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
       }
    }

    public override void getErrorResults( int cursor ,
                                          IFieldGetter rslt ,
                                          Object[] buf )
    {
       switch ( cursor )
       {
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                break;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (short)parms[2]);
                stmt.SetParameter(4, (bool)parms[3]);
                stmt.SetParameter(5, (String)parms[4]);
                stmt.SetParameter(6, (long)parms[5]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
       }
    }

 }

}
