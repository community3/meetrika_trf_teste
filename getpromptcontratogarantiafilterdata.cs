/*
               File: GetPromptContratoGarantiaFilterData
        Description: Get Prompt Contrato Garantia Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:55:13.47
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getpromptcontratogarantiafilterdata : GXProcedure
   {
      public getpromptcontratogarantiafilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getpromptcontratogarantiafilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV38DDOName = aP0_DDOName;
         this.AV36SearchTxt = aP1_SearchTxt;
         this.AV37SearchTxtTo = aP2_SearchTxtTo;
         this.AV42OptionsJson = "" ;
         this.AV45OptionsDescJson = "" ;
         this.AV47OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV42OptionsJson;
         aP4_OptionsDescJson=this.AV45OptionsDescJson;
         aP5_OptionIndexesJson=this.AV47OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV38DDOName = aP0_DDOName;
         this.AV36SearchTxt = aP1_SearchTxt;
         this.AV37SearchTxtTo = aP2_SearchTxtTo;
         this.AV42OptionsJson = "" ;
         this.AV45OptionsDescJson = "" ;
         this.AV47OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV42OptionsJson;
         aP4_OptionsDescJson=this.AV45OptionsDescJson;
         aP5_OptionIndexesJson=this.AV47OptionIndexesJson;
         return AV47OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getpromptcontratogarantiafilterdata objgetpromptcontratogarantiafilterdata;
         objgetpromptcontratogarantiafilterdata = new getpromptcontratogarantiafilterdata();
         objgetpromptcontratogarantiafilterdata.AV38DDOName = aP0_DDOName;
         objgetpromptcontratogarantiafilterdata.AV36SearchTxt = aP1_SearchTxt;
         objgetpromptcontratogarantiafilterdata.AV37SearchTxtTo = aP2_SearchTxtTo;
         objgetpromptcontratogarantiafilterdata.AV42OptionsJson = "" ;
         objgetpromptcontratogarantiafilterdata.AV45OptionsDescJson = "" ;
         objgetpromptcontratogarantiafilterdata.AV47OptionIndexesJson = "" ;
         objgetpromptcontratogarantiafilterdata.context.SetSubmitInitialConfig(context);
         objgetpromptcontratogarantiafilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetpromptcontratogarantiafilterdata);
         aP3_OptionsJson=this.AV42OptionsJson;
         aP4_OptionsDescJson=this.AV45OptionsDescJson;
         aP5_OptionIndexesJson=this.AV47OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getpromptcontratogarantiafilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV41Options = (IGxCollection)(new GxSimpleCollection());
         AV44OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV46OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV38DDOName), "DDO_CONTRATO_NUMERO") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATO_NUMEROOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV38DDOName), "DDO_CONTRATADA_PESSOANOM") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATADA_PESSOANOMOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV38DDOName), "DDO_CONTRATADA_PESSOACNPJ") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATADA_PESSOACNPJOPTIONS' */
            S141 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV42OptionsJson = AV41Options.ToJSonString(false);
         AV45OptionsDescJson = AV44OptionsDesc.ToJSonString(false);
         AV47OptionIndexesJson = AV46OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV49Session.Get("PromptContratoGarantiaGridState"), "") == 0 )
         {
            AV51GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "PromptContratoGarantiaGridState"), "");
         }
         else
         {
            AV51GridState.FromXml(AV49Session.Get("PromptContratoGarantiaGridState"), "");
         }
         AV67GXV1 = 1;
         while ( AV67GXV1 <= AV51GridState.gxTpr_Filtervalues.Count )
         {
            AV52GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV51GridState.gxTpr_Filtervalues.Item(AV67GXV1));
            if ( StringUtil.StrCmp(AV52GridStateFilterValue.gxTpr_Name, "TFCONTRATOGARANTIA_CODIGO") == 0 )
            {
               AV10TFContratoGarantia_Codigo = (int)(NumberUtil.Val( AV52GridStateFilterValue.gxTpr_Value, "."));
               AV11TFContratoGarantia_Codigo_To = (int)(NumberUtil.Val( AV52GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV52GridStateFilterValue.gxTpr_Name, "TFCONTRATO_CODIGO") == 0 )
            {
               AV12TFContrato_Codigo = (int)(NumberUtil.Val( AV52GridStateFilterValue.gxTpr_Value, "."));
               AV13TFContrato_Codigo_To = (int)(NumberUtil.Val( AV52GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV52GridStateFilterValue.gxTpr_Name, "TFCONTRATO_NUMERO") == 0 )
            {
               AV14TFContrato_Numero = AV52GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV52GridStateFilterValue.gxTpr_Name, "TFCONTRATO_NUMERO_SEL") == 0 )
            {
               AV15TFContrato_Numero_Sel = AV52GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV52GridStateFilterValue.gxTpr_Name, "TFCONTRATADA_CODIGO") == 0 )
            {
               AV16TFContratada_Codigo = (int)(NumberUtil.Val( AV52GridStateFilterValue.gxTpr_Value, "."));
               AV17TFContratada_Codigo_To = (int)(NumberUtil.Val( AV52GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV52GridStateFilterValue.gxTpr_Name, "TFCONTRATADA_PESSOACOD") == 0 )
            {
               AV18TFContratada_PessoaCod = (int)(NumberUtil.Val( AV52GridStateFilterValue.gxTpr_Value, "."));
               AV19TFContratada_PessoaCod_To = (int)(NumberUtil.Val( AV52GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV52GridStateFilterValue.gxTpr_Name, "TFCONTRATADA_PESSOANOM") == 0 )
            {
               AV20TFContratada_PessoaNom = AV52GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV52GridStateFilterValue.gxTpr_Name, "TFCONTRATADA_PESSOANOM_SEL") == 0 )
            {
               AV21TFContratada_PessoaNom_Sel = AV52GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV52GridStateFilterValue.gxTpr_Name, "TFCONTRATADA_PESSOACNPJ") == 0 )
            {
               AV22TFContratada_PessoaCNPJ = AV52GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV52GridStateFilterValue.gxTpr_Name, "TFCONTRATADA_PESSOACNPJ_SEL") == 0 )
            {
               AV23TFContratada_PessoaCNPJ_Sel = AV52GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV52GridStateFilterValue.gxTpr_Name, "TFCONTRATOGARANTIA_DATAPAGTOGARANTIA") == 0 )
            {
               AV24TFContratoGarantia_DataPagtoGarantia = context.localUtil.CToD( AV52GridStateFilterValue.gxTpr_Value, 2);
               AV25TFContratoGarantia_DataPagtoGarantia_To = context.localUtil.CToD( AV52GridStateFilterValue.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV52GridStateFilterValue.gxTpr_Name, "TFCONTRATOGARANTIA_PERCENTUAL") == 0 )
            {
               AV26TFContratoGarantia_Percentual = NumberUtil.Val( AV52GridStateFilterValue.gxTpr_Value, ".");
               AV27TFContratoGarantia_Percentual_To = NumberUtil.Val( AV52GridStateFilterValue.gxTpr_Valueto, ".");
            }
            else if ( StringUtil.StrCmp(AV52GridStateFilterValue.gxTpr_Name, "TFCONTRATOGARANTIA_DATACAUCAO") == 0 )
            {
               AV28TFContratoGarantia_DataCaucao = context.localUtil.CToD( AV52GridStateFilterValue.gxTpr_Value, 2);
               AV29TFContratoGarantia_DataCaucao_To = context.localUtil.CToD( AV52GridStateFilterValue.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV52GridStateFilterValue.gxTpr_Name, "TFCONTRATOGARANTIA_VALORGARANTIA") == 0 )
            {
               AV30TFContratoGarantia_ValorGarantia = NumberUtil.Val( AV52GridStateFilterValue.gxTpr_Value, ".");
               AV31TFContratoGarantia_ValorGarantia_To = NumberUtil.Val( AV52GridStateFilterValue.gxTpr_Valueto, ".");
            }
            else if ( StringUtil.StrCmp(AV52GridStateFilterValue.gxTpr_Name, "TFCONTRATOGARANTIA_DATAENCERRAMENTO") == 0 )
            {
               AV32TFContratoGarantia_DataEncerramento = context.localUtil.CToD( AV52GridStateFilterValue.gxTpr_Value, 2);
               AV33TFContratoGarantia_DataEncerramento_To = context.localUtil.CToD( AV52GridStateFilterValue.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV52GridStateFilterValue.gxTpr_Name, "TFCONTRATOGARANTIA_VALORENCERRAMENTO") == 0 )
            {
               AV34TFContratoGarantia_ValorEncerramento = NumberUtil.Val( AV52GridStateFilterValue.gxTpr_Value, ".");
               AV35TFContratoGarantia_ValorEncerramento_To = NumberUtil.Val( AV52GridStateFilterValue.gxTpr_Valueto, ".");
            }
            AV67GXV1 = (int)(AV67GXV1+1);
         }
         if ( AV51GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV53GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV51GridState.gxTpr_Dynamicfilters.Item(1));
            AV54DynamicFiltersSelector1 = AV53GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV54DynamicFiltersSelector1, "CONTRATOGARANTIA_DATAPAGTOGARANTIA") == 0 )
            {
               AV55ContratoGarantia_DataPagtoGarantia1 = context.localUtil.CToD( AV53GridStateDynamicFilter.gxTpr_Value, 2);
               AV56ContratoGarantia_DataPagtoGarantia_To1 = context.localUtil.CToD( AV53GridStateDynamicFilter.gxTpr_Valueto, 2);
            }
            if ( AV51GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV57DynamicFiltersEnabled2 = true;
               AV53GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV51GridState.gxTpr_Dynamicfilters.Item(2));
               AV58DynamicFiltersSelector2 = AV53GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV58DynamicFiltersSelector2, "CONTRATOGARANTIA_DATAPAGTOGARANTIA") == 0 )
               {
                  AV59ContratoGarantia_DataPagtoGarantia2 = context.localUtil.CToD( AV53GridStateDynamicFilter.gxTpr_Value, 2);
                  AV60ContratoGarantia_DataPagtoGarantia_To2 = context.localUtil.CToD( AV53GridStateDynamicFilter.gxTpr_Valueto, 2);
               }
               if ( AV51GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV61DynamicFiltersEnabled3 = true;
                  AV53GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV51GridState.gxTpr_Dynamicfilters.Item(3));
                  AV62DynamicFiltersSelector3 = AV53GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV62DynamicFiltersSelector3, "CONTRATOGARANTIA_DATAPAGTOGARANTIA") == 0 )
                  {
                     AV63ContratoGarantia_DataPagtoGarantia3 = context.localUtil.CToD( AV53GridStateDynamicFilter.gxTpr_Value, 2);
                     AV64ContratoGarantia_DataPagtoGarantia_To3 = context.localUtil.CToD( AV53GridStateDynamicFilter.gxTpr_Valueto, 2);
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADCONTRATO_NUMEROOPTIONS' Routine */
         AV14TFContrato_Numero = AV36SearchTxt;
         AV15TFContrato_Numero_Sel = "";
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV54DynamicFiltersSelector1 ,
                                              AV55ContratoGarantia_DataPagtoGarantia1 ,
                                              AV56ContratoGarantia_DataPagtoGarantia_To1 ,
                                              AV57DynamicFiltersEnabled2 ,
                                              AV58DynamicFiltersSelector2 ,
                                              AV59ContratoGarantia_DataPagtoGarantia2 ,
                                              AV60ContratoGarantia_DataPagtoGarantia_To2 ,
                                              AV61DynamicFiltersEnabled3 ,
                                              AV62DynamicFiltersSelector3 ,
                                              AV63ContratoGarantia_DataPagtoGarantia3 ,
                                              AV64ContratoGarantia_DataPagtoGarantia_To3 ,
                                              AV10TFContratoGarantia_Codigo ,
                                              AV11TFContratoGarantia_Codigo_To ,
                                              AV12TFContrato_Codigo ,
                                              AV13TFContrato_Codigo_To ,
                                              AV15TFContrato_Numero_Sel ,
                                              AV14TFContrato_Numero ,
                                              AV16TFContratada_Codigo ,
                                              AV17TFContratada_Codigo_To ,
                                              AV18TFContratada_PessoaCod ,
                                              AV19TFContratada_PessoaCod_To ,
                                              AV21TFContratada_PessoaNom_Sel ,
                                              AV20TFContratada_PessoaNom ,
                                              AV23TFContratada_PessoaCNPJ_Sel ,
                                              AV22TFContratada_PessoaCNPJ ,
                                              AV24TFContratoGarantia_DataPagtoGarantia ,
                                              AV25TFContratoGarantia_DataPagtoGarantia_To ,
                                              AV26TFContratoGarantia_Percentual ,
                                              AV27TFContratoGarantia_Percentual_To ,
                                              AV28TFContratoGarantia_DataCaucao ,
                                              AV29TFContratoGarantia_DataCaucao_To ,
                                              AV30TFContratoGarantia_ValorGarantia ,
                                              AV31TFContratoGarantia_ValorGarantia_To ,
                                              AV32TFContratoGarantia_DataEncerramento ,
                                              AV33TFContratoGarantia_DataEncerramento_To ,
                                              AV34TFContratoGarantia_ValorEncerramento ,
                                              AV35TFContratoGarantia_ValorEncerramento_To ,
                                              A102ContratoGarantia_DataPagtoGarantia ,
                                              A101ContratoGarantia_Codigo ,
                                              A74Contrato_Codigo ,
                                              A77Contrato_Numero ,
                                              A39Contratada_Codigo ,
                                              A40Contratada_PessoaCod ,
                                              A41Contratada_PessoaNom ,
                                              A42Contratada_PessoaCNPJ ,
                                              A103ContratoGarantia_Percentual ,
                                              A104ContratoGarantia_DataCaucao ,
                                              A105ContratoGarantia_ValorGarantia ,
                                              A106ContratoGarantia_DataEncerramento ,
                                              A107ContratoGarantia_ValorEncerramento },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.DATE, TypeConstants.DECIMAL,
                                              TypeConstants.DATE, TypeConstants.DECIMAL
                                              }
         });
         lV14TFContrato_Numero = StringUtil.PadR( StringUtil.RTrim( AV14TFContrato_Numero), 20, "%");
         lV20TFContratada_PessoaNom = StringUtil.PadR( StringUtil.RTrim( AV20TFContratada_PessoaNom), 100, "%");
         lV22TFContratada_PessoaCNPJ = StringUtil.Concat( StringUtil.RTrim( AV22TFContratada_PessoaCNPJ), "%", "");
         /* Using cursor P00UI2 */
         pr_default.execute(0, new Object[] {AV55ContratoGarantia_DataPagtoGarantia1, AV56ContratoGarantia_DataPagtoGarantia_To1, AV59ContratoGarantia_DataPagtoGarantia2, AV60ContratoGarantia_DataPagtoGarantia_To2, AV63ContratoGarantia_DataPagtoGarantia3, AV64ContratoGarantia_DataPagtoGarantia_To3, AV10TFContratoGarantia_Codigo, AV11TFContratoGarantia_Codigo_To, AV12TFContrato_Codigo, AV13TFContrato_Codigo_To, lV14TFContrato_Numero, AV15TFContrato_Numero_Sel, AV16TFContratada_Codigo, AV17TFContratada_Codigo_To, AV18TFContratada_PessoaCod, AV19TFContratada_PessoaCod_To, lV20TFContratada_PessoaNom, AV21TFContratada_PessoaNom_Sel, lV22TFContratada_PessoaCNPJ, AV23TFContratada_PessoaCNPJ_Sel, AV24TFContratoGarantia_DataPagtoGarantia, AV25TFContratoGarantia_DataPagtoGarantia_To, AV26TFContratoGarantia_Percentual, AV27TFContratoGarantia_Percentual_To, AV28TFContratoGarantia_DataCaucao, AV29TFContratoGarantia_DataCaucao_To, AV30TFContratoGarantia_ValorGarantia, AV31TFContratoGarantia_ValorGarantia_To, AV32TFContratoGarantia_DataEncerramento, AV33TFContratoGarantia_DataEncerramento_To, AV34TFContratoGarantia_ValorEncerramento, AV35TFContratoGarantia_ValorEncerramento_To});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKUI2 = false;
            A77Contrato_Numero = P00UI2_A77Contrato_Numero[0];
            A107ContratoGarantia_ValorEncerramento = P00UI2_A107ContratoGarantia_ValorEncerramento[0];
            A106ContratoGarantia_DataEncerramento = P00UI2_A106ContratoGarantia_DataEncerramento[0];
            A105ContratoGarantia_ValorGarantia = P00UI2_A105ContratoGarantia_ValorGarantia[0];
            A104ContratoGarantia_DataCaucao = P00UI2_A104ContratoGarantia_DataCaucao[0];
            A103ContratoGarantia_Percentual = P00UI2_A103ContratoGarantia_Percentual[0];
            A42Contratada_PessoaCNPJ = P00UI2_A42Contratada_PessoaCNPJ[0];
            n42Contratada_PessoaCNPJ = P00UI2_n42Contratada_PessoaCNPJ[0];
            A41Contratada_PessoaNom = P00UI2_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P00UI2_n41Contratada_PessoaNom[0];
            A40Contratada_PessoaCod = P00UI2_A40Contratada_PessoaCod[0];
            A39Contratada_Codigo = P00UI2_A39Contratada_Codigo[0];
            A74Contrato_Codigo = P00UI2_A74Contrato_Codigo[0];
            A101ContratoGarantia_Codigo = P00UI2_A101ContratoGarantia_Codigo[0];
            A102ContratoGarantia_DataPagtoGarantia = P00UI2_A102ContratoGarantia_DataPagtoGarantia[0];
            A77Contrato_Numero = P00UI2_A77Contrato_Numero[0];
            A39Contratada_Codigo = P00UI2_A39Contratada_Codigo[0];
            A40Contratada_PessoaCod = P00UI2_A40Contratada_PessoaCod[0];
            A42Contratada_PessoaCNPJ = P00UI2_A42Contratada_PessoaCNPJ[0];
            n42Contratada_PessoaCNPJ = P00UI2_n42Contratada_PessoaCNPJ[0];
            A41Contratada_PessoaNom = P00UI2_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P00UI2_n41Contratada_PessoaNom[0];
            AV48count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00UI2_A77Contrato_Numero[0], A77Contrato_Numero) == 0 ) )
            {
               BRKUI2 = false;
               A74Contrato_Codigo = P00UI2_A74Contrato_Codigo[0];
               A101ContratoGarantia_Codigo = P00UI2_A101ContratoGarantia_Codigo[0];
               AV48count = (long)(AV48count+1);
               BRKUI2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A77Contrato_Numero)) )
            {
               AV40Option = A77Contrato_Numero;
               AV41Options.Add(AV40Option, 0);
               AV46OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV48count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV41Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKUI2 )
            {
               BRKUI2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADCONTRATADA_PESSOANOMOPTIONS' Routine */
         AV20TFContratada_PessoaNom = AV36SearchTxt;
         AV21TFContratada_PessoaNom_Sel = "";
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV54DynamicFiltersSelector1 ,
                                              AV55ContratoGarantia_DataPagtoGarantia1 ,
                                              AV56ContratoGarantia_DataPagtoGarantia_To1 ,
                                              AV57DynamicFiltersEnabled2 ,
                                              AV58DynamicFiltersSelector2 ,
                                              AV59ContratoGarantia_DataPagtoGarantia2 ,
                                              AV60ContratoGarantia_DataPagtoGarantia_To2 ,
                                              AV61DynamicFiltersEnabled3 ,
                                              AV62DynamicFiltersSelector3 ,
                                              AV63ContratoGarantia_DataPagtoGarantia3 ,
                                              AV64ContratoGarantia_DataPagtoGarantia_To3 ,
                                              AV10TFContratoGarantia_Codigo ,
                                              AV11TFContratoGarantia_Codigo_To ,
                                              AV12TFContrato_Codigo ,
                                              AV13TFContrato_Codigo_To ,
                                              AV15TFContrato_Numero_Sel ,
                                              AV14TFContrato_Numero ,
                                              AV16TFContratada_Codigo ,
                                              AV17TFContratada_Codigo_To ,
                                              AV18TFContratada_PessoaCod ,
                                              AV19TFContratada_PessoaCod_To ,
                                              AV21TFContratada_PessoaNom_Sel ,
                                              AV20TFContratada_PessoaNom ,
                                              AV23TFContratada_PessoaCNPJ_Sel ,
                                              AV22TFContratada_PessoaCNPJ ,
                                              AV24TFContratoGarantia_DataPagtoGarantia ,
                                              AV25TFContratoGarantia_DataPagtoGarantia_To ,
                                              AV26TFContratoGarantia_Percentual ,
                                              AV27TFContratoGarantia_Percentual_To ,
                                              AV28TFContratoGarantia_DataCaucao ,
                                              AV29TFContratoGarantia_DataCaucao_To ,
                                              AV30TFContratoGarantia_ValorGarantia ,
                                              AV31TFContratoGarantia_ValorGarantia_To ,
                                              AV32TFContratoGarantia_DataEncerramento ,
                                              AV33TFContratoGarantia_DataEncerramento_To ,
                                              AV34TFContratoGarantia_ValorEncerramento ,
                                              AV35TFContratoGarantia_ValorEncerramento_To ,
                                              A102ContratoGarantia_DataPagtoGarantia ,
                                              A101ContratoGarantia_Codigo ,
                                              A74Contrato_Codigo ,
                                              A77Contrato_Numero ,
                                              A39Contratada_Codigo ,
                                              A40Contratada_PessoaCod ,
                                              A41Contratada_PessoaNom ,
                                              A42Contratada_PessoaCNPJ ,
                                              A103ContratoGarantia_Percentual ,
                                              A104ContratoGarantia_DataCaucao ,
                                              A105ContratoGarantia_ValorGarantia ,
                                              A106ContratoGarantia_DataEncerramento ,
                                              A107ContratoGarantia_ValorEncerramento },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.DATE, TypeConstants.DECIMAL,
                                              TypeConstants.DATE, TypeConstants.DECIMAL
                                              }
         });
         lV14TFContrato_Numero = StringUtil.PadR( StringUtil.RTrim( AV14TFContrato_Numero), 20, "%");
         lV20TFContratada_PessoaNom = StringUtil.PadR( StringUtil.RTrim( AV20TFContratada_PessoaNom), 100, "%");
         lV22TFContratada_PessoaCNPJ = StringUtil.Concat( StringUtil.RTrim( AV22TFContratada_PessoaCNPJ), "%", "");
         /* Using cursor P00UI3 */
         pr_default.execute(1, new Object[] {AV55ContratoGarantia_DataPagtoGarantia1, AV56ContratoGarantia_DataPagtoGarantia_To1, AV59ContratoGarantia_DataPagtoGarantia2, AV60ContratoGarantia_DataPagtoGarantia_To2, AV63ContratoGarantia_DataPagtoGarantia3, AV64ContratoGarantia_DataPagtoGarantia_To3, AV10TFContratoGarantia_Codigo, AV11TFContratoGarantia_Codigo_To, AV12TFContrato_Codigo, AV13TFContrato_Codigo_To, lV14TFContrato_Numero, AV15TFContrato_Numero_Sel, AV16TFContratada_Codigo, AV17TFContratada_Codigo_To, AV18TFContratada_PessoaCod, AV19TFContratada_PessoaCod_To, lV20TFContratada_PessoaNom, AV21TFContratada_PessoaNom_Sel, lV22TFContratada_PessoaCNPJ, AV23TFContratada_PessoaCNPJ_Sel, AV24TFContratoGarantia_DataPagtoGarantia, AV25TFContratoGarantia_DataPagtoGarantia_To, AV26TFContratoGarantia_Percentual, AV27TFContratoGarantia_Percentual_To, AV28TFContratoGarantia_DataCaucao, AV29TFContratoGarantia_DataCaucao_To, AV30TFContratoGarantia_ValorGarantia, AV31TFContratoGarantia_ValorGarantia_To, AV32TFContratoGarantia_DataEncerramento, AV33TFContratoGarantia_DataEncerramento_To, AV34TFContratoGarantia_ValorEncerramento, AV35TFContratoGarantia_ValorEncerramento_To});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKUI4 = false;
            A41Contratada_PessoaNom = P00UI3_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P00UI3_n41Contratada_PessoaNom[0];
            A107ContratoGarantia_ValorEncerramento = P00UI3_A107ContratoGarantia_ValorEncerramento[0];
            A106ContratoGarantia_DataEncerramento = P00UI3_A106ContratoGarantia_DataEncerramento[0];
            A105ContratoGarantia_ValorGarantia = P00UI3_A105ContratoGarantia_ValorGarantia[0];
            A104ContratoGarantia_DataCaucao = P00UI3_A104ContratoGarantia_DataCaucao[0];
            A103ContratoGarantia_Percentual = P00UI3_A103ContratoGarantia_Percentual[0];
            A42Contratada_PessoaCNPJ = P00UI3_A42Contratada_PessoaCNPJ[0];
            n42Contratada_PessoaCNPJ = P00UI3_n42Contratada_PessoaCNPJ[0];
            A40Contratada_PessoaCod = P00UI3_A40Contratada_PessoaCod[0];
            A39Contratada_Codigo = P00UI3_A39Contratada_Codigo[0];
            A77Contrato_Numero = P00UI3_A77Contrato_Numero[0];
            A74Contrato_Codigo = P00UI3_A74Contrato_Codigo[0];
            A101ContratoGarantia_Codigo = P00UI3_A101ContratoGarantia_Codigo[0];
            A102ContratoGarantia_DataPagtoGarantia = P00UI3_A102ContratoGarantia_DataPagtoGarantia[0];
            A39Contratada_Codigo = P00UI3_A39Contratada_Codigo[0];
            A77Contrato_Numero = P00UI3_A77Contrato_Numero[0];
            A40Contratada_PessoaCod = P00UI3_A40Contratada_PessoaCod[0];
            A41Contratada_PessoaNom = P00UI3_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P00UI3_n41Contratada_PessoaNom[0];
            A42Contratada_PessoaCNPJ = P00UI3_A42Contratada_PessoaCNPJ[0];
            n42Contratada_PessoaCNPJ = P00UI3_n42Contratada_PessoaCNPJ[0];
            AV48count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( StringUtil.StrCmp(P00UI3_A41Contratada_PessoaNom[0], A41Contratada_PessoaNom) == 0 ) )
            {
               BRKUI4 = false;
               A40Contratada_PessoaCod = P00UI3_A40Contratada_PessoaCod[0];
               A39Contratada_Codigo = P00UI3_A39Contratada_Codigo[0];
               A74Contrato_Codigo = P00UI3_A74Contrato_Codigo[0];
               A101ContratoGarantia_Codigo = P00UI3_A101ContratoGarantia_Codigo[0];
               A39Contratada_Codigo = P00UI3_A39Contratada_Codigo[0];
               A40Contratada_PessoaCod = P00UI3_A40Contratada_PessoaCod[0];
               AV48count = (long)(AV48count+1);
               BRKUI4 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A41Contratada_PessoaNom)) )
            {
               AV40Option = A41Contratada_PessoaNom;
               AV41Options.Add(AV40Option, 0);
               AV46OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV48count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV41Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKUI4 )
            {
               BRKUI4 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      protected void S141( )
      {
         /* 'LOADCONTRATADA_PESSOACNPJOPTIONS' Routine */
         AV22TFContratada_PessoaCNPJ = AV36SearchTxt;
         AV23TFContratada_PessoaCNPJ_Sel = "";
         pr_default.dynParam(2, new Object[]{ new Object[]{
                                              AV54DynamicFiltersSelector1 ,
                                              AV55ContratoGarantia_DataPagtoGarantia1 ,
                                              AV56ContratoGarantia_DataPagtoGarantia_To1 ,
                                              AV57DynamicFiltersEnabled2 ,
                                              AV58DynamicFiltersSelector2 ,
                                              AV59ContratoGarantia_DataPagtoGarantia2 ,
                                              AV60ContratoGarantia_DataPagtoGarantia_To2 ,
                                              AV61DynamicFiltersEnabled3 ,
                                              AV62DynamicFiltersSelector3 ,
                                              AV63ContratoGarantia_DataPagtoGarantia3 ,
                                              AV64ContratoGarantia_DataPagtoGarantia_To3 ,
                                              AV10TFContratoGarantia_Codigo ,
                                              AV11TFContratoGarantia_Codigo_To ,
                                              AV12TFContrato_Codigo ,
                                              AV13TFContrato_Codigo_To ,
                                              AV15TFContrato_Numero_Sel ,
                                              AV14TFContrato_Numero ,
                                              AV16TFContratada_Codigo ,
                                              AV17TFContratada_Codigo_To ,
                                              AV18TFContratada_PessoaCod ,
                                              AV19TFContratada_PessoaCod_To ,
                                              AV21TFContratada_PessoaNom_Sel ,
                                              AV20TFContratada_PessoaNom ,
                                              AV23TFContratada_PessoaCNPJ_Sel ,
                                              AV22TFContratada_PessoaCNPJ ,
                                              AV24TFContratoGarantia_DataPagtoGarantia ,
                                              AV25TFContratoGarantia_DataPagtoGarantia_To ,
                                              AV26TFContratoGarantia_Percentual ,
                                              AV27TFContratoGarantia_Percentual_To ,
                                              AV28TFContratoGarantia_DataCaucao ,
                                              AV29TFContratoGarantia_DataCaucao_To ,
                                              AV30TFContratoGarantia_ValorGarantia ,
                                              AV31TFContratoGarantia_ValorGarantia_To ,
                                              AV32TFContratoGarantia_DataEncerramento ,
                                              AV33TFContratoGarantia_DataEncerramento_To ,
                                              AV34TFContratoGarantia_ValorEncerramento ,
                                              AV35TFContratoGarantia_ValorEncerramento_To ,
                                              A102ContratoGarantia_DataPagtoGarantia ,
                                              A101ContratoGarantia_Codigo ,
                                              A74Contrato_Codigo ,
                                              A77Contrato_Numero ,
                                              A39Contratada_Codigo ,
                                              A40Contratada_PessoaCod ,
                                              A41Contratada_PessoaNom ,
                                              A42Contratada_PessoaCNPJ ,
                                              A103ContratoGarantia_Percentual ,
                                              A104ContratoGarantia_DataCaucao ,
                                              A105ContratoGarantia_ValorGarantia ,
                                              A106ContratoGarantia_DataEncerramento ,
                                              A107ContratoGarantia_ValorEncerramento },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.DATE, TypeConstants.DECIMAL,
                                              TypeConstants.DATE, TypeConstants.DECIMAL
                                              }
         });
         lV14TFContrato_Numero = StringUtil.PadR( StringUtil.RTrim( AV14TFContrato_Numero), 20, "%");
         lV20TFContratada_PessoaNom = StringUtil.PadR( StringUtil.RTrim( AV20TFContratada_PessoaNom), 100, "%");
         lV22TFContratada_PessoaCNPJ = StringUtil.Concat( StringUtil.RTrim( AV22TFContratada_PessoaCNPJ), "%", "");
         /* Using cursor P00UI4 */
         pr_default.execute(2, new Object[] {AV55ContratoGarantia_DataPagtoGarantia1, AV56ContratoGarantia_DataPagtoGarantia_To1, AV59ContratoGarantia_DataPagtoGarantia2, AV60ContratoGarantia_DataPagtoGarantia_To2, AV63ContratoGarantia_DataPagtoGarantia3, AV64ContratoGarantia_DataPagtoGarantia_To3, AV10TFContratoGarantia_Codigo, AV11TFContratoGarantia_Codigo_To, AV12TFContrato_Codigo, AV13TFContrato_Codigo_To, lV14TFContrato_Numero, AV15TFContrato_Numero_Sel, AV16TFContratada_Codigo, AV17TFContratada_Codigo_To, AV18TFContratada_PessoaCod, AV19TFContratada_PessoaCod_To, lV20TFContratada_PessoaNom, AV21TFContratada_PessoaNom_Sel, lV22TFContratada_PessoaCNPJ, AV23TFContratada_PessoaCNPJ_Sel, AV24TFContratoGarantia_DataPagtoGarantia, AV25TFContratoGarantia_DataPagtoGarantia_To, AV26TFContratoGarantia_Percentual, AV27TFContratoGarantia_Percentual_To, AV28TFContratoGarantia_DataCaucao, AV29TFContratoGarantia_DataCaucao_To, AV30TFContratoGarantia_ValorGarantia, AV31TFContratoGarantia_ValorGarantia_To, AV32TFContratoGarantia_DataEncerramento, AV33TFContratoGarantia_DataEncerramento_To, AV34TFContratoGarantia_ValorEncerramento, AV35TFContratoGarantia_ValorEncerramento_To});
         while ( (pr_default.getStatus(2) != 101) )
         {
            BRKUI6 = false;
            A42Contratada_PessoaCNPJ = P00UI4_A42Contratada_PessoaCNPJ[0];
            n42Contratada_PessoaCNPJ = P00UI4_n42Contratada_PessoaCNPJ[0];
            A107ContratoGarantia_ValorEncerramento = P00UI4_A107ContratoGarantia_ValorEncerramento[0];
            A106ContratoGarantia_DataEncerramento = P00UI4_A106ContratoGarantia_DataEncerramento[0];
            A105ContratoGarantia_ValorGarantia = P00UI4_A105ContratoGarantia_ValorGarantia[0];
            A104ContratoGarantia_DataCaucao = P00UI4_A104ContratoGarantia_DataCaucao[0];
            A103ContratoGarantia_Percentual = P00UI4_A103ContratoGarantia_Percentual[0];
            A41Contratada_PessoaNom = P00UI4_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P00UI4_n41Contratada_PessoaNom[0];
            A40Contratada_PessoaCod = P00UI4_A40Contratada_PessoaCod[0];
            A39Contratada_Codigo = P00UI4_A39Contratada_Codigo[0];
            A77Contrato_Numero = P00UI4_A77Contrato_Numero[0];
            A74Contrato_Codigo = P00UI4_A74Contrato_Codigo[0];
            A101ContratoGarantia_Codigo = P00UI4_A101ContratoGarantia_Codigo[0];
            A102ContratoGarantia_DataPagtoGarantia = P00UI4_A102ContratoGarantia_DataPagtoGarantia[0];
            A39Contratada_Codigo = P00UI4_A39Contratada_Codigo[0];
            A77Contrato_Numero = P00UI4_A77Contrato_Numero[0];
            A40Contratada_PessoaCod = P00UI4_A40Contratada_PessoaCod[0];
            A42Contratada_PessoaCNPJ = P00UI4_A42Contratada_PessoaCNPJ[0];
            n42Contratada_PessoaCNPJ = P00UI4_n42Contratada_PessoaCNPJ[0];
            A41Contratada_PessoaNom = P00UI4_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P00UI4_n41Contratada_PessoaNom[0];
            AV48count = 0;
            while ( (pr_default.getStatus(2) != 101) && ( StringUtil.StrCmp(P00UI4_A42Contratada_PessoaCNPJ[0], A42Contratada_PessoaCNPJ) == 0 ) )
            {
               BRKUI6 = false;
               A40Contratada_PessoaCod = P00UI4_A40Contratada_PessoaCod[0];
               A39Contratada_Codigo = P00UI4_A39Contratada_Codigo[0];
               A74Contrato_Codigo = P00UI4_A74Contrato_Codigo[0];
               A101ContratoGarantia_Codigo = P00UI4_A101ContratoGarantia_Codigo[0];
               A39Contratada_Codigo = P00UI4_A39Contratada_Codigo[0];
               A40Contratada_PessoaCod = P00UI4_A40Contratada_PessoaCod[0];
               AV48count = (long)(AV48count+1);
               BRKUI6 = true;
               pr_default.readNext(2);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A42Contratada_PessoaCNPJ)) )
            {
               AV40Option = A42Contratada_PessoaCNPJ;
               AV41Options.Add(AV40Option, 0);
               AV46OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV48count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV41Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKUI6 )
            {
               BRKUI6 = true;
               pr_default.readNext(2);
            }
         }
         pr_default.close(2);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV41Options = new GxSimpleCollection();
         AV44OptionsDesc = new GxSimpleCollection();
         AV46OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV49Session = context.GetSession();
         AV51GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV52GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV14TFContrato_Numero = "";
         AV15TFContrato_Numero_Sel = "";
         AV20TFContratada_PessoaNom = "";
         AV21TFContratada_PessoaNom_Sel = "";
         AV22TFContratada_PessoaCNPJ = "";
         AV23TFContratada_PessoaCNPJ_Sel = "";
         AV24TFContratoGarantia_DataPagtoGarantia = DateTime.MinValue;
         AV25TFContratoGarantia_DataPagtoGarantia_To = DateTime.MinValue;
         AV28TFContratoGarantia_DataCaucao = DateTime.MinValue;
         AV29TFContratoGarantia_DataCaucao_To = DateTime.MinValue;
         AV32TFContratoGarantia_DataEncerramento = DateTime.MinValue;
         AV33TFContratoGarantia_DataEncerramento_To = DateTime.MinValue;
         AV53GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV54DynamicFiltersSelector1 = "";
         AV55ContratoGarantia_DataPagtoGarantia1 = DateTime.MinValue;
         AV56ContratoGarantia_DataPagtoGarantia_To1 = DateTime.MinValue;
         AV58DynamicFiltersSelector2 = "";
         AV59ContratoGarantia_DataPagtoGarantia2 = DateTime.MinValue;
         AV60ContratoGarantia_DataPagtoGarantia_To2 = DateTime.MinValue;
         AV62DynamicFiltersSelector3 = "";
         AV63ContratoGarantia_DataPagtoGarantia3 = DateTime.MinValue;
         AV64ContratoGarantia_DataPagtoGarantia_To3 = DateTime.MinValue;
         scmdbuf = "";
         lV14TFContrato_Numero = "";
         lV20TFContratada_PessoaNom = "";
         lV22TFContratada_PessoaCNPJ = "";
         A102ContratoGarantia_DataPagtoGarantia = DateTime.MinValue;
         A77Contrato_Numero = "";
         A41Contratada_PessoaNom = "";
         A42Contratada_PessoaCNPJ = "";
         A104ContratoGarantia_DataCaucao = DateTime.MinValue;
         A106ContratoGarantia_DataEncerramento = DateTime.MinValue;
         P00UI2_A77Contrato_Numero = new String[] {""} ;
         P00UI2_A107ContratoGarantia_ValorEncerramento = new decimal[1] ;
         P00UI2_A106ContratoGarantia_DataEncerramento = new DateTime[] {DateTime.MinValue} ;
         P00UI2_A105ContratoGarantia_ValorGarantia = new decimal[1] ;
         P00UI2_A104ContratoGarantia_DataCaucao = new DateTime[] {DateTime.MinValue} ;
         P00UI2_A103ContratoGarantia_Percentual = new decimal[1] ;
         P00UI2_A42Contratada_PessoaCNPJ = new String[] {""} ;
         P00UI2_n42Contratada_PessoaCNPJ = new bool[] {false} ;
         P00UI2_A41Contratada_PessoaNom = new String[] {""} ;
         P00UI2_n41Contratada_PessoaNom = new bool[] {false} ;
         P00UI2_A40Contratada_PessoaCod = new int[1] ;
         P00UI2_A39Contratada_Codigo = new int[1] ;
         P00UI2_A74Contrato_Codigo = new int[1] ;
         P00UI2_A101ContratoGarantia_Codigo = new int[1] ;
         P00UI2_A102ContratoGarantia_DataPagtoGarantia = new DateTime[] {DateTime.MinValue} ;
         AV40Option = "";
         P00UI3_A41Contratada_PessoaNom = new String[] {""} ;
         P00UI3_n41Contratada_PessoaNom = new bool[] {false} ;
         P00UI3_A107ContratoGarantia_ValorEncerramento = new decimal[1] ;
         P00UI3_A106ContratoGarantia_DataEncerramento = new DateTime[] {DateTime.MinValue} ;
         P00UI3_A105ContratoGarantia_ValorGarantia = new decimal[1] ;
         P00UI3_A104ContratoGarantia_DataCaucao = new DateTime[] {DateTime.MinValue} ;
         P00UI3_A103ContratoGarantia_Percentual = new decimal[1] ;
         P00UI3_A42Contratada_PessoaCNPJ = new String[] {""} ;
         P00UI3_n42Contratada_PessoaCNPJ = new bool[] {false} ;
         P00UI3_A40Contratada_PessoaCod = new int[1] ;
         P00UI3_A39Contratada_Codigo = new int[1] ;
         P00UI3_A77Contrato_Numero = new String[] {""} ;
         P00UI3_A74Contrato_Codigo = new int[1] ;
         P00UI3_A101ContratoGarantia_Codigo = new int[1] ;
         P00UI3_A102ContratoGarantia_DataPagtoGarantia = new DateTime[] {DateTime.MinValue} ;
         P00UI4_A42Contratada_PessoaCNPJ = new String[] {""} ;
         P00UI4_n42Contratada_PessoaCNPJ = new bool[] {false} ;
         P00UI4_A107ContratoGarantia_ValorEncerramento = new decimal[1] ;
         P00UI4_A106ContratoGarantia_DataEncerramento = new DateTime[] {DateTime.MinValue} ;
         P00UI4_A105ContratoGarantia_ValorGarantia = new decimal[1] ;
         P00UI4_A104ContratoGarantia_DataCaucao = new DateTime[] {DateTime.MinValue} ;
         P00UI4_A103ContratoGarantia_Percentual = new decimal[1] ;
         P00UI4_A41Contratada_PessoaNom = new String[] {""} ;
         P00UI4_n41Contratada_PessoaNom = new bool[] {false} ;
         P00UI4_A40Contratada_PessoaCod = new int[1] ;
         P00UI4_A39Contratada_Codigo = new int[1] ;
         P00UI4_A77Contrato_Numero = new String[] {""} ;
         P00UI4_A74Contrato_Codigo = new int[1] ;
         P00UI4_A101ContratoGarantia_Codigo = new int[1] ;
         P00UI4_A102ContratoGarantia_DataPagtoGarantia = new DateTime[] {DateTime.MinValue} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getpromptcontratogarantiafilterdata__default(),
            new Object[][] {
                new Object[] {
               P00UI2_A77Contrato_Numero, P00UI2_A107ContratoGarantia_ValorEncerramento, P00UI2_A106ContratoGarantia_DataEncerramento, P00UI2_A105ContratoGarantia_ValorGarantia, P00UI2_A104ContratoGarantia_DataCaucao, P00UI2_A103ContratoGarantia_Percentual, P00UI2_A42Contratada_PessoaCNPJ, P00UI2_n42Contratada_PessoaCNPJ, P00UI2_A41Contratada_PessoaNom, P00UI2_n41Contratada_PessoaNom,
               P00UI2_A40Contratada_PessoaCod, P00UI2_A39Contratada_Codigo, P00UI2_A74Contrato_Codigo, P00UI2_A101ContratoGarantia_Codigo, P00UI2_A102ContratoGarantia_DataPagtoGarantia
               }
               , new Object[] {
               P00UI3_A41Contratada_PessoaNom, P00UI3_n41Contratada_PessoaNom, P00UI3_A107ContratoGarantia_ValorEncerramento, P00UI3_A106ContratoGarantia_DataEncerramento, P00UI3_A105ContratoGarantia_ValorGarantia, P00UI3_A104ContratoGarantia_DataCaucao, P00UI3_A103ContratoGarantia_Percentual, P00UI3_A42Contratada_PessoaCNPJ, P00UI3_n42Contratada_PessoaCNPJ, P00UI3_A40Contratada_PessoaCod,
               P00UI3_A39Contratada_Codigo, P00UI3_A77Contrato_Numero, P00UI3_A74Contrato_Codigo, P00UI3_A101ContratoGarantia_Codigo, P00UI3_A102ContratoGarantia_DataPagtoGarantia
               }
               , new Object[] {
               P00UI4_A42Contratada_PessoaCNPJ, P00UI4_n42Contratada_PessoaCNPJ, P00UI4_A107ContratoGarantia_ValorEncerramento, P00UI4_A106ContratoGarantia_DataEncerramento, P00UI4_A105ContratoGarantia_ValorGarantia, P00UI4_A104ContratoGarantia_DataCaucao, P00UI4_A103ContratoGarantia_Percentual, P00UI4_A41Contratada_PessoaNom, P00UI4_n41Contratada_PessoaNom, P00UI4_A40Contratada_PessoaCod,
               P00UI4_A39Contratada_Codigo, P00UI4_A77Contrato_Numero, P00UI4_A74Contrato_Codigo, P00UI4_A101ContratoGarantia_Codigo, P00UI4_A102ContratoGarantia_DataPagtoGarantia
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV67GXV1 ;
      private int AV10TFContratoGarantia_Codigo ;
      private int AV11TFContratoGarantia_Codigo_To ;
      private int AV12TFContrato_Codigo ;
      private int AV13TFContrato_Codigo_To ;
      private int AV16TFContratada_Codigo ;
      private int AV17TFContratada_Codigo_To ;
      private int AV18TFContratada_PessoaCod ;
      private int AV19TFContratada_PessoaCod_To ;
      private int A101ContratoGarantia_Codigo ;
      private int A74Contrato_Codigo ;
      private int A39Contratada_Codigo ;
      private int A40Contratada_PessoaCod ;
      private long AV48count ;
      private decimal AV26TFContratoGarantia_Percentual ;
      private decimal AV27TFContratoGarantia_Percentual_To ;
      private decimal AV30TFContratoGarantia_ValorGarantia ;
      private decimal AV31TFContratoGarantia_ValorGarantia_To ;
      private decimal AV34TFContratoGarantia_ValorEncerramento ;
      private decimal AV35TFContratoGarantia_ValorEncerramento_To ;
      private decimal A103ContratoGarantia_Percentual ;
      private decimal A105ContratoGarantia_ValorGarantia ;
      private decimal A107ContratoGarantia_ValorEncerramento ;
      private String AV14TFContrato_Numero ;
      private String AV15TFContrato_Numero_Sel ;
      private String AV20TFContratada_PessoaNom ;
      private String AV21TFContratada_PessoaNom_Sel ;
      private String scmdbuf ;
      private String lV14TFContrato_Numero ;
      private String lV20TFContratada_PessoaNom ;
      private String A77Contrato_Numero ;
      private String A41Contratada_PessoaNom ;
      private DateTime AV24TFContratoGarantia_DataPagtoGarantia ;
      private DateTime AV25TFContratoGarantia_DataPagtoGarantia_To ;
      private DateTime AV28TFContratoGarantia_DataCaucao ;
      private DateTime AV29TFContratoGarantia_DataCaucao_To ;
      private DateTime AV32TFContratoGarantia_DataEncerramento ;
      private DateTime AV33TFContratoGarantia_DataEncerramento_To ;
      private DateTime AV55ContratoGarantia_DataPagtoGarantia1 ;
      private DateTime AV56ContratoGarantia_DataPagtoGarantia_To1 ;
      private DateTime AV59ContratoGarantia_DataPagtoGarantia2 ;
      private DateTime AV60ContratoGarantia_DataPagtoGarantia_To2 ;
      private DateTime AV63ContratoGarantia_DataPagtoGarantia3 ;
      private DateTime AV64ContratoGarantia_DataPagtoGarantia_To3 ;
      private DateTime A102ContratoGarantia_DataPagtoGarantia ;
      private DateTime A104ContratoGarantia_DataCaucao ;
      private DateTime A106ContratoGarantia_DataEncerramento ;
      private bool returnInSub ;
      private bool AV57DynamicFiltersEnabled2 ;
      private bool AV61DynamicFiltersEnabled3 ;
      private bool BRKUI2 ;
      private bool n42Contratada_PessoaCNPJ ;
      private bool n41Contratada_PessoaNom ;
      private bool BRKUI4 ;
      private bool BRKUI6 ;
      private String AV47OptionIndexesJson ;
      private String AV42OptionsJson ;
      private String AV45OptionsDescJson ;
      private String AV38DDOName ;
      private String AV36SearchTxt ;
      private String AV37SearchTxtTo ;
      private String AV22TFContratada_PessoaCNPJ ;
      private String AV23TFContratada_PessoaCNPJ_Sel ;
      private String AV54DynamicFiltersSelector1 ;
      private String AV58DynamicFiltersSelector2 ;
      private String AV62DynamicFiltersSelector3 ;
      private String lV22TFContratada_PessoaCNPJ ;
      private String A42Contratada_PessoaCNPJ ;
      private String AV40Option ;
      private IGxSession AV49Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] P00UI2_A77Contrato_Numero ;
      private decimal[] P00UI2_A107ContratoGarantia_ValorEncerramento ;
      private DateTime[] P00UI2_A106ContratoGarantia_DataEncerramento ;
      private decimal[] P00UI2_A105ContratoGarantia_ValorGarantia ;
      private DateTime[] P00UI2_A104ContratoGarantia_DataCaucao ;
      private decimal[] P00UI2_A103ContratoGarantia_Percentual ;
      private String[] P00UI2_A42Contratada_PessoaCNPJ ;
      private bool[] P00UI2_n42Contratada_PessoaCNPJ ;
      private String[] P00UI2_A41Contratada_PessoaNom ;
      private bool[] P00UI2_n41Contratada_PessoaNom ;
      private int[] P00UI2_A40Contratada_PessoaCod ;
      private int[] P00UI2_A39Contratada_Codigo ;
      private int[] P00UI2_A74Contrato_Codigo ;
      private int[] P00UI2_A101ContratoGarantia_Codigo ;
      private DateTime[] P00UI2_A102ContratoGarantia_DataPagtoGarantia ;
      private String[] P00UI3_A41Contratada_PessoaNom ;
      private bool[] P00UI3_n41Contratada_PessoaNom ;
      private decimal[] P00UI3_A107ContratoGarantia_ValorEncerramento ;
      private DateTime[] P00UI3_A106ContratoGarantia_DataEncerramento ;
      private decimal[] P00UI3_A105ContratoGarantia_ValorGarantia ;
      private DateTime[] P00UI3_A104ContratoGarantia_DataCaucao ;
      private decimal[] P00UI3_A103ContratoGarantia_Percentual ;
      private String[] P00UI3_A42Contratada_PessoaCNPJ ;
      private bool[] P00UI3_n42Contratada_PessoaCNPJ ;
      private int[] P00UI3_A40Contratada_PessoaCod ;
      private int[] P00UI3_A39Contratada_Codigo ;
      private String[] P00UI3_A77Contrato_Numero ;
      private int[] P00UI3_A74Contrato_Codigo ;
      private int[] P00UI3_A101ContratoGarantia_Codigo ;
      private DateTime[] P00UI3_A102ContratoGarantia_DataPagtoGarantia ;
      private String[] P00UI4_A42Contratada_PessoaCNPJ ;
      private bool[] P00UI4_n42Contratada_PessoaCNPJ ;
      private decimal[] P00UI4_A107ContratoGarantia_ValorEncerramento ;
      private DateTime[] P00UI4_A106ContratoGarantia_DataEncerramento ;
      private decimal[] P00UI4_A105ContratoGarantia_ValorGarantia ;
      private DateTime[] P00UI4_A104ContratoGarantia_DataCaucao ;
      private decimal[] P00UI4_A103ContratoGarantia_Percentual ;
      private String[] P00UI4_A41Contratada_PessoaNom ;
      private bool[] P00UI4_n41Contratada_PessoaNom ;
      private int[] P00UI4_A40Contratada_PessoaCod ;
      private int[] P00UI4_A39Contratada_Codigo ;
      private String[] P00UI4_A77Contrato_Numero ;
      private int[] P00UI4_A74Contrato_Codigo ;
      private int[] P00UI4_A101ContratoGarantia_Codigo ;
      private DateTime[] P00UI4_A102ContratoGarantia_DataPagtoGarantia ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV41Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV44OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV46OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV51GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV52GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV53GridStateDynamicFilter ;
   }

   public class getpromptcontratogarantiafilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00UI2( IGxContext context ,
                                             String AV54DynamicFiltersSelector1 ,
                                             DateTime AV55ContratoGarantia_DataPagtoGarantia1 ,
                                             DateTime AV56ContratoGarantia_DataPagtoGarantia_To1 ,
                                             bool AV57DynamicFiltersEnabled2 ,
                                             String AV58DynamicFiltersSelector2 ,
                                             DateTime AV59ContratoGarantia_DataPagtoGarantia2 ,
                                             DateTime AV60ContratoGarantia_DataPagtoGarantia_To2 ,
                                             bool AV61DynamicFiltersEnabled3 ,
                                             String AV62DynamicFiltersSelector3 ,
                                             DateTime AV63ContratoGarantia_DataPagtoGarantia3 ,
                                             DateTime AV64ContratoGarantia_DataPagtoGarantia_To3 ,
                                             int AV10TFContratoGarantia_Codigo ,
                                             int AV11TFContratoGarantia_Codigo_To ,
                                             int AV12TFContrato_Codigo ,
                                             int AV13TFContrato_Codigo_To ,
                                             String AV15TFContrato_Numero_Sel ,
                                             String AV14TFContrato_Numero ,
                                             int AV16TFContratada_Codigo ,
                                             int AV17TFContratada_Codigo_To ,
                                             int AV18TFContratada_PessoaCod ,
                                             int AV19TFContratada_PessoaCod_To ,
                                             String AV21TFContratada_PessoaNom_Sel ,
                                             String AV20TFContratada_PessoaNom ,
                                             String AV23TFContratada_PessoaCNPJ_Sel ,
                                             String AV22TFContratada_PessoaCNPJ ,
                                             DateTime AV24TFContratoGarantia_DataPagtoGarantia ,
                                             DateTime AV25TFContratoGarantia_DataPagtoGarantia_To ,
                                             decimal AV26TFContratoGarantia_Percentual ,
                                             decimal AV27TFContratoGarantia_Percentual_To ,
                                             DateTime AV28TFContratoGarantia_DataCaucao ,
                                             DateTime AV29TFContratoGarantia_DataCaucao_To ,
                                             decimal AV30TFContratoGarantia_ValorGarantia ,
                                             decimal AV31TFContratoGarantia_ValorGarantia_To ,
                                             DateTime AV32TFContratoGarantia_DataEncerramento ,
                                             DateTime AV33TFContratoGarantia_DataEncerramento_To ,
                                             decimal AV34TFContratoGarantia_ValorEncerramento ,
                                             decimal AV35TFContratoGarantia_ValorEncerramento_To ,
                                             DateTime A102ContratoGarantia_DataPagtoGarantia ,
                                             int A101ContratoGarantia_Codigo ,
                                             int A74Contrato_Codigo ,
                                             String A77Contrato_Numero ,
                                             int A39Contratada_Codigo ,
                                             int A40Contratada_PessoaCod ,
                                             String A41Contratada_PessoaNom ,
                                             String A42Contratada_PessoaCNPJ ,
                                             decimal A103ContratoGarantia_Percentual ,
                                             DateTime A104ContratoGarantia_DataCaucao ,
                                             decimal A105ContratoGarantia_ValorGarantia ,
                                             DateTime A106ContratoGarantia_DataEncerramento ,
                                             decimal A107ContratoGarantia_ValorEncerramento )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [32] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T2.[Contrato_Numero], T1.[ContratoGarantia_ValorEncerramento], T1.[ContratoGarantia_DataEncerramento], T1.[ContratoGarantia_ValorGarantia], T1.[ContratoGarantia_DataCaucao], T1.[ContratoGarantia_Percentual], T4.[Pessoa_Docto] AS Contratada_PessoaCNPJ, T4.[Pessoa_Nome] AS Contratada_PessoaNom, T3.[Contratada_PessoaCod] AS Contratada_PessoaCod, T2.[Contratada_Codigo], T1.[Contrato_Codigo], T1.[ContratoGarantia_Codigo], T1.[ContratoGarantia_DataPagtoGarantia] FROM ((([ContratoGarantia] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo]) INNER JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T2.[Contratada_Codigo]) INNER JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T3.[Contratada_PessoaCod])";
         if ( ( StringUtil.StrCmp(AV54DynamicFiltersSelector1, "CONTRATOGARANTIA_DATAPAGTOGARANTIA") == 0 ) && ( ! (DateTime.MinValue==AV55ContratoGarantia_DataPagtoGarantia1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataPagtoGarantia] >= @AV55ContratoGarantia_DataPagtoGarantia1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataPagtoGarantia] >= @AV55ContratoGarantia_DataPagtoGarantia1)";
            }
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV54DynamicFiltersSelector1, "CONTRATOGARANTIA_DATAPAGTOGARANTIA") == 0 ) && ( ! (DateTime.MinValue==AV56ContratoGarantia_DataPagtoGarantia_To1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataPagtoGarantia] <= @AV56ContratoGarantia_DataPagtoGarantia_To1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataPagtoGarantia] <= @AV56ContratoGarantia_DataPagtoGarantia_To1)";
            }
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( AV57DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV58DynamicFiltersSelector2, "CONTRATOGARANTIA_DATAPAGTOGARANTIA") == 0 ) && ( ! (DateTime.MinValue==AV59ContratoGarantia_DataPagtoGarantia2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataPagtoGarantia] >= @AV59ContratoGarantia_DataPagtoGarantia2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataPagtoGarantia] >= @AV59ContratoGarantia_DataPagtoGarantia2)";
            }
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( AV57DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV58DynamicFiltersSelector2, "CONTRATOGARANTIA_DATAPAGTOGARANTIA") == 0 ) && ( ! (DateTime.MinValue==AV60ContratoGarantia_DataPagtoGarantia_To2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataPagtoGarantia] <= @AV60ContratoGarantia_DataPagtoGarantia_To2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataPagtoGarantia] <= @AV60ContratoGarantia_DataPagtoGarantia_To2)";
            }
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( AV61DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV62DynamicFiltersSelector3, "CONTRATOGARANTIA_DATAPAGTOGARANTIA") == 0 ) && ( ! (DateTime.MinValue==AV63ContratoGarantia_DataPagtoGarantia3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataPagtoGarantia] >= @AV63ContratoGarantia_DataPagtoGarantia3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataPagtoGarantia] >= @AV63ContratoGarantia_DataPagtoGarantia3)";
            }
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( AV61DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV62DynamicFiltersSelector3, "CONTRATOGARANTIA_DATAPAGTOGARANTIA") == 0 ) && ( ! (DateTime.MinValue==AV64ContratoGarantia_DataPagtoGarantia_To3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataPagtoGarantia] <= @AV64ContratoGarantia_DataPagtoGarantia_To3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataPagtoGarantia] <= @AV64ContratoGarantia_DataPagtoGarantia_To3)";
            }
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( ! (0==AV10TFContratoGarantia_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_Codigo] >= @AV10TFContratoGarantia_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_Codigo] >= @AV10TFContratoGarantia_Codigo)";
            }
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( ! (0==AV11TFContratoGarantia_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_Codigo] <= @AV11TFContratoGarantia_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_Codigo] <= @AV11TFContratoGarantia_Codigo_To)";
            }
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( ! (0==AV12TFContrato_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Codigo] >= @AV12TFContrato_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Codigo] >= @AV12TFContrato_Codigo)";
            }
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( ! (0==AV13TFContrato_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Codigo] <= @AV13TFContrato_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Codigo] <= @AV13TFContrato_Codigo_To)";
            }
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFContrato_Numero_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFContrato_Numero)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] like @lV14TFContrato_Numero)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] like @lV14TFContrato_Numero)";
            }
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFContrato_Numero_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] = @AV15TFContrato_Numero_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] = @AV15TFContrato_Numero_Sel)";
            }
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( ! (0==AV16TFContratada_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contratada_Codigo] >= @AV16TFContratada_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contratada_Codigo] >= @AV16TFContratada_Codigo)";
            }
         }
         else
         {
            GXv_int1[12] = 1;
         }
         if ( ! (0==AV17TFContratada_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contratada_Codigo] <= @AV17TFContratada_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contratada_Codigo] <= @AV17TFContratada_Codigo_To)";
            }
         }
         else
         {
            GXv_int1[13] = 1;
         }
         if ( ! (0==AV18TFContratada_PessoaCod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Contratada_PessoaCod] >= @AV18TFContratada_PessoaCod)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Contratada_PessoaCod] >= @AV18TFContratada_PessoaCod)";
            }
         }
         else
         {
            GXv_int1[14] = 1;
         }
         if ( ! (0==AV19TFContratada_PessoaCod_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Contratada_PessoaCod] <= @AV19TFContratada_PessoaCod_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Contratada_PessoaCod] <= @AV19TFContratada_PessoaCod_To)";
            }
         }
         else
         {
            GXv_int1[15] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV21TFContratada_PessoaNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV20TFContratada_PessoaNom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like @lV20TFContratada_PessoaNom)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like @lV20TFContratada_PessoaNom)";
            }
         }
         else
         {
            GXv_int1[16] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV21TFContratada_PessoaNom_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] = @AV21TFContratada_PessoaNom_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] = @AV21TFContratada_PessoaNom_Sel)";
            }
         }
         else
         {
            GXv_int1[17] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV23TFContratada_PessoaCNPJ_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV22TFContratada_PessoaCNPJ)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Docto] like @lV22TFContratada_PessoaCNPJ)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Docto] like @lV22TFContratada_PessoaCNPJ)";
            }
         }
         else
         {
            GXv_int1[18] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23TFContratada_PessoaCNPJ_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Docto] = @AV23TFContratada_PessoaCNPJ_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Docto] = @AV23TFContratada_PessoaCNPJ_Sel)";
            }
         }
         else
         {
            GXv_int1[19] = 1;
         }
         if ( ! (DateTime.MinValue==AV24TFContratoGarantia_DataPagtoGarantia) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataPagtoGarantia] >= @AV24TFContratoGarantia_DataPagtoGarantia)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataPagtoGarantia] >= @AV24TFContratoGarantia_DataPagtoGarantia)";
            }
         }
         else
         {
            GXv_int1[20] = 1;
         }
         if ( ! (DateTime.MinValue==AV25TFContratoGarantia_DataPagtoGarantia_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataPagtoGarantia] <= @AV25TFContratoGarantia_DataPagtoGarantia_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataPagtoGarantia] <= @AV25TFContratoGarantia_DataPagtoGarantia_To)";
            }
         }
         else
         {
            GXv_int1[21] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV26TFContratoGarantia_Percentual) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_Percentual] >= @AV26TFContratoGarantia_Percentual)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_Percentual] >= @AV26TFContratoGarantia_Percentual)";
            }
         }
         else
         {
            GXv_int1[22] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV27TFContratoGarantia_Percentual_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_Percentual] <= @AV27TFContratoGarantia_Percentual_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_Percentual] <= @AV27TFContratoGarantia_Percentual_To)";
            }
         }
         else
         {
            GXv_int1[23] = 1;
         }
         if ( ! (DateTime.MinValue==AV28TFContratoGarantia_DataCaucao) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataCaucao] >= @AV28TFContratoGarantia_DataCaucao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataCaucao] >= @AV28TFContratoGarantia_DataCaucao)";
            }
         }
         else
         {
            GXv_int1[24] = 1;
         }
         if ( ! (DateTime.MinValue==AV29TFContratoGarantia_DataCaucao_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataCaucao] <= @AV29TFContratoGarantia_DataCaucao_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataCaucao] <= @AV29TFContratoGarantia_DataCaucao_To)";
            }
         }
         else
         {
            GXv_int1[25] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV30TFContratoGarantia_ValorGarantia) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_ValorGarantia] >= @AV30TFContratoGarantia_ValorGarantia)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_ValorGarantia] >= @AV30TFContratoGarantia_ValorGarantia)";
            }
         }
         else
         {
            GXv_int1[26] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV31TFContratoGarantia_ValorGarantia_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_ValorGarantia] <= @AV31TFContratoGarantia_ValorGarantia_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_ValorGarantia] <= @AV31TFContratoGarantia_ValorGarantia_To)";
            }
         }
         else
         {
            GXv_int1[27] = 1;
         }
         if ( ! (DateTime.MinValue==AV32TFContratoGarantia_DataEncerramento) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataEncerramento] >= @AV32TFContratoGarantia_DataEncerramento)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataEncerramento] >= @AV32TFContratoGarantia_DataEncerramento)";
            }
         }
         else
         {
            GXv_int1[28] = 1;
         }
         if ( ! (DateTime.MinValue==AV33TFContratoGarantia_DataEncerramento_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataEncerramento] <= @AV33TFContratoGarantia_DataEncerramento_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataEncerramento] <= @AV33TFContratoGarantia_DataEncerramento_To)";
            }
         }
         else
         {
            GXv_int1[29] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV34TFContratoGarantia_ValorEncerramento) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_ValorEncerramento] >= @AV34TFContratoGarantia_ValorEncerramento)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_ValorEncerramento] >= @AV34TFContratoGarantia_ValorEncerramento)";
            }
         }
         else
         {
            GXv_int1[30] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV35TFContratoGarantia_ValorEncerramento_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_ValorEncerramento] <= @AV35TFContratoGarantia_ValorEncerramento_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_ValorEncerramento] <= @AV35TFContratoGarantia_ValorEncerramento_To)";
            }
         }
         else
         {
            GXv_int1[31] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T2.[Contrato_Numero]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00UI3( IGxContext context ,
                                             String AV54DynamicFiltersSelector1 ,
                                             DateTime AV55ContratoGarantia_DataPagtoGarantia1 ,
                                             DateTime AV56ContratoGarantia_DataPagtoGarantia_To1 ,
                                             bool AV57DynamicFiltersEnabled2 ,
                                             String AV58DynamicFiltersSelector2 ,
                                             DateTime AV59ContratoGarantia_DataPagtoGarantia2 ,
                                             DateTime AV60ContratoGarantia_DataPagtoGarantia_To2 ,
                                             bool AV61DynamicFiltersEnabled3 ,
                                             String AV62DynamicFiltersSelector3 ,
                                             DateTime AV63ContratoGarantia_DataPagtoGarantia3 ,
                                             DateTime AV64ContratoGarantia_DataPagtoGarantia_To3 ,
                                             int AV10TFContratoGarantia_Codigo ,
                                             int AV11TFContratoGarantia_Codigo_To ,
                                             int AV12TFContrato_Codigo ,
                                             int AV13TFContrato_Codigo_To ,
                                             String AV15TFContrato_Numero_Sel ,
                                             String AV14TFContrato_Numero ,
                                             int AV16TFContratada_Codigo ,
                                             int AV17TFContratada_Codigo_To ,
                                             int AV18TFContratada_PessoaCod ,
                                             int AV19TFContratada_PessoaCod_To ,
                                             String AV21TFContratada_PessoaNom_Sel ,
                                             String AV20TFContratada_PessoaNom ,
                                             String AV23TFContratada_PessoaCNPJ_Sel ,
                                             String AV22TFContratada_PessoaCNPJ ,
                                             DateTime AV24TFContratoGarantia_DataPagtoGarantia ,
                                             DateTime AV25TFContratoGarantia_DataPagtoGarantia_To ,
                                             decimal AV26TFContratoGarantia_Percentual ,
                                             decimal AV27TFContratoGarantia_Percentual_To ,
                                             DateTime AV28TFContratoGarantia_DataCaucao ,
                                             DateTime AV29TFContratoGarantia_DataCaucao_To ,
                                             decimal AV30TFContratoGarantia_ValorGarantia ,
                                             decimal AV31TFContratoGarantia_ValorGarantia_To ,
                                             DateTime AV32TFContratoGarantia_DataEncerramento ,
                                             DateTime AV33TFContratoGarantia_DataEncerramento_To ,
                                             decimal AV34TFContratoGarantia_ValorEncerramento ,
                                             decimal AV35TFContratoGarantia_ValorEncerramento_To ,
                                             DateTime A102ContratoGarantia_DataPagtoGarantia ,
                                             int A101ContratoGarantia_Codigo ,
                                             int A74Contrato_Codigo ,
                                             String A77Contrato_Numero ,
                                             int A39Contratada_Codigo ,
                                             int A40Contratada_PessoaCod ,
                                             String A41Contratada_PessoaNom ,
                                             String A42Contratada_PessoaCNPJ ,
                                             decimal A103ContratoGarantia_Percentual ,
                                             DateTime A104ContratoGarantia_DataCaucao ,
                                             decimal A105ContratoGarantia_ValorGarantia ,
                                             DateTime A106ContratoGarantia_DataEncerramento ,
                                             decimal A107ContratoGarantia_ValorEncerramento )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [32] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT T4.[Pessoa_Nome] AS Contratada_PessoaNom, T1.[ContratoGarantia_ValorEncerramento], T1.[ContratoGarantia_DataEncerramento], T1.[ContratoGarantia_ValorGarantia], T1.[ContratoGarantia_DataCaucao], T1.[ContratoGarantia_Percentual], T4.[Pessoa_Docto] AS Contratada_PessoaCNPJ, T3.[Contratada_PessoaCod] AS Contratada_PessoaCod, T2.[Contratada_Codigo], T2.[Contrato_Numero], T1.[Contrato_Codigo], T1.[ContratoGarantia_Codigo], T1.[ContratoGarantia_DataPagtoGarantia] FROM ((([ContratoGarantia] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo]) INNER JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T2.[Contratada_Codigo]) INNER JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T3.[Contratada_PessoaCod])";
         if ( ( StringUtil.StrCmp(AV54DynamicFiltersSelector1, "CONTRATOGARANTIA_DATAPAGTOGARANTIA") == 0 ) && ( ! (DateTime.MinValue==AV55ContratoGarantia_DataPagtoGarantia1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataPagtoGarantia] >= @AV55ContratoGarantia_DataPagtoGarantia1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataPagtoGarantia] >= @AV55ContratoGarantia_DataPagtoGarantia1)";
            }
         }
         else
         {
            GXv_int3[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV54DynamicFiltersSelector1, "CONTRATOGARANTIA_DATAPAGTOGARANTIA") == 0 ) && ( ! (DateTime.MinValue==AV56ContratoGarantia_DataPagtoGarantia_To1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataPagtoGarantia] <= @AV56ContratoGarantia_DataPagtoGarantia_To1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataPagtoGarantia] <= @AV56ContratoGarantia_DataPagtoGarantia_To1)";
            }
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( AV57DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV58DynamicFiltersSelector2, "CONTRATOGARANTIA_DATAPAGTOGARANTIA") == 0 ) && ( ! (DateTime.MinValue==AV59ContratoGarantia_DataPagtoGarantia2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataPagtoGarantia] >= @AV59ContratoGarantia_DataPagtoGarantia2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataPagtoGarantia] >= @AV59ContratoGarantia_DataPagtoGarantia2)";
            }
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( AV57DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV58DynamicFiltersSelector2, "CONTRATOGARANTIA_DATAPAGTOGARANTIA") == 0 ) && ( ! (DateTime.MinValue==AV60ContratoGarantia_DataPagtoGarantia_To2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataPagtoGarantia] <= @AV60ContratoGarantia_DataPagtoGarantia_To2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataPagtoGarantia] <= @AV60ContratoGarantia_DataPagtoGarantia_To2)";
            }
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( AV61DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV62DynamicFiltersSelector3, "CONTRATOGARANTIA_DATAPAGTOGARANTIA") == 0 ) && ( ! (DateTime.MinValue==AV63ContratoGarantia_DataPagtoGarantia3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataPagtoGarantia] >= @AV63ContratoGarantia_DataPagtoGarantia3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataPagtoGarantia] >= @AV63ContratoGarantia_DataPagtoGarantia3)";
            }
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( AV61DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV62DynamicFiltersSelector3, "CONTRATOGARANTIA_DATAPAGTOGARANTIA") == 0 ) && ( ! (DateTime.MinValue==AV64ContratoGarantia_DataPagtoGarantia_To3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataPagtoGarantia] <= @AV64ContratoGarantia_DataPagtoGarantia_To3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataPagtoGarantia] <= @AV64ContratoGarantia_DataPagtoGarantia_To3)";
            }
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( ! (0==AV10TFContratoGarantia_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_Codigo] >= @AV10TFContratoGarantia_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_Codigo] >= @AV10TFContratoGarantia_Codigo)";
            }
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( ! (0==AV11TFContratoGarantia_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_Codigo] <= @AV11TFContratoGarantia_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_Codigo] <= @AV11TFContratoGarantia_Codigo_To)";
            }
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( ! (0==AV12TFContrato_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Codigo] >= @AV12TFContrato_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Codigo] >= @AV12TFContrato_Codigo)";
            }
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( ! (0==AV13TFContrato_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Codigo] <= @AV13TFContrato_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Codigo] <= @AV13TFContrato_Codigo_To)";
            }
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFContrato_Numero_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFContrato_Numero)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] like @lV14TFContrato_Numero)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] like @lV14TFContrato_Numero)";
            }
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFContrato_Numero_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] = @AV15TFContrato_Numero_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] = @AV15TFContrato_Numero_Sel)";
            }
         }
         else
         {
            GXv_int3[11] = 1;
         }
         if ( ! (0==AV16TFContratada_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contratada_Codigo] >= @AV16TFContratada_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contratada_Codigo] >= @AV16TFContratada_Codigo)";
            }
         }
         else
         {
            GXv_int3[12] = 1;
         }
         if ( ! (0==AV17TFContratada_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contratada_Codigo] <= @AV17TFContratada_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contratada_Codigo] <= @AV17TFContratada_Codigo_To)";
            }
         }
         else
         {
            GXv_int3[13] = 1;
         }
         if ( ! (0==AV18TFContratada_PessoaCod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Contratada_PessoaCod] >= @AV18TFContratada_PessoaCod)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Contratada_PessoaCod] >= @AV18TFContratada_PessoaCod)";
            }
         }
         else
         {
            GXv_int3[14] = 1;
         }
         if ( ! (0==AV19TFContratada_PessoaCod_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Contratada_PessoaCod] <= @AV19TFContratada_PessoaCod_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Contratada_PessoaCod] <= @AV19TFContratada_PessoaCod_To)";
            }
         }
         else
         {
            GXv_int3[15] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV21TFContratada_PessoaNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV20TFContratada_PessoaNom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like @lV20TFContratada_PessoaNom)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like @lV20TFContratada_PessoaNom)";
            }
         }
         else
         {
            GXv_int3[16] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV21TFContratada_PessoaNom_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] = @AV21TFContratada_PessoaNom_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] = @AV21TFContratada_PessoaNom_Sel)";
            }
         }
         else
         {
            GXv_int3[17] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV23TFContratada_PessoaCNPJ_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV22TFContratada_PessoaCNPJ)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Docto] like @lV22TFContratada_PessoaCNPJ)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Docto] like @lV22TFContratada_PessoaCNPJ)";
            }
         }
         else
         {
            GXv_int3[18] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23TFContratada_PessoaCNPJ_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Docto] = @AV23TFContratada_PessoaCNPJ_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Docto] = @AV23TFContratada_PessoaCNPJ_Sel)";
            }
         }
         else
         {
            GXv_int3[19] = 1;
         }
         if ( ! (DateTime.MinValue==AV24TFContratoGarantia_DataPagtoGarantia) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataPagtoGarantia] >= @AV24TFContratoGarantia_DataPagtoGarantia)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataPagtoGarantia] >= @AV24TFContratoGarantia_DataPagtoGarantia)";
            }
         }
         else
         {
            GXv_int3[20] = 1;
         }
         if ( ! (DateTime.MinValue==AV25TFContratoGarantia_DataPagtoGarantia_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataPagtoGarantia] <= @AV25TFContratoGarantia_DataPagtoGarantia_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataPagtoGarantia] <= @AV25TFContratoGarantia_DataPagtoGarantia_To)";
            }
         }
         else
         {
            GXv_int3[21] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV26TFContratoGarantia_Percentual) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_Percentual] >= @AV26TFContratoGarantia_Percentual)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_Percentual] >= @AV26TFContratoGarantia_Percentual)";
            }
         }
         else
         {
            GXv_int3[22] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV27TFContratoGarantia_Percentual_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_Percentual] <= @AV27TFContratoGarantia_Percentual_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_Percentual] <= @AV27TFContratoGarantia_Percentual_To)";
            }
         }
         else
         {
            GXv_int3[23] = 1;
         }
         if ( ! (DateTime.MinValue==AV28TFContratoGarantia_DataCaucao) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataCaucao] >= @AV28TFContratoGarantia_DataCaucao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataCaucao] >= @AV28TFContratoGarantia_DataCaucao)";
            }
         }
         else
         {
            GXv_int3[24] = 1;
         }
         if ( ! (DateTime.MinValue==AV29TFContratoGarantia_DataCaucao_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataCaucao] <= @AV29TFContratoGarantia_DataCaucao_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataCaucao] <= @AV29TFContratoGarantia_DataCaucao_To)";
            }
         }
         else
         {
            GXv_int3[25] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV30TFContratoGarantia_ValorGarantia) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_ValorGarantia] >= @AV30TFContratoGarantia_ValorGarantia)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_ValorGarantia] >= @AV30TFContratoGarantia_ValorGarantia)";
            }
         }
         else
         {
            GXv_int3[26] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV31TFContratoGarantia_ValorGarantia_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_ValorGarantia] <= @AV31TFContratoGarantia_ValorGarantia_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_ValorGarantia] <= @AV31TFContratoGarantia_ValorGarantia_To)";
            }
         }
         else
         {
            GXv_int3[27] = 1;
         }
         if ( ! (DateTime.MinValue==AV32TFContratoGarantia_DataEncerramento) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataEncerramento] >= @AV32TFContratoGarantia_DataEncerramento)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataEncerramento] >= @AV32TFContratoGarantia_DataEncerramento)";
            }
         }
         else
         {
            GXv_int3[28] = 1;
         }
         if ( ! (DateTime.MinValue==AV33TFContratoGarantia_DataEncerramento_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataEncerramento] <= @AV33TFContratoGarantia_DataEncerramento_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataEncerramento] <= @AV33TFContratoGarantia_DataEncerramento_To)";
            }
         }
         else
         {
            GXv_int3[29] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV34TFContratoGarantia_ValorEncerramento) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_ValorEncerramento] >= @AV34TFContratoGarantia_ValorEncerramento)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_ValorEncerramento] >= @AV34TFContratoGarantia_ValorEncerramento)";
            }
         }
         else
         {
            GXv_int3[30] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV35TFContratoGarantia_ValorEncerramento_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_ValorEncerramento] <= @AV35TFContratoGarantia_ValorEncerramento_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_ValorEncerramento] <= @AV35TFContratoGarantia_ValorEncerramento_To)";
            }
         }
         else
         {
            GXv_int3[31] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T4.[Pessoa_Nome]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      protected Object[] conditional_P00UI4( IGxContext context ,
                                             String AV54DynamicFiltersSelector1 ,
                                             DateTime AV55ContratoGarantia_DataPagtoGarantia1 ,
                                             DateTime AV56ContratoGarantia_DataPagtoGarantia_To1 ,
                                             bool AV57DynamicFiltersEnabled2 ,
                                             String AV58DynamicFiltersSelector2 ,
                                             DateTime AV59ContratoGarantia_DataPagtoGarantia2 ,
                                             DateTime AV60ContratoGarantia_DataPagtoGarantia_To2 ,
                                             bool AV61DynamicFiltersEnabled3 ,
                                             String AV62DynamicFiltersSelector3 ,
                                             DateTime AV63ContratoGarantia_DataPagtoGarantia3 ,
                                             DateTime AV64ContratoGarantia_DataPagtoGarantia_To3 ,
                                             int AV10TFContratoGarantia_Codigo ,
                                             int AV11TFContratoGarantia_Codigo_To ,
                                             int AV12TFContrato_Codigo ,
                                             int AV13TFContrato_Codigo_To ,
                                             String AV15TFContrato_Numero_Sel ,
                                             String AV14TFContrato_Numero ,
                                             int AV16TFContratada_Codigo ,
                                             int AV17TFContratada_Codigo_To ,
                                             int AV18TFContratada_PessoaCod ,
                                             int AV19TFContratada_PessoaCod_To ,
                                             String AV21TFContratada_PessoaNom_Sel ,
                                             String AV20TFContratada_PessoaNom ,
                                             String AV23TFContratada_PessoaCNPJ_Sel ,
                                             String AV22TFContratada_PessoaCNPJ ,
                                             DateTime AV24TFContratoGarantia_DataPagtoGarantia ,
                                             DateTime AV25TFContratoGarantia_DataPagtoGarantia_To ,
                                             decimal AV26TFContratoGarantia_Percentual ,
                                             decimal AV27TFContratoGarantia_Percentual_To ,
                                             DateTime AV28TFContratoGarantia_DataCaucao ,
                                             DateTime AV29TFContratoGarantia_DataCaucao_To ,
                                             decimal AV30TFContratoGarantia_ValorGarantia ,
                                             decimal AV31TFContratoGarantia_ValorGarantia_To ,
                                             DateTime AV32TFContratoGarantia_DataEncerramento ,
                                             DateTime AV33TFContratoGarantia_DataEncerramento_To ,
                                             decimal AV34TFContratoGarantia_ValorEncerramento ,
                                             decimal AV35TFContratoGarantia_ValorEncerramento_To ,
                                             DateTime A102ContratoGarantia_DataPagtoGarantia ,
                                             int A101ContratoGarantia_Codigo ,
                                             int A74Contrato_Codigo ,
                                             String A77Contrato_Numero ,
                                             int A39Contratada_Codigo ,
                                             int A40Contratada_PessoaCod ,
                                             String A41Contratada_PessoaNom ,
                                             String A42Contratada_PessoaCNPJ ,
                                             decimal A103ContratoGarantia_Percentual ,
                                             DateTime A104ContratoGarantia_DataCaucao ,
                                             decimal A105ContratoGarantia_ValorGarantia ,
                                             DateTime A106ContratoGarantia_DataEncerramento ,
                                             decimal A107ContratoGarantia_ValorEncerramento )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int5 ;
         GXv_int5 = new short [32] ;
         Object[] GXv_Object6 ;
         GXv_Object6 = new Object [2] ;
         scmdbuf = "SELECT T4.[Pessoa_Docto] AS Contratada_PessoaCNPJ, T1.[ContratoGarantia_ValorEncerramento], T1.[ContratoGarantia_DataEncerramento], T1.[ContratoGarantia_ValorGarantia], T1.[ContratoGarantia_DataCaucao], T1.[ContratoGarantia_Percentual], T4.[Pessoa_Nome] AS Contratada_PessoaNom, T3.[Contratada_PessoaCod] AS Contratada_PessoaCod, T2.[Contratada_Codigo], T2.[Contrato_Numero], T1.[Contrato_Codigo], T1.[ContratoGarantia_Codigo], T1.[ContratoGarantia_DataPagtoGarantia] FROM ((([ContratoGarantia] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo]) INNER JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T2.[Contratada_Codigo]) INNER JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T3.[Contratada_PessoaCod])";
         if ( ( StringUtil.StrCmp(AV54DynamicFiltersSelector1, "CONTRATOGARANTIA_DATAPAGTOGARANTIA") == 0 ) && ( ! (DateTime.MinValue==AV55ContratoGarantia_DataPagtoGarantia1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataPagtoGarantia] >= @AV55ContratoGarantia_DataPagtoGarantia1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataPagtoGarantia] >= @AV55ContratoGarantia_DataPagtoGarantia1)";
            }
         }
         else
         {
            GXv_int5[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV54DynamicFiltersSelector1, "CONTRATOGARANTIA_DATAPAGTOGARANTIA") == 0 ) && ( ! (DateTime.MinValue==AV56ContratoGarantia_DataPagtoGarantia_To1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataPagtoGarantia] <= @AV56ContratoGarantia_DataPagtoGarantia_To1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataPagtoGarantia] <= @AV56ContratoGarantia_DataPagtoGarantia_To1)";
            }
         }
         else
         {
            GXv_int5[1] = 1;
         }
         if ( AV57DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV58DynamicFiltersSelector2, "CONTRATOGARANTIA_DATAPAGTOGARANTIA") == 0 ) && ( ! (DateTime.MinValue==AV59ContratoGarantia_DataPagtoGarantia2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataPagtoGarantia] >= @AV59ContratoGarantia_DataPagtoGarantia2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataPagtoGarantia] >= @AV59ContratoGarantia_DataPagtoGarantia2)";
            }
         }
         else
         {
            GXv_int5[2] = 1;
         }
         if ( AV57DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV58DynamicFiltersSelector2, "CONTRATOGARANTIA_DATAPAGTOGARANTIA") == 0 ) && ( ! (DateTime.MinValue==AV60ContratoGarantia_DataPagtoGarantia_To2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataPagtoGarantia] <= @AV60ContratoGarantia_DataPagtoGarantia_To2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataPagtoGarantia] <= @AV60ContratoGarantia_DataPagtoGarantia_To2)";
            }
         }
         else
         {
            GXv_int5[3] = 1;
         }
         if ( AV61DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV62DynamicFiltersSelector3, "CONTRATOGARANTIA_DATAPAGTOGARANTIA") == 0 ) && ( ! (DateTime.MinValue==AV63ContratoGarantia_DataPagtoGarantia3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataPagtoGarantia] >= @AV63ContratoGarantia_DataPagtoGarantia3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataPagtoGarantia] >= @AV63ContratoGarantia_DataPagtoGarantia3)";
            }
         }
         else
         {
            GXv_int5[4] = 1;
         }
         if ( AV61DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV62DynamicFiltersSelector3, "CONTRATOGARANTIA_DATAPAGTOGARANTIA") == 0 ) && ( ! (DateTime.MinValue==AV64ContratoGarantia_DataPagtoGarantia_To3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataPagtoGarantia] <= @AV64ContratoGarantia_DataPagtoGarantia_To3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataPagtoGarantia] <= @AV64ContratoGarantia_DataPagtoGarantia_To3)";
            }
         }
         else
         {
            GXv_int5[5] = 1;
         }
         if ( ! (0==AV10TFContratoGarantia_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_Codigo] >= @AV10TFContratoGarantia_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_Codigo] >= @AV10TFContratoGarantia_Codigo)";
            }
         }
         else
         {
            GXv_int5[6] = 1;
         }
         if ( ! (0==AV11TFContratoGarantia_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_Codigo] <= @AV11TFContratoGarantia_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_Codigo] <= @AV11TFContratoGarantia_Codigo_To)";
            }
         }
         else
         {
            GXv_int5[7] = 1;
         }
         if ( ! (0==AV12TFContrato_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Codigo] >= @AV12TFContrato_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Codigo] >= @AV12TFContrato_Codigo)";
            }
         }
         else
         {
            GXv_int5[8] = 1;
         }
         if ( ! (0==AV13TFContrato_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Contrato_Codigo] <= @AV13TFContrato_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Contrato_Codigo] <= @AV13TFContrato_Codigo_To)";
            }
         }
         else
         {
            GXv_int5[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFContrato_Numero_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFContrato_Numero)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] like @lV14TFContrato_Numero)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] like @lV14TFContrato_Numero)";
            }
         }
         else
         {
            GXv_int5[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFContrato_Numero_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] = @AV15TFContrato_Numero_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] = @AV15TFContrato_Numero_Sel)";
            }
         }
         else
         {
            GXv_int5[11] = 1;
         }
         if ( ! (0==AV16TFContratada_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contratada_Codigo] >= @AV16TFContratada_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contratada_Codigo] >= @AV16TFContratada_Codigo)";
            }
         }
         else
         {
            GXv_int5[12] = 1;
         }
         if ( ! (0==AV17TFContratada_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contratada_Codigo] <= @AV17TFContratada_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contratada_Codigo] <= @AV17TFContratada_Codigo_To)";
            }
         }
         else
         {
            GXv_int5[13] = 1;
         }
         if ( ! (0==AV18TFContratada_PessoaCod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Contratada_PessoaCod] >= @AV18TFContratada_PessoaCod)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Contratada_PessoaCod] >= @AV18TFContratada_PessoaCod)";
            }
         }
         else
         {
            GXv_int5[14] = 1;
         }
         if ( ! (0==AV19TFContratada_PessoaCod_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Contratada_PessoaCod] <= @AV19TFContratada_PessoaCod_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Contratada_PessoaCod] <= @AV19TFContratada_PessoaCod_To)";
            }
         }
         else
         {
            GXv_int5[15] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV21TFContratada_PessoaNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV20TFContratada_PessoaNom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like @lV20TFContratada_PessoaNom)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like @lV20TFContratada_PessoaNom)";
            }
         }
         else
         {
            GXv_int5[16] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV21TFContratada_PessoaNom_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] = @AV21TFContratada_PessoaNom_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] = @AV21TFContratada_PessoaNom_Sel)";
            }
         }
         else
         {
            GXv_int5[17] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV23TFContratada_PessoaCNPJ_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV22TFContratada_PessoaCNPJ)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Docto] like @lV22TFContratada_PessoaCNPJ)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Docto] like @lV22TFContratada_PessoaCNPJ)";
            }
         }
         else
         {
            GXv_int5[18] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23TFContratada_PessoaCNPJ_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Docto] = @AV23TFContratada_PessoaCNPJ_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Docto] = @AV23TFContratada_PessoaCNPJ_Sel)";
            }
         }
         else
         {
            GXv_int5[19] = 1;
         }
         if ( ! (DateTime.MinValue==AV24TFContratoGarantia_DataPagtoGarantia) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataPagtoGarantia] >= @AV24TFContratoGarantia_DataPagtoGarantia)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataPagtoGarantia] >= @AV24TFContratoGarantia_DataPagtoGarantia)";
            }
         }
         else
         {
            GXv_int5[20] = 1;
         }
         if ( ! (DateTime.MinValue==AV25TFContratoGarantia_DataPagtoGarantia_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataPagtoGarantia] <= @AV25TFContratoGarantia_DataPagtoGarantia_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataPagtoGarantia] <= @AV25TFContratoGarantia_DataPagtoGarantia_To)";
            }
         }
         else
         {
            GXv_int5[21] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV26TFContratoGarantia_Percentual) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_Percentual] >= @AV26TFContratoGarantia_Percentual)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_Percentual] >= @AV26TFContratoGarantia_Percentual)";
            }
         }
         else
         {
            GXv_int5[22] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV27TFContratoGarantia_Percentual_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_Percentual] <= @AV27TFContratoGarantia_Percentual_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_Percentual] <= @AV27TFContratoGarantia_Percentual_To)";
            }
         }
         else
         {
            GXv_int5[23] = 1;
         }
         if ( ! (DateTime.MinValue==AV28TFContratoGarantia_DataCaucao) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataCaucao] >= @AV28TFContratoGarantia_DataCaucao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataCaucao] >= @AV28TFContratoGarantia_DataCaucao)";
            }
         }
         else
         {
            GXv_int5[24] = 1;
         }
         if ( ! (DateTime.MinValue==AV29TFContratoGarantia_DataCaucao_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataCaucao] <= @AV29TFContratoGarantia_DataCaucao_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataCaucao] <= @AV29TFContratoGarantia_DataCaucao_To)";
            }
         }
         else
         {
            GXv_int5[25] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV30TFContratoGarantia_ValorGarantia) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_ValorGarantia] >= @AV30TFContratoGarantia_ValorGarantia)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_ValorGarantia] >= @AV30TFContratoGarantia_ValorGarantia)";
            }
         }
         else
         {
            GXv_int5[26] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV31TFContratoGarantia_ValorGarantia_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_ValorGarantia] <= @AV31TFContratoGarantia_ValorGarantia_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_ValorGarantia] <= @AV31TFContratoGarantia_ValorGarantia_To)";
            }
         }
         else
         {
            GXv_int5[27] = 1;
         }
         if ( ! (DateTime.MinValue==AV32TFContratoGarantia_DataEncerramento) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataEncerramento] >= @AV32TFContratoGarantia_DataEncerramento)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataEncerramento] >= @AV32TFContratoGarantia_DataEncerramento)";
            }
         }
         else
         {
            GXv_int5[28] = 1;
         }
         if ( ! (DateTime.MinValue==AV33TFContratoGarantia_DataEncerramento_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_DataEncerramento] <= @AV33TFContratoGarantia_DataEncerramento_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_DataEncerramento] <= @AV33TFContratoGarantia_DataEncerramento_To)";
            }
         }
         else
         {
            GXv_int5[29] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV34TFContratoGarantia_ValorEncerramento) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_ValorEncerramento] >= @AV34TFContratoGarantia_ValorEncerramento)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_ValorEncerramento] >= @AV34TFContratoGarantia_ValorEncerramento)";
            }
         }
         else
         {
            GXv_int5[30] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV35TFContratoGarantia_ValorEncerramento_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoGarantia_ValorEncerramento] <= @AV35TFContratoGarantia_ValorEncerramento_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoGarantia_ValorEncerramento] <= @AV35TFContratoGarantia_ValorEncerramento_To)";
            }
         }
         else
         {
            GXv_int5[31] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T4.[Pessoa_Docto]";
         GXv_Object6[0] = scmdbuf;
         GXv_Object6[1] = GXv_int5;
         return GXv_Object6 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00UI2(context, (String)dynConstraints[0] , (DateTime)dynConstraints[1] , (DateTime)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (DateTime)dynConstraints[5] , (DateTime)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (DateTime)dynConstraints[9] , (DateTime)dynConstraints[10] , (int)dynConstraints[11] , (int)dynConstraints[12] , (int)dynConstraints[13] , (int)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (int)dynConstraints[17] , (int)dynConstraints[18] , (int)dynConstraints[19] , (int)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (DateTime)dynConstraints[25] , (DateTime)dynConstraints[26] , (decimal)dynConstraints[27] , (decimal)dynConstraints[28] , (DateTime)dynConstraints[29] , (DateTime)dynConstraints[30] , (decimal)dynConstraints[31] , (decimal)dynConstraints[32] , (DateTime)dynConstraints[33] , (DateTime)dynConstraints[34] , (decimal)dynConstraints[35] , (decimal)dynConstraints[36] , (DateTime)dynConstraints[37] , (int)dynConstraints[38] , (int)dynConstraints[39] , (String)dynConstraints[40] , (int)dynConstraints[41] , (int)dynConstraints[42] , (String)dynConstraints[43] , (String)dynConstraints[44] , (decimal)dynConstraints[45] , (DateTime)dynConstraints[46] , (decimal)dynConstraints[47] , (DateTime)dynConstraints[48] , (decimal)dynConstraints[49] );
               case 1 :
                     return conditional_P00UI3(context, (String)dynConstraints[0] , (DateTime)dynConstraints[1] , (DateTime)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (DateTime)dynConstraints[5] , (DateTime)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (DateTime)dynConstraints[9] , (DateTime)dynConstraints[10] , (int)dynConstraints[11] , (int)dynConstraints[12] , (int)dynConstraints[13] , (int)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (int)dynConstraints[17] , (int)dynConstraints[18] , (int)dynConstraints[19] , (int)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (DateTime)dynConstraints[25] , (DateTime)dynConstraints[26] , (decimal)dynConstraints[27] , (decimal)dynConstraints[28] , (DateTime)dynConstraints[29] , (DateTime)dynConstraints[30] , (decimal)dynConstraints[31] , (decimal)dynConstraints[32] , (DateTime)dynConstraints[33] , (DateTime)dynConstraints[34] , (decimal)dynConstraints[35] , (decimal)dynConstraints[36] , (DateTime)dynConstraints[37] , (int)dynConstraints[38] , (int)dynConstraints[39] , (String)dynConstraints[40] , (int)dynConstraints[41] , (int)dynConstraints[42] , (String)dynConstraints[43] , (String)dynConstraints[44] , (decimal)dynConstraints[45] , (DateTime)dynConstraints[46] , (decimal)dynConstraints[47] , (DateTime)dynConstraints[48] , (decimal)dynConstraints[49] );
               case 2 :
                     return conditional_P00UI4(context, (String)dynConstraints[0] , (DateTime)dynConstraints[1] , (DateTime)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (DateTime)dynConstraints[5] , (DateTime)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (DateTime)dynConstraints[9] , (DateTime)dynConstraints[10] , (int)dynConstraints[11] , (int)dynConstraints[12] , (int)dynConstraints[13] , (int)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (int)dynConstraints[17] , (int)dynConstraints[18] , (int)dynConstraints[19] , (int)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (DateTime)dynConstraints[25] , (DateTime)dynConstraints[26] , (decimal)dynConstraints[27] , (decimal)dynConstraints[28] , (DateTime)dynConstraints[29] , (DateTime)dynConstraints[30] , (decimal)dynConstraints[31] , (decimal)dynConstraints[32] , (DateTime)dynConstraints[33] , (DateTime)dynConstraints[34] , (decimal)dynConstraints[35] , (decimal)dynConstraints[36] , (DateTime)dynConstraints[37] , (int)dynConstraints[38] , (int)dynConstraints[39] , (String)dynConstraints[40] , (int)dynConstraints[41] , (int)dynConstraints[42] , (String)dynConstraints[43] , (String)dynConstraints[44] , (decimal)dynConstraints[45] , (DateTime)dynConstraints[46] , (decimal)dynConstraints[47] , (DateTime)dynConstraints[48] , (decimal)dynConstraints[49] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00UI2 ;
          prmP00UI2 = new Object[] {
          new Object[] {"@AV55ContratoGarantia_DataPagtoGarantia1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV56ContratoGarantia_DataPagtoGarantia_To1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV59ContratoGarantia_DataPagtoGarantia2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV60ContratoGarantia_DataPagtoGarantia_To2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV63ContratoGarantia_DataPagtoGarantia3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV64ContratoGarantia_DataPagtoGarantia_To3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV10TFContratoGarantia_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV11TFContratoGarantia_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV12TFContrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV13TFContrato_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV14TFContrato_Numero",SqlDbType.Char,20,0} ,
          new Object[] {"@AV15TFContrato_Numero_Sel",SqlDbType.Char,20,0} ,
          new Object[] {"@AV16TFContratada_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV17TFContratada_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV18TFContratada_PessoaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV19TFContratada_PessoaCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV20TFContratada_PessoaNom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV21TFContratada_PessoaNom_Sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV22TFContratada_PessoaCNPJ",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV23TFContratada_PessoaCNPJ_Sel",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV24TFContratoGarantia_DataPagtoGarantia",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV25TFContratoGarantia_DataPagtoGarantia_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV26TFContratoGarantia_Percentual",SqlDbType.Decimal,6,2} ,
          new Object[] {"@AV27TFContratoGarantia_Percentual_To",SqlDbType.Decimal,6,2} ,
          new Object[] {"@AV28TFContratoGarantia_DataCaucao",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV29TFContratoGarantia_DataCaucao_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV30TFContratoGarantia_ValorGarantia",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV31TFContratoGarantia_ValorGarantia_To",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV32TFContratoGarantia_DataEncerramento",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV33TFContratoGarantia_DataEncerramento_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV34TFContratoGarantia_ValorEncerramento",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV35TFContratoGarantia_ValorEncerramento_To",SqlDbType.Decimal,18,5}
          } ;
          Object[] prmP00UI3 ;
          prmP00UI3 = new Object[] {
          new Object[] {"@AV55ContratoGarantia_DataPagtoGarantia1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV56ContratoGarantia_DataPagtoGarantia_To1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV59ContratoGarantia_DataPagtoGarantia2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV60ContratoGarantia_DataPagtoGarantia_To2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV63ContratoGarantia_DataPagtoGarantia3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV64ContratoGarantia_DataPagtoGarantia_To3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV10TFContratoGarantia_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV11TFContratoGarantia_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV12TFContrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV13TFContrato_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV14TFContrato_Numero",SqlDbType.Char,20,0} ,
          new Object[] {"@AV15TFContrato_Numero_Sel",SqlDbType.Char,20,0} ,
          new Object[] {"@AV16TFContratada_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV17TFContratada_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV18TFContratada_PessoaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV19TFContratada_PessoaCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV20TFContratada_PessoaNom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV21TFContratada_PessoaNom_Sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV22TFContratada_PessoaCNPJ",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV23TFContratada_PessoaCNPJ_Sel",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV24TFContratoGarantia_DataPagtoGarantia",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV25TFContratoGarantia_DataPagtoGarantia_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV26TFContratoGarantia_Percentual",SqlDbType.Decimal,6,2} ,
          new Object[] {"@AV27TFContratoGarantia_Percentual_To",SqlDbType.Decimal,6,2} ,
          new Object[] {"@AV28TFContratoGarantia_DataCaucao",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV29TFContratoGarantia_DataCaucao_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV30TFContratoGarantia_ValorGarantia",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV31TFContratoGarantia_ValorGarantia_To",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV32TFContratoGarantia_DataEncerramento",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV33TFContratoGarantia_DataEncerramento_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV34TFContratoGarantia_ValorEncerramento",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV35TFContratoGarantia_ValorEncerramento_To",SqlDbType.Decimal,18,5}
          } ;
          Object[] prmP00UI4 ;
          prmP00UI4 = new Object[] {
          new Object[] {"@AV55ContratoGarantia_DataPagtoGarantia1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV56ContratoGarantia_DataPagtoGarantia_To1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV59ContratoGarantia_DataPagtoGarantia2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV60ContratoGarantia_DataPagtoGarantia_To2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV63ContratoGarantia_DataPagtoGarantia3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV64ContratoGarantia_DataPagtoGarantia_To3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV10TFContratoGarantia_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV11TFContratoGarantia_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV12TFContrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV13TFContrato_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV14TFContrato_Numero",SqlDbType.Char,20,0} ,
          new Object[] {"@AV15TFContrato_Numero_Sel",SqlDbType.Char,20,0} ,
          new Object[] {"@AV16TFContratada_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV17TFContratada_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV18TFContratada_PessoaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV19TFContratada_PessoaCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV20TFContratada_PessoaNom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV21TFContratada_PessoaNom_Sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV22TFContratada_PessoaCNPJ",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV23TFContratada_PessoaCNPJ_Sel",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV24TFContratoGarantia_DataPagtoGarantia",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV25TFContratoGarantia_DataPagtoGarantia_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV26TFContratoGarantia_Percentual",SqlDbType.Decimal,6,2} ,
          new Object[] {"@AV27TFContratoGarantia_Percentual_To",SqlDbType.Decimal,6,2} ,
          new Object[] {"@AV28TFContratoGarantia_DataCaucao",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV29TFContratoGarantia_DataCaucao_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV30TFContratoGarantia_ValorGarantia",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV31TFContratoGarantia_ValorGarantia_To",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV32TFContratoGarantia_DataEncerramento",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV33TFContratoGarantia_DataEncerramento_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV34TFContratoGarantia_ValorEncerramento",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV35TFContratoGarantia_ValorEncerramento_To",SqlDbType.Decimal,18,5}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00UI2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00UI2,100,0,true,false )
             ,new CursorDef("P00UI3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00UI3,100,0,true,false )
             ,new CursorDef("P00UI4", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00UI4,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                ((decimal[]) buf[1])[0] = rslt.getDecimal(2) ;
                ((DateTime[]) buf[2])[0] = rslt.getGXDate(3) ;
                ((decimal[]) buf[3])[0] = rslt.getDecimal(4) ;
                ((DateTime[]) buf[4])[0] = rslt.getGXDate(5) ;
                ((decimal[]) buf[5])[0] = rslt.getDecimal(6) ;
                ((String[]) buf[6])[0] = rslt.getVarchar(7) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(7);
                ((String[]) buf[8])[0] = rslt.getString(8, 100) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(8);
                ((int[]) buf[10])[0] = rslt.getInt(9) ;
                ((int[]) buf[11])[0] = rslt.getInt(10) ;
                ((int[]) buf[12])[0] = rslt.getInt(11) ;
                ((int[]) buf[13])[0] = rslt.getInt(12) ;
                ((DateTime[]) buf[14])[0] = rslt.getGXDate(13) ;
                return;
             case 1 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((decimal[]) buf[2])[0] = rslt.getDecimal(2) ;
                ((DateTime[]) buf[3])[0] = rslt.getGXDate(3) ;
                ((decimal[]) buf[4])[0] = rslt.getDecimal(4) ;
                ((DateTime[]) buf[5])[0] = rslt.getGXDate(5) ;
                ((decimal[]) buf[6])[0] = rslt.getDecimal(6) ;
                ((String[]) buf[7])[0] = rslt.getVarchar(7) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(7);
                ((int[]) buf[9])[0] = rslt.getInt(8) ;
                ((int[]) buf[10])[0] = rslt.getInt(9) ;
                ((String[]) buf[11])[0] = rslt.getString(10, 20) ;
                ((int[]) buf[12])[0] = rslt.getInt(11) ;
                ((int[]) buf[13])[0] = rslt.getInt(12) ;
                ((DateTime[]) buf[14])[0] = rslt.getGXDate(13) ;
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((decimal[]) buf[2])[0] = rslt.getDecimal(2) ;
                ((DateTime[]) buf[3])[0] = rslt.getGXDate(3) ;
                ((decimal[]) buf[4])[0] = rslt.getDecimal(4) ;
                ((DateTime[]) buf[5])[0] = rslt.getGXDate(5) ;
                ((decimal[]) buf[6])[0] = rslt.getDecimal(6) ;
                ((String[]) buf[7])[0] = rslt.getString(7, 100) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(7);
                ((int[]) buf[9])[0] = rslt.getInt(8) ;
                ((int[]) buf[10])[0] = rslt.getInt(9) ;
                ((String[]) buf[11])[0] = rslt.getString(10, 20) ;
                ((int[]) buf[12])[0] = rslt.getInt(11) ;
                ((int[]) buf[13])[0] = rslt.getInt(12) ;
                ((DateTime[]) buf[14])[0] = rslt.getGXDate(13) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[32]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[33]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[34]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[35]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[36]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[37]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[38]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[39]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[40]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[41]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[44]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[45]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[46]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[47]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[48]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[49]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[50]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[51]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[52]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[53]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[54]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[55]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[56]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[57]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[58]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[59]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[60]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[61]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[62]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[63]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[32]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[33]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[34]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[35]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[36]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[37]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[38]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[39]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[40]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[41]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[44]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[45]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[46]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[47]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[48]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[49]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[50]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[51]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[52]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[53]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[54]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[55]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[56]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[57]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[58]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[59]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[60]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[61]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[62]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[63]);
                }
                return;
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[32]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[33]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[34]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[35]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[36]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[37]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[38]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[39]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[40]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[41]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[44]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[45]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[46]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[47]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[48]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[49]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[50]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[51]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[52]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[53]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[54]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[55]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[56]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[57]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[58]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[59]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[60]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[61]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[62]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[63]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getpromptcontratogarantiafilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getpromptcontratogarantiafilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getpromptcontratogarantiafilterdata") )
          {
             return  ;
          }
          getpromptcontratogarantiafilterdata worker = new getpromptcontratogarantiafilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
