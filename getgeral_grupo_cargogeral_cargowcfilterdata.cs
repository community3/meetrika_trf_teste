/*
               File: GetGeral_Grupo_CargoGeral_CargoWCFilterData
        Description: Get Geral_Grupo_Cargo Geral_Cargo WCFilter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/21/2020 18:4:48.62
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getgeral_grupo_cargogeral_cargowcfilterdata : GXProcedure
   {
      public getgeral_grupo_cargogeral_cargowcfilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getgeral_grupo_cargogeral_cargowcfilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV16DDOName = aP0_DDOName;
         this.AV14SearchTxt = aP1_SearchTxt;
         this.AV15SearchTxtTo = aP2_SearchTxtTo;
         this.AV20OptionsJson = "" ;
         this.AV23OptionsDescJson = "" ;
         this.AV25OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV20OptionsJson;
         aP4_OptionsDescJson=this.AV23OptionsDescJson;
         aP5_OptionIndexesJson=this.AV25OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV16DDOName = aP0_DDOName;
         this.AV14SearchTxt = aP1_SearchTxt;
         this.AV15SearchTxtTo = aP2_SearchTxtTo;
         this.AV20OptionsJson = "" ;
         this.AV23OptionsDescJson = "" ;
         this.AV25OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV20OptionsJson;
         aP4_OptionsDescJson=this.AV23OptionsDescJson;
         aP5_OptionIndexesJson=this.AV25OptionIndexesJson;
         return AV25OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getgeral_grupo_cargogeral_cargowcfilterdata objgetgeral_grupo_cargogeral_cargowcfilterdata;
         objgetgeral_grupo_cargogeral_cargowcfilterdata = new getgeral_grupo_cargogeral_cargowcfilterdata();
         objgetgeral_grupo_cargogeral_cargowcfilterdata.AV16DDOName = aP0_DDOName;
         objgetgeral_grupo_cargogeral_cargowcfilterdata.AV14SearchTxt = aP1_SearchTxt;
         objgetgeral_grupo_cargogeral_cargowcfilterdata.AV15SearchTxtTo = aP2_SearchTxtTo;
         objgetgeral_grupo_cargogeral_cargowcfilterdata.AV20OptionsJson = "" ;
         objgetgeral_grupo_cargogeral_cargowcfilterdata.AV23OptionsDescJson = "" ;
         objgetgeral_grupo_cargogeral_cargowcfilterdata.AV25OptionIndexesJson = "" ;
         objgetgeral_grupo_cargogeral_cargowcfilterdata.context.SetSubmitInitialConfig(context);
         objgetgeral_grupo_cargogeral_cargowcfilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetgeral_grupo_cargogeral_cargowcfilterdata);
         aP3_OptionsJson=this.AV20OptionsJson;
         aP4_OptionsDescJson=this.AV23OptionsDescJson;
         aP5_OptionIndexesJson=this.AV25OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getgeral_grupo_cargogeral_cargowcfilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV19Options = (IGxCollection)(new GxSimpleCollection());
         AV22OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV24OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV16DDOName), "DDO_CARGO_NOME") == 0 )
         {
            /* Execute user subroutine: 'LOADCARGO_NOMEOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV16DDOName), "DDO_CARGO_UONOM") == 0 )
         {
            /* Execute user subroutine: 'LOADCARGO_UONOMOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV20OptionsJson = AV19Options.ToJSonString(false);
         AV23OptionsDescJson = AV22OptionsDesc.ToJSonString(false);
         AV25OptionIndexesJson = AV24OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV27Session.Get("Geral_Grupo_CargoGeral_CargoWCGridState"), "") == 0 )
         {
            AV29GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "Geral_Grupo_CargoGeral_CargoWCGridState"), "");
         }
         else
         {
            AV29GridState.FromXml(AV27Session.Get("Geral_Grupo_CargoGeral_CargoWCGridState"), "");
         }
         AV47GXV1 = 1;
         while ( AV47GXV1 <= AV29GridState.gxTpr_Filtervalues.Count )
         {
            AV30GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV29GridState.gxTpr_Filtervalues.Item(AV47GXV1));
            if ( StringUtil.StrCmp(AV30GridStateFilterValue.gxTpr_Name, "CARGO_ATIVO") == 0 )
            {
               AV32Cargo_Ativo = BooleanUtil.Val( AV30GridStateFilterValue.gxTpr_Value);
            }
            else if ( StringUtil.StrCmp(AV30GridStateFilterValue.gxTpr_Name, "TFCARGO_NOME") == 0 )
            {
               AV10TFCargo_Nome = AV30GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV30GridStateFilterValue.gxTpr_Name, "TFCARGO_NOME_SEL") == 0 )
            {
               AV11TFCargo_Nome_Sel = AV30GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV30GridStateFilterValue.gxTpr_Name, "TFCARGO_UONOM") == 0 )
            {
               AV12TFCargo_UONom = AV30GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV30GridStateFilterValue.gxTpr_Name, "TFCARGO_UONOM_SEL") == 0 )
            {
               AV13TFCargo_UONom_Sel = AV30GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV30GridStateFilterValue.gxTpr_Name, "PARM_&GRUPOCARGO_CODIGO") == 0 )
            {
               AV44GrupoCargo_Codigo = (int)(NumberUtil.Val( AV30GridStateFilterValue.gxTpr_Value, "."));
            }
            AV47GXV1 = (int)(AV47GXV1+1);
         }
         if ( AV29GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV31GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV29GridState.gxTpr_Dynamicfilters.Item(1));
            AV33DynamicFiltersSelector1 = AV31GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV33DynamicFiltersSelector1, "CARGO_NOME") == 0 )
            {
               AV34Cargo_Nome1 = AV31GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV33DynamicFiltersSelector1, "CARGO_UONOM") == 0 )
            {
               AV35Cargo_UONom1 = AV31GridStateDynamicFilter.gxTpr_Value;
            }
            if ( AV29GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV36DynamicFiltersEnabled2 = true;
               AV31GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV29GridState.gxTpr_Dynamicfilters.Item(2));
               AV37DynamicFiltersSelector2 = AV31GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV37DynamicFiltersSelector2, "CARGO_NOME") == 0 )
               {
                  AV38Cargo_Nome2 = AV31GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV37DynamicFiltersSelector2, "CARGO_UONOM") == 0 )
               {
                  AV39Cargo_UONom2 = AV31GridStateDynamicFilter.gxTpr_Value;
               }
               if ( AV29GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV40DynamicFiltersEnabled3 = true;
                  AV31GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV29GridState.gxTpr_Dynamicfilters.Item(3));
                  AV41DynamicFiltersSelector3 = AV31GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV41DynamicFiltersSelector3, "CARGO_NOME") == 0 )
                  {
                     AV42Cargo_Nome3 = AV31GridStateDynamicFilter.gxTpr_Value;
                  }
                  else if ( StringUtil.StrCmp(AV41DynamicFiltersSelector3, "CARGO_UONOM") == 0 )
                  {
                     AV43Cargo_UONom3 = AV31GridStateDynamicFilter.gxTpr_Value;
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADCARGO_NOMEOPTIONS' Routine */
         AV10TFCargo_Nome = AV14SearchTxt;
         AV11TFCargo_Nome_Sel = "";
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV33DynamicFiltersSelector1 ,
                                              AV34Cargo_Nome1 ,
                                              AV35Cargo_UONom1 ,
                                              AV36DynamicFiltersEnabled2 ,
                                              AV37DynamicFiltersSelector2 ,
                                              AV38Cargo_Nome2 ,
                                              AV39Cargo_UONom2 ,
                                              AV40DynamicFiltersEnabled3 ,
                                              AV41DynamicFiltersSelector3 ,
                                              AV42Cargo_Nome3 ,
                                              AV43Cargo_UONom3 ,
                                              AV11TFCargo_Nome_Sel ,
                                              AV10TFCargo_Nome ,
                                              AV13TFCargo_UONom_Sel ,
                                              AV12TFCargo_UONom ,
                                              A618Cargo_Nome ,
                                              A1003Cargo_UONom ,
                                              A628Cargo_Ativo ,
                                              AV44GrupoCargo_Codigo ,
                                              A615GrupoCargo_Codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.INT,
                                              TypeConstants.INT
                                              }
         });
         lV34Cargo_Nome1 = StringUtil.Concat( StringUtil.RTrim( AV34Cargo_Nome1), "%", "");
         lV35Cargo_UONom1 = StringUtil.PadR( StringUtil.RTrim( AV35Cargo_UONom1), 50, "%");
         lV38Cargo_Nome2 = StringUtil.Concat( StringUtil.RTrim( AV38Cargo_Nome2), "%", "");
         lV39Cargo_UONom2 = StringUtil.PadR( StringUtil.RTrim( AV39Cargo_UONom2), 50, "%");
         lV42Cargo_Nome3 = StringUtil.Concat( StringUtil.RTrim( AV42Cargo_Nome3), "%", "");
         lV43Cargo_UONom3 = StringUtil.PadR( StringUtil.RTrim( AV43Cargo_UONom3), 50, "%");
         lV10TFCargo_Nome = StringUtil.Concat( StringUtil.RTrim( AV10TFCargo_Nome), "%", "");
         lV12TFCargo_UONom = StringUtil.PadR( StringUtil.RTrim( AV12TFCargo_UONom), 50, "%");
         /* Using cursor P00NE2 */
         pr_default.execute(0, new Object[] {AV44GrupoCargo_Codigo, lV34Cargo_Nome1, lV35Cargo_UONom1, lV38Cargo_Nome2, lV39Cargo_UONom2, lV42Cargo_Nome3, lV43Cargo_UONom3, lV10TFCargo_Nome, AV11TFCargo_Nome_Sel, lV12TFCargo_UONom, AV13TFCargo_UONom_Sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKNE2 = false;
            A1002Cargo_UOCod = P00NE2_A1002Cargo_UOCod[0];
            n1002Cargo_UOCod = P00NE2_n1002Cargo_UOCod[0];
            A615GrupoCargo_Codigo = P00NE2_A615GrupoCargo_Codigo[0];
            A628Cargo_Ativo = P00NE2_A628Cargo_Ativo[0];
            A618Cargo_Nome = P00NE2_A618Cargo_Nome[0];
            A1003Cargo_UONom = P00NE2_A1003Cargo_UONom[0];
            n1003Cargo_UONom = P00NE2_n1003Cargo_UONom[0];
            A617Cargo_Codigo = P00NE2_A617Cargo_Codigo[0];
            A1003Cargo_UONom = P00NE2_A1003Cargo_UONom[0];
            n1003Cargo_UONom = P00NE2_n1003Cargo_UONom[0];
            AV26count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( P00NE2_A615GrupoCargo_Codigo[0] == A615GrupoCargo_Codigo ) && ( StringUtil.StrCmp(P00NE2_A618Cargo_Nome[0], A618Cargo_Nome) == 0 ) )
            {
               BRKNE2 = false;
               A617Cargo_Codigo = P00NE2_A617Cargo_Codigo[0];
               AV26count = (long)(AV26count+1);
               BRKNE2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A618Cargo_Nome)) )
            {
               AV18Option = A618Cargo_Nome;
               AV21OptionDesc = StringUtil.Trim( StringUtil.RTrim( context.localUtil.Format( A618Cargo_Nome, "@!")));
               AV19Options.Add(AV18Option, 0);
               AV22OptionsDesc.Add(AV21OptionDesc, 0);
               AV24OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV26count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV19Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKNE2 )
            {
               BRKNE2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADCARGO_UONOMOPTIONS' Routine */
         AV12TFCargo_UONom = AV14SearchTxt;
         AV13TFCargo_UONom_Sel = "";
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV33DynamicFiltersSelector1 ,
                                              AV34Cargo_Nome1 ,
                                              AV35Cargo_UONom1 ,
                                              AV36DynamicFiltersEnabled2 ,
                                              AV37DynamicFiltersSelector2 ,
                                              AV38Cargo_Nome2 ,
                                              AV39Cargo_UONom2 ,
                                              AV40DynamicFiltersEnabled3 ,
                                              AV41DynamicFiltersSelector3 ,
                                              AV42Cargo_Nome3 ,
                                              AV43Cargo_UONom3 ,
                                              AV11TFCargo_Nome_Sel ,
                                              AV10TFCargo_Nome ,
                                              AV13TFCargo_UONom_Sel ,
                                              AV12TFCargo_UONom ,
                                              A618Cargo_Nome ,
                                              A1003Cargo_UONom ,
                                              A628Cargo_Ativo ,
                                              AV44GrupoCargo_Codigo ,
                                              A615GrupoCargo_Codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.INT,
                                              TypeConstants.INT
                                              }
         });
         lV34Cargo_Nome1 = StringUtil.Concat( StringUtil.RTrim( AV34Cargo_Nome1), "%", "");
         lV35Cargo_UONom1 = StringUtil.PadR( StringUtil.RTrim( AV35Cargo_UONom1), 50, "%");
         lV38Cargo_Nome2 = StringUtil.Concat( StringUtil.RTrim( AV38Cargo_Nome2), "%", "");
         lV39Cargo_UONom2 = StringUtil.PadR( StringUtil.RTrim( AV39Cargo_UONom2), 50, "%");
         lV42Cargo_Nome3 = StringUtil.Concat( StringUtil.RTrim( AV42Cargo_Nome3), "%", "");
         lV43Cargo_UONom3 = StringUtil.PadR( StringUtil.RTrim( AV43Cargo_UONom3), 50, "%");
         lV10TFCargo_Nome = StringUtil.Concat( StringUtil.RTrim( AV10TFCargo_Nome), "%", "");
         lV12TFCargo_UONom = StringUtil.PadR( StringUtil.RTrim( AV12TFCargo_UONom), 50, "%");
         /* Using cursor P00NE3 */
         pr_default.execute(1, new Object[] {AV44GrupoCargo_Codigo, lV34Cargo_Nome1, lV35Cargo_UONom1, lV38Cargo_Nome2, lV39Cargo_UONom2, lV42Cargo_Nome3, lV43Cargo_UONom3, lV10TFCargo_Nome, AV11TFCargo_Nome_Sel, lV12TFCargo_UONom, AV13TFCargo_UONom_Sel});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKNE4 = false;
            A615GrupoCargo_Codigo = P00NE3_A615GrupoCargo_Codigo[0];
            A628Cargo_Ativo = P00NE3_A628Cargo_Ativo[0];
            A1002Cargo_UOCod = P00NE3_A1002Cargo_UOCod[0];
            n1002Cargo_UOCod = P00NE3_n1002Cargo_UOCod[0];
            A1003Cargo_UONom = P00NE3_A1003Cargo_UONom[0];
            n1003Cargo_UONom = P00NE3_n1003Cargo_UONom[0];
            A618Cargo_Nome = P00NE3_A618Cargo_Nome[0];
            A617Cargo_Codigo = P00NE3_A617Cargo_Codigo[0];
            A1003Cargo_UONom = P00NE3_A1003Cargo_UONom[0];
            n1003Cargo_UONom = P00NE3_n1003Cargo_UONom[0];
            AV26count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( P00NE3_A615GrupoCargo_Codigo[0] == A615GrupoCargo_Codigo ) && ( P00NE3_A1002Cargo_UOCod[0] == A1002Cargo_UOCod ) )
            {
               BRKNE4 = false;
               A617Cargo_Codigo = P00NE3_A617Cargo_Codigo[0];
               AV26count = (long)(AV26count+1);
               BRKNE4 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A1003Cargo_UONom)) )
            {
               AV18Option = A1003Cargo_UONom;
               AV17InsertIndex = 1;
               while ( ( AV17InsertIndex <= AV19Options.Count ) && ( StringUtil.StrCmp(((String)AV19Options.Item(AV17InsertIndex)), AV18Option) < 0 ) )
               {
                  AV17InsertIndex = (int)(AV17InsertIndex+1);
               }
               AV19Options.Add(AV18Option, AV17InsertIndex);
               AV24OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV26count), "Z,ZZZ,ZZZ,ZZ9")), AV17InsertIndex);
            }
            if ( AV19Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKNE4 )
            {
               BRKNE4 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV19Options = new GxSimpleCollection();
         AV22OptionsDesc = new GxSimpleCollection();
         AV24OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV27Session = context.GetSession();
         AV29GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV30GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV32Cargo_Ativo = true;
         AV10TFCargo_Nome = "";
         AV11TFCargo_Nome_Sel = "";
         AV12TFCargo_UONom = "";
         AV13TFCargo_UONom_Sel = "";
         AV31GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV33DynamicFiltersSelector1 = "";
         AV34Cargo_Nome1 = "";
         AV35Cargo_UONom1 = "";
         AV37DynamicFiltersSelector2 = "";
         AV38Cargo_Nome2 = "";
         AV39Cargo_UONom2 = "";
         AV41DynamicFiltersSelector3 = "";
         AV42Cargo_Nome3 = "";
         AV43Cargo_UONom3 = "";
         scmdbuf = "";
         lV34Cargo_Nome1 = "";
         lV35Cargo_UONom1 = "";
         lV38Cargo_Nome2 = "";
         lV39Cargo_UONom2 = "";
         lV42Cargo_Nome3 = "";
         lV43Cargo_UONom3 = "";
         lV10TFCargo_Nome = "";
         lV12TFCargo_UONom = "";
         A618Cargo_Nome = "";
         A1003Cargo_UONom = "";
         P00NE2_A1002Cargo_UOCod = new int[1] ;
         P00NE2_n1002Cargo_UOCod = new bool[] {false} ;
         P00NE2_A615GrupoCargo_Codigo = new int[1] ;
         P00NE2_A628Cargo_Ativo = new bool[] {false} ;
         P00NE2_A618Cargo_Nome = new String[] {""} ;
         P00NE2_A1003Cargo_UONom = new String[] {""} ;
         P00NE2_n1003Cargo_UONom = new bool[] {false} ;
         P00NE2_A617Cargo_Codigo = new int[1] ;
         AV18Option = "";
         AV21OptionDesc = "";
         P00NE3_A615GrupoCargo_Codigo = new int[1] ;
         P00NE3_A628Cargo_Ativo = new bool[] {false} ;
         P00NE3_A1002Cargo_UOCod = new int[1] ;
         P00NE3_n1002Cargo_UOCod = new bool[] {false} ;
         P00NE3_A1003Cargo_UONom = new String[] {""} ;
         P00NE3_n1003Cargo_UONom = new bool[] {false} ;
         P00NE3_A618Cargo_Nome = new String[] {""} ;
         P00NE3_A617Cargo_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getgeral_grupo_cargogeral_cargowcfilterdata__default(),
            new Object[][] {
                new Object[] {
               P00NE2_A1002Cargo_UOCod, P00NE2_n1002Cargo_UOCod, P00NE2_A615GrupoCargo_Codigo, P00NE2_A628Cargo_Ativo, P00NE2_A618Cargo_Nome, P00NE2_A1003Cargo_UONom, P00NE2_n1003Cargo_UONom, P00NE2_A617Cargo_Codigo
               }
               , new Object[] {
               P00NE3_A615GrupoCargo_Codigo, P00NE3_A628Cargo_Ativo, P00NE3_A1002Cargo_UOCod, P00NE3_n1002Cargo_UOCod, P00NE3_A1003Cargo_UONom, P00NE3_n1003Cargo_UONom, P00NE3_A618Cargo_Nome, P00NE3_A617Cargo_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV47GXV1 ;
      private int AV44GrupoCargo_Codigo ;
      private int A615GrupoCargo_Codigo ;
      private int A1002Cargo_UOCod ;
      private int A617Cargo_Codigo ;
      private int AV17InsertIndex ;
      private long AV26count ;
      private String AV12TFCargo_UONom ;
      private String AV13TFCargo_UONom_Sel ;
      private String AV35Cargo_UONom1 ;
      private String AV39Cargo_UONom2 ;
      private String AV43Cargo_UONom3 ;
      private String scmdbuf ;
      private String lV35Cargo_UONom1 ;
      private String lV39Cargo_UONom2 ;
      private String lV43Cargo_UONom3 ;
      private String lV12TFCargo_UONom ;
      private String A1003Cargo_UONom ;
      private bool returnInSub ;
      private bool AV32Cargo_Ativo ;
      private bool AV36DynamicFiltersEnabled2 ;
      private bool AV40DynamicFiltersEnabled3 ;
      private bool A628Cargo_Ativo ;
      private bool BRKNE2 ;
      private bool n1002Cargo_UOCod ;
      private bool n1003Cargo_UONom ;
      private bool BRKNE4 ;
      private String AV25OptionIndexesJson ;
      private String AV20OptionsJson ;
      private String AV23OptionsDescJson ;
      private String AV16DDOName ;
      private String AV14SearchTxt ;
      private String AV15SearchTxtTo ;
      private String AV10TFCargo_Nome ;
      private String AV11TFCargo_Nome_Sel ;
      private String AV33DynamicFiltersSelector1 ;
      private String AV34Cargo_Nome1 ;
      private String AV37DynamicFiltersSelector2 ;
      private String AV38Cargo_Nome2 ;
      private String AV41DynamicFiltersSelector3 ;
      private String AV42Cargo_Nome3 ;
      private String lV34Cargo_Nome1 ;
      private String lV38Cargo_Nome2 ;
      private String lV42Cargo_Nome3 ;
      private String lV10TFCargo_Nome ;
      private String A618Cargo_Nome ;
      private String AV18Option ;
      private String AV21OptionDesc ;
      private IGxSession AV27Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00NE2_A1002Cargo_UOCod ;
      private bool[] P00NE2_n1002Cargo_UOCod ;
      private int[] P00NE2_A615GrupoCargo_Codigo ;
      private bool[] P00NE2_A628Cargo_Ativo ;
      private String[] P00NE2_A618Cargo_Nome ;
      private String[] P00NE2_A1003Cargo_UONom ;
      private bool[] P00NE2_n1003Cargo_UONom ;
      private int[] P00NE2_A617Cargo_Codigo ;
      private int[] P00NE3_A615GrupoCargo_Codigo ;
      private bool[] P00NE3_A628Cargo_Ativo ;
      private int[] P00NE3_A1002Cargo_UOCod ;
      private bool[] P00NE3_n1002Cargo_UOCod ;
      private String[] P00NE3_A1003Cargo_UONom ;
      private bool[] P00NE3_n1003Cargo_UONom ;
      private String[] P00NE3_A618Cargo_Nome ;
      private int[] P00NE3_A617Cargo_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV19Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV22OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV24OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV29GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV30GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV31GridStateDynamicFilter ;
   }

   public class getgeral_grupo_cargogeral_cargowcfilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00NE2( IGxContext context ,
                                             String AV33DynamicFiltersSelector1 ,
                                             String AV34Cargo_Nome1 ,
                                             String AV35Cargo_UONom1 ,
                                             bool AV36DynamicFiltersEnabled2 ,
                                             String AV37DynamicFiltersSelector2 ,
                                             String AV38Cargo_Nome2 ,
                                             String AV39Cargo_UONom2 ,
                                             bool AV40DynamicFiltersEnabled3 ,
                                             String AV41DynamicFiltersSelector3 ,
                                             String AV42Cargo_Nome3 ,
                                             String AV43Cargo_UONom3 ,
                                             String AV11TFCargo_Nome_Sel ,
                                             String AV10TFCargo_Nome ,
                                             String AV13TFCargo_UONom_Sel ,
                                             String AV12TFCargo_UONom ,
                                             String A618Cargo_Nome ,
                                             String A1003Cargo_UONom ,
                                             bool A628Cargo_Ativo ,
                                             int AV44GrupoCargo_Codigo ,
                                             int A615GrupoCargo_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [11] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[Cargo_UOCod] AS Cargo_UOCod, T1.[GrupoCargo_Codigo], T1.[Cargo_Ativo], T1.[Cargo_Nome], T2.[UnidadeOrganizacional_Nome] AS Cargo_UONom, T1.[Cargo_Codigo] FROM ([Geral_Cargo] T1 WITH (NOLOCK) LEFT JOIN [Geral_UnidadeOrganizacional] T2 WITH (NOLOCK) ON T2.[UnidadeOrganizacional_Codigo] = T1.[Cargo_UOCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[GrupoCargo_Codigo] = @AV44GrupoCargo_Codigo)";
         scmdbuf = scmdbuf + " and (T1.[Cargo_Ativo] = 1)";
         if ( ( StringUtil.StrCmp(AV33DynamicFiltersSelector1, "CARGO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV34Cargo_Nome1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Cargo_Nome] like '%' + @lV34Cargo_Nome1 + '%')";
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV33DynamicFiltersSelector1, "CARGO_UONOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV35Cargo_UONom1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[UnidadeOrganizacional_Nome] like @lV35Cargo_UONom1 + '%')";
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( AV36DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV37DynamicFiltersSelector2, "CARGO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV38Cargo_Nome2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Cargo_Nome] like '%' + @lV38Cargo_Nome2 + '%')";
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( AV36DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV37DynamicFiltersSelector2, "CARGO_UONOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV39Cargo_UONom2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[UnidadeOrganizacional_Nome] like @lV39Cargo_UONom2 + '%')";
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( AV40DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV41DynamicFiltersSelector3, "CARGO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42Cargo_Nome3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Cargo_Nome] like '%' + @lV42Cargo_Nome3 + '%')";
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( AV40DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV41DynamicFiltersSelector3, "CARGO_UONOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV43Cargo_UONom3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[UnidadeOrganizacional_Nome] like @lV43Cargo_UONom3 + '%')";
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFCargo_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFCargo_Nome)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Cargo_Nome] like @lV10TFCargo_Nome)";
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFCargo_Nome_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Cargo_Nome] = @AV11TFCargo_Nome_Sel)";
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFCargo_UONom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFCargo_UONom)) ) )
         {
            sWhereString = sWhereString + " and (T2.[UnidadeOrganizacional_Nome] like @lV12TFCargo_UONom)";
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFCargo_UONom_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[UnidadeOrganizacional_Nome] = @AV13TFCargo_UONom_Sel)";
         }
         else
         {
            GXv_int1[10] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[GrupoCargo_Codigo], T1.[Cargo_Nome]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00NE3( IGxContext context ,
                                             String AV33DynamicFiltersSelector1 ,
                                             String AV34Cargo_Nome1 ,
                                             String AV35Cargo_UONom1 ,
                                             bool AV36DynamicFiltersEnabled2 ,
                                             String AV37DynamicFiltersSelector2 ,
                                             String AV38Cargo_Nome2 ,
                                             String AV39Cargo_UONom2 ,
                                             bool AV40DynamicFiltersEnabled3 ,
                                             String AV41DynamicFiltersSelector3 ,
                                             String AV42Cargo_Nome3 ,
                                             String AV43Cargo_UONom3 ,
                                             String AV11TFCargo_Nome_Sel ,
                                             String AV10TFCargo_Nome ,
                                             String AV13TFCargo_UONom_Sel ,
                                             String AV12TFCargo_UONom ,
                                             String A618Cargo_Nome ,
                                             String A1003Cargo_UONom ,
                                             bool A628Cargo_Ativo ,
                                             int AV44GrupoCargo_Codigo ,
                                             int A615GrupoCargo_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [11] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT T1.[GrupoCargo_Codigo], T1.[Cargo_Ativo], T1.[Cargo_UOCod] AS Cargo_UOCod, T2.[UnidadeOrganizacional_Nome] AS Cargo_UONom, T1.[Cargo_Nome], T1.[Cargo_Codigo] FROM ([Geral_Cargo] T1 WITH (NOLOCK) LEFT JOIN [Geral_UnidadeOrganizacional] T2 WITH (NOLOCK) ON T2.[UnidadeOrganizacional_Codigo] = T1.[Cargo_UOCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[GrupoCargo_Codigo] = @AV44GrupoCargo_Codigo)";
         scmdbuf = scmdbuf + " and (T1.[Cargo_Ativo] = 1)";
         if ( ( StringUtil.StrCmp(AV33DynamicFiltersSelector1, "CARGO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV34Cargo_Nome1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Cargo_Nome] like '%' + @lV34Cargo_Nome1 + '%')";
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV33DynamicFiltersSelector1, "CARGO_UONOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV35Cargo_UONom1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[UnidadeOrganizacional_Nome] like @lV35Cargo_UONom1 + '%')";
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( AV36DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV37DynamicFiltersSelector2, "CARGO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV38Cargo_Nome2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Cargo_Nome] like '%' + @lV38Cargo_Nome2 + '%')";
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( AV36DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV37DynamicFiltersSelector2, "CARGO_UONOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV39Cargo_UONom2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[UnidadeOrganizacional_Nome] like @lV39Cargo_UONom2 + '%')";
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( AV40DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV41DynamicFiltersSelector3, "CARGO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42Cargo_Nome3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Cargo_Nome] like '%' + @lV42Cargo_Nome3 + '%')";
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( AV40DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV41DynamicFiltersSelector3, "CARGO_UONOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV43Cargo_UONom3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[UnidadeOrganizacional_Nome] like @lV43Cargo_UONom3 + '%')";
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFCargo_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFCargo_Nome)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Cargo_Nome] like @lV10TFCargo_Nome)";
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFCargo_Nome_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Cargo_Nome] = @AV11TFCargo_Nome_Sel)";
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFCargo_UONom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFCargo_UONom)) ) )
         {
            sWhereString = sWhereString + " and (T2.[UnidadeOrganizacional_Nome] like @lV12TFCargo_UONom)";
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFCargo_UONom_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[UnidadeOrganizacional_Nome] = @AV13TFCargo_UONom_Sel)";
         }
         else
         {
            GXv_int3[10] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[GrupoCargo_Codigo], T1.[Cargo_UOCod]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00NE2(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (bool)dynConstraints[17] , (int)dynConstraints[18] , (int)dynConstraints[19] );
               case 1 :
                     return conditional_P00NE3(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (bool)dynConstraints[17] , (int)dynConstraints[18] , (int)dynConstraints[19] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00NE2 ;
          prmP00NE2 = new Object[] {
          new Object[] {"@AV44GrupoCargo_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV34Cargo_Nome1",SqlDbType.VarChar,80,0} ,
          new Object[] {"@lV35Cargo_UONom1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV38Cargo_Nome2",SqlDbType.VarChar,80,0} ,
          new Object[] {"@lV39Cargo_UONom2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV42Cargo_Nome3",SqlDbType.VarChar,80,0} ,
          new Object[] {"@lV43Cargo_UONom3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV10TFCargo_Nome",SqlDbType.VarChar,80,0} ,
          new Object[] {"@AV11TFCargo_Nome_Sel",SqlDbType.VarChar,80,0} ,
          new Object[] {"@lV12TFCargo_UONom",SqlDbType.Char,50,0} ,
          new Object[] {"@AV13TFCargo_UONom_Sel",SqlDbType.Char,50,0}
          } ;
          Object[] prmP00NE3 ;
          prmP00NE3 = new Object[] {
          new Object[] {"@AV44GrupoCargo_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV34Cargo_Nome1",SqlDbType.VarChar,80,0} ,
          new Object[] {"@lV35Cargo_UONom1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV38Cargo_Nome2",SqlDbType.VarChar,80,0} ,
          new Object[] {"@lV39Cargo_UONom2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV42Cargo_Nome3",SqlDbType.VarChar,80,0} ,
          new Object[] {"@lV43Cargo_UONom3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV10TFCargo_Nome",SqlDbType.VarChar,80,0} ,
          new Object[] {"@AV11TFCargo_Nome_Sel",SqlDbType.VarChar,80,0} ,
          new Object[] {"@lV12TFCargo_UONom",SqlDbType.Char,50,0} ,
          new Object[] {"@AV13TFCargo_UONom_Sel",SqlDbType.Char,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00NE2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00NE2,100,0,true,false )
             ,new CursorDef("P00NE3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00NE3,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.getBool(3) ;
                ((String[]) buf[4])[0] = rslt.getVarchar(4) ;
                ((String[]) buf[5])[0] = rslt.getString(5, 50) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((int[]) buf[7])[0] = rslt.getInt(6) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getString(4, 50) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getVarchar(5) ;
                ((int[]) buf[7])[0] = rslt.getInt(6) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[11]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[11]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getgeral_grupo_cargogeral_cargowcfilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getgeral_grupo_cargogeral_cargowcfilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getgeral_grupo_cargogeral_cargowcfilterdata") )
          {
             return  ;
          }
          getgeral_grupo_cargogeral_cargowcfilterdata worker = new getgeral_grupo_cargogeral_cargowcfilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
