/*
               File: GetWWFeriadosFilterData
        Description: Get WWFeriados Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:54:21.70
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getwwferiadosfilterdata : GXProcedure
   {
      public getwwferiadosfilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getwwferiadosfilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV17DDOName = aP0_DDOName;
         this.AV15SearchTxt = aP1_SearchTxt;
         this.AV16SearchTxtTo = aP2_SearchTxtTo;
         this.AV21OptionsJson = "" ;
         this.AV24OptionsDescJson = "" ;
         this.AV26OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV21OptionsJson;
         aP4_OptionsDescJson=this.AV24OptionsDescJson;
         aP5_OptionIndexesJson=this.AV26OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV17DDOName = aP0_DDOName;
         this.AV15SearchTxt = aP1_SearchTxt;
         this.AV16SearchTxtTo = aP2_SearchTxtTo;
         this.AV21OptionsJson = "" ;
         this.AV24OptionsDescJson = "" ;
         this.AV26OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV21OptionsJson;
         aP4_OptionsDescJson=this.AV24OptionsDescJson;
         aP5_OptionIndexesJson=this.AV26OptionIndexesJson;
         return AV26OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getwwferiadosfilterdata objgetwwferiadosfilterdata;
         objgetwwferiadosfilterdata = new getwwferiadosfilterdata();
         objgetwwferiadosfilterdata.AV17DDOName = aP0_DDOName;
         objgetwwferiadosfilterdata.AV15SearchTxt = aP1_SearchTxt;
         objgetwwferiadosfilterdata.AV16SearchTxtTo = aP2_SearchTxtTo;
         objgetwwferiadosfilterdata.AV21OptionsJson = "" ;
         objgetwwferiadosfilterdata.AV24OptionsDescJson = "" ;
         objgetwwferiadosfilterdata.AV26OptionIndexesJson = "" ;
         objgetwwferiadosfilterdata.context.SetSubmitInitialConfig(context);
         objgetwwferiadosfilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetwwferiadosfilterdata);
         aP3_OptionsJson=this.AV21OptionsJson;
         aP4_OptionsDescJson=this.AV24OptionsDescJson;
         aP5_OptionIndexesJson=this.AV26OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getwwferiadosfilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV20Options = (IGxCollection)(new GxSimpleCollection());
         AV23OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV25OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV17DDOName), "DDO_FERIADO_NOME") == 0 )
         {
            /* Execute user subroutine: 'LOADFERIADO_NOMEOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV21OptionsJson = AV20Options.ToJSonString(false);
         AV24OptionsDescJson = AV23OptionsDesc.ToJSonString(false);
         AV26OptionIndexesJson = AV25OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV28Session.Get("WWFeriadosGridState"), "") == 0 )
         {
            AV30GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "WWFeriadosGridState"), "");
         }
         else
         {
            AV30GridState.FromXml(AV28Session.Get("WWFeriadosGridState"), "");
         }
         AV52GXV1 = 1;
         while ( AV52GXV1 <= AV30GridState.gxTpr_Filtervalues.Count )
         {
            AV31GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV30GridState.gxTpr_Filtervalues.Item(AV52GXV1));
            if ( StringUtil.StrCmp(AV31GridStateFilterValue.gxTpr_Name, "TFFERIADO_DATA") == 0 )
            {
               AV10TFFeriado_Data = context.localUtil.CToD( AV31GridStateFilterValue.gxTpr_Value, 2);
               AV11TFFeriado_Data_To = context.localUtil.CToD( AV31GridStateFilterValue.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV31GridStateFilterValue.gxTpr_Name, "TFFERIADO_NOME") == 0 )
            {
               AV12TFFeriado_Nome = AV31GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV31GridStateFilterValue.gxTpr_Name, "TFFERIADO_NOME_SEL") == 0 )
            {
               AV13TFFeriado_Nome_Sel = AV31GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV31GridStateFilterValue.gxTpr_Name, "TFFERIADO_FIXO_SEL") == 0 )
            {
               AV14TFFeriado_Fixo_Sel = (short)(NumberUtil.Val( AV31GridStateFilterValue.gxTpr_Value, "."));
            }
            AV52GXV1 = (int)(AV52GXV1+1);
         }
         if ( AV30GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV32GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV30GridState.gxTpr_Dynamicfilters.Item(1));
            AV33DynamicFiltersSelector1 = AV32GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV33DynamicFiltersSelector1, "ANO") == 0 )
            {
               AV34Ano1 = (short)(NumberUtil.Val( AV32GridStateDynamicFilter.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV33DynamicFiltersSelector1, "FERIADO_NOME") == 0 )
            {
               AV35Feriado_Nome1 = AV32GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV33DynamicFiltersSelector1, "FERIADO_DATA") == 0 )
            {
               AV36Feriado_Data1 = context.localUtil.CToD( AV32GridStateDynamicFilter.gxTpr_Value, 2);
               AV37Feriado_Data_To1 = context.localUtil.CToD( AV32GridStateDynamicFilter.gxTpr_Valueto, 2);
            }
            if ( AV30GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV38DynamicFiltersEnabled2 = true;
               AV32GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV30GridState.gxTpr_Dynamicfilters.Item(2));
               AV39DynamicFiltersSelector2 = AV32GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV39DynamicFiltersSelector2, "ANO") == 0 )
               {
                  AV40Ano2 = (short)(NumberUtil.Val( AV32GridStateDynamicFilter.gxTpr_Value, "."));
               }
               else if ( StringUtil.StrCmp(AV39DynamicFiltersSelector2, "FERIADO_NOME") == 0 )
               {
                  AV41Feriado_Nome2 = AV32GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV39DynamicFiltersSelector2, "FERIADO_DATA") == 0 )
               {
                  AV42Feriado_Data2 = context.localUtil.CToD( AV32GridStateDynamicFilter.gxTpr_Value, 2);
                  AV43Feriado_Data_To2 = context.localUtil.CToD( AV32GridStateDynamicFilter.gxTpr_Valueto, 2);
               }
               if ( AV30GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV44DynamicFiltersEnabled3 = true;
                  AV32GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV30GridState.gxTpr_Dynamicfilters.Item(3));
                  AV45DynamicFiltersSelector3 = AV32GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV45DynamicFiltersSelector3, "ANO") == 0 )
                  {
                     AV46Ano3 = (short)(NumberUtil.Val( AV32GridStateDynamicFilter.gxTpr_Value, "."));
                  }
                  else if ( StringUtil.StrCmp(AV45DynamicFiltersSelector3, "FERIADO_NOME") == 0 )
                  {
                     AV47Feriado_Nome3 = AV32GridStateDynamicFilter.gxTpr_Value;
                  }
                  else if ( StringUtil.StrCmp(AV45DynamicFiltersSelector3, "FERIADO_DATA") == 0 )
                  {
                     AV48Feriado_Data3 = context.localUtil.CToD( AV32GridStateDynamicFilter.gxTpr_Value, 2);
                     AV49Feriado_Data_To3 = context.localUtil.CToD( AV32GridStateDynamicFilter.gxTpr_Valueto, 2);
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADFERIADO_NOMEOPTIONS' Routine */
         AV12TFFeriado_Nome = AV15SearchTxt;
         AV13TFFeriado_Nome_Sel = "";
         AV54WWFeriadosDS_1_Dynamicfiltersselector1 = AV33DynamicFiltersSelector1;
         AV55WWFeriadosDS_2_Ano1 = AV34Ano1;
         AV56WWFeriadosDS_3_Feriado_nome1 = AV35Feriado_Nome1;
         AV57WWFeriadosDS_4_Feriado_data1 = AV36Feriado_Data1;
         AV58WWFeriadosDS_5_Feriado_data_to1 = AV37Feriado_Data_To1;
         AV59WWFeriadosDS_6_Dynamicfiltersenabled2 = AV38DynamicFiltersEnabled2;
         AV60WWFeriadosDS_7_Dynamicfiltersselector2 = AV39DynamicFiltersSelector2;
         AV61WWFeriadosDS_8_Ano2 = AV40Ano2;
         AV62WWFeriadosDS_9_Feriado_nome2 = AV41Feriado_Nome2;
         AV63WWFeriadosDS_10_Feriado_data2 = AV42Feriado_Data2;
         AV64WWFeriadosDS_11_Feriado_data_to2 = AV43Feriado_Data_To2;
         AV65WWFeriadosDS_12_Dynamicfiltersenabled3 = AV44DynamicFiltersEnabled3;
         AV66WWFeriadosDS_13_Dynamicfiltersselector3 = AV45DynamicFiltersSelector3;
         AV67WWFeriadosDS_14_Ano3 = AV46Ano3;
         AV68WWFeriadosDS_15_Feriado_nome3 = AV47Feriado_Nome3;
         AV69WWFeriadosDS_16_Feriado_data3 = AV48Feriado_Data3;
         AV70WWFeriadosDS_17_Feriado_data_to3 = AV49Feriado_Data_To3;
         AV71WWFeriadosDS_18_Tfferiado_data = AV10TFFeriado_Data;
         AV72WWFeriadosDS_19_Tfferiado_data_to = AV11TFFeriado_Data_To;
         AV73WWFeriadosDS_20_Tfferiado_nome = AV12TFFeriado_Nome;
         AV74WWFeriadosDS_21_Tfferiado_nome_sel = AV13TFFeriado_Nome_Sel;
         AV75WWFeriadosDS_22_Tfferiado_fixo_sel = AV14TFFeriado_Fixo_Sel;
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV54WWFeriadosDS_1_Dynamicfiltersselector1 ,
                                              AV55WWFeriadosDS_2_Ano1 ,
                                              AV56WWFeriadosDS_3_Feriado_nome1 ,
                                              AV57WWFeriadosDS_4_Feriado_data1 ,
                                              AV58WWFeriadosDS_5_Feriado_data_to1 ,
                                              AV59WWFeriadosDS_6_Dynamicfiltersenabled2 ,
                                              AV60WWFeriadosDS_7_Dynamicfiltersselector2 ,
                                              AV61WWFeriadosDS_8_Ano2 ,
                                              AV62WWFeriadosDS_9_Feriado_nome2 ,
                                              AV63WWFeriadosDS_10_Feriado_data2 ,
                                              AV64WWFeriadosDS_11_Feriado_data_to2 ,
                                              AV65WWFeriadosDS_12_Dynamicfiltersenabled3 ,
                                              AV66WWFeriadosDS_13_Dynamicfiltersselector3 ,
                                              AV67WWFeriadosDS_14_Ano3 ,
                                              AV68WWFeriadosDS_15_Feriado_nome3 ,
                                              AV69WWFeriadosDS_16_Feriado_data3 ,
                                              AV70WWFeriadosDS_17_Feriado_data_to3 ,
                                              AV71WWFeriadosDS_18_Tfferiado_data ,
                                              AV72WWFeriadosDS_19_Tfferiado_data_to ,
                                              AV74WWFeriadosDS_21_Tfferiado_nome_sel ,
                                              AV73WWFeriadosDS_20_Tfferiado_nome ,
                                              AV75WWFeriadosDS_22_Tfferiado_fixo_sel ,
                                              A1175Feriado_Data ,
                                              A1176Feriado_Nome ,
                                              A1195Feriado_Fixo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN
                                              }
         });
         lV56WWFeriadosDS_3_Feriado_nome1 = StringUtil.PadR( StringUtil.RTrim( AV56WWFeriadosDS_3_Feriado_nome1), 50, "%");
         lV62WWFeriadosDS_9_Feriado_nome2 = StringUtil.PadR( StringUtil.RTrim( AV62WWFeriadosDS_9_Feriado_nome2), 50, "%");
         lV68WWFeriadosDS_15_Feriado_nome3 = StringUtil.PadR( StringUtil.RTrim( AV68WWFeriadosDS_15_Feriado_nome3), 50, "%");
         lV73WWFeriadosDS_20_Tfferiado_nome = StringUtil.PadR( StringUtil.RTrim( AV73WWFeriadosDS_20_Tfferiado_nome), 50, "%");
         /* Using cursor P00PV2 */
         pr_default.execute(0, new Object[] {AV55WWFeriadosDS_2_Ano1, lV56WWFeriadosDS_3_Feriado_nome1, AV57WWFeriadosDS_4_Feriado_data1, AV58WWFeriadosDS_5_Feriado_data_to1, AV61WWFeriadosDS_8_Ano2, lV62WWFeriadosDS_9_Feriado_nome2, AV63WWFeriadosDS_10_Feriado_data2, AV64WWFeriadosDS_11_Feriado_data_to2, AV67WWFeriadosDS_14_Ano3, lV68WWFeriadosDS_15_Feriado_nome3, AV69WWFeriadosDS_16_Feriado_data3, AV70WWFeriadosDS_17_Feriado_data_to3, AV71WWFeriadosDS_18_Tfferiado_data, AV72WWFeriadosDS_19_Tfferiado_data_to, lV73WWFeriadosDS_20_Tfferiado_nome, AV74WWFeriadosDS_21_Tfferiado_nome_sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKPV2 = false;
            A1176Feriado_Nome = P00PV2_A1176Feriado_Nome[0];
            A1195Feriado_Fixo = P00PV2_A1195Feriado_Fixo[0];
            n1195Feriado_Fixo = P00PV2_n1195Feriado_Fixo[0];
            A1175Feriado_Data = P00PV2_A1175Feriado_Data[0];
            AV27count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00PV2_A1176Feriado_Nome[0], A1176Feriado_Nome) == 0 ) )
            {
               BRKPV2 = false;
               A1175Feriado_Data = P00PV2_A1175Feriado_Data[0];
               AV27count = (long)(AV27count+1);
               BRKPV2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A1176Feriado_Nome)) )
            {
               AV19Option = A1176Feriado_Nome;
               AV20Options.Add(AV19Option, 0);
               AV25OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV27count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV20Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKPV2 )
            {
               BRKPV2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV20Options = new GxSimpleCollection();
         AV23OptionsDesc = new GxSimpleCollection();
         AV25OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV28Session = context.GetSession();
         AV30GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV31GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFFeriado_Data = DateTime.MinValue;
         AV11TFFeriado_Data_To = DateTime.MinValue;
         AV12TFFeriado_Nome = "";
         AV13TFFeriado_Nome_Sel = "";
         AV32GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV33DynamicFiltersSelector1 = "";
         AV35Feriado_Nome1 = "";
         AV36Feriado_Data1 = DateTime.MinValue;
         AV37Feriado_Data_To1 = DateTime.MinValue;
         AV39DynamicFiltersSelector2 = "";
         AV41Feriado_Nome2 = "";
         AV42Feriado_Data2 = DateTime.MinValue;
         AV43Feriado_Data_To2 = DateTime.MinValue;
         AV45DynamicFiltersSelector3 = "";
         AV47Feriado_Nome3 = "";
         AV48Feriado_Data3 = DateTime.MinValue;
         AV49Feriado_Data_To3 = DateTime.MinValue;
         AV54WWFeriadosDS_1_Dynamicfiltersselector1 = "";
         AV56WWFeriadosDS_3_Feriado_nome1 = "";
         AV57WWFeriadosDS_4_Feriado_data1 = DateTime.MinValue;
         AV58WWFeriadosDS_5_Feriado_data_to1 = DateTime.MinValue;
         AV60WWFeriadosDS_7_Dynamicfiltersselector2 = "";
         AV62WWFeriadosDS_9_Feriado_nome2 = "";
         AV63WWFeriadosDS_10_Feriado_data2 = DateTime.MinValue;
         AV64WWFeriadosDS_11_Feriado_data_to2 = DateTime.MinValue;
         AV66WWFeriadosDS_13_Dynamicfiltersselector3 = "";
         AV68WWFeriadosDS_15_Feriado_nome3 = "";
         AV69WWFeriadosDS_16_Feriado_data3 = DateTime.MinValue;
         AV70WWFeriadosDS_17_Feriado_data_to3 = DateTime.MinValue;
         AV71WWFeriadosDS_18_Tfferiado_data = DateTime.MinValue;
         AV72WWFeriadosDS_19_Tfferiado_data_to = DateTime.MinValue;
         AV73WWFeriadosDS_20_Tfferiado_nome = "";
         AV74WWFeriadosDS_21_Tfferiado_nome_sel = "";
         scmdbuf = "";
         lV56WWFeriadosDS_3_Feriado_nome1 = "";
         lV62WWFeriadosDS_9_Feriado_nome2 = "";
         lV68WWFeriadosDS_15_Feriado_nome3 = "";
         lV73WWFeriadosDS_20_Tfferiado_nome = "";
         A1175Feriado_Data = DateTime.MinValue;
         A1176Feriado_Nome = "";
         P00PV2_A1176Feriado_Nome = new String[] {""} ;
         P00PV2_A1195Feriado_Fixo = new bool[] {false} ;
         P00PV2_n1195Feriado_Fixo = new bool[] {false} ;
         P00PV2_A1175Feriado_Data = new DateTime[] {DateTime.MinValue} ;
         AV19Option = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getwwferiadosfilterdata__default(),
            new Object[][] {
                new Object[] {
               P00PV2_A1176Feriado_Nome, P00PV2_A1195Feriado_Fixo, P00PV2_n1195Feriado_Fixo, P00PV2_A1175Feriado_Data
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV14TFFeriado_Fixo_Sel ;
      private short AV34Ano1 ;
      private short AV40Ano2 ;
      private short AV46Ano3 ;
      private short AV55WWFeriadosDS_2_Ano1 ;
      private short AV61WWFeriadosDS_8_Ano2 ;
      private short AV67WWFeriadosDS_14_Ano3 ;
      private short AV75WWFeriadosDS_22_Tfferiado_fixo_sel ;
      private int AV52GXV1 ;
      private long AV27count ;
      private String AV12TFFeriado_Nome ;
      private String AV13TFFeriado_Nome_Sel ;
      private String AV35Feriado_Nome1 ;
      private String AV41Feriado_Nome2 ;
      private String AV47Feriado_Nome3 ;
      private String AV56WWFeriadosDS_3_Feriado_nome1 ;
      private String AV62WWFeriadosDS_9_Feriado_nome2 ;
      private String AV68WWFeriadosDS_15_Feriado_nome3 ;
      private String AV73WWFeriadosDS_20_Tfferiado_nome ;
      private String AV74WWFeriadosDS_21_Tfferiado_nome_sel ;
      private String scmdbuf ;
      private String lV56WWFeriadosDS_3_Feriado_nome1 ;
      private String lV62WWFeriadosDS_9_Feriado_nome2 ;
      private String lV68WWFeriadosDS_15_Feriado_nome3 ;
      private String lV73WWFeriadosDS_20_Tfferiado_nome ;
      private String A1176Feriado_Nome ;
      private DateTime AV10TFFeriado_Data ;
      private DateTime AV11TFFeriado_Data_To ;
      private DateTime AV36Feriado_Data1 ;
      private DateTime AV37Feriado_Data_To1 ;
      private DateTime AV42Feriado_Data2 ;
      private DateTime AV43Feriado_Data_To2 ;
      private DateTime AV48Feriado_Data3 ;
      private DateTime AV49Feriado_Data_To3 ;
      private DateTime AV57WWFeriadosDS_4_Feriado_data1 ;
      private DateTime AV58WWFeriadosDS_5_Feriado_data_to1 ;
      private DateTime AV63WWFeriadosDS_10_Feriado_data2 ;
      private DateTime AV64WWFeriadosDS_11_Feriado_data_to2 ;
      private DateTime AV69WWFeriadosDS_16_Feriado_data3 ;
      private DateTime AV70WWFeriadosDS_17_Feriado_data_to3 ;
      private DateTime AV71WWFeriadosDS_18_Tfferiado_data ;
      private DateTime AV72WWFeriadosDS_19_Tfferiado_data_to ;
      private DateTime A1175Feriado_Data ;
      private bool returnInSub ;
      private bool AV38DynamicFiltersEnabled2 ;
      private bool AV44DynamicFiltersEnabled3 ;
      private bool AV59WWFeriadosDS_6_Dynamicfiltersenabled2 ;
      private bool AV65WWFeriadosDS_12_Dynamicfiltersenabled3 ;
      private bool A1195Feriado_Fixo ;
      private bool BRKPV2 ;
      private bool n1195Feriado_Fixo ;
      private String AV26OptionIndexesJson ;
      private String AV21OptionsJson ;
      private String AV24OptionsDescJson ;
      private String AV17DDOName ;
      private String AV15SearchTxt ;
      private String AV16SearchTxtTo ;
      private String AV33DynamicFiltersSelector1 ;
      private String AV39DynamicFiltersSelector2 ;
      private String AV45DynamicFiltersSelector3 ;
      private String AV54WWFeriadosDS_1_Dynamicfiltersselector1 ;
      private String AV60WWFeriadosDS_7_Dynamicfiltersselector2 ;
      private String AV66WWFeriadosDS_13_Dynamicfiltersselector3 ;
      private String AV19Option ;
      private IGxSession AV28Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] P00PV2_A1176Feriado_Nome ;
      private bool[] P00PV2_A1195Feriado_Fixo ;
      private bool[] P00PV2_n1195Feriado_Fixo ;
      private DateTime[] P00PV2_A1175Feriado_Data ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV20Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV23OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV25OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV30GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV31GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV32GridStateDynamicFilter ;
   }

   public class getwwferiadosfilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00PV2( IGxContext context ,
                                             String AV54WWFeriadosDS_1_Dynamicfiltersselector1 ,
                                             short AV55WWFeriadosDS_2_Ano1 ,
                                             String AV56WWFeriadosDS_3_Feriado_nome1 ,
                                             DateTime AV57WWFeriadosDS_4_Feriado_data1 ,
                                             DateTime AV58WWFeriadosDS_5_Feriado_data_to1 ,
                                             bool AV59WWFeriadosDS_6_Dynamicfiltersenabled2 ,
                                             String AV60WWFeriadosDS_7_Dynamicfiltersselector2 ,
                                             short AV61WWFeriadosDS_8_Ano2 ,
                                             String AV62WWFeriadosDS_9_Feriado_nome2 ,
                                             DateTime AV63WWFeriadosDS_10_Feriado_data2 ,
                                             DateTime AV64WWFeriadosDS_11_Feriado_data_to2 ,
                                             bool AV65WWFeriadosDS_12_Dynamicfiltersenabled3 ,
                                             String AV66WWFeriadosDS_13_Dynamicfiltersselector3 ,
                                             short AV67WWFeriadosDS_14_Ano3 ,
                                             String AV68WWFeriadosDS_15_Feriado_nome3 ,
                                             DateTime AV69WWFeriadosDS_16_Feriado_data3 ,
                                             DateTime AV70WWFeriadosDS_17_Feriado_data_to3 ,
                                             DateTime AV71WWFeriadosDS_18_Tfferiado_data ,
                                             DateTime AV72WWFeriadosDS_19_Tfferiado_data_to ,
                                             String AV74WWFeriadosDS_21_Tfferiado_nome_sel ,
                                             String AV73WWFeriadosDS_20_Tfferiado_nome ,
                                             short AV75WWFeriadosDS_22_Tfferiado_fixo_sel ,
                                             DateTime A1175Feriado_Data ,
                                             String A1176Feriado_Nome ,
                                             bool A1195Feriado_Fixo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [16] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT [Feriado_Nome], [Feriado_Fixo], [Feriado_Data] FROM [Feriados] WITH (NOLOCK)";
         if ( ( StringUtil.StrCmp(AV54WWFeriadosDS_1_Dynamicfiltersselector1, "ANO") == 0 ) && ( ! (0==AV55WWFeriadosDS_2_Ano1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (YEAR([Feriado_Data]) = @AV55WWFeriadosDS_2_Ano1)";
            }
            else
            {
               sWhereString = sWhereString + " (YEAR([Feriado_Data]) = @AV55WWFeriadosDS_2_Ano1)";
            }
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV54WWFeriadosDS_1_Dynamicfiltersselector1, "FERIADO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56WWFeriadosDS_3_Feriado_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Feriado_Nome] like '%' + @lV56WWFeriadosDS_3_Feriado_nome1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([Feriado_Nome] like '%' + @lV56WWFeriadosDS_3_Feriado_nome1 + '%')";
            }
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV54WWFeriadosDS_1_Dynamicfiltersselector1, "FERIADO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV57WWFeriadosDS_4_Feriado_data1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Feriado_Data] >= @AV57WWFeriadosDS_4_Feriado_data1)";
            }
            else
            {
               sWhereString = sWhereString + " ([Feriado_Data] >= @AV57WWFeriadosDS_4_Feriado_data1)";
            }
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV54WWFeriadosDS_1_Dynamicfiltersselector1, "FERIADO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV58WWFeriadosDS_5_Feriado_data_to1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Feriado_Data] <= @AV58WWFeriadosDS_5_Feriado_data_to1)";
            }
            else
            {
               sWhereString = sWhereString + " ([Feriado_Data] <= @AV58WWFeriadosDS_5_Feriado_data_to1)";
            }
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( AV59WWFeriadosDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV60WWFeriadosDS_7_Dynamicfiltersselector2, "ANO") == 0 ) && ( ! (0==AV61WWFeriadosDS_8_Ano2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (YEAR([Feriado_Data]) = @AV61WWFeriadosDS_8_Ano2)";
            }
            else
            {
               sWhereString = sWhereString + " (YEAR([Feriado_Data]) = @AV61WWFeriadosDS_8_Ano2)";
            }
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( AV59WWFeriadosDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV60WWFeriadosDS_7_Dynamicfiltersselector2, "FERIADO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62WWFeriadosDS_9_Feriado_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Feriado_Nome] like '%' + @lV62WWFeriadosDS_9_Feriado_nome2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([Feriado_Nome] like '%' + @lV62WWFeriadosDS_9_Feriado_nome2 + '%')";
            }
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( AV59WWFeriadosDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV60WWFeriadosDS_7_Dynamicfiltersselector2, "FERIADO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV63WWFeriadosDS_10_Feriado_data2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Feriado_Data] >= @AV63WWFeriadosDS_10_Feriado_data2)";
            }
            else
            {
               sWhereString = sWhereString + " ([Feriado_Data] >= @AV63WWFeriadosDS_10_Feriado_data2)";
            }
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( AV59WWFeriadosDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV60WWFeriadosDS_7_Dynamicfiltersselector2, "FERIADO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV64WWFeriadosDS_11_Feriado_data_to2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Feriado_Data] <= @AV64WWFeriadosDS_11_Feriado_data_to2)";
            }
            else
            {
               sWhereString = sWhereString + " ([Feriado_Data] <= @AV64WWFeriadosDS_11_Feriado_data_to2)";
            }
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( AV65WWFeriadosDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV66WWFeriadosDS_13_Dynamicfiltersselector3, "ANO") == 0 ) && ( ! (0==AV67WWFeriadosDS_14_Ano3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (YEAR([Feriado_Data]) = @AV67WWFeriadosDS_14_Ano3)";
            }
            else
            {
               sWhereString = sWhereString + " (YEAR([Feriado_Data]) = @AV67WWFeriadosDS_14_Ano3)";
            }
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( AV65WWFeriadosDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV66WWFeriadosDS_13_Dynamicfiltersselector3, "FERIADO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV68WWFeriadosDS_15_Feriado_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Feriado_Nome] like '%' + @lV68WWFeriadosDS_15_Feriado_nome3 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([Feriado_Nome] like '%' + @lV68WWFeriadosDS_15_Feriado_nome3 + '%')";
            }
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( AV65WWFeriadosDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV66WWFeriadosDS_13_Dynamicfiltersselector3, "FERIADO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV69WWFeriadosDS_16_Feriado_data3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Feriado_Data] >= @AV69WWFeriadosDS_16_Feriado_data3)";
            }
            else
            {
               sWhereString = sWhereString + " ([Feriado_Data] >= @AV69WWFeriadosDS_16_Feriado_data3)";
            }
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( AV65WWFeriadosDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV66WWFeriadosDS_13_Dynamicfiltersselector3, "FERIADO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV70WWFeriadosDS_17_Feriado_data_to3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Feriado_Data] <= @AV70WWFeriadosDS_17_Feriado_data_to3)";
            }
            else
            {
               sWhereString = sWhereString + " ([Feriado_Data] <= @AV70WWFeriadosDS_17_Feriado_data_to3)";
            }
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( ! (DateTime.MinValue==AV71WWFeriadosDS_18_Tfferiado_data) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Feriado_Data] >= @AV71WWFeriadosDS_18_Tfferiado_data)";
            }
            else
            {
               sWhereString = sWhereString + " ([Feriado_Data] >= @AV71WWFeriadosDS_18_Tfferiado_data)";
            }
         }
         else
         {
            GXv_int1[12] = 1;
         }
         if ( ! (DateTime.MinValue==AV72WWFeriadosDS_19_Tfferiado_data_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Feriado_Data] <= @AV72WWFeriadosDS_19_Tfferiado_data_to)";
            }
            else
            {
               sWhereString = sWhereString + " ([Feriado_Data] <= @AV72WWFeriadosDS_19_Tfferiado_data_to)";
            }
         }
         else
         {
            GXv_int1[13] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV74WWFeriadosDS_21_Tfferiado_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73WWFeriadosDS_20_Tfferiado_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Feriado_Nome] like @lV73WWFeriadosDS_20_Tfferiado_nome)";
            }
            else
            {
               sWhereString = sWhereString + " ([Feriado_Nome] like @lV73WWFeriadosDS_20_Tfferiado_nome)";
            }
         }
         else
         {
            GXv_int1[14] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74WWFeriadosDS_21_Tfferiado_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Feriado_Nome] = @AV74WWFeriadosDS_21_Tfferiado_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([Feriado_Nome] = @AV74WWFeriadosDS_21_Tfferiado_nome_sel)";
            }
         }
         else
         {
            GXv_int1[15] = 1;
         }
         if ( AV75WWFeriadosDS_22_Tfferiado_fixo_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Feriado_Fixo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " ([Feriado_Fixo] = 1)";
            }
         }
         if ( AV75WWFeriadosDS_22_Tfferiado_fixo_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Feriado_Fixo] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " ([Feriado_Fixo] = 0)";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY [Feriado_Nome]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00PV2(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (DateTime)dynConstraints[3] , (DateTime)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (short)dynConstraints[7] , (String)dynConstraints[8] , (DateTime)dynConstraints[9] , (DateTime)dynConstraints[10] , (bool)dynConstraints[11] , (String)dynConstraints[12] , (short)dynConstraints[13] , (String)dynConstraints[14] , (DateTime)dynConstraints[15] , (DateTime)dynConstraints[16] , (DateTime)dynConstraints[17] , (DateTime)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (short)dynConstraints[21] , (DateTime)dynConstraints[22] , (String)dynConstraints[23] , (bool)dynConstraints[24] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00PV2 ;
          prmP00PV2 = new Object[] {
          new Object[] {"@AV55WWFeriadosDS_2_Ano1",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@lV56WWFeriadosDS_3_Feriado_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@AV57WWFeriadosDS_4_Feriado_data1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV58WWFeriadosDS_5_Feriado_data_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV61WWFeriadosDS_8_Ano2",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@lV62WWFeriadosDS_9_Feriado_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@AV63WWFeriadosDS_10_Feriado_data2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV64WWFeriadosDS_11_Feriado_data_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV67WWFeriadosDS_14_Ano3",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@lV68WWFeriadosDS_15_Feriado_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@AV69WWFeriadosDS_16_Feriado_data3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV70WWFeriadosDS_17_Feriado_data_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV71WWFeriadosDS_18_Tfferiado_data",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV72WWFeriadosDS_19_Tfferiado_data_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV73WWFeriadosDS_20_Tfferiado_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV74WWFeriadosDS_21_Tfferiado_nome_sel",SqlDbType.Char,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00PV2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00PV2,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((DateTime[]) buf[3])[0] = rslt.getGXDate(3) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[16]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[18]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[19]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[20]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[22]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[23]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[24]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[26]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[27]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[28]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[29]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getwwferiadosfilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getwwferiadosfilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getwwferiadosfilterdata") )
          {
             return  ;
          }
          getwwferiadosfilterdata worker = new getwwferiadosfilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
