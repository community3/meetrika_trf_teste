/*
               File: WC_ViewContagemResultadoEvidencias
        Description: View Contagem Resultado Evidencias
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:17:15.80
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wc_viewcontagemresultadoevidencias : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public wc_viewcontagemresultadoevidencias( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public wc_viewcontagemresultadoevidencias( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_ContagemResultado_Codigo )
      {
         this.A456ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         executePrivate();
         aP0_ContagemResultado_Codigo=this.A456ContagemResultado_Codigo;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  A456ContagemResultado_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)A456ContagemResultado_Codigo});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
               {
                  nRC_GXsfl_5 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  nGXsfl_5_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  sGXsfl_5_idx = GetNextPar( );
                  sPrefix = GetNextPar( );
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxnrGrid_newrow( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
               {
                  subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  A456ContagemResultado_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
                  A587ContagemResultadoEvidencia_Descricao = GetNextPar( );
                  n587ContagemResultadoEvidencia_Descricao = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A587ContagemResultadoEvidencia_Descricao", A587ContagemResultadoEvidencia_Descricao);
                  A588ContagemResultadoEvidencia_Arquivo = GetNextPar( );
                  n588ContagemResultadoEvidencia_Arquivo = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A588ContagemResultadoEvidencia_Arquivo", A588ContagemResultadoEvidencia_Arquivo);
                  A1493ContagemResultadoEvidencia_Owner = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  n1493ContagemResultadoEvidencia_Owner = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1493ContagemResultadoEvidencia_Owner", StringUtil.LTrim( StringUtil.Str( (decimal)(A1493ContagemResultadoEvidencia_Owner), 6, 0)));
                  ajax_req_read_hidden_sdt(GetNextPar( ), AV13WWPContext);
                  sPrefix = GetNextPar( );
                  init_default_properties( ) ;
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxgrGrid_refresh( subGrid_Rows, A456ContagemResultado_Codigo, A587ContagemResultadoEvidencia_Descricao, A588ContagemResultadoEvidencia_Arquivo, A1493ContagemResultadoEvidencia_Owner, AV13WWPContext, sPrefix) ;
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
                  return  ;
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PAC42( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               context.Gx_err = 0;
               /* Using cursor H00C42 */
               pr_default.execute(0, new Object[] {A456ContagemResultado_Codigo});
               A490ContagemResultado_ContratadaCod = H00C42_A490ContagemResultado_ContratadaCod[0];
               n490ContagemResultado_ContratadaCod = H00C42_n490ContagemResultado_ContratadaCod[0];
               pr_default.close(0);
               /* Using cursor H00C43 */
               pr_default.execute(1, new Object[] {n490ContagemResultado_ContratadaCod, A490ContagemResultado_ContratadaCod});
               A52Contratada_AreaTrabalhoCod = H00C43_A52Contratada_AreaTrabalhoCod[0];
               n52Contratada_AreaTrabalhoCod = H00C43_n52Contratada_AreaTrabalhoCod[0];
               pr_default.close(1);
               WSC42( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "View Contagem Resultado Evidencias") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20206216171593");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("DVelop/Shared/yahoo.js", "");
         context.AddJavascriptSource("DVelop/Shared/dom.js", "");
         context.AddJavascriptSource("DVelop/Shared/event.js", "");
         context.AddJavascriptSource("DVelop/Shared/dragdrop.js", "");
         context.AddJavascriptSource("DVelop/Shared/container.js", "");
         context.AddJavascriptSource("DVelop/ConfirmPanel/ConfirmPanelRender.js", "");
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wc_viewcontagemresultadoevidencias.aspx") + "?" + UrlEncode("" +A456ContagemResultado_Codigo)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"nRC_GXsfl_5", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_5), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOA456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOA456ContagemResultado_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A456ContagemResultado_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADOEVIDENCIA_DESCRICAO", A587ContagemResultadoEvidencia_Descricao);
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADOEVIDENCIA_ARQUIVO", A588ContagemResultadoEvidencia_Arquivo);
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vWWPCONTEXT", AV13WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vWWPCONTEXT", AV13WWPContext);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADOEVIDENCIA_OWNER", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1493ContagemResultadoEvidencia_Owner), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATADA_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A52Contratada_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVELOP_CONFIRMPANEL_APAGAR_Title", StringUtil.RTrim( Dvelop_confirmpanel_apagar_Title));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVELOP_CONFIRMPANEL_APAGAR_Icon", StringUtil.RTrim( Dvelop_confirmpanel_apagar_Icon));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVELOP_CONFIRMPANEL_APAGAR_Confirmtext", StringUtil.RTrim( Dvelop_confirmpanel_apagar_Confirmtext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVELOP_CONFIRMPANEL_APAGAR_Buttonyestext", StringUtil.RTrim( Dvelop_confirmpanel_apagar_Buttonyestext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVELOP_CONFIRMPANEL_APAGAR_Buttonnotext", StringUtil.RTrim( Dvelop_confirmpanel_apagar_Buttonnotext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVELOP_CONFIRMPANEL_APAGAR_Buttoncanceltext", StringUtil.RTrim( Dvelop_confirmpanel_apagar_Buttoncanceltext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVELOP_CONFIRMPANEL_APAGAR_Confirmtype", StringUtil.RTrim( Dvelop_confirmpanel_apagar_Confirmtype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVELOP_CONFIRMPANEL_APAGAR_Result", StringUtil.RTrim( Dvelop_confirmpanel_apagar_Result));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormC42( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("wc_viewcontagemresultadoevidencias.js", "?2020621617165");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "WC_ViewContagemResultadoEvidencias" ;
      }

      public override String GetPgmdesc( )
      {
         return "View Contagem Resultado Evidencias" ;
      }

      protected void WBC40( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "wc_viewcontagemresultadoevidencias.aspx");
               context.AddJavascriptSource("DVelop/Shared/yahoo.js", "");
               context.AddJavascriptSource("DVelop/Shared/dom.js", "");
               context.AddJavascriptSource("DVelop/Shared/event.js", "");
               context.AddJavascriptSource("DVelop/Shared/dragdrop.js", "");
               context.AddJavascriptSource("DVelop/Shared/container.js", "");
               context.AddJavascriptSource("DVelop/ConfirmPanel/ConfirmPanelRender.js", "");
            }
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", sPrefix, "false");
            wb_table1_2_C42( true) ;
         }
         else
         {
            wb_table1_2_C42( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_C42e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DVELOP_CONFIRMPANEL_APAGARContainer"+"\"></div>") ;
         }
         wbLoad = true;
      }

      protected void STARTC42( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "View Contagem Resultado Evidencias", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPC40( ) ;
            }
         }
      }

      protected void WSC42( )
      {
         STARTC42( ) ;
         EVTC42( ) ;
      }

      protected void EVTC42( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPC40( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DVELOP_CONFIRMPANEL_APAGAR.CLOSE") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPC40( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11C42 */
                                    E11C42 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOINSERT'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPC40( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12C42 */
                                    E12C42 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPC40( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGING") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPC40( ) ;
                              }
                              sEvt = cgiGet( sPrefix+"GRIDPAGING");
                              if ( StringUtil.StrCmp(sEvt, "FIRST") == 0 )
                              {
                                 subgrid_firstpage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "PREV") == 0 )
                              {
                                 subgrid_previouspage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "NEXT") == 0 )
                              {
                                 subgrid_nextpage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "LAST") == 0 )
                              {
                                 subgrid_lastpage( ) ;
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 4), "LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPC40( ) ;
                              }
                              nGXsfl_5_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_5_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_5_idx), 4, 0)), 4, "0");
                              SubsflControlProps_52( ) ;
                              AV5Display = cgiGet( edtavDisplay_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDisplay_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV5Display)) ? AV18Display_GXI : context.convertURL( context.PathToRelativeUrl( AV5Display))));
                              AV6Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV6Delete)) ? AV19Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV6Delete))));
                              A586ContagemResultadoEvidencia_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContagemResultadoEvidencia_Codigo_Internalname), ",", "."));
                              A646TipoDocumento_Nome = StringUtil.Upper( cgiGet( edtTipoDocumento_Nome_Internalname));
                              A591ContagemResultadoEvidencia_Data = context.localUtil.CToT( cgiGet( edtContagemResultadoEvidencia_Data_Internalname), 0);
                              n591ContagemResultadoEvidencia_Data = false;
                              A1494ContagemResultadoEvidencia_Entidade = StringUtil.Upper( cgiGet( edtContagemResultadoEvidencia_Entidade_Internalname));
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          /* Execute user event: E13C42 */
                                          E13C42 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          /* Execute user event: E14C42 */
                                          E14C42 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          if ( ! wbErr )
                                          {
                                             Rfr0gs = false;
                                             if ( ! Rfr0gs )
                                             {
                                             }
                                             dynload_actions( ) ;
                                          }
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                                    {
                                       STRUPC40( ) ;
                                    }
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                       }
                                    }
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEC42( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormC42( ) ;
            }
         }
      }

      protected void PAC42( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_52( ) ;
         while ( nGXsfl_5_idx <= nRC_GXsfl_5 )
         {
            sendrow_52( ) ;
            nGXsfl_5_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_5_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_5_idx+1));
            sGXsfl_5_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_5_idx), 4, 0)), 4, "0");
            SubsflControlProps_52( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       int A456ContagemResultado_Codigo ,
                                       String A587ContagemResultadoEvidencia_Descricao ,
                                       String A588ContagemResultadoEvidencia_Arquivo ,
                                       int A1493ContagemResultadoEvidencia_Owner ,
                                       wwpbaseobjects.SdtWWPContext AV13WWPContext ,
                                       String sPrefix )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFC42( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADOEVIDENCIA_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A586ContagemResultadoEvidencia_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADOEVIDENCIA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A586ContagemResultadoEvidencia_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADOEVIDENCIA_DATA", GetSecureSignedToken( sPrefix, context.localUtil.Format( A591ContagemResultadoEvidencia_Data, "99/99/99 99:99")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADOEVIDENCIA_DATA", context.localUtil.TToC( A591ContagemResultadoEvidencia_Data, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADOEVIDENCIA_ENTIDADE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1494ContagemResultadoEvidencia_Entidade, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTAGEMRESULTADOEVIDENCIA_ENTIDADE", StringUtil.RTrim( A1494ContagemResultadoEvidencia_Entidade));
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFC42( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      protected void RFC42( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 5;
         nGXsfl_5_idx = 1;
         sGXsfl_5_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_5_idx), 4, 0)), 4, "0");
         SubsflControlProps_52( ) ;
         nGXsfl_5_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", sPrefix);
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_52( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            /* Using cursor H00C44 */
            pr_default.execute(2, new Object[] {A456ContagemResultado_Codigo, GXPagingFrom2, GXPagingTo2});
            nGXsfl_5_idx = 1;
            while ( ( (pr_default.getStatus(2) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A645TipoDocumento_Codigo = H00C44_A645TipoDocumento_Codigo[0];
               n645TipoDocumento_Codigo = H00C44_n645TipoDocumento_Codigo[0];
               A589ContagemResultadoEvidencia_NomeArq = H00C44_A589ContagemResultadoEvidencia_NomeArq[0];
               n589ContagemResultadoEvidencia_NomeArq = H00C44_n589ContagemResultadoEvidencia_NomeArq[0];
               A588ContagemResultadoEvidencia_Arquivo_Filename = A589ContagemResultadoEvidencia_NomeArq;
               A590ContagemResultadoEvidencia_TipoArq = H00C44_A590ContagemResultadoEvidencia_TipoArq[0];
               n590ContagemResultadoEvidencia_TipoArq = H00C44_n590ContagemResultadoEvidencia_TipoArq[0];
               A588ContagemResultadoEvidencia_Arquivo_Filetype = A590ContagemResultadoEvidencia_TipoArq;
               A587ContagemResultadoEvidencia_Descricao = H00C44_A587ContagemResultadoEvidencia_Descricao[0];
               n587ContagemResultadoEvidencia_Descricao = H00C44_n587ContagemResultadoEvidencia_Descricao[0];
               A591ContagemResultadoEvidencia_Data = H00C44_A591ContagemResultadoEvidencia_Data[0];
               n591ContagemResultadoEvidencia_Data = H00C44_n591ContagemResultadoEvidencia_Data[0];
               A646TipoDocumento_Nome = H00C44_A646TipoDocumento_Nome[0];
               A586ContagemResultadoEvidencia_Codigo = H00C44_A586ContagemResultadoEvidencia_Codigo[0];
               A1493ContagemResultadoEvidencia_Owner = H00C44_A1493ContagemResultadoEvidencia_Owner[0];
               n1493ContagemResultadoEvidencia_Owner = H00C44_n1493ContagemResultadoEvidencia_Owner[0];
               A588ContagemResultadoEvidencia_Arquivo = H00C44_A588ContagemResultadoEvidencia_Arquivo[0];
               n588ContagemResultadoEvidencia_Arquivo = H00C44_n588ContagemResultadoEvidencia_Arquivo[0];
               A646TipoDocumento_Nome = H00C44_A646TipoDocumento_Nome[0];
               GXt_char1 = A1494ContagemResultadoEvidencia_Entidade;
               new prc_entidadedousuario(context ).execute(  A1493ContagemResultadoEvidencia_Owner,  A52Contratada_AreaTrabalhoCod, out  GXt_char1) ;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1493ContagemResultadoEvidencia_Owner", StringUtil.LTrim( StringUtil.Str( (decimal)(A1493ContagemResultadoEvidencia_Owner), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A52Contratada_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A52Contratada_AreaTrabalhoCod), 6, 0)));
               A1494ContagemResultadoEvidencia_Entidade = GXt_char1;
               /* Execute user event: E14C42 */
               E14C42 ();
               pr_default.readNext(2);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(2) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(2);
            wbEnd = 5;
            WBC40( ) ;
         }
         nGXsfl_5_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         /* Using cursor H00C45 */
         pr_default.execute(3, new Object[] {A456ContagemResultado_Codigo});
         GRID_nRecordCount = H00C45_AGRID_nRecordCount[0];
         pr_default.close(3);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, A456ContagemResultado_Codigo, A587ContagemResultadoEvidencia_Descricao, A588ContagemResultadoEvidencia_Arquivo, A1493ContagemResultadoEvidencia_Owner, AV13WWPContext, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, A456ContagemResultado_Codigo, A587ContagemResultadoEvidencia_Descricao, A588ContagemResultadoEvidencia_Arquivo, A1493ContagemResultadoEvidencia_Owner, AV13WWPContext, sPrefix) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, A456ContagemResultado_Codigo, A587ContagemResultadoEvidencia_Descricao, A588ContagemResultadoEvidencia_Arquivo, A1493ContagemResultadoEvidencia_Owner, AV13WWPContext, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, A456ContagemResultado_Codigo, A587ContagemResultadoEvidencia_Descricao, A588ContagemResultadoEvidencia_Arquivo, A1493ContagemResultadoEvidencia_Owner, AV13WWPContext, sPrefix) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, A456ContagemResultado_Codigo, A587ContagemResultadoEvidencia_Descricao, A588ContagemResultadoEvidencia_Arquivo, A1493ContagemResultadoEvidencia_Owner, AV13WWPContext, sPrefix) ;
         }
         return (int)(0) ;
      }

      protected void STRUPC40( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         /* Using cursor H00C46 */
         pr_default.execute(4, new Object[] {A456ContagemResultado_Codigo});
         A490ContagemResultado_ContratadaCod = H00C46_A490ContagemResultado_ContratadaCod[0];
         n490ContagemResultado_ContratadaCod = H00C46_n490ContagemResultado_ContratadaCod[0];
         pr_default.close(4);
         /* Using cursor H00C47 */
         pr_default.execute(5, new Object[] {n490ContagemResultado_ContratadaCod, A490ContagemResultado_ContratadaCod});
         A52Contratada_AreaTrabalhoCod = H00C47_A52Contratada_AreaTrabalhoCod[0];
         n52Contratada_AreaTrabalhoCod = H00C47_n52Contratada_AreaTrabalhoCod[0];
         pr_default.close(5);
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E13C42 */
         E13C42 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            /* Read saved values. */
            nRC_GXsfl_5 = (short)(context.localUtil.CToN( cgiGet( sPrefix+"nRC_GXsfl_5"), ",", "."));
            wcpOA456ContagemResultado_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA456ContagemResultado_Codigo"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Dvelop_confirmpanel_apagar_Title = cgiGet( sPrefix+"DVELOP_CONFIRMPANEL_APAGAR_Title");
            Dvelop_confirmpanel_apagar_Icon = cgiGet( sPrefix+"DVELOP_CONFIRMPANEL_APAGAR_Icon");
            Dvelop_confirmpanel_apagar_Confirmtext = cgiGet( sPrefix+"DVELOP_CONFIRMPANEL_APAGAR_Confirmtext");
            Dvelop_confirmpanel_apagar_Buttonyestext = cgiGet( sPrefix+"DVELOP_CONFIRMPANEL_APAGAR_Buttonyestext");
            Dvelop_confirmpanel_apagar_Buttonnotext = cgiGet( sPrefix+"DVELOP_CONFIRMPANEL_APAGAR_Buttonnotext");
            Dvelop_confirmpanel_apagar_Buttoncanceltext = cgiGet( sPrefix+"DVELOP_CONFIRMPANEL_APAGAR_Buttoncanceltext");
            Dvelop_confirmpanel_apagar_Confirmtype = cgiGet( sPrefix+"DVELOP_CONFIRMPANEL_APAGAR_Confirmtype");
            Dvelop_confirmpanel_apagar_Result = cgiGet( sPrefix+"DVELOP_CONFIRMPANEL_APAGAR_Result");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E13C42 */
         E13C42 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E13C42( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV13WWPContext) ;
         subGrid_Rows = 0;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
      }

      private void E14C42( )
      {
         /* Load Routine */
         AV5Display = context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavDisplay_Internalname, AV5Display);
         AV18Display_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( )));
         edtavDisplay_Tooltiptext = A587ContagemResultadoEvidencia_Descricao;
         edtavDisplay_Link = context.PathToUrl( A588ContagemResultadoEvidencia_Arquivo);
         edtavDisplay_Linktarget = "_blank";
         AV6Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavDelete_Internalname, AV6Delete);
         AV19Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
         edtavDelete_Visible = (((A1493ContagemResultadoEvidencia_Owner==AV13WWPContext.gxTpr_Userid)) ? 1 : 0);
         edtavDelete_Tooltiptext = "Eliminar";
         sendrow_52( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_5_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(5, GridRow);
         }
      }

      protected void E12C42( )
      {
         /* 'DoInsert' Routine */
         AV15WebSession.Set("OSCodigo", StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0));
         context.PopUp(formatLink("wp_contagemresultadoevidencia.aspx") + "?" + UrlEncode("" +A456ContagemResultado_Codigo), new Object[] {"A456ContagemResultado_Codigo"});
         context.DoAjaxRefreshCmp(sPrefix);
      }

      protected void E11C42( )
      {
         /* Dvelop_confirmpanel_apagar_Close Routine */
         if ( StringUtil.StrCmp(Dvelop_confirmpanel_apagar_Result, "Yes") == 0 )
         {
            AV12ContagemResultadoEvidencia.Load(A586ContagemResultadoEvidencia_Codigo);
            AV12ContagemResultadoEvidencia.Delete();
            if ( AV12ContagemResultadoEvidencia.Success() )
            {
               context.CommitDataStores( "WC_ViewContagemResultadoEvidencias");
               context.DoAjaxRefreshCmp(sPrefix);
            }
         }
      }

      protected void wb_table1_2_C42( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"DivS\" data-gxgridid=\"5\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWith", 0, "", "", 1, 2, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavDelete_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Evidencia") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Nome do Arquivo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Tipo Documento") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Upload") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Respons�vel") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", sPrefix);
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV5Display));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDisplay_Link));
               GridColumn.AddObjectProperty("Linktarget", StringUtil.RTrim( edtavDisplay_Linktarget));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDisplay_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV6Delete));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDelete_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A586ContagemResultadoEvidencia_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A589ContagemResultadoEvidencia_NomeArq));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A646TipoDocumento_Nome));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.TToC( A591ContagemResultadoEvidencia_Data, 10, 8, 0, 3, "/", ":", " "));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A1494ContagemResultadoEvidencia_Entidade));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 5 )
         {
            wbEnd = 0;
            nRC_GXsfl_5 = (short)(nGXsfl_5_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               GridContainer.AddObjectProperty("GRID_nEOF", GRID_nEOF);
               GridContainer.AddObjectProperty("GRID_nFirstRecordOnPage", GRID_nFirstRecordOnPage);
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid(sPrefix+"_"+"Grid", GridContainer);
               if ( ! isAjaxCallMode( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+sPrefix+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "width:4px")+"\">") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 15,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgInsert_Internalname, context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Inserir", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgInsert_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOINSERT\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WC_ViewContagemResultadoEvidencias.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_C42e( true) ;
         }
         else
         {
            wb_table1_2_C42e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         A456ContagemResultado_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAC42( ) ;
         WSC42( ) ;
         WEC42( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlA456ContagemResultado_Codigo = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PAC42( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "wc_viewcontagemresultadoevidencias");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PAC42( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            A456ContagemResultado_Codigo = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
         }
         wcpOA456ContagemResultado_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA456ContagemResultado_Codigo"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( A456ContagemResultado_Codigo != wcpOA456ContagemResultado_Codigo ) ) )
         {
            setjustcreated();
         }
         wcpOA456ContagemResultado_Codigo = A456ContagemResultado_Codigo;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlA456ContagemResultado_Codigo = cgiGet( sPrefix+"A456ContagemResultado_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlA456ContagemResultado_Codigo) > 0 )
         {
            A456ContagemResultado_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlA456ContagemResultado_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
         }
         else
         {
            A456ContagemResultado_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"A456ContagemResultado_Codigo_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PAC42( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSC42( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSC42( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"A456ContagemResultado_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(A456ContagemResultado_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlA456ContagemResultado_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"A456ContagemResultado_Codigo_CTRL", StringUtil.RTrim( sCtrlA456ContagemResultado_Codigo));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WEC42( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20206216171670");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("wc_viewcontagemresultadoevidencias.js", "?20206216171670");
         context.AddJavascriptSource("DVelop/Shared/yahoo.js", "");
         context.AddJavascriptSource("DVelop/Shared/dom.js", "");
         context.AddJavascriptSource("DVelop/Shared/event.js", "");
         context.AddJavascriptSource("DVelop/Shared/dragdrop.js", "");
         context.AddJavascriptSource("DVelop/Shared/container.js", "");
         context.AddJavascriptSource("DVelop/ConfirmPanel/ConfirmPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_52( )
      {
         edtavDisplay_Internalname = sPrefix+"vDISPLAY_"+sGXsfl_5_idx;
         edtavDelete_Internalname = sPrefix+"vDELETE_"+sGXsfl_5_idx;
         edtContagemResultadoEvidencia_Codigo_Internalname = sPrefix+"CONTAGEMRESULTADOEVIDENCIA_CODIGO_"+sGXsfl_5_idx;
         edtContagemResultadoEvidencia_NomeArq_Internalname = sPrefix+"CONTAGEMRESULTADOEVIDENCIA_NOMEARQ_"+sGXsfl_5_idx;
         edtTipoDocumento_Nome_Internalname = sPrefix+"TIPODOCUMENTO_NOME_"+sGXsfl_5_idx;
         edtContagemResultadoEvidencia_Data_Internalname = sPrefix+"CONTAGEMRESULTADOEVIDENCIA_DATA_"+sGXsfl_5_idx;
         edtContagemResultadoEvidencia_Entidade_Internalname = sPrefix+"CONTAGEMRESULTADOEVIDENCIA_ENTIDADE_"+sGXsfl_5_idx;
      }

      protected void SubsflControlProps_fel_52( )
      {
         edtavDisplay_Internalname = sPrefix+"vDISPLAY_"+sGXsfl_5_fel_idx;
         edtavDelete_Internalname = sPrefix+"vDELETE_"+sGXsfl_5_fel_idx;
         edtContagemResultadoEvidencia_Codigo_Internalname = sPrefix+"CONTAGEMRESULTADOEVIDENCIA_CODIGO_"+sGXsfl_5_fel_idx;
         edtContagemResultadoEvidencia_NomeArq_Internalname = sPrefix+"CONTAGEMRESULTADOEVIDENCIA_NOMEARQ_"+sGXsfl_5_fel_idx;
         edtTipoDocumento_Nome_Internalname = sPrefix+"TIPODOCUMENTO_NOME_"+sGXsfl_5_fel_idx;
         edtContagemResultadoEvidencia_Data_Internalname = sPrefix+"CONTAGEMRESULTADOEVIDENCIA_DATA_"+sGXsfl_5_fel_idx;
         edtContagemResultadoEvidencia_Entidade_Internalname = sPrefix+"CONTAGEMRESULTADOEVIDENCIA_ENTIDADE_"+sGXsfl_5_fel_idx;
      }

      protected void sendrow_52( )
      {
         SubsflControlProps_52( ) ;
         WBC40( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_5_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_5_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_5_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV5Display_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV5Display))&&String.IsNullOrEmpty(StringUtil.RTrim( AV18Display_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV5Display)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDisplay_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV5Display)) ? AV18Display_GXI : context.PathToRelativeUrl( AV5Display)),(String)edtavDisplay_Link,(String)edtavDisplay_Linktarget,(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavDisplay_Tooltiptext,(short)0,(short)-1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV5Display_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavDelete_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavDelete_Enabled!=0)&&(edtavDelete_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 7,'"+sPrefix+"',false,'',5)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV6Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV6Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV19Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV6Delete)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV6Delete)) ? AV19Delete_GXI : context.PathToRelativeUrl( AV6Delete)),(String)"",(String)"",(String)"",context.GetTheme( ),(int)edtavDelete_Visible,(short)1,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)-1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)7,(String)edtavDelete_Jsonclick,(String)"'"+sPrefix+"'"+",false,"+"'"+"e15c42_client"+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV6Delete_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "Attribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultadoEvidencia_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A586ContagemResultadoEvidencia_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A586ContagemResultadoEvidencia_Codigo), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultadoEvidencia_Codigo_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)5,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "Attribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultadoEvidencia_NomeArq_Internalname,StringUtil.RTrim( A589ContagemResultadoEvidencia_NomeArq),(String)"",(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultadoEvidencia_NomeArq_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)5,(short)1,(short)-1,(short)-1,(bool)true,(String)"NomeArq",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "Attribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtTipoDocumento_Nome_Internalname,StringUtil.RTrim( A646TipoDocumento_Nome),StringUtil.RTrim( context.localUtil.Format( A646TipoDocumento_Nome, "@!")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtTipoDocumento_Nome_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)5,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "Attribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultadoEvidencia_Data_Internalname,context.localUtil.TToC( A591ContagemResultadoEvidencia_Data, 10, 8, 0, 3, "/", ":", " "),context.localUtil.Format( A591ContagemResultadoEvidencia_Data, "99/99/99 99:99"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultadoEvidencia_Data_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)5,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "Attribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultadoEvidencia_Entidade_Internalname,StringUtil.RTrim( A1494ContagemResultadoEvidencia_Entidade),StringUtil.RTrim( context.localUtil.Format( A1494ContagemResultadoEvidencia_Entidade, "@!")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultadoEvidencia_Entidade_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)5,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADOEVIDENCIA_CODIGO"+"_"+sGXsfl_5_idx, GetSecureSignedToken( sPrefix+sGXsfl_5_idx, context.localUtil.Format( (decimal)(A586ContagemResultadoEvidencia_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADOEVIDENCIA_DATA"+"_"+sGXsfl_5_idx, GetSecureSignedToken( sPrefix+sGXsfl_5_idx, context.localUtil.Format( A591ContagemResultadoEvidencia_Data, "99/99/99 99:99")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMRESULTADOEVIDENCIA_ENTIDADE"+"_"+sGXsfl_5_idx, GetSecureSignedToken( sPrefix+sGXsfl_5_idx, StringUtil.RTrim( context.localUtil.Format( A1494ContagemResultadoEvidencia_Entidade, "@!"))));
            GridContainer.AddRow(GridRow);
            nGXsfl_5_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_5_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_5_idx+1));
            sGXsfl_5_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_5_idx), 4, 0)), 4, "0");
            SubsflControlProps_52( ) ;
         }
         /* End function sendrow_52 */
      }

      protected void init_default_properties( )
      {
         edtavDisplay_Internalname = sPrefix+"vDISPLAY";
         edtavDelete_Internalname = sPrefix+"vDELETE";
         edtContagemResultadoEvidencia_Codigo_Internalname = sPrefix+"CONTAGEMRESULTADOEVIDENCIA_CODIGO";
         edtContagemResultadoEvidencia_NomeArq_Internalname = sPrefix+"CONTAGEMRESULTADOEVIDENCIA_NOMEARQ";
         edtTipoDocumento_Nome_Internalname = sPrefix+"TIPODOCUMENTO_NOME";
         edtContagemResultadoEvidencia_Data_Internalname = sPrefix+"CONTAGEMRESULTADOEVIDENCIA_DATA";
         edtContagemResultadoEvidencia_Entidade_Internalname = sPrefix+"CONTAGEMRESULTADOEVIDENCIA_ENTIDADE";
         imgInsert_Internalname = sPrefix+"INSERT";
         tblTablemain_Internalname = sPrefix+"TABLEMAIN";
         Dvelop_confirmpanel_apagar_Internalname = sPrefix+"DVELOP_CONFIRMPANEL_APAGAR";
         Form.Internalname = sPrefix+"FORM";
         subGrid_Internalname = sPrefix+"GRID";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtContagemResultadoEvidencia_Entidade_Jsonclick = "";
         edtContagemResultadoEvidencia_Data_Jsonclick = "";
         edtTipoDocumento_Nome_Jsonclick = "";
         edtContagemResultadoEvidencia_NomeArq_Jsonclick = "";
         edtContagemResultadoEvidencia_Codigo_Jsonclick = "";
         edtavDelete_Jsonclick = "";
         edtavDelete_Enabled = 1;
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavDelete_Tooltiptext = "";
         edtavDisplay_Tooltiptext = "";
         edtavDisplay_Linktarget = "";
         edtavDisplay_Link = "";
         edtavDelete_Visible = -1;
         subGrid_Class = "WorkWith";
         subGrid_Backcolorstyle = 3;
         Dvelop_confirmpanel_apagar_Confirmtype = "1";
         Dvelop_confirmpanel_apagar_Buttoncanceltext = "Cancelar";
         Dvelop_confirmpanel_apagar_Buttonnotext = "N�o";
         Dvelop_confirmpanel_apagar_Buttonyestext = "Sim";
         Dvelop_confirmpanel_apagar_Confirmtext = "Confirma apagar o arquivo anexado?";
         Dvelop_confirmpanel_apagar_Icon = "2";
         Dvelop_confirmpanel_apagar_Title = "Aten��o";
         subGrid_Rows = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A587ContagemResultadoEvidencia_Descricao',fld:'CONTAGEMRESULTADOEVIDENCIA_DESCRICAO',pic:'',nv:''},{av:'A588ContagemResultadoEvidencia_Arquivo',fld:'CONTAGEMRESULTADOEVIDENCIA_ARQUIVO',pic:'',nv:''},{av:'A1493ContagemResultadoEvidencia_Owner',fld:'CONTAGEMRESULTADOEVIDENCIA_OWNER',pic:'ZZZZZ9',nv:0},{av:'AV13WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'sPrefix',nv:''}],oparms:[]}");
         setEventMetadata("'DOINSERT'","{handler:'E12C42',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A587ContagemResultadoEvidencia_Descricao',fld:'CONTAGEMRESULTADOEVIDENCIA_DESCRICAO',pic:'',nv:''},{av:'A588ContagemResultadoEvidencia_Arquivo',fld:'CONTAGEMRESULTADOEVIDENCIA_ARQUIVO',pic:'',nv:''},{av:'A1493ContagemResultadoEvidencia_Owner',fld:'CONTAGEMRESULTADOEVIDENCIA_OWNER',pic:'ZZZZZ9',nv:0},{av:'AV13WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'sPrefix',nv:''}],oparms:[{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("VDELETE.CLICK","{handler:'E15C42',iparms:[],oparms:[]}");
         setEventMetadata("DVELOP_CONFIRMPANEL_APAGAR.CLOSE","{handler:'E11C42',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A587ContagemResultadoEvidencia_Descricao',fld:'CONTAGEMRESULTADOEVIDENCIA_DESCRICAO',pic:'',nv:''},{av:'A588ContagemResultadoEvidencia_Arquivo',fld:'CONTAGEMRESULTADOEVIDENCIA_ARQUIVO',pic:'',nv:''},{av:'A1493ContagemResultadoEvidencia_Owner',fld:'CONTAGEMRESULTADOEVIDENCIA_OWNER',pic:'ZZZZZ9',nv:0},{av:'AV13WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'sPrefix',nv:''},{av:'Dvelop_confirmpanel_apagar_Result',ctrl:'DVELOP_CONFIRMPANEL_APAGAR',prop:'Result'},{av:'A586ContagemResultadoEvidencia_Codigo',fld:'CONTAGEMRESULTADOEVIDENCIA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("GRID_FIRSTPAGE","{handler:'subgrid_firstpage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A587ContagemResultadoEvidencia_Descricao',fld:'CONTAGEMRESULTADOEVIDENCIA_DESCRICAO',pic:'',nv:''},{av:'A588ContagemResultadoEvidencia_Arquivo',fld:'CONTAGEMRESULTADOEVIDENCIA_ARQUIVO',pic:'',nv:''},{av:'A1493ContagemResultadoEvidencia_Owner',fld:'CONTAGEMRESULTADOEVIDENCIA_OWNER',pic:'ZZZZZ9',nv:0},{av:'AV13WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'sPrefix',nv:''}],oparms:[]}");
         setEventMetadata("GRID_PREVPAGE","{handler:'subgrid_previouspage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A587ContagemResultadoEvidencia_Descricao',fld:'CONTAGEMRESULTADOEVIDENCIA_DESCRICAO',pic:'',nv:''},{av:'A588ContagemResultadoEvidencia_Arquivo',fld:'CONTAGEMRESULTADOEVIDENCIA_ARQUIVO',pic:'',nv:''},{av:'A1493ContagemResultadoEvidencia_Owner',fld:'CONTAGEMRESULTADOEVIDENCIA_OWNER',pic:'ZZZZZ9',nv:0},{av:'AV13WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'sPrefix',nv:''}],oparms:[]}");
         setEventMetadata("GRID_NEXTPAGE","{handler:'subgrid_nextpage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A587ContagemResultadoEvidencia_Descricao',fld:'CONTAGEMRESULTADOEVIDENCIA_DESCRICAO',pic:'',nv:''},{av:'A588ContagemResultadoEvidencia_Arquivo',fld:'CONTAGEMRESULTADOEVIDENCIA_ARQUIVO',pic:'',nv:''},{av:'A1493ContagemResultadoEvidencia_Owner',fld:'CONTAGEMRESULTADOEVIDENCIA_OWNER',pic:'ZZZZZ9',nv:0},{av:'AV13WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'sPrefix',nv:''}],oparms:[]}");
         setEventMetadata("GRID_LASTPAGE","{handler:'subgrid_lastpage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A587ContagemResultadoEvidencia_Descricao',fld:'CONTAGEMRESULTADOEVIDENCIA_DESCRICAO',pic:'',nv:''},{av:'A588ContagemResultadoEvidencia_Arquivo',fld:'CONTAGEMRESULTADOEVIDENCIA_ARQUIVO',pic:'',nv:''},{av:'A1493ContagemResultadoEvidencia_Owner',fld:'CONTAGEMRESULTADOEVIDENCIA_OWNER',pic:'ZZZZZ9',nv:0},{av:'AV13WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'sPrefix',nv:''}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Dvelop_confirmpanel_apagar_Result = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         A587ContagemResultadoEvidencia_Descricao = "";
         A588ContagemResultadoEvidencia_Arquivo = "";
         AV13WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GXKey = "";
         scmdbuf = "";
         H00C42_A490ContagemResultado_ContratadaCod = new int[1] ;
         H00C42_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         H00C43_A52Contratada_AreaTrabalhoCod = new int[1] ;
         H00C43_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV5Display = "";
         AV18Display_GXI = "";
         AV6Delete = "";
         AV19Delete_GXI = "";
         A646TipoDocumento_Nome = "";
         A591ContagemResultadoEvidencia_Data = (DateTime)(DateTime.MinValue);
         A1494ContagemResultadoEvidencia_Entidade = "";
         GridContainer = new GXWebGrid( context);
         H00C44_A490ContagemResultado_ContratadaCod = new int[1] ;
         H00C44_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         H00C44_A645TipoDocumento_Codigo = new int[1] ;
         H00C44_n645TipoDocumento_Codigo = new bool[] {false} ;
         H00C44_A456ContagemResultado_Codigo = new int[1] ;
         H00C44_A589ContagemResultadoEvidencia_NomeArq = new String[] {""} ;
         H00C44_n589ContagemResultadoEvidencia_NomeArq = new bool[] {false} ;
         H00C44_A590ContagemResultadoEvidencia_TipoArq = new String[] {""} ;
         H00C44_n590ContagemResultadoEvidencia_TipoArq = new bool[] {false} ;
         H00C44_A587ContagemResultadoEvidencia_Descricao = new String[] {""} ;
         H00C44_n587ContagemResultadoEvidencia_Descricao = new bool[] {false} ;
         H00C44_A591ContagemResultadoEvidencia_Data = new DateTime[] {DateTime.MinValue} ;
         H00C44_n591ContagemResultadoEvidencia_Data = new bool[] {false} ;
         H00C44_A646TipoDocumento_Nome = new String[] {""} ;
         H00C44_A586ContagemResultadoEvidencia_Codigo = new int[1] ;
         H00C44_A52Contratada_AreaTrabalhoCod = new int[1] ;
         H00C44_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         H00C44_A1493ContagemResultadoEvidencia_Owner = new int[1] ;
         H00C44_n1493ContagemResultadoEvidencia_Owner = new bool[] {false} ;
         H00C44_A588ContagemResultadoEvidencia_Arquivo = new String[] {""} ;
         H00C44_n588ContagemResultadoEvidencia_Arquivo = new bool[] {false} ;
         A589ContagemResultadoEvidencia_NomeArq = "";
         A588ContagemResultadoEvidencia_Arquivo_Filename = "";
         A590ContagemResultadoEvidencia_TipoArq = "";
         A588ContagemResultadoEvidencia_Arquivo_Filetype = "";
         GXt_char1 = "";
         H00C45_AGRID_nRecordCount = new long[1] ;
         H00C46_A490ContagemResultado_ContratadaCod = new int[1] ;
         H00C46_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         H00C47_A52Contratada_AreaTrabalhoCod = new int[1] ;
         H00C47_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         GridRow = new GXWebRow();
         AV15WebSession = context.GetSession();
         AV12ContagemResultadoEvidencia = new SdtContagemResultadoEvidencia(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         TempTags = "";
         ClassString = "";
         StyleString = "";
         imgInsert_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlA456ContagemResultado_Codigo = "";
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wc_viewcontagemresultadoevidencias__default(),
            new Object[][] {
                new Object[] {
               H00C42_A490ContagemResultado_ContratadaCod, H00C42_n490ContagemResultado_ContratadaCod
               }
               , new Object[] {
               H00C43_A52Contratada_AreaTrabalhoCod, H00C43_n52Contratada_AreaTrabalhoCod
               }
               , new Object[] {
               H00C44_A490ContagemResultado_ContratadaCod, H00C44_n490ContagemResultado_ContratadaCod, H00C44_A645TipoDocumento_Codigo, H00C44_n645TipoDocumento_Codigo, H00C44_A456ContagemResultado_Codigo, H00C44_A589ContagemResultadoEvidencia_NomeArq, H00C44_n589ContagemResultadoEvidencia_NomeArq, H00C44_A590ContagemResultadoEvidencia_TipoArq, H00C44_n590ContagemResultadoEvidencia_TipoArq, H00C44_A587ContagemResultadoEvidencia_Descricao,
               H00C44_n587ContagemResultadoEvidencia_Descricao, H00C44_A591ContagemResultadoEvidencia_Data, H00C44_n591ContagemResultadoEvidencia_Data, H00C44_A646TipoDocumento_Nome, H00C44_A586ContagemResultadoEvidencia_Codigo, H00C44_A52Contratada_AreaTrabalhoCod, H00C44_n52Contratada_AreaTrabalhoCod, H00C44_A1493ContagemResultadoEvidencia_Owner, H00C44_n1493ContagemResultadoEvidencia_Owner, H00C44_A588ContagemResultadoEvidencia_Arquivo,
               H00C44_n588ContagemResultadoEvidencia_Arquivo
               }
               , new Object[] {
               H00C45_AGRID_nRecordCount
               }
               , new Object[] {
               H00C46_A490ContagemResultado_ContratadaCod, H00C46_n490ContagemResultado_ContratadaCod
               }
               , new Object[] {
               H00C47_A52Contratada_AreaTrabalhoCod, H00C47_n52Contratada_AreaTrabalhoCod
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short nRC_GXsfl_5 ;
      private short nGXsfl_5_idx=1 ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_5_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int A456ContagemResultado_Codigo ;
      private int wcpOA456ContagemResultado_Codigo ;
      private int subGrid_Rows ;
      private int A1493ContagemResultadoEvidencia_Owner ;
      private int A490ContagemResultado_ContratadaCod ;
      private int A52Contratada_AreaTrabalhoCod ;
      private int A586ContagemResultadoEvidencia_Codigo ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int A645TipoDocumento_Codigo ;
      private int edtavDelete_Visible ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private int edtavDelete_Enabled ;
      private long GRID_nFirstRecordOnPage ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Dvelop_confirmpanel_apagar_Result ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String sGXsfl_5_idx="0001" ;
      private String GXKey ;
      private String scmdbuf ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Dvelop_confirmpanel_apagar_Title ;
      private String Dvelop_confirmpanel_apagar_Icon ;
      private String Dvelop_confirmpanel_apagar_Confirmtext ;
      private String Dvelop_confirmpanel_apagar_Buttonyestext ;
      private String Dvelop_confirmpanel_apagar_Buttonnotext ;
      private String Dvelop_confirmpanel_apagar_Buttoncanceltext ;
      private String Dvelop_confirmpanel_apagar_Confirmtype ;
      private String GX_FocusControl ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavDisplay_Internalname ;
      private String edtavDelete_Internalname ;
      private String edtContagemResultadoEvidencia_Codigo_Internalname ;
      private String A646TipoDocumento_Nome ;
      private String edtTipoDocumento_Nome_Internalname ;
      private String edtContagemResultadoEvidencia_Data_Internalname ;
      private String A1494ContagemResultadoEvidencia_Entidade ;
      private String edtContagemResultadoEvidencia_Entidade_Internalname ;
      private String A589ContagemResultadoEvidencia_NomeArq ;
      private String A588ContagemResultadoEvidencia_Arquivo_Filename ;
      private String A590ContagemResultadoEvidencia_TipoArq ;
      private String A588ContagemResultadoEvidencia_Arquivo_Filetype ;
      private String GXt_char1 ;
      private String edtavDisplay_Tooltiptext ;
      private String edtavDisplay_Link ;
      private String edtavDisplay_Linktarget ;
      private String edtavDelete_Tooltiptext ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String subGrid_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String imgInsert_Internalname ;
      private String imgInsert_Jsonclick ;
      private String sCtrlA456ContagemResultado_Codigo ;
      private String edtContagemResultadoEvidencia_NomeArq_Internalname ;
      private String sGXsfl_5_fel_idx="0001" ;
      private String edtavDelete_Jsonclick ;
      private String ROClassString ;
      private String edtContagemResultadoEvidencia_Codigo_Jsonclick ;
      private String edtContagemResultadoEvidencia_NomeArq_Jsonclick ;
      private String edtTipoDocumento_Nome_Jsonclick ;
      private String edtContagemResultadoEvidencia_Data_Jsonclick ;
      private String edtContagemResultadoEvidencia_Entidade_Jsonclick ;
      private String Dvelop_confirmpanel_apagar_Internalname ;
      private DateTime A591ContagemResultadoEvidencia_Data ;
      private bool entryPointCalled ;
      private bool n587ContagemResultadoEvidencia_Descricao ;
      private bool n588ContagemResultadoEvidencia_Arquivo ;
      private bool n1493ContagemResultadoEvidencia_Owner ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n52Contratada_AreaTrabalhoCod ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n591ContagemResultadoEvidencia_Data ;
      private bool n645TipoDocumento_Codigo ;
      private bool n589ContagemResultadoEvidencia_NomeArq ;
      private bool n590ContagemResultadoEvidencia_TipoArq ;
      private bool returnInSub ;
      private bool AV5Display_IsBlob ;
      private bool AV6Delete_IsBlob ;
      private String A587ContagemResultadoEvidencia_Descricao ;
      private String AV18Display_GXI ;
      private String AV19Delete_GXI ;
      private String AV5Display ;
      private String AV6Delete ;
      private String A588ContagemResultadoEvidencia_Arquivo ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_ContagemResultado_Codigo ;
      private IDataStoreProvider pr_default ;
      private int[] H00C42_A490ContagemResultado_ContratadaCod ;
      private bool[] H00C42_n490ContagemResultado_ContratadaCod ;
      private int[] H00C43_A52Contratada_AreaTrabalhoCod ;
      private bool[] H00C43_n52Contratada_AreaTrabalhoCod ;
      private int[] H00C44_A490ContagemResultado_ContratadaCod ;
      private bool[] H00C44_n490ContagemResultado_ContratadaCod ;
      private int[] H00C44_A645TipoDocumento_Codigo ;
      private bool[] H00C44_n645TipoDocumento_Codigo ;
      private int[] H00C44_A456ContagemResultado_Codigo ;
      private String[] H00C44_A589ContagemResultadoEvidencia_NomeArq ;
      private bool[] H00C44_n589ContagemResultadoEvidencia_NomeArq ;
      private String[] H00C44_A590ContagemResultadoEvidencia_TipoArq ;
      private bool[] H00C44_n590ContagemResultadoEvidencia_TipoArq ;
      private String[] H00C44_A587ContagemResultadoEvidencia_Descricao ;
      private bool[] H00C44_n587ContagemResultadoEvidencia_Descricao ;
      private DateTime[] H00C44_A591ContagemResultadoEvidencia_Data ;
      private bool[] H00C44_n591ContagemResultadoEvidencia_Data ;
      private String[] H00C44_A646TipoDocumento_Nome ;
      private int[] H00C44_A586ContagemResultadoEvidencia_Codigo ;
      private int[] H00C44_A52Contratada_AreaTrabalhoCod ;
      private bool[] H00C44_n52Contratada_AreaTrabalhoCod ;
      private int[] H00C44_A1493ContagemResultadoEvidencia_Owner ;
      private bool[] H00C44_n1493ContagemResultadoEvidencia_Owner ;
      private String[] H00C44_A588ContagemResultadoEvidencia_Arquivo ;
      private bool[] H00C44_n588ContagemResultadoEvidencia_Arquivo ;
      private long[] H00C45_AGRID_nRecordCount ;
      private int[] H00C46_A490ContagemResultado_ContratadaCod ;
      private bool[] H00C46_n490ContagemResultado_ContratadaCod ;
      private int[] H00C47_A52Contratada_AreaTrabalhoCod ;
      private bool[] H00C47_n52Contratada_AreaTrabalhoCod ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private IGxSession AV15WebSession ;
      private SdtContagemResultadoEvidencia AV12ContagemResultadoEvidencia ;
      private wwpbaseobjects.SdtWWPContext AV13WWPContext ;
   }

   public class wc_viewcontagemresultadoevidencias__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00C42 ;
          prmH00C42 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00C43 ;
          prmH00C43 = new Object[] {
          new Object[] {"@ContagemResultado_ContratadaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00C44 ;
          prmH00C44 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00C45 ;
          prmH00C45 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00C46 ;
          prmH00C46 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00C47 ;
          prmH00C47 = new Object[] {
          new Object[] {"@ContagemResultado_ContratadaCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00C42", "SELECT [ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00C42,1,0,true,false )
             ,new CursorDef("H00C43", "SELECT [Contratada_AreaTrabalhoCod] FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_Codigo] = @ContagemResultado_ContratadaCod ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00C43,1,0,true,false )
             ,new CursorDef("H00C44", "SELECT * FROM (SELECT  T3.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T1.[TipoDocumento_Codigo], T1.[ContagemResultado_Codigo], T1.[ContagemResultadoEvidencia_NomeArq], T1.[ContagemResultadoEvidencia_TipoArq], T1.[ContagemResultadoEvidencia_Descricao], T1.[ContagemResultadoEvidencia_Data], T2.[TipoDocumento_Nome], T1.[ContagemResultadoEvidencia_Codigo], T4.[Contratada_AreaTrabalhoCod], T1.[ContagemResultadoEvidencia_Owner], T1.[ContagemResultadoEvidencia_Arquivo], ROW_NUMBER() OVER ( ORDER BY T1.[ContagemResultado_Codigo] ) AS GX_ROW_NUMBER FROM ((([ContagemResultadoEvidencia] T1 WITH (NOLOCK) LEFT JOIN [TipoDocumento] T2 WITH (NOLOCK) ON T2.[TipoDocumento_Codigo] = T1.[TipoDocumento_Codigo]) INNER JOIN [ContagemResultado] T3 WITH (NOLOCK) ON T3.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) LEFT JOIN [Contratada] T4 WITH (NOLOCK) ON T4.[Contratada_Codigo] = T3.[ContagemResultado_ContratadaCod]) WHERE T1.[ContagemResultado_Codigo] = @ContagemResultado_Codigo) AS GX_CTE WHERE GX_ROW_NUMBER BETWEEN @GXPagingFrom2 AND @GXPagingTo2 OR @GXPagingTo2 < @GXPagingFrom2 AND GX_ROW_NUMBER >= @GXPagingFrom2",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00C44,11,0,true,false )
             ,new CursorDef("H00C45", "SELECT COUNT(*) FROM ((([ContagemResultadoEvidencia] T1 WITH (NOLOCK) LEFT JOIN [TipoDocumento] T4 WITH (NOLOCK) ON T4.[TipoDocumento_Codigo] = T1.[TipoDocumento_Codigo]) INNER JOIN [ContagemResultado] T2 WITH (NOLOCK) ON T2.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) LEFT JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T2.[ContagemResultado_ContratadaCod]) WHERE T1.[ContagemResultado_Codigo] = @ContagemResultado_Codigo ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00C45,1,0,true,false )
             ,new CursorDef("H00C46", "SELECT [ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00C46,1,0,true,false )
             ,new CursorDef("H00C47", "SELECT [Contratada_AreaTrabalhoCod] FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_Codigo] = @ContagemResultado_ContratadaCod ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00C47,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((String[]) buf[5])[0] = rslt.getString(4, 50) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getString(5, 10) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getLongVarchar(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((DateTime[]) buf[11])[0] = rslt.getGXDateTime(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((String[]) buf[13])[0] = rslt.getString(8, 50) ;
                ((int[]) buf[14])[0] = rslt.getInt(9) ;
                ((int[]) buf[15])[0] = rslt.getInt(10) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(10);
                ((int[]) buf[17])[0] = rslt.getInt(11) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(11);
                ((String[]) buf[19])[0] = rslt.getBLOBFile(12, rslt.getString(5, 10), rslt.getString(4, 50)) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(12);
                return;
             case 3 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
       }
    }

 }

}
