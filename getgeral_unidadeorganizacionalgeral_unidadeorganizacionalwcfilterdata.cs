/*
               File: GetGeral_UnidadeOrganizacionalGeral_UnidadeOrganizacionalWCFilterData
        Description: Get Geral_Unidade Organizacional Geral_Unidade Organizacional WCFilter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:54:3.54
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getgeral_unidadeorganizacionalgeral_unidadeorganizacionalwcfilterdata : GXProcedure
   {
      public getgeral_unidadeorganizacionalgeral_unidadeorganizacionalwcfilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getgeral_unidadeorganizacionalgeral_unidadeorganizacionalwcfilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV18DDOName = aP0_DDOName;
         this.AV16SearchTxt = aP1_SearchTxt;
         this.AV17SearchTxtTo = aP2_SearchTxtTo;
         this.AV22OptionsJson = "" ;
         this.AV25OptionsDescJson = "" ;
         this.AV27OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV22OptionsJson;
         aP4_OptionsDescJson=this.AV25OptionsDescJson;
         aP5_OptionIndexesJson=this.AV27OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV18DDOName = aP0_DDOName;
         this.AV16SearchTxt = aP1_SearchTxt;
         this.AV17SearchTxtTo = aP2_SearchTxtTo;
         this.AV22OptionsJson = "" ;
         this.AV25OptionsDescJson = "" ;
         this.AV27OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV22OptionsJson;
         aP4_OptionsDescJson=this.AV25OptionsDescJson;
         aP5_OptionIndexesJson=this.AV27OptionIndexesJson;
         return AV27OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getgeral_unidadeorganizacionalgeral_unidadeorganizacionalwcfilterdata objgetgeral_unidadeorganizacionalgeral_unidadeorganizacionalwcfilterdata;
         objgetgeral_unidadeorganizacionalgeral_unidadeorganizacionalwcfilterdata = new getgeral_unidadeorganizacionalgeral_unidadeorganizacionalwcfilterdata();
         objgetgeral_unidadeorganizacionalgeral_unidadeorganizacionalwcfilterdata.AV18DDOName = aP0_DDOName;
         objgetgeral_unidadeorganizacionalgeral_unidadeorganizacionalwcfilterdata.AV16SearchTxt = aP1_SearchTxt;
         objgetgeral_unidadeorganizacionalgeral_unidadeorganizacionalwcfilterdata.AV17SearchTxtTo = aP2_SearchTxtTo;
         objgetgeral_unidadeorganizacionalgeral_unidadeorganizacionalwcfilterdata.AV22OptionsJson = "" ;
         objgetgeral_unidadeorganizacionalgeral_unidadeorganizacionalwcfilterdata.AV25OptionsDescJson = "" ;
         objgetgeral_unidadeorganizacionalgeral_unidadeorganizacionalwcfilterdata.AV27OptionIndexesJson = "" ;
         objgetgeral_unidadeorganizacionalgeral_unidadeorganizacionalwcfilterdata.context.SetSubmitInitialConfig(context);
         objgetgeral_unidadeorganizacionalgeral_unidadeorganizacionalwcfilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetgeral_unidadeorganizacionalgeral_unidadeorganizacionalwcfilterdata);
         aP3_OptionsJson=this.AV22OptionsJson;
         aP4_OptionsDescJson=this.AV25OptionsDescJson;
         aP5_OptionIndexesJson=this.AV27OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getgeral_unidadeorganizacionalgeral_unidadeorganizacionalwcfilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV21Options = (IGxCollection)(new GxSimpleCollection());
         AV24OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV26OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV18DDOName), "DDO_UNIDADEORGANIZACIONAL_NOME") == 0 )
         {
            /* Execute user subroutine: 'LOADUNIDADEORGANIZACIONAL_NOMEOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV18DDOName), "DDO_ESTADO_UF") == 0 )
         {
            /* Execute user subroutine: 'LOADESTADO_UFOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV22OptionsJson = AV21Options.ToJSonString(false);
         AV25OptionsDescJson = AV24OptionsDesc.ToJSonString(false);
         AV27OptionIndexesJson = AV26OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV29Session.Get("Geral_UnidadeOrganizacionalGeral_UnidadeOrganizacionalWCGridState"), "") == 0 )
         {
            AV31GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "Geral_UnidadeOrganizacionalGeral_UnidadeOrganizacionalWCGridState"), "");
         }
         else
         {
            AV31GridState.FromXml(AV29Session.Get("Geral_UnidadeOrganizacionalGeral_UnidadeOrganizacionalWCGridState"), "");
         }
         AV46GXV1 = 1;
         while ( AV46GXV1 <= AV31GridState.gxTpr_Filtervalues.Count )
         {
            AV32GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV31GridState.gxTpr_Filtervalues.Item(AV46GXV1));
            if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "UNIDADEORGANIZACIONAL_ATIVO") == 0 )
            {
               AV34UnidadeOrganizacional_Ativo = BooleanUtil.Val( AV32GridStateFilterValue.gxTpr_Value);
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFUNIDADEORGANIZACIONAL_NOME") == 0 )
            {
               AV10TFUnidadeOrganizacional_Nome = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFUNIDADEORGANIZACIONAL_NOME_SEL") == 0 )
            {
               AV11TFUnidadeOrganizacional_Nome_Sel = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFTPUO_CODIGO") == 0 )
            {
               AV12TFTpUo_Codigo = (int)(NumberUtil.Val( AV32GridStateFilterValue.gxTpr_Value, "."));
               AV13TFTpUo_Codigo_To = (int)(NumberUtil.Val( AV32GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFESTADO_UF") == 0 )
            {
               AV14TFEstado_UF = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFESTADO_UF_SEL") == 0 )
            {
               AV15TFEstado_UF_Sel = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "PARM_&UNIDADEORGANIZACIONAL_VINCULADA") == 0 )
            {
               AV43UnidadeOrganizacional_Vinculada = (int)(NumberUtil.Val( AV32GridStateFilterValue.gxTpr_Value, "."));
            }
            AV46GXV1 = (int)(AV46GXV1+1);
         }
         if ( AV31GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV33GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV31GridState.gxTpr_Dynamicfilters.Item(1));
            AV35DynamicFiltersSelector1 = AV33GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV35DynamicFiltersSelector1, "UNIDADEORGANIZACIONAL_NOME") == 0 )
            {
               AV36UnidadeOrganizacional_Nome1 = AV33GridStateDynamicFilter.gxTpr_Value;
            }
            if ( AV31GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV37DynamicFiltersEnabled2 = true;
               AV33GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV31GridState.gxTpr_Dynamicfilters.Item(2));
               AV38DynamicFiltersSelector2 = AV33GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV38DynamicFiltersSelector2, "UNIDADEORGANIZACIONAL_NOME") == 0 )
               {
                  AV39UnidadeOrganizacional_Nome2 = AV33GridStateDynamicFilter.gxTpr_Value;
               }
               if ( AV31GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV40DynamicFiltersEnabled3 = true;
                  AV33GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV31GridState.gxTpr_Dynamicfilters.Item(3));
                  AV41DynamicFiltersSelector3 = AV33GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV41DynamicFiltersSelector3, "UNIDADEORGANIZACIONAL_NOME") == 0 )
                  {
                     AV42UnidadeOrganizacional_Nome3 = AV33GridStateDynamicFilter.gxTpr_Value;
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADUNIDADEORGANIZACIONAL_NOMEOPTIONS' Routine */
         AV10TFUnidadeOrganizacional_Nome = AV16SearchTxt;
         AV11TFUnidadeOrganizacional_Nome_Sel = "";
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV35DynamicFiltersSelector1 ,
                                              AV36UnidadeOrganizacional_Nome1 ,
                                              AV37DynamicFiltersEnabled2 ,
                                              AV38DynamicFiltersSelector2 ,
                                              AV39UnidadeOrganizacional_Nome2 ,
                                              AV40DynamicFiltersEnabled3 ,
                                              AV41DynamicFiltersSelector3 ,
                                              AV42UnidadeOrganizacional_Nome3 ,
                                              AV11TFUnidadeOrganizacional_Nome_Sel ,
                                              AV10TFUnidadeOrganizacional_Nome ,
                                              AV12TFTpUo_Codigo ,
                                              AV13TFTpUo_Codigo_To ,
                                              AV15TFEstado_UF_Sel ,
                                              AV14TFEstado_UF ,
                                              A612UnidadeOrganizacional_Nome ,
                                              A609TpUo_Codigo ,
                                              A23Estado_UF ,
                                              A629UnidadeOrganizacional_Ativo ,
                                              AV43UnidadeOrganizacional_Vinculada ,
                                              A613UnidadeOrganizacional_Vinculada },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.BOOLEAN
                                              }
         });
         lV36UnidadeOrganizacional_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV36UnidadeOrganizacional_Nome1), 50, "%");
         lV39UnidadeOrganizacional_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV39UnidadeOrganizacional_Nome2), 50, "%");
         lV42UnidadeOrganizacional_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV42UnidadeOrganizacional_Nome3), 50, "%");
         lV10TFUnidadeOrganizacional_Nome = StringUtil.PadR( StringUtil.RTrim( AV10TFUnidadeOrganizacional_Nome), 50, "%");
         lV14TFEstado_UF = StringUtil.PadR( StringUtil.RTrim( AV14TFEstado_UF), 2, "%");
         /* Using cursor P00NP2 */
         pr_default.execute(0, new Object[] {AV43UnidadeOrganizacional_Vinculada, lV36UnidadeOrganizacional_Nome1, lV39UnidadeOrganizacional_Nome2, lV42UnidadeOrganizacional_Nome3, lV10TFUnidadeOrganizacional_Nome, AV11TFUnidadeOrganizacional_Nome_Sel, AV12TFTpUo_Codigo, AV13TFTpUo_Codigo_To, lV14TFEstado_UF, AV15TFEstado_UF_Sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKNP2 = false;
            A613UnidadeOrganizacional_Vinculada = P00NP2_A613UnidadeOrganizacional_Vinculada[0];
            n613UnidadeOrganizacional_Vinculada = P00NP2_n613UnidadeOrganizacional_Vinculada[0];
            A629UnidadeOrganizacional_Ativo = P00NP2_A629UnidadeOrganizacional_Ativo[0];
            A612UnidadeOrganizacional_Nome = P00NP2_A612UnidadeOrganizacional_Nome[0];
            A23Estado_UF = P00NP2_A23Estado_UF[0];
            A609TpUo_Codigo = P00NP2_A609TpUo_Codigo[0];
            A611UnidadeOrganizacional_Codigo = P00NP2_A611UnidadeOrganizacional_Codigo[0];
            AV28count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( P00NP2_A613UnidadeOrganizacional_Vinculada[0] == A613UnidadeOrganizacional_Vinculada ) && ( StringUtil.StrCmp(P00NP2_A612UnidadeOrganizacional_Nome[0], A612UnidadeOrganizacional_Nome) == 0 ) )
            {
               BRKNP2 = false;
               A611UnidadeOrganizacional_Codigo = P00NP2_A611UnidadeOrganizacional_Codigo[0];
               AV28count = (long)(AV28count+1);
               BRKNP2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A612UnidadeOrganizacional_Nome)) )
            {
               AV20Option = A612UnidadeOrganizacional_Nome;
               AV21Options.Add(AV20Option, 0);
               AV26OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV28count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV21Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKNP2 )
            {
               BRKNP2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADESTADO_UFOPTIONS' Routine */
         AV14TFEstado_UF = AV16SearchTxt;
         AV15TFEstado_UF_Sel = "";
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV35DynamicFiltersSelector1 ,
                                              AV36UnidadeOrganizacional_Nome1 ,
                                              AV37DynamicFiltersEnabled2 ,
                                              AV38DynamicFiltersSelector2 ,
                                              AV39UnidadeOrganizacional_Nome2 ,
                                              AV40DynamicFiltersEnabled3 ,
                                              AV41DynamicFiltersSelector3 ,
                                              AV42UnidadeOrganizacional_Nome3 ,
                                              AV11TFUnidadeOrganizacional_Nome_Sel ,
                                              AV10TFUnidadeOrganizacional_Nome ,
                                              AV12TFTpUo_Codigo ,
                                              AV13TFTpUo_Codigo_To ,
                                              AV15TFEstado_UF_Sel ,
                                              AV14TFEstado_UF ,
                                              A612UnidadeOrganizacional_Nome ,
                                              A609TpUo_Codigo ,
                                              A23Estado_UF ,
                                              A629UnidadeOrganizacional_Ativo ,
                                              AV43UnidadeOrganizacional_Vinculada ,
                                              A613UnidadeOrganizacional_Vinculada },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.BOOLEAN
                                              }
         });
         lV36UnidadeOrganizacional_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV36UnidadeOrganizacional_Nome1), 50, "%");
         lV39UnidadeOrganizacional_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV39UnidadeOrganizacional_Nome2), 50, "%");
         lV42UnidadeOrganizacional_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV42UnidadeOrganizacional_Nome3), 50, "%");
         lV10TFUnidadeOrganizacional_Nome = StringUtil.PadR( StringUtil.RTrim( AV10TFUnidadeOrganizacional_Nome), 50, "%");
         lV14TFEstado_UF = StringUtil.PadR( StringUtil.RTrim( AV14TFEstado_UF), 2, "%");
         /* Using cursor P00NP3 */
         pr_default.execute(1, new Object[] {AV43UnidadeOrganizacional_Vinculada, lV36UnidadeOrganizacional_Nome1, lV39UnidadeOrganizacional_Nome2, lV42UnidadeOrganizacional_Nome3, lV10TFUnidadeOrganizacional_Nome, AV11TFUnidadeOrganizacional_Nome_Sel, AV12TFTpUo_Codigo, AV13TFTpUo_Codigo_To, lV14TFEstado_UF, AV15TFEstado_UF_Sel});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKNP4 = false;
            A613UnidadeOrganizacional_Vinculada = P00NP3_A613UnidadeOrganizacional_Vinculada[0];
            n613UnidadeOrganizacional_Vinculada = P00NP3_n613UnidadeOrganizacional_Vinculada[0];
            A629UnidadeOrganizacional_Ativo = P00NP3_A629UnidadeOrganizacional_Ativo[0];
            A23Estado_UF = P00NP3_A23Estado_UF[0];
            A609TpUo_Codigo = P00NP3_A609TpUo_Codigo[0];
            A612UnidadeOrganizacional_Nome = P00NP3_A612UnidadeOrganizacional_Nome[0];
            A611UnidadeOrganizacional_Codigo = P00NP3_A611UnidadeOrganizacional_Codigo[0];
            AV28count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( P00NP3_A613UnidadeOrganizacional_Vinculada[0] == A613UnidadeOrganizacional_Vinculada ) && ( StringUtil.StrCmp(P00NP3_A23Estado_UF[0], A23Estado_UF) == 0 ) )
            {
               BRKNP4 = false;
               A611UnidadeOrganizacional_Codigo = P00NP3_A611UnidadeOrganizacional_Codigo[0];
               AV28count = (long)(AV28count+1);
               BRKNP4 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A23Estado_UF)) )
            {
               AV20Option = A23Estado_UF;
               AV21Options.Add(AV20Option, 0);
               AV26OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV28count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV21Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKNP4 )
            {
               BRKNP4 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV21Options = new GxSimpleCollection();
         AV24OptionsDesc = new GxSimpleCollection();
         AV26OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV29Session = context.GetSession();
         AV31GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV32GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV34UnidadeOrganizacional_Ativo = true;
         AV10TFUnidadeOrganizacional_Nome = "";
         AV11TFUnidadeOrganizacional_Nome_Sel = "";
         AV14TFEstado_UF = "";
         AV15TFEstado_UF_Sel = "";
         AV33GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV35DynamicFiltersSelector1 = "";
         AV36UnidadeOrganizacional_Nome1 = "";
         AV38DynamicFiltersSelector2 = "";
         AV39UnidadeOrganizacional_Nome2 = "";
         AV41DynamicFiltersSelector3 = "";
         AV42UnidadeOrganizacional_Nome3 = "";
         scmdbuf = "";
         lV36UnidadeOrganizacional_Nome1 = "";
         lV39UnidadeOrganizacional_Nome2 = "";
         lV42UnidadeOrganizacional_Nome3 = "";
         lV10TFUnidadeOrganizacional_Nome = "";
         lV14TFEstado_UF = "";
         A612UnidadeOrganizacional_Nome = "";
         A23Estado_UF = "";
         P00NP2_A613UnidadeOrganizacional_Vinculada = new int[1] ;
         P00NP2_n613UnidadeOrganizacional_Vinculada = new bool[] {false} ;
         P00NP2_A629UnidadeOrganizacional_Ativo = new bool[] {false} ;
         P00NP2_A612UnidadeOrganizacional_Nome = new String[] {""} ;
         P00NP2_A23Estado_UF = new String[] {""} ;
         P00NP2_A609TpUo_Codigo = new int[1] ;
         P00NP2_A611UnidadeOrganizacional_Codigo = new int[1] ;
         AV20Option = "";
         P00NP3_A613UnidadeOrganizacional_Vinculada = new int[1] ;
         P00NP3_n613UnidadeOrganizacional_Vinculada = new bool[] {false} ;
         P00NP3_A629UnidadeOrganizacional_Ativo = new bool[] {false} ;
         P00NP3_A23Estado_UF = new String[] {""} ;
         P00NP3_A609TpUo_Codigo = new int[1] ;
         P00NP3_A612UnidadeOrganizacional_Nome = new String[] {""} ;
         P00NP3_A611UnidadeOrganizacional_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getgeral_unidadeorganizacionalgeral_unidadeorganizacionalwcfilterdata__default(),
            new Object[][] {
                new Object[] {
               P00NP2_A613UnidadeOrganizacional_Vinculada, P00NP2_n613UnidadeOrganizacional_Vinculada, P00NP2_A629UnidadeOrganizacional_Ativo, P00NP2_A612UnidadeOrganizacional_Nome, P00NP2_A23Estado_UF, P00NP2_A609TpUo_Codigo, P00NP2_A611UnidadeOrganizacional_Codigo
               }
               , new Object[] {
               P00NP3_A613UnidadeOrganizacional_Vinculada, P00NP3_n613UnidadeOrganizacional_Vinculada, P00NP3_A629UnidadeOrganizacional_Ativo, P00NP3_A23Estado_UF, P00NP3_A609TpUo_Codigo, P00NP3_A612UnidadeOrganizacional_Nome, P00NP3_A611UnidadeOrganizacional_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV46GXV1 ;
      private int AV12TFTpUo_Codigo ;
      private int AV13TFTpUo_Codigo_To ;
      private int AV43UnidadeOrganizacional_Vinculada ;
      private int A609TpUo_Codigo ;
      private int A613UnidadeOrganizacional_Vinculada ;
      private int A611UnidadeOrganizacional_Codigo ;
      private long AV28count ;
      private String AV10TFUnidadeOrganizacional_Nome ;
      private String AV11TFUnidadeOrganizacional_Nome_Sel ;
      private String AV14TFEstado_UF ;
      private String AV15TFEstado_UF_Sel ;
      private String AV36UnidadeOrganizacional_Nome1 ;
      private String AV39UnidadeOrganizacional_Nome2 ;
      private String AV42UnidadeOrganizacional_Nome3 ;
      private String scmdbuf ;
      private String lV36UnidadeOrganizacional_Nome1 ;
      private String lV39UnidadeOrganizacional_Nome2 ;
      private String lV42UnidadeOrganizacional_Nome3 ;
      private String lV10TFUnidadeOrganizacional_Nome ;
      private String lV14TFEstado_UF ;
      private String A612UnidadeOrganizacional_Nome ;
      private String A23Estado_UF ;
      private bool returnInSub ;
      private bool AV34UnidadeOrganizacional_Ativo ;
      private bool AV37DynamicFiltersEnabled2 ;
      private bool AV40DynamicFiltersEnabled3 ;
      private bool A629UnidadeOrganizacional_Ativo ;
      private bool BRKNP2 ;
      private bool n613UnidadeOrganizacional_Vinculada ;
      private bool BRKNP4 ;
      private String AV27OptionIndexesJson ;
      private String AV22OptionsJson ;
      private String AV25OptionsDescJson ;
      private String AV18DDOName ;
      private String AV16SearchTxt ;
      private String AV17SearchTxtTo ;
      private String AV35DynamicFiltersSelector1 ;
      private String AV38DynamicFiltersSelector2 ;
      private String AV41DynamicFiltersSelector3 ;
      private String AV20Option ;
      private IGxSession AV29Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00NP2_A613UnidadeOrganizacional_Vinculada ;
      private bool[] P00NP2_n613UnidadeOrganizacional_Vinculada ;
      private bool[] P00NP2_A629UnidadeOrganizacional_Ativo ;
      private String[] P00NP2_A612UnidadeOrganizacional_Nome ;
      private String[] P00NP2_A23Estado_UF ;
      private int[] P00NP2_A609TpUo_Codigo ;
      private int[] P00NP2_A611UnidadeOrganizacional_Codigo ;
      private int[] P00NP3_A613UnidadeOrganizacional_Vinculada ;
      private bool[] P00NP3_n613UnidadeOrganizacional_Vinculada ;
      private bool[] P00NP3_A629UnidadeOrganizacional_Ativo ;
      private String[] P00NP3_A23Estado_UF ;
      private int[] P00NP3_A609TpUo_Codigo ;
      private String[] P00NP3_A612UnidadeOrganizacional_Nome ;
      private int[] P00NP3_A611UnidadeOrganizacional_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV21Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV24OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV26OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV31GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV32GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV33GridStateDynamicFilter ;
   }

   public class getgeral_unidadeorganizacionalgeral_unidadeorganizacionalwcfilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00NP2( IGxContext context ,
                                             String AV35DynamicFiltersSelector1 ,
                                             String AV36UnidadeOrganizacional_Nome1 ,
                                             bool AV37DynamicFiltersEnabled2 ,
                                             String AV38DynamicFiltersSelector2 ,
                                             String AV39UnidadeOrganizacional_Nome2 ,
                                             bool AV40DynamicFiltersEnabled3 ,
                                             String AV41DynamicFiltersSelector3 ,
                                             String AV42UnidadeOrganizacional_Nome3 ,
                                             String AV11TFUnidadeOrganizacional_Nome_Sel ,
                                             String AV10TFUnidadeOrganizacional_Nome ,
                                             int AV12TFTpUo_Codigo ,
                                             int AV13TFTpUo_Codigo_To ,
                                             String AV15TFEstado_UF_Sel ,
                                             String AV14TFEstado_UF ,
                                             String A612UnidadeOrganizacional_Nome ,
                                             int A609TpUo_Codigo ,
                                             String A23Estado_UF ,
                                             bool A629UnidadeOrganizacional_Ativo ,
                                             int AV43UnidadeOrganizacional_Vinculada ,
                                             int A613UnidadeOrganizacional_Vinculada )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [10] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT [UnidadeOrganizacional_Vinculada], [UnidadeOrganizacional_Ativo], [UnidadeOrganizacional_Nome], [Estado_UF], [TpUo_Codigo], [UnidadeOrganizacional_Codigo] FROM [Geral_UnidadeOrganizacional] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([UnidadeOrganizacional_Vinculada] = @AV43UnidadeOrganizacional_Vinculada)";
         scmdbuf = scmdbuf + " and ([UnidadeOrganizacional_Ativo] = 1)";
         if ( ( StringUtil.StrCmp(AV35DynamicFiltersSelector1, "UNIDADEORGANIZACIONAL_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV36UnidadeOrganizacional_Nome1)) ) )
         {
            sWhereString = sWhereString + " and ([UnidadeOrganizacional_Nome] like '%' + @lV36UnidadeOrganizacional_Nome1 + '%')";
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( AV37DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV38DynamicFiltersSelector2, "UNIDADEORGANIZACIONAL_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV39UnidadeOrganizacional_Nome2)) ) )
         {
            sWhereString = sWhereString + " and ([UnidadeOrganizacional_Nome] like '%' + @lV39UnidadeOrganizacional_Nome2 + '%')";
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( AV40DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV41DynamicFiltersSelector3, "UNIDADEORGANIZACIONAL_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42UnidadeOrganizacional_Nome3)) ) )
         {
            sWhereString = sWhereString + " and ([UnidadeOrganizacional_Nome] like '%' + @lV42UnidadeOrganizacional_Nome3 + '%')";
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFUnidadeOrganizacional_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFUnidadeOrganizacional_Nome)) ) )
         {
            sWhereString = sWhereString + " and ([UnidadeOrganizacional_Nome] like @lV10TFUnidadeOrganizacional_Nome)";
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFUnidadeOrganizacional_Nome_Sel)) )
         {
            sWhereString = sWhereString + " and ([UnidadeOrganizacional_Nome] = @AV11TFUnidadeOrganizacional_Nome_Sel)";
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( ! (0==AV12TFTpUo_Codigo) )
         {
            sWhereString = sWhereString + " and ([TpUo_Codigo] >= @AV12TFTpUo_Codigo)";
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( ! (0==AV13TFTpUo_Codigo_To) )
         {
            sWhereString = sWhereString + " and ([TpUo_Codigo] <= @AV13TFTpUo_Codigo_To)";
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFEstado_UF_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFEstado_UF)) ) )
         {
            sWhereString = sWhereString + " and ([Estado_UF] like @lV14TFEstado_UF)";
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFEstado_UF_Sel)) )
         {
            sWhereString = sWhereString + " and ([Estado_UF] = @AV15TFEstado_UF_Sel)";
         }
         else
         {
            GXv_int1[9] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [UnidadeOrganizacional_Vinculada], [UnidadeOrganizacional_Nome]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00NP3( IGxContext context ,
                                             String AV35DynamicFiltersSelector1 ,
                                             String AV36UnidadeOrganizacional_Nome1 ,
                                             bool AV37DynamicFiltersEnabled2 ,
                                             String AV38DynamicFiltersSelector2 ,
                                             String AV39UnidadeOrganizacional_Nome2 ,
                                             bool AV40DynamicFiltersEnabled3 ,
                                             String AV41DynamicFiltersSelector3 ,
                                             String AV42UnidadeOrganizacional_Nome3 ,
                                             String AV11TFUnidadeOrganizacional_Nome_Sel ,
                                             String AV10TFUnidadeOrganizacional_Nome ,
                                             int AV12TFTpUo_Codigo ,
                                             int AV13TFTpUo_Codigo_To ,
                                             String AV15TFEstado_UF_Sel ,
                                             String AV14TFEstado_UF ,
                                             String A612UnidadeOrganizacional_Nome ,
                                             int A609TpUo_Codigo ,
                                             String A23Estado_UF ,
                                             bool A629UnidadeOrganizacional_Ativo ,
                                             int AV43UnidadeOrganizacional_Vinculada ,
                                             int A613UnidadeOrganizacional_Vinculada )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [10] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT [UnidadeOrganizacional_Vinculada], [UnidadeOrganizacional_Ativo], [Estado_UF], [TpUo_Codigo], [UnidadeOrganizacional_Nome], [UnidadeOrganizacional_Codigo] FROM [Geral_UnidadeOrganizacional] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([UnidadeOrganizacional_Vinculada] = @AV43UnidadeOrganizacional_Vinculada)";
         scmdbuf = scmdbuf + " and ([UnidadeOrganizacional_Ativo] = 1)";
         if ( ( StringUtil.StrCmp(AV35DynamicFiltersSelector1, "UNIDADEORGANIZACIONAL_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV36UnidadeOrganizacional_Nome1)) ) )
         {
            sWhereString = sWhereString + " and ([UnidadeOrganizacional_Nome] like '%' + @lV36UnidadeOrganizacional_Nome1 + '%')";
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( AV37DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV38DynamicFiltersSelector2, "UNIDADEORGANIZACIONAL_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV39UnidadeOrganizacional_Nome2)) ) )
         {
            sWhereString = sWhereString + " and ([UnidadeOrganizacional_Nome] like '%' + @lV39UnidadeOrganizacional_Nome2 + '%')";
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( AV40DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV41DynamicFiltersSelector3, "UNIDADEORGANIZACIONAL_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42UnidadeOrganizacional_Nome3)) ) )
         {
            sWhereString = sWhereString + " and ([UnidadeOrganizacional_Nome] like '%' + @lV42UnidadeOrganizacional_Nome3 + '%')";
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFUnidadeOrganizacional_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFUnidadeOrganizacional_Nome)) ) )
         {
            sWhereString = sWhereString + " and ([UnidadeOrganizacional_Nome] like @lV10TFUnidadeOrganizacional_Nome)";
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFUnidadeOrganizacional_Nome_Sel)) )
         {
            sWhereString = sWhereString + " and ([UnidadeOrganizacional_Nome] = @AV11TFUnidadeOrganizacional_Nome_Sel)";
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( ! (0==AV12TFTpUo_Codigo) )
         {
            sWhereString = sWhereString + " and ([TpUo_Codigo] >= @AV12TFTpUo_Codigo)";
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( ! (0==AV13TFTpUo_Codigo_To) )
         {
            sWhereString = sWhereString + " and ([TpUo_Codigo] <= @AV13TFTpUo_Codigo_To)";
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFEstado_UF_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFEstado_UF)) ) )
         {
            sWhereString = sWhereString + " and ([Estado_UF] like @lV14TFEstado_UF)";
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFEstado_UF_Sel)) )
         {
            sWhereString = sWhereString + " and ([Estado_UF] = @AV15TFEstado_UF_Sel)";
         }
         else
         {
            GXv_int3[9] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [UnidadeOrganizacional_Vinculada], [Estado_UF]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00NP2(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (bool)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (int)dynConstraints[10] , (int)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (int)dynConstraints[15] , (String)dynConstraints[16] , (bool)dynConstraints[17] , (int)dynConstraints[18] , (int)dynConstraints[19] );
               case 1 :
                     return conditional_P00NP3(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (bool)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (int)dynConstraints[10] , (int)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (int)dynConstraints[15] , (String)dynConstraints[16] , (bool)dynConstraints[17] , (int)dynConstraints[18] , (int)dynConstraints[19] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00NP2 ;
          prmP00NP2 = new Object[] {
          new Object[] {"@AV43UnidadeOrganizacional_Vinculada",SqlDbType.Int,6,0} ,
          new Object[] {"@lV36UnidadeOrganizacional_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV39UnidadeOrganizacional_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV42UnidadeOrganizacional_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV10TFUnidadeOrganizacional_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV11TFUnidadeOrganizacional_Nome_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@AV12TFTpUo_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV13TFTpUo_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV14TFEstado_UF",SqlDbType.Char,2,0} ,
          new Object[] {"@AV15TFEstado_UF_Sel",SqlDbType.Char,2,0}
          } ;
          Object[] prmP00NP3 ;
          prmP00NP3 = new Object[] {
          new Object[] {"@AV43UnidadeOrganizacional_Vinculada",SqlDbType.Int,6,0} ,
          new Object[] {"@lV36UnidadeOrganizacional_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV39UnidadeOrganizacional_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV42UnidadeOrganizacional_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV10TFUnidadeOrganizacional_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV11TFUnidadeOrganizacional_Nome_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@AV12TFTpUo_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV13TFTpUo_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV14TFEstado_UF",SqlDbType.Char,2,0} ,
          new Object[] {"@AV15TFEstado_UF_Sel",SqlDbType.Char,2,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00NP2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00NP2,100,0,true,false )
             ,new CursorDef("P00NP3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00NP3,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((bool[]) buf[2])[0] = rslt.getBool(2) ;
                ((String[]) buf[3])[0] = rslt.getString(3, 50) ;
                ((String[]) buf[4])[0] = rslt.getString(4, 2) ;
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                ((int[]) buf[6])[0] = rslt.getInt(6) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((bool[]) buf[2])[0] = rslt.getBool(2) ;
                ((String[]) buf[3])[0] = rslt.getString(3, 2) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((String[]) buf[5])[0] = rslt.getString(5, 50) ;
                ((int[]) buf[6])[0] = rslt.getInt(6) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[10]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[16]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[17]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[10]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[16]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[17]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getgeral_unidadeorganizacionalgeral_unidadeorganizacionalwcfilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getgeral_unidadeorganizacionalgeral_unidadeorganizacionalwcfilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getgeral_unidadeorganizacionalgeral_unidadeorganizacionalwcfilterdata") )
          {
             return  ;
          }
          getgeral_unidadeorganizacionalgeral_unidadeorganizacionalwcfilterdata worker = new getgeral_unidadeorganizacionalgeral_unidadeorganizacionalwcfilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
