/*
               File: GetWWMunicipioFilterData
        Description: Get WWMunicipio Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:53:6.88
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getwwmunicipiofilterdata : GXProcedure
   {
      public getwwmunicipiofilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getwwmunicipiofilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV20DDOName = aP0_DDOName;
         this.AV18SearchTxt = aP1_SearchTxt;
         this.AV19SearchTxtTo = aP2_SearchTxtTo;
         this.AV24OptionsJson = "" ;
         this.AV27OptionsDescJson = "" ;
         this.AV29OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV24OptionsJson;
         aP4_OptionsDescJson=this.AV27OptionsDescJson;
         aP5_OptionIndexesJson=this.AV29OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV20DDOName = aP0_DDOName;
         this.AV18SearchTxt = aP1_SearchTxt;
         this.AV19SearchTxtTo = aP2_SearchTxtTo;
         this.AV24OptionsJson = "" ;
         this.AV27OptionsDescJson = "" ;
         this.AV29OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV24OptionsJson;
         aP4_OptionsDescJson=this.AV27OptionsDescJson;
         aP5_OptionIndexesJson=this.AV29OptionIndexesJson;
         return AV29OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getwwmunicipiofilterdata objgetwwmunicipiofilterdata;
         objgetwwmunicipiofilterdata = new getwwmunicipiofilterdata();
         objgetwwmunicipiofilterdata.AV20DDOName = aP0_DDOName;
         objgetwwmunicipiofilterdata.AV18SearchTxt = aP1_SearchTxt;
         objgetwwmunicipiofilterdata.AV19SearchTxtTo = aP2_SearchTxtTo;
         objgetwwmunicipiofilterdata.AV24OptionsJson = "" ;
         objgetwwmunicipiofilterdata.AV27OptionsDescJson = "" ;
         objgetwwmunicipiofilterdata.AV29OptionIndexesJson = "" ;
         objgetwwmunicipiofilterdata.context.SetSubmitInitialConfig(context);
         objgetwwmunicipiofilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetwwmunicipiofilterdata);
         aP3_OptionsJson=this.AV24OptionsJson;
         aP4_OptionsDescJson=this.AV27OptionsDescJson;
         aP5_OptionIndexesJson=this.AV29OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getwwmunicipiofilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV23Options = (IGxCollection)(new GxSimpleCollection());
         AV26OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV28OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV20DDOName), "DDO_MUNICIPIO_NOME") == 0 )
         {
            /* Execute user subroutine: 'LOADMUNICIPIO_NOMEOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV20DDOName), "DDO_ESTADO_NOME") == 0 )
         {
            /* Execute user subroutine: 'LOADESTADO_NOMEOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV20DDOName), "DDO_PAIS_NOME") == 0 )
         {
            /* Execute user subroutine: 'LOADPAIS_NOMEOPTIONS' */
            S141 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV24OptionsJson = AV23Options.ToJSonString(false);
         AV27OptionsDescJson = AV26OptionsDesc.ToJSonString(false);
         AV29OptionIndexesJson = AV28OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV31Session.Get("WWMunicipioGridState"), "") == 0 )
         {
            AV33GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "WWMunicipioGridState"), "");
         }
         else
         {
            AV33GridState.FromXml(AV31Session.Get("WWMunicipioGridState"), "");
         }
         AV55GXV1 = 1;
         while ( AV55GXV1 <= AV33GridState.gxTpr_Filtervalues.Count )
         {
            AV34GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV33GridState.gxTpr_Filtervalues.Item(AV55GXV1));
            if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFMUNICIPIO_NOME") == 0 )
            {
               AV10TFMunicipio_Nome = AV34GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFMUNICIPIO_NOME_SEL") == 0 )
            {
               AV11TFMunicipio_Nome_Sel = AV34GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFESTADO_NOME") == 0 )
            {
               AV14TFEstado_Nome = AV34GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFESTADO_NOME_SEL") == 0 )
            {
               AV15TFEstado_Nome_Sel = AV34GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFPAIS_NOME") == 0 )
            {
               AV16TFPais_Nome = AV34GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFPAIS_NOME_SEL") == 0 )
            {
               AV17TFPais_Nome_Sel = AV34GridStateFilterValue.gxTpr_Value;
            }
            AV55GXV1 = (int)(AV55GXV1+1);
         }
         if ( AV33GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV35GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV33GridState.gxTpr_Dynamicfilters.Item(1));
            AV36DynamicFiltersSelector1 = AV35GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV36DynamicFiltersSelector1, "MUNICIPIO_NOME") == 0 )
            {
               AV37DynamicFiltersOperator1 = AV35GridStateDynamicFilter.gxTpr_Operator;
               AV38Municipio_Nome1 = AV35GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV36DynamicFiltersSelector1, "ESTADO_NOME") == 0 )
            {
               AV37DynamicFiltersOperator1 = AV35GridStateDynamicFilter.gxTpr_Operator;
               AV39Estado_Nome1 = AV35GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV36DynamicFiltersSelector1, "PAIS_NOME") == 0 )
            {
               AV37DynamicFiltersOperator1 = AV35GridStateDynamicFilter.gxTpr_Operator;
               AV40Pais_Nome1 = AV35GridStateDynamicFilter.gxTpr_Value;
            }
            if ( AV33GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV41DynamicFiltersEnabled2 = true;
               AV35GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV33GridState.gxTpr_Dynamicfilters.Item(2));
               AV42DynamicFiltersSelector2 = AV35GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV42DynamicFiltersSelector2, "MUNICIPIO_NOME") == 0 )
               {
                  AV43DynamicFiltersOperator2 = AV35GridStateDynamicFilter.gxTpr_Operator;
                  AV44Municipio_Nome2 = AV35GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV42DynamicFiltersSelector2, "ESTADO_NOME") == 0 )
               {
                  AV43DynamicFiltersOperator2 = AV35GridStateDynamicFilter.gxTpr_Operator;
                  AV45Estado_Nome2 = AV35GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV42DynamicFiltersSelector2, "PAIS_NOME") == 0 )
               {
                  AV43DynamicFiltersOperator2 = AV35GridStateDynamicFilter.gxTpr_Operator;
                  AV46Pais_Nome2 = AV35GridStateDynamicFilter.gxTpr_Value;
               }
               if ( AV33GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV47DynamicFiltersEnabled3 = true;
                  AV35GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV33GridState.gxTpr_Dynamicfilters.Item(3));
                  AV48DynamicFiltersSelector3 = AV35GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV48DynamicFiltersSelector3, "MUNICIPIO_NOME") == 0 )
                  {
                     AV49DynamicFiltersOperator3 = AV35GridStateDynamicFilter.gxTpr_Operator;
                     AV50Municipio_Nome3 = AV35GridStateDynamicFilter.gxTpr_Value;
                  }
                  else if ( StringUtil.StrCmp(AV48DynamicFiltersSelector3, "ESTADO_NOME") == 0 )
                  {
                     AV49DynamicFiltersOperator3 = AV35GridStateDynamicFilter.gxTpr_Operator;
                     AV51Estado_Nome3 = AV35GridStateDynamicFilter.gxTpr_Value;
                  }
                  else if ( StringUtil.StrCmp(AV48DynamicFiltersSelector3, "PAIS_NOME") == 0 )
                  {
                     AV49DynamicFiltersOperator3 = AV35GridStateDynamicFilter.gxTpr_Operator;
                     AV52Pais_Nome3 = AV35GridStateDynamicFilter.gxTpr_Value;
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADMUNICIPIO_NOMEOPTIONS' Routine */
         AV10TFMunicipio_Nome = AV18SearchTxt;
         AV11TFMunicipio_Nome_Sel = "";
         AV57WWMunicipioDS_1_Dynamicfiltersselector1 = AV36DynamicFiltersSelector1;
         AV58WWMunicipioDS_2_Dynamicfiltersoperator1 = AV37DynamicFiltersOperator1;
         AV59WWMunicipioDS_3_Municipio_nome1 = AV38Municipio_Nome1;
         AV60WWMunicipioDS_4_Estado_nome1 = AV39Estado_Nome1;
         AV61WWMunicipioDS_5_Pais_nome1 = AV40Pais_Nome1;
         AV62WWMunicipioDS_6_Dynamicfiltersenabled2 = AV41DynamicFiltersEnabled2;
         AV63WWMunicipioDS_7_Dynamicfiltersselector2 = AV42DynamicFiltersSelector2;
         AV64WWMunicipioDS_8_Dynamicfiltersoperator2 = AV43DynamicFiltersOperator2;
         AV65WWMunicipioDS_9_Municipio_nome2 = AV44Municipio_Nome2;
         AV66WWMunicipioDS_10_Estado_nome2 = AV45Estado_Nome2;
         AV67WWMunicipioDS_11_Pais_nome2 = AV46Pais_Nome2;
         AV68WWMunicipioDS_12_Dynamicfiltersenabled3 = AV47DynamicFiltersEnabled3;
         AV69WWMunicipioDS_13_Dynamicfiltersselector3 = AV48DynamicFiltersSelector3;
         AV70WWMunicipioDS_14_Dynamicfiltersoperator3 = AV49DynamicFiltersOperator3;
         AV71WWMunicipioDS_15_Municipio_nome3 = AV50Municipio_Nome3;
         AV72WWMunicipioDS_16_Estado_nome3 = AV51Estado_Nome3;
         AV73WWMunicipioDS_17_Pais_nome3 = AV52Pais_Nome3;
         AV74WWMunicipioDS_18_Tfmunicipio_nome = AV10TFMunicipio_Nome;
         AV75WWMunicipioDS_19_Tfmunicipio_nome_sel = AV11TFMunicipio_Nome_Sel;
         AV76WWMunicipioDS_20_Tfestado_nome = AV14TFEstado_Nome;
         AV77WWMunicipioDS_21_Tfestado_nome_sel = AV15TFEstado_Nome_Sel;
         AV78WWMunicipioDS_22_Tfpais_nome = AV16TFPais_Nome;
         AV79WWMunicipioDS_23_Tfpais_nome_sel = AV17TFPais_Nome_Sel;
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV57WWMunicipioDS_1_Dynamicfiltersselector1 ,
                                              AV58WWMunicipioDS_2_Dynamicfiltersoperator1 ,
                                              AV59WWMunicipioDS_3_Municipio_nome1 ,
                                              AV60WWMunicipioDS_4_Estado_nome1 ,
                                              AV61WWMunicipioDS_5_Pais_nome1 ,
                                              AV62WWMunicipioDS_6_Dynamicfiltersenabled2 ,
                                              AV63WWMunicipioDS_7_Dynamicfiltersselector2 ,
                                              AV64WWMunicipioDS_8_Dynamicfiltersoperator2 ,
                                              AV65WWMunicipioDS_9_Municipio_nome2 ,
                                              AV66WWMunicipioDS_10_Estado_nome2 ,
                                              AV67WWMunicipioDS_11_Pais_nome2 ,
                                              AV68WWMunicipioDS_12_Dynamicfiltersenabled3 ,
                                              AV69WWMunicipioDS_13_Dynamicfiltersselector3 ,
                                              AV70WWMunicipioDS_14_Dynamicfiltersoperator3 ,
                                              AV71WWMunicipioDS_15_Municipio_nome3 ,
                                              AV72WWMunicipioDS_16_Estado_nome3 ,
                                              AV73WWMunicipioDS_17_Pais_nome3 ,
                                              AV75WWMunicipioDS_19_Tfmunicipio_nome_sel ,
                                              AV74WWMunicipioDS_18_Tfmunicipio_nome ,
                                              AV77WWMunicipioDS_21_Tfestado_nome_sel ,
                                              AV76WWMunicipioDS_20_Tfestado_nome ,
                                              AV79WWMunicipioDS_23_Tfpais_nome_sel ,
                                              AV78WWMunicipioDS_22_Tfpais_nome ,
                                              A26Municipio_Nome ,
                                              A24Estado_Nome ,
                                              A22Pais_Nome },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING
                                              }
         });
         lV59WWMunicipioDS_3_Municipio_nome1 = StringUtil.PadR( StringUtil.RTrim( AV59WWMunicipioDS_3_Municipio_nome1), 50, "%");
         lV59WWMunicipioDS_3_Municipio_nome1 = StringUtil.PadR( StringUtil.RTrim( AV59WWMunicipioDS_3_Municipio_nome1), 50, "%");
         lV60WWMunicipioDS_4_Estado_nome1 = StringUtil.PadR( StringUtil.RTrim( AV60WWMunicipioDS_4_Estado_nome1), 50, "%");
         lV60WWMunicipioDS_4_Estado_nome1 = StringUtil.PadR( StringUtil.RTrim( AV60WWMunicipioDS_4_Estado_nome1), 50, "%");
         lV61WWMunicipioDS_5_Pais_nome1 = StringUtil.PadR( StringUtil.RTrim( AV61WWMunicipioDS_5_Pais_nome1), 50, "%");
         lV61WWMunicipioDS_5_Pais_nome1 = StringUtil.PadR( StringUtil.RTrim( AV61WWMunicipioDS_5_Pais_nome1), 50, "%");
         lV65WWMunicipioDS_9_Municipio_nome2 = StringUtil.PadR( StringUtil.RTrim( AV65WWMunicipioDS_9_Municipio_nome2), 50, "%");
         lV65WWMunicipioDS_9_Municipio_nome2 = StringUtil.PadR( StringUtil.RTrim( AV65WWMunicipioDS_9_Municipio_nome2), 50, "%");
         lV66WWMunicipioDS_10_Estado_nome2 = StringUtil.PadR( StringUtil.RTrim( AV66WWMunicipioDS_10_Estado_nome2), 50, "%");
         lV66WWMunicipioDS_10_Estado_nome2 = StringUtil.PadR( StringUtil.RTrim( AV66WWMunicipioDS_10_Estado_nome2), 50, "%");
         lV67WWMunicipioDS_11_Pais_nome2 = StringUtil.PadR( StringUtil.RTrim( AV67WWMunicipioDS_11_Pais_nome2), 50, "%");
         lV67WWMunicipioDS_11_Pais_nome2 = StringUtil.PadR( StringUtil.RTrim( AV67WWMunicipioDS_11_Pais_nome2), 50, "%");
         lV71WWMunicipioDS_15_Municipio_nome3 = StringUtil.PadR( StringUtil.RTrim( AV71WWMunicipioDS_15_Municipio_nome3), 50, "%");
         lV71WWMunicipioDS_15_Municipio_nome3 = StringUtil.PadR( StringUtil.RTrim( AV71WWMunicipioDS_15_Municipio_nome3), 50, "%");
         lV72WWMunicipioDS_16_Estado_nome3 = StringUtil.PadR( StringUtil.RTrim( AV72WWMunicipioDS_16_Estado_nome3), 50, "%");
         lV72WWMunicipioDS_16_Estado_nome3 = StringUtil.PadR( StringUtil.RTrim( AV72WWMunicipioDS_16_Estado_nome3), 50, "%");
         lV73WWMunicipioDS_17_Pais_nome3 = StringUtil.PadR( StringUtil.RTrim( AV73WWMunicipioDS_17_Pais_nome3), 50, "%");
         lV73WWMunicipioDS_17_Pais_nome3 = StringUtil.PadR( StringUtil.RTrim( AV73WWMunicipioDS_17_Pais_nome3), 50, "%");
         lV74WWMunicipioDS_18_Tfmunicipio_nome = StringUtil.PadR( StringUtil.RTrim( AV74WWMunicipioDS_18_Tfmunicipio_nome), 50, "%");
         lV76WWMunicipioDS_20_Tfestado_nome = StringUtil.PadR( StringUtil.RTrim( AV76WWMunicipioDS_20_Tfestado_nome), 50, "%");
         lV78WWMunicipioDS_22_Tfpais_nome = StringUtil.PadR( StringUtil.RTrim( AV78WWMunicipioDS_22_Tfpais_nome), 50, "%");
         /* Using cursor P00ER2 */
         pr_default.execute(0, new Object[] {lV59WWMunicipioDS_3_Municipio_nome1, lV59WWMunicipioDS_3_Municipio_nome1, lV60WWMunicipioDS_4_Estado_nome1, lV60WWMunicipioDS_4_Estado_nome1, lV61WWMunicipioDS_5_Pais_nome1, lV61WWMunicipioDS_5_Pais_nome1, lV65WWMunicipioDS_9_Municipio_nome2, lV65WWMunicipioDS_9_Municipio_nome2, lV66WWMunicipioDS_10_Estado_nome2, lV66WWMunicipioDS_10_Estado_nome2, lV67WWMunicipioDS_11_Pais_nome2, lV67WWMunicipioDS_11_Pais_nome2, lV71WWMunicipioDS_15_Municipio_nome3, lV71WWMunicipioDS_15_Municipio_nome3, lV72WWMunicipioDS_16_Estado_nome3, lV72WWMunicipioDS_16_Estado_nome3, lV73WWMunicipioDS_17_Pais_nome3, lV73WWMunicipioDS_17_Pais_nome3, lV74WWMunicipioDS_18_Tfmunicipio_nome, AV75WWMunicipioDS_19_Tfmunicipio_nome_sel, lV76WWMunicipioDS_20_Tfestado_nome, AV77WWMunicipioDS_21_Tfestado_nome_sel, lV78WWMunicipioDS_22_Tfpais_nome, AV79WWMunicipioDS_23_Tfpais_nome_sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKER2 = false;
            A23Estado_UF = P00ER2_A23Estado_UF[0];
            A21Pais_Codigo = P00ER2_A21Pais_Codigo[0];
            A26Municipio_Nome = P00ER2_A26Municipio_Nome[0];
            A22Pais_Nome = P00ER2_A22Pais_Nome[0];
            A24Estado_Nome = P00ER2_A24Estado_Nome[0];
            A25Municipio_Codigo = P00ER2_A25Municipio_Codigo[0];
            A24Estado_Nome = P00ER2_A24Estado_Nome[0];
            A22Pais_Nome = P00ER2_A22Pais_Nome[0];
            AV30count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00ER2_A26Municipio_Nome[0], A26Municipio_Nome) == 0 ) )
            {
               BRKER2 = false;
               A25Municipio_Codigo = P00ER2_A25Municipio_Codigo[0];
               AV30count = (long)(AV30count+1);
               BRKER2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A26Municipio_Nome)) )
            {
               AV22Option = A26Municipio_Nome;
               AV23Options.Add(AV22Option, 0);
               AV28OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV30count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV23Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKER2 )
            {
               BRKER2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADESTADO_NOMEOPTIONS' Routine */
         AV14TFEstado_Nome = AV18SearchTxt;
         AV15TFEstado_Nome_Sel = "";
         AV57WWMunicipioDS_1_Dynamicfiltersselector1 = AV36DynamicFiltersSelector1;
         AV58WWMunicipioDS_2_Dynamicfiltersoperator1 = AV37DynamicFiltersOperator1;
         AV59WWMunicipioDS_3_Municipio_nome1 = AV38Municipio_Nome1;
         AV60WWMunicipioDS_4_Estado_nome1 = AV39Estado_Nome1;
         AV61WWMunicipioDS_5_Pais_nome1 = AV40Pais_Nome1;
         AV62WWMunicipioDS_6_Dynamicfiltersenabled2 = AV41DynamicFiltersEnabled2;
         AV63WWMunicipioDS_7_Dynamicfiltersselector2 = AV42DynamicFiltersSelector2;
         AV64WWMunicipioDS_8_Dynamicfiltersoperator2 = AV43DynamicFiltersOperator2;
         AV65WWMunicipioDS_9_Municipio_nome2 = AV44Municipio_Nome2;
         AV66WWMunicipioDS_10_Estado_nome2 = AV45Estado_Nome2;
         AV67WWMunicipioDS_11_Pais_nome2 = AV46Pais_Nome2;
         AV68WWMunicipioDS_12_Dynamicfiltersenabled3 = AV47DynamicFiltersEnabled3;
         AV69WWMunicipioDS_13_Dynamicfiltersselector3 = AV48DynamicFiltersSelector3;
         AV70WWMunicipioDS_14_Dynamicfiltersoperator3 = AV49DynamicFiltersOperator3;
         AV71WWMunicipioDS_15_Municipio_nome3 = AV50Municipio_Nome3;
         AV72WWMunicipioDS_16_Estado_nome3 = AV51Estado_Nome3;
         AV73WWMunicipioDS_17_Pais_nome3 = AV52Pais_Nome3;
         AV74WWMunicipioDS_18_Tfmunicipio_nome = AV10TFMunicipio_Nome;
         AV75WWMunicipioDS_19_Tfmunicipio_nome_sel = AV11TFMunicipio_Nome_Sel;
         AV76WWMunicipioDS_20_Tfestado_nome = AV14TFEstado_Nome;
         AV77WWMunicipioDS_21_Tfestado_nome_sel = AV15TFEstado_Nome_Sel;
         AV78WWMunicipioDS_22_Tfpais_nome = AV16TFPais_Nome;
         AV79WWMunicipioDS_23_Tfpais_nome_sel = AV17TFPais_Nome_Sel;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV57WWMunicipioDS_1_Dynamicfiltersselector1 ,
                                              AV58WWMunicipioDS_2_Dynamicfiltersoperator1 ,
                                              AV59WWMunicipioDS_3_Municipio_nome1 ,
                                              AV60WWMunicipioDS_4_Estado_nome1 ,
                                              AV61WWMunicipioDS_5_Pais_nome1 ,
                                              AV62WWMunicipioDS_6_Dynamicfiltersenabled2 ,
                                              AV63WWMunicipioDS_7_Dynamicfiltersselector2 ,
                                              AV64WWMunicipioDS_8_Dynamicfiltersoperator2 ,
                                              AV65WWMunicipioDS_9_Municipio_nome2 ,
                                              AV66WWMunicipioDS_10_Estado_nome2 ,
                                              AV67WWMunicipioDS_11_Pais_nome2 ,
                                              AV68WWMunicipioDS_12_Dynamicfiltersenabled3 ,
                                              AV69WWMunicipioDS_13_Dynamicfiltersselector3 ,
                                              AV70WWMunicipioDS_14_Dynamicfiltersoperator3 ,
                                              AV71WWMunicipioDS_15_Municipio_nome3 ,
                                              AV72WWMunicipioDS_16_Estado_nome3 ,
                                              AV73WWMunicipioDS_17_Pais_nome3 ,
                                              AV75WWMunicipioDS_19_Tfmunicipio_nome_sel ,
                                              AV74WWMunicipioDS_18_Tfmunicipio_nome ,
                                              AV77WWMunicipioDS_21_Tfestado_nome_sel ,
                                              AV76WWMunicipioDS_20_Tfestado_nome ,
                                              AV79WWMunicipioDS_23_Tfpais_nome_sel ,
                                              AV78WWMunicipioDS_22_Tfpais_nome ,
                                              A26Municipio_Nome ,
                                              A24Estado_Nome ,
                                              A22Pais_Nome },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING
                                              }
         });
         lV59WWMunicipioDS_3_Municipio_nome1 = StringUtil.PadR( StringUtil.RTrim( AV59WWMunicipioDS_3_Municipio_nome1), 50, "%");
         lV59WWMunicipioDS_3_Municipio_nome1 = StringUtil.PadR( StringUtil.RTrim( AV59WWMunicipioDS_3_Municipio_nome1), 50, "%");
         lV60WWMunicipioDS_4_Estado_nome1 = StringUtil.PadR( StringUtil.RTrim( AV60WWMunicipioDS_4_Estado_nome1), 50, "%");
         lV60WWMunicipioDS_4_Estado_nome1 = StringUtil.PadR( StringUtil.RTrim( AV60WWMunicipioDS_4_Estado_nome1), 50, "%");
         lV61WWMunicipioDS_5_Pais_nome1 = StringUtil.PadR( StringUtil.RTrim( AV61WWMunicipioDS_5_Pais_nome1), 50, "%");
         lV61WWMunicipioDS_5_Pais_nome1 = StringUtil.PadR( StringUtil.RTrim( AV61WWMunicipioDS_5_Pais_nome1), 50, "%");
         lV65WWMunicipioDS_9_Municipio_nome2 = StringUtil.PadR( StringUtil.RTrim( AV65WWMunicipioDS_9_Municipio_nome2), 50, "%");
         lV65WWMunicipioDS_9_Municipio_nome2 = StringUtil.PadR( StringUtil.RTrim( AV65WWMunicipioDS_9_Municipio_nome2), 50, "%");
         lV66WWMunicipioDS_10_Estado_nome2 = StringUtil.PadR( StringUtil.RTrim( AV66WWMunicipioDS_10_Estado_nome2), 50, "%");
         lV66WWMunicipioDS_10_Estado_nome2 = StringUtil.PadR( StringUtil.RTrim( AV66WWMunicipioDS_10_Estado_nome2), 50, "%");
         lV67WWMunicipioDS_11_Pais_nome2 = StringUtil.PadR( StringUtil.RTrim( AV67WWMunicipioDS_11_Pais_nome2), 50, "%");
         lV67WWMunicipioDS_11_Pais_nome2 = StringUtil.PadR( StringUtil.RTrim( AV67WWMunicipioDS_11_Pais_nome2), 50, "%");
         lV71WWMunicipioDS_15_Municipio_nome3 = StringUtil.PadR( StringUtil.RTrim( AV71WWMunicipioDS_15_Municipio_nome3), 50, "%");
         lV71WWMunicipioDS_15_Municipio_nome3 = StringUtil.PadR( StringUtil.RTrim( AV71WWMunicipioDS_15_Municipio_nome3), 50, "%");
         lV72WWMunicipioDS_16_Estado_nome3 = StringUtil.PadR( StringUtil.RTrim( AV72WWMunicipioDS_16_Estado_nome3), 50, "%");
         lV72WWMunicipioDS_16_Estado_nome3 = StringUtil.PadR( StringUtil.RTrim( AV72WWMunicipioDS_16_Estado_nome3), 50, "%");
         lV73WWMunicipioDS_17_Pais_nome3 = StringUtil.PadR( StringUtil.RTrim( AV73WWMunicipioDS_17_Pais_nome3), 50, "%");
         lV73WWMunicipioDS_17_Pais_nome3 = StringUtil.PadR( StringUtil.RTrim( AV73WWMunicipioDS_17_Pais_nome3), 50, "%");
         lV74WWMunicipioDS_18_Tfmunicipio_nome = StringUtil.PadR( StringUtil.RTrim( AV74WWMunicipioDS_18_Tfmunicipio_nome), 50, "%");
         lV76WWMunicipioDS_20_Tfestado_nome = StringUtil.PadR( StringUtil.RTrim( AV76WWMunicipioDS_20_Tfestado_nome), 50, "%");
         lV78WWMunicipioDS_22_Tfpais_nome = StringUtil.PadR( StringUtil.RTrim( AV78WWMunicipioDS_22_Tfpais_nome), 50, "%");
         /* Using cursor P00ER3 */
         pr_default.execute(1, new Object[] {lV59WWMunicipioDS_3_Municipio_nome1, lV59WWMunicipioDS_3_Municipio_nome1, lV60WWMunicipioDS_4_Estado_nome1, lV60WWMunicipioDS_4_Estado_nome1, lV61WWMunicipioDS_5_Pais_nome1, lV61WWMunicipioDS_5_Pais_nome1, lV65WWMunicipioDS_9_Municipio_nome2, lV65WWMunicipioDS_9_Municipio_nome2, lV66WWMunicipioDS_10_Estado_nome2, lV66WWMunicipioDS_10_Estado_nome2, lV67WWMunicipioDS_11_Pais_nome2, lV67WWMunicipioDS_11_Pais_nome2, lV71WWMunicipioDS_15_Municipio_nome3, lV71WWMunicipioDS_15_Municipio_nome3, lV72WWMunicipioDS_16_Estado_nome3, lV72WWMunicipioDS_16_Estado_nome3, lV73WWMunicipioDS_17_Pais_nome3, lV73WWMunicipioDS_17_Pais_nome3, lV74WWMunicipioDS_18_Tfmunicipio_nome, AV75WWMunicipioDS_19_Tfmunicipio_nome_sel, lV76WWMunicipioDS_20_Tfestado_nome, AV77WWMunicipioDS_21_Tfestado_nome_sel, lV78WWMunicipioDS_22_Tfpais_nome, AV79WWMunicipioDS_23_Tfpais_nome_sel});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKER4 = false;
            A21Pais_Codigo = P00ER3_A21Pais_Codigo[0];
            A23Estado_UF = P00ER3_A23Estado_UF[0];
            A22Pais_Nome = P00ER3_A22Pais_Nome[0];
            A24Estado_Nome = P00ER3_A24Estado_Nome[0];
            A26Municipio_Nome = P00ER3_A26Municipio_Nome[0];
            A25Municipio_Codigo = P00ER3_A25Municipio_Codigo[0];
            A22Pais_Nome = P00ER3_A22Pais_Nome[0];
            A24Estado_Nome = P00ER3_A24Estado_Nome[0];
            AV30count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( StringUtil.StrCmp(P00ER3_A23Estado_UF[0], A23Estado_UF) == 0 ) )
            {
               BRKER4 = false;
               A25Municipio_Codigo = P00ER3_A25Municipio_Codigo[0];
               AV30count = (long)(AV30count+1);
               BRKER4 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A24Estado_Nome)) )
            {
               AV22Option = A24Estado_Nome;
               AV21InsertIndex = 1;
               while ( ( AV21InsertIndex <= AV23Options.Count ) && ( StringUtil.StrCmp(((String)AV23Options.Item(AV21InsertIndex)), AV22Option) < 0 ) )
               {
                  AV21InsertIndex = (int)(AV21InsertIndex+1);
               }
               AV23Options.Add(AV22Option, AV21InsertIndex);
               AV28OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV30count), "Z,ZZZ,ZZZ,ZZ9")), AV21InsertIndex);
            }
            if ( AV23Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKER4 )
            {
               BRKER4 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      protected void S141( )
      {
         /* 'LOADPAIS_NOMEOPTIONS' Routine */
         AV16TFPais_Nome = AV18SearchTxt;
         AV17TFPais_Nome_Sel = "";
         AV57WWMunicipioDS_1_Dynamicfiltersselector1 = AV36DynamicFiltersSelector1;
         AV58WWMunicipioDS_2_Dynamicfiltersoperator1 = AV37DynamicFiltersOperator1;
         AV59WWMunicipioDS_3_Municipio_nome1 = AV38Municipio_Nome1;
         AV60WWMunicipioDS_4_Estado_nome1 = AV39Estado_Nome1;
         AV61WWMunicipioDS_5_Pais_nome1 = AV40Pais_Nome1;
         AV62WWMunicipioDS_6_Dynamicfiltersenabled2 = AV41DynamicFiltersEnabled2;
         AV63WWMunicipioDS_7_Dynamicfiltersselector2 = AV42DynamicFiltersSelector2;
         AV64WWMunicipioDS_8_Dynamicfiltersoperator2 = AV43DynamicFiltersOperator2;
         AV65WWMunicipioDS_9_Municipio_nome2 = AV44Municipio_Nome2;
         AV66WWMunicipioDS_10_Estado_nome2 = AV45Estado_Nome2;
         AV67WWMunicipioDS_11_Pais_nome2 = AV46Pais_Nome2;
         AV68WWMunicipioDS_12_Dynamicfiltersenabled3 = AV47DynamicFiltersEnabled3;
         AV69WWMunicipioDS_13_Dynamicfiltersselector3 = AV48DynamicFiltersSelector3;
         AV70WWMunicipioDS_14_Dynamicfiltersoperator3 = AV49DynamicFiltersOperator3;
         AV71WWMunicipioDS_15_Municipio_nome3 = AV50Municipio_Nome3;
         AV72WWMunicipioDS_16_Estado_nome3 = AV51Estado_Nome3;
         AV73WWMunicipioDS_17_Pais_nome3 = AV52Pais_Nome3;
         AV74WWMunicipioDS_18_Tfmunicipio_nome = AV10TFMunicipio_Nome;
         AV75WWMunicipioDS_19_Tfmunicipio_nome_sel = AV11TFMunicipio_Nome_Sel;
         AV76WWMunicipioDS_20_Tfestado_nome = AV14TFEstado_Nome;
         AV77WWMunicipioDS_21_Tfestado_nome_sel = AV15TFEstado_Nome_Sel;
         AV78WWMunicipioDS_22_Tfpais_nome = AV16TFPais_Nome;
         AV79WWMunicipioDS_23_Tfpais_nome_sel = AV17TFPais_Nome_Sel;
         pr_default.dynParam(2, new Object[]{ new Object[]{
                                              AV57WWMunicipioDS_1_Dynamicfiltersselector1 ,
                                              AV58WWMunicipioDS_2_Dynamicfiltersoperator1 ,
                                              AV59WWMunicipioDS_3_Municipio_nome1 ,
                                              AV60WWMunicipioDS_4_Estado_nome1 ,
                                              AV61WWMunicipioDS_5_Pais_nome1 ,
                                              AV62WWMunicipioDS_6_Dynamicfiltersenabled2 ,
                                              AV63WWMunicipioDS_7_Dynamicfiltersselector2 ,
                                              AV64WWMunicipioDS_8_Dynamicfiltersoperator2 ,
                                              AV65WWMunicipioDS_9_Municipio_nome2 ,
                                              AV66WWMunicipioDS_10_Estado_nome2 ,
                                              AV67WWMunicipioDS_11_Pais_nome2 ,
                                              AV68WWMunicipioDS_12_Dynamicfiltersenabled3 ,
                                              AV69WWMunicipioDS_13_Dynamicfiltersselector3 ,
                                              AV70WWMunicipioDS_14_Dynamicfiltersoperator3 ,
                                              AV71WWMunicipioDS_15_Municipio_nome3 ,
                                              AV72WWMunicipioDS_16_Estado_nome3 ,
                                              AV73WWMunicipioDS_17_Pais_nome3 ,
                                              AV75WWMunicipioDS_19_Tfmunicipio_nome_sel ,
                                              AV74WWMunicipioDS_18_Tfmunicipio_nome ,
                                              AV77WWMunicipioDS_21_Tfestado_nome_sel ,
                                              AV76WWMunicipioDS_20_Tfestado_nome ,
                                              AV79WWMunicipioDS_23_Tfpais_nome_sel ,
                                              AV78WWMunicipioDS_22_Tfpais_nome ,
                                              A26Municipio_Nome ,
                                              A24Estado_Nome ,
                                              A22Pais_Nome },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING
                                              }
         });
         lV59WWMunicipioDS_3_Municipio_nome1 = StringUtil.PadR( StringUtil.RTrim( AV59WWMunicipioDS_3_Municipio_nome1), 50, "%");
         lV59WWMunicipioDS_3_Municipio_nome1 = StringUtil.PadR( StringUtil.RTrim( AV59WWMunicipioDS_3_Municipio_nome1), 50, "%");
         lV60WWMunicipioDS_4_Estado_nome1 = StringUtil.PadR( StringUtil.RTrim( AV60WWMunicipioDS_4_Estado_nome1), 50, "%");
         lV60WWMunicipioDS_4_Estado_nome1 = StringUtil.PadR( StringUtil.RTrim( AV60WWMunicipioDS_4_Estado_nome1), 50, "%");
         lV61WWMunicipioDS_5_Pais_nome1 = StringUtil.PadR( StringUtil.RTrim( AV61WWMunicipioDS_5_Pais_nome1), 50, "%");
         lV61WWMunicipioDS_5_Pais_nome1 = StringUtil.PadR( StringUtil.RTrim( AV61WWMunicipioDS_5_Pais_nome1), 50, "%");
         lV65WWMunicipioDS_9_Municipio_nome2 = StringUtil.PadR( StringUtil.RTrim( AV65WWMunicipioDS_9_Municipio_nome2), 50, "%");
         lV65WWMunicipioDS_9_Municipio_nome2 = StringUtil.PadR( StringUtil.RTrim( AV65WWMunicipioDS_9_Municipio_nome2), 50, "%");
         lV66WWMunicipioDS_10_Estado_nome2 = StringUtil.PadR( StringUtil.RTrim( AV66WWMunicipioDS_10_Estado_nome2), 50, "%");
         lV66WWMunicipioDS_10_Estado_nome2 = StringUtil.PadR( StringUtil.RTrim( AV66WWMunicipioDS_10_Estado_nome2), 50, "%");
         lV67WWMunicipioDS_11_Pais_nome2 = StringUtil.PadR( StringUtil.RTrim( AV67WWMunicipioDS_11_Pais_nome2), 50, "%");
         lV67WWMunicipioDS_11_Pais_nome2 = StringUtil.PadR( StringUtil.RTrim( AV67WWMunicipioDS_11_Pais_nome2), 50, "%");
         lV71WWMunicipioDS_15_Municipio_nome3 = StringUtil.PadR( StringUtil.RTrim( AV71WWMunicipioDS_15_Municipio_nome3), 50, "%");
         lV71WWMunicipioDS_15_Municipio_nome3 = StringUtil.PadR( StringUtil.RTrim( AV71WWMunicipioDS_15_Municipio_nome3), 50, "%");
         lV72WWMunicipioDS_16_Estado_nome3 = StringUtil.PadR( StringUtil.RTrim( AV72WWMunicipioDS_16_Estado_nome3), 50, "%");
         lV72WWMunicipioDS_16_Estado_nome3 = StringUtil.PadR( StringUtil.RTrim( AV72WWMunicipioDS_16_Estado_nome3), 50, "%");
         lV73WWMunicipioDS_17_Pais_nome3 = StringUtil.PadR( StringUtil.RTrim( AV73WWMunicipioDS_17_Pais_nome3), 50, "%");
         lV73WWMunicipioDS_17_Pais_nome3 = StringUtil.PadR( StringUtil.RTrim( AV73WWMunicipioDS_17_Pais_nome3), 50, "%");
         lV74WWMunicipioDS_18_Tfmunicipio_nome = StringUtil.PadR( StringUtil.RTrim( AV74WWMunicipioDS_18_Tfmunicipio_nome), 50, "%");
         lV76WWMunicipioDS_20_Tfestado_nome = StringUtil.PadR( StringUtil.RTrim( AV76WWMunicipioDS_20_Tfestado_nome), 50, "%");
         lV78WWMunicipioDS_22_Tfpais_nome = StringUtil.PadR( StringUtil.RTrim( AV78WWMunicipioDS_22_Tfpais_nome), 50, "%");
         /* Using cursor P00ER4 */
         pr_default.execute(2, new Object[] {lV59WWMunicipioDS_3_Municipio_nome1, lV59WWMunicipioDS_3_Municipio_nome1, lV60WWMunicipioDS_4_Estado_nome1, lV60WWMunicipioDS_4_Estado_nome1, lV61WWMunicipioDS_5_Pais_nome1, lV61WWMunicipioDS_5_Pais_nome1, lV65WWMunicipioDS_9_Municipio_nome2, lV65WWMunicipioDS_9_Municipio_nome2, lV66WWMunicipioDS_10_Estado_nome2, lV66WWMunicipioDS_10_Estado_nome2, lV67WWMunicipioDS_11_Pais_nome2, lV67WWMunicipioDS_11_Pais_nome2, lV71WWMunicipioDS_15_Municipio_nome3, lV71WWMunicipioDS_15_Municipio_nome3, lV72WWMunicipioDS_16_Estado_nome3, lV72WWMunicipioDS_16_Estado_nome3, lV73WWMunicipioDS_17_Pais_nome3, lV73WWMunicipioDS_17_Pais_nome3, lV74WWMunicipioDS_18_Tfmunicipio_nome, AV75WWMunicipioDS_19_Tfmunicipio_nome_sel, lV76WWMunicipioDS_20_Tfestado_nome, AV77WWMunicipioDS_21_Tfestado_nome_sel, lV78WWMunicipioDS_22_Tfpais_nome, AV79WWMunicipioDS_23_Tfpais_nome_sel});
         while ( (pr_default.getStatus(2) != 101) )
         {
            BRKER6 = false;
            A23Estado_UF = P00ER4_A23Estado_UF[0];
            A21Pais_Codigo = P00ER4_A21Pais_Codigo[0];
            A22Pais_Nome = P00ER4_A22Pais_Nome[0];
            A24Estado_Nome = P00ER4_A24Estado_Nome[0];
            A26Municipio_Nome = P00ER4_A26Municipio_Nome[0];
            A25Municipio_Codigo = P00ER4_A25Municipio_Codigo[0];
            A24Estado_Nome = P00ER4_A24Estado_Nome[0];
            A22Pais_Nome = P00ER4_A22Pais_Nome[0];
            AV30count = 0;
            while ( (pr_default.getStatus(2) != 101) && ( P00ER4_A21Pais_Codigo[0] == A21Pais_Codigo ) )
            {
               BRKER6 = false;
               A25Municipio_Codigo = P00ER4_A25Municipio_Codigo[0];
               AV30count = (long)(AV30count+1);
               BRKER6 = true;
               pr_default.readNext(2);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A22Pais_Nome)) )
            {
               AV22Option = A22Pais_Nome;
               AV21InsertIndex = 1;
               while ( ( AV21InsertIndex <= AV23Options.Count ) && ( StringUtil.StrCmp(((String)AV23Options.Item(AV21InsertIndex)), AV22Option) < 0 ) )
               {
                  AV21InsertIndex = (int)(AV21InsertIndex+1);
               }
               AV23Options.Add(AV22Option, AV21InsertIndex);
               AV28OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV30count), "Z,ZZZ,ZZZ,ZZ9")), AV21InsertIndex);
            }
            if ( AV23Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKER6 )
            {
               BRKER6 = true;
               pr_default.readNext(2);
            }
         }
         pr_default.close(2);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV23Options = new GxSimpleCollection();
         AV26OptionsDesc = new GxSimpleCollection();
         AV28OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV31Session = context.GetSession();
         AV33GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV34GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFMunicipio_Nome = "";
         AV11TFMunicipio_Nome_Sel = "";
         AV14TFEstado_Nome = "";
         AV15TFEstado_Nome_Sel = "";
         AV16TFPais_Nome = "";
         AV17TFPais_Nome_Sel = "";
         AV35GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV36DynamicFiltersSelector1 = "";
         AV38Municipio_Nome1 = "";
         AV39Estado_Nome1 = "";
         AV40Pais_Nome1 = "";
         AV42DynamicFiltersSelector2 = "";
         AV44Municipio_Nome2 = "";
         AV45Estado_Nome2 = "";
         AV46Pais_Nome2 = "";
         AV48DynamicFiltersSelector3 = "";
         AV50Municipio_Nome3 = "";
         AV51Estado_Nome3 = "";
         AV52Pais_Nome3 = "";
         AV57WWMunicipioDS_1_Dynamicfiltersselector1 = "";
         AV59WWMunicipioDS_3_Municipio_nome1 = "";
         AV60WWMunicipioDS_4_Estado_nome1 = "";
         AV61WWMunicipioDS_5_Pais_nome1 = "";
         AV63WWMunicipioDS_7_Dynamicfiltersselector2 = "";
         AV65WWMunicipioDS_9_Municipio_nome2 = "";
         AV66WWMunicipioDS_10_Estado_nome2 = "";
         AV67WWMunicipioDS_11_Pais_nome2 = "";
         AV69WWMunicipioDS_13_Dynamicfiltersselector3 = "";
         AV71WWMunicipioDS_15_Municipio_nome3 = "";
         AV72WWMunicipioDS_16_Estado_nome3 = "";
         AV73WWMunicipioDS_17_Pais_nome3 = "";
         AV74WWMunicipioDS_18_Tfmunicipio_nome = "";
         AV75WWMunicipioDS_19_Tfmunicipio_nome_sel = "";
         AV76WWMunicipioDS_20_Tfestado_nome = "";
         AV77WWMunicipioDS_21_Tfestado_nome_sel = "";
         AV78WWMunicipioDS_22_Tfpais_nome = "";
         AV79WWMunicipioDS_23_Tfpais_nome_sel = "";
         scmdbuf = "";
         lV59WWMunicipioDS_3_Municipio_nome1 = "";
         lV60WWMunicipioDS_4_Estado_nome1 = "";
         lV61WWMunicipioDS_5_Pais_nome1 = "";
         lV65WWMunicipioDS_9_Municipio_nome2 = "";
         lV66WWMunicipioDS_10_Estado_nome2 = "";
         lV67WWMunicipioDS_11_Pais_nome2 = "";
         lV71WWMunicipioDS_15_Municipio_nome3 = "";
         lV72WWMunicipioDS_16_Estado_nome3 = "";
         lV73WWMunicipioDS_17_Pais_nome3 = "";
         lV74WWMunicipioDS_18_Tfmunicipio_nome = "";
         lV76WWMunicipioDS_20_Tfestado_nome = "";
         lV78WWMunicipioDS_22_Tfpais_nome = "";
         A26Municipio_Nome = "";
         A24Estado_Nome = "";
         A22Pais_Nome = "";
         P00ER2_A23Estado_UF = new String[] {""} ;
         P00ER2_A21Pais_Codigo = new int[1] ;
         P00ER2_A26Municipio_Nome = new String[] {""} ;
         P00ER2_A22Pais_Nome = new String[] {""} ;
         P00ER2_A24Estado_Nome = new String[] {""} ;
         P00ER2_A25Municipio_Codigo = new int[1] ;
         A23Estado_UF = "";
         AV22Option = "";
         P00ER3_A21Pais_Codigo = new int[1] ;
         P00ER3_A23Estado_UF = new String[] {""} ;
         P00ER3_A22Pais_Nome = new String[] {""} ;
         P00ER3_A24Estado_Nome = new String[] {""} ;
         P00ER3_A26Municipio_Nome = new String[] {""} ;
         P00ER3_A25Municipio_Codigo = new int[1] ;
         P00ER4_A23Estado_UF = new String[] {""} ;
         P00ER4_A21Pais_Codigo = new int[1] ;
         P00ER4_A22Pais_Nome = new String[] {""} ;
         P00ER4_A24Estado_Nome = new String[] {""} ;
         P00ER4_A26Municipio_Nome = new String[] {""} ;
         P00ER4_A25Municipio_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getwwmunicipiofilterdata__default(),
            new Object[][] {
                new Object[] {
               P00ER2_A23Estado_UF, P00ER2_A21Pais_Codigo, P00ER2_A26Municipio_Nome, P00ER2_A22Pais_Nome, P00ER2_A24Estado_Nome, P00ER2_A25Municipio_Codigo
               }
               , new Object[] {
               P00ER3_A21Pais_Codigo, P00ER3_A23Estado_UF, P00ER3_A22Pais_Nome, P00ER3_A24Estado_Nome, P00ER3_A26Municipio_Nome, P00ER3_A25Municipio_Codigo
               }
               , new Object[] {
               P00ER4_A23Estado_UF, P00ER4_A21Pais_Codigo, P00ER4_A22Pais_Nome, P00ER4_A24Estado_Nome, P00ER4_A26Municipio_Nome, P00ER4_A25Municipio_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV37DynamicFiltersOperator1 ;
      private short AV43DynamicFiltersOperator2 ;
      private short AV49DynamicFiltersOperator3 ;
      private short AV58WWMunicipioDS_2_Dynamicfiltersoperator1 ;
      private short AV64WWMunicipioDS_8_Dynamicfiltersoperator2 ;
      private short AV70WWMunicipioDS_14_Dynamicfiltersoperator3 ;
      private int AV55GXV1 ;
      private int A21Pais_Codigo ;
      private int A25Municipio_Codigo ;
      private int AV21InsertIndex ;
      private long AV30count ;
      private String AV10TFMunicipio_Nome ;
      private String AV11TFMunicipio_Nome_Sel ;
      private String AV14TFEstado_Nome ;
      private String AV15TFEstado_Nome_Sel ;
      private String AV16TFPais_Nome ;
      private String AV17TFPais_Nome_Sel ;
      private String AV38Municipio_Nome1 ;
      private String AV39Estado_Nome1 ;
      private String AV40Pais_Nome1 ;
      private String AV44Municipio_Nome2 ;
      private String AV45Estado_Nome2 ;
      private String AV46Pais_Nome2 ;
      private String AV50Municipio_Nome3 ;
      private String AV51Estado_Nome3 ;
      private String AV52Pais_Nome3 ;
      private String AV59WWMunicipioDS_3_Municipio_nome1 ;
      private String AV60WWMunicipioDS_4_Estado_nome1 ;
      private String AV61WWMunicipioDS_5_Pais_nome1 ;
      private String AV65WWMunicipioDS_9_Municipio_nome2 ;
      private String AV66WWMunicipioDS_10_Estado_nome2 ;
      private String AV67WWMunicipioDS_11_Pais_nome2 ;
      private String AV71WWMunicipioDS_15_Municipio_nome3 ;
      private String AV72WWMunicipioDS_16_Estado_nome3 ;
      private String AV73WWMunicipioDS_17_Pais_nome3 ;
      private String AV74WWMunicipioDS_18_Tfmunicipio_nome ;
      private String AV75WWMunicipioDS_19_Tfmunicipio_nome_sel ;
      private String AV76WWMunicipioDS_20_Tfestado_nome ;
      private String AV77WWMunicipioDS_21_Tfestado_nome_sel ;
      private String AV78WWMunicipioDS_22_Tfpais_nome ;
      private String AV79WWMunicipioDS_23_Tfpais_nome_sel ;
      private String scmdbuf ;
      private String lV59WWMunicipioDS_3_Municipio_nome1 ;
      private String lV60WWMunicipioDS_4_Estado_nome1 ;
      private String lV61WWMunicipioDS_5_Pais_nome1 ;
      private String lV65WWMunicipioDS_9_Municipio_nome2 ;
      private String lV66WWMunicipioDS_10_Estado_nome2 ;
      private String lV67WWMunicipioDS_11_Pais_nome2 ;
      private String lV71WWMunicipioDS_15_Municipio_nome3 ;
      private String lV72WWMunicipioDS_16_Estado_nome3 ;
      private String lV73WWMunicipioDS_17_Pais_nome3 ;
      private String lV74WWMunicipioDS_18_Tfmunicipio_nome ;
      private String lV76WWMunicipioDS_20_Tfestado_nome ;
      private String lV78WWMunicipioDS_22_Tfpais_nome ;
      private String A26Municipio_Nome ;
      private String A24Estado_Nome ;
      private String A22Pais_Nome ;
      private String A23Estado_UF ;
      private bool returnInSub ;
      private bool AV41DynamicFiltersEnabled2 ;
      private bool AV47DynamicFiltersEnabled3 ;
      private bool AV62WWMunicipioDS_6_Dynamicfiltersenabled2 ;
      private bool AV68WWMunicipioDS_12_Dynamicfiltersenabled3 ;
      private bool BRKER2 ;
      private bool BRKER4 ;
      private bool BRKER6 ;
      private String AV29OptionIndexesJson ;
      private String AV24OptionsJson ;
      private String AV27OptionsDescJson ;
      private String AV20DDOName ;
      private String AV18SearchTxt ;
      private String AV19SearchTxtTo ;
      private String AV36DynamicFiltersSelector1 ;
      private String AV42DynamicFiltersSelector2 ;
      private String AV48DynamicFiltersSelector3 ;
      private String AV57WWMunicipioDS_1_Dynamicfiltersselector1 ;
      private String AV63WWMunicipioDS_7_Dynamicfiltersselector2 ;
      private String AV69WWMunicipioDS_13_Dynamicfiltersselector3 ;
      private String AV22Option ;
      private IGxSession AV31Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] P00ER2_A23Estado_UF ;
      private int[] P00ER2_A21Pais_Codigo ;
      private String[] P00ER2_A26Municipio_Nome ;
      private String[] P00ER2_A22Pais_Nome ;
      private String[] P00ER2_A24Estado_Nome ;
      private int[] P00ER2_A25Municipio_Codigo ;
      private int[] P00ER3_A21Pais_Codigo ;
      private String[] P00ER3_A23Estado_UF ;
      private String[] P00ER3_A22Pais_Nome ;
      private String[] P00ER3_A24Estado_Nome ;
      private String[] P00ER3_A26Municipio_Nome ;
      private int[] P00ER3_A25Municipio_Codigo ;
      private String[] P00ER4_A23Estado_UF ;
      private int[] P00ER4_A21Pais_Codigo ;
      private String[] P00ER4_A22Pais_Nome ;
      private String[] P00ER4_A24Estado_Nome ;
      private String[] P00ER4_A26Municipio_Nome ;
      private int[] P00ER4_A25Municipio_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV23Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV26OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV28OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV33GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV34GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV35GridStateDynamicFilter ;
   }

   public class getwwmunicipiofilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00ER2( IGxContext context ,
                                             String AV57WWMunicipioDS_1_Dynamicfiltersselector1 ,
                                             short AV58WWMunicipioDS_2_Dynamicfiltersoperator1 ,
                                             String AV59WWMunicipioDS_3_Municipio_nome1 ,
                                             String AV60WWMunicipioDS_4_Estado_nome1 ,
                                             String AV61WWMunicipioDS_5_Pais_nome1 ,
                                             bool AV62WWMunicipioDS_6_Dynamicfiltersenabled2 ,
                                             String AV63WWMunicipioDS_7_Dynamicfiltersselector2 ,
                                             short AV64WWMunicipioDS_8_Dynamicfiltersoperator2 ,
                                             String AV65WWMunicipioDS_9_Municipio_nome2 ,
                                             String AV66WWMunicipioDS_10_Estado_nome2 ,
                                             String AV67WWMunicipioDS_11_Pais_nome2 ,
                                             bool AV68WWMunicipioDS_12_Dynamicfiltersenabled3 ,
                                             String AV69WWMunicipioDS_13_Dynamicfiltersselector3 ,
                                             short AV70WWMunicipioDS_14_Dynamicfiltersoperator3 ,
                                             String AV71WWMunicipioDS_15_Municipio_nome3 ,
                                             String AV72WWMunicipioDS_16_Estado_nome3 ,
                                             String AV73WWMunicipioDS_17_Pais_nome3 ,
                                             String AV75WWMunicipioDS_19_Tfmunicipio_nome_sel ,
                                             String AV74WWMunicipioDS_18_Tfmunicipio_nome ,
                                             String AV77WWMunicipioDS_21_Tfestado_nome_sel ,
                                             String AV76WWMunicipioDS_20_Tfestado_nome ,
                                             String AV79WWMunicipioDS_23_Tfpais_nome_sel ,
                                             String AV78WWMunicipioDS_22_Tfpais_nome ,
                                             String A26Municipio_Nome ,
                                             String A24Estado_Nome ,
                                             String A22Pais_Nome )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [24] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[Estado_UF], T1.[Pais_Codigo], T1.[Municipio_Nome], T3.[Pais_Nome], T2.[Estado_Nome], T1.[Municipio_Codigo] FROM (([Municipio] T1 WITH (NOLOCK) INNER JOIN [Estado] T2 WITH (NOLOCK) ON T2.[Estado_UF] = T1.[Estado_UF]) INNER JOIN [Pais] T3 WITH (NOLOCK) ON T3.[Pais_Codigo] = T1.[Pais_Codigo])";
         if ( ( StringUtil.StrCmp(AV57WWMunicipioDS_1_Dynamicfiltersselector1, "MUNICIPIO_NOME") == 0 ) && ( AV58WWMunicipioDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59WWMunicipioDS_3_Municipio_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Municipio_Nome] like @lV59WWMunicipioDS_3_Municipio_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Municipio_Nome] like @lV59WWMunicipioDS_3_Municipio_nome1)";
            }
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV57WWMunicipioDS_1_Dynamicfiltersselector1, "MUNICIPIO_NOME") == 0 ) && ( AV58WWMunicipioDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59WWMunicipioDS_3_Municipio_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Municipio_Nome] like '%' + @lV59WWMunicipioDS_3_Municipio_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Municipio_Nome] like '%' + @lV59WWMunicipioDS_3_Municipio_nome1)";
            }
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV57WWMunicipioDS_1_Dynamicfiltersselector1, "ESTADO_NOME") == 0 ) && ( AV58WWMunicipioDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60WWMunicipioDS_4_Estado_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Estado_Nome] like @lV60WWMunicipioDS_4_Estado_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Estado_Nome] like @lV60WWMunicipioDS_4_Estado_nome1)";
            }
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV57WWMunicipioDS_1_Dynamicfiltersselector1, "ESTADO_NOME") == 0 ) && ( AV58WWMunicipioDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60WWMunicipioDS_4_Estado_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Estado_Nome] like '%' + @lV60WWMunicipioDS_4_Estado_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Estado_Nome] like '%' + @lV60WWMunicipioDS_4_Estado_nome1)";
            }
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV57WWMunicipioDS_1_Dynamicfiltersselector1, "PAIS_NOME") == 0 ) && ( AV58WWMunicipioDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61WWMunicipioDS_5_Pais_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pais_Nome] like @lV61WWMunicipioDS_5_Pais_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pais_Nome] like @lV61WWMunicipioDS_5_Pais_nome1)";
            }
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV57WWMunicipioDS_1_Dynamicfiltersselector1, "PAIS_NOME") == 0 ) && ( AV58WWMunicipioDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61WWMunicipioDS_5_Pais_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pais_Nome] like '%' + @lV61WWMunicipioDS_5_Pais_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pais_Nome] like '%' + @lV61WWMunicipioDS_5_Pais_nome1)";
            }
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( AV62WWMunicipioDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV63WWMunicipioDS_7_Dynamicfiltersselector2, "MUNICIPIO_NOME") == 0 ) && ( AV64WWMunicipioDS_8_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65WWMunicipioDS_9_Municipio_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Municipio_Nome] like @lV65WWMunicipioDS_9_Municipio_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Municipio_Nome] like @lV65WWMunicipioDS_9_Municipio_nome2)";
            }
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( AV62WWMunicipioDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV63WWMunicipioDS_7_Dynamicfiltersselector2, "MUNICIPIO_NOME") == 0 ) && ( AV64WWMunicipioDS_8_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65WWMunicipioDS_9_Municipio_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Municipio_Nome] like '%' + @lV65WWMunicipioDS_9_Municipio_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Municipio_Nome] like '%' + @lV65WWMunicipioDS_9_Municipio_nome2)";
            }
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( AV62WWMunicipioDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV63WWMunicipioDS_7_Dynamicfiltersselector2, "ESTADO_NOME") == 0 ) && ( AV64WWMunicipioDS_8_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66WWMunicipioDS_10_Estado_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Estado_Nome] like @lV66WWMunicipioDS_10_Estado_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Estado_Nome] like @lV66WWMunicipioDS_10_Estado_nome2)";
            }
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( AV62WWMunicipioDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV63WWMunicipioDS_7_Dynamicfiltersselector2, "ESTADO_NOME") == 0 ) && ( AV64WWMunicipioDS_8_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66WWMunicipioDS_10_Estado_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Estado_Nome] like '%' + @lV66WWMunicipioDS_10_Estado_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Estado_Nome] like '%' + @lV66WWMunicipioDS_10_Estado_nome2)";
            }
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( AV62WWMunicipioDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV63WWMunicipioDS_7_Dynamicfiltersselector2, "PAIS_NOME") == 0 ) && ( AV64WWMunicipioDS_8_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67WWMunicipioDS_11_Pais_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pais_Nome] like @lV67WWMunicipioDS_11_Pais_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pais_Nome] like @lV67WWMunicipioDS_11_Pais_nome2)";
            }
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( AV62WWMunicipioDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV63WWMunicipioDS_7_Dynamicfiltersselector2, "PAIS_NOME") == 0 ) && ( AV64WWMunicipioDS_8_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67WWMunicipioDS_11_Pais_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pais_Nome] like '%' + @lV67WWMunicipioDS_11_Pais_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pais_Nome] like '%' + @lV67WWMunicipioDS_11_Pais_nome2)";
            }
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( AV68WWMunicipioDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV69WWMunicipioDS_13_Dynamicfiltersselector3, "MUNICIPIO_NOME") == 0 ) && ( AV70WWMunicipioDS_14_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV71WWMunicipioDS_15_Municipio_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Municipio_Nome] like @lV71WWMunicipioDS_15_Municipio_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Municipio_Nome] like @lV71WWMunicipioDS_15_Municipio_nome3)";
            }
         }
         else
         {
            GXv_int1[12] = 1;
         }
         if ( AV68WWMunicipioDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV69WWMunicipioDS_13_Dynamicfiltersselector3, "MUNICIPIO_NOME") == 0 ) && ( AV70WWMunicipioDS_14_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV71WWMunicipioDS_15_Municipio_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Municipio_Nome] like '%' + @lV71WWMunicipioDS_15_Municipio_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Municipio_Nome] like '%' + @lV71WWMunicipioDS_15_Municipio_nome3)";
            }
         }
         else
         {
            GXv_int1[13] = 1;
         }
         if ( AV68WWMunicipioDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV69WWMunicipioDS_13_Dynamicfiltersselector3, "ESTADO_NOME") == 0 ) && ( AV70WWMunicipioDS_14_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV72WWMunicipioDS_16_Estado_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Estado_Nome] like @lV72WWMunicipioDS_16_Estado_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Estado_Nome] like @lV72WWMunicipioDS_16_Estado_nome3)";
            }
         }
         else
         {
            GXv_int1[14] = 1;
         }
         if ( AV68WWMunicipioDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV69WWMunicipioDS_13_Dynamicfiltersselector3, "ESTADO_NOME") == 0 ) && ( AV70WWMunicipioDS_14_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV72WWMunicipioDS_16_Estado_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Estado_Nome] like '%' + @lV72WWMunicipioDS_16_Estado_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Estado_Nome] like '%' + @lV72WWMunicipioDS_16_Estado_nome3)";
            }
         }
         else
         {
            GXv_int1[15] = 1;
         }
         if ( AV68WWMunicipioDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV69WWMunicipioDS_13_Dynamicfiltersselector3, "PAIS_NOME") == 0 ) && ( AV70WWMunicipioDS_14_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73WWMunicipioDS_17_Pais_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pais_Nome] like @lV73WWMunicipioDS_17_Pais_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pais_Nome] like @lV73WWMunicipioDS_17_Pais_nome3)";
            }
         }
         else
         {
            GXv_int1[16] = 1;
         }
         if ( AV68WWMunicipioDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV69WWMunicipioDS_13_Dynamicfiltersselector3, "PAIS_NOME") == 0 ) && ( AV70WWMunicipioDS_14_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73WWMunicipioDS_17_Pais_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pais_Nome] like '%' + @lV73WWMunicipioDS_17_Pais_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pais_Nome] like '%' + @lV73WWMunicipioDS_17_Pais_nome3)";
            }
         }
         else
         {
            GXv_int1[17] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV75WWMunicipioDS_19_Tfmunicipio_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74WWMunicipioDS_18_Tfmunicipio_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Municipio_Nome] like @lV74WWMunicipioDS_18_Tfmunicipio_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Municipio_Nome] like @lV74WWMunicipioDS_18_Tfmunicipio_nome)";
            }
         }
         else
         {
            GXv_int1[18] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV75WWMunicipioDS_19_Tfmunicipio_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Municipio_Nome] = @AV75WWMunicipioDS_19_Tfmunicipio_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Municipio_Nome] = @AV75WWMunicipioDS_19_Tfmunicipio_nome_sel)";
            }
         }
         else
         {
            GXv_int1[19] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV77WWMunicipioDS_21_Tfestado_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV76WWMunicipioDS_20_Tfestado_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Estado_Nome] like @lV76WWMunicipioDS_20_Tfestado_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Estado_Nome] like @lV76WWMunicipioDS_20_Tfestado_nome)";
            }
         }
         else
         {
            GXv_int1[20] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV77WWMunicipioDS_21_Tfestado_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Estado_Nome] = @AV77WWMunicipioDS_21_Tfestado_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Estado_Nome] = @AV77WWMunicipioDS_21_Tfestado_nome_sel)";
            }
         }
         else
         {
            GXv_int1[21] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV79WWMunicipioDS_23_Tfpais_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV78WWMunicipioDS_22_Tfpais_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pais_Nome] like @lV78WWMunicipioDS_22_Tfpais_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pais_Nome] like @lV78WWMunicipioDS_22_Tfpais_nome)";
            }
         }
         else
         {
            GXv_int1[22] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV79WWMunicipioDS_23_Tfpais_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pais_Nome] = @AV79WWMunicipioDS_23_Tfpais_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pais_Nome] = @AV79WWMunicipioDS_23_Tfpais_nome_sel)";
            }
         }
         else
         {
            GXv_int1[23] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[Municipio_Nome]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00ER3( IGxContext context ,
                                             String AV57WWMunicipioDS_1_Dynamicfiltersselector1 ,
                                             short AV58WWMunicipioDS_2_Dynamicfiltersoperator1 ,
                                             String AV59WWMunicipioDS_3_Municipio_nome1 ,
                                             String AV60WWMunicipioDS_4_Estado_nome1 ,
                                             String AV61WWMunicipioDS_5_Pais_nome1 ,
                                             bool AV62WWMunicipioDS_6_Dynamicfiltersenabled2 ,
                                             String AV63WWMunicipioDS_7_Dynamicfiltersselector2 ,
                                             short AV64WWMunicipioDS_8_Dynamicfiltersoperator2 ,
                                             String AV65WWMunicipioDS_9_Municipio_nome2 ,
                                             String AV66WWMunicipioDS_10_Estado_nome2 ,
                                             String AV67WWMunicipioDS_11_Pais_nome2 ,
                                             bool AV68WWMunicipioDS_12_Dynamicfiltersenabled3 ,
                                             String AV69WWMunicipioDS_13_Dynamicfiltersselector3 ,
                                             short AV70WWMunicipioDS_14_Dynamicfiltersoperator3 ,
                                             String AV71WWMunicipioDS_15_Municipio_nome3 ,
                                             String AV72WWMunicipioDS_16_Estado_nome3 ,
                                             String AV73WWMunicipioDS_17_Pais_nome3 ,
                                             String AV75WWMunicipioDS_19_Tfmunicipio_nome_sel ,
                                             String AV74WWMunicipioDS_18_Tfmunicipio_nome ,
                                             String AV77WWMunicipioDS_21_Tfestado_nome_sel ,
                                             String AV76WWMunicipioDS_20_Tfestado_nome ,
                                             String AV79WWMunicipioDS_23_Tfpais_nome_sel ,
                                             String AV78WWMunicipioDS_22_Tfpais_nome ,
                                             String A26Municipio_Nome ,
                                             String A24Estado_Nome ,
                                             String A22Pais_Nome )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [24] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT T1.[Pais_Codigo], T1.[Estado_UF], T2.[Pais_Nome], T3.[Estado_Nome], T1.[Municipio_Nome], T1.[Municipio_Codigo] FROM (([Municipio] T1 WITH (NOLOCK) INNER JOIN [Pais] T2 WITH (NOLOCK) ON T2.[Pais_Codigo] = T1.[Pais_Codigo]) INNER JOIN [Estado] T3 WITH (NOLOCK) ON T3.[Estado_UF] = T1.[Estado_UF])";
         if ( ( StringUtil.StrCmp(AV57WWMunicipioDS_1_Dynamicfiltersselector1, "MUNICIPIO_NOME") == 0 ) && ( AV58WWMunicipioDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59WWMunicipioDS_3_Municipio_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Municipio_Nome] like @lV59WWMunicipioDS_3_Municipio_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Municipio_Nome] like @lV59WWMunicipioDS_3_Municipio_nome1)";
            }
         }
         else
         {
            GXv_int3[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV57WWMunicipioDS_1_Dynamicfiltersselector1, "MUNICIPIO_NOME") == 0 ) && ( AV58WWMunicipioDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59WWMunicipioDS_3_Municipio_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Municipio_Nome] like '%' + @lV59WWMunicipioDS_3_Municipio_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Municipio_Nome] like '%' + @lV59WWMunicipioDS_3_Municipio_nome1)";
            }
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV57WWMunicipioDS_1_Dynamicfiltersselector1, "ESTADO_NOME") == 0 ) && ( AV58WWMunicipioDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60WWMunicipioDS_4_Estado_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Estado_Nome] like @lV60WWMunicipioDS_4_Estado_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Estado_Nome] like @lV60WWMunicipioDS_4_Estado_nome1)";
            }
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV57WWMunicipioDS_1_Dynamicfiltersselector1, "ESTADO_NOME") == 0 ) && ( AV58WWMunicipioDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60WWMunicipioDS_4_Estado_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Estado_Nome] like '%' + @lV60WWMunicipioDS_4_Estado_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Estado_Nome] like '%' + @lV60WWMunicipioDS_4_Estado_nome1)";
            }
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV57WWMunicipioDS_1_Dynamicfiltersselector1, "PAIS_NOME") == 0 ) && ( AV58WWMunicipioDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61WWMunicipioDS_5_Pais_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pais_Nome] like @lV61WWMunicipioDS_5_Pais_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pais_Nome] like @lV61WWMunicipioDS_5_Pais_nome1)";
            }
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV57WWMunicipioDS_1_Dynamicfiltersselector1, "PAIS_NOME") == 0 ) && ( AV58WWMunicipioDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61WWMunicipioDS_5_Pais_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pais_Nome] like '%' + @lV61WWMunicipioDS_5_Pais_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pais_Nome] like '%' + @lV61WWMunicipioDS_5_Pais_nome1)";
            }
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( AV62WWMunicipioDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV63WWMunicipioDS_7_Dynamicfiltersselector2, "MUNICIPIO_NOME") == 0 ) && ( AV64WWMunicipioDS_8_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65WWMunicipioDS_9_Municipio_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Municipio_Nome] like @lV65WWMunicipioDS_9_Municipio_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Municipio_Nome] like @lV65WWMunicipioDS_9_Municipio_nome2)";
            }
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( AV62WWMunicipioDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV63WWMunicipioDS_7_Dynamicfiltersselector2, "MUNICIPIO_NOME") == 0 ) && ( AV64WWMunicipioDS_8_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65WWMunicipioDS_9_Municipio_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Municipio_Nome] like '%' + @lV65WWMunicipioDS_9_Municipio_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Municipio_Nome] like '%' + @lV65WWMunicipioDS_9_Municipio_nome2)";
            }
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( AV62WWMunicipioDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV63WWMunicipioDS_7_Dynamicfiltersselector2, "ESTADO_NOME") == 0 ) && ( AV64WWMunicipioDS_8_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66WWMunicipioDS_10_Estado_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Estado_Nome] like @lV66WWMunicipioDS_10_Estado_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Estado_Nome] like @lV66WWMunicipioDS_10_Estado_nome2)";
            }
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( AV62WWMunicipioDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV63WWMunicipioDS_7_Dynamicfiltersselector2, "ESTADO_NOME") == 0 ) && ( AV64WWMunicipioDS_8_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66WWMunicipioDS_10_Estado_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Estado_Nome] like '%' + @lV66WWMunicipioDS_10_Estado_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Estado_Nome] like '%' + @lV66WWMunicipioDS_10_Estado_nome2)";
            }
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( AV62WWMunicipioDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV63WWMunicipioDS_7_Dynamicfiltersselector2, "PAIS_NOME") == 0 ) && ( AV64WWMunicipioDS_8_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67WWMunicipioDS_11_Pais_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pais_Nome] like @lV67WWMunicipioDS_11_Pais_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pais_Nome] like @lV67WWMunicipioDS_11_Pais_nome2)";
            }
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( AV62WWMunicipioDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV63WWMunicipioDS_7_Dynamicfiltersselector2, "PAIS_NOME") == 0 ) && ( AV64WWMunicipioDS_8_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67WWMunicipioDS_11_Pais_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pais_Nome] like '%' + @lV67WWMunicipioDS_11_Pais_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pais_Nome] like '%' + @lV67WWMunicipioDS_11_Pais_nome2)";
            }
         }
         else
         {
            GXv_int3[11] = 1;
         }
         if ( AV68WWMunicipioDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV69WWMunicipioDS_13_Dynamicfiltersselector3, "MUNICIPIO_NOME") == 0 ) && ( AV70WWMunicipioDS_14_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV71WWMunicipioDS_15_Municipio_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Municipio_Nome] like @lV71WWMunicipioDS_15_Municipio_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Municipio_Nome] like @lV71WWMunicipioDS_15_Municipio_nome3)";
            }
         }
         else
         {
            GXv_int3[12] = 1;
         }
         if ( AV68WWMunicipioDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV69WWMunicipioDS_13_Dynamicfiltersselector3, "MUNICIPIO_NOME") == 0 ) && ( AV70WWMunicipioDS_14_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV71WWMunicipioDS_15_Municipio_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Municipio_Nome] like '%' + @lV71WWMunicipioDS_15_Municipio_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Municipio_Nome] like '%' + @lV71WWMunicipioDS_15_Municipio_nome3)";
            }
         }
         else
         {
            GXv_int3[13] = 1;
         }
         if ( AV68WWMunicipioDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV69WWMunicipioDS_13_Dynamicfiltersselector3, "ESTADO_NOME") == 0 ) && ( AV70WWMunicipioDS_14_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV72WWMunicipioDS_16_Estado_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Estado_Nome] like @lV72WWMunicipioDS_16_Estado_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Estado_Nome] like @lV72WWMunicipioDS_16_Estado_nome3)";
            }
         }
         else
         {
            GXv_int3[14] = 1;
         }
         if ( AV68WWMunicipioDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV69WWMunicipioDS_13_Dynamicfiltersselector3, "ESTADO_NOME") == 0 ) && ( AV70WWMunicipioDS_14_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV72WWMunicipioDS_16_Estado_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Estado_Nome] like '%' + @lV72WWMunicipioDS_16_Estado_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Estado_Nome] like '%' + @lV72WWMunicipioDS_16_Estado_nome3)";
            }
         }
         else
         {
            GXv_int3[15] = 1;
         }
         if ( AV68WWMunicipioDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV69WWMunicipioDS_13_Dynamicfiltersselector3, "PAIS_NOME") == 0 ) && ( AV70WWMunicipioDS_14_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73WWMunicipioDS_17_Pais_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pais_Nome] like @lV73WWMunicipioDS_17_Pais_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pais_Nome] like @lV73WWMunicipioDS_17_Pais_nome3)";
            }
         }
         else
         {
            GXv_int3[16] = 1;
         }
         if ( AV68WWMunicipioDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV69WWMunicipioDS_13_Dynamicfiltersselector3, "PAIS_NOME") == 0 ) && ( AV70WWMunicipioDS_14_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73WWMunicipioDS_17_Pais_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pais_Nome] like '%' + @lV73WWMunicipioDS_17_Pais_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pais_Nome] like '%' + @lV73WWMunicipioDS_17_Pais_nome3)";
            }
         }
         else
         {
            GXv_int3[17] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV75WWMunicipioDS_19_Tfmunicipio_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74WWMunicipioDS_18_Tfmunicipio_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Municipio_Nome] like @lV74WWMunicipioDS_18_Tfmunicipio_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Municipio_Nome] like @lV74WWMunicipioDS_18_Tfmunicipio_nome)";
            }
         }
         else
         {
            GXv_int3[18] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV75WWMunicipioDS_19_Tfmunicipio_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Municipio_Nome] = @AV75WWMunicipioDS_19_Tfmunicipio_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Municipio_Nome] = @AV75WWMunicipioDS_19_Tfmunicipio_nome_sel)";
            }
         }
         else
         {
            GXv_int3[19] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV77WWMunicipioDS_21_Tfestado_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV76WWMunicipioDS_20_Tfestado_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Estado_Nome] like @lV76WWMunicipioDS_20_Tfestado_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Estado_Nome] like @lV76WWMunicipioDS_20_Tfestado_nome)";
            }
         }
         else
         {
            GXv_int3[20] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV77WWMunicipioDS_21_Tfestado_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Estado_Nome] = @AV77WWMunicipioDS_21_Tfestado_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Estado_Nome] = @AV77WWMunicipioDS_21_Tfestado_nome_sel)";
            }
         }
         else
         {
            GXv_int3[21] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV79WWMunicipioDS_23_Tfpais_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV78WWMunicipioDS_22_Tfpais_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pais_Nome] like @lV78WWMunicipioDS_22_Tfpais_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pais_Nome] like @lV78WWMunicipioDS_22_Tfpais_nome)";
            }
         }
         else
         {
            GXv_int3[22] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV79WWMunicipioDS_23_Tfpais_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pais_Nome] = @AV79WWMunicipioDS_23_Tfpais_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pais_Nome] = @AV79WWMunicipioDS_23_Tfpais_nome_sel)";
            }
         }
         else
         {
            GXv_int3[23] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[Estado_UF]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      protected Object[] conditional_P00ER4( IGxContext context ,
                                             String AV57WWMunicipioDS_1_Dynamicfiltersselector1 ,
                                             short AV58WWMunicipioDS_2_Dynamicfiltersoperator1 ,
                                             String AV59WWMunicipioDS_3_Municipio_nome1 ,
                                             String AV60WWMunicipioDS_4_Estado_nome1 ,
                                             String AV61WWMunicipioDS_5_Pais_nome1 ,
                                             bool AV62WWMunicipioDS_6_Dynamicfiltersenabled2 ,
                                             String AV63WWMunicipioDS_7_Dynamicfiltersselector2 ,
                                             short AV64WWMunicipioDS_8_Dynamicfiltersoperator2 ,
                                             String AV65WWMunicipioDS_9_Municipio_nome2 ,
                                             String AV66WWMunicipioDS_10_Estado_nome2 ,
                                             String AV67WWMunicipioDS_11_Pais_nome2 ,
                                             bool AV68WWMunicipioDS_12_Dynamicfiltersenabled3 ,
                                             String AV69WWMunicipioDS_13_Dynamicfiltersselector3 ,
                                             short AV70WWMunicipioDS_14_Dynamicfiltersoperator3 ,
                                             String AV71WWMunicipioDS_15_Municipio_nome3 ,
                                             String AV72WWMunicipioDS_16_Estado_nome3 ,
                                             String AV73WWMunicipioDS_17_Pais_nome3 ,
                                             String AV75WWMunicipioDS_19_Tfmunicipio_nome_sel ,
                                             String AV74WWMunicipioDS_18_Tfmunicipio_nome ,
                                             String AV77WWMunicipioDS_21_Tfestado_nome_sel ,
                                             String AV76WWMunicipioDS_20_Tfestado_nome ,
                                             String AV79WWMunicipioDS_23_Tfpais_nome_sel ,
                                             String AV78WWMunicipioDS_22_Tfpais_nome ,
                                             String A26Municipio_Nome ,
                                             String A24Estado_Nome ,
                                             String A22Pais_Nome )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int5 ;
         GXv_int5 = new short [24] ;
         Object[] GXv_Object6 ;
         GXv_Object6 = new Object [2] ;
         scmdbuf = "SELECT T1.[Estado_UF], T1.[Pais_Codigo], T3.[Pais_Nome], T2.[Estado_Nome], T1.[Municipio_Nome], T1.[Municipio_Codigo] FROM (([Municipio] T1 WITH (NOLOCK) INNER JOIN [Estado] T2 WITH (NOLOCK) ON T2.[Estado_UF] = T1.[Estado_UF]) INNER JOIN [Pais] T3 WITH (NOLOCK) ON T3.[Pais_Codigo] = T1.[Pais_Codigo])";
         if ( ( StringUtil.StrCmp(AV57WWMunicipioDS_1_Dynamicfiltersselector1, "MUNICIPIO_NOME") == 0 ) && ( AV58WWMunicipioDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59WWMunicipioDS_3_Municipio_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Municipio_Nome] like @lV59WWMunicipioDS_3_Municipio_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Municipio_Nome] like @lV59WWMunicipioDS_3_Municipio_nome1)";
            }
         }
         else
         {
            GXv_int5[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV57WWMunicipioDS_1_Dynamicfiltersselector1, "MUNICIPIO_NOME") == 0 ) && ( AV58WWMunicipioDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59WWMunicipioDS_3_Municipio_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Municipio_Nome] like '%' + @lV59WWMunicipioDS_3_Municipio_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Municipio_Nome] like '%' + @lV59WWMunicipioDS_3_Municipio_nome1)";
            }
         }
         else
         {
            GXv_int5[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV57WWMunicipioDS_1_Dynamicfiltersselector1, "ESTADO_NOME") == 0 ) && ( AV58WWMunicipioDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60WWMunicipioDS_4_Estado_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Estado_Nome] like @lV60WWMunicipioDS_4_Estado_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Estado_Nome] like @lV60WWMunicipioDS_4_Estado_nome1)";
            }
         }
         else
         {
            GXv_int5[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV57WWMunicipioDS_1_Dynamicfiltersselector1, "ESTADO_NOME") == 0 ) && ( AV58WWMunicipioDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60WWMunicipioDS_4_Estado_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Estado_Nome] like '%' + @lV60WWMunicipioDS_4_Estado_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Estado_Nome] like '%' + @lV60WWMunicipioDS_4_Estado_nome1)";
            }
         }
         else
         {
            GXv_int5[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV57WWMunicipioDS_1_Dynamicfiltersselector1, "PAIS_NOME") == 0 ) && ( AV58WWMunicipioDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61WWMunicipioDS_5_Pais_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pais_Nome] like @lV61WWMunicipioDS_5_Pais_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pais_Nome] like @lV61WWMunicipioDS_5_Pais_nome1)";
            }
         }
         else
         {
            GXv_int5[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV57WWMunicipioDS_1_Dynamicfiltersselector1, "PAIS_NOME") == 0 ) && ( AV58WWMunicipioDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61WWMunicipioDS_5_Pais_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pais_Nome] like '%' + @lV61WWMunicipioDS_5_Pais_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pais_Nome] like '%' + @lV61WWMunicipioDS_5_Pais_nome1)";
            }
         }
         else
         {
            GXv_int5[5] = 1;
         }
         if ( AV62WWMunicipioDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV63WWMunicipioDS_7_Dynamicfiltersselector2, "MUNICIPIO_NOME") == 0 ) && ( AV64WWMunicipioDS_8_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65WWMunicipioDS_9_Municipio_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Municipio_Nome] like @lV65WWMunicipioDS_9_Municipio_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Municipio_Nome] like @lV65WWMunicipioDS_9_Municipio_nome2)";
            }
         }
         else
         {
            GXv_int5[6] = 1;
         }
         if ( AV62WWMunicipioDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV63WWMunicipioDS_7_Dynamicfiltersselector2, "MUNICIPIO_NOME") == 0 ) && ( AV64WWMunicipioDS_8_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65WWMunicipioDS_9_Municipio_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Municipio_Nome] like '%' + @lV65WWMunicipioDS_9_Municipio_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Municipio_Nome] like '%' + @lV65WWMunicipioDS_9_Municipio_nome2)";
            }
         }
         else
         {
            GXv_int5[7] = 1;
         }
         if ( AV62WWMunicipioDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV63WWMunicipioDS_7_Dynamicfiltersselector2, "ESTADO_NOME") == 0 ) && ( AV64WWMunicipioDS_8_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66WWMunicipioDS_10_Estado_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Estado_Nome] like @lV66WWMunicipioDS_10_Estado_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Estado_Nome] like @lV66WWMunicipioDS_10_Estado_nome2)";
            }
         }
         else
         {
            GXv_int5[8] = 1;
         }
         if ( AV62WWMunicipioDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV63WWMunicipioDS_7_Dynamicfiltersselector2, "ESTADO_NOME") == 0 ) && ( AV64WWMunicipioDS_8_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66WWMunicipioDS_10_Estado_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Estado_Nome] like '%' + @lV66WWMunicipioDS_10_Estado_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Estado_Nome] like '%' + @lV66WWMunicipioDS_10_Estado_nome2)";
            }
         }
         else
         {
            GXv_int5[9] = 1;
         }
         if ( AV62WWMunicipioDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV63WWMunicipioDS_7_Dynamicfiltersselector2, "PAIS_NOME") == 0 ) && ( AV64WWMunicipioDS_8_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67WWMunicipioDS_11_Pais_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pais_Nome] like @lV67WWMunicipioDS_11_Pais_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pais_Nome] like @lV67WWMunicipioDS_11_Pais_nome2)";
            }
         }
         else
         {
            GXv_int5[10] = 1;
         }
         if ( AV62WWMunicipioDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV63WWMunicipioDS_7_Dynamicfiltersselector2, "PAIS_NOME") == 0 ) && ( AV64WWMunicipioDS_8_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67WWMunicipioDS_11_Pais_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pais_Nome] like '%' + @lV67WWMunicipioDS_11_Pais_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pais_Nome] like '%' + @lV67WWMunicipioDS_11_Pais_nome2)";
            }
         }
         else
         {
            GXv_int5[11] = 1;
         }
         if ( AV68WWMunicipioDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV69WWMunicipioDS_13_Dynamicfiltersselector3, "MUNICIPIO_NOME") == 0 ) && ( AV70WWMunicipioDS_14_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV71WWMunicipioDS_15_Municipio_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Municipio_Nome] like @lV71WWMunicipioDS_15_Municipio_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Municipio_Nome] like @lV71WWMunicipioDS_15_Municipio_nome3)";
            }
         }
         else
         {
            GXv_int5[12] = 1;
         }
         if ( AV68WWMunicipioDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV69WWMunicipioDS_13_Dynamicfiltersselector3, "MUNICIPIO_NOME") == 0 ) && ( AV70WWMunicipioDS_14_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV71WWMunicipioDS_15_Municipio_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Municipio_Nome] like '%' + @lV71WWMunicipioDS_15_Municipio_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Municipio_Nome] like '%' + @lV71WWMunicipioDS_15_Municipio_nome3)";
            }
         }
         else
         {
            GXv_int5[13] = 1;
         }
         if ( AV68WWMunicipioDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV69WWMunicipioDS_13_Dynamicfiltersselector3, "ESTADO_NOME") == 0 ) && ( AV70WWMunicipioDS_14_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV72WWMunicipioDS_16_Estado_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Estado_Nome] like @lV72WWMunicipioDS_16_Estado_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Estado_Nome] like @lV72WWMunicipioDS_16_Estado_nome3)";
            }
         }
         else
         {
            GXv_int5[14] = 1;
         }
         if ( AV68WWMunicipioDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV69WWMunicipioDS_13_Dynamicfiltersselector3, "ESTADO_NOME") == 0 ) && ( AV70WWMunicipioDS_14_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV72WWMunicipioDS_16_Estado_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Estado_Nome] like '%' + @lV72WWMunicipioDS_16_Estado_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Estado_Nome] like '%' + @lV72WWMunicipioDS_16_Estado_nome3)";
            }
         }
         else
         {
            GXv_int5[15] = 1;
         }
         if ( AV68WWMunicipioDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV69WWMunicipioDS_13_Dynamicfiltersselector3, "PAIS_NOME") == 0 ) && ( AV70WWMunicipioDS_14_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73WWMunicipioDS_17_Pais_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pais_Nome] like @lV73WWMunicipioDS_17_Pais_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pais_Nome] like @lV73WWMunicipioDS_17_Pais_nome3)";
            }
         }
         else
         {
            GXv_int5[16] = 1;
         }
         if ( AV68WWMunicipioDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV69WWMunicipioDS_13_Dynamicfiltersselector3, "PAIS_NOME") == 0 ) && ( AV70WWMunicipioDS_14_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73WWMunicipioDS_17_Pais_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pais_Nome] like '%' + @lV73WWMunicipioDS_17_Pais_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pais_Nome] like '%' + @lV73WWMunicipioDS_17_Pais_nome3)";
            }
         }
         else
         {
            GXv_int5[17] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV75WWMunicipioDS_19_Tfmunicipio_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74WWMunicipioDS_18_Tfmunicipio_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Municipio_Nome] like @lV74WWMunicipioDS_18_Tfmunicipio_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Municipio_Nome] like @lV74WWMunicipioDS_18_Tfmunicipio_nome)";
            }
         }
         else
         {
            GXv_int5[18] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV75WWMunicipioDS_19_Tfmunicipio_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Municipio_Nome] = @AV75WWMunicipioDS_19_Tfmunicipio_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Municipio_Nome] = @AV75WWMunicipioDS_19_Tfmunicipio_nome_sel)";
            }
         }
         else
         {
            GXv_int5[19] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV77WWMunicipioDS_21_Tfestado_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV76WWMunicipioDS_20_Tfestado_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Estado_Nome] like @lV76WWMunicipioDS_20_Tfestado_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Estado_Nome] like @lV76WWMunicipioDS_20_Tfestado_nome)";
            }
         }
         else
         {
            GXv_int5[20] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV77WWMunicipioDS_21_Tfestado_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Estado_Nome] = @AV77WWMunicipioDS_21_Tfestado_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Estado_Nome] = @AV77WWMunicipioDS_21_Tfestado_nome_sel)";
            }
         }
         else
         {
            GXv_int5[21] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV79WWMunicipioDS_23_Tfpais_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV78WWMunicipioDS_22_Tfpais_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pais_Nome] like @lV78WWMunicipioDS_22_Tfpais_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pais_Nome] like @lV78WWMunicipioDS_22_Tfpais_nome)";
            }
         }
         else
         {
            GXv_int5[22] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV79WWMunicipioDS_23_Tfpais_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pais_Nome] = @AV79WWMunicipioDS_23_Tfpais_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pais_Nome] = @AV79WWMunicipioDS_23_Tfpais_nome_sel)";
            }
         }
         else
         {
            GXv_int5[23] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[Pais_Codigo]";
         GXv_Object6[0] = scmdbuf;
         GXv_Object6[1] = GXv_int5;
         return GXv_Object6 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00ER2(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (short)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (bool)dynConstraints[11] , (String)dynConstraints[12] , (short)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] );
               case 1 :
                     return conditional_P00ER3(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (short)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (bool)dynConstraints[11] , (String)dynConstraints[12] , (short)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] );
               case 2 :
                     return conditional_P00ER4(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (short)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (bool)dynConstraints[11] , (String)dynConstraints[12] , (short)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00ER2 ;
          prmP00ER2 = new Object[] {
          new Object[] {"@lV59WWMunicipioDS_3_Municipio_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV59WWMunicipioDS_3_Municipio_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV60WWMunicipioDS_4_Estado_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV60WWMunicipioDS_4_Estado_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV61WWMunicipioDS_5_Pais_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV61WWMunicipioDS_5_Pais_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV65WWMunicipioDS_9_Municipio_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV65WWMunicipioDS_9_Municipio_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV66WWMunicipioDS_10_Estado_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV66WWMunicipioDS_10_Estado_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV67WWMunicipioDS_11_Pais_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV67WWMunicipioDS_11_Pais_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV71WWMunicipioDS_15_Municipio_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV71WWMunicipioDS_15_Municipio_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV72WWMunicipioDS_16_Estado_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV72WWMunicipioDS_16_Estado_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV73WWMunicipioDS_17_Pais_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV73WWMunicipioDS_17_Pais_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV74WWMunicipioDS_18_Tfmunicipio_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV75WWMunicipioDS_19_Tfmunicipio_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV76WWMunicipioDS_20_Tfestado_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV77WWMunicipioDS_21_Tfestado_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV78WWMunicipioDS_22_Tfpais_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV79WWMunicipioDS_23_Tfpais_nome_sel",SqlDbType.Char,50,0}
          } ;
          Object[] prmP00ER3 ;
          prmP00ER3 = new Object[] {
          new Object[] {"@lV59WWMunicipioDS_3_Municipio_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV59WWMunicipioDS_3_Municipio_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV60WWMunicipioDS_4_Estado_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV60WWMunicipioDS_4_Estado_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV61WWMunicipioDS_5_Pais_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV61WWMunicipioDS_5_Pais_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV65WWMunicipioDS_9_Municipio_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV65WWMunicipioDS_9_Municipio_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV66WWMunicipioDS_10_Estado_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV66WWMunicipioDS_10_Estado_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV67WWMunicipioDS_11_Pais_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV67WWMunicipioDS_11_Pais_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV71WWMunicipioDS_15_Municipio_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV71WWMunicipioDS_15_Municipio_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV72WWMunicipioDS_16_Estado_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV72WWMunicipioDS_16_Estado_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV73WWMunicipioDS_17_Pais_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV73WWMunicipioDS_17_Pais_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV74WWMunicipioDS_18_Tfmunicipio_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV75WWMunicipioDS_19_Tfmunicipio_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV76WWMunicipioDS_20_Tfestado_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV77WWMunicipioDS_21_Tfestado_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV78WWMunicipioDS_22_Tfpais_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV79WWMunicipioDS_23_Tfpais_nome_sel",SqlDbType.Char,50,0}
          } ;
          Object[] prmP00ER4 ;
          prmP00ER4 = new Object[] {
          new Object[] {"@lV59WWMunicipioDS_3_Municipio_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV59WWMunicipioDS_3_Municipio_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV60WWMunicipioDS_4_Estado_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV60WWMunicipioDS_4_Estado_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV61WWMunicipioDS_5_Pais_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV61WWMunicipioDS_5_Pais_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV65WWMunicipioDS_9_Municipio_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV65WWMunicipioDS_9_Municipio_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV66WWMunicipioDS_10_Estado_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV66WWMunicipioDS_10_Estado_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV67WWMunicipioDS_11_Pais_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV67WWMunicipioDS_11_Pais_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV71WWMunicipioDS_15_Municipio_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV71WWMunicipioDS_15_Municipio_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV72WWMunicipioDS_16_Estado_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV72WWMunicipioDS_16_Estado_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV73WWMunicipioDS_17_Pais_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV73WWMunicipioDS_17_Pais_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV74WWMunicipioDS_18_Tfmunicipio_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV75WWMunicipioDS_19_Tfmunicipio_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV76WWMunicipioDS_20_Tfestado_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV77WWMunicipioDS_21_Tfestado_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV78WWMunicipioDS_22_Tfpais_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV79WWMunicipioDS_23_Tfpais_nome_sel",SqlDbType.Char,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00ER2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00ER2,100,0,true,false )
             ,new CursorDef("P00ER3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00ER3,100,0,true,false )
             ,new CursorDef("P00ER4", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00ER4,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getString(1, 2) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 50) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 50) ;
                ((String[]) buf[4])[0] = rslt.getString(5, 50) ;
                ((int[]) buf[5])[0] = rslt.getInt(6) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 50) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 50) ;
                ((String[]) buf[4])[0] = rslt.getString(5, 50) ;
                ((int[]) buf[5])[0] = rslt.getInt(6) ;
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getString(1, 2) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 50) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 50) ;
                ((String[]) buf[4])[0] = rslt.getString(5, 50) ;
                ((int[]) buf[5])[0] = rslt.getInt(6) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                return;
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getwwmunicipiofilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getwwmunicipiofilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getwwmunicipiofilterdata") )
          {
             return  ;
          }
          getwwmunicipiofilterdata worker = new getwwmunicipiofilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
