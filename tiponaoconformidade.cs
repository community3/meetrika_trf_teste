/*
               File: TipoNaoConformidade
        Description: Tipo de N�o Conformidade
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 23:9:54.50
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class tiponaoconformidade : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Tipo de N�o Conformidade", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtTipoNaoCnf_Codigo_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public tiponaoconformidade( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public tiponaoconformidade( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_51223( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_51223e( bool wbgen )
      {
         if ( wbgen )
         {
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_51223( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableBorder100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_51223( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_51223e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Control Group */
            GxWebStd.gx_group_start( context, grpGroupdata_Internalname, "Tipo de N�o Conformidade", 1, 0, "px", 0, "px", "Group", " "+"style=\"-moz-border-radius: 3pt;;\""+" ", "HLP_TipoNaoConformidade.htm");
            wb_table3_28_51223( true) ;
         }
         return  ;
      }

      protected void wb_table3_28_51223e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</fieldset>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_51223e( true) ;
         }
         else
         {
            wb_table1_2_51223e( false) ;
         }
      }

      protected void wb_table3_28_51223( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_34_51223( true) ;
         }
         return  ;
      }

      protected void wb_table4_34_51223e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 47,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_enter_Internalname, "", "Confirmar", bttBtn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_enter_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_TipoNaoConformidade.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 48,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_cancel_Internalname, "", "Fechar", bttBtn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_TipoNaoConformidade.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'',0)\"";
            ClassString = "BtnDelete";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_delete_Internalname, "", "Eliminar", bttBtn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_delete_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_TipoNaoConformidade.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_28_51223e( true) ;
         }
         else
         {
            wb_table3_28_51223e( false) ;
         }
      }

      protected void wb_table4_34_51223( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable2_Internalname, tblTable2_Internalname, "", "Container", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocktiponaocnf_codigo_Internalname, "Id", "", "", lblTextblocktiponaocnf_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_TipoNaoConformidade.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtTipoNaoCnf_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A2018TipoNaoCnf_Codigo), 6, 0, ",", "")), ((edtTipoNaoCnf_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A2018TipoNaoCnf_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A2018TipoNaoCnf_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,39);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtTipoNaoCnf_Codigo_Jsonclick, 0, "Attribute", "", "", "", 1, edtTipoNaoCnf_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_TipoNaoConformidade.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocktiponaocnf_nome_Internalname, "Nome", "", "", lblTextblocktiponaocnf_nome_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_TipoNaoConformidade.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtTipoNaoCnf_Nome_Internalname, StringUtil.RTrim( A2019TipoNaoCnf_Nome), StringUtil.RTrim( context.localUtil.Format( A2019TipoNaoCnf_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,44);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtTipoNaoCnf_Nome_Jsonclick, 0, "BootstrapAttribute100", "", "", "", 1, edtTipoNaoCnf_Nome_Enabled, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "Nome100", "left", true, "HLP_TipoNaoConformidade.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_34_51223e( true) ;
         }
         else
         {
            wb_table4_34_51223e( false) ;
         }
      }

      protected void wb_table2_5_51223( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabletoolbar_Internalname, tblTabletoolbar_Internalname, "", "ViewTable", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divSectiontoolbar_Internalname, 1, 0, "px", 0, "px", "ToolbarMain", "left", "top", "", "WHITE-SPACE: nowrap;", "div");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 9,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_first_Internalname, context.GetImagePath( "b9e06284-17ac-4c88-8937-5dbd84ad5d80", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_first_Visible, 1, "", "Primeiro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_first_Jsonclick, "'"+""+"'"+",false,"+"'"+"EFIRST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_TipoNaoConformidade.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_first_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_first_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_first_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EFIRST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_TipoNaoConformidade.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_previous_Internalname, context.GetImagePath( "7d212604-db7b-4785-9c0d-5faffe71aa33", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_previous_Visible, 1, "", "Anterior", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_previous_Jsonclick, "'"+""+"'"+",false,"+"'"+"EPREVIOUS."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_TipoNaoConformidade.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 12,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_previous_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_previous_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_previous_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EPREVIOUS."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_TipoNaoConformidade.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 13,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_next_Internalname, context.GetImagePath( "1ae947cf-1354-41a9-8d59-d91daebf554f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_next_Visible, 1, "", "Pr�ximo", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_next_Jsonclick, "'"+""+"'"+",false,"+"'"+"ENEXT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_TipoNaoConformidade.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 14,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_next_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_next_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_next_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ENEXT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_TipoNaoConformidade.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 15,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_last_Internalname, context.GetImagePath( "29211874-e613-48e5-9011-8017d984217e", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_last_Visible, 1, "", "�ltimo", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_last_Jsonclick, "'"+""+"'"+",false,"+"'"+"ELAST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_TipoNaoConformidade.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_last_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_last_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_last_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ELAST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_TipoNaoConformidade.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_select_Internalname, context.GetImagePath( "1ca03f75-9947-4d2c-90a4-e8ab9c5cedea", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_select_Visible, 1, "", "Selecionar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_select_Jsonclick, "'"+""+"'"+",false,"+"'"+"ESELECT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_TipoNaoConformidade.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_select_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_select_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_select_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ESELECT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_TipoNaoConformidade.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_enter2_Internalname, context.GetImagePath( "2061cf2c-bd33-4433-a13e-34af954142e9", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_enter2_Visible, imgBtn_enter2_Enabled, "", "Confirmar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_enter2_Jsonclick, "'"+""+"'"+",false,"+"'"+"EENTER."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_TipoNaoConformidade.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_enter2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_enter2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_enter2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EENTER."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_TipoNaoConformidade.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_cancel2_Internalname, context.GetImagePath( "0e94ced8-bc34-47ff-9a53-bc683736a686", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_cancel2_Visible, 1, "", "Fechar", 0, 0, 0, "px", 0, "px", 0, 0, 1, imgBtn_cancel2_Jsonclick, "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_TipoNaoConformidade.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 22,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_cancel2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_cancel2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 1, imgBtn_cancel2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_TipoNaoConformidade.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_delete2_Internalname, context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_delete2_Visible, imgBtn_delete2_Enabled, "", "Eliminar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_delete2_Jsonclick, "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_TipoNaoConformidade.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_delete2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_delete2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_delete2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_TipoNaoConformidade.htm");
            GxWebStd.gx_div_end( context, "left", "top");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_51223e( true) ;
         }
         else
         {
            wb_table2_5_51223e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               if ( ( ( context.localUtil.CToN( cgiGet( edtTipoNaoCnf_Codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtTipoNaoCnf_Codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "TIPONAOCNF_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtTipoNaoCnf_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A2018TipoNaoCnf_Codigo = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2018TipoNaoCnf_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2018TipoNaoCnf_Codigo), 6, 0)));
               }
               else
               {
                  A2018TipoNaoCnf_Codigo = (int)(context.localUtil.CToN( cgiGet( edtTipoNaoCnf_Codigo_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2018TipoNaoCnf_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2018TipoNaoCnf_Codigo), 6, 0)));
               }
               A2019TipoNaoCnf_Nome = StringUtil.Upper( cgiGet( edtTipoNaoCnf_Nome_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2019TipoNaoCnf_Nome", A2019TipoNaoCnf_Nome);
               /* Read saved values. */
               Z2018TipoNaoCnf_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z2018TipoNaoCnf_Codigo"), ",", "."));
               Z2019TipoNaoCnf_Nome = cgiGet( "Z2019TipoNaoCnf_Nome");
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               Gx_mode = cgiGet( "vMODE");
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  A2018TipoNaoCnf_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2018TipoNaoCnf_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2018TipoNaoCnf_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  disable_std_buttons_dsp( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  standaloneModal( ) ;
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_enter( ) ;
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                        else if ( StringUtil.StrCmp(sEvt, "FIRST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_first( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "PREVIOUS") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_previous( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "NEXT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_next( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LAST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_last( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "SELECT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_select( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DELETE") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_delete( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           AfterKeyLoadScreen( ) ;
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll51223( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            imgBtn_delete2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
      }

      protected void disable_std_buttons_dsp( )
      {
         imgBtn_delete2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Visible), 5, 0)));
         imgBtn_delete2_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_separator_Visible), 5, 0)));
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)));
         imgBtn_first_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_first_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_first_Visible), 5, 0)));
         imgBtn_first_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_first_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_first_separator_Visible), 5, 0)));
         imgBtn_previous_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_previous_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_previous_Visible), 5, 0)));
         imgBtn_previous_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_previous_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_previous_separator_Visible), 5, 0)));
         imgBtn_next_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_next_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_next_Visible), 5, 0)));
         imgBtn_next_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_next_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_next_separator_Visible), 5, 0)));
         imgBtn_last_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_last_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_last_Visible), 5, 0)));
         imgBtn_last_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_last_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_last_separator_Visible), 5, 0)));
         imgBtn_select_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_select_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_select_Visible), 5, 0)));
         imgBtn_select_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_select_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_select_separator_Visible), 5, 0)));
         imgBtn_delete2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Visible), 5, 0)));
         imgBtn_delete2_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_separator_Visible), 5, 0)));
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)));
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            imgBtn_enter2_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Visible), 5, 0)));
            imgBtn_enter2_separator_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_separator_Visible), 5, 0)));
            bttBtn_enter_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_enter_Visible), 5, 0)));
         }
         DisableAttributes51223( ) ;
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void ResetCaption510( )
      {
      }

      protected void ZM51223( short GX_JID )
      {
         if ( ( GX_JID == 1 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z2019TipoNaoCnf_Nome = T00513_A2019TipoNaoCnf_Nome[0];
            }
            else
            {
               Z2019TipoNaoCnf_Nome = A2019TipoNaoCnf_Nome;
            }
         }
         if ( GX_JID == -1 )
         {
            Z2018TipoNaoCnf_Codigo = A2018TipoNaoCnf_Codigo;
            Z2019TipoNaoCnf_Nome = A2019TipoNaoCnf_Nome;
         }
      }

      protected void standaloneNotModal( )
      {
      }

      protected void standaloneModal( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            imgBtn_delete2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
         else
         {
            imgBtn_delete2_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            imgBtn_enter2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Enabled), 5, 0)));
         }
         else
         {
            imgBtn_enter2_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Enabled), 5, 0)));
         }
      }

      protected void Load51223( )
      {
         /* Using cursor T00514 */
         pr_default.execute(2, new Object[] {A2018TipoNaoCnf_Codigo});
         if ( (pr_default.getStatus(2) != 101) )
         {
            RcdFound223 = 1;
            A2019TipoNaoCnf_Nome = T00514_A2019TipoNaoCnf_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2019TipoNaoCnf_Nome", A2019TipoNaoCnf_Nome);
            ZM51223( -1) ;
         }
         pr_default.close(2);
         OnLoadActions51223( ) ;
      }

      protected void OnLoadActions51223( )
      {
      }

      protected void CheckExtendedTable51223( )
      {
         Gx_BScreen = 1;
         standaloneModal( ) ;
      }

      protected void CloseExtendedTableCursors51223( )
      {
      }

      protected void enableDisable( )
      {
      }

      protected void GetKey51223( )
      {
         /* Using cursor T00515 */
         pr_default.execute(3, new Object[] {A2018TipoNaoCnf_Codigo});
         if ( (pr_default.getStatus(3) != 101) )
         {
            RcdFound223 = 1;
         }
         else
         {
            RcdFound223 = 0;
         }
         pr_default.close(3);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T00513 */
         pr_default.execute(1, new Object[] {A2018TipoNaoCnf_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM51223( 1) ;
            RcdFound223 = 1;
            A2018TipoNaoCnf_Codigo = T00513_A2018TipoNaoCnf_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2018TipoNaoCnf_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2018TipoNaoCnf_Codigo), 6, 0)));
            A2019TipoNaoCnf_Nome = T00513_A2019TipoNaoCnf_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2019TipoNaoCnf_Nome", A2019TipoNaoCnf_Nome);
            Z2018TipoNaoCnf_Codigo = A2018TipoNaoCnf_Codigo;
            sMode223 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Load51223( ) ;
            if ( AnyError == 1 )
            {
               RcdFound223 = 0;
               InitializeNonKey51223( ) ;
            }
            Gx_mode = sMode223;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            RcdFound223 = 0;
            InitializeNonKey51223( ) ;
            sMode223 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Gx_mode = sMode223;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey51223( ) ;
         if ( RcdFound223 == 0 )
         {
            Gx_mode = "INS";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound223 = 0;
         /* Using cursor T00516 */
         pr_default.execute(4, new Object[] {A2018TipoNaoCnf_Codigo});
         if ( (pr_default.getStatus(4) != 101) )
         {
            while ( (pr_default.getStatus(4) != 101) && ( ( T00516_A2018TipoNaoCnf_Codigo[0] < A2018TipoNaoCnf_Codigo ) ) )
            {
               pr_default.readNext(4);
            }
            if ( (pr_default.getStatus(4) != 101) && ( ( T00516_A2018TipoNaoCnf_Codigo[0] > A2018TipoNaoCnf_Codigo ) ) )
            {
               A2018TipoNaoCnf_Codigo = T00516_A2018TipoNaoCnf_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2018TipoNaoCnf_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2018TipoNaoCnf_Codigo), 6, 0)));
               RcdFound223 = 1;
            }
         }
         pr_default.close(4);
      }

      protected void move_previous( )
      {
         RcdFound223 = 0;
         /* Using cursor T00517 */
         pr_default.execute(5, new Object[] {A2018TipoNaoCnf_Codigo});
         if ( (pr_default.getStatus(5) != 101) )
         {
            while ( (pr_default.getStatus(5) != 101) && ( ( T00517_A2018TipoNaoCnf_Codigo[0] > A2018TipoNaoCnf_Codigo ) ) )
            {
               pr_default.readNext(5);
            }
            if ( (pr_default.getStatus(5) != 101) && ( ( T00517_A2018TipoNaoCnf_Codigo[0] < A2018TipoNaoCnf_Codigo ) ) )
            {
               A2018TipoNaoCnf_Codigo = T00517_A2018TipoNaoCnf_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2018TipoNaoCnf_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2018TipoNaoCnf_Codigo), 6, 0)));
               RcdFound223 = 1;
            }
         }
         pr_default.close(5);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey51223( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtTipoNaoCnf_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert51223( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound223 == 1 )
            {
               if ( A2018TipoNaoCnf_Codigo != Z2018TipoNaoCnf_Codigo )
               {
                  A2018TipoNaoCnf_Codigo = Z2018TipoNaoCnf_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2018TipoNaoCnf_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2018TipoNaoCnf_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "TIPONAOCNF_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtTipoNaoCnf_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtTipoNaoCnf_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  Gx_mode = "UPD";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Update record */
                  Update51223( ) ;
                  GX_FocusControl = edtTipoNaoCnf_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A2018TipoNaoCnf_Codigo != Z2018TipoNaoCnf_Codigo )
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Insert record */
                  GX_FocusControl = edtTipoNaoCnf_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert51223( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "TIPONAOCNF_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtTipoNaoCnf_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     Gx_mode = "INS";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     /* Insert record */
                     GX_FocusControl = edtTipoNaoCnf_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert51223( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
      }

      protected void btn_delete( )
      {
         if ( A2018TipoNaoCnf_Codigo != Z2018TipoNaoCnf_Codigo )
         {
            A2018TipoNaoCnf_Codigo = Z2018TipoNaoCnf_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2018TipoNaoCnf_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2018TipoNaoCnf_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "TIPONAOCNF_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtTipoNaoCnf_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtTipoNaoCnf_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            getByPrimaryKey( ) ;
         }
         CloseOpenCursors();
      }

      protected void btn_get( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         if ( RcdFound223 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "TIPONAOCNF_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtTipoNaoCnf_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GX_FocusControl = edtTipoNaoCnf_Nome_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_first( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart51223( ) ;
         if ( RcdFound223 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtTipoNaoCnf_Nome_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         ScanEnd51223( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_previous( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_previous( ) ;
         if ( RcdFound223 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtTipoNaoCnf_Nome_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_next( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_next( ) ;
         if ( RcdFound223 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtTipoNaoCnf_Nome_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_last( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart51223( ) ;
         if ( RcdFound223 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            while ( RcdFound223 != 0 )
            {
               ScanNext51223( ) ;
            }
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtTipoNaoCnf_Nome_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         ScanEnd51223( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_select( )
      {
         getEqualNoModal( ) ;
      }

      protected void CheckOptimisticConcurrency51223( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T00512 */
            pr_default.execute(0, new Object[] {A2018TipoNaoCnf_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"TipoNaoConformidade"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z2019TipoNaoCnf_Nome, T00512_A2019TipoNaoCnf_Nome[0]) != 0 ) )
            {
               if ( StringUtil.StrCmp(Z2019TipoNaoCnf_Nome, T00512_A2019TipoNaoCnf_Nome[0]) != 0 )
               {
                  GXUtil.WriteLog("tiponaoconformidade:[seudo value changed for attri]"+"TipoNaoCnf_Nome");
                  GXUtil.WriteLogRaw("Old: ",Z2019TipoNaoCnf_Nome);
                  GXUtil.WriteLogRaw("Current: ",T00512_A2019TipoNaoCnf_Nome[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"TipoNaoConformidade"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert51223( )
      {
         BeforeValidate51223( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable51223( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM51223( 0) ;
            CheckOptimisticConcurrency51223( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm51223( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert51223( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T00518 */
                     pr_default.execute(6, new Object[] {A2019TipoNaoCnf_Nome});
                     A2018TipoNaoCnf_Codigo = T00518_A2018TipoNaoCnf_Codigo[0];
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2018TipoNaoCnf_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2018TipoNaoCnf_Codigo), 6, 0)));
                     pr_default.close(6);
                     dsDefault.SmartCacheProvider.SetUpdated("TipoNaoConformidade") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption510( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load51223( ) ;
            }
            EndLevel51223( ) ;
         }
         CloseExtendedTableCursors51223( ) ;
      }

      protected void Update51223( )
      {
         BeforeValidate51223( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable51223( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency51223( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm51223( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate51223( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T00519 */
                     pr_default.execute(7, new Object[] {A2019TipoNaoCnf_Nome, A2018TipoNaoCnf_Codigo});
                     pr_default.close(7);
                     dsDefault.SmartCacheProvider.SetUpdated("TipoNaoConformidade") ;
                     if ( (pr_default.getStatus(7) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"TipoNaoConformidade"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate51223( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                           ResetCaption510( ) ;
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel51223( ) ;
         }
         CloseExtendedTableCursors51223( ) ;
      }

      protected void DeferredUpdate51223( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         BeforeValidate51223( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency51223( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls51223( ) ;
            AfterConfirm51223( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete51223( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T005110 */
                  pr_default.execute(8, new Object[] {A2018TipoNaoCnf_Codigo});
                  pr_default.close(8);
                  dsDefault.SmartCacheProvider.SetUpdated("TipoNaoConformidade") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        move_next( ) ;
                        if ( RcdFound223 == 0 )
                        {
                           InitAll51223( ) ;
                           Gx_mode = "INS";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        }
                        else
                        {
                           getByPrimaryKey( ) ;
                           Gx_mode = "UPD";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        }
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                        ResetCaption510( ) ;
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode223 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         EndLevel51223( ) ;
         Gx_mode = sMode223;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
      }

      protected void OnDeleteControls51223( )
      {
         standaloneModal( ) ;
         /* No delete mode formulas found. */
         if ( AnyError == 0 )
         {
            /* Using cursor T005111 */
            pr_default.execute(9, new Object[] {A2018TipoNaoCnf_Codigo});
            if ( (pr_default.getStatus(9) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"N�o Conformidade"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(9);
         }
      }

      protected void EndLevel51223( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete51223( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            context.CommitDataStores( "TipoNaoConformidade");
            if ( AnyError == 0 )
            {
               ConfirmValues510( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            context.RollbackDataStores( "TipoNaoConformidade");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart51223( )
      {
         /* Using cursor T005112 */
         pr_default.execute(10);
         RcdFound223 = 0;
         if ( (pr_default.getStatus(10) != 101) )
         {
            RcdFound223 = 1;
            A2018TipoNaoCnf_Codigo = T005112_A2018TipoNaoCnf_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2018TipoNaoCnf_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2018TipoNaoCnf_Codigo), 6, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext51223( )
      {
         /* Scan next routine */
         pr_default.readNext(10);
         RcdFound223 = 0;
         if ( (pr_default.getStatus(10) != 101) )
         {
            RcdFound223 = 1;
            A2018TipoNaoCnf_Codigo = T005112_A2018TipoNaoCnf_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2018TipoNaoCnf_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2018TipoNaoCnf_Codigo), 6, 0)));
         }
      }

      protected void ScanEnd51223( )
      {
         pr_default.close(10);
      }

      protected void AfterConfirm51223( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert51223( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate51223( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete51223( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete51223( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate51223( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes51223( )
      {
         edtTipoNaoCnf_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtTipoNaoCnf_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtTipoNaoCnf_Codigo_Enabled), 5, 0)));
         edtTipoNaoCnf_Nome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtTipoNaoCnf_Nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtTipoNaoCnf_Nome_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues510( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020428239553");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("tiponaoconformidade.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z2018TipoNaoCnf_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z2018TipoNaoCnf_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z2019TipoNaoCnf_Nome", StringUtil.RTrim( Z2019TipoNaoCnf_Nome));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("tiponaoconformidade.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "TipoNaoConformidade" ;
      }

      public override String GetPgmdesc( )
      {
         return "Tipo de N�o Conformidade" ;
      }

      protected void InitializeNonKey51223( )
      {
         A2019TipoNaoCnf_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2019TipoNaoCnf_Nome", A2019TipoNaoCnf_Nome);
         Z2019TipoNaoCnf_Nome = "";
      }

      protected void InitAll51223( )
      {
         A2018TipoNaoCnf_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2018TipoNaoCnf_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2018TipoNaoCnf_Codigo), 6, 0)));
         InitializeNonKey51223( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?2249787");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020428239556");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("tiponaoconformidade.js", "?2020428239556");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         imgBtn_first_Internalname = "BTN_FIRST";
         imgBtn_first_separator_Internalname = "BTN_FIRST_SEPARATOR";
         imgBtn_previous_Internalname = "BTN_PREVIOUS";
         imgBtn_previous_separator_Internalname = "BTN_PREVIOUS_SEPARATOR";
         imgBtn_next_Internalname = "BTN_NEXT";
         imgBtn_next_separator_Internalname = "BTN_NEXT_SEPARATOR";
         imgBtn_last_Internalname = "BTN_LAST";
         imgBtn_last_separator_Internalname = "BTN_LAST_SEPARATOR";
         imgBtn_select_Internalname = "BTN_SELECT";
         imgBtn_select_separator_Internalname = "BTN_SELECT_SEPARATOR";
         imgBtn_enter2_Internalname = "BTN_ENTER2";
         imgBtn_enter2_separator_Internalname = "BTN_ENTER2_SEPARATOR";
         imgBtn_cancel2_Internalname = "BTN_CANCEL2";
         imgBtn_cancel2_separator_Internalname = "BTN_CANCEL2_SEPARATOR";
         imgBtn_delete2_Internalname = "BTN_DELETE2";
         imgBtn_delete2_separator_Internalname = "BTN_DELETE2_SEPARATOR";
         divSectiontoolbar_Internalname = "SECTIONTOOLBAR";
         tblTabletoolbar_Internalname = "TABLETOOLBAR";
         lblTextblocktiponaocnf_codigo_Internalname = "TEXTBLOCKTIPONAOCNF_CODIGO";
         edtTipoNaoCnf_Codigo_Internalname = "TIPONAOCNF_CODIGO";
         lblTextblocktiponaocnf_nome_Internalname = "TEXTBLOCKTIPONAOCNF_NOME";
         edtTipoNaoCnf_Nome_Internalname = "TIPONAOCNF_NOME";
         tblTable2_Internalname = "TABLE2";
         bttBtn_enter_Internalname = "BTN_ENTER";
         bttBtn_cancel_Internalname = "BTN_CANCEL";
         bttBtn_delete_Internalname = "BTN_DELETE";
         tblTable1_Internalname = "TABLE1";
         grpGroupdata_Internalname = "GROUPDATA";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Tipo de N�o Conformidade";
         imgBtn_delete2_separator_Visible = 1;
         imgBtn_delete2_Enabled = 1;
         imgBtn_delete2_Visible = 1;
         imgBtn_cancel2_separator_Visible = 1;
         imgBtn_cancel2_Visible = 1;
         imgBtn_enter2_separator_Visible = 1;
         imgBtn_enter2_Enabled = 1;
         imgBtn_enter2_Visible = 1;
         imgBtn_select_separator_Visible = 1;
         imgBtn_select_Visible = 1;
         imgBtn_last_separator_Visible = 1;
         imgBtn_last_Visible = 1;
         imgBtn_next_separator_Visible = 1;
         imgBtn_next_Visible = 1;
         imgBtn_previous_separator_Visible = 1;
         imgBtn_previous_Visible = 1;
         imgBtn_first_separator_Visible = 1;
         imgBtn_first_Visible = 1;
         edtTipoNaoCnf_Nome_Jsonclick = "";
         edtTipoNaoCnf_Nome_Enabled = 1;
         edtTipoNaoCnf_Codigo_Jsonclick = "";
         edtTipoNaoCnf_Codigo_Enabled = 1;
         bttBtn_delete_Visible = 1;
         bttBtn_cancel_Visible = 1;
         bttBtn_enter_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void AfterKeyLoadScreen( )
      {
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         GX_FocusControl = edtTipoNaoCnf_Nome_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         standaloneNotModal( ) ;
         standaloneModal( ) ;
         /* End function AfterKeyLoadScreen */
      }

      public void Valid_Tiponaocnf_codigo( int GX_Parm1 ,
                                           String GX_Parm2 )
      {
         A2018TipoNaoCnf_Codigo = GX_Parm1;
         A2019TipoNaoCnf_Nome = GX_Parm2;
         context.wbHandled = 1;
         AfterKeyLoadScreen( ) ;
         Draw( ) ;
         SendCloseFormHiddens( ) ;
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
         }
         isValidOutput.Add(StringUtil.RTrim( A2019TipoNaoCnf_Nome));
         isValidOutput.Add(StringUtil.RTrim( Gx_mode));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z2018TipoNaoCnf_Codigo), 6, 0, ",", "")));
         isValidOutput.Add(StringUtil.RTrim( Z2019TipoNaoCnf_Nome));
         isValidOutput.Add(imgBtn_delete2_Enabled);
         isValidOutput.Add(imgBtn_enter2_Enabled);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
      }

      public override void initialize( )
      {
         sPrefix = "";
         Z2019TipoNaoCnf_Nome = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtn_enter_Jsonclick = "";
         bttBtn_cancel_Jsonclick = "";
         bttBtn_delete_Jsonclick = "";
         lblTextblocktiponaocnf_codigo_Jsonclick = "";
         lblTextblocktiponaocnf_nome_Jsonclick = "";
         A2019TipoNaoCnf_Nome = "";
         imgBtn_first_Jsonclick = "";
         imgBtn_first_separator_Jsonclick = "";
         imgBtn_previous_Jsonclick = "";
         imgBtn_previous_separator_Jsonclick = "";
         imgBtn_next_Jsonclick = "";
         imgBtn_next_separator_Jsonclick = "";
         imgBtn_last_Jsonclick = "";
         imgBtn_last_separator_Jsonclick = "";
         imgBtn_select_Jsonclick = "";
         imgBtn_select_separator_Jsonclick = "";
         imgBtn_enter2_Jsonclick = "";
         imgBtn_enter2_separator_Jsonclick = "";
         imgBtn_cancel2_Jsonclick = "";
         imgBtn_cancel2_separator_Jsonclick = "";
         imgBtn_delete2_Jsonclick = "";
         imgBtn_delete2_separator_Jsonclick = "";
         Gx_mode = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         T00514_A2018TipoNaoCnf_Codigo = new int[1] ;
         T00514_A2019TipoNaoCnf_Nome = new String[] {""} ;
         T00515_A2018TipoNaoCnf_Codigo = new int[1] ;
         T00513_A2018TipoNaoCnf_Codigo = new int[1] ;
         T00513_A2019TipoNaoCnf_Nome = new String[] {""} ;
         sMode223 = "";
         T00516_A2018TipoNaoCnf_Codigo = new int[1] ;
         T00517_A2018TipoNaoCnf_Codigo = new int[1] ;
         T00512_A2018TipoNaoCnf_Codigo = new int[1] ;
         T00512_A2019TipoNaoCnf_Nome = new String[] {""} ;
         T00518_A2018TipoNaoCnf_Codigo = new int[1] ;
         T005111_A426NaoConformidade_Codigo = new int[1] ;
         T005112_A2018TipoNaoCnf_Codigo = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         isValidOutput = new GxUnknownObjectCollection();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.tiponaoconformidade__default(),
            new Object[][] {
                new Object[] {
               T00512_A2018TipoNaoCnf_Codigo, T00512_A2019TipoNaoCnf_Nome
               }
               , new Object[] {
               T00513_A2018TipoNaoCnf_Codigo, T00513_A2019TipoNaoCnf_Nome
               }
               , new Object[] {
               T00514_A2018TipoNaoCnf_Codigo, T00514_A2019TipoNaoCnf_Nome
               }
               , new Object[] {
               T00515_A2018TipoNaoCnf_Codigo
               }
               , new Object[] {
               T00516_A2018TipoNaoCnf_Codigo
               }
               , new Object[] {
               T00517_A2018TipoNaoCnf_Codigo
               }
               , new Object[] {
               T00518_A2018TipoNaoCnf_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T005111_A426NaoConformidade_Codigo
               }
               , new Object[] {
               T005112_A2018TipoNaoCnf_Codigo
               }
            }
         );
      }

      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short GX_JID ;
      private short RcdFound223 ;
      private short Gx_BScreen ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int Z2018TipoNaoCnf_Codigo ;
      private int trnEnded ;
      private int bttBtn_enter_Visible ;
      private int bttBtn_cancel_Visible ;
      private int bttBtn_delete_Visible ;
      private int A2018TipoNaoCnf_Codigo ;
      private int edtTipoNaoCnf_Codigo_Enabled ;
      private int edtTipoNaoCnf_Nome_Enabled ;
      private int imgBtn_first_Visible ;
      private int imgBtn_first_separator_Visible ;
      private int imgBtn_previous_Visible ;
      private int imgBtn_previous_separator_Visible ;
      private int imgBtn_next_Visible ;
      private int imgBtn_next_separator_Visible ;
      private int imgBtn_last_Visible ;
      private int imgBtn_last_separator_Visible ;
      private int imgBtn_select_Visible ;
      private int imgBtn_select_separator_Visible ;
      private int imgBtn_enter2_Visible ;
      private int imgBtn_enter2_Enabled ;
      private int imgBtn_enter2_separator_Visible ;
      private int imgBtn_cancel2_Visible ;
      private int imgBtn_cancel2_separator_Visible ;
      private int imgBtn_delete2_Visible ;
      private int imgBtn_delete2_Enabled ;
      private int imgBtn_delete2_separator_Visible ;
      private int idxLst ;
      private String sPrefix ;
      private String Z2019TipoNaoCnf_Nome ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtTipoNaoCnf_Codigo_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String grpGroupdata_Internalname ;
      private String tblTable1_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String TempTags ;
      private String bttBtn_enter_Internalname ;
      private String bttBtn_enter_Jsonclick ;
      private String bttBtn_cancel_Internalname ;
      private String bttBtn_cancel_Jsonclick ;
      private String bttBtn_delete_Internalname ;
      private String bttBtn_delete_Jsonclick ;
      private String tblTable2_Internalname ;
      private String lblTextblocktiponaocnf_codigo_Internalname ;
      private String lblTextblocktiponaocnf_codigo_Jsonclick ;
      private String edtTipoNaoCnf_Codigo_Jsonclick ;
      private String lblTextblocktiponaocnf_nome_Internalname ;
      private String lblTextblocktiponaocnf_nome_Jsonclick ;
      private String edtTipoNaoCnf_Nome_Internalname ;
      private String A2019TipoNaoCnf_Nome ;
      private String edtTipoNaoCnf_Nome_Jsonclick ;
      private String tblTabletoolbar_Internalname ;
      private String divSectiontoolbar_Internalname ;
      private String imgBtn_first_Internalname ;
      private String imgBtn_first_Jsonclick ;
      private String imgBtn_first_separator_Internalname ;
      private String imgBtn_first_separator_Jsonclick ;
      private String imgBtn_previous_Internalname ;
      private String imgBtn_previous_Jsonclick ;
      private String imgBtn_previous_separator_Internalname ;
      private String imgBtn_previous_separator_Jsonclick ;
      private String imgBtn_next_Internalname ;
      private String imgBtn_next_Jsonclick ;
      private String imgBtn_next_separator_Internalname ;
      private String imgBtn_next_separator_Jsonclick ;
      private String imgBtn_last_Internalname ;
      private String imgBtn_last_Jsonclick ;
      private String imgBtn_last_separator_Internalname ;
      private String imgBtn_last_separator_Jsonclick ;
      private String imgBtn_select_Internalname ;
      private String imgBtn_select_Jsonclick ;
      private String imgBtn_select_separator_Internalname ;
      private String imgBtn_select_separator_Jsonclick ;
      private String imgBtn_enter2_Internalname ;
      private String imgBtn_enter2_Jsonclick ;
      private String imgBtn_enter2_separator_Internalname ;
      private String imgBtn_enter2_separator_Jsonclick ;
      private String imgBtn_cancel2_Internalname ;
      private String imgBtn_cancel2_Jsonclick ;
      private String imgBtn_cancel2_separator_Internalname ;
      private String imgBtn_cancel2_separator_Jsonclick ;
      private String imgBtn_delete2_Internalname ;
      private String imgBtn_delete2_Jsonclick ;
      private String imgBtn_delete2_separator_Internalname ;
      private String imgBtn_delete2_separator_Jsonclick ;
      private String Gx_mode ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String sMode223 ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] T00514_A2018TipoNaoCnf_Codigo ;
      private String[] T00514_A2019TipoNaoCnf_Nome ;
      private int[] T00515_A2018TipoNaoCnf_Codigo ;
      private int[] T00513_A2018TipoNaoCnf_Codigo ;
      private String[] T00513_A2019TipoNaoCnf_Nome ;
      private int[] T00516_A2018TipoNaoCnf_Codigo ;
      private int[] T00517_A2018TipoNaoCnf_Codigo ;
      private int[] T00512_A2018TipoNaoCnf_Codigo ;
      private String[] T00512_A2019TipoNaoCnf_Nome ;
      private int[] T00518_A2018TipoNaoCnf_Codigo ;
      private int[] T005111_A426NaoConformidade_Codigo ;
      private int[] T005112_A2018TipoNaoCnf_Codigo ;
      private GXWebForm Form ;
   }

   public class tiponaoconformidade__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new UpdateCursor(def[7])
         ,new UpdateCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT00514 ;
          prmT00514 = new Object[] {
          new Object[] {"@TipoNaoCnf_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00515 ;
          prmT00515 = new Object[] {
          new Object[] {"@TipoNaoCnf_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00513 ;
          prmT00513 = new Object[] {
          new Object[] {"@TipoNaoCnf_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00516 ;
          prmT00516 = new Object[] {
          new Object[] {"@TipoNaoCnf_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00517 ;
          prmT00517 = new Object[] {
          new Object[] {"@TipoNaoCnf_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00512 ;
          prmT00512 = new Object[] {
          new Object[] {"@TipoNaoCnf_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00518 ;
          prmT00518 = new Object[] {
          new Object[] {"@TipoNaoCnf_Nome",SqlDbType.Char,100,0}
          } ;
          Object[] prmT00519 ;
          prmT00519 = new Object[] {
          new Object[] {"@TipoNaoCnf_Nome",SqlDbType.Char,100,0} ,
          new Object[] {"@TipoNaoCnf_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005110 ;
          prmT005110 = new Object[] {
          new Object[] {"@TipoNaoCnf_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005111 ;
          prmT005111 = new Object[] {
          new Object[] {"@TipoNaoCnf_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005112 ;
          prmT005112 = new Object[] {
          } ;
          def= new CursorDef[] {
              new CursorDef("T00512", "SELECT [TipoNaoCnf_Codigo], [TipoNaoCnf_Nome] FROM [TipoNaoConformidade] WITH (UPDLOCK) WHERE [TipoNaoCnf_Codigo] = @TipoNaoCnf_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00512,1,0,true,false )
             ,new CursorDef("T00513", "SELECT [TipoNaoCnf_Codigo], [TipoNaoCnf_Nome] FROM [TipoNaoConformidade] WITH (NOLOCK) WHERE [TipoNaoCnf_Codigo] = @TipoNaoCnf_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00513,1,0,true,false )
             ,new CursorDef("T00514", "SELECT TM1.[TipoNaoCnf_Codigo], TM1.[TipoNaoCnf_Nome] FROM [TipoNaoConformidade] TM1 WITH (NOLOCK) WHERE TM1.[TipoNaoCnf_Codigo] = @TipoNaoCnf_Codigo ORDER BY TM1.[TipoNaoCnf_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT00514,100,0,true,false )
             ,new CursorDef("T00515", "SELECT [TipoNaoCnf_Codigo] FROM [TipoNaoConformidade] WITH (NOLOCK) WHERE [TipoNaoCnf_Codigo] = @TipoNaoCnf_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT00515,1,0,true,false )
             ,new CursorDef("T00516", "SELECT TOP 1 [TipoNaoCnf_Codigo] FROM [TipoNaoConformidade] WITH (NOLOCK) WHERE ( [TipoNaoCnf_Codigo] > @TipoNaoCnf_Codigo) ORDER BY [TipoNaoCnf_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT00516,1,0,true,true )
             ,new CursorDef("T00517", "SELECT TOP 1 [TipoNaoCnf_Codigo] FROM [TipoNaoConformidade] WITH (NOLOCK) WHERE ( [TipoNaoCnf_Codigo] < @TipoNaoCnf_Codigo) ORDER BY [TipoNaoCnf_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT00517,1,0,true,true )
             ,new CursorDef("T00518", "INSERT INTO [TipoNaoConformidade]([TipoNaoCnf_Nome]) VALUES(@TipoNaoCnf_Nome); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT00518)
             ,new CursorDef("T00519", "UPDATE [TipoNaoConformidade] SET [TipoNaoCnf_Nome]=@TipoNaoCnf_Nome  WHERE [TipoNaoCnf_Codigo] = @TipoNaoCnf_Codigo", GxErrorMask.GX_NOMASK,prmT00519)
             ,new CursorDef("T005110", "DELETE FROM [TipoNaoConformidade]  WHERE [TipoNaoCnf_Codigo] = @TipoNaoCnf_Codigo", GxErrorMask.GX_NOMASK,prmT005110)
             ,new CursorDef("T005111", "SELECT TOP 1 [NaoConformidade_Codigo] FROM [NaoConformidade] WITH (NOLOCK) WHERE [NaoConformidade_TipoCod] = @TipoNaoCnf_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT005111,1,0,true,true )
             ,new CursorDef("T005112", "SELECT [TipoNaoCnf_Codigo] FROM [TipoNaoConformidade] WITH (NOLOCK) ORDER BY [TipoNaoCnf_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT005112,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 100) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 100) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 100) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (String)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
