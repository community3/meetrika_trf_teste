/*
               File: ParametrosSistema
        Description: Parametros do Sistema
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/18/2020 12:49:11.36
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Mail;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class parametrossistema : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV7ParametrosSistema_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7ParametrosSistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ParametrosSistema_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPARAMETROSSISTEMA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7ParametrosSistema_Codigo), "ZZZZZ9")));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         cmbParametrosSistema_PadronizarStrings.Name = "PARAMETROSSISTEMA_PADRONIZARSTRINGS";
         cmbParametrosSistema_PadronizarStrings.WebTags = "";
         cmbParametrosSistema_PadronizarStrings.addItem(StringUtil.BoolToStr( true), "Sim", 0);
         cmbParametrosSistema_PadronizarStrings.addItem(StringUtil.BoolToStr( false), "N�o", 0);
         if ( cmbParametrosSistema_PadronizarStrings.ItemCount > 0 )
         {
            if ( (false==A417ParametrosSistema_PadronizarStrings) )
            {
               A417ParametrosSistema_PadronizarStrings = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A417ParametrosSistema_PadronizarStrings", A417ParametrosSistema_PadronizarStrings);
            }
            A417ParametrosSistema_PadronizarStrings = StringUtil.StrToBool( cmbParametrosSistema_PadronizarStrings.getValidValue(StringUtil.BoolToStr( A417ParametrosSistema_PadronizarStrings)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A417ParametrosSistema_PadronizarStrings", A417ParametrosSistema_PadronizarStrings);
         }
         cmbParametrosSistema_EmailSdaAut.Name = "PARAMETROSSISTEMA_EMAILSDAAUT";
         cmbParametrosSistema_EmailSdaAut.WebTags = "";
         cmbParametrosSistema_EmailSdaAut.addItem(StringUtil.BoolToStr( true), "Sim", 0);
         cmbParametrosSistema_EmailSdaAut.addItem(StringUtil.BoolToStr( false), "N�o", 0);
         if ( cmbParametrosSistema_EmailSdaAut.ItemCount > 0 )
         {
            if ( (false==A535ParametrosSistema_EmailSdaAut) )
            {
               A535ParametrosSistema_EmailSdaAut = false;
               n535ParametrosSistema_EmailSdaAut = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A535ParametrosSistema_EmailSdaAut", A535ParametrosSistema_EmailSdaAut);
            }
            A535ParametrosSistema_EmailSdaAut = StringUtil.StrToBool( cmbParametrosSistema_EmailSdaAut.getValidValue(StringUtil.BoolToStr( A535ParametrosSistema_EmailSdaAut)));
            n535ParametrosSistema_EmailSdaAut = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A535ParametrosSistema_EmailSdaAut", A535ParametrosSistema_EmailSdaAut);
         }
         cmbParametrosSistema_EmailSdaSec.Name = "PARAMETROSSISTEMA_EMAILSDASEC";
         cmbParametrosSistema_EmailSdaSec.WebTags = "";
         cmbParametrosSistema_EmailSdaSec.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 4, 0)), "Nenhuma", 0);
         cmbParametrosSistema_EmailSdaSec.addItem("1", "SSL e TLS", 0);
         if ( cmbParametrosSistema_EmailSdaSec.ItemCount > 0 )
         {
            A2032ParametrosSistema_EmailSdaSec = (short)(NumberUtil.Val( cmbParametrosSistema_EmailSdaSec.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A2032ParametrosSistema_EmailSdaSec), 4, 0))), "."));
            n2032ParametrosSistema_EmailSdaSec = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2032ParametrosSistema_EmailSdaSec", StringUtil.LTrim( StringUtil.Str( (decimal)(A2032ParametrosSistema_EmailSdaSec), 4, 0)));
         }
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Parametros do Sistema", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtParametrosSistema_NomeSistema_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public parametrossistema( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public parametrossistema( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           int aP1_ParametrosSistema_Codigo )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV7ParametrosSistema_Codigo = aP1_ParametrosSistema_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbParametrosSistema_PadronizarStrings = new GXCombobox();
         cmbParametrosSistema_EmailSdaAut = new GXCombobox();
         cmbParametrosSistema_EmailSdaSec = new GXCombobox();
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbParametrosSistema_PadronizarStrings.ItemCount > 0 )
         {
            A417ParametrosSistema_PadronizarStrings = StringUtil.StrToBool( cmbParametrosSistema_PadronizarStrings.getValidValue(StringUtil.BoolToStr( A417ParametrosSistema_PadronizarStrings)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A417ParametrosSistema_PadronizarStrings", A417ParametrosSistema_PadronizarStrings);
         }
         if ( cmbParametrosSistema_EmailSdaAut.ItemCount > 0 )
         {
            A535ParametrosSistema_EmailSdaAut = StringUtil.StrToBool( cmbParametrosSistema_EmailSdaAut.getValidValue(StringUtil.BoolToStr( A535ParametrosSistema_EmailSdaAut)));
            n535ParametrosSistema_EmailSdaAut = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A535ParametrosSistema_EmailSdaAut", A535ParametrosSistema_EmailSdaAut);
         }
         if ( cmbParametrosSistema_EmailSdaSec.ItemCount > 0 )
         {
            A2032ParametrosSistema_EmailSdaSec = (short)(NumberUtil.Val( cmbParametrosSistema_EmailSdaSec.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A2032ParametrosSistema_EmailSdaSec), 4, 0))), "."));
            n2032ParametrosSistema_EmailSdaSec = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2032ParametrosSistema_EmailSdaSec", StringUtil.LTrim( StringUtil.Str( (decimal)(A2032ParametrosSistema_EmailSdaSec), 4, 0)));
         }
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_1J56( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_1J56e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 136,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtParametrosSistema_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A330ParametrosSistema_Codigo), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(A330ParametrosSistema_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,136);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtParametrosSistema_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtParametrosSistema_Codigo_Visible, edtParametrosSistema_Codigo_Enabled, 1, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ParametrosSistema.htm");
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtParametrosSistema_AppID_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A334ParametrosSistema_AppID), 12, 0, ",", "")), ((edtParametrosSistema_AppID_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A334ParametrosSistema_AppID), "ZZZZZZZZZZZ9")) : context.localUtil.Format( (decimal)(A334ParametrosSistema_AppID), "ZZZZZZZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtParametrosSistema_AppID_Jsonclick, 0, "Attribute", "", "", "", edtParametrosSistema_AppID_Visible, edtParametrosSistema_AppID_Enabled, 0, "text", "", 12, "chr", 1, "row", 12, 0, 0, 0, 1, -1, 0, true, "GAMKeyNumLong", "right", false, "HLP_ParametrosSistema.htm");
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_1J56( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_1J56( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_1J56e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_128_1J56( true) ;
         }
         return  ;
      }

      protected void wb_table3_128_1J56e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_1J56e( true) ;
         }
         else
         {
            wb_table1_2_1J56e( false) ;
         }
      }

      protected void wb_table3_128_1J56( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 131,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", "Confirmar", bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_trn_enter_Visible, bttBtn_trn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_ParametrosSistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 133,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "", "Fechar", bttBtn_trn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_trn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_ParametrosSistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 135,'',false,'',0)\"";
            ClassString = "btn btn-danger";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_delete_Internalname, "", "Eliminar", bttBtn_trn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_trn_delete_Visible, bttBtn_trn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_ParametrosSistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_128_1J56e( true) ;
         }
         else
         {
            wb_table3_128_1J56e( false) ;
         }
      }

      protected void wb_table2_5_1J56( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table4_13_1J56( true) ;
         }
         return  ;
      }

      protected void wb_table4_13_1J56e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_1J56e( true) ;
         }
         else
         {
            wb_table2_5_1J56e( false) ;
         }
      }

      protected void wb_table4_13_1J56( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "Table", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GXUITABSPANEL_TABSPARAMETROSContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"GXUITABSPANEL_TABSPARAMETROSContainer"+"TitleTabPrincipal"+"\" style=\"display:none;\">") ;
            context.WriteHtmlText( "Principal") ;
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"GXUITABSPANEL_TABSPARAMETROSContainer"+"TabPrincipal"+"\" style=\"display:none;\">") ;
            wb_table5_19_1J56( true) ;
         }
         return  ;
      }

      protected void wb_table5_19_1J56e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"GXUITABSPANEL_TABSPARAMETROSContainer"+"TitleTabHome"+"\" style=\"display:none;\">") ;
            context.WriteHtmlText( "Home") ;
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"GXUITABSPANEL_TABSPARAMETROSContainer"+"TabHome"+"\" style=\"display:none;\">") ;
            wb_table6_120_1J56( true) ;
         }
         return  ;
      }

      protected void wb_table6_120_1J56e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_13_1J56e( true) ;
         }
         else
         {
            wb_table4_13_1J56e( false) ;
         }
      }

      protected void wb_table6_120_1J56( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable1_Internalname, tblUnnamedtable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockparametrossistema_textohome_Internalname, "Texto Home", "", "", lblTextblockparametrossistema_textohome_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ParametrosSistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"PARAMETROSSISTEMA_TEXTOHOMEContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_120_1J56e( true) ;
         }
         else
         {
            wb_table6_120_1J56e( false) ;
         }
      }

      protected void wb_table5_19_1J56( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable2_Internalname, tblUnnamedtable2_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockparametrossistema_nomesistema_Internalname, "Nome do Sistema", "", "", lblTextblockparametrossistema_nomesistema_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ParametrosSistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtParametrosSistema_NomeSistema_Internalname, StringUtil.RTrim( A331ParametrosSistema_NomeSistema), StringUtil.RTrim( context.localUtil.Format( A331ParametrosSistema_NomeSistema, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,24);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtParametrosSistema_NomeSistema_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtParametrosSistema_NomeSistema_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_ParametrosSistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockparametrossistema_padronizarstrings_Internalname, "Padronizar Strings", "", "", lblTextblockparametrossistema_padronizarstrings_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ParametrosSistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 28,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbParametrosSistema_PadronizarStrings, cmbParametrosSistema_PadronizarStrings_Internalname, StringUtil.BoolToStr( A417ParametrosSistema_PadronizarStrings), 1, cmbParametrosSistema_PadronizarStrings_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "boolean", "", 1, cmbParametrosSistema_PadronizarStrings.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,28);\"", "", true, "HLP_ParametrosSistema.htm");
            cmbParametrosSistema_PadronizarStrings.CurrentValue = StringUtil.BoolToStr( A417ParametrosSistema_PadronizarStrings);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbParametrosSistema_PadronizarStrings_Internalname, "Values", (String)(cmbParametrosSistema_PadronizarStrings.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockparametrossistema_fatorajuste_Internalname, "Fator de Ajuste padr�o", "", "", lblTextblockparametrossistema_fatorajuste_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ParametrosSistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 32,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtParametrosSistema_FatorAjuste_Internalname, StringUtil.LTrim( StringUtil.NToC( A708ParametrosSistema_FatorAjuste, 6, 2, ",", "")), ((edtParametrosSistema_FatorAjuste_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A708ParametrosSistema_FatorAjuste, "ZZ9.99")) : context.localUtil.Format( A708ParametrosSistema_FatorAjuste, "ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(this,32);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtParametrosSistema_FatorAjuste_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtParametrosSistema_FatorAjuste_Enabled, 0, "text", "", 50, "px", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Percentual", "right", false, "HLP_ParametrosSistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockparametrossistema_pathcrtf_Internalname, "Caminho certifica��o digital", "", "", lblTextblockparametrossistema_pathcrtf_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ParametrosSistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table7_37_1J56( true) ;
         }
         return  ;
      }

      protected void wb_table7_37_1J56e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockparametrossistema_flsevd_Internalname, "Caminho das Evid�ncias", "", "", lblTextblockparametrossistema_flsevd_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ParametrosSistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtParametrosSistema_FlsEvd_Internalname, A1499ParametrosSistema_FlsEvd, StringUtil.RTrim( context.localUtil.Format( A1499ParametrosSistema_FlsEvd, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,46);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtParametrosSistema_FlsEvd_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtParametrosSistema_FlsEvd_Enabled, 0, "text", "", 60, "chr", 1, "row", 60, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ParametrosSistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"6\" >") ;
            /* Control Group */
            GxWebStd.gx_group_start( context, grpUnnamedgroup3_Internalname, "Servidor de E-mail:", 1, 0, "px", 0, "px", "Group", "", "HLP_ParametrosSistema.htm");
            wb_table8_52_1J56( true) ;
         }
         return  ;
      }

      protected void wb_table8_52_1J56e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</fieldset>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"6\" >") ;
            /* Control Group */
            GxWebStd.gx_group_start( context, grpUnnamedgroup4_Internalname, "Sistema", 1, 0, "px", 0, "px", "Group", "", "HLP_ParametrosSistema.htm");
            wb_table9_86_1J56( true) ;
         }
         return  ;
      }

      protected void wb_table9_86_1J56e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</fieldset>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"6\" >") ;
            /* Control Group */
            GxWebStd.gx_group_start( context, grpUnnamedgroup5_Internalname, "Login", 1, 0, "px", 0, "px", "Group", "", "HLP_ParametrosSistema.htm");
            wb_table10_110_1J56( true) ;
         }
         return  ;
      }

      protected void wb_table10_110_1J56e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</fieldset>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_19_1J56e( true) ;
         }
         else
         {
            wb_table5_19_1J56e( false) ;
         }
      }

      protected void wb_table10_110_1J56( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " height: " + StringUtil.LTrim( StringUtil.Str( (decimal)(300), 10, 0)) + "px" + ";";
            GxWebStd.gx_table_start( context, tblGrouplogin_Internalname, tblGrouplogin_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 113,'',false,'',0)\"";
            ClassString = "btn btn-success";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnbntimagemlogin_Internalname, "", "Imagem Padr�o", bttBtnbntimagemlogin_Jsonclick, 5, "Imagem Padr�o", "", StyleString, ClassString, bttBtnbntimagemlogin_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'DOBNTIMAGEMLOGIN\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_ParametrosSistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockparametrossistema_imagem_Internalname, "Upload da Imagem", "", "", lblTextblockparametrossistema_imagem_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ParametrosSistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Static Bitmap Variable */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 117,'',false,'',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            A2117ParametrosSistema_Imagem_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( A2117ParametrosSistema_Imagem))&&String.IsNullOrEmpty(StringUtil.RTrim( A40000ParametrosSistema_Imagem_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( A2117ParametrosSistema_Imagem)));
            GxWebStd.gx_bitmap( context, imgParametrosSistema_Imagem_Internalname, (String.IsNullOrEmpty(StringUtil.RTrim( A2117ParametrosSistema_Imagem)) ? A40000ParametrosSistema_Imagem_GXI : context.PathToRelativeUrl( A2117ParametrosSistema_Imagem)), "", "", "", context.GetTheme( ), 1, imgParametrosSistema_Imagem_Enabled, "", "", 1, -1, 0, "", 0, "", 0, 0, 0, "", "", StyleString, ClassString, "", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,117);\"", "", "", "", 0, A2117ParametrosSistema_Imagem_IsBlob, true, "HLP_ParametrosSistema.htm");
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgParametrosSistema_Imagem_Internalname, "URL", (String.IsNullOrEmpty(StringUtil.RTrim( A2117ParametrosSistema_Imagem)) ? A40000ParametrosSistema_Imagem_GXI : context.PathToRelativeUrl( A2117ParametrosSistema_Imagem)));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgParametrosSistema_Imagem_Internalname, "IsBlob", StringUtil.BoolToStr( A2117ParametrosSistema_Imagem_IsBlob));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table10_110_1J56e( true) ;
         }
         else
         {
            wb_table10_110_1J56e( false) ;
         }
      }

      protected void wb_table9_86_1J56( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblSistema_Internalname, tblSistema_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockparametrossistema_urlapp_Internalname, "URL do Sistema", "", "", lblTextblockparametrossistema_urlapp_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ParametrosSistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 91,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtParametrosSistema_URLApp_Internalname, A1679ParametrosSistema_URLApp, StringUtil.RTrim( context.localUtil.Format( A1679ParametrosSistema_URLApp, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,91);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtParametrosSistema_URLApp_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtParametrosSistema_URLApp_Enabled, 0, "text", "", 300, "px", 1, "row", 1000, 0, 0, 0, 1, -1, -1, true, "URLString", "left", true, "HLP_ParametrosSistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockparametrossistema_urlotherver_Internalname, "URL da outra vers�o", "", "", lblTextblockparametrossistema_urlotherver_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ParametrosSistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 95,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtParametrosSistema_URLOtherVer_Internalname, A1952ParametrosSistema_URLOtherVer, StringUtil.RTrim( context.localUtil.Format( A1952ParametrosSistema_URLOtherVer, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,95);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtParametrosSistema_URLOtherVer_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtParametrosSistema_URLOtherVer_Enabled, 0, "text", "", 300, "px", 1, "row", 1000, 0, 0, 0, 1, -1, -1, true, "URLString", "left", true, "HLP_ParametrosSistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockparametrossistema_hostmensuracao_Internalname, "URL de Mensura��o", "", "", lblTextblockparametrossistema_hostmensuracao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ParametrosSistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 102,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtParametrosSistema_HostMensuracao_Internalname, A1163ParametrosSistema_HostMensuracao, StringUtil.RTrim( context.localUtil.Format( A1163ParametrosSistema_HostMensuracao, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,102);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtParametrosSistema_HostMensuracao_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtParametrosSistema_HostMensuracao_Enabled, 0, "text", "", 300, "px", 1, "row", 255, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ParametrosSistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_86_1J56e( true) ;
         }
         else
         {
            wb_table9_86_1J56e( false) ;
         }
      }

      protected void wb_table8_52_1J56( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblEmailsaida_Internalname, tblEmailsaida_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockparametrossistema_emailsdahost_Internalname, "Host SMTP", "", "", lblTextblockparametrossistema_emailsdahost_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ParametrosSistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 57,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtParametrosSistema_EmailSdaHost_Internalname, A532ParametrosSistema_EmailSdaHost, StringUtil.RTrim( context.localUtil.Format( A532ParametrosSistema_EmailSdaHost, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,57);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtParametrosSistema_EmailSdaHost_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtParametrosSistema_EmailSdaHost_Enabled, 0, "text", "", 300, "px", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ParametrosSistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockparametrossistema_emailsdauser_Internalname, "Usu�rio", "", "", lblTextblockparametrossistema_emailsdauser_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ParametrosSistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 61,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtParametrosSistema_EmailSdaUser_Internalname, A533ParametrosSistema_EmailSdaUser, StringUtil.RTrim( context.localUtil.Format( A533ParametrosSistema_EmailSdaUser, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,61);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtParametrosSistema_EmailSdaUser_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtParametrosSistema_EmailSdaUser_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, 0, true, "", "left", true, "HLP_ParametrosSistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockparametrossistema_emailsdapass_Internalname, "Senha", "", "", lblTextblockparametrossistema_emailsdapass_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ParametrosSistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\"  class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 65,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtParametrosSistema_EmailSdaPass_Internalname, A534ParametrosSistema_EmailSdaPass, StringUtil.RTrim( context.localUtil.Format( A534ParametrosSistema_EmailSdaPass, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,65);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtParametrosSistema_EmailSdaPass_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtParametrosSistema_EmailSdaPass_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, -1, 0, 0, 1, 0, 0, true, "", "left", true, "HLP_ParametrosSistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockparametrossistema_emailsdaport_Internalname, "Porta", "", "", lblTextblockparametrossistema_emailsdaport_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ParametrosSistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 71,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtParametrosSistema_EmailSdaPort_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A536ParametrosSistema_EmailSdaPort), 4, 0, ",", "")), ((edtParametrosSistema_EmailSdaPort_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A536ParametrosSistema_EmailSdaPort), "ZZZ9")) : context.localUtil.Format( (decimal)(A536ParametrosSistema_EmailSdaPort), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,71);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtParametrosSistema_EmailSdaPort_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtParametrosSistema_EmailSdaPort_Enabled, 0, "text", "", 45, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ParametrosSistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockparametrossistema_emailsdaaut_Internalname, "Autentica��o", "", "", lblTextblockparametrossistema_emailsdaaut_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ParametrosSistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 75,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbParametrosSistema_EmailSdaAut, cmbParametrosSistema_EmailSdaAut_Internalname, StringUtil.BoolToStr( A535ParametrosSistema_EmailSdaAut), 1, cmbParametrosSistema_EmailSdaAut_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "boolean", "", 1, cmbParametrosSistema_EmailSdaAut.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,75);\"", "", true, "HLP_ParametrosSistema.htm");
            cmbParametrosSistema_EmailSdaAut.CurrentValue = StringUtil.BoolToStr( A535ParametrosSistema_EmailSdaAut);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbParametrosSistema_EmailSdaAut_Internalname, "Values", (String)(cmbParametrosSistema_EmailSdaAut.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockparametrossistema_emailsdasec_Internalname, "Seguran�a", "", "", lblTextblockparametrossistema_emailsdasec_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ParametrosSistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 79,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbParametrosSistema_EmailSdaSec, cmbParametrosSistema_EmailSdaSec_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A2032ParametrosSistema_EmailSdaSec), 4, 0)), 1, cmbParametrosSistema_EmailSdaSec_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, cmbParametrosSistema_EmailSdaSec.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,79);\"", "", true, "HLP_ParametrosSistema.htm");
            cmbParametrosSistema_EmailSdaSec.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A2032ParametrosSistema_EmailSdaSec), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbParametrosSistema_EmailSdaSec_Internalname, "Values", (String)(cmbParametrosSistema_EmailSdaSec.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 81,'',false,'',0)\"";
            ClassString = "btn-default";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtntestaremail_Internalname, "", "Testar envio", bttBtntestaremail_Jsonclick, 5, "Envia um email de teste", "", StyleString, ClassString, bttBtntestaremail_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'DOTESTAREMAIL\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_ParametrosSistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_52_1J56e( true) ;
         }
         else
         {
            wb_table8_52_1J56e( false) ;
         }
      }

      protected void wb_table7_37_1J56( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedparametrossistema_pathcrtf_Internalname, tblTablemergedparametrossistema_pathcrtf_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtParametrosSistema_PathCrtf_Internalname, A1021ParametrosSistema_PathCrtf, StringUtil.RTrim( context.localUtil.Format( A1021ParametrosSistema_PathCrtf, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,40);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtParametrosSistema_PathCrtf_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtParametrosSistema_PathCrtf_Enabled, 0, "text", "", 300, "px", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ParametrosSistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 42,'',false,'',0)\"";
            ClassString = "btn-default";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnassinar_Internalname, "", "Testar", bttBtnassinar_Jsonclick, 7, "Testar", "", StyleString, ClassString, bttBtnassinar_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"e111j56_client"+"'", TempTags, "", 2, "HLP_ParametrosSistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_37_1J56e( true) ;
         }
         else
         {
            wb_table7_37_1J56e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E121J2 */
         E121J2 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               A331ParametrosSistema_NomeSistema = StringUtil.Upper( cgiGet( edtParametrosSistema_NomeSistema_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A331ParametrosSistema_NomeSistema", A331ParametrosSistema_NomeSistema);
               cmbParametrosSistema_PadronizarStrings.CurrentValue = cgiGet( cmbParametrosSistema_PadronizarStrings_Internalname);
               A417ParametrosSistema_PadronizarStrings = StringUtil.StrToBool( cgiGet( cmbParametrosSistema_PadronizarStrings_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A417ParametrosSistema_PadronizarStrings", A417ParametrosSistema_PadronizarStrings);
               if ( ( ( context.localUtil.CToN( cgiGet( edtParametrosSistema_FatorAjuste_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtParametrosSistema_FatorAjuste_Internalname), ",", ".") > 999.99m ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "PARAMETROSSISTEMA_FATORAJUSTE");
                  AnyError = 1;
                  GX_FocusControl = edtParametrosSistema_FatorAjuste_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A708ParametrosSistema_FatorAjuste = 0;
                  n708ParametrosSistema_FatorAjuste = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A708ParametrosSistema_FatorAjuste", StringUtil.LTrim( StringUtil.Str( A708ParametrosSistema_FatorAjuste, 6, 2)));
               }
               else
               {
                  A708ParametrosSistema_FatorAjuste = context.localUtil.CToN( cgiGet( edtParametrosSistema_FatorAjuste_Internalname), ",", ".");
                  n708ParametrosSistema_FatorAjuste = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A708ParametrosSistema_FatorAjuste", StringUtil.LTrim( StringUtil.Str( A708ParametrosSistema_FatorAjuste, 6, 2)));
               }
               n708ParametrosSistema_FatorAjuste = ((Convert.ToDecimal(0)==A708ParametrosSistema_FatorAjuste) ? true : false);
               A1021ParametrosSistema_PathCrtf = cgiGet( edtParametrosSistema_PathCrtf_Internalname);
               n1021ParametrosSistema_PathCrtf = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1021ParametrosSistema_PathCrtf", A1021ParametrosSistema_PathCrtf);
               n1021ParametrosSistema_PathCrtf = (String.IsNullOrEmpty(StringUtil.RTrim( A1021ParametrosSistema_PathCrtf)) ? true : false);
               A1499ParametrosSistema_FlsEvd = cgiGet( edtParametrosSistema_FlsEvd_Internalname);
               n1499ParametrosSistema_FlsEvd = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1499ParametrosSistema_FlsEvd", A1499ParametrosSistema_FlsEvd);
               n1499ParametrosSistema_FlsEvd = (String.IsNullOrEmpty(StringUtil.RTrim( A1499ParametrosSistema_FlsEvd)) ? true : false);
               A532ParametrosSistema_EmailSdaHost = cgiGet( edtParametrosSistema_EmailSdaHost_Internalname);
               n532ParametrosSistema_EmailSdaHost = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A532ParametrosSistema_EmailSdaHost", A532ParametrosSistema_EmailSdaHost);
               n532ParametrosSistema_EmailSdaHost = (String.IsNullOrEmpty(StringUtil.RTrim( A532ParametrosSistema_EmailSdaHost)) ? true : false);
               A533ParametrosSistema_EmailSdaUser = cgiGet( edtParametrosSistema_EmailSdaUser_Internalname);
               n533ParametrosSistema_EmailSdaUser = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A533ParametrosSistema_EmailSdaUser", A533ParametrosSistema_EmailSdaUser);
               n533ParametrosSistema_EmailSdaUser = (String.IsNullOrEmpty(StringUtil.RTrim( A533ParametrosSistema_EmailSdaUser)) ? true : false);
               A534ParametrosSistema_EmailSdaPass = cgiGet( edtParametrosSistema_EmailSdaPass_Internalname);
               n534ParametrosSistema_EmailSdaPass = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A534ParametrosSistema_EmailSdaPass", A534ParametrosSistema_EmailSdaPass);
               n534ParametrosSistema_EmailSdaPass = (String.IsNullOrEmpty(StringUtil.RTrim( A534ParametrosSistema_EmailSdaPass)) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtParametrosSistema_EmailSdaPort_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtParametrosSistema_EmailSdaPort_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "PARAMETROSSISTEMA_EMAILSDAPORT");
                  AnyError = 1;
                  GX_FocusControl = edtParametrosSistema_EmailSdaPort_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A536ParametrosSistema_EmailSdaPort = 0;
                  n536ParametrosSistema_EmailSdaPort = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A536ParametrosSistema_EmailSdaPort", StringUtil.LTrim( StringUtil.Str( (decimal)(A536ParametrosSistema_EmailSdaPort), 4, 0)));
               }
               else
               {
                  A536ParametrosSistema_EmailSdaPort = (short)(context.localUtil.CToN( cgiGet( edtParametrosSistema_EmailSdaPort_Internalname), ",", "."));
                  n536ParametrosSistema_EmailSdaPort = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A536ParametrosSistema_EmailSdaPort", StringUtil.LTrim( StringUtil.Str( (decimal)(A536ParametrosSistema_EmailSdaPort), 4, 0)));
               }
               n536ParametrosSistema_EmailSdaPort = ((0==A536ParametrosSistema_EmailSdaPort) ? true : false);
               cmbParametrosSistema_EmailSdaAut.CurrentValue = cgiGet( cmbParametrosSistema_EmailSdaAut_Internalname);
               A535ParametrosSistema_EmailSdaAut = StringUtil.StrToBool( cgiGet( cmbParametrosSistema_EmailSdaAut_Internalname));
               n535ParametrosSistema_EmailSdaAut = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A535ParametrosSistema_EmailSdaAut", A535ParametrosSistema_EmailSdaAut);
               n535ParametrosSistema_EmailSdaAut = ((false==A535ParametrosSistema_EmailSdaAut) ? true : false);
               cmbParametrosSistema_EmailSdaSec.CurrentValue = cgiGet( cmbParametrosSistema_EmailSdaSec_Internalname);
               A2032ParametrosSistema_EmailSdaSec = (short)(NumberUtil.Val( cgiGet( cmbParametrosSistema_EmailSdaSec_Internalname), "."));
               n2032ParametrosSistema_EmailSdaSec = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2032ParametrosSistema_EmailSdaSec", StringUtil.LTrim( StringUtil.Str( (decimal)(A2032ParametrosSistema_EmailSdaSec), 4, 0)));
               n2032ParametrosSistema_EmailSdaSec = ((0==A2032ParametrosSistema_EmailSdaSec) ? true : false);
               A1679ParametrosSistema_URLApp = cgiGet( edtParametrosSistema_URLApp_Internalname);
               n1679ParametrosSistema_URLApp = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1679ParametrosSistema_URLApp", A1679ParametrosSistema_URLApp);
               n1679ParametrosSistema_URLApp = (String.IsNullOrEmpty(StringUtil.RTrim( A1679ParametrosSistema_URLApp)) ? true : false);
               A1952ParametrosSistema_URLOtherVer = cgiGet( edtParametrosSistema_URLOtherVer_Internalname);
               n1952ParametrosSistema_URLOtherVer = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1952ParametrosSistema_URLOtherVer", A1952ParametrosSistema_URLOtherVer);
               n1952ParametrosSistema_URLOtherVer = (String.IsNullOrEmpty(StringUtil.RTrim( A1952ParametrosSistema_URLOtherVer)) ? true : false);
               A1163ParametrosSistema_HostMensuracao = cgiGet( edtParametrosSistema_HostMensuracao_Internalname);
               n1163ParametrosSistema_HostMensuracao = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1163ParametrosSistema_HostMensuracao", A1163ParametrosSistema_HostMensuracao);
               n1163ParametrosSistema_HostMensuracao = (String.IsNullOrEmpty(StringUtil.RTrim( A1163ParametrosSistema_HostMensuracao)) ? true : false);
               A2117ParametrosSistema_Imagem = cgiGet( imgParametrosSistema_Imagem_Internalname);
               n2117ParametrosSistema_Imagem = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2117ParametrosSistema_Imagem", A2117ParametrosSistema_Imagem);
               n2117ParametrosSistema_Imagem = (String.IsNullOrEmpty(StringUtil.RTrim( A2117ParametrosSistema_Imagem)) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtParametrosSistema_Codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtParametrosSistema_Codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "PARAMETROSSISTEMA_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtParametrosSistema_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A330ParametrosSistema_Codigo = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A330ParametrosSistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A330ParametrosSistema_Codigo), 6, 0)));
               }
               else
               {
                  A330ParametrosSistema_Codigo = (int)(context.localUtil.CToN( cgiGet( edtParametrosSistema_Codigo_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A330ParametrosSistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A330ParametrosSistema_Codigo), 6, 0)));
               }
               A334ParametrosSistema_AppID = (long)(context.localUtil.CToN( cgiGet( edtParametrosSistema_AppID_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A334ParametrosSistema_AppID", StringUtil.LTrim( StringUtil.Str( (decimal)(A334ParametrosSistema_AppID), 12, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_PARAMETROSSISTEMA_APPID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A334ParametrosSistema_AppID), "ZZZZZZZZZZZ9")));
               /* Read saved values. */
               Z330ParametrosSistema_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z330ParametrosSistema_Codigo"), ",", "."));
               Z1021ParametrosSistema_PathCrtf = cgiGet( "Z1021ParametrosSistema_PathCrtf");
               n1021ParametrosSistema_PathCrtf = (String.IsNullOrEmpty(StringUtil.RTrim( A1021ParametrosSistema_PathCrtf)) ? true : false);
               Z1499ParametrosSistema_FlsEvd = cgiGet( "Z1499ParametrosSistema_FlsEvd");
               n1499ParametrosSistema_FlsEvd = (String.IsNullOrEmpty(StringUtil.RTrim( A1499ParametrosSistema_FlsEvd)) ? true : false);
               Z331ParametrosSistema_NomeSistema = cgiGet( "Z331ParametrosSistema_NomeSistema");
               Z334ParametrosSistema_AppID = (long)(context.localUtil.CToN( cgiGet( "Z334ParametrosSistema_AppID"), ",", "."));
               Z399ParametrosSistema_LicensiadoCadastrado = StringUtil.StrToBool( cgiGet( "Z399ParametrosSistema_LicensiadoCadastrado"));
               n399ParametrosSistema_LicensiadoCadastrado = ((false==A399ParametrosSistema_LicensiadoCadastrado) ? true : false);
               Z417ParametrosSistema_PadronizarStrings = StringUtil.StrToBool( cgiGet( "Z417ParametrosSistema_PadronizarStrings"));
               Z532ParametrosSistema_EmailSdaHost = cgiGet( "Z532ParametrosSistema_EmailSdaHost");
               n532ParametrosSistema_EmailSdaHost = (String.IsNullOrEmpty(StringUtil.RTrim( A532ParametrosSistema_EmailSdaHost)) ? true : false);
               Z533ParametrosSistema_EmailSdaUser = cgiGet( "Z533ParametrosSistema_EmailSdaUser");
               n533ParametrosSistema_EmailSdaUser = (String.IsNullOrEmpty(StringUtil.RTrim( A533ParametrosSistema_EmailSdaUser)) ? true : false);
               Z534ParametrosSistema_EmailSdaPass = cgiGet( "Z534ParametrosSistema_EmailSdaPass");
               n534ParametrosSistema_EmailSdaPass = (String.IsNullOrEmpty(StringUtil.RTrim( A534ParametrosSistema_EmailSdaPass)) ? true : false);
               Z537ParametrosSistema_EmailSdaKey = cgiGet( "Z537ParametrosSistema_EmailSdaKey");
               n537ParametrosSistema_EmailSdaKey = (String.IsNullOrEmpty(StringUtil.RTrim( A537ParametrosSistema_EmailSdaKey)) ? true : false);
               Z535ParametrosSistema_EmailSdaAut = StringUtil.StrToBool( cgiGet( "Z535ParametrosSistema_EmailSdaAut"));
               n535ParametrosSistema_EmailSdaAut = ((false==A535ParametrosSistema_EmailSdaAut) ? true : false);
               Z536ParametrosSistema_EmailSdaPort = (short)(context.localUtil.CToN( cgiGet( "Z536ParametrosSistema_EmailSdaPort"), ",", "."));
               n536ParametrosSistema_EmailSdaPort = ((0==A536ParametrosSistema_EmailSdaPort) ? true : false);
               Z2032ParametrosSistema_EmailSdaSec = (short)(context.localUtil.CToN( cgiGet( "Z2032ParametrosSistema_EmailSdaSec"), ",", "."));
               n2032ParametrosSistema_EmailSdaSec = ((0==A2032ParametrosSistema_EmailSdaSec) ? true : false);
               Z708ParametrosSistema_FatorAjuste = context.localUtil.CToN( cgiGet( "Z708ParametrosSistema_FatorAjuste"), ",", ".");
               n708ParametrosSistema_FatorAjuste = ((Convert.ToDecimal(0)==A708ParametrosSistema_FatorAjuste) ? true : false);
               Z1163ParametrosSistema_HostMensuracao = cgiGet( "Z1163ParametrosSistema_HostMensuracao");
               n1163ParametrosSistema_HostMensuracao = (String.IsNullOrEmpty(StringUtil.RTrim( A1163ParametrosSistema_HostMensuracao)) ? true : false);
               Z1171ParametrosSistema_SQLComputerName = cgiGet( "Z1171ParametrosSistema_SQLComputerName");
               n1171ParametrosSistema_SQLComputerName = (String.IsNullOrEmpty(StringUtil.RTrim( A1171ParametrosSistema_SQLComputerName)) ? true : false);
               Z1172ParametrosSistema_SQLServerName = cgiGet( "Z1172ParametrosSistema_SQLServerName");
               n1172ParametrosSistema_SQLServerName = (String.IsNullOrEmpty(StringUtil.RTrim( A1172ParametrosSistema_SQLServerName)) ? true : false);
               Z1168ParametrosSistema_SQLEdition = cgiGet( "Z1168ParametrosSistema_SQLEdition");
               n1168ParametrosSistema_SQLEdition = (String.IsNullOrEmpty(StringUtil.RTrim( A1168ParametrosSistema_SQLEdition)) ? true : false);
               Z1169ParametrosSistema_SQLInstance = cgiGet( "Z1169ParametrosSistema_SQLInstance");
               n1169ParametrosSistema_SQLInstance = (String.IsNullOrEmpty(StringUtil.RTrim( A1169ParametrosSistema_SQLInstance)) ? true : false);
               Z1170ParametrosSistema_DBName = cgiGet( "Z1170ParametrosSistema_DBName");
               n1170ParametrosSistema_DBName = (String.IsNullOrEmpty(StringUtil.RTrim( A1170ParametrosSistema_DBName)) ? true : false);
               Z1679ParametrosSistema_URLApp = cgiGet( "Z1679ParametrosSistema_URLApp");
               n1679ParametrosSistema_URLApp = (String.IsNullOrEmpty(StringUtil.RTrim( A1679ParametrosSistema_URLApp)) ? true : false);
               Z1952ParametrosSistema_URLOtherVer = cgiGet( "Z1952ParametrosSistema_URLOtherVer");
               n1952ParametrosSistema_URLOtherVer = (String.IsNullOrEmpty(StringUtil.RTrim( A1952ParametrosSistema_URLOtherVer)) ? true : false);
               Z1954ParametrosSistema_Validacao = cgiGet( "Z1954ParametrosSistema_Validacao");
               n1954ParametrosSistema_Validacao = (String.IsNullOrEmpty(StringUtil.RTrim( A1954ParametrosSistema_Validacao)) ? true : false);
               A399ParametrosSistema_LicensiadoCadastrado = StringUtil.StrToBool( cgiGet( "Z399ParametrosSistema_LicensiadoCadastrado"));
               n399ParametrosSistema_LicensiadoCadastrado = false;
               n399ParametrosSistema_LicensiadoCadastrado = ((false==A399ParametrosSistema_LicensiadoCadastrado) ? true : false);
               A537ParametrosSistema_EmailSdaKey = cgiGet( "Z537ParametrosSistema_EmailSdaKey");
               n537ParametrosSistema_EmailSdaKey = false;
               n537ParametrosSistema_EmailSdaKey = (String.IsNullOrEmpty(StringUtil.RTrim( A537ParametrosSistema_EmailSdaKey)) ? true : false);
               A1171ParametrosSistema_SQLComputerName = cgiGet( "Z1171ParametrosSistema_SQLComputerName");
               n1171ParametrosSistema_SQLComputerName = false;
               n1171ParametrosSistema_SQLComputerName = (String.IsNullOrEmpty(StringUtil.RTrim( A1171ParametrosSistema_SQLComputerName)) ? true : false);
               A1172ParametrosSistema_SQLServerName = cgiGet( "Z1172ParametrosSistema_SQLServerName");
               n1172ParametrosSistema_SQLServerName = false;
               n1172ParametrosSistema_SQLServerName = (String.IsNullOrEmpty(StringUtil.RTrim( A1172ParametrosSistema_SQLServerName)) ? true : false);
               A1168ParametrosSistema_SQLEdition = cgiGet( "Z1168ParametrosSistema_SQLEdition");
               n1168ParametrosSistema_SQLEdition = false;
               n1168ParametrosSistema_SQLEdition = (String.IsNullOrEmpty(StringUtil.RTrim( A1168ParametrosSistema_SQLEdition)) ? true : false);
               A1169ParametrosSistema_SQLInstance = cgiGet( "Z1169ParametrosSistema_SQLInstance");
               n1169ParametrosSistema_SQLInstance = false;
               n1169ParametrosSistema_SQLInstance = (String.IsNullOrEmpty(StringUtil.RTrim( A1169ParametrosSistema_SQLInstance)) ? true : false);
               A1170ParametrosSistema_DBName = cgiGet( "Z1170ParametrosSistema_DBName");
               n1170ParametrosSistema_DBName = false;
               n1170ParametrosSistema_DBName = (String.IsNullOrEmpty(StringUtil.RTrim( A1170ParametrosSistema_DBName)) ? true : false);
               A1954ParametrosSistema_Validacao = cgiGet( "Z1954ParametrosSistema_Validacao");
               n1954ParametrosSistema_Validacao = false;
               n1954ParametrosSistema_Validacao = (String.IsNullOrEmpty(StringUtil.RTrim( A1954ParametrosSistema_Validacao)) ? true : false);
               O534ParametrosSistema_EmailSdaPass = cgiGet( "O534ParametrosSistema_EmailSdaPass");
               n534ParametrosSistema_EmailSdaPass = (String.IsNullOrEmpty(StringUtil.RTrim( A534ParametrosSistema_EmailSdaPass)) ? true : false);
               O1499ParametrosSistema_FlsEvd = cgiGet( "O1499ParametrosSistema_FlsEvd");
               n1499ParametrosSistema_FlsEvd = (String.IsNullOrEmpty(StringUtil.RTrim( A1499ParametrosSistema_FlsEvd)) ? true : false);
               O1021ParametrosSistema_PathCrtf = cgiGet( "O1021ParametrosSistema_PathCrtf");
               n1021ParametrosSistema_PathCrtf = (String.IsNullOrEmpty(StringUtil.RTrim( A1021ParametrosSistema_PathCrtf)) ? true : false);
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               AV7ParametrosSistema_Codigo = (int)(context.localUtil.CToN( cgiGet( "vPARAMETROSSISTEMA_CODIGO"), ",", "."));
               AV14KeyEnc = cgiGet( "vKEYENC");
               Gx_BScreen = (short)(context.localUtil.CToN( cgiGet( "vGXBSCREEN"), ",", "."));
               A399ParametrosSistema_LicensiadoCadastrado = StringUtil.StrToBool( cgiGet( "PARAMETROSSISTEMA_LICENSIADOCADASTRADO"));
               n399ParametrosSistema_LicensiadoCadastrado = ((false==A399ParametrosSistema_LicensiadoCadastrado) ? true : false);
               A537ParametrosSistema_EmailSdaKey = cgiGet( "PARAMETROSSISTEMA_EMAILSDAKEY");
               n537ParametrosSistema_EmailSdaKey = (String.IsNullOrEmpty(StringUtil.RTrim( A537ParametrosSistema_EmailSdaKey)) ? true : false);
               A332ParametrosSistema_TextoHome = cgiGet( "PARAMETROSSISTEMA_TEXTOHOME");
               A1171ParametrosSistema_SQLComputerName = cgiGet( "PARAMETROSSISTEMA_SQLCOMPUTERNAME");
               n1171ParametrosSistema_SQLComputerName = (String.IsNullOrEmpty(StringUtil.RTrim( A1171ParametrosSistema_SQLComputerName)) ? true : false);
               A1172ParametrosSistema_SQLServerName = cgiGet( "PARAMETROSSISTEMA_SQLSERVERNAME");
               n1172ParametrosSistema_SQLServerName = (String.IsNullOrEmpty(StringUtil.RTrim( A1172ParametrosSistema_SQLServerName)) ? true : false);
               A1168ParametrosSistema_SQLEdition = cgiGet( "PARAMETROSSISTEMA_SQLEDITION");
               n1168ParametrosSistema_SQLEdition = (String.IsNullOrEmpty(StringUtil.RTrim( A1168ParametrosSistema_SQLEdition)) ? true : false);
               A1169ParametrosSistema_SQLInstance = cgiGet( "PARAMETROSSISTEMA_SQLINSTANCE");
               n1169ParametrosSistema_SQLInstance = (String.IsNullOrEmpty(StringUtil.RTrim( A1169ParametrosSistema_SQLInstance)) ? true : false);
               A1170ParametrosSistema_DBName = cgiGet( "PARAMETROSSISTEMA_DBNAME");
               n1170ParametrosSistema_DBName = (String.IsNullOrEmpty(StringUtil.RTrim( A1170ParametrosSistema_DBName)) ? true : false);
               A1954ParametrosSistema_Validacao = cgiGet( "PARAMETROSSISTEMA_VALIDACAO");
               n1954ParametrosSistema_Validacao = (String.IsNullOrEmpty(StringUtil.RTrim( A1954ParametrosSistema_Validacao)) ? true : false);
               A40000ParametrosSistema_Imagem_GXI = cgiGet( "PARAMETROSSISTEMA_IMAGEM_GXI");
               n40000ParametrosSistema_Imagem_GXI = (String.IsNullOrEmpty(StringUtil.RTrim( A40000ParametrosSistema_Imagem_GXI))&&String.IsNullOrEmpty(StringUtil.RTrim( A2117ParametrosSistema_Imagem)) ? true : false);
               Gx_mode = cgiGet( "vMODE");
               Parametrossistema_textohome_Width = cgiGet( "PARAMETROSSISTEMA_TEXTOHOME_Width");
               Parametrossistema_textohome_Height = cgiGet( "PARAMETROSSISTEMA_TEXTOHOME_Height");
               Parametrossistema_textohome_Skin = cgiGet( "PARAMETROSSISTEMA_TEXTOHOME_Skin");
               Parametrossistema_textohome_Toolbar = cgiGet( "PARAMETROSSISTEMA_TEXTOHOME_Toolbar");
               Parametrossistema_textohome_Color = (int)(context.localUtil.CToN( cgiGet( "PARAMETROSSISTEMA_TEXTOHOME_Color"), ",", "."));
               Parametrossistema_textohome_Enabled = StringUtil.StrToBool( cgiGet( "PARAMETROSSISTEMA_TEXTOHOME_Enabled"));
               Parametrossistema_textohome_Class = cgiGet( "PARAMETROSSISTEMA_TEXTOHOME_Class");
               Parametrossistema_textohome_Customtoolbar = cgiGet( "PARAMETROSSISTEMA_TEXTOHOME_Customtoolbar");
               Parametrossistema_textohome_Customconfiguration = cgiGet( "PARAMETROSSISTEMA_TEXTOHOME_Customconfiguration");
               Parametrossistema_textohome_Toolbarcancollapse = StringUtil.StrToBool( cgiGet( "PARAMETROSSISTEMA_TEXTOHOME_Toolbarcancollapse"));
               Parametrossistema_textohome_Toolbarexpanded = StringUtil.StrToBool( cgiGet( "PARAMETROSSISTEMA_TEXTOHOME_Toolbarexpanded"));
               Parametrossistema_textohome_Buttonpressedid = cgiGet( "PARAMETROSSISTEMA_TEXTOHOME_Buttonpressedid");
               Parametrossistema_textohome_Captionvalue = cgiGet( "PARAMETROSSISTEMA_TEXTOHOME_Captionvalue");
               Parametrossistema_textohome_Captionclass = cgiGet( "PARAMETROSSISTEMA_TEXTOHOME_Captionclass");
               Parametrossistema_textohome_Captionposition = cgiGet( "PARAMETROSSISTEMA_TEXTOHOME_Captionposition");
               Parametrossistema_textohome_Coltitle = cgiGet( "PARAMETROSSISTEMA_TEXTOHOME_Coltitle");
               Parametrossistema_textohome_Coltitlefont = cgiGet( "PARAMETROSSISTEMA_TEXTOHOME_Coltitlefont");
               Parametrossistema_textohome_Coltitlecolor = (int)(context.localUtil.CToN( cgiGet( "PARAMETROSSISTEMA_TEXTOHOME_Coltitlecolor"), ",", "."));
               Parametrossistema_textohome_Usercontroliscolumn = StringUtil.StrToBool( cgiGet( "PARAMETROSSISTEMA_TEXTOHOME_Usercontroliscolumn"));
               Parametrossistema_textohome_Visible = StringUtil.StrToBool( cgiGet( "PARAMETROSSISTEMA_TEXTOHOME_Visible"));
               Gxuitabspanel_tabsparametros_Width = cgiGet( "GXUITABSPANEL_TABSPARAMETROS_Width");
               Gxuitabspanel_tabsparametros_Height = cgiGet( "GXUITABSPANEL_TABSPARAMETROS_Height");
               Gxuitabspanel_tabsparametros_Cls = cgiGet( "GXUITABSPANEL_TABSPARAMETROS_Cls");
               Gxuitabspanel_tabsparametros_Enabled = StringUtil.StrToBool( cgiGet( "GXUITABSPANEL_TABSPARAMETROS_Enabled"));
               Gxuitabspanel_tabsparametros_Class = cgiGet( "GXUITABSPANEL_TABSPARAMETROS_Class");
               Gxuitabspanel_tabsparametros_Autowidth = StringUtil.StrToBool( cgiGet( "GXUITABSPANEL_TABSPARAMETROS_Autowidth"));
               Gxuitabspanel_tabsparametros_Autoheight = StringUtil.StrToBool( cgiGet( "GXUITABSPANEL_TABSPARAMETROS_Autoheight"));
               Gxuitabspanel_tabsparametros_Autoscroll = StringUtil.StrToBool( cgiGet( "GXUITABSPANEL_TABSPARAMETROS_Autoscroll"));
               Gxuitabspanel_tabsparametros_Activetabid = cgiGet( "GXUITABSPANEL_TABSPARAMETROS_Activetabid");
               Gxuitabspanel_tabsparametros_Designtimetabs = cgiGet( "GXUITABSPANEL_TABSPARAMETROS_Designtimetabs");
               Gxuitabspanel_tabsparametros_Selectedtabindex = (int)(context.localUtil.CToN( cgiGet( "GXUITABSPANEL_TABSPARAMETROS_Selectedtabindex"), ",", "."));
               Gxuitabspanel_tabsparametros_Visible = StringUtil.StrToBool( cgiGet( "GXUITABSPANEL_TABSPARAMETROS_Visible"));
               Dvpanel_tableattributes_Width = cgiGet( "DVPANEL_TABLEATTRIBUTES_Width");
               Dvpanel_tableattributes_Height = cgiGet( "DVPANEL_TABLEATTRIBUTES_Height");
               Dvpanel_tableattributes_Cls = cgiGet( "DVPANEL_TABLEATTRIBUTES_Cls");
               Dvpanel_tableattributes_Title = cgiGet( "DVPANEL_TABLEATTRIBUTES_Title");
               Dvpanel_tableattributes_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsible"));
               Dvpanel_tableattributes_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsed"));
               Dvpanel_tableattributes_Enabled = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Enabled"));
               Dvpanel_tableattributes_Class = cgiGet( "DVPANEL_TABLEATTRIBUTES_Class");
               Dvpanel_tableattributes_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autowidth"));
               Dvpanel_tableattributes_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoheight"));
               Dvpanel_tableattributes_Showheader = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showheader"));
               Dvpanel_tableattributes_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showcollapseicon"));
               Dvpanel_tableattributes_Iconposition = cgiGet( "DVPANEL_TABLEATTRIBUTES_Iconposition");
               Dvpanel_tableattributes_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoscroll"));
               Dvpanel_tableattributes_Visible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Visible"));
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               getMultimediaValue(imgParametrosSistema_Imagem_Internalname, ref  A2117ParametrosSistema_Imagem, ref  A40000ParametrosSistema_Imagem_GXI);
               n40000ParametrosSistema_Imagem_GXI = (String.IsNullOrEmpty(StringUtil.RTrim( A40000ParametrosSistema_Imagem_GXI))&&String.IsNullOrEmpty(StringUtil.RTrim( A2117ParametrosSistema_Imagem)) ? true : false);
               n2117ParametrosSistema_Imagem = (String.IsNullOrEmpty(StringUtil.RTrim( A2117ParametrosSistema_Imagem)) ? true : false);
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "ParametrosSistema";
               A334ParametrosSistema_AppID = (long)(context.localUtil.CToN( cgiGet( edtParametrosSistema_AppID_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A334ParametrosSistema_AppID", StringUtil.LTrim( StringUtil.Str( (decimal)(A334ParametrosSistema_AppID), 12, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_PARAMETROSSISTEMA_APPID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A334ParametrosSistema_AppID), "ZZZZZZZZZZZ9")));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A334ParametrosSistema_AppID), "ZZZZZZZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               forbiddenHiddens = forbiddenHiddens + StringUtil.BoolToStr( A399ParametrosSistema_LicensiadoCadastrado);
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( A1171ParametrosSistema_SQLComputerName, ""));
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( A1172ParametrosSistema_SQLServerName, ""));
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( A1168ParametrosSistema_SQLEdition, ""));
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( A1169ParametrosSistema_SQLInstance, ""));
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( A1170ParametrosSistema_DBName, ""));
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( A1954ParametrosSistema_Validacao, ""));
               hsh = cgiGet( "hsh");
               if ( ( ! ( ( A330ParametrosSistema_Codigo != Z330ParametrosSistema_Codigo ) ) || ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ) && ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("parametrossistema:[SecurityCheckFailed value for]"+"ParametrosSistema_AppID:"+context.localUtil.Format( (decimal)(A334ParametrosSistema_AppID), "ZZZZZZZZZZZ9"));
                  GXUtil.WriteLog("parametrossistema:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GXUtil.WriteLog("parametrossistema:[SecurityCheckFailed value for]"+"ParametrosSistema_LicensiadoCadastrado:"+StringUtil.BoolToStr( A399ParametrosSistema_LicensiadoCadastrado));
                  GXUtil.WriteLog("parametrossistema:[SecurityCheckFailed value for]"+"ParametrosSistema_SQLComputerName:"+StringUtil.RTrim( context.localUtil.Format( A1171ParametrosSistema_SQLComputerName, "")));
                  GXUtil.WriteLog("parametrossistema:[SecurityCheckFailed value for]"+"ParametrosSistema_SQLServerName:"+StringUtil.RTrim( context.localUtil.Format( A1172ParametrosSistema_SQLServerName, "")));
                  GXUtil.WriteLog("parametrossistema:[SecurityCheckFailed value for]"+"ParametrosSistema_SQLEdition:"+StringUtil.RTrim( context.localUtil.Format( A1168ParametrosSistema_SQLEdition, "")));
                  GXUtil.WriteLog("parametrossistema:[SecurityCheckFailed value for]"+"ParametrosSistema_SQLInstance:"+StringUtil.RTrim( context.localUtil.Format( A1169ParametrosSistema_SQLInstance, "")));
                  GXUtil.WriteLog("parametrossistema:[SecurityCheckFailed value for]"+"ParametrosSistema_DBName:"+StringUtil.RTrim( context.localUtil.Format( A1170ParametrosSistema_DBName, "")));
                  GXUtil.WriteLog("parametrossistema:[SecurityCheckFailed value for]"+"ParametrosSistema_Validacao:"+StringUtil.RTrim( context.localUtil.Format( A1954ParametrosSistema_Validacao, "")));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  A330ParametrosSistema_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A330ParametrosSistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A330ParametrosSistema_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode56 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     Gx_mode = sMode56;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound56 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_1J0( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_trn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "PARAMETROSSISTEMA_CODIGO");
                        AnyError = 1;
                        GX_FocusControl = edtParametrosSistema_Codigo_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E121J2 */
                           E121J2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E131J2 */
                           E131J2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DOBNTIMAGEMLOGIN'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E141J2 */
                           E141J2 ();
                           nKeyPressed = 3;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DOTESTAREMAIL'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E151J2 */
                           E151J2 ();
                           nKeyPressed = 3;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E131J2 */
            E131J2 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll1J56( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         bttBtn_trn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtn_trn_delete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_trn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
            }
            DisableAttributes1J56( ) ;
         }
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_1J0( )
      {
         BeforeValidate1J56( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls1J56( ) ;
            }
            else
            {
               CheckExtendedTable1J56( ) ;
               CloseExtendedTableCursors1J56( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         }
      }

      protected void ResetCaption1J0( )
      {
      }

      protected void E121J2( )
      {
         /* Start Routine */
         Form.Meta.addItem("Versao", "1.0 - Data: 14/04/2020 14:02", 0) ;
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
         edtParametrosSistema_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtParametrosSistema_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtParametrosSistema_Codigo_Visible), 5, 0)));
         edtParametrosSistema_AppID_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtParametrosSistema_AppID_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtParametrosSistema_AppID_Visible), 5, 0)));
         /* User SQL Command. */
         cmdBuffer="  Update ParametrosSistema set ParametrosSistema_SQLComputerName = convert(sysname,serverproperty('ComputerNamePhysicalNetBIOS')), ParametrosSistema_SQLServerName = convert(sysname,serverproperty('ServerName')), ParametrosSistema_SQLEdition = convert(sysname,serverproperty('Edition')), ParametrosSistema_SQLInstance = convert(sysname,serverproperty('InstanceName')), ParametrosSistema_DBName = convert(sysname,db_name()) where ParametrosSistema_Codigo = 1 "
         ;
         RGZ = new GxCommand(dsDefault.Db, cmdBuffer, dsDefault,0,true,false,null);
         RGZ.ErrorMask = GxErrorMask.GX_NOMASK;
         RGZ.ExecuteStmt() ;
         RGZ.Drop();
      }

      protected void E131J2( )
      {
         /* After Trn Routine */
         if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV9TrnContext.gxTpr_Callerondelete )
         {
            context.wjLoc = formatLink("wwparametrossistema.aspx") ;
            context.wjLocDisableFrm = 1;
         }
         context.setWebReturnParms(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void E141J2( )
      {
         /* 'DoBntImagemLogin' Routine */
         new prc_parametrossistemaimagem(context ).execute(  1, out  AV19ParametrosSistema_Imagem) ;
      }

      protected void E151J2( )
      {
         /* 'DoTestarEmail' Routine */
         AV12MailRecipient.Name = "Se mesmo";
         AV12MailRecipient.Address = StringUtil.Trim( A533ParametrosSistema_EmailSdaUser);
         AV11Email.Clear();
         AV11Email.To.Add(AV12MailRecipient) ;
         AV11Email.Subject = "Teste de envio de email";
         AV11Email.Text = "Teste de envio de email";
         AV13SMTPSession.Sender.Name = StringUtil.Trim( A533ParametrosSistema_EmailSdaUser);
         AV13SMTPSession.Sender.Address = StringUtil.Trim( A533ParametrosSistema_EmailSdaUser);
         AV13SMTPSession.Host = StringUtil.Trim( A532ParametrosSistema_EmailSdaHost);
         AV13SMTPSession.UserName = StringUtil.Trim( A533ParametrosSistema_EmailSdaUser);
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) || ( StringUtil.StrCmp(A534ParametrosSistema_EmailSdaPass, O534ParametrosSistema_EmailSdaPass) != 0 ) )
         {
            AV13SMTPSession.Password = StringUtil.Trim( A534ParametrosSistema_EmailSdaPass);
         }
         else
         {
            AV13SMTPSession.Password = Crypto.Decrypt64( A534ParametrosSistema_EmailSdaPass, A537ParametrosSistema_EmailSdaKey);
         }
         AV13SMTPSession.Port = A536ParametrosSistema_EmailSdaPort;
         AV13SMTPSession.Authentication = (short)((A535ParametrosSistema_EmailSdaAut ? 1 : 0));
         AV13SMTPSession.Secure = A2032ParametrosSistema_EmailSdaSec;
         AV13SMTPSession.Login();
         if ( AV13SMTPSession.ErrCode != 0 )
         {
            GX_msglist.addItem(AV13SMTPSession.ErrDescription);
         }
         else
         {
            GX_msglist.addItem("Login bem sucedido!");
         }
         AV13SMTPSession.Send(AV11Email);
         if ( AV13SMTPSession.ErrCode != 0 )
         {
            GX_msglist.addItem(AV13SMTPSession.ErrDescription+": "+A533ParametrosSistema_EmailSdaUser);
         }
         else
         {
            GX_msglist.addItem("Mensagem enviada com sucesso!");
         }
         AV13SMTPSession.Logout();
      }

      protected void ZM1J56( short GX_JID )
      {
         if ( ( GX_JID == 14 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z1021ParametrosSistema_PathCrtf = T001J3_A1021ParametrosSistema_PathCrtf[0];
               Z1499ParametrosSistema_FlsEvd = T001J3_A1499ParametrosSistema_FlsEvd[0];
               Z331ParametrosSistema_NomeSistema = T001J3_A331ParametrosSistema_NomeSistema[0];
               Z334ParametrosSistema_AppID = T001J3_A334ParametrosSistema_AppID[0];
               Z399ParametrosSistema_LicensiadoCadastrado = T001J3_A399ParametrosSistema_LicensiadoCadastrado[0];
               Z417ParametrosSistema_PadronizarStrings = T001J3_A417ParametrosSistema_PadronizarStrings[0];
               Z532ParametrosSistema_EmailSdaHost = T001J3_A532ParametrosSistema_EmailSdaHost[0];
               Z533ParametrosSistema_EmailSdaUser = T001J3_A533ParametrosSistema_EmailSdaUser[0];
               Z534ParametrosSistema_EmailSdaPass = T001J3_A534ParametrosSistema_EmailSdaPass[0];
               Z537ParametrosSistema_EmailSdaKey = T001J3_A537ParametrosSistema_EmailSdaKey[0];
               Z535ParametrosSistema_EmailSdaAut = T001J3_A535ParametrosSistema_EmailSdaAut[0];
               Z536ParametrosSistema_EmailSdaPort = T001J3_A536ParametrosSistema_EmailSdaPort[0];
               Z2032ParametrosSistema_EmailSdaSec = T001J3_A2032ParametrosSistema_EmailSdaSec[0];
               Z708ParametrosSistema_FatorAjuste = T001J3_A708ParametrosSistema_FatorAjuste[0];
               Z1163ParametrosSistema_HostMensuracao = T001J3_A1163ParametrosSistema_HostMensuracao[0];
               Z1171ParametrosSistema_SQLComputerName = T001J3_A1171ParametrosSistema_SQLComputerName[0];
               Z1172ParametrosSistema_SQLServerName = T001J3_A1172ParametrosSistema_SQLServerName[0];
               Z1168ParametrosSistema_SQLEdition = T001J3_A1168ParametrosSistema_SQLEdition[0];
               Z1169ParametrosSistema_SQLInstance = T001J3_A1169ParametrosSistema_SQLInstance[0];
               Z1170ParametrosSistema_DBName = T001J3_A1170ParametrosSistema_DBName[0];
               Z1679ParametrosSistema_URLApp = T001J3_A1679ParametrosSistema_URLApp[0];
               Z1952ParametrosSistema_URLOtherVer = T001J3_A1952ParametrosSistema_URLOtherVer[0];
               Z1954ParametrosSistema_Validacao = T001J3_A1954ParametrosSistema_Validacao[0];
            }
            else
            {
               Z1021ParametrosSistema_PathCrtf = A1021ParametrosSistema_PathCrtf;
               Z1499ParametrosSistema_FlsEvd = A1499ParametrosSistema_FlsEvd;
               Z331ParametrosSistema_NomeSistema = A331ParametrosSistema_NomeSistema;
               Z334ParametrosSistema_AppID = A334ParametrosSistema_AppID;
               Z399ParametrosSistema_LicensiadoCadastrado = A399ParametrosSistema_LicensiadoCadastrado;
               Z417ParametrosSistema_PadronizarStrings = A417ParametrosSistema_PadronizarStrings;
               Z532ParametrosSistema_EmailSdaHost = A532ParametrosSistema_EmailSdaHost;
               Z533ParametrosSistema_EmailSdaUser = A533ParametrosSistema_EmailSdaUser;
               Z534ParametrosSistema_EmailSdaPass = A534ParametrosSistema_EmailSdaPass;
               Z537ParametrosSistema_EmailSdaKey = A537ParametrosSistema_EmailSdaKey;
               Z535ParametrosSistema_EmailSdaAut = A535ParametrosSistema_EmailSdaAut;
               Z536ParametrosSistema_EmailSdaPort = A536ParametrosSistema_EmailSdaPort;
               Z2032ParametrosSistema_EmailSdaSec = A2032ParametrosSistema_EmailSdaSec;
               Z708ParametrosSistema_FatorAjuste = A708ParametrosSistema_FatorAjuste;
               Z1163ParametrosSistema_HostMensuracao = A1163ParametrosSistema_HostMensuracao;
               Z1171ParametrosSistema_SQLComputerName = A1171ParametrosSistema_SQLComputerName;
               Z1172ParametrosSistema_SQLServerName = A1172ParametrosSistema_SQLServerName;
               Z1168ParametrosSistema_SQLEdition = A1168ParametrosSistema_SQLEdition;
               Z1169ParametrosSistema_SQLInstance = A1169ParametrosSistema_SQLInstance;
               Z1170ParametrosSistema_DBName = A1170ParametrosSistema_DBName;
               Z1679ParametrosSistema_URLApp = A1679ParametrosSistema_URLApp;
               Z1952ParametrosSistema_URLOtherVer = A1952ParametrosSistema_URLOtherVer;
               Z1954ParametrosSistema_Validacao = A1954ParametrosSistema_Validacao;
            }
         }
         if ( GX_JID == -14 )
         {
            Z330ParametrosSistema_Codigo = A330ParametrosSistema_Codigo;
            Z1021ParametrosSistema_PathCrtf = A1021ParametrosSistema_PathCrtf;
            Z1499ParametrosSistema_FlsEvd = A1499ParametrosSistema_FlsEvd;
            Z331ParametrosSistema_NomeSistema = A331ParametrosSistema_NomeSistema;
            Z334ParametrosSistema_AppID = A334ParametrosSistema_AppID;
            Z332ParametrosSistema_TextoHome = A332ParametrosSistema_TextoHome;
            Z399ParametrosSistema_LicensiadoCadastrado = A399ParametrosSistema_LicensiadoCadastrado;
            Z417ParametrosSistema_PadronizarStrings = A417ParametrosSistema_PadronizarStrings;
            Z532ParametrosSistema_EmailSdaHost = A532ParametrosSistema_EmailSdaHost;
            Z533ParametrosSistema_EmailSdaUser = A533ParametrosSistema_EmailSdaUser;
            Z534ParametrosSistema_EmailSdaPass = A534ParametrosSistema_EmailSdaPass;
            Z537ParametrosSistema_EmailSdaKey = A537ParametrosSistema_EmailSdaKey;
            Z535ParametrosSistema_EmailSdaAut = A535ParametrosSistema_EmailSdaAut;
            Z536ParametrosSistema_EmailSdaPort = A536ParametrosSistema_EmailSdaPort;
            Z2032ParametrosSistema_EmailSdaSec = A2032ParametrosSistema_EmailSdaSec;
            Z708ParametrosSistema_FatorAjuste = A708ParametrosSistema_FatorAjuste;
            Z1163ParametrosSistema_HostMensuracao = A1163ParametrosSistema_HostMensuracao;
            Z1171ParametrosSistema_SQLComputerName = A1171ParametrosSistema_SQLComputerName;
            Z1172ParametrosSistema_SQLServerName = A1172ParametrosSistema_SQLServerName;
            Z1168ParametrosSistema_SQLEdition = A1168ParametrosSistema_SQLEdition;
            Z1169ParametrosSistema_SQLInstance = A1169ParametrosSistema_SQLInstance;
            Z1170ParametrosSistema_DBName = A1170ParametrosSistema_DBName;
            Z1679ParametrosSistema_URLApp = A1679ParametrosSistema_URLApp;
            Z1952ParametrosSistema_URLOtherVer = A1952ParametrosSistema_URLOtherVer;
            Z1954ParametrosSistema_Validacao = A1954ParametrosSistema_Validacao;
            Z2117ParametrosSistema_Imagem = A2117ParametrosSistema_Imagem;
            Z40000ParametrosSistema_Imagem_GXI = A40000ParametrosSistema_Imagem_GXI;
         }
      }

      protected void standaloneNotModal( )
      {
         edtParametrosSistema_AppID_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtParametrosSistema_AppID_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtParametrosSistema_AppID_Enabled), 5, 0)));
         Gx_BScreen = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         edtParametrosSistema_AppID_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtParametrosSistema_AppID_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtParametrosSistema_AppID_Enabled), 5, 0)));
         bttBtn_trn_delete_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         if ( ! (0==AV7ParametrosSistema_Codigo) )
         {
            A330ParametrosSistema_Codigo = AV7ParametrosSistema_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A330ParametrosSistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A330ParametrosSistema_Codigo), 6, 0)));
         }
         if ( ! (0==AV7ParametrosSistema_Codigo) )
         {
            edtParametrosSistema_Codigo_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtParametrosSistema_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtParametrosSistema_Codigo_Enabled), 5, 0)));
         }
         else
         {
            edtParametrosSistema_Codigo_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtParametrosSistema_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtParametrosSistema_Codigo_Enabled), 5, 0)));
         }
         if ( ! (0==AV7ParametrosSistema_Codigo) )
         {
            edtParametrosSistema_Codigo_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtParametrosSistema_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtParametrosSistema_Codigo_Enabled), 5, 0)));
         }
      }

      protected void standaloneModal( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (Convert.ToDecimal(0)==A708ParametrosSistema_FatorAjuste) && ( Gx_BScreen == 0 ) )
         {
            A708ParametrosSistema_FatorAjuste = (decimal)(1);
            n708ParametrosSistema_FatorAjuste = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A708ParametrosSistema_FatorAjuste", StringUtil.LTrim( StringUtil.Str( A708ParametrosSistema_FatorAjuste, 6, 2)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (false==A535ParametrosSistema_EmailSdaAut) && ( Gx_BScreen == 0 ) )
         {
            A535ParametrosSistema_EmailSdaAut = false;
            n535ParametrosSistema_EmailSdaAut = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A535ParametrosSistema_EmailSdaAut", A535ParametrosSistema_EmailSdaAut);
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (false==A417ParametrosSistema_PadronizarStrings) && ( Gx_BScreen == 0 ) )
         {
            A417ParametrosSistema_PadronizarStrings = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A417ParametrosSistema_PadronizarStrings", A417ParametrosSistema_PadronizarStrings);
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (false==A399ParametrosSistema_LicensiadoCadastrado) && ( Gx_BScreen == 0 ) )
         {
            A399ParametrosSistema_LicensiadoCadastrado = false;
            n399ParametrosSistema_LicensiadoCadastrado = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A399ParametrosSistema_LicensiadoCadastrado", A399ParametrosSistema_LicensiadoCadastrado);
         }
      }

      protected void Load1J56( )
      {
         /* Using cursor T001J4 */
         pr_default.execute(2, new Object[] {A330ParametrosSistema_Codigo});
         if ( (pr_default.getStatus(2) != 101) )
         {
            RcdFound56 = 1;
            A1021ParametrosSistema_PathCrtf = T001J4_A1021ParametrosSistema_PathCrtf[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1021ParametrosSistema_PathCrtf", A1021ParametrosSistema_PathCrtf);
            n1021ParametrosSistema_PathCrtf = T001J4_n1021ParametrosSistema_PathCrtf[0];
            A1499ParametrosSistema_FlsEvd = T001J4_A1499ParametrosSistema_FlsEvd[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1499ParametrosSistema_FlsEvd", A1499ParametrosSistema_FlsEvd);
            n1499ParametrosSistema_FlsEvd = T001J4_n1499ParametrosSistema_FlsEvd[0];
            A331ParametrosSistema_NomeSistema = T001J4_A331ParametrosSistema_NomeSistema[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A331ParametrosSistema_NomeSistema", A331ParametrosSistema_NomeSistema);
            A334ParametrosSistema_AppID = T001J4_A334ParametrosSistema_AppID[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A334ParametrosSistema_AppID", StringUtil.LTrim( StringUtil.Str( (decimal)(A334ParametrosSistema_AppID), 12, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_PARAMETROSSISTEMA_APPID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A334ParametrosSistema_AppID), "ZZZZZZZZZZZ9")));
            A332ParametrosSistema_TextoHome = T001J4_A332ParametrosSistema_TextoHome[0];
            A399ParametrosSistema_LicensiadoCadastrado = T001J4_A399ParametrosSistema_LicensiadoCadastrado[0];
            n399ParametrosSistema_LicensiadoCadastrado = T001J4_n399ParametrosSistema_LicensiadoCadastrado[0];
            A417ParametrosSistema_PadronizarStrings = T001J4_A417ParametrosSistema_PadronizarStrings[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A417ParametrosSistema_PadronizarStrings", A417ParametrosSistema_PadronizarStrings);
            A532ParametrosSistema_EmailSdaHost = T001J4_A532ParametrosSistema_EmailSdaHost[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A532ParametrosSistema_EmailSdaHost", A532ParametrosSistema_EmailSdaHost);
            n532ParametrosSistema_EmailSdaHost = T001J4_n532ParametrosSistema_EmailSdaHost[0];
            A533ParametrosSistema_EmailSdaUser = T001J4_A533ParametrosSistema_EmailSdaUser[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A533ParametrosSistema_EmailSdaUser", A533ParametrosSistema_EmailSdaUser);
            n533ParametrosSistema_EmailSdaUser = T001J4_n533ParametrosSistema_EmailSdaUser[0];
            A534ParametrosSistema_EmailSdaPass = T001J4_A534ParametrosSistema_EmailSdaPass[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A534ParametrosSistema_EmailSdaPass", A534ParametrosSistema_EmailSdaPass);
            n534ParametrosSistema_EmailSdaPass = T001J4_n534ParametrosSistema_EmailSdaPass[0];
            A537ParametrosSistema_EmailSdaKey = T001J4_A537ParametrosSistema_EmailSdaKey[0];
            n537ParametrosSistema_EmailSdaKey = T001J4_n537ParametrosSistema_EmailSdaKey[0];
            A535ParametrosSistema_EmailSdaAut = T001J4_A535ParametrosSistema_EmailSdaAut[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A535ParametrosSistema_EmailSdaAut", A535ParametrosSistema_EmailSdaAut);
            n535ParametrosSistema_EmailSdaAut = T001J4_n535ParametrosSistema_EmailSdaAut[0];
            A536ParametrosSistema_EmailSdaPort = T001J4_A536ParametrosSistema_EmailSdaPort[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A536ParametrosSistema_EmailSdaPort", StringUtil.LTrim( StringUtil.Str( (decimal)(A536ParametrosSistema_EmailSdaPort), 4, 0)));
            n536ParametrosSistema_EmailSdaPort = T001J4_n536ParametrosSistema_EmailSdaPort[0];
            A2032ParametrosSistema_EmailSdaSec = T001J4_A2032ParametrosSistema_EmailSdaSec[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2032ParametrosSistema_EmailSdaSec", StringUtil.LTrim( StringUtil.Str( (decimal)(A2032ParametrosSistema_EmailSdaSec), 4, 0)));
            n2032ParametrosSistema_EmailSdaSec = T001J4_n2032ParametrosSistema_EmailSdaSec[0];
            A708ParametrosSistema_FatorAjuste = T001J4_A708ParametrosSistema_FatorAjuste[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A708ParametrosSistema_FatorAjuste", StringUtil.LTrim( StringUtil.Str( A708ParametrosSistema_FatorAjuste, 6, 2)));
            n708ParametrosSistema_FatorAjuste = T001J4_n708ParametrosSistema_FatorAjuste[0];
            A1163ParametrosSistema_HostMensuracao = T001J4_A1163ParametrosSistema_HostMensuracao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1163ParametrosSistema_HostMensuracao", A1163ParametrosSistema_HostMensuracao);
            n1163ParametrosSistema_HostMensuracao = T001J4_n1163ParametrosSistema_HostMensuracao[0];
            A1171ParametrosSistema_SQLComputerName = T001J4_A1171ParametrosSistema_SQLComputerName[0];
            n1171ParametrosSistema_SQLComputerName = T001J4_n1171ParametrosSistema_SQLComputerName[0];
            A1172ParametrosSistema_SQLServerName = T001J4_A1172ParametrosSistema_SQLServerName[0];
            n1172ParametrosSistema_SQLServerName = T001J4_n1172ParametrosSistema_SQLServerName[0];
            A1168ParametrosSistema_SQLEdition = T001J4_A1168ParametrosSistema_SQLEdition[0];
            n1168ParametrosSistema_SQLEdition = T001J4_n1168ParametrosSistema_SQLEdition[0];
            A1169ParametrosSistema_SQLInstance = T001J4_A1169ParametrosSistema_SQLInstance[0];
            n1169ParametrosSistema_SQLInstance = T001J4_n1169ParametrosSistema_SQLInstance[0];
            A1170ParametrosSistema_DBName = T001J4_A1170ParametrosSistema_DBName[0];
            n1170ParametrosSistema_DBName = T001J4_n1170ParametrosSistema_DBName[0];
            A1679ParametrosSistema_URLApp = T001J4_A1679ParametrosSistema_URLApp[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1679ParametrosSistema_URLApp", A1679ParametrosSistema_URLApp);
            n1679ParametrosSistema_URLApp = T001J4_n1679ParametrosSistema_URLApp[0];
            A1952ParametrosSistema_URLOtherVer = T001J4_A1952ParametrosSistema_URLOtherVer[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1952ParametrosSistema_URLOtherVer", A1952ParametrosSistema_URLOtherVer);
            n1952ParametrosSistema_URLOtherVer = T001J4_n1952ParametrosSistema_URLOtherVer[0];
            A1954ParametrosSistema_Validacao = T001J4_A1954ParametrosSistema_Validacao[0];
            n1954ParametrosSistema_Validacao = T001J4_n1954ParametrosSistema_Validacao[0];
            A40000ParametrosSistema_Imagem_GXI = T001J4_A40000ParametrosSistema_Imagem_GXI[0];
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgParametrosSistema_Imagem_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( A2117ParametrosSistema_Imagem)) ? A40000ParametrosSistema_Imagem_GXI : context.convertURL( context.PathToRelativeUrl( A2117ParametrosSistema_Imagem))));
            n40000ParametrosSistema_Imagem_GXI = T001J4_n40000ParametrosSistema_Imagem_GXI[0];
            A2117ParametrosSistema_Imagem = T001J4_A2117ParametrosSistema_Imagem[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2117ParametrosSistema_Imagem", A2117ParametrosSistema_Imagem);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgParametrosSistema_Imagem_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( A2117ParametrosSistema_Imagem)) ? A40000ParametrosSistema_Imagem_GXI : context.convertURL( context.PathToRelativeUrl( A2117ParametrosSistema_Imagem))));
            n2117ParametrosSistema_Imagem = T001J4_n2117ParametrosSistema_Imagem[0];
            ZM1J56( -14) ;
         }
         pr_default.close(2);
         OnLoadActions1J56( ) ;
      }

      protected void OnLoadActions1J56( )
      {
      }

      protected void CheckExtendedTable1J56( )
      {
         Gx_BScreen = 1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         standaloneModal( ) ;
      }

      protected void CloseExtendedTableCursors1J56( )
      {
      }

      protected void enableDisable( )
      {
      }

      protected void GetKey1J56( )
      {
         /* Using cursor T001J5 */
         pr_default.execute(3, new Object[] {A330ParametrosSistema_Codigo});
         if ( (pr_default.getStatus(3) != 101) )
         {
            RcdFound56 = 1;
         }
         else
         {
            RcdFound56 = 0;
         }
         pr_default.close(3);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T001J3 */
         pr_default.execute(1, new Object[] {A330ParametrosSistema_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM1J56( 14) ;
            RcdFound56 = 1;
            A330ParametrosSistema_Codigo = T001J3_A330ParametrosSistema_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A330ParametrosSistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A330ParametrosSistema_Codigo), 6, 0)));
            A1021ParametrosSistema_PathCrtf = T001J3_A1021ParametrosSistema_PathCrtf[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1021ParametrosSistema_PathCrtf", A1021ParametrosSistema_PathCrtf);
            n1021ParametrosSistema_PathCrtf = T001J3_n1021ParametrosSistema_PathCrtf[0];
            A1499ParametrosSistema_FlsEvd = T001J3_A1499ParametrosSistema_FlsEvd[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1499ParametrosSistema_FlsEvd", A1499ParametrosSistema_FlsEvd);
            n1499ParametrosSistema_FlsEvd = T001J3_n1499ParametrosSistema_FlsEvd[0];
            A331ParametrosSistema_NomeSistema = T001J3_A331ParametrosSistema_NomeSistema[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A331ParametrosSistema_NomeSistema", A331ParametrosSistema_NomeSistema);
            A334ParametrosSistema_AppID = T001J3_A334ParametrosSistema_AppID[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A334ParametrosSistema_AppID", StringUtil.LTrim( StringUtil.Str( (decimal)(A334ParametrosSistema_AppID), 12, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_PARAMETROSSISTEMA_APPID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A334ParametrosSistema_AppID), "ZZZZZZZZZZZ9")));
            A332ParametrosSistema_TextoHome = T001J3_A332ParametrosSistema_TextoHome[0];
            A399ParametrosSistema_LicensiadoCadastrado = T001J3_A399ParametrosSistema_LicensiadoCadastrado[0];
            n399ParametrosSistema_LicensiadoCadastrado = T001J3_n399ParametrosSistema_LicensiadoCadastrado[0];
            A417ParametrosSistema_PadronizarStrings = T001J3_A417ParametrosSistema_PadronizarStrings[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A417ParametrosSistema_PadronizarStrings", A417ParametrosSistema_PadronizarStrings);
            A532ParametrosSistema_EmailSdaHost = T001J3_A532ParametrosSistema_EmailSdaHost[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A532ParametrosSistema_EmailSdaHost", A532ParametrosSistema_EmailSdaHost);
            n532ParametrosSistema_EmailSdaHost = T001J3_n532ParametrosSistema_EmailSdaHost[0];
            A533ParametrosSistema_EmailSdaUser = T001J3_A533ParametrosSistema_EmailSdaUser[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A533ParametrosSistema_EmailSdaUser", A533ParametrosSistema_EmailSdaUser);
            n533ParametrosSistema_EmailSdaUser = T001J3_n533ParametrosSistema_EmailSdaUser[0];
            A534ParametrosSistema_EmailSdaPass = T001J3_A534ParametrosSistema_EmailSdaPass[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A534ParametrosSistema_EmailSdaPass", A534ParametrosSistema_EmailSdaPass);
            n534ParametrosSistema_EmailSdaPass = T001J3_n534ParametrosSistema_EmailSdaPass[0];
            A537ParametrosSistema_EmailSdaKey = T001J3_A537ParametrosSistema_EmailSdaKey[0];
            n537ParametrosSistema_EmailSdaKey = T001J3_n537ParametrosSistema_EmailSdaKey[0];
            A535ParametrosSistema_EmailSdaAut = T001J3_A535ParametrosSistema_EmailSdaAut[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A535ParametrosSistema_EmailSdaAut", A535ParametrosSistema_EmailSdaAut);
            n535ParametrosSistema_EmailSdaAut = T001J3_n535ParametrosSistema_EmailSdaAut[0];
            A536ParametrosSistema_EmailSdaPort = T001J3_A536ParametrosSistema_EmailSdaPort[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A536ParametrosSistema_EmailSdaPort", StringUtil.LTrim( StringUtil.Str( (decimal)(A536ParametrosSistema_EmailSdaPort), 4, 0)));
            n536ParametrosSistema_EmailSdaPort = T001J3_n536ParametrosSistema_EmailSdaPort[0];
            A2032ParametrosSistema_EmailSdaSec = T001J3_A2032ParametrosSistema_EmailSdaSec[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2032ParametrosSistema_EmailSdaSec", StringUtil.LTrim( StringUtil.Str( (decimal)(A2032ParametrosSistema_EmailSdaSec), 4, 0)));
            n2032ParametrosSistema_EmailSdaSec = T001J3_n2032ParametrosSistema_EmailSdaSec[0];
            A708ParametrosSistema_FatorAjuste = T001J3_A708ParametrosSistema_FatorAjuste[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A708ParametrosSistema_FatorAjuste", StringUtil.LTrim( StringUtil.Str( A708ParametrosSistema_FatorAjuste, 6, 2)));
            n708ParametrosSistema_FatorAjuste = T001J3_n708ParametrosSistema_FatorAjuste[0];
            A1163ParametrosSistema_HostMensuracao = T001J3_A1163ParametrosSistema_HostMensuracao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1163ParametrosSistema_HostMensuracao", A1163ParametrosSistema_HostMensuracao);
            n1163ParametrosSistema_HostMensuracao = T001J3_n1163ParametrosSistema_HostMensuracao[0];
            A1171ParametrosSistema_SQLComputerName = T001J3_A1171ParametrosSistema_SQLComputerName[0];
            n1171ParametrosSistema_SQLComputerName = T001J3_n1171ParametrosSistema_SQLComputerName[0];
            A1172ParametrosSistema_SQLServerName = T001J3_A1172ParametrosSistema_SQLServerName[0];
            n1172ParametrosSistema_SQLServerName = T001J3_n1172ParametrosSistema_SQLServerName[0];
            A1168ParametrosSistema_SQLEdition = T001J3_A1168ParametrosSistema_SQLEdition[0];
            n1168ParametrosSistema_SQLEdition = T001J3_n1168ParametrosSistema_SQLEdition[0];
            A1169ParametrosSistema_SQLInstance = T001J3_A1169ParametrosSistema_SQLInstance[0];
            n1169ParametrosSistema_SQLInstance = T001J3_n1169ParametrosSistema_SQLInstance[0];
            A1170ParametrosSistema_DBName = T001J3_A1170ParametrosSistema_DBName[0];
            n1170ParametrosSistema_DBName = T001J3_n1170ParametrosSistema_DBName[0];
            A1679ParametrosSistema_URLApp = T001J3_A1679ParametrosSistema_URLApp[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1679ParametrosSistema_URLApp", A1679ParametrosSistema_URLApp);
            n1679ParametrosSistema_URLApp = T001J3_n1679ParametrosSistema_URLApp[0];
            A1952ParametrosSistema_URLOtherVer = T001J3_A1952ParametrosSistema_URLOtherVer[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1952ParametrosSistema_URLOtherVer", A1952ParametrosSistema_URLOtherVer);
            n1952ParametrosSistema_URLOtherVer = T001J3_n1952ParametrosSistema_URLOtherVer[0];
            A1954ParametrosSistema_Validacao = T001J3_A1954ParametrosSistema_Validacao[0];
            n1954ParametrosSistema_Validacao = T001J3_n1954ParametrosSistema_Validacao[0];
            A40000ParametrosSistema_Imagem_GXI = T001J3_A40000ParametrosSistema_Imagem_GXI[0];
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgParametrosSistema_Imagem_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( A2117ParametrosSistema_Imagem)) ? A40000ParametrosSistema_Imagem_GXI : context.convertURL( context.PathToRelativeUrl( A2117ParametrosSistema_Imagem))));
            n40000ParametrosSistema_Imagem_GXI = T001J3_n40000ParametrosSistema_Imagem_GXI[0];
            A2117ParametrosSistema_Imagem = T001J3_A2117ParametrosSistema_Imagem[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2117ParametrosSistema_Imagem", A2117ParametrosSistema_Imagem);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgParametrosSistema_Imagem_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( A2117ParametrosSistema_Imagem)) ? A40000ParametrosSistema_Imagem_GXI : context.convertURL( context.PathToRelativeUrl( A2117ParametrosSistema_Imagem))));
            n2117ParametrosSistema_Imagem = T001J3_n2117ParametrosSistema_Imagem[0];
            O534ParametrosSistema_EmailSdaPass = A534ParametrosSistema_EmailSdaPass;
            n534ParametrosSistema_EmailSdaPass = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A534ParametrosSistema_EmailSdaPass", A534ParametrosSistema_EmailSdaPass);
            O1499ParametrosSistema_FlsEvd = A1499ParametrosSistema_FlsEvd;
            n1499ParametrosSistema_FlsEvd = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1499ParametrosSistema_FlsEvd", A1499ParametrosSistema_FlsEvd);
            O1021ParametrosSistema_PathCrtf = A1021ParametrosSistema_PathCrtf;
            n1021ParametrosSistema_PathCrtf = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1021ParametrosSistema_PathCrtf", A1021ParametrosSistema_PathCrtf);
            Z330ParametrosSistema_Codigo = A330ParametrosSistema_Codigo;
            sMode56 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load1J56( ) ;
            if ( AnyError == 1 )
            {
               RcdFound56 = 0;
               InitializeNonKey1J56( ) ;
            }
            Gx_mode = sMode56;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound56 = 0;
            InitializeNonKey1J56( ) ;
            sMode56 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal( ) ;
            Gx_mode = sMode56;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey1J56( ) ;
         if ( RcdFound56 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound56 = 0;
         /* Using cursor T001J6 */
         pr_default.execute(4, new Object[] {A330ParametrosSistema_Codigo});
         if ( (pr_default.getStatus(4) != 101) )
         {
            while ( (pr_default.getStatus(4) != 101) && ( ( T001J6_A330ParametrosSistema_Codigo[0] < A330ParametrosSistema_Codigo ) ) )
            {
               pr_default.readNext(4);
            }
            if ( (pr_default.getStatus(4) != 101) && ( ( T001J6_A330ParametrosSistema_Codigo[0] > A330ParametrosSistema_Codigo ) ) )
            {
               A330ParametrosSistema_Codigo = T001J6_A330ParametrosSistema_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A330ParametrosSistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A330ParametrosSistema_Codigo), 6, 0)));
               RcdFound56 = 1;
            }
         }
         pr_default.close(4);
      }

      protected void move_previous( )
      {
         RcdFound56 = 0;
         /* Using cursor T001J7 */
         pr_default.execute(5, new Object[] {A330ParametrosSistema_Codigo});
         if ( (pr_default.getStatus(5) != 101) )
         {
            while ( (pr_default.getStatus(5) != 101) && ( ( T001J7_A330ParametrosSistema_Codigo[0] > A330ParametrosSistema_Codigo ) ) )
            {
               pr_default.readNext(5);
            }
            if ( (pr_default.getStatus(5) != 101) && ( ( T001J7_A330ParametrosSistema_Codigo[0] < A330ParametrosSistema_Codigo ) ) )
            {
               A330ParametrosSistema_Codigo = T001J7_A330ParametrosSistema_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A330ParametrosSistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A330ParametrosSistema_Codigo), 6, 0)));
               RcdFound56 = 1;
            }
         }
         pr_default.close(5);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey1J56( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtParametrosSistema_NomeSistema_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert1J56( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound56 == 1 )
            {
               if ( A330ParametrosSistema_Codigo != Z330ParametrosSistema_Codigo )
               {
                  A330ParametrosSistema_Codigo = Z330ParametrosSistema_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A330ParametrosSistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A330ParametrosSistema_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "PARAMETROSSISTEMA_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtParametrosSistema_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtParametrosSistema_NomeSistema_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  Update1J56( ) ;
                  GX_FocusControl = edtParametrosSistema_NomeSistema_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A330ParametrosSistema_Codigo != Z330ParametrosSistema_Codigo )
               {
                  /* Insert record */
                  GX_FocusControl = edtParametrosSistema_NomeSistema_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert1J56( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "PARAMETROSSISTEMA_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtParametrosSistema_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     /* Insert record */
                     GX_FocusControl = edtParametrosSistema_NomeSistema_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert1J56( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( A330ParametrosSistema_Codigo != Z330ParametrosSistema_Codigo )
         {
            A330ParametrosSistema_Codigo = Z330ParametrosSistema_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A330ParametrosSistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A330ParametrosSistema_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "PARAMETROSSISTEMA_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtParametrosSistema_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtParametrosSistema_NomeSistema_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency1J56( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T001J2 */
            pr_default.execute(0, new Object[] {A330ParametrosSistema_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ParametrosSistema"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            Gx_longc = false;
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z1021ParametrosSistema_PathCrtf, T001J2_A1021ParametrosSistema_PathCrtf[0]) != 0 ) || ( StringUtil.StrCmp(Z1499ParametrosSistema_FlsEvd, T001J2_A1499ParametrosSistema_FlsEvd[0]) != 0 ) || ( StringUtil.StrCmp(Z331ParametrosSistema_NomeSistema, T001J2_A331ParametrosSistema_NomeSistema[0]) != 0 ) || ( Z334ParametrosSistema_AppID != T001J2_A334ParametrosSistema_AppID[0] ) || ( Z399ParametrosSistema_LicensiadoCadastrado != T001J2_A399ParametrosSistema_LicensiadoCadastrado[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z417ParametrosSistema_PadronizarStrings != T001J2_A417ParametrosSistema_PadronizarStrings[0] ) || ( StringUtil.StrCmp(Z532ParametrosSistema_EmailSdaHost, T001J2_A532ParametrosSistema_EmailSdaHost[0]) != 0 ) || ( StringUtil.StrCmp(Z533ParametrosSistema_EmailSdaUser, T001J2_A533ParametrosSistema_EmailSdaUser[0]) != 0 ) || ( StringUtil.StrCmp(Z534ParametrosSistema_EmailSdaPass, T001J2_A534ParametrosSistema_EmailSdaPass[0]) != 0 ) || ( StringUtil.StrCmp(Z537ParametrosSistema_EmailSdaKey, T001J2_A537ParametrosSistema_EmailSdaKey[0]) != 0 ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z535ParametrosSistema_EmailSdaAut != T001J2_A535ParametrosSistema_EmailSdaAut[0] ) || ( Z536ParametrosSistema_EmailSdaPort != T001J2_A536ParametrosSistema_EmailSdaPort[0] ) || ( Z2032ParametrosSistema_EmailSdaSec != T001J2_A2032ParametrosSistema_EmailSdaSec[0] ) || ( Z708ParametrosSistema_FatorAjuste != T001J2_A708ParametrosSistema_FatorAjuste[0] ) || ( StringUtil.StrCmp(Z1163ParametrosSistema_HostMensuracao, T001J2_A1163ParametrosSistema_HostMensuracao[0]) != 0 ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( StringUtil.StrCmp(Z1171ParametrosSistema_SQLComputerName, T001J2_A1171ParametrosSistema_SQLComputerName[0]) != 0 ) || ( StringUtil.StrCmp(Z1172ParametrosSistema_SQLServerName, T001J2_A1172ParametrosSistema_SQLServerName[0]) != 0 ) || ( StringUtil.StrCmp(Z1168ParametrosSistema_SQLEdition, T001J2_A1168ParametrosSistema_SQLEdition[0]) != 0 ) || ( StringUtil.StrCmp(Z1169ParametrosSistema_SQLInstance, T001J2_A1169ParametrosSistema_SQLInstance[0]) != 0 ) || ( StringUtil.StrCmp(Z1170ParametrosSistema_DBName, T001J2_A1170ParametrosSistema_DBName[0]) != 0 ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( StringUtil.StrCmp(Z1679ParametrosSistema_URLApp, T001J2_A1679ParametrosSistema_URLApp[0]) != 0 ) || ( StringUtil.StrCmp(Z1952ParametrosSistema_URLOtherVer, T001J2_A1952ParametrosSistema_URLOtherVer[0]) != 0 ) || ( StringUtil.StrCmp(Z1954ParametrosSistema_Validacao, T001J2_A1954ParametrosSistema_Validacao[0]) != 0 ) )
            {
               if ( StringUtil.StrCmp(Z1021ParametrosSistema_PathCrtf, T001J2_A1021ParametrosSistema_PathCrtf[0]) != 0 )
               {
                  GXUtil.WriteLog("parametrossistema:[seudo value changed for attri]"+"ParametrosSistema_PathCrtf");
                  GXUtil.WriteLogRaw("Old: ",Z1021ParametrosSistema_PathCrtf);
                  GXUtil.WriteLogRaw("Current: ",T001J2_A1021ParametrosSistema_PathCrtf[0]);
               }
               if ( StringUtil.StrCmp(Z1499ParametrosSistema_FlsEvd, T001J2_A1499ParametrosSistema_FlsEvd[0]) != 0 )
               {
                  GXUtil.WriteLog("parametrossistema:[seudo value changed for attri]"+"ParametrosSistema_FlsEvd");
                  GXUtil.WriteLogRaw("Old: ",Z1499ParametrosSistema_FlsEvd);
                  GXUtil.WriteLogRaw("Current: ",T001J2_A1499ParametrosSistema_FlsEvd[0]);
               }
               if ( StringUtil.StrCmp(Z331ParametrosSistema_NomeSistema, T001J2_A331ParametrosSistema_NomeSistema[0]) != 0 )
               {
                  GXUtil.WriteLog("parametrossistema:[seudo value changed for attri]"+"ParametrosSistema_NomeSistema");
                  GXUtil.WriteLogRaw("Old: ",Z331ParametrosSistema_NomeSistema);
                  GXUtil.WriteLogRaw("Current: ",T001J2_A331ParametrosSistema_NomeSistema[0]);
               }
               if ( Z334ParametrosSistema_AppID != T001J2_A334ParametrosSistema_AppID[0] )
               {
                  GXUtil.WriteLog("parametrossistema:[seudo value changed for attri]"+"ParametrosSistema_AppID");
                  GXUtil.WriteLogRaw("Old: ",Z334ParametrosSistema_AppID);
                  GXUtil.WriteLogRaw("Current: ",T001J2_A334ParametrosSistema_AppID[0]);
               }
               if ( Z399ParametrosSistema_LicensiadoCadastrado != T001J2_A399ParametrosSistema_LicensiadoCadastrado[0] )
               {
                  GXUtil.WriteLog("parametrossistema:[seudo value changed for attri]"+"ParametrosSistema_LicensiadoCadastrado");
                  GXUtil.WriteLogRaw("Old: ",Z399ParametrosSistema_LicensiadoCadastrado);
                  GXUtil.WriteLogRaw("Current: ",T001J2_A399ParametrosSistema_LicensiadoCadastrado[0]);
               }
               if ( Z417ParametrosSistema_PadronizarStrings != T001J2_A417ParametrosSistema_PadronizarStrings[0] )
               {
                  GXUtil.WriteLog("parametrossistema:[seudo value changed for attri]"+"ParametrosSistema_PadronizarStrings");
                  GXUtil.WriteLogRaw("Old: ",Z417ParametrosSistema_PadronizarStrings);
                  GXUtil.WriteLogRaw("Current: ",T001J2_A417ParametrosSistema_PadronizarStrings[0]);
               }
               if ( StringUtil.StrCmp(Z532ParametrosSistema_EmailSdaHost, T001J2_A532ParametrosSistema_EmailSdaHost[0]) != 0 )
               {
                  GXUtil.WriteLog("parametrossistema:[seudo value changed for attri]"+"ParametrosSistema_EmailSdaHost");
                  GXUtil.WriteLogRaw("Old: ",Z532ParametrosSistema_EmailSdaHost);
                  GXUtil.WriteLogRaw("Current: ",T001J2_A532ParametrosSistema_EmailSdaHost[0]);
               }
               if ( StringUtil.StrCmp(Z533ParametrosSistema_EmailSdaUser, T001J2_A533ParametrosSistema_EmailSdaUser[0]) != 0 )
               {
                  GXUtil.WriteLog("parametrossistema:[seudo value changed for attri]"+"ParametrosSistema_EmailSdaUser");
                  GXUtil.WriteLogRaw("Old: ",Z533ParametrosSistema_EmailSdaUser);
                  GXUtil.WriteLogRaw("Current: ",T001J2_A533ParametrosSistema_EmailSdaUser[0]);
               }
               if ( StringUtil.StrCmp(Z534ParametrosSistema_EmailSdaPass, T001J2_A534ParametrosSistema_EmailSdaPass[0]) != 0 )
               {
                  GXUtil.WriteLog("parametrossistema:[seudo value changed for attri]"+"ParametrosSistema_EmailSdaPass");
                  GXUtil.WriteLogRaw("Old: ",Z534ParametrosSistema_EmailSdaPass);
                  GXUtil.WriteLogRaw("Current: ",T001J2_A534ParametrosSistema_EmailSdaPass[0]);
               }
               if ( StringUtil.StrCmp(Z537ParametrosSistema_EmailSdaKey, T001J2_A537ParametrosSistema_EmailSdaKey[0]) != 0 )
               {
                  GXUtil.WriteLog("parametrossistema:[seudo value changed for attri]"+"ParametrosSistema_EmailSdaKey");
                  GXUtil.WriteLogRaw("Old: ",Z537ParametrosSistema_EmailSdaKey);
                  GXUtil.WriteLogRaw("Current: ",T001J2_A537ParametrosSistema_EmailSdaKey[0]);
               }
               if ( Z535ParametrosSistema_EmailSdaAut != T001J2_A535ParametrosSistema_EmailSdaAut[0] )
               {
                  GXUtil.WriteLog("parametrossistema:[seudo value changed for attri]"+"ParametrosSistema_EmailSdaAut");
                  GXUtil.WriteLogRaw("Old: ",Z535ParametrosSistema_EmailSdaAut);
                  GXUtil.WriteLogRaw("Current: ",T001J2_A535ParametrosSistema_EmailSdaAut[0]);
               }
               if ( Z536ParametrosSistema_EmailSdaPort != T001J2_A536ParametrosSistema_EmailSdaPort[0] )
               {
                  GXUtil.WriteLog("parametrossistema:[seudo value changed for attri]"+"ParametrosSistema_EmailSdaPort");
                  GXUtil.WriteLogRaw("Old: ",Z536ParametrosSistema_EmailSdaPort);
                  GXUtil.WriteLogRaw("Current: ",T001J2_A536ParametrosSistema_EmailSdaPort[0]);
               }
               if ( Z2032ParametrosSistema_EmailSdaSec != T001J2_A2032ParametrosSistema_EmailSdaSec[0] )
               {
                  GXUtil.WriteLog("parametrossistema:[seudo value changed for attri]"+"ParametrosSistema_EmailSdaSec");
                  GXUtil.WriteLogRaw("Old: ",Z2032ParametrosSistema_EmailSdaSec);
                  GXUtil.WriteLogRaw("Current: ",T001J2_A2032ParametrosSistema_EmailSdaSec[0]);
               }
               if ( Z708ParametrosSistema_FatorAjuste != T001J2_A708ParametrosSistema_FatorAjuste[0] )
               {
                  GXUtil.WriteLog("parametrossistema:[seudo value changed for attri]"+"ParametrosSistema_FatorAjuste");
                  GXUtil.WriteLogRaw("Old: ",Z708ParametrosSistema_FatorAjuste);
                  GXUtil.WriteLogRaw("Current: ",T001J2_A708ParametrosSistema_FatorAjuste[0]);
               }
               if ( StringUtil.StrCmp(Z1163ParametrosSistema_HostMensuracao, T001J2_A1163ParametrosSistema_HostMensuracao[0]) != 0 )
               {
                  GXUtil.WriteLog("parametrossistema:[seudo value changed for attri]"+"ParametrosSistema_HostMensuracao");
                  GXUtil.WriteLogRaw("Old: ",Z1163ParametrosSistema_HostMensuracao);
                  GXUtil.WriteLogRaw("Current: ",T001J2_A1163ParametrosSistema_HostMensuracao[0]);
               }
               if ( StringUtil.StrCmp(Z1171ParametrosSistema_SQLComputerName, T001J2_A1171ParametrosSistema_SQLComputerName[0]) != 0 )
               {
                  GXUtil.WriteLog("parametrossistema:[seudo value changed for attri]"+"ParametrosSistema_SQLComputerName");
                  GXUtil.WriteLogRaw("Old: ",Z1171ParametrosSistema_SQLComputerName);
                  GXUtil.WriteLogRaw("Current: ",T001J2_A1171ParametrosSistema_SQLComputerName[0]);
               }
               if ( StringUtil.StrCmp(Z1172ParametrosSistema_SQLServerName, T001J2_A1172ParametrosSistema_SQLServerName[0]) != 0 )
               {
                  GXUtil.WriteLog("parametrossistema:[seudo value changed for attri]"+"ParametrosSistema_SQLServerName");
                  GXUtil.WriteLogRaw("Old: ",Z1172ParametrosSistema_SQLServerName);
                  GXUtil.WriteLogRaw("Current: ",T001J2_A1172ParametrosSistema_SQLServerName[0]);
               }
               if ( StringUtil.StrCmp(Z1168ParametrosSistema_SQLEdition, T001J2_A1168ParametrosSistema_SQLEdition[0]) != 0 )
               {
                  GXUtil.WriteLog("parametrossistema:[seudo value changed for attri]"+"ParametrosSistema_SQLEdition");
                  GXUtil.WriteLogRaw("Old: ",Z1168ParametrosSistema_SQLEdition);
                  GXUtil.WriteLogRaw("Current: ",T001J2_A1168ParametrosSistema_SQLEdition[0]);
               }
               if ( StringUtil.StrCmp(Z1169ParametrosSistema_SQLInstance, T001J2_A1169ParametrosSistema_SQLInstance[0]) != 0 )
               {
                  GXUtil.WriteLog("parametrossistema:[seudo value changed for attri]"+"ParametrosSistema_SQLInstance");
                  GXUtil.WriteLogRaw("Old: ",Z1169ParametrosSistema_SQLInstance);
                  GXUtil.WriteLogRaw("Current: ",T001J2_A1169ParametrosSistema_SQLInstance[0]);
               }
               if ( StringUtil.StrCmp(Z1170ParametrosSistema_DBName, T001J2_A1170ParametrosSistema_DBName[0]) != 0 )
               {
                  GXUtil.WriteLog("parametrossistema:[seudo value changed for attri]"+"ParametrosSistema_DBName");
                  GXUtil.WriteLogRaw("Old: ",Z1170ParametrosSistema_DBName);
                  GXUtil.WriteLogRaw("Current: ",T001J2_A1170ParametrosSistema_DBName[0]);
               }
               if ( StringUtil.StrCmp(Z1679ParametrosSistema_URLApp, T001J2_A1679ParametrosSistema_URLApp[0]) != 0 )
               {
                  GXUtil.WriteLog("parametrossistema:[seudo value changed for attri]"+"ParametrosSistema_URLApp");
                  GXUtil.WriteLogRaw("Old: ",Z1679ParametrosSistema_URLApp);
                  GXUtil.WriteLogRaw("Current: ",T001J2_A1679ParametrosSistema_URLApp[0]);
               }
               if ( StringUtil.StrCmp(Z1952ParametrosSistema_URLOtherVer, T001J2_A1952ParametrosSistema_URLOtherVer[0]) != 0 )
               {
                  GXUtil.WriteLog("parametrossistema:[seudo value changed for attri]"+"ParametrosSistema_URLOtherVer");
                  GXUtil.WriteLogRaw("Old: ",Z1952ParametrosSistema_URLOtherVer);
                  GXUtil.WriteLogRaw("Current: ",T001J2_A1952ParametrosSistema_URLOtherVer[0]);
               }
               if ( StringUtil.StrCmp(Z1954ParametrosSistema_Validacao, T001J2_A1954ParametrosSistema_Validacao[0]) != 0 )
               {
                  GXUtil.WriteLog("parametrossistema:[seudo value changed for attri]"+"ParametrosSistema_Validacao");
                  GXUtil.WriteLogRaw("Old: ",Z1954ParametrosSistema_Validacao);
                  GXUtil.WriteLogRaw("Current: ",T001J2_A1954ParametrosSistema_Validacao[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"ParametrosSistema"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert1J56( )
      {
         BeforeValidate1J56( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable1J56( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM1J56( 0) ;
            CheckOptimisticConcurrency1J56( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm1J56( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert1J56( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T001J8 */
                     pr_default.execute(6, new Object[] {A330ParametrosSistema_Codigo, n1021ParametrosSistema_PathCrtf, A1021ParametrosSistema_PathCrtf, n1499ParametrosSistema_FlsEvd, A1499ParametrosSistema_FlsEvd, A331ParametrosSistema_NomeSistema, A334ParametrosSistema_AppID, A332ParametrosSistema_TextoHome, n399ParametrosSistema_LicensiadoCadastrado, A399ParametrosSistema_LicensiadoCadastrado, A417ParametrosSistema_PadronizarStrings, n532ParametrosSistema_EmailSdaHost, A532ParametrosSistema_EmailSdaHost, n533ParametrosSistema_EmailSdaUser, A533ParametrosSistema_EmailSdaUser, n534ParametrosSistema_EmailSdaPass, A534ParametrosSistema_EmailSdaPass, n537ParametrosSistema_EmailSdaKey, A537ParametrosSistema_EmailSdaKey, n535ParametrosSistema_EmailSdaAut, A535ParametrosSistema_EmailSdaAut, n536ParametrosSistema_EmailSdaPort, A536ParametrosSistema_EmailSdaPort, n2032ParametrosSistema_EmailSdaSec, A2032ParametrosSistema_EmailSdaSec, n708ParametrosSistema_FatorAjuste, A708ParametrosSistema_FatorAjuste, n1163ParametrosSistema_HostMensuracao, A1163ParametrosSistema_HostMensuracao, n1171ParametrosSistema_SQLComputerName, A1171ParametrosSistema_SQLComputerName, n1172ParametrosSistema_SQLServerName, A1172ParametrosSistema_SQLServerName, n1168ParametrosSistema_SQLEdition, A1168ParametrosSistema_SQLEdition, n1169ParametrosSistema_SQLInstance, A1169ParametrosSistema_SQLInstance, n1170ParametrosSistema_DBName, A1170ParametrosSistema_DBName, n1679ParametrosSistema_URLApp, A1679ParametrosSistema_URLApp, n1952ParametrosSistema_URLOtherVer, A1952ParametrosSistema_URLOtherVer, n1954ParametrosSistema_Validacao, A1954ParametrosSistema_Validacao, n2117ParametrosSistema_Imagem, A2117ParametrosSistema_Imagem, n40000ParametrosSistema_Imagem_GXI, A40000ParametrosSistema_Imagem_GXI});
                     pr_default.close(6);
                     dsDefault.SmartCacheProvider.SetUpdated("ParametrosSistema") ;
                     if ( (pr_default.getStatus(6) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption1J0( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load1J56( ) ;
            }
            EndLevel1J56( ) ;
         }
         CloseExtendedTableCursors1J56( ) ;
      }

      protected void Update1J56( )
      {
         BeforeValidate1J56( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable1J56( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency1J56( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm1J56( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate1J56( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T001J9 */
                     pr_default.execute(7, new Object[] {n1021ParametrosSistema_PathCrtf, A1021ParametrosSistema_PathCrtf, n1499ParametrosSistema_FlsEvd, A1499ParametrosSistema_FlsEvd, A331ParametrosSistema_NomeSistema, A334ParametrosSistema_AppID, A332ParametrosSistema_TextoHome, n399ParametrosSistema_LicensiadoCadastrado, A399ParametrosSistema_LicensiadoCadastrado, A417ParametrosSistema_PadronizarStrings, n532ParametrosSistema_EmailSdaHost, A532ParametrosSistema_EmailSdaHost, n533ParametrosSistema_EmailSdaUser, A533ParametrosSistema_EmailSdaUser, n534ParametrosSistema_EmailSdaPass, A534ParametrosSistema_EmailSdaPass, n537ParametrosSistema_EmailSdaKey, A537ParametrosSistema_EmailSdaKey, n535ParametrosSistema_EmailSdaAut, A535ParametrosSistema_EmailSdaAut, n536ParametrosSistema_EmailSdaPort, A536ParametrosSistema_EmailSdaPort, n2032ParametrosSistema_EmailSdaSec, A2032ParametrosSistema_EmailSdaSec, n708ParametrosSistema_FatorAjuste, A708ParametrosSistema_FatorAjuste, n1163ParametrosSistema_HostMensuracao, A1163ParametrosSistema_HostMensuracao, n1171ParametrosSistema_SQLComputerName, A1171ParametrosSistema_SQLComputerName, n1172ParametrosSistema_SQLServerName, A1172ParametrosSistema_SQLServerName, n1168ParametrosSistema_SQLEdition, A1168ParametrosSistema_SQLEdition, n1169ParametrosSistema_SQLInstance, A1169ParametrosSistema_SQLInstance, n1170ParametrosSistema_DBName, A1170ParametrosSistema_DBName, n1679ParametrosSistema_URLApp, A1679ParametrosSistema_URLApp, n1952ParametrosSistema_URLOtherVer, A1952ParametrosSistema_URLOtherVer, n1954ParametrosSistema_Validacao, A1954ParametrosSistema_Validacao, A330ParametrosSistema_Codigo});
                     pr_default.close(7);
                     dsDefault.SmartCacheProvider.SetUpdated("ParametrosSistema") ;
                     if ( (pr_default.getStatus(7) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ParametrosSistema"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate1J56( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel1J56( ) ;
         }
         CloseExtendedTableCursors1J56( ) ;
      }

      protected void DeferredUpdate1J56( )
      {
         if ( AnyError == 0 )
         {
            /* Using cursor T001J10 */
            pr_default.execute(8, new Object[] {n2117ParametrosSistema_Imagem, A2117ParametrosSistema_Imagem, n40000ParametrosSistema_Imagem_GXI, A40000ParametrosSistema_Imagem_GXI, A330ParametrosSistema_Codigo});
            pr_default.close(8);
            dsDefault.SmartCacheProvider.SetUpdated("ParametrosSistema") ;
         }
      }

      protected void delete( )
      {
         BeforeValidate1J56( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency1J56( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls1J56( ) ;
            AfterConfirm1J56( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete1J56( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T001J11 */
                  pr_default.execute(9, new Object[] {A330ParametrosSistema_Codigo});
                  pr_default.close(9);
                  dsDefault.SmartCacheProvider.SetUpdated("ParametrosSistema") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                        {
                           if ( AnyError == 0 )
                           {
                              context.nUserReturn = 1;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode56 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel1J56( ) ;
         Gx_mode = sMode56;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls1J56( )
      {
         standaloneModal( ) ;
         /* No delete mode formulas found. */
      }

      protected void EndLevel1J56( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete1J56( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            context.CommitDataStores( "ParametrosSistema");
            if ( AnyError == 0 )
            {
               ConfirmValues1J0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            context.RollbackDataStores( "ParametrosSistema");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart1J56( )
      {
         /* Scan By routine */
         /* Using cursor T001J12 */
         pr_default.execute(10);
         RcdFound56 = 0;
         if ( (pr_default.getStatus(10) != 101) )
         {
            RcdFound56 = 1;
            A330ParametrosSistema_Codigo = T001J12_A330ParametrosSistema_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A330ParametrosSistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A330ParametrosSistema_Codigo), 6, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext1J56( )
      {
         /* Scan next routine */
         pr_default.readNext(10);
         RcdFound56 = 0;
         if ( (pr_default.getStatus(10) != 101) )
         {
            RcdFound56 = 1;
            A330ParametrosSistema_Codigo = T001J12_A330ParametrosSistema_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A330ParametrosSistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A330ParametrosSistema_Codigo), 6, 0)));
         }
      }

      protected void ScanEnd1J56( )
      {
         pr_default.close(10);
      }

      protected void AfterConfirm1J56( )
      {
         /* After Confirm Rules */
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  || ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )  && ( StringUtil.StrCmp(A534ParametrosSistema_EmailSdaPass, O534ParametrosSistema_EmailSdaPass) != 0 ) ) )
         {
            AV14KeyEnc = Crypto.GetEncryptionKey( );
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14KeyEnc", AV14KeyEnc);
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  || ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )  && ( StringUtil.StrCmp(A534ParametrosSistema_EmailSdaPass, O534ParametrosSistema_EmailSdaPass) != 0 ) ) )
         {
            A534ParametrosSistema_EmailSdaPass = Crypto.Encrypt64( O534ParametrosSistema_EmailSdaPass, AV14KeyEnc);
            n534ParametrosSistema_EmailSdaPass = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A534ParametrosSistema_EmailSdaPass", A534ParametrosSistema_EmailSdaPass);
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  || ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )  && ( StringUtil.StrCmp(A534ParametrosSistema_EmailSdaPass, O534ParametrosSistema_EmailSdaPass) != 0 ) ) )
         {
            A537ParametrosSistema_EmailSdaKey = AV14KeyEnc;
            n537ParametrosSistema_EmailSdaKey = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A537ParametrosSistema_EmailSdaKey", A537ParametrosSistema_EmailSdaKey);
         }
      }

      protected void BeforeInsert1J56( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate1J56( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete1J56( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete1J56( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate1J56( )
      {
         /* Before Validate Rules */
         if ( ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  || ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )  ) && ( StringUtil.StringSearch( A1499ParametrosSistema_FlsEvd, "/", 1) > 0 ) )
         {
            A1499ParametrosSistema_FlsEvd = StringUtil.StringReplace( O1499ParametrosSistema_FlsEvd, "/", "\\");
            n1499ParametrosSistema_FlsEvd = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1499ParametrosSistema_FlsEvd", A1499ParametrosSistema_FlsEvd);
         }
         else
         {
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  || ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )  && ! String.IsNullOrEmpty(StringUtil.RTrim( A1499ParametrosSistema_FlsEvd)) && ( StringUtil.StrCmp(StringUtil.Substring( A1499ParametrosSistema_FlsEvd, StringUtil.Len( StringUtil.Trim( A1499ParametrosSistema_FlsEvd)), 1), "\\") != 0 ) )
            {
               A1499ParametrosSistema_FlsEvd = StringUtil.Trim( A1499ParametrosSistema_FlsEvd) + "\\";
               n1499ParametrosSistema_FlsEvd = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1499ParametrosSistema_FlsEvd", A1499ParametrosSistema_FlsEvd);
            }
         }
         if ( ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  || ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )  ) && ( StringUtil.StringSearch( A1021ParametrosSistema_PathCrtf, "/", 1) > 0 ) )
         {
            A1021ParametrosSistema_PathCrtf = StringUtil.StringReplace( O1021ParametrosSistema_PathCrtf, "/", "\\");
            n1021ParametrosSistema_PathCrtf = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1021ParametrosSistema_PathCrtf", A1021ParametrosSistema_PathCrtf);
         }
         else
         {
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  || ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )  && ! String.IsNullOrEmpty(StringUtil.RTrim( A1021ParametrosSistema_PathCrtf)) && ( StringUtil.StrCmp(StringUtil.Substring( A1021ParametrosSistema_PathCrtf, StringUtil.Len( StringUtil.Trim( A1021ParametrosSistema_PathCrtf)), 1), "\\") != 0 ) )
            {
               A1021ParametrosSistema_PathCrtf = StringUtil.Trim( A1021ParametrosSistema_PathCrtf) + "\\";
               n1021ParametrosSistema_PathCrtf = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1021ParametrosSistema_PathCrtf", A1021ParametrosSistema_PathCrtf);
            }
         }
      }

      protected void DisableAttributes1J56( )
      {
         edtParametrosSistema_NomeSistema_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtParametrosSistema_NomeSistema_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtParametrosSistema_NomeSistema_Enabled), 5, 0)));
         cmbParametrosSistema_PadronizarStrings.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbParametrosSistema_PadronizarStrings_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbParametrosSistema_PadronizarStrings.Enabled), 5, 0)));
         edtParametrosSistema_FatorAjuste_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtParametrosSistema_FatorAjuste_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtParametrosSistema_FatorAjuste_Enabled), 5, 0)));
         edtParametrosSistema_PathCrtf_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtParametrosSistema_PathCrtf_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtParametrosSistema_PathCrtf_Enabled), 5, 0)));
         edtParametrosSistema_FlsEvd_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtParametrosSistema_FlsEvd_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtParametrosSistema_FlsEvd_Enabled), 5, 0)));
         edtParametrosSistema_EmailSdaHost_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtParametrosSistema_EmailSdaHost_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtParametrosSistema_EmailSdaHost_Enabled), 5, 0)));
         edtParametrosSistema_EmailSdaUser_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtParametrosSistema_EmailSdaUser_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtParametrosSistema_EmailSdaUser_Enabled), 5, 0)));
         edtParametrosSistema_EmailSdaPass_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtParametrosSistema_EmailSdaPass_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtParametrosSistema_EmailSdaPass_Enabled), 5, 0)));
         edtParametrosSistema_EmailSdaPort_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtParametrosSistema_EmailSdaPort_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtParametrosSistema_EmailSdaPort_Enabled), 5, 0)));
         cmbParametrosSistema_EmailSdaAut.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbParametrosSistema_EmailSdaAut_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbParametrosSistema_EmailSdaAut.Enabled), 5, 0)));
         cmbParametrosSistema_EmailSdaSec.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbParametrosSistema_EmailSdaSec_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbParametrosSistema_EmailSdaSec.Enabled), 5, 0)));
         edtParametrosSistema_URLApp_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtParametrosSistema_URLApp_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtParametrosSistema_URLApp_Enabled), 5, 0)));
         edtParametrosSistema_URLOtherVer_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtParametrosSistema_URLOtherVer_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtParametrosSistema_URLOtherVer_Enabled), 5, 0)));
         edtParametrosSistema_HostMensuracao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtParametrosSistema_HostMensuracao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtParametrosSistema_HostMensuracao_Enabled), 5, 0)));
         imgParametrosSistema_Imagem_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgParametrosSistema_Imagem_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgParametrosSistema_Imagem_Enabled), 5, 0)));
         edtParametrosSistema_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtParametrosSistema_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtParametrosSistema_Codigo_Enabled), 5, 0)));
         edtParametrosSistema_AppID_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtParametrosSistema_AppID_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtParametrosSistema_AppID_Enabled), 5, 0)));
         Parametrossistema_textohome_Enabled = Convert.ToBoolean( 0);
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Parametrossistema_textohome_Internalname, "Enabled", StringUtil.BoolToStr( Parametrossistema_textohome_Enabled));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues1J0( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202051812491566");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/TabsPanel/BootstrapTabsPanelRender.js", "");
         context.AddJavascriptSource("CKEditor/ckeditor/ckeditor.js", "");
         context.AddJavascriptSource("CKEditor/CKEditorRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("parametrossistema.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7ParametrosSistema_Codigo)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z330ParametrosSistema_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z330ParametrosSistema_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1021ParametrosSistema_PathCrtf", Z1021ParametrosSistema_PathCrtf);
         GxWebStd.gx_hidden_field( context, "Z1499ParametrosSistema_FlsEvd", Z1499ParametrosSistema_FlsEvd);
         GxWebStd.gx_hidden_field( context, "Z331ParametrosSistema_NomeSistema", StringUtil.RTrim( Z331ParametrosSistema_NomeSistema));
         GxWebStd.gx_hidden_field( context, "Z334ParametrosSistema_AppID", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z334ParametrosSistema_AppID), 12, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "Z399ParametrosSistema_LicensiadoCadastrado", Z399ParametrosSistema_LicensiadoCadastrado);
         GxWebStd.gx_boolean_hidden_field( context, "Z417ParametrosSistema_PadronizarStrings", Z417ParametrosSistema_PadronizarStrings);
         GxWebStd.gx_hidden_field( context, "Z532ParametrosSistema_EmailSdaHost", Z532ParametrosSistema_EmailSdaHost);
         GxWebStd.gx_hidden_field( context, "Z533ParametrosSistema_EmailSdaUser", Z533ParametrosSistema_EmailSdaUser);
         GxWebStd.gx_hidden_field( context, "Z534ParametrosSistema_EmailSdaPass", Z534ParametrosSistema_EmailSdaPass);
         GxWebStd.gx_hidden_field( context, "Z537ParametrosSistema_EmailSdaKey", StringUtil.RTrim( Z537ParametrosSistema_EmailSdaKey));
         GxWebStd.gx_boolean_hidden_field( context, "Z535ParametrosSistema_EmailSdaAut", Z535ParametrosSistema_EmailSdaAut);
         GxWebStd.gx_hidden_field( context, "Z536ParametrosSistema_EmailSdaPort", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z536ParametrosSistema_EmailSdaPort), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z2032ParametrosSistema_EmailSdaSec", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z2032ParametrosSistema_EmailSdaSec), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z708ParametrosSistema_FatorAjuste", StringUtil.LTrim( StringUtil.NToC( Z708ParametrosSistema_FatorAjuste, 6, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1163ParametrosSistema_HostMensuracao", Z1163ParametrosSistema_HostMensuracao);
         GxWebStd.gx_hidden_field( context, "Z1171ParametrosSistema_SQLComputerName", Z1171ParametrosSistema_SQLComputerName);
         GxWebStd.gx_hidden_field( context, "Z1172ParametrosSistema_SQLServerName", Z1172ParametrosSistema_SQLServerName);
         GxWebStd.gx_hidden_field( context, "Z1168ParametrosSistema_SQLEdition", Z1168ParametrosSistema_SQLEdition);
         GxWebStd.gx_hidden_field( context, "Z1169ParametrosSistema_SQLInstance", Z1169ParametrosSistema_SQLInstance);
         GxWebStd.gx_hidden_field( context, "Z1170ParametrosSistema_DBName", Z1170ParametrosSistema_DBName);
         GxWebStd.gx_hidden_field( context, "Z1679ParametrosSistema_URLApp", Z1679ParametrosSistema_URLApp);
         GxWebStd.gx_hidden_field( context, "Z1952ParametrosSistema_URLOtherVer", Z1952ParametrosSistema_URLOtherVer);
         GxWebStd.gx_hidden_field( context, "Z1954ParametrosSistema_Validacao", Z1954ParametrosSistema_Validacao);
         GxWebStd.gx_hidden_field( context, "O534ParametrosSistema_EmailSdaPass", O534ParametrosSistema_EmailSdaPass);
         GxWebStd.gx_hidden_field( context, "O1499ParametrosSistema_FlsEvd", O1499ParametrosSistema_FlsEvd);
         GxWebStd.gx_hidden_field( context, "O1021ParametrosSistema_PathCrtf", O1021ParametrosSistema_PathCrtf);
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV9TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV9TrnContext);
         }
         GxWebStd.gx_hidden_field( context, "vPARAMETROSSISTEMA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7ParametrosSistema_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vKEYENC", StringUtil.RTrim( AV14KeyEnc));
         GxWebStd.gx_hidden_field( context, "vGXBSCREEN", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gx_BScreen), 1, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "PARAMETROSSISTEMA_LICENSIADOCADASTRADO", A399ParametrosSistema_LicensiadoCadastrado);
         GxWebStd.gx_hidden_field( context, "PARAMETROSSISTEMA_EMAILSDAKEY", StringUtil.RTrim( A537ParametrosSistema_EmailSdaKey));
         GxWebStd.gx_hidden_field( context, "PARAMETROSSISTEMA_TEXTOHOME", A332ParametrosSistema_TextoHome);
         GxWebStd.gx_hidden_field( context, "PARAMETROSSISTEMA_SQLCOMPUTERNAME", A1171ParametrosSistema_SQLComputerName);
         GxWebStd.gx_hidden_field( context, "PARAMETROSSISTEMA_SQLSERVERNAME", A1172ParametrosSistema_SQLServerName);
         GxWebStd.gx_hidden_field( context, "PARAMETROSSISTEMA_SQLEDITION", A1168ParametrosSistema_SQLEdition);
         GxWebStd.gx_hidden_field( context, "PARAMETROSSISTEMA_SQLINSTANCE", A1169ParametrosSistema_SQLInstance);
         GxWebStd.gx_hidden_field( context, "PARAMETROSSISTEMA_DBNAME", A1170ParametrosSistema_DBName);
         GxWebStd.gx_hidden_field( context, "PARAMETROSSISTEMA_VALIDACAO", A1954ParametrosSistema_Validacao);
         GxWebStd.gx_hidden_field( context, "PARAMETROSSISTEMA_IMAGEM_GXI", A40000ParametrosSistema_Imagem_GXI);
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_PARAMETROSSISTEMA_APPID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A334ParametrosSistema_AppID), "ZZZZZZZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vPARAMETROSSISTEMA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7ParametrosSistema_Codigo), "ZZZZZ9")));
         GXCCtlgxBlob = "PARAMETROSSISTEMA_IMAGEM" + "_gxBlob";
         GxWebStd.gx_hidden_field( context, GXCCtlgxBlob, A2117ParametrosSistema_Imagem);
         GxWebStd.gx_hidden_field( context, "PARAMETROSSISTEMA_TEXTOHOME_Enabled", StringUtil.BoolToStr( Parametrossistema_textohome_Enabled));
         GxWebStd.gx_hidden_field( context, "GXUITABSPANEL_TABSPARAMETROS_Width", StringUtil.RTrim( Gxuitabspanel_tabsparametros_Width));
         GxWebStd.gx_hidden_field( context, "GXUITABSPANEL_TABSPARAMETROS_Cls", StringUtil.RTrim( Gxuitabspanel_tabsparametros_Cls));
         GxWebStd.gx_hidden_field( context, "GXUITABSPANEL_TABSPARAMETROS_Enabled", StringUtil.BoolToStr( Gxuitabspanel_tabsparametros_Enabled));
         GxWebStd.gx_hidden_field( context, "GXUITABSPANEL_TABSPARAMETROS_Autowidth", StringUtil.BoolToStr( Gxuitabspanel_tabsparametros_Autowidth));
         GxWebStd.gx_hidden_field( context, "GXUITABSPANEL_TABSPARAMETROS_Autoheight", StringUtil.BoolToStr( Gxuitabspanel_tabsparametros_Autoheight));
         GxWebStd.gx_hidden_field( context, "GXUITABSPANEL_TABSPARAMETROS_Autoscroll", StringUtil.BoolToStr( Gxuitabspanel_tabsparametros_Autoscroll));
         GxWebStd.gx_hidden_field( context, "GXUITABSPANEL_TABSPARAMETROS_Designtimetabs", StringUtil.RTrim( Gxuitabspanel_tabsparametros_Designtimetabs));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Width", StringUtil.RTrim( Dvpanel_tableattributes_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Cls", StringUtil.RTrim( Dvpanel_tableattributes_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Title", StringUtil.RTrim( Dvpanel_tableattributes_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsible", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsed", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Enabled", StringUtil.BoolToStr( Dvpanel_tableattributes_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autowidth", StringUtil.BoolToStr( Dvpanel_tableattributes_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoheight", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tableattributes_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Iconposition", StringUtil.RTrim( Dvpanel_tableattributes_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoscroll", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoscroll));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "ParametrosSistema";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A334ParametrosSistema_AppID), "ZZZZZZZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         forbiddenHiddens = forbiddenHiddens + StringUtil.BoolToStr( A399ParametrosSistema_LicensiadoCadastrado);
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( A1171ParametrosSistema_SQLComputerName, ""));
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( A1172ParametrosSistema_SQLServerName, ""));
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( A1168ParametrosSistema_SQLEdition, ""));
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( A1169ParametrosSistema_SQLInstance, ""));
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( A1170ParametrosSistema_DBName, ""));
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( A1954ParametrosSistema_Validacao, ""));
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("parametrossistema:[SendSecurityCheck value for]"+"ParametrosSistema_AppID:"+context.localUtil.Format( (decimal)(A334ParametrosSistema_AppID), "ZZZZZZZZZZZ9"));
         GXUtil.WriteLog("parametrossistema:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
         GXUtil.WriteLog("parametrossistema:[SendSecurityCheck value for]"+"ParametrosSistema_LicensiadoCadastrado:"+StringUtil.BoolToStr( A399ParametrosSistema_LicensiadoCadastrado));
         GXUtil.WriteLog("parametrossistema:[SendSecurityCheck value for]"+"ParametrosSistema_SQLComputerName:"+StringUtil.RTrim( context.localUtil.Format( A1171ParametrosSistema_SQLComputerName, "")));
         GXUtil.WriteLog("parametrossistema:[SendSecurityCheck value for]"+"ParametrosSistema_SQLServerName:"+StringUtil.RTrim( context.localUtil.Format( A1172ParametrosSistema_SQLServerName, "")));
         GXUtil.WriteLog("parametrossistema:[SendSecurityCheck value for]"+"ParametrosSistema_SQLEdition:"+StringUtil.RTrim( context.localUtil.Format( A1168ParametrosSistema_SQLEdition, "")));
         GXUtil.WriteLog("parametrossistema:[SendSecurityCheck value for]"+"ParametrosSistema_SQLInstance:"+StringUtil.RTrim( context.localUtil.Format( A1169ParametrosSistema_SQLInstance, "")));
         GXUtil.WriteLog("parametrossistema:[SendSecurityCheck value for]"+"ParametrosSistema_DBName:"+StringUtil.RTrim( context.localUtil.Format( A1170ParametrosSistema_DBName, "")));
         GXUtil.WriteLog("parametrossistema:[SendSecurityCheck value for]"+"ParametrosSistema_Validacao:"+StringUtil.RTrim( context.localUtil.Format( A1954ParametrosSistema_Validacao, "")));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("parametrossistema.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7ParametrosSistema_Codigo) ;
      }

      public override String GetPgmname( )
      {
         return "ParametrosSistema" ;
      }

      public override String GetPgmdesc( )
      {
         return "Parametros do Sistema" ;
      }

      protected void InitializeNonKey1J56( )
      {
         AV14KeyEnc = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14KeyEnc", AV14KeyEnc);
         A1021ParametrosSistema_PathCrtf = "";
         n1021ParametrosSistema_PathCrtf = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1021ParametrosSistema_PathCrtf", A1021ParametrosSistema_PathCrtf);
         n1021ParametrosSistema_PathCrtf = (String.IsNullOrEmpty(StringUtil.RTrim( A1021ParametrosSistema_PathCrtf)) ? true : false);
         A1499ParametrosSistema_FlsEvd = "";
         n1499ParametrosSistema_FlsEvd = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1499ParametrosSistema_FlsEvd", A1499ParametrosSistema_FlsEvd);
         n1499ParametrosSistema_FlsEvd = (String.IsNullOrEmpty(StringUtil.RTrim( A1499ParametrosSistema_FlsEvd)) ? true : false);
         A331ParametrosSistema_NomeSistema = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A331ParametrosSistema_NomeSistema", A331ParametrosSistema_NomeSistema);
         A334ParametrosSistema_AppID = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A334ParametrosSistema_AppID", StringUtil.LTrim( StringUtil.Str( (decimal)(A334ParametrosSistema_AppID), 12, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_PARAMETROSSISTEMA_APPID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A334ParametrosSistema_AppID), "ZZZZZZZZZZZ9")));
         A332ParametrosSistema_TextoHome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A332ParametrosSistema_TextoHome", A332ParametrosSistema_TextoHome);
         A532ParametrosSistema_EmailSdaHost = "";
         n532ParametrosSistema_EmailSdaHost = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A532ParametrosSistema_EmailSdaHost", A532ParametrosSistema_EmailSdaHost);
         n532ParametrosSistema_EmailSdaHost = (String.IsNullOrEmpty(StringUtil.RTrim( A532ParametrosSistema_EmailSdaHost)) ? true : false);
         A533ParametrosSistema_EmailSdaUser = "";
         n533ParametrosSistema_EmailSdaUser = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A533ParametrosSistema_EmailSdaUser", A533ParametrosSistema_EmailSdaUser);
         n533ParametrosSistema_EmailSdaUser = (String.IsNullOrEmpty(StringUtil.RTrim( A533ParametrosSistema_EmailSdaUser)) ? true : false);
         A534ParametrosSistema_EmailSdaPass = "";
         n534ParametrosSistema_EmailSdaPass = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A534ParametrosSistema_EmailSdaPass", A534ParametrosSistema_EmailSdaPass);
         n534ParametrosSistema_EmailSdaPass = (String.IsNullOrEmpty(StringUtil.RTrim( A534ParametrosSistema_EmailSdaPass)) ? true : false);
         A537ParametrosSistema_EmailSdaKey = "";
         n537ParametrosSistema_EmailSdaKey = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A537ParametrosSistema_EmailSdaKey", A537ParametrosSistema_EmailSdaKey);
         A536ParametrosSistema_EmailSdaPort = 0;
         n536ParametrosSistema_EmailSdaPort = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A536ParametrosSistema_EmailSdaPort", StringUtil.LTrim( StringUtil.Str( (decimal)(A536ParametrosSistema_EmailSdaPort), 4, 0)));
         n536ParametrosSistema_EmailSdaPort = ((0==A536ParametrosSistema_EmailSdaPort) ? true : false);
         A2032ParametrosSistema_EmailSdaSec = 0;
         n2032ParametrosSistema_EmailSdaSec = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2032ParametrosSistema_EmailSdaSec", StringUtil.LTrim( StringUtil.Str( (decimal)(A2032ParametrosSistema_EmailSdaSec), 4, 0)));
         n2032ParametrosSistema_EmailSdaSec = ((0==A2032ParametrosSistema_EmailSdaSec) ? true : false);
         A1163ParametrosSistema_HostMensuracao = "";
         n1163ParametrosSistema_HostMensuracao = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1163ParametrosSistema_HostMensuracao", A1163ParametrosSistema_HostMensuracao);
         n1163ParametrosSistema_HostMensuracao = (String.IsNullOrEmpty(StringUtil.RTrim( A1163ParametrosSistema_HostMensuracao)) ? true : false);
         A1171ParametrosSistema_SQLComputerName = "";
         n1171ParametrosSistema_SQLComputerName = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1171ParametrosSistema_SQLComputerName", A1171ParametrosSistema_SQLComputerName);
         A1172ParametrosSistema_SQLServerName = "";
         n1172ParametrosSistema_SQLServerName = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1172ParametrosSistema_SQLServerName", A1172ParametrosSistema_SQLServerName);
         A1168ParametrosSistema_SQLEdition = "";
         n1168ParametrosSistema_SQLEdition = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1168ParametrosSistema_SQLEdition", A1168ParametrosSistema_SQLEdition);
         A1169ParametrosSistema_SQLInstance = "";
         n1169ParametrosSistema_SQLInstance = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1169ParametrosSistema_SQLInstance", A1169ParametrosSistema_SQLInstance);
         A1170ParametrosSistema_DBName = "";
         n1170ParametrosSistema_DBName = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1170ParametrosSistema_DBName", A1170ParametrosSistema_DBName);
         A1679ParametrosSistema_URLApp = "";
         n1679ParametrosSistema_URLApp = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1679ParametrosSistema_URLApp", A1679ParametrosSistema_URLApp);
         n1679ParametrosSistema_URLApp = (String.IsNullOrEmpty(StringUtil.RTrim( A1679ParametrosSistema_URLApp)) ? true : false);
         A1952ParametrosSistema_URLOtherVer = "";
         n1952ParametrosSistema_URLOtherVer = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1952ParametrosSistema_URLOtherVer", A1952ParametrosSistema_URLOtherVer);
         n1952ParametrosSistema_URLOtherVer = (String.IsNullOrEmpty(StringUtil.RTrim( A1952ParametrosSistema_URLOtherVer)) ? true : false);
         A1954ParametrosSistema_Validacao = "";
         n1954ParametrosSistema_Validacao = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1954ParametrosSistema_Validacao", A1954ParametrosSistema_Validacao);
         A2117ParametrosSistema_Imagem = "";
         n2117ParametrosSistema_Imagem = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2117ParametrosSistema_Imagem", A2117ParametrosSistema_Imagem);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgParametrosSistema_Imagem_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( A2117ParametrosSistema_Imagem)) ? A40000ParametrosSistema_Imagem_GXI : context.convertURL( context.PathToRelativeUrl( A2117ParametrosSistema_Imagem))));
         n2117ParametrosSistema_Imagem = (String.IsNullOrEmpty(StringUtil.RTrim( A2117ParametrosSistema_Imagem)) ? true : false);
         A40000ParametrosSistema_Imagem_GXI = "";
         n40000ParametrosSistema_Imagem_GXI = false;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgParametrosSistema_Imagem_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( A2117ParametrosSistema_Imagem)) ? A40000ParametrosSistema_Imagem_GXI : context.convertURL( context.PathToRelativeUrl( A2117ParametrosSistema_Imagem))));
         A399ParametrosSistema_LicensiadoCadastrado = false;
         n399ParametrosSistema_LicensiadoCadastrado = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A399ParametrosSistema_LicensiadoCadastrado", A399ParametrosSistema_LicensiadoCadastrado);
         A417ParametrosSistema_PadronizarStrings = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A417ParametrosSistema_PadronizarStrings", A417ParametrosSistema_PadronizarStrings);
         A535ParametrosSistema_EmailSdaAut = false;
         n535ParametrosSistema_EmailSdaAut = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A535ParametrosSistema_EmailSdaAut", A535ParametrosSistema_EmailSdaAut);
         A708ParametrosSistema_FatorAjuste = (decimal)(1);
         n708ParametrosSistema_FatorAjuste = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A708ParametrosSistema_FatorAjuste", StringUtil.LTrim( StringUtil.Str( A708ParametrosSistema_FatorAjuste, 6, 2)));
         O534ParametrosSistema_EmailSdaPass = A534ParametrosSistema_EmailSdaPass;
         n534ParametrosSistema_EmailSdaPass = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A534ParametrosSistema_EmailSdaPass", A534ParametrosSistema_EmailSdaPass);
         O1499ParametrosSistema_FlsEvd = A1499ParametrosSistema_FlsEvd;
         n1499ParametrosSistema_FlsEvd = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1499ParametrosSistema_FlsEvd", A1499ParametrosSistema_FlsEvd);
         O1021ParametrosSistema_PathCrtf = A1021ParametrosSistema_PathCrtf;
         n1021ParametrosSistema_PathCrtf = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1021ParametrosSistema_PathCrtf", A1021ParametrosSistema_PathCrtf);
         Z1021ParametrosSistema_PathCrtf = "";
         Z1499ParametrosSistema_FlsEvd = "";
         Z331ParametrosSistema_NomeSistema = "";
         Z334ParametrosSistema_AppID = 0;
         Z399ParametrosSistema_LicensiadoCadastrado = false;
         Z417ParametrosSistema_PadronizarStrings = false;
         Z532ParametrosSistema_EmailSdaHost = "";
         Z533ParametrosSistema_EmailSdaUser = "";
         Z534ParametrosSistema_EmailSdaPass = "";
         Z537ParametrosSistema_EmailSdaKey = "";
         Z535ParametrosSistema_EmailSdaAut = false;
         Z536ParametrosSistema_EmailSdaPort = 0;
         Z2032ParametrosSistema_EmailSdaSec = 0;
         Z708ParametrosSistema_FatorAjuste = 0;
         Z1163ParametrosSistema_HostMensuracao = "";
         Z1171ParametrosSistema_SQLComputerName = "";
         Z1172ParametrosSistema_SQLServerName = "";
         Z1168ParametrosSistema_SQLEdition = "";
         Z1169ParametrosSistema_SQLInstance = "";
         Z1170ParametrosSistema_DBName = "";
         Z1679ParametrosSistema_URLApp = "";
         Z1952ParametrosSistema_URLOtherVer = "";
         Z1954ParametrosSistema_Validacao = "";
      }

      protected void InitAll1J56( )
      {
         A330ParametrosSistema_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A330ParametrosSistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A330ParametrosSistema_Codigo), 6, 0)));
         InitializeNonKey1J56( ) ;
      }

      protected void StandaloneModalInsert( )
      {
         A708ParametrosSistema_FatorAjuste = i708ParametrosSistema_FatorAjuste;
         n708ParametrosSistema_FatorAjuste = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A708ParametrosSistema_FatorAjuste", StringUtil.LTrim( StringUtil.Str( A708ParametrosSistema_FatorAjuste, 6, 2)));
         A535ParametrosSistema_EmailSdaAut = i535ParametrosSistema_EmailSdaAut;
         n535ParametrosSistema_EmailSdaAut = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A535ParametrosSistema_EmailSdaAut", A535ParametrosSistema_EmailSdaAut);
         A417ParametrosSistema_PadronizarStrings = i417ParametrosSistema_PadronizarStrings;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A417ParametrosSistema_PadronizarStrings", A417ParametrosSistema_PadronizarStrings);
         A399ParametrosSistema_LicensiadoCadastrado = i399ParametrosSistema_LicensiadoCadastrado;
         n399ParametrosSistema_LicensiadoCadastrado = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A399ParametrosSistema_LicensiadoCadastrado", A399ParametrosSistema_LicensiadoCadastrado);
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202051812491636");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("parametrossistema.js", "?202051812491636");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/TabsPanel/BootstrapTabsPanelRender.js", "");
         context.AddJavascriptSource("CKEditor/ckeditor/ckeditor.js", "");
         context.AddJavascriptSource("CKEditor/CKEditorRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockparametrossistema_nomesistema_Internalname = "TEXTBLOCKPARAMETROSSISTEMA_NOMESISTEMA";
         edtParametrosSistema_NomeSistema_Internalname = "PARAMETROSSISTEMA_NOMESISTEMA";
         lblTextblockparametrossistema_padronizarstrings_Internalname = "TEXTBLOCKPARAMETROSSISTEMA_PADRONIZARSTRINGS";
         cmbParametrosSistema_PadronizarStrings_Internalname = "PARAMETROSSISTEMA_PADRONIZARSTRINGS";
         lblTextblockparametrossistema_fatorajuste_Internalname = "TEXTBLOCKPARAMETROSSISTEMA_FATORAJUSTE";
         edtParametrosSistema_FatorAjuste_Internalname = "PARAMETROSSISTEMA_FATORAJUSTE";
         lblTextblockparametrossistema_pathcrtf_Internalname = "TEXTBLOCKPARAMETROSSISTEMA_PATHCRTF";
         edtParametrosSistema_PathCrtf_Internalname = "PARAMETROSSISTEMA_PATHCRTF";
         bttBtnassinar_Internalname = "BTNASSINAR";
         tblTablemergedparametrossistema_pathcrtf_Internalname = "TABLEMERGEDPARAMETROSSISTEMA_PATHCRTF";
         lblTextblockparametrossistema_flsevd_Internalname = "TEXTBLOCKPARAMETROSSISTEMA_FLSEVD";
         edtParametrosSistema_FlsEvd_Internalname = "PARAMETROSSISTEMA_FLSEVD";
         lblTextblockparametrossistema_emailsdahost_Internalname = "TEXTBLOCKPARAMETROSSISTEMA_EMAILSDAHOST";
         edtParametrosSistema_EmailSdaHost_Internalname = "PARAMETROSSISTEMA_EMAILSDAHOST";
         lblTextblockparametrossistema_emailsdauser_Internalname = "TEXTBLOCKPARAMETROSSISTEMA_EMAILSDAUSER";
         edtParametrosSistema_EmailSdaUser_Internalname = "PARAMETROSSISTEMA_EMAILSDAUSER";
         lblTextblockparametrossistema_emailsdapass_Internalname = "TEXTBLOCKPARAMETROSSISTEMA_EMAILSDAPASS";
         edtParametrosSistema_EmailSdaPass_Internalname = "PARAMETROSSISTEMA_EMAILSDAPASS";
         lblTextblockparametrossistema_emailsdaport_Internalname = "TEXTBLOCKPARAMETROSSISTEMA_EMAILSDAPORT";
         edtParametrosSistema_EmailSdaPort_Internalname = "PARAMETROSSISTEMA_EMAILSDAPORT";
         lblTextblockparametrossistema_emailsdaaut_Internalname = "TEXTBLOCKPARAMETROSSISTEMA_EMAILSDAAUT";
         cmbParametrosSistema_EmailSdaAut_Internalname = "PARAMETROSSISTEMA_EMAILSDAAUT";
         lblTextblockparametrossistema_emailsdasec_Internalname = "TEXTBLOCKPARAMETROSSISTEMA_EMAILSDASEC";
         cmbParametrosSistema_EmailSdaSec_Internalname = "PARAMETROSSISTEMA_EMAILSDASEC";
         bttBtntestaremail_Internalname = "BTNTESTAREMAIL";
         tblEmailsaida_Internalname = "EMAILSAIDA";
         grpUnnamedgroup3_Internalname = "UNNAMEDGROUP3";
         lblTextblockparametrossistema_urlapp_Internalname = "TEXTBLOCKPARAMETROSSISTEMA_URLAPP";
         edtParametrosSistema_URLApp_Internalname = "PARAMETROSSISTEMA_URLAPP";
         lblTextblockparametrossistema_urlotherver_Internalname = "TEXTBLOCKPARAMETROSSISTEMA_URLOTHERVER";
         edtParametrosSistema_URLOtherVer_Internalname = "PARAMETROSSISTEMA_URLOTHERVER";
         lblTextblockparametrossistema_hostmensuracao_Internalname = "TEXTBLOCKPARAMETROSSISTEMA_HOSTMENSURACAO";
         edtParametrosSistema_HostMensuracao_Internalname = "PARAMETROSSISTEMA_HOSTMENSURACAO";
         tblSistema_Internalname = "SISTEMA";
         grpUnnamedgroup4_Internalname = "UNNAMEDGROUP4";
         bttBtnbntimagemlogin_Internalname = "BTNBNTIMAGEMLOGIN";
         lblTextblockparametrossistema_imagem_Internalname = "TEXTBLOCKPARAMETROSSISTEMA_IMAGEM";
         imgParametrosSistema_Imagem_Internalname = "PARAMETROSSISTEMA_IMAGEM";
         tblGrouplogin_Internalname = "GROUPLOGIN";
         grpUnnamedgroup5_Internalname = "UNNAMEDGROUP5";
         tblUnnamedtable2_Internalname = "UNNAMEDTABLE2";
         lblTextblockparametrossistema_textohome_Internalname = "TEXTBLOCKPARAMETROSSISTEMA_TEXTOHOME";
         Parametrossistema_textohome_Internalname = "PARAMETROSSISTEMA_TEXTOHOME";
         tblUnnamedtable1_Internalname = "UNNAMEDTABLE1";
         Gxuitabspanel_tabsparametros_Internalname = "GXUITABSPANEL_TABSPARAMETROS";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         Dvpanel_tableattributes_Internalname = "DVPANEL_TABLEATTRIBUTES";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         bttBtn_trn_delete_Internalname = "BTN_TRN_DELETE";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         edtParametrosSistema_Codigo_Internalname = "PARAMETROSSISTEMA_CODIGO";
         edtParametrosSistema_AppID_Internalname = "PARAMETROSSISTEMA_APPID";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Dvpanel_tableattributes_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Iconposition = "left";
         Dvpanel_tableattributes_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tableattributes_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Title = "Par�metros do Sistema";
         Dvpanel_tableattributes_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tableattributes_Width = "100%";
         Gxuitabspanel_tabsparametros_Designtimetabs = "[{\"id\":\"TabPrincipal\"},{\"id\":\"TabHome\"}]";
         Gxuitabspanel_tabsparametros_Autoscroll = Convert.ToBoolean( -1);
         Gxuitabspanel_tabsparametros_Autoheight = Convert.ToBoolean( -1);
         Gxuitabspanel_tabsparametros_Autowidth = Convert.ToBoolean( 0);
         Gxuitabspanel_tabsparametros_Cls = "GXUI-DVelop-Tabs";
         Gxuitabspanel_tabsparametros_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Parametros do Sistema";
         bttBtnassinar_Visible = 1;
         edtParametrosSistema_PathCrtf_Jsonclick = "";
         edtParametrosSistema_PathCrtf_Enabled = 1;
         bttBtntestaremail_Visible = 1;
         cmbParametrosSistema_EmailSdaSec_Jsonclick = "";
         cmbParametrosSistema_EmailSdaSec.Enabled = 1;
         cmbParametrosSistema_EmailSdaAut_Jsonclick = "";
         cmbParametrosSistema_EmailSdaAut.Enabled = 1;
         edtParametrosSistema_EmailSdaPort_Jsonclick = "";
         edtParametrosSistema_EmailSdaPort_Enabled = 1;
         edtParametrosSistema_EmailSdaPass_Jsonclick = "";
         edtParametrosSistema_EmailSdaPass_Enabled = 1;
         edtParametrosSistema_EmailSdaUser_Jsonclick = "";
         edtParametrosSistema_EmailSdaUser_Enabled = 1;
         edtParametrosSistema_EmailSdaHost_Jsonclick = "";
         edtParametrosSistema_EmailSdaHost_Enabled = 1;
         edtParametrosSistema_HostMensuracao_Jsonclick = "";
         edtParametrosSistema_HostMensuracao_Enabled = 1;
         edtParametrosSistema_URLOtherVer_Jsonclick = "";
         edtParametrosSistema_URLOtherVer_Enabled = 1;
         edtParametrosSistema_URLApp_Jsonclick = "";
         edtParametrosSistema_URLApp_Enabled = 1;
         imgParametrosSistema_Imagem_Enabled = 1;
         bttBtnbntimagemlogin_Visible = 1;
         edtParametrosSistema_FlsEvd_Jsonclick = "";
         edtParametrosSistema_FlsEvd_Enabled = 1;
         edtParametrosSistema_FatorAjuste_Jsonclick = "";
         edtParametrosSistema_FatorAjuste_Enabled = 1;
         cmbParametrosSistema_PadronizarStrings_Jsonclick = "";
         cmbParametrosSistema_PadronizarStrings.Enabled = 1;
         edtParametrosSistema_NomeSistema_Jsonclick = "";
         edtParametrosSistema_NomeSistema_Enabled = 1;
         Parametrossistema_textohome_Enabled = Convert.ToBoolean( 1);
         bttBtn_trn_delete_Enabled = 0;
         bttBtn_trn_delete_Visible = 1;
         bttBtn_trn_cancel_Visible = 1;
         bttBtn_trn_enter_Enabled = 1;
         bttBtn_trn_enter_Visible = 1;
         edtParametrosSistema_AppID_Jsonclick = "";
         edtParametrosSistema_AppID_Enabled = 0;
         edtParametrosSistema_AppID_Visible = 1;
         edtParametrosSistema_Codigo_Jsonclick = "";
         edtParametrosSistema_Codigo_Enabled = 1;
         edtParametrosSistema_Codigo_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV7ParametrosSistema_Codigo',fld:'vPARAMETROSSISTEMA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E131J2',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV9TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null}],oparms:[]}");
         setEventMetadata("'DOASSINAR'","{handler:'E111J56',iparms:[{av:'A1021ParametrosSistema_PathCrtf',fld:'PARAMETROSSISTEMA_PATHCRTF',pic:'',nv:''}],oparms:[{av:'A1021ParametrosSistema_PathCrtf',fld:'PARAMETROSSISTEMA_PATHCRTF',pic:'',nv:''}]}");
         setEventMetadata("'DOBNTIMAGEMLOGIN'","{handler:'E141J2',iparms:[],oparms:[]}");
         setEventMetadata("'DOTESTAREMAIL'","{handler:'E151J2',iparms:[{av:'A533ParametrosSistema_EmailSdaUser',fld:'PARAMETROSSISTEMA_EMAILSDAUSER',pic:'',nv:''},{av:'A532ParametrosSistema_EmailSdaHost',fld:'PARAMETROSSISTEMA_EMAILSDAHOST',pic:'',nv:''},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'A534ParametrosSistema_EmailSdaPass',fld:'PARAMETROSSISTEMA_EMAILSDAPASS',pic:'',nv:''},{av:'A537ParametrosSistema_EmailSdaKey',fld:'PARAMETROSSISTEMA_EMAILSDAKEY',pic:'',nv:''},{av:'A536ParametrosSistema_EmailSdaPort',fld:'PARAMETROSSISTEMA_EMAILSDAPORT',pic:'ZZZ9',nv:0},{av:'A535ParametrosSistema_EmailSdaAut',fld:'PARAMETROSSISTEMA_EMAILSDAAUT',pic:'',nv:false},{av:'A2032ParametrosSistema_EmailSdaSec',fld:'PARAMETROSSISTEMA_EMAILSDASEC',pic:'ZZZ9',nv:0}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         Z1021ParametrosSistema_PathCrtf = "";
         Z1499ParametrosSistema_FlsEvd = "";
         Z331ParametrosSistema_NomeSistema = "";
         Z532ParametrosSistema_EmailSdaHost = "";
         Z533ParametrosSistema_EmailSdaUser = "";
         Z534ParametrosSistema_EmailSdaPass = "";
         Z537ParametrosSistema_EmailSdaKey = "";
         Z1163ParametrosSistema_HostMensuracao = "";
         Z1171ParametrosSistema_SQLComputerName = "";
         Z1172ParametrosSistema_SQLServerName = "";
         Z1168ParametrosSistema_SQLEdition = "";
         Z1169ParametrosSistema_SQLInstance = "";
         Z1170ParametrosSistema_DBName = "";
         Z1679ParametrosSistema_URLApp = "";
         Z1952ParametrosSistema_URLOtherVer = "";
         Z1954ParametrosSistema_Validacao = "";
         O534ParametrosSistema_EmailSdaPass = "";
         O1499ParametrosSistema_FlsEvd = "";
         O1021ParametrosSistema_PathCrtf = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         TempTags = "";
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         bttBtn_trn_delete_Jsonclick = "";
         lblTextblockparametrossistema_textohome_Jsonclick = "";
         lblTextblockparametrossistema_nomesistema_Jsonclick = "";
         A331ParametrosSistema_NomeSistema = "";
         lblTextblockparametrossistema_padronizarstrings_Jsonclick = "";
         lblTextblockparametrossistema_fatorajuste_Jsonclick = "";
         lblTextblockparametrossistema_pathcrtf_Jsonclick = "";
         lblTextblockparametrossistema_flsevd_Jsonclick = "";
         A1499ParametrosSistema_FlsEvd = "";
         bttBtnbntimagemlogin_Jsonclick = "";
         lblTextblockparametrossistema_imagem_Jsonclick = "";
         A2117ParametrosSistema_Imagem = "";
         A40000ParametrosSistema_Imagem_GXI = "";
         lblTextblockparametrossistema_urlapp_Jsonclick = "";
         A1679ParametrosSistema_URLApp = "";
         lblTextblockparametrossistema_urlotherver_Jsonclick = "";
         A1952ParametrosSistema_URLOtherVer = "";
         lblTextblockparametrossistema_hostmensuracao_Jsonclick = "";
         A1163ParametrosSistema_HostMensuracao = "";
         lblTextblockparametrossistema_emailsdahost_Jsonclick = "";
         A532ParametrosSistema_EmailSdaHost = "";
         lblTextblockparametrossistema_emailsdauser_Jsonclick = "";
         A533ParametrosSistema_EmailSdaUser = "";
         lblTextblockparametrossistema_emailsdapass_Jsonclick = "";
         A534ParametrosSistema_EmailSdaPass = "";
         lblTextblockparametrossistema_emailsdaport_Jsonclick = "";
         lblTextblockparametrossistema_emailsdaaut_Jsonclick = "";
         lblTextblockparametrossistema_emailsdasec_Jsonclick = "";
         bttBtntestaremail_Jsonclick = "";
         A1021ParametrosSistema_PathCrtf = "";
         bttBtnassinar_Jsonclick = "";
         A537ParametrosSistema_EmailSdaKey = "";
         A1171ParametrosSistema_SQLComputerName = "";
         A1172ParametrosSistema_SQLServerName = "";
         A1168ParametrosSistema_SQLEdition = "";
         A1169ParametrosSistema_SQLInstance = "";
         A1170ParametrosSistema_DBName = "";
         A1954ParametrosSistema_Validacao = "";
         AV14KeyEnc = "";
         A332ParametrosSistema_TextoHome = "";
         Parametrossistema_textohome_Width = "";
         Parametrossistema_textohome_Height = "";
         Parametrossistema_textohome_Skin = "";
         Parametrossistema_textohome_Toolbar = "";
         Parametrossistema_textohome_Class = "";
         Parametrossistema_textohome_Customtoolbar = "";
         Parametrossistema_textohome_Customconfiguration = "";
         Parametrossistema_textohome_Buttonpressedid = "";
         Parametrossistema_textohome_Captionvalue = "";
         Parametrossistema_textohome_Captionclass = "";
         Parametrossistema_textohome_Captionposition = "";
         Parametrossistema_textohome_Coltitle = "";
         Parametrossistema_textohome_Coltitlefont = "";
         Gxuitabspanel_tabsparametros_Height = "";
         Gxuitabspanel_tabsparametros_Class = "";
         Gxuitabspanel_tabsparametros_Activetabid = "";
         Dvpanel_tableattributes_Height = "";
         Dvpanel_tableattributes_Class = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode56 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         cmdBuffer = "";
         AV19ParametrosSistema_Imagem = "";
         AV12MailRecipient = new GeneXus.Mail.GXMailRecipient();
         AV11Email = new GeneXus.Mail.GXMailMessage();
         AV13SMTPSession = new GeneXus.Mail.GXSMTPSession(context.GetPhysicalPath());
         Z332ParametrosSistema_TextoHome = "";
         Z2117ParametrosSistema_Imagem = "";
         Z40000ParametrosSistema_Imagem_GXI = "";
         T001J4_A330ParametrosSistema_Codigo = new int[1] ;
         T001J4_A1021ParametrosSistema_PathCrtf = new String[] {""} ;
         T001J4_n1021ParametrosSistema_PathCrtf = new bool[] {false} ;
         T001J4_A1499ParametrosSistema_FlsEvd = new String[] {""} ;
         T001J4_n1499ParametrosSistema_FlsEvd = new bool[] {false} ;
         T001J4_A331ParametrosSistema_NomeSistema = new String[] {""} ;
         T001J4_A334ParametrosSistema_AppID = new long[1] ;
         T001J4_A332ParametrosSistema_TextoHome = new String[] {""} ;
         T001J4_A399ParametrosSistema_LicensiadoCadastrado = new bool[] {false} ;
         T001J4_n399ParametrosSistema_LicensiadoCadastrado = new bool[] {false} ;
         T001J4_A417ParametrosSistema_PadronizarStrings = new bool[] {false} ;
         T001J4_A532ParametrosSistema_EmailSdaHost = new String[] {""} ;
         T001J4_n532ParametrosSistema_EmailSdaHost = new bool[] {false} ;
         T001J4_A533ParametrosSistema_EmailSdaUser = new String[] {""} ;
         T001J4_n533ParametrosSistema_EmailSdaUser = new bool[] {false} ;
         T001J4_A534ParametrosSistema_EmailSdaPass = new String[] {""} ;
         T001J4_n534ParametrosSistema_EmailSdaPass = new bool[] {false} ;
         T001J4_A537ParametrosSistema_EmailSdaKey = new String[] {""} ;
         T001J4_n537ParametrosSistema_EmailSdaKey = new bool[] {false} ;
         T001J4_A535ParametrosSistema_EmailSdaAut = new bool[] {false} ;
         T001J4_n535ParametrosSistema_EmailSdaAut = new bool[] {false} ;
         T001J4_A536ParametrosSistema_EmailSdaPort = new short[1] ;
         T001J4_n536ParametrosSistema_EmailSdaPort = new bool[] {false} ;
         T001J4_A2032ParametrosSistema_EmailSdaSec = new short[1] ;
         T001J4_n2032ParametrosSistema_EmailSdaSec = new bool[] {false} ;
         T001J4_A708ParametrosSistema_FatorAjuste = new decimal[1] ;
         T001J4_n708ParametrosSistema_FatorAjuste = new bool[] {false} ;
         T001J4_A1163ParametrosSistema_HostMensuracao = new String[] {""} ;
         T001J4_n1163ParametrosSistema_HostMensuracao = new bool[] {false} ;
         T001J4_A1171ParametrosSistema_SQLComputerName = new String[] {""} ;
         T001J4_n1171ParametrosSistema_SQLComputerName = new bool[] {false} ;
         T001J4_A1172ParametrosSistema_SQLServerName = new String[] {""} ;
         T001J4_n1172ParametrosSistema_SQLServerName = new bool[] {false} ;
         T001J4_A1168ParametrosSistema_SQLEdition = new String[] {""} ;
         T001J4_n1168ParametrosSistema_SQLEdition = new bool[] {false} ;
         T001J4_A1169ParametrosSistema_SQLInstance = new String[] {""} ;
         T001J4_n1169ParametrosSistema_SQLInstance = new bool[] {false} ;
         T001J4_A1170ParametrosSistema_DBName = new String[] {""} ;
         T001J4_n1170ParametrosSistema_DBName = new bool[] {false} ;
         T001J4_A1679ParametrosSistema_URLApp = new String[] {""} ;
         T001J4_n1679ParametrosSistema_URLApp = new bool[] {false} ;
         T001J4_A1952ParametrosSistema_URLOtherVer = new String[] {""} ;
         T001J4_n1952ParametrosSistema_URLOtherVer = new bool[] {false} ;
         T001J4_A1954ParametrosSistema_Validacao = new String[] {""} ;
         T001J4_n1954ParametrosSistema_Validacao = new bool[] {false} ;
         T001J4_A40000ParametrosSistema_Imagem_GXI = new String[] {""} ;
         T001J4_n40000ParametrosSistema_Imagem_GXI = new bool[] {false} ;
         T001J4_A2117ParametrosSistema_Imagem = new String[] {""} ;
         T001J4_n2117ParametrosSistema_Imagem = new bool[] {false} ;
         T001J5_A330ParametrosSistema_Codigo = new int[1] ;
         T001J3_A330ParametrosSistema_Codigo = new int[1] ;
         T001J3_A1021ParametrosSistema_PathCrtf = new String[] {""} ;
         T001J3_n1021ParametrosSistema_PathCrtf = new bool[] {false} ;
         T001J3_A1499ParametrosSistema_FlsEvd = new String[] {""} ;
         T001J3_n1499ParametrosSistema_FlsEvd = new bool[] {false} ;
         T001J3_A331ParametrosSistema_NomeSistema = new String[] {""} ;
         T001J3_A334ParametrosSistema_AppID = new long[1] ;
         T001J3_A332ParametrosSistema_TextoHome = new String[] {""} ;
         T001J3_A399ParametrosSistema_LicensiadoCadastrado = new bool[] {false} ;
         T001J3_n399ParametrosSistema_LicensiadoCadastrado = new bool[] {false} ;
         T001J3_A417ParametrosSistema_PadronizarStrings = new bool[] {false} ;
         T001J3_A532ParametrosSistema_EmailSdaHost = new String[] {""} ;
         T001J3_n532ParametrosSistema_EmailSdaHost = new bool[] {false} ;
         T001J3_A533ParametrosSistema_EmailSdaUser = new String[] {""} ;
         T001J3_n533ParametrosSistema_EmailSdaUser = new bool[] {false} ;
         T001J3_A534ParametrosSistema_EmailSdaPass = new String[] {""} ;
         T001J3_n534ParametrosSistema_EmailSdaPass = new bool[] {false} ;
         T001J3_A537ParametrosSistema_EmailSdaKey = new String[] {""} ;
         T001J3_n537ParametrosSistema_EmailSdaKey = new bool[] {false} ;
         T001J3_A535ParametrosSistema_EmailSdaAut = new bool[] {false} ;
         T001J3_n535ParametrosSistema_EmailSdaAut = new bool[] {false} ;
         T001J3_A536ParametrosSistema_EmailSdaPort = new short[1] ;
         T001J3_n536ParametrosSistema_EmailSdaPort = new bool[] {false} ;
         T001J3_A2032ParametrosSistema_EmailSdaSec = new short[1] ;
         T001J3_n2032ParametrosSistema_EmailSdaSec = new bool[] {false} ;
         T001J3_A708ParametrosSistema_FatorAjuste = new decimal[1] ;
         T001J3_n708ParametrosSistema_FatorAjuste = new bool[] {false} ;
         T001J3_A1163ParametrosSistema_HostMensuracao = new String[] {""} ;
         T001J3_n1163ParametrosSistema_HostMensuracao = new bool[] {false} ;
         T001J3_A1171ParametrosSistema_SQLComputerName = new String[] {""} ;
         T001J3_n1171ParametrosSistema_SQLComputerName = new bool[] {false} ;
         T001J3_A1172ParametrosSistema_SQLServerName = new String[] {""} ;
         T001J3_n1172ParametrosSistema_SQLServerName = new bool[] {false} ;
         T001J3_A1168ParametrosSistema_SQLEdition = new String[] {""} ;
         T001J3_n1168ParametrosSistema_SQLEdition = new bool[] {false} ;
         T001J3_A1169ParametrosSistema_SQLInstance = new String[] {""} ;
         T001J3_n1169ParametrosSistema_SQLInstance = new bool[] {false} ;
         T001J3_A1170ParametrosSistema_DBName = new String[] {""} ;
         T001J3_n1170ParametrosSistema_DBName = new bool[] {false} ;
         T001J3_A1679ParametrosSistema_URLApp = new String[] {""} ;
         T001J3_n1679ParametrosSistema_URLApp = new bool[] {false} ;
         T001J3_A1952ParametrosSistema_URLOtherVer = new String[] {""} ;
         T001J3_n1952ParametrosSistema_URLOtherVer = new bool[] {false} ;
         T001J3_A1954ParametrosSistema_Validacao = new String[] {""} ;
         T001J3_n1954ParametrosSistema_Validacao = new bool[] {false} ;
         T001J3_A40000ParametrosSistema_Imagem_GXI = new String[] {""} ;
         T001J3_n40000ParametrosSistema_Imagem_GXI = new bool[] {false} ;
         T001J3_A2117ParametrosSistema_Imagem = new String[] {""} ;
         T001J3_n2117ParametrosSistema_Imagem = new bool[] {false} ;
         T001J6_A330ParametrosSistema_Codigo = new int[1] ;
         T001J7_A330ParametrosSistema_Codigo = new int[1] ;
         T001J2_A330ParametrosSistema_Codigo = new int[1] ;
         T001J2_A1021ParametrosSistema_PathCrtf = new String[] {""} ;
         T001J2_n1021ParametrosSistema_PathCrtf = new bool[] {false} ;
         T001J2_A1499ParametrosSistema_FlsEvd = new String[] {""} ;
         T001J2_n1499ParametrosSistema_FlsEvd = new bool[] {false} ;
         T001J2_A331ParametrosSistema_NomeSistema = new String[] {""} ;
         T001J2_A334ParametrosSistema_AppID = new long[1] ;
         T001J2_A332ParametrosSistema_TextoHome = new String[] {""} ;
         T001J2_A399ParametrosSistema_LicensiadoCadastrado = new bool[] {false} ;
         T001J2_n399ParametrosSistema_LicensiadoCadastrado = new bool[] {false} ;
         T001J2_A417ParametrosSistema_PadronizarStrings = new bool[] {false} ;
         T001J2_A532ParametrosSistema_EmailSdaHost = new String[] {""} ;
         T001J2_n532ParametrosSistema_EmailSdaHost = new bool[] {false} ;
         T001J2_A533ParametrosSistema_EmailSdaUser = new String[] {""} ;
         T001J2_n533ParametrosSistema_EmailSdaUser = new bool[] {false} ;
         T001J2_A534ParametrosSistema_EmailSdaPass = new String[] {""} ;
         T001J2_n534ParametrosSistema_EmailSdaPass = new bool[] {false} ;
         T001J2_A537ParametrosSistema_EmailSdaKey = new String[] {""} ;
         T001J2_n537ParametrosSistema_EmailSdaKey = new bool[] {false} ;
         T001J2_A535ParametrosSistema_EmailSdaAut = new bool[] {false} ;
         T001J2_n535ParametrosSistema_EmailSdaAut = new bool[] {false} ;
         T001J2_A536ParametrosSistema_EmailSdaPort = new short[1] ;
         T001J2_n536ParametrosSistema_EmailSdaPort = new bool[] {false} ;
         T001J2_A2032ParametrosSistema_EmailSdaSec = new short[1] ;
         T001J2_n2032ParametrosSistema_EmailSdaSec = new bool[] {false} ;
         T001J2_A708ParametrosSistema_FatorAjuste = new decimal[1] ;
         T001J2_n708ParametrosSistema_FatorAjuste = new bool[] {false} ;
         T001J2_A1163ParametrosSistema_HostMensuracao = new String[] {""} ;
         T001J2_n1163ParametrosSistema_HostMensuracao = new bool[] {false} ;
         T001J2_A1171ParametrosSistema_SQLComputerName = new String[] {""} ;
         T001J2_n1171ParametrosSistema_SQLComputerName = new bool[] {false} ;
         T001J2_A1172ParametrosSistema_SQLServerName = new String[] {""} ;
         T001J2_n1172ParametrosSistema_SQLServerName = new bool[] {false} ;
         T001J2_A1168ParametrosSistema_SQLEdition = new String[] {""} ;
         T001J2_n1168ParametrosSistema_SQLEdition = new bool[] {false} ;
         T001J2_A1169ParametrosSistema_SQLInstance = new String[] {""} ;
         T001J2_n1169ParametrosSistema_SQLInstance = new bool[] {false} ;
         T001J2_A1170ParametrosSistema_DBName = new String[] {""} ;
         T001J2_n1170ParametrosSistema_DBName = new bool[] {false} ;
         T001J2_A1679ParametrosSistema_URLApp = new String[] {""} ;
         T001J2_n1679ParametrosSistema_URLApp = new bool[] {false} ;
         T001J2_A1952ParametrosSistema_URLOtherVer = new String[] {""} ;
         T001J2_n1952ParametrosSistema_URLOtherVer = new bool[] {false} ;
         T001J2_A1954ParametrosSistema_Validacao = new String[] {""} ;
         T001J2_n1954ParametrosSistema_Validacao = new bool[] {false} ;
         T001J2_A40000ParametrosSistema_Imagem_GXI = new String[] {""} ;
         T001J2_n40000ParametrosSistema_Imagem_GXI = new bool[] {false} ;
         T001J2_A2117ParametrosSistema_Imagem = new String[] {""} ;
         T001J2_n2117ParametrosSistema_Imagem = new bool[] {false} ;
         T001J12_A330ParametrosSistema_Codigo = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GXCCtlgxBlob = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.parametrossistema__default(),
            new Object[][] {
                new Object[] {
               T001J2_A330ParametrosSistema_Codigo, T001J2_A1021ParametrosSistema_PathCrtf, T001J2_n1021ParametrosSistema_PathCrtf, T001J2_A1499ParametrosSistema_FlsEvd, T001J2_n1499ParametrosSistema_FlsEvd, T001J2_A331ParametrosSistema_NomeSistema, T001J2_A334ParametrosSistema_AppID, T001J2_A332ParametrosSistema_TextoHome, T001J2_A399ParametrosSistema_LicensiadoCadastrado, T001J2_n399ParametrosSistema_LicensiadoCadastrado,
               T001J2_A417ParametrosSistema_PadronizarStrings, T001J2_A532ParametrosSistema_EmailSdaHost, T001J2_n532ParametrosSistema_EmailSdaHost, T001J2_A533ParametrosSistema_EmailSdaUser, T001J2_n533ParametrosSistema_EmailSdaUser, T001J2_A534ParametrosSistema_EmailSdaPass, T001J2_n534ParametrosSistema_EmailSdaPass, T001J2_A537ParametrosSistema_EmailSdaKey, T001J2_n537ParametrosSistema_EmailSdaKey, T001J2_A535ParametrosSistema_EmailSdaAut,
               T001J2_n535ParametrosSistema_EmailSdaAut, T001J2_A536ParametrosSistema_EmailSdaPort, T001J2_n536ParametrosSistema_EmailSdaPort, T001J2_A2032ParametrosSistema_EmailSdaSec, T001J2_n2032ParametrosSistema_EmailSdaSec, T001J2_A708ParametrosSistema_FatorAjuste, T001J2_n708ParametrosSistema_FatorAjuste, T001J2_A1163ParametrosSistema_HostMensuracao, T001J2_n1163ParametrosSistema_HostMensuracao, T001J2_A1171ParametrosSistema_SQLComputerName,
               T001J2_n1171ParametrosSistema_SQLComputerName, T001J2_A1172ParametrosSistema_SQLServerName, T001J2_n1172ParametrosSistema_SQLServerName, T001J2_A1168ParametrosSistema_SQLEdition, T001J2_n1168ParametrosSistema_SQLEdition, T001J2_A1169ParametrosSistema_SQLInstance, T001J2_n1169ParametrosSistema_SQLInstance, T001J2_A1170ParametrosSistema_DBName, T001J2_n1170ParametrosSistema_DBName, T001J2_A1679ParametrosSistema_URLApp,
               T001J2_n1679ParametrosSistema_URLApp, T001J2_A1952ParametrosSistema_URLOtherVer, T001J2_n1952ParametrosSistema_URLOtherVer, T001J2_A1954ParametrosSistema_Validacao, T001J2_n1954ParametrosSistema_Validacao, T001J2_A40000ParametrosSistema_Imagem_GXI, T001J2_n40000ParametrosSistema_Imagem_GXI, T001J2_A2117ParametrosSistema_Imagem, T001J2_n2117ParametrosSistema_Imagem
               }
               , new Object[] {
               T001J3_A330ParametrosSistema_Codigo, T001J3_A1021ParametrosSistema_PathCrtf, T001J3_n1021ParametrosSistema_PathCrtf, T001J3_A1499ParametrosSistema_FlsEvd, T001J3_n1499ParametrosSistema_FlsEvd, T001J3_A331ParametrosSistema_NomeSistema, T001J3_A334ParametrosSistema_AppID, T001J3_A332ParametrosSistema_TextoHome, T001J3_A399ParametrosSistema_LicensiadoCadastrado, T001J3_n399ParametrosSistema_LicensiadoCadastrado,
               T001J3_A417ParametrosSistema_PadronizarStrings, T001J3_A532ParametrosSistema_EmailSdaHost, T001J3_n532ParametrosSistema_EmailSdaHost, T001J3_A533ParametrosSistema_EmailSdaUser, T001J3_n533ParametrosSistema_EmailSdaUser, T001J3_A534ParametrosSistema_EmailSdaPass, T001J3_n534ParametrosSistema_EmailSdaPass, T001J3_A537ParametrosSistema_EmailSdaKey, T001J3_n537ParametrosSistema_EmailSdaKey, T001J3_A535ParametrosSistema_EmailSdaAut,
               T001J3_n535ParametrosSistema_EmailSdaAut, T001J3_A536ParametrosSistema_EmailSdaPort, T001J3_n536ParametrosSistema_EmailSdaPort, T001J3_A2032ParametrosSistema_EmailSdaSec, T001J3_n2032ParametrosSistema_EmailSdaSec, T001J3_A708ParametrosSistema_FatorAjuste, T001J3_n708ParametrosSistema_FatorAjuste, T001J3_A1163ParametrosSistema_HostMensuracao, T001J3_n1163ParametrosSistema_HostMensuracao, T001J3_A1171ParametrosSistema_SQLComputerName,
               T001J3_n1171ParametrosSistema_SQLComputerName, T001J3_A1172ParametrosSistema_SQLServerName, T001J3_n1172ParametrosSistema_SQLServerName, T001J3_A1168ParametrosSistema_SQLEdition, T001J3_n1168ParametrosSistema_SQLEdition, T001J3_A1169ParametrosSistema_SQLInstance, T001J3_n1169ParametrosSistema_SQLInstance, T001J3_A1170ParametrosSistema_DBName, T001J3_n1170ParametrosSistema_DBName, T001J3_A1679ParametrosSistema_URLApp,
               T001J3_n1679ParametrosSistema_URLApp, T001J3_A1952ParametrosSistema_URLOtherVer, T001J3_n1952ParametrosSistema_URLOtherVer, T001J3_A1954ParametrosSistema_Validacao, T001J3_n1954ParametrosSistema_Validacao, T001J3_A40000ParametrosSistema_Imagem_GXI, T001J3_n40000ParametrosSistema_Imagem_GXI, T001J3_A2117ParametrosSistema_Imagem, T001J3_n2117ParametrosSistema_Imagem
               }
               , new Object[] {
               T001J4_A330ParametrosSistema_Codigo, T001J4_A1021ParametrosSistema_PathCrtf, T001J4_n1021ParametrosSistema_PathCrtf, T001J4_A1499ParametrosSistema_FlsEvd, T001J4_n1499ParametrosSistema_FlsEvd, T001J4_A331ParametrosSistema_NomeSistema, T001J4_A334ParametrosSistema_AppID, T001J4_A332ParametrosSistema_TextoHome, T001J4_A399ParametrosSistema_LicensiadoCadastrado, T001J4_n399ParametrosSistema_LicensiadoCadastrado,
               T001J4_A417ParametrosSistema_PadronizarStrings, T001J4_A532ParametrosSistema_EmailSdaHost, T001J4_n532ParametrosSistema_EmailSdaHost, T001J4_A533ParametrosSistema_EmailSdaUser, T001J4_n533ParametrosSistema_EmailSdaUser, T001J4_A534ParametrosSistema_EmailSdaPass, T001J4_n534ParametrosSistema_EmailSdaPass, T001J4_A537ParametrosSistema_EmailSdaKey, T001J4_n537ParametrosSistema_EmailSdaKey, T001J4_A535ParametrosSistema_EmailSdaAut,
               T001J4_n535ParametrosSistema_EmailSdaAut, T001J4_A536ParametrosSistema_EmailSdaPort, T001J4_n536ParametrosSistema_EmailSdaPort, T001J4_A2032ParametrosSistema_EmailSdaSec, T001J4_n2032ParametrosSistema_EmailSdaSec, T001J4_A708ParametrosSistema_FatorAjuste, T001J4_n708ParametrosSistema_FatorAjuste, T001J4_A1163ParametrosSistema_HostMensuracao, T001J4_n1163ParametrosSistema_HostMensuracao, T001J4_A1171ParametrosSistema_SQLComputerName,
               T001J4_n1171ParametrosSistema_SQLComputerName, T001J4_A1172ParametrosSistema_SQLServerName, T001J4_n1172ParametrosSistema_SQLServerName, T001J4_A1168ParametrosSistema_SQLEdition, T001J4_n1168ParametrosSistema_SQLEdition, T001J4_A1169ParametrosSistema_SQLInstance, T001J4_n1169ParametrosSistema_SQLInstance, T001J4_A1170ParametrosSistema_DBName, T001J4_n1170ParametrosSistema_DBName, T001J4_A1679ParametrosSistema_URLApp,
               T001J4_n1679ParametrosSistema_URLApp, T001J4_A1952ParametrosSistema_URLOtherVer, T001J4_n1952ParametrosSistema_URLOtherVer, T001J4_A1954ParametrosSistema_Validacao, T001J4_n1954ParametrosSistema_Validacao, T001J4_A40000ParametrosSistema_Imagem_GXI, T001J4_n40000ParametrosSistema_Imagem_GXI, T001J4_A2117ParametrosSistema_Imagem, T001J4_n2117ParametrosSistema_Imagem
               }
               , new Object[] {
               T001J5_A330ParametrosSistema_Codigo
               }
               , new Object[] {
               T001J6_A330ParametrosSistema_Codigo
               }
               , new Object[] {
               T001J7_A330ParametrosSistema_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T001J12_A330ParametrosSistema_Codigo
               }
            }
         );
         Z708ParametrosSistema_FatorAjuste = (decimal)(1);
         n708ParametrosSistema_FatorAjuste = false;
         A708ParametrosSistema_FatorAjuste = (decimal)(1);
         n708ParametrosSistema_FatorAjuste = false;
         i708ParametrosSistema_FatorAjuste = (decimal)(1);
         n708ParametrosSistema_FatorAjuste = false;
         Z535ParametrosSistema_EmailSdaAut = false;
         n535ParametrosSistema_EmailSdaAut = false;
         A535ParametrosSistema_EmailSdaAut = false;
         n535ParametrosSistema_EmailSdaAut = false;
         i535ParametrosSistema_EmailSdaAut = false;
         n535ParametrosSistema_EmailSdaAut = false;
         Z417ParametrosSistema_PadronizarStrings = true;
         A417ParametrosSistema_PadronizarStrings = true;
         i417ParametrosSistema_PadronizarStrings = true;
         Z399ParametrosSistema_LicensiadoCadastrado = false;
         n399ParametrosSistema_LicensiadoCadastrado = false;
         A399ParametrosSistema_LicensiadoCadastrado = false;
         n399ParametrosSistema_LicensiadoCadastrado = false;
         i399ParametrosSistema_LicensiadoCadastrado = false;
         n399ParametrosSistema_LicensiadoCadastrado = false;
      }

      private short Z536ParametrosSistema_EmailSdaPort ;
      private short Z2032ParametrosSistema_EmailSdaSec ;
      private short GxWebError ;
      private short gxcookieaux ;
      private short A2032ParametrosSistema_EmailSdaSec ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short A536ParametrosSistema_EmailSdaPort ;
      private short Gx_BScreen ;
      private short RcdFound56 ;
      private short GX_JID ;
      private short gxajaxcallmode ;
      private int wcpOAV7ParametrosSistema_Codigo ;
      private int Z330ParametrosSistema_Codigo ;
      private int AV7ParametrosSistema_Codigo ;
      private int trnEnded ;
      private int A330ParametrosSistema_Codigo ;
      private int edtParametrosSistema_Codigo_Visible ;
      private int edtParametrosSistema_Codigo_Enabled ;
      private int edtParametrosSistema_AppID_Enabled ;
      private int edtParametrosSistema_AppID_Visible ;
      private int bttBtn_trn_enter_Visible ;
      private int bttBtn_trn_enter_Enabled ;
      private int bttBtn_trn_cancel_Visible ;
      private int bttBtn_trn_delete_Visible ;
      private int bttBtn_trn_delete_Enabled ;
      private int edtParametrosSistema_NomeSistema_Enabled ;
      private int edtParametrosSistema_FatorAjuste_Enabled ;
      private int edtParametrosSistema_FlsEvd_Enabled ;
      private int bttBtnbntimagemlogin_Visible ;
      private int imgParametrosSistema_Imagem_Enabled ;
      private int edtParametrosSistema_URLApp_Enabled ;
      private int edtParametrosSistema_URLOtherVer_Enabled ;
      private int edtParametrosSistema_HostMensuracao_Enabled ;
      private int edtParametrosSistema_EmailSdaHost_Enabled ;
      private int edtParametrosSistema_EmailSdaUser_Enabled ;
      private int edtParametrosSistema_EmailSdaPass_Enabled ;
      private int edtParametrosSistema_EmailSdaPort_Enabled ;
      private int bttBtntestaremail_Visible ;
      private int edtParametrosSistema_PathCrtf_Enabled ;
      private int bttBtnassinar_Visible ;
      private int Parametrossistema_textohome_Color ;
      private int Parametrossistema_textohome_Coltitlecolor ;
      private int Gxuitabspanel_tabsparametros_Selectedtabindex ;
      private int idxLst ;
      private long Z334ParametrosSistema_AppID ;
      private long A334ParametrosSistema_AppID ;
      private decimal Z708ParametrosSistema_FatorAjuste ;
      private decimal A708ParametrosSistema_FatorAjuste ;
      private decimal i708ParametrosSistema_FatorAjuste ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String Z331ParametrosSistema_NomeSistema ;
      private String Z537ParametrosSistema_EmailSdaKey ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String Gx_mode ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtParametrosSistema_NomeSistema_Internalname ;
      private String TempTags ;
      private String edtParametrosSistema_Codigo_Internalname ;
      private String edtParametrosSistema_Codigo_Jsonclick ;
      private String edtParametrosSistema_AppID_Internalname ;
      private String edtParametrosSistema_AppID_Jsonclick ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTableactions_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String bttBtn_trn_delete_Internalname ;
      private String bttBtn_trn_delete_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblTableattributes_Internalname ;
      private String tblUnnamedtable1_Internalname ;
      private String lblTextblockparametrossistema_textohome_Internalname ;
      private String lblTextblockparametrossistema_textohome_Jsonclick ;
      private String tblUnnamedtable2_Internalname ;
      private String lblTextblockparametrossistema_nomesistema_Internalname ;
      private String lblTextblockparametrossistema_nomesistema_Jsonclick ;
      private String A331ParametrosSistema_NomeSistema ;
      private String edtParametrosSistema_NomeSistema_Jsonclick ;
      private String lblTextblockparametrossistema_padronizarstrings_Internalname ;
      private String lblTextblockparametrossistema_padronizarstrings_Jsonclick ;
      private String cmbParametrosSistema_PadronizarStrings_Internalname ;
      private String cmbParametrosSistema_PadronizarStrings_Jsonclick ;
      private String lblTextblockparametrossistema_fatorajuste_Internalname ;
      private String lblTextblockparametrossistema_fatorajuste_Jsonclick ;
      private String edtParametrosSistema_FatorAjuste_Internalname ;
      private String edtParametrosSistema_FatorAjuste_Jsonclick ;
      private String lblTextblockparametrossistema_pathcrtf_Internalname ;
      private String lblTextblockparametrossistema_pathcrtf_Jsonclick ;
      private String lblTextblockparametrossistema_flsevd_Internalname ;
      private String lblTextblockparametrossistema_flsevd_Jsonclick ;
      private String edtParametrosSistema_FlsEvd_Internalname ;
      private String edtParametrosSistema_FlsEvd_Jsonclick ;
      private String grpUnnamedgroup3_Internalname ;
      private String grpUnnamedgroup4_Internalname ;
      private String grpUnnamedgroup5_Internalname ;
      private String tblGrouplogin_Internalname ;
      private String bttBtnbntimagemlogin_Internalname ;
      private String bttBtnbntimagemlogin_Jsonclick ;
      private String lblTextblockparametrossistema_imagem_Internalname ;
      private String lblTextblockparametrossistema_imagem_Jsonclick ;
      private String imgParametrosSistema_Imagem_Internalname ;
      private String tblSistema_Internalname ;
      private String lblTextblockparametrossistema_urlapp_Internalname ;
      private String lblTextblockparametrossistema_urlapp_Jsonclick ;
      private String edtParametrosSistema_URLApp_Internalname ;
      private String edtParametrosSistema_URLApp_Jsonclick ;
      private String lblTextblockparametrossistema_urlotherver_Internalname ;
      private String lblTextblockparametrossistema_urlotherver_Jsonclick ;
      private String edtParametrosSistema_URLOtherVer_Internalname ;
      private String edtParametrosSistema_URLOtherVer_Jsonclick ;
      private String lblTextblockparametrossistema_hostmensuracao_Internalname ;
      private String lblTextblockparametrossistema_hostmensuracao_Jsonclick ;
      private String edtParametrosSistema_HostMensuracao_Internalname ;
      private String edtParametrosSistema_HostMensuracao_Jsonclick ;
      private String tblEmailsaida_Internalname ;
      private String lblTextblockparametrossistema_emailsdahost_Internalname ;
      private String lblTextblockparametrossistema_emailsdahost_Jsonclick ;
      private String edtParametrosSistema_EmailSdaHost_Internalname ;
      private String edtParametrosSistema_EmailSdaHost_Jsonclick ;
      private String lblTextblockparametrossistema_emailsdauser_Internalname ;
      private String lblTextblockparametrossistema_emailsdauser_Jsonclick ;
      private String edtParametrosSistema_EmailSdaUser_Internalname ;
      private String edtParametrosSistema_EmailSdaUser_Jsonclick ;
      private String lblTextblockparametrossistema_emailsdapass_Internalname ;
      private String lblTextblockparametrossistema_emailsdapass_Jsonclick ;
      private String edtParametrosSistema_EmailSdaPass_Internalname ;
      private String edtParametrosSistema_EmailSdaPass_Jsonclick ;
      private String lblTextblockparametrossistema_emailsdaport_Internalname ;
      private String lblTextblockparametrossistema_emailsdaport_Jsonclick ;
      private String edtParametrosSistema_EmailSdaPort_Internalname ;
      private String edtParametrosSistema_EmailSdaPort_Jsonclick ;
      private String lblTextblockparametrossistema_emailsdaaut_Internalname ;
      private String lblTextblockparametrossistema_emailsdaaut_Jsonclick ;
      private String cmbParametrosSistema_EmailSdaAut_Internalname ;
      private String cmbParametrosSistema_EmailSdaAut_Jsonclick ;
      private String lblTextblockparametrossistema_emailsdasec_Internalname ;
      private String lblTextblockparametrossistema_emailsdasec_Jsonclick ;
      private String cmbParametrosSistema_EmailSdaSec_Internalname ;
      private String cmbParametrosSistema_EmailSdaSec_Jsonclick ;
      private String bttBtntestaremail_Internalname ;
      private String bttBtntestaremail_Jsonclick ;
      private String tblTablemergedparametrossistema_pathcrtf_Internalname ;
      private String edtParametrosSistema_PathCrtf_Internalname ;
      private String edtParametrosSistema_PathCrtf_Jsonclick ;
      private String bttBtnassinar_Internalname ;
      private String bttBtnassinar_Jsonclick ;
      private String A537ParametrosSistema_EmailSdaKey ;
      private String AV14KeyEnc ;
      private String Parametrossistema_textohome_Width ;
      private String Parametrossistema_textohome_Height ;
      private String Parametrossistema_textohome_Skin ;
      private String Parametrossistema_textohome_Toolbar ;
      private String Parametrossistema_textohome_Class ;
      private String Parametrossistema_textohome_Customtoolbar ;
      private String Parametrossistema_textohome_Customconfiguration ;
      private String Parametrossistema_textohome_Buttonpressedid ;
      private String Parametrossistema_textohome_Captionvalue ;
      private String Parametrossistema_textohome_Captionclass ;
      private String Parametrossistema_textohome_Captionposition ;
      private String Parametrossistema_textohome_Coltitle ;
      private String Parametrossistema_textohome_Coltitlefont ;
      private String Gxuitabspanel_tabsparametros_Width ;
      private String Gxuitabspanel_tabsparametros_Height ;
      private String Gxuitabspanel_tabsparametros_Cls ;
      private String Gxuitabspanel_tabsparametros_Class ;
      private String Gxuitabspanel_tabsparametros_Activetabid ;
      private String Gxuitabspanel_tabsparametros_Designtimetabs ;
      private String Dvpanel_tableattributes_Width ;
      private String Dvpanel_tableattributes_Height ;
      private String Dvpanel_tableattributes_Cls ;
      private String Dvpanel_tableattributes_Title ;
      private String Dvpanel_tableattributes_Class ;
      private String Dvpanel_tableattributes_Iconposition ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode56 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String cmdBuffer ;
      private String Parametrossistema_textohome_Internalname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GXCCtlgxBlob ;
      private String Gxuitabspanel_tabsparametros_Internalname ;
      private String Dvpanel_tableattributes_Internalname ;
      private bool Z399ParametrosSistema_LicensiadoCadastrado ;
      private bool Z417ParametrosSistema_PadronizarStrings ;
      private bool Z535ParametrosSistema_EmailSdaAut ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool A417ParametrosSistema_PadronizarStrings ;
      private bool A535ParametrosSistema_EmailSdaAut ;
      private bool n535ParametrosSistema_EmailSdaAut ;
      private bool n2032ParametrosSistema_EmailSdaSec ;
      private bool wbErr ;
      private bool A2117ParametrosSistema_Imagem_IsBlob ;
      private bool n708ParametrosSistema_FatorAjuste ;
      private bool n1021ParametrosSistema_PathCrtf ;
      private bool n1499ParametrosSistema_FlsEvd ;
      private bool n532ParametrosSistema_EmailSdaHost ;
      private bool n533ParametrosSistema_EmailSdaUser ;
      private bool n534ParametrosSistema_EmailSdaPass ;
      private bool n536ParametrosSistema_EmailSdaPort ;
      private bool n1679ParametrosSistema_URLApp ;
      private bool n1952ParametrosSistema_URLOtherVer ;
      private bool n1163ParametrosSistema_HostMensuracao ;
      private bool n2117ParametrosSistema_Imagem ;
      private bool n399ParametrosSistema_LicensiadoCadastrado ;
      private bool A399ParametrosSistema_LicensiadoCadastrado ;
      private bool n537ParametrosSistema_EmailSdaKey ;
      private bool n1171ParametrosSistema_SQLComputerName ;
      private bool n1172ParametrosSistema_SQLServerName ;
      private bool n1168ParametrosSistema_SQLEdition ;
      private bool n1169ParametrosSistema_SQLInstance ;
      private bool n1170ParametrosSistema_DBName ;
      private bool n1954ParametrosSistema_Validacao ;
      private bool n40000ParametrosSistema_Imagem_GXI ;
      private bool Parametrossistema_textohome_Enabled ;
      private bool Parametrossistema_textohome_Toolbarcancollapse ;
      private bool Parametrossistema_textohome_Toolbarexpanded ;
      private bool Parametrossistema_textohome_Usercontroliscolumn ;
      private bool Parametrossistema_textohome_Visible ;
      private bool Gxuitabspanel_tabsparametros_Enabled ;
      private bool Gxuitabspanel_tabsparametros_Autowidth ;
      private bool Gxuitabspanel_tabsparametros_Autoheight ;
      private bool Gxuitabspanel_tabsparametros_Autoscroll ;
      private bool Gxuitabspanel_tabsparametros_Visible ;
      private bool Dvpanel_tableattributes_Collapsible ;
      private bool Dvpanel_tableattributes_Collapsed ;
      private bool Dvpanel_tableattributes_Enabled ;
      private bool Dvpanel_tableattributes_Autowidth ;
      private bool Dvpanel_tableattributes_Autoheight ;
      private bool Dvpanel_tableattributes_Showheader ;
      private bool Dvpanel_tableattributes_Showcollapseicon ;
      private bool Dvpanel_tableattributes_Autoscroll ;
      private bool Dvpanel_tableattributes_Visible ;
      private bool returnInSub ;
      private bool Gx_longc ;
      private bool i535ParametrosSistema_EmailSdaAut ;
      private bool i417ParametrosSistema_PadronizarStrings ;
      private bool i399ParametrosSistema_LicensiadoCadastrado ;
      private String A332ParametrosSistema_TextoHome ;
      private String Z332ParametrosSistema_TextoHome ;
      private String Z1021ParametrosSistema_PathCrtf ;
      private String Z1499ParametrosSistema_FlsEvd ;
      private String Z532ParametrosSistema_EmailSdaHost ;
      private String Z533ParametrosSistema_EmailSdaUser ;
      private String Z534ParametrosSistema_EmailSdaPass ;
      private String Z1163ParametrosSistema_HostMensuracao ;
      private String Z1171ParametrosSistema_SQLComputerName ;
      private String Z1172ParametrosSistema_SQLServerName ;
      private String Z1168ParametrosSistema_SQLEdition ;
      private String Z1169ParametrosSistema_SQLInstance ;
      private String Z1170ParametrosSistema_DBName ;
      private String Z1679ParametrosSistema_URLApp ;
      private String Z1952ParametrosSistema_URLOtherVer ;
      private String Z1954ParametrosSistema_Validacao ;
      private String O534ParametrosSistema_EmailSdaPass ;
      private String O1499ParametrosSistema_FlsEvd ;
      private String O1021ParametrosSistema_PathCrtf ;
      private String A1499ParametrosSistema_FlsEvd ;
      private String A40000ParametrosSistema_Imagem_GXI ;
      private String A1679ParametrosSistema_URLApp ;
      private String A1952ParametrosSistema_URLOtherVer ;
      private String A1163ParametrosSistema_HostMensuracao ;
      private String A532ParametrosSistema_EmailSdaHost ;
      private String A533ParametrosSistema_EmailSdaUser ;
      private String A534ParametrosSistema_EmailSdaPass ;
      private String A1021ParametrosSistema_PathCrtf ;
      private String A1171ParametrosSistema_SQLComputerName ;
      private String A1172ParametrosSistema_SQLServerName ;
      private String A1168ParametrosSistema_SQLEdition ;
      private String A1169ParametrosSistema_SQLInstance ;
      private String A1170ParametrosSistema_DBName ;
      private String A1954ParametrosSistema_Validacao ;
      private String Z40000ParametrosSistema_Imagem_GXI ;
      private String A2117ParametrosSistema_Imagem ;
      private String AV19ParametrosSistema_Imagem ;
      private String Z2117ParametrosSistema_Imagem ;
      private IGxSession AV10WebSession ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbParametrosSistema_PadronizarStrings ;
      private GXCombobox cmbParametrosSistema_EmailSdaAut ;
      private GXCombobox cmbParametrosSistema_EmailSdaSec ;
      private GxCommand RGZ ;
      private IDataStoreProvider pr_default ;
      private int[] T001J4_A330ParametrosSistema_Codigo ;
      private String[] T001J4_A1021ParametrosSistema_PathCrtf ;
      private bool[] T001J4_n1021ParametrosSistema_PathCrtf ;
      private String[] T001J4_A1499ParametrosSistema_FlsEvd ;
      private bool[] T001J4_n1499ParametrosSistema_FlsEvd ;
      private String[] T001J4_A331ParametrosSistema_NomeSistema ;
      private long[] T001J4_A334ParametrosSistema_AppID ;
      private String[] T001J4_A332ParametrosSistema_TextoHome ;
      private bool[] T001J4_A399ParametrosSistema_LicensiadoCadastrado ;
      private bool[] T001J4_n399ParametrosSistema_LicensiadoCadastrado ;
      private bool[] T001J4_A417ParametrosSistema_PadronizarStrings ;
      private String[] T001J4_A532ParametrosSistema_EmailSdaHost ;
      private bool[] T001J4_n532ParametrosSistema_EmailSdaHost ;
      private String[] T001J4_A533ParametrosSistema_EmailSdaUser ;
      private bool[] T001J4_n533ParametrosSistema_EmailSdaUser ;
      private String[] T001J4_A534ParametrosSistema_EmailSdaPass ;
      private bool[] T001J4_n534ParametrosSistema_EmailSdaPass ;
      private String[] T001J4_A537ParametrosSistema_EmailSdaKey ;
      private bool[] T001J4_n537ParametrosSistema_EmailSdaKey ;
      private bool[] T001J4_A535ParametrosSistema_EmailSdaAut ;
      private bool[] T001J4_n535ParametrosSistema_EmailSdaAut ;
      private short[] T001J4_A536ParametrosSistema_EmailSdaPort ;
      private bool[] T001J4_n536ParametrosSistema_EmailSdaPort ;
      private short[] T001J4_A2032ParametrosSistema_EmailSdaSec ;
      private bool[] T001J4_n2032ParametrosSistema_EmailSdaSec ;
      private decimal[] T001J4_A708ParametrosSistema_FatorAjuste ;
      private bool[] T001J4_n708ParametrosSistema_FatorAjuste ;
      private String[] T001J4_A1163ParametrosSistema_HostMensuracao ;
      private bool[] T001J4_n1163ParametrosSistema_HostMensuracao ;
      private String[] T001J4_A1171ParametrosSistema_SQLComputerName ;
      private bool[] T001J4_n1171ParametrosSistema_SQLComputerName ;
      private String[] T001J4_A1172ParametrosSistema_SQLServerName ;
      private bool[] T001J4_n1172ParametrosSistema_SQLServerName ;
      private String[] T001J4_A1168ParametrosSistema_SQLEdition ;
      private bool[] T001J4_n1168ParametrosSistema_SQLEdition ;
      private String[] T001J4_A1169ParametrosSistema_SQLInstance ;
      private bool[] T001J4_n1169ParametrosSistema_SQLInstance ;
      private String[] T001J4_A1170ParametrosSistema_DBName ;
      private bool[] T001J4_n1170ParametrosSistema_DBName ;
      private String[] T001J4_A1679ParametrosSistema_URLApp ;
      private bool[] T001J4_n1679ParametrosSistema_URLApp ;
      private String[] T001J4_A1952ParametrosSistema_URLOtherVer ;
      private bool[] T001J4_n1952ParametrosSistema_URLOtherVer ;
      private String[] T001J4_A1954ParametrosSistema_Validacao ;
      private bool[] T001J4_n1954ParametrosSistema_Validacao ;
      private String[] T001J4_A40000ParametrosSistema_Imagem_GXI ;
      private bool[] T001J4_n40000ParametrosSistema_Imagem_GXI ;
      private String[] T001J4_A2117ParametrosSistema_Imagem ;
      private bool[] T001J4_n2117ParametrosSistema_Imagem ;
      private int[] T001J5_A330ParametrosSistema_Codigo ;
      private int[] T001J3_A330ParametrosSistema_Codigo ;
      private String[] T001J3_A1021ParametrosSistema_PathCrtf ;
      private bool[] T001J3_n1021ParametrosSistema_PathCrtf ;
      private String[] T001J3_A1499ParametrosSistema_FlsEvd ;
      private bool[] T001J3_n1499ParametrosSistema_FlsEvd ;
      private String[] T001J3_A331ParametrosSistema_NomeSistema ;
      private long[] T001J3_A334ParametrosSistema_AppID ;
      private String[] T001J3_A332ParametrosSistema_TextoHome ;
      private bool[] T001J3_A399ParametrosSistema_LicensiadoCadastrado ;
      private bool[] T001J3_n399ParametrosSistema_LicensiadoCadastrado ;
      private bool[] T001J3_A417ParametrosSistema_PadronizarStrings ;
      private String[] T001J3_A532ParametrosSistema_EmailSdaHost ;
      private bool[] T001J3_n532ParametrosSistema_EmailSdaHost ;
      private String[] T001J3_A533ParametrosSistema_EmailSdaUser ;
      private bool[] T001J3_n533ParametrosSistema_EmailSdaUser ;
      private String[] T001J3_A534ParametrosSistema_EmailSdaPass ;
      private bool[] T001J3_n534ParametrosSistema_EmailSdaPass ;
      private String[] T001J3_A537ParametrosSistema_EmailSdaKey ;
      private bool[] T001J3_n537ParametrosSistema_EmailSdaKey ;
      private bool[] T001J3_A535ParametrosSistema_EmailSdaAut ;
      private bool[] T001J3_n535ParametrosSistema_EmailSdaAut ;
      private short[] T001J3_A536ParametrosSistema_EmailSdaPort ;
      private bool[] T001J3_n536ParametrosSistema_EmailSdaPort ;
      private short[] T001J3_A2032ParametrosSistema_EmailSdaSec ;
      private bool[] T001J3_n2032ParametrosSistema_EmailSdaSec ;
      private decimal[] T001J3_A708ParametrosSistema_FatorAjuste ;
      private bool[] T001J3_n708ParametrosSistema_FatorAjuste ;
      private String[] T001J3_A1163ParametrosSistema_HostMensuracao ;
      private bool[] T001J3_n1163ParametrosSistema_HostMensuracao ;
      private String[] T001J3_A1171ParametrosSistema_SQLComputerName ;
      private bool[] T001J3_n1171ParametrosSistema_SQLComputerName ;
      private String[] T001J3_A1172ParametrosSistema_SQLServerName ;
      private bool[] T001J3_n1172ParametrosSistema_SQLServerName ;
      private String[] T001J3_A1168ParametrosSistema_SQLEdition ;
      private bool[] T001J3_n1168ParametrosSistema_SQLEdition ;
      private String[] T001J3_A1169ParametrosSistema_SQLInstance ;
      private bool[] T001J3_n1169ParametrosSistema_SQLInstance ;
      private String[] T001J3_A1170ParametrosSistema_DBName ;
      private bool[] T001J3_n1170ParametrosSistema_DBName ;
      private String[] T001J3_A1679ParametrosSistema_URLApp ;
      private bool[] T001J3_n1679ParametrosSistema_URLApp ;
      private String[] T001J3_A1952ParametrosSistema_URLOtherVer ;
      private bool[] T001J3_n1952ParametrosSistema_URLOtherVer ;
      private String[] T001J3_A1954ParametrosSistema_Validacao ;
      private bool[] T001J3_n1954ParametrosSistema_Validacao ;
      private String[] T001J3_A40000ParametrosSistema_Imagem_GXI ;
      private bool[] T001J3_n40000ParametrosSistema_Imagem_GXI ;
      private String[] T001J3_A2117ParametrosSistema_Imagem ;
      private bool[] T001J3_n2117ParametrosSistema_Imagem ;
      private int[] T001J6_A330ParametrosSistema_Codigo ;
      private int[] T001J7_A330ParametrosSistema_Codigo ;
      private int[] T001J2_A330ParametrosSistema_Codigo ;
      private String[] T001J2_A1021ParametrosSistema_PathCrtf ;
      private bool[] T001J2_n1021ParametrosSistema_PathCrtf ;
      private String[] T001J2_A1499ParametrosSistema_FlsEvd ;
      private bool[] T001J2_n1499ParametrosSistema_FlsEvd ;
      private String[] T001J2_A331ParametrosSistema_NomeSistema ;
      private long[] T001J2_A334ParametrosSistema_AppID ;
      private String[] T001J2_A332ParametrosSistema_TextoHome ;
      private bool[] T001J2_A399ParametrosSistema_LicensiadoCadastrado ;
      private bool[] T001J2_n399ParametrosSistema_LicensiadoCadastrado ;
      private bool[] T001J2_A417ParametrosSistema_PadronizarStrings ;
      private String[] T001J2_A532ParametrosSistema_EmailSdaHost ;
      private bool[] T001J2_n532ParametrosSistema_EmailSdaHost ;
      private String[] T001J2_A533ParametrosSistema_EmailSdaUser ;
      private bool[] T001J2_n533ParametrosSistema_EmailSdaUser ;
      private String[] T001J2_A534ParametrosSistema_EmailSdaPass ;
      private bool[] T001J2_n534ParametrosSistema_EmailSdaPass ;
      private String[] T001J2_A537ParametrosSistema_EmailSdaKey ;
      private bool[] T001J2_n537ParametrosSistema_EmailSdaKey ;
      private bool[] T001J2_A535ParametrosSistema_EmailSdaAut ;
      private bool[] T001J2_n535ParametrosSistema_EmailSdaAut ;
      private short[] T001J2_A536ParametrosSistema_EmailSdaPort ;
      private bool[] T001J2_n536ParametrosSistema_EmailSdaPort ;
      private short[] T001J2_A2032ParametrosSistema_EmailSdaSec ;
      private bool[] T001J2_n2032ParametrosSistema_EmailSdaSec ;
      private decimal[] T001J2_A708ParametrosSistema_FatorAjuste ;
      private bool[] T001J2_n708ParametrosSistema_FatorAjuste ;
      private String[] T001J2_A1163ParametrosSistema_HostMensuracao ;
      private bool[] T001J2_n1163ParametrosSistema_HostMensuracao ;
      private String[] T001J2_A1171ParametrosSistema_SQLComputerName ;
      private bool[] T001J2_n1171ParametrosSistema_SQLComputerName ;
      private String[] T001J2_A1172ParametrosSistema_SQLServerName ;
      private bool[] T001J2_n1172ParametrosSistema_SQLServerName ;
      private String[] T001J2_A1168ParametrosSistema_SQLEdition ;
      private bool[] T001J2_n1168ParametrosSistema_SQLEdition ;
      private String[] T001J2_A1169ParametrosSistema_SQLInstance ;
      private bool[] T001J2_n1169ParametrosSistema_SQLInstance ;
      private String[] T001J2_A1170ParametrosSistema_DBName ;
      private bool[] T001J2_n1170ParametrosSistema_DBName ;
      private String[] T001J2_A1679ParametrosSistema_URLApp ;
      private bool[] T001J2_n1679ParametrosSistema_URLApp ;
      private String[] T001J2_A1952ParametrosSistema_URLOtherVer ;
      private bool[] T001J2_n1952ParametrosSistema_URLOtherVer ;
      private String[] T001J2_A1954ParametrosSistema_Validacao ;
      private bool[] T001J2_n1954ParametrosSistema_Validacao ;
      private String[] T001J2_A40000ParametrosSistema_Imagem_GXI ;
      private bool[] T001J2_n40000ParametrosSistema_Imagem_GXI ;
      private String[] T001J2_A2117ParametrosSistema_Imagem ;
      private bool[] T001J2_n2117ParametrosSistema_Imagem ;
      private int[] T001J12_A330ParametrosSistema_Codigo ;
      private GeneXus.Mail.GXMailMessage AV11Email ;
      private GeneXus.Mail.GXMailRecipient AV12MailRecipient ;
      private GeneXus.Mail.GXSMTPSession AV13SMTPSession ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
   }

   public class parametrossistema__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new UpdateCursor(def[6])
         ,new UpdateCursor(def[7])
         ,new UpdateCursor(def[8])
         ,new UpdateCursor(def[9])
         ,new ForEachCursor(def[10])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT001J4 ;
          prmT001J4 = new Object[] {
          new Object[] {"@ParametrosSistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001J5 ;
          prmT001J5 = new Object[] {
          new Object[] {"@ParametrosSistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001J3 ;
          prmT001J3 = new Object[] {
          new Object[] {"@ParametrosSistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001J6 ;
          prmT001J6 = new Object[] {
          new Object[] {"@ParametrosSistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001J7 ;
          prmT001J7 = new Object[] {
          new Object[] {"@ParametrosSistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001J2 ;
          prmT001J2 = new Object[] {
          new Object[] {"@ParametrosSistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001J8 ;
          prmT001J8 = new Object[] {
          new Object[] {"@ParametrosSistema_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ParametrosSistema_PathCrtf",SqlDbType.VarChar,100,0} ,
          new Object[] {"@ParametrosSistema_FlsEvd",SqlDbType.VarChar,60,0} ,
          new Object[] {"@ParametrosSistema_NomeSistema",SqlDbType.Char,50,0} ,
          new Object[] {"@ParametrosSistema_AppID",SqlDbType.Decimal,12,0} ,
          new Object[] {"@ParametrosSistema_TextoHome",SqlDbType.VarChar,20000,0} ,
          new Object[] {"@ParametrosSistema_LicensiadoCadastrado",SqlDbType.Bit,4,0} ,
          new Object[] {"@ParametrosSistema_PadronizarStrings",SqlDbType.Bit,4,0} ,
          new Object[] {"@ParametrosSistema_EmailSdaHost",SqlDbType.VarChar,50,0} ,
          new Object[] {"@ParametrosSistema_EmailSdaUser",SqlDbType.VarChar,50,0} ,
          new Object[] {"@ParametrosSistema_EmailSdaPass",SqlDbType.VarChar,50,0} ,
          new Object[] {"@ParametrosSistema_EmailSdaKey",SqlDbType.Char,32,0} ,
          new Object[] {"@ParametrosSistema_EmailSdaAut",SqlDbType.Bit,4,0} ,
          new Object[] {"@ParametrosSistema_EmailSdaPort",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ParametrosSistema_EmailSdaSec",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ParametrosSistema_FatorAjuste",SqlDbType.Decimal,6,2} ,
          new Object[] {"@ParametrosSistema_HostMensuracao",SqlDbType.VarChar,255,0} ,
          new Object[] {"@ParametrosSistema_SQLComputerName",SqlDbType.VarChar,60,0} ,
          new Object[] {"@ParametrosSistema_SQLServerName",SqlDbType.VarChar,60,0} ,
          new Object[] {"@ParametrosSistema_SQLEdition",SqlDbType.VarChar,60,0} ,
          new Object[] {"@ParametrosSistema_SQLInstance",SqlDbType.VarChar,60,0} ,
          new Object[] {"@ParametrosSistema_DBName",SqlDbType.VarChar,60,0} ,
          new Object[] {"@ParametrosSistema_URLApp",SqlDbType.VarChar,1000,0} ,
          new Object[] {"@ParametrosSistema_URLOtherVer",SqlDbType.VarChar,1000,0} ,
          new Object[] {"@ParametrosSistema_Validacao",SqlDbType.VarChar,80,0} ,
          new Object[] {"@ParametrosSistema_Imagem",SqlDbType.VarBinary,1024,0} ,
          new Object[] {"@ParametrosSistema_Imagem_GXI",SqlDbType.VarChar,2048,0}
          } ;
          Object[] prmT001J9 ;
          prmT001J9 = new Object[] {
          new Object[] {"@ParametrosSistema_PathCrtf",SqlDbType.VarChar,100,0} ,
          new Object[] {"@ParametrosSistema_FlsEvd",SqlDbType.VarChar,60,0} ,
          new Object[] {"@ParametrosSistema_NomeSistema",SqlDbType.Char,50,0} ,
          new Object[] {"@ParametrosSistema_AppID",SqlDbType.Decimal,12,0} ,
          new Object[] {"@ParametrosSistema_TextoHome",SqlDbType.VarChar,20000,0} ,
          new Object[] {"@ParametrosSistema_LicensiadoCadastrado",SqlDbType.Bit,4,0} ,
          new Object[] {"@ParametrosSistema_PadronizarStrings",SqlDbType.Bit,4,0} ,
          new Object[] {"@ParametrosSistema_EmailSdaHost",SqlDbType.VarChar,50,0} ,
          new Object[] {"@ParametrosSistema_EmailSdaUser",SqlDbType.VarChar,50,0} ,
          new Object[] {"@ParametrosSistema_EmailSdaPass",SqlDbType.VarChar,50,0} ,
          new Object[] {"@ParametrosSistema_EmailSdaKey",SqlDbType.Char,32,0} ,
          new Object[] {"@ParametrosSistema_EmailSdaAut",SqlDbType.Bit,4,0} ,
          new Object[] {"@ParametrosSistema_EmailSdaPort",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ParametrosSistema_EmailSdaSec",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ParametrosSistema_FatorAjuste",SqlDbType.Decimal,6,2} ,
          new Object[] {"@ParametrosSistema_HostMensuracao",SqlDbType.VarChar,255,0} ,
          new Object[] {"@ParametrosSistema_SQLComputerName",SqlDbType.VarChar,60,0} ,
          new Object[] {"@ParametrosSistema_SQLServerName",SqlDbType.VarChar,60,0} ,
          new Object[] {"@ParametrosSistema_SQLEdition",SqlDbType.VarChar,60,0} ,
          new Object[] {"@ParametrosSistema_SQLInstance",SqlDbType.VarChar,60,0} ,
          new Object[] {"@ParametrosSistema_DBName",SqlDbType.VarChar,60,0} ,
          new Object[] {"@ParametrosSistema_URLApp",SqlDbType.VarChar,1000,0} ,
          new Object[] {"@ParametrosSistema_URLOtherVer",SqlDbType.VarChar,1000,0} ,
          new Object[] {"@ParametrosSistema_Validacao",SqlDbType.VarChar,80,0} ,
          new Object[] {"@ParametrosSistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001J10 ;
          prmT001J10 = new Object[] {
          new Object[] {"@ParametrosSistema_Imagem",SqlDbType.VarBinary,1024,0} ,
          new Object[] {"@ParametrosSistema_Imagem_GXI",SqlDbType.VarChar,2048,0} ,
          new Object[] {"@ParametrosSistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001J11 ;
          prmT001J11 = new Object[] {
          new Object[] {"@ParametrosSistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001J12 ;
          prmT001J12 = new Object[] {
          } ;
          def= new CursorDef[] {
              new CursorDef("T001J2", "SELECT [ParametrosSistema_Codigo], [ParametrosSistema_PathCrtf], [ParametrosSistema_FlsEvd], [ParametrosSistema_NomeSistema], [ParametrosSistema_AppID], [ParametrosSistema_TextoHome], [ParametrosSistema_LicensiadoCadastrado], [ParametrosSistema_PadronizarStrings], [ParametrosSistema_EmailSdaHost], [ParametrosSistema_EmailSdaUser], [ParametrosSistema_EmailSdaPass], [ParametrosSistema_EmailSdaKey], [ParametrosSistema_EmailSdaAut], [ParametrosSistema_EmailSdaPort], [ParametrosSistema_EmailSdaSec], [ParametrosSistema_FatorAjuste], [ParametrosSistema_HostMensuracao], [ParametrosSistema_SQLComputerName], [ParametrosSistema_SQLServerName], [ParametrosSistema_SQLEdition], [ParametrosSistema_SQLInstance], [ParametrosSistema_DBName], [ParametrosSistema_URLApp], [ParametrosSistema_URLOtherVer], [ParametrosSistema_Validacao], [ParametrosSistema_Imagem_GXI], [ParametrosSistema_Imagem] FROM [ParametrosSistema] WITH (UPDLOCK) WHERE [ParametrosSistema_Codigo] = @ParametrosSistema_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001J2,1,0,true,false )
             ,new CursorDef("T001J3", "SELECT [ParametrosSistema_Codigo], [ParametrosSistema_PathCrtf], [ParametrosSistema_FlsEvd], [ParametrosSistema_NomeSistema], [ParametrosSistema_AppID], [ParametrosSistema_TextoHome], [ParametrosSistema_LicensiadoCadastrado], [ParametrosSistema_PadronizarStrings], [ParametrosSistema_EmailSdaHost], [ParametrosSistema_EmailSdaUser], [ParametrosSistema_EmailSdaPass], [ParametrosSistema_EmailSdaKey], [ParametrosSistema_EmailSdaAut], [ParametrosSistema_EmailSdaPort], [ParametrosSistema_EmailSdaSec], [ParametrosSistema_FatorAjuste], [ParametrosSistema_HostMensuracao], [ParametrosSistema_SQLComputerName], [ParametrosSistema_SQLServerName], [ParametrosSistema_SQLEdition], [ParametrosSistema_SQLInstance], [ParametrosSistema_DBName], [ParametrosSistema_URLApp], [ParametrosSistema_URLOtherVer], [ParametrosSistema_Validacao], [ParametrosSistema_Imagem_GXI], [ParametrosSistema_Imagem] FROM [ParametrosSistema] WITH (NOLOCK) WHERE [ParametrosSistema_Codigo] = @ParametrosSistema_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001J3,1,0,true,false )
             ,new CursorDef("T001J4", "SELECT TM1.[ParametrosSistema_Codigo], TM1.[ParametrosSistema_PathCrtf], TM1.[ParametrosSistema_FlsEvd], TM1.[ParametrosSistema_NomeSistema], TM1.[ParametrosSistema_AppID], TM1.[ParametrosSistema_TextoHome], TM1.[ParametrosSistema_LicensiadoCadastrado], TM1.[ParametrosSistema_PadronizarStrings], TM1.[ParametrosSistema_EmailSdaHost], TM1.[ParametrosSistema_EmailSdaUser], TM1.[ParametrosSistema_EmailSdaPass], TM1.[ParametrosSistema_EmailSdaKey], TM1.[ParametrosSistema_EmailSdaAut], TM1.[ParametrosSistema_EmailSdaPort], TM1.[ParametrosSistema_EmailSdaSec], TM1.[ParametrosSistema_FatorAjuste], TM1.[ParametrosSistema_HostMensuracao], TM1.[ParametrosSistema_SQLComputerName], TM1.[ParametrosSistema_SQLServerName], TM1.[ParametrosSistema_SQLEdition], TM1.[ParametrosSistema_SQLInstance], TM1.[ParametrosSistema_DBName], TM1.[ParametrosSistema_URLApp], TM1.[ParametrosSistema_URLOtherVer], TM1.[ParametrosSistema_Validacao], TM1.[ParametrosSistema_Imagem_GXI], TM1.[ParametrosSistema_Imagem] FROM [ParametrosSistema] TM1 WITH (NOLOCK) WHERE TM1.[ParametrosSistema_Codigo] = @ParametrosSistema_Codigo ORDER BY TM1.[ParametrosSistema_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT001J4,100,0,true,false )
             ,new CursorDef("T001J5", "SELECT [ParametrosSistema_Codigo] FROM [ParametrosSistema] WITH (NOLOCK) WHERE [ParametrosSistema_Codigo] = @ParametrosSistema_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT001J5,1,0,true,false )
             ,new CursorDef("T001J6", "SELECT TOP 1 [ParametrosSistema_Codigo] FROM [ParametrosSistema] WITH (NOLOCK) WHERE ( [ParametrosSistema_Codigo] > @ParametrosSistema_Codigo) ORDER BY [ParametrosSistema_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT001J6,1,0,true,true )
             ,new CursorDef("T001J7", "SELECT TOP 1 [ParametrosSistema_Codigo] FROM [ParametrosSistema] WITH (NOLOCK) WHERE ( [ParametrosSistema_Codigo] < @ParametrosSistema_Codigo) ORDER BY [ParametrosSistema_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT001J7,1,0,true,true )
             ,new CursorDef("T001J8", "INSERT INTO [ParametrosSistema]([ParametrosSistema_Codigo], [ParametrosSistema_PathCrtf], [ParametrosSistema_FlsEvd], [ParametrosSistema_NomeSistema], [ParametrosSistema_AppID], [ParametrosSistema_TextoHome], [ParametrosSistema_LicensiadoCadastrado], [ParametrosSistema_PadronizarStrings], [ParametrosSistema_EmailSdaHost], [ParametrosSistema_EmailSdaUser], [ParametrosSistema_EmailSdaPass], [ParametrosSistema_EmailSdaKey], [ParametrosSistema_EmailSdaAut], [ParametrosSistema_EmailSdaPort], [ParametrosSistema_EmailSdaSec], [ParametrosSistema_FatorAjuste], [ParametrosSistema_HostMensuracao], [ParametrosSistema_SQLComputerName], [ParametrosSistema_SQLServerName], [ParametrosSistema_SQLEdition], [ParametrosSistema_SQLInstance], [ParametrosSistema_DBName], [ParametrosSistema_URLApp], [ParametrosSistema_URLOtherVer], [ParametrosSistema_Validacao], [ParametrosSistema_Imagem], [ParametrosSistema_Imagem_GXI]) VALUES(@ParametrosSistema_Codigo, @ParametrosSistema_PathCrtf, @ParametrosSistema_FlsEvd, @ParametrosSistema_NomeSistema, @ParametrosSistema_AppID, @ParametrosSistema_TextoHome, @ParametrosSistema_LicensiadoCadastrado, @ParametrosSistema_PadronizarStrings, @ParametrosSistema_EmailSdaHost, @ParametrosSistema_EmailSdaUser, @ParametrosSistema_EmailSdaPass, @ParametrosSistema_EmailSdaKey, @ParametrosSistema_EmailSdaAut, @ParametrosSistema_EmailSdaPort, @ParametrosSistema_EmailSdaSec, @ParametrosSistema_FatorAjuste, @ParametrosSistema_HostMensuracao, @ParametrosSistema_SQLComputerName, @ParametrosSistema_SQLServerName, @ParametrosSistema_SQLEdition, @ParametrosSistema_SQLInstance, @ParametrosSistema_DBName, @ParametrosSistema_URLApp, @ParametrosSistema_URLOtherVer, @ParametrosSistema_Validacao, @ParametrosSistema_Imagem, @ParametrosSistema_Imagem_GXI)", GxErrorMask.GX_NOMASK,prmT001J8)
             ,new CursorDef("T001J9", "UPDATE [ParametrosSistema] SET [ParametrosSistema_PathCrtf]=@ParametrosSistema_PathCrtf, [ParametrosSistema_FlsEvd]=@ParametrosSistema_FlsEvd, [ParametrosSistema_NomeSistema]=@ParametrosSistema_NomeSistema, [ParametrosSistema_AppID]=@ParametrosSistema_AppID, [ParametrosSistema_TextoHome]=@ParametrosSistema_TextoHome, [ParametrosSistema_LicensiadoCadastrado]=@ParametrosSistema_LicensiadoCadastrado, [ParametrosSistema_PadronizarStrings]=@ParametrosSistema_PadronizarStrings, [ParametrosSistema_EmailSdaHost]=@ParametrosSistema_EmailSdaHost, [ParametrosSistema_EmailSdaUser]=@ParametrosSistema_EmailSdaUser, [ParametrosSistema_EmailSdaPass]=@ParametrosSistema_EmailSdaPass, [ParametrosSistema_EmailSdaKey]=@ParametrosSistema_EmailSdaKey, [ParametrosSistema_EmailSdaAut]=@ParametrosSistema_EmailSdaAut, [ParametrosSistema_EmailSdaPort]=@ParametrosSistema_EmailSdaPort, [ParametrosSistema_EmailSdaSec]=@ParametrosSistema_EmailSdaSec, [ParametrosSistema_FatorAjuste]=@ParametrosSistema_FatorAjuste, [ParametrosSistema_HostMensuracao]=@ParametrosSistema_HostMensuracao, [ParametrosSistema_SQLComputerName]=@ParametrosSistema_SQLComputerName, [ParametrosSistema_SQLServerName]=@ParametrosSistema_SQLServerName, [ParametrosSistema_SQLEdition]=@ParametrosSistema_SQLEdition, [ParametrosSistema_SQLInstance]=@ParametrosSistema_SQLInstance, [ParametrosSistema_DBName]=@ParametrosSistema_DBName, [ParametrosSistema_URLApp]=@ParametrosSistema_URLApp, [ParametrosSistema_URLOtherVer]=@ParametrosSistema_URLOtherVer, [ParametrosSistema_Validacao]=@ParametrosSistema_Validacao  WHERE [ParametrosSistema_Codigo] = @ParametrosSistema_Codigo", GxErrorMask.GX_NOMASK,prmT001J9)
             ,new CursorDef("T001J10", "UPDATE [ParametrosSistema] SET [ParametrosSistema_Imagem]=@ParametrosSistema_Imagem, [ParametrosSistema_Imagem_GXI]=@ParametrosSistema_Imagem_GXI  WHERE [ParametrosSistema_Codigo] = @ParametrosSistema_Codigo", GxErrorMask.GX_NOMASK,prmT001J10)
             ,new CursorDef("T001J11", "DELETE FROM [ParametrosSistema]  WHERE [ParametrosSistema_Codigo] = @ParametrosSistema_Codigo", GxErrorMask.GX_NOMASK,prmT001J11)
             ,new CursorDef("T001J12", "SELECT [ParametrosSistema_Codigo] FROM [ParametrosSistema] WITH (NOLOCK) ORDER BY [ParametrosSistema_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT001J12,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getString(4, 50) ;
                ((long[]) buf[6])[0] = rslt.getLong(5) ;
                ((String[]) buf[7])[0] = rslt.getLongVarchar(6) ;
                ((bool[]) buf[8])[0] = rslt.getBool(7) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(7);
                ((bool[]) buf[10])[0] = rslt.getBool(8) ;
                ((String[]) buf[11])[0] = rslt.getVarchar(9) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(9);
                ((String[]) buf[13])[0] = rslt.getVarchar(10) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(10);
                ((String[]) buf[15])[0] = rslt.getVarchar(11) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(11);
                ((String[]) buf[17])[0] = rslt.getString(12, 32) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(12);
                ((bool[]) buf[19])[0] = rslt.getBool(13) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(13);
                ((short[]) buf[21])[0] = rslt.getShort(14) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(14);
                ((short[]) buf[23])[0] = rslt.getShort(15) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(15);
                ((decimal[]) buf[25])[0] = rslt.getDecimal(16) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(16);
                ((String[]) buf[27])[0] = rslt.getVarchar(17) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(17);
                ((String[]) buf[29])[0] = rslt.getVarchar(18) ;
                ((bool[]) buf[30])[0] = rslt.wasNull(18);
                ((String[]) buf[31])[0] = rslt.getVarchar(19) ;
                ((bool[]) buf[32])[0] = rslt.wasNull(19);
                ((String[]) buf[33])[0] = rslt.getVarchar(20) ;
                ((bool[]) buf[34])[0] = rslt.wasNull(20);
                ((String[]) buf[35])[0] = rslt.getVarchar(21) ;
                ((bool[]) buf[36])[0] = rslt.wasNull(21);
                ((String[]) buf[37])[0] = rslt.getVarchar(22) ;
                ((bool[]) buf[38])[0] = rslt.wasNull(22);
                ((String[]) buf[39])[0] = rslt.getVarchar(23) ;
                ((bool[]) buf[40])[0] = rslt.wasNull(23);
                ((String[]) buf[41])[0] = rslt.getVarchar(24) ;
                ((bool[]) buf[42])[0] = rslt.wasNull(24);
                ((String[]) buf[43])[0] = rslt.getVarchar(25) ;
                ((bool[]) buf[44])[0] = rslt.wasNull(25);
                ((String[]) buf[45])[0] = rslt.getMultimediaUri(26) ;
                ((bool[]) buf[46])[0] = rslt.wasNull(26);
                ((String[]) buf[47])[0] = rslt.getMultimediaFile(27, rslt.getVarchar(26)) ;
                ((bool[]) buf[48])[0] = rslt.wasNull(27);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getString(4, 50) ;
                ((long[]) buf[6])[0] = rslt.getLong(5) ;
                ((String[]) buf[7])[0] = rslt.getLongVarchar(6) ;
                ((bool[]) buf[8])[0] = rslt.getBool(7) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(7);
                ((bool[]) buf[10])[0] = rslt.getBool(8) ;
                ((String[]) buf[11])[0] = rslt.getVarchar(9) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(9);
                ((String[]) buf[13])[0] = rslt.getVarchar(10) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(10);
                ((String[]) buf[15])[0] = rslt.getVarchar(11) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(11);
                ((String[]) buf[17])[0] = rslt.getString(12, 32) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(12);
                ((bool[]) buf[19])[0] = rslt.getBool(13) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(13);
                ((short[]) buf[21])[0] = rslt.getShort(14) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(14);
                ((short[]) buf[23])[0] = rslt.getShort(15) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(15);
                ((decimal[]) buf[25])[0] = rslt.getDecimal(16) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(16);
                ((String[]) buf[27])[0] = rslt.getVarchar(17) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(17);
                ((String[]) buf[29])[0] = rslt.getVarchar(18) ;
                ((bool[]) buf[30])[0] = rslt.wasNull(18);
                ((String[]) buf[31])[0] = rslt.getVarchar(19) ;
                ((bool[]) buf[32])[0] = rslt.wasNull(19);
                ((String[]) buf[33])[0] = rslt.getVarchar(20) ;
                ((bool[]) buf[34])[0] = rslt.wasNull(20);
                ((String[]) buf[35])[0] = rslt.getVarchar(21) ;
                ((bool[]) buf[36])[0] = rslt.wasNull(21);
                ((String[]) buf[37])[0] = rslt.getVarchar(22) ;
                ((bool[]) buf[38])[0] = rslt.wasNull(22);
                ((String[]) buf[39])[0] = rslt.getVarchar(23) ;
                ((bool[]) buf[40])[0] = rslt.wasNull(23);
                ((String[]) buf[41])[0] = rslt.getVarchar(24) ;
                ((bool[]) buf[42])[0] = rslt.wasNull(24);
                ((String[]) buf[43])[0] = rslt.getVarchar(25) ;
                ((bool[]) buf[44])[0] = rslt.wasNull(25);
                ((String[]) buf[45])[0] = rslt.getMultimediaUri(26) ;
                ((bool[]) buf[46])[0] = rslt.wasNull(26);
                ((String[]) buf[47])[0] = rslt.getMultimediaFile(27, rslt.getVarchar(26)) ;
                ((bool[]) buf[48])[0] = rslt.wasNull(27);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getString(4, 50) ;
                ((long[]) buf[6])[0] = rslt.getLong(5) ;
                ((String[]) buf[7])[0] = rslt.getLongVarchar(6) ;
                ((bool[]) buf[8])[0] = rslt.getBool(7) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(7);
                ((bool[]) buf[10])[0] = rslt.getBool(8) ;
                ((String[]) buf[11])[0] = rslt.getVarchar(9) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(9);
                ((String[]) buf[13])[0] = rslt.getVarchar(10) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(10);
                ((String[]) buf[15])[0] = rslt.getVarchar(11) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(11);
                ((String[]) buf[17])[0] = rslt.getString(12, 32) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(12);
                ((bool[]) buf[19])[0] = rslt.getBool(13) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(13);
                ((short[]) buf[21])[0] = rslt.getShort(14) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(14);
                ((short[]) buf[23])[0] = rslt.getShort(15) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(15);
                ((decimal[]) buf[25])[0] = rslt.getDecimal(16) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(16);
                ((String[]) buf[27])[0] = rslt.getVarchar(17) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(17);
                ((String[]) buf[29])[0] = rslt.getVarchar(18) ;
                ((bool[]) buf[30])[0] = rslt.wasNull(18);
                ((String[]) buf[31])[0] = rslt.getVarchar(19) ;
                ((bool[]) buf[32])[0] = rslt.wasNull(19);
                ((String[]) buf[33])[0] = rslt.getVarchar(20) ;
                ((bool[]) buf[34])[0] = rslt.wasNull(20);
                ((String[]) buf[35])[0] = rslt.getVarchar(21) ;
                ((bool[]) buf[36])[0] = rslt.wasNull(21);
                ((String[]) buf[37])[0] = rslt.getVarchar(22) ;
                ((bool[]) buf[38])[0] = rslt.wasNull(22);
                ((String[]) buf[39])[0] = rslt.getVarchar(23) ;
                ((bool[]) buf[40])[0] = rslt.wasNull(23);
                ((String[]) buf[41])[0] = rslt.getVarchar(24) ;
                ((bool[]) buf[42])[0] = rslt.wasNull(24);
                ((String[]) buf[43])[0] = rslt.getVarchar(25) ;
                ((bool[]) buf[44])[0] = rslt.wasNull(25);
                ((String[]) buf[45])[0] = rslt.getMultimediaUri(26) ;
                ((bool[]) buf[46])[0] = rslt.wasNull(26);
                ((String[]) buf[47])[0] = rslt.getMultimediaFile(27, rslt.getVarchar(26)) ;
                ((bool[]) buf[48])[0] = rslt.wasNull(27);
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[2]);
                }
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 3 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[4]);
                }
                stmt.SetParameter(4, (String)parms[5]);
                stmt.SetParameter(5, (long)parms[6]);
                stmt.SetParameter(6, (String)parms[7]);
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 7 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(7, (bool)parms[9]);
                }
                stmt.SetParameter(8, (bool)parms[10]);
                if ( (bool)parms[11] )
                {
                   stmt.setNull( 9 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(9, (String)parms[12]);
                }
                if ( (bool)parms[13] )
                {
                   stmt.setNull( 10 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(10, (String)parms[14]);
                }
                if ( (bool)parms[15] )
                {
                   stmt.setNull( 11 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(11, (String)parms[16]);
                }
                if ( (bool)parms[17] )
                {
                   stmt.setNull( 12 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(12, (String)parms[18]);
                }
                if ( (bool)parms[19] )
                {
                   stmt.setNull( 13 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(13, (bool)parms[20]);
                }
                if ( (bool)parms[21] )
                {
                   stmt.setNull( 14 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(14, (short)parms[22]);
                }
                if ( (bool)parms[23] )
                {
                   stmt.setNull( 15 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(15, (short)parms[24]);
                }
                if ( (bool)parms[25] )
                {
                   stmt.setNull( 16 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(16, (decimal)parms[26]);
                }
                if ( (bool)parms[27] )
                {
                   stmt.setNull( 17 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(17, (String)parms[28]);
                }
                if ( (bool)parms[29] )
                {
                   stmt.setNull( 18 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(18, (String)parms[30]);
                }
                if ( (bool)parms[31] )
                {
                   stmt.setNull( 19 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(19, (String)parms[32]);
                }
                if ( (bool)parms[33] )
                {
                   stmt.setNull( 20 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(20, (String)parms[34]);
                }
                if ( (bool)parms[35] )
                {
                   stmt.setNull( 21 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(21, (String)parms[36]);
                }
                if ( (bool)parms[37] )
                {
                   stmt.setNull( 22 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(22, (String)parms[38]);
                }
                if ( (bool)parms[39] )
                {
                   stmt.setNull( 23 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(23, (String)parms[40]);
                }
                if ( (bool)parms[41] )
                {
                   stmt.setNull( 24 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(24, (String)parms[42]);
                }
                if ( (bool)parms[43] )
                {
                   stmt.setNull( 25 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(25, (String)parms[44]);
                }
                if ( (bool)parms[45] )
                {
                   stmt.setNull( 26 , SqlDbType.VarBinary );
                }
                else
                {
                   stmt.SetParameter(26, (String)parms[46]);
                }
                if ( (bool)parms[47] )
                {
                   stmt.setNull( 27 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameterMultimedia(27, (String)parms[48], (String)parms[46]);
                }
                return;
             case 7 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[3]);
                }
                stmt.SetParameter(3, (String)parms[4]);
                stmt.SetParameter(4, (long)parms[5]);
                stmt.SetParameter(5, (String)parms[6]);
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 6 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(6, (bool)parms[8]);
                }
                stmt.SetParameter(7, (bool)parms[9]);
                if ( (bool)parms[10] )
                {
                   stmt.setNull( 8 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(8, (String)parms[11]);
                }
                if ( (bool)parms[12] )
                {
                   stmt.setNull( 9 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(9, (String)parms[13]);
                }
                if ( (bool)parms[14] )
                {
                   stmt.setNull( 10 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(10, (String)parms[15]);
                }
                if ( (bool)parms[16] )
                {
                   stmt.setNull( 11 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(11, (String)parms[17]);
                }
                if ( (bool)parms[18] )
                {
                   stmt.setNull( 12 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(12, (bool)parms[19]);
                }
                if ( (bool)parms[20] )
                {
                   stmt.setNull( 13 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(13, (short)parms[21]);
                }
                if ( (bool)parms[22] )
                {
                   stmt.setNull( 14 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(14, (short)parms[23]);
                }
                if ( (bool)parms[24] )
                {
                   stmt.setNull( 15 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(15, (decimal)parms[25]);
                }
                if ( (bool)parms[26] )
                {
                   stmt.setNull( 16 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(16, (String)parms[27]);
                }
                if ( (bool)parms[28] )
                {
                   stmt.setNull( 17 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(17, (String)parms[29]);
                }
                if ( (bool)parms[30] )
                {
                   stmt.setNull( 18 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(18, (String)parms[31]);
                }
                if ( (bool)parms[32] )
                {
                   stmt.setNull( 19 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(19, (String)parms[33]);
                }
                if ( (bool)parms[34] )
                {
                   stmt.setNull( 20 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(20, (String)parms[35]);
                }
                if ( (bool)parms[36] )
                {
                   stmt.setNull( 21 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(21, (String)parms[37]);
                }
                if ( (bool)parms[38] )
                {
                   stmt.setNull( 22 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(22, (String)parms[39]);
                }
                if ( (bool)parms[40] )
                {
                   stmt.setNull( 23 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(23, (String)parms[41]);
                }
                if ( (bool)parms[42] )
                {
                   stmt.setNull( 24 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(24, (String)parms[43]);
                }
                stmt.SetParameter(25, (int)parms[44]);
                return;
             case 8 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.VarBinary );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameterMultimedia(2, (String)parms[3], (String)parms[1]);
                }
                stmt.SetParameter(3, (int)parms[4]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
