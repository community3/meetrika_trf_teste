/*
               File: GetWWContratoServicosFilterData
        Description: Get WWContrato Servicos Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/21/2020 18:4:37.87
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getwwcontratoservicosfilterdata : GXProcedure
   {
      public getwwcontratoservicosfilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getwwcontratoservicosfilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV16DDOName = aP0_DDOName;
         this.AV14SearchTxt = aP1_SearchTxt;
         this.AV15SearchTxtTo = aP2_SearchTxtTo;
         this.AV20OptionsJson = "" ;
         this.AV23OptionsDescJson = "" ;
         this.AV25OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV20OptionsJson;
         aP4_OptionsDescJson=this.AV23OptionsDescJson;
         aP5_OptionIndexesJson=this.AV25OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV16DDOName = aP0_DDOName;
         this.AV14SearchTxt = aP1_SearchTxt;
         this.AV15SearchTxtTo = aP2_SearchTxtTo;
         this.AV20OptionsJson = "" ;
         this.AV23OptionsDescJson = "" ;
         this.AV25OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV20OptionsJson;
         aP4_OptionsDescJson=this.AV23OptionsDescJson;
         aP5_OptionIndexesJson=this.AV25OptionIndexesJson;
         return AV25OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getwwcontratoservicosfilterdata objgetwwcontratoservicosfilterdata;
         objgetwwcontratoservicosfilterdata = new getwwcontratoservicosfilterdata();
         objgetwwcontratoservicosfilterdata.AV16DDOName = aP0_DDOName;
         objgetwwcontratoservicosfilterdata.AV14SearchTxt = aP1_SearchTxt;
         objgetwwcontratoservicosfilterdata.AV15SearchTxtTo = aP2_SearchTxtTo;
         objgetwwcontratoservicosfilterdata.AV20OptionsJson = "" ;
         objgetwwcontratoservicosfilterdata.AV23OptionsDescJson = "" ;
         objgetwwcontratoservicosfilterdata.AV25OptionIndexesJson = "" ;
         objgetwwcontratoservicosfilterdata.context.SetSubmitInitialConfig(context);
         objgetwwcontratoservicosfilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetwwcontratoservicosfilterdata);
         aP3_OptionsJson=this.AV20OptionsJson;
         aP4_OptionsDescJson=this.AV23OptionsDescJson;
         aP5_OptionIndexesJson=this.AV25OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getwwcontratoservicosfilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV19Options = (IGxCollection)(new GxSimpleCollection());
         AV22OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV24OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV16DDOName), "DDO_CONTRATO_NUMERO") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATO_NUMEROOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV16DDOName), "DDO_SERVICO_NOME") == 0 )
         {
            /* Execute user subroutine: 'LOADSERVICO_NOMEOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV20OptionsJson = AV19Options.ToJSonString(false);
         AV23OptionsDescJson = AV22OptionsDesc.ToJSonString(false);
         AV25OptionIndexesJson = AV24OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV27Session.Get("WWContratoServicosGridState"), "") == 0 )
         {
            AV29GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "WWContratoServicosGridState"), "");
         }
         else
         {
            AV29GridState.FromXml(AV27Session.Get("WWContratoServicosGridState"), "");
         }
         AV41GXV1 = 1;
         while ( AV41GXV1 <= AV29GridState.gxTpr_Filtervalues.Count )
         {
            AV30GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV29GridState.gxTpr_Filtervalues.Item(AV41GXV1));
            if ( StringUtil.StrCmp(AV30GridStateFilterValue.gxTpr_Name, "TFCONTRATO_NUMERO") == 0 )
            {
               AV10TFContrato_Numero = AV30GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV30GridStateFilterValue.gxTpr_Name, "TFCONTRATO_NUMERO_SEL") == 0 )
            {
               AV11TFContrato_Numero_Sel = AV30GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV30GridStateFilterValue.gxTpr_Name, "TFSERVICO_NOME") == 0 )
            {
               AV12TFServico_Nome = AV30GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV30GridStateFilterValue.gxTpr_Name, "TFSERVICO_NOME_SEL") == 0 )
            {
               AV13TFServico_Nome_Sel = AV30GridStateFilterValue.gxTpr_Value;
            }
            AV41GXV1 = (int)(AV41GXV1+1);
         }
         if ( AV29GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV31GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV29GridState.gxTpr_Dynamicfilters.Item(1));
            AV32DynamicFiltersSelector1 = AV31GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV32DynamicFiltersSelector1, "CONTRATOSERVICOS_CODIGO") == 0 )
            {
               AV33ContratoServicos_Codigo1 = (int)(NumberUtil.Val( AV31GridStateDynamicFilter.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV32DynamicFiltersSelector1, "SERVICO_NOME") == 0 )
            {
               AV34Servico_Nome1 = AV31GridStateDynamicFilter.gxTpr_Value;
            }
            if ( AV29GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV35DynamicFiltersEnabled2 = true;
               AV31GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV29GridState.gxTpr_Dynamicfilters.Item(2));
               AV36DynamicFiltersSelector2 = AV31GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV36DynamicFiltersSelector2, "CONTRATOSERVICOS_CODIGO") == 0 )
               {
                  AV37ContratoServicos_Codigo2 = (int)(NumberUtil.Val( AV31GridStateDynamicFilter.gxTpr_Value, "."));
               }
               else if ( StringUtil.StrCmp(AV36DynamicFiltersSelector2, "SERVICO_NOME") == 0 )
               {
                  AV38Servico_Nome2 = AV31GridStateDynamicFilter.gxTpr_Value;
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADCONTRATO_NUMEROOPTIONS' Routine */
         AV10TFContrato_Numero = AV14SearchTxt;
         AV11TFContrato_Numero_Sel = "";
         AV43WWContratoServicosDS_1_Dynamicfiltersselector1 = AV32DynamicFiltersSelector1;
         AV44WWContratoServicosDS_2_Contratoservicos_codigo1 = AV33ContratoServicos_Codigo1;
         AV45WWContratoServicosDS_3_Servico_nome1 = AV34Servico_Nome1;
         AV46WWContratoServicosDS_4_Dynamicfiltersenabled2 = AV35DynamicFiltersEnabled2;
         AV47WWContratoServicosDS_5_Dynamicfiltersselector2 = AV36DynamicFiltersSelector2;
         AV48WWContratoServicosDS_6_Contratoservicos_codigo2 = AV37ContratoServicos_Codigo2;
         AV49WWContratoServicosDS_7_Servico_nome2 = AV38Servico_Nome2;
         AV50WWContratoServicosDS_8_Tfcontrato_numero = AV10TFContrato_Numero;
         AV51WWContratoServicosDS_9_Tfcontrato_numero_sel = AV11TFContrato_Numero_Sel;
         AV52WWContratoServicosDS_10_Tfservico_nome = AV12TFServico_Nome;
         AV53WWContratoServicosDS_11_Tfservico_nome_sel = AV13TFServico_Nome_Sel;
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV43WWContratoServicosDS_1_Dynamicfiltersselector1 ,
                                              AV44WWContratoServicosDS_2_Contratoservicos_codigo1 ,
                                              AV45WWContratoServicosDS_3_Servico_nome1 ,
                                              AV46WWContratoServicosDS_4_Dynamicfiltersenabled2 ,
                                              AV47WWContratoServicosDS_5_Dynamicfiltersselector2 ,
                                              AV48WWContratoServicosDS_6_Contratoservicos_codigo2 ,
                                              AV49WWContratoServicosDS_7_Servico_nome2 ,
                                              AV51WWContratoServicosDS_9_Tfcontrato_numero_sel ,
                                              AV50WWContratoServicosDS_8_Tfcontrato_numero ,
                                              AV53WWContratoServicosDS_11_Tfservico_nome_sel ,
                                              AV52WWContratoServicosDS_10_Tfservico_nome ,
                                              A160ContratoServicos_Codigo ,
                                              A608Servico_Nome ,
                                              A77Contrato_Numero },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING
                                              }
         });
         lV45WWContratoServicosDS_3_Servico_nome1 = StringUtil.PadR( StringUtil.RTrim( AV45WWContratoServicosDS_3_Servico_nome1), 50, "%");
         lV49WWContratoServicosDS_7_Servico_nome2 = StringUtil.PadR( StringUtil.RTrim( AV49WWContratoServicosDS_7_Servico_nome2), 50, "%");
         lV50WWContratoServicosDS_8_Tfcontrato_numero = StringUtil.PadR( StringUtil.RTrim( AV50WWContratoServicosDS_8_Tfcontrato_numero), 20, "%");
         lV52WWContratoServicosDS_10_Tfservico_nome = StringUtil.PadR( StringUtil.RTrim( AV52WWContratoServicosDS_10_Tfservico_nome), 50, "%");
         /* Using cursor P00JU2 */
         pr_default.execute(0, new Object[] {AV44WWContratoServicosDS_2_Contratoservicos_codigo1, lV45WWContratoServicosDS_3_Servico_nome1, AV48WWContratoServicosDS_6_Contratoservicos_codigo2, lV49WWContratoServicosDS_7_Servico_nome2, lV50WWContratoServicosDS_8_Tfcontrato_numero, AV51WWContratoServicosDS_9_Tfcontrato_numero_sel, lV52WWContratoServicosDS_10_Tfservico_nome, AV53WWContratoServicosDS_11_Tfservico_nome_sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKJU2 = false;
            A155Servico_Codigo = P00JU2_A155Servico_Codigo[0];
            A74Contrato_Codigo = P00JU2_A74Contrato_Codigo[0];
            A77Contrato_Numero = P00JU2_A77Contrato_Numero[0];
            A608Servico_Nome = P00JU2_A608Servico_Nome[0];
            A160ContratoServicos_Codigo = P00JU2_A160ContratoServicos_Codigo[0];
            A608Servico_Nome = P00JU2_A608Servico_Nome[0];
            A77Contrato_Numero = P00JU2_A77Contrato_Numero[0];
            AV26count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00JU2_A77Contrato_Numero[0], A77Contrato_Numero) == 0 ) )
            {
               BRKJU2 = false;
               A74Contrato_Codigo = P00JU2_A74Contrato_Codigo[0];
               A160ContratoServicos_Codigo = P00JU2_A160ContratoServicos_Codigo[0];
               AV26count = (long)(AV26count+1);
               BRKJU2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A77Contrato_Numero)) )
            {
               AV18Option = A77Contrato_Numero;
               AV19Options.Add(AV18Option, 0);
               AV24OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV26count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV19Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKJU2 )
            {
               BRKJU2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADSERVICO_NOMEOPTIONS' Routine */
         AV12TFServico_Nome = AV14SearchTxt;
         AV13TFServico_Nome_Sel = "";
         AV43WWContratoServicosDS_1_Dynamicfiltersselector1 = AV32DynamicFiltersSelector1;
         AV44WWContratoServicosDS_2_Contratoservicos_codigo1 = AV33ContratoServicos_Codigo1;
         AV45WWContratoServicosDS_3_Servico_nome1 = AV34Servico_Nome1;
         AV46WWContratoServicosDS_4_Dynamicfiltersenabled2 = AV35DynamicFiltersEnabled2;
         AV47WWContratoServicosDS_5_Dynamicfiltersselector2 = AV36DynamicFiltersSelector2;
         AV48WWContratoServicosDS_6_Contratoservicos_codigo2 = AV37ContratoServicos_Codigo2;
         AV49WWContratoServicosDS_7_Servico_nome2 = AV38Servico_Nome2;
         AV50WWContratoServicosDS_8_Tfcontrato_numero = AV10TFContrato_Numero;
         AV51WWContratoServicosDS_9_Tfcontrato_numero_sel = AV11TFContrato_Numero_Sel;
         AV52WWContratoServicosDS_10_Tfservico_nome = AV12TFServico_Nome;
         AV53WWContratoServicosDS_11_Tfservico_nome_sel = AV13TFServico_Nome_Sel;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV43WWContratoServicosDS_1_Dynamicfiltersselector1 ,
                                              AV44WWContratoServicosDS_2_Contratoservicos_codigo1 ,
                                              AV45WWContratoServicosDS_3_Servico_nome1 ,
                                              AV46WWContratoServicosDS_4_Dynamicfiltersenabled2 ,
                                              AV47WWContratoServicosDS_5_Dynamicfiltersselector2 ,
                                              AV48WWContratoServicosDS_6_Contratoservicos_codigo2 ,
                                              AV49WWContratoServicosDS_7_Servico_nome2 ,
                                              AV51WWContratoServicosDS_9_Tfcontrato_numero_sel ,
                                              AV50WWContratoServicosDS_8_Tfcontrato_numero ,
                                              AV53WWContratoServicosDS_11_Tfservico_nome_sel ,
                                              AV52WWContratoServicosDS_10_Tfservico_nome ,
                                              A160ContratoServicos_Codigo ,
                                              A608Servico_Nome ,
                                              A77Contrato_Numero },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING
                                              }
         });
         lV45WWContratoServicosDS_3_Servico_nome1 = StringUtil.PadR( StringUtil.RTrim( AV45WWContratoServicosDS_3_Servico_nome1), 50, "%");
         lV49WWContratoServicosDS_7_Servico_nome2 = StringUtil.PadR( StringUtil.RTrim( AV49WWContratoServicosDS_7_Servico_nome2), 50, "%");
         lV50WWContratoServicosDS_8_Tfcontrato_numero = StringUtil.PadR( StringUtil.RTrim( AV50WWContratoServicosDS_8_Tfcontrato_numero), 20, "%");
         lV52WWContratoServicosDS_10_Tfservico_nome = StringUtil.PadR( StringUtil.RTrim( AV52WWContratoServicosDS_10_Tfservico_nome), 50, "%");
         /* Using cursor P00JU3 */
         pr_default.execute(1, new Object[] {AV44WWContratoServicosDS_2_Contratoservicos_codigo1, lV45WWContratoServicosDS_3_Servico_nome1, AV48WWContratoServicosDS_6_Contratoservicos_codigo2, lV49WWContratoServicosDS_7_Servico_nome2, lV50WWContratoServicosDS_8_Tfcontrato_numero, AV51WWContratoServicosDS_9_Tfcontrato_numero_sel, lV52WWContratoServicosDS_10_Tfservico_nome, AV53WWContratoServicosDS_11_Tfservico_nome_sel});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKJU4 = false;
            A74Contrato_Codigo = P00JU3_A74Contrato_Codigo[0];
            A155Servico_Codigo = P00JU3_A155Servico_Codigo[0];
            A77Contrato_Numero = P00JU3_A77Contrato_Numero[0];
            A608Servico_Nome = P00JU3_A608Servico_Nome[0];
            A160ContratoServicos_Codigo = P00JU3_A160ContratoServicos_Codigo[0];
            A77Contrato_Numero = P00JU3_A77Contrato_Numero[0];
            A608Servico_Nome = P00JU3_A608Servico_Nome[0];
            AV26count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( P00JU3_A155Servico_Codigo[0] == A155Servico_Codigo ) )
            {
               BRKJU4 = false;
               A160ContratoServicos_Codigo = P00JU3_A160ContratoServicos_Codigo[0];
               AV26count = (long)(AV26count+1);
               BRKJU4 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A608Servico_Nome)) )
            {
               AV18Option = A608Servico_Nome;
               AV17InsertIndex = 1;
               while ( ( AV17InsertIndex <= AV19Options.Count ) && ( StringUtil.StrCmp(((String)AV19Options.Item(AV17InsertIndex)), AV18Option) < 0 ) )
               {
                  AV17InsertIndex = (int)(AV17InsertIndex+1);
               }
               AV19Options.Add(AV18Option, AV17InsertIndex);
               AV24OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV26count), "Z,ZZZ,ZZZ,ZZ9")), AV17InsertIndex);
            }
            if ( AV19Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKJU4 )
            {
               BRKJU4 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV19Options = new GxSimpleCollection();
         AV22OptionsDesc = new GxSimpleCollection();
         AV24OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV27Session = context.GetSession();
         AV29GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV30GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFContrato_Numero = "";
         AV11TFContrato_Numero_Sel = "";
         AV12TFServico_Nome = "";
         AV13TFServico_Nome_Sel = "";
         AV31GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV32DynamicFiltersSelector1 = "";
         AV34Servico_Nome1 = "";
         AV36DynamicFiltersSelector2 = "";
         AV38Servico_Nome2 = "";
         AV43WWContratoServicosDS_1_Dynamicfiltersselector1 = "";
         AV45WWContratoServicosDS_3_Servico_nome1 = "";
         AV47WWContratoServicosDS_5_Dynamicfiltersselector2 = "";
         AV49WWContratoServicosDS_7_Servico_nome2 = "";
         AV50WWContratoServicosDS_8_Tfcontrato_numero = "";
         AV51WWContratoServicosDS_9_Tfcontrato_numero_sel = "";
         AV52WWContratoServicosDS_10_Tfservico_nome = "";
         AV53WWContratoServicosDS_11_Tfservico_nome_sel = "";
         scmdbuf = "";
         lV45WWContratoServicosDS_3_Servico_nome1 = "";
         lV49WWContratoServicosDS_7_Servico_nome2 = "";
         lV50WWContratoServicosDS_8_Tfcontrato_numero = "";
         lV52WWContratoServicosDS_10_Tfservico_nome = "";
         A608Servico_Nome = "";
         A77Contrato_Numero = "";
         P00JU2_A155Servico_Codigo = new int[1] ;
         P00JU2_A74Contrato_Codigo = new int[1] ;
         P00JU2_A77Contrato_Numero = new String[] {""} ;
         P00JU2_A608Servico_Nome = new String[] {""} ;
         P00JU2_A160ContratoServicos_Codigo = new int[1] ;
         AV18Option = "";
         P00JU3_A74Contrato_Codigo = new int[1] ;
         P00JU3_A155Servico_Codigo = new int[1] ;
         P00JU3_A77Contrato_Numero = new String[] {""} ;
         P00JU3_A608Servico_Nome = new String[] {""} ;
         P00JU3_A160ContratoServicos_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getwwcontratoservicosfilterdata__default(),
            new Object[][] {
                new Object[] {
               P00JU2_A155Servico_Codigo, P00JU2_A74Contrato_Codigo, P00JU2_A77Contrato_Numero, P00JU2_A608Servico_Nome, P00JU2_A160ContratoServicos_Codigo
               }
               , new Object[] {
               P00JU3_A74Contrato_Codigo, P00JU3_A155Servico_Codigo, P00JU3_A77Contrato_Numero, P00JU3_A608Servico_Nome, P00JU3_A160ContratoServicos_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV41GXV1 ;
      private int AV33ContratoServicos_Codigo1 ;
      private int AV37ContratoServicos_Codigo2 ;
      private int AV44WWContratoServicosDS_2_Contratoservicos_codigo1 ;
      private int AV48WWContratoServicosDS_6_Contratoservicos_codigo2 ;
      private int A160ContratoServicos_Codigo ;
      private int A155Servico_Codigo ;
      private int A74Contrato_Codigo ;
      private int AV17InsertIndex ;
      private long AV26count ;
      private String AV10TFContrato_Numero ;
      private String AV11TFContrato_Numero_Sel ;
      private String AV12TFServico_Nome ;
      private String AV13TFServico_Nome_Sel ;
      private String AV34Servico_Nome1 ;
      private String AV38Servico_Nome2 ;
      private String AV45WWContratoServicosDS_3_Servico_nome1 ;
      private String AV49WWContratoServicosDS_7_Servico_nome2 ;
      private String AV50WWContratoServicosDS_8_Tfcontrato_numero ;
      private String AV51WWContratoServicosDS_9_Tfcontrato_numero_sel ;
      private String AV52WWContratoServicosDS_10_Tfservico_nome ;
      private String AV53WWContratoServicosDS_11_Tfservico_nome_sel ;
      private String scmdbuf ;
      private String lV45WWContratoServicosDS_3_Servico_nome1 ;
      private String lV49WWContratoServicosDS_7_Servico_nome2 ;
      private String lV50WWContratoServicosDS_8_Tfcontrato_numero ;
      private String lV52WWContratoServicosDS_10_Tfservico_nome ;
      private String A608Servico_Nome ;
      private String A77Contrato_Numero ;
      private bool returnInSub ;
      private bool AV35DynamicFiltersEnabled2 ;
      private bool AV46WWContratoServicosDS_4_Dynamicfiltersenabled2 ;
      private bool BRKJU2 ;
      private bool BRKJU4 ;
      private String AV25OptionIndexesJson ;
      private String AV20OptionsJson ;
      private String AV23OptionsDescJson ;
      private String AV16DDOName ;
      private String AV14SearchTxt ;
      private String AV15SearchTxtTo ;
      private String AV32DynamicFiltersSelector1 ;
      private String AV36DynamicFiltersSelector2 ;
      private String AV43WWContratoServicosDS_1_Dynamicfiltersselector1 ;
      private String AV47WWContratoServicosDS_5_Dynamicfiltersselector2 ;
      private String AV18Option ;
      private IGxSession AV27Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00JU2_A155Servico_Codigo ;
      private int[] P00JU2_A74Contrato_Codigo ;
      private String[] P00JU2_A77Contrato_Numero ;
      private String[] P00JU2_A608Servico_Nome ;
      private int[] P00JU2_A160ContratoServicos_Codigo ;
      private int[] P00JU3_A74Contrato_Codigo ;
      private int[] P00JU3_A155Servico_Codigo ;
      private String[] P00JU3_A77Contrato_Numero ;
      private String[] P00JU3_A608Servico_Nome ;
      private int[] P00JU3_A160ContratoServicos_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV19Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV22OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV24OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV29GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV30GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV31GridStateDynamicFilter ;
   }

   public class getwwcontratoservicosfilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00JU2( IGxContext context ,
                                             String AV43WWContratoServicosDS_1_Dynamicfiltersselector1 ,
                                             int AV44WWContratoServicosDS_2_Contratoservicos_codigo1 ,
                                             String AV45WWContratoServicosDS_3_Servico_nome1 ,
                                             bool AV46WWContratoServicosDS_4_Dynamicfiltersenabled2 ,
                                             String AV47WWContratoServicosDS_5_Dynamicfiltersselector2 ,
                                             int AV48WWContratoServicosDS_6_Contratoservicos_codigo2 ,
                                             String AV49WWContratoServicosDS_7_Servico_nome2 ,
                                             String AV51WWContratoServicosDS_9_Tfcontrato_numero_sel ,
                                             String AV50WWContratoServicosDS_8_Tfcontrato_numero ,
                                             String AV53WWContratoServicosDS_11_Tfservico_nome_sel ,
                                             String AV52WWContratoServicosDS_10_Tfservico_nome ,
                                             int A160ContratoServicos_Codigo ,
                                             String A608Servico_Nome ,
                                             String A77Contrato_Numero )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [8] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[Servico_Codigo], T1.[Contrato_Codigo], T3.[Contrato_Numero], T2.[Servico_Nome], T1.[ContratoServicos_Codigo] FROM (([ContratoServicos] T1 WITH (NOLOCK) INNER JOIN [Servico] T2 WITH (NOLOCK) ON T2.[Servico_Codigo] = T1.[Servico_Codigo]) INNER JOIN [Contrato] T3 WITH (NOLOCK) ON T3.[Contrato_Codigo] = T1.[Contrato_Codigo])";
         if ( ( StringUtil.StrCmp(AV43WWContratoServicosDS_1_Dynamicfiltersselector1, "CONTRATOSERVICOS_CODIGO") == 0 ) && ( ! (0==AV44WWContratoServicosDS_2_Contratoservicos_codigo1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoServicos_Codigo] = @AV44WWContratoServicosDS_2_Contratoservicos_codigo1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoServicos_Codigo] = @AV44WWContratoServicosDS_2_Contratoservicos_codigo1)";
            }
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV43WWContratoServicosDS_1_Dynamicfiltersselector1, "SERVICO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV45WWContratoServicosDS_3_Servico_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Nome] like '%' + @lV45WWContratoServicosDS_3_Servico_nome1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Nome] like '%' + @lV45WWContratoServicosDS_3_Servico_nome1 + '%')";
            }
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( AV46WWContratoServicosDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV47WWContratoServicosDS_5_Dynamicfiltersselector2, "CONTRATOSERVICOS_CODIGO") == 0 ) && ( ! (0==AV48WWContratoServicosDS_6_Contratoservicos_codigo2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoServicos_Codigo] = @AV48WWContratoServicosDS_6_Contratoservicos_codigo2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoServicos_Codigo] = @AV48WWContratoServicosDS_6_Contratoservicos_codigo2)";
            }
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( AV46WWContratoServicosDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV47WWContratoServicosDS_5_Dynamicfiltersselector2, "SERVICO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49WWContratoServicosDS_7_Servico_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Nome] like '%' + @lV49WWContratoServicosDS_7_Servico_nome2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Nome] like '%' + @lV49WWContratoServicosDS_7_Servico_nome2 + '%')";
            }
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV51WWContratoServicosDS_9_Tfcontrato_numero_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV50WWContratoServicosDS_8_Tfcontrato_numero)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Contrato_Numero] like @lV50WWContratoServicosDS_8_Tfcontrato_numero)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Contrato_Numero] like @lV50WWContratoServicosDS_8_Tfcontrato_numero)";
            }
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV51WWContratoServicosDS_9_Tfcontrato_numero_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Contrato_Numero] = @AV51WWContratoServicosDS_9_Tfcontrato_numero_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Contrato_Numero] = @AV51WWContratoServicosDS_9_Tfcontrato_numero_sel)";
            }
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV53WWContratoServicosDS_11_Tfservico_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52WWContratoServicosDS_10_Tfservico_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Nome] like @lV52WWContratoServicosDS_10_Tfservico_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Nome] like @lV52WWContratoServicosDS_10_Tfservico_nome)";
            }
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53WWContratoServicosDS_11_Tfservico_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Nome] = @AV53WWContratoServicosDS_11_Tfservico_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Nome] = @AV53WWContratoServicosDS_11_Tfservico_nome_sel)";
            }
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T3.[Contrato_Numero]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00JU3( IGxContext context ,
                                             String AV43WWContratoServicosDS_1_Dynamicfiltersselector1 ,
                                             int AV44WWContratoServicosDS_2_Contratoservicos_codigo1 ,
                                             String AV45WWContratoServicosDS_3_Servico_nome1 ,
                                             bool AV46WWContratoServicosDS_4_Dynamicfiltersenabled2 ,
                                             String AV47WWContratoServicosDS_5_Dynamicfiltersselector2 ,
                                             int AV48WWContratoServicosDS_6_Contratoservicos_codigo2 ,
                                             String AV49WWContratoServicosDS_7_Servico_nome2 ,
                                             String AV51WWContratoServicosDS_9_Tfcontrato_numero_sel ,
                                             String AV50WWContratoServicosDS_8_Tfcontrato_numero ,
                                             String AV53WWContratoServicosDS_11_Tfservico_nome_sel ,
                                             String AV52WWContratoServicosDS_10_Tfservico_nome ,
                                             int A160ContratoServicos_Codigo ,
                                             String A608Servico_Nome ,
                                             String A77Contrato_Numero )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [8] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT T1.[Contrato_Codigo], T1.[Servico_Codigo], T2.[Contrato_Numero], T3.[Servico_Nome], T1.[ContratoServicos_Codigo] FROM (([ContratoServicos] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo]) INNER JOIN [Servico] T3 WITH (NOLOCK) ON T3.[Servico_Codigo] = T1.[Servico_Codigo])";
         if ( ( StringUtil.StrCmp(AV43WWContratoServicosDS_1_Dynamicfiltersselector1, "CONTRATOSERVICOS_CODIGO") == 0 ) && ( ! (0==AV44WWContratoServicosDS_2_Contratoservicos_codigo1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoServicos_Codigo] = @AV44WWContratoServicosDS_2_Contratoservicos_codigo1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoServicos_Codigo] = @AV44WWContratoServicosDS_2_Contratoservicos_codigo1)";
            }
         }
         else
         {
            GXv_int3[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV43WWContratoServicosDS_1_Dynamicfiltersselector1, "SERVICO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV45WWContratoServicosDS_3_Servico_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Servico_Nome] like '%' + @lV45WWContratoServicosDS_3_Servico_nome1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Servico_Nome] like '%' + @lV45WWContratoServicosDS_3_Servico_nome1 + '%')";
            }
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( AV46WWContratoServicosDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV47WWContratoServicosDS_5_Dynamicfiltersselector2, "CONTRATOSERVICOS_CODIGO") == 0 ) && ( ! (0==AV48WWContratoServicosDS_6_Contratoservicos_codigo2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoServicos_Codigo] = @AV48WWContratoServicosDS_6_Contratoservicos_codigo2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoServicos_Codigo] = @AV48WWContratoServicosDS_6_Contratoservicos_codigo2)";
            }
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( AV46WWContratoServicosDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV47WWContratoServicosDS_5_Dynamicfiltersselector2, "SERVICO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49WWContratoServicosDS_7_Servico_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Servico_Nome] like '%' + @lV49WWContratoServicosDS_7_Servico_nome2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Servico_Nome] like '%' + @lV49WWContratoServicosDS_7_Servico_nome2 + '%')";
            }
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV51WWContratoServicosDS_9_Tfcontrato_numero_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV50WWContratoServicosDS_8_Tfcontrato_numero)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] like @lV50WWContratoServicosDS_8_Tfcontrato_numero)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] like @lV50WWContratoServicosDS_8_Tfcontrato_numero)";
            }
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV51WWContratoServicosDS_9_Tfcontrato_numero_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Contrato_Numero] = @AV51WWContratoServicosDS_9_Tfcontrato_numero_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Contrato_Numero] = @AV51WWContratoServicosDS_9_Tfcontrato_numero_sel)";
            }
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV53WWContratoServicosDS_11_Tfservico_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52WWContratoServicosDS_10_Tfservico_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Servico_Nome] like @lV52WWContratoServicosDS_10_Tfservico_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Servico_Nome] like @lV52WWContratoServicosDS_10_Tfservico_nome)";
            }
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53WWContratoServicosDS_11_Tfservico_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Servico_Nome] = @AV53WWContratoServicosDS_11_Tfservico_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Servico_Nome] = @AV53WWContratoServicosDS_11_Tfservico_nome_sel)";
            }
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[Servico_Codigo]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00JU2(context, (String)dynConstraints[0] , (int)dynConstraints[1] , (String)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (int)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (int)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] );
               case 1 :
                     return conditional_P00JU3(context, (String)dynConstraints[0] , (int)dynConstraints[1] , (String)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (int)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (int)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00JU2 ;
          prmP00JU2 = new Object[] {
          new Object[] {"@AV44WWContratoServicosDS_2_Contratoservicos_codigo1",SqlDbType.Int,6,0} ,
          new Object[] {"@lV45WWContratoServicosDS_3_Servico_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@AV48WWContratoServicosDS_6_Contratoservicos_codigo2",SqlDbType.Int,6,0} ,
          new Object[] {"@lV49WWContratoServicosDS_7_Servico_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV50WWContratoServicosDS_8_Tfcontrato_numero",SqlDbType.Char,20,0} ,
          new Object[] {"@AV51WWContratoServicosDS_9_Tfcontrato_numero_sel",SqlDbType.Char,20,0} ,
          new Object[] {"@lV52WWContratoServicosDS_10_Tfservico_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV53WWContratoServicosDS_11_Tfservico_nome_sel",SqlDbType.Char,50,0}
          } ;
          Object[] prmP00JU3 ;
          prmP00JU3 = new Object[] {
          new Object[] {"@AV44WWContratoServicosDS_2_Contratoservicos_codigo1",SqlDbType.Int,6,0} ,
          new Object[] {"@lV45WWContratoServicosDS_3_Servico_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@AV48WWContratoServicosDS_6_Contratoservicos_codigo2",SqlDbType.Int,6,0} ,
          new Object[] {"@lV49WWContratoServicosDS_7_Servico_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV50WWContratoServicosDS_8_Tfcontrato_numero",SqlDbType.Char,20,0} ,
          new Object[] {"@AV51WWContratoServicosDS_9_Tfcontrato_numero_sel",SqlDbType.Char,20,0} ,
          new Object[] {"@lV52WWContratoServicosDS_10_Tfservico_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV53WWContratoServicosDS_11_Tfservico_nome_sel",SqlDbType.Char,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00JU2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00JU2,100,0,true,false )
             ,new CursorDef("P00JU3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00JU3,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 20) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 50) ;
                ((int[]) buf[4])[0] = rslt.getInt(5) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 20) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 50) ;
                ((int[]) buf[4])[0] = rslt.getInt(5) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[8]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[9]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[10]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[8]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[9]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[10]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getwwcontratoservicosfilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getwwcontratoservicosfilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getwwcontratoservicosfilterdata") )
          {
             return  ;
          }
          getwwcontratoservicosfilterdata worker = new getwwcontratoservicosfilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
