/*
               File: MunicipioContratadaWC
        Description: Municipio Contratada WC
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:57:38.13
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class municipiocontratadawc : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public municipiocontratadawc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public municipiocontratadawc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Contratada_MunicipioCod )
      {
         this.AV7Contratada_MunicipioCod = aP0_Contratada_MunicipioCod;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersoperator1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersoperator2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         cmbavDynamicfiltersoperator3 = new GXCombobox();
         cmbContratada_AreaTrbClcPFnl = new GXCombobox();
         cmbContratada_TipoFabrica = new GXCombobox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  AV7Contratada_MunicipioCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Contratada_MunicipioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Contratada_MunicipioCod), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)AV7Contratada_MunicipioCod});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
               {
                  nRC_GXsfl_84 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  nGXsfl_84_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  sGXsfl_84_idx = GetNextPar( );
                  sPrefix = GetNextPar( );
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxnrGrid_newrow( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
               {
                  subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV14OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
                  AV15OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
                  AV16DynamicFiltersSelector1 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
                  AV17DynamicFiltersOperator1 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
                  AV18Contratada_AreaTrabalhoDes1 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18Contratada_AreaTrabalhoDes1", AV18Contratada_AreaTrabalhoDes1);
                  AV19Contratada_PessoaNom1 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19Contratada_PessoaNom1", AV19Contratada_PessoaNom1);
                  AV21DynamicFiltersSelector2 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
                  AV22DynamicFiltersOperator2 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
                  AV23Contratada_AreaTrabalhoDes2 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23Contratada_AreaTrabalhoDes2", AV23Contratada_AreaTrabalhoDes2);
                  AV24Contratada_PessoaNom2 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24Contratada_PessoaNom2", AV24Contratada_PessoaNom2);
                  AV26DynamicFiltersSelector3 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26DynamicFiltersSelector3", AV26DynamicFiltersSelector3);
                  AV27DynamicFiltersOperator3 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0)));
                  AV28Contratada_AreaTrabalhoDes3 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28Contratada_AreaTrabalhoDes3", AV28Contratada_AreaTrabalhoDes3);
                  AV29Contratada_PessoaNom3 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29Contratada_PessoaNom3", AV29Contratada_PessoaNom3);
                  AV20DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
                  AV25DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25DynamicFiltersEnabled3", AV25DynamicFiltersEnabled3);
                  AV7Contratada_MunicipioCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Contratada_MunicipioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Contratada_MunicipioCod), 6, 0)));
                  AV44Pgmname = GetNextPar( );
                  ajax_req_read_hidden_sdt(GetNextPar( ), AV11GridState);
                  AV31DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31DynamicFiltersIgnoreFirst", AV31DynamicFiltersIgnoreFirst);
                  AV30DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30DynamicFiltersRemoving", AV30DynamicFiltersRemoving);
                  ajax_req_read_hidden_sdt(GetNextPar( ), AV6WWPContext);
                  A39Contratada_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  A52Contratada_AreaTrabalhoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  sPrefix = GetNextPar( );
                  init_default_properties( ) ;
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18Contratada_AreaTrabalhoDes1, AV19Contratada_PessoaNom1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23Contratada_AreaTrabalhoDes2, AV24Contratada_PessoaNom2, AV26DynamicFiltersSelector3, AV27DynamicFiltersOperator3, AV28Contratada_AreaTrabalhoDes3, AV29Contratada_PessoaNom3, AV20DynamicFiltersEnabled2, AV25DynamicFiltersEnabled3, AV7Contratada_MunicipioCod, AV44Pgmname, AV11GridState, AV31DynamicFiltersIgnoreFirst, AV30DynamicFiltersRemoving, AV6WWPContext, A39Contratada_Codigo, A52Contratada_AreaTrabalhoCod, sPrefix) ;
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
                  return  ;
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PAP62( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV44Pgmname = "MunicipioContratadaWC";
               context.Gx_err = 0;
               WSP62( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Municipio Contratada WC") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202042822573854");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("municipiocontratadawc.aspx") + "?" + UrlEncode("" +AV7Contratada_MunicipioCod)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV14OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDDSC", StringUtil.BoolToStr( AV15OrderedDsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSSELECTOR1", AV16DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSOPERATOR1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV17DynamicFiltersOperator1), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vCONTRATADA_AREATRABALHODES1", AV18Contratada_AreaTrabalhoDes1);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vCONTRATADA_PESSOANOM1", StringUtil.RTrim( AV19Contratada_PessoaNom1));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSSELECTOR2", AV21DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSOPERATOR2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV22DynamicFiltersOperator2), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vCONTRATADA_AREATRABALHODES2", AV23Contratada_AreaTrabalhoDes2);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vCONTRATADA_PESSOANOM2", StringUtil.RTrim( AV24Contratada_PessoaNom2));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSSELECTOR3", AV26DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSOPERATOR3", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV27DynamicFiltersOperator3), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vCONTRATADA_AREATRABALHODES3", AV28Contratada_AreaTrabalhoDes3);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vCONTRATADA_PESSOANOM3", StringUtil.RTrim( AV29Contratada_PessoaNom3));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV20DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV25DynamicFiltersEnabled3));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"nRC_GXsfl_84", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_84), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV37GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV38GridPageCount), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOAV7Contratada_MunicipioCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOAV7Contratada_MunicipioCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vCONTRATADA_MUNICIPIOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7Contratada_MunicipioCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vPGMNAME", StringUtil.RTrim( AV44Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vGRIDSTATE", AV11GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vGRIDSTATE", AV11GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, sPrefix+"vDYNAMICFILTERSIGNOREFIRST", AV31DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, sPrefix+"vDYNAMICFILTERSREMOVING", AV30DynamicFiltersRemoving);
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vWWPCONTEXT", AV6WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vWWPCONTEXT", AV6WWPContext);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormP62( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("municipiocontratadawc.js", "?202042822573898");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "MunicipioContratadaWC" ;
      }

      public override String GetPgmdesc( )
      {
         return "Municipio Contratada WC" ;
      }

      protected void WBP60( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "municipiocontratadawc.aspx");
               context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
               context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
            }
            wb_table1_2_P62( true) ;
         }
         else
         {
            wb_table1_2_P62( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_P62e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratada_MunicipioCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A349Contratada_MunicipioCod), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A349Contratada_MunicipioCod), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratada_MunicipioCod_Jsonclick, 0, "Attribute", "", "", "", edtContratada_MunicipioCod_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_MunicipioContratadaWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 118,'" + sPrefix + "',false,'" + sGXsfl_84_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV20DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(118, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,118);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 119,'" + sPrefix + "',false,'" + sGXsfl_84_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV25DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(119, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,119);\"");
         }
         wbLoad = true;
      }

      protected void STARTP62( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Municipio Contratada WC", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPP60( ) ;
            }
         }
      }

      protected void WSP62( )
      {
         STARTP62( ) ;
         EVTP62( ) ;
      }

      protected void EVTP62( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPP60( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPP60( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11P62 */
                                    E11P62 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPP60( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12P62 */
                                    E12P62 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPP60( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E13P62 */
                                    E13P62 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPP60( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E14P62 */
                                    E14P62 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPP60( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E15P62 */
                                    E15P62 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOINSERT'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPP60( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E16P62 */
                                    E16P62 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPP60( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E17P62 */
                                    E17P62 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPP60( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E18P62 */
                                    E18P62 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPP60( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E19P62 */
                                    E19P62 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPP60( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E20P62 */
                                    E20P62 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR3.CLICK") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPP60( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E21P62 */
                                    E21P62 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPP60( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    GX_FocusControl = cmbavOrderedby_Internalname;
                                    context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPP60( ) ;
                              }
                              nGXsfl_84_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_84_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_84_idx), 4, 0)), 4, "0");
                              SubsflControlProps_842( ) ;
                              AV32Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV32Update)) ? AV41Update_GXI : context.convertURL( context.PathToRelativeUrl( AV32Update))));
                              AV33Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV33Delete)) ? AV42Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV33Delete))));
                              AV34Display = cgiGet( edtavDisplay_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDisplay_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV34Display)) ? AV43Display_GXI : context.convertURL( context.PathToRelativeUrl( AV34Display))));
                              A39Contratada_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContratada_Codigo_Internalname), ",", "."));
                              A52Contratada_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( edtContratada_AreaTrabalhoCod_Internalname), ",", "."));
                              A53Contratada_AreaTrabalhoDes = StringUtil.Upper( cgiGet( edtContratada_AreaTrabalhoDes_Internalname));
                              n53Contratada_AreaTrabalhoDes = false;
                              cmbContratada_AreaTrbClcPFnl.Name = cmbContratada_AreaTrbClcPFnl_Internalname;
                              cmbContratada_AreaTrbClcPFnl.CurrentValue = cgiGet( cmbContratada_AreaTrbClcPFnl_Internalname);
                              A1592Contratada_AreaTrbClcPFnl = cgiGet( cmbContratada_AreaTrbClcPFnl_Internalname);
                              n1592Contratada_AreaTrbClcPFnl = false;
                              A1595Contratada_AreaTrbSrvPdr = (int)(context.localUtil.CToN( cgiGet( edtContratada_AreaTrbSrvPdr_Internalname), ",", "."));
                              n1595Contratada_AreaTrbSrvPdr = false;
                              A40Contratada_PessoaCod = (int)(context.localUtil.CToN( cgiGet( edtContratada_PessoaCod_Internalname), ",", "."));
                              A41Contratada_PessoaNom = StringUtil.Upper( cgiGet( edtContratada_PessoaNom_Internalname));
                              n41Contratada_PessoaNom = false;
                              A42Contratada_PessoaCNPJ = cgiGet( edtContratada_PessoaCNPJ_Internalname);
                              n42Contratada_PessoaCNPJ = false;
                              A518Pessoa_IE = cgiGet( edtPessoa_IE_Internalname);
                              n518Pessoa_IE = false;
                              A519Pessoa_Endereco = cgiGet( edtPessoa_Endereco_Internalname);
                              n519Pessoa_Endereco = false;
                              A521Pessoa_CEP = cgiGet( edtPessoa_CEP_Internalname);
                              n521Pessoa_CEP = false;
                              A522Pessoa_Telefone = cgiGet( edtPessoa_Telefone_Internalname);
                              n522Pessoa_Telefone = false;
                              A523Pessoa_Fax = cgiGet( edtPessoa_Fax_Internalname);
                              n523Pessoa_Fax = false;
                              A438Contratada_Sigla = StringUtil.Upper( cgiGet( edtContratada_Sigla_Internalname));
                              cmbContratada_TipoFabrica.Name = cmbContratada_TipoFabrica_Internalname;
                              cmbContratada_TipoFabrica.CurrentValue = cgiGet( cmbContratada_TipoFabrica_Internalname);
                              A516Contratada_TipoFabrica = cgiGet( cmbContratada_TipoFabrica_Internalname);
                              A342Contratada_BancoNome = StringUtil.Upper( cgiGet( edtContratada_BancoNome_Internalname));
                              n342Contratada_BancoNome = false;
                              A343Contratada_BancoNro = cgiGet( edtContratada_BancoNro_Internalname);
                              n343Contratada_BancoNro = false;
                              A344Contratada_AgenciaNome = StringUtil.Upper( cgiGet( edtContratada_AgenciaNome_Internalname));
                              n344Contratada_AgenciaNome = false;
                              A345Contratada_AgenciaNro = cgiGet( edtContratada_AgenciaNro_Internalname);
                              n345Contratada_AgenciaNro = false;
                              A51Contratada_ContaCorrente = cgiGet( edtContratada_ContaCorrente_Internalname);
                              n51Contratada_ContaCorrente = false;
                              A524Contratada_OS = (int)(context.localUtil.CToN( cgiGet( edtContratada_OS_Internalname), ",", "."));
                              n524Contratada_OS = false;
                              A1451Contratada_SS = (int)(context.localUtil.CToN( cgiGet( edtContratada_SS_Internalname), ",", "."));
                              n1451Contratada_SS = false;
                              A530Contratada_Lote = (short)(context.localUtil.CToN( cgiGet( edtContratada_Lote_Internalname), ",", "."));
                              n530Contratada_Lote = false;
                              A1127Contratada_LogoArquivo = cgiGet( edtContratada_LogoArquivo_Internalname);
                              n1127Contratada_LogoArquivo = false;
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratada_LogoArquivo_Internalname, "URL", context.PathToRelativeUrl( A1127Contratada_LogoArquivo));
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E22P62 */
                                          E22P62 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E23P62 */
                                          E23P62 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E24P62 */
                                          E24P62 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          if ( ! wbErr )
                                          {
                                             Rfr0gs = false;
                                             /* Set Refresh If Orderedby Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV14OrderedBy )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Ordereddsc Changed */
                                             if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV15OrderedDsc )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersselector1 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR1"), AV16DynamicFiltersSelector1) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersoperator1 Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV17DynamicFiltersOperator1 )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Contratada_areatrabalhodes1 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vCONTRATADA_AREATRABALHODES1"), AV18Contratada_AreaTrabalhoDes1) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Contratada_pessoanom1 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vCONTRATADA_PESSOANOM1"), AV19Contratada_PessoaNom1) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersselector2 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR2"), AV21DynamicFiltersSelector2) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersoperator2 Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV22DynamicFiltersOperator2 )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Contratada_areatrabalhodes2 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vCONTRATADA_AREATRABALHODES2"), AV23Contratada_AreaTrabalhoDes2) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Contratada_pessoanom2 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vCONTRATADA_PESSOANOM2"), AV24Contratada_PessoaNom2) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersselector3 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR3"), AV26DynamicFiltersSelector3) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersoperator3 Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV27DynamicFiltersOperator3 )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Contratada_areatrabalhodes3 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vCONTRATADA_AREATRABALHODES3"), AV28Contratada_AreaTrabalhoDes3) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Contratada_pessoanom3 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vCONTRATADA_PESSOANOM3"), AV29Contratada_PessoaNom3) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                             if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSENABLED2")) != AV20DynamicFiltersEnabled2 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                             if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSENABLED3")) != AV25DynamicFiltersEnabled3 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             if ( ! Rfr0gs )
                                             {
                                             }
                                             dynload_actions( ) ;
                                          }
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                                    {
                                       STRUPP60( ) ;
                                    }
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEP62( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormP62( ) ;
            }
         }
      }

      protected void PAP62( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV14OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("CONTRATADA_AREATRABALHODES", "de Trabalho", 0);
            cmbavDynamicfiltersselector1.addItem("CONTRATADA_PESSOANOM", "Nome", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV16DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV16DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersoperator1.Name = "vDYNAMICFILTERSOPERATOR1";
            cmbavDynamicfiltersoperator1.WebTags = "";
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
            {
               AV17DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("CONTRATADA_AREATRABALHODES", "de Trabalho", 0);
            cmbavDynamicfiltersselector2.addItem("CONTRATADA_PESSOANOM", "Nome", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV21DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV21DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersoperator2.Name = "vDYNAMICFILTERSOPERATOR2";
            cmbavDynamicfiltersoperator2.WebTags = "";
            cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
            {
               AV22DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
            }
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("CONTRATADA_AREATRABALHODES", "de Trabalho", 0);
            cmbavDynamicfiltersselector3.addItem("CONTRATADA_PESSOANOM", "Nome", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV26DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV26DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26DynamicFiltersSelector3", AV26DynamicFiltersSelector3);
            }
            cmbavDynamicfiltersoperator3.Name = "vDYNAMICFILTERSOPERATOR3";
            cmbavDynamicfiltersoperator3.WebTags = "";
            cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
            {
               AV27DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0)));
            }
            GXCCtl = "CONTRATADA_AREATRBCLCPFNL_" + sGXsfl_84_idx;
            cmbContratada_AreaTrbClcPFnl.Name = GXCCtl;
            cmbContratada_AreaTrbClcPFnl.WebTags = "";
            cmbContratada_AreaTrbClcPFnl.addItem("MB", "Menor PFB", 0);
            cmbContratada_AreaTrbClcPFnl.addItem("BM", "PFB FM", 0);
            if ( cmbContratada_AreaTrbClcPFnl.ItemCount > 0 )
            {
               A1592Contratada_AreaTrbClcPFnl = cmbContratada_AreaTrbClcPFnl.getValidValue(A1592Contratada_AreaTrbClcPFnl);
               n1592Contratada_AreaTrbClcPFnl = false;
            }
            GXCCtl = "CONTRATADA_TIPOFABRICA_" + sGXsfl_84_idx;
            cmbContratada_TipoFabrica.Name = GXCCtl;
            cmbContratada_TipoFabrica.WebTags = "";
            cmbContratada_TipoFabrica.addItem("", "(Nenhuma)", 0);
            cmbContratada_TipoFabrica.addItem("S", "F�brica de Software", 0);
            cmbContratada_TipoFabrica.addItem("M", "F�brica de M�trica", 0);
            cmbContratada_TipoFabrica.addItem("I", "Departamento / Setor", 0);
            cmbContratada_TipoFabrica.addItem("O", "Outras", 0);
            if ( cmbContratada_TipoFabrica.ItemCount > 0 )
            {
               A516Contratada_TipoFabrica = cmbContratada_TipoFabrica.getValidValue(A516Contratada_TipoFabrica);
            }
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_842( ) ;
         while ( nGXsfl_84_idx <= nRC_GXsfl_84 )
         {
            sendrow_842( ) ;
            nGXsfl_84_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_84_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_84_idx+1));
            sGXsfl_84_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_84_idx), 4, 0)), 4, "0");
            SubsflControlProps_842( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV14OrderedBy ,
                                       bool AV15OrderedDsc ,
                                       String AV16DynamicFiltersSelector1 ,
                                       short AV17DynamicFiltersOperator1 ,
                                       String AV18Contratada_AreaTrabalhoDes1 ,
                                       String AV19Contratada_PessoaNom1 ,
                                       String AV21DynamicFiltersSelector2 ,
                                       short AV22DynamicFiltersOperator2 ,
                                       String AV23Contratada_AreaTrabalhoDes2 ,
                                       String AV24Contratada_PessoaNom2 ,
                                       String AV26DynamicFiltersSelector3 ,
                                       short AV27DynamicFiltersOperator3 ,
                                       String AV28Contratada_AreaTrabalhoDes3 ,
                                       String AV29Contratada_PessoaNom3 ,
                                       bool AV20DynamicFiltersEnabled2 ,
                                       bool AV25DynamicFiltersEnabled3 ,
                                       int AV7Contratada_MunicipioCod ,
                                       String AV44Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV11GridState ,
                                       bool AV31DynamicFiltersIgnoreFirst ,
                                       bool AV30DynamicFiltersRemoving ,
                                       wwpbaseobjects.SdtWWPContext AV6WWPContext ,
                                       int A39Contratada_Codigo ,
                                       int A52Contratada_AreaTrabalhoCod ,
                                       String sPrefix )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFP62( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATADA_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A39Contratada_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATADA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A39Contratada_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATADA_AREATRABALHOCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A52Contratada_AreaTrabalhoCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATADA_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A52Contratada_AreaTrabalhoCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATADA_PESSOACOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A40Contratada_PessoaCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATADA_PESSOACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A40Contratada_PessoaCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATADA_SIGLA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A438Contratada_Sigla, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATADA_SIGLA", StringUtil.RTrim( A438Contratada_Sigla));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATADA_TIPOFABRICA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A516Contratada_TipoFabrica, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATADA_TIPOFABRICA", StringUtil.RTrim( A516Contratada_TipoFabrica));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATADA_BANCONOME", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A342Contratada_BancoNome, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATADA_BANCONOME", StringUtil.RTrim( A342Contratada_BancoNome));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATADA_BANCONRO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A343Contratada_BancoNro, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATADA_BANCONRO", StringUtil.RTrim( A343Contratada_BancoNro));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATADA_AGENCIANOME", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A344Contratada_AgenciaNome, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATADA_AGENCIANOME", StringUtil.RTrim( A344Contratada_AgenciaNome));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATADA_AGENCIANRO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A345Contratada_AgenciaNro, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATADA_AGENCIANRO", StringUtil.RTrim( A345Contratada_AgenciaNro));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATADA_CONTACORRENTE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A51Contratada_ContaCorrente, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATADA_CONTACORRENTE", StringUtil.RTrim( A51Contratada_ContaCorrente));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATADA_OS", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A524Contratada_OS), "ZZZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATADA_OS", StringUtil.LTrim( StringUtil.NToC( (decimal)(A524Contratada_OS), 8, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATADA_SS", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1451Contratada_SS), "ZZZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATADA_SS", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1451Contratada_SS), 8, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATADA_LOTE", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A530Contratada_Lote), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATADA_LOTE", StringUtil.LTrim( StringUtil.NToC( (decimal)(A530Contratada_Lote), 4, 0, ".", "")));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV14OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV16DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV16DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
         {
            AV17DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV21DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV21DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
         {
            AV22DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV26DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV26DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26DynamicFiltersSelector3", AV26DynamicFiltersSelector3);
         }
         if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
         {
            AV27DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFP62( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV44Pgmname = "MunicipioContratadaWC";
         context.Gx_err = 0;
      }

      protected void RFP62( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 84;
         /* Execute user event: E23P62 */
         E23P62 ();
         nGXsfl_84_idx = 1;
         sGXsfl_84_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_84_idx), 4, 0)), 4, "0");
         SubsflControlProps_842( ) ;
         nGXsfl_84_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", sPrefix);
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_842( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV16DynamicFiltersSelector1 ,
                                                 AV17DynamicFiltersOperator1 ,
                                                 AV18Contratada_AreaTrabalhoDes1 ,
                                                 AV19Contratada_PessoaNom1 ,
                                                 AV20DynamicFiltersEnabled2 ,
                                                 AV21DynamicFiltersSelector2 ,
                                                 AV22DynamicFiltersOperator2 ,
                                                 AV23Contratada_AreaTrabalhoDes2 ,
                                                 AV24Contratada_PessoaNom2 ,
                                                 AV25DynamicFiltersEnabled3 ,
                                                 AV26DynamicFiltersSelector3 ,
                                                 AV27DynamicFiltersOperator3 ,
                                                 AV28Contratada_AreaTrabalhoDes3 ,
                                                 AV29Contratada_PessoaNom3 ,
                                                 A53Contratada_AreaTrabalhoDes ,
                                                 A41Contratada_PessoaNom ,
                                                 AV14OrderedBy ,
                                                 AV15OrderedDsc ,
                                                 A349Contratada_MunicipioCod ,
                                                 AV7Contratada_MunicipioCod },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                                 TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN,
                                                 TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT
                                                 }
            });
            lV18Contratada_AreaTrabalhoDes1 = StringUtil.Concat( StringUtil.RTrim( AV18Contratada_AreaTrabalhoDes1), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18Contratada_AreaTrabalhoDes1", AV18Contratada_AreaTrabalhoDes1);
            lV18Contratada_AreaTrabalhoDes1 = StringUtil.Concat( StringUtil.RTrim( AV18Contratada_AreaTrabalhoDes1), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18Contratada_AreaTrabalhoDes1", AV18Contratada_AreaTrabalhoDes1);
            lV19Contratada_PessoaNom1 = StringUtil.PadR( StringUtil.RTrim( AV19Contratada_PessoaNom1), 100, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19Contratada_PessoaNom1", AV19Contratada_PessoaNom1);
            lV19Contratada_PessoaNom1 = StringUtil.PadR( StringUtil.RTrim( AV19Contratada_PessoaNom1), 100, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19Contratada_PessoaNom1", AV19Contratada_PessoaNom1);
            lV23Contratada_AreaTrabalhoDes2 = StringUtil.Concat( StringUtil.RTrim( AV23Contratada_AreaTrabalhoDes2), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23Contratada_AreaTrabalhoDes2", AV23Contratada_AreaTrabalhoDes2);
            lV23Contratada_AreaTrabalhoDes2 = StringUtil.Concat( StringUtil.RTrim( AV23Contratada_AreaTrabalhoDes2), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23Contratada_AreaTrabalhoDes2", AV23Contratada_AreaTrabalhoDes2);
            lV24Contratada_PessoaNom2 = StringUtil.PadR( StringUtil.RTrim( AV24Contratada_PessoaNom2), 100, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24Contratada_PessoaNom2", AV24Contratada_PessoaNom2);
            lV24Contratada_PessoaNom2 = StringUtil.PadR( StringUtil.RTrim( AV24Contratada_PessoaNom2), 100, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24Contratada_PessoaNom2", AV24Contratada_PessoaNom2);
            lV28Contratada_AreaTrabalhoDes3 = StringUtil.Concat( StringUtil.RTrim( AV28Contratada_AreaTrabalhoDes3), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28Contratada_AreaTrabalhoDes3", AV28Contratada_AreaTrabalhoDes3);
            lV28Contratada_AreaTrabalhoDes3 = StringUtil.Concat( StringUtil.RTrim( AV28Contratada_AreaTrabalhoDes3), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28Contratada_AreaTrabalhoDes3", AV28Contratada_AreaTrabalhoDes3);
            lV29Contratada_PessoaNom3 = StringUtil.PadR( StringUtil.RTrim( AV29Contratada_PessoaNom3), 100, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29Contratada_PessoaNom3", AV29Contratada_PessoaNom3);
            lV29Contratada_PessoaNom3 = StringUtil.PadR( StringUtil.RTrim( AV29Contratada_PessoaNom3), 100, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29Contratada_PessoaNom3", AV29Contratada_PessoaNom3);
            /* Using cursor H00P62 */
            pr_default.execute(0, new Object[] {AV7Contratada_MunicipioCod, lV18Contratada_AreaTrabalhoDes1, lV18Contratada_AreaTrabalhoDes1, lV19Contratada_PessoaNom1, lV19Contratada_PessoaNom1, lV23Contratada_AreaTrabalhoDes2, lV23Contratada_AreaTrabalhoDes2, lV24Contratada_PessoaNom2, lV24Contratada_PessoaNom2, lV28Contratada_AreaTrabalhoDes3, lV28Contratada_AreaTrabalhoDes3, lV29Contratada_PessoaNom3, lV29Contratada_PessoaNom3, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_84_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A1128Contratada_LogoNomeArq = H00P62_A1128Contratada_LogoNomeArq[0];
               n1128Contratada_LogoNomeArq = H00P62_n1128Contratada_LogoNomeArq[0];
               edtContratada_LogoArquivo_Filename = A1128Contratada_LogoNomeArq;
               A1129Contratada_LogoTipoArq = H00P62_A1129Contratada_LogoTipoArq[0];
               n1129Contratada_LogoTipoArq = H00P62_n1129Contratada_LogoTipoArq[0];
               edtContratada_LogoArquivo_Filetype = A1129Contratada_LogoTipoArq;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratada_LogoArquivo_Internalname, "Filetype", edtContratada_LogoArquivo_Filetype);
               A349Contratada_MunicipioCod = H00P62_A349Contratada_MunicipioCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A349Contratada_MunicipioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A349Contratada_MunicipioCod), 6, 0)));
               n349Contratada_MunicipioCod = H00P62_n349Contratada_MunicipioCod[0];
               A530Contratada_Lote = H00P62_A530Contratada_Lote[0];
               n530Contratada_Lote = H00P62_n530Contratada_Lote[0];
               A1451Contratada_SS = H00P62_A1451Contratada_SS[0];
               n1451Contratada_SS = H00P62_n1451Contratada_SS[0];
               A524Contratada_OS = H00P62_A524Contratada_OS[0];
               n524Contratada_OS = H00P62_n524Contratada_OS[0];
               A51Contratada_ContaCorrente = H00P62_A51Contratada_ContaCorrente[0];
               n51Contratada_ContaCorrente = H00P62_n51Contratada_ContaCorrente[0];
               A345Contratada_AgenciaNro = H00P62_A345Contratada_AgenciaNro[0];
               n345Contratada_AgenciaNro = H00P62_n345Contratada_AgenciaNro[0];
               A344Contratada_AgenciaNome = H00P62_A344Contratada_AgenciaNome[0];
               n344Contratada_AgenciaNome = H00P62_n344Contratada_AgenciaNome[0];
               A343Contratada_BancoNro = H00P62_A343Contratada_BancoNro[0];
               n343Contratada_BancoNro = H00P62_n343Contratada_BancoNro[0];
               A342Contratada_BancoNome = H00P62_A342Contratada_BancoNome[0];
               n342Contratada_BancoNome = H00P62_n342Contratada_BancoNome[0];
               A516Contratada_TipoFabrica = H00P62_A516Contratada_TipoFabrica[0];
               A438Contratada_Sigla = H00P62_A438Contratada_Sigla[0];
               A523Pessoa_Fax = H00P62_A523Pessoa_Fax[0];
               n523Pessoa_Fax = H00P62_n523Pessoa_Fax[0];
               A522Pessoa_Telefone = H00P62_A522Pessoa_Telefone[0];
               n522Pessoa_Telefone = H00P62_n522Pessoa_Telefone[0];
               A521Pessoa_CEP = H00P62_A521Pessoa_CEP[0];
               n521Pessoa_CEP = H00P62_n521Pessoa_CEP[0];
               A519Pessoa_Endereco = H00P62_A519Pessoa_Endereco[0];
               n519Pessoa_Endereco = H00P62_n519Pessoa_Endereco[0];
               A518Pessoa_IE = H00P62_A518Pessoa_IE[0];
               n518Pessoa_IE = H00P62_n518Pessoa_IE[0];
               A42Contratada_PessoaCNPJ = H00P62_A42Contratada_PessoaCNPJ[0];
               n42Contratada_PessoaCNPJ = H00P62_n42Contratada_PessoaCNPJ[0];
               A41Contratada_PessoaNom = H00P62_A41Contratada_PessoaNom[0];
               n41Contratada_PessoaNom = H00P62_n41Contratada_PessoaNom[0];
               A40Contratada_PessoaCod = H00P62_A40Contratada_PessoaCod[0];
               A1595Contratada_AreaTrbSrvPdr = H00P62_A1595Contratada_AreaTrbSrvPdr[0];
               n1595Contratada_AreaTrbSrvPdr = H00P62_n1595Contratada_AreaTrbSrvPdr[0];
               A1592Contratada_AreaTrbClcPFnl = H00P62_A1592Contratada_AreaTrbClcPFnl[0];
               n1592Contratada_AreaTrbClcPFnl = H00P62_n1592Contratada_AreaTrbClcPFnl[0];
               A53Contratada_AreaTrabalhoDes = H00P62_A53Contratada_AreaTrabalhoDes[0];
               n53Contratada_AreaTrabalhoDes = H00P62_n53Contratada_AreaTrabalhoDes[0];
               A52Contratada_AreaTrabalhoCod = H00P62_A52Contratada_AreaTrabalhoCod[0];
               A39Contratada_Codigo = H00P62_A39Contratada_Codigo[0];
               A1127Contratada_LogoArquivo = H00P62_A1127Contratada_LogoArquivo[0];
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratada_LogoArquivo_Internalname, "URL", context.PathToRelativeUrl( A1127Contratada_LogoArquivo));
               n1127Contratada_LogoArquivo = H00P62_n1127Contratada_LogoArquivo[0];
               A523Pessoa_Fax = H00P62_A523Pessoa_Fax[0];
               n523Pessoa_Fax = H00P62_n523Pessoa_Fax[0];
               A522Pessoa_Telefone = H00P62_A522Pessoa_Telefone[0];
               n522Pessoa_Telefone = H00P62_n522Pessoa_Telefone[0];
               A521Pessoa_CEP = H00P62_A521Pessoa_CEP[0];
               n521Pessoa_CEP = H00P62_n521Pessoa_CEP[0];
               A519Pessoa_Endereco = H00P62_A519Pessoa_Endereco[0];
               n519Pessoa_Endereco = H00P62_n519Pessoa_Endereco[0];
               A518Pessoa_IE = H00P62_A518Pessoa_IE[0];
               n518Pessoa_IE = H00P62_n518Pessoa_IE[0];
               A42Contratada_PessoaCNPJ = H00P62_A42Contratada_PessoaCNPJ[0];
               n42Contratada_PessoaCNPJ = H00P62_n42Contratada_PessoaCNPJ[0];
               A41Contratada_PessoaNom = H00P62_A41Contratada_PessoaNom[0];
               n41Contratada_PessoaNom = H00P62_n41Contratada_PessoaNom[0];
               A1595Contratada_AreaTrbSrvPdr = H00P62_A1595Contratada_AreaTrbSrvPdr[0];
               n1595Contratada_AreaTrbSrvPdr = H00P62_n1595Contratada_AreaTrbSrvPdr[0];
               A1592Contratada_AreaTrbClcPFnl = H00P62_A1592Contratada_AreaTrbClcPFnl[0];
               n1592Contratada_AreaTrbClcPFnl = H00P62_n1592Contratada_AreaTrbClcPFnl[0];
               A53Contratada_AreaTrabalhoDes = H00P62_A53Contratada_AreaTrabalhoDes[0];
               n53Contratada_AreaTrabalhoDes = H00P62_n53Contratada_AreaTrabalhoDes[0];
               /* Execute user event: E24P62 */
               E24P62 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 84;
            WBP60( ) ;
         }
         nGXsfl_84_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV16DynamicFiltersSelector1 ,
                                              AV17DynamicFiltersOperator1 ,
                                              AV18Contratada_AreaTrabalhoDes1 ,
                                              AV19Contratada_PessoaNom1 ,
                                              AV20DynamicFiltersEnabled2 ,
                                              AV21DynamicFiltersSelector2 ,
                                              AV22DynamicFiltersOperator2 ,
                                              AV23Contratada_AreaTrabalhoDes2 ,
                                              AV24Contratada_PessoaNom2 ,
                                              AV25DynamicFiltersEnabled3 ,
                                              AV26DynamicFiltersSelector3 ,
                                              AV27DynamicFiltersOperator3 ,
                                              AV28Contratada_AreaTrabalhoDes3 ,
                                              AV29Contratada_PessoaNom3 ,
                                              A53Contratada_AreaTrabalhoDes ,
                                              A41Contratada_PessoaNom ,
                                              AV14OrderedBy ,
                                              AV15OrderedDsc ,
                                              A349Contratada_MunicipioCod ,
                                              AV7Contratada_MunicipioCod },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN,
                                              TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT
                                              }
         });
         lV18Contratada_AreaTrabalhoDes1 = StringUtil.Concat( StringUtil.RTrim( AV18Contratada_AreaTrabalhoDes1), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18Contratada_AreaTrabalhoDes1", AV18Contratada_AreaTrabalhoDes1);
         lV18Contratada_AreaTrabalhoDes1 = StringUtil.Concat( StringUtil.RTrim( AV18Contratada_AreaTrabalhoDes1), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18Contratada_AreaTrabalhoDes1", AV18Contratada_AreaTrabalhoDes1);
         lV19Contratada_PessoaNom1 = StringUtil.PadR( StringUtil.RTrim( AV19Contratada_PessoaNom1), 100, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19Contratada_PessoaNom1", AV19Contratada_PessoaNom1);
         lV19Contratada_PessoaNom1 = StringUtil.PadR( StringUtil.RTrim( AV19Contratada_PessoaNom1), 100, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19Contratada_PessoaNom1", AV19Contratada_PessoaNom1);
         lV23Contratada_AreaTrabalhoDes2 = StringUtil.Concat( StringUtil.RTrim( AV23Contratada_AreaTrabalhoDes2), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23Contratada_AreaTrabalhoDes2", AV23Contratada_AreaTrabalhoDes2);
         lV23Contratada_AreaTrabalhoDes2 = StringUtil.Concat( StringUtil.RTrim( AV23Contratada_AreaTrabalhoDes2), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23Contratada_AreaTrabalhoDes2", AV23Contratada_AreaTrabalhoDes2);
         lV24Contratada_PessoaNom2 = StringUtil.PadR( StringUtil.RTrim( AV24Contratada_PessoaNom2), 100, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24Contratada_PessoaNom2", AV24Contratada_PessoaNom2);
         lV24Contratada_PessoaNom2 = StringUtil.PadR( StringUtil.RTrim( AV24Contratada_PessoaNom2), 100, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24Contratada_PessoaNom2", AV24Contratada_PessoaNom2);
         lV28Contratada_AreaTrabalhoDes3 = StringUtil.Concat( StringUtil.RTrim( AV28Contratada_AreaTrabalhoDes3), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28Contratada_AreaTrabalhoDes3", AV28Contratada_AreaTrabalhoDes3);
         lV28Contratada_AreaTrabalhoDes3 = StringUtil.Concat( StringUtil.RTrim( AV28Contratada_AreaTrabalhoDes3), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28Contratada_AreaTrabalhoDes3", AV28Contratada_AreaTrabalhoDes3);
         lV29Contratada_PessoaNom3 = StringUtil.PadR( StringUtil.RTrim( AV29Contratada_PessoaNom3), 100, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29Contratada_PessoaNom3", AV29Contratada_PessoaNom3);
         lV29Contratada_PessoaNom3 = StringUtil.PadR( StringUtil.RTrim( AV29Contratada_PessoaNom3), 100, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29Contratada_PessoaNom3", AV29Contratada_PessoaNom3);
         /* Using cursor H00P63 */
         pr_default.execute(1, new Object[] {AV7Contratada_MunicipioCod, lV18Contratada_AreaTrabalhoDes1, lV18Contratada_AreaTrabalhoDes1, lV19Contratada_PessoaNom1, lV19Contratada_PessoaNom1, lV23Contratada_AreaTrabalhoDes2, lV23Contratada_AreaTrabalhoDes2, lV24Contratada_PessoaNom2, lV24Contratada_PessoaNom2, lV28Contratada_AreaTrabalhoDes3, lV28Contratada_AreaTrabalhoDes3, lV29Contratada_PessoaNom3, lV29Contratada_PessoaNom3});
         GRID_nRecordCount = H00P63_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18Contratada_AreaTrabalhoDes1, AV19Contratada_PessoaNom1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23Contratada_AreaTrabalhoDes2, AV24Contratada_PessoaNom2, AV26DynamicFiltersSelector3, AV27DynamicFiltersOperator3, AV28Contratada_AreaTrabalhoDes3, AV29Contratada_PessoaNom3, AV20DynamicFiltersEnabled2, AV25DynamicFiltersEnabled3, AV7Contratada_MunicipioCod, AV44Pgmname, AV11GridState, AV31DynamicFiltersIgnoreFirst, AV30DynamicFiltersRemoving, AV6WWPContext, A39Contratada_Codigo, A52Contratada_AreaTrabalhoCod, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18Contratada_AreaTrabalhoDes1, AV19Contratada_PessoaNom1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23Contratada_AreaTrabalhoDes2, AV24Contratada_PessoaNom2, AV26DynamicFiltersSelector3, AV27DynamicFiltersOperator3, AV28Contratada_AreaTrabalhoDes3, AV29Contratada_PessoaNom3, AV20DynamicFiltersEnabled2, AV25DynamicFiltersEnabled3, AV7Contratada_MunicipioCod, AV44Pgmname, AV11GridState, AV31DynamicFiltersIgnoreFirst, AV30DynamicFiltersRemoving, AV6WWPContext, A39Contratada_Codigo, A52Contratada_AreaTrabalhoCod, sPrefix) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18Contratada_AreaTrabalhoDes1, AV19Contratada_PessoaNom1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23Contratada_AreaTrabalhoDes2, AV24Contratada_PessoaNom2, AV26DynamicFiltersSelector3, AV27DynamicFiltersOperator3, AV28Contratada_AreaTrabalhoDes3, AV29Contratada_PessoaNom3, AV20DynamicFiltersEnabled2, AV25DynamicFiltersEnabled3, AV7Contratada_MunicipioCod, AV44Pgmname, AV11GridState, AV31DynamicFiltersIgnoreFirst, AV30DynamicFiltersRemoving, AV6WWPContext, A39Contratada_Codigo, A52Contratada_AreaTrabalhoCod, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18Contratada_AreaTrabalhoDes1, AV19Contratada_PessoaNom1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23Contratada_AreaTrabalhoDes2, AV24Contratada_PessoaNom2, AV26DynamicFiltersSelector3, AV27DynamicFiltersOperator3, AV28Contratada_AreaTrabalhoDes3, AV29Contratada_PessoaNom3, AV20DynamicFiltersEnabled2, AV25DynamicFiltersEnabled3, AV7Contratada_MunicipioCod, AV44Pgmname, AV11GridState, AV31DynamicFiltersIgnoreFirst, AV30DynamicFiltersRemoving, AV6WWPContext, A39Contratada_Codigo, A52Contratada_AreaTrabalhoCod, sPrefix) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18Contratada_AreaTrabalhoDes1, AV19Contratada_PessoaNom1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23Contratada_AreaTrabalhoDes2, AV24Contratada_PessoaNom2, AV26DynamicFiltersSelector3, AV27DynamicFiltersOperator3, AV28Contratada_AreaTrabalhoDes3, AV29Contratada_PessoaNom3, AV20DynamicFiltersEnabled2, AV25DynamicFiltersEnabled3, AV7Contratada_MunicipioCod, AV44Pgmname, AV11GridState, AV31DynamicFiltersIgnoreFirst, AV30DynamicFiltersRemoving, AV6WWPContext, A39Contratada_Codigo, A52Contratada_AreaTrabalhoCod, sPrefix) ;
         }
         return (int)(0) ;
      }

      protected void STRUPP60( )
      {
         /* Before Start, stand alone formulas. */
         AV44Pgmname = "MunicipioContratadaWC";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E22P62 */
         E22P62 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV14OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV16DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
            cmbavDynamicfiltersoperator1.Name = cmbavDynamicfiltersoperator1_Internalname;
            cmbavDynamicfiltersoperator1.CurrentValue = cgiGet( cmbavDynamicfiltersoperator1_Internalname);
            AV17DynamicFiltersOperator1 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator1_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
            AV18Contratada_AreaTrabalhoDes1 = StringUtil.Upper( cgiGet( edtavContratada_areatrabalhodes1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18Contratada_AreaTrabalhoDes1", AV18Contratada_AreaTrabalhoDes1);
            AV19Contratada_PessoaNom1 = StringUtil.Upper( cgiGet( edtavContratada_pessoanom1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19Contratada_PessoaNom1", AV19Contratada_PessoaNom1);
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV21DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
            cmbavDynamicfiltersoperator2.Name = cmbavDynamicfiltersoperator2_Internalname;
            cmbavDynamicfiltersoperator2.CurrentValue = cgiGet( cmbavDynamicfiltersoperator2_Internalname);
            AV22DynamicFiltersOperator2 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator2_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
            AV23Contratada_AreaTrabalhoDes2 = StringUtil.Upper( cgiGet( edtavContratada_areatrabalhodes2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23Contratada_AreaTrabalhoDes2", AV23Contratada_AreaTrabalhoDes2);
            AV24Contratada_PessoaNom2 = StringUtil.Upper( cgiGet( edtavContratada_pessoanom2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24Contratada_PessoaNom2", AV24Contratada_PessoaNom2);
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV26DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26DynamicFiltersSelector3", AV26DynamicFiltersSelector3);
            cmbavDynamicfiltersoperator3.Name = cmbavDynamicfiltersoperator3_Internalname;
            cmbavDynamicfiltersoperator3.CurrentValue = cgiGet( cmbavDynamicfiltersoperator3_Internalname);
            AV27DynamicFiltersOperator3 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator3_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0)));
            AV28Contratada_AreaTrabalhoDes3 = StringUtil.Upper( cgiGet( edtavContratada_areatrabalhodes3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28Contratada_AreaTrabalhoDes3", AV28Contratada_AreaTrabalhoDes3);
            AV29Contratada_PessoaNom3 = StringUtil.Upper( cgiGet( edtavContratada_pessoanom3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29Contratada_PessoaNom3", AV29Contratada_PessoaNom3);
            A349Contratada_MunicipioCod = (int)(context.localUtil.CToN( cgiGet( edtContratada_MunicipioCod_Internalname), ",", "."));
            n349Contratada_MunicipioCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A349Contratada_MunicipioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A349Contratada_MunicipioCod), 6, 0)));
            AV20DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
            AV25DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25DynamicFiltersEnabled3", AV25DynamicFiltersEnabled3);
            /* Read saved values. */
            nRC_GXsfl_84 = (short)(context.localUtil.CToN( cgiGet( sPrefix+"nRC_GXsfl_84"), ",", "."));
            AV37GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"vGRIDCURRENTPAGE"), ",", "."));
            AV38GridPageCount = (long)(context.localUtil.CToN( cgiGet( sPrefix+"vGRIDPAGECOUNT"), ",", "."));
            wcpOAV7Contratada_MunicipioCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV7Contratada_MunicipioCod"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( sPrefix+"GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Emptygridcaption");
            Gridpaginationbar_Selectedpage = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Selectedpage");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV14OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV15OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR1"), AV16DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV17DynamicFiltersOperator1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vCONTRATADA_AREATRABALHODES1"), AV18Contratada_AreaTrabalhoDes1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vCONTRATADA_PESSOANOM1"), AV19Contratada_PessoaNom1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR2"), AV21DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV22DynamicFiltersOperator2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vCONTRATADA_AREATRABALHODES2"), AV23Contratada_AreaTrabalhoDes2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vCONTRATADA_PESSOANOM2"), AV24Contratada_PessoaNom2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR3"), AV26DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV27DynamicFiltersOperator3 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vCONTRATADA_AREATRABALHODES3"), AV28Contratada_AreaTrabalhoDes3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vCONTRATADA_PESSOANOM3"), AV29Contratada_PessoaNom3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSENABLED2")) != AV20DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSENABLED3")) != AV25DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E22P62 */
         E22P62 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E22P62( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV16DynamicFiltersSelector1 = "CONTRATADA_AREATRABALHODES";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV21DynamicFiltersSelector2 = "CONTRATADA_AREATRABALHODES";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersSelector3 = "CONTRATADA_AREATRABALHODES";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26DynamicFiltersSelector3", AV26DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         edtContratada_MunicipioCod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratada_MunicipioCod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratada_MunicipioCod_Visible), 5, 0)));
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "de Trabalho", 0);
         cmbavOrderedby.addItem("2", "de Trabalho", 0);
         cmbavOrderedby.addItem("3", "P Final", 0);
         cmbavOrderedby.addItem("4", "padr�o", 0);
         cmbavOrderedby.addItem("5", "da Pessoa", 0);
         cmbavOrderedby.addItem("6", "Nome", 0);
         cmbavOrderedby.addItem("7", "da Pessoa", 0);
         cmbavOrderedby.addItem("8", "Inscri��o Estadual", 0);
         cmbavOrderedby.addItem("9", "Endere�o", 0);
         cmbavOrderedby.addItem("10", "CEP", 0);
         cmbavOrderedby.addItem("11", "Telefone", 0);
         cmbavOrderedby.addItem("12", "Fax", 0);
         cmbavOrderedby.addItem("13", "Sigla", 0);
         cmbavOrderedby.addItem("14", "Tipo", 0);
         cmbavOrderedby.addItem("15", "Banco", 0);
         cmbavOrderedby.addItem("16", "N�mero", 0);
         cmbavOrderedby.addItem("17", "Ag�ncia", 0);
         cmbavOrderedby.addItem("18", "N�mero", 0);
         cmbavOrderedby.addItem("19", "Corrente", 0);
         cmbavOrderedby.addItem("20", "OS", 0);
         cmbavOrderedby.addItem("21", "SS", 0);
         cmbavOrderedby.addItem("22", "Lote", 0);
         if ( AV14OrderedBy < 1 )
         {
            AV14OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
      }

      protected void E23P62( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         cmbavDynamicfiltersoperator1.removeAllItems();
         if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTRATADA_AREATRABALHODES") == 0 )
         {
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
         }
         else if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTRATADA_PESSOANOM") == 0 )
         {
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
         }
         if ( AV20DynamicFiltersEnabled2 )
         {
            cmbavDynamicfiltersoperator2.removeAllItems();
            if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTRATADA_AREATRABALHODES") == 0 )
            {
               cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
               cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            }
            else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTRATADA_PESSOANOM") == 0 )
            {
               cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
               cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            }
            if ( AV25DynamicFiltersEnabled3 )
            {
               cmbavDynamicfiltersoperator3.removeAllItems();
               if ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "CONTRATADA_AREATRABALHODES") == 0 )
               {
                  cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
                  cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
               }
               else if ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "CONTRATADA_PESSOANOM") == 0 )
               {
                  cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
                  cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
               }
            }
         }
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtContratada_AreaTrabalhoCod_Titleformat = 2;
         edtContratada_AreaTrabalhoCod_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 1);' >%5</span>", ((AV14OrderedBy==1) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "de Trabalho", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratada_AreaTrabalhoCod_Internalname, "Title", edtContratada_AreaTrabalhoCod_Title);
         edtContratada_AreaTrabalhoDes_Titleformat = 2;
         edtContratada_AreaTrabalhoDes_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 2);' >%5</span>", ((AV14OrderedBy==2) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "de Trabalho", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratada_AreaTrabalhoDes_Internalname, "Title", edtContratada_AreaTrabalhoDes_Title);
         cmbContratada_AreaTrbClcPFnl_Titleformat = 2;
         cmbContratada_AreaTrbClcPFnl.Title.Text = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 3);' >%5</span>", ((AV14OrderedBy==3) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "P Final", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbContratada_AreaTrbClcPFnl_Internalname, "Title", cmbContratada_AreaTrbClcPFnl.Title.Text);
         edtContratada_AreaTrbSrvPdr_Titleformat = 2;
         edtContratada_AreaTrbSrvPdr_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 4);' >%5</span>", ((AV14OrderedBy==4) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "padr�o", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratada_AreaTrbSrvPdr_Internalname, "Title", edtContratada_AreaTrbSrvPdr_Title);
         edtContratada_PessoaCod_Titleformat = 2;
         edtContratada_PessoaCod_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 5);' >%5</span>", ((AV14OrderedBy==5) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "da Pessoa", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratada_PessoaCod_Internalname, "Title", edtContratada_PessoaCod_Title);
         edtContratada_PessoaNom_Titleformat = 2;
         edtContratada_PessoaNom_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 6);' >%5</span>", ((AV14OrderedBy==6) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Nome", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratada_PessoaNom_Internalname, "Title", edtContratada_PessoaNom_Title);
         edtContratada_PessoaCNPJ_Titleformat = 2;
         edtContratada_PessoaCNPJ_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 7);' >%5</span>", ((AV14OrderedBy==7) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "da Pessoa", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratada_PessoaCNPJ_Internalname, "Title", edtContratada_PessoaCNPJ_Title);
         edtPessoa_IE_Titleformat = 2;
         edtPessoa_IE_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 8);' >%5</span>", ((AV14OrderedBy==8) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Inscri��o Estadual", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtPessoa_IE_Internalname, "Title", edtPessoa_IE_Title);
         edtPessoa_Endereco_Titleformat = 2;
         edtPessoa_Endereco_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 9);' >%5</span>", ((AV14OrderedBy==9) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Endere�o", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtPessoa_Endereco_Internalname, "Title", edtPessoa_Endereco_Title);
         edtPessoa_CEP_Titleformat = 2;
         edtPessoa_CEP_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 10);' >%5</span>", ((AV14OrderedBy==10) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "CEP", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtPessoa_CEP_Internalname, "Title", edtPessoa_CEP_Title);
         edtPessoa_Telefone_Titleformat = 2;
         edtPessoa_Telefone_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 11);' >%5</span>", ((AV14OrderedBy==11) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Telefone", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtPessoa_Telefone_Internalname, "Title", edtPessoa_Telefone_Title);
         edtPessoa_Fax_Titleformat = 2;
         edtPessoa_Fax_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 12);' >%5</span>", ((AV14OrderedBy==12) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Fax", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtPessoa_Fax_Internalname, "Title", edtPessoa_Fax_Title);
         edtContratada_Sigla_Titleformat = 2;
         edtContratada_Sigla_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 13);' >%5</span>", ((AV14OrderedBy==13) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Sigla", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratada_Sigla_Internalname, "Title", edtContratada_Sigla_Title);
         cmbContratada_TipoFabrica_Titleformat = 2;
         cmbContratada_TipoFabrica.Title.Text = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 14);' >%5</span>", ((AV14OrderedBy==14) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Tipo", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbContratada_TipoFabrica_Internalname, "Title", cmbContratada_TipoFabrica.Title.Text);
         edtContratada_BancoNome_Titleformat = 2;
         edtContratada_BancoNome_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 15);' >%5</span>", ((AV14OrderedBy==15) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Banco", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratada_BancoNome_Internalname, "Title", edtContratada_BancoNome_Title);
         edtContratada_BancoNro_Titleformat = 2;
         edtContratada_BancoNro_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 16);' >%5</span>", ((AV14OrderedBy==16) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "N�mero", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratada_BancoNro_Internalname, "Title", edtContratada_BancoNro_Title);
         edtContratada_AgenciaNome_Titleformat = 2;
         edtContratada_AgenciaNome_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 17);' >%5</span>", ((AV14OrderedBy==17) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Ag�ncia", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratada_AgenciaNome_Internalname, "Title", edtContratada_AgenciaNome_Title);
         edtContratada_AgenciaNro_Titleformat = 2;
         edtContratada_AgenciaNro_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 18);' >%5</span>", ((AV14OrderedBy==18) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "N�mero", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratada_AgenciaNro_Internalname, "Title", edtContratada_AgenciaNro_Title);
         edtContratada_ContaCorrente_Titleformat = 2;
         edtContratada_ContaCorrente_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 19);' >%5</span>", ((AV14OrderedBy==19) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Corrente", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratada_ContaCorrente_Internalname, "Title", edtContratada_ContaCorrente_Title);
         edtContratada_OS_Titleformat = 2;
         edtContratada_OS_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 20);' >%5</span>", ((AV14OrderedBy==20) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "OS", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratada_OS_Internalname, "Title", edtContratada_OS_Title);
         edtContratada_SS_Titleformat = 2;
         edtContratada_SS_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 21);' >%5</span>", ((AV14OrderedBy==21) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "SS", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratada_SS_Internalname, "Title", edtContratada_SS_Title);
         edtContratada_Lote_Titleformat = 2;
         edtContratada_Lote_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 22);' >%5</span>", ((AV14OrderedBy==22) ? (AV15OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Lote", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratada_Lote_Internalname, "Title", edtContratada_Lote_Title);
         AV37GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV37GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV37GridCurrentPage), 10, 0)));
         AV38GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV38GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38GridPageCount), 10, 0)));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV6WWPContext", AV6WWPContext);
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV11GridState", AV11GridState);
      }

      protected void E11P62( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV36PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV36PageToGo) ;
         }
      }

      private void E24P62( )
      {
         /* Grid_Load Routine */
         edtavUpdate_Tooltiptext = "Modifica";
         if ( AV6WWPContext.gxTpr_Update )
         {
            edtavUpdate_Link = formatLink("contratada.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A39Contratada_Codigo);
            AV32Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavUpdate_Internalname, AV32Update);
            AV41Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
            edtavUpdate_Enabled = 1;
         }
         else
         {
            edtavUpdate_Link = "";
            AV32Update = context.GetImagePath( "0211baea-a1e2-4bb5-849c-f3afb69b09ee", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavUpdate_Internalname, AV32Update);
            AV41Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "0211baea-a1e2-4bb5-849c-f3afb69b09ee", "", context.GetTheme( )));
            edtavUpdate_Enabled = 0;
         }
         edtavDelete_Tooltiptext = "Eliminar";
         if ( AV6WWPContext.gxTpr_Delete )
         {
            edtavDelete_Link = formatLink("contratada.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A39Contratada_Codigo);
            AV33Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavDelete_Internalname, AV33Delete);
            AV42Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
            edtavDelete_Enabled = 1;
         }
         else
         {
            edtavDelete_Link = "";
            AV33Delete = context.GetImagePath( "138fdfa8-ae9b-4664-8007-e04d47468418", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavDelete_Internalname, AV33Delete);
            AV42Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "138fdfa8-ae9b-4664-8007-e04d47468418", "", context.GetTheme( )));
            edtavDelete_Enabled = 0;
         }
         edtavDisplay_Tooltiptext = "Mostrar";
         if ( AV6WWPContext.gxTpr_Display )
         {
            edtavDisplay_Link = formatLink("viewcontratada.aspx") + "?" + UrlEncode("" +A39Contratada_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
            AV34Display = context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavDisplay_Internalname, AV34Display);
            AV43Display_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( )));
            edtavDisplay_Enabled = 1;
         }
         else
         {
            edtavDisplay_Link = "";
            AV34Display = context.GetImagePath( "52c6f766-90b4-4f77-87a4-fb602a2ea1b6", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavDisplay_Internalname, AV34Display);
            AV43Display_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "52c6f766-90b4-4f77-87a4-fb602a2ea1b6", "", context.GetTheme( )));
            edtavDisplay_Enabled = 0;
         }
         edtContratada_AreaTrabalhoDes_Link = formatLink("viewareatrabalho.aspx") + "?" + UrlEncode("" +A52Contratada_AreaTrabalhoCod) + "," + UrlEncode(StringUtil.RTrim(""));
         edtContratada_PessoaNom_Link = formatLink("viewcontratada.aspx") + "?" + UrlEncode("" +A39Contratada_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 84;
         }
         sendrow_842( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_84_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(84, GridRow);
         }
      }

      protected void E12P62( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefreshCmp(sPrefix);
      }

      protected void E17P62( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV20DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18Contratada_AreaTrabalhoDes1, AV19Contratada_PessoaNom1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23Contratada_AreaTrabalhoDes2, AV24Contratada_PessoaNom2, AV26DynamicFiltersSelector3, AV27DynamicFiltersOperator3, AV28Contratada_AreaTrabalhoDes3, AV29Contratada_PessoaNom3, AV20DynamicFiltersEnabled2, AV25DynamicFiltersEnabled3, AV7Contratada_MunicipioCod, AV44Pgmname, AV11GridState, AV31DynamicFiltersIgnoreFirst, AV30DynamicFiltersRemoving, AV6WWPContext, A39Contratada_Codigo, A52Contratada_AreaTrabalhoCod, sPrefix) ;
      }

      protected void E13P62( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV30DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30DynamicFiltersRemoving", AV30DynamicFiltersRemoving);
         AV31DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31DynamicFiltersIgnoreFirst", AV31DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV30DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30DynamicFiltersRemoving", AV30DynamicFiltersRemoving);
         AV31DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31DynamicFiltersIgnoreFirst", AV31DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18Contratada_AreaTrabalhoDes1, AV19Contratada_PessoaNom1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23Contratada_AreaTrabalhoDes2, AV24Contratada_PessoaNom2, AV26DynamicFiltersSelector3, AV27DynamicFiltersOperator3, AV28Contratada_AreaTrabalhoDes3, AV29Contratada_PessoaNom3, AV20DynamicFiltersEnabled2, AV25DynamicFiltersEnabled3, AV7Contratada_MunicipioCod, AV44Pgmname, AV11GridState, AV31DynamicFiltersIgnoreFirst, AV30DynamicFiltersRemoving, AV6WWPContext, A39Contratada_Codigo, A52Contratada_AreaTrabalhoCod, sPrefix) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV11GridState", AV11GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV26DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E18P62( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         AV17DynamicFiltersOperator1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E19P62( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV25DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25DynamicFiltersEnabled3", AV25DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18Contratada_AreaTrabalhoDes1, AV19Contratada_PessoaNom1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23Contratada_AreaTrabalhoDes2, AV24Contratada_PessoaNom2, AV26DynamicFiltersSelector3, AV27DynamicFiltersOperator3, AV28Contratada_AreaTrabalhoDes3, AV29Contratada_PessoaNom3, AV20DynamicFiltersEnabled2, AV25DynamicFiltersEnabled3, AV7Contratada_MunicipioCod, AV44Pgmname, AV11GridState, AV31DynamicFiltersIgnoreFirst, AV30DynamicFiltersRemoving, AV6WWPContext, A39Contratada_Codigo, A52Contratada_AreaTrabalhoCod, sPrefix) ;
      }

      protected void E14P62( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV30DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30DynamicFiltersRemoving", AV30DynamicFiltersRemoving);
         AV20DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV30DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30DynamicFiltersRemoving", AV30DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18Contratada_AreaTrabalhoDes1, AV19Contratada_PessoaNom1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23Contratada_AreaTrabalhoDes2, AV24Contratada_PessoaNom2, AV26DynamicFiltersSelector3, AV27DynamicFiltersOperator3, AV28Contratada_AreaTrabalhoDes3, AV29Contratada_PessoaNom3, AV20DynamicFiltersEnabled2, AV25DynamicFiltersEnabled3, AV7Contratada_MunicipioCod, AV44Pgmname, AV11GridState, AV31DynamicFiltersIgnoreFirst, AV30DynamicFiltersRemoving, AV6WWPContext, A39Contratada_Codigo, A52Contratada_AreaTrabalhoCod, sPrefix) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV11GridState", AV11GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV26DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E20P62( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         AV22DynamicFiltersOperator2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
      }

      protected void E15P62( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV30DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30DynamicFiltersRemoving", AV30DynamicFiltersRemoving);
         AV25DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25DynamicFiltersEnabled3", AV25DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV30DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30DynamicFiltersRemoving", AV30DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18Contratada_AreaTrabalhoDes1, AV19Contratada_PessoaNom1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23Contratada_AreaTrabalhoDes2, AV24Contratada_PessoaNom2, AV26DynamicFiltersSelector3, AV27DynamicFiltersOperator3, AV28Contratada_AreaTrabalhoDes3, AV29Contratada_PessoaNom3, AV20DynamicFiltersEnabled2, AV25DynamicFiltersEnabled3, AV7Contratada_MunicipioCod, AV44Pgmname, AV11GridState, AV31DynamicFiltersIgnoreFirst, AV30DynamicFiltersRemoving, AV6WWPContext, A39Contratada_Codigo, A52Contratada_AreaTrabalhoCod, sPrefix) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV11GridState", AV11GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV26DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E21P62( )
      {
         /* Dynamicfiltersselector3_Click Routine */
         AV27DynamicFiltersOperator3 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
      }

      protected void E16P62( )
      {
         /* 'DoInsert' Routine */
         context.wjLoc = formatLink("contratada.aspx") + "?" + UrlEncode(StringUtil.RTrim("INS")) + "," + UrlEncode("" +0);
         context.wjLocDisableFrm = 1;
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavContratada_areatrabalhodes1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContratada_areatrabalhodes1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratada_areatrabalhodes1_Visible), 5, 0)));
         edtavContratada_pessoanom1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContratada_pessoanom1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratada_pessoanom1_Visible), 5, 0)));
         cmbavDynamicfiltersoperator1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTRATADA_AREATRABALHODES") == 0 )
         {
            edtavContratada_areatrabalhodes1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContratada_areatrabalhodes1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratada_areatrabalhodes1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTRATADA_PESSOANOM") == 0 )
         {
            edtavContratada_pessoanom1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContratada_pessoanom1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratada_pessoanom1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         edtavContratada_areatrabalhodes2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContratada_areatrabalhodes2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratada_areatrabalhodes2_Visible), 5, 0)));
         edtavContratada_pessoanom2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContratada_pessoanom2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratada_pessoanom2_Visible), 5, 0)));
         cmbavDynamicfiltersoperator2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTRATADA_AREATRABALHODES") == 0 )
         {
            edtavContratada_areatrabalhodes2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContratada_areatrabalhodes2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratada_areatrabalhodes2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTRATADA_PESSOANOM") == 0 )
         {
            edtavContratada_pessoanom2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContratada_pessoanom2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratada_pessoanom2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         edtavContratada_areatrabalhodes3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContratada_areatrabalhodes3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratada_areatrabalhodes3_Visible), 5, 0)));
         edtavContratada_pessoanom3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContratada_pessoanom3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratada_pessoanom3_Visible), 5, 0)));
         cmbavDynamicfiltersoperator3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "CONTRATADA_AREATRABALHODES") == 0 )
         {
            edtavContratada_areatrabalhodes3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContratada_areatrabalhodes3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratada_areatrabalhodes3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "CONTRATADA_PESSOANOM") == 0 )
         {
            edtavContratada_pessoanom3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContratada_pessoanom3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratada_pessoanom3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
      }

      protected void S182( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV20DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
         AV21DynamicFiltersSelector2 = "CONTRATADA_AREATRABALHODES";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
         AV22DynamicFiltersOperator2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
         AV23Contratada_AreaTrabalhoDes2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23Contratada_AreaTrabalhoDes2", AV23Contratada_AreaTrabalhoDes2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV25DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25DynamicFiltersEnabled3", AV25DynamicFiltersEnabled3);
         AV26DynamicFiltersSelector3 = "CONTRATADA_AREATRABALHODES";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26DynamicFiltersSelector3", AV26DynamicFiltersSelector3);
         AV27DynamicFiltersOperator3 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0)));
         AV28Contratada_AreaTrabalhoDes3 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28Contratada_AreaTrabalhoDes3", AV28Contratada_AreaTrabalhoDes3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S152( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV35Session.Get(AV44Pgmname+"GridState"), "") == 0 )
         {
            AV11GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV44Pgmname+"GridState"), "");
         }
         else
         {
            AV11GridState.FromXml(AV35Session.Get(AV44Pgmname+"GridState"), "");
         }
         AV14OrderedBy = AV11GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
         AV15OrderedDsc = AV11GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S192( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         if ( AV11GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV13GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV11GridState.gxTpr_Dynamicfilters.Item(1));
            AV16DynamicFiltersSelector1 = AV13GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTRATADA_AREATRABALHODES") == 0 )
            {
               AV17DynamicFiltersOperator1 = AV13GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
               AV18Contratada_AreaTrabalhoDes1 = AV13GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18Contratada_AreaTrabalhoDes1", AV18Contratada_AreaTrabalhoDes1);
            }
            else if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTRATADA_PESSOANOM") == 0 )
            {
               AV17DynamicFiltersOperator1 = AV13GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
               AV19Contratada_PessoaNom1 = AV13GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19Contratada_PessoaNom1", AV19Contratada_PessoaNom1);
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV11GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV20DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
               AV13GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV11GridState.gxTpr_Dynamicfilters.Item(2));
               AV21DynamicFiltersSelector2 = AV13GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTRATADA_AREATRABALHODES") == 0 )
               {
                  AV22DynamicFiltersOperator2 = AV13GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
                  AV23Contratada_AreaTrabalhoDes2 = AV13GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23Contratada_AreaTrabalhoDes2", AV23Contratada_AreaTrabalhoDes2);
               }
               else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTRATADA_PESSOANOM") == 0 )
               {
                  AV22DynamicFiltersOperator2 = AV13GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
                  AV24Contratada_PessoaNom2 = AV13GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24Contratada_PessoaNom2", AV24Contratada_PessoaNom2);
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV11GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV25DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25DynamicFiltersEnabled3", AV25DynamicFiltersEnabled3);
                  AV13GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV11GridState.gxTpr_Dynamicfilters.Item(3));
                  AV26DynamicFiltersSelector3 = AV13GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26DynamicFiltersSelector3", AV26DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "CONTRATADA_AREATRABALHODES") == 0 )
                  {
                     AV27DynamicFiltersOperator3 = AV13GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0)));
                     AV28Contratada_AreaTrabalhoDes3 = AV13GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28Contratada_AreaTrabalhoDes3", AV28Contratada_AreaTrabalhoDes3);
                  }
                  else if ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "CONTRATADA_PESSOANOM") == 0 )
                  {
                     AV27DynamicFiltersOperator3 = AV13GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0)));
                     AV29Contratada_PessoaNom3 = AV13GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29Contratada_PessoaNom3", AV29Contratada_PessoaNom3);
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S132 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV30DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S162( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV11GridState.FromXml(AV35Session.Get(AV44Pgmname+"GridState"), "");
         AV11GridState.gxTpr_Orderedby = AV14OrderedBy;
         AV11GridState.gxTpr_Ordereddsc = AV15OrderedDsc;
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         new wwpbaseobjects.savegridstate(context ).execute(  AV44Pgmname+"GridState",  AV11GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_MeetrikaVs3")) ;
      }

      protected void S172( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV11GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV31DynamicFiltersIgnoreFirst )
         {
            AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV13GridStateDynamicFilter.gxTpr_Selected = AV16DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTRATADA_AREATRABALHODES") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV18Contratada_AreaTrabalhoDes1)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV18Contratada_AreaTrabalhoDes1;
               AV13GridStateDynamicFilter.gxTpr_Operator = AV17DynamicFiltersOperator1;
            }
            else if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTRATADA_PESSOANOM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV19Contratada_PessoaNom1)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV19Contratada_PessoaNom1;
               AV13GridStateDynamicFilter.gxTpr_Operator = AV17DynamicFiltersOperator1;
            }
            if ( AV30DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV13GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV11GridState.gxTpr_Dynamicfilters.Add(AV13GridStateDynamicFilter, 0);
            }
         }
         if ( AV20DynamicFiltersEnabled2 )
         {
            AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV13GridStateDynamicFilter.gxTpr_Selected = AV21DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTRATADA_AREATRABALHODES") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV23Contratada_AreaTrabalhoDes2)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV23Contratada_AreaTrabalhoDes2;
               AV13GridStateDynamicFilter.gxTpr_Operator = AV22DynamicFiltersOperator2;
            }
            else if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTRATADA_PESSOANOM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV24Contratada_PessoaNom2)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV24Contratada_PessoaNom2;
               AV13GridStateDynamicFilter.gxTpr_Operator = AV22DynamicFiltersOperator2;
            }
            if ( AV30DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV13GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV11GridState.gxTpr_Dynamicfilters.Add(AV13GridStateDynamicFilter, 0);
            }
         }
         if ( AV25DynamicFiltersEnabled3 )
         {
            AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV13GridStateDynamicFilter.gxTpr_Selected = AV26DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "CONTRATADA_AREATRABALHODES") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV28Contratada_AreaTrabalhoDes3)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV28Contratada_AreaTrabalhoDes3;
               AV13GridStateDynamicFilter.gxTpr_Operator = AV27DynamicFiltersOperator3;
            }
            else if ( ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "CONTRATADA_PESSOANOM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV29Contratada_PessoaNom3)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV29Contratada_PessoaNom3;
               AV13GridStateDynamicFilter.gxTpr_Operator = AV27DynamicFiltersOperator3;
            }
            if ( AV30DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV13GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV11GridState.gxTpr_Dynamicfilters.Add(AV13GridStateDynamicFilter, 0);
            }
         }
      }

      protected void S142( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV9TrnContext.gxTpr_Callerobject = AV44Pgmname;
         AV9TrnContext.gxTpr_Callerondelete = true;
         AV9TrnContext.gxTpr_Callerurl = AV8HTTPRequest.ScriptName+"?"+AV8HTTPRequest.QueryString;
         AV9TrnContext.gxTpr_Transactionname = "Contratada";
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10TrnContextAtt.gxTpr_Attributename = "Contratada_MunicipioCod";
         AV10TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7Contratada_MunicipioCod), 6, 0);
         AV9TrnContext.gxTpr_Attributes.Add(AV10TrnContextAtt, 0);
         AV35Session.Set("TrnContext", AV9TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_MeetrikaVs3"));
      }

      protected void wb_table1_2_P62( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='TableSearchCell'>") ;
            wb_table2_8_P62( true) ;
         }
         else
         {
            wb_table2_8_P62( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_P62e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_81_P62( true) ;
         }
         else
         {
            wb_table3_81_P62( false) ;
         }
         return  ;
      }

      protected void wb_table3_81_P62e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_P62e( true) ;
         }
         else
         {
            wb_table1_2_P62e( false) ;
         }
      }

      protected void wb_table3_81_P62( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"DivS\" data-gxgridid=\"84\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Contratada") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratada_AreaTrabalhoCod_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratada_AreaTrabalhoCod_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratada_AreaTrabalhoCod_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratada_AreaTrabalhoDes_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratada_AreaTrabalhoDes_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratada_AreaTrabalhoDes_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbContratada_AreaTrbClcPFnl_Titleformat == 0 )
               {
                  context.SendWebValue( cmbContratada_AreaTrbClcPFnl.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbContratada_AreaTrbClcPFnl.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratada_AreaTrbSrvPdr_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratada_AreaTrbSrvPdr_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratada_AreaTrbSrvPdr_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratada_PessoaCod_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratada_PessoaCod_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratada_PessoaCod_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratada_PessoaNom_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratada_PessoaNom_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratada_PessoaNom_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratada_PessoaCNPJ_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratada_PessoaCNPJ_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratada_PessoaCNPJ_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtPessoa_IE_Titleformat == 0 )
               {
                  context.SendWebValue( edtPessoa_IE_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtPessoa_IE_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtPessoa_Endereco_Titleformat == 0 )
               {
                  context.SendWebValue( edtPessoa_Endereco_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtPessoa_Endereco_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtPessoa_CEP_Titleformat == 0 )
               {
                  context.SendWebValue( edtPessoa_CEP_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtPessoa_CEP_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtPessoa_Telefone_Titleformat == 0 )
               {
                  context.SendWebValue( edtPessoa_Telefone_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtPessoa_Telefone_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtPessoa_Fax_Titleformat == 0 )
               {
                  context.SendWebValue( edtPessoa_Fax_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtPessoa_Fax_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratada_Sigla_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratada_Sigla_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratada_Sigla_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbContratada_TipoFabrica_Titleformat == 0 )
               {
                  context.SendWebValue( cmbContratada_TipoFabrica.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbContratada_TipoFabrica.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratada_BancoNome_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratada_BancoNome_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratada_BancoNome_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratada_BancoNro_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratada_BancoNro_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratada_BancoNro_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratada_AgenciaNome_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratada_AgenciaNome_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratada_AgenciaNome_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratada_AgenciaNro_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratada_AgenciaNro_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratada_AgenciaNro_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratada_ContaCorrente_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratada_ContaCorrente_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratada_ContaCorrente_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(58), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratada_OS_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratada_OS_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratada_OS_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(58), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratada_SS_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratada_SS_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratada_SS_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(34), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratada_Lote_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratada_Lote_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratada_Lote_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Logomarca") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "de arquivo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", sPrefix);
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV32Update));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavUpdate_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavUpdate_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV33Delete));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDelete_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDelete_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV34Display));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDisplay_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDisplay_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDisplay_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A39Contratada_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A52Contratada_AreaTrabalhoCod), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratada_AreaTrabalhoCod_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratada_AreaTrabalhoCod_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A53Contratada_AreaTrabalhoDes);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratada_AreaTrabalhoDes_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratada_AreaTrabalhoDes_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtContratada_AreaTrabalhoDes_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A1592Contratada_AreaTrbClcPFnl));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbContratada_AreaTrbClcPFnl.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbContratada_AreaTrbClcPFnl_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1595Contratada_AreaTrbSrvPdr), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratada_AreaTrbSrvPdr_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratada_AreaTrbSrvPdr_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A40Contratada_PessoaCod), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratada_PessoaCod_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratada_PessoaCod_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A41Contratada_PessoaNom));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratada_PessoaNom_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratada_PessoaNom_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtContratada_PessoaNom_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A42Contratada_PessoaCNPJ);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratada_PessoaCNPJ_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratada_PessoaCNPJ_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A518Pessoa_IE));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtPessoa_IE_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtPessoa_IE_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A519Pessoa_Endereco);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtPessoa_Endereco_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtPessoa_Endereco_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A521Pessoa_CEP));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtPessoa_CEP_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtPessoa_CEP_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A522Pessoa_Telefone));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtPessoa_Telefone_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtPessoa_Telefone_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A523Pessoa_Fax));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtPessoa_Fax_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtPessoa_Fax_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A438Contratada_Sigla));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratada_Sigla_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratada_Sigla_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A516Contratada_TipoFabrica));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbContratada_TipoFabrica.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbContratada_TipoFabrica_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A342Contratada_BancoNome));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratada_BancoNome_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratada_BancoNome_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A343Contratada_BancoNro));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratada_BancoNro_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratada_BancoNro_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A344Contratada_AgenciaNome));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratada_AgenciaNome_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratada_AgenciaNome_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A345Contratada_AgenciaNro));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratada_AgenciaNro_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratada_AgenciaNro_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A51Contratada_ContaCorrente));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratada_ContaCorrente_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratada_ContaCorrente_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A524Contratada_OS), 8, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratada_OS_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratada_OS_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1451Contratada_SS), 8, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratada_SS_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratada_SS_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A530Contratada_Lote), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratada_Lote_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratada_Lote_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A1127Contratada_LogoArquivo);
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A1128Contratada_LogoNomeArq));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 84 )
         {
            wbEnd = 0;
            nRC_GXsfl_84 = (short)(nGXsfl_84_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid(sPrefix+"_"+"Grid", GridContainer);
               if ( ! isAjaxCallMode( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+sPrefix+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_81_P62e( true) ;
         }
         else
         {
            wb_table3_81_P62e( false) ;
         }
      }

      protected void wb_table2_8_P62( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablesearch_Internalname, tblTablesearch_Internalname, "", "TableSearch", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='Width100'>") ;
            wb_table4_11_P62( true) ;
         }
         else
         {
            wb_table4_11_P62( false) ;
         }
         return  ;
      }

      protected void wb_table4_11_P62e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_MunicipioContratadaWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'" + sPrefix + "',false,'" + sGXsfl_84_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,18);\"", "", true, "HLP_MunicipioContratadaWC.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'" + sPrefix + "',false,'" + sGXsfl_84_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV15OrderedDsc), StringUtil.BoolToStr( AV15OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,19);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_MunicipioContratadaWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table5_21_P62( true) ;
         }
         else
         {
            wb_table5_21_P62( false) ;
         }
         return  ;
      }

      protected void wb_table5_21_P62e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_P62e( true) ;
         }
         else
         {
            wb_table2_8_P62e( false) ;
         }
      }

      protected void wb_table5_21_P62( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataFilterContentCell'>") ;
            wb_table6_24_P62( true) ;
         }
         else
         {
            wb_table6_24_P62( false) ;
         }
         return  ;
      }

      protected void wb_table6_24_P62e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_MunicipioContratadaWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_21_P62e( true) ;
         }
         else
         {
            wb_table5_21_P62e( false) ;
         }
      }

      protected void wb_table6_24_P62( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_MunicipioContratadaWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 29,'" + sPrefix + "',false,'" + sGXsfl_84_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV16DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,29);\"", "", true, "HLP_MunicipioContratadaWC.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_MunicipioContratadaWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_33_P62( true) ;
         }
         else
         {
            wb_table7_33_P62( false) ;
         }
         return  ;
      }

      protected void wb_table7_33_P62e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 41,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_MunicipioContratadaWC.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 42,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_MunicipioContratadaWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_MunicipioContratadaWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 47,'" + sPrefix + "',false,'" + sGXsfl_84_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV21DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,47);\"", "", true, "HLP_MunicipioContratadaWC.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_MunicipioContratadaWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table8_51_P62( true) ;
         }
         else
         {
            wb_table8_51_P62( false) ;
         }
         return  ;
      }

      protected void wb_table8_51_P62e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_MunicipioContratadaWC.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 60,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_MunicipioContratadaWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_MunicipioContratadaWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 65,'" + sPrefix + "',false,'" + sGXsfl_84_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV26DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 5, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVDYNAMICFILTERSSELECTOR3.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,65);\"", "", true, "HLP_MunicipioContratadaWC.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV26DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_MunicipioContratadaWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table9_69_P62( true) ;
         }
         else
         {
            wb_table9_69_P62( false) ;
         }
         return  ;
      }

      protected void wb_table9_69_P62e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 77,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_MunicipioContratadaWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_24_P62e( true) ;
         }
         else
         {
            wb_table6_24_P62e( false) ;
         }
      }

      protected void wb_table9_69_P62( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters3_Internalname, tblTablemergeddynamicfilters3_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 72,'" + sPrefix + "',false,'" + sGXsfl_84_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator3, cmbavDynamicfiltersoperator3_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0)), 1, cmbavDynamicfiltersoperator3_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator3.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,72);\"", "", true, "HLP_MunicipioContratadaWC.htm");
            cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator3_Internalname, "Values", (String)(cmbavDynamicfiltersoperator3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 74,'" + sPrefix + "',false,'" + sGXsfl_84_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratada_areatrabalhodes3_Internalname, AV28Contratada_AreaTrabalhoDes3, StringUtil.RTrim( context.localUtil.Format( AV28Contratada_AreaTrabalhoDes3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,74);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratada_areatrabalhodes3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContratada_areatrabalhodes3_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_MunicipioContratadaWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 75,'" + sPrefix + "',false,'" + sGXsfl_84_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratada_pessoanom3_Internalname, StringUtil.RTrim( AV29Contratada_PessoaNom3), StringUtil.RTrim( context.localUtil.Format( AV29Contratada_PessoaNom3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,75);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratada_pessoanom3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContratada_pessoanom3_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_MunicipioContratadaWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_69_P62e( true) ;
         }
         else
         {
            wb_table9_69_P62e( false) ;
         }
      }

      protected void wb_table8_51_P62( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters2_Internalname, tblTablemergeddynamicfilters2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'" + sPrefix + "',false,'" + sGXsfl_84_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator2, cmbavDynamicfiltersoperator2_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)), 1, cmbavDynamicfiltersoperator2_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,54);\"", "", true, "HLP_MunicipioContratadaWC.htm");
            cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator2_Internalname, "Values", (String)(cmbavDynamicfiltersoperator2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 56,'" + sPrefix + "',false,'" + sGXsfl_84_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratada_areatrabalhodes2_Internalname, AV23Contratada_AreaTrabalhoDes2, StringUtil.RTrim( context.localUtil.Format( AV23Contratada_AreaTrabalhoDes2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,56);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratada_areatrabalhodes2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContratada_areatrabalhodes2_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_MunicipioContratadaWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 57,'" + sPrefix + "',false,'" + sGXsfl_84_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratada_pessoanom2_Internalname, StringUtil.RTrim( AV24Contratada_PessoaNom2), StringUtil.RTrim( context.localUtil.Format( AV24Contratada_PessoaNom2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,57);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratada_pessoanom2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContratada_pessoanom2_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_MunicipioContratadaWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_51_P62e( true) ;
         }
         else
         {
            wb_table8_51_P62e( false) ;
         }
      }

      protected void wb_table7_33_P62( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters1_Internalname, tblTablemergeddynamicfilters1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'" + sPrefix + "',false,'" + sGXsfl_84_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator1, cmbavDynamicfiltersoperator1_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)), 1, cmbavDynamicfiltersoperator1_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,36);\"", "", true, "HLP_MunicipioContratadaWC.htm");
            cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Values", (String)(cmbavDynamicfiltersoperator1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'" + sPrefix + "',false,'" + sGXsfl_84_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratada_areatrabalhodes1_Internalname, AV18Contratada_AreaTrabalhoDes1, StringUtil.RTrim( context.localUtil.Format( AV18Contratada_AreaTrabalhoDes1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,38);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratada_areatrabalhodes1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContratada_areatrabalhodes1_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_MunicipioContratadaWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'" + sPrefix + "',false,'" + sGXsfl_84_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratada_pessoanom1_Internalname, StringUtil.RTrim( AV19Contratada_PessoaNom1), StringUtil.RTrim( context.localUtil.Format( AV19Contratada_PessoaNom1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,39);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratada_pessoanom1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContratada_pessoanom1_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_MunicipioContratadaWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_33_P62e( true) ;
         }
         else
         {
            wb_table7_33_P62e( false) ;
         }
      }

      protected void wb_table4_11_P62( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 14,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgInsert_Internalname, context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Inserir", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgInsert_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOINSERT\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_MunicipioContratadaWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_11_P62e( true) ;
         }
         else
         {
            wb_table4_11_P62e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7Contratada_MunicipioCod = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Contratada_MunicipioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Contratada_MunicipioCod), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAP62( ) ;
         WSP62( ) ;
         WEP62( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlAV7Contratada_MunicipioCod = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PAP62( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "municipiocontratadawc");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PAP62( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            AV7Contratada_MunicipioCod = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Contratada_MunicipioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Contratada_MunicipioCod), 6, 0)));
         }
         wcpOAV7Contratada_MunicipioCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV7Contratada_MunicipioCod"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( AV7Contratada_MunicipioCod != wcpOAV7Contratada_MunicipioCod ) ) )
         {
            setjustcreated();
         }
         wcpOAV7Contratada_MunicipioCod = AV7Contratada_MunicipioCod;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlAV7Contratada_MunicipioCod = cgiGet( sPrefix+"AV7Contratada_MunicipioCod_CTRL");
         if ( StringUtil.Len( sCtrlAV7Contratada_MunicipioCod) > 0 )
         {
            AV7Contratada_MunicipioCod = (int)(context.localUtil.CToN( cgiGet( sCtrlAV7Contratada_MunicipioCod), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Contratada_MunicipioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Contratada_MunicipioCod), 6, 0)));
         }
         else
         {
            AV7Contratada_MunicipioCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"AV7Contratada_MunicipioCod_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PAP62( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSP62( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSP62( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"AV7Contratada_MunicipioCod_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7Contratada_MunicipioCod), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlAV7Contratada_MunicipioCod)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"AV7Contratada_MunicipioCod_CTRL", StringUtil.RTrim( sCtrlAV7Contratada_MunicipioCod));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WEP62( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?2249787");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202042822574388");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("municipiocontratadawc.js", "?202042822574388");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_842( )
      {
         edtavUpdate_Internalname = sPrefix+"vUPDATE_"+sGXsfl_84_idx;
         edtavDelete_Internalname = sPrefix+"vDELETE_"+sGXsfl_84_idx;
         edtavDisplay_Internalname = sPrefix+"vDISPLAY_"+sGXsfl_84_idx;
         edtContratada_Codigo_Internalname = sPrefix+"CONTRATADA_CODIGO_"+sGXsfl_84_idx;
         edtContratada_AreaTrabalhoCod_Internalname = sPrefix+"CONTRATADA_AREATRABALHOCOD_"+sGXsfl_84_idx;
         edtContratada_AreaTrabalhoDes_Internalname = sPrefix+"CONTRATADA_AREATRABALHODES_"+sGXsfl_84_idx;
         cmbContratada_AreaTrbClcPFnl_Internalname = sPrefix+"CONTRATADA_AREATRBCLCPFNL_"+sGXsfl_84_idx;
         edtContratada_AreaTrbSrvPdr_Internalname = sPrefix+"CONTRATADA_AREATRBSRVPDR_"+sGXsfl_84_idx;
         edtContratada_PessoaCod_Internalname = sPrefix+"CONTRATADA_PESSOACOD_"+sGXsfl_84_idx;
         edtContratada_PessoaNom_Internalname = sPrefix+"CONTRATADA_PESSOANOM_"+sGXsfl_84_idx;
         edtContratada_PessoaCNPJ_Internalname = sPrefix+"CONTRATADA_PESSOACNPJ_"+sGXsfl_84_idx;
         edtPessoa_IE_Internalname = sPrefix+"PESSOA_IE_"+sGXsfl_84_idx;
         edtPessoa_Endereco_Internalname = sPrefix+"PESSOA_ENDERECO_"+sGXsfl_84_idx;
         edtPessoa_CEP_Internalname = sPrefix+"PESSOA_CEP_"+sGXsfl_84_idx;
         edtPessoa_Telefone_Internalname = sPrefix+"PESSOA_TELEFONE_"+sGXsfl_84_idx;
         edtPessoa_Fax_Internalname = sPrefix+"PESSOA_FAX_"+sGXsfl_84_idx;
         edtContratada_Sigla_Internalname = sPrefix+"CONTRATADA_SIGLA_"+sGXsfl_84_idx;
         cmbContratada_TipoFabrica_Internalname = sPrefix+"CONTRATADA_TIPOFABRICA_"+sGXsfl_84_idx;
         edtContratada_BancoNome_Internalname = sPrefix+"CONTRATADA_BANCONOME_"+sGXsfl_84_idx;
         edtContratada_BancoNro_Internalname = sPrefix+"CONTRATADA_BANCONRO_"+sGXsfl_84_idx;
         edtContratada_AgenciaNome_Internalname = sPrefix+"CONTRATADA_AGENCIANOME_"+sGXsfl_84_idx;
         edtContratada_AgenciaNro_Internalname = sPrefix+"CONTRATADA_AGENCIANRO_"+sGXsfl_84_idx;
         edtContratada_ContaCorrente_Internalname = sPrefix+"CONTRATADA_CONTACORRENTE_"+sGXsfl_84_idx;
         edtContratada_OS_Internalname = sPrefix+"CONTRATADA_OS_"+sGXsfl_84_idx;
         edtContratada_SS_Internalname = sPrefix+"CONTRATADA_SS_"+sGXsfl_84_idx;
         edtContratada_Lote_Internalname = sPrefix+"CONTRATADA_LOTE_"+sGXsfl_84_idx;
         edtContratada_LogoArquivo_Internalname = sPrefix+"CONTRATADA_LOGOARQUIVO_"+sGXsfl_84_idx;
         edtContratada_LogoNomeArq_Internalname = sPrefix+"CONTRATADA_LOGONOMEARQ_"+sGXsfl_84_idx;
      }

      protected void SubsflControlProps_fel_842( )
      {
         edtavUpdate_Internalname = sPrefix+"vUPDATE_"+sGXsfl_84_fel_idx;
         edtavDelete_Internalname = sPrefix+"vDELETE_"+sGXsfl_84_fel_idx;
         edtavDisplay_Internalname = sPrefix+"vDISPLAY_"+sGXsfl_84_fel_idx;
         edtContratada_Codigo_Internalname = sPrefix+"CONTRATADA_CODIGO_"+sGXsfl_84_fel_idx;
         edtContratada_AreaTrabalhoCod_Internalname = sPrefix+"CONTRATADA_AREATRABALHOCOD_"+sGXsfl_84_fel_idx;
         edtContratada_AreaTrabalhoDes_Internalname = sPrefix+"CONTRATADA_AREATRABALHODES_"+sGXsfl_84_fel_idx;
         cmbContratada_AreaTrbClcPFnl_Internalname = sPrefix+"CONTRATADA_AREATRBCLCPFNL_"+sGXsfl_84_fel_idx;
         edtContratada_AreaTrbSrvPdr_Internalname = sPrefix+"CONTRATADA_AREATRBSRVPDR_"+sGXsfl_84_fel_idx;
         edtContratada_PessoaCod_Internalname = sPrefix+"CONTRATADA_PESSOACOD_"+sGXsfl_84_fel_idx;
         edtContratada_PessoaNom_Internalname = sPrefix+"CONTRATADA_PESSOANOM_"+sGXsfl_84_fel_idx;
         edtContratada_PessoaCNPJ_Internalname = sPrefix+"CONTRATADA_PESSOACNPJ_"+sGXsfl_84_fel_idx;
         edtPessoa_IE_Internalname = sPrefix+"PESSOA_IE_"+sGXsfl_84_fel_idx;
         edtPessoa_Endereco_Internalname = sPrefix+"PESSOA_ENDERECO_"+sGXsfl_84_fel_idx;
         edtPessoa_CEP_Internalname = sPrefix+"PESSOA_CEP_"+sGXsfl_84_fel_idx;
         edtPessoa_Telefone_Internalname = sPrefix+"PESSOA_TELEFONE_"+sGXsfl_84_fel_idx;
         edtPessoa_Fax_Internalname = sPrefix+"PESSOA_FAX_"+sGXsfl_84_fel_idx;
         edtContratada_Sigla_Internalname = sPrefix+"CONTRATADA_SIGLA_"+sGXsfl_84_fel_idx;
         cmbContratada_TipoFabrica_Internalname = sPrefix+"CONTRATADA_TIPOFABRICA_"+sGXsfl_84_fel_idx;
         edtContratada_BancoNome_Internalname = sPrefix+"CONTRATADA_BANCONOME_"+sGXsfl_84_fel_idx;
         edtContratada_BancoNro_Internalname = sPrefix+"CONTRATADA_BANCONRO_"+sGXsfl_84_fel_idx;
         edtContratada_AgenciaNome_Internalname = sPrefix+"CONTRATADA_AGENCIANOME_"+sGXsfl_84_fel_idx;
         edtContratada_AgenciaNro_Internalname = sPrefix+"CONTRATADA_AGENCIANRO_"+sGXsfl_84_fel_idx;
         edtContratada_ContaCorrente_Internalname = sPrefix+"CONTRATADA_CONTACORRENTE_"+sGXsfl_84_fel_idx;
         edtContratada_OS_Internalname = sPrefix+"CONTRATADA_OS_"+sGXsfl_84_fel_idx;
         edtContratada_SS_Internalname = sPrefix+"CONTRATADA_SS_"+sGXsfl_84_fel_idx;
         edtContratada_Lote_Internalname = sPrefix+"CONTRATADA_LOTE_"+sGXsfl_84_fel_idx;
         edtContratada_LogoArquivo_Internalname = sPrefix+"CONTRATADA_LOGOARQUIVO_"+sGXsfl_84_fel_idx;
         edtContratada_LogoNomeArq_Internalname = sPrefix+"CONTRATADA_LOGONOMEARQ_"+sGXsfl_84_fel_idx;
      }

      protected void sendrow_842( )
      {
         SubsflControlProps_842( ) ;
         WBP60( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_84_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_84_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_84_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV32Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV32Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV41Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV32Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV32Update)) ? AV41Update_GXI : context.PathToRelativeUrl( AV32Update)),(String)edtavUpdate_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(int)edtavUpdate_Enabled,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV32Update_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV33Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV33Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV42Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV33Delete)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV33Delete)) ? AV42Delete_GXI : context.PathToRelativeUrl( AV33Delete)),(String)edtavDelete_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(int)edtavDelete_Enabled,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV33Delete_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV34Display_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV34Display))&&String.IsNullOrEmpty(StringUtil.RTrim( AV43Display_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV34Display)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDisplay_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV34Display)) ? AV43Display_GXI : context.PathToRelativeUrl( AV34Display)),(String)edtavDisplay_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(int)edtavDisplay_Enabled,(String)"",(String)edtavDisplay_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV34Display_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratada_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A39Contratada_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A39Contratada_Codigo), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratada_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)84,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratada_AreaTrabalhoCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A52Contratada_AreaTrabalhoCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A52Contratada_AreaTrabalhoCod), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratada_AreaTrabalhoCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)84,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratada_AreaTrabalhoDes_Internalname,(String)A53Contratada_AreaTrabalhoDes,StringUtil.RTrim( context.localUtil.Format( A53Contratada_AreaTrabalhoDes, "@!")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)edtContratada_AreaTrabalhoDes_Link,(String)"",(String)"",(String)"",(String)edtContratada_AreaTrabalhoDes_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)84,(short)1,(short)-1,(short)-1,(bool)true,(String)"Descricao",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            GXCCtl = "CONTRATADA_AREATRBCLCPFNL_" + sGXsfl_84_idx;
            cmbContratada_AreaTrbClcPFnl.Name = GXCCtl;
            cmbContratada_AreaTrbClcPFnl.WebTags = "";
            cmbContratada_AreaTrbClcPFnl.addItem("MB", "Menor PFB", 0);
            cmbContratada_AreaTrbClcPFnl.addItem("BM", "PFB FM", 0);
            if ( cmbContratada_AreaTrbClcPFnl.ItemCount > 0 )
            {
               A1592Contratada_AreaTrbClcPFnl = cmbContratada_AreaTrbClcPFnl.getValidValue(A1592Contratada_AreaTrbClcPFnl);
               n1592Contratada_AreaTrbClcPFnl = false;
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbContratada_AreaTrbClcPFnl,(String)cmbContratada_AreaTrbClcPFnl_Internalname,StringUtil.RTrim( A1592Contratada_AreaTrbClcPFnl),(short)1,(String)cmbContratada_AreaTrbClcPFnl_Jsonclick,(short)0,(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"char",(String)"",(short)-1,(short)0,(short)1,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbContratada_AreaTrbClcPFnl.CurrentValue = StringUtil.RTrim( A1592Contratada_AreaTrbClcPFnl);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbContratada_AreaTrbClcPFnl_Internalname, "Values", (String)(cmbContratada_AreaTrbClcPFnl.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratada_AreaTrbSrvPdr_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1595Contratada_AreaTrbSrvPdr), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A1595Contratada_AreaTrbSrvPdr), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratada_AreaTrbSrvPdr_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)84,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratada_PessoaCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A40Contratada_PessoaCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A40Contratada_PessoaCod), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratada_PessoaCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)84,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratada_PessoaNom_Internalname,StringUtil.RTrim( A41Contratada_PessoaNom),StringUtil.RTrim( context.localUtil.Format( A41Contratada_PessoaNom, "@!")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)edtContratada_PessoaNom_Link,(String)"",(String)"",(String)"",(String)edtContratada_PessoaNom_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)100,(short)0,(short)0,(short)84,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome100",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratada_PessoaCNPJ_Internalname,(String)A42Contratada_PessoaCNPJ,(String)"",(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratada_PessoaCNPJ_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)15,(short)0,(short)0,(short)84,(short)1,(short)-1,(short)-1,(bool)true,(String)"Docto",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtPessoa_IE_Internalname,StringUtil.RTrim( A518Pessoa_IE),(String)"",(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtPessoa_IE_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)15,(short)0,(short)0,(short)84,(short)1,(short)-1,(short)-1,(bool)true,(String)"IE",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtPessoa_Endereco_Internalname,(String)A519Pessoa_Endereco,(String)"",(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtPessoa_Endereco_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)100,(short)0,(short)0,(short)84,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtPessoa_CEP_Internalname,StringUtil.RTrim( A521Pessoa_CEP),(String)"",(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtPessoa_CEP_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)10,(short)0,(short)0,(short)84,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtPessoa_Telefone_Internalname,StringUtil.RTrim( A522Pessoa_Telefone),(String)"",(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtPessoa_Telefone_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)15,(short)0,(short)0,(short)84,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtPessoa_Fax_Internalname,StringUtil.RTrim( A523Pessoa_Fax),(String)"",(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtPessoa_Fax_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)15,(short)0,(short)0,(short)84,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratada_Sigla_Internalname,StringUtil.RTrim( A438Contratada_Sigla),StringUtil.RTrim( context.localUtil.Format( A438Contratada_Sigla, "@!")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratada_Sigla_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)15,(short)0,(short)0,(short)84,(short)1,(short)-1,(short)-1,(bool)true,(String)"Sigla",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_84_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "CONTRATADA_TIPOFABRICA_" + sGXsfl_84_idx;
               cmbContratada_TipoFabrica.Name = GXCCtl;
               cmbContratada_TipoFabrica.WebTags = "";
               cmbContratada_TipoFabrica.addItem("", "(Nenhuma)", 0);
               cmbContratada_TipoFabrica.addItem("S", "F�brica de Software", 0);
               cmbContratada_TipoFabrica.addItem("M", "F�brica de M�trica", 0);
               cmbContratada_TipoFabrica.addItem("I", "Departamento / Setor", 0);
               cmbContratada_TipoFabrica.addItem("O", "Outras", 0);
               if ( cmbContratada_TipoFabrica.ItemCount > 0 )
               {
                  A516Contratada_TipoFabrica = cmbContratada_TipoFabrica.getValidValue(A516Contratada_TipoFabrica);
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbContratada_TipoFabrica,(String)cmbContratada_TipoFabrica_Internalname,StringUtil.RTrim( A516Contratada_TipoFabrica),(short)1,(String)cmbContratada_TipoFabrica_Jsonclick,(short)0,(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"char",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbContratada_TipoFabrica.CurrentValue = StringUtil.RTrim( A516Contratada_TipoFabrica);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbContratada_TipoFabrica_Internalname, "Values", (String)(cmbContratada_TipoFabrica.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratada_BancoNome_Internalname,StringUtil.RTrim( A342Contratada_BancoNome),StringUtil.RTrim( context.localUtil.Format( A342Contratada_BancoNome, "@!")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratada_BancoNome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)84,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratada_BancoNro_Internalname,StringUtil.RTrim( A343Contratada_BancoNro),(String)"",(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratada_BancoNro_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)84,(short)1,(short)-1,(short)-1,(bool)true,(String)"NumeroBanco",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratada_AgenciaNome_Internalname,StringUtil.RTrim( A344Contratada_AgenciaNome),StringUtil.RTrim( context.localUtil.Format( A344Contratada_AgenciaNome, "@!")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratada_AgenciaNome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)84,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratada_AgenciaNro_Internalname,StringUtil.RTrim( A345Contratada_AgenciaNro),(String)"",(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratada_AgenciaNro_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)10,(short)0,(short)0,(short)84,(short)1,(short)-1,(short)-1,(bool)true,(String)"Agencia",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratada_ContaCorrente_Internalname,StringUtil.RTrim( A51Contratada_ContaCorrente),(String)"",(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratada_ContaCorrente_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)10,(short)0,(short)0,(short)84,(short)1,(short)-1,(short)-1,(bool)true,(String)"ContaCorrente",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratada_OS_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A524Contratada_OS), 8, 0, ",", "")),context.localUtil.Format( (decimal)(A524Contratada_OS), "ZZZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratada_OS_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)58,(String)"px",(short)17,(String)"px",(short)8,(short)0,(short)0,(short)84,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratada_SS_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1451Contratada_SS), 8, 0, ",", "")),context.localUtil.Format( (decimal)(A1451Contratada_SS), "ZZZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratada_SS_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)58,(String)"px",(short)17,(String)"px",(short)8,(short)0,(short)0,(short)84,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratada_Lote_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A530Contratada_Lote), 4, 0, ",", "")),context.localUtil.Format( (decimal)(A530Contratada_Lote), "ZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratada_Lote_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)34,(String)"px",(short)17,(String)"px",(short)4,(short)0,(short)0,(short)84,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            ClassString = "Image";
            StyleString = "";
            edtContratada_LogoArquivo_Filename = A1128Contratada_LogoNomeArq;
            edtContratada_LogoArquivo_Filetype = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratada_LogoArquivo_Internalname, "Filetype", edtContratada_LogoArquivo_Filetype);
            edtContratada_LogoArquivo_Filetype = A1129Contratada_LogoTipoArq;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratada_LogoArquivo_Internalname, "Filetype", edtContratada_LogoArquivo_Filetype);
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A1127Contratada_LogoArquivo)) )
            {
               gxblobfileaux.Source = A1127Contratada_LogoArquivo;
               if ( ! gxblobfileaux.HasExtension() || ( StringUtil.StrCmp(edtContratada_LogoArquivo_Filetype, "tmp") != 0 ) )
               {
                  gxblobfileaux.SetExtension(StringUtil.Trim( edtContratada_LogoArquivo_Filetype));
               }
               if ( gxblobfileaux.ErrCode == 0 )
               {
                  A1127Contratada_LogoArquivo = gxblobfileaux.GetAbsoluteName();
                  n1127Contratada_LogoArquivo = false;
                  context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratada_LogoArquivo_Internalname, "URL", context.PathToRelativeUrl( A1127Contratada_LogoArquivo));
                  edtContratada_LogoArquivo_Filetype = gxblobfileaux.GetExtension();
                  context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratada_LogoArquivo_Internalname, "Filetype", edtContratada_LogoArquivo_Filetype);
               }
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratada_LogoArquivo_Internalname, "URL", context.PathToRelativeUrl( A1127Contratada_LogoArquivo));
            }
            GridRow.AddColumnProperties("blob", 2, isAjaxCallMode( ), new Object[] {(String)edtContratada_LogoArquivo_Internalname,StringUtil.RTrim( A1127Contratada_LogoArquivo),context.PathToRelativeUrl( A1127Contratada_LogoArquivo),(String.IsNullOrEmpty(StringUtil.RTrim( edtContratada_LogoArquivo_Contenttype)) ? context.GetContentType( (String.IsNullOrEmpty(StringUtil.RTrim( edtContratada_LogoArquivo_Filetype)) ? A1127Contratada_LogoArquivo : edtContratada_LogoArquivo_Filetype)) : edtContratada_LogoArquivo_Contenttype),(bool)true,(String)"",(String)edtContratada_LogoArquivo_Parameters,(short)0,(short)0,(short)-1,(String)"",(String)"",(short)0,(short)-1,(short)0,(String)"px",(short)60,(String)"px",(short)0,(short)0,(short)0,(String)edtContratada_LogoArquivo_Jsonclick,(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)StyleString,(String)ClassString,(String)"",(String)""+"",(String)"",(String)""});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratada_LogoNomeArq_Internalname,StringUtil.RTrim( A1128Contratada_LogoNomeArq),(String)"",(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratada_LogoNomeArq_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)84,(short)1,(short)-1,(short)-1,(bool)true,(String)"NomeArq",(String)"left",(bool)true});
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATADA_CODIGO"+"_"+sGXsfl_84_idx, GetSecureSignedToken( sPrefix+sGXsfl_84_idx, context.localUtil.Format( (decimal)(A39Contratada_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATADA_AREATRABALHOCOD"+"_"+sGXsfl_84_idx, GetSecureSignedToken( sPrefix+sGXsfl_84_idx, context.localUtil.Format( (decimal)(A52Contratada_AreaTrabalhoCod), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATADA_PESSOACOD"+"_"+sGXsfl_84_idx, GetSecureSignedToken( sPrefix+sGXsfl_84_idx, context.localUtil.Format( (decimal)(A40Contratada_PessoaCod), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATADA_SIGLA"+"_"+sGXsfl_84_idx, GetSecureSignedToken( sPrefix+sGXsfl_84_idx, StringUtil.RTrim( context.localUtil.Format( A438Contratada_Sigla, "@!"))));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATADA_TIPOFABRICA"+"_"+sGXsfl_84_idx, GetSecureSignedToken( sPrefix+sGXsfl_84_idx, StringUtil.RTrim( context.localUtil.Format( A516Contratada_TipoFabrica, ""))));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATADA_BANCONOME"+"_"+sGXsfl_84_idx, GetSecureSignedToken( sPrefix+sGXsfl_84_idx, StringUtil.RTrim( context.localUtil.Format( A342Contratada_BancoNome, "@!"))));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATADA_BANCONRO"+"_"+sGXsfl_84_idx, GetSecureSignedToken( sPrefix+sGXsfl_84_idx, StringUtil.RTrim( context.localUtil.Format( A343Contratada_BancoNro, ""))));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATADA_AGENCIANOME"+"_"+sGXsfl_84_idx, GetSecureSignedToken( sPrefix+sGXsfl_84_idx, StringUtil.RTrim( context.localUtil.Format( A344Contratada_AgenciaNome, "@!"))));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATADA_AGENCIANRO"+"_"+sGXsfl_84_idx, GetSecureSignedToken( sPrefix+sGXsfl_84_idx, StringUtil.RTrim( context.localUtil.Format( A345Contratada_AgenciaNro, ""))));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATADA_CONTACORRENTE"+"_"+sGXsfl_84_idx, GetSecureSignedToken( sPrefix+sGXsfl_84_idx, StringUtil.RTrim( context.localUtil.Format( A51Contratada_ContaCorrente, ""))));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATADA_OS"+"_"+sGXsfl_84_idx, GetSecureSignedToken( sPrefix+sGXsfl_84_idx, context.localUtil.Format( (decimal)(A524Contratada_OS), "ZZZZZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATADA_SS"+"_"+sGXsfl_84_idx, GetSecureSignedToken( sPrefix+sGXsfl_84_idx, context.localUtil.Format( (decimal)(A1451Contratada_SS), "ZZZZZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATADA_LOTE"+"_"+sGXsfl_84_idx, GetSecureSignedToken( sPrefix+sGXsfl_84_idx, context.localUtil.Format( (decimal)(A530Contratada_Lote), "ZZZ9")));
            GridContainer.AddRow(GridRow);
            nGXsfl_84_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_84_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_84_idx+1));
            sGXsfl_84_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_84_idx), 4, 0)), 4, "0");
            SubsflControlProps_842( ) ;
         }
         /* End function sendrow_842 */
      }

      protected void init_default_properties( )
      {
         imgInsert_Internalname = sPrefix+"INSERT";
         tblTableactions_Internalname = sPrefix+"TABLEACTIONS";
         lblOrderedtext_Internalname = sPrefix+"ORDEREDTEXT";
         cmbavOrderedby_Internalname = sPrefix+"vORDEREDBY";
         edtavOrdereddsc_Internalname = sPrefix+"vORDEREDDSC";
         lblDynamicfiltersprefix1_Internalname = sPrefix+"DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = sPrefix+"vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = sPrefix+"DYNAMICFILTERSMIDDLE1";
         cmbavDynamicfiltersoperator1_Internalname = sPrefix+"vDYNAMICFILTERSOPERATOR1";
         edtavContratada_areatrabalhodes1_Internalname = sPrefix+"vCONTRATADA_AREATRABALHODES1";
         edtavContratada_pessoanom1_Internalname = sPrefix+"vCONTRATADA_PESSOANOM1";
         tblTablemergeddynamicfilters1_Internalname = sPrefix+"TABLEMERGEDDYNAMICFILTERS1";
         imgAdddynamicfilters1_Internalname = sPrefix+"ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = sPrefix+"REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = sPrefix+"DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = sPrefix+"vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = sPrefix+"DYNAMICFILTERSMIDDLE2";
         cmbavDynamicfiltersoperator2_Internalname = sPrefix+"vDYNAMICFILTERSOPERATOR2";
         edtavContratada_areatrabalhodes2_Internalname = sPrefix+"vCONTRATADA_AREATRABALHODES2";
         edtavContratada_pessoanom2_Internalname = sPrefix+"vCONTRATADA_PESSOANOM2";
         tblTablemergeddynamicfilters2_Internalname = sPrefix+"TABLEMERGEDDYNAMICFILTERS2";
         imgAdddynamicfilters2_Internalname = sPrefix+"ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = sPrefix+"REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = sPrefix+"DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = sPrefix+"vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = sPrefix+"DYNAMICFILTERSMIDDLE3";
         cmbavDynamicfiltersoperator3_Internalname = sPrefix+"vDYNAMICFILTERSOPERATOR3";
         edtavContratada_areatrabalhodes3_Internalname = sPrefix+"vCONTRATADA_AREATRABALHODES3";
         edtavContratada_pessoanom3_Internalname = sPrefix+"vCONTRATADA_PESSOANOM3";
         tblTablemergeddynamicfilters3_Internalname = sPrefix+"TABLEMERGEDDYNAMICFILTERS3";
         imgRemovedynamicfilters3_Internalname = sPrefix+"REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = sPrefix+"TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = sPrefix+"JSDYNAMICFILTERS";
         tblTablefilters_Internalname = sPrefix+"TABLEFILTERS";
         tblTablesearch_Internalname = sPrefix+"TABLESEARCH";
         edtavUpdate_Internalname = sPrefix+"vUPDATE";
         edtavDelete_Internalname = sPrefix+"vDELETE";
         edtavDisplay_Internalname = sPrefix+"vDISPLAY";
         edtContratada_Codigo_Internalname = sPrefix+"CONTRATADA_CODIGO";
         edtContratada_AreaTrabalhoCod_Internalname = sPrefix+"CONTRATADA_AREATRABALHOCOD";
         edtContratada_AreaTrabalhoDes_Internalname = sPrefix+"CONTRATADA_AREATRABALHODES";
         cmbContratada_AreaTrbClcPFnl_Internalname = sPrefix+"CONTRATADA_AREATRBCLCPFNL";
         edtContratada_AreaTrbSrvPdr_Internalname = sPrefix+"CONTRATADA_AREATRBSRVPDR";
         edtContratada_PessoaCod_Internalname = sPrefix+"CONTRATADA_PESSOACOD";
         edtContratada_PessoaNom_Internalname = sPrefix+"CONTRATADA_PESSOANOM";
         edtContratada_PessoaCNPJ_Internalname = sPrefix+"CONTRATADA_PESSOACNPJ";
         edtPessoa_IE_Internalname = sPrefix+"PESSOA_IE";
         edtPessoa_Endereco_Internalname = sPrefix+"PESSOA_ENDERECO";
         edtPessoa_CEP_Internalname = sPrefix+"PESSOA_CEP";
         edtPessoa_Telefone_Internalname = sPrefix+"PESSOA_TELEFONE";
         edtPessoa_Fax_Internalname = sPrefix+"PESSOA_FAX";
         edtContratada_Sigla_Internalname = sPrefix+"CONTRATADA_SIGLA";
         cmbContratada_TipoFabrica_Internalname = sPrefix+"CONTRATADA_TIPOFABRICA";
         edtContratada_BancoNome_Internalname = sPrefix+"CONTRATADA_BANCONOME";
         edtContratada_BancoNro_Internalname = sPrefix+"CONTRATADA_BANCONRO";
         edtContratada_AgenciaNome_Internalname = sPrefix+"CONTRATADA_AGENCIANOME";
         edtContratada_AgenciaNro_Internalname = sPrefix+"CONTRATADA_AGENCIANRO";
         edtContratada_ContaCorrente_Internalname = sPrefix+"CONTRATADA_CONTACORRENTE";
         edtContratada_OS_Internalname = sPrefix+"CONTRATADA_OS";
         edtContratada_SS_Internalname = sPrefix+"CONTRATADA_SS";
         edtContratada_Lote_Internalname = sPrefix+"CONTRATADA_LOTE";
         edtContratada_LogoArquivo_Internalname = sPrefix+"CONTRATADA_LOGOARQUIVO";
         edtContratada_LogoNomeArq_Internalname = sPrefix+"CONTRATADA_LOGONOMEARQ";
         Gridpaginationbar_Internalname = sPrefix+"GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = sPrefix+"GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = sPrefix+"TABLEGRIDHEADER";
         edtContratada_MunicipioCod_Internalname = sPrefix+"CONTRATADA_MUNICIPIOCOD";
         Workwithplusutilities1_Internalname = sPrefix+"WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = sPrefix+"vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = sPrefix+"vDYNAMICFILTERSENABLED3";
         Form.Internalname = sPrefix+"FORM";
         subGrid_Internalname = sPrefix+"GRID";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtContratada_LogoNomeArq_Jsonclick = "";
         edtContratada_LogoArquivo_Jsonclick = "";
         edtContratada_LogoArquivo_Parameters = "";
         edtContratada_LogoArquivo_Contenttype = "";
         edtContratada_Lote_Jsonclick = "";
         edtContratada_SS_Jsonclick = "";
         edtContratada_OS_Jsonclick = "";
         edtContratada_ContaCorrente_Jsonclick = "";
         edtContratada_AgenciaNro_Jsonclick = "";
         edtContratada_AgenciaNome_Jsonclick = "";
         edtContratada_BancoNro_Jsonclick = "";
         edtContratada_BancoNome_Jsonclick = "";
         cmbContratada_TipoFabrica_Jsonclick = "";
         edtContratada_Sigla_Jsonclick = "";
         edtPessoa_Fax_Jsonclick = "";
         edtPessoa_Telefone_Jsonclick = "";
         edtPessoa_CEP_Jsonclick = "";
         edtPessoa_Endereco_Jsonclick = "";
         edtPessoa_IE_Jsonclick = "";
         edtContratada_PessoaCNPJ_Jsonclick = "";
         edtContratada_PessoaNom_Jsonclick = "";
         edtContratada_PessoaCod_Jsonclick = "";
         edtContratada_AreaTrbSrvPdr_Jsonclick = "";
         cmbContratada_AreaTrbClcPFnl_Jsonclick = "";
         edtContratada_AreaTrabalhoDes_Jsonclick = "";
         edtContratada_AreaTrabalhoCod_Jsonclick = "";
         edtContratada_Codigo_Jsonclick = "";
         edtavContratada_pessoanom1_Jsonclick = "";
         edtavContratada_areatrabalhodes1_Jsonclick = "";
         cmbavDynamicfiltersoperator1_Jsonclick = "";
         edtavContratada_pessoanom2_Jsonclick = "";
         edtavContratada_areatrabalhodes2_Jsonclick = "";
         cmbavDynamicfiltersoperator2_Jsonclick = "";
         edtavContratada_pessoanom3_Jsonclick = "";
         edtavContratada_areatrabalhodes3_Jsonclick = "";
         cmbavDynamicfiltersoperator3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtContratada_PessoaNom_Link = "";
         edtContratada_AreaTrabalhoDes_Link = "";
         edtavDisplay_Tooltiptext = "Mostrar";
         edtavDisplay_Link = "";
         edtavDisplay_Enabled = 1;
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = "";
         edtavDelete_Enabled = 1;
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = "";
         edtavUpdate_Enabled = 1;
         edtContratada_Lote_Titleformat = 0;
         edtContratada_SS_Titleformat = 0;
         edtContratada_OS_Titleformat = 0;
         edtContratada_ContaCorrente_Titleformat = 0;
         edtContratada_AgenciaNro_Titleformat = 0;
         edtContratada_AgenciaNome_Titleformat = 0;
         edtContratada_BancoNro_Titleformat = 0;
         edtContratada_BancoNome_Titleformat = 0;
         cmbContratada_TipoFabrica_Titleformat = 0;
         edtContratada_Sigla_Titleformat = 0;
         edtPessoa_Fax_Titleformat = 0;
         edtPessoa_Telefone_Titleformat = 0;
         edtPessoa_CEP_Titleformat = 0;
         edtPessoa_Endereco_Titleformat = 0;
         edtPessoa_IE_Titleformat = 0;
         edtContratada_PessoaCNPJ_Titleformat = 0;
         edtContratada_PessoaNom_Titleformat = 0;
         edtContratada_PessoaCod_Titleformat = 0;
         edtContratada_AreaTrbSrvPdr_Titleformat = 0;
         cmbContratada_AreaTrbClcPFnl_Titleformat = 0;
         edtContratada_AreaTrabalhoDes_Titleformat = 0;
         edtContratada_AreaTrabalhoCod_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         cmbavDynamicfiltersoperator3.Visible = 1;
         edtavContratada_pessoanom3_Visible = 1;
         edtavContratada_areatrabalhodes3_Visible = 1;
         cmbavDynamicfiltersoperator2.Visible = 1;
         edtavContratada_pessoanom2_Visible = 1;
         edtavContratada_areatrabalhodes2_Visible = 1;
         cmbavDynamicfiltersoperator1.Visible = 1;
         edtavContratada_pessoanom1_Visible = 1;
         edtavContratada_areatrabalhodes1_Visible = 1;
         edtContratada_Lote_Title = "Lote";
         edtContratada_SS_Title = "SS";
         edtContratada_OS_Title = "OS";
         edtContratada_ContaCorrente_Title = "Corrente";
         edtContratada_AgenciaNro_Title = "N�mero";
         edtContratada_AgenciaNome_Title = "Ag�ncia";
         edtContratada_BancoNro_Title = "N�mero";
         edtContratada_BancoNome_Title = "Banco";
         cmbContratada_TipoFabrica.Title.Text = "Tipo";
         edtContratada_Sigla_Title = "Sigla";
         edtPessoa_Fax_Title = "Fax";
         edtPessoa_Telefone_Title = "Telefone";
         edtPessoa_CEP_Title = "CEP";
         edtPessoa_Endereco_Title = "Endere�o";
         edtPessoa_IE_Title = "Inscri��o Estadual";
         edtContratada_PessoaCNPJ_Title = "da Pessoa";
         edtContratada_PessoaNom_Title = "Nome";
         edtContratada_PessoaCod_Title = "da Pessoa";
         edtContratada_AreaTrbSrvPdr_Title = "padr�o";
         cmbContratada_AreaTrbClcPFnl.Title.Text = "P Final";
         edtContratada_AreaTrabalhoDes_Title = "de Trabalho";
         edtContratada_AreaTrabalhoCod_Title = "de Trabalho";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         edtContratada_LogoArquivo_Filetype = "";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         edtContratada_MunicipioCod_Jsonclick = "";
         edtContratada_MunicipioCod_Visible = 1;
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV7Contratada_MunicipioCod',fld:'vCONTRATADA_MUNICIPIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A52Contratada_AreaTrabalhoCod',fld:'CONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'sPrefix',nv:''},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV44Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV18Contratada_AreaTrabalhoDes1',fld:'vCONTRATADA_AREATRABALHODES1',pic:'@!',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV19Contratada_PessoaNom1',fld:'vCONTRATADA_PESSOANOM1',pic:'@!',nv:''},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV23Contratada_AreaTrabalhoDes2',fld:'vCONTRATADA_AREATRABALHODES2',pic:'@!',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV24Contratada_PessoaNom2',fld:'vCONTRATADA_PESSOANOM2',pic:'@!',nv:''},{av:'AV28Contratada_AreaTrabalhoDes3',fld:'vCONTRATADA_AREATRABALHODES3',pic:'@!',nv:''},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29Contratada_PessoaNom3',fld:'vCONTRATADA_PESSOANOM3',pic:'@!',nv:''}],oparms:[{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'edtContratada_AreaTrabalhoCod_Titleformat',ctrl:'CONTRATADA_AREATRABALHOCOD',prop:'Titleformat'},{av:'edtContratada_AreaTrabalhoCod_Title',ctrl:'CONTRATADA_AREATRABALHOCOD',prop:'Title'},{av:'edtContratada_AreaTrabalhoDes_Titleformat',ctrl:'CONTRATADA_AREATRABALHODES',prop:'Titleformat'},{av:'edtContratada_AreaTrabalhoDes_Title',ctrl:'CONTRATADA_AREATRABALHODES',prop:'Title'},{av:'cmbContratada_AreaTrbClcPFnl'},{av:'edtContratada_AreaTrbSrvPdr_Titleformat',ctrl:'CONTRATADA_AREATRBSRVPDR',prop:'Titleformat'},{av:'edtContratada_AreaTrbSrvPdr_Title',ctrl:'CONTRATADA_AREATRBSRVPDR',prop:'Title'},{av:'edtContratada_PessoaCod_Titleformat',ctrl:'CONTRATADA_PESSOACOD',prop:'Titleformat'},{av:'edtContratada_PessoaCod_Title',ctrl:'CONTRATADA_PESSOACOD',prop:'Title'},{av:'edtContratada_PessoaNom_Titleformat',ctrl:'CONTRATADA_PESSOANOM',prop:'Titleformat'},{av:'edtContratada_PessoaNom_Title',ctrl:'CONTRATADA_PESSOANOM',prop:'Title'},{av:'edtContratada_PessoaCNPJ_Titleformat',ctrl:'CONTRATADA_PESSOACNPJ',prop:'Titleformat'},{av:'edtContratada_PessoaCNPJ_Title',ctrl:'CONTRATADA_PESSOACNPJ',prop:'Title'},{av:'edtPessoa_IE_Titleformat',ctrl:'PESSOA_IE',prop:'Titleformat'},{av:'edtPessoa_IE_Title',ctrl:'PESSOA_IE',prop:'Title'},{av:'edtPessoa_Endereco_Titleformat',ctrl:'PESSOA_ENDERECO',prop:'Titleformat'},{av:'edtPessoa_Endereco_Title',ctrl:'PESSOA_ENDERECO',prop:'Title'},{av:'edtPessoa_CEP_Titleformat',ctrl:'PESSOA_CEP',prop:'Titleformat'},{av:'edtPessoa_CEP_Title',ctrl:'PESSOA_CEP',prop:'Title'},{av:'edtPessoa_Telefone_Titleformat',ctrl:'PESSOA_TELEFONE',prop:'Titleformat'},{av:'edtPessoa_Telefone_Title',ctrl:'PESSOA_TELEFONE',prop:'Title'},{av:'edtPessoa_Fax_Titleformat',ctrl:'PESSOA_FAX',prop:'Titleformat'},{av:'edtPessoa_Fax_Title',ctrl:'PESSOA_FAX',prop:'Title'},{av:'edtContratada_Sigla_Titleformat',ctrl:'CONTRATADA_SIGLA',prop:'Titleformat'},{av:'edtContratada_Sigla_Title',ctrl:'CONTRATADA_SIGLA',prop:'Title'},{av:'cmbContratada_TipoFabrica'},{av:'edtContratada_BancoNome_Titleformat',ctrl:'CONTRATADA_BANCONOME',prop:'Titleformat'},{av:'edtContratada_BancoNome_Title',ctrl:'CONTRATADA_BANCONOME',prop:'Title'},{av:'edtContratada_BancoNro_Titleformat',ctrl:'CONTRATADA_BANCONRO',prop:'Titleformat'},{av:'edtContratada_BancoNro_Title',ctrl:'CONTRATADA_BANCONRO',prop:'Title'},{av:'edtContratada_AgenciaNome_Titleformat',ctrl:'CONTRATADA_AGENCIANOME',prop:'Titleformat'},{av:'edtContratada_AgenciaNome_Title',ctrl:'CONTRATADA_AGENCIANOME',prop:'Title'},{av:'edtContratada_AgenciaNro_Titleformat',ctrl:'CONTRATADA_AGENCIANRO',prop:'Titleformat'},{av:'edtContratada_AgenciaNro_Title',ctrl:'CONTRATADA_AGENCIANRO',prop:'Title'},{av:'edtContratada_ContaCorrente_Titleformat',ctrl:'CONTRATADA_CONTACORRENTE',prop:'Titleformat'},{av:'edtContratada_ContaCorrente_Title',ctrl:'CONTRATADA_CONTACORRENTE',prop:'Title'},{av:'edtContratada_OS_Titleformat',ctrl:'CONTRATADA_OS',prop:'Titleformat'},{av:'edtContratada_OS_Title',ctrl:'CONTRATADA_OS',prop:'Title'},{av:'edtContratada_SS_Titleformat',ctrl:'CONTRATADA_SS',prop:'Titleformat'},{av:'edtContratada_SS_Title',ctrl:'CONTRATADA_SS',prop:'Title'},{av:'edtContratada_Lote_Titleformat',ctrl:'CONTRATADA_LOTE',prop:'Titleformat'},{av:'edtContratada_Lote_Title',ctrl:'CONTRATADA_LOTE',prop:'Title'},{av:'AV37GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV38GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11P62',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18Contratada_AreaTrabalhoDes1',fld:'vCONTRATADA_AREATRABALHODES1',pic:'@!',nv:''},{av:'AV19Contratada_PessoaNom1',fld:'vCONTRATADA_PESSOANOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23Contratada_AreaTrabalhoDes2',fld:'vCONTRATADA_AREATRABALHODES2',pic:'@!',nv:''},{av:'AV24Contratada_PessoaNom2',fld:'vCONTRATADA_PESSOANOM2',pic:'@!',nv:''},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV28Contratada_AreaTrabalhoDes3',fld:'vCONTRATADA_AREATRABALHODES3',pic:'@!',nv:''},{av:'AV29Contratada_PessoaNom3',fld:'vCONTRATADA_PESSOANOM3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV7Contratada_MunicipioCod',fld:'vCONTRATADA_MUNICIPIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV44Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A52Contratada_AreaTrabalhoCod',fld:'CONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'sPrefix',nv:''},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("GRID.LOAD","{handler:'E24P62',iparms:[{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A52Contratada_AreaTrabalhoCod',fld:'CONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'edtavUpdate_Link',ctrl:'vUPDATE',prop:'Link'},{av:'AV32Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Enabled',ctrl:'vUPDATE',prop:'Enabled'},{av:'edtavDelete_Tooltiptext',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'edtavDelete_Link',ctrl:'vDELETE',prop:'Link'},{av:'AV33Delete',fld:'vDELETE',pic:'',nv:''},{av:'edtavDelete_Enabled',ctrl:'vDELETE',prop:'Enabled'},{av:'edtavDisplay_Tooltiptext',ctrl:'vDISPLAY',prop:'Tooltiptext'},{av:'edtavDisplay_Link',ctrl:'vDISPLAY',prop:'Link'},{av:'AV34Display',fld:'vDISPLAY',pic:'',nv:''},{av:'edtavDisplay_Enabled',ctrl:'vDISPLAY',prop:'Enabled'},{av:'edtContratada_AreaTrabalhoDes_Link',ctrl:'CONTRATADA_AREATRABALHODES',prop:'Link'},{av:'edtContratada_PessoaNom_Link',ctrl:'CONTRATADA_PESSOANOM',prop:'Link'}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E12P62',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18Contratada_AreaTrabalhoDes1',fld:'vCONTRATADA_AREATRABALHODES1',pic:'@!',nv:''},{av:'AV19Contratada_PessoaNom1',fld:'vCONTRATADA_PESSOANOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23Contratada_AreaTrabalhoDes2',fld:'vCONTRATADA_AREATRABALHODES2',pic:'@!',nv:''},{av:'AV24Contratada_PessoaNom2',fld:'vCONTRATADA_PESSOANOM2',pic:'@!',nv:''},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV28Contratada_AreaTrabalhoDes3',fld:'vCONTRATADA_AREATRABALHODES3',pic:'@!',nv:''},{av:'AV29Contratada_PessoaNom3',fld:'vCONTRATADA_PESSOANOM3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV7Contratada_MunicipioCod',fld:'vCONTRATADA_MUNICIPIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV44Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A52Contratada_AreaTrabalhoCod',fld:'CONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'sPrefix',nv:''}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E17P62',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18Contratada_AreaTrabalhoDes1',fld:'vCONTRATADA_AREATRABALHODES1',pic:'@!',nv:''},{av:'AV19Contratada_PessoaNom1',fld:'vCONTRATADA_PESSOANOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23Contratada_AreaTrabalhoDes2',fld:'vCONTRATADA_AREATRABALHODES2',pic:'@!',nv:''},{av:'AV24Contratada_PessoaNom2',fld:'vCONTRATADA_PESSOANOM2',pic:'@!',nv:''},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV28Contratada_AreaTrabalhoDes3',fld:'vCONTRATADA_AREATRABALHODES3',pic:'@!',nv:''},{av:'AV29Contratada_PessoaNom3',fld:'vCONTRATADA_PESSOANOM3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV7Contratada_MunicipioCod',fld:'vCONTRATADA_MUNICIPIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV44Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A52Contratada_AreaTrabalhoCod',fld:'CONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'sPrefix',nv:''}],oparms:[{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E13P62',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18Contratada_AreaTrabalhoDes1',fld:'vCONTRATADA_AREATRABALHODES1',pic:'@!',nv:''},{av:'AV19Contratada_PessoaNom1',fld:'vCONTRATADA_PESSOANOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23Contratada_AreaTrabalhoDes2',fld:'vCONTRATADA_AREATRABALHODES2',pic:'@!',nv:''},{av:'AV24Contratada_PessoaNom2',fld:'vCONTRATADA_PESSOANOM2',pic:'@!',nv:''},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV28Contratada_AreaTrabalhoDes3',fld:'vCONTRATADA_AREATRABALHODES3',pic:'@!',nv:''},{av:'AV29Contratada_PessoaNom3',fld:'vCONTRATADA_PESSOANOM3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV7Contratada_MunicipioCod',fld:'vCONTRATADA_MUNICIPIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV44Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A52Contratada_AreaTrabalhoCod',fld:'CONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'sPrefix',nv:''}],oparms:[{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23Contratada_AreaTrabalhoDes2',fld:'vCONTRATADA_AREATRABALHODES2',pic:'@!',nv:''},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV28Contratada_AreaTrabalhoDes3',fld:'vCONTRATADA_AREATRABALHODES3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18Contratada_AreaTrabalhoDes1',fld:'vCONTRATADA_AREATRABALHODES1',pic:'@!',nv:''},{av:'AV19Contratada_PessoaNom1',fld:'vCONTRATADA_PESSOANOM1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV24Contratada_PessoaNom2',fld:'vCONTRATADA_PESSOANOM2',pic:'@!',nv:''},{av:'AV29Contratada_PessoaNom3',fld:'vCONTRATADA_PESSOANOM3',pic:'@!',nv:''},{av:'edtavContratada_areatrabalhodes2_Visible',ctrl:'vCONTRATADA_AREATRABALHODES2',prop:'Visible'},{av:'edtavContratada_pessoanom2_Visible',ctrl:'vCONTRATADA_PESSOANOM2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavContratada_areatrabalhodes3_Visible',ctrl:'vCONTRATADA_AREATRABALHODES3',prop:'Visible'},{av:'edtavContratada_pessoanom3_Visible',ctrl:'vCONTRATADA_PESSOANOM3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavContratada_areatrabalhodes1_Visible',ctrl:'vCONTRATADA_AREATRABALHODES1',prop:'Visible'},{av:'edtavContratada_pessoanom1_Visible',ctrl:'vCONTRATADA_PESSOANOM1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E18P62',iparms:[{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'edtavContratada_areatrabalhodes1_Visible',ctrl:'vCONTRATADA_AREATRABALHODES1',prop:'Visible'},{av:'edtavContratada_pessoanom1_Visible',ctrl:'vCONTRATADA_PESSOANOM1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E19P62',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18Contratada_AreaTrabalhoDes1',fld:'vCONTRATADA_AREATRABALHODES1',pic:'@!',nv:''},{av:'AV19Contratada_PessoaNom1',fld:'vCONTRATADA_PESSOANOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23Contratada_AreaTrabalhoDes2',fld:'vCONTRATADA_AREATRABALHODES2',pic:'@!',nv:''},{av:'AV24Contratada_PessoaNom2',fld:'vCONTRATADA_PESSOANOM2',pic:'@!',nv:''},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV28Contratada_AreaTrabalhoDes3',fld:'vCONTRATADA_AREATRABALHODES3',pic:'@!',nv:''},{av:'AV29Contratada_PessoaNom3',fld:'vCONTRATADA_PESSOANOM3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV7Contratada_MunicipioCod',fld:'vCONTRATADA_MUNICIPIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV44Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A52Contratada_AreaTrabalhoCod',fld:'CONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'sPrefix',nv:''}],oparms:[{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E14P62',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18Contratada_AreaTrabalhoDes1',fld:'vCONTRATADA_AREATRABALHODES1',pic:'@!',nv:''},{av:'AV19Contratada_PessoaNom1',fld:'vCONTRATADA_PESSOANOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23Contratada_AreaTrabalhoDes2',fld:'vCONTRATADA_AREATRABALHODES2',pic:'@!',nv:''},{av:'AV24Contratada_PessoaNom2',fld:'vCONTRATADA_PESSOANOM2',pic:'@!',nv:''},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV28Contratada_AreaTrabalhoDes3',fld:'vCONTRATADA_AREATRABALHODES3',pic:'@!',nv:''},{av:'AV29Contratada_PessoaNom3',fld:'vCONTRATADA_PESSOANOM3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV7Contratada_MunicipioCod',fld:'vCONTRATADA_MUNICIPIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV44Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A52Contratada_AreaTrabalhoCod',fld:'CONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'sPrefix',nv:''}],oparms:[{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23Contratada_AreaTrabalhoDes2',fld:'vCONTRATADA_AREATRABALHODES2',pic:'@!',nv:''},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV28Contratada_AreaTrabalhoDes3',fld:'vCONTRATADA_AREATRABALHODES3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18Contratada_AreaTrabalhoDes1',fld:'vCONTRATADA_AREATRABALHODES1',pic:'@!',nv:''},{av:'AV19Contratada_PessoaNom1',fld:'vCONTRATADA_PESSOANOM1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV24Contratada_PessoaNom2',fld:'vCONTRATADA_PESSOANOM2',pic:'@!',nv:''},{av:'AV29Contratada_PessoaNom3',fld:'vCONTRATADA_PESSOANOM3',pic:'@!',nv:''},{av:'edtavContratada_areatrabalhodes2_Visible',ctrl:'vCONTRATADA_AREATRABALHODES2',prop:'Visible'},{av:'edtavContratada_pessoanom2_Visible',ctrl:'vCONTRATADA_PESSOANOM2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavContratada_areatrabalhodes3_Visible',ctrl:'vCONTRATADA_AREATRABALHODES3',prop:'Visible'},{av:'edtavContratada_pessoanom3_Visible',ctrl:'vCONTRATADA_PESSOANOM3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavContratada_areatrabalhodes1_Visible',ctrl:'vCONTRATADA_AREATRABALHODES1',prop:'Visible'},{av:'edtavContratada_pessoanom1_Visible',ctrl:'vCONTRATADA_PESSOANOM1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E20P62',iparms:[{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'edtavContratada_areatrabalhodes2_Visible',ctrl:'vCONTRATADA_AREATRABALHODES2',prop:'Visible'},{av:'edtavContratada_pessoanom2_Visible',ctrl:'vCONTRATADA_PESSOANOM2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E15P62',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18Contratada_AreaTrabalhoDes1',fld:'vCONTRATADA_AREATRABALHODES1',pic:'@!',nv:''},{av:'AV19Contratada_PessoaNom1',fld:'vCONTRATADA_PESSOANOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23Contratada_AreaTrabalhoDes2',fld:'vCONTRATADA_AREATRABALHODES2',pic:'@!',nv:''},{av:'AV24Contratada_PessoaNom2',fld:'vCONTRATADA_PESSOANOM2',pic:'@!',nv:''},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV28Contratada_AreaTrabalhoDes3',fld:'vCONTRATADA_AREATRABALHODES3',pic:'@!',nv:''},{av:'AV29Contratada_PessoaNom3',fld:'vCONTRATADA_PESSOANOM3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV7Contratada_MunicipioCod',fld:'vCONTRATADA_MUNICIPIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV44Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A52Contratada_AreaTrabalhoCod',fld:'CONTRATADA_AREATRABALHOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'sPrefix',nv:''}],oparms:[{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23Contratada_AreaTrabalhoDes2',fld:'vCONTRATADA_AREATRABALHODES2',pic:'@!',nv:''},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV28Contratada_AreaTrabalhoDes3',fld:'vCONTRATADA_AREATRABALHODES3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18Contratada_AreaTrabalhoDes1',fld:'vCONTRATADA_AREATRABALHODES1',pic:'@!',nv:''},{av:'AV19Contratada_PessoaNom1',fld:'vCONTRATADA_PESSOANOM1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV24Contratada_PessoaNom2',fld:'vCONTRATADA_PESSOANOM2',pic:'@!',nv:''},{av:'AV29Contratada_PessoaNom3',fld:'vCONTRATADA_PESSOANOM3',pic:'@!',nv:''},{av:'edtavContratada_areatrabalhodes2_Visible',ctrl:'vCONTRATADA_AREATRABALHODES2',prop:'Visible'},{av:'edtavContratada_pessoanom2_Visible',ctrl:'vCONTRATADA_PESSOANOM2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavContratada_areatrabalhodes3_Visible',ctrl:'vCONTRATADA_AREATRABALHODES3',prop:'Visible'},{av:'edtavContratada_pessoanom3_Visible',ctrl:'vCONTRATADA_PESSOANOM3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavContratada_areatrabalhodes1_Visible',ctrl:'vCONTRATADA_AREATRABALHODES1',prop:'Visible'},{av:'edtavContratada_pessoanom1_Visible',ctrl:'vCONTRATADA_PESSOANOM1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E21P62',iparms:[{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'edtavContratada_areatrabalhodes3_Visible',ctrl:'vCONTRATADA_AREATRABALHODES3',prop:'Visible'},{av:'edtavContratada_pessoanom3_Visible',ctrl:'vCONTRATADA_PESSOANOM3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'}]}");
         setEventMetadata("'DOINSERT'","{handler:'E16P62',iparms:[{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV16DynamicFiltersSelector1 = "";
         AV18Contratada_AreaTrabalhoDes1 = "";
         AV19Contratada_PessoaNom1 = "";
         AV21DynamicFiltersSelector2 = "";
         AV23Contratada_AreaTrabalhoDes2 = "";
         AV24Contratada_PessoaNom2 = "";
         AV26DynamicFiltersSelector3 = "";
         AV28Contratada_AreaTrabalhoDes3 = "";
         AV29Contratada_PessoaNom3 = "";
         AV44Pgmname = "";
         AV11GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GX_FocusControl = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV32Update = "";
         AV41Update_GXI = "";
         AV33Delete = "";
         AV42Delete_GXI = "";
         AV34Display = "";
         AV43Display_GXI = "";
         A53Contratada_AreaTrabalhoDes = "";
         A1592Contratada_AreaTrbClcPFnl = "";
         A41Contratada_PessoaNom = "";
         A42Contratada_PessoaCNPJ = "";
         A518Pessoa_IE = "";
         A519Pessoa_Endereco = "";
         A521Pessoa_CEP = "";
         A522Pessoa_Telefone = "";
         A523Pessoa_Fax = "";
         A438Contratada_Sigla = "";
         A516Contratada_TipoFabrica = "";
         A342Contratada_BancoNome = "";
         A343Contratada_BancoNro = "";
         A344Contratada_AgenciaNome = "";
         A345Contratada_AgenciaNro = "";
         A51Contratada_ContaCorrente = "";
         A1127Contratada_LogoArquivo = "";
         GXCCtl = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV18Contratada_AreaTrabalhoDes1 = "";
         lV19Contratada_PessoaNom1 = "";
         lV23Contratada_AreaTrabalhoDes2 = "";
         lV24Contratada_PessoaNom2 = "";
         lV28Contratada_AreaTrabalhoDes3 = "";
         lV29Contratada_PessoaNom3 = "";
         H00P62_A1128Contratada_LogoNomeArq = new String[] {""} ;
         H00P62_n1128Contratada_LogoNomeArq = new bool[] {false} ;
         H00P62_A1129Contratada_LogoTipoArq = new String[] {""} ;
         H00P62_n1129Contratada_LogoTipoArq = new bool[] {false} ;
         H00P62_A349Contratada_MunicipioCod = new int[1] ;
         H00P62_n349Contratada_MunicipioCod = new bool[] {false} ;
         H00P62_A530Contratada_Lote = new short[1] ;
         H00P62_n530Contratada_Lote = new bool[] {false} ;
         H00P62_A1451Contratada_SS = new int[1] ;
         H00P62_n1451Contratada_SS = new bool[] {false} ;
         H00P62_A524Contratada_OS = new int[1] ;
         H00P62_n524Contratada_OS = new bool[] {false} ;
         H00P62_A51Contratada_ContaCorrente = new String[] {""} ;
         H00P62_n51Contratada_ContaCorrente = new bool[] {false} ;
         H00P62_A345Contratada_AgenciaNro = new String[] {""} ;
         H00P62_n345Contratada_AgenciaNro = new bool[] {false} ;
         H00P62_A344Contratada_AgenciaNome = new String[] {""} ;
         H00P62_n344Contratada_AgenciaNome = new bool[] {false} ;
         H00P62_A343Contratada_BancoNro = new String[] {""} ;
         H00P62_n343Contratada_BancoNro = new bool[] {false} ;
         H00P62_A342Contratada_BancoNome = new String[] {""} ;
         H00P62_n342Contratada_BancoNome = new bool[] {false} ;
         H00P62_A516Contratada_TipoFabrica = new String[] {""} ;
         H00P62_A438Contratada_Sigla = new String[] {""} ;
         H00P62_A523Pessoa_Fax = new String[] {""} ;
         H00P62_n523Pessoa_Fax = new bool[] {false} ;
         H00P62_A522Pessoa_Telefone = new String[] {""} ;
         H00P62_n522Pessoa_Telefone = new bool[] {false} ;
         H00P62_A521Pessoa_CEP = new String[] {""} ;
         H00P62_n521Pessoa_CEP = new bool[] {false} ;
         H00P62_A519Pessoa_Endereco = new String[] {""} ;
         H00P62_n519Pessoa_Endereco = new bool[] {false} ;
         H00P62_A518Pessoa_IE = new String[] {""} ;
         H00P62_n518Pessoa_IE = new bool[] {false} ;
         H00P62_A42Contratada_PessoaCNPJ = new String[] {""} ;
         H00P62_n42Contratada_PessoaCNPJ = new bool[] {false} ;
         H00P62_A41Contratada_PessoaNom = new String[] {""} ;
         H00P62_n41Contratada_PessoaNom = new bool[] {false} ;
         H00P62_A40Contratada_PessoaCod = new int[1] ;
         H00P62_A1595Contratada_AreaTrbSrvPdr = new int[1] ;
         H00P62_n1595Contratada_AreaTrbSrvPdr = new bool[] {false} ;
         H00P62_A1592Contratada_AreaTrbClcPFnl = new String[] {""} ;
         H00P62_n1592Contratada_AreaTrbClcPFnl = new bool[] {false} ;
         H00P62_A53Contratada_AreaTrabalhoDes = new String[] {""} ;
         H00P62_n53Contratada_AreaTrabalhoDes = new bool[] {false} ;
         H00P62_A52Contratada_AreaTrabalhoCod = new int[1] ;
         H00P62_A39Contratada_Codigo = new int[1] ;
         H00P62_A1127Contratada_LogoArquivo = new String[] {""} ;
         H00P62_n1127Contratada_LogoArquivo = new bool[] {false} ;
         A1128Contratada_LogoNomeArq = "";
         edtContratada_LogoArquivo_Filename = "";
         A1129Contratada_LogoTipoArq = "";
         H00P63_AGRID_nRecordCount = new long[1] ;
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         GridRow = new GXWebRow();
         AV35Session = context.GetSession();
         AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8HTTPRequest = new GxHttpRequest( context);
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblOrderedtext_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         imgInsert_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlAV7Contratada_MunicipioCod = "";
         ROClassString = "";
         gxblobfileaux = new GxFile(context.GetPhysicalPath());
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.municipiocontratadawc__default(),
            new Object[][] {
                new Object[] {
               H00P62_A1128Contratada_LogoNomeArq, H00P62_n1128Contratada_LogoNomeArq, H00P62_A1129Contratada_LogoTipoArq, H00P62_n1129Contratada_LogoTipoArq, H00P62_A349Contratada_MunicipioCod, H00P62_n349Contratada_MunicipioCod, H00P62_A530Contratada_Lote, H00P62_n530Contratada_Lote, H00P62_A1451Contratada_SS, H00P62_n1451Contratada_SS,
               H00P62_A524Contratada_OS, H00P62_n524Contratada_OS, H00P62_A51Contratada_ContaCorrente, H00P62_n51Contratada_ContaCorrente, H00P62_A345Contratada_AgenciaNro, H00P62_n345Contratada_AgenciaNro, H00P62_A344Contratada_AgenciaNome, H00P62_n344Contratada_AgenciaNome, H00P62_A343Contratada_BancoNro, H00P62_n343Contratada_BancoNro,
               H00P62_A342Contratada_BancoNome, H00P62_n342Contratada_BancoNome, H00P62_A516Contratada_TipoFabrica, H00P62_A438Contratada_Sigla, H00P62_A523Pessoa_Fax, H00P62_n523Pessoa_Fax, H00P62_A522Pessoa_Telefone, H00P62_n522Pessoa_Telefone, H00P62_A521Pessoa_CEP, H00P62_n521Pessoa_CEP,
               H00P62_A519Pessoa_Endereco, H00P62_n519Pessoa_Endereco, H00P62_A518Pessoa_IE, H00P62_n518Pessoa_IE, H00P62_A42Contratada_PessoaCNPJ, H00P62_n42Contratada_PessoaCNPJ, H00P62_A41Contratada_PessoaNom, H00P62_n41Contratada_PessoaNom, H00P62_A40Contratada_PessoaCod, H00P62_A1595Contratada_AreaTrbSrvPdr,
               H00P62_n1595Contratada_AreaTrbSrvPdr, H00P62_A1592Contratada_AreaTrbClcPFnl, H00P62_n1592Contratada_AreaTrbClcPFnl, H00P62_A53Contratada_AreaTrabalhoDes, H00P62_n53Contratada_AreaTrabalhoDes, H00P62_A52Contratada_AreaTrabalhoCod, H00P62_A39Contratada_Codigo, H00P62_A1127Contratada_LogoArquivo, H00P62_n1127Contratada_LogoArquivo
               }
               , new Object[] {
               H00P63_AGRID_nRecordCount
               }
            }
         );
         AV44Pgmname = "MunicipioContratadaWC";
         /* GeneXus formulas. */
         AV44Pgmname = "MunicipioContratadaWC";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short nRC_GXsfl_84 ;
      private short nGXsfl_84_idx=1 ;
      private short AV14OrderedBy ;
      private short AV17DynamicFiltersOperator1 ;
      private short AV22DynamicFiltersOperator2 ;
      private short AV27DynamicFiltersOperator3 ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short A530Contratada_Lote ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_84_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtContratada_AreaTrabalhoCod_Titleformat ;
      private short edtContratada_AreaTrabalhoDes_Titleformat ;
      private short cmbContratada_AreaTrbClcPFnl_Titleformat ;
      private short edtContratada_AreaTrbSrvPdr_Titleformat ;
      private short edtContratada_PessoaCod_Titleformat ;
      private short edtContratada_PessoaNom_Titleformat ;
      private short edtContratada_PessoaCNPJ_Titleformat ;
      private short edtPessoa_IE_Titleformat ;
      private short edtPessoa_Endereco_Titleformat ;
      private short edtPessoa_CEP_Titleformat ;
      private short edtPessoa_Telefone_Titleformat ;
      private short edtPessoa_Fax_Titleformat ;
      private short edtContratada_Sigla_Titleformat ;
      private short cmbContratada_TipoFabrica_Titleformat ;
      private short edtContratada_BancoNome_Titleformat ;
      private short edtContratada_BancoNro_Titleformat ;
      private short edtContratada_AgenciaNome_Titleformat ;
      private short edtContratada_AgenciaNro_Titleformat ;
      private short edtContratada_ContaCorrente_Titleformat ;
      private short edtContratada_OS_Titleformat ;
      private short edtContratada_SS_Titleformat ;
      private short edtContratada_Lote_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV7Contratada_MunicipioCod ;
      private int wcpOAV7Contratada_MunicipioCod ;
      private int subGrid_Rows ;
      private int A39Contratada_Codigo ;
      private int A52Contratada_AreaTrabalhoCod ;
      private int Gridpaginationbar_Pagestoshow ;
      private int A349Contratada_MunicipioCod ;
      private int edtContratada_MunicipioCod_Visible ;
      private int A1595Contratada_AreaTrbSrvPdr ;
      private int A40Contratada_PessoaCod ;
      private int A524Contratada_OS ;
      private int A1451Contratada_SS ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int edtavOrdereddsc_Visible ;
      private int AV36PageToGo ;
      private int edtavUpdate_Enabled ;
      private int edtavDelete_Enabled ;
      private int edtavDisplay_Enabled ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int edtavContratada_areatrabalhodes1_Visible ;
      private int edtavContratada_pessoanom1_Visible ;
      private int edtavContratada_areatrabalhodes2_Visible ;
      private int edtavContratada_pessoanom2_Visible ;
      private int edtavContratada_areatrabalhodes3_Visible ;
      private int edtavContratada_pessoanom3_Visible ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long AV37GridCurrentPage ;
      private long AV38GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Gridpaginationbar_Selectedpage ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String sGXsfl_84_idx="0001" ;
      private String AV19Contratada_PessoaNom1 ;
      private String AV24Contratada_PessoaNom2 ;
      private String AV29Contratada_PessoaNom3 ;
      private String AV44Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String GX_FocusControl ;
      private String edtContratada_MunicipioCod_Internalname ;
      private String edtContratada_MunicipioCod_Jsonclick ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String cmbavOrderedby_Internalname ;
      private String edtavUpdate_Internalname ;
      private String edtavDelete_Internalname ;
      private String edtavDisplay_Internalname ;
      private String edtContratada_Codigo_Internalname ;
      private String edtContratada_AreaTrabalhoCod_Internalname ;
      private String edtContratada_AreaTrabalhoDes_Internalname ;
      private String cmbContratada_AreaTrbClcPFnl_Internalname ;
      private String A1592Contratada_AreaTrbClcPFnl ;
      private String edtContratada_AreaTrbSrvPdr_Internalname ;
      private String edtContratada_PessoaCod_Internalname ;
      private String A41Contratada_PessoaNom ;
      private String edtContratada_PessoaNom_Internalname ;
      private String edtContratada_PessoaCNPJ_Internalname ;
      private String A518Pessoa_IE ;
      private String edtPessoa_IE_Internalname ;
      private String edtPessoa_Endereco_Internalname ;
      private String A521Pessoa_CEP ;
      private String edtPessoa_CEP_Internalname ;
      private String A522Pessoa_Telefone ;
      private String edtPessoa_Telefone_Internalname ;
      private String A523Pessoa_Fax ;
      private String edtPessoa_Fax_Internalname ;
      private String A438Contratada_Sigla ;
      private String edtContratada_Sigla_Internalname ;
      private String cmbContratada_TipoFabrica_Internalname ;
      private String A516Contratada_TipoFabrica ;
      private String A342Contratada_BancoNome ;
      private String edtContratada_BancoNome_Internalname ;
      private String A343Contratada_BancoNro ;
      private String edtContratada_BancoNro_Internalname ;
      private String A344Contratada_AgenciaNome ;
      private String edtContratada_AgenciaNome_Internalname ;
      private String A345Contratada_AgenciaNro ;
      private String edtContratada_AgenciaNro_Internalname ;
      private String A51Contratada_ContaCorrente ;
      private String edtContratada_ContaCorrente_Internalname ;
      private String edtContratada_OS_Internalname ;
      private String edtContratada_SS_Internalname ;
      private String edtContratada_Lote_Internalname ;
      private String edtContratada_LogoArquivo_Internalname ;
      private String GXCCtl ;
      private String scmdbuf ;
      private String lV19Contratada_PessoaNom1 ;
      private String lV24Contratada_PessoaNom2 ;
      private String lV29Contratada_PessoaNom3 ;
      private String A1128Contratada_LogoNomeArq ;
      private String edtContratada_LogoArquivo_Filename ;
      private String A1129Contratada_LogoTipoArq ;
      private String edtContratada_LogoArquivo_Filetype ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Internalname ;
      private String edtavContratada_areatrabalhodes1_Internalname ;
      private String edtavContratada_pessoanom1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Internalname ;
      private String edtavContratada_areatrabalhodes2_Internalname ;
      private String edtavContratada_pessoanom2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Internalname ;
      private String edtavContratada_areatrabalhodes3_Internalname ;
      private String edtavContratada_pessoanom3_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String edtContratada_AreaTrabalhoCod_Title ;
      private String edtContratada_AreaTrabalhoDes_Title ;
      private String edtContratada_AreaTrbSrvPdr_Title ;
      private String edtContratada_PessoaCod_Title ;
      private String edtContratada_PessoaNom_Title ;
      private String edtContratada_PessoaCNPJ_Title ;
      private String edtPessoa_IE_Title ;
      private String edtPessoa_Endereco_Title ;
      private String edtPessoa_CEP_Title ;
      private String edtPessoa_Telefone_Title ;
      private String edtPessoa_Fax_Title ;
      private String edtContratada_Sigla_Title ;
      private String edtContratada_BancoNome_Title ;
      private String edtContratada_BancoNro_Title ;
      private String edtContratada_AgenciaNome_Title ;
      private String edtContratada_AgenciaNro_Title ;
      private String edtContratada_ContaCorrente_Title ;
      private String edtContratada_OS_Title ;
      private String edtContratada_SS_Title ;
      private String edtContratada_Lote_Title ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavUpdate_Link ;
      private String edtavDelete_Tooltiptext ;
      private String edtavDelete_Link ;
      private String edtavDisplay_Tooltiptext ;
      private String edtavDisplay_Link ;
      private String edtContratada_AreaTrabalhoDes_Link ;
      private String edtContratada_PessoaNom_Link ;
      private String sStyleString ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTablesearch_Internalname ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String tblTablemergeddynamicfilters3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Jsonclick ;
      private String edtavContratada_areatrabalhodes3_Jsonclick ;
      private String edtavContratada_pessoanom3_Jsonclick ;
      private String tblTablemergeddynamicfilters2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Jsonclick ;
      private String edtavContratada_areatrabalhodes2_Jsonclick ;
      private String edtavContratada_pessoanom2_Jsonclick ;
      private String tblTablemergeddynamicfilters1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Jsonclick ;
      private String edtavContratada_areatrabalhodes1_Jsonclick ;
      private String edtavContratada_pessoanom1_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String imgInsert_Internalname ;
      private String imgInsert_Jsonclick ;
      private String sCtrlAV7Contratada_MunicipioCod ;
      private String edtContratada_LogoNomeArq_Internalname ;
      private String sGXsfl_84_fel_idx="0001" ;
      private String ROClassString ;
      private String edtContratada_Codigo_Jsonclick ;
      private String edtContratada_AreaTrabalhoCod_Jsonclick ;
      private String edtContratada_AreaTrabalhoDes_Jsonclick ;
      private String cmbContratada_AreaTrbClcPFnl_Jsonclick ;
      private String edtContratada_AreaTrbSrvPdr_Jsonclick ;
      private String edtContratada_PessoaCod_Jsonclick ;
      private String edtContratada_PessoaNom_Jsonclick ;
      private String edtContratada_PessoaCNPJ_Jsonclick ;
      private String edtPessoa_IE_Jsonclick ;
      private String edtPessoa_Endereco_Jsonclick ;
      private String edtPessoa_CEP_Jsonclick ;
      private String edtPessoa_Telefone_Jsonclick ;
      private String edtPessoa_Fax_Jsonclick ;
      private String edtContratada_Sigla_Jsonclick ;
      private String cmbContratada_TipoFabrica_Jsonclick ;
      private String edtContratada_BancoNome_Jsonclick ;
      private String edtContratada_BancoNro_Jsonclick ;
      private String edtContratada_AgenciaNome_Jsonclick ;
      private String edtContratada_AgenciaNro_Jsonclick ;
      private String edtContratada_ContaCorrente_Jsonclick ;
      private String edtContratada_OS_Jsonclick ;
      private String edtContratada_SS_Jsonclick ;
      private String edtContratada_Lote_Jsonclick ;
      private String edtContratada_LogoArquivo_Contenttype ;
      private String edtContratada_LogoArquivo_Parameters ;
      private String edtContratada_LogoArquivo_Jsonclick ;
      private String edtContratada_LogoNomeArq_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV15OrderedDsc ;
      private bool AV20DynamicFiltersEnabled2 ;
      private bool AV25DynamicFiltersEnabled3 ;
      private bool AV31DynamicFiltersIgnoreFirst ;
      private bool AV30DynamicFiltersRemoving ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n53Contratada_AreaTrabalhoDes ;
      private bool n1592Contratada_AreaTrbClcPFnl ;
      private bool n1595Contratada_AreaTrbSrvPdr ;
      private bool n41Contratada_PessoaNom ;
      private bool n42Contratada_PessoaCNPJ ;
      private bool n518Pessoa_IE ;
      private bool n519Pessoa_Endereco ;
      private bool n521Pessoa_CEP ;
      private bool n522Pessoa_Telefone ;
      private bool n523Pessoa_Fax ;
      private bool n342Contratada_BancoNome ;
      private bool n343Contratada_BancoNro ;
      private bool n344Contratada_AgenciaNome ;
      private bool n345Contratada_AgenciaNro ;
      private bool n51Contratada_ContaCorrente ;
      private bool n524Contratada_OS ;
      private bool n1451Contratada_SS ;
      private bool n530Contratada_Lote ;
      private bool n1127Contratada_LogoArquivo ;
      private bool n1128Contratada_LogoNomeArq ;
      private bool n1129Contratada_LogoTipoArq ;
      private bool n349Contratada_MunicipioCod ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV32Update_IsBlob ;
      private bool AV33Delete_IsBlob ;
      private bool AV34Display_IsBlob ;
      private String AV16DynamicFiltersSelector1 ;
      private String AV18Contratada_AreaTrabalhoDes1 ;
      private String AV21DynamicFiltersSelector2 ;
      private String AV23Contratada_AreaTrabalhoDes2 ;
      private String AV26DynamicFiltersSelector3 ;
      private String AV28Contratada_AreaTrabalhoDes3 ;
      private String AV41Update_GXI ;
      private String AV42Delete_GXI ;
      private String AV43Display_GXI ;
      private String A53Contratada_AreaTrabalhoDes ;
      private String A42Contratada_PessoaCNPJ ;
      private String A519Pessoa_Endereco ;
      private String lV18Contratada_AreaTrabalhoDes1 ;
      private String lV23Contratada_AreaTrabalhoDes2 ;
      private String lV28Contratada_AreaTrabalhoDes3 ;
      private String AV32Update ;
      private String AV33Delete ;
      private String AV34Display ;
      private String A1127Contratada_LogoArquivo ;
      private IGxSession AV35Session ;
      private GxFile gxblobfileaux ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersoperator1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersoperator2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCombobox cmbavDynamicfiltersoperator3 ;
      private GXCombobox cmbContratada_AreaTrbClcPFnl ;
      private GXCombobox cmbContratada_TipoFabrica ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private String[] H00P62_A1128Contratada_LogoNomeArq ;
      private bool[] H00P62_n1128Contratada_LogoNomeArq ;
      private String[] H00P62_A1129Contratada_LogoTipoArq ;
      private bool[] H00P62_n1129Contratada_LogoTipoArq ;
      private int[] H00P62_A349Contratada_MunicipioCod ;
      private bool[] H00P62_n349Contratada_MunicipioCod ;
      private short[] H00P62_A530Contratada_Lote ;
      private bool[] H00P62_n530Contratada_Lote ;
      private int[] H00P62_A1451Contratada_SS ;
      private bool[] H00P62_n1451Contratada_SS ;
      private int[] H00P62_A524Contratada_OS ;
      private bool[] H00P62_n524Contratada_OS ;
      private String[] H00P62_A51Contratada_ContaCorrente ;
      private bool[] H00P62_n51Contratada_ContaCorrente ;
      private String[] H00P62_A345Contratada_AgenciaNro ;
      private bool[] H00P62_n345Contratada_AgenciaNro ;
      private String[] H00P62_A344Contratada_AgenciaNome ;
      private bool[] H00P62_n344Contratada_AgenciaNome ;
      private String[] H00P62_A343Contratada_BancoNro ;
      private bool[] H00P62_n343Contratada_BancoNro ;
      private String[] H00P62_A342Contratada_BancoNome ;
      private bool[] H00P62_n342Contratada_BancoNome ;
      private String[] H00P62_A516Contratada_TipoFabrica ;
      private String[] H00P62_A438Contratada_Sigla ;
      private String[] H00P62_A523Pessoa_Fax ;
      private bool[] H00P62_n523Pessoa_Fax ;
      private String[] H00P62_A522Pessoa_Telefone ;
      private bool[] H00P62_n522Pessoa_Telefone ;
      private String[] H00P62_A521Pessoa_CEP ;
      private bool[] H00P62_n521Pessoa_CEP ;
      private String[] H00P62_A519Pessoa_Endereco ;
      private bool[] H00P62_n519Pessoa_Endereco ;
      private String[] H00P62_A518Pessoa_IE ;
      private bool[] H00P62_n518Pessoa_IE ;
      private String[] H00P62_A42Contratada_PessoaCNPJ ;
      private bool[] H00P62_n42Contratada_PessoaCNPJ ;
      private String[] H00P62_A41Contratada_PessoaNom ;
      private bool[] H00P62_n41Contratada_PessoaNom ;
      private int[] H00P62_A40Contratada_PessoaCod ;
      private int[] H00P62_A1595Contratada_AreaTrbSrvPdr ;
      private bool[] H00P62_n1595Contratada_AreaTrbSrvPdr ;
      private String[] H00P62_A1592Contratada_AreaTrbClcPFnl ;
      private bool[] H00P62_n1592Contratada_AreaTrbClcPFnl ;
      private String[] H00P62_A53Contratada_AreaTrabalhoDes ;
      private bool[] H00P62_n53Contratada_AreaTrabalhoDes ;
      private int[] H00P62_A52Contratada_AreaTrabalhoCod ;
      private int[] H00P62_A39Contratada_Codigo ;
      private String[] H00P62_A1127Contratada_LogoArquivo ;
      private bool[] H00P62_n1127Contratada_LogoArquivo ;
      private long[] H00P63_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV8HTTPRequest ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV10TrnContextAtt ;
      private wwpbaseobjects.SdtWWPGridState AV11GridState ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV13GridStateDynamicFilter ;
   }

   public class municipiocontratadawc__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00P62( IGxContext context ,
                                             String AV16DynamicFiltersSelector1 ,
                                             short AV17DynamicFiltersOperator1 ,
                                             String AV18Contratada_AreaTrabalhoDes1 ,
                                             String AV19Contratada_PessoaNom1 ,
                                             bool AV20DynamicFiltersEnabled2 ,
                                             String AV21DynamicFiltersSelector2 ,
                                             short AV22DynamicFiltersOperator2 ,
                                             String AV23Contratada_AreaTrabalhoDes2 ,
                                             String AV24Contratada_PessoaNom2 ,
                                             bool AV25DynamicFiltersEnabled3 ,
                                             String AV26DynamicFiltersSelector3 ,
                                             short AV27DynamicFiltersOperator3 ,
                                             String AV28Contratada_AreaTrabalhoDes3 ,
                                             String AV29Contratada_PessoaNom3 ,
                                             String A53Contratada_AreaTrabalhoDes ,
                                             String A41Contratada_PessoaNom ,
                                             short AV14OrderedBy ,
                                             bool AV15OrderedDsc ,
                                             int A349Contratada_MunicipioCod ,
                                             int AV7Contratada_MunicipioCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [18] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T1.[Contratada_LogoNomeArq], T1.[Contratada_LogoTipoArq], T1.[Contratada_MunicipioCod], T1.[Contratada_Lote], T1.[Contratada_SS], T1.[Contratada_OS], T1.[Contratada_ContaCorrente], T1.[Contratada_AgenciaNro], T1.[Contratada_AgenciaNome], T1.[Contratada_BancoNro], T1.[Contratada_BancoNome], T1.[Contratada_TipoFabrica], T1.[Contratada_Sigla], T2.[Pessoa_Fax], T2.[Pessoa_Telefone], T2.[Pessoa_CEP], T2.[Pessoa_Endereco], T2.[Pessoa_IE], T2.[Pessoa_Docto] AS Contratada_PessoaCNPJ, T2.[Pessoa_Nome] AS Contratada_PessoaNom, T1.[Contratada_PessoaCod] AS Contratada_PessoaCod, T3.[AreaTrabalho_ServicoPadrao] AS Contratada_AreaTrbSrvPdr, T3.[AreaTrabalho_CalculoPFinal] AS Contratada_AreaTrbClcPFnl, T3.[AreaTrabalho_Descricao] AS Contratada_AreaTrabalhoDes, T1.[Contratada_AreaTrabalhoCod] AS Contratada_AreaTrabalhoCod, T1.[Contratada_Codigo], T1.[Contratada_LogoArquivo]";
         sFromString = " FROM (([Contratada] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Contratada_PessoaCod]) INNER JOIN [AreaTrabalho] T3 WITH (NOLOCK) ON T3.[AreaTrabalho_Codigo] = T1.[Contratada_AreaTrabalhoCod])";
         sOrderString = "";
         sWhereString = sWhereString + " WHERE (T1.[Contratada_MunicipioCod] = @AV7Contratada_MunicipioCod)";
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTRATADA_AREATRABALHODES") == 0 ) && ( AV17DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18Contratada_AreaTrabalhoDes1)) ) )
         {
            sWhereString = sWhereString + " and (T3.[AreaTrabalho_Descricao] like @lV18Contratada_AreaTrabalhoDes1)";
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTRATADA_AREATRABALHODES") == 0 ) && ( AV17DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18Contratada_AreaTrabalhoDes1)) ) )
         {
            sWhereString = sWhereString + " and (T3.[AreaTrabalho_Descricao] like '%' + @lV18Contratada_AreaTrabalhoDes1)";
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTRATADA_PESSOANOM") == 0 ) && ( AV17DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19Contratada_PessoaNom1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like @lV19Contratada_PessoaNom1)";
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTRATADA_PESSOANOM") == 0 ) && ( AV17DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19Contratada_PessoaNom1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like '%' + @lV19Contratada_PessoaNom1)";
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTRATADA_AREATRABALHODES") == 0 ) && ( AV22DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23Contratada_AreaTrabalhoDes2)) ) )
         {
            sWhereString = sWhereString + " and (T3.[AreaTrabalho_Descricao] like @lV23Contratada_AreaTrabalhoDes2)";
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTRATADA_AREATRABALHODES") == 0 ) && ( AV22DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23Contratada_AreaTrabalhoDes2)) ) )
         {
            sWhereString = sWhereString + " and (T3.[AreaTrabalho_Descricao] like '%' + @lV23Contratada_AreaTrabalhoDes2)";
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTRATADA_PESSOANOM") == 0 ) && ( AV22DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV24Contratada_PessoaNom2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like @lV24Contratada_PessoaNom2)";
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTRATADA_PESSOANOM") == 0 ) && ( AV22DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV24Contratada_PessoaNom2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like '%' + @lV24Contratada_PessoaNom2)";
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( AV25DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "CONTRATADA_AREATRABALHODES") == 0 ) && ( AV27DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV28Contratada_AreaTrabalhoDes3)) ) )
         {
            sWhereString = sWhereString + " and (T3.[AreaTrabalho_Descricao] like @lV28Contratada_AreaTrabalhoDes3)";
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( AV25DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "CONTRATADA_AREATRABALHODES") == 0 ) && ( AV27DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV28Contratada_AreaTrabalhoDes3)) ) )
         {
            sWhereString = sWhereString + " and (T3.[AreaTrabalho_Descricao] like '%' + @lV28Contratada_AreaTrabalhoDes3)";
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( AV25DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "CONTRATADA_PESSOANOM") == 0 ) && ( AV27DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV29Contratada_PessoaNom3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like @lV29Contratada_PessoaNom3)";
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( AV25DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "CONTRATADA_PESSOANOM") == 0 ) && ( AV27DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV29Contratada_PessoaNom3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like '%' + @lV29Contratada_PessoaNom3)";
         }
         else
         {
            GXv_int1[12] = 1;
         }
         if ( ( AV14OrderedBy == 1 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contratada_MunicipioCod], T1.[Contratada_AreaTrabalhoCod]";
         }
         else if ( ( AV14OrderedBy == 1 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contratada_MunicipioCod] DESC, T1.[Contratada_AreaTrabalhoCod] DESC";
         }
         else if ( ( AV14OrderedBy == 2 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contratada_MunicipioCod], T3.[AreaTrabalho_Descricao]";
         }
         else if ( ( AV14OrderedBy == 2 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contratada_MunicipioCod] DESC, T3.[AreaTrabalho_Descricao] DESC";
         }
         else if ( ( AV14OrderedBy == 3 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contratada_MunicipioCod], T3.[AreaTrabalho_CalculoPFinal]";
         }
         else if ( ( AV14OrderedBy == 3 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contratada_MunicipioCod] DESC, T3.[AreaTrabalho_CalculoPFinal] DESC";
         }
         else if ( ( AV14OrderedBy == 4 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contratada_MunicipioCod], T3.[AreaTrabalho_ServicoPadrao]";
         }
         else if ( ( AV14OrderedBy == 4 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contratada_MunicipioCod] DESC, T3.[AreaTrabalho_ServicoPadrao] DESC";
         }
         else if ( ( AV14OrderedBy == 5 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contratada_MunicipioCod], T1.[Contratada_PessoaCod]";
         }
         else if ( ( AV14OrderedBy == 5 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contratada_MunicipioCod] DESC, T1.[Contratada_PessoaCod] DESC";
         }
         else if ( ( AV14OrderedBy == 6 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contratada_MunicipioCod], T2.[Pessoa_Nome]";
         }
         else if ( ( AV14OrderedBy == 6 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contratada_MunicipioCod] DESC, T2.[Pessoa_Nome] DESC";
         }
         else if ( ( AV14OrderedBy == 7 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contratada_MunicipioCod], T2.[Pessoa_Docto]";
         }
         else if ( ( AV14OrderedBy == 7 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contratada_MunicipioCod] DESC, T2.[Pessoa_Docto] DESC";
         }
         else if ( ( AV14OrderedBy == 8 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contratada_MunicipioCod], T2.[Pessoa_IE]";
         }
         else if ( ( AV14OrderedBy == 8 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contratada_MunicipioCod] DESC, T2.[Pessoa_IE] DESC";
         }
         else if ( ( AV14OrderedBy == 9 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contratada_MunicipioCod], T2.[Pessoa_Endereco]";
         }
         else if ( ( AV14OrderedBy == 9 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contratada_MunicipioCod] DESC, T2.[Pessoa_Endereco] DESC";
         }
         else if ( ( AV14OrderedBy == 10 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contratada_MunicipioCod], T2.[Pessoa_CEP]";
         }
         else if ( ( AV14OrderedBy == 10 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contratada_MunicipioCod] DESC, T2.[Pessoa_CEP] DESC";
         }
         else if ( ( AV14OrderedBy == 11 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contratada_MunicipioCod], T2.[Pessoa_Telefone]";
         }
         else if ( ( AV14OrderedBy == 11 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contratada_MunicipioCod] DESC, T2.[Pessoa_Telefone] DESC";
         }
         else if ( ( AV14OrderedBy == 12 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contratada_MunicipioCod], T2.[Pessoa_Fax]";
         }
         else if ( ( AV14OrderedBy == 12 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contratada_MunicipioCod] DESC, T2.[Pessoa_Fax] DESC";
         }
         else if ( ( AV14OrderedBy == 13 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contratada_MunicipioCod], T1.[Contratada_Sigla]";
         }
         else if ( ( AV14OrderedBy == 13 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contratada_MunicipioCod] DESC, T1.[Contratada_Sigla] DESC";
         }
         else if ( ( AV14OrderedBy == 14 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contratada_MunicipioCod], T1.[Contratada_TipoFabrica]";
         }
         else if ( ( AV14OrderedBy == 14 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contratada_MunicipioCod] DESC, T1.[Contratada_TipoFabrica] DESC";
         }
         else if ( ( AV14OrderedBy == 15 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contratada_MunicipioCod], T1.[Contratada_BancoNome]";
         }
         else if ( ( AV14OrderedBy == 15 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contratada_MunicipioCod] DESC, T1.[Contratada_BancoNome] DESC";
         }
         else if ( ( AV14OrderedBy == 16 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contratada_MunicipioCod], T1.[Contratada_BancoNro]";
         }
         else if ( ( AV14OrderedBy == 16 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contratada_MunicipioCod] DESC, T1.[Contratada_BancoNro] DESC";
         }
         else if ( ( AV14OrderedBy == 17 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contratada_MunicipioCod], T1.[Contratada_AgenciaNome]";
         }
         else if ( ( AV14OrderedBy == 17 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contratada_MunicipioCod] DESC, T1.[Contratada_AgenciaNome] DESC";
         }
         else if ( ( AV14OrderedBy == 18 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contratada_MunicipioCod], T1.[Contratada_AgenciaNro]";
         }
         else if ( ( AV14OrderedBy == 18 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contratada_MunicipioCod] DESC, T1.[Contratada_AgenciaNro] DESC";
         }
         else if ( ( AV14OrderedBy == 19 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contratada_MunicipioCod], T1.[Contratada_ContaCorrente]";
         }
         else if ( ( AV14OrderedBy == 19 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contratada_MunicipioCod] DESC, T1.[Contratada_ContaCorrente] DESC";
         }
         else if ( ( AV14OrderedBy == 20 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contratada_MunicipioCod], T1.[Contratada_OS]";
         }
         else if ( ( AV14OrderedBy == 20 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contratada_MunicipioCod] DESC, T1.[Contratada_OS] DESC";
         }
         else if ( ( AV14OrderedBy == 21 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contratada_MunicipioCod], T1.[Contratada_SS]";
         }
         else if ( ( AV14OrderedBy == 21 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contratada_MunicipioCod] DESC, T1.[Contratada_SS] DESC";
         }
         else if ( ( AV14OrderedBy == 22 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contratada_MunicipioCod], T1.[Contratada_Lote]";
         }
         else if ( ( AV14OrderedBy == 22 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contratada_MunicipioCod] DESC, T1.[Contratada_Lote] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contratada_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_H00P63( IGxContext context ,
                                             String AV16DynamicFiltersSelector1 ,
                                             short AV17DynamicFiltersOperator1 ,
                                             String AV18Contratada_AreaTrabalhoDes1 ,
                                             String AV19Contratada_PessoaNom1 ,
                                             bool AV20DynamicFiltersEnabled2 ,
                                             String AV21DynamicFiltersSelector2 ,
                                             short AV22DynamicFiltersOperator2 ,
                                             String AV23Contratada_AreaTrabalhoDes2 ,
                                             String AV24Contratada_PessoaNom2 ,
                                             bool AV25DynamicFiltersEnabled3 ,
                                             String AV26DynamicFiltersSelector3 ,
                                             short AV27DynamicFiltersOperator3 ,
                                             String AV28Contratada_AreaTrabalhoDes3 ,
                                             String AV29Contratada_PessoaNom3 ,
                                             String A53Contratada_AreaTrabalhoDes ,
                                             String A41Contratada_PessoaNom ,
                                             short AV14OrderedBy ,
                                             bool AV15OrderedDsc ,
                                             int A349Contratada_MunicipioCod ,
                                             int AV7Contratada_MunicipioCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [13] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM (([Contratada] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Contratada_PessoaCod]) INNER JOIN [AreaTrabalho] T3 WITH (NOLOCK) ON T3.[AreaTrabalho_Codigo] = T1.[Contratada_AreaTrabalhoCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[Contratada_MunicipioCod] = @AV7Contratada_MunicipioCod)";
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTRATADA_AREATRABALHODES") == 0 ) && ( AV17DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18Contratada_AreaTrabalhoDes1)) ) )
         {
            sWhereString = sWhereString + " and (T3.[AreaTrabalho_Descricao] like @lV18Contratada_AreaTrabalhoDes1)";
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTRATADA_AREATRABALHODES") == 0 ) && ( AV17DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18Contratada_AreaTrabalhoDes1)) ) )
         {
            sWhereString = sWhereString + " and (T3.[AreaTrabalho_Descricao] like '%' + @lV18Contratada_AreaTrabalhoDes1)";
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTRATADA_PESSOANOM") == 0 ) && ( AV17DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19Contratada_PessoaNom1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like @lV19Contratada_PessoaNom1)";
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "CONTRATADA_PESSOANOM") == 0 ) && ( AV17DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19Contratada_PessoaNom1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like '%' + @lV19Contratada_PessoaNom1)";
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTRATADA_AREATRABALHODES") == 0 ) && ( AV22DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23Contratada_AreaTrabalhoDes2)) ) )
         {
            sWhereString = sWhereString + " and (T3.[AreaTrabalho_Descricao] like @lV23Contratada_AreaTrabalhoDes2)";
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTRATADA_AREATRABALHODES") == 0 ) && ( AV22DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23Contratada_AreaTrabalhoDes2)) ) )
         {
            sWhereString = sWhereString + " and (T3.[AreaTrabalho_Descricao] like '%' + @lV23Contratada_AreaTrabalhoDes2)";
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTRATADA_PESSOANOM") == 0 ) && ( AV22DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV24Contratada_PessoaNom2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like @lV24Contratada_PessoaNom2)";
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "CONTRATADA_PESSOANOM") == 0 ) && ( AV22DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV24Contratada_PessoaNom2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like '%' + @lV24Contratada_PessoaNom2)";
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( AV25DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "CONTRATADA_AREATRABALHODES") == 0 ) && ( AV27DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV28Contratada_AreaTrabalhoDes3)) ) )
         {
            sWhereString = sWhereString + " and (T3.[AreaTrabalho_Descricao] like @lV28Contratada_AreaTrabalhoDes3)";
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( AV25DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "CONTRATADA_AREATRABALHODES") == 0 ) && ( AV27DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV28Contratada_AreaTrabalhoDes3)) ) )
         {
            sWhereString = sWhereString + " and (T3.[AreaTrabalho_Descricao] like '%' + @lV28Contratada_AreaTrabalhoDes3)";
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( AV25DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "CONTRATADA_PESSOANOM") == 0 ) && ( AV27DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV29Contratada_PessoaNom3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like @lV29Contratada_PessoaNom3)";
         }
         else
         {
            GXv_int3[11] = 1;
         }
         if ( AV25DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "CONTRATADA_PESSOANOM") == 0 ) && ( AV27DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV29Contratada_PessoaNom3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like '%' + @lV29Contratada_PessoaNom3)";
         }
         else
         {
            GXv_int3[12] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( ( AV14OrderedBy == 1 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 1 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 2 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 2 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 3 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 3 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 4 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 4 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 5 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 5 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 6 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 6 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 7 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 7 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 8 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 8 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 9 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 9 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 10 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 10 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 11 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 11 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 12 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 12 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 13 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 13 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 14 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 14 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 15 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 15 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 16 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 16 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 17 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 17 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 18 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 18 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 19 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 19 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 20 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 20 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 21 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 21 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 22 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 22 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00P62(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (bool)dynConstraints[4] , (String)dynConstraints[5] , (short)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (short)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (short)dynConstraints[16] , (bool)dynConstraints[17] , (int)dynConstraints[18] , (int)dynConstraints[19] );
               case 1 :
                     return conditional_H00P63(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (bool)dynConstraints[4] , (String)dynConstraints[5] , (short)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (short)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (short)dynConstraints[16] , (bool)dynConstraints[17] , (int)dynConstraints[18] , (int)dynConstraints[19] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00P62 ;
          prmH00P62 = new Object[] {
          new Object[] {"@AV7Contratada_MunicipioCod",SqlDbType.Int,6,0} ,
          new Object[] {"@lV18Contratada_AreaTrabalhoDes1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV18Contratada_AreaTrabalhoDes1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV19Contratada_PessoaNom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV19Contratada_PessoaNom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV23Contratada_AreaTrabalhoDes2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV23Contratada_AreaTrabalhoDes2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV24Contratada_PessoaNom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV24Contratada_PessoaNom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV28Contratada_AreaTrabalhoDes3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV28Contratada_AreaTrabalhoDes3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV29Contratada_PessoaNom3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV29Contratada_PessoaNom3",SqlDbType.Char,100,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00P63 ;
          prmH00P63 = new Object[] {
          new Object[] {"@AV7Contratada_MunicipioCod",SqlDbType.Int,6,0} ,
          new Object[] {"@lV18Contratada_AreaTrabalhoDes1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV18Contratada_AreaTrabalhoDes1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV19Contratada_PessoaNom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV19Contratada_PessoaNom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV23Contratada_AreaTrabalhoDes2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV23Contratada_AreaTrabalhoDes2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV24Contratada_PessoaNom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV24Contratada_PessoaNom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV28Contratada_AreaTrabalhoDes3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV28Contratada_AreaTrabalhoDes3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV29Contratada_PessoaNom3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV29Contratada_PessoaNom3",SqlDbType.Char,100,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00P62", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00P62,11,0,true,false )
             ,new CursorDef("H00P63", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00P63,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 10) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((short[]) buf[6])[0] = rslt.getShort(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((int[]) buf[8])[0] = rslt.getInt(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((int[]) buf[10])[0] = rslt.getInt(6) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((String[]) buf[12])[0] = rslt.getString(7, 10) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(7);
                ((String[]) buf[14])[0] = rslt.getString(8, 10) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(8);
                ((String[]) buf[16])[0] = rslt.getString(9, 50) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(9);
                ((String[]) buf[18])[0] = rslt.getString(10, 6) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(10);
                ((String[]) buf[20])[0] = rslt.getString(11, 50) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(11);
                ((String[]) buf[22])[0] = rslt.getString(12, 1) ;
                ((String[]) buf[23])[0] = rslt.getString(13, 15) ;
                ((String[]) buf[24])[0] = rslt.getString(14, 15) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(14);
                ((String[]) buf[26])[0] = rslt.getString(15, 15) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(15);
                ((String[]) buf[28])[0] = rslt.getString(16, 10) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(16);
                ((String[]) buf[30])[0] = rslt.getVarchar(17) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(17);
                ((String[]) buf[32])[0] = rslt.getString(18, 15) ;
                ((bool[]) buf[33])[0] = rslt.wasNull(18);
                ((String[]) buf[34])[0] = rslt.getVarchar(19) ;
                ((bool[]) buf[35])[0] = rslt.wasNull(19);
                ((String[]) buf[36])[0] = rslt.getString(20, 100) ;
                ((bool[]) buf[37])[0] = rslt.wasNull(20);
                ((int[]) buf[38])[0] = rslt.getInt(21) ;
                ((int[]) buf[39])[0] = rslt.getInt(22) ;
                ((bool[]) buf[40])[0] = rslt.wasNull(22);
                ((String[]) buf[41])[0] = rslt.getString(23, 2) ;
                ((bool[]) buf[42])[0] = rslt.wasNull(23);
                ((String[]) buf[43])[0] = rslt.getVarchar(24) ;
                ((bool[]) buf[44])[0] = rslt.wasNull(24);
                ((int[]) buf[45])[0] = rslt.getInt(25) ;
                ((int[]) buf[46])[0] = rslt.getInt(26) ;
                ((String[]) buf[47])[0] = rslt.getBLOBFile(27, rslt.getString(2, 10), rslt.getString(1, 50)) ;
                ((bool[]) buf[48])[0] = rslt.wasNull(27);
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[18]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[31]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[32]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[33]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[34]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[35]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[13]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                return;
       }
    }

 }

}
