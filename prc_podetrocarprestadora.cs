/*
               File: PRC_PodeTrocarPrestadora
        Description: Pode Trocar Prestadora
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:13:57.67
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_podetrocarprestadora : GXProcedure
   {
      public prc_podetrocarprestadora( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_podetrocarprestadora( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Codigo ,
                           int aP1_UserId ,
                           out bool aP2_PodeTrocar )
      {
         this.AV10Codigo = aP0_Codigo;
         this.AV9UserId = aP1_UserId;
         this.AV8PodeTrocar = false ;
         initialize();
         executePrivate();
         aP2_PodeTrocar=this.AV8PodeTrocar;
      }

      public bool executeUdp( int aP0_Codigo ,
                              int aP1_UserId )
      {
         this.AV10Codigo = aP0_Codigo;
         this.AV9UserId = aP1_UserId;
         this.AV8PodeTrocar = false ;
         initialize();
         executePrivate();
         aP2_PodeTrocar=this.AV8PodeTrocar;
         return AV8PodeTrocar ;
      }

      public void executeSubmit( int aP0_Codigo ,
                                 int aP1_UserId ,
                                 out bool aP2_PodeTrocar )
      {
         prc_podetrocarprestadora objprc_podetrocarprestadora;
         objprc_podetrocarprestadora = new prc_podetrocarprestadora();
         objprc_podetrocarprestadora.AV10Codigo = aP0_Codigo;
         objprc_podetrocarprestadora.AV9UserId = aP1_UserId;
         objprc_podetrocarprestadora.AV8PodeTrocar = false ;
         objprc_podetrocarprestadora.context.SetSubmitInitialConfig(context);
         objprc_podetrocarprestadora.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_podetrocarprestadora);
         aP2_PodeTrocar=this.AV8PodeTrocar;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_podetrocarprestadora)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV13GXLvl1 = 0;
         /* Using cursor P00TL2 */
         pr_default.execute(0, new Object[] {AV10Codigo, AV9UserId});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A508ContagemResultado_Owner = P00TL2_A508ContagemResultado_Owner[0];
            n508ContagemResultado_Owner = P00TL2_n508ContagemResultado_Owner[0];
            A892LogResponsavel_DemandaCod = P00TL2_A892LogResponsavel_DemandaCod[0];
            n892LogResponsavel_DemandaCod = P00TL2_n892LogResponsavel_DemandaCod[0];
            A1234LogResponsavel_NovoStatus = P00TL2_A1234LogResponsavel_NovoStatus[0];
            n1234LogResponsavel_NovoStatus = P00TL2_n1234LogResponsavel_NovoStatus[0];
            A1130LogResponsavel_Status = P00TL2_A1130LogResponsavel_Status[0];
            n1130LogResponsavel_Status = P00TL2_n1130LogResponsavel_Status[0];
            A1797LogResponsavel_Codigo = P00TL2_A1797LogResponsavel_Codigo[0];
            A508ContagemResultado_Owner = P00TL2_A508ContagemResultado_Owner[0];
            n508ContagemResultado_Owner = P00TL2_n508ContagemResultado_Owner[0];
            AV13GXLvl1 = 1;
            AV8PodeTrocar = (bool)((StringUtil.StrCmp(A1130LogResponsavel_Status, "S")==0)||(StringUtil.StrCmp(A1234LogResponsavel_NovoStatus, "S")==0));
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            pr_default.readNext(0);
         }
         pr_default.close(0);
         if ( AV13GXLvl1 == 0 )
         {
            /* Using cursor P00TL3 */
            pr_default.execute(1, new Object[] {AV10Codigo});
            while ( (pr_default.getStatus(1) != 101) )
            {
               A1553ContagemResultado_CntSrvCod = P00TL3_A1553ContagemResultado_CntSrvCod[0];
               n1553ContagemResultado_CntSrvCod = P00TL3_n1553ContagemResultado_CntSrvCod[0];
               A1603ContagemResultado_CntCod = P00TL3_A1603ContagemResultado_CntCod[0];
               n1603ContagemResultado_CntCod = P00TL3_n1603ContagemResultado_CntCod[0];
               A1234LogResponsavel_NovoStatus = P00TL3_A1234LogResponsavel_NovoStatus[0];
               n1234LogResponsavel_NovoStatus = P00TL3_n1234LogResponsavel_NovoStatus[0];
               A1130LogResponsavel_Status = P00TL3_A1130LogResponsavel_Status[0];
               n1130LogResponsavel_Status = P00TL3_n1130LogResponsavel_Status[0];
               A892LogResponsavel_DemandaCod = P00TL3_A892LogResponsavel_DemandaCod[0];
               n892LogResponsavel_DemandaCod = P00TL3_n892LogResponsavel_DemandaCod[0];
               A1797LogResponsavel_Codigo = P00TL3_A1797LogResponsavel_Codigo[0];
               A1553ContagemResultado_CntSrvCod = P00TL3_A1553ContagemResultado_CntSrvCod[0];
               n1553ContagemResultado_CntSrvCod = P00TL3_n1553ContagemResultado_CntSrvCod[0];
               A1603ContagemResultado_CntCod = P00TL3_A1603ContagemResultado_CntCod[0];
               n1603ContagemResultado_CntCod = P00TL3_n1603ContagemResultado_CntCod[0];
               /* Using cursor P00TL4 */
               pr_default.execute(2, new Object[] {n1603ContagemResultado_CntCod, A1603ContagemResultado_CntCod, AV9UserId});
               while ( (pr_default.getStatus(2) != 101) )
               {
                  A1078ContratoGestor_ContratoCod = P00TL4_A1078ContratoGestor_ContratoCod[0];
                  A1079ContratoGestor_UsuarioCod = P00TL4_A1079ContratoGestor_UsuarioCod[0];
                  AV8PodeTrocar = (bool)((StringUtil.StrCmp(A1130LogResponsavel_Status, "S")==0)||(StringUtil.StrCmp(A1234LogResponsavel_NovoStatus, "S")==0));
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
                  /* Exiting from a For First loop. */
                  if (true) break;
               }
               pr_default.close(2);
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               pr_default.readNext(1);
            }
            pr_default.close(1);
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00TL2_A508ContagemResultado_Owner = new int[1] ;
         P00TL2_n508ContagemResultado_Owner = new bool[] {false} ;
         P00TL2_A892LogResponsavel_DemandaCod = new int[1] ;
         P00TL2_n892LogResponsavel_DemandaCod = new bool[] {false} ;
         P00TL2_A1234LogResponsavel_NovoStatus = new String[] {""} ;
         P00TL2_n1234LogResponsavel_NovoStatus = new bool[] {false} ;
         P00TL2_A1130LogResponsavel_Status = new String[] {""} ;
         P00TL2_n1130LogResponsavel_Status = new bool[] {false} ;
         P00TL2_A1797LogResponsavel_Codigo = new long[1] ;
         A1234LogResponsavel_NovoStatus = "";
         A1130LogResponsavel_Status = "";
         P00TL3_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P00TL3_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P00TL3_A1603ContagemResultado_CntCod = new int[1] ;
         P00TL3_n1603ContagemResultado_CntCod = new bool[] {false} ;
         P00TL3_A1234LogResponsavel_NovoStatus = new String[] {""} ;
         P00TL3_n1234LogResponsavel_NovoStatus = new bool[] {false} ;
         P00TL3_A1130LogResponsavel_Status = new String[] {""} ;
         P00TL3_n1130LogResponsavel_Status = new bool[] {false} ;
         P00TL3_A892LogResponsavel_DemandaCod = new int[1] ;
         P00TL3_n892LogResponsavel_DemandaCod = new bool[] {false} ;
         P00TL3_A1797LogResponsavel_Codigo = new long[1] ;
         P00TL4_A1078ContratoGestor_ContratoCod = new int[1] ;
         P00TL4_A1079ContratoGestor_UsuarioCod = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_podetrocarprestadora__default(),
            new Object[][] {
                new Object[] {
               P00TL2_A508ContagemResultado_Owner, P00TL2_n508ContagemResultado_Owner, P00TL2_A892LogResponsavel_DemandaCod, P00TL2_n892LogResponsavel_DemandaCod, P00TL2_A1234LogResponsavel_NovoStatus, P00TL2_n1234LogResponsavel_NovoStatus, P00TL2_A1130LogResponsavel_Status, P00TL2_n1130LogResponsavel_Status, P00TL2_A1797LogResponsavel_Codigo
               }
               , new Object[] {
               P00TL3_A1553ContagemResultado_CntSrvCod, P00TL3_n1553ContagemResultado_CntSrvCod, P00TL3_A1603ContagemResultado_CntCod, P00TL3_n1603ContagemResultado_CntCod, P00TL3_A1234LogResponsavel_NovoStatus, P00TL3_n1234LogResponsavel_NovoStatus, P00TL3_A1130LogResponsavel_Status, P00TL3_n1130LogResponsavel_Status, P00TL3_A892LogResponsavel_DemandaCod, P00TL3_n892LogResponsavel_DemandaCod,
               P00TL3_A1797LogResponsavel_Codigo
               }
               , new Object[] {
               P00TL4_A1078ContratoGestor_ContratoCod, P00TL4_A1079ContratoGestor_UsuarioCod
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV13GXLvl1 ;
      private int AV10Codigo ;
      private int AV9UserId ;
      private int A508ContagemResultado_Owner ;
      private int A892LogResponsavel_DemandaCod ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A1603ContagemResultado_CntCod ;
      private int A1078ContratoGestor_ContratoCod ;
      private int A1079ContratoGestor_UsuarioCod ;
      private long A1797LogResponsavel_Codigo ;
      private String scmdbuf ;
      private String A1234LogResponsavel_NovoStatus ;
      private String A1130LogResponsavel_Status ;
      private bool AV8PodeTrocar ;
      private bool n508ContagemResultado_Owner ;
      private bool n892LogResponsavel_DemandaCod ;
      private bool n1234LogResponsavel_NovoStatus ;
      private bool n1130LogResponsavel_Status ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n1603ContagemResultado_CntCod ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00TL2_A508ContagemResultado_Owner ;
      private bool[] P00TL2_n508ContagemResultado_Owner ;
      private int[] P00TL2_A892LogResponsavel_DemandaCod ;
      private bool[] P00TL2_n892LogResponsavel_DemandaCod ;
      private String[] P00TL2_A1234LogResponsavel_NovoStatus ;
      private bool[] P00TL2_n1234LogResponsavel_NovoStatus ;
      private String[] P00TL2_A1130LogResponsavel_Status ;
      private bool[] P00TL2_n1130LogResponsavel_Status ;
      private long[] P00TL2_A1797LogResponsavel_Codigo ;
      private int[] P00TL3_A1553ContagemResultado_CntSrvCod ;
      private bool[] P00TL3_n1553ContagemResultado_CntSrvCod ;
      private int[] P00TL3_A1603ContagemResultado_CntCod ;
      private bool[] P00TL3_n1603ContagemResultado_CntCod ;
      private String[] P00TL3_A1234LogResponsavel_NovoStatus ;
      private bool[] P00TL3_n1234LogResponsavel_NovoStatus ;
      private String[] P00TL3_A1130LogResponsavel_Status ;
      private bool[] P00TL3_n1130LogResponsavel_Status ;
      private int[] P00TL3_A892LogResponsavel_DemandaCod ;
      private bool[] P00TL3_n892LogResponsavel_DemandaCod ;
      private long[] P00TL3_A1797LogResponsavel_Codigo ;
      private int[] P00TL4_A1078ContratoGestor_ContratoCod ;
      private int[] P00TL4_A1079ContratoGestor_UsuarioCod ;
      private bool aP2_PodeTrocar ;
   }

   public class prc_podetrocarprestadora__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00TL2 ;
          prmP00TL2 = new Object[] {
          new Object[] {"@AV10Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV9UserId",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00TL3 ;
          prmP00TL3 = new Object[] {
          new Object[] {"@AV10Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00TL4 ;
          prmP00TL4 = new Object[] {
          new Object[] {"@ContagemResultado_CntCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV9UserId",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00TL2", "SELECT TOP 1 T2.[ContagemResultado_Owner], T1.[LogResponsavel_DemandaCod] AS LogResponsavel_DemandaCod, T1.[LogResponsavel_NovoStatus], T1.[LogResponsavel_Status], T1.[LogResponsavel_Codigo] FROM ([LogResponsavel] T1 WITH (NOLOCK) LEFT JOIN [ContagemResultado] T2 WITH (NOLOCK) ON T2.[ContagemResultado_Codigo] = T1.[LogResponsavel_DemandaCod]) WHERE (T1.[LogResponsavel_DemandaCod] = @AV10Codigo) AND (T2.[ContagemResultado_Owner] = @AV9UserId) ORDER BY T1.[LogResponsavel_DemandaCod], T1.[LogResponsavel_Codigo] DESC ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00TL2,1,0,false,true )
             ,new CursorDef("P00TL3", "SELECT TOP 1 T2.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T3.[Contrato_Codigo] AS ContagemResultado_CntCod, T1.[LogResponsavel_NovoStatus], T1.[LogResponsavel_Status], T1.[LogResponsavel_DemandaCod] AS LogResponsavel_DemandaCod, T1.[LogResponsavel_Codigo] FROM (([LogResponsavel] T1 WITH (NOLOCK) LEFT JOIN [ContagemResultado] T2 WITH (NOLOCK) ON T2.[ContagemResultado_Codigo] = T1.[LogResponsavel_DemandaCod]) LEFT JOIN [ContratoServicos] T3 WITH (NOLOCK) ON T3.[ContratoServicos_Codigo] = T2.[ContagemResultado_CntSrvCod]) WHERE T1.[LogResponsavel_DemandaCod] = @AV10Codigo ORDER BY T1.[LogResponsavel_DemandaCod], T1.[LogResponsavel_Codigo] DESC ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00TL3,1,0,true,true )
             ,new CursorDef("P00TL4", "SELECT TOP 1 [ContratoGestor_ContratoCod], [ContratoGestor_UsuarioCod] FROM [ContratoGestor] WITH (NOLOCK) WHERE [ContratoGestor_ContratoCod] = @ContagemResultado_CntCod and [ContratoGestor_UsuarioCod] = @AV9UserId ORDER BY [ContratoGestor_ContratoCod], [ContratoGestor_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00TL4,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getString(3, 1) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((String[]) buf[6])[0] = rslt.getString(4, 1) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((long[]) buf[8])[0] = rslt.getLong(5) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getString(3, 1) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((String[]) buf[6])[0] = rslt.getString(4, 1) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((int[]) buf[8])[0] = rslt.getInt(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((long[]) buf[10])[0] = rslt.getLong(6) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
       }
    }

 }

}
